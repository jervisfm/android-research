package com.chase.sig.analytics;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.text.SpannedString;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.activity.eb;
import com.chase.sig.android.view.JPSpinner;
import com.chase.sig.android.view.ai;
import com.chase.sig.android.view.aj;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;

public class AspectAnalyticsUtil
{
  private static final String ACCT_ID = "accountId";
  private static final String CARRIER_NETWORK = "CARRIER_NETWORK";
  private static final String EPAY_ACCT_ID = "epayAccountId";
  private static final String QP_SELECTED_ACCT_ID = "selectedAccountId";
  private static final String TRACKING_DATA = "trackingData";
  private static final String TRANSFER_ACTIVITY_PREFIX = "Transfer";
  protected static final String UNRECOGNIZABLE = "Unrecognized widget clicked!";
  protected static final String UNRECOGNIZABLE_SCREEN = "Unrecognized Screen";
  private static final String WIFI = "WIFI";
  private static EditText currentEditText = null;
  public static String iServiceErrorCode = "";
  private static ScreenAnalyticsData lastVisitedScreen = null;

  private static TextView a(ArrayList<View> paramArrayList)
  {
    Iterator localIterator = paramArrayList.iterator();
    View localView;
    do
    {
      if (!localIterator.hasNext())
        return null;
      localView = (View)localIterator.next();
    }
    while (!(localView instanceof TextView));
    return (TextView)localView;
  }

  public static ScreenAnalyticsData a()
  {
    return lastVisitedScreen;
  }

  public static String a(View paramView)
  {
    String str = (String)paramView.getContentDescription();
    if (str == null)
    {
      if (!(paramView instanceof EditText))
        break label39;
      str = (String)((EditText)paramView).getHint();
    }
    while (true)
    {
      if (str == null)
        str = "Unrecognized widget clicked!";
      return str;
      label39: if ((paramView instanceof CheckBox))
        str = (String)((CheckBox)paramView).getText();
      else if ((paramView instanceof JPSpinner))
        str = ((JPSpinner)paramView).getHint();
      else if ((paramView instanceof Button))
        str = a((TextView)paramView);
      else if ((paramView instanceof ImageView))
        str = d(paramView);
      else if (c(paramView))
        str = a(a(a(paramView, new ArrayList())));
    }
  }

  public static String a(AdapterView<?> paramAdapterView, int paramInt)
  {
    String str = ((ai)paramAdapterView.getItemAtPosition(paramInt)).b;
    if (str == null)
      str = "Unrecognized widget clicked!";
    return str;
  }

  private static String a(TextView paramTextView)
  {
    if ((paramTextView.getText() instanceof SpannedString))
      return ((SpannedString)paramTextView.getText()).toString();
    return (String)paramTextView.getText();
  }

  public static ArrayList<View> a(View paramView, ArrayList<View> paramArrayList)
  {
    int j;
    if (c(paramView))
    {
      int i = ((ViewGroup)paramView).getChildCount();
      j = 0;
      if (j < i);
    }
    else
    {
      return paramArrayList;
    }
    View localView = ((ViewGroup)paramView).getChildAt(j);
    if (c(localView))
      a(localView, paramArrayList);
    while (true)
    {
      j++;
      break;
      paramArrayList.add(localView);
    }
  }

  public static void a(eb parameb)
  {
    String str = parameb.getClass().getSimpleName();
    ScreenAnalyticsData localScreenAnalyticsData;
    Display localDisplay;
    int i;
    if (!d(str))
    {
      localScreenAnalyticsData = new ScreenAnalyticsData(str);
      localDisplay = parameb.getWindowManager().getDefaultDisplay();
      if (localDisplay.getWidth() != localDisplay.getHeight())
        break label59;
      i = 3;
    }
    while (true)
    {
      localScreenAnalyticsData.a(i);
      lastVisitedScreen = localScreenAnalyticsData;
      return;
      label59: if (localDisplay.getWidth() < localDisplay.getHeight())
        i = 1;
      else
        i = 2;
    }
  }

  public static void a(String paramString)
  {
    if ((paramString != null) && (!paramString.equalsIgnoreCase("Unrecognized widget clicked!")))
    {
      FieldAnalyticsData localFieldAnalyticsData = new FieldAnalyticsData(paramString);
      lastVisitedScreen.a(localFieldAnalyticsData);
    }
  }

  public static void a(Hashtable<String, String> paramHashtable)
  {
    TrackingData localTrackingData = TrackingData.a();
    String str1;
    if (paramHashtable.containsKey("epayAccountId"))
      str1 = (String)paramHashtable.get("epayAccountId");
    while (true)
    {
      if (str1 != null)
      {
        ArrayList localArrayList = TrackingData.a().b();
        if ((localArrayList != null) && (localArrayList.size() != 0))
        {
          ScreenAnalyticsData localScreenAnalyticsData2 = (ScreenAnalyticsData)localArrayList.get(-1 + localArrayList.size());
          if (!localScreenAnalyticsData2.a().startsWith("Transfer"))
            localScreenAnalyticsData2.a(str1);
        }
      }
      try
      {
        String str2 = localTrackingData.c();
        paramHashtable.put("trackingData", str2);
        TrackingData.a().d();
        ScreenAnalyticsData localScreenAnalyticsData1 = lastVisitedScreen;
        if (localScreenAnalyticsData1 != null);
        return;
        if (paramHashtable.containsKey("selectedAccountId"))
          str1 = (String)paramHashtable.get("selectedAccountId");
        else if (paramHashtable.containsKey("accountId"))
          str1 = (String)paramHashtable.get("accountId");
      }
      finally
      {
      }
    }
  }

  public static boolean a(EditText paramEditText)
  {
    if ((currentEditText == null) || (currentEditText != paramEditText))
    {
      currentEditText = paramEditText;
      return true;
    }
    return false;
  }

  public static boolean a(EditText paramEditText, KeyEvent paramKeyEvent, int paramInt)
  {
    if (a(paramEditText))
    {
      switch (paramInt)
      {
      default:
      case 19:
      case 20:
      case 21:
      case 22:
      case 23:
      case 66:
      }
      for (int i = 1; (i != 0) && (paramKeyEvent.getAction() == 1); i = 0)
        return true;
    }
    return false;
  }

  public static String b(View paramView)
  {
    boolean bool1 = paramView instanceof aj;
    String str1 = null;
    ai localai;
    if (bool1)
    {
      localai = ((aj)paramView).getDetails();
      String str2 = localai.b;
      boolean bool2 = localai.f;
      str1 = null;
      if (bool2)
        if ((str2.equalsIgnoreCase("Business Accounts")) || (str2.equalsIgnoreCase("Personal Accounts")))
          break label77;
    }
    label77: for (int i = 0; ; i = 1)
    {
      str1 = null;
      if (i == 0)
        str1 = localai.d;
      return str1;
    }
  }

  public static void b()
  {
    if (((WifiManager)ChaseApplication.a().getApplicationContext().getSystemService("wifi")).getConnectionInfo().getBSSID() != null)
    {
      TrackingData.a().a("WIFI");
      return;
    }
    TrackingData.a().a("CARRIER_NETWORK");
  }

  public static void b(eb parameb)
  {
    if (!d(parameb.getClass().getSimpleName()))
    {
      TextView localTextView = parameb.o();
      if ((localTextView != null) && (localTextView.getText() != null))
      {
        String str = localTextView.getText().toString();
        ScreenAnalyticsData localScreenAnalyticsData = lastVisitedScreen;
        if (str == null)
          str = "";
        localScreenAnalyticsData.b(str);
      }
      TrackingData.a().a(lastVisitedScreen);
    }
  }

  public static void b(String paramString)
  {
    ErrorAnalyticsData localErrorAnalyticsData = new ErrorAnalyticsData(iServiceErrorCode, paramString);
    lastVisitedScreen.a(localErrorAnalyticsData);
    iServiceErrorCode = "";
  }

  public static void c(String paramString)
  {
    iServiceErrorCode = paramString;
  }

  private static boolean c(View paramView)
  {
    return ((paramView instanceof RelativeLayout)) || ((paramView instanceof LinearLayout)) || ((paramView instanceof ScrollView)) || ((paramView instanceof ListView)) || ((paramView instanceof FrameLayout));
  }

  // ERROR //
  private static String d(View paramView)
  {
    // Byte code:
    //   0: ldc_w 323
    //   3: astore_1
    //   4: aload_0
    //   5: invokevirtual 327	android/view/View:getResources	()Landroid/content/res/Resources;
    //   8: aload_0
    //   9: invokevirtual 330	android/view/View:getId	()I
    //   12: invokevirtual 336	android/content/res/Resources:getResourceName	(I)Ljava/lang/String;
    //   15: astore 4
    //   17: aload 4
    //   19: invokestatic 341	com/chase/sig/android/util/s:m	(Ljava/lang/String;)Z
    //   22: ifeq +41 -> 63
    //   25: new 343	java/lang/StringBuilder
    //   28: dup
    //   29: aload 4
    //   31: aload 4
    //   33: bipush 47
    //   35: invokevirtual 347	java/lang/String:lastIndexOf	(I)I
    //   38: invokevirtual 350	java/lang/String:substring	(I)Ljava/lang/String;
    //   41: invokespecial 351	java/lang/StringBuilder:<init>	(Ljava/lang/String;)V
    //   44: astore 5
    //   46: aload 5
    //   48: aload_1
    //   49: invokevirtual 355	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   52: pop
    //   53: aload 5
    //   55: invokevirtual 356	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   58: astore 7
    //   60: aload 7
    //   62: astore_1
    //   63: aload_1
    //   64: areturn
    //   65: astore_3
    //   66: aload_1
    //   67: areturn
    //   68: astore_2
    //   69: aload_1
    //   70: areturn
    //
    // Exception table:
    //   from	to	target	type
    //   4	60	65	finally
    //   4	60	68	java/lang/Exception
  }

  private static boolean d(String paramString)
  {
    ArrayList localArrayList = TrackingData.a().b();
    boolean bool1 = localArrayList.isEmpty();
    boolean bool2 = false;
    if (!bool1)
      bool2 = ((ScreenAnalyticsData)localArrayList.get(-1 + localArrayList.size())).a().equalsIgnoreCase(paramString);
    return bool2;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.analytics.AspectAnalyticsUtil
 * JD-Core Version:    0.6.2
 */