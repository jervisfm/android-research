package com.chase.sig.analytics;

import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import com.chase.sig.android.activity.eb;
import com.chase.sig.android.service.IServiceError;
import com.chase.sig.android.util.s;
import com.chase.sig.android.view.aj;
import com.chase.sig.android.view.k.a;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;

public class BehaviorAnalyticsAspect
{
  private static final String GWO_PATH = "/gws/";

  static
  {
    try
    {
      ajc$perSingletonInstance = new BehaviorAnalyticsAspect();
      return;
    }
    catch (Throwable localThrowable)
    {
      ajc$initFailureCause = localThrowable;
    }
  }

  public static BehaviorAnalyticsAspect a()
  {
    if (ajc$perSingletonInstance == null)
      throw new a.a.a.a("com_chase_sig_analytics_BehaviorAnalyticsAspect", ajc$initFailureCause);
    return ajc$perSingletonInstance;
  }

  public static void a(View paramView)
  {
    AspectAnalyticsUtil.a(AspectAnalyticsUtil.a(paramView));
  }

  public static void a(View paramView, int paramInt, KeyEvent paramKeyEvent)
  {
    String str;
    if (((paramView instanceof CheckBox)) && (paramInt == 23) && (paramKeyEvent.getAction() == 0))
      str = AspectAnalyticsUtil.a(paramView);
    while (true)
    {
      AspectAnalyticsUtil.a(str);
      return;
      if (((paramView instanceof EditText)) && (AspectAnalyticsUtil.a((EditText)paramView, paramKeyEvent, paramInt)))
        str = AspectAnalyticsUtil.a(paramView);
      else
        str = null;
    }
  }

  public static void a(View paramView, MotionEvent paramMotionEvent)
  {
    String str;
    if (paramMotionEvent.getAction() == 1)
      if ((paramView instanceof EditText))
      {
        if (!AspectAnalyticsUtil.a((EditText)paramView))
          break label50;
        str = AspectAnalyticsUtil.a(paramView);
      }
    while (true)
    {
      AspectAnalyticsUtil.a(str);
      return;
      if ((paramView instanceof CheckBox))
        str = AspectAnalyticsUtil.a(paramView);
      else
        label50: str = null;
    }
  }

  public static void a(AdapterView<?> paramAdapterView, View paramView, int paramInt)
  {
    if ((paramView instanceof aj));
    for (String str1 = AspectAnalyticsUtil.a(paramAdapterView, paramInt); ; str1 = AspectAnalyticsUtil.a(paramView))
    {
      if (!str1.equalsIgnoreCase("Unrecognized widget clicked!"))
      {
        ScreenAnalyticsData localScreenAnalyticsData = AspectAnalyticsUtil.a();
        String str2 = AspectAnalyticsUtil.b(paramView);
        if (str2 != null)
          localScreenAnalyticsData.a(str2);
        localScreenAnalyticsData.a(new FieldAnalyticsData(str1));
      }
      return;
    }
  }

  public static void a(eb parameb)
  {
    AspectAnalyticsUtil.a(parameb);
  }

  public static void a(IServiceError paramIServiceError)
  {
    AspectAnalyticsUtil.c(paramIServiceError.c());
  }

  public static void a(com.chase.sig.android.view.detail.a parama, View paramView)
  {
    String str = (String)((TextView)paramView.findViewById(2131296277)).getText();
    if (s.l(str))
      str = parama.i();
    View localView = paramView.findViewById(parama.e());
    if (localView != null)
      localView.setContentDescription(str);
  }

  public static void a(k.a parama)
  {
    AspectAnalyticsUtil.b(parama.c.toString());
  }

  public static void a(String paramString, Hashtable<String, String> paramHashtable)
  {
    a(paramHashtable, paramString);
  }

  private static void a(Hashtable<String, String> paramHashtable, String paramString)
  {
    if (paramString.contains("/gws/"))
    {
      AspectAnalyticsUtil.b();
      AspectAnalyticsUtil.a(paramHashtable);
    }
  }

  public static void b(eb parameb)
  {
    AspectAnalyticsUtil.b(parameb);
  }

  public static void b(IServiceError paramIServiceError)
  {
    AspectAnalyticsUtil.c(paramIServiceError.c());
  }

  public static void b(String paramString, Hashtable<String, String> paramHashtable)
  {
    a(paramHashtable, paramString);
  }

  public static void c(String paramString, Hashtable<String, String> paramHashtable)
  {
    a(paramHashtable, paramString);
  }

  public final void a(com.chase.sig.android.view.detail.a parama)
  {
    View localView = parama.k().findViewById(parama.e());
    if (localView != null)
    {
      localView.setOnTouchListener(new a(this));
      localView.setOnKeyListener(new b(this));
    }
  }

  public final void c(eb parameb)
  {
    Iterator localIterator = AspectAnalyticsUtil.a(parameb.n(), new ArrayList()).iterator();
    while (true)
    {
      if (!localIterator.hasNext())
        return;
      View localView = (View)localIterator.next();
      if (((localView instanceof EditText)) || ((localView instanceof CheckBox)))
      {
        localView.setOnTouchListener(new c(this));
        localView.setOnKeyListener(new d(this));
      }
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.analytics.BehaviorAnalyticsAspect
 * JD-Core Version:    0.6.2
 */