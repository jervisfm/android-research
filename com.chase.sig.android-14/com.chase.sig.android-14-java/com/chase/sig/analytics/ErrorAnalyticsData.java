package com.chase.sig.analytics;

public class ErrorAnalyticsData
{
  private int code = -1;
  private String message = "";

  public ErrorAnalyticsData(String paramString1, String paramString2)
  {
    this.message = paramString2;
    if ((paramString1 != null) && (!paramString1.equalsIgnoreCase("null")) && (!paramString1.equalsIgnoreCase("LOCAL")) && (!paramString1.equalsIgnoreCase("")))
      this.code = Integer.valueOf(paramString1).intValue();
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.analytics.ErrorAnalyticsData
 * JD-Core Version:    0.6.2
 */