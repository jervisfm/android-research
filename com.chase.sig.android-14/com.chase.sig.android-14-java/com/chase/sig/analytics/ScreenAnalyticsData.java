package com.chase.sig.analytics;

import java.util.ArrayList;
import java.util.Date;

public class ScreenAnalyticsData
{
  private static final String LANDSCAPE = "landscape";
  private static final String PORTRAIT = "portrait";
  private String accountId;
  private ArrayList<AttributeAnalyticsData> attributes;
  private ArrayList<ErrorAnalyticsData> errors;
  private ArrayList<FieldAnalyticsData> fields;
  private String name;
  private long timestamp = new Date().getTime();
  private String title;

  public ScreenAnalyticsData(String paramString)
  {
    this.name = paramString;
    this.attributes = new ArrayList();
    this.fields = new ArrayList();
    this.errors = new ArrayList();
  }

  public final String a()
  {
    return this.name;
  }

  public final void a(int paramInt)
  {
    if (paramInt == 2);
    for (String str = "landscape"; ; str = "portrait")
    {
      AttributeAnalyticsData localAttributeAnalyticsData = new AttributeAnalyticsData("interfaceOrientation", str);
      this.attributes.add(localAttributeAnalyticsData);
      return;
    }
  }

  public final void a(ErrorAnalyticsData paramErrorAnalyticsData)
  {
    this.errors.add(paramErrorAnalyticsData);
  }

  public final void a(FieldAnalyticsData paramFieldAnalyticsData)
  {
    this.fields.add(paramFieldAnalyticsData);
  }

  public final void a(String paramString)
  {
    this.accountId = paramString;
  }

  public final void b()
  {
    this.attributes.clear();
    this.errors.clear();
    this.fields.clear();
  }

  public final void b(String paramString)
  {
    if (this.title == null)
      this.title = paramString;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.analytics.ScreenAnalyticsData
 * JD-Core Version:    0.6.2
 */