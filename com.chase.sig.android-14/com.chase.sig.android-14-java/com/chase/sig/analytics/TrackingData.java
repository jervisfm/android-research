package com.chase.sig.analytics;

import com.chase.sig.android.ChaseApplication;
import com.google.gson.chase.Gson;
import com.google.gson.chase.GsonBuilder;
import java.util.ArrayList;

public class TrackingData
{
  private static TrackingData trackingData = new TrackingData();
  private ArrayList<AttributeAnalyticsData> attributes = new ArrayList();
  private String bandwidth = "WIFI";
  private String channelId = ChaseApplication.a().h();
  private ArrayList<ScreenAnalyticsData> screens = new ArrayList();

  public static TrackingData a()
  {
    return trackingData;
  }

  public final void a(ScreenAnalyticsData paramScreenAnalyticsData)
  {
    this.screens.add(paramScreenAnalyticsData);
  }

  public final void a(String paramString)
  {
    this.bandwidth = paramString;
  }

  public final ArrayList<ScreenAnalyticsData> b()
  {
    return this.screens;
  }

  public final String c()
  {
    return new GsonBuilder().a().b().a(this);
  }

  public final void d()
  {
    this.screens.clear();
    this.attributes.clear();
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.analytics.TrackingData
 * JD-Core Version:    0.6.2
 */