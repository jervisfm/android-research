package com.chase.sig.analytics;

import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnKeyListener;

final class d
  implements View.OnKeyListener
{
  d(BehaviorAnalyticsAspect paramBehaviorAnalyticsAspect)
  {
  }

  public final boolean onKey(View paramView, int paramInt, KeyEvent paramKeyEvent)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView, paramInt, paramKeyEvent);
    return false;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.analytics.d
 * JD-Core Version:    0.6.2
 */