package com.chase.sig.android;

import android.app.Application;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import com.chase.sig.android.activity.AccountsActivity;
import com.chase.sig.android.domain.MobileAdResponse;
import com.chase.sig.android.domain.o;
import com.chase.sig.android.util.s;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.Properties;
import java.util.UUID;

public class ChaseApplication extends Application
{
  public static boolean a = false;
  private static Hashtable<String, Object> b = new Hashtable();
  private static Properties c;
  private static ChaseApplication d;
  private static f e;
  private static a f;
  private o g;
  private MobileAdResponse h;

  public ChaseApplication()
  {
    d = this;
  }

  public static ChaseApplication a()
  {
    return d;
  }

  public static void a(a parama)
  {
    f = parama;
  }

  public static void a(f paramf)
  {
    e = paramf;
  }

  public static void n()
  {
    if (e != null)
      f.a();
  }

  public static f o()
  {
    return e;
  }

  public static a p()
  {
    return f;
  }

  public static void u()
  {
    if (e != null)
    {
      e.interrupt();
      e = null;
    }
  }

  public final String a(String paramString)
  {
    if (c == null);
    try
    {
      InputStream localInputStream = getAssets().open("application_config.properties");
      Properties localProperties = new Properties();
      c = localProperties;
      localProperties.load(localInputStream);
      label36: return c.getProperty(paramString);
    }
    catch (Exception localException)
    {
      break label36;
    }
  }

  public final void a(MobileAdResponse paramMobileAdResponse)
  {
    this.h = paramMobileAdResponse;
  }

  public final void a(o paramo)
  {
    this.g = paramo;
  }

  public final void a(String paramString1, String paramString2)
  {
    SharedPreferences.Editor localEditor = getSharedPreferences("application.preferences", 0).edit();
    localEditor.putString("CURRENT_ENVIRONMENT", paramString1);
    if (paramString2 != null)
      localEditor.putString("CURRENT_ENVIRONMENT_HOSTNAME", paramString2);
    localEditor.commit();
  }

  public final void a(boolean paramBoolean)
  {
    SharedPreferences.Editor localEditor = getSharedPreferences("application.preferences", 0).edit();
    localEditor.putBoolean("can_auto_focus", paramBoolean);
    localEditor.commit();
  }

  public final o b()
  {
    if (this.g == null)
      return new o();
    return this.g;
  }

  public final void b(String paramString)
  {
    a(paramString, null);
  }

  public final void b(boolean paramBoolean)
  {
    a = paramBoolean;
    this.h = null;
    if (AccountsActivity.a != null)
    {
      AccountsActivity.a.recycle();
      AccountsActivity.a = null;
    }
  }

  public final String c(String paramString)
  {
    String str1 = a("environment." + f() + "." + paramString);
    String str2 = g();
    if ((str1 != null) && (str2 != null))
      str1 = str1.replace("_HOST_AND_PORT_", str2);
    return str1;
  }

  public final boolean c()
  {
    return (this.g != null) && (this.g.a != null) && (this.g.a.b());
  }

  public final String d()
  {
    String str1 = getSharedPreferences("application.preferences", 0).getString("deviceId", null);
    if (str1 != null)
      return str1;
    String str2 = UUID.randomUUID().toString();
    new Object[] { str2 };
    SharedPreferences.Editor localEditor = getSharedPreferences("application.preferences", 0).edit();
    localEditor.putString("deviceId", str2);
    localEditor.commit();
    return str2;
  }

  public final SharedPreferences e()
  {
    return getSharedPreferences("application.preferences", 0);
  }

  public final String f()
  {
    SharedPreferences localSharedPreferences = getSharedPreferences("application.preferences", 0);
    if (localSharedPreferences.contains("CURRENT_ENVIRONMENT"))
      return localSharedPreferences.getString("CURRENT_ENVIRONMENT", null);
    return a("environment.default");
  }

  public final String g()
  {
    return getSharedPreferences("application.preferences", 0).getString("CURRENT_ENVIRONMENT_HOSTNAME", null);
  }

  public final String h()
  {
    return getResources().getString(2131165184);
  }

  public final boolean i()
  {
    return "PBD".equals(h());
  }

  public final boolean j()
  {
    String str = a("debug");
    return (s.m(str)) && (str.equals("true"));
  }

  public final String k()
  {
    String str = String.format("%s.%s.%s", new Object[] { getResources().getString(2131165185), getResources().getString(2131165186), getResources().getString(2131165187) });
    if (str.startsWith("%"))
      str = "0";
    return str;
  }

  public final Date l()
  {
    SharedPreferences localSharedPreferences = getSharedPreferences("application.preferences", 0);
    long l = localSharedPreferences.getLong("app_install_date", -1L);
    if (l < 1L)
    {
      l = new Date().getTime();
      SharedPreferences.Editor localEditor = localSharedPreferences.edit();
      localEditor.putLong("app_install_date", l);
      localEditor.commit();
    }
    return new Date(l);
  }

  public final String m()
  {
    return new SimpleDateFormat("yyy-MM-dd HH:mm:ss.S").format(l());
  }

  public void onCreate()
  {
    super.onCreate();
    l();
  }

  public void onLowMemory()
  {
    j();
  }

  public final void q()
  {
    this.g = null;
    new com.chase.sig.android.util.f(this).a();
    c.a();
    b(false);
  }

  public final boolean r()
  {
    return getSharedPreferences("application.preferences", 0).getBoolean("show_jpm_upgrade_prompt", true);
  }

  public final void s()
  {
    SharedPreferences.Editor localEditor = getSharedPreferences("application.preferences", 0).edit();
    localEditor.putBoolean("show_jpm_upgrade_prompt", false);
    localEditor.commit();
  }

  public final boolean t()
  {
    return getSharedPreferences("application.preferences", 0).getBoolean("can_auto_focus", true);
  }

  public final MobileAdResponse v()
  {
    return this.h;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.ChaseApplication
 * JD-Core Version:    0.6.2
 */