package com.chase.sig.android.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask.Status;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.activity.a.f;
import com.chase.sig.android.b;
import com.chase.sig.android.domain.AccountActivity;
import com.chase.sig.android.domain.ActivityValue;
import com.chase.sig.android.domain.IAccount;
import com.chase.sig.android.domain.g;
import com.chase.sig.android.service.AccountActivityResponse;
import com.chase.sig.android.service.SplashResponse;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class AccountActivityActivity extends ai
  implements fk.d
{
  private static String d;
  private static boolean k;
  private String a;
  private ListView b;
  private View c;
  private IAccount l;
  private String m;
  private a n;

  private String a(IAccount paramIAccount)
  {
    if (paramIAccount.g())
    {
      IAccount localIAccount = ((ChaseApplication)getApplication()).b().b.e(paramIAccount.b());
      Object[] arrayOfObject = new Object[2];
      arrayOfObject[0] = paramIAccount.a();
      arrayOfObject[1] = localIAccount.c();
      return String.format("%s (%s)", arrayOfObject);
    }
    return paramIAccount.w().toString();
  }

  private void a(List<AccountActivity> paramList)
  {
    this.n = new a(this, paramList);
    this.b = new ListView(this);
    if (k)
    {
      this.c = ((LayoutInflater)getSystemService("layout_inflater")).inflate(2130903137, null);
      this.c.setOnClickListener(new o(this));
      this.b.addFooterView(this.c);
    }
    ListView localListView = this.b;
    if ((localListView != null) && (ChaseApplication.a().i()))
    {
      View localView1 = getLayoutInflater().inflate(2130903089, null);
      localView1.setBackgroundResource(2131099656);
      localView1.setPadding(5, 10, 5, 10);
      View localView2 = localView1.findViewById(2131296435);
      localListView.addFooterView(localView1);
      TextView localTextView2 = (TextView)localView1.findViewById(2131296435);
      localView2.setOnClickListener(a(PrivateBankingDisclosuresActivity.class).a("title", localTextView2.getText().toString()));
    }
    this.b.setAdapter(this.n);
    this.b.setFooterDividersEnabled(false);
    this.b.setDividerHeight(0);
    this.b.setCacheColorHint(getResources().getColor(2131099680));
    this.b.requestFocus();
    if (((ChaseApplication)getApplication()).b().b.C())
    {
      if (!this.l.m())
        break label372;
      this.b.setOnItemClickListener(new b((byte)0));
    }
    while (true)
    {
      ViewGroup localViewGroup = (ViewGroup)findViewById(2131296259);
      IAccount localIAccount = ((ChaseApplication)getApplication()).b().b.a(this.a);
      TextView localTextView1 = (TextView)localViewGroup.findViewById(2131296260);
      this.m = a(localIAccount);
      localTextView1.setText(this.m);
      localTextView1.setVisibility(0);
      ViewGroup.LayoutParams localLayoutParams = new ViewGroup.LayoutParams(-1, -2);
      localViewGroup.addView(this.b, localLayoutParams);
      return;
      label372: this.b.setClickable(false);
    }
  }

  private boolean d()
  {
    return this.l.D();
  }

  public final void a(Bundle paramBundle)
  {
    setTitle(2131165438);
    b(2130903040);
    this.a = getIntent().getExtras().getString("selectedAccountId");
    this.l = ((ChaseApplication)getApplication()).b().b.a(this.a);
    k = false;
    if ((paramBundle != null) && (paramBundle.containsKey("activity")))
    {
      List localList = (List)paramBundle.getSerializable("activity");
      d = paramBundle.getString("activity_last_page");
      k = paramBundle.getBoolean("activity_has_more", false);
      a(localList);
      this.b.onRestoreInstanceState(paramBundle.getParcelable("activity_last_position"));
    }
    c localc;
    do
    {
      return;
      localc = (c)this.e.a(c.class);
    }
    while (localc.getStatus() == AsyncTask.Status.RUNNING);
    Object[] arrayOfObject = new Object[2];
    arrayOfObject[0] = this.a;
    arrayOfObject[1] = Boolean.valueOf(d());
    localc.execute(arrayOfObject);
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    if ((this.n != null) && (a.a(this.n) != null))
    {
      paramBundle.putSerializable("activity", (Serializable)a.a(this.n));
      paramBundle.putString("activity_last_page", d);
      paramBundle.putBoolean("activity_has_more", k);
      paramBundle.putInt("activity_last_position", this.b.getFirstVisiblePosition());
      paramBundle.putParcelable("activity_last_position", this.b.onSaveInstanceState());
    }
  }

  private final class a extends ArrayAdapter<AccountActivity>
  {
    private List<AccountActivity> b;

    public a(int arg2)
    {
      super(2130903045, localList);
      this.b = localList;
    }

    private RelativeLayout a(RelativeLayout paramRelativeLayout, ActivityValue paramActivityValue)
    {
      TextView localTextView1 = (TextView)paramRelativeLayout.findViewById(2131296278);
      TextView localTextView2 = (TextView)paramRelativeLayout.findViewById(2131296279);
      int i;
      if (paramActivityValue.d())
      {
        i = AccountActivityActivity.this.getResources().getColor(2131099669);
        localTextView2.setTextColor(i);
        localTextView1.setText(paramActivityValue.label);
        if (!paramActivityValue.a())
          break label94;
      }
      label94: for (String str = "N/A"; ; str = paramActivityValue.value)
      {
        localTextView2.setText(str);
        return paramRelativeLayout;
        i = AccountActivityActivity.this.getResources().getColor(2131099653);
        break;
      }
    }

    public final void a(Collection<AccountActivity> paramCollection)
    {
      this.b.addAll(paramCollection);
      notifyDataSetChanged();
    }

    public final View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      int i = 0;
      AccountActivity localAccountActivity = (AccountActivity)this.b.get(paramInt);
      ArrayList localArrayList = localAccountActivity.b();
      int j = localArrayList.size();
      LayoutInflater localLayoutInflater = (LayoutInflater)AccountActivityActivity.this.getSystemService("layout_inflater");
      if (paramView == null)
      {
        localObject = (LinearLayout)localLayoutInflater.inflate(2130903041, null);
        ViewGroup localViewGroup2 = (ViewGroup)((LinearLayout)localObject).findViewById(2131296264);
        View localView = ((LinearLayout)localObject).findViewById(2131296265);
        if ((AccountActivityActivity.e(AccountActivityActivity.this).m()) && (AccountActivityActivity.this.r().b.C()));
        for (int k = 0; ; k = 8)
        {
          localView.setVisibility(k);
          for (int m = 0; m < j; m++)
          {
            ActivityValue localActivityValue2 = (ActivityValue)localArrayList.get(m);
            localViewGroup2.addView(a((RelativeLayout)localLayoutInflater.inflate(2130903045, null), localActivityValue2));
          }
        }
      }
      LinearLayout localLinearLayout = (LinearLayout)paramView;
      ViewGroup localViewGroup1 = (ViewGroup)localLinearLayout.findViewById(2131296264);
      while (i < j)
      {
        RelativeLayout localRelativeLayout = (RelativeLayout)localViewGroup1.getChildAt(i);
        ActivityValue localActivityValue1 = (ActivityValue)localArrayList.get(i);
        if (localRelativeLayout == null)
        {
          localRelativeLayout = (RelativeLayout)localLayoutInflater.inflate(2130903045, null);
          localViewGroup1.addView(localRelativeLayout);
        }
        a(localRelativeLayout, localActivityValue1);
        i++;
      }
      Object localObject = localLinearLayout;
      ((TextView)((LinearLayout)localObject).findViewById(2131296263)).setText(localAccountActivity.a());
      return localObject;
    }
  }

  private final class b
    implements AdapterView.OnItemClickListener
  {
    private b()
    {
    }

    public final void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
    {
      try
      {
        if ((AccountActivityActivity.this.s() != null) && (AccountActivityActivity.this.s().l()))
          AccountActivityActivity.this.e(AccountActivityActivity.this.s().s());
        while (true)
        {
          BehaviorAnalyticsAspect.a();
          BehaviorAnalyticsAspect.a(paramAdapterView, paramView, paramInt);
          return;
          AccountActivity localAccountActivity = (AccountActivity)paramAdapterView.getItemAtPosition(paramInt);
          AccountActivityActivity.a(AccountActivityActivity.this, localAccountActivity);
        }
      }
      catch (Throwable localThrowable)
      {
        BehaviorAnalyticsAspect.a();
        BehaviorAnalyticsAspect.a(paramAdapterView, paramView, paramInt);
        throw localThrowable;
      }
    }
  }

  public static class c extends b<AccountActivityActivity, Object, Void, AccountActivityResponse>
  {
    protected void onCancelled()
    {
      ((AccountActivityActivity)this.b).finish();
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.AccountActivityActivity
 * JD-Core Version:    0.6.2
 */