package com.chase.sig.android.activity;

import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask.Status;
import android.os.Bundle;
import android.os.Handler.Callback;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.b;
import com.chase.sig.android.d;
import com.chase.sig.android.domain.Account.Value;
import com.chase.sig.android.domain.AccountDetail;
import com.chase.sig.android.domain.IAccount;
import com.chase.sig.android.domain.g;
import com.chase.sig.android.domain.o;
import com.chase.sig.android.domain.ui.Action;
import com.chase.sig.android.service.AccountDetailResponse;
import com.chase.sig.android.service.SmartService.SmartResponse;
import com.chase.sig.android.util.Dollar;
import com.chase.sig.android.util.s;
import com.chase.sig.android.view.detail.DetailRow;
import com.chase.sig.android.view.detail.DetailRowWithSubItem;
import com.chase.sig.android.view.detail.DetailView;
import com.chase.sig.android.view.detail.TogglableDetailRow;
import com.chase.sig.android.view.detail.a;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

public class AccountDetailActivity extends ai
  implements fk.d
{
  private DetailView a;
  private AccountDetail b;
  private IAccount c;
  private g d;
  private Handler.Callback k = new p(this);

  private IAccount a(IAccount paramIAccount)
  {
    return ((ChaseApplication)getApplication()).b().b.e(paramIAccount.b());
  }

  private String a(String paramString)
  {
    Hashtable localHashtable = this.c.e();
    if ((localHashtable != null) && (localHashtable.containsKey(paramString)))
      return (String)localHashtable.get(paramString);
    return null;
  }

  private String b(String paramString)
  {
    String str = a(paramString);
    if (s.l(str))
      return "--";
    return s.h(str);
  }

  private String c(String paramString)
  {
    String str = a(paramString);
    if (s.l(str))
      return "--";
    return new Dollar(str).h();
  }

  private void d()
  {
    switch (this.c.s())
    {
    case 8:
    case 9:
    default:
      finish();
      return;
    case 7:
      ArrayList localArrayList3 = new ArrayList();
      localArrayList3.add(new DetailRow(getResources().getString(2131165393), c("current")));
      localArrayList3.add(new DetailRow(getResources().getString(2131165394), c("available")));
      if (this.c.I())
      {
        String str4 = (String)this.c.e().get("txnCounter");
        String str5 = (String)this.c.e().get("txnCounterMsg");
        if (str4 == null)
        {
          com.chase.sig.android.domain.f localf2 = this.c.c("Withdrawals this period");
          if (localf2 != null)
          {
            str4 = localf2.c().b();
            str5 = localf2.b();
          }
        }
        localArrayList3.add(new DetailRowWithSubItem(getResources().getString(2131165434), str5, str4));
      }
      Serializable localSerializable = getIntent().getExtras().getSerializable("accountDetails");
      if (localSerializable != null)
      {
        List localList = (List)localSerializable;
        for (int i = 0; i < localList.size(); i++)
        {
          com.chase.sig.android.domain.f localf1 = (com.chase.sig.android.domain.f)localList.get(i);
          if (localf1.d().equals("SHOW_HIDE_LIST"))
          {
            String str3 = ((Action)localf1.e().get(0)).a();
            Bundle localBundle = new Bundle();
            localBundle.putSerializable("action", (Serializable)localf1.e().get(0));
            TogglableDetailRow localTogglableDetailRow = new TogglableDetailRow(str3, "", this.k, localBundle);
            localTogglableDetailRow.b = ("extraDetailRow" + Integer.toString(i));
            localArrayList3.add(localTogglableDetailRow);
          }
        }
      }
      a[] arrayOfa4 = new a[localArrayList3.size()];
      this.a.setRows((a[])localArrayList3.toArray(arrayOfa4));
      return;
    case 10:
      e();
      return;
    case 2:
    case 3:
      ArrayList localArrayList2 = new ArrayList();
      if (this.c.E())
      {
        if ((this.c.h()) && (h("spendingLimitFlag")))
          break label1147;
        localArrayList2.add(new DetailRow(getResources().getString(2131165402), c("current")));
        localArrayList2.add(new DetailRow(getResources().getString(2131165403), b("nextPaymentDate")));
        localArrayList2.add(new DetailRow(getResources().getString(2131165415), c("nextPaymentAmount")));
        localArrayList2.add(new DetailRow(getResources().getString(2131165405), c("lastStmtBalance")));
      }
      while (true)
      {
        if (this.c.l())
        {
          String str2 = c("goalPayAmountDue");
          localArrayList2.add(new DetailRowWithSubItem(getString(2131165423), getString(2131165424), str2));
        }
        a[] arrayOfa3 = new a[localArrayList2.size()];
        this.a.setRows((a[])localArrayList2.toArray(arrayOfa3));
        return;
        if (!h("spendingLimitFlag"))
        {
          localArrayList2.add(new DetailRow(getResources().getString(2131165402), c("current")));
          if ((this.c.i()) || (this.c.h()))
          {
            localArrayList2.add(new DetailRow(getResources().getString(2131165397), c("spendingLimit")));
            if (this.c.i())
            {
              localArrayList2.add(new DetailRow(getResources().getString(2131165399), c("cardCashAdvanceBalance")));
              localArrayList2.add(new DetailRow(getResources().getString(2131165400), c("cardCashAdvanceLimit")));
              localArrayList2.add(new DetailRow(getResources().getString(2131165401), c("cardCashAdvanceAvailable")));
            }
            else
            {
              localArrayList2.add(new DetailRow(getResources().getString(2131165399), c("cashAdvanceBalance")));
              localArrayList2.add(new DetailRow(getResources().getString(2131165400), c("cashAdvanceLimit")));
              localArrayList2.add(new DetailRow(getResources().getString(2131165401), c("cashAdvanceAvailable")));
            }
          }
          else
          {
            localArrayList2.add(new DetailRow(getResources().getString(2131165412), b("nextPaymentDate")));
            localArrayList2.add(new DetailRow(getResources().getString(2131165404), c("nextPaymentAmount")));
            localArrayList2.add(new DetailRow(getResources().getString(2131165405), c("lastStmtBalance")));
            localArrayList2.add(new DetailRow(getResources().getString(2131165406), c("available")));
            localArrayList2.add(new DetailRow(getResources().getString(2131165407), c("creditLimit")));
          }
        }
        else
        {
          localArrayList2.add(new DetailRow(getResources().getString(2131165395), c("spendingBalance")));
          localArrayList2.add(new DetailRow(getResources().getString(2131165396), c("spendingLimit")));
          localArrayList2.add(new DetailRow(getResources().getString(2131165398), c("spendingLimitAvailable")));
          localArrayList2.add(new DetailRow(getResources().getString(2131165399), c("cardCashAdvanceBalance")));
          localArrayList2.add(new DetailRow(getResources().getString(2131165400), c("cardCashAdvanceLimit")));
          localArrayList2.add(new DetailRow(getResources().getString(2131165401), c("cardCashAdvanceAvailable")));
        }
      }
    case 5:
      boolean bool2 = this.c.d().equals("ALA");
      DetailRow[] arrayOfDetailRow = new DetailRow[5];
      DetailRow localDetailRow3 = new DetailRow(getResources().getString(2131165419), c("current"));
      localDetailRow3.j = bool2;
      arrayOfDetailRow[0] = ((DetailRow)localDetailRow3);
      arrayOfDetailRow[1] = new DetailRow(getResources().getString(2131165403), c("nextPaymentAmount"));
      arrayOfDetailRow[2] = new DetailRow(getResources().getString(2131165420), b("nextPaymentDate"));
      arrayOfDetailRow[3] = new DetailRow(getResources().getString(2131165408), b("lastPaymentDate"));
      arrayOfDetailRow[4] = new DetailRow(getResources().getString(2131165410), c("lastPaymentAmount"));
      this.a.setRows(arrayOfDetailRow);
      return;
    case 4:
      label1147: ArrayList localArrayList1 = new ArrayList();
      boolean bool1 = this.c.n();
      localArrayList1.add(new DetailRow(getResources().getString(2131165408), b("lastPaymentDate")));
      localArrayList1.add(new DetailRow(getResources().getString(2131165409), c("current")));
      localArrayList1.add(new DetailRow(getResources().getString(2131165410), c("lastPaymentAmount")));
      if (bool1)
      {
        localArrayList1.add(new DetailRow(getString(2131165413), c("nextPaymentAmount")));
        DetailRow localDetailRow2 = new DetailRow("", getString(2131165425));
        localDetailRow2.i = 2130903044;
        localDetailRow2.e(getResources().getColor(2131099648));
        localArrayList1.add(localDetailRow2);
        a[] arrayOfa2 = new a[localArrayList1.size()];
        this.a.setRows((a[])localArrayList1.toArray(arrayOfa2));
        return;
      }
      if ("HMG".equalsIgnoreCase(this.c.d()));
      for (String str1 = "nextPaymentDate"; ; str1 = "oldestDueDate")
      {
        DetailRow localDetailRow1 = new DetailRow(getResources().getString(2131165406), c("available"));
        localDetailRow1.j = "HMG".equalsIgnoreCase(this.c.d());
        localArrayList1.add(localDetailRow1);
        localArrayList1.add(new DetailRow(getResources().getString(2131165411), b(str1)));
        localArrayList1.add(new DetailRow(getResources().getString(2131165414), c("nextPaymentAmount")));
        break;
      }
    case 6:
    }
    DetailView localDetailView = this.a;
    a[] arrayOfa1 = new a[5];
    arrayOfa1[0] = new DetailRow(getResources().getString(2131165393), c("current"));
    arrayOfa1[1] = new DetailRow(getResources().getString(2131165416), c("available"));
    arrayOfa1[2] = new DetailRow(getResources().getString(2131165417), c("creditLine"));
    arrayOfa1[3] = new DetailRow(getResources().getString(2131165418), c("lastPaymentAmount"));
    arrayOfa1[4] = new DetailRow(getResources().getString(2131165411), b("nextPaymentDate"));
    localDetailView.setRows(arrayOfa1);
  }

  private void e()
  {
    ArrayList localArrayList = new ArrayList();
    if (this.c.M())
    {
      Iterator localIterator = this.c.K().iterator();
      while (localIterator.hasNext())
      {
        com.chase.sig.android.domain.f localf2 = (com.chase.sig.android.domain.f)localIterator.next();
        if (!localf2.d().equals("SHOW_HIDE_LIST"))
        {
          String str2 = localf2.a();
          String str3 = localf2.c().b();
          String str4 = localf2.c().a();
          String str5;
          if ("CURRENCY".equals(str4))
            str5 = new Dollar(str3).h();
          while (true)
          {
            localArrayList.add(new DetailRow(str2, str5));
            break;
            if ("DATE".equals(str4))
            {
              if ((s.l(str3)) || (s.B(str3)))
                str5 = "--";
              else
                str5 = s.h(str3);
            }
            else
              str5 = str3;
          }
        }
      }
      Serializable localSerializable = getIntent().getExtras().getSerializable("accountDetails");
      if (localSerializable != null)
      {
        List localList = (List)localSerializable;
        for (int i = 0; i < localList.size(); i++)
        {
          com.chase.sig.android.domain.f localf1 = (com.chase.sig.android.domain.f)localList.get(i);
          if (localf1.d().equals("SHOW_HIDE_LIST"))
          {
            String str1 = ((Action)localf1.e().get(0)).a();
            Bundle localBundle = new Bundle();
            localBundle.putSerializable("action", (Serializable)localf1.e().get(0));
            TogglableDetailRow localTogglableDetailRow = new TogglableDetailRow(str1, "", this.k, localBundle);
            localTogglableDetailRow.b = ("extraDetailRow" + Integer.toString(i));
            localArrayList.add(localTogglableDetailRow);
          }
        }
      }
    }
    a[] arrayOfa = new a[localArrayList.size()];
    this.a.setRows((a[])localArrayList.toArray(arrayOfa));
  }

  private boolean h(String paramString)
  {
    String str = a(paramString);
    return (str != null) && (str.trim().toUpperCase().equals("Y"));
  }

  public final void a(Bundle paramBundle)
  {
    b(2130903042);
    setTitle(2131165392);
    Bundle localBundle = getIntent().getExtras();
    this.d = ((ChaseApplication)getApplication()).b().b;
    String str1 = localBundle.getString("selectedAccountId");
    this.c = this.d.a(str1);
    TextView localTextView1 = (TextView)findViewById(2131296269);
    if (s.m(this.c.L()))
    {
      TextView localTextView3 = (TextView)findViewById(2131296270);
      localTextView3.setText(this.c.L());
      localTextView3.setVisibility(0);
    }
    String str2;
    if (this.c.g())
    {
      IAccount localIAccount = a(this.c);
      Object[] arrayOfObject = new Object[2];
      arrayOfObject[0] = this.c.a();
      arrayOfObject[1] = localIAccount.c();
      str2 = String.format("%s (%s)", arrayOfObject);
      localTextView1.setText(str2);
      this.a = ((DetailView)findViewById(2131296271));
      this.a.d();
      if (paramBundle != null)
        break label465;
      if (!this.c.h())
        break label334;
      locala = (a)this.e.a(a.class);
      if (locala.getStatus() != AsyncTask.Status.RUNNING)
      {
        arrayOfString = new String[1];
        if (!this.c.g())
          break label320;
        str3 = a(this.c).b();
        arrayOfString[0] = str3;
        locala.execute(arrayOfString);
      }
    }
    label320: label334: label465: 
    while (!paramBundle.containsKey("detailRows"))
    {
      do
      {
        while (true)
        {
          a locala;
          String[] arrayOfString;
          return;
          str2 = this.c.a(ChaseApplication.a().i());
          break;
          String str3 = this.c.b();
        }
        this.b = new AccountDetail();
        this.b.a(this.c.e());
        d();
      }
      while (!ChaseApplication.a().i());
      View localView1 = getLayoutInflater().inflate(2130903089, null);
      localView1.setBackgroundResource(2131099656);
      localView1.setPadding(5, 10, 5, 10);
      View localView2 = localView1.findViewById(2131296435);
      this.a.addView(localView1);
      TextView localTextView2 = (TextView)localView1.findViewById(2131296435);
      localView2.setOnClickListener(a(PrivateBankingDisclosuresActivity.class).a("title", localTextView2.getText().toString()));
      return;
    }
    this.a.setRows((a[])paramBundle.getSerializable("detailRows"));
  }

  public final void a(AccountDetail paramAccountDetail)
  {
    this.b = paramAccountDetail;
  }

  public final void a(String paramString1, String paramString2, String paramString3)
  {
    a locala = this.a.e(paramString1);
    if ((locala != null) && ((locala instanceof TogglableDetailRow)))
    {
      TogglableDetailRow localTogglableDetailRow = (TogglableDetailRow)locala;
      localTogglableDetailRow.a(paramString2, paramString3);
      localTogglableDetailRow.p();
      this.a.c();
    }
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    if (this.b != null)
      paramBundle.putSerializable("accountDetails", this.b);
    paramBundle.putSerializable("detailRows", this.a.getRows());
  }

  public static class a extends d<AccountDetailActivity, String, Void, AccountDetailResponse>
  {
  }

  public static class b extends b<AccountDetailActivity, Bundle, Void, SmartService.SmartResponse>
  {
    private Bundle a;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.AccountDetailActivity
 * JD-Core Version:    0.6.2
 */