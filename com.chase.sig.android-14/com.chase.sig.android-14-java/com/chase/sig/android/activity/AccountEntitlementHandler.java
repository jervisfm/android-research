package com.chase.sig.android.activity;

import android.app.Activity;
import com.chase.sig.android.domain.IAccount;

public enum AccountEntitlementHandler
{
  static
  {
    AccountEntitlementHandler[] arrayOfAccountEntitlementHandler = new AccountEntitlementHandler[4];
    arrayOfAccountEntitlementHandler[0] = a;
    arrayOfAccountEntitlementHandler[1] = b;
    arrayOfAccountEntitlementHandler[2] = c;
    arrayOfAccountEntitlementHandler[3] = d;
  }

  abstract int a();

  abstract boolean a(IAccount paramIAccount);

  abstract Class<? extends Activity> b();
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.AccountEntitlementHandler
 * JD-Core Version:    0.6.2
 */