package com.chase.sig.android.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask.Status;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.chase.sig.android.d;
import com.chase.sig.android.domain.AccountRewardsDetail;
import com.chase.sig.android.service.AccountRewardsDetailResponse;
import com.chase.sig.android.util.s;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

public class AccountRewardsDetailActivity extends ai
  implements fk.d
{
  private ListView a;
  private List<AccountRewardsDetail> b;
  private b c;
  private String d;
  private boolean k;

  private void a(List<AccountRewardsDetail> paramList)
  {
    if (this.k)
      findViewById(2131296933).setVisibility(0);
    while (true)
    {
      this.c = new b(this, d(paramList), this.d);
      if (s.m(this.d))
      {
        View localView = ((LayoutInflater)getSystemService("layout_inflater")).inflate(2130903210, null);
        TextView localTextView2 = (TextView)localView.findViewById(2131296935);
        this.d = s.q(this.d);
        localTextView2.setTypeface(Typeface.DEFAULT, 0);
        localTextView2.setText(Html.fromHtml(this.d));
        this.a.addFooterView(localView);
      }
      this.a.setAdapter(this.c);
      return;
      String str = s.r(s.q(s.p((String)((AccountRewardsDetail)paramList.get(0)).a().get("programName"))));
      TextView localTextView1 = (TextView)findViewById(2131296933);
      localTextView1.setVisibility(0);
      localTextView1.setTypeface(Typeface.DEFAULT, 0);
      localTextView1.setText(Html.fromHtml(str));
    }
  }

  private ArrayList<com.chase.sig.android.view.ai> d(List<AccountRewardsDetail> paramList)
  {
    int i = paramList.size();
    ArrayList localArrayList = new ArrayList();
    int j = 0;
    if (j < i)
    {
      Hashtable localHashtable = ((AccountRewardsDetail)paramList.get(j)).a();
      int m;
      label85: com.chase.sig.android.view.ai localai1;
      if ((localHashtable.size() > 0) && ((!((String)localHashtable.get("showBalance")).equalsIgnoreCase("false")) || (s.m((String)localHashtable.get("bodyNote")))))
      {
        m = 1;
        if (m != 0)
        {
          localai1 = new com.chase.sig.android.view.ai();
          localai1.j = false;
          if (!((String)localHashtable.get("displayFormat")).equals("0"))
            break label275;
          if (!((String)localHashtable.get("failed")).equals("true"))
            break label240;
          localai1.b = ((String)localHashtable.get("accountMask"));
          localai1.c = ((String)localHashtable.get("formattedBalance"));
        }
      }
      while (true)
      {
        String str4 = (String)localHashtable.get("bodyNote");
        if (s.m(str4))
          localai1.g = str4;
        localArrayList.add(localai1);
        com.chase.sig.android.view.ai localai2 = new com.chase.sig.android.view.ai();
        localai2.a = 2130903225;
        localArrayList.add(localai2);
        j++;
        break;
        m = 0;
        break label85;
        label240: if (((String)localHashtable.get("showBalance")).equalsIgnoreCase("false"))
        {
          localai1.b = "";
          localai1.c = "";
        }
      }
      label275: String str1 = (String)localHashtable.get("programName");
      String str2;
      String str3;
      if (this.k)
      {
        str2 = str1 + " (" + (String)localHashtable.get("accountMask") + ")";
        if (localHashtable.get("balance") == null)
          str3 = "N/A";
      }
      while (true)
      {
        localai1.b = s.p(str2);
        localai1.c = str3;
        break;
        str3 = (String)localHashtable.get("formattedBalance");
        continue;
        str2 = (String)localHashtable.get("balanceType");
        if (str1.indexOf("<i>") != -1)
          str2 = "<i>" + str2 + "</i>";
        str3 = (String)localHashtable.get("formattedBalance");
      }
    }
    return localArrayList;
  }

  public final void a(Bundle paramBundle)
  {
    b(2130903209);
    this.a = ((ListView)findViewById(2131296934));
    Bundle localBundle = getIntent().getExtras();
    String str = localBundle.getString("selectedAccountId");
    this.k = localBundle.getBoolean("isATMRewards");
    if ((paramBundle != null) && (paramBundle.containsKey("rewards_details")))
    {
      this.b = ((List)paramBundle.getSerializable("rewards_details"));
      this.d = ((String)paramBundle.getSerializable("rewards_footnote"));
      if (!this.b.isEmpty())
        a(this.b);
    }
    a locala;
    do
    {
      return;
      f(2131165820);
      return;
      locala = (a)this.e.a(a.class);
    }
    while (locala.getStatus() == AsyncTask.Status.RUNNING);
    locala.execute(new String[] { str });
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    if ((this.c != null) && (b.a(this.c) != null))
    {
      paramBundle.putSerializable("rewards_details", (Serializable)this.b);
      if (s.m(this.d))
        paramBundle.putSerializable("rewards_footnote", this.d);
    }
  }

  public static class a extends d<AccountRewardsDetailActivity, String, Void, AccountRewardsDetailResponse>
  {
  }

  private final class b extends ArrayAdapter<com.chase.sig.android.view.ai>
  {
    private List<com.chase.sig.android.view.ai> b;
    private String c;

    public b(int paramString, String arg3)
    {
      super(2130903211, localList);
      this.b = localList;
      Object localObject;
      this.c = localObject;
    }

    public final View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      this.b.size();
      com.chase.sig.android.view.ai localai = (com.chase.sig.android.view.ai)this.b.get(paramInt);
      LayoutInflater localLayoutInflater = (LayoutInflater)AccountRewardsDetailActivity.this.getSystemService("layout_inflater");
      RelativeLayout localRelativeLayout;
      if (localai.a == 2130903225)
        localRelativeLayout = (RelativeLayout)localLayoutInflater.inflate(2130903225, null);
      LinearLayout localLinearLayout;
      String str;
      do
      {
        return localRelativeLayout;
        localRelativeLayout = (RelativeLayout)localLayoutInflater.inflate(2130903211, null);
        ((TextView)localRelativeLayout.findViewById(2131296940)).setText(localai.c);
        localLinearLayout = (LinearLayout)localRelativeLayout.findViewById(2131296937);
        ((TextView)localLinearLayout.findViewById(2131296938)).setText(Html.fromHtml(localai.b));
        str = localai.g;
      }
      while (!s.m(str));
      TextView localTextView = (TextView)localLinearLayout.findViewById(2131296939);
      localTextView.setVisibility(0);
      localTextView.setText(Html.fromHtml(str));
      return localRelativeLayout;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.AccountRewardsDetailActivity
 * JD-Core Version:    0.6.2
 */