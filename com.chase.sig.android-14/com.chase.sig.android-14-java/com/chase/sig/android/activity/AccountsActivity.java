package com.chase.sig.android.activity;

import android.app.Dialog;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.domain.Announcement;
import com.chase.sig.android.domain.IAccount;
import com.chase.sig.android.domain.MaintenanceInfo;
import com.chase.sig.android.domain.MobileAdResponse;
import com.chase.sig.android.domain.g;
import com.chase.sig.android.domain.o;
import com.chase.sig.android.util.Dollar;
import com.chase.sig.android.util.q;
import com.chase.sig.android.util.r;
import com.chase.sig.android.util.s;
import com.chase.sig.android.view.ah;
import com.chase.sig.android.view.k.a;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

public class AccountsActivity extends ai
  implements fk.d
{
  public static Bitmap a;
  private ListView b;
  private View c;
  private final boolean d = ChaseApplication.a().i();
  private final boolean k = false;
  private final AdapterView.OnItemClickListener l = new v(this);

  private static int a(List<IAccount> paramList, ArrayList<com.chase.sig.android.view.ai> paramArrayList)
  {
    int i = paramArrayList.size();
    Iterator localIterator = paramList.iterator();
    int j = 0;
    int i3;
    if (localIterator.hasNext())
    {
      String str = ((IAccount)localIterator.next()).d();
      if (!a(str, new String[] { "ATM", "RWA" }))
      {
        i3 = 1;
        label65: if (i3 == 0)
          break label197;
        if (!a(str, new String[] { "CHK", "AMA", "SAV", "MMA" }))
          break label121;
      }
    }
    label190: label197: for (int i4 = j + 1; ; i4 = j)
    {
      j = i4;
      break;
      i3 = 0;
      break label65;
      label121: if (j == 0)
        return 0;
      int m = 0;
      int n = 0;
      int i1 = 0;
      if (m < i)
        if (!((com.chase.sig.android.view.ai)paramArrayList.get(m)).a())
          break label190;
      for (int i2 = n + 1; ; i2 = n)
      {
        if (i2 == j)
        {
          i1 = m;
          return i1 + 1;
        }
        m++;
        n = i2;
        break;
      }
    }
  }

  private static com.chase.sig.android.view.ai a(IAccount paramIAccount, String paramString)
  {
    com.chase.sig.android.view.ai localai = new com.chase.sig.android.view.ai();
    localai.e = 0;
    localai.a = 2130903216;
    localai.b = paramString;
    localai.c = "";
    localai.d = paramIAccount.b();
    return localai;
  }

  private String a(IAccount paramIAccount)
  {
    switch (paramIAccount.s())
    {
    default:
      return "";
    case 5:
      return "Principal balance";
    case 2:
      return "Outstanding balance";
    case 7:
    case 10:
      return "Available balance";
    case 3:
      return "Spending this period";
    case 4:
      return "Last payment date";
    case 1:
      if (this.d)
        return "Value";
      return "Market value";
    case 8:
      if (this.d)
        return "Value";
      return "Market value";
    case 9:
      return "Outstanding balance";
    case 6:
    }
    return "Current balance";
  }

  private static ArrayList<String> a(List<IAccount> paramList)
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = paramList.iterator();
    while (localIterator.hasNext())
    {
      IAccount localIAccount = (IAccount)localIterator.next();
      if ((localIAccount.d().equals("RWA")) || (localIAccount.r()))
        localArrayList.add(localIAccount.b());
    }
    return localArrayList;
  }

  private void a(IAccount paramIAccount, ArrayList<com.chase.sig.android.view.ai> paramArrayList, String paramString)
  {
    IAccount localIAccount1 = paramIAccount.x();
    paramArrayList.add(a(paramIAccount, paramString));
    com.chase.sig.android.view.ai localai1 = new com.chase.sig.android.view.ai();
    localai1.e = 6;
    localai1.a = 2130903222;
    Object[] arrayOfObject1 = new Object[2];
    arrayOfObject1[0] = localIAccount1.a();
    arrayOfObject1[1] = paramIAccount.c();
    localai1.b = String.format("%s (%s)", arrayOfObject1);
    localai1.c = "";
    localai1.f = true;
    localai1.d = localIAccount1.b();
    paramArrayList.add(localai1);
    com.chase.sig.android.view.ai localai2 = new com.chase.sig.android.view.ai();
    localai2.e = 5;
    localai2.g = b(localIAccount1);
    String str1 = e(localIAccount1);
    com.chase.sig.android.view.ai localai5;
    if (localai2.g != null)
    {
      localai2.a = 2130903227;
      String str2 = g(localIAccount1);
      localai2.b = a(localIAccount1);
      localai2.d = localIAccount1.b();
      localai2.c = str2;
      localai2.k = g(str2);
      localai2.f = localIAccount1.f();
      paramArrayList.add(localai2);
      if (s.m(str1))
      {
        com.chase.sig.android.view.ai localai3 = new com.chase.sig.android.view.ai();
        localIAccount1.u();
        localai3.a = 2130903224;
        localai3.e = 4;
        localai3.b = str1;
        localai3.c = "";
        localai3.f = true;
        localai3.d = localIAccount1.b();
        paramArrayList.add(localai3);
      }
      com.chase.sig.android.view.ai localai4 = new com.chase.sig.android.view.ai();
      localai4.a = 2130903223;
      localai4.e = 6;
      Object[] arrayOfObject2 = new Object[2];
      arrayOfObject2[0] = paramIAccount.a();
      arrayOfObject2[1] = paramIAccount.c();
      localai4.b = String.format("%s (%s)", arrayOfObject2);
      localai4.c = "";
      localai4.f = true;
      localai4.d = paramIAccount.b();
      paramArrayList.add(localai4);
      if (paramIAccount.f())
      {
        localai5 = new com.chase.sig.android.view.ai();
        if (paramIAccount.v().size() > 1)
          break label592;
      }
    }
    ArrayList localArrayList1;
    label592: for (localai5.a = 2130903217; ; localai5.a = 2130903226)
    {
      localai5.e = 5;
      localai5.b = getResources().getString(2131165391);
      localai5.c = "";
      localai5.f = true;
      localai5.d = paramIAccount.b();
      paramArrayList.add(localai5);
      new r();
      List localList = paramIAccount.v();
      y localy = new y(this);
      localArrayList1 = new ArrayList();
      Iterator localIterator = localList.iterator();
      while (localIterator.hasNext())
      {
        Object localObject = localIterator.next();
        if (!localy.a(localObject))
          localArrayList1.add(localObject);
      }
      localai2.a = 2130903226;
      break;
    }
    ArrayList localArrayList2 = new ArrayList(localArrayList1);
    int i = localArrayList2.size();
    int j = 0;
    if (j < i)
    {
      IAccount localIAccount2 = (IAccount)localArrayList2.get(j);
      com.chase.sig.android.view.ai localai6;
      label680: com.chase.sig.android.view.ai localai7;
      if (!localIAccount2.g())
      {
        localai6 = new com.chase.sig.android.view.ai();
        if (!localIAccount2.g())
          break label832;
        localai6.a = 2130903223;
        localai6.e = 6;
        localai6.b = localIAccount2.a(ChaseApplication.a().i());
        localai6.c = "";
        localai6.f = true;
        localai6.d = localIAccount2.b();
        paramArrayList.add(localai6);
        if (localIAccount2.f())
        {
          localai7 = new com.chase.sig.android.view.ai();
          if (j != i - 1)
            break label842;
        }
      }
      label832: label842: for (localai7.a = 2130903217; ; localai7.a = 2130903226)
      {
        localai7.e = 5;
        localai7.b = getResources().getString(2131165391);
        localai7.c = "";
        localai7.f = true;
        localai7.d = localIAccount2.b();
        paramArrayList.add(localai7);
        j++;
        break;
        localai6.a = 2130903222;
        break label680;
      }
    }
  }

  private void a(com.chase.sig.android.domain.e parame)
  {
    String str = parame.c();
    ArrayList localArrayList1 = new ArrayList();
    ArrayList localArrayList2 = parame.a();
    if (localArrayList2.size() == 0)
      f(2131165363);
    Iterator localIterator = localArrayList2.iterator();
    int i = 0;
    while (localIterator.hasNext())
    {
      IAccount localIAccount = (IAccount)localIterator.next();
      if (i != 0)
      {
        b(localIAccount, localArrayList1, null);
      }
      else
      {
        b(localIAccount, localArrayList1, str);
        i = 1;
      }
    }
    ArrayList localArrayList3 = a(localArrayList2);
    if (localArrayList3.size() > 0)
      a(localArrayList3, localArrayList1, a(localArrayList2, localArrayList1));
    a(localArrayList1);
  }

  private void a(ArrayList<com.chase.sig.android.view.ai> paramArrayList)
  {
    if ((!paramArrayList.isEmpty()) && (((com.chase.sig.android.view.ai)paramArrayList.get(-1 + paramArrayList.size())).a == 2130903225))
      paramArrayList.remove(-1 + paramArrayList.size());
    ah localah = new ah(this, paramArrayList);
    this.b.setAdapter(localah);
  }

  private void a(ArrayList<String> paramArrayList, ArrayList<com.chase.sig.android.view.ai> paramArrayList1, int paramInt)
  {
    String str1 = paramArrayList.toString();
    String str2 = (String)str1.subSequence(1, -1 + str1.length());
    com.chase.sig.android.view.ai localai1 = new com.chase.sig.android.view.ai();
    localai1.e = 9;
    localai1.a = 2130903220;
    localai1.b = getResources().getString(2131165439);
    localai1.c = "";
    localai1.f = true;
    localai1.d = str2;
    paramArrayList1.add(paramInt, localai1);
    com.chase.sig.android.view.ai localai2 = new com.chase.sig.android.view.ai();
    localai2.a = 2130903217;
    localai2.b = getResources().getString(2131165440);
    localai2.e = 9;
    localai2.f = true;
    localai2.d = str2;
    paramArrayList1.add(paramInt + 1, localai2);
    paramArrayList1.add(paramInt + 2, f());
  }

  private static boolean a(String paramString, String[] paramArrayOfString)
  {
    int i = paramArrayOfString.length;
    for (int j = 0; ; j++)
    {
      boolean bool = false;
      if (j < i)
      {
        if (paramString.equalsIgnoreCase(paramArrayOfString[j]))
          bool = true;
      }
      else
        return bool;
    }
  }

  private String b(IAccount paramIAccount)
  {
    if (c(paramIAccount))
      return "see positions";
    if ((d(paramIAccount)) && (!this.d))
      return "see activity";
    if ((d(paramIAccount)) && (this.d))
      return "see transactions";
    return null;
  }

  private void b(IAccount paramIAccount, ArrayList<com.chase.sig.android.view.ai> paramArrayList, String paramString)
  {
    if ((d(paramIAccount)) && (this.d) && (paramIAccount.G()));
    com.chase.sig.android.view.ai localai1;
    com.chase.sig.android.view.ai localai2;
    for (int i = 1; ; i = 0)
    {
      localai1 = new com.chase.sig.android.view.ai();
      localai2 = new com.chase.sig.android.view.ai();
      String str1 = paramIAccount.d();
      if ((!str1.equals("RWA")) && (!str1.equals("ATM")))
        break;
      return;
    }
    boolean bool1 = s.m(paramString);
    if (bool1)
      paramArrayList.add(a(paramIAccount, paramString));
    IAccount localIAccount;
    if (paramIAccount.i())
      if (paramIAccount.x() != null)
        localIAccount = paramIAccount.x();
    while (true)
    {
      com.chase.sig.android.view.ai localai3 = new com.chase.sig.android.view.ai();
      int j;
      int m;
      label166: String str2;
      label216: String str3;
      label255: String str4;
      int n;
      boolean bool4;
      label439: boolean bool5;
      label498: int i3;
      label556: int i1;
      label631: int i2;
      label649: com.chase.sig.android.view.ai localai4;
      if (localIAccount.g())
      {
        j = 7;
        localai3.e = j;
        if (!bool1)
          break label1069;
        m = 2130903222;
        localai3.a = m;
        if (paramIAccount != null)
          break label1095;
        if (!localIAccount.i())
          break label1077;
        str2 = ((IAccount)localIAccount.v().get(0)).a(ChaseApplication.a().i());
        localai3.b = str2;
        localai3.c = "";
        localai3.f = localIAccount.C();
        if (paramIAccount != null)
          break label1134;
        str3 = localIAccount.b();
        localai3.d = str3;
        localai3.l = localIAccount.F();
        paramArrayList.add(localai3);
        str4 = e(localIAccount);
        localai1.e = 5;
        localai1.g = b(localIAccount);
        String str5 = (String)localIAccount.e().get("marginAcctMask");
        boolean bool2 = localIAccount.u();
        n = 0;
        if (bool2)
        {
          boolean bool6 = s.m(str5);
          n = 0;
          if (bool6)
          {
            boolean bool7 = "null".equalsIgnoreCase(str5.trim());
            n = 0;
            if (!bool7)
              n = 1;
          }
        }
        boolean bool3 = s.m((String)localIAccount.e().get("rewardsProgramName"));
        bool4 = s.m(str4);
        if ((localai1.g == null) || ((str4 == null) && (!bool3) && (i == 0)))
          break label1145;
        localai1.a = 2130903227;
        localai1.b = a(localIAccount);
        String str6 = g(localIAccount);
        localai1.d = localIAccount.b();
        localai1.c = str6;
        if ((!c(localIAccount)) && (!d(localIAccount)))
          break label1213;
        bool5 = true;
        localai1.f = bool5;
        localai1.j = bool5;
        localai1.h = localIAccount.H();
        localai1.k = g(str6);
        paramArrayList.add(localai1);
        if (i != 0)
        {
          if (!bool4)
            break label1219;
          i3 = 2130903226;
          localai2.a = i3;
          localai2.b = "See transactions";
          localai2.d = localIAccount.b();
          localai2.c = "";
          localai2.f = true;
          localai2.j = true;
          localai2.e = 12;
          paramArrayList.add(localai2);
        }
        if ((n == 0) || (this.d))
          break label1227;
        i1 = 1;
        if ((!localIAccount.A()) || (!bool3))
          break label1233;
        i2 = 1;
        if (bool4)
        {
          localai4 = new com.chase.sig.android.view.ai();
          if ((i1 == 0) && (i2 == 0))
            break label1239;
        }
      }
      label1069: label1077: label1095: label1227: label1233: label1239: for (localai4.a = 2130903224; ; localai4.a = 2130903218)
      {
        localai4.e = 4;
        localai4.b = str4;
        localai4.c = "";
        localai4.f = true;
        localai4.d = localIAccount.b();
        paramArrayList.add(localai4);
        if (i1 != 0)
        {
          com.chase.sig.android.view.ai localai5 = new com.chase.sig.android.view.ai();
          localai5.a = 2130903222;
          Object[] arrayOfObject2 = new Object[1];
          arrayOfObject2[0] = localIAccount.e().get("marginAcctMask");
          localai5.b = String.format("MARGIN ACCOUNT (%s)", arrayOfObject2);
          localai5.c = "";
          localai5.f = false;
          localai5.d = null;
          localai5.e = 6;
          paramArrayList.add(localai5);
          com.chase.sig.android.view.ai localai6 = new com.chase.sig.android.view.ai();
          localai6.a = 2130903217;
          localai6.b = "Market value";
          localai6.g = b(localIAccount);
          localai6.c = new Dollar((String)localIAccount.e().get("marginAcctMktVal")).h();
          localai6.k = g((String)localIAccount.e().get("marginAcctMktVal"));
          localai6.f = false;
          localai6.d = localIAccount.b();
          paramArrayList.add(localai6);
        }
        if (i2 != 0)
        {
          com.chase.sig.android.view.ai localai7 = new com.chase.sig.android.view.ai();
          localai7.a = 2130903218;
          localai7.e = 8;
          localai7.i = true;
          localai7.b = ((String)localIAccount.e().get("rewardsProgramName"));
          localai7.c = "";
          localai7.f = true;
          localai7.d = localIAccount.b();
          paramArrayList.add(localai7);
        }
        paramArrayList.add(f());
        return;
        if (localIAccount.D())
        {
          j = 10;
          break;
        }
        if (localIAccount.G())
        {
          j = 11;
          break;
        }
        j = 6;
        break;
        m = 2130903220;
        break label166;
        str2 = localIAccount.a(ChaseApplication.a().i());
        break label216;
        Object[] arrayOfObject1 = new Object[2];
        arrayOfObject1[0] = localIAccount.a();
        arrayOfObject1[1] = paramIAccount.c();
        str2 = String.format("%s (%s)", arrayOfObject1);
        break label216;
        label1134: str3 = paramIAccount.b();
        break label255;
        label1145: if ((localai1.g != null) && (str4 == null) && ((this.d) || (n == 0)))
        {
          localai1.a = 2130903219;
          break label439;
        }
        if ((bool4) || (n != 0))
        {
          localai1.a = 2130903226;
          break label439;
        }
        localai1.a = 2130903217;
        break label439;
        bool5 = false;
        break label498;
        i3 = 2130903217;
        break label556;
        i1 = 0;
        break label631;
        i2 = 0;
        break label649;
      }
      label1213: label1219: localIAccount = paramIAccount;
      continue;
      localIAccount = paramIAccount;
      paramIAccount = null;
    }
  }

  private boolean c(IAccount paramIAccount)
  {
    return (this.d) && (paramIAccount.H());
  }

  private void d()
  {
    List localList = ((ChaseApplication)getApplication()).b().b.o();
    ArrayList localArrayList = new ArrayList();
    int i = localList.size();
    int j = 0;
    if (i == 1)
    {
      a((com.chase.sig.android.domain.e)localList.get(0));
      return;
    }
    if (j < i)
    {
      com.chase.sig.android.domain.e locale = (com.chase.sig.android.domain.e)localList.get(j);
      com.chase.sig.android.view.ai localai = new com.chase.sig.android.view.ai();
      localai.e = 3;
      localai.b = locale.c();
      localai.c = "";
      localai.f = true;
      localai.d = locale.b();
      if (j == 0)
        localai.a = 2130903220;
      while (true)
      {
        localArrayList.add(localai);
        j++;
        break;
        if (j == i - 1)
          localai.a = 2130903221;
        else
          localai.a = 2130903222;
      }
    }
    a(localArrayList);
    this.b.setOnItemClickListener(new w(this));
  }

  private boolean d(IAccount paramIAccount)
  {
    if (this.d)
      return paramIAccount.f();
    return (!paramIAccount.G()) && (paramIAccount.f());
  }

  private String e(IAccount paramIAccount)
  {
    AccountEntitlementHandler localAccountEntitlementHandler = f(paramIAccount);
    if (localAccountEntitlementHandler != null)
      return getResources().getText(localAccountEntitlementHandler.a()).toString();
    return null;
  }

  private void e()
  {
    List localList = ((ChaseApplication)getApplication()).b().b.p();
    ArrayList localArrayList1 = new ArrayList();
    Iterator localIterator = localList.iterator();
    IAccount localIAccount;
    com.chase.sig.android.domain.e locale;
    while (localIterator.hasNext())
    {
      localIAccount = (IAccount)localIterator.next();
      if (localIAccount.e() != null)
      {
        if (!localIAccount.h())
          break label174;
        locale = ((ChaseApplication)getApplication()).b().b.c(localIAccount.b());
        if (locale == null)
          break label174;
      }
    }
    label174: for (String str = locale.c(); ; str = null)
    {
      b(localIAccount, localArrayList1, str);
      break;
      ArrayList localArrayList2 = a(localList);
      if (localArrayList2.size() > 0)
        a(localArrayList2, localArrayList1, a(localList, localArrayList1));
      ah localah = new ah(this, localArrayList1);
      this.b.setAdapter(localah);
      return;
    }
  }

  private static AccountEntitlementHandler f(IAccount paramIAccount)
  {
    AccountEntitlementHandler[] arrayOfAccountEntitlementHandler = new AccountEntitlementHandler[4];
    arrayOfAccountEntitlementHandler[0] = AccountEntitlementHandler.b;
    arrayOfAccountEntitlementHandler[1] = AccountEntitlementHandler.c;
    arrayOfAccountEntitlementHandler[2] = AccountEntitlementHandler.a;
    arrayOfAccountEntitlementHandler[3] = AccountEntitlementHandler.d;
    Iterator localIterator = Arrays.asList(arrayOfAccountEntitlementHandler).iterator();
    while (localIterator.hasNext())
    {
      AccountEntitlementHandler localAccountEntitlementHandler = (AccountEntitlementHandler)localIterator.next();
      if (localAccountEntitlementHandler.a(paramIAccount))
        return localAccountEntitlementHandler;
    }
    return null;
  }

  private static com.chase.sig.android.view.ai f()
  {
    com.chase.sig.android.view.ai localai = new com.chase.sig.android.view.ai();
    localai.a = 2130903225;
    return localai;
  }

  private String g(IAccount paramIAccount)
  {
    Hashtable localHashtable = paramIAccount.e();
    if (localHashtable != null)
      switch (paramIAccount.s())
      {
      default:
      case 7:
      case 10:
      case 8:
      case 1:
      case 2:
      case 5:
      case 6:
      case 9:
      case 3:
      case 4:
      }
    label286: for (String str1 = null; ; str1 = null)
      try
      {
        while (true)
        {
          Dollar localDollar = new Dollar(str1);
          if (!localDollar.a())
            break;
          String str2 = localDollar.h();
          return str2;
          str1 = (String)localHashtable.get("available");
          continue;
          if (this.d)
          {
            str1 = (String)localHashtable.get("marketValue");
          }
          else
          {
            str1 = (String)localHashtable.get("current");
            continue;
            if ((this.d) && (paramIAccount.u()))
            {
              str1 = (String)localHashtable.get("marketValue");
            }
            else
            {
              str1 = (String)localHashtable.get("current");
              continue;
              str1 = (String)localHashtable.get("outstanding");
              continue;
              str1 = (String)localHashtable.get("spendingBalance");
              continue;
              String str3 = (String)localHashtable.get("lastPaymentDate");
              if (str3 == null)
                break label286;
              try
              {
                String str4 = s.h(str3);
                return str4;
              }
              catch (Exception localException2)
              {
                new StringBuilder("Unable to parse date from this value: ").append(str3).toString();
                str1 = null;
              }
            }
          }
        }
        return "N/A";
      }
      catch (Exception localException1)
      {
        return "N/A";
      }
  }

  private void g()
  {
    k(this.b.getPaddingTop());
    n().removeView(findViewById(2131296291));
    ChaseApplication.a().b(true);
  }

  private void k(int paramInt)
  {
    this.b.setPadding(this.b.getPaddingLeft(), this.b.getPaddingTop(), this.b.getPaddingRight(), paramInt);
  }

  public final void a(Bundle paramBundle)
  {
    g localg = ((ChaseApplication)getApplication()).b().b;
    b(2130903048);
    a(b.class, new Void[0]);
    this.b = ((ListView)findViewById(2131296286));
    MaintenanceInfo localMaintenanceInfo = localg.E();
    if ((localMaintenanceInfo != null) && (localMaintenanceInfo.a() != null) && (localMaintenanceInfo.a().a()))
      a(findViewById(2131296285), localg.E().a());
    this.b.setOnItemClickListener(this.l);
    Bundle localBundle = getIntent().getExtras();
    List localList = ((ChaseApplication)getApplication()).b().b.i();
    if ((this.d) && ((localBundle != null) || (!localg.s())))
      a(this.b);
    if (localList.size() != 0)
    {
      Iterator localIterator = localList.iterator();
      do
        if (!localIterator.hasNext())
          break;
      while (((IAccount)localIterator.next()).z());
    }
    for (int i = 0; i != 0; i = 1)
    {
      f(2131165363);
      return;
    }
    if (com.chase.sig.android.util.e.a(localBundle, "showAccountOfficer"))
    {
      IAccount localIAccount = localg.a(localBundle.getString("showAccountOfficer"));
      String str2 = localBundle.getString("customerId");
      String str3 = ((ChaseApplication)getApplication()).b().b.d(str2).c();
      ArrayList localArrayList2 = new ArrayList();
      ah localah = new ah(this, localArrayList2);
      a(localIAccount, localArrayList2, str3);
      this.b.setAdapter(localah);
    }
    while (true)
    {
      this.b.setItemsCanFocus(true);
      return;
      if (com.chase.sig.android.util.e.a(localBundle, "showParticularCustomer"))
      {
        a(localg.b(localBundle.getString("showParticularCustomer")));
      }
      else if (com.chase.sig.android.util.e.a(localBundle, "showCustomerGroup"))
      {
        if (localBundle.getInt("showCustomerGroup") == 0)
        {
          if (localg.o().size() > 1)
            d();
          else
            a((com.chase.sig.android.domain.e)localg.o().get(0));
        }
        else
          e();
      }
      else if (com.chase.sig.android.util.e.a(localBundle, "showBusinessAccounts"))
      {
        String str1 = String.valueOf(localBundle.get("showBusinessAccounts"));
        a(((ChaseApplication)getApplication()).b().b.d(str1));
      }
      else if ((localg.s()) && (!localg.J()) && (!localg.K()))
      {
        ArrayList localArrayList1 = new ArrayList();
        com.chase.sig.android.view.ai localai1 = new com.chase.sig.android.view.ai();
        localai1.e = 0;
        localai1.a = 2130903220;
        localai1.b = getString(2131165436);
        localai1.c = "";
        localai1.f = true;
        localai1.d = "0";
        localArrayList1.add(localai1);
        com.chase.sig.android.view.ai localai2 = new com.chase.sig.android.view.ai();
        localai2.e = 1;
        localai2.a = 2130903221;
        localai2.b = getString(2131165437);
        localai2.c = "";
        localai2.f = true;
        localai2.d = "1";
        localArrayList1.add(localai2);
        a(localArrayList1);
        this.b.setOnItemClickListener(new x(this));
      }
      else if ((localg.r()) && (!localg.K()))
      {
        d();
      }
      else if ((localg.q()) && (!localg.J()))
      {
        e();
      }
    }
  }

  protected final void a(com.chase.sig.android.view.ai paramai)
  {
    Intent localIntent = new Intent(this, AccountsActivity.class);
    com.chase.sig.android.domain.e locale = ((ChaseApplication)getApplication()).b().b.c(paramai.d);
    localIntent.putExtra("showAccountOfficer", paramai.d);
    localIntent.putExtra("customerId", locale.b());
    startActivity(localIntent);
  }

  protected final void b(com.chase.sig.android.view.ai paramai)
  {
    if (!paramai.f)
      return;
    Intent localIntent = new Intent(this, InvestmentTransactionsActivity.class);
    localIntent.putExtra("selectedAccountId", paramai.d);
    startActivity(localIntent);
  }

  protected final void c(com.chase.sig.android.view.ai paramai)
  {
    Intent localIntent = new Intent(this, AccountRewardsDetailActivity.class);
    localIntent.putExtra("selectedAccountId", paramai.d);
    if (paramai.e == 9);
    for (boolean bool = true; ; bool = false)
    {
      localIntent.putExtra("isATMRewards", bool);
      startActivity(localIntent);
      return;
    }
  }

  protected final void d(com.chase.sig.android.view.ai paramai)
  {
    IAccount localIAccount = ((ChaseApplication)getApplication()).b().b.a(paramai.d);
    AccountEntitlementHandler localAccountEntitlementHandler = f(localIAccount);
    if (localAccountEntitlementHandler == null)
      return;
    Intent localIntent = new Intent(this, localAccountEntitlementHandler.b());
    localIntent.putExtra("selectedAccountId", localIAccount.b());
    if (localIAccount.a(new String[] { "PPA", "PPX" }))
      localIntent.putExtra("isDebitAccount", false);
    while (true)
    {
      startActivity(localIntent);
      return;
      localIntent.putExtra("isDebitAccount", AccountEntitlementHandler.d.equals(localAccountEntitlementHandler));
    }
  }

  protected final void e(com.chase.sig.android.view.ai paramai)
  {
    if (s.m(paramai.d))
    {
      Intent localIntent = new Intent(this, AccountDetailActivity.class);
      localIntent.putExtra("selectedAccountId", paramai.d);
      localIntent.putExtra("accountDetails", (Serializable)((ChaseApplication)getApplication()).b().b.a(paramai.d).K());
      startActivity(localIntent);
    }
  }

  protected final void f(com.chase.sig.android.view.ai paramai)
  {
    Intent localIntent = new Intent(this, CreditFacilityAccountDetailActivity.class);
    localIntent.putExtra("selectedAccountId", paramai.d);
    localIntent.putExtra("outstandingValue", paramai.l);
    startActivity(localIntent);
  }

  protected final void g(com.chase.sig.android.view.ai paramai)
  {
    if (!paramai.f)
      return;
    if (paramai.h);
    for (Object localObject = PositionsActivity.class; ; localObject = AccountActivityActivity.class)
    {
      Intent localIntent = new Intent(this, (Class)localObject);
      localIntent.putExtra("selectedAccountId", paramai.d);
      startActivity(localIntent);
      return;
    }
  }

  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    if ((paramInt1 == 1010) && (paramInt2 == -1))
      g();
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    k.a locala1 = new k.a(this);
    switch (paramInt)
    {
    default:
      return super.onCreateDialog(paramInt);
    case 0:
    }
    k.a locala2 = locala1.b(2131165315);
    locala2.i = true;
    locala2.a(2131165291, new aa(this)).b(2131165289, new z(this));
    return locala1.d(-1);
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if (j(paramInt))
      return true;
    if ((paramInt == 4) && (getIntent().getExtras() == null))
    {
      showDialog(0);
      return true;
    }
    return super.onKeyDown(paramInt, paramKeyEvent);
  }

  protected void onResume()
  {
    super.onResume();
    if ((!this.d) && (!ChaseApplication.a))
      if (ChaseApplication.a().v() != null)
        new Handler().post(new u(this));
    while (this.c == null)
    {
      return;
      a(a.class, new Void[0]);
      return;
    }
    this.c.setVisibility(8);
  }

  public static class a extends ae<AccountsActivity, Void, Void, MobileAdResponse>
  {
  }

  public static class b extends ae<AccountsActivity, Void, Void, Void>
  {
    // ERROR //
    private static java.util.Calendar[] a()
    {
      // Byte code:
      //   0: new 15	java/io/ObjectInputStream
      //   3: dup
      //   4: invokestatic 20	com/chase/sig/android/ChaseApplication:a	()Lcom/chase/sig/android/ChaseApplication;
      //   7: ldc 22
      //   9: invokevirtual 26	com/chase/sig/android/ChaseApplication:openFileInput	(Ljava/lang/String;)Ljava/io/FileInputStream;
      //   12: invokespecial 29	java/io/ObjectInputStream:<init>	(Ljava/io/InputStream;)V
      //   15: astore_0
      //   16: aload_0
      //   17: invokevirtual 33	java/io/ObjectInputStream:readObject	()Ljava/lang/Object;
      //   20: checkcast 35	[Ljava/util/GregorianCalendar;
      //   23: checkcast 35	[Ljava/util/GregorianCalendar;
      //   26: astore_2
      //   27: aload_0
      //   28: invokevirtual 38	java/io/ObjectInputStream:close	()V
      //   31: aload_2
      //   32: areturn
      //   33: astore_1
      //   34: aconst_null
      //   35: areturn
      //   36: astore_3
      //   37: aload_2
      //   38: areturn
      //
      // Exception table:
      //   from	to	target	type
      //   0	27	33	java/lang/Exception
      //   27	31	36	java/lang/Exception
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.AccountsActivity
 * JD-Core Version:    0.6.2
 */