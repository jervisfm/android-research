package com.chase.sig.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.util.Linkify;
import android.widget.TextView;
import android.widget.TextView.BufferType;
import com.chase.sig.android.domain.QuoteNewsArticle;
import com.chase.sig.android.util.s;

public class ArticleActivity extends ai
  implements fk.c
{
  private QuoteNewsArticle a;

  public final void a(Bundle paramBundle)
  {
    setTitle(2131165897);
    b(2130903212);
    this.a = ((QuoteNewsArticle)getIntent().getExtras().getSerializable("article_content"));
    if ((this.a == null) && (paramBundle != null))
      this.a = ((QuoteNewsArticle)paramBundle.getSerializable("current_article"));
    if (this.a != null)
    {
      ((TextView)findViewById(2131296942)).setText(Html.fromHtml(this.a.c().trim()), TextView.BufferType.SPANNABLE);
      ((TextView)findViewById(2131296943)).setText(s.c(this.a.d()));
      ((TextView)findViewById(2131296944)).setText(this.a.b());
      TextView localTextView = (TextView)findViewById(2131296945);
      localTextView.setText(Html.fromHtml(this.a.e()), TextView.BufferType.SPANNABLE);
      Linkify.addLinks(localTextView, 15);
    }
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    paramBundle.putSerializable("current_article", this.a);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.ArticleActivity
 * JD-Core Version:    0.6.2
 */