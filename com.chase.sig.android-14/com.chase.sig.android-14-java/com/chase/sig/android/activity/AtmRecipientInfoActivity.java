package com.chase.sig.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;
import com.chase.sig.android.domain.QuickPayAcceptMoneyTransaction;
import com.chase.sig.android.domain.QuickPayRecipient;

public class AtmRecipientInfoActivity extends eb
{
  public final void a(Bundle paramBundle)
  {
    b(2130903166);
    QuickPayAcceptMoneyTransaction localQuickPayAcceptMoneyTransaction = (QuickPayAcceptMoneyTransaction)getIntent().getSerializableExtra("quick_pay_transaction");
    ((TextView)findViewById(2131296766)).setText(localQuickPayAcceptMoneyTransaction.m());
    ((TextView)findViewById(2131296767)).setText("You must get this from " + localQuickPayAcceptMoneyTransaction.j().a());
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.AtmRecipientInfoActivity
 * JD-Core Version:    0.6.2
 */