package com.chase.sig.android.activity;

import android.os.Bundle;
import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.c;
import com.chase.sig.android.domain.BillPayTransaction;
import com.chase.sig.android.domain.IAccount;
import com.chase.sig.android.domain.g;
import com.chase.sig.android.domain.o;
import com.chase.sig.android.service.movemoney.d;
import com.chase.sig.android.service.n;
import com.chase.sig.android.util.Dollar;
import com.chase.sig.android.util.s;
import com.chase.sig.android.view.detail.DetailRow;
import com.chase.sig.android.view.detail.DetailView;
import com.chase.sig.android.view.detail.a;

public class BillPayAddCompleteActivity extends m<BillPayTransaction>
{
  public final void a(Bundle paramBundle)
  {
    setTitle(2131165300);
    b(2130903134);
    c.a();
    a(paramBundle, a.class);
  }

  protected final void a(boolean paramBoolean)
  {
    IAccount localIAccount = ((ChaseApplication)getApplication()).b().b.a(((BillPayTransaction)this.a).c());
    DetailView localDetailView = (DetailView)findViewById(2131296595);
    a[] arrayOfa = new a[8];
    arrayOfa[0] = new DetailRow("Transaction Number", ((BillPayTransaction)this.a).n());
    arrayOfa[1] = new DetailRow("Pay To", ((BillPayTransaction)this.a).h());
    arrayOfa[2] = new DetailRow("Pay From", localIAccount.B().toString());
    arrayOfa[3] = new DetailRow("Send On", s.h(((BillPayTransaction)this.a).f()));
    arrayOfa[4] = new DetailRow("Deliver By", s.h(((BillPayTransaction)this.a).q()));
    arrayOfa[5] = new DetailRow("Amount", ((BillPayTransaction)this.a).e().h());
    arrayOfa[6] = new DetailRow("Memo", ((BillPayTransaction)this.a).o());
    arrayOfa[7] = new DetailRow("Status", ((BillPayTransaction)this.a).t());
    localDetailView.setRows(arrayOfa);
    b(2131165572, BillPayHomeActivity.class);
    a(2131165571, BillPayHistoryActivity.class);
  }

  protected final Class<? extends eb> b()
  {
    return BillPayHomeActivity.class;
  }

  public static class a extends n.a<BillPayTransaction, BillPayAddCompleteActivity>
  {
    protected final d<BillPayTransaction> a()
    {
      ((BillPayAddCompleteActivity)this.b).A();
      return n.g();
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.BillPayAddCompleteActivity
 * JD-Core Version:    0.6.2
 */