package com.chase.sig.android.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.AsyncTask.Status;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.Button;
import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.domain.BillPayTransaction;
import com.chase.sig.android.domain.IAccount;
import com.chase.sig.android.domain.Payee;
import com.chase.sig.android.domain.g;
import com.chase.sig.android.domain.o;
import com.chase.sig.android.service.movemoney.ServiceResponse;
import com.chase.sig.android.util.e;
import com.chase.sig.android.util.s;
import com.chase.sig.android.view.JPSpinner;
import com.chase.sig.android.view.b;
import com.chase.sig.android.view.b.a;
import com.chase.sig.android.view.detail.DetailRow;
import com.chase.sig.android.view.detail.DetailView;
import com.chase.sig.android.view.detail.c;
import com.chase.sig.android.view.detail.i;
import com.chase.sig.android.view.detail.l;
import com.chase.sig.android.view.detail.q;
import com.chase.sig.android.view.k.a;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class BillPayAddStartActivity extends a
{
  b.a a = new al(this);
  private boolean b = false;
  private DetailView c;
  private String d;

  private Object c(String paramString)
  {
    return getIntent().getExtras().get(paramString);
  }

  private Payee d()
  {
    return (Payee)c("payee");
  }

  private List<IAccount> e()
  {
    return ((ChaseApplication)getApplication()).b().b.t();
  }

  public final void a(Bundle paramBundle)
  {
    super.a(paramBundle);
    setTitle(2131165560);
    Bundle localBundle = getIntent().getExtras();
    Payee localPayee = (Payee)c("payee");
    String str1 = (String)e.a(localBundle, "selectedAccountId", null);
    String str2 = (String)e.a(localBundle, "scheduledAmount", null);
    this.d = ((String)e.a(localBundle, "deliverBydate", null));
    this.c = ((DetailView)findViewById(2131296969));
    DetailView localDetailView = this.c;
    com.chase.sig.android.view.detail.a[] arrayOfa = new com.chase.sig.android.view.detail.a[7];
    com.chase.sig.android.view.detail.d locald1 = new com.chase.sig.android.view.detail.d("Pay To", localPayee.h());
    locald1.a = a(BillPayHomeActivity.class);
    arrayOfa[0] = locald1;
    arrayOfa[1] = new DetailRow("Date Last Paid", localPayee.k());
    arrayOfa[2] = new DetailRow("Amount Last Paid", localPayee.l()).c();
    q localq1 = new q("Pay From");
    localq1.b = "PAY_FROM";
    q localq2 = (q)((q)localq1).a("Select Account");
    localq2.o = true;
    arrayOfa[3] = localq2;
    String str4;
    List localList;
    JPSpinner localJPSpinner;
    int j;
    if ((paramBundle != null) && (s.m(paramBundle.getString("deliverBy"))))
    {
      str4 = paramBundle.getString("deliverBy");
      com.chase.sig.android.view.detail.d locald2 = new com.chase.sig.android.view.detail.d("Deliver By", str4);
      locald2.b = "DATE";
      com.chase.sig.android.view.detail.d locald3 = (com.chase.sig.android.view.detail.d)locald2;
      locald3.a = new am(this);
      arrayOfa[4] = locald3.q();
      c localc1 = new c("Amount $");
      localc1.b = "AMOUNT";
      c localc2 = (c)((c)localc1).a("Enter Amount");
      localc2.o = true;
      arrayOfa[5] = localc2;
      l locall1 = new l("Memo");
      locall1.b = "MEMO";
      l locall2 = (l)locall1;
      locall2.a = 120;
      arrayOfa[6] = ((l)locall2.a("Optional")).q();
      localDetailView.setRows(arrayOfa);
      a(this.c, 2131296971);
      localList = e();
      localJPSpinner = ((q)a("PAY_FROM")).q();
      localJPSpinner.setAdapter(com.chase.sig.android.util.a.c(this, localList));
      if (!s.l(str1))
        break label534;
      int k = localList.size();
      j = 0;
      if (k != 1)
        break label534;
    }
    while (true)
    {
      localJPSpinner.setSelection(j);
      ((c)a("AMOUNT")).b(str2);
      ((Button)findViewById(2131296970)).setText(2131165531);
      a(2131296971, new aj(this));
      return;
      if (this.d != null);
      for (String str3 = this.d; ; str3 = d().e())
      {
        str4 = s.i(str3);
        break;
      }
      label534: for (int i = 0; ; i++)
      {
        if (i >= localList.size())
          break label587;
        if (((IAccount)localList.get(i)).b().equals(str1))
        {
          j = i;
          break;
        }
      }
      label587: j = -1;
    }
  }

  protected final void b()
  {
    y();
    boolean bool = new i(a()).a();
    String str1 = ((com.chase.sig.android.view.detail.d)a("DATE")).p().toString();
    int i;
    if (str1 == null)
    {
      i = 0;
      if (i != 0)
        break label283;
      a().a("DATE");
    }
    label275: label283: for (int j = 0; ; j = bool)
    {
      BillPayTransaction localBillPayTransaction;
      if (j != 0)
      {
        localBillPayTransaction = new BillPayTransaction();
        localBillPayTransaction.a(((c)a("AMOUNT")).q());
        localBillPayTransaction.a(s.t(((com.chase.sig.android.view.detail.d)a("DATE")).p()));
        int k = ((q)a("PAY_FROM")).q().getSelectedItemPosition();
        localBillPayTransaction.b(((IAccount)e().get(k)).b());
        if (!s.m(a().d("MEMO")))
          break label275;
      }
      for (String str2 = a().d("MEMO"); ; str2 = "")
      {
        localBillPayTransaction.l(str2);
        localBillPayTransaction.a(d());
        a locala = (a)this.e.a(a.class);
        if (locala.getStatus() != AsyncTask.Status.RUNNING)
          locala.execute(new BillPayTransaction[] { localBillPayTransaction });
        return;
        if (str1.equalsIgnoreCase("N/A"))
        {
          i = 0;
          break;
        }
        if (s.t(str1).before(d().f()))
        {
          i = 0;
          break;
        }
        i = 1;
        break;
      }
    }
  }

  protected final void c()
  {
    k.a locala1 = new k.a(this);
    k.a locala2 = locala1.b(2131165564);
    locala2.i = true;
    locala2.a(2131165288, new ak(this));
    locala1.d(-1).show();
  }

  public final void k()
  {
    a().a();
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    Dialog localDialog = super.onCreateDialog(paramInt);
    switch (paramInt)
    {
    default:
      return localDialog;
    case 0:
    }
    Calendar localCalendar = Calendar.getInstance();
    localCalendar.setTime(d().f());
    return a(localCalendar, this.a).a(s.t(a().e("DATE").g()));
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if (j(paramInt))
      return true;
    if (paramInt == 4)
    {
      BillPayHomeActivity.a(this, true);
      return true;
    }
    return super.onKeyDown(paramInt, paramKeyEvent);
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    paramBundle.putString("deliverBy", ((com.chase.sig.android.view.detail.d)a("DATE")).p().toString());
  }

  public static class a extends com.chase.sig.android.d<BillPayAddStartActivity, BillPayTransaction, Void, ServiceResponse>
  {
    BillPayTransaction a;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.BillPayAddStartActivity
 * JD-Core Version:    0.6.2
 */