package com.chase.sig.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.c.a;
import com.chase.sig.android.d;
import com.chase.sig.android.domain.BillPayTransaction;
import com.chase.sig.android.domain.IAccount;
import com.chase.sig.android.domain.Payee;
import com.chase.sig.android.domain.g;
import com.chase.sig.android.domain.o;
import com.chase.sig.android.service.movemoney.ServiceResponse;
import com.chase.sig.android.util.Dollar;
import com.chase.sig.android.util.s;
import com.chase.sig.android.view.detail.DetailRow;
import com.chase.sig.android.view.detail.DetailView;
import com.chase.sig.android.view.detail.a;

public class BillPayAddVerifyActivity extends ai
  implements fk.e, c.a
{
  public final void a()
  {
    Intent localIntent = new Intent(this, PaymentsAndTransfersHomeActivity.class);
    localIntent.setFlags(67108864);
    startActivity(localIntent);
  }

  public final void a(Bundle paramBundle)
  {
    setTitle(2131165561);
    b(2130903135);
    BillPayTransaction localBillPayTransaction = (BillPayTransaction)getIntent().getExtras().get("transaction_object");
    IAccount localIAccount = ((ChaseApplication)getApplication()).b().b.a(localBillPayTransaction.c());
    DetailView localDetailView = (DetailView)findViewById(2131296595);
    a[] arrayOfa = new a[6];
    arrayOfa[0] = new DetailRow("Pay To", localBillPayTransaction.a().h());
    arrayOfa[1] = new DetailRow("Pay From", localIAccount.w().toString());
    arrayOfa[2] = new DetailRow("Send On", s.h(localBillPayTransaction.f()));
    arrayOfa[3] = new DetailRow("Deliver By", s.h(localBillPayTransaction.q()));
    arrayOfa[4] = new DetailRow("Amount", localBillPayTransaction.e().h());
    arrayOfa[5] = new DetailRow("Memo", localBillPayTransaction.o());
    localDetailView.setRows(arrayOfa);
    ((Button)findViewById(2131296599)).setText(2131165562);
    a(2131296599, new an(this, localBillPayTransaction));
    a(2131296598, a(this));
  }

  public static class a extends d<BillPayAddVerifyActivity, BillPayTransaction, Void, ServiceResponse>
  {
    BillPayTransaction a;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.BillPayAddVerifyActivity
 * JD-Core Version:    0.6.2
 */