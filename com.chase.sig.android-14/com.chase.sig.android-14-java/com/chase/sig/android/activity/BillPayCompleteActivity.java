package com.chase.sig.android.activity;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import com.chase.sig.android.c;
import com.chase.sig.android.domain.BillPayTransaction;
import com.chase.sig.android.service.movemoney.d;
import com.chase.sig.android.service.n;
import com.chase.sig.android.util.Dollar;
import com.chase.sig.android.util.s;
import com.chase.sig.android.view.detail.DetailRow;
import com.chase.sig.android.view.detail.DetailView;
import com.chase.sig.android.view.detail.a;
import com.chase.sig.android.view.k.a;

public class BillPayCompleteActivity extends m<BillPayTransaction>
{
  public final void a(Bundle paramBundle)
  {
    b(2130903134);
    setTitle(2131165300);
    c.a();
    a(paramBundle, a.class);
  }

  protected final void a(boolean paramBoolean)
  {
    BillPayTransaction localBillPayTransaction = (BillPayTransaction)this.a;
    DetailView localDetailView = (DetailView)findViewById(2131296595);
    a[] arrayOfa = new a[10];
    arrayOfa[0] = new DetailRow("Transaction Number", localBillPayTransaction.n());
    arrayOfa[1] = new DetailRow("Pay To", localBillPayTransaction.h() + localBillPayTransaction.A());
    arrayOfa[2] = new DetailRow("Pay From", localBillPayTransaction.m());
    arrayOfa[3] = new DetailRow("Send On", s.i(localBillPayTransaction.f()));
    arrayOfa[4] = new DetailRow("Deliver By", s.i(localBillPayTransaction.q()));
    arrayOfa[5] = new DetailRow("Amount", localBillPayTransaction.e().h());
    arrayOfa[6] = new DetailRow("Memo", localBillPayTransaction.o());
    DetailRow localDetailRow1 = new DetailRow("Frequency", a(localBillPayTransaction.i()));
    localDetailRow1.j = c();
    arrayOfa[7] = localDetailRow1;
    DetailRow localDetailRow2 = new DetailRow("Remaining Payments", localBillPayTransaction.w());
    localDetailRow2.j = c();
    arrayOfa[8] = localDetailRow2;
    arrayOfa[9] = new DetailRow("Status", localBillPayTransaction.t());
    localDetailView.setRows(arrayOfa);
    a(2131165538, BillPayHistoryActivity.class);
    b(2131165572, BillPayHomeActivity.class);
    b(paramBoolean);
  }

  protected final Class<? extends eb> b()
  {
    return BillPayHomeActivity.class;
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    k.a locala1 = new k.a(this);
    switch (paramInt)
    {
    default:
      return super.onCreateDialog(paramInt);
    case 12:
      BillPayTransaction localBillPayTransaction = (BillPayTransaction)this.a;
      locala1.i = true;
      k.a locala2 = locala1.a(2131165288, new ao(this)).c(2131165547);
      Context localContext = getBaseContext();
      Object[] arrayOfObject = new Object[4];
      arrayOfObject[0] = a(localBillPayTransaction.i());
      arrayOfObject[1] = (localBillPayTransaction.r() + localBillPayTransaction.s());
      arrayOfObject[2] = (localBillPayTransaction.h() + localBillPayTransaction.A());
      arrayOfObject[3] = (localBillPayTransaction.h() + s.s(localBillPayTransaction.z()));
      locala2.a(s.a(localContext, 2131165549, arrayOfObject));
      return locala1.d(-1);
    case 13:
    }
    locala1.i = true;
    locala1.a(2131165288, new ap(this)).b(2131165548);
    return locala1.d(-1);
  }

  public static class a extends n.a<BillPayTransaction, BillPayCompleteActivity>
  {
    protected final d<BillPayTransaction> a()
    {
      ((BillPayCompleteActivity)this.b).A();
      return n.g();
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.BillPayCompleteActivity
 * JD-Core Version:    0.6.2
 */