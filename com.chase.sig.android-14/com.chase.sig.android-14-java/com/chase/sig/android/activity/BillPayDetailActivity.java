package com.chase.sig.android.activity;

import android.os.Bundle;
import com.chase.sig.android.domain.BillPayTransaction;
import com.chase.sig.android.service.af;
import com.chase.sig.android.service.movemoney.d;
import com.chase.sig.android.service.n;

public class BillPayDetailActivity extends c<BillPayTransaction>
{
  public final int a(DetailResource paramDetailResource)
  {
    switch (1.a[paramDetailResource.ordinal()])
    {
    default:
      return 0;
    case 1:
      return 2131165576;
    case 2:
      return 2131165575;
    case 3:
      return 2131165293;
    case 4:
      return 2131165290;
    case 5:
      return 2131165546;
    case 6:
      return 2131165577;
    case 7:
      return 2131165578;
    case 8:
      return 2131165317;
    case 9:
      return 2131165551;
    case 10:
      return 2131165550;
    case 11:
    }
    return 2131165566;
  }

  public final void a(Bundle paramBundle)
  {
    b(2130903238);
    setTitle(2131165574);
    a(paramBundle, b.class);
  }

  protected final d<BillPayTransaction> b()
  {
    getApplication();
    af.a();
    return n.g();
  }

  protected final Class<? extends ae<?, Boolean, ?, ?>> c()
  {
    return a.class;
  }

  protected final Class<? extends eb> d()
  {
    return BillPayCompleteActivity.class;
  }

  protected final Class<? extends eb> e()
  {
    return BillPayEditActivity.class;
  }

  public static class a extends c.a<BillPayTransaction>
  {
  }

  public static class b extends n.a<BillPayTransaction, BillPayDetailActivity>
  {
    protected final d<BillPayTransaction> a()
    {
      ((BillPayDetailActivity)this.b).A();
      return n.g();
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.BillPayDetailActivity
 * JD-Core Version:    0.6.2
 */