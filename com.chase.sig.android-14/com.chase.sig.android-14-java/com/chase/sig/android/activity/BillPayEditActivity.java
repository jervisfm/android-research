package com.chase.sig.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.CheckBox;
import com.chase.sig.android.domain.BillPayTransaction;
import com.chase.sig.android.service.ServiceError;
import com.chase.sig.android.service.movemoney.RequestFlags;
import com.chase.sig.android.service.movemoney.ServiceResponse;
import com.chase.sig.android.service.n;
import com.chase.sig.android.util.e;
import com.chase.sig.android.util.s;
import com.chase.sig.android.view.AmountView;
import com.chase.sig.android.view.detail.DetailRow;
import com.chase.sig.android.view.detail.DetailView;
import com.chase.sig.android.view.detail.a;
import com.chase.sig.android.view.detail.j;
import com.chase.sig.android.view.detail.l;
import com.chase.sig.android.view.detail.o;
import com.chase.sig.android.view.detail.q;
import com.chase.sig.android.view.detail.r;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BillPayEditActivity extends h<BillPayTransaction>
{
  private Date b = null;

  private boolean D()
  {
    return ((r)a().e("REMAINING")).q().isChecked();
  }

  public final void a(Bundle paramBundle)
  {
    super.a(paramBundle);
    setTitle(2131165565);
    BillPayTransaction localBillPayTransaction = (BillPayTransaction)d();
    DetailView localDetailView = a();
    a[] arrayOfa = new a[7];
    arrayOfa[0] = new DetailRow("Pay To ", localBillPayTransaction.d());
    arrayOfa[1] = new DetailRow("Pay From ", localBillPayTransaction.m());
    com.chase.sig.android.view.detail.c localc = new com.chase.sig.android.view.detail.c("Amount $ ", localBillPayTransaction.e());
    localc.b = "AMOUNT";
    arrayOfa[2] = localc;
    com.chase.sig.android.view.detail.d locald1 = new com.chase.sig.android.view.detail.d("Deliver By ", s.i(localBillPayTransaction.q()));
    locald1.b = "DATE";
    com.chase.sig.android.view.detail.d locald2 = (com.chase.sig.android.view.detail.d)locald1;
    locald2.a = i(0);
    arrayOfa[3] = locald2.q();
    o localo = a(localBillPayTransaction);
    localo.b = "FREQUENCY";
    q localq = (q)localo;
    localq.j = b();
    arrayOfa[4] = localq;
    r localr = new r("Remaining Payments", localBillPayTransaction.x());
    localr.b = "REMAINING";
    j localj = (j)localr;
    localj.j = b();
    arrayOfa[5] = localj;
    if (s.m(localBillPayTransaction.o()));
    for (String str = localBillPayTransaction.o(); ; str = "")
    {
      l locall1 = new l("Memo ", str);
      locall1.b = "MEMO";
      l locall2 = (l)locall1;
      locall2.a = 120;
      arrayOfa[6] = locall2.a("Optional");
      localDetailView.setRows(arrayOfa);
      if (!e.a(paramBundle, "earliest_payment_date"))
        break;
      this.b = s.t(paramBundle.getString("earliest_payment_date"));
      return;
    }
    a(a.class, new BillPayTransaction[] { localBillPayTransaction });
  }

  protected final boolean g()
  {
    boolean bool = true;
    y();
    if (!((com.chase.sig.android.view.detail.c)a().e("AMOUNT")).p().a())
    {
      a().a("AMOUNT");
      bool = false;
    }
    if ((!b()) && (!D()) && (!((r)a().e("REMAINING")).n()))
    {
      a().a("REMAINING");
      bool = false;
    }
    return bool;
  }

  protected final Class<b> h()
  {
    return b.class;
  }

  protected final Class<? extends eb> i()
  {
    return BillPayEditVerifyActivity.class;
  }

  protected final Date j()
  {
    return this.b;
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    if (this.b != null)
      paramBundle.putString("earliest_payment_date", s.a(this.b));
  }

  public static class a extends com.chase.sig.android.d<BillPayEditActivity, BillPayTransaction, Void, Void>
  {
  }

  public static class b extends h.a<BillPayTransaction>
  {
    protected final com.chase.sig.android.service.movemoney.d<BillPayTransaction> a()
    {
      ((h)this.b).A();
      return n.g();
    }

    protected final void a(ServiceResponse paramServiceResponse)
    {
      boolean bool = paramServiceResponse.c("50532");
      String str = ((h)this.b).getString(2131165567);
      ArrayList localArrayList = new ArrayList();
      localArrayList.add(new ServiceError("50532", str, false));
      b(paramServiceResponse);
      if (((!bool) && (paramServiceResponse.f())) || ((bool) && (!((BillPayTransaction)this.a).l()) && (!((h)this.b).c())))
      {
        ((h)this.b).c(paramServiceResponse.g());
        return;
      }
      Intent localIntent1 = new Intent(((h)this.b).getApplicationContext(), PaymentsAndTransfersHomeActivity.class);
      localIntent1.setFlags(67108864);
      com.chase.sig.android.c.a(localIntent1);
      Intent localIntent2 = new Intent(((h)this.b).getBaseContext(), BillPayEditVerifyActivity.class);
      localIntent2.putExtra("transaction_object", ((h)this.b).d());
      RequestFlags localRequestFlags;
      if (((h)this.b).c())
      {
        localRequestFlags = RequestFlags.d;
        localIntent2.putExtra("request_flags", localRequestFlags);
        if (!bool)
          break label248;
        localIntent2.putExtra("queued_errors", (Serializable)localArrayList);
      }
      while (true)
      {
        ((h)this.b).startActivity(localIntent2);
        return;
        localRequestFlags = RequestFlags.c;
        break;
        label248: localIntent2.putExtra("queued_errors", (Serializable)paramServiceResponse.g());
      }
    }

    protected final int b()
    {
      return 0;
    }

    protected final void b(ServiceResponse paramServiceResponse)
    {
      ((BillPayTransaction)this.a).c(paramServiceResponse.a());
      ((BillPayTransaction)this.a).m(paramServiceResponse.j());
      ((h)this.b).getIntent().getExtras().putSerializable("transaction_object", this.a);
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.BillPayEditActivity
 * JD-Core Version:    0.6.2
 */