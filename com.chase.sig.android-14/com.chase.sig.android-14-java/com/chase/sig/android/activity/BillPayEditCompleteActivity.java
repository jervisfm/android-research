package com.chase.sig.android.activity;

import android.os.Bundle;
import com.chase.sig.android.c;
import com.chase.sig.android.domain.BillPayTransaction;
import com.chase.sig.android.service.movemoney.d;
import com.chase.sig.android.service.n;
import com.chase.sig.android.util.Dollar;
import com.chase.sig.android.util.s;
import com.chase.sig.android.view.detail.DetailRow;
import com.chase.sig.android.view.detail.DetailView;
import com.chase.sig.android.view.detail.a;

public class BillPayEditCompleteActivity extends m<BillPayTransaction>
{
  public final void a(Bundle paramBundle)
  {
    b(2130903134);
    setTitle(2131165300);
    c.a();
    a(paramBundle, a.class);
  }

  protected final void a(boolean paramBoolean)
  {
    BillPayTransaction localBillPayTransaction = (BillPayTransaction)this.a;
    DetailView localDetailView = (DetailView)findViewById(2131296595);
    a[] arrayOfa = new a[10];
    arrayOfa[0] = new DetailRow("Transaction Number", localBillPayTransaction.n());
    arrayOfa[1] = new DetailRow("Pay To", localBillPayTransaction.h() + localBillPayTransaction.A());
    arrayOfa[2] = new DetailRow("Pay From", localBillPayTransaction.m());
    arrayOfa[3] = new DetailRow("Send On", s.i(localBillPayTransaction.f()));
    arrayOfa[4] = new DetailRow("Deliver By", s.i(localBillPayTransaction.q()));
    arrayOfa[5] = new DetailRow("Amount", localBillPayTransaction.e().h());
    arrayOfa[6] = new DetailRow("Memo", localBillPayTransaction.o());
    DetailRow localDetailRow1 = new DetailRow("Frequency", a(localBillPayTransaction.i()));
    localDetailRow1.j = c();
    arrayOfa[7] = localDetailRow1;
    DetailRow localDetailRow2 = new DetailRow("Remaining Payments", localBillPayTransaction.w());
    localDetailRow2.j = c();
    arrayOfa[8] = localDetailRow2;
    arrayOfa[9] = new DetailRow("Status", localBillPayTransaction.t());
    localDetailView.setRows(arrayOfa);
    a(2131165571, BillPayHistoryActivity.class);
    b(2131165572, BillPayHomeActivity.class);
    b(paramBoolean);
  }

  protected final Class<? extends eb> b()
  {
    return BillPayHomeActivity.class;
  }

  public static class a extends n.a<BillPayTransaction, BillPayEditCompleteActivity>
  {
    protected final d<BillPayTransaction> a()
    {
      ((BillPayEditCompleteActivity)this.b).A();
      return n.g();
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.BillPayEditCompleteActivity
 * JD-Core Version:    0.6.2
 */