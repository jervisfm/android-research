package com.chase.sig.android.activity;

import android.os.Bundle;
import android.widget.Button;
import com.chase.sig.android.domain.BillPayTransaction;
import com.chase.sig.android.service.af;
import com.chase.sig.android.service.movemoney.d;
import com.chase.sig.android.service.n;
import com.chase.sig.android.util.Dollar;
import com.chase.sig.android.util.s;
import com.chase.sig.android.view.detail.DetailRow;
import com.chase.sig.android.view.detail.DetailView;
import com.chase.sig.android.view.detail.a;

public class BillPayEditVerifyActivity extends k<BillPayTransaction>
{
  public final void a(Bundle paramBundle)
  {
    b(2130903239);
    setTitle(2131165568);
    h();
    ((Button)findViewById(2131296977)).setText(2131165570);
    ((Button)findViewById(2131296978)).setText(2131165569);
  }

  protected final void a(boolean paramBoolean)
  {
    DetailView localDetailView = (DetailView)findViewById(2131296976);
    a[] arrayOfa = new a[8];
    arrayOfa[0] = new DetailRow("Pay To", ((BillPayTransaction)this.a).d());
    arrayOfa[1] = new DetailRow("Pay From", ((BillPayTransaction)this.a).m());
    arrayOfa[2] = new DetailRow("Send On", s.i(((BillPayTransaction)this.a).f()));
    arrayOfa[3] = new DetailRow("Deliver By", s.i(((BillPayTransaction)this.a).q()));
    arrayOfa[4] = new DetailRow("Amount", ((BillPayTransaction)this.a).e().h());
    DetailRow localDetailRow1 = new DetailRow("Frequency", a(((BillPayTransaction)this.a).i()));
    localDetailRow1.j = b();
    arrayOfa[5] = localDetailRow1;
    DetailRow localDetailRow2 = new DetailRow("Remaining Payments", ((BillPayTransaction)this.a).w());
    localDetailRow2.j = b();
    arrayOfa[6] = localDetailRow2;
    arrayOfa[7] = new DetailRow("Memo", ((BillPayTransaction)this.a).o());
    localDetailView.setRows(arrayOfa);
    a(2131296978, new aq(this));
    a(2131296977, B());
  }

  protected final d<BillPayTransaction> c()
  {
    getApplication();
    af.a();
    return n.g();
  }

  protected final Class<BillPayEditCompleteActivity> d()
  {
    return BillPayEditCompleteActivity.class;
  }

  public static class a extends k.a<BillPayTransaction>
  {
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.BillPayEditVerifyActivity
 * JD-Core Version:    0.6.2
 */