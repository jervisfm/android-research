package com.chase.sig.android.activity;

import android.os.Bundle;
import com.chase.sig.android.domain.BillPayTransaction;
import com.chase.sig.android.service.movemoney.d;
import com.chase.sig.android.service.n;

public class BillPayHistoryActivity extends l<BillPayTransaction>
{
  protected final int a()
  {
    return 2131165573;
  }

  public final void a(Bundle paramBundle)
  {
    super.a(paramBundle);
    b(paramBundle);
  }

  protected final Class<? extends l.a<BillPayTransaction>> b()
  {
    return a.class;
  }

  protected final Class<? extends eb> c()
  {
    return BillPayDetailActivity.class;
  }

  public static class a extends l.a<BillPayTransaction>
  {
    protected final d<BillPayTransaction> a()
    {
      ((l)this.b).A();
      return n.g();
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.BillPayHistoryActivity
 * JD-Core Version:    0.6.2
 */