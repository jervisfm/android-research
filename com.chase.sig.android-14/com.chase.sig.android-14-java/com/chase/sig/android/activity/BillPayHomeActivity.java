package com.chase.sig.android.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask.Status;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.d;
import com.chase.sig.android.domain.Payee;
import com.chase.sig.android.domain.g;
import com.chase.sig.android.domain.o;
import com.chase.sig.android.service.SplashResponse;
import com.chase.sig.android.service.billpay.BillPayGetPayeesResponse;
import com.chase.sig.android.util.e;
import com.chase.sig.android.view.af;
import com.chase.sig.android.view.an;
import com.chase.sig.android.view.k.a;
import java.io.Serializable;
import java.util.List;

public class BillPayHomeActivity extends ai
  implements fk.e
{
  private List<Payee> a;
  private ListView b;
  private String c;
  private boolean d;
  private boolean k;
  private boolean l;
  private g m;
  private View n;
  private View o;

  public static void a(Context paramContext, boolean paramBoolean)
  {
    Intent localIntent = new Intent(paramContext, BillPayHomeActivity.class);
    localIntent.addFlags(67108864);
    localIntent.putExtra("refresh_payees_flag", paramBoolean);
    paramContext.startActivity(localIntent);
  }

  private void a(List<Payee> paramList)
  {
    this.a = paramList;
    if ((paramList == null) || ((paramList != null) && (paramList.size() == 0)))
    {
      if ((!this.d) && (!this.k))
      {
        f(2131165826);
        return;
      }
      d();
      return;
    }
    d();
    findViewById(2131296327).setVisibility(0);
    this.b.setAdapter(new af(new a(this, this.a)));
    this.b.setItemsCanFocus(true);
  }

  private void d()
  {
    View localView1 = findViewById(2131296321);
    View localView2 = findViewById(2131296322);
    View localView3 = findViewById(2131296324);
    View localView4 = findViewById(2131296323);
    View localView5 = findViewById(2131296325);
    TextView localTextView = (TextView)findViewById(2131296326);
    if ((this.d) || (this.k))
    {
      localView1.setVisibility(0);
      localView2.setVisibility(0);
    }
    if (!this.k)
    {
      localView3.setVisibility(8);
      localView4.setVisibility(8);
      localView1.setVisibility(8);
      this.n.setBackgroundResource(2130837715);
      localTextView.setText(2131165540);
    }
    if (!this.d)
    {
      localView3.setVisibility(8);
      localView5.setVisibility(8);
      this.o.setBackgroundResource(2130837715);
    }
    if ((this.d) && (!this.k))
    {
      localView1.setVisibility(8);
      localTextView.setText(getResources().getString(2131165540));
    }
    if (this.l)
    {
      this.o.setBackgroundResource(2130837714);
      this.n.setBackgroundResource(2130837713);
      localView3.setVisibility(0);
      localView1.setVisibility(0);
      return;
    }
    localView3.setVisibility(8);
  }

  public final void a(Bundle paramBundle)
  {
    boolean bool1 = true;
    setTitle(2131165536);
    b(2130903056);
    if ((r().d != null) && (r().d.c()))
    {
      e(r().d.n());
      return;
    }
    this.c = "";
    Bundle localBundle = getIntent().getExtras();
    if (localBundle != null)
      this.c = localBundle.getString("selectedAccountId");
    this.m = ((ChaseApplication)getApplication()).b().b;
    if (!this.m.k())
    {
      f(2131165545);
      return;
    }
    this.d = this.m.l();
    boolean bool2;
    if ((x()) && (this.m.m()) && ((r().d == null) || ((r().d != null) && (!r().d.m()))))
    {
      bool2 = bool1;
      label184: this.k = bool2;
      if ((!this.d) || (!this.k))
        break label331;
    }
    while (true)
    {
      this.l = bool1;
      this.b = ((ListView)findViewById(2131296328));
      a(2131296320, a(BillPayHistoryActivity.class));
      this.o = findViewById(2131296323);
      this.o.setOnClickListener(new ar(this));
      this.n = findViewById(2131296325);
      this.n.setOnClickListener(new as(this));
      if (paramBundle != null)
        break label336;
      b localb = (b)this.e.a(b.class);
      if (localb.getStatus() == AsyncTask.Status.RUNNING)
        break;
      localb.execute(new String[0]);
      return;
      bool2 = false;
      break label184;
      label331: bool1 = false;
    }
    label336: a((List)paramBundle.getSerializable("payees"));
  }

  protected Dialog onCreateDialog(int paramInt, Bundle paramBundle)
  {
    switch (paramInt)
    {
    default:
      return super.onCreateDialog(paramInt);
    case 42:
    }
    k.a locala = new k.a(this).a(2131165606);
    locala.i = false;
    return locala.c(2131165603).a("Hide", new au(this, paramBundle)).b("Remind", new at(this, paramBundle)).d(-1);
  }

  protected void onNewIntent(Intent paramIntent)
  {
    setIntent(paramIntent);
    if (e.b(paramIntent.getExtras(), "refresh_payees_flag"))
    {
      b localb = (b)this.e.a(b.class);
      if (localb.getStatus() != AsyncTask.Status.RUNNING)
        localb.execute(new String[0]);
    }
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    paramBundle.putSerializable("payees", (Serializable)this.a);
  }

  final class a extends ArrayAdapter<Payee>
  {
    private final List<Payee> b;
    private final int c;

    public a(int arg2)
    {
      super(2130903064, localList);
      this.b = localList;
      this.c = (-1 + localList.size());
    }

    public final View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      Payee localPayee = (Payee)this.b.get(paramInt);
      if (paramView == null)
      {
        paramView = BillPayHomeActivity.this.getLayoutInflater().inflate(2130903064, null);
        if (paramInt == this.c)
          paramView.findViewById(2131296275).setVisibility(4);
      }
      View localView1 = paramView.findViewById(2131296349);
      TextView localTextView1 = (TextView)localView1.findViewById(2131296339);
      localTextView1.setText(localPayee.h());
      TextView localTextView2 = (TextView)localView1.findViewById(2131296350);
      switch (Integer.valueOf(localPayee.a()).intValue())
      {
      case 3:
      case 4:
      default:
        localTextView2.setText("");
        if ("Active".equalsIgnoreCase(localPayee.m()))
        {
          localView1.setFocusable(true);
          localView1.setFocusableInTouchMode(true);
          localView1.setClickable(true);
          localTextView1.setTextColor(BillPayHomeActivity.this.getResources().getColor(2131099653));
          localTextView2.setTextColor(BillPayHomeActivity.this.getResources().getColor(2131099653));
          localView1.setOnClickListener(new av(this, localPayee));
        }
        break;
      case 0:
      case 1:
      case 2:
      case 5:
      }
      View localView2;
      while (true)
      {
        localView2 = paramView.findViewById(2131296346);
        if ((!"Active".equalsIgnoreCase(localPayee.m())) || ((!BillPayHomeActivity.b(BillPayHomeActivity.this)) && (!BillPayHomeActivity.c(BillPayHomeActivity.this))))
          break label423;
        localView2.setVisibility(0);
        View localView3 = localView2.findViewById(2131296348);
        localView3.setOnClickListener(new aw(this, localPayee));
        an.a(localView2, localView3);
        return paramView;
        localTextView2.setText(2131165541);
        break;
        localTextView2.setText(2131165542);
        break;
        localTextView2.setText(2131165543);
        break;
        localTextView2.setText(2131165544);
        break;
        localTextView1.setTextColor(BillPayHomeActivity.this.getResources().getColor(2131099655));
        localTextView2.setTextColor(BillPayHomeActivity.this.getResources().getColor(2131099655));
        localTextView2.setText(BillPayHomeActivity.this.getResources().getString(2131165323));
        localView1.setFocusable(false);
        localView1.setFocusableInTouchMode(false);
        localView1.setClickable(false);
      }
      label423: localView2.setVisibility(8);
      return paramView;
    }
  }

  public static class b extends d<BillPayHomeActivity, String, Integer, BillPayGetPayeesResponse>
  {
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.BillPayHomeActivity
 * JD-Core Version:    0.6.2
 */