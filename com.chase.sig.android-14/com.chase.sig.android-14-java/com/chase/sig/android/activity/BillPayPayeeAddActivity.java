package com.chase.sig.android.activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.c;
import com.chase.sig.android.domain.BillPayVerifyPayeeInfo;
import com.chase.sig.android.domain.ImageCaptureData;
import com.chase.sig.android.domain.MerchantPayee;
import com.chase.sig.android.domain.State;
import com.chase.sig.android.util.e;
import com.chase.sig.android.util.s;
import com.chase.sig.android.view.detail.DetailRow;
import com.chase.sig.android.view.detail.DetailView;
import com.chase.sig.android.view.detail.a;
import com.chase.sig.android.view.detail.i;
import com.chase.sig.android.view.detail.l;
import java.util.List;

public class BillPayPayeeAddActivity extends ai
  implements fk.e
{
  private DetailView a;
  private i b;
  private BillPayVerifyPayeeInfo c;
  private EditText d;
  private EditText k;

  public static Intent a(Context paramContext, String paramString1, String paramString2, String paramString3)
  {
    Intent localIntent = new Intent(paramContext, BillPayPayeeAddActivity.class);
    localIntent.putExtra("payee_name", paramString1);
    localIntent.putExtra("payee_zip_code", paramString2);
    localIntent.putExtra("account_number", paramString3);
    return localIntent;
  }

  private void a(EditText paramEditText, boolean paramBoolean1, boolean paramBoolean2, int paramInt)
  {
    paramEditText.setEnabled(paramBoolean1);
    paramEditText.setFocusable(paramBoolean1);
    paramEditText.setFocusableInTouchMode(paramBoolean1);
    if ((paramBoolean1) || (paramBoolean2))
    {
      paramEditText.setHint(getString(paramInt));
      return;
    }
    paramEditText.setHint("");
  }

  private void b(Bundle paramBundle)
  {
    if (e.a(paramBundle, "errors"))
    {
      List localList = (List)paramBundle.getSerializable("errors");
      this.b.a(localList);
    }
  }

  private boolean d()
  {
    return (s.l(this.a.d("ACCOUNT_NUMBER"))) && (s.l(this.a.d("BILLPAY_MESSAGE")));
  }

  private void e()
  {
    boolean bool1 = true;
    boolean bool2 = s.m(this.d.getText().toString());
    boolean bool3 = s.m(this.k.getText().toString());
    boolean bool4;
    boolean bool5;
    label54: EditText localEditText2;
    if ((!bool2) && (!bool3))
    {
      bool4 = bool1;
      EditText localEditText1 = this.k;
      if (bool2)
        break label94;
      bool5 = bool1;
      a(localEditText1, bool5, bool4, 2131165312);
      localEditText2 = this.d;
      if (bool3)
        break label100;
    }
    while (true)
    {
      a(localEditText2, bool1, bool4, 2131165337);
      return;
      bool4 = false;
      break;
      label94: bool5 = false;
      break label54;
      label100: bool1 = false;
    }
  }

  public final void a(Bundle paramBundle)
  {
    setTitle(2131165579);
    b(2130903057);
    this.a = ((DetailView)findViewById(2131296329));
    this.b = new i(this.a);
    Bundle localBundle = getIntent().getExtras();
    bd localbd;
    if (e.a(localBundle, "payee_info"))
    {
      this.c = ((BillPayVerifyPayeeInfo)e.a(localBundle, "payee_info", null));
      this.a = ((DetailView)findViewById(2131296329));
      localbd = new bd();
      if (!this.c.a())
        break label937;
      DetailView localDetailView2 = this.a;
      a[] arrayOfa2 = new a[10];
      String str1 = this.c.c().b();
      DetailRow localDetailRow1 = new DetailRow(ChaseApplication.a().getResources().getString(2131165329), str1);
      localDetailRow1.b = "PAYEE";
      arrayOfa2[0] = ((DetailRow)localDetailRow1);
      arrayOfa2[1] = bd.h(this.c.f());
      arrayOfa2[2] = bd.g(this.c.d());
      String str2 = this.c.c().d();
      DetailRow localDetailRow2 = new DetailRow(ChaseApplication.a().getResources().getString(2131165330), str2);
      localDetailRow2.b = "ADDRESS1";
      arrayOfa2[3] = ((DetailRow)((DetailRow)localDetailRow2).c());
      arrayOfa2[4] = bd.e(this.c.c().e());
      String str3 = this.c.c().f();
      DetailRow localDetailRow3 = new DetailRow(ChaseApplication.a().getResources().getString(2131165327), str3);
      localDetailRow3.b = "CITY";
      arrayOfa2[5] = ((DetailRow)((DetailRow)localDetailRow3).c());
      State localState = this.c.c().g();
      DetailRow localDetailRow4 = new DetailRow(ChaseApplication.a().getResources().getString(2131165332), localState.a());
      localDetailRow4.b = "STATE";
      DetailRow localDetailRow5 = (DetailRow)localDetailRow4;
      localDetailRow5.r = "state";
      arrayOfa2[6] = ((DetailRow)((DetailRow)localDetailRow5).c());
      String str4 = this.c.c().h();
      DetailRow localDetailRow6 = new DetailRow(ChaseApplication.a().getResources().getString(2131165324), str4);
      localDetailRow6.b = "ZIP_CODE";
      DetailRow localDetailRow7 = (DetailRow)localDetailRow6;
      localDetailRow7.r = "zipCode";
      arrayOfa2[7] = ((DetailRow)((DetailRow)localDetailRow7).c());
      arrayOfa2[8] = bd.b(this.c.c().c());
      arrayOfa2[9] = bd.a(this.c.g());
      localDetailView2.setRows(arrayOfa2);
    }
    while (true)
    {
      a(this.a, 2131296331);
      this.d = ((l)this.a.e("BILLPAY_MESSAGE")).p();
      this.k = ((l)this.a.e("ACCOUNT_NUMBER")).p();
      this.d.addTextChangedListener(new a());
      this.k.addTextChangedListener(new a());
      e();
      b(localBundle);
      findViewById(2131296330).setOnClickListener(new ax(this));
      findViewById(2131296331).setOnClickListener(new ay(this));
      return;
      if (e.a(localBundle, "image_capture_data"))
      {
        ImageCaptureData localImageCaptureData = (ImageCaptureData)e.a(localBundle, "image_capture_data", null);
        this.c = new BillPayVerifyPayeeInfo();
        MerchantPayee localMerchantPayee = new MerchantPayee();
        localMerchantPayee.b(localImageCaptureData.a());
        localMerchantPayee.d(localImageCaptureData.e());
        localMerchantPayee.e(localImageCaptureData.f());
        localMerchantPayee.f(localImageCaptureData.g());
        localMerchantPayee.a(localImageCaptureData.h());
        localMerchantPayee.g(localImageCaptureData.i());
        this.c.a(localMerchantPayee);
        this.c.b(localImageCaptureData.b());
        this.c.a((String)e.a(localBundle, "selectedAccountId", ""));
        this.c.f(localImageCaptureData.c());
        this.c.g(localImageCaptureData.d());
        break;
      }
      this.c = new BillPayVerifyPayeeInfo();
      this.c.a((MerchantPayee)e.a(localBundle, "merchant_payee_data", new MerchantPayee()));
      this.c.c().b((String)e.a(localBundle, "payee_name", null));
      this.c.c().g((String)e.a(localBundle, "payee_zip_code", null));
      this.c.b((String)e.a(localBundle, "account_number", ""));
      this.c.a((String)e.a(localBundle, "selectedAccountId", ""));
      this.c.a(e.b(localBundle, "from_merchant_directory"));
      break;
      label937: DetailView localDetailView1 = this.a;
      a[] arrayOfa1 = new a[10];
      arrayOfa1[0] = bd.i(this.c.c().b());
      arrayOfa1[1] = bd.h(this.c.f());
      arrayOfa1[2] = bd.g(this.c.d());
      arrayOfa1[3] = bd.f(this.c.c().d());
      arrayOfa1[4] = bd.e(this.c.c().e());
      arrayOfa1[5] = bd.d(this.c.c().f());
      arrayOfa1[6] = localbd.a(this.c.c().g());
      arrayOfa1[7] = bd.c(this.c.c().h());
      arrayOfa1[8] = bd.b(this.c.c().c());
      arrayOfa1[9] = bd.a(this.c.g());
      localDetailView1.setRows(arrayOfa1);
    }
  }

  public final void a(boolean paramBoolean, int paramInt)
  {
    if ((!d()) && (paramBoolean));
    for (boolean bool = true; ; bool = false)
    {
      super.a(bool, paramInt);
      return;
    }
  }

  protected final boolean a()
  {
    boolean bool1 = true;
    if (!this.b.a());
    for (boolean bool2 = false; ; bool2 = bool1)
    {
      if (d())
      {
        this.a.a("ACCOUNT_NUMBER");
        this.a.a("BILLPAY_MESSAGE");
        bool1 = false;
      }
      if (!bool1)
        return false;
      return bool2;
    }
  }

  public final void b(boolean paramBoolean, int paramInt)
  {
    if ((paramBoolean) && (!d()));
    for (boolean bool = true; ; bool = false)
    {
      super.b(bool, paramInt);
      return;
    }
  }

  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    if (paramInt1 == 10)
      c.a();
    switch (paramInt2)
    {
    default:
      setResult(30);
      finish();
    case 20:
      return;
    case 25:
    }
    b(paramIntent.getExtras());
  }

  protected void onResume()
  {
    super.onResume();
    c.a();
  }

  protected final class a
    implements TextWatcher
  {
    protected a()
    {
    }

    public final void afterTextChanged(Editable paramEditable)
    {
      BillPayPayeeAddActivity.c(BillPayPayeeAddActivity.this);
    }

    public final void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
    {
    }

    public final void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
    {
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.BillPayPayeeAddActivity
 * JD-Core Version:    0.6.2
 */