package com.chase.sig.android.activity;

import android.app.Dialog;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.chase.sig.android.c;
import com.chase.sig.android.d;
import com.chase.sig.android.domain.BillPayVerifyPayeeInfo;
import com.chase.sig.android.domain.MerchantPayee;
import com.chase.sig.android.domain.Payee;
import com.chase.sig.android.domain.State;
import com.chase.sig.android.service.IServiceError;
import com.chase.sig.android.service.billpay.BillPayGetPayeesResponse;
import com.chase.sig.android.service.billpay.BillPayPayeeAddResponse;
import com.chase.sig.android.util.e;
import com.chase.sig.android.util.s;
import com.chase.sig.android.view.detail.DetailRow;
import com.chase.sig.android.view.detail.DetailView;
import com.chase.sig.android.view.detail.b;
import com.chase.sig.android.view.k.a;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class BillPayPayeeAddVerifyActivity extends ai
  implements fk.e
{
  private com.chase.sig.android.service.billpay.a a;
  private DetailView b;
  private BillPayPayeeAddResponse c;
  private BillPayVerifyPayeeInfo d;
  private List<IServiceError> k;

  private void a(BillPayPayeeAddResponse paramBillPayPayeeAddResponse)
  {
    DetailRow localDetailRow1 = (DetailRow)this.b.e("DELIVERY_METHOD");
    Payee localPayee = paramBillPayPayeeAddResponse.b();
    localDetailRow1.p().setText(localPayee.b());
    this.b.setVisibility(0);
    if (!this.d.a())
      ((DetailRow)this.b.e("ACCOUNT_NUMBER")).p().setText(localPayee.d());
    this.d.c(localPayee.d());
    DetailRow localDetailRow2 = (DetailRow)this.b.e("ACCOUNT_NUMBER");
    DetailRow localDetailRow3 = (DetailRow)this.b.e("BILLPAY_MESSAGE");
    if (s.l(this.d.e()))
    {
      localDetailRow2.k().setVisibility(8);
      localDetailRow3.k().setVisibility(0);
      return;
    }
    localDetailRow2.k().setVisibility(0);
    localDetailRow3.k().setVisibility(8);
  }

  private DialogInterface.OnClickListener d()
  {
    return new bc(this);
  }

  protected final void a(int paramInt)
  {
    switch (paramInt)
    {
    default:
      super.a(paramInt);
      return;
    case 12:
      Intent localIntent = getIntent();
      localIntent.putExtra("errors", (Serializable)this.k);
      localIntent.putExtra("payee_info", this.d);
      setResult(25, localIntent);
      finish();
      return;
    case 13:
    }
    setResult(20);
    finish();
  }

  public final void a(Bundle paramBundle)
  {
    setTitle(2131165580);
    b(2130903058);
    Bundle localBundle = getIntent().getExtras();
    String str;
    if (e.a(paramBundle, "payee_info"))
    {
      this.d = ((BillPayVerifyPayeeInfo)e.a(paramBundle, "payee_info", null));
      if (s.l(this.d.f()))
        this.d.d(this.d.c().b());
      this.c = ((BillPayPayeeAddResponse)e.a(paramBundle, "add_payee_response", null));
      this.b = ((DetailView)findViewById(2131296271));
      ((Button)findViewById(2131296335)).setOnClickListener(new az(this));
      DetailView localDetailView = this.b;
      com.chase.sig.android.view.detail.a[] arrayOfa = new com.chase.sig.android.view.detail.a[10];
      DetailRow localDetailRow1 = new DetailRow("Payee name", this.d.c().b());
      localDetailRow1.b = "PAYEE";
      arrayOfa[0] = localDetailRow1;
      DetailRow localDetailRow2 = new DetailRow("Payee nickname", this.d.f());
      localDetailRow2.b = "PAYEE_NICKNAME";
      arrayOfa[1] = localDetailRow2;
      DetailRow localDetailRow3 = new DetailRow("Account number", this.d.d());
      localDetailRow3.b = "ACCOUNT_NUMBER";
      arrayOfa[2] = localDetailRow3;
      b localb = new b("Payee address", this.d.c().d(), s.s(this.d.c().e()));
      localb.b = "ADDRESS";
      arrayOfa[3] = localb;
      DetailRow localDetailRow4 = new DetailRow("City", this.d.c().f());
      localDetailRow4.b = "CITY";
      arrayOfa[4] = localDetailRow4;
      DetailRow localDetailRow5 = new DetailRow("State", this.d.c().g().toString());
      localDetailRow5.b = "STATE";
      arrayOfa[5] = localDetailRow5;
      DetailRow localDetailRow6 = new DetailRow("ZIP code", s.a(s.x(this.d.c().h())));
      localDetailRow6.b = "ZIP_CODE";
      arrayOfa[6] = localDetailRow6;
      DetailRow localDetailRow7 = new DetailRow("Phone number", this.d.c().c());
      localDetailRow7.b = "PHONE_NUMBER";
      DetailRow localDetailRow8 = (DetailRow)localDetailRow7;
      localDetailRow8.j = s.l(this.d.c().c());
      arrayOfa[7] = localDetailRow8;
      if (!s.l(this.d.g()))
        break label781;
      str = "";
      label468: DetailRow localDetailRow9 = new DetailRow("Message", str);
      localDetailRow9.b = "BILLPAY_MESSAGE";
      arrayOfa[8] = localDetailRow9;
      DetailRow localDetailRow10 = new DetailRow("Delivery method", "--");
      localDetailRow10.b = "DELIVERY_METHOD";
      arrayOfa[9] = localDetailRow10;
      localDetailView.setRows(arrayOfa);
      if (this.c != null)
        break label793;
      this.a = new com.chase.sig.android.service.billpay.a(this.d);
      a(c.class, new String[0]);
    }
    while (true)
    {
      a(2131296334, new ba(this));
      Intent localIntent = new Intent(this, BillPayHomeActivity.class);
      localIntent.setFlags(67108864);
      c.a(localIntent);
      return;
      if (e.a(localBundle, "payee_info"))
      {
        this.d = ((BillPayVerifyPayeeInfo)e.a(localBundle, "payee_info", null));
        break;
      }
      this.d = new BillPayVerifyPayeeInfo();
      this.d.a((MerchantPayee)e.a(localBundle, "merchant_payee_data", null));
      if (this.d.c() == null)
        finish();
      this.d.b(s.s((String)e.a(localBundle, "account_number", null)));
      this.d.e((String)e.a(localBundle, "merchant_bill_pay_message", ""));
      this.d.d((String)e.a(localBundle, "payee_nick_name", this.d.c().b()));
      this.d.a((String)e.a(localBundle, "selectedAccountId", ""));
      this.d.a(e.b(localBundle, "from_merchant_directory"));
      break;
      label781: str = this.d.g();
      break label468;
      label793: a(this.c);
    }
  }

  public final void a(ArrayList<Payee> paramArrayList, String paramString)
  {
    Iterator localIterator = paramArrayList.iterator();
    Payee localPayee;
    do
    {
      if (!localIterator.hasNext())
        break;
      localPayee = (Payee)localIterator.next();
    }
    while (!localPayee.j().equalsIgnoreCase(paramString));
    while (true)
    {
      Intent localIntent = new Intent(this, BillPayAddStartActivity.class);
      localIntent.putExtra("payee", localPayee);
      localIntent.putExtra("selectedAccountId", this.d.b());
      localIntent.putExtra("scheduledAmount", this.d.h());
      localIntent.putExtra("deliverBydate", localPayee.e());
      startActivity(localIntent);
      return;
      localPayee = null;
    }
  }

  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    if ((paramInt1 == 50) && (paramInt2 == 30))
      finish();
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    k.a locala = new k.a(this);
    switch (paramInt)
    {
    default:
      return super.onCreateDialog(paramInt);
    case 10:
      if (s.m(this.d.e()));
      for (String str2 = getResources().getString(2131165585) + ' ' + this.d.f() + " (" + s.s(this.d.e()) + ") " + getResources().getString(2131165586); ; str2 = getResources().getString(2131165585) + ' ' + this.d.f() + ' ' + getResources().getString(2131165586))
      {
        locala.i = false;
        locala.c(2131165584).a(str2).a("Done", d()).b("Pay Bill", new bb(this));
        return locala.d(-1);
      }
    case 14:
    }
    if (s.m(this.d.e()));
    for (String str1 = getResources().getString(2131165588) + ' ' + this.d.f() + " (" + s.s(this.d.e()) + ") " + getResources().getString(2131165589); ; str1 = getResources().getString(2131165588) + ' ' + this.d.f() + ' ' + getResources().getString(2131165589))
    {
      locala.i = false;
      locala.a("Ok", d()).c(2131165587).a(str1);
      return locala.d(-1);
    }
  }

  public void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    paramBundle.putSerializable("add_payee_response", this.c);
    paramBundle.putSerializable("payee_info", this.d);
  }

  public static class a extends d<BillPayPayeeAddVerifyActivity, Void, Void, BillPayPayeeAddResponse>
  {
  }

  public static class b extends d<BillPayPayeeAddVerifyActivity, String, Void, BillPayGetPayeesResponse>
  {
    private String a = "";
  }

  public static class c extends d<BillPayPayeeAddVerifyActivity, String, Void, BillPayPayeeAddResponse>
  {
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.BillPayPayeeAddVerifyActivity
 * JD-Core Version:    0.6.2
 */