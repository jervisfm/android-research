package com.chase.sig.android.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.chase.sig.android.activity.a.b;
import com.chase.sig.android.d;
import com.chase.sig.android.domain.Payee;
import com.chase.sig.android.domain.PayeeAddress;
import com.chase.sig.android.service.billpay.DeletePayeeResponse;
import com.chase.sig.android.util.e;
import com.chase.sig.android.util.s;
import com.chase.sig.android.view.detail.DetailView;
import com.chase.sig.android.view.detail.a;
import com.chase.sig.android.view.detail.i;
import com.chase.sig.android.view.detail.l;
import com.chase.sig.android.view.k.a;
import java.util.List;

public class BillPayPayeeEditActivity extends ai
  implements fk.e
{
  private DetailView a;
  private i b;
  private Payee c;
  private EditText d;
  private EditText k;
  private Button l;

  private static void a(EditText paramEditText)
  {
    paramEditText.setEnabled(false);
    paramEditText.setFocusable(false);
    paramEditText.setHint("");
  }

  private void a(EditText paramEditText, boolean paramBoolean1, boolean paramBoolean2, int paramInt)
  {
    paramEditText.setEnabled(paramBoolean1);
    paramEditText.setFocusable(paramBoolean1);
    paramEditText.setFocusableInTouchMode(paramBoolean1);
    if ((paramBoolean1) || (paramBoolean2))
    {
      paramEditText.setHint(getString(paramInt));
      return;
    }
    paramEditText.setHint("");
  }

  private void b(Bundle paramBundle)
  {
    if (e.a(paramBundle, "errors"))
    {
      List localList = (List)paramBundle.getSerializable("errors");
      this.b.a(localList);
    }
  }

  private void d()
  {
    b(this.a, 2131296337);
  }

  private boolean e()
  {
    return (s.l(this.a.d("ACCOUNT_NUMBER"))) && (s.l(this.a.d("BILLPAY_MESSAGE")));
  }

  public final void a(Bundle paramBundle)
  {
    setTitle(2131165581);
    b(2130903059);
    this.l = ((Button)findViewById(2131296337));
    Bundle localBundle = getIntent().getExtras();
    if ((localBundle != null) && (localBundle.containsKey("payee")))
    {
      this.c = ((Payee)localBundle.get("payee"));
      bd localbd = new bd();
      PayeeAddress localPayeeAddress = this.c.o();
      this.a = ((DetailView)findViewById(2131296329));
      DetailView localDetailView = this.a;
      a[] arrayOfa = new a[10];
      arrayOfa[0] = bd.i(this.c.i());
      arrayOfa[1] = bd.h(this.c.h());
      arrayOfa[2] = bd.g(this.c.c());
      arrayOfa[3] = bd.f(localPayeeAddress.a());
      arrayOfa[4] = bd.e(localPayeeAddress.b());
      arrayOfa[5] = bd.d(localPayeeAddress.c());
      arrayOfa[6] = localbd.a(localPayeeAddress.e());
      arrayOfa[7] = bd.c(localPayeeAddress.d());
      arrayOfa[8] = bd.b(this.c.p());
      arrayOfa[9] = bd.a(this.c.g());
      localDetailView.setRows(arrayOfa);
      this.d = ((l)this.a.e("BILLPAY_MESSAGE")).p();
      this.k = ((l)this.a.e("ACCOUNT_NUMBER")).p();
      this.d.addTextChangedListener(new b());
      this.k.addTextChangedListener(new b());
      if (!s.m(this.d.getText().toString()))
        break label446;
      a(this.k);
    }
    while (true)
    {
      a(this.a, 2131296337);
      b(localBundle);
      if (paramBundle == null)
      {
        this.l.setEnabled(false);
        d();
      }
      this.b = new i(this.a);
      findViewById(2131296336).setOnClickListener(new bf(this));
      a(2131296330, new bg(this));
      findViewById(2131296337).setOnClickListener(new bh(this));
      return;
      this.c = new Payee();
      break;
      label446: if (s.m(this.k.getText().toString()))
        a(this.d);
    }
  }

  public final void a(boolean paramBoolean, int paramInt)
  {
    if ((!e()) && (paramBoolean));
    for (boolean bool = true; ; bool = false)
    {
      super.a(bool, paramInt);
      return;
    }
  }

  protected final boolean a()
  {
    boolean bool1 = true;
    if (!this.b.a());
    for (boolean bool2 = false; ; bool2 = bool1)
    {
      if (e())
      {
        this.a.a("ACCOUNT_NUMBER");
        this.a.a("BILLPAY_MESSAGE");
        bool1 = false;
      }
      if (!bool1)
        return false;
      return bool2;
    }
  }

  public final void b(boolean paramBoolean, int paramInt)
  {
    if ((paramBoolean) && (!e()));
    for (boolean bool = true; ; bool = false)
    {
      super.b(bool, paramInt);
      return;
    }
  }

  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    if (paramInt1 == 12);
    switch (paramInt2)
    {
    default:
      setResult(30);
      finish();
    case 20:
      return;
    case 25:
    }
    b(paramIntent.getExtras());
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    k.a locala1 = new k.a(this);
    switch (paramInt)
    {
    default:
      return super.onCreateDialog(paramInt);
    case 10:
      k.a locala2 = locala1.b(2131165608);
      locala2.i = false;
      locala2.a("Delete Payee", new bi(this)).b("Cancel", new b());
      return locala1.d(-1);
    case 11:
    }
    if (s.m(this.c.d()));
    for (String str = "Payee " + this.c.h() + " (" + s.s(this.c.d()) + ") has been deleted."; ; str = "Payee " + this.c.h() + " has been deleted.")
    {
      locala1.i = false;
      locala1.a("Ok", new bj(this)).c(2131165609).a(str);
      return locala1.d(-1);
    }
  }

  protected void onRestoreInstanceState(Bundle paramBundle)
  {
    super.onRestoreInstanceState(paramBundle);
    this.l.setEnabled(paramBundle.getBoolean("change_button_is_enabled"));
    d();
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    paramBundle.putBoolean("change_button_is_enabled", this.l.isEnabled());
  }

  public static class a extends d<BillPayPayeeEditActivity, Payee, Void, DeletePayeeResponse>
  {
    private Payee a;
  }

  protected final class b
    implements TextWatcher
  {
    protected b()
    {
    }

    public final void afterTextChanged(Editable paramEditable)
    {
      BillPayPayeeEditActivity.c(BillPayPayeeEditActivity.this);
    }

    public final void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
    {
    }

    public final void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
    {
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.BillPayPayeeEditActivity
 * JD-Core Version:    0.6.2
 */