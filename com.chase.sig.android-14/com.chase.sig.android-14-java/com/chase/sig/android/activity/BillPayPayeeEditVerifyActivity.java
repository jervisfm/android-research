package com.chase.sig.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.chase.sig.android.c;
import com.chase.sig.android.d;
import com.chase.sig.android.domain.Payee;
import com.chase.sig.android.domain.PayeeAddress;
import com.chase.sig.android.domain.State;
import com.chase.sig.android.service.IServiceError;
import com.chase.sig.android.service.billpay.BillPayPayeeEditResponse;
import com.chase.sig.android.util.e;
import com.chase.sig.android.util.s;
import com.chase.sig.android.view.detail.DetailRow;
import com.chase.sig.android.view.detail.DetailView;
import com.chase.sig.android.view.detail.a;
import com.chase.sig.android.view.detail.b;
import java.io.Serializable;
import java.util.List;

public class BillPayPayeeEditVerifyActivity extends ai
  implements fk.e
{
  private DetailView a;
  private BillPayPayeeEditResponse b;
  private Payee c;
  private List<IServiceError> d;

  private void b(BillPayPayeeEditResponse paramBillPayPayeeEditResponse)
  {
    ((DetailRow)this.a.e("DELIVERY_METHOD")).p().setText(paramBillPayPayeeEditResponse.c());
    this.a.setVisibility(0);
    ((DetailRow)this.a.e("ACCOUNT_NUMBER")).p().setText(paramBillPayPayeeEditResponse.d());
    DetailRow localDetailRow1 = (DetailRow)this.a.e("ACCOUNT_NUMBER");
    DetailRow localDetailRow2 = (DetailRow)this.a.e("BILLPAY_MESSAGE");
    if (s.l(this.c.d()))
    {
      localDetailRow1.k().setVisibility(8);
      localDetailRow2.k().setVisibility(0);
      return;
    }
    localDetailRow1.k().setVisibility(0);
    localDetailRow2.k().setVisibility(8);
  }

  protected final void a(int paramInt)
  {
    super.a(paramInt);
    switch (paramInt)
    {
    default:
      return;
    case 12:
      Intent localIntent = getIntent();
      localIntent.putExtra("errors", (Serializable)this.d);
      setResult(25, localIntent);
      finish();
      return;
    case 13:
    }
    setResult(20);
    finish();
  }

  public final void a(Bundle paramBundle)
  {
    setTitle(2131165580);
    b(2130903060);
    Intent localIntent = new Intent(getApplicationContext(), PaymentsAndTransfersHomeActivity.class);
    localIntent.setFlags(67108864);
    c.a(localIntent);
    Bundle localBundle = getIntent().getExtras();
    DetailView localDetailView;
    a[] arrayOfa;
    if (e.a(paramBundle, "payee"))
    {
      this.c = ((Payee)e.a(paramBundle, "payee", null));
      this.b = ((BillPayPayeeEditResponse)e.a(paramBundle, "edit_payee_response", null));
      this.a = ((DetailView)findViewById(2131296271));
      findViewById(2131296330).setOnClickListener(new bk(this));
      ((Button)findViewById(2131296338)).setOnClickListener(new bm(this));
      localDetailView = this.a;
      arrayOfa = new a[10];
      arrayOfa[0] = new DetailRow("Payee name", this.c.i());
      DetailRow localDetailRow1 = new DetailRow("Payee nickname", this.c.h());
      localDetailRow1.b = "PAYEE_NICKNAME";
      arrayOfa[1] = localDetailRow1;
      DetailRow localDetailRow2 = new DetailRow("Account number", this.c.d());
      localDetailRow2.b = "ACCOUNT_NUMBER";
      arrayOfa[2] = localDetailRow2;
      b localb = new b("Payee address", this.c.o().a(), s.s(this.c.o().b()));
      localb.b = "ADDRESS";
      arrayOfa[3] = localb;
      DetailRow localDetailRow3 = new DetailRow("City", this.c.o().c());
      localDetailRow3.b = "CITY";
      arrayOfa[4] = localDetailRow3;
      DetailRow localDetailRow4 = new DetailRow("State", this.c.o().e().toString());
      localDetailRow4.b = "STATE";
      arrayOfa[5] = localDetailRow4;
      DetailRow localDetailRow5 = new DetailRow("ZIP code", this.c.o().d());
      localDetailRow5.b = "ZIP_CODE";
      arrayOfa[6] = localDetailRow5;
      DetailRow localDetailRow6 = new DetailRow("Phone number", this.c.p());
      localDetailRow6.b = "PHONE_NUMBER";
      DetailRow localDetailRow7 = (DetailRow)localDetailRow6;
      localDetailRow7.j = s.l(this.c.p());
      arrayOfa[7] = localDetailRow7;
      if (!s.l(this.c.g()))
        break label571;
    }
    label571: for (String str = ""; ; str = this.c.g())
    {
      DetailRow localDetailRow8 = new DetailRow("Message", str);
      localDetailRow8.b = "BILLPAY_MESSAGE";
      arrayOfa[8] = localDetailRow8;
      DetailRow localDetailRow9 = new DetailRow("Delivery method", "--");
      localDetailRow9.b = "DELIVERY_METHOD";
      arrayOfa[9] = localDetailRow9;
      localDetailView.setRows(arrayOfa);
      if (this.b != null)
        break label583;
      a(b.class, new String[0]);
      return;
      if (e.a(localBundle, "payee"))
      {
        this.c = ((Payee)e.a(localBundle, "payee", null));
        break;
      }
      finish();
      break;
    }
    label583: b(this.b);
  }

  public final void a(BillPayPayeeEditResponse paramBillPayPayeeEditResponse)
  {
    if ("Active".equals(paramBillPayPayeeEditResponse.b()))
    {
      c.a();
      BillPayHomeActivity.a(this, true);
    }
  }

  public void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    paramBundle.putSerializable("edit_payee_response", this.b);
    paramBundle.putSerializable("payee", this.c);
  }

  public static class a extends d<BillPayPayeeEditVerifyActivity, Void, Void, BillPayPayeeEditResponse>
  {
  }

  public static class b extends d<BillPayPayeeEditVerifyActivity, String, Void, BillPayPayeeEditResponse>
  {
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.BillPayPayeeEditVerifyActivity
 * JD-Core Version:    0.6.2
 */