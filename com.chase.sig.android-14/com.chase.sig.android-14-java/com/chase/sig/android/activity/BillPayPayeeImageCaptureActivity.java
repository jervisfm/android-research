package com.chase.sig.android.activity;

import android.content.Intent;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.ShutterCallback;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

public class BillPayPayeeImageCaptureActivity extends ai
  implements fk.e
{
  Camera.ShutterCallback a = new bq(this);
  Camera.PictureCallback b = new br(this);
  Camera.PictureCallback c = new bs(this);
  private ImageView d;
  private FrameLayout k;
  private String l;

  public final void a(Bundle paramBundle)
  {
    CapturePreview localCapturePreview = new CapturePreview(this, (byte)0);
    Bundle localBundle = getIntent().getExtras();
    if (localBundle != null)
      this.l = localBundle.getString("selectedAccountId");
    ((FrameLayout)findViewById(2131296306)).addView(localCapturePreview);
    this.k = ((FrameLayout)findViewById(2131296319));
    this.k.setVisibility(4);
    Spanned localSpanned = Html.fromHtml(getText(2131165606).toString());
    ((TextView)findViewById(2131296318)).setText(localSpanned);
    Button localButton = (Button)findViewById(2131296310);
    localButton.setVisibility(0);
    this.d = ((ImageView)findViewById(2131296316));
    this.d.setVisibility(0);
    localButton.setOnClickListener(new bn(this, localButton));
    this.d.setOnClickListener(new bo(this, localCapturePreview));
  }

  protected final void a_()
  {
    setContentView(2130903055);
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if (paramInt == 27)
    {
      if (paramKeyEvent.getRepeatCount() == 0)
        this.d.performClick();
      return true;
    }
    return super.onKeyDown(paramInt, paramKeyEvent);
  }

  protected void onResume()
  {
    super.onResume();
    findViewById(2131296312).setVisibility(8);
    findViewById(2131296311).setVisibility(8);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.BillPayPayeeImageCaptureActivity
 * JD-Core Version:    0.6.2
 */