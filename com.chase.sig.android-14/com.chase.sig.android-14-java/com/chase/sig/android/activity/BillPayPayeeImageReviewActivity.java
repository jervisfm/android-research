package com.chase.sig.android.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.TextView;
import com.chase.sig.android.d;
import com.chase.sig.android.domain.ImageCaptureData;
import com.chase.sig.android.service.billpay.BillPayPayeeSearchResponse;
import com.chase.sig.android.view.k.a;

public class BillPayPayeeImageReviewActivity extends ai
  implements fk.e
{
  private b a;
  private boolean b;
  private TextView c;
  private TextView d;
  private String k;
  private String l;
  private ImageCaptureData m;
  private byte[] n;
  private final GestureDetector.OnGestureListener o = new bu(this);

  private static Bitmap a(byte[] paramArrayOfByte)
  {
    try
    {
      BitmapFactory.Options localOptions = new BitmapFactory.Options();
      localOptions.inJustDecodeBounds = true;
      BitmapFactory.decodeByteArray(paramArrayOfByte, 0, paramArrayOfByte.length, localOptions);
      if ((!localOptions.mCancel) && (localOptions.outWidth != -1))
      {
        if (localOptions.outHeight == -1)
          return null;
        localOptions.inJustDecodeBounds = false;
        localOptions.inDither = false;
        localOptions.inPurgeable = true;
        localOptions.inPreferredConfig = Bitmap.Config.RGB_565;
        Bitmap localBitmap = BitmapFactory.decodeByteArray(paramArrayOfByte, 0, paramArrayOfByte.length, localOptions);
        return localBitmap;
      }
    }
    catch (OutOfMemoryError localOutOfMemoryError)
    {
    }
    return null;
  }

  protected final void a(int paramInt)
  {
    super.a(paramInt);
    if (2 == paramInt)
    {
      Intent localIntent = new Intent(this, BillPayPayeeAddActivity.class);
      localIntent.putExtra("selectedAccountId", this.k);
      localIntent.putExtra("image_capture_data", this.m);
      startActivity(localIntent);
    }
  }

  public final void a(Bundle paramBundle)
  {
    setContentView(2130903055);
    Bundle localBundle = getIntent().getExtras();
    if (localBundle != null)
      this.k = localBundle.getString("selectedAccountId");
    this.c = ((TextView)findViewById(2131296308));
    this.c.setText(getString(2131165605));
    this.d = ((TextView)findViewById(2131296314));
    this.d.setText(2131165607);
    findViewById(2131296315).setVisibility(4);
    findViewById(2131296319).setVisibility(8);
    FrameLayout localFrameLayout = (FrameLayout)findViewById(2131296305);
    GestureDetector localGestureDetector = new GestureDetector(this.o);
    this.a = new b(this);
    this.a.a(true);
    FrameLayout.LayoutParams localLayoutParams = new FrameLayout.LayoutParams(-1, -1);
    this.a.setLayoutParams(localLayoutParams);
    this.a.setOnTouchListener(new bt(this, localGestureDetector));
    localFrameLayout.addView(this.a, 0);
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    Object localObject = super.onCreateDialog(paramInt);
    if (paramInt == 1)
    {
      k.a locala = new k.a(this);
      locala.a(this.l).a(2131165553, new by(this)).b(2131165554, new bx(this));
      localObject = locala.d(-1);
    }
    return localObject;
  }

  protected void onDestroy()
  {
    super.onDestroy();
    this.a.a();
    this.a = null;
  }

  protected void onResume()
  {
    super.onResume();
    this.n = getIntent().getExtras().getByteArray("image_data");
    Bitmap localBitmap = a(this.n);
    findViewById(2131296287).setVisibility(0);
    Button localButton1 = (Button)findViewById(2131296311);
    localButton1.setVisibility(0);
    localButton1.setOnClickListener(new bv(this));
    Button localButton2 = (Button)findViewById(2131296312);
    localButton2.setVisibility(0);
    localButton2.setOnClickListener(new bw(this));
    this.a.a(localBitmap);
  }

  public static class a extends d<BillPayPayeeImageReviewActivity, byte[], Void, BillPayPayeeSearchResponse>
  {
  }

  private static final class b extends View
  {
    boolean a;
    private Bitmap b;
    private final Paint c = new Paint();
    private int d = 0;
    private int e = 0;
    private int f;
    private int g;
    private int h;
    private int i;

    public b(Context paramContext)
    {
      super();
    }

    public final void a()
    {
      this.b.recycle();
      this.b = null;
    }

    public final void a(float paramFloat1, float paramFloat2)
    {
      if (Math.abs(paramFloat1) > 4.0F)
        this.d = ((int)(paramFloat1 + this.d));
      if (Math.abs(paramFloat2) > 4.0F)
        this.e = ((int)(paramFloat2 + this.e));
      invalidate();
    }

    public final void a(Bitmap paramBitmap)
    {
      this.b = paramBitmap;
      this.h = paramBitmap.getWidth();
      this.i = paramBitmap.getHeight();
    }

    public final void a(boolean paramBoolean)
    {
      this.a = paramBoolean;
      invalidate();
    }

    protected final void onDraw(Canvas paramCanvas)
    {
      if ((this.b == null) || (this.b.getWidth() <= 0) || (this.b.getHeight() <= 0))
        return;
      if (this.a)
      {
        int i1 = this.b.getWidth();
        int i2 = this.b.getHeight();
        int i3 = getHeight();
        float f1 = getWidth() / i1;
        float f2 = i3 / i2;
        Matrix localMatrix = new Matrix();
        localMatrix.postScale(f1, f2);
        paramCanvas.drawBitmap(Bitmap.createBitmap(this.b, 0, 0, i1, i2, localMatrix, true), 0.0F, 0.0F, this.c);
        return;
      }
      this.f = (this.h - getWidth());
      this.g = (this.i - getHeight());
      int j;
      int k;
      label174: int m;
      if (this.f < 0)
      {
        j = 0;
        this.f = j;
        if (this.g >= 0)
          break label342;
        k = 0;
        this.g = k;
        this.d = Math.min(this.d, this.f);
        this.d = Math.max(this.d, 0);
        this.e = Math.min(this.e, this.g);
        this.e = Math.max(this.e, 0);
        if (this.h >= getWidth())
          break label350;
        m = this.h;
        label250: if (this.i >= getHeight())
          break label359;
      }
      label342: label350: label359: for (int n = this.i; ; n = getHeight())
      {
        Rect localRect1 = new Rect(this.d, this.e, m + this.d, n + this.e);
        Rect localRect2 = new Rect(0, 0, paramCanvas.getWidth(), paramCanvas.getHeight());
        paramCanvas.drawBitmap(this.b, localRect1, localRect2, this.c);
        return;
        j = this.f;
        break;
        k = this.g;
        break label174;
        m = getWidth();
        break label250;
      }
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.BillPayPayeeImageReviewActivity
 * JD-Core Version:    0.6.2
 */