package com.chase.sig.android.activity;

import android.content.Intent;
import android.os.AsyncTask.Status;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import com.chase.sig.android.activity.a.f;
import com.chase.sig.android.c;
import com.chase.sig.android.d;
import com.chase.sig.android.domain.ImageCaptureData;
import com.chase.sig.android.domain.MerchantPayee;
import com.chase.sig.android.service.billpay.BillPayPayeeSearchResponse;
import com.chase.sig.android.util.e;
import com.chase.sig.android.view.af;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class BillPayPayeeSearchResultActivity extends ai
  implements fk.e
{
  private List<MerchantPayee> a;
  private ListView b;
  private SimpleAdapter c;
  private String d;
  private String k;
  private String l;
  private String m;
  private String n;
  private String o;
  private ImageCaptureData p;

  private void d()
  {
    findViewById(2131296342).setVisibility(0);
    findViewById(2131296344).setVisibility(0);
    findViewById(2131296345).setVisibility(0);
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = this.a.iterator();
    while (localIterator.hasNext())
    {
      MerchantPayee localMerchantPayee = (MerchantPayee)localIterator.next();
      HashMap localHashMap = new HashMap();
      String str = localMerchantPayee.d() + " " + localMerchantPayee.e() + ", " + localMerchantPayee.f() + ", " + localMerchantPayee.g() + ", " + localMerchantPayee.h();
      localHashMap.put("payeename", localMerchantPayee.b());
      localHashMap.put("optionId", localMerchantPayee.a());
      localHashMap.put("address", str);
      localArrayList.add(localHashMap);
    }
    this.c = new SimpleAdapter(this, localArrayList, 2130903061, new String[] { "payeename", "address" }, new int[] { 2131296339, 2131296340 });
    this.b.setAdapter(new af(this.c));
  }

  protected final void a(int paramInt)
  {
    super.a(paramInt);
    if (paramInt == 0)
    {
      Intent localIntent = BillPayPayeeAddActivity.a(this, this.k, this.d, this.l);
      localIntent.putExtra("selectedAccountId", this.m);
      startActivity(localIntent);
      finish();
    }
  }

  public final void a(Bundle paramBundle)
  {
    setTitle(2131165579);
    b(2130903063);
    Bundle localBundle = getIntent().getExtras();
    if (e.a(localBundle, "image_capture_data"))
    {
      this.p = ((ImageCaptureData)e.a(localBundle, "image_capture_data", null));
      this.d = this.p.i();
      this.k = this.p.a();
      this.l = this.p.b();
      this.m = ((String)e.a(localBundle, "selectedAccountId", null));
      this.n = this.p.d();
      this.o = this.p.c();
      f localf = new f(this, BillPayPayeeAddActivity.class);
      if (this.p != null)
        localf.a("image_capture_data", this.p);
      localf.a("selectedAccountId", this.m);
      localf.a("payee_name", this.k);
      localf.a("payee_zip_code", this.d);
      localf.a("account_number", this.l);
      a(2131296342, localf);
      this.b = ((ListView)findViewById(2131296345));
      this.b.setOnItemClickListener(new cb(this));
      if ((paramBundle != null) || (localBundle.containsKey("image_search_results")))
        break label364;
      a locala = (a)this.e.a(a.class);
      if (locala.getStatus() != AsyncTask.Status.RUNNING)
        locala.execute(new String[0]);
    }
    while (true)
    {
      return;
      this.d = ((String)e.a(localBundle, "zip_code", null));
      this.k = ((String)e.a(localBundle, "payee_name", null));
      this.l = ((String)e.a(localBundle, "payee_account_number", null));
      this.m = ((String)e.a(localBundle, "selectedAccountId", null));
      this.n = ((String)e.a(localBundle, "scheduled_due_date", null));
      this.o = ((String)e.a(localBundle, "scheduled_amount_due", null));
      break;
      label364: if ((localBundle.containsKey("image_search_results")) && (localBundle.getSerializable("image_search_results") != null));
      for (this.a = ((List)localBundle.getSerializable("image_search_results")); this.a != null; this.a = ((List)paramBundle.getSerializable("payees")))
      {
        d();
        return;
      }
    }
  }

  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    if (paramInt1 == 25)
    {
      c.a();
      switch (paramInt2)
      {
      default:
      case 25:
      }
    }
    while (true)
    {
      super.onActivityResult(paramInt1, paramInt2, paramIntent);
      return;
      Intent localIntent = new Intent(this, BillPayPayeeAddActivity.class);
      localIntent.fillIn(paramIntent, 2);
      startActivity(localIntent);
    }
  }

  protected void onResume()
  {
    super.onResume();
    c.a();
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    paramBundle.putSerializable("payees", (Serializable)this.a);
  }

  public static class a extends d<BillPayPayeeSearchResultActivity, String, Void, BillPayPayeeSearchResponse>
  {
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.BillPayPayeeSearchResultActivity
 * JD-Core Version:    0.6.2
 */