package com.chase.sig.android.activity;

import android.content.Context;
import android.hardware.Camera;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;

public final class CapturePreview extends SurfaceView
  implements SurfaceHolder.Callback
{
  public static final Orientation a = Orientation.a;
  public Camera b;
  private Orientation c = Orientation.a;
  private int d = 30;
  private SurfaceHolder e;

  public CapturePreview(Context paramContext)
  {
    this(paramContext, 30, a);
  }

  public CapturePreview(Context paramContext, byte paramByte)
  {
    this(paramContext, 50, a);
  }

  public CapturePreview(Context paramContext, int paramInt, Orientation paramOrientation)
  {
    super(paramContext);
    this.d = paramInt;
    this.c = paramOrientation;
    this.e = getHolder();
    this.e.addCallback(this);
    this.e.setType(3);
  }

  public final int getJpegQuality()
  {
    return this.d;
  }

  public final void setJpegQuality(int paramInt)
  {
    this.d = paramInt;
  }

  // ERROR //
  public final void surfaceChanged(SurfaceHolder paramSurfaceHolder, int paramInt1, int paramInt2, int paramInt3)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 63	com/chase/sig/android/activity/CapturePreview:b	Landroid/hardware/Camera;
    //   4: ifnonnull +4 -> 8
    //   7: return
    //   8: aload_1
    //   9: ifnull +12 -> 21
    //   12: aload_1
    //   13: invokeinterface 67 1 0
    //   18: ifnull -11 -> 7
    //   21: aload_0
    //   22: getfield 63	com/chase/sig/android/activity/CapturePreview:b	Landroid/hardware/Camera;
    //   25: aload_1
    //   26: invokevirtual 73	android/hardware/Camera:setPreviewDisplay	(Landroid/view/SurfaceHolder;)V
    //   29: aload_0
    //   30: getfield 63	com/chase/sig/android/activity/CapturePreview:b	Landroid/hardware/Camera;
    //   33: invokevirtual 77	android/hardware/Camera:getParameters	()Landroid/hardware/Camera$Parameters;
    //   36: astore 6
    //   38: aload 6
    //   40: ldc 79
    //   42: invokevirtual 85	android/hardware/Camera$Parameters:setFocusMode	(Ljava/lang/String;)V
    //   45: aload 6
    //   47: invokevirtual 89	android/hardware/Camera$Parameters:getSupportedFlashModes	()Ljava/util/List;
    //   50: astore 22
    //   52: aload 22
    //   54: ifnull +69 -> 123
    //   57: aload 22
    //   59: invokeinterface 94 1 0
    //   64: ifle +59 -> 123
    //   67: aconst_null
    //   68: astore 23
    //   70: aload 22
    //   72: invokeinterface 98 1 0
    //   77: astore 24
    //   79: aload 24
    //   81: invokeinterface 104 1 0
    //   86: ifeq +25 -> 111
    //   89: ldc 79
    //   91: aload 24
    //   93: invokeinterface 108 1 0
    //   98: checkcast 110	java/lang/String
    //   101: invokevirtual 114	java/lang/String:equalsIgnoreCase	(Ljava/lang/String;)Z
    //   104: ifeq +260 -> 364
    //   107: ldc 79
    //   109: astore 23
    //   111: aload 23
    //   113: ifnull +10 -> 123
    //   116: aload 6
    //   118: aload 23
    //   120: invokevirtual 117	android/hardware/Camera$Parameters:setFlashMode	(Ljava/lang/String;)V
    //   123: aload 6
    //   125: invokevirtual 120	android/hardware/Camera$Parameters:getSupportedPictureSizes	()Ljava/util/List;
    //   128: astore 25
    //   130: aload 25
    //   132: ifnonnull +239 -> 371
    //   135: aconst_null
    //   136: astore 30
    //   138: aload 6
    //   140: aload 30
    //   142: getfield 125	android/hardware/Camera$Size:width	I
    //   145: aload 30
    //   147: getfield 128	android/hardware/Camera$Size:height	I
    //   150: invokevirtual 132	android/hardware/Camera$Parameters:setPictureSize	(II)V
    //   153: aload 6
    //   155: sipush 256
    //   158: invokevirtual 135	android/hardware/Camera$Parameters:setPictureFormat	(I)V
    //   161: aload 6
    //   163: invokevirtual 138	android/hardware/Camera$Parameters:getSupportedPreviewSizes	()Ljava/util/List;
    //   166: astore 31
    //   168: iload_3
    //   169: i2d
    //   170: iload 4
    //   172: i2d
    //   173: ddiv
    //   174: dstore 32
    //   176: aload 31
    //   178: ifnonnull +390 -> 568
    //   181: aconst_null
    //   182: astore 34
    //   184: aload_0
    //   185: getfield 32	com/chase/sig/android/activity/CapturePreview:c	Lcom/chase/sig/android/activity/CapturePreview$Orientation;
    //   188: getstatic 140	com/chase/sig/android/activity/CapturePreview$Orientation:b	Lcom/chase/sig/android/activity/CapturePreview$Orientation;
    //   191: if_acmpne +587 -> 778
    //   194: aload 6
    //   196: bipush 90
    //   198: invokevirtual 143	android/hardware/Camera$Parameters:setRotation	(I)V
    //   201: getstatic 148	android/os/Build$VERSION:SDK_INT	I
    //   204: bipush 8
    //   206: if_icmplt +12 -> 218
    //   209: aload_0
    //   210: getfield 63	com/chase/sig/android/activity/CapturePreview:b	Landroid/hardware/Camera;
    //   213: bipush 90
    //   215: invokevirtual 151	android/hardware/Camera:setDisplayOrientation	(I)V
    //   218: aload 30
    //   220: getfield 128	android/hardware/Camera$Size:height	I
    //   223: aload 30
    //   225: getfield 125	android/hardware/Camera$Size:width	I
    //   228: if_icmple +532 -> 760
    //   231: aload 6
    //   233: aload 34
    //   235: getfield 128	android/hardware/Camera$Size:height	I
    //   238: aload 34
    //   240: getfield 125	android/hardware/Camera$Size:width	I
    //   243: invokevirtual 154	android/hardware/Camera$Parameters:setPreviewSize	(II)V
    //   246: aload 6
    //   248: aload_0
    //   249: getfield 34	com/chase/sig/android/activity/CapturePreview:d	I
    //   252: invokevirtual 156	android/hardware/Camera$Parameters:setJpegQuality	(I)V
    //   255: aload_0
    //   256: getfield 63	com/chase/sig/android/activity/CapturePreview:b	Landroid/hardware/Camera;
    //   259: aload 6
    //   261: invokevirtual 160	android/hardware/Camera:setParameters	(Landroid/hardware/Camera$Parameters;)V
    //   264: aload 6
    //   266: invokevirtual 163	android/hardware/Camera$Parameters:getSupportedSceneModes	()Ljava/util/List;
    //   269: astore 19
    //   271: aload 19
    //   273: ifnull +59 -> 332
    //   276: aload 19
    //   278: invokeinterface 98 1 0
    //   283: astore 20
    //   285: aload 20
    //   287: invokeinterface 104 1 0
    //   292: ifeq +607 -> 899
    //   295: ldc 165
    //   297: aload 20
    //   299: invokeinterface 108 1 0
    //   304: checkcast 110	java/lang/String
    //   307: invokevirtual 114	java/lang/String:equalsIgnoreCase	(Ljava/lang/String;)Z
    //   310: ifeq -25 -> 285
    //   313: ldc 165
    //   315: astore 21
    //   317: aload 21
    //   319: invokestatic 170	com/chase/sig/android/util/s:m	(Ljava/lang/String;)Z
    //   322: ifeq +10 -> 332
    //   325: aload 6
    //   327: aload 21
    //   329: invokevirtual 173	android/hardware/Camera$Parameters:setSceneMode	(Ljava/lang/String;)V
    //   332: aload_0
    //   333: getfield 63	com/chase/sig/android/activity/CapturePreview:b	Landroid/hardware/Camera;
    //   336: aload 6
    //   338: invokevirtual 160	android/hardware/Camera:setParameters	(Landroid/hardware/Camera$Parameters;)V
    //   341: aload_0
    //   342: getfield 63	com/chase/sig/android/activity/CapturePreview:b	Landroid/hardware/Camera;
    //   345: invokevirtual 176	android/hardware/Camera:startPreview	()V
    //   348: return
    //   349: astore 8
    //   351: aload_0
    //   352: invokevirtual 180	com/chase/sig/android/activity/CapturePreview:getContext	()Landroid/content/Context;
    //   355: checkcast 182	com/chase/sig/android/activity/eb
    //   358: ldc 183
    //   360: invokevirtual 186	com/chase/sig/android/activity/eb:f	(I)V
    //   363: return
    //   364: ldc 188
    //   366: astore 23
    //   368: goto -289 -> 79
    //   371: aconst_null
    //   372: astore 26
    //   374: ldc2_w 189
    //   377: dstore 27
    //   379: aload 25
    //   381: invokeinterface 98 1 0
    //   386: astore 29
    //   388: aload 29
    //   390: invokeinterface 104 1 0
    //   395: ifeq +87 -> 482
    //   398: aload 29
    //   400: invokeinterface 108 1 0
    //   405: checkcast 122	android/hardware/Camera$Size
    //   408: astore 62
    //   410: aload 62
    //   412: getfield 125	android/hardware/Camera$Size:width	I
    //   415: i2d
    //   416: aload 62
    //   418: getfield 128	android/hardware/Camera$Size:height	I
    //   421: i2d
    //   422: ddiv
    //   423: ldc2_w 191
    //   426: dsub
    //   427: invokestatic 198	java/lang/Math:abs	(D)D
    //   430: ldc2_w 199
    //   433: dcmpl
    //   434: ifgt -46 -> 388
    //   437: sipush -1200
    //   440: aload 62
    //   442: getfield 128	android/hardware/Camera$Size:height	I
    //   445: iadd
    //   446: invokestatic 203	java/lang/Math:abs	(I)I
    //   449: i2d
    //   450: dload 27
    //   452: dcmpg
    //   453: ifge +485 -> 938
    //   456: sipush -1200
    //   459: aload 62
    //   461: getfield 128	android/hardware/Camera$Size:height	I
    //   464: iadd
    //   465: invokestatic 203	java/lang/Math:abs	(I)I
    //   468: i2d
    //   469: dstore 66
    //   471: aload 62
    //   473: astore 65
    //   475: dload 66
    //   477: dstore 63
    //   479: goto +467 -> 946
    //   482: aload 26
    //   484: ifnonnull +484 -> 968
    //   487: ldc2_w 189
    //   490: dstore 53
    //   492: aload 25
    //   494: invokeinterface 98 1 0
    //   499: astore 55
    //   501: aload 55
    //   503: invokeinterface 104 1 0
    //   508: ifeq +460 -> 968
    //   511: aload 55
    //   513: invokeinterface 108 1 0
    //   518: checkcast 122	android/hardware/Camera$Size
    //   521: astore 56
    //   523: sipush -1200
    //   526: aload 56
    //   528: getfield 128	android/hardware/Camera$Size:height	I
    //   531: iadd
    //   532: invokestatic 203	java/lang/Math:abs	(I)I
    //   535: i2d
    //   536: dload 53
    //   538: dcmpg
    //   539: ifge +388 -> 927
    //   542: sipush -1200
    //   545: aload 56
    //   547: getfield 128	android/hardware/Camera$Size:height	I
    //   550: iadd
    //   551: invokestatic 203	java/lang/Math:abs	(I)I
    //   554: i2d
    //   555: dstore 60
    //   557: aload 56
    //   559: astore 59
    //   561: dload 60
    //   563: dstore 57
    //   565: goto +392 -> 957
    //   568: aconst_null
    //   569: astore 34
    //   571: ldc2_w 189
    //   574: dstore 35
    //   576: aload 31
    //   578: invokeinterface 98 1 0
    //   583: astore 37
    //   585: aload 37
    //   587: invokeinterface 104 1 0
    //   592: ifeq +84 -> 676
    //   595: aload 37
    //   597: invokeinterface 108 1 0
    //   602: checkcast 122	android/hardware/Camera$Size
    //   605: astore 47
    //   607: aload 47
    //   609: getfield 125	android/hardware/Camera$Size:width	I
    //   612: i2d
    //   613: aload 47
    //   615: getfield 128	android/hardware/Camera$Size:height	I
    //   618: i2d
    //   619: ddiv
    //   620: dload 32
    //   622: dsub
    //   623: invokestatic 198	java/lang/Math:abs	(D)D
    //   626: ldc2_w 204
    //   629: dcmpl
    //   630: ifgt -45 -> 585
    //   633: aload 47
    //   635: getfield 128	android/hardware/Camera$Size:height	I
    //   638: iload 4
    //   640: isub
    //   641: invokestatic 203	java/lang/Math:abs	(I)I
    //   644: i2d
    //   645: dload 35
    //   647: dcmpg
    //   648: ifge +268 -> 916
    //   651: aload 47
    //   653: getfield 128	android/hardware/Camera$Size:height	I
    //   656: iload 4
    //   658: isub
    //   659: invokestatic 203	java/lang/Math:abs	(I)I
    //   662: i2d
    //   663: dstore 51
    //   665: aload 47
    //   667: astore 50
    //   669: dload 51
    //   671: dstore 48
    //   673: goto +302 -> 975
    //   676: aload 34
    //   678: ifnonnull -494 -> 184
    //   681: ldc2_w 189
    //   684: dstore 38
    //   686: aload 31
    //   688: invokeinterface 98 1 0
    //   693: astore 40
    //   695: aload 40
    //   697: invokeinterface 104 1 0
    //   702: ifeq -518 -> 184
    //   705: aload 40
    //   707: invokeinterface 108 1 0
    //   712: checkcast 122	android/hardware/Camera$Size
    //   715: astore 41
    //   717: aload 41
    //   719: getfield 128	android/hardware/Camera$Size:height	I
    //   722: iload 4
    //   724: isub
    //   725: invokestatic 203	java/lang/Math:abs	(I)I
    //   728: i2d
    //   729: dload 38
    //   731: dcmpg
    //   732: ifge +173 -> 905
    //   735: aload 41
    //   737: getfield 128	android/hardware/Camera$Size:height	I
    //   740: iload 4
    //   742: isub
    //   743: invokestatic 203	java/lang/Math:abs	(I)I
    //   746: i2d
    //   747: dstore 45
    //   749: aload 41
    //   751: astore 44
    //   753: dload 45
    //   755: dstore 42
    //   757: goto +229 -> 986
    //   760: aload 6
    //   762: aload 34
    //   764: getfield 125	android/hardware/Camera$Size:width	I
    //   767: aload 34
    //   769: getfield 128	android/hardware/Camera$Size:height	I
    //   772: invokevirtual 154	android/hardware/Camera$Parameters:setPreviewSize	(II)V
    //   775: goto -529 -> 246
    //   778: aload 6
    //   780: aload 34
    //   782: getfield 125	android/hardware/Camera$Size:width	I
    //   785: aload 34
    //   787: getfield 128	android/hardware/Camera$Size:height	I
    //   790: invokevirtual 154	android/hardware/Camera$Parameters:setPreviewSize	(II)V
    //   793: goto -547 -> 246
    //   796: astore 9
    //   798: iconst_0
    //   799: anewarray 207	java/lang/Object
    //   802: astore 10
    //   804: aload 10
    //   806: arraylength
    //   807: ifgt +71 -> 878
    //   810: ldc 209
    //   812: astore 11
    //   814: aload 11
    //   816: invokevirtual 212	java/lang/String:length	()I
    //   819: istore 12
    //   821: iload 12
    //   823: sipush 4000
    //   826: idiv
    //   827: istore 13
    //   829: iload 12
    //   831: sipush 4000
    //   834: irem
    //   835: ifle +167 -> 1002
    //   838: iinc 13 1
    //   841: goto +161 -> 1002
    //   844: ldc 214
    //   846: ldc 216
    //   848: iconst_1
    //   849: anewarray 207	java/lang/Object
    //   852: dup
    //   853: iconst_0
    //   854: aload 11
    //   856: iload 15
    //   858: iload 16
    //   860: invokevirtual 220	java/lang/String:substring	(II)Ljava/lang/String;
    //   863: aastore
    //   864: invokestatic 224	java/lang/String:format	(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    //   867: aconst_null
    //   868: invokestatic 229	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   871: pop
    //   872: iinc 14 1
    //   875: goto +130 -> 1005
    //   878: ldc 209
    //   880: aload 10
    //   882: invokestatic 224	java/lang/String:format	(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    //   885: astore 18
    //   887: aload 18
    //   889: astore 11
    //   891: goto -77 -> 814
    //   894: astore 5
    //   896: goto -867 -> 29
    //   899: aconst_null
    //   900: astore 21
    //   902: goto -585 -> 317
    //   905: dload 38
    //   907: dstore 42
    //   909: aload 34
    //   911: astore 44
    //   913: goto +73 -> 986
    //   916: dload 35
    //   918: dstore 48
    //   920: aload 34
    //   922: astore 50
    //   924: goto +51 -> 975
    //   927: dload 53
    //   929: dstore 57
    //   931: aload 26
    //   933: astore 59
    //   935: goto +22 -> 957
    //   938: dload 27
    //   940: dstore 63
    //   942: aload 26
    //   944: astore 65
    //   946: aload 65
    //   948: astore 26
    //   950: dload 63
    //   952: dstore 27
    //   954: goto -566 -> 388
    //   957: aload 59
    //   959: astore 26
    //   961: dload 57
    //   963: dstore 53
    //   965: goto -464 -> 501
    //   968: aload 26
    //   970: astore 30
    //   972: goto -834 -> 138
    //   975: aload 50
    //   977: astore 34
    //   979: dload 48
    //   981: dstore 35
    //   983: goto -398 -> 585
    //   986: aload 44
    //   988: astore 34
    //   990: dload 42
    //   992: dstore 38
    //   994: goto -299 -> 695
    //   997: astore 7
    //   999: goto -744 -> 255
    //   1002: iconst_0
    //   1003: istore 14
    //   1005: iload 14
    //   1007: iload 13
    //   1009: if_icmpge -668 -> 341
    //   1012: iload 14
    //   1014: sipush 4000
    //   1017: imul
    //   1018: istore 15
    //   1020: iload 15
    //   1022: sipush 4000
    //   1025: iadd
    //   1026: istore 16
    //   1028: iload 16
    //   1030: iload 12
    //   1032: if_icmple -188 -> 844
    //   1035: iload 12
    //   1037: istore 16
    //   1039: goto -195 -> 844
    //
    // Exception table:
    //   from	to	target	type
    //   255	264	349	java/lang/Exception
    //   341	348	349	java/lang/Exception
    //   798	810	349	java/lang/Exception
    //   814	829	349	java/lang/Exception
    //   844	872	349	java/lang/Exception
    //   878	887	349	java/lang/Exception
    //   264	271	796	java/lang/Exception
    //   276	285	796	java/lang/Exception
    //   285	313	796	java/lang/Exception
    //   317	332	796	java/lang/Exception
    //   332	341	796	java/lang/Exception
    //   21	29	894	java/io/IOException
    //   38	52	997	java/lang/Throwable
    //   57	67	997	java/lang/Throwable
    //   70	79	997	java/lang/Throwable
    //   79	107	997	java/lang/Throwable
    //   116	123	997	java/lang/Throwable
    //   123	130	997	java/lang/Throwable
    //   138	176	997	java/lang/Throwable
    //   184	218	997	java/lang/Throwable
    //   218	246	997	java/lang/Throwable
    //   246	255	997	java/lang/Throwable
    //   379	388	997	java/lang/Throwable
    //   388	471	997	java/lang/Throwable
    //   492	501	997	java/lang/Throwable
    //   501	557	997	java/lang/Throwable
    //   576	585	997	java/lang/Throwable
    //   585	665	997	java/lang/Throwable
    //   686	695	997	java/lang/Throwable
    //   695	749	997	java/lang/Throwable
    //   760	775	997	java/lang/Throwable
    //   778	793	997	java/lang/Throwable
  }

  public final void surfaceCreated(SurfaceHolder paramSurfaceHolder)
  {
    if (paramSurfaceHolder == null)
      return;
    try
    {
      this.b = Camera.open();
      return;
    }
    catch (RuntimeException localRuntimeException)
    {
      ((QuickDepositCaptureActivity)getContext()).f(2131165828);
    }
  }

  public final void surfaceDestroyed(SurfaceHolder paramSurfaceHolder)
  {
    if (this.b == null)
      return;
    this.b.setPreviewCallback(null);
    this.b.stopPreview();
    this.b.release();
    this.b = null;
  }

  public static enum Orientation
  {
    static
    {
      Orientation[] arrayOfOrientation = new Orientation[2];
      arrayOfOrientation[0] = a;
      arrayOfOrientation[1] = b;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.CapturePreview
 * JD-Core Version:    0.6.2
 */