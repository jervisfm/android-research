package com.chase.sig.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.chase.sig.android.activity.a.f;
import com.chase.sig.android.domain.Email;
import com.chase.sig.android.domain.PhoneBookContact;
import com.chase.sig.android.domain.QuickPayRecipient;
import com.chase.sig.android.domain.RecipientContact;
import com.chase.sig.android.view.af;
import java.util.ArrayList;
import java.util.List;

public class ContactEmailAddressesActivity extends ai
{
  private PhoneBookContact a;
  private ListView b;
  private ListView c;
  private QuickPayRecipient d;

  private void a(PhoneBookContact paramPhoneBookContact)
  {
    ArrayAdapter localArrayAdapter = new ArrayAdapter(this, 2130903214, paramPhoneBookContact.a().toArray());
    this.b.setVisibility(0);
    this.b.setAdapter(new af(localArrayAdapter));
    this.b.setOnItemClickListener(new cf(this));
  }

  public static long[] a(ListAdapter paramListAdapter, SparseBooleanArray paramSparseBooleanArray)
  {
    int i = 0;
    long[] arrayOfLong;
    int n;
    int i1;
    if ((paramSparseBooleanArray != null) && (paramListAdapter != null))
    {
      int j = paramSparseBooleanArray.size();
      int k = 0;
      int m = 0;
      while (k < j)
      {
        if (paramSparseBooleanArray.valueAt(k))
          m++;
        k++;
      }
      arrayOfLong = new long[m];
      n = 0;
      if (n < j)
      {
        if (!paramSparseBooleanArray.valueAt(n))
          break label106;
        i1 = i + 1;
        arrayOfLong[i] = paramListAdapter.getItemId(paramSparseBooleanArray.keyAt(n));
      }
    }
    while (true)
    {
      n++;
      i = i1;
      break;
      return arrayOfLong;
      return new long[0];
      label106: i1 = i;
    }
  }

  private void d()
  {
    ArrayList localArrayList = new ArrayList();
    RecipientContact localRecipientContact = new RecipientContact();
    localRecipientContact.c("None");
    localArrayList.add(localRecipientContact);
    ArrayAdapter localArrayAdapter = new ArrayAdapter(this, 2130903214, localArrayList.toArray());
    this.c.setVisibility(0);
    this.c.setAdapter(new af(localArrayAdapter));
    this.c.setOnItemClickListener(new cg(this));
  }

  public final void a(Bundle paramBundle)
  {
    b(2130903067);
    setTitle(2131165283);
    this.b = ((ListView)findViewById(2131296363));
    this.c = ((ListView)findViewById(2131296364));
    this.a = ((PhoneBookContact)getIntent().getExtras().getSerializable("contact"));
    this.d = ((QuickPayRecipient)getIntent().getExtras().getSerializable("recipient"));
    a(this.a);
    d();
    a(2131296362, new ce(this));
    findViewById(2131296361).setOnClickListener(a(QuickPayChooseRecipientActivity.class).c().b().a());
  }

  protected final QuickPayRecipient b()
  {
    this.d.h();
    SparseBooleanArray localSparseBooleanArray = this.b.getCheckedItemPositions();
    ListAdapter localListAdapter = this.b.getAdapter();
    int i = localListAdapter.getCount();
    boolean bool1 = true;
    int j = 0;
    if (j < i)
    {
      if (!localSparseBooleanArray.get(j))
        break label105;
      String str = ((RecipientContact)localListAdapter.getItem(j)).b();
      this.d.a(new Email(str, bool1, "0"));
    }
    label105: for (boolean bool2 = false; ; bool2 = bool1)
    {
      j++;
      bool1 = bool2;
      break;
      return this.d;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.ContactEmailAddressesActivity
 * JD-Core Version:    0.6.2
 */