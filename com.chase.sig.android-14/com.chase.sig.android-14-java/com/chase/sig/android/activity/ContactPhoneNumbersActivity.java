package com.chase.sig.android.activity;

import android.content.Intent;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.ListView;
import android.widget.TextView;
import com.chase.sig.android.activity.a.f;
import com.chase.sig.android.domain.PhoneBookContact;
import com.chase.sig.android.domain.QuickPayRecipient;
import com.chase.sig.android.domain.RecipientContact;
import com.chase.sig.android.view.af;
import java.util.List;

public class ContactPhoneNumbersActivity extends ai
{
  private PhoneBookContact a;
  private a b;
  private CheckedTextView c;
  private ListView d;
  private QuickPayRecipient k;
  private final cc l = cc.b();
  private int m = -1;

  public final void a(Bundle paramBundle)
  {
    b(2130903068);
    setTitle(2131165284);
    this.a = ((PhoneBookContact)getIntent().getExtras().getSerializable("contact"));
    this.k = ((QuickPayRecipient)getIntent().getExtras().getSerializable("recipient"));
    int i;
    int i1;
    if (paramBundle == null)
    {
      i = -1;
      this.m = i;
      PhoneBookContact localPhoneBookContact = this.a;
      int j = this.m;
      List localList = localPhoneBookContact.b();
      RecipientContact localRecipientContact = new RecipientContact();
      localRecipientContact.b("None");
      localRecipientContact.c("");
      localRecipientContact.a("None");
      localRecipientContact.d("-2");
      localList.add(localRecipientContact);
      this.b = new a(localPhoneBookContact.b());
      this.d = ((ListView)findViewById(2131296367));
      this.d.setAdapter(new af(this.b));
      this.d.setOnItemClickListener(new ch(this));
      if (j != -1)
        break label324;
      String str = this.l.a(this);
      int n = this.b.getCount();
      i1 = 0;
      label215: if (i1 < n)
      {
        if (!((RecipientContact)this.b.getItem(i1)).a().equals(str))
          break label318;
        this.b.a(i1);
      }
    }
    while (true)
    {
      Button localButton = (Button)findViewById(2131296368);
      localButton.setText("Next");
      localButton.setOnClickListener(new ci(this));
      a(2131296361, a(QuickPayChooseRecipientActivity.class).b().a().c());
      return;
      i = paramBundle.getInt("selectedPhone", -1);
      break;
      label318: i1++;
      break label215;
      label324: this.b.a(this.m);
    }
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    paramBundle.putInt("selectedPhone", this.m);
  }

  private final class a extends BaseAdapter
  {
    private final List<RecipientContact> b;
    private int c = -1;

    public a()
    {
      Object localObject;
      this.b = localObject;
    }

    public final RecipientContact a()
    {
      return (RecipientContact)this.b.get(this.c);
    }

    public final void a(int paramInt)
    {
      this.c = paramInt;
      super.notifyDataSetChanged();
    }

    public final boolean areAllItemsEnabled()
    {
      return true;
    }

    public final int b()
    {
      return this.c;
    }

    public final int getCount()
    {
      return this.b.size();
    }

    public final Object getItem(int paramInt)
    {
      return this.b.get(paramInt);
    }

    public final long getItemId(int paramInt)
    {
      return Long.parseLong(((RecipientContact)this.b.get(paramInt)).c());
    }

    public final int getItemViewType(int paramInt)
    {
      return 0;
    }

    public final View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      if (paramView == null)
        paramView = ContactPhoneNumbersActivity.this.getLayoutInflater().inflate(2130903141, null);
      RecipientContact localRecipientContact = (RecipientContact)this.b.get(paramInt);
      CheckedTextView localCheckedTextView = (CheckedTextView)paramView.findViewById(2131296617);
      localCheckedTextView.setText(localRecipientContact.b());
      if (this.c == paramInt);
      for (boolean bool = true; ; bool = false)
      {
        localCheckedTextView.setChecked(bool);
        ((TextView)paramView.findViewById(2131296496)).setText(localRecipientContact.a());
        return paramView;
      }
    }

    public final int getViewTypeCount()
    {
      return 1;
    }

    public final boolean hasStableIds()
    {
      return true;
    }

    public final boolean isEmpty()
    {
      return (this.b == null) || (this.b.isEmpty());
    }

    public final boolean isEnabled(int paramInt)
    {
      return true;
    }

    public final void registerDataSetObserver(DataSetObserver paramDataSetObserver)
    {
      super.registerDataSetObserver(paramDataSetObserver);
    }

    public final void unregisterDataSetObserver(DataSetObserver paramDataSetObserver)
    {
      super.unregisterDataSetObserver(paramDataSetObserver);
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.ContactPhoneNumbersActivity
 * JD-Core Version:    0.6.2
 */