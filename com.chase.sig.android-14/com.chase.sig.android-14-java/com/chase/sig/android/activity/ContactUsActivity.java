package com.chase.sig.android.activity;

import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.ChaseApplication;

public class ContactUsActivity extends eb
{
  public final void a(Bundle paramBundle)
  {
    b(2130903069);
    a(2131296369, new a(2131165367, ((TextView)findViewById(2131296370)).getText().toString()));
    a(2131296372, new a(2131165366, ((TextView)findViewById(2131296373)).getText().toString()));
    a(2131296375, new a(2131165368, ((TextView)findViewById(2131296376)).getText().toString()));
    View localView = findViewById(2131296378);
    if ((getResources().getString(2131165184).equalsIgnoreCase("PBD")) && (!ChaseApplication.a().c()))
      localView.setVisibility(8);
    localView.setOnClickListener(new cj(this));
  }

  private final class a
    implements View.OnClickListener
  {
    private int b;
    private String c;

    public a(int paramString, String arg3)
    {
      this.b = paramString;
      Object localObject;
      this.c = localObject;
    }

    public final void onClick(View paramView)
    {
      BehaviorAnalyticsAspect.a();
      BehaviorAnalyticsAspect.a(paramView);
      String str = ContactUsActivity.this.getString(this.b);
      ContactUsActivity localContactUsActivity = ContactUsActivity.this;
      Object[] arrayOfObject = new Object[2];
      arrayOfObject[0] = this.c;
      arrayOfObject[1] = str;
      localContactUsActivity.b(str, String.format("Please call %s at %s", arrayOfObject), false);
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.ContactUsActivity
 * JD-Core Version:    0.6.2
 */