package com.chase.sig.android.activity;

import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import com.chase.sig.android.domain.PhoneBookContact;
import com.chase.sig.android.domain.QuickPayRecipient;
import com.chase.sig.android.service.ServiceError;
import com.chase.sig.android.view.af;
import java.util.ArrayList;
import java.util.List;

public class ContactsActivity extends ai
{
  private ListView a = null;
  private final cc b = cc.b();

  private static QuickPayRecipient c(PhoneBookContact paramPhoneBookContact)
  {
    QuickPayRecipient localQuickPayRecipient = new QuickPayRecipient();
    localQuickPayRecipient.a(paramPhoneBookContact.c());
    return localQuickPayRecipient;
  }

  protected final void a(int paramInt)
  {
    super.a(paramInt);
    if (paramInt == 1)
    {
      Intent localIntent = new Intent(this, QuickPayChooseRecipientActivity.class);
      localIntent.putExtras(getIntent());
      localIntent.putExtra("fromContactsActivity", true);
      localIntent.putExtra("recipient", new QuickPayRecipient());
      startActivity(localIntent);
    }
  }

  public final void a(Bundle paramBundle)
  {
    b(2130903071);
    setTitle(2131165845);
    this.a = ((ListView)findViewById(2131296383));
    String[] arrayOfString = { "_id", "display_name" };
    Cursor localCursor = managedQuery(this.b.a().getData(), arrayOfString, null, null, "display_name COLLATE LOCALIZED ASC");
    ArrayList localArrayList = new ArrayList();
    localArrayList.add(new ServiceError(getResources().getString(2131165768), false));
    if (localCursor.getCount() == 0)
      a(1, localArrayList);
    af localaf = new af(new SimpleCursorAdapter(this, 2130903140, localCursor, new String[] { "display_name" }, new int[] { 2131296616 }));
    this.a.setAdapter(localaf);
    this.a.setOnItemClickListener(new ck(this));
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.ContactsActivity
 * JD-Core Version:    0.6.2
 */