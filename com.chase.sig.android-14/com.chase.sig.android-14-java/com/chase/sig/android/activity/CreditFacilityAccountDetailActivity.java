package com.chase.sig.android.activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.chase.sig.android.d;
import com.chase.sig.android.domain.CreditFacilityAccountDetail;
import com.chase.sig.android.domain.CreditFacilityAccountLoanDetail;
import com.chase.sig.android.service.CreditFacilityAccountDetailResponse;
import com.chase.sig.android.util.e;
import java.util.List;

public class CreditFacilityAccountDetailActivity extends ai
  implements fk.d
{
  private CreditFacilityAccountDetail a;
  private ListView b;
  private View c;
  private RelativeLayout d;
  private Button k;
  private boolean l = false;

  private void a(CreditFacilityAccountDetail paramCreditFacilityAccountDetail)
  {
    this.a = paramCreditFacilityAccountDetail;
    ((TextView)findViewById(2131296385)).setText(this.a.d());
    ((TextView)findViewById(2131296390)).setText(this.a.c());
    ((TextView)findViewById(2131296392)).setText(String.valueOf(this.a.a()));
    ((TextView)findViewById(2131296394)).setText(String.valueOf(this.a.b()));
    ((TextView)findViewById(2131296396)).setText(this.a.e());
    List localList = this.a.f();
    if (localList.isEmpty())
    {
      findViewById(2131296400).setVisibility(0);
      findViewById(2131296402).setVisibility(8);
      return;
    }
    this.b.setAdapter(new b(this, localList));
  }

  public final void a(Bundle paramBundle)
  {
    int i = 8;
    b(2130903072);
    setTitle(2131165392);
    this.c = findViewById(2131296386);
    this.d = ((RelativeLayout)findViewById(2131296388));
    this.k = ((Button)findViewById(2131296399));
    boolean bool;
    int j;
    label89: label108: Drawable localDrawable;
    label138: String str1;
    String str2;
    if (paramBundle != null)
    {
      if (paramBundle.getInt("account_summary_state", 0) == 0)
      {
        bool = false;
        this.l = bool;
      }
    }
    else
    {
      View localView = this.c;
      if (!this.l)
        break label254;
      j = i;
      localView.setVisibility(j);
      RelativeLayout localRelativeLayout = this.d;
      if (!this.l)
        break label260;
      localRelativeLayout.setVisibility(i);
      Button localButton = this.k;
      if (!this.l)
        break label265;
      localDrawable = getResources().getDrawable(2130837741);
      localButton.setBackgroundDrawable(localDrawable);
      Bundle localBundle = getIntent().getExtras();
      str1 = localBundle.getString("selectedAccountId");
      str2 = localBundle.getString("outstandingValue");
      this.b = ((ListView)findViewById(2131296402));
      if (!e.a(paramBundle, "credit_facility_account_detail"))
        break label279;
      this.a = ((CreditFacilityAccountDetail)paramBundle.getSerializable("credit_facility_account_detail"));
      a(this.a);
    }
    while (true)
    {
      findViewById(2131296397).setOnClickListener(new cl(this));
      this.k.setOnClickListener(new cm(this));
      return;
      bool = true;
      break;
      label254: j = 0;
      break label89;
      label260: i = 0;
      break label108;
      label265: localDrawable = getResources().getDrawable(2130837742);
      break label138;
      label279: a(a.class, new String[] { str1, str2 });
    }
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    if (this.a != null)
    {
      super.onSaveInstanceState(paramBundle);
      paramBundle.putSerializable("credit_facility_account_detail", this.a);
    }
    paramBundle.putInt("account_summary_state", this.d.getVisibility());
  }

  public static class a extends d<CreditFacilityAccountDetailActivity, String, Void, CreditFacilityAccountDetailResponse>
  {
    private String a;
  }

  private final class b extends ArrayAdapter<CreditFacilityAccountLoanDetail>
  {
    private List<CreditFacilityAccountLoanDetail> b;

    public b(List<CreditFacilityAccountLoanDetail> arg2)
    {
      this(localContext, localList, (byte)0);
    }

    private b(int paramByte1, byte paramByte2, byte arg4)
    {
      super(2130903127);
      this.b = paramByte2;
    }

    public final int getCount()
    {
      return this.b.size();
    }

    public final long getItemId(int paramInt)
    {
      return paramInt;
    }

    public final View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      if (paramView == null)
        paramView = CreditFacilityAccountDetailActivity.this.getLayoutInflater().inflate(2130903127, null);
      CreditFacilityAccountLoanDetail localCreditFacilityAccountLoanDetail = (CreditFacilityAccountLoanDetail)this.b.get(paramInt);
      TextView localTextView = (TextView)paramView.findViewById(2131296535);
      Object[] arrayOfObject = new Object[2];
      arrayOfObject[0] = getContext().getString(2131165857);
      arrayOfObject[1] = localCreditFacilityAccountLoanDetail.a();
      localTextView.setText(String.format("%s   %s", arrayOfObject));
      ((TextView)paramView.findViewById(2131296537)).setText(localCreditFacilityAccountLoanDetail.b());
      ((TextView)paramView.findViewById(2131296539)).setText(localCreditFacilityAccountLoanDetail.e());
      ((TextView)paramView.findViewById(2131296541)).setText(localCreditFacilityAccountLoanDetail.f());
      ((TextView)paramView.findViewById(2131296543)).setText(localCreditFacilityAccountLoanDetail.c());
      ((TextView)paramView.findViewById(2131296545)).setText(localCreditFacilityAccountLoanDetail.d());
      return paramView;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.CreditFacilityAccountDetailActivity
 * JD-Core Version:    0.6.2
 */