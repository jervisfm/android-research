package com.chase.sig.android.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.AsyncTask.Status;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.d;
import com.chase.sig.android.domain.OneTimePasswordContact;
import com.chase.sig.android.domain.a;
import com.chase.sig.android.domain.o;
import com.chase.sig.android.service.GenericResponse;
import com.chase.sig.android.util.s;
import com.chase.sig.android.view.ag;
import com.chase.sig.android.view.ag.a;
import java.util.List;

public class DeviceCodeLoginActivity extends eb
{
  ag.a a = new cn(this);
  private List<OneTimePasswordContact> b;
  private EditText c;

  private boolean d()
  {
    a locala = ((ChaseApplication)getApplication()).b().a;
    if (locala != null)
    {
      if ((locala.e != null) && (locala.e.length > 0) && (s.m(locala.e[0])));
      for (int i = 1; i != 0; i = 0)
        return true;
    }
    return false;
  }

  protected final void a(int paramInt)
  {
    super.a(paramInt);
    if (paramInt == 0)
    {
      Intent localIntent = new Intent(this, LoginActivity.class);
      localIntent.setFlags(67108864);
      startActivity(localIntent);
    }
  }

  public final void a(Bundle paramBundle)
  {
    b(2130903086);
    a(2131296427, new a((byte)0));
    a(2131296428, i(1));
    Bundle localBundle = getIntent().getExtras();
    if ((localBundle != null) && (localBundle.getString("extra_id_code") != null))
    {
      ((EditText)findViewById(2131296424)).setText(localBundle.getString("extra_id_code"));
      findViewById(2131296425).requestFocus();
    }
    if ((localBundle != null) && (localBundle.containsKey("otp_contacts")))
      this.b = ((List)localBundle.get("otp_contacts"));
    if (d())
    {
      this.c = ((EditText)findViewById(2131296426));
      this.c.setVisibility(0);
    }
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    Dialog localDialog = super.onCreateDialog(paramInt);
    switch (paramInt)
    {
    default:
      return localDialog;
    case 1:
    }
    return new ag(this, this.b, this.a);
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    return false;
  }

  private final class a
    implements View.OnClickListener
  {
    private a()
    {
    }

    public final void onClick(View paramView)
    {
      BehaviorAnalyticsAspect.a();
      BehaviorAnalyticsAspect.a(paramView);
      String str1 = ((EditText)DeviceCodeLoginActivity.this.findViewById(2131296424)).getText().toString();
      String str2 = ((EditText)DeviceCodeLoginActivity.this.findViewById(2131296425)).getText().toString();
      boolean bool = DeviceCodeLoginActivity.a(DeviceCodeLoginActivity.this);
      String str3 = null;
      if (bool)
        str3 = DeviceCodeLoginActivity.b(DeviceCodeLoginActivity.this).getText().toString();
      if ((s.m(str1)) && (s.m(str2)) && ((!DeviceCodeLoginActivity.a(DeviceCodeLoginActivity.this)) || (s.m(str3))));
      for (int i = 1; i != 0; i = 0)
      {
        DeviceCodeLoginActivity.b localb = (DeviceCodeLoginActivity.b)DeviceCodeLoginActivity.this.e.a(DeviceCodeLoginActivity.b.class);
        if (localb.getStatus() != AsyncTask.Status.RUNNING)
          localb.execute(new Void[0]);
        return;
      }
      DeviceCodeLoginActivity localDeviceCodeLoginActivity = DeviceCodeLoginActivity.this;
      if (DeviceCodeLoginActivity.a(DeviceCodeLoginActivity.this));
      for (int j = 2131165377; ; j = 2131165376)
      {
        localDeviceCodeLoginActivity.g(j);
        return;
      }
    }
  }

  public static class b extends d<DeviceCodeLoginActivity, Void, Void, GenericResponse>
  {
    private String a;
    private String e;
    private String f;

    protected void onPreExecute()
    {
      super.onPreExecute();
      this.e = ((EditText)((DeviceCodeLoginActivity)this.b).findViewById(2131296424)).getText().toString();
      this.a = ((EditText)((DeviceCodeLoginActivity)this.b).findViewById(2131296425)).getText().toString();
      if (DeviceCodeLoginActivity.a((DeviceCodeLoginActivity)this.b))
        this.f = DeviceCodeLoginActivity.b((DeviceCodeLoginActivity)this.b).getText().toString();
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.DeviceCodeLoginActivity
 * JD-Core Version:    0.6.2
 */