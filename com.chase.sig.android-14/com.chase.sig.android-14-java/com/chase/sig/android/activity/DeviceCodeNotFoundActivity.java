package com.chase.sig.android.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import com.chase.sig.android.domain.OneTimePasswordContact;
import com.chase.sig.android.view.ag;
import com.chase.sig.android.view.ag.a;
import java.util.List;

public class DeviceCodeNotFoundActivity extends eb
{
  List<OneTimePasswordContact> a;
  ag.a b = new cp(this);

  public final void a(Bundle paramBundle)
  {
    b(2130903087);
    Bundle localBundle = getIntent().getExtras();
    if ((localBundle != null) && (localBundle.containsKey("otp_contacts")))
      this.a = ((List)localBundle.get("otp_contacts"));
    a(2131296430, i(1));
    a(2131296431, new co(this));
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    Object localObject = super.onCreateDialog(paramInt);
    if (paramInt == 1)
      localObject = new ag(this, this.a, this.b);
    return localObject;
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    return false;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.DeviceCodeNotFoundActivity
 * JD-Core Version:    0.6.2
 */