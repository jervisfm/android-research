package com.chase.sig.android.activity;

import android.content.Intent;
import android.os.AsyncTask.Status;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import com.chase.sig.android.d;
import com.chase.sig.android.domain.Disclosure;
import com.chase.sig.android.service.disclosures.DisclosuresResponse;
import com.chase.sig.android.util.e;
import com.chase.sig.android.util.r;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class DisclosuresActivity extends ai
{
  private ListView a;
  private List<Disclosure> b;
  private TextView c;
  private String d = "Loading";
  private String k;

  private void d()
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator;
    if ((this.b != null) && (this.b.size() > 0))
    {
      this.a.setVisibility(0);
      localIterator = this.b.iterator();
    }
    while (localIterator.hasNext())
    {
      Disclosure localDisclosure = (Disclosure)localIterator.next();
      HashMap localHashMap = new HashMap();
      localHashMap.put("title", localDisclosure.c());
      localArrayList.add(localHashMap);
      continue;
      this.c.setVisibility(0);
    }
    SimpleAdapter localSimpleAdapter = new SimpleAdapter(this, localArrayList, 2130903093, new String[] { "title" }, new int[] { 2131296444 });
    this.a.setAdapter(localSimpleAdapter);
  }

  public final void a(Bundle paramBundle)
  {
    b(2130903091);
    this.a = ((ListView)findViewById(2131296440));
    this.c = ((TextView)findViewById(2131296441));
    this.d = ((String)e.a(paramBundle, "footNote", null));
    this.k = ((String)e.a(getIntent().getExtras(), "title", null));
    int i;
    if (this.k != null)
    {
      setTitle(this.k);
      a locala = (a)this.e.a(a.class);
      if (locala.getStatus() == AsyncTask.Status.RUNNING)
        break label148;
      i = 1;
      label106: if ((paramBundle != null) || (i == 0))
        break label153;
      locala.execute(new Void[0]);
    }
    while (true)
    {
      this.a.setOnItemClickListener(new cq(this));
      return;
      setTitle(2131165833);
      break;
      label148: i = 0;
      break label106;
      label153: if (i != 0)
      {
        this.b = ((List)paramBundle.getSerializable("disclosures"));
        d();
      }
    }
  }

  public final void a(List<Disclosure> paramList)
  {
    this.b = paramList;
    int i;
    if (getIntent().hasExtra("dislosure id"))
    {
      String str = getIntent().getExtras().getString("dislosure id");
      new r();
      i = r.a(paramList, new cr(this, str));
      if (i == -1);
    }
    for (Disclosure localDisclosure = (Disclosure)paramList.get(i); localDisclosure == null; localDisclosure = null)
    {
      d();
      return;
    }
    Intent localIntent = new Intent(this, DisclosuresDetailActivity.class);
    localIntent.setFlags(1073741824);
    e.a(localIntent, localDisclosure);
    startActivity(localIntent);
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    paramBundle.putSerializable("disclosures", (Serializable)this.b);
    paramBundle.putString("footNote", this.d);
  }

  public static class a extends d<DisclosuresActivity, Void, Void, DisclosuresResponse>
  {
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.DisclosuresActivity
 * JD-Core Version:    0.6.2
 */