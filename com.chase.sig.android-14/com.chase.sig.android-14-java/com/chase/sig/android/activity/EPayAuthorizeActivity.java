package com.chase.sig.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;
import com.chase.sig.android.d;
import com.chase.sig.android.domain.EPayment;
import com.chase.sig.android.service.GenericResponse;

public class EPayAuthorizeActivity extends ai
  implements fk.e
{
  private EPayment a;

  protected final void a(int paramInt)
  {
    super.a(paramInt);
    if (paramInt == 0)
    {
      Intent localIntent = new Intent(this, EPayStartActivity.class);
      localIntent.setFlags(67108864);
      startActivity(localIntent);
    }
  }

  public final void a(Bundle paramBundle)
  {
    b(2130903100);
    setTitle(2131165612);
    this.a = ((EPayment)getIntent().getExtras().get(EPayVerifyActivity.a));
    TextView localTextView = (TextView)findViewById(2131296460);
    if (this.a != null)
      localTextView.setText(this.a.h());
    a(2131296457, new cs(this));
    a(2131296458, B());
  }

  public static class a extends d<EPayAuthorizeActivity, EPayment, Void, GenericResponse>
  {
    EPayment a;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.EPayAuthorizeActivity
 * JD-Core Version:    0.6.2
 */