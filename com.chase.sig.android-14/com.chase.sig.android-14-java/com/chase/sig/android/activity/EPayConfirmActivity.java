package com.chase.sig.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.Button;
import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.c;
import com.chase.sig.android.domain.EPayment;
import com.chase.sig.android.domain.IAccount;
import com.chase.sig.android.domain.g;
import com.chase.sig.android.domain.o;
import com.chase.sig.android.util.Dollar;
import com.chase.sig.android.view.detail.DetailRow;
import com.chase.sig.android.view.detail.DetailView;

public class EPayConfirmActivity extends ai
  implements fk.e
{
  EPayment a;

  public final void a(Bundle paramBundle)
  {
    b(2130903134);
    setTitle(2131165300);
    this.a = ((EPayment)getIntent().getExtras().get(EPayVerifyActivity.a));
    c.a();
    DetailView localDetailView;
    com.chase.sig.android.view.detail.a[] arrayOfa;
    IAccount localIAccount1;
    String str1;
    String str2;
    String str3;
    label245: String str4;
    if (this.a != null)
    {
      localDetailView = (DetailView)findViewById(2131296595);
      arrayOfa = new com.chase.sig.android.view.detail.a[5];
      arrayOfa[0] = new DetailRow("Confirmation", this.a.a());
      localIAccount1 = ((ChaseApplication)getApplication()).b().b.a(this.a.f());
      if (!localIAccount1.g())
        break label383;
      IAccount localIAccount2 = ((ChaseApplication)getApplication()).b().b.e(localIAccount1.b());
      Object[] arrayOfObject = new Object[2];
      arrayOfObject[0] = localIAccount1.a();
      arrayOfObject[1] = localIAccount2.c();
      str1 = String.format("%s (%s)", arrayOfObject);
      arrayOfa[1] = new DetailRow("To", str1);
      arrayOfa[2] = new DetailRow("From", this.a.i().toString());
      str2 = this.a.g();
      if (!str2.equals("3"))
        break label398;
      str3 = getString(2131165422);
      if (this.a.d() == null)
        break label468;
      str4 = this.a.d().h();
      label267: arrayOfa[3] = new DetailRow(str3, str4);
      if (this.a.e() != null)
        break label475;
    }
    label398: label468: label475: for (String str5 = "N/A"; ; str5 = this.a.e())
    {
      arrayOfa[4] = new DetailRow("Payment Date", str5);
      localDetailView.setRows(arrayOfa);
      Button localButton1 = (Button)findViewById(2131296596);
      localButton1.setVisibility(0);
      localButton1.setText(2131165621);
      localButton1.setOnClickListener(a(EPayHistoryActivity.class));
      Button localButton2 = (Button)findViewById(2131296597);
      localButton2.setVisibility(0);
      localButton2.setText(2131165622);
      localButton2.setOnClickListener(new ct(this));
      return;
      label383: str1 = localIAccount1.w().toString();
      break;
      if (str2.equals("2"))
      {
        str3 = getString(2131165625);
        break label245;
      }
      if (str2.equals("-1"))
      {
        str3 = getString(2131165624);
        break label245;
      }
      if (str2.equals("1"))
      {
        str3 = getString(2131165623);
        break label245;
      }
      str3 = "Amount";
      break label245;
      str4 = "";
      break label267;
    }
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if (j(paramInt))
      return true;
    com.chase.sig.android.a.a.a(paramInt, this, EPayHomeActivity.class);
    return super.onKeyDown(paramInt, paramKeyEvent);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.EPayConfirmActivity
 * JD-Core Version:    0.6.2
 */