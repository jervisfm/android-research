package com.chase.sig.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import com.chase.sig.android.a.a;
import com.chase.sig.android.service.epay.EPayTransaction;
import com.chase.sig.android.util.Dollar;
import com.chase.sig.android.util.s;
import com.chase.sig.android.view.detail.DetailRow;
import com.chase.sig.android.view.detail.DetailView;

public class EPayConfirmationActivity extends ai
  implements fk.e
{
  private DetailView a = null;
  private EPayTransaction b = null;

  public final void a(Bundle paramBundle)
  {
    b(2130903097);
    setTitle(2131165300);
    this.a = ((DetailView)findViewById(2131296450));
    int i;
    int j;
    if (paramBundle != null)
    {
      if (paramBundle == null)
        break label387;
      i = 1;
      if (paramBundle.containsKey("bundleCancelConfirm"))
        break label393;
      j = 1;
      label48: if ((i & j) == 0)
        break label399;
    }
    label387: label393: label399: for (this.b = ((EPayTransaction)getIntent().getExtras().getSerializable("ePayCancelConfirm")); ; this.b = ((EPayTransaction)paramBundle.get("bundleCancelConfirm")))
    {
      DetailRow[] arrayOfDetailRow = new DetailRow[6];
      DetailRow localDetailRow1 = new DetailRow(getString(2131165300), this.b.n());
      localDetailRow1.e = 2131099664;
      arrayOfDetailRow[0] = ((DetailRow)((DetailRow)localDetailRow1).b(2131099649));
      DetailRow localDetailRow2 = new DetailRow(getString(2131165301), this.b.d());
      localDetailRow2.e = 2131099664;
      arrayOfDetailRow[1] = ((DetailRow)((DetailRow)localDetailRow2).b(2131099649));
      DetailRow localDetailRow3 = new DetailRow(getString(2131165302), this.b.s());
      localDetailRow3.e = 2131099664;
      arrayOfDetailRow[2] = ((DetailRow)((DetailRow)localDetailRow3).b(2131099649));
      DetailRow localDetailRow4 = new DetailRow(getString(2131165303), this.b.e().h());
      localDetailRow4.e = 2131099664;
      arrayOfDetailRow[3] = ((DetailRow)((DetailRow)localDetailRow4).b(2131099649));
      DetailRow localDetailRow5 = new DetailRow(getString(2131165304), s.i(this.b.q()));
      localDetailRow5.e = 2131099664;
      arrayOfDetailRow[4] = ((DetailRow)((DetailRow)localDetailRow5).b(2131099649));
      DetailRow localDetailRow6 = new DetailRow(getString(2131165305), this.b.t());
      localDetailRow6.e = 2131099664;
      arrayOfDetailRow[5] = ((DetailRow)((DetailRow)localDetailRow6).b(2131099649));
      this.a.setRows(arrayOfDetailRow);
      a(2131296451, a(EPayHistoryActivity.class));
      a(2131296452, a(EPaySelectToAccount.class));
      return;
      i = 0;
      break;
      j = 0;
      break label48;
    }
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if (j(paramInt))
      return true;
    a.a(paramInt, this, EPayHomeActivity.class);
    return super.onKeyDown(paramInt, paramKeyEvent);
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    paramBundle.putSerializable("bundleCancelConfirm", this.b);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.EPayConfirmationActivity
 * JD-Core Version:    0.6.2
 */