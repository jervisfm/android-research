package com.chase.sig.android.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.SimpleAdapter;
import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.domain.IAccount;
import com.chase.sig.android.domain.g;
import com.chase.sig.android.domain.o;
import com.chase.sig.android.service.epay.EPayTransaction;
import com.chase.sig.android.service.movemoney.ListServiceResponse;
import com.chase.sig.android.service.movemoney.d;
import com.chase.sig.android.service.n;
import com.chase.sig.android.util.e;
import com.chase.sig.android.view.JPSpinner;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class EPayHistoryActivity extends l<EPayTransaction>
{
  private JPSpinner k;
  private List<IAccount> l;
  private g m;
  private int n;

  protected final int a()
  {
    return 2131165618;
  }

  public final void a(Bundle paramBundle)
  {
    super.a(paramBundle);
    findViewById(2131296979).setVisibility(0);
    this.m = ((ChaseApplication)getApplication()).b().b;
    this.k = ((JPSpinner)findViewById(2131296980));
    this.l = this.m.u();
    String[] arrayOfString = { "account_name" };
    int[] arrayOfInt = { 2131296524 };
    ArrayList localArrayList = new ArrayList();
    if ((this.l != null) && (this.l.size() > 0))
    {
      Iterator localIterator = this.l.iterator();
      if (localIterator.hasNext())
      {
        IAccount localIAccount1 = (IAccount)localIterator.next();
        HashMap localHashMap = new HashMap();
        if (!localIAccount1.g())
          localHashMap.put("account_name", localIAccount1.a() + localIAccount1.y());
        while (true)
        {
          localArrayList.add(localHashMap);
          break;
          IAccount localIAccount2 = this.m.e(localIAccount1.b());
          localHashMap.put("account_name", localIAccount1.a() + localIAccount2.y());
        }
      }
    }
    SimpleAdapter localSimpleAdapter = new SimpleAdapter(getBaseContext(), localArrayList, 2130903121, arrayOfString, arrayOfInt);
    this.k.setAdapter(localSimpleAdapter);
    this.k.a(new cu(this));
    if ((e.a(paramBundle, "payment_items")) && (e.a(paramBundle, "spinner_selection")))
    {
      c(paramBundle);
      this.n = paramBundle.getInt("spinner_selection");
    }
    do
    {
      return;
      this.n = -1;
    }
    while ((this.l == null) || (this.l.size() <= 0));
    this.k.setSelection(0);
  }

  protected final Class<? extends l.a<EPayTransaction>> b()
  {
    return a.class;
  }

  protected final Class<? extends eb> c()
  {
    return EPayHistoryDetailActivity.class;
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    paramBundle.putInt("spinner_selection", this.k.getSelectedItemPosition());
  }

  public static class a extends l.a<EPayTransaction>
  {
    protected final ListServiceResponse<EPayTransaction> a(Integer[] paramArrayOfInteger)
    {
      Integer localInteger = paramArrayOfInteger[0];
      return a().c(localInteger.intValue());
    }

    protected final d<EPayTransaction> a()
    {
      ((l)this.b).A();
      return n.h();
    }

    protected final void a(ListServiceResponse<EPayTransaction> paramListServiceResponse)
    {
      ((l)this.b).a.clear();
      super.a(paramListServiceResponse);
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.EPayHistoryActivity
 * JD-Core Version:    0.6.2
 */