package com.chase.sig.android.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import com.chase.sig.android.activity.a.b;
import com.chase.sig.android.d;
import com.chase.sig.android.service.epay.EPayTransaction;
import com.chase.sig.android.service.movemoney.ServiceResponse;
import com.chase.sig.android.util.Dollar;
import com.chase.sig.android.util.s;
import com.chase.sig.android.view.detail.DetailColoredValueRow;
import com.chase.sig.android.view.detail.DetailRow;
import com.chase.sig.android.view.detail.DetailView;
import com.chase.sig.android.view.detail.a;
import com.chase.sig.android.view.k.a;

public class EPayHistoryDetailActivity extends n<EPayTransaction>
{
  private EPayTransaction d = null;

  public final void a(Bundle paramBundle)
  {
    b(2130903238);
    setTitle(2131165619);
    this.d = ((EPayTransaction)getIntent().getExtras().getSerializable("transaction_object"));
    DetailView localDetailView = (DetailView)findViewById(2131296595);
    a[] arrayOfa = new a[7];
    DetailRow localDetailRow = new DetailRow(this.d.d(), "");
    localDetailRow.e = 2131099649;
    arrayOfa[0] = ((DetailRow)localDetailRow).b(2131099664);
    arrayOfa[1] = new DetailColoredValueRow("Payment Date", s.i(this.d.q()));
    arrayOfa[2] = new DetailColoredValueRow("Confirmation", this.d.n());
    arrayOfa[3] = new DetailColoredValueRow("Description", s.s(this.d.a()));
    arrayOfa[4] = new DetailColoredValueRow("Debit/Credit Amount", this.d.e().h());
    arrayOfa[5] = new DetailColoredValueRow("Checking acct.", this.d.s());
    arrayOfa[6] = new DetailColoredValueRow("Status", this.d.t());
    localDetailView.setRows(arrayOfa);
    ((Button)findViewById(2131296895)).setVisibility(8);
    Button localButton = (Button)findViewById(2131296894);
    if (this.d.u());
    for (int i = 0; ; i = 8)
    {
      localButton.setVisibility(i);
      localButton.setText(getString(2131165620));
      localButton.setOnClickListener(new cv(this));
      return;
    }
  }

  protected final void a(boolean paramBoolean)
  {
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    k.a locala = new k.a(this);
    switch (paramInt)
    {
    default:
      return super.onCreateDialog(paramInt);
    case 0:
    }
    locala.i = true;
    locala.a(2131165620, new cw(this)).b(2131165293, new b()).b(2131165290);
    return locala.d(-1);
  }

  public static class a extends d<EPayHistoryDetailActivity, Void, Void, ServiceResponse>
  {
    private EPayTransaction a;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.EPayHistoryDetailActivity
 * JD-Core Version:    0.6.2
 */