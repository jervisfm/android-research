package com.chase.sig.android.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.SimpleAdapter;
import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.domain.EPayAccount;
import com.chase.sig.android.domain.EPayment;
import com.chase.sig.android.domain.EPaymentOption;
import com.chase.sig.android.domain.IAccount;
import com.chase.sig.android.domain.g;
import com.chase.sig.android.domain.o;
import com.chase.sig.android.service.GenericResponse;
import com.chase.sig.android.service.SplashResponse;
import com.chase.sig.android.service.epay.EPayAccountsAndPaymentOptionsResponse;
import com.chase.sig.android.util.Dollar;
import com.chase.sig.android.util.e;
import com.chase.sig.android.util.s;
import com.chase.sig.android.view.AmountView;
import com.chase.sig.android.view.JPSpinner;
import com.chase.sig.android.view.b;
import com.chase.sig.android.view.b.a;
import com.chase.sig.android.view.detail.DetailRow;
import com.chase.sig.android.view.detail.DetailView;
import com.chase.sig.android.view.detail.q;
import com.chase.sig.android.view.x;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class EPayStartActivity extends a
{
  private IAccount a;
  private List<EPayAccount> b = new ArrayList();
  private List<EPaymentOption> c = new ArrayList();
  private List<Map<String, String>> d = new ArrayList();
  private String k = "";
  private SimpleAdapter l;
  private String m;
  private g n;
  private x o = new db(this);
  private View.OnClickListener p = new dc(this);
  private b.a q = new dd(this);

  private JPSpinner D()
  {
    return ((q)a("PAYMENT_OPTIONS")).q();
  }

  private void E()
  {
    String[] arrayOfString = { "account_name" };
    int[] arrayOfInt = { 2131296524 };
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = this.b.iterator();
    while (localIterator.hasNext())
    {
      EPayAccount localEPayAccount = (EPayAccount)localIterator.next();
      HashMap localHashMap = new HashMap();
      localHashMap.put("account_name", localEPayAccount.a());
      localArrayList.add(localHashMap);
    }
    SimpleAdapter localSimpleAdapter = new SimpleAdapter(getBaseContext(), localArrayList, 2130903121, arrayOfString, arrayOfInt);
    j().setAdapter(localSimpleAdapter);
  }

  private void F()
  {
    this.d.clear();
    String[] arrayOfString = { "label", "value", "note" };
    int[] arrayOfInt = { 2131296453, 2131296454, 2131296455 };
    Iterator localIterator = this.c.iterator();
    if (localIterator.hasNext())
    {
      EPaymentOption localEPaymentOption = (EPaymentOption)localIterator.next();
      String str1 = localEPaymentOption.b();
      String str2 = "";
      HashMap localHashMap = new HashMap();
      String str3 = getString(2131165313);
      String str4;
      if (localEPaymentOption.d().equals("-1"))
      {
        str1 = getString(2131165624);
        str4 = "$(xxx.xx)";
        label131: if (!s.l(localEPaymentOption.b()))
          localEPaymentOption.b();
        if (!s.l(localEPaymentOption.c()))
          break label283;
      }
      label283: for (String str5 = ""; ; str5 = localEPaymentOption.c())
      {
        localHashMap.put("label", str1);
        localHashMap.put("value", str4);
        localHashMap.put("note", str5);
        localHashMap.put("id", localEPaymentOption.d());
        this.d.add(localHashMap);
        break;
        Dollar localDollar = localEPaymentOption.a();
        if ((localDollar != null) && (localDollar.b() != null))
          str2 = localDollar.f().trim();
        str4 = String.format("%s%s", new Object[] { str3, str2 });
        break label131;
      }
    }
    this.l = new SimpleAdapter(getBaseContext(), this.d, 2130903098, arrayOfString, arrayOfInt);
    D().setAdapter(this.l);
  }

  private boolean d()
  {
    return this.a.l();
  }

  private void e()
  {
    Intent localIntent = new Intent(this, EPaySelectToAccount.class);
    localIntent.setFlags(1073741824);
    startActivity(localIntent);
    finish();
  }

  private boolean f()
  {
    return ((ChaseApplication)getApplication()).b().b.u().size() == 1;
  }

  private String g()
  {
    if (!h(this.k))
      return "N/A";
    return s.i(this.k);
  }

  private String h()
  {
    return ((com.chase.sig.android.view.detail.d)a("DATE")).p();
  }

  private static boolean h(String paramString)
  {
    return (s.m(paramString)) && (!"N/A".equals(paramString));
  }

  private String i()
  {
    return ((com.chase.sig.android.view.detail.d)a("DATE")).p();
  }

  private void i(String paramString)
  {
    ((com.chase.sig.android.view.detail.d)a("DATE")).b(paramString);
  }

  private JPSpinner j()
  {
    return ((q)a("PAY_FROM")).q();
  }

  protected final void a(int paramInt)
  {
    int i = 1;
    super.a(paramInt);
    int j;
    if ((paramInt == 0) && (!f()))
    {
      j = i;
      if ((paramInt != 0) || (!f()))
        break label45;
      label31: if (j == 0)
        break label50;
      e();
    }
    label45: label50: 
    while (i == 0)
    {
      return;
      j = 0;
      break;
      i = 0;
      break label31;
    }
    finish();
  }

  public final void a(Bundle paramBundle)
  {
    super.a(paramBundle);
    setTitle(2131165611);
    if ((r().d != null) && (r().d.j()))
    {
      e(r().d.o());
      return;
    }
    String str1 = getIntent().getExtras().getString("selectedAccountId");
    this.n = ((ChaseApplication)getApplication()).b().b;
    this.a = this.n.a(str1);
    DetailView localDetailView = (DetailView)findViewById(2131296969);
    com.chase.sig.android.view.detail.a[] arrayOfa = new com.chase.sig.android.view.detail.a[8];
    com.chase.sig.android.view.detail.d locald1 = new com.chase.sig.android.view.detail.d("Pay To", ((ChaseApplication)getApplication()).b().b.a(this.a));
    locald1.b = "PAY_TO";
    com.chase.sig.android.view.detail.d locald2 = (com.chase.sig.android.view.detail.d)locald1;
    locald2.a = new da(this);
    arrayOfa[0] = locald2;
    String str2 = (String)this.a.e().get("nextPaymentDate");
    String str3;
    String str4;
    label258: String str5;
    String str6;
    label291: boolean bool1;
    label314: String str9;
    label348: boolean bool2;
    if (s.l(str2))
    {
      str3 = "N/A";
      arrayOfa[1] = new DetailRow("Due Date", str3);
      if ((this.a == null) || (!this.a.E()))
        break label695;
      str4 = getString(2131165415);
      str5 = (String)this.a.e().get("nextPaymentAmount");
      if (!s.l(str5))
        break label703;
      str6 = "N/A";
      DetailRow localDetailRow1 = new DetailRow(str4, str6);
      if (d())
        break label720;
      bool1 = true;
      localDetailRow1.l = bool1;
      arrayOfa[2] = localDetailRow1;
      String str7 = getString(2131165421);
      if (d())
        break label726;
      str9 = "N/A";
      DetailRow localDetailRow2 = new DetailRow(str7, str9);
      if (d())
        break label776;
      bool2 = true;
      label371: localDetailRow2.j = bool2;
      arrayOfa[3] = ((DetailRow)localDetailRow2).c();
      q localq1 = new q("Pay From");
      localq1.b = "PAY_FROM";
      arrayOfa[4] = ((q)localq1).a("Select Pay From Account");
      com.chase.sig.android.view.detail.d locald3 = new com.chase.sig.android.view.detail.d("Payment Date", g());
      locald3.b = "DATE";
      com.chase.sig.android.view.detail.d locald4 = ((com.chase.sig.android.view.detail.d)locald3).q();
      locald4.a = i(1);
      arrayOfa[5] = locald4;
      com.chase.sig.android.view.detail.c localc1 = (com.chase.sig.android.view.detail.c)new com.chase.sig.android.view.detail.c("Other Amount $").a("Enter Other Amount");
      localc1.b = "AMOUNT";
      com.chase.sig.android.view.detail.c localc2 = (com.chase.sig.android.view.detail.c)localc1;
      localc2.j = true;
      arrayOfa[6] = localc2;
      q localq2 = new q("Amount $");
      localq2.b = "PAYMENT_OPTIONS";
      q localq3 = ((q)((q)localq2).a(getString(2131165610))).b(this.o);
      localq3.j = false;
      arrayOfa[7] = localq3;
      localDetailView.setRows(arrayOfa);
      if (!e.a(paramBundle, "ePayFromAccounts"))
        break label782;
      this.b = ((List)paramBundle.getSerializable("ePayFromAccounts"));
      this.m = paramBundle.getString("options_loaded_for_account_id");
      this.k = paramBundle.getString("earliest_payment_date");
      this.c = ((List)paramBundle.getSerializable("ePayAmountOptions"));
      E();
      F();
    }
    while (true)
    {
      a(2131296971, this.p);
      ((Button)findViewById(2131296970)).setText(2131165846);
      return;
      str3 = s.i(str2);
      break;
      label695: str4 = "Minimum Due";
      break label258;
      label703: str6 = new Dollar(str5).h();
      break label291;
      label720: bool1 = false;
      break label314;
      label726: String str8 = (String)this.a.e().get("goalPayAmountDue");
      if (str8 == null)
      {
        str9 = "N/A";
        break label348;
      }
      str9 = new Dollar(str8).h();
      break label348;
      label776: bool2 = false;
      break label371;
      label782: String[] arrayOfString = new String[1];
      arrayOfString[0] = this.a.b();
      a(a.class, arrayOfString);
    }
  }

  public final boolean b()
  {
    boolean bool1 = true;
    y();
    if (D().getSelectedItemPosition() == -1)
      a().a("PAYMENT_OPTIONS");
    for (boolean bool2 = false; ; bool2 = bool1)
    {
      if (j().getSelectedItemPosition() == -1)
        a().a("PAY_FROM");
      for (boolean bool3 = false; ; bool3 = bool2)
      {
        int i = D().getSelectedItemPosition();
        int j;
        if (i == -1)
        {
          j = 0;
          if (j == 0)
          {
            a().a("AMOUNT");
            bool3 = false;
          }
          if (h(this.k))
            break label210;
          bool1 = false;
        }
        while (true)
        {
          if (bool1)
            break label259;
          a().a("DATE");
          return false;
          EPaymentOption localEPaymentOption = (EPaymentOption)this.c.get(i);
          if (!"-1".equals(localEPaymentOption.d()))
          {
            if ((localEPaymentOption.a() == null) || (localEPaymentOption.a().b().longValue() <= 0L))
            {
              j = 0;
              break;
            }
            j = bool1;
            break;
          }
          if (!((com.chase.sig.android.view.detail.c)a("AMOUNT")).p().a())
          {
            j = 0;
            break;
          }
          j = bool1;
          break;
          label210: Date localDate = s.t(s.i(this.k));
          String str = i();
          if (s.l(str))
            bool1 = false;
          else if (s.t(str).before(localDate))
            bool1 = false;
        }
        label259: return bool3;
      }
    }
  }

  public final void k()
  {
    y();
    if (f())
    {
      ((q)a("PAYMENT_OPTIONS")).n = false;
      D().a();
      ((com.chase.sig.android.view.detail.c)a("AMOUNT")).j = true;
      ((com.chase.sig.android.view.detail.c)a("AMOUNT")).b("");
      j().a();
      i(g());
    }
    while (true)
    {
      a().c();
      return;
      e();
    }
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    Dialog localDialog = super.onCreateDialog(paramInt);
    switch (paramInt)
    {
    default:
      return localDialog;
    case 1:
    }
    Calendar localCalendar1 = Calendar.getInstance();
    localCalendar1.add(5, 93);
    boolean bool = h(h());
    Calendar localCalendar2 = null;
    if (bool)
    {
      localCalendar2 = Calendar.getInstance();
      localCalendar2.setTime(s.t(h()));
    }
    Calendar localCalendar3 = Calendar.getInstance();
    localCalendar3.setTime(s.t(g()));
    return new b(this, this.q, false, false, localCalendar3, localCalendar1, localCalendar2);
  }

  protected void onResume()
  {
    super.onResume();
    com.chase.sig.android.c.a();
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    paramBundle.putSerializable("ePayFromAccounts", (Serializable)this.b);
    paramBundle.putString("pay_to_account_id", this.a.b());
    paramBundle.putInt("ePayToAccountSelected", j().getSelectedItemPosition());
    paramBundle.putString("selected_payment_date", i());
    paramBundle.putInt("ePayAmountSelected", D().getSelectedItemPosition());
    paramBundle.putSerializable("ePayAmountOptions", (Serializable)this.c);
    paramBundle.putString("options_loaded_for_account_id", this.m);
    paramBundle.putString("earliest_payment_date", this.k);
    paramBundle.putString("due_date", h());
    if ((D() != null) && (D().getSelectedItemPosition() != -1) && (((EPaymentOption)this.c.get(D().getSelectedItemPosition())).d().equals("-1")))
    {
      com.chase.sig.android.view.detail.c localc = (com.chase.sig.android.view.detail.c)a("AMOUNT");
      paramBundle.putString("ePayOtherAmount", localc.g());
      paramBundle.putInt("visibility", localc.p().getVisibility());
    }
  }

  public static class a extends com.chase.sig.android.d<EPayStartActivity, String, Void, EPayAccountsAndPaymentOptionsResponse>
  {
    String a;
  }

  public static class b extends com.chase.sig.android.d<EPayStartActivity, EPayment, Void, GenericResponse>
  {
    EPayment a;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.EPayStartActivity
 * JD-Core Version:    0.6.2
 */