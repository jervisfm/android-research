package com.chase.sig.android.activity;

import android.content.Intent;
import android.os.Bundle;
import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.domain.EPayment;
import com.chase.sig.android.domain.IAccount;
import com.chase.sig.android.domain.g;
import com.chase.sig.android.domain.o;
import com.chase.sig.android.util.Dollar;
import com.chase.sig.android.view.detail.DetailRow;
import com.chase.sig.android.view.detail.DetailView;
import com.chase.sig.android.view.detail.a;

public class EPayVerifyActivity extends ai
  implements fk.e
{
  static String a = "transaction_object";
  private EPayment b;

  public final void a(Bundle paramBundle)
  {
    b(2130903101);
    setTitle(2131165612);
    this.b = ((EPayment)getIntent().getExtras().get(a));
    DetailView localDetailView;
    a[] arrayOfa;
    IAccount localIAccount1;
    String str1;
    String str2;
    String str3;
    label218: String str4;
    if (this.b != null)
    {
      localDetailView = (DetailView)findViewById(2131296461);
      arrayOfa = new a[4];
      localIAccount1 = ((ChaseApplication)getApplication()).b().b.a(this.b.f());
      if (!localIAccount1.g())
        break label312;
      IAccount localIAccount2 = ((ChaseApplication)getApplication()).b().b.e(localIAccount1.b());
      Object[] arrayOfObject = new Object[2];
      arrayOfObject[0] = localIAccount1.a();
      arrayOfObject[1] = localIAccount2.c();
      str1 = String.format("%s (%s)", arrayOfObject);
      arrayOfa[0] = new DetailRow("To", str1);
      arrayOfa[1] = new DetailRow("From", this.b.i().toString());
      str2 = this.b.g();
      if (!str2.equals("3"))
        break label327;
      str3 = getString(2131165422);
      if (this.b.d() == null)
        break label397;
      str4 = this.b.d().h();
      label240: arrayOfa[2] = new DetailRow(str3, str4);
      if (this.b.e() != null)
        break label404;
    }
    label397: label404: for (String str5 = "N/A"; ; str5 = this.b.e())
    {
      arrayOfa[3] = new DetailRow("Payment Date", str5);
      localDetailView.setRows(arrayOfa);
      a(2131296463, new de(this));
      a(2131296462, B());
      return;
      label312: str1 = localIAccount1.w().toString();
      break;
      label327: if (str2.equals("2"))
      {
        str3 = getString(2131165625);
        break label218;
      }
      if (str2.equals("-1"))
      {
        str3 = getString(2131165624);
        break label218;
      }
      if (str2.equals("1"))
      {
        str3 = getString(2131165623);
        break label218;
      }
      str3 = "Amount";
      break label218;
      str4 = "";
      break label240;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.EPayVerifyActivity
 * JD-Core Version:    0.6.2
 */