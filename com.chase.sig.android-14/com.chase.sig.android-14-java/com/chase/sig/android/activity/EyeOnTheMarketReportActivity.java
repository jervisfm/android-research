package com.chase.sig.android.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.b;
import com.chase.sig.android.service.EyeOnTheMarketReport;
import com.chase.sig.android.service.EyeOnTheMarketReportResponse;
import com.chase.sig.android.util.e;
import com.chase.sig.android.util.s;
import java.util.List;

public class EyeOnTheMarketReportActivity extends ai
  implements fk.c
{
  private EyeOnTheMarketReportResponse a;

  public final void a(Bundle paramBundle)
  {
    b(2130903102);
    setTitle(2131165891);
    a((ListView)findViewById(2131296466));
    if (e.a(paramBundle, "report_response"))
    {
      this.a = ((EyeOnTheMarketReportResponse)paramBundle.getSerializable("report_response"));
      a(this.a);
      return;
    }
    a(b.class, new String[0]);
  }

  public final void a(EyeOnTheMarketReportResponse paramEyeOnTheMarketReportResponse)
  {
    ListView localListView = (ListView)findViewById(2131296466);
    localListView.setAdapter(new a(this, paramEyeOnTheMarketReportResponse.a()));
    localListView.setItemsCanFocus(true);
  }

  protected final void c_()
  {
    super.c_();
    finish();
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    if (this.a != null)
      paramBundle.putSerializable("report_response", this.a);
  }

  final class a extends ArrayAdapter<EyeOnTheMarketReport>
  {
    private final List<EyeOnTheMarketReport> b;
    private final LayoutInflater c;

    public a(int arg2)
    {
      super(2130903104, localList);
      this.b = localList;
      this.c = ((LayoutInflater)localContext.getSystemService("layout_inflater"));
    }

    public final View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      EyeOnTheMarketReport localEyeOnTheMarketReport = (EyeOnTheMarketReport)this.b.get(paramInt);
      if (paramView == null)
        paramView = this.c.inflate(2130903104, null);
      ((TextView)paramView.findViewById(2131296468)).setText(localEyeOnTheMarketReport.a());
      ((TextView)paramView.findViewById(2131296469)).setText(localEyeOnTheMarketReport.b());
      ((ListView)EyeOnTheMarketReportActivity.this.findViewById(2131296466)).setOnItemClickListener(new a((byte)0));
      StringBuilder localStringBuilder = new StringBuilder();
      String str1 = localEyeOnTheMarketReport.a();
      String str2 = localEyeOnTheMarketReport.b();
      if (s.m(str1))
      {
        localStringBuilder.append(str1);
        localStringBuilder.append(" ");
      }
      if (s.m(str2))
        localStringBuilder.append(str2);
      if (s.m(localStringBuilder.toString()))
        paramView.setContentDescription(localStringBuilder.toString());
      return paramView;
    }

    private final class a
      implements AdapterView.OnItemClickListener
    {
      private a()
      {
      }

      public final void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
      {
        try
        {
          EyeOnTheMarketReport localEyeOnTheMarketReport = (EyeOnTheMarketReport)paramAdapterView.getItemAtPosition(paramInt);
          Intent localIntent = new Intent(EyeOnTheMarketReportActivity.this, EyeOnTheMarketReportArticleActivity.class);
          localIntent.putExtra("transaction_object", localEyeOnTheMarketReport);
          EyeOnTheMarketReportActivity.this.startActivity(localIntent);
          BehaviorAnalyticsAspect.a();
          BehaviorAnalyticsAspect.a(paramAdapterView, paramView, paramInt);
          return;
        }
        catch (Throwable localThrowable)
        {
          BehaviorAnalyticsAspect.a();
          BehaviorAnalyticsAspect.a(paramAdapterView, paramView, paramInt);
          throw localThrowable;
        }
      }
    }
  }

  public static class b extends b<EyeOnTheMarketReportActivity, String, Void, EyeOnTheMarketReportResponse>
  {
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.EyeOnTheMarketReportActivity
 * JD-Core Version:    0.6.2
 */