package com.chase.sig.android.activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.net.Uri.Builder;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import com.chase.sig.android.service.EyeOnTheMarketReport;
import com.chase.sig.android.service.JPService;

public class EyeOnTheMarketReportArticleActivity extends eb
  implements fk.c
{
  private final String a = "https://docs.google.com";
  private WebView b;
  private EyeOnTheMarketReport c;

  public final void a(Bundle paramBundle)
  {
    setTitle(2131165891);
    b(2130903103);
    this.c = ((EyeOnTheMarketReport)getIntent().getExtras().get("transaction_object"));
    String str1 = JPService.b(this.c.c());
    Uri.Builder localBuilder = Uri.parse("https://docs.google.com").buildUpon().appendPath("viewer").appendQueryParameter("embedded", "true").appendQueryParameter("url", str1);
    this.b = ((WebView)findViewById(2131296467));
    this.b.setWebViewClient(new df(this));
    String str2 = localBuilder.build().toString();
    new StringBuilder("Loading EOTM Article URL: ").append(str2).toString();
    this.b.getSettings().setJavaScriptEnabled(true);
    this.b.loadUrl(str2);
  }

  protected final void c_()
  {
    super.c_();
    this.b.stopLoading();
    finish();
  }

  public void onConfigurationChanged(Configuration paramConfiguration)
  {
    super.onConfigurationChanged(paramConfiguration);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.EyeOnTheMarketReportArticleActivity
 * JD-Core Version:    0.6.2
 */