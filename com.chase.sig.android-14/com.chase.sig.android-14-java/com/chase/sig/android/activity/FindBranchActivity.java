package com.chase.sig.android.activity;

import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.chase.sig.android.b;
import com.chase.sig.android.domain.BranchLocation;
import com.chase.sig.android.service.BranchLocateResponse;
import com.chase.sig.android.service.IServiceError;
import com.chase.sig.android.util.e;
import com.chase.sig.android.util.s;
import com.chase.sig.android.view.u;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class FindBranchActivity extends eb
{
  TextView a;
  private List<BranchLocation> b;
  private ListView c;
  private AutoCompleteTextView d;
  private Location k;
  private List<String> l;
  private ArrayAdapter<String> m;
  private AdapterView.OnItemClickListener n = new di(this);

  private void a(List<BranchLocation> paramList)
  {
    if ((paramList == null) || (paramList.size() == 0))
      return;
    this.b = paramList;
    u localu = new u(this, paramList);
    this.c.setAdapter(localu);
    this.c.setVisibility(0);
  }

  private void d()
  {
    this.c.setVisibility(8);
    Animation localAnimation = AnimationUtils.loadAnimation(this, 2130968580);
    findViewById(2131296475).startAnimation(localAnimation);
    findViewById(2131296475).setVisibility(0);
    k(2131165450);
  }

  private void k(int paramInt)
  {
    TextView localTextView = (TextView)findViewById(2131296473);
    localTextView.setText(paramInt);
    localTextView.setVisibility(0);
  }

  public final void a(Bundle paramBundle)
  {
    b(2130903105);
    setTitle(2131165447);
    this.a = ((TextView)findViewById(2131296473));
    if (e.a(paramBundle, "message"))
      this.a.setText((String)e.a(paramBundle, "message", null));
    this.d = ((AutoCompleteTextView)findViewById(2131296471));
    this.d.setOnKeyListener(new dh(this));
    this.l = ((List)e.a(paramBundle, "prev_searches", new ArrayList()));
    this.m = new ArrayAdapter(this, 17367050, this.l);
    this.d.setAdapter(this.m);
    ((ImageView)findViewById(2131296472)).setOnClickListener(new dg(this));
    this.c = ((ListView)findViewById(2131296474));
    this.c.setOnItemClickListener(this.n);
    int i;
    b localb;
    if ((paramBundle != null) && (paramBundle.containsKey("atm_locations")))
    {
      i = 1;
      localb = (b)this.e.a(b.class);
      if (localb.d())
        d();
      if (i == 0)
        break label242;
      this.b = ((List)paramBundle.getSerializable("atm_locations"));
      a(this.b);
    }
    label242: 
    while (localb.d())
    {
      return;
      i = 0;
      break;
    }
    a(b.class, new String[0]);
  }

  public final void a(IServiceError paramIServiceError)
  {
    this.a.setText(paramIServiceError.a());
    this.a.setVisibility(0);
  }

  public final Location c()
  {
    return this.k;
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    paramBundle.putSerializable("atm_locations", (Serializable)this.b);
    paramBundle.putSerializable("prev_searches", (Serializable)this.l);
    if (s.m(this.a.getText().toString()))
      paramBundle.putString("message", this.a.getText().toString());
  }

  public static class a extends b<FindBranchActivity, Void, Void, BranchLocateResponse>
  {
    protected void onPreExecute()
    {
      FindBranchActivity.j((FindBranchActivity)this.b).setVisibility(8);
      FindBranchActivity.f((FindBranchActivity)this.b);
      super.onPreExecute();
    }
  }

  public static class b extends ae<FindBranchActivity, String, Void, Location>
  {
    protected void onCancelled()
    {
      FindBranchActivity.h((FindBranchActivity)this.b);
    }

    protected void onPreExecute()
    {
      super.onPreExecute();
      FindBranchActivity.f((FindBranchActivity)this.b);
      FindBranchActivity.g((FindBranchActivity)this.b);
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.FindBranchActivity
 * JD-Core Version:    0.6.2
 */