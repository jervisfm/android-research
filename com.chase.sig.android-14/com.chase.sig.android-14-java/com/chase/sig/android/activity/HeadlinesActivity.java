package com.chase.sig.android.activity;

import android.content.Intent;
import android.os.Bundle;
import com.chase.sig.android.domain.QuoteNewsArticle;
import com.chase.sig.android.service.QuoteNewsResponse;
import com.chase.sig.android.util.s;
import com.chase.sig.android.view.detail.DetailView;
import com.chase.sig.android.view.detail.a;
import com.chase.sig.android.view.detail.t;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class HeadlinesActivity extends ai
{
  public static final SimpleDateFormat a = new SimpleDateFormat("h:mm a M/d/yy");
  private DetailView b;
  private QuoteNewsResponse c;

  public final void a(Bundle paramBundle)
  {
    setTitle(2131165896);
    b(2130903108);
    this.b = ((DetailView)findViewById(2131296533));
    this.b.d();
    this.c = ((QuoteNewsResponse)getIntent().getExtras().getSerializable("quote_news_response"));
    if ((this.c == null) || (paramBundle != null))
      this.c = ((QuoteNewsResponse)paramBundle.getSerializable("quote_news_response"));
    if (this.c != null)
    {
      DetailView localDetailView = this.b;
      ArrayList localArrayList1 = this.c.a();
      ArrayList localArrayList2 = new ArrayList();
      Iterator localIterator = localArrayList1.iterator();
      while (localIterator.hasNext())
      {
        QuoteNewsArticle localQuoteNewsArticle = (QuoteNewsArticle)localIterator.next();
        String str = localQuoteNewsArticle.c().trim();
        String[] arrayOfString = new String[3];
        arrayOfString[0] = localQuoteNewsArticle.a();
        arrayOfString[1] = localQuoteNewsArticle.b();
        arrayOfString[2] = a.format(s.b(localQuoteNewsArticle.d()));
        t localt = new t(str, s.a(" ", arrayOfString).trim(), (byte)0);
        localt.m = true;
        localt.h = new dj(this, localQuoteNewsArticle);
        localArrayList2.add(localt);
      }
      localDetailView.setRows((a[])localArrayList2.toArray(new t[0]));
      localDetailView.setVisibility(0);
    }
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    paramBundle.putSerializable("quote_news_response", this.c);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.HeadlinesActivity
 * JD-Core Version:    0.6.2
 */