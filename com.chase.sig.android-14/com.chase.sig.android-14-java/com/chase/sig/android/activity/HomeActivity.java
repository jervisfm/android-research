package com.chase.sig.android.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.b;
import com.chase.sig.android.service.GenericResponse;
import com.chase.sig.android.view.k;
import com.chase.sig.android.view.k.a;
import java.util.HashSet;
import java.util.StringTokenizer;

public class HomeActivity extends eb
  implements View.OnClickListener
{
  private String a;
  private String b;

  private static ApplicationInfo a(ChaseApplication paramChaseApplication)
  {
    try
    {
      ApplicationInfo localApplicationInfo = paramChaseApplication.getPackageManager().getApplicationInfo(paramChaseApplication.getPackageName(), 2);
      return localApplicationInfo;
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
    }
    return null;
  }

  // ERROR //
  private String d()
  {
    // Byte code:
    //   0: new 43	java/io/BufferedReader
    //   3: dup
    //   4: new 45	java/io/InputStreamReader
    //   7: dup
    //   8: aload_0
    //   9: invokevirtual 49	com/chase/sig/android/activity/HomeActivity:getAssets	()Landroid/content/res/AssetManager;
    //   12: ldc 51
    //   14: invokevirtual 57	android/content/res/AssetManager:open	(Ljava/lang/String;)Ljava/io/InputStream;
    //   17: invokespecial 60	java/io/InputStreamReader:<init>	(Ljava/io/InputStream;)V
    //   20: invokespecial 63	java/io/BufferedReader:<init>	(Ljava/io/Reader;)V
    //   23: astore_1
    //   24: new 65	java/lang/StringBuilder
    //   27: dup
    //   28: invokespecial 66	java/lang/StringBuilder:<init>	()V
    //   31: astore_2
    //   32: aload_1
    //   33: invokevirtual 69	java/io/BufferedReader:readLine	()Ljava/lang/String;
    //   36: astore 7
    //   38: aload 7
    //   40: ifnull +30 -> 70
    //   43: aload_2
    //   44: aload 7
    //   46: invokevirtual 73	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   49: bipush 10
    //   51: invokevirtual 76	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
    //   54: pop
    //   55: goto -23 -> 32
    //   58: astore 5
    //   60: aload_1
    //   61: ifnull +7 -> 68
    //   64: aload_1
    //   65: invokevirtual 79	java/io/BufferedReader:close	()V
    //   68: aconst_null
    //   69: areturn
    //   70: aload_2
    //   71: invokevirtual 82	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   74: astore 9
    //   76: aload_1
    //   77: invokevirtual 79	java/io/BufferedReader:close	()V
    //   80: aload 9
    //   82: areturn
    //   83: astore 10
    //   85: aload 9
    //   87: areturn
    //   88: astore 12
    //   90: aconst_null
    //   91: astore_1
    //   92: aload 12
    //   94: astore_3
    //   95: aload_1
    //   96: ifnull +7 -> 103
    //   99: aload_1
    //   100: invokevirtual 79	java/io/BufferedReader:close	()V
    //   103: aload_3
    //   104: athrow
    //   105: astore 6
    //   107: aconst_null
    //   108: areturn
    //   109: astore 4
    //   111: goto -8 -> 103
    //   114: astore_3
    //   115: goto -20 -> 95
    //   118: astore 11
    //   120: aconst_null
    //   121: astore_1
    //   122: goto -62 -> 60
    //
    // Exception table:
    //   from	to	target	type
    //   24	32	58	java/io/IOException
    //   32	38	58	java/io/IOException
    //   43	55	58	java/io/IOException
    //   70	76	58	java/io/IOException
    //   76	80	83	java/io/IOException
    //   0	24	88	finally
    //   64	68	105	java/io/IOException
    //   99	103	109	java/io/IOException
    //   24	32	114	finally
    //   32	38	114	finally
    //   43	55	114	finally
    //   70	76	114	finally
    //   0	24	118	java/io/IOException
  }

  public final void a(Bundle paramBundle)
  {
    ChaseApplication localChaseApplication1 = (ChaseApplication)getApplication();
    ApplicationInfo localApplicationInfo = a(localChaseApplication1);
    int i;
    if (localApplicationInfo != null)
    {
      int k = 0x2 & localApplicationInfo.flags;
      localApplicationInfo.flags = k;
      if (k != 0)
        i = 1;
    }
    while (true)
    {
      if ((!localChaseApplication1.j()) && (i != 0))
        finish();
      ChaseApplication localChaseApplication2 = ChaseApplication.a();
      HashSet localHashSet = new HashSet();
      StringTokenizer localStringTokenizer = new StringTokenizer(localChaseApplication2.a("allowed_devices"), ",");
      while (true)
        if (localStringTokenizer.hasMoreElements())
        {
          localHashSet.add(localStringTokenizer.nextToken());
          continue;
          i = 0;
          break;
        }
      String str;
      int j;
      if (localHashSet.size() != 0)
      {
        str = Settings.Secure.getString(ChaseApplication.a().getApplicationContext().getContentResolver(), "android_id");
        if ((str == null) || (Build.PRODUCT.equals("google_sdk")) || (Build.PRODUCT.equals("sdk")))
          j = 1;
      }
      while (j == 0)
      {
        e("Invalid Device.");
        return;
        if (!localHashSet.contains(str))
        {
          new Object[] { str };
          j = 0;
        }
        else
        {
          j = 1;
        }
      }
      if (!((ChaseApplication)getApplication()).e().contains("can_auto_focus"))
        a(b.class, new Void[0]);
      if (paramBundle != null)
      {
        this.a = paramBundle.getString("app_update_msg");
        this.b = paramBundle.getString("app_update_url");
      }
      while (true)
      {
        b(2130903109);
        ((View)n().getParent()).setBackgroundResource(2130837519);
        findViewById(2131296481).setVisibility(0);
        a(2131296483, this);
        a(2131296485, this);
        a(2131296484, this);
        a(2131296302, i(2));
        if (localChaseApplication1.j())
          findViewById(2131296483).setOnLongClickListener(new dk(this, localChaseApplication1));
        a(2131296484, a(ContactUsActivity.class));
        Window localWindow = getWindow();
        localWindow.setFormat(1);
        localWindow.addFlags(4096);
        return;
        a(a.class, new Void[0]);
      }
      i = 0;
    }
  }

  protected final boolean b_()
  {
    return false;
  }

  public void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    int i = paramView.getId();
    Intent localIntent = null;
    switch (i)
    {
    default:
    case 2131296483:
    case 2131296485:
    case 2131296484:
    }
    while (true)
    {
      if (localIntent != null)
        startActivity(localIntent);
      return;
      localIntent = new Intent(this, LoginActivity.class);
      continue;
      localIntent = new Intent(this, FindBranchActivity.class);
      continue;
      localIntent = new Intent(this, ContactUsActivity.class);
    }
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    switch (paramInt)
    {
    default:
      return super.onCreateDialog(paramInt);
    case 0:
      k.a locala5 = new k.a(this);
      k.a locala6 = locala5.a(this.a);
      locala6.i = false;
      locala6.c(2131165314).b(2131165295, new dq(this));
      return locala5.d(-1);
    case 1:
      k.a locala2 = new k.a(this);
      k.a locala3 = locala2.a(this.a).c(2131165314);
      locala3.i = true;
      k.a locala4 = locala3.a(2131165295, new ds(this));
      locala4.i = true;
      locala4.b(2131165296, new dr(this));
      return locala2.d(-1);
    case 2:
    }
    k.a locala1 = new k.a(this);
    View localView = ((LayoutInflater)getSystemService("layout_inflater")).inflate(2130903113, null);
    ChaseApplication localChaseApplication = (ChaseApplication)getApplication();
    ((TextView)localView.findViewById(2131296494)).setText(localChaseApplication.k());
    ((TextView)localView.findViewById(2131296493)).setText(getString(2131165188));
    locala1.k = localView;
    return locala1.d(-1);
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    return false;
  }

  protected void onResume()
  {
    super.onResume();
    SharedPreferences localSharedPreferences = getSharedPreferences("eula", 0);
    if (!localSharedPreferences.getBoolean("eula.accepted", false))
    {
      k.a locala = new k.a(this);
      locala.c(2131165830).i = true;
      locala.a(2131165831, new dt(this, localSharedPreferences));
      locala.b(2131165832, new du(this));
      locala.g = new dv(this);
      locala.a(d());
      locala.d(-1).show();
    }
    ((ChaseApplication)getApplication()).q();
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    paramBundle.putString("app_update_msg", this.a);
    paramBundle.putString("app_update_url", this.b);
  }

  public static class a extends b<HomeActivity, Void, Void, GenericResponse>
  {
    protected void onCancelled()
    {
      ((HomeActivity)this.b).finish();
    }
  }

  public static class b extends ae<HomeActivity, Void, Void, Boolean>
  {
    // ERROR //
    private static Boolean a()
    {
      // Byte code:
      //   0: invokestatic 19	android/hardware/Camera:open	()Landroid/hardware/Camera;
      //   3: astore 5
      //   5: aload 5
      //   7: astore_1
      //   8: aload_1
      //   9: invokevirtual 23	android/hardware/Camera:getParameters	()Landroid/hardware/Camera$Parameters;
      //   12: invokevirtual 29	android/hardware/Camera$Parameters:getFocusMode	()Ljava/lang/String;
      //   15: astore 7
      //   17: aload 7
      //   19: invokestatic 35	com/chase/sig/android/util/s:m	(Ljava/lang/String;)Z
      //   22: ifeq +38 -> 60
      //   25: aload 7
      //   27: ldc 37
      //   29: invokevirtual 43	java/lang/String:equals	(Ljava/lang/Object;)Z
      //   32: ifne +28 -> 60
      //   35: iconst_1
      //   36: istore 8
      //   38: iload 8
      //   40: invokestatic 49	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
      //   43: astore 9
      //   45: aload 9
      //   47: astore 4
      //   49: aload_1
      //   50: ifnull +7 -> 57
      //   53: aload_1
      //   54: invokevirtual 52	android/hardware/Camera:release	()V
      //   57: aload 4
      //   59: areturn
      //   60: iconst_0
      //   61: istore 8
      //   63: goto -25 -> 38
      //   66: astore_3
      //   67: aconst_null
      //   68: astore_1
      //   69: aconst_null
      //   70: astore 4
      //   72: aload_1
      //   73: ifnull -16 -> 57
      //   76: aload_1
      //   77: invokevirtual 52	android/hardware/Camera:release	()V
      //   80: aconst_null
      //   81: areturn
      //   82: astore_0
      //   83: aconst_null
      //   84: astore_1
      //   85: aload_0
      //   86: astore_2
      //   87: aload_1
      //   88: ifnull +7 -> 95
      //   91: aload_1
      //   92: invokevirtual 52	android/hardware/Camera:release	()V
      //   95: aload_2
      //   96: athrow
      //   97: astore_2
      //   98: goto -11 -> 87
      //   101: astore 6
      //   103: goto -34 -> 69
      //
      // Exception table:
      //   from	to	target	type
      //   0	5	66	java/lang/Exception
      //   0	5	82	finally
      //   8	35	97	finally
      //   38	45	97	finally
      //   8	35	101	java/lang/Exception
      //   38	45	101	java/lang/Exception
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.HomeActivity
 * JD-Core Version:    0.6.2
 */