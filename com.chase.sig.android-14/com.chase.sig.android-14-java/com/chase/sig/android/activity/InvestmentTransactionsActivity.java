package com.chase.sig.android.activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.d;
import com.chase.sig.android.domain.IAccount;
import com.chase.sig.android.domain.InvestmentTransaction;
import com.chase.sig.android.domain.InvestmentTransaction.FilterType;
import com.chase.sig.android.domain.g;
import com.chase.sig.android.domain.o;
import com.chase.sig.android.service.InvestmentTransactionsResponse;
import com.chase.sig.android.util.Dollar;
import com.chase.sig.android.util.s;
import com.chase.sig.android.view.JPSortableButtonBar;
import java.util.List;

public class InvestmentTransactionsActivity extends ai
  implements fk.d
{
  private g a;
  private IAccount b;
  private ListView c;
  private TextView d;
  private TextView k;
  private TextView l;
  private JPSortableButtonBar m;
  private InvestmentTransactionsResponse n;
  private InvestmentTransaction.FilterType o;
  private String p;
  private boolean q;
  private Button r;

  private void d()
  {
    this.m.a(1, Boolean.valueOf(false));
  }

  public final void a(Bundle paramBundle)
  {
    setTitle(2131165869);
    b(2130903117);
    this.a = ((ChaseApplication)getApplication()).b().b;
    String str = getIntent().getExtras().getString("selectedAccountId");
    this.b = this.a.a(str);
    this.d = ((TextView)findViewById(2131296269));
    this.d.setText(this.b.a());
    this.k = ((TextView)findViewById(2131296503));
    TextView localTextView = this.k;
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = this.b.c();
    localTextView.setText(String.format("(%s)", arrayOfObject));
    this.l = ((TextView)findViewById(2131296507));
    this.c = ((ListView)findViewById(2131296506));
    this.m = ((JPSortableButtonBar)findViewById(2131296505));
    this.m.a(1, 2131165320);
    this.m.a(2, 2131165321);
    this.m.a(3, 2131165322);
    d();
    this.m.setSortableButtonBarListener(new dx(this));
    if (paramBundle == null)
      a(a.class, new Object[0]);
    while (true)
    {
      this.r = ((Button)findViewById(2131296504));
      this.r.setOnClickListener(new dw(this));
      return;
      this.q = paramBundle.getBoolean("fromFilter");
      this.n = ((InvestmentTransactionsResponse)paramBundle.getSerializable("transactionsResponse"));
      this.o = ((InvestmentTransaction.FilterType)paramBundle.getSerializable("filterType"));
      this.p = paramBundle.getString("dateRange");
      a(this.n);
    }
  }

  public final void a(InvestmentTransactionsResponse paramInvestmentTransactionsResponse)
  {
    if ((paramInvestmentTransactionsResponse != null) && (paramInvestmentTransactionsResponse.a() != null) && (paramInvestmentTransactionsResponse.a().size() > 0))
    {
      this.c.setAdapter(new c(this, paramInvestmentTransactionsResponse.a(), this.m));
      this.c.setOnItemClickListener(new b((byte)0));
      this.c.setVisibility(0);
      this.m.setVisibility(0);
      this.l.setVisibility(8);
      return;
    }
    this.c.setVisibility(8);
    this.m.setVisibility(8);
    this.l.setVisibility(0);
    if (this.q)
    {
      this.l.setText(2131165871);
      return;
    }
    this.l.setText(2131165870);
  }

  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    if ((paramInt1 == 0) && (paramInt2 == 1))
    {
      this.q = true;
      Bundle localBundle = paramIntent.getExtras();
      this.o = ((InvestmentTransaction.FilterType)localBundle.getSerializable("filter_type"));
      this.p = localBundle.getString("range");
      new Object[1][0] = this.o;
      new Object[1][0] = this.p;
      Object[] arrayOfObject = new Object[2];
      arrayOfObject[0] = this.p;
      arrayOfObject[1] = this.o;
      a(a.class, arrayOfObject);
    }
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    paramBundle.putSerializable("transactionsResponse", this.n);
    paramBundle.putBoolean("fromFilter", this.q);
    paramBundle.putSerializable("filterType", this.o);
    paramBundle.putString("dateRange", this.p);
  }

  public static class a extends d<InvestmentTransactionsActivity, Object, Void, InvestmentTransactionsResponse>
  {
  }

  private final class b
    implements AdapterView.OnItemClickListener
  {
    private b()
    {
    }

    public final void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
    {
      try
      {
        InvestmentTransaction localInvestmentTransaction = (InvestmentTransaction)paramAdapterView.getItemAtPosition(paramInt);
        Intent localIntent = new Intent(InvestmentTransactionsActivity.this, InvestmentTransactionsDetailActivity.class);
        localIntent.putExtra("transaction_object", localInvestmentTransaction);
        localIntent.putExtra("account_nickname_mask", InvestmentTransactionsActivity.d(InvestmentTransactionsActivity.this).B());
        InvestmentTransactionsActivity.this.startActivity(localIntent);
        BehaviorAnalyticsAspect.a();
        BehaviorAnalyticsAspect.a(paramAdapterView, paramView, paramInt);
        return;
      }
      catch (Throwable localThrowable)
      {
        BehaviorAnalyticsAspect.a();
        BehaviorAnalyticsAspect.a(paramAdapterView, paramView, paramInt);
        throw localThrowable;
      }
    }
  }

  final class c extends ArrayAdapter<InvestmentTransaction>
  {
    private final List<InvestmentTransaction> b;
    private final JPSortableButtonBar c;
    private final LayoutInflater d;

    public c(int paramJPSortableButtonBar, JPSortableButtonBar arg3)
    {
      super(2130903116, localList);
      this.b = localList;
      Object localObject;
      this.c = localObject;
      this.d = ((LayoutInflater)paramJPSortableButtonBar.getSystemService("layout_inflater"));
    }

    public final View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      InvestmentTransaction localInvestmentTransaction = (InvestmentTransaction)this.b.get(paramInt);
      if (paramView == null)
        paramView = this.d.inflate(2130903116, null);
      ((TextView)paramView.findViewById(2131296469)).setText(localInvestmentTransaction.f());
      int i = this.c.getChildAt(0).getWidth();
      TextView localTextView1 = (TextView)paramView.findViewById(2131296499);
      localTextView1.setText(s.k(localInvestmentTransaction.b()));
      localTextView1.setWidth(i);
      ((TextView)paramView.findViewById(2131296496)).setText(localInvestmentTransaction.e());
      TextView localTextView2 = (TextView)paramView.findViewById(2131296500);
      localTextView2.setText(localInvestmentTransaction.a().h());
      if (localInvestmentTransaction.a().d())
      {
        localTextView2.setTextColor(InvestmentTransactionsActivity.this.getResources().getColor(2131099678));
        return paramView;
      }
      localTextView2.setTextColor(InvestmentTransactionsActivity.this.getResources().getColor(2131099650));
      return paramView;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.InvestmentTransactionsActivity
 * JD-Core Version:    0.6.2
 */