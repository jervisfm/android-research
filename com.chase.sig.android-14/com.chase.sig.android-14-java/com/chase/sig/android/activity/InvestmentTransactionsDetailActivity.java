package com.chase.sig.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;
import com.chase.sig.android.domain.InvestmentTransaction;
import com.chase.sig.android.util.Dollar;
import com.chase.sig.android.util.s;
import com.chase.sig.android.view.detail.DetailRow;
import com.chase.sig.android.view.detail.DetailView;
import com.chase.sig.android.view.detail.a;

public class InvestmentTransactionsDetailActivity extends ai
  implements fk.d
{
  public final void a(Bundle paramBundle)
  {
    setTitle(2131165872);
    b(2130903114);
    InvestmentTransaction localInvestmentTransaction = (InvestmentTransaction)getIntent().getExtras().get("transaction_object");
    String str1;
    String str2;
    label52: String str3;
    if (localInvestmentTransaction.g() == null)
    {
      str1 = "";
      if (!s.l(localInvestmentTransaction.h()))
        break label435;
      str2 = "";
      if (!s.l(localInvestmentTransaction.b()))
        break label447;
      str3 = "";
      label66: if (!localInvestmentTransaction.a().d())
        break label619;
    }
    label435: label447: label619: for (int i = 2131099678; ; i = 2131099650)
    {
      if ((localInvestmentTransaction.d()) && (localInvestmentTransaction.k().d()));
      for (int j = 2131099678; ; j = 2131099650)
      {
        DetailView localDetailView = (DetailView)findViewById(2131296271);
        localDetailView.d();
        ((TextView)findViewById(2131296495)).setText((CharSequence)getIntent().getExtras().get("account_nickname_mask"));
        if (localInvestmentTransaction.d())
        {
          a[] arrayOfa2 = new a[9];
          arrayOfa2[0] = new DetailRow("Trade date", s.a(str1));
          arrayOfa2[1] = new DetailRow("Settle date", s.a(str2));
          arrayOfa2[2] = new DetailRow("Security ID", s.a(localInvestmentTransaction.i()));
          arrayOfa2[3] = new DetailRow("Type", s.a(localInvestmentTransaction.e()));
          arrayOfa2[4] = new DetailRow("Description", s.a(localInvestmentTransaction.f()));
          arrayOfa2[5] = new DetailRow("Price", s.a(localInvestmentTransaction.j().toString()));
          arrayOfa2[6] = new DetailRow("Quantity", s.a(Double.valueOf(localInvestmentTransaction.c()), "#,##0.00"));
          arrayOfa2[7] = new DetailRow("Amount", s.a(localInvestmentTransaction.a().f().toString()) + " " + localInvestmentTransaction.l()).e(i);
          arrayOfa2[8] = new DetailRow("Cost", s.a(localInvestmentTransaction.k().f().toString() + " " + localInvestmentTransaction.m())).e(j);
          localDetailView.setRows(arrayOfa2);
          return;
          str1 = s.d(localInvestmentTransaction.g());
          break;
          str2 = s.d(localInvestmentTransaction.h());
          break label52;
          str3 = s.d(localInvestmentTransaction.b());
          break label66;
        }
        a[] arrayOfa1 = new a[5];
        arrayOfa1[0] = new DetailRow("Date", s.a(str3));
        arrayOfa1[1] = new DetailRow("Type", s.a(localInvestmentTransaction.e()));
        arrayOfa1[2] = new DetailRow("Description", s.a(localInvestmentTransaction.f()));
        arrayOfa1[3] = new DetailRow("Quantity", s.a(Double.valueOf(localInvestmentTransaction.c()), "#,##0.00"));
        arrayOfa1[4] = new DetailRow("Amount", s.a(localInvestmentTransaction.a().f().toString()) + " " + localInvestmentTransaction.l()).e(i);
        localDetailView.setRows(arrayOfa1);
        return;
      }
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.InvestmentTransactionsDetailActivity
 * JD-Core Version:    0.6.2
 */