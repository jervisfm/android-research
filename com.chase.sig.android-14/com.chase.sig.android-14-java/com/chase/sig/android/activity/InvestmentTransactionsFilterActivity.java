package com.chase.sig.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import com.chase.sig.android.domain.InvestmentTransaction.FilterType;
import com.chase.sig.android.util.r;
import com.chase.sig.android.util.s;
import com.chase.sig.android.view.JPSpinner;
import java.util.List;

public class InvestmentTransactionsFilterActivity extends ai
  implements fk.d
{
  private static final String[] d = { "Current Day", "Prior Day", "Last 7 Days", "Last 14 Days", "Last 30 Days" };
  private static final String[] k = { "CURRENT_DAY", "PRIOR_DAY", "LAST_7_DAYS", "LAST_14_DAYS", "LAST_30_DAYS" };
  private JPSpinner a;
  private JPSpinner b;
  private List<InvestmentTransaction.FilterType> c;

  public final void a(Bundle paramBundle)
  {
    int i = 0;
    setTitle(2131165873);
    b(2130903115);
    Bundle localBundle = getIntent().getExtras();
    this.a = ((JPSpinner)findViewById(2131296496));
    JPSpinner localJPSpinner = this.a;
    this.c = ((List)getIntent().getSerializableExtra("filter_types"));
    localJPSpinner.setAdapter(new ArrayAdapter(this, 2130903215, this.c));
    this.a.setChoiceMode(1);
    int m;
    if (localBundle.containsKey("filter_type"))
    {
      InvestmentTransaction.FilterType localFilterType = (InvestmentTransaction.FilterType)localBundle.getSerializable("filter_type");
      new r();
      m = r.a(this.c, new ea(this, localFilterType));
      if (m >= 0)
        this.a.setSelection(m);
    }
    while (true)
    {
      this.b = ((JPSpinner)findViewById(2131296497));
      this.b.setAdapter(new ArrayAdapter(this, 2130903215, d));
      this.b.setChoiceMode(1);
      String str = localBundle.getString("range");
      if (!s.m(str))
        break label252;
      j = 0;
      while (i < k.length)
      {
        if (str.equals(k[i]))
          j = i;
        i++;
      }
      m = 0;
      break;
      this.a.setSelection(0);
    }
    label252: int j = 0;
    this.b.setSelection(j);
    findViewById(2131296368).setOnClickListener(new dz(this));
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.InvestmentTransactionsFilterActivity
 * JD-Core Version:    0.6.2
 */