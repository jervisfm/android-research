package com.chase.sig.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import com.chase.sig.android.domain.BranchLocation;
import com.chase.sig.android.util.s;
import com.google.common.a.a;

public class LocationInfoActivity extends eb
{
  public final void a(Bundle paramBundle)
  {
    b(2130903128);
    BranchLocation localBranchLocation = (BranchLocation)getIntent().getExtras().get("location");
    boolean bool = "branch".equalsIgnoreCase(localBranchLocation.d());
    int i;
    if (bool)
    {
      i = 2131165449;
      setTitle(i);
      ((TextView)findViewById(2131296546)).setText(localBranchLocation.e());
      ((TextView)findViewById(2131296550)).setText(localBranchLocation.a() + "\n" + localBranchLocation.c() + ", " + localBranchLocation.g() + "\n" + localBranchLocation.h());
      ((TextView)findViewById(2131296556)).setText(localBranchLocation.j() + " miles");
      if (!bool)
        break label310;
      ((TextView)findViewById(2131296559)).setText(localBranchLocation.b());
      ((TextView)findViewById(2131296562)).setText(a.a("\n").a().a(localBranchLocation.k()));
      ((TextView)findViewById(2131296565)).setText(a.a("\n").a().a(localBranchLocation.l()));
      ((TextView)findViewById(2131296553)).setText(s.f(localBranchLocation.f()));
      findViewById(2131296569).setVisibility(8);
      findViewById(2131296572).setVisibility(8);
      findViewById(2131296566).setVisibility(8);
    }
    while (true)
    {
      a(2131296575, new ev(this, localBranchLocation));
      return;
      i = 2131165448;
      break;
      label310: ((TextView)findViewById(2131296568)).setText(localBranchLocation.i());
      ((TextView)findViewById(2131296571)).setText(a.a("\n").a().a(localBranchLocation.n()));
      ((TextView)findViewById(2131296574)).setText(a.a("\n").a().a(localBranchLocation.m()));
      findViewById(2131296551).setVisibility(8);
      findViewById(2131296563).setVisibility(8);
      findViewById(2131296560).setVisibility(8);
      findViewById(2131296557).setVisibility(8);
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.LocationInfoActivity
 * JD-Core Version:    0.6.2
 */