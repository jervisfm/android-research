package com.chase.sig.android.activity;

import android.content.Intent;
import android.os.AsyncTask.Status;
import android.os.Bundle;
import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.c;
import com.chase.sig.android.domain.a;
import com.chase.sig.android.domain.o;
import com.chase.sig.android.service.n;
import com.chase.sig.android.service.q;
import com.chase.sig.android.util.m;
import java.util.Hashtable;

public class LogOutActivity extends ai
{
  private boolean a;

  public final void a(Bundle paramBundle)
  {
    b(2130903131);
    this.h = true;
    this.a = getIntent().getExtras().getBoolean("wasBecauseSessionTimedOut");
  }

  protected void onResume()
  {
    super.onResume();
    ChaseApplication.u();
    c.a();
    a locala = (a)this.e.a(a.class);
    if (locala.getStatus() != AsyncTask.Status.RUNNING)
    {
      Boolean[] arrayOfBoolean = new Boolean[1];
      arrayOfBoolean[0] = Boolean.valueOf(this.a);
      locala.execute(arrayOfBoolean);
    }
  }

  public static class a extends ae<eb, Boolean, Void, Void>
  {
    private boolean a;

    private Void a(Boolean[] paramArrayOfBoolean)
    {
      this.a = paramArrayOfBoolean[0].booleanValue();
      n localn = this.b.A();
      if (localn.a == null)
        localn.a = new q();
      try
      {
        String str = q.a(2131165193);
        Hashtable localHashtable = q.c();
        localHashtable.put("userId", ChaseApplication.a().b().a.c);
        m.a(ChaseApplication.a(), str, localHashtable);
        label84: return null;
      }
      catch (Exception localException)
      {
        break label84;
      }
    }

    protected void onPreExecute()
    {
      super.onPreExecute();
      this.b.showDialog(-3);
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.LogOutActivity
 * JD-Core Version:    0.6.2
 */