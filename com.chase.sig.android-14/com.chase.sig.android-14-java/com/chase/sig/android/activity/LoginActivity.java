package com.chase.sig.android.activity;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask.Status;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.b;
import com.chase.sig.android.d;
import com.chase.sig.android.domain.OneTimePasswordContact;
import com.chase.sig.android.domain.a;
import com.chase.sig.android.domain.o;
import com.chase.sig.android.service.GenericResponse;
import com.chase.sig.android.service.OneTimePasswordContactsResponse;
import com.chase.sig.android.service.SplashResponse;
import com.chase.sig.android.service.n;
import com.chase.sig.android.service.t;
import com.chase.sig.android.util.ChaseException;
import com.chase.sig.android.util.s;
import com.chase.sig.android.view.k.a;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

public class LoginActivity extends eb
{
  private TextView a;
  private EditText b;
  private EditText c;
  private CheckBox d;
  private CheckBox k;
  private EditText l;
  private EditText m;

  private void e()
  {
    ey localey = new ey(this);
    if (f())
    {
      this.m.setOnKeyListener(localey);
      this.c.setOnKeyListener(null);
      this.l.setOnKeyListener(null);
      return;
    }
    if (this.k.isChecked())
    {
      this.l.setOnKeyListener(localey);
      this.c.setOnKeyListener(null);
      return;
    }
    this.c.setOnKeyListener(localey);
    this.l.setOnKeyListener(null);
  }

  private boolean f()
  {
    return getIntent().getBooleanExtra("sync_auth_tokens", false);
  }

  public final void a(Bundle paramBundle)
  {
    String str;
    EditText localEditText;
    if (f())
    {
      b(2130903234);
      this.m = ((EditText)findViewById(2131296961));
      this.b = ((EditText)findViewById(2131296580));
      this.c = ((EditText)findViewById(2131296581));
      this.d = ((CheckBox)findViewById(2131296583));
      this.k = ((CheckBox)findViewById(2131296584));
      this.l = ((EditText)findViewById(2131296582));
      this.a = ((TextView)findViewById(2131296578));
      SharedPreferences localSharedPreferences = getSharedPreferences("chase", 0);
      str = localSharedPreferences.getString("user_id", null);
      if (this.k != null)
      {
        this.k.setChecked(localSharedPreferences.getBoolean("token_user", false));
        this.k.setOnCheckedChangeListener(new ew(this));
        localEditText = this.l;
        if (!this.k.isChecked())
          break label262;
      }
    }
    label262: for (int i = 0; ; i = 8)
    {
      localEditText.setVisibility(i);
      if (str != null)
      {
        this.b.setText(str);
        ((CheckBox)findViewById(2131296583)).setChecked(true);
        this.a.setText(2131165370);
      }
      a(2131296585, new ez(this));
      a(2131296586, new ex(this));
      e();
      return;
      b(2130903130);
      break;
    }
  }

  protected final void a(List<OneTimePasswordContact> paramList)
  {
    Intent localIntent = new Intent(this, DeviceCodeNotFoundActivity.class);
    localIntent.putExtra("otp_contacts", (Serializable)paramList);
    startActivity(localIntent);
  }

  protected final boolean b_()
  {
    return false;
  }

  protected final void d()
  {
    String str1 = this.b.getText().toString();
    String str2 = this.c.getText().toString();
    if (!str1.trim().equals(str1))
    {
      str1 = str1.trim();
      this.b.setText(str1);
    }
    int i;
    String str4;
    String str3;
    if ((s.l(str1)) || (s.l(str2)))
    {
      i = 1;
      if (!f())
        break label210;
      str4 = this.l.getText().toString();
      str3 = this.m.getText().toString();
      if ((!s.l(str4)) && (!s.l(str3)))
        break label189;
      i = 1;
    }
    while (true)
    {
      label111: EditText localEditText = this.c;
      char[] arrayOfChar = new char[localEditText.getText().length()];
      Arrays.fill(arrayOfChar, '*');
      localEditText.setText(String.valueOf(arrayOfChar));
      if (i != 0)
      {
        if (this.l.getVisibility() != 0);
        for (int j = 2131165379; ; j = 2131165380)
        {
          if (f())
            j = 2131165381;
          g(j);
          return;
          i = 0;
          break;
          label189: this.l.setText("");
          this.m.setText("");
          break label111;
          label210: if ((this.k == null) || (!this.k.isChecked()))
            break label422;
          str4 = this.l.getText().toString();
          if ((i != 0) || (s.l(str4)));
          for (i = 1; ; i = 0)
          {
            this.l.setText("");
            str3 = null;
            break;
          }
        }
      }
      boolean bool = this.d.isChecked();
      SharedPreferences.Editor localEditor = getSharedPreferences("chase", 0).edit();
      if (bool)
        localEditor.putString("user_id", str1);
      while (true)
      {
        if (this.k != null)
          localEditor.putBoolean("token_user", this.k.isChecked());
        localEditor.commit();
        String[] arrayOfString = { str1, str2, str4, str3 };
        a locala = (a)this.e.a(a.class);
        if (locala.getStatus() == AsyncTask.Status.RUNNING)
          break;
        locala.execute(arrayOfString);
        return;
        localEditor.remove("user_id");
      }
      label422: str3 = null;
      str4 = null;
    }
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    switch (paramInt)
    {
    default:
      return super.onCreateDialog(paramInt);
    case -10:
      return a(this, 2131165371);
    case 0:
      k.a locala5 = new k.a(this);
      k.a locala6 = locala5.b(2131165359);
      locala6.i = false;
      locala6.a(2131165288, new fa(this));
      return locala5.d(-1);
    case 1:
      k.a locala3 = new k.a(this);
      k.a locala4 = locala3.b(2131165385);
      locala4.i = false;
      locala4.a(2131165288, new fb(this));
      return locala3.d(-1);
    case 2:
    }
    PackageManager localPackageManager = getPackageManager();
    try
    {
      PackageInfo localPackageInfo2 = localPackageManager.getPackageInfo("com.jpm.sig.android", 0);
      localPackageInfo1 = localPackageInfo2;
      int i = 2131165917;
      if (localPackageInfo1 != null)
        i = 2131165918;
      k.a locala1 = new k.a(this);
      Html.fromHtml("");
      k.a locala2 = locala1.b(2131165916);
      locala2.i = true;
      locala2.b = Html.fromHtml(getString(2131165915));
      locala2.e = 0;
      locala2.a(i, new fe(this, localPackageInfo1)).b(2131165920, new fd(this)).c(2131165919, new fc(this));
      return locala1.d(-1);
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
      while (true)
      {
        localNameNotFoundException.getMessage();
        PackageInfo localPackageInfo1 = null;
      }
    }
  }

  protected void onResume()
  {
    super.onResume();
    ((ChaseApplication)getApplication()).q();
    Bundle localBundle = getIntent().getExtras();
    if ((localBundle != null) && (localBundle.getBoolean("SESSION_TIMED_OUT")))
      showDialog(0);
  }

  public static class a extends b<LoginActivity, String, Void, GenericResponse>
  {
    static void a(LoginActivity paramLoginActivity)
    {
      switch (paramLoginActivity.z().b().a.a())
      {
      case 3:
      default:
        LoginActivity.e(paramLoginActivity);
      case 2:
      case 4:
        LoginActivity.b localb;
        do
        {
          return;
          LoginActivity.d(paramLoginActivity);
          return;
          localb = (LoginActivity.b)paramLoginActivity.e.a(LoginActivity.b.class);
        }
        while (localb.getStatus() == AsyncTask.Status.RUNNING);
        localb.execute(new Void[0]);
        return;
      case 5:
      }
      paramLoginActivity.showDialog(1);
    }

    protected void onCancelled()
    {
      LoginActivity.c((LoginActivity)this.b);
      super.onCancelled();
    }
  }

  public static class b extends d<LoginActivity, Void, Void, OneTimePasswordContactsResponse>
  {
    OneTimePasswordContactsResponse a = new OneTimePasswordContactsResponse();

    private OneTimePasswordContactsResponse a()
    {
      n localn = ((LoginActivity)this.b).A();
      if (localn.b == null)
        localn.b = new t();
      try
      {
        this.a = t.a(((LoginActivity)this.b).r());
        return this.a;
      }
      catch (ChaseException localChaseException)
      {
        while (true)
          this.a.a(this.b);
      }
    }
  }

  public static class c extends b<LoginActivity, Void, Void, SplashResponse>
  {
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.LoginActivity
 * JD-Core Version:    0.6.2
 */