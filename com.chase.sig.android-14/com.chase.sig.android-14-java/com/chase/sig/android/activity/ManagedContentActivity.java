package com.chase.sig.android.activity;

import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.d;
import com.chase.sig.android.service.content.ContentResponse;
import com.chase.sig.android.util.e;
import com.chase.sig.android.util.f;
import com.chase.sig.android.util.s;
import java.util.Iterator;
import java.util.List;
import org.apache.http.cookie.Cookie;

public class ManagedContentActivity extends eb
{
  private WebView a;
  private String b;

  private boolean d()
  {
    return (getIntent().getExtras() != null) && (getIntent().getExtras().getBoolean("subuser"));
  }

  public final void a(Bundle paramBundle)
  {
    b(2130903132);
    Bundle localBundle = getIntent().getExtras();
    String str1;
    label57: int i;
    if (localBundle.getInt("viewTitle", -1) != -1)
    {
      setTitle(localBundle.getInt("viewTitle"));
      if (!e.a(localBundle, "content"))
        break label403;
      this.b = localBundle.getString("content");
      str1 = null;
      this.a = ((WebView)findViewById(2131296589));
      this.a.restoreState(paramBundle);
      this.a.setWebViewClient(new a((byte)0));
      this.a.getSettings().setJavaScriptEnabled(true);
      if (e.a(localBundle, "userAgent"))
        this.a.getSettings().setUserAgentString((String)e.a(localBundle, "userAgent", null));
      this.a = ((WebView)findViewById(2131296589));
      this.a.restoreState(paramBundle);
      this.a.setWebViewClient(new a((byte)0));
      this.a.getSettings().setJavaScriptEnabled(true);
      if (e.a(localBundle, "userAgent"))
        this.a.getSettings().setUserAgentString((String)e.a(localBundle, "userAgent", null));
      if (this.a.canGoBack())
        break label458;
      i = 1;
    }
    label403: label458: 
    while (true)
      label226: if (i != 0)
      {
        if (!s.m(str1))
          break label474;
        CookieSyncManager.createInstance(this);
        localCookieManager = CookieManager.getInstance();
        new f((ChaseApplication)getApplication());
        localIterator = f.b().iterator();
        while (true)
          if (localIterator.hasNext())
          {
            localCookie = (Cookie)localIterator.next();
            str2 = localCookie.getName() + "=" + localCookie.getValue() + "; domain=" + localCookie.getDomain() + "; expires=" + localCookie.getExpiryDate();
            new Object[] { str1, str2 };
            localCookieManager.setCookie(str1, str2);
            continue;
            findViewById(2131296302).setVisibility(0);
            break;
            if (e.a(localBundle, "webUrl"))
            {
              str1 = (String)e.a(localBundle, "webUrl", null);
              break label57;
            }
            arrayOfString = new String[1];
            arrayOfString[0] = ((String)e.a(localBundle, "documentId", null));
            a(b.class, arrayOfString);
            str1 = null;
            break label57;
            i = 0;
            break label226;
          }
        this.a.loadUrl(str1);
      }
    label474: 
    while (!s.m(this.b))
    {
      CookieManager localCookieManager;
      Iterator localIterator;
      Cookie localCookie;
      String str2;
      String[] arrayOfString;
      return;
    }
    this.a.loadDataWithBaseURL(null, this.b, "text/html", "utf-8", null);
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if (j(paramInt))
      return true;
    if (paramInt == 4)
    {
      if (this.a.canGoBack())
      {
        this.a.goBack();
        return true;
      }
      if (d())
      {
        d(false);
        return true;
      }
    }
    return super.onKeyDown(paramInt, paramKeyEvent);
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    this.a.saveState(paramBundle);
  }

  private final class a extends WebViewClient
  {
    private a()
    {
    }

    public final void onLoadResource(WebView paramWebView, String paramString)
    {
      super.onLoadResource(paramWebView, paramString);
      if ((ManagedContentActivity.b(ManagedContentActivity.this)) && (paramString.endsWith("/Public/Home/Timeout")))
      {
        CookieManager.getInstance().removeSessionCookie();
        localManagedContentActivity = ManagedContentActivity.this;
        localIntent = new Intent(localManagedContentActivity, LoginActivity.class);
        localIntent.setFlags(67108864);
        localIntent.putExtra("SESSION_TIMED_OUT", true);
        localManagedContentActivity.startActivity(localIntent);
      }
      while (ManagedContentActivity.this.isFinishing())
      {
        ManagedContentActivity localManagedContentActivity;
        Intent localIntent;
        return;
      }
      ManagedContentActivity.this.t();
    }

    public final void onPageFinished(WebView paramWebView, String paramString)
    {
      super.onPageFinished(paramWebView, paramString);
      if (!ManagedContentActivity.this.isFinishing())
        ManagedContentActivity.this.u();
    }

    public final void onReceivedError(WebView paramWebView, int paramInt, String paramString1, String paramString2)
    {
      Object[] arrayOfObject = new Object[3];
      arrayOfObject[0] = paramString2;
      arrayOfObject[1] = Integer.valueOf(paramInt);
      arrayOfObject[2] = paramString1;
      if (!ManagedContentActivity.this.isFinishing())
      {
        ManagedContentActivity.a(ManagedContentActivity.this).setVisibility(8);
        ManagedContentActivity.this.f(2131165820);
      }
    }

    public final boolean shouldOverrideUrlLoading(WebView paramWebView, String paramString)
    {
      if (paramString.startsWith("tel:"))
      {
        Intent localIntent1 = new Intent("android.intent.action.DIAL");
        localIntent1.setData(Uri.parse(paramString));
        if (ManagedContentActivity.this.a(localIntent1))
          ManagedContentActivity.this.startActivity(localIntent1);
        return true;
      }
      if (paramString.endsWith("/Public/Home/LogOff"))
      {
        ManagedContentActivity.this.d(false);
        return true;
      }
      if (paramString.endsWith("/Public/FindUs"))
      {
        Intent localIntent2 = new Intent(ManagedContentActivity.this.getBaseContext(), FindBranchActivity.class);
        ManagedContentActivity.this.startActivity(localIntent2);
        return true;
      }
      if (paramString.endsWith("/Public/Docs/Faqs"))
      {
        Intent localIntent3 = new Intent(ManagedContentActivity.this.getBaseContext(), ManagedContentActivity.class);
        localIntent3.putExtra("webUrl", ManagedContentActivity.this.z().a(new StringBuilder("environment.").append(ManagedContentActivity.this.z().f()).append(".content").toString()) + ManagedContentActivity.this.getResources().getString(2131165285));
        localIntent3.putExtra("viewTitle", 2131165626);
        ManagedContentActivity.this.startActivity(localIntent3);
        return true;
      }
      if (paramString.endsWith("/Public/Home/LogOn"))
      {
        Intent localIntent4 = new Intent(ManagedContentActivity.this.getBaseContext(), HomeActivity.class);
        ManagedContentActivity.this.startActivity(localIntent4);
        return true;
      }
      if (paramString.endsWith("/Public/Docs/PrivacyPolicy?json=1"))
      {
        Intent localIntent5 = new Intent(ManagedContentActivity.this.getBaseContext(), ManagedContentActivity.class);
        localIntent5.putExtra("webUrl", ManagedContentActivity.this.getResources().getString(2131165277));
        localIntent5.putExtra("viewTitle", 2131165854);
        ManagedContentActivity.this.startActivity(localIntent5);
        return true;
      }
      return !paramString.endsWith("/Public/Docs/ContactUs");
    }
  }

  public static class b extends d<ManagedContentActivity, String, Void, ContentResponse>
  {
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.ManagedContentActivity
 * JD-Core Version:    0.6.2
 */