package com.chase.sig.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import com.chase.sig.android.domain.QuoteResponse;
import com.chase.sig.android.domain.g;
import com.chase.sig.android.domain.n;
import com.chase.sig.android.domain.o;
import com.chase.sig.android.service.QuoteNewsResponse;
import com.chase.sig.android.view.JPTabWidget;
import com.chase.sig.android.view.TickerValueTextView;
import com.chase.sig.android.view.detail.DetailView;

public class MarketsActivity extends b
  implements fk.c
{
  private DetailView b;
  private QuoteNewsResponse c;
  private ag<MarketsActivity> d;
  private JPTabWidget k;
  private TickerValueTextView l;
  private QuoteResponse m = new QuoteResponse();

  public MarketsActivity()
  {
    super(2130837622, 2131296593);
  }

  private static String l(int paramInt)
  {
    switch (paramInt)
    {
    default:
      return null;
    case 4:
      return "$XAX";
    case 1:
      return "$INDU";
    case 2:
      return "$COMPQ";
    case 3:
      return "$NYA";
    case 0:
    }
    return "$SPX";
  }

  public final void a(Bundle paramBundle)
  {
    this.f = getString(2131165855);
    b(2130903133);
    if (r().b.z())
      a(2131296256, 2130837569, 2130837568, new fj(this));
    a(2131296257, 2130837573, 2130837572, new ff(this));
    a(2131296258, 2130837571, 2130837570, new fg(this));
    this.l = ((TickerValueTextView)findViewById(2131296592));
    this.k = ((JPTabWidget)findViewById(2131296590));
    this.b = ((DetailView)findViewById(2131296533));
    if (this.d == null)
      this.d = new ag();
    this.k.a();
    this.k.a(0, 2131165882);
    this.k.a(1, 2131165883);
    this.k.a(2, 2131165884);
    this.k.a(3, 2131165885);
    this.k.a(4, 2131165886);
    this.k.setTabListener(new fh(this));
    if (paramBundle != null)
    {
      this.c = ag.a(paramBundle);
      this.m = ((QuoteResponse)paramBundle.getSerializable("indexes_resposne"));
      b(paramBundle);
      this.k.a(paramBundle.getInt("tab_selected"), true);
      e();
    }
    while (true)
    {
      this.d.a(this, this.c, this.b, a.class, new String[0]);
      return;
      d();
      Animation localAnimation = AnimationUtils.loadAnimation(this, 2130968580);
      View localView = findViewById(2131296475);
      localView.setVisibility(0);
      localView.startAnimation(localAnimation);
    }
  }

  protected final void c_()
  {
    super.c_();
    finish();
  }

  protected final void d()
  {
    a(b.class, new Void[0]);
  }

  protected final void e()
  {
    findViewById(2131296593).setOnClickListener(new fi(this));
  }

  public final int f()
  {
    if (this.k.b())
      return this.k.getSelectedTabId();
    return 0;
  }

  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    if ((paramInt1 == 35) && (paramInt2 == 35))
      d();
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    paramBundle.putSerializable("quote_news_response", this.c);
    paramBundle.putInt("tab_selected", this.k.getSelectedTabId());
    paramBundle.putSerializable("indexes_resposne", this.m);
    c(paramBundle);
  }

  public static class a extends ae<MarketsActivity, String, Void, QuoteNewsResponse>
  {
  }

  public static class b extends com.chase.sig.android.b<MarketsActivity, Void, Void, n>
  {
    private String a;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.MarketsActivity
 * JD-Core Version:    0.6.2
 */