package com.chase.sig.android.activity;

import android.content.Intent;
import android.os.AsyncTask.Status;
import android.os.Bundle;
import android.view.View.OnClickListener;
import android.widget.Button;
import com.chase.sig.android.d;
import com.chase.sig.android.domain.MobileAdResponse;
import com.chase.sig.android.domain.MobileAdResponseContent;
import com.chase.sig.android.domain.ResponseActions;
import com.chase.sig.android.domain.ResponseActions.Action;
import com.chase.sig.android.util.e;

public class MobileAdOfferDetailsActivity extends ai
  implements fk.d
{
  private MobileAdResponse a;
  private boolean b = false;
  private View.OnClickListener c = new fl(this);

  public final void a(Bundle paramBundle)
  {
    b(2130903049);
    Button localButton = (Button)findViewById(2131296289);
    ((Button)findViewById(2131296288)).setOnClickListener(this.c);
    localButton.setOnClickListener(this.c);
    MobileAdResponse localMobileAdResponse = (MobileAdResponse)e.a(getIntent().getExtras(), "ad_offer", null);
    b localb = (b)this.e.a(b.class);
    if (localb.getStatus() != AsyncTask.Status.RUNNING)
    {
      String[] arrayOfString = new String[1];
      arrayOfString[0] = localMobileAdResponse.a().b().b().a();
      localb.execute(arrayOfString);
    }
  }

  public void onBackPressed()
  {
    if (this.b)
      setResult(-1);
    while (true)
    {
      super.onBackPressed();
      return;
      setResult(0);
    }
  }

  public static class a extends d<eb, String, Void, MobileAdResponse>
  {
    boolean a;
  }

  public static class b extends d<eb, String, Void, MobileAdResponse>
  {
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.MobileAdOfferDetailsActivity
 * JD-Core Version:    0.6.2
 */