package com.chase.sig.android.activity;

import android.content.res.Resources;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.chase.sig.android.activity.wire.WireHomeActivity;
import com.chase.sig.android.domain.g;
import com.chase.sig.android.domain.o;
import com.chase.sig.android.service.quickpay.TodoListResponse;
import com.chase.sig.android.util.s;

public class PaymentsAndTransfersHomeActivity extends ai
  implements fk.e
{
  LinearLayout a;
  LinearLayout b;
  LinearLayout c;
  LinearLayout d;
  LinearLayout k;
  View l;
  View m;
  View n;
  View o;
  private g p;

  private static void a(LinearLayout paramLinearLayout, int paramInt)
  {
    paramLinearLayout.setBackgroundDrawable(paramLinearLayout.getResources().getDrawable(paramInt));
    paramLinearLayout.setPadding(10, 10, 10, 10);
  }

  private void e()
  {
    findViewById(2131296475).clearAnimation();
    findViewById(2131296475).setVisibility(8);
    findViewById(2131296610).setPadding(5, 0, 0, 0);
  }

  public final void a(Bundle paramBundle)
  {
    b(2130903139);
    setTitle(2131165496);
    this.p = r().b;
    this.a = ((LinearLayout)findViewById(2131296606));
    this.b = ((LinearLayout)findViewById(2131296607));
    this.c = ((LinearLayout)findViewById(2131296613));
    this.d = ((LinearLayout)findViewById(2131296615));
    this.k = ((LinearLayout)findViewById(2131296609));
    this.l = findViewById(2131296324);
    this.m = findViewById(2131296608);
    this.n = findViewById(2131296612);
    this.o = findViewById(2131296614);
    ((TextView)findViewById(2131296610)).setText(Html.fromHtml(getResources().getString(2131165777) + s.z(getResources().getString(2131165778))));
    g localg = this.p;
    LinearLayout localLinearLayout1;
    label337: LinearLayout localLinearLayout2;
    if (!localg.B())
    {
      this.k.setVisibility(8);
      this.n.setVisibility(8);
      if (!localg.n())
      {
        this.c.setVisibility(8);
        this.n.setVisibility(8);
      }
      if (!localg.G())
      {
        this.a.setVisibility(8);
        this.l.setVisibility(8);
      }
      if (!localg.H())
      {
        this.b.setVisibility(8);
        this.m.setVisibility(8);
      }
      if (!localg.I())
      {
        this.d.setVisibility(8);
        this.o.setVisibility(8);
      }
      if (this.a.getVisibility() == 8)
        break label435;
      localLinearLayout1 = this.a;
      if (this.d.getVisibility() == 8)
        break label503;
      localLinearLayout2 = this.d;
      label355: if (localLinearLayout1 != localLinearLayout2)
        break label575;
      a(localLinearLayout1, 2130837715);
    }
    while (true)
    {
      a(2131296606, a(TransferHomeActivity.class));
      a(2131296607, a(BillPayHomeActivity.class));
      a(2131296613, a(EPayHomeActivity.class));
      a(2131296615, a(WireHomeActivity.class));
      return;
      this.k.setVisibility(0);
      this.n.setVisibility(0);
      break;
      label435: if (this.b.getVisibility() != 8)
      {
        localLinearLayout1 = this.b;
        break label337;
      }
      if (this.k.getVisibility() != 8)
      {
        localLinearLayout1 = this.k;
        break label337;
      }
      if (this.c.getVisibility() != 8)
      {
        localLinearLayout1 = this.c;
        break label337;
      }
      localLinearLayout1 = this.d;
      break label337;
      label503: if (this.c.getVisibility() != 8)
      {
        localLinearLayout2 = this.c;
        break label355;
      }
      if (this.k.getVisibility() != 8)
      {
        localLinearLayout2 = this.k;
        break label355;
      }
      if (this.b.getVisibility() != 8)
      {
        localLinearLayout2 = this.b;
        break label355;
      }
      localLinearLayout2 = this.a;
      break label355;
      label575: a(localLinearLayout1, 2130837714);
      a(localLinearLayout2, 2130837713);
    }
  }

  public final void d()
  {
    if ((r().c != null) && (r().c.d() > 0))
      ((TextView)findViewById(2131296611)).setText("(" + r().c.d() + ")");
    this.k.setFocusable(true);
    this.k.setClickable(true);
    this.k.setOnClickListener(new fn(this));
  }

  protected void onResume()
  {
    super.onResume();
    if (r().c == null)
    {
      Animation localAnimation = AnimationUtils.loadAnimation(this, 2130968580);
      findViewById(2131296475).startAnimation(localAnimation);
      Integer[] arrayOfInteger = new Integer[1];
      arrayOfInteger[0] = Integer.valueOf(1);
      a(a.class, arrayOfInteger);
      return;
    }
    d();
    e();
  }

  protected void onStop()
  {
    super.onStop();
    ((a)this.e.a(a.class)).c();
  }

  public static class a extends ae<PaymentsAndTransfersHomeActivity, Integer, Void, TodoListResponse>
  {
    public final boolean e()
    {
      return false;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.PaymentsAndTransfersHomeActivity
 * JD-Core Version:    0.6.2
 */