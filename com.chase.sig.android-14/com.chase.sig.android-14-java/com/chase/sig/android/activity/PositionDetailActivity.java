package com.chase.sig.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import com.chase.sig.android.activity.a.f;
import com.chase.sig.android.domain.Position;
import com.chase.sig.android.util.Dollar;
import com.chase.sig.android.view.detail.DetailRow;
import com.chase.sig.android.view.detail.DetailRowWithSubItem;
import com.chase.sig.android.view.detail.DetailView;
import com.chase.sig.android.view.detail.a;
import com.chase.sig.android.view.detail.u;

public class PositionDetailActivity extends ai
  implements fk.d
{
  Position a;

  public final void a(Bundle paramBundle)
  {
    boolean bool1 = true;
    setTitle(2131165852);
    b(2130903142);
    Button localButton = (Button)findViewById(2131296435);
    this.a = ((Position)getIntent().getExtras().get("transaction_object"));
    boolean bool2 = this.a.h();
    String str1 = com.chase.sig.android.util.s.w(this.a.m());
    fo localfo = new fo(this);
    DetailView localDetailView = (DetailView)findViewById(2131296271);
    localDetailView.d();
    String str4;
    a[] arrayOfa;
    boolean bool3;
    label228: String str2;
    label271: boolean bool4;
    label297: boolean bool5;
    label349: String str3;
    label422: boolean bool6;
    label461: boolean bool7;
    label511: boolean bool8;
    label558: boolean bool9;
    label609: boolean bool10;
    label665: DetailRow localDetailRow6;
    if (this.a.t())
    {
      TextView localTextView = (TextView)findViewById(2131296495);
      StringBuilder localStringBuilder = new StringBuilder().append(this.a.b());
      if (this.a.u())
      {
        str4 = " (" + this.a.i() + ")";
        localTextView.setText(str4);
      }
    }
    else
    {
      arrayOfa = new a[9];
      DetailRow localDetailRow1 = new DetailRow("Value", com.chase.sig.android.util.s.a(this.a.a().h()));
      if (this.a.w())
        break label758;
      bool3 = bool1;
      localDetailRow1.j = bool3;
      arrayOfa[0] = localDetailRow1;
      if (!com.chase.sig.android.util.s.m(this.a.f()))
        break label764;
      str2 = com.chase.sig.android.util.s.a(Double.valueOf(this.a.f()), "#,##0.00");
      DetailRow localDetailRow2 = new DetailRow("Quantity", str2);
      if (this.a.s())
        break label772;
      bool4 = bool1;
      localDetailRow2.j = bool4;
      arrayOfa[bool1] = localDetailRow2;
      DetailRow localDetailRow3 = new DetailRow(this.a.j(), com.chase.sig.android.util.s.a(this.a.i()));
      if (this.a.u())
        break label778;
      bool5 = bool1;
      localDetailRow3.j = bool5;
      DetailRow localDetailRow4 = (DetailRow)localDetailRow3;
      localDetailRow4.m = bool2;
      if (bool2)
        localDetailRow4.h = localfo;
      arrayOfa[2] = localDetailRow4;
      if (!this.a.r())
        break label784;
      str3 = "(as of " + str1 + ")";
      DetailRowWithSubItem localDetailRowWithSubItem = new DetailRowWithSubItem("Price", str3, com.chase.sig.android.util.s.a(this.a.c().h()));
      if (this.a.q())
        break label792;
      bool6 = bool1;
      localDetailRowWithSubItem.j = bool6;
      arrayOfa[3] = localDetailRowWithSubItem;
      DetailRow localDetailRow5 = new DetailRow("Cost", com.chase.sig.android.util.s.a(this.a.e().h()));
      if (this.a.o())
        break label798;
      bool7 = bool1;
      localDetailRow5.j = bool7;
      arrayOfa[4] = localDetailRow5;
      u localu1 = new u("Gain/Loss", this.a.d().h());
      if (this.a.v())
        break label804;
      bool8 = bool1;
      localu1.j = bool8;
      arrayOfa[5] = localu1;
      u localu2 = new u(getString(2131165876), this.a.x().h());
      if (this.a.x() != null)
        break label810;
      bool9 = bool1;
      localu2.j = bool9;
      arrayOfa[6] = localu2;
      com.chase.sig.android.view.detail.s locals = new com.chase.sig.android.view.detail.s(this.a.g());
      if ((this.a.p()) && (!com.chase.sig.android.util.s.l(this.a.g())))
        break label816;
      bool10 = bool1;
      locals.j = bool10;
      arrayOfa[7] = locals;
      localDetailRow6 = new DetailRow("Asset class", this.a.k());
      if (this.a.n())
        break label822;
    }
    while (true)
    {
      localDetailRow6.j = bool1;
      arrayOfa[8] = localDetailRow6;
      localDetailView.setRows(arrayOfa);
      localButton.setOnClickListener(a(PrivateBankingDisclosuresActivity.class).a("title", localButton.getText().toString()));
      return;
      str4 = "";
      break;
      label758: bool3 = false;
      break label228;
      label764: str2 = "--";
      break label271;
      label772: bool4 = false;
      break label297;
      label778: bool5 = false;
      break label349;
      label784: str3 = "(as of )";
      break label422;
      label792: bool6 = false;
      break label461;
      label798: bool7 = false;
      break label511;
      label804: bool8 = false;
      break label558;
      label810: bool9 = false;
      break label609;
      label816: bool10 = false;
      break label665;
      label822: bool1 = false;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.PositionDetailActivity
 * JD-Core Version:    0.6.2
 */