package com.chase.sig.android.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.d;
import com.chase.sig.android.domain.Position;
import com.chase.sig.android.domain.PositionType;
import com.chase.sig.android.service.PositionResponse;
import com.chase.sig.android.service.PositionsAccount;
import com.chase.sig.android.util.Dollar;
import com.chase.sig.android.util.e;
import com.chase.sig.android.util.s;
import com.chase.sig.android.view.TickerValueTextView;
import com.chase.sig.android.view.w;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class PositionsActivity extends ai
  implements fk.d
{
  private ListView a;
  private PositionResponse b;
  private View c;
  private View d;
  private View k;
  private boolean l = false;
  private Button m;

  private void a(Map<PositionType, ? extends Set<Position>> paramMap)
  {
    fp localfp = new fp(this);
    Iterator localIterator = paramMap.keySet().iterator();
    while (localIterator.hasNext())
    {
      PositionType localPositionType = (PositionType)localIterator.next();
      if (!((Set)paramMap.get(localPositionType)).isEmpty())
        localfp.a(localPositionType.a().toUpperCase(), new a(this, (Set)paramMap.get(localPositionType)));
    }
    this.a.setAdapter(localfp);
    this.a.setOnItemClickListener(new b((byte)0));
  }

  public final void a(Bundle paramBundle)
  {
    int i = 8;
    setTitle(2131165847);
    b(2130903144);
    Bundle localBundle = getIntent().getExtras();
    this.a = ((ListView)findViewById(2131296636));
    this.c = findViewById(2131296624);
    this.d = findViewById(2131296625);
    this.k = findViewById(2131296626);
    this.m = ((Button)findViewById(2131296399));
    boolean bool;
    int j;
    label118: int n;
    label141: label161: int i1;
    if (paramBundle != null)
    {
      if (paramBundle.getInt("account_summary_state", 0) != 0)
      {
        bool = true;
        this.l = bool;
      }
    }
    else
    {
      View localView1 = this.c;
      if (!this.l)
        break label267;
      j = i;
      localView1.setVisibility(j);
      View localView2 = this.k;
      if (!this.l)
        break label273;
      n = i;
      localView2.setVisibility(n);
      View localView3 = this.d;
      if (!this.l)
        break label279;
      localView3.setVisibility(i);
      Button localButton = this.m;
      if (!this.l)
        break label284;
      i1 = 2130837741;
      label184: localButton.setBackgroundResource(i1);
      a(this.a);
      if (!e.a(paramBundle, "response"))
        break label291;
      this.b = ((PositionResponse)paramBundle.getSerializable("response"));
      a(this.b, true);
    }
    while (true)
    {
      fq localfq = new fq(this);
      findViewById(2131296634).setOnClickListener(localfq);
      this.m.setOnClickListener(localfq);
      return;
      bool = false;
      break;
      label267: j = 0;
      break label118;
      label273: n = 0;
      break label141;
      label279: i = 0;
      break label161;
      label284: i1 = 2130837742;
      break label184;
      label291: if (!e.a(paramBundle, "simple_dialogs"))
        a(c.class, new String[] { localBundle.getString("selectedAccountId") });
    }
  }

  public final void a(PositionResponse paramPositionResponse, boolean paramBoolean)
  {
    if ((!paramPositionResponse.c()) && (!paramBoolean))
      g(2131165820);
    while ((paramPositionResponse.a() == null) || (paramPositionResponse.a().size() <= 0))
      return;
    PositionsAccount localPositionsAccount = (PositionsAccount)paramPositionResponse.a().get(0);
    ((TextView)findViewById(2131296385)).setText(localPositionsAccount.b());
    ((TextView)findViewById(2131296628)).setText(new Dollar(localPositionsAccount.e()).toString());
    ((TickerValueTextView)findViewById(2131296630)).setTickerValue(new Dollar(localPositionsAccount.d()));
    ((TickerValueTextView)findViewById(2131296633)).setTickerValue(localPositionsAccount.g());
    ((TextView)findViewById(2131296625)).setText("As of " + s.w(localPositionsAccount.c()));
    a(paramPositionResponse.b());
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    if (this.b != null)
      paramBundle.putSerializable("response", this.b);
    paramBundle.putInt("account_summary_state", this.k.getVisibility());
  }

  public final class a extends ArrayAdapter<Position>
  {
    private final List<Position> b;

    public a(Set<Position> arg2)
    {
      this(localContext, localSet, (byte)0);
    }

    private a(int paramByte1, byte paramByte2, byte arg4)
    {
      super(2130903143);
      this.b = new ArrayList(paramByte2);
    }

    public final int getCount()
    {
      return this.b.size();
    }

    public final long getItemId(int paramInt)
    {
      return paramInt;
    }

    public final View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      if (paramView == null)
        paramView = PositionsActivity.this.getLayoutInflater().inflate(2130903143, null);
      Position localPosition = (Position)this.b.get(paramInt);
      String str1;
      String str2;
      label72: String str3;
      label93: String str5;
      label140: TickerValueTextView localTickerValueTextView;
      if (localPosition.t())
      {
        str1 = s.a(localPosition.b());
        if (!localPosition.q())
          break label244;
        str2 = s.a(localPosition.c().h());
        if (!localPosition.w())
          break label251;
        str3 = s.a(localPosition.a().h());
        String str4 = localPosition.i();
        if ((!localPosition.u()) || (!s.m(str4)))
          break label258;
        str5 = " (" + str4 + ")";
        ((TextView)paramView.findViewById(2131296618)).setText(str1 + str5);
        ((TextView)paramView.findViewById(2131296620)).setText(str2);
        localTickerValueTextView = (TickerValueTextView)paramView.findViewById(2131296621);
        if (!localPosition.v())
          break label265;
        localTickerValueTextView.setDisplayPlaceholder(true);
        localTickerValueTextView.setTickerValue(localPosition.d());
      }
      while (true)
      {
        ((TextView)paramView.findViewById(2131296622)).setText(str3);
        return paramView;
        str1 = "--";
        break;
        label244: str2 = "--";
        break label72;
        label251: str3 = "--";
        break label93;
        label258: str5 = "--";
        break label140;
        label265: localTickerValueTextView.setVisibility(8);
      }
    }
  }

  private final class b
    implements AdapterView.OnItemClickListener
  {
    private b()
    {
    }

    public final void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
    {
      try
      {
        Position localPosition = (Position)paramAdapterView.getItemAtPosition(paramInt);
        Intent localIntent = new Intent(PositionsActivity.this, PositionDetailActivity.class);
        localIntent.putExtra("transaction_object", localPosition);
        PositionsActivity.this.startActivity(localIntent);
        BehaviorAnalyticsAspect.a();
        BehaviorAnalyticsAspect.a(paramAdapterView, paramView, paramInt);
        return;
      }
      catch (Throwable localThrowable)
      {
        BehaviorAnalyticsAspect.a();
        BehaviorAnalyticsAspect.a(paramAdapterView, paramView, paramInt);
        throw localThrowable;
      }
    }
  }

  public static class c extends d<PositionsActivity, String, Void, PositionResponse>
  {
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.PositionsActivity
 * JD-Core Version:    0.6.2
 */