package com.chase.sig.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;
import com.chase.sig.android.d;
import com.chase.sig.android.service.content.ContentResponse;

public class PrivacyNoticeDetailActivity extends eb
{
  private static int b;
  private final String[] a = { "JPMprivacyPolicy.txt", "JPMSecuritiesprivacyPolicyNative.txt", "privacyPolicyNative.txt", "JPMCCprivacyPolicyNative.txt" };
  private WebView c;
  private String d;

  private void a(String paramString)
  {
    this.d = paramString;
    g();
    this.c = ((WebView)findViewById(2131296605));
    this.c.loadData(this.d, "text/html", "utf-8");
    fu localfu = new fu(this);
    this.c.setWebViewClient(localfu);
    Button localButton1 = (Button)findViewById(2131296603);
    Button localButton2 = (Button)findViewById(2131296602);
    localButton2.setEnabled(true);
    localButton1.setEnabled(true);
    if (b >= -1 + this.a.length)
      localButton2.setEnabled(false);
    while (b > 0)
      return;
    localButton1.setEnabled(false);
  }

  private void g()
  {
    TextView localTextView = (TextView)findViewById(2131296604);
    Object[] arrayOfObject = new Object[1];
    String str;
    switch (b)
    {
    default:
      str = null;
    case 0:
    case 1:
    case 2:
    }
    while (true)
    {
      arrayOfObject[0] = str;
      localTextView.setText(String.format("*%s", arrayOfObject));
      return;
      str = getString(2131165837);
      continue;
      str = getString(2131165838);
      continue;
      str = getString(2131165839);
    }
  }

  private void h()
  {
    a("");
    g();
    String[] arrayOfString = new String[1];
    arrayOfString[0] = this.a[b];
    a(a.class, arrayOfString);
  }

  public final void a(Bundle paramBundle)
  {
    setTitle(2131165835);
    b(2130903138);
    if ((paramBundle != null) && (paramBundle.containsKey("document_index")))
    {
      b = paramBundle.getInt("document_index");
      this.d = paramBundle.getString("privacyNoticeData");
      a(this.d);
    }
    while (true)
    {
      a(2131296602, new fs(this));
      a(2131296603, new ft(this));
      return;
      b = getIntent().getExtras().getInt("document_index");
      h();
    }
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    paramBundle.putString("privacyNoticeData", this.d);
    paramBundle.putInt("document_index", b);
  }

  public static class a extends d<PrivacyNoticeDetailActivity, String, Void, ContentResponse>
  {
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.PrivacyNoticeDetailActivity
 * JD-Core Version:    0.6.2
 */