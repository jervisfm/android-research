package com.chase.sig.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.SimpleAdapter;
import com.chase.sig.android.util.e;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class PrivacyNoticesActivity extends eb
{
  private ListView a;
  private String b;

  public final void a(Bundle paramBundle)
  {
    b(2130903091);
    this.a = ((ListView)findViewById(2131296440));
    RelativeLayout.LayoutParams localLayoutParams = new RelativeLayout.LayoutParams(-1, -2);
    this.a.setLayoutParams(localLayoutParams);
    findViewById(2131296439).setBackgroundResource(17170444);
    this.b = ((String)e.a(getIntent().getExtras(), "title", null));
    if (this.b != null)
      setTitle(this.b);
    while (true)
    {
      ArrayList localArrayList = new ArrayList();
      this.a.setVisibility(0);
      HashMap localHashMap1 = new HashMap();
      localHashMap1.put("title", getString(2131165837));
      localArrayList.add(localHashMap1);
      HashMap localHashMap2 = new HashMap();
      localHashMap2.put("title", getString(2131165838));
      localArrayList.add(localHashMap2);
      HashMap localHashMap3 = new HashMap();
      localHashMap3.put("title", getString(2131165839));
      localArrayList.add(localHashMap3);
      SimpleAdapter localSimpleAdapter = new SimpleAdapter(this, localArrayList, 2130903093, new String[] { "title" }, new int[] { 2131296444 });
      this.a.setAdapter(localSimpleAdapter);
      this.a.setOnItemClickListener(new fv(this));
      return;
      setTitle(2131165836);
    }
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.PrivacyNoticesActivity
 * JD-Core Version:    0.6.2
 */