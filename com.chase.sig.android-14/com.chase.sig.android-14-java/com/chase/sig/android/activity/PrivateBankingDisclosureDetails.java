package com.chase.sig.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;
import android.widget.TextView;
import com.chase.sig.android.d;
import com.chase.sig.android.service.content.ContentResponse;

public class PrivateBankingDisclosureDetails extends eb
{
  TextView a;
  private String b;
  private int c = -1;
  private WebView d;
  private final String[] k = { "jpm_gen_disc.html", "jpm_invtacct_disc.html", "jpm_bnkacct_disc.html", "jpm_credacct_disc.html" };

  private void a(String paramString)
  {
    this.b = paramString;
    d();
    this.d = ((WebView)findViewById(2131296438));
    this.d.loadData(this.b, "text/html", "utf-8");
    fw localfw = new fw(this);
    this.d.setWebViewClient(localfw);
    d();
  }

  private void d()
  {
    this.a = ((TextView)findViewById(2131296437));
    TextView localTextView = this.a;
    Object[] arrayOfObject = new Object[1];
    String str;
    switch (this.c)
    {
    default:
      str = null;
    case 0:
    case 1:
    case 2:
    case 3:
    }
    while (true)
    {
      arrayOfObject[0] = str;
      localTextView.setText(String.format("*%s", arrayOfObject));
      return;
      str = getString(2131165840);
      continue;
      str = getString(2131165841);
      continue;
      str = getString(2131165842);
      continue;
      str = getString(2131165843);
    }
  }

  public final void a(Bundle paramBundle)
  {
    setTitle(2131165833);
    b(2130903090);
    this.a = ((TextView)findViewById(2131296437));
    if (paramBundle != null)
    {
      this.b = paramBundle.getString("disclosureData");
      this.c = getIntent().getExtras().getInt("disclosure_document_index");
      a(this.b);
      return;
    }
    this.c = getIntent().getExtras().getInt("disclosure_document_index");
    String[] arrayOfString = new String[1];
    arrayOfString[0] = this.k[this.c];
    a(a.class, arrayOfString);
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    paramBundle.putString("disclosureData", this.b);
    paramBundle.putInt("disclosure_document_index", this.c);
  }

  public static class a extends d<PrivateBankingDisclosureDetails, String, Void, ContentResponse>
  {
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.PrivateBankingDisclosureDetails
 * JD-Core Version:    0.6.2
 */