package com.chase.sig.android.activity;

import android.content.Intent;
import android.os.AsyncTask.Status;
import android.os.Bundle;
import android.webkit.WebView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import com.chase.sig.android.d;
import com.chase.sig.android.service.content.ContentResponse;
import com.chase.sig.android.util.e;
import com.chase.sig.android.util.s;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class PrivateBankingDisclosuresActivity extends ai
{
  private ListView a;
  private String b = "";
  private WebView c;
  private String d;
  private SimpleAdapter k;

  public final void a(Bundle paramBundle)
  {
    b(2130903091);
    this.a = ((ListView)findViewById(2131296440));
    this.b = ((String)e.a(paramBundle, "footNote", null));
    if (this.b == null)
      this.b = "";
    this.d = ((String)e.a(getIntent().getExtras(), "title", null));
    int i;
    if (this.d != null)
    {
      setTitle(this.d);
      ArrayList localArrayList = new ArrayList();
      this.a.setVisibility(0);
      HashMap localHashMap1 = new HashMap();
      localHashMap1.put("title", getString(2131165840));
      localArrayList.add(localHashMap1);
      HashMap localHashMap2 = new HashMap();
      localHashMap2.put("title", getString(2131165841));
      localArrayList.add(localHashMap2);
      HashMap localHashMap3 = new HashMap();
      localHashMap3.put("title", getString(2131165842));
      localArrayList.add(localHashMap3);
      HashMap localHashMap4 = new HashMap();
      localHashMap4.put("title", getString(2131165843));
      localArrayList.add(localHashMap4);
      HashMap localHashMap5 = new HashMap();
      localHashMap5.put("title", "");
      localArrayList.add(localHashMap5);
      HashMap localHashMap6 = new HashMap();
      localHashMap6.put("title", getString(2131165836));
      localArrayList.add(localHashMap6);
      HashMap localHashMap7 = new HashMap();
      localHashMap7.put("title", "");
      localArrayList.add(localHashMap7);
      this.c = new WebView(this);
      this.c.loadData(this.b, "text/html", "utf-8");
      this.a.addFooterView(this.c);
      fy localfy = new fy(this);
      this.c.setWebViewClient(localfy);
      this.k = new fz(this, this, localArrayList, new String[] { "title" }, new int[] { 2131296444 });
      this.a.setAdapter(this.k);
      a locala = (a)this.e.a(a.class);
      if (locala.getStatus() == AsyncTask.Status.RUNNING)
        break label491;
      i = 1;
      label437: if ((!s.l(this.b)) || (paramBundle != null) || (i == 0))
        break label497;
      locala.execute(new Void[0]);
    }
    while (true)
    {
      this.a.setOnItemClickListener(new fx(this));
      return;
      setTitle(2131165833);
      break;
      label491: i = 0;
      break label437;
      label497: if (i != 0)
        a(this.b);
    }
  }

  public final void a(String paramString)
  {
    this.b = paramString;
    if ((this.c != null) && (s.m(this.b)))
      this.c.loadData(this.b, "text/html", "utf-8");
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    paramBundle.putString("footNote", this.b);
  }

  public static class a extends d<PrivateBankingDisclosuresActivity, Void, Void, ContentResponse>
  {
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.PrivateBankingDisclosuresActivity
 * JD-Core Version:    0.6.2
 */