package com.chase.sig.android.activity;

import android.content.Intent;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.ShutterCallback;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

public class QuickDepositCaptureActivity extends ai
  implements fk.a
{
  Camera.ShutterCallback a = new gd(this);
  Camera.PictureCallback b = new ge(this);
  Camera.PictureCallback c = new gf(this);
  private String d;
  private ImageView k;

  private void d()
  {
    TextView localTextView = (TextView)findViewById(2131296308);
    if (this.d.equals("qd_check_front_image"))
    {
      localTextView.setText(2131165646);
      return;
    }
    localTextView.setText(2131165647);
  }

  public final void a(Bundle paramBundle)
  {
    CapturePreview localCapturePreview = new CapturePreview(this);
    ((FrameLayout)findViewById(2131296306)).addView(localCapturePreview);
    Button localButton = (Button)findViewById(2131296310);
    localButton.setVisibility(0);
    localButton.setOnClickListener(new ga(this, localButton));
    this.k = ((ImageView)findViewById(2131296316));
    this.k.setVisibility(0);
    this.k.setOnClickListener(new gb(this, localCapturePreview));
  }

  protected final void a_()
  {
    setContentView(2130903150);
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if (paramInt == 27)
    {
      if (paramKeyEvent.getRepeatCount() == 0)
        this.k.performClick();
      return true;
    }
    return super.onKeyDown(paramInt, paramKeyEvent);
  }

  protected void onResume()
  {
    super.onResume();
    findViewById(2131296312).setVisibility(8);
    findViewById(2131296311).setVisibility(8);
    this.d = getIntent().getExtras().getString("qd_image_side");
    d();
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.QuickDepositCaptureActivity
 * JD-Core Version:    0.6.2
 */