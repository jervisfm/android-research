package com.chase.sig.android.activity;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.chase.sig.android.activity.a.f;
import com.chase.sig.android.c;
import com.chase.sig.android.domain.QuickDeposit;
import com.chase.sig.android.domain.QuickDepositAccount;
import com.chase.sig.android.util.Dollar;
import com.chase.sig.android.util.s;

public class QuickDepositCompleteActivity extends ai
  implements fk.a
{
  private QuickDeposit a;

  public final void a(Bundle paramBundle)
  {
    b(2130903151);
    setTitle(2131165300);
    c.a();
    this.a = QuickDeposit.a(getIntent());
    ((ImageView)findViewById(2131296656)).setImageBitmap(this.a.g());
    ((ImageView)findViewById(2131296658)).setImageBitmap(this.a.e());
    ((TextView)findViewById(2131296653)).setText(this.a.i().h());
    TextView localTextView = (TextView)findViewById(2131296652);
    if (s.m(this.a.m()))
      localTextView.setText(this.a.n());
    while (true)
    {
      ViewGroup localViewGroup = (ViewGroup)findViewById(2131296650);
      QuickDepositAccount localQuickDepositAccount = this.a.o();
      localViewGroup.addView(new com.chase.sig.android.view.a(localViewGroup.getContext(), "Deposit to", localQuickDepositAccount.b(), localQuickDepositAccount.d()).getView());
      ((TextView)findViewById(2131296654)).setText(s.b());
      a(2131296660, a(QuickDepositStartActivity.class).b());
      a(2131296659, new gg(this));
      return;
      ((TextView)findViewById(2131296651)).setVisibility(8);
      localTextView.setVisibility(8);
    }
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if (j(paramInt))
      return true;
    com.chase.sig.android.a.a.a(paramInt, this, AccountsActivity.class);
    return super.onKeyDown(paramInt, paramKeyEvent);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.QuickDepositCompleteActivity
 * JD-Core Version:    0.6.2
 */