package com.chase.sig.android.activity;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.Html;
import android.widget.TextView;
import com.chase.sig.android.domain.IAccount;
import com.chase.sig.android.domain.QuickDeposit;
import com.chase.sig.android.domain.QuickDepositAccount;
import com.chase.sig.android.domain.g;
import com.chase.sig.android.domain.o;
import com.chase.sig.android.util.Dollar;
import com.chase.sig.android.util.s;

public class QuickDepositReceiptActivity extends ai
  implements fk.a
{
  private QuickDeposit a;
  private IAccount b;

  public final void a(Bundle paramBundle)
  {
    b(2130903152);
    setTitle(2131165650);
    TextView localTextView1 = (TextView)findViewById(2131296662);
    TextView localTextView2 = (TextView)findViewById(2131296666);
    TextView localTextView3 = (TextView)findViewById(2131296664);
    TextView localTextView4 = (TextView)findViewById(2131296663);
    ((TextView)findViewById(2131296669)).setText(Html.fromHtml("Available balance<sup><small>1</small></sup>"));
    TextView localTextView5 = (TextView)findViewById(2131296670);
    TextView localTextView6 = (TextView)findViewById(2131296672);
    ((TextView)findViewById(2131296671)).setText(Html.fromHtml("Present balance<sup><small>2</small></sup>"));
    TextView localTextView7 = (TextView)findViewById(2131296668);
    if (paramBundle == null)
      if ((getIntent().getExtras() == null) || (getIntent().getExtras().get("quick_deposit") == null));
    for (this.a = ((QuickDeposit)getIntent().getExtras().get("quick_deposit")); ; this.a = ((QuickDeposit)paramBundle.get("deposit")))
    {
      if (this.a != null)
      {
        QuickDepositAccount localQuickDepositAccount = this.a.o();
        if (localQuickDepositAccount != null)
        {
          localTextView1.setText(localQuickDepositAccount.d());
          localTextView2.setText(this.a.i().h());
          if (s.l(this.a.m()))
          {
            localTextView3.setVisibility(8);
            localTextView4.setVisibility(8);
          }
          String str = this.a.o().a();
          IAccount localIAccount = r().b.a(str);
          if (localIAccount != null)
          {
            localTextView6.setText(localIAccount.k().h());
            localTextView5.setText(localIAccount.j().h());
          }
          localTextView3.setText(this.a.n());
          localTextView7.setText(s.a());
        }
      }
      ((TextView)findViewById(2131296674)).setText(Html.fromHtml("<sup><small>1</small></sup>" + getResources().getString(2131165664)));
      ((TextView)findViewById(2131296675)).setText(Html.fromHtml("<sup><small>2</small></sup>" + getResources().getString(2131165665)));
      return;
    }
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    paramBundle.putSerializable("deposit", this.a);
    paramBundle.putSerializable("account_deposited_into", this.b);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.QuickDepositReceiptActivity
 * JD-Core Version:    0.6.2
 */