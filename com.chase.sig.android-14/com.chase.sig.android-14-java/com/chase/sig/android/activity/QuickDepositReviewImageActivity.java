package com.chase.sig.android.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.chase.sig.android.domain.QuickDeposit;
import com.chase.sig.android.domain.QuickDepositAccount;
import com.chase.sig.android.util.Dollar;

public class QuickDepositReviewImageActivity extends eb
  implements fk.a
{
  GestureDetector.OnGestureListener a = new gk(this);
  private a b;
  private String c;
  private boolean d;
  private boolean k = false;

  private static Bitmap a(byte[] paramArrayOfByte)
  {
    try
    {
      BitmapFactory.Options localOptions = new BitmapFactory.Options();
      localOptions.inJustDecodeBounds = true;
      BitmapFactory.decodeByteArray(paramArrayOfByte, 0, paramArrayOfByte.length, localOptions);
      if ((!localOptions.mCancel) && (localOptions.outWidth != -1))
      {
        if (localOptions.outHeight == -1)
          return null;
        localOptions.inJustDecodeBounds = false;
        localOptions.inDither = false;
        localOptions.inPurgeable = true;
        localOptions.inPreferredConfig = Bitmap.Config.RGB_565;
        Bitmap localBitmap = BitmapFactory.decodeByteArray(paramArrayOfByte, 0, paramArrayOfByte.length, localOptions);
        return localBitmap;
      }
    }
    catch (OutOfMemoryError localOutOfMemoryError)
    {
    }
    return null;
  }

  public final void a(Bundle paramBundle)
  {
    setContentView(2130903150);
    findViewById(2131296315).setVisibility(4);
    FrameLayout localFrameLayout = (FrameLayout)findViewById(2131296305);
    GestureDetector localGestureDetector = new GestureDetector(this.a);
    this.b = new a(this);
    this.b.a(true);
    FrameLayout.LayoutParams localLayoutParams = new FrameLayout.LayoutParams(-1, -1);
    this.b.setLayoutParams(localLayoutParams);
    this.b.setOnTouchListener(new gh(this, localGestureDetector));
    localFrameLayout.addView(this.b, 0);
  }

  protected void onDestroy()
  {
    super.onDestroy();
    this.b.a();
    this.b = null;
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if (j(paramInt))
      return true;
    Bundle localBundle = getIntent().getExtras();
    boolean bool = false;
    if (localBundle != null)
      bool = localBundle.getBoolean("qd_review_from_capture_image");
    if ((bool) && (paramInt == 4))
    {
      Intent localIntent = new Intent(this, QuickDepositCaptureActivity.class);
      localIntent.putExtra("qd_image_side", this.c);
      localIntent.setFlags(1073741824);
      startActivity(localIntent);
      return true;
    }
    return super.onKeyDown(paramInt, paramKeyEvent);
  }

  protected void onResume()
  {
    super.onResume();
    this.k = getIntent().getExtras().getBoolean("qd_review_only_mode");
    QuickDeposit localQuickDeposit = QuickDeposit.a(getIntent());
    Bitmap localBitmap = a(getIntent().getExtras().getByteArray("image_data"));
    if (this.k)
    {
      ((TextView)findViewById(2131296308)).setText(2131165649);
      ((TextView)findViewById(2131296649)).setText(2131165642);
      LinearLayout localLinearLayout = (LinearLayout)findViewById(2131296287);
      localLinearLayout.setOrientation(1);
      localLinearLayout.setVisibility(0);
      findViewById(2131296310).setVisibility(8);
      findViewById(2131296312).setVisibility(8);
      findViewById(2131296311).setVisibility(8);
      QuickDepositAccount localQuickDepositAccount = localQuickDeposit.o();
      TextView localTextView1 = (TextView)findViewById(2131296646);
      localTextView1.setVisibility(0);
      String str = localQuickDepositAccount.b();
      if (str.length() > 16)
        str = str.substring(0, 16) + "...";
      localTextView1.setText(str);
      TextView localTextView2 = (TextView)findViewById(2131296647);
      localTextView2.setVisibility(0);
      Object[] arrayOfObject = new Object[1];
      arrayOfObject[0] = localQuickDepositAccount.d();
      localTextView2.setText(String.format("(%s)", arrayOfObject));
      TextView localTextView3 = (TextView)findViewById(2131296648);
      localTextView3.setVisibility(0);
      localTextView3.setText(localQuickDeposit.i().h());
    }
    while (true)
    {
      this.b.a(localBitmap);
      return;
      findViewById(2131296287).setVisibility(0);
      ((TextView)findViewById(2131296649)).setText(2131165641);
      Button localButton1 = (Button)findViewById(2131296311);
      localButton1.setVisibility(0);
      localButton1.setOnClickListener(new gi(this));
      Button localButton2 = (Button)findViewById(2131296312);
      localButton2.setVisibility(0);
      localButton2.setOnClickListener(new gj(this));
      this.c = getIntent().getExtras().getString("qd_image_side");
    }
  }

  private static final class a extends View
  {
    boolean a;
    private Bitmap b;
    private Paint c = new Paint();
    private int d = 0;
    private int e = 0;
    private int f;
    private int g;
    private int h;
    private int i;

    public a(Context paramContext)
    {
      super();
    }

    public final void a()
    {
      this.b.recycle();
      this.b = null;
    }

    public final void a(float paramFloat1, float paramFloat2)
    {
      if (Math.abs(paramFloat1) > 4.0F)
        this.d = ((int)(paramFloat1 + this.d));
      if (Math.abs(paramFloat2) > 4.0F)
        this.e = ((int)(paramFloat2 + this.e));
      invalidate();
    }

    public final void a(Bitmap paramBitmap)
    {
      this.b = paramBitmap;
      this.h = paramBitmap.getWidth();
      this.i = paramBitmap.getHeight();
    }

    public final void a(boolean paramBoolean)
    {
      this.a = paramBoolean;
      invalidate();
    }

    protected final void onDraw(Canvas paramCanvas)
    {
      if ((this.b == null) || (this.b.getWidth() <= 0) || (this.b.getHeight() <= 0))
        return;
      if (this.a)
      {
        int i1 = this.b.getWidth();
        int i2 = this.b.getHeight();
        int i3 = getHeight();
        float f1 = getWidth() / i1;
        float f2 = i3 / i2;
        Matrix localMatrix = new Matrix();
        localMatrix.postScale(f1, f2);
        paramCanvas.drawBitmap(Bitmap.createBitmap(this.b, 0, 0, i1, i2, localMatrix, true), 0.0F, 0.0F, this.c);
        return;
      }
      this.f = (this.h - getWidth());
      this.g = (this.i - getHeight());
      int j;
      int k;
      label174: int m;
      if (this.f < 0)
      {
        j = 0;
        this.f = j;
        if (this.g >= 0)
          break label342;
        k = 0;
        this.g = k;
        this.d = Math.min(this.d, this.f);
        this.d = Math.max(this.d, 0);
        this.e = Math.min(this.e, this.g);
        this.e = Math.max(this.e, 0);
        if (this.h >= getWidth())
          break label350;
        m = this.h;
        label250: if (this.i >= getHeight())
          break label359;
      }
      label342: label350: label359: for (int n = this.i; ; n = getHeight())
      {
        Rect localRect1 = new Rect(this.d, this.e, m + this.d, n + this.e);
        Rect localRect2 = new Rect(0, 0, paramCanvas.getWidth(), paramCanvas.getHeight());
        paramCanvas.drawBitmap(this.b, localRect1, localRect2, this.c);
        return;
        j = this.f;
        break;
        k = this.g;
        break label174;
        m = getWidth();
        break label250;
      }
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.QuickDepositReviewImageActivity
 * JD-Core Version:    0.6.2
 */