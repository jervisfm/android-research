package com.chase.sig.android.activity;

import android.app.Dialog;
import android.content.res.Resources;
import android.os.AsyncTask.Status;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.d;
import com.chase.sig.android.service.LegalAgreementGetDetailsResponse;
import com.chase.sig.android.service.l;
import com.chase.sig.android.view.k.a;

public class QuickDepositServiceActivationActivity extends ai
  implements fk.a
{
  private static String b = "first_time_user";
  LegalAgreementGetDetailsResponse a;
  private boolean c = false;
  private boolean d = false;
  private String k = "agree_enabled";

  public final void a(Bundle paramBundle)
  {
    setTitle(2131165628);
    b(2130903153);
    if (paramBundle == null)
    {
      a locala = (a)this.e.a(a.class);
      if (locala.getStatus() != AsyncTask.Status.RUNNING)
        locala.execute(new Void[0]);
    }
    while (true)
    {
      findViewById(2131296677).setEnabled(this.d);
      WebView localWebView = (WebView)findViewById(2131296679);
      localWebView.getSettings().setSaveFormData(false);
      localWebView.setWebViewClient(new gl(this));
      localWebView.loadUrl(((ChaseApplication)getApplication()).c("documentum") + getResources().getString(2131165287));
      a(2131296678, a(AccountsActivity.class));
      a(2131296677, new gm(this));
      return;
      if (paramBundle.containsKey(this.k))
        this.d = paramBundle.getBoolean(this.k);
    }
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    if (paramInt == 0)
    {
      k.a locala1 = new k.a(this);
      k.a locala2 = locala1.b(2131165660);
      locala2.i = true;
      locala2.g = new gp(this);
      locala2.a(2131165661, new go(this)).c(2131165662, new gn(this));
      return locala1.d(-1);
    }
    return super.onCreateDialog(paramInt);
  }

  protected void onResume()
  {
    super.onResume();
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    paramBundle.putSerializable(this.k, Boolean.valueOf(findViewById(2131296677).isEnabled()));
  }

  public static class a extends d<QuickDepositServiceActivationActivity, Void, Void, LegalAgreementGetDetailsResponse>
  {
  }

  public static class b extends d<QuickDepositServiceActivationActivity, Void, Void, l>
  {
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.QuickDepositServiceActivationActivity
 * JD-Core Version:    0.6.2
 */