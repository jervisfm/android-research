package com.chase.sig.android.activity;

import android.content.Intent;
import android.os.AsyncTask.Status;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import com.chase.sig.android.b;
import com.chase.sig.android.c;
import com.chase.sig.android.domain.QuickDeposit;
import com.chase.sig.android.domain.QuickDepositAccount;
import com.chase.sig.android.domain.QuickDepositAccount.Authorization;
import com.chase.sig.android.service.quickdeposit.QuickDepositAccountsResponse;
import com.chase.sig.android.service.quickdeposit.QuickDepositStartResponse;
import com.chase.sig.android.service.quickdeposit.QuickDepositVerifyResponse;
import com.chase.sig.android.util.Dollar;
import com.chase.sig.android.util.a;
import com.chase.sig.android.view.AmountView;
import com.chase.sig.android.view.JPSpinner;
import java.util.List;

public class QuickDepositStartActivity extends ai
  implements fk.a
{
  private View.OnClickListener A = new gw(this);
  private View.OnClickListener B = new gy(this);
  private View.OnFocusChangeListener C = new gz(this);
  int a = -1;
  private boolean b;
  private SimpleAdapter c;
  private ListAdapter d;
  private TextView k;
  private AmountView l;
  private TextView m;
  private ImageView n;
  private ImageView o;
  private Button p;
  private Button q;
  private JPSpinner r;
  private JPSpinner s;
  private int t = 0;
  private List<QuickDepositAccount> u;
  private QuickDepositVerifyResponse v = null;
  private QuickDepositAccountsResponse w = null;
  private QuickDeposit x = null;
  private View.OnClickListener y = new gu(this);
  private View.OnClickListener z = new gv(this);

  private void a(QuickDepositVerifyResponse paramQuickDepositVerifyResponse)
  {
    if ((paramQuickDepositVerifyResponse.a()) && (paramQuickDepositVerifyResponse.m() != null))
    {
      this.m.setText("Scanned Amount: " + paramQuickDepositVerifyResponse.m().h());
      this.m.setVisibility(0);
      a(this.k);
      return;
    }
    this.k.setError(null);
    this.m.setVisibility(8);
  }

  private void a(List<QuickDepositAccount> paramList)
  {
    this.c = a.b(this, paramList);
    this.r.setAdapter(this.c);
    if ((this.c.getCount() == 1) && (((QuickDepositAccount)this.u.get(0)).e().b()))
    {
      this.r.setSelection(0);
      f();
    }
  }

  private boolean b(boolean paramBoolean)
  {
    boolean bool = true;
    if (!this.r.b())
    {
      if (paramBoolean)
        a((TextView)findViewById(2131296685));
      bool = false;
      if ((this.d != null) && (this.d.getCount() != 0))
      {
        if (this.s.b())
          break label181;
        if (paramBoolean)
          a((TextView)findViewById(2131296687));
        bool = false;
      }
    }
    while (true)
    {
      if (!this.l.a())
      {
        if (paramBoolean)
          a((TextView)findViewById(2131296689));
        bool = false;
      }
      if (!this.x.j())
      {
        if (paramBoolean)
          a((TextView)findViewById(2131296692));
        bool = false;
      }
      if (this.x.k())
        break label191;
      if (paramBoolean)
        a((TextView)findViewById(2131296695));
      return false;
      d(2131296685);
      break;
      label181: d(2131296687);
    }
    label191: return bool;
  }

  private void e()
  {
    Button localButton = (Button)findViewById(2131296699);
    if (b(false))
    {
      localButton.setBackgroundResource(2130837661);
      return;
    }
    localButton.setBackgroundResource(2130837522);
  }

  private void f()
  {
    QuickDepositAccount localQuickDepositAccount = (QuickDepositAccount)this.u.get(this.r.getSelectedItemPosition());
    this.d = a.a(this, this.u, localQuickDepositAccount.a());
    this.s.setAdapter(this.d);
    TextView localTextView = (TextView)findViewById(2131296687);
    if (this.d.getCount() == 0)
    {
      localTextView.setVisibility(8);
      this.s.setVisibility(8);
      return;
    }
    if (this.d.getCount() == 1)
      this.s.setSelection(0);
    localTextView.setVisibility(0);
    this.s.setVisibility(0);
    this.s.setClickable(true);
    this.s.setEnabled(true);
  }

  public final void a(Bundle paramBundle)
  {
    b(2130903155);
    setTitle(2131165629);
    Bundle localBundle = getIntent().getExtras();
    if (localBundle != null)
      this.b = localBundle.getBoolean("first_time_user", false);
    ((Button)findViewById(2131296699)).setOnClickListener(this.B);
    ((Button)findViewById(2131296698)).setOnClickListener(this.A);
    this.r = ((JPSpinner)findViewById(2131296686));
    this.r.a(new gr(this));
    this.s = ((JPSpinner)findViewById(2131296688));
    if (paramBundle == null)
    {
      a locala = (a)this.e.a(a.class);
      if (locala.getStatus() != AsyncTask.Status.RUNNING)
        locala.execute(new QuickDeposit[0]);
    }
    findViewById(2131296687).setVisibility(8);
    findViewById(2131296688).setVisibility(8);
    this.p = ((Button)findViewById(2131296694));
    this.p.setOnClickListener(this.y);
    this.q = ((Button)findViewById(2131296697));
    this.q.setOnClickListener(this.z);
    this.n = ((ImageView)findViewById(2131296693));
    this.n.setOnClickListener(this.z);
    this.o = ((ImageView)findViewById(2131296696));
    this.o.setOnClickListener(this.z);
    this.l = ((AmountView)findViewById(2131296690));
    this.l.addTextChangedListener(new com.chase.sig.android.util.d());
    this.k = ((TextView)findViewById(2131296689));
    this.m = ((TextView)findViewById(2131296691));
    this.l.setOnFocusChangeListener(this.C);
    QuickDepositAccountsResponse localQuickDepositAccountsResponse;
    if (paramBundle != null)
    {
      if (paramBundle.get("response") != null)
        this.v = ((QuickDepositVerifyResponse)paramBundle.getSerializable("response"));
      if (paramBundle.get("submitCounter") != null)
        this.t = paramBundle.getInt("submitCounter");
      this.r.a();
      this.s.a();
      if (paramBundle.get("qdAccountResponse") != null)
        this.w = ((QuickDepositAccountsResponse)paramBundle.getSerializable("qdAccountResponse"));
      if (this.w != null)
      {
        localQuickDepositAccountsResponse = this.w;
        if (localQuickDepositAccountsResponse != null)
          break label564;
        f(2131165363);
      }
    }
    while (true)
    {
      if (paramBundle.getInt("qd_account_spinner_selection") != -1)
        this.r.setSelection(paramBundle.getInt("qd_account_spinner_selection"));
      if (paramBundle.getInt("locations_spinner_selection") != -1)
      {
        this.s.setSelection(paramBundle.getInt("locations_spinner_selection"));
        this.s.setSelection(paramBundle.getInt("locations_spinner_selection"));
      }
      if (this.v != null)
        a(this.v);
      this.n.setOnClickListener(new gs(this));
      this.o.setOnClickListener(new gt(this));
      return;
      label564: if (localQuickDepositAccountsResponse.e())
      {
        b(localQuickDepositAccountsResponse.g());
      }
      else
      {
        this.u = localQuickDepositAccountsResponse.a();
        a(localQuickDepositAccountsResponse.a());
      }
    }
  }

  public final void a(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      this.r.a();
      this.s.a();
      findViewById(2131296687).setVisibility(8);
      this.s.setVisibility(8);
      this.a = -1;
    }
    this.l.setText("");
    this.n.setVisibility(4);
    this.o.setVisibility(4);
    this.p.setVisibility(0);
    this.q.setVisibility(0);
    this.t = 0;
    this.x = new QuickDeposit();
    getIntent().removeExtra("quick_deposit");
    y();
  }

  protected final void c_()
  {
    super.c_();
    finish();
  }

  public final int d()
  {
    return this.t;
  }

  public boolean dispatchKeyEvent(KeyEvent paramKeyEvent)
  {
    boolean bool = super.dispatchKeyEvent(paramKeyEvent);
    e();
    return bool;
  }

  public final void k(int paramInt)
  {
    this.t = paramInt;
  }

  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    if ((paramInt1 == 1001) && (paramInt2 == 10))
      a(true);
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if (j(paramInt))
      return true;
    if ((paramInt == 4) && (c.a))
    {
      c.a(this, new ha(this, paramInt));
      return true;
    }
    return super.onKeyDown(paramInt, paramKeyEvent);
  }

  protected void onNewIntent(Intent paramIntent)
  {
    setIntent(paramIntent);
  }

  protected void onResume()
  {
    super.onResume();
    int i;
    if ((getIntent().getExtras() != null) && (getIntent().getExtras().containsKey("quick_deposit")))
    {
      i = 1;
      if (i == 0)
        break label191;
      if (!c.a)
      {
        Intent localIntent = new Intent(this, AccountsActivity.class);
        localIntent.setFlags(67108864);
        c.a(localIntent);
      }
      this.x = ((QuickDeposit)getIntent().getExtras().getSerializable("quick_deposit"));
      label84: if (!this.x.j())
        break label205;
      this.n.setImageBitmap(this.x.g());
      this.n.setVisibility(0);
      this.p.setVisibility(4);
      label124: if (!this.x.k())
        break label224;
      this.o.setImageBitmap(this.x.e());
      this.o.setVisibility(0);
      this.q.setVisibility(4);
    }
    while (true)
    {
      this.k.setError(null);
      this.m.setVisibility(8);
      e();
      return;
      i = 0;
      break;
      label191: this.x = new QuickDeposit();
      break label84;
      label205: this.n.setVisibility(4);
      this.p.setVisibility(0);
      break label124;
      label224: this.o.setVisibility(4);
      this.q.setVisibility(0);
    }
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    paramBundle.putInt("qd_account_spinner_selection", this.r.getSelectedItemPosition());
    paramBundle.putInt("locations_spinner_selection", this.s.getSelectedItemPosition());
    paramBundle.putSerializable("qdAccountResponse", this.w);
    paramBundle.putSerializable("response", this.v);
    paramBundle.putInt("submitCounter", this.t);
  }

  public static class a extends b<QuickDepositStartActivity, QuickDeposit, Void, QuickDepositAccountsResponse>
  {
  }

  public static class b extends com.chase.sig.android.d<QuickDepositStartActivity, QuickDeposit, Void, QuickDepositStartResponse>
  {
    private QuickDeposit a;
  }

  public static class c extends com.chase.sig.android.d<QuickDepositStartActivity, QuickDeposit, Void, QuickDepositVerifyResponse>
  {
    private QuickDeposit a;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.QuickDepositStartActivity
 * JD-Core Version:    0.6.2
 */