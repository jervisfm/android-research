package com.chase.sig.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.chase.sig.android.d;
import com.chase.sig.android.domain.QuickDeposit;
import com.chase.sig.android.domain.QuickDepositAccount;
import com.chase.sig.android.service.quickdeposit.QuickDepositCompleteResponse;
import com.chase.sig.android.service.quickdeposit.QuickDepositVerifyResponse;
import com.chase.sig.android.util.Dollar;
import com.chase.sig.android.util.s;
import com.chase.sig.android.view.a;

public class QuickDepositVerifyActivity extends ai
  implements fk.a
{
  private QuickDeposit a;
  private EditText b;
  private EditText c;
  private TextView d;
  private TextView k;
  private Button l;

  public final void a(Bundle paramBundle)
  {
    this.a = QuickDeposit.a(getIntent());
    QuickDepositVerifyResponse localQuickDepositVerifyResponse = (QuickDepositVerifyResponse)getIntent().getExtras().get("response");
    this.a.g(localQuickDepositVerifyResponse.o());
    b(2130903156);
    setTitle(2131165630);
    ((ImageView)findViewById(2131296708)).setImageBitmap(this.a.g());
    ((ImageView)findViewById(2131296709)).setImageBitmap(this.a.e());
    ViewGroup localViewGroup = (ViewGroup)findViewById(2131296701);
    QuickDepositAccount localQuickDepositAccount = this.a.o();
    localViewGroup.addView(new a(localViewGroup.getContext(), "Deposit to", localQuickDepositAccount.b(), localQuickDepositAccount.d()).getView());
    this.d = ((TextView)findViewById(2131296704));
    View localView = findViewById(2131296702);
    localView.setVisibility(0);
    if (this.a.m() != null)
    {
      this.d.setText(this.a.n());
      this.k = ((TextView)findViewById(2131296707));
      if (!s.m(this.a.i().h()))
        break label563;
      this.k.setText(this.a.i().h());
      label234: this.b = ((EditText)findViewById(2131296712));
      if (!s.m(this.a.h()))
        break label576;
      this.b.setText(this.a.h());
      label274: if (localQuickDepositVerifyResponse.d())
        e(2131296711);
      if (!localQuickDepositVerifyResponse.k())
        break label589;
      this.b.setBackgroundResource(17301526);
      this.b.setEnabled(true);
      this.b.setFocusable(true);
      label320: this.c = ((EditText)findViewById(2131296715));
      if (!s.m(this.a.c()))
        break label616;
      this.c.setText(this.a.c());
      label360: if (localQuickDepositVerifyResponse.j())
        e(2131296714);
      if ((localQuickDepositVerifyResponse.j()) || (localQuickDepositVerifyResponse.d()))
      {
        this.l = ((Button)findViewById(2131296716));
        this.l.setVisibility(0);
        this.l.setOnClickListener(a(QuickDepositHelpFindAcctOrRoutingNumber.class));
      }
      if (!localQuickDepositVerifyResponse.l())
        break label629;
      this.c.setBackgroundResource(17301526);
      this.c.setEnabled(true);
      this.c.setFocusable(true);
    }
    while (true)
    {
      ((Button)findViewById(2131296699)).setOnClickListener(new hb(this));
      ((Button)findViewById(2131296698)).setOnClickListener(B());
      a(2131296708, new hc(this));
      a(2131296709, new hd(this));
      if (localQuickDepositVerifyResponse.e())
        c(localQuickDepositVerifyResponse.g());
      if (localQuickDepositVerifyResponse.p() != null)
        a(2131165318, localQuickDepositVerifyResponse.p(), true);
      return;
      localView.setVisibility(8);
      break;
      label563: this.k.setText("");
      break label234;
      label576: this.b.setText("");
      break label274;
      label589: this.b.setBackgroundResource(0);
      this.b.setEnabled(false);
      this.b.setFocusable(false);
      break label320;
      label616: this.c.setText("");
      break label360;
      label629: this.c.setBackgroundResource(0);
      this.c.setEnabled(false);
      this.c.setFocusable(false);
    }
  }

  public static class a extends d<QuickDepositVerifyActivity, Void, Void, QuickDepositCompleteResponse>
  {
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.QuickDepositVerifyActivity
 * JD-Core Version:    0.6.2
 */