package com.chase.sig.android.activity;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import com.chase.sig.android.d;
import com.chase.sig.android.domain.LabeledValue;
import com.chase.sig.android.domain.QuickPayActivityItem;
import com.chase.sig.android.domain.QuickPayActivityType;
import com.chase.sig.android.service.quickpay.QuickPayTransactionListResponse;
import com.chase.sig.android.util.s;
import com.chase.sig.android.view.JPTabWidget;
import com.chase.sig.android.view.JPTabWidget.b;
import com.chase.sig.android.view.af;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class QuickPayActivityActivity extends ai
  implements fk.e
{
  private String a;
  private int b;
  private int c;
  private int d;
  private int k;
  private boolean l;
  private ListView m;
  private View n;
  private ArrayList<QuickPayActivityItem> o;
  private TextView p;
  private TextView q;
  private TextView r;
  private String s;
  private QuickPayActivityType t;
  private JPTabWidget.b u = new hf(this);

  private void a(ArrayList<QuickPayActivityItem> paramArrayList, int paramInt1, int paramInt2, String paramString1, int paramInt3, String paramString2, boolean paramBoolean)
  {
    if (this.l)
    {
      this.m.setVisibility(8);
      if (paramString2 != null)
        this.q.setText(paramString2);
      this.q.setVisibility(0);
      return;
    }
    this.m.setVisibility(0);
    this.q.setVisibility(8);
    this.m.removeFooterView(this.n);
    this.n = null;
    String[] arrayOfString;
    int[] arrayOfInt;
    ArrayList localArrayList;
    label180: QuickPayActivityItem localQuickPayActivityItem;
    HashMap localHashMap;
    if ((paramInt1 == 1) || ((this.o != null) && (this.o.isEmpty())))
    {
      this.o = new ArrayList();
      this.o = paramArrayList;
      arrayOfString = new String[] { "date", "amount", "name", "status" };
      arrayOfInt = new int[] { 2131296499, 2131296500, 2131296733, 2131296734 };
      localArrayList = new ArrayList();
      Iterator localIterator = this.o.iterator();
      if (!localIterator.hasNext())
        break label407;
      localQuickPayActivityItem = (QuickPayActivityItem)localIterator.next();
      localHashMap = new HashMap();
      localHashMap.put("date", s.c(s.g(localQuickPayActivityItem.m().b())));
      localHashMap.put("amount", "$" + localQuickPayActivityItem.n().b());
      localHashMap.put("name", localQuickPayActivityItem.o().b());
      localHashMap.put("status", localQuickPayActivityItem.p().b());
      localHashMap.put("id", localQuickPayActivityItem.q().b());
      localHashMap.put("type", localQuickPayActivityItem.r().b());
      if (localQuickPayActivityItem.C() != null)
        break label394;
    }
    label394: for (String str = "false"; ; str = localQuickPayActivityItem.C().b())
    {
      localHashMap.put("isinvoicerequest", str);
      localArrayList.add(localHashMap);
      break label180;
      if (paramBoolean)
        break;
      this.o.addAll(paramArrayList);
      break;
    }
    label407: if (paramString1.equalsIgnoreCase("True"))
    {
      this.n = ((LayoutInflater)getBaseContext().getSystemService("layout_inflater")).inflate(2130903162, null);
      ((TextView)this.n.findViewById(2131296732)).setText(paramInt2 + " transactions total, " + (paramInt2 - paramInt1 * 25) + " to load...");
      this.m.addFooterView(this.n);
    }
    this.m.setOnItemClickListener(new hg(this, paramInt3));
    af localaf = new af(new SimpleAdapter(this, localArrayList, 2130903163, arrayOfString, arrayOfInt));
    this.m.setAdapter(localaf);
    this.m.setSelectionFromTop(25 * (paramInt1 - 1), 0);
  }

  public final void a(Bundle paramBundle)
  {
    b(2130903158);
    setTitle(2131165666);
    this.p = ((TextView)findViewById(2131296722));
    this.r = ((TextView)findViewById(2131296721));
    this.q = ((TextView)findViewById(2131296724));
    this.m = ((ListView)findViewById(2131296723));
    JPTabWidget localJPTabWidget1 = (JPTabWidget)findViewById(2131296505);
    localJPTabWidget1.setTabListener(this.u);
    JPTabWidget localJPTabWidget2 = (JPTabWidget)findViewById(2131296505);
    localJPTabWidget2.a(0, 2131165687);
    localJPTabWidget2.a(1, 2131165688);
    localJPTabWidget2.a(2, 2131165689);
    localJPTabWidget2.a(3, 2131165690);
    if ((paramBundle != null) && (paramBundle.getSerializable("bundle_activity") != null));
    for (int i = 1; i != 0; i = 0)
    {
      String str1 = paramBundle.getString("bundle_as_of_header");
      String str2 = paramBundle.getString("bundle_activity_type_title");
      this.b = paramBundle.getInt("bundle_totalRows");
      this.a = paramBundle.getString("bundle_showNext");
      this.c = paramBundle.getInt("bundle_nextPage");
      this.d = paramBundle.getInt("bundle_currentPage");
      this.k = paramBundle.getInt("bundle_selected_tab");
      this.s = paramBundle.getString("bundle_empty_message");
      this.p.setText(str1);
      this.r.setText(str2);
      this.o = ((ArrayList)paramBundle.getSerializable("bundle_activity"));
      this.t = ((QuickPayActivityType)paramBundle.getSerializable("activity_type"));
      localJPTabWidget1.a(this.k, false);
      this.l = paramBundle.getBoolean("no_activities");
      a(this.o, this.d, this.b, this.a, this.c, this.s, true);
      return;
    }
    localJPTabWidget1.a(0, true);
  }

  public final void a(String paramString)
  {
    ((TextView)findViewById(2131296722)).setText(paramString);
  }

  public final void b(String paramString)
  {
    ((TextView)findViewById(2131296721)).setText(paramString);
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    paramBundle.putInt("bundle_type_position", this.m.getSelectedItemPosition());
    paramBundle.putSerializable("bundle_activity", this.o);
    paramBundle.putString("bundle_as_of_header", this.p.getText().toString());
    paramBundle.putString("bundle_activity_type_title", this.r.getText().toString());
    paramBundle.putInt("bundle_selected_tab", this.k);
    paramBundle.putInt("bundle_currentPage", this.d);
    paramBundle.putInt("bundle_nextPage", this.c);
    paramBundle.putInt("bundle_totalRows", this.b);
    paramBundle.putString("bundle_showNext", this.a);
    paramBundle.putSerializable("activity_type", this.t);
    paramBundle.putBoolean("no_activities", this.l);
    paramBundle.putString("bundle_empty_message", this.s);
  }

  public static class a extends d<QuickPayActivityActivity, Object, Void, QuickPayTransactionListResponse>
  {
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.QuickPayActivityActivity
 * JD-Core Version:    0.6.2
 */