package com.chase.sig.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.InputFilter.LengthFilter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.chase.sig.android.domain.Email;
import com.chase.sig.android.domain.MobilePhoneNumber;
import com.chase.sig.android.domain.QuickPayRecipient;
import com.chase.sig.android.util.e;
import java.util.HashMap;
import java.util.List;

public class QuickPayAddRecipientActivity extends hr
{
  private boolean k;

  private void h()
  {
    this.a = new int[5];
    this.a[0] = 2131296739;
    this.a[1] = 2131296744;
    this.a[2] = 2131296748;
    this.a[3] = 2131296752;
    this.a[4] = 2131296756;
    this.b = new int[5];
    this.b[0] = 2131296741;
    this.b[1] = 2131296747;
    this.b[2] = 2131296751;
    this.b[3] = 2131296755;
    this.b[4] = 2131296759;
    this.c = new int[5];
    this.c[0] = 2131296783;
    this.c[1] = 2131296745;
    this.c[2] = 2131296749;
    this.c[3] = 2131296753;
    this.c[4] = 2131296757;
  }

  public final void a(Bundle paramBundle)
  {
    this.k = e.b(getIntent().getExtras(), "quick_pay_manage_recipient");
    if (this.k)
      b(2130903175);
    Button localButton1;
    Button localButton2;
    while (true)
    {
      super.a(paramBundle);
      setTitle(2131165717);
      boolean bool = getIntent().getBooleanExtra("fromContactsActivity", false);
      localButton1 = (Button)findViewById(2131296761);
      localButton2 = (Button)findViewById(2131296760);
      if (!bool)
        break;
      QuickPayRecipient localQuickPayRecipient = (QuickPayRecipient)getIntent().getSerializableExtra("recipient");
      ((TextView)findViewById(2131296738)).setText(localQuickPayRecipient.a());
      if (localQuickPayRecipient.f() != null)
      {
        InputFilter[] arrayOfInputFilter = new InputFilter[1];
        arrayOfInputFilter[0] = new InputFilter.LengthFilter(16);
        ((EditText)findViewById(2131296743)).setFilters(arrayOfInputFilter);
        ((TextView)findViewById(2131296743)).setText(localQuickPayRecipient.i().a());
      }
      if ((this.b.length == 1) && (localQuickPayRecipient.j().size() > 1))
      {
        h();
        e();
      }
      for (int i = 0; i < localQuickPayRecipient.j().size(); i++)
        ((TextView)findViewById(this.b[i])).setText(((Email)localQuickPayRecipient.j().get(i)).a());
      b(2130903164);
    }
    localButton1.setText(2131165702);
    localButton2.setVisibility(0);
    localButton2.setText(2131165846);
    localButton2.setOnClickListener(new hh(this));
  }

  protected final void d()
  {
    if (this.k)
    {
      h();
      return;
    }
    this.a = new int[1];
    this.a[0] = 2131296739;
    this.b = new int[1];
    this.b[0] = 2131296741;
  }

  protected final void e()
  {
    d.clear();
    d.put("mobile", Integer.valueOf(2131296742));
    d.put("nickname", Integer.valueOf(2131296737));
    d.put("email1", Integer.valueOf(2131296740));
    if (this.k)
    {
      d.put("email2", Integer.valueOf(2131296746));
      d.put("email3", Integer.valueOf(2131296750));
      d.put("email4", Integer.valueOf(2131296754));
      d.put("email5", Integer.valueOf(2131296758));
    }
  }

  public final void f()
  {
    ((EditText)findViewById(2131296738)).setText("");
    ((EditText)findViewById(2131296743)).setText("");
    for (int i = 0; i < this.b.length; i++)
      ((EditText)findViewById(this.b[i])).setText("");
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.QuickPayAddRecipientActivity
 * JD-Core Version:    0.6.2
 */