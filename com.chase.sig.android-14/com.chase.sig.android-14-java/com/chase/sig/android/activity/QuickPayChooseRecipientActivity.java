package com.chase.sig.android.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import com.chase.sig.android.activity.a.f;
import com.chase.sig.android.d;
import com.chase.sig.android.domain.QuickPayRecipient;
import com.chase.sig.android.service.quickpay.QuickPayRecipientListResponse;
import com.chase.sig.android.util.e;
import com.chase.sig.android.view.g;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class QuickPayChooseRecipientActivity extends ai
  implements fk.e
{
  public QuickPayRecipientListResponse a;
  public ListView b;

  public final void a(Bundle paramBundle)
  {
    b(2130903179);
    setTitle(2131165747);
    a(2131296805, a(QuickPayAddRecipientActivity.class).c());
    a(2131296806, a(ContactsActivity.class).c().a("quick_pay_manage_recipient", Boolean.valueOf(true)));
    if (e.a(paramBundle, "recipients"))
    {
      this.a = ((QuickPayRecipientListResponse)paramBundle.getSerializable("recipients"));
      d();
      return;
    }
    a(a.class, new Object[0]);
  }

  public final void d()
  {
    this.b = ((ListView)findViewById(2131296807));
    ArrayList localArrayList1 = this.a.a();
    if ((this.a.a() == null) || (this.a.a().size() == 0))
    {
      findViewById(2131296808).setVisibility(0);
      findViewById(2131296807).setVisibility(8);
      return;
    }
    this.b.setVisibility(0);
    ArrayList localArrayList2 = new ArrayList();
    Iterator localIterator = localArrayList1.iterator();
    while (localIterator.hasNext())
    {
      QuickPayRecipient localQuickPayRecipient = (QuickPayRecipient)localIterator.next();
      HashMap localHashMap = new HashMap();
      localHashMap.put("name", localQuickPayRecipient.a());
      localHashMap.put("email", localQuickPayRecipient.d());
      localHashMap.put("status", localQuickPayRecipient.l());
      localArrayList2.add(localHashMap);
    }
    g localg = new g(new SimpleAdapter(this, localArrayList2, 2130903180, new String[] { "name", "email", "status" }, new int[] { 2131296770, 2131296772, 2131296809 }));
    this.b.setOnItemClickListener(new hi(this, localArrayList1));
    this.b.setFooterDividersEnabled(false);
    this.b.setAdapter(localg);
  }

  protected void onRestart()
  {
    super.onRestart();
    a(a.class, new Object[0]);
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    if (this.a != null)
      paramBundle.putSerializable("recipients", this.a);
  }

  public static class a extends d<QuickPayChooseRecipientActivity, Object, Void, QuickPayRecipientListResponse>
  {
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.QuickPayChooseRecipientActivity
 * JD-Core Version:    0.6.2
 */