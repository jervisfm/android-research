package com.chase.sig.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.chase.sig.android.domain.Email;
import com.chase.sig.android.domain.QuickPayRecipient;
import com.chase.sig.android.util.s;
import com.google.common.a.e;
import java.util.List;

public class QuickPayChooseRecipientNotificatonActivity extends ai
  implements fk.e
{
  private LayoutInflater a;
  private QuickPayRecipient b;
  private String c;
  private String d;

  public final void a(Bundle paramBundle)
  {
    b(2130903181);
    setTitle(2131165737);
    this.a = ((LayoutInflater)getSystemService("layout_inflater"));
    List localList;
    LinearLayout localLinearLayout;
    int i;
    label122: Email localEmail;
    View localView2;
    CheckBox localCheckBox2;
    if (paramBundle == null)
    {
      this.b = ((QuickPayRecipient)getIntent().getExtras().getSerializable("recipient"));
      this.c = this.b.f();
      this.d = this.b.d();
      ((TextView)findViewById(2131296738)).setText(this.b.a());
      localList = this.b.j();
      localLinearLayout = (LinearLayout)findViewById(2131296813);
      ((LinearLayout)findViewById(2131296813)).removeAllViews();
      i = 0;
      if (i >= localList.size())
        break label368;
      localEmail = (Email)localList.get(i);
      localView2 = this.a.inflate(2130903148, null);
      localCheckBox2 = (CheckBox)localView2.findViewById(2131296641);
      ((TextView)localView2.findViewById(2131296640)).setText(localEmail.a());
      if (!localEmail.b())
        break label341;
    }
    label341: for (String str3 = "Default"; ; str3 = "Email " + Integer.toString(i + 1))
    {
      ((TextView)localView2.findViewById(2131296639)).setText(str3);
      localCheckBox2.setChecked(Boolean.valueOf(e.a(this.d).equals(localEmail.a())).booleanValue());
      localCheckBox2.setOnCheckedChangeListener(new hk(this, localLinearLayout, localList, localCheckBox2, localEmail));
      if (i + 1 == localList.size())
        localView2.findViewById(2131296642).setVisibility(8);
      ((LinearLayout)findViewById(2131296813)).addView(localView2);
      i++;
      break label122;
      this.b = ((QuickPayRecipient)paramBundle.getSerializable("recipient"));
      this.c = paramBundle.getString("savedMobileNumber");
      this.d = paramBundle.getString("savedEmail");
      break;
    }
    label368: if (s.m(this.b.f()))
    {
      String str1 = this.b.f();
      View localView1 = this.a.inflate(2130903148, null);
      ((TextView)localView1.findViewById(2131296639)).setText("Mobile");
      ((TextView)localView1.findViewById(2131296640)).setText(str1);
      localView1.findViewById(2131296642).setVisibility(8);
      CheckBox localCheckBox1 = (CheckBox)localView1.findViewById(2131296641);
      localCheckBox1.setOnCheckedChangeListener(new hl(this, str1));
      String str2 = this.c;
      boolean bool = false;
      if (str2 != null)
        bool = true;
      localCheckBox1.setChecked(bool);
      ((LinearLayout)n().findViewById(2131296811)).addView(localView1);
    }
    while (true)
    {
      ((Button)findViewById(2131296814)).setOnClickListener(new hj(this));
      return;
      ((TextView)findViewById(2131296810)).setText(2131165761);
      findViewById(2131296812).setVisibility(8);
      findViewById(2131296811).setVisibility(8);
    }
  }

  protected void onRestoreInstanceState(Bundle paramBundle)
  {
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    paramBundle.putSerializable("recipient", this.b);
    paramBundle.putSerializable("savedEmail", this.d);
    paramBundle.putSerializable("savedMobileNumber", this.c);
    super.onSaveInstanceState(paramBundle);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.QuickPayChooseRecipientNotificatonActivity
 * JD-Core Version:    0.6.2
 */