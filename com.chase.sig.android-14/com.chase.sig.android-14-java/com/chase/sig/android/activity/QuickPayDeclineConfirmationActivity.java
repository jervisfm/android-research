package com.chase.sig.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.ViewGroup;
import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.domain.QuickPayActivityType;
import com.chase.sig.android.domain.QuickPayDeclineTransaction;
import com.chase.sig.android.view.aa;

public class QuickPayDeclineConfirmationActivity extends hm
{
  public final void a(Bundle paramBundle)
  {
    b(2130903170);
    QuickPayDeclineTransaction localQuickPayDeclineTransaction = (QuickPayDeclineTransaction)getIntent().getSerializableExtra("quick_pay_transaction");
    if (QuickPayActivityType.d.a(localQuickPayDeclineTransaction.I()))
      setTitle(2131165726);
    while (true)
    {
      aa localaa = new aa(getBaseContext(), localQuickPayDeclineTransaction, true, (ChaseApplication)getApplication());
      ((ViewGroup)findViewById(2131296718)).addView(localaa);
      d();
      return;
      if (QuickPayActivityType.a.a(localQuickPayDeclineTransaction.I()))
        setTitle(2131165727);
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.QuickPayDeclineConfirmationActivity
 * JD-Core Version:    0.6.2
 */