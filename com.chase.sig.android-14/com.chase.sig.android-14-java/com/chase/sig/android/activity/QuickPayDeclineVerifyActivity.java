package com.chase.sig.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.LinearLayout;
import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.d;
import com.chase.sig.android.domain.QuickPayActivityType;
import com.chase.sig.android.domain.QuickPayDeclineTransaction;
import com.chase.sig.android.service.quickpay.QuickPayDeclineResponse;
import com.chase.sig.android.view.aa;

public class QuickPayDeclineVerifyActivity extends eb
  implements fk.e
{
  public final void a(Bundle paramBundle)
  {
    b(2130903172);
    QuickPayDeclineTransaction localQuickPayDeclineTransaction = (QuickPayDeclineTransaction)getIntent().getExtras().getSerializable("quick_pay_transaction");
    if (QuickPayActivityType.d.a(localQuickPayDeclineTransaction.I()))
      setTitle(2131165734);
    while (true)
    {
      aa localaa = new aa(this, localQuickPayDeclineTransaction, false, (ChaseApplication)getApplication());
      ((LinearLayout)findViewById(2131296718)).addView(localaa);
      a(2131296781, new hn(this));
      a(2131296780, C());
      return;
      if (QuickPayActivityType.a.a(localQuickPayDeclineTransaction.I()))
        setTitle(2131165745);
    }
  }

  public static class a extends d<QuickPayDeclineVerifyActivity, Void, Void, QuickPayDeclineResponse>
  {
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.QuickPayDeclineVerifyActivity
 * JD-Core Version:    0.6.2
 */