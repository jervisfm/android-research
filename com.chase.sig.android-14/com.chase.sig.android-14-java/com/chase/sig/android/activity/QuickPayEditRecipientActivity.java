package com.chase.sig.android.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import com.chase.sig.android.d;
import com.chase.sig.android.domain.Email;
import com.chase.sig.android.domain.MobilePhoneNumber;
import com.chase.sig.android.domain.QuickPayRecipient;
import com.chase.sig.android.service.quickpay.QuickPayDeleteRecipientResponse;
import com.chase.sig.android.view.k.a;
import java.util.HashMap;
import java.util.List;

public class QuickPayEditRecipientActivity extends hr
{
  private int k = 101;
  private int l = 102;

  public final void a(Bundle paramBundle)
  {
    b(2130903175);
    super.a(paramBundle);
    setTitle(2131165715);
    QuickPayRecipient localQuickPayRecipient = (QuickPayRecipient)getIntent().getExtras().getSerializable("recipient");
    ((TextView)findViewById(2131296738)).setText(localQuickPayRecipient.a());
    if (localQuickPayRecipient.f() != null)
      ((TextView)findViewById(2131296743)).setText(localQuickPayRecipient.i().a());
    for (int i = 0; i < localQuickPayRecipient.j().size(); i++)
      ((TextView)findViewById(this.b[i])).setText(((Email)localQuickPayRecipient.j().get(i)).a());
    Button localButton = (Button)findViewById(2131296735);
    localButton.setVisibility(0);
    localButton.setOnClickListener(i(this.k));
  }

  protected final void d()
  {
    this.a = new int[5];
    this.a[0] = 2131296739;
    this.a[1] = 2131296744;
    this.a[2] = 2131296748;
    this.a[3] = 2131296752;
    this.a[4] = 2131296756;
    this.b = new int[5];
    this.b[0] = 2131296741;
    this.b[1] = 2131296747;
    this.b[2] = 2131296751;
    this.b[3] = 2131296755;
    this.b[4] = 2131296759;
    this.c = new int[5];
    this.c[0] = 2131296783;
    this.c[1] = 2131296745;
    this.c[2] = 2131296749;
    this.c[3] = 2131296753;
    this.c[4] = 2131296757;
  }

  protected final void e()
  {
    d.clear();
    d.put("mobile", Integer.valueOf(2131296742));
    d.put("nickname", Integer.valueOf(2131296737));
    d.put("email1", Integer.valueOf(2131296740));
    d.put("email2", Integer.valueOf(2131296746));
    d.put("email3", Integer.valueOf(2131296750));
    d.put("email4", Integer.valueOf(2131296754));
    d.put("email5", Integer.valueOf(2131296758));
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    k.a locala1 = new k.a(this);
    if (paramInt == this.k)
    {
      k.a locala2 = locala1.b(2131165708);
      locala2.i = true;
      locala2.a(2131165291, new hp(this)).b(2131165697, new ho(this));
      return locala1.d(-1);
    }
    if (paramInt == this.l)
    {
      locala1.b(2131165709).a(2131165288, new hq(this));
      return locala1.d(-1);
    }
    return super.onCreateDialog(paramInt);
  }

  public static class a extends d<QuickPayEditRecipientActivity, Void, Void, QuickPayDeleteRecipientResponse>
  {
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.QuickPayEditRecipientActivity
 * JD-Core Version:    0.6.2
 */