package com.chase.sig.android.activity;

import android.content.res.Resources;
import android.os.Bundle;
import android.text.Html;
import android.widget.LinearLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.chase.sig.android.domain.o;
import com.chase.sig.android.service.quickpay.TodoListResponse;
import com.chase.sig.android.util.s;

public class QuickPayHomeActivity extends ai
  implements fk.e
{
  public final void a(Bundle paramBundle)
  {
    b(2130903176);
    ((TextView)findViewById(2131296784)).setText(Html.fromHtml(getResources().getString(2131165777) + s.z(getResources().getString(2131165778)) + " " + getResources().getString(2131165779)));
    LinearLayout localLinearLayout1 = (LinearLayout)findViewById(2131296785);
    LinearLayout localLinearLayout2 = (LinearLayout)findViewById(2131296786);
    LinearLayout localLinearLayout3 = (LinearLayout)findViewById(2131296787);
    LinearLayout localLinearLayout4 = (LinearLayout)findViewById(2131296791);
    LinearLayout localLinearLayout5 = (LinearLayout)findViewById(2131296792);
    LinearLayout localLinearLayout6 = (LinearLayout)findViewById(2131296793);
    localLinearLayout1.setOnClickListener(new hv(this));
    localLinearLayout2.setOnClickListener(new hw(this));
    localLinearLayout3.setOnClickListener(a(QuickPayTodoListActivity.class));
    localLinearLayout4.setOnClickListener(new hx(this));
    localLinearLayout6.setOnClickListener(a(QuickPayActivityActivity.class));
    localLinearLayout5.setOnClickListener(a(QuickPayPendingTransactionsActivity.class));
  }

  protected void onResume()
  {
    super.onResume();
    int i = r().c.d();
    TextView localTextView1 = (TextView)findViewById(2131296790);
    if (i > 0)
    {
      if (i > 1)
      {
        localTextView1.setText("(You have " + i + " new items)");
        return;
      }
      localTextView1.setText("(You have " + i + " new item)");
      return;
    }
    TextView localTextView2 = (TextView)findViewById(2131296789);
    RelativeLayout.LayoutParams localLayoutParams = (RelativeLayout.LayoutParams)localTextView2.getLayoutParams();
    localLayoutParams.addRule(15);
    localTextView2.setLayoutParams(localLayoutParams);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.QuickPayHomeActivity
 * JD-Core Version:    0.6.2
 */