package com.chase.sig.android.activity;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import com.chase.sig.android.d;
import com.chase.sig.android.domain.QuickPayPendingTransaction;
import com.chase.sig.android.domain.m;
import com.chase.sig.android.domain.o;
import com.chase.sig.android.service.quickpay.QuickPayTransactionListResponse;
import com.chase.sig.android.service.quickpay.TodoListResponse;
import com.chase.sig.android.util.Dollar;
import com.chase.sig.android.util.s;
import com.chase.sig.android.view.af;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class QuickPayPendingTransactionsActivity extends ai
  implements fk.e
{
  protected m a;
  private String b;
  private int c;
  private int d;
  private int k;
  private String l;
  private boolean m = false;
  private ListView n;
  private View o;
  private ArrayList<QuickPayPendingTransaction> p;
  private TextView q;

  private void a(ArrayList<QuickPayPendingTransaction> paramArrayList, int paramInt1, int paramInt2, String paramString, int paramInt3)
  {
    if (!this.m)
    {
      this.n.setVisibility(0);
      this.q.setVisibility(8);
      String[] arrayOfString;
      int[] arrayOfInt;
      ArrayList localArrayList;
      label148: QuickPayPendingTransaction localQuickPayPendingTransaction;
      HashMap localHashMap;
      if ((paramInt1 == 1) || ((this.p != null) && (this.p.isEmpty())))
      {
        this.p = new ArrayList();
        this.p = paramArrayList;
        this.n.removeFooterView(this.o);
        this.o = null;
        arrayOfString = new String[] { "date", "amount", "name", "frequency_foot_note" };
        arrayOfInt = new int[] { 2131296499, 2131296500, 2131296733, 2131296734 };
        localArrayList = new ArrayList();
        Iterator localIterator = this.p.iterator();
        if (!localIterator.hasNext())
          break label451;
        localQuickPayPendingTransaction = (QuickPayPendingTransaction)localIterator.next();
        localHashMap = new HashMap();
        localHashMap.put("name", localQuickPayPendingTransaction.v());
        localHashMap.put("amount", new Dollar(localQuickPayPendingTransaction.r()).h());
        localHashMap.put("date", s.i(localQuickPayPendingTransaction.m()));
        localHashMap.put("transactionId", localQuickPayPendingTransaction.w());
        if (localQuickPayPendingTransaction.X() == null)
          break label436;
        localHashMap.put("nextPaymentDate", localQuickPayPendingTransaction.F());
        localHashMap.put("noOfPayments", Integer.valueOf(localQuickPayPendingTransaction.H()));
        Object[] arrayOfObject2 = new Object[1];
        arrayOfObject2[0] = Integer.valueOf(localQuickPayPendingTransaction.H());
        String str = String.format("%d payments left", arrayOfObject2);
        if (localQuickPayPendingTransaction.C())
          str = "Unlimited";
        Object[] arrayOfObject3 = new Object[2];
        arrayOfObject3[0] = this.a.a(localQuickPayPendingTransaction.X());
        arrayOfObject3[1] = str;
        localHashMap.put("frequency_foot_note", String.format("%s, %s", arrayOfObject3));
      }
      while (true)
      {
        localHashMap.put("transaction", localQuickPayPendingTransaction);
        localArrayList.add(localHashMap);
        break label148;
        if ((this.p == null) || (paramInt1 == 0))
          this.p = new ArrayList();
        this.p.addAll(paramArrayList);
        break;
        label436: localHashMap.put("frequency_foot_note", "One Time");
      }
      label451: if ("True".equals(paramString))
      {
        if (this.o == null)
        {
          this.o = ((LayoutInflater)getBaseContext().getSystemService("layout_inflater")).inflate(2130903162, null);
          TextView localTextView = (TextView)this.o.findViewById(2131296732);
          Object[] arrayOfObject1 = new Object[2];
          arrayOfObject1[0] = Integer.valueOf(paramInt2);
          arrayOfObject1[1] = Integer.valueOf(paramInt2 - 25);
          localTextView.setText(String.format("%d transactions total, %d to load...", arrayOfObject1));
          this.n.addFooterView(this.o);
        }
        this.o.setOnClickListener(new ia(this, paramInt3));
      }
      while (true)
      {
        this.n.setOnItemClickListener(new ib(this));
        af localaf = new af(new SimpleAdapter(this, localArrayList, 2130903163, arrayOfString, arrayOfInt));
        this.n.setAdapter(localaf);
        return;
        if (this.o != null)
          this.n.removeFooterView(this.o);
      }
    }
    this.n.setVisibility(8);
    this.q.setVisibility(0);
  }

  public final void a(Bundle paramBundle)
  {
    b(2130903187);
    setTitle(2131165711);
    this.q = ((TextView)findViewById(2131296724));
    this.n = ((ListView)findViewById(2131296723));
    this.a = r().c.t();
    int i;
    if ((paramBundle != null) && (paramBundle.containsKey("bundle_activity")))
    {
      i = 1;
      if (i == 0)
        break label216;
      this.p = ((ArrayList)paramBundle.get("bundle_activity"));
      this.k = paramBundle.getInt("bundle_currentPage");
      this.d = paramBundle.getInt("bundle_nextPage");
      this.c = paramBundle.getInt("bundle_totalRows");
      this.b = paramBundle.getString("bundle_showNext");
      this.m = paramBundle.getBoolean("no_activities");
      this.l = paramBundle.getString("bundle_sort_column");
      a(this.p, this.k, this.c, this.b, this.d);
    }
    while (true)
    {
      a(2131296852, new hy(this));
      a(2131296853, new hz(this));
      return;
      i = 0;
      break;
      label216: a locala = (a)this.e.a(a.class);
      if (!locala.d())
      {
        this.l = "recipient";
        Object[] arrayOfObject = new Object[2];
        arrayOfObject[0] = this.l;
        arrayOfObject[1] = Integer.valueOf(1);
        locala.execute(arrayOfObject);
      }
    }
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    if (!((a)this.e.a(a.class)).d())
    {
      paramBundle.putSerializable("bundle_activity", this.p);
      paramBundle.putInt("bundle_currentPage", this.k);
      paramBundle.putInt("bundle_nextPage", this.d);
      paramBundle.putInt("bundle_totalRows", this.c);
      paramBundle.putString("bundle_showNext", this.b);
      paramBundle.putBoolean("no_activities", this.m);
      paramBundle.putString("bundle_sort_column", this.l);
    }
  }

  public static class a extends d<QuickPayPendingTransactionsActivity, Object, Void, QuickPayTransactionListResponse>
  {
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.QuickPayPendingTransactionsActivity
 * JD-Core Version:    0.6.2
 */