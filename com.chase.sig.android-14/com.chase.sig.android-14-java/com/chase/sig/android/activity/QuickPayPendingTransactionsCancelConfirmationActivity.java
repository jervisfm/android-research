package com.chase.sig.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.ViewGroup;
import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.a.a;
import com.chase.sig.android.activity.a.f;
import com.chase.sig.android.domain.QuickPayPendingTransaction;
import com.chase.sig.android.view.aa;

public class QuickPayPendingTransactionsCancelConfirmationActivity extends ai
  implements fk.e
{
  public final void a(Bundle paramBundle)
  {
    b(2130903169);
    setTitle(2131165300);
    QuickPayPendingTransaction localQuickPayPendingTransaction = (QuickPayPendingTransaction)getIntent().getExtras().getSerializable("sendTransaction");
    aa localaa = new aa(getBaseContext(), localQuickPayPendingTransaction, (ChaseApplication)getApplication());
    ((ViewGroup)findViewById(2131296773)).addView(localaa);
    a(2131296774, a(QuickPayPendingTransactionsActivity.class).b());
    a(2131296775, a(QuickPayHomeActivity.class).b());
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if (j(paramInt))
      return true;
    a.a(paramInt, this, QuickPayPendingTransactionsActivity.class);
    return super.onKeyDown(paramInt, paramKeyEvent);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.QuickPayPendingTransactionsCancelConfirmationActivity
 * JD-Core Version:    0.6.2
 */