package com.chase.sig.android.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.Button;
import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.c.a;
import com.chase.sig.android.d;
import com.chase.sig.android.domain.QuickPayPendingTransaction;
import com.chase.sig.android.service.quickpay.QuickPayCancelPendingTransactionResponse;
import com.chase.sig.android.view.aa;
import com.chase.sig.android.view.k.a;

public class QuickPayPendingTransactionsDetailActivity extends ai
  implements fk.e, c.a
{
  private QuickPayPendingTransaction a = null;

  public final void a()
  {
    Intent localIntent = new Intent(this, QuickPayHomeActivity.class);
    localIntent.setFlags(67108864);
    startActivity(localIntent);
  }

  public final void a(Bundle paramBundle)
  {
    b(2130903177);
    setTitle(2131165711);
    this.a = ((QuickPayPendingTransaction)getIntent().getExtras().getSerializable("sendTransaction"));
    aa localaa = new aa(getBaseContext(), this.a, (ChaseApplication)getApplication());
    ((ViewGroup)findViewById(2131296794)).addView(localaa);
    Button localButton1 = (Button)findViewById(2131296796);
    localButton1.setEnabled(true);
    localButton1.setVisibility(0);
    if ((this.a.D()) && ((this.a.H() > 1) || (this.a.C())));
    for (boolean bool = true; ; bool = false)
    {
      localButton1.setOnClickListener(new ic(this, bool));
      if (!this.a.P())
      {
        localButton1.setEnabled(false);
        localButton1.setVisibility(8);
      }
      Button localButton2 = (Button)findViewById(2131296795);
      localButton2.setEnabled(true);
      localButton2.setVisibility(0);
      localButton2.setOnClickListener(new ii(this, bool));
      if (!this.a.Q())
      {
        localButton2.setEnabled(false);
        localButton2.setVisibility(8);
      }
      if ((this.a.D()) && (bool))
        localButton2.setText(2131165759);
      return;
    }
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    Object localObject = super.onCreateDialog(paramInt);
    k.a locala = new k.a(this);
    if (paramInt == 0)
    {
      locala.i = true;
      locala.a(2131165758, new ik(this)).b(2131165293, new ij(this)).b(2131165762);
      localObject = locala.d(-1);
    }
    do
    {
      return localObject;
      if (paramInt == 1)
      {
        locala.b(2131165770);
        locala.i = true;
        locala.a(2131165757, new in(this)).c(2131165756, new im(this)).b(2131165755, new il(this));
        return locala.d(-1);
      }
      if (paramInt == 2)
      {
        locala.i = true;
        locala.a(2131165754, new ip(this)).b(2131165753, new io(this)).b(2131165752);
        return locala.d(-1);
      }
      if (paramInt == 3)
      {
        locala.i = true;
        locala.a(2131165750, new ie(this)).b(2131165751, new id(this)).b(2131165749);
        return locala.d(-1);
      }
    }
    while (paramInt != 5);
    locala.b(2131165776);
    locala.i = true;
    locala.a(2131165722, new ih(this)).c(2131165723, new ig(this)).b(2131165724, new if(this));
    return locala.d(-1);
  }

  public static class a extends d<QuickPayPendingTransactionsDetailActivity, Boolean, Void, QuickPayCancelPendingTransactionResponse>
  {
    Boolean a;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.QuickPayPendingTransactionsDetailActivity
 * JD-Core Version:    0.6.2
 */