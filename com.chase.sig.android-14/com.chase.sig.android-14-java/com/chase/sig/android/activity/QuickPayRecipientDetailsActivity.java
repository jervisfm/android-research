package com.chase.sig.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.chase.sig.android.d;
import com.chase.sig.android.domain.Email;
import com.chase.sig.android.domain.QuickPayRecipient;
import com.chase.sig.android.service.quickpay.QuickPayAddRecipientResponse;
import com.chase.sig.android.util.e;
import com.chase.sig.android.util.s;
import java.util.List;

public class QuickPayRecipientDetailsActivity extends ai
  implements fk.e
{
  private QuickPayRecipient a;

  private View.OnClickListener a(boolean paramBoolean, QuickPayRecipient paramQuickPayRecipient)
  {
    return new is(this, paramBoolean, paramQuickPayRecipient);
  }

  private void d()
  {
    a(2131296804, a(false, this.a));
    a(2131296786, a(true, this.a));
  }

  public final void a(Bundle paramBundle)
  {
    b(2130903178);
    setTitle(2131165718);
    this.a = ((QuickPayRecipient)e.a(paramBundle, getIntent(), "recipient"));
    ((TextView)findViewById(2131296738)).setText(this.a.a());
    TextView localTextView = (TextView)findViewById(2131296743);
    if (this.a.i() != null)
      if (!s.m(this.a.f()))
        break label212;
    label212: for (String str = this.a.f(); ; str = "")
    {
      localTextView.setText(s.f(str));
      RadioGroup localRadioGroup = (RadioGroup)findViewById(2131296798);
      int i = this.a.e().size();
      List localList = this.a.e();
      for (int j = 0; j < i; j++)
      {
        RadioButton localRadioButton = (RadioButton)localRadioGroup.getChildAt(j);
        Email localEmail = (Email)localList.get(j);
        localRadioButton.setText(localEmail.a());
        localRadioButton.setVisibility(0);
        localRadioButton.setChecked(localEmail.b());
        localRadioButton.setOnCheckedChangeListener(new iq(this));
      }
    }
    d();
    ((Button)findViewById(2131296797)).setOnClickListener(new ir(this));
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if (j(paramInt))
      return true;
    if (paramInt == 4)
    {
      Intent localIntent = new Intent(this, QuickPayChooseRecipientActivity.class);
      localIntent.addFlags(67108864);
      localIntent.putExtra("quick_pay_manage_recipient", getIntent().getBooleanExtra("quick_pay_manage_recipient", false));
      startActivity(localIntent);
      return true;
    }
    return super.onKeyDown(paramInt, paramKeyEvent);
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    paramBundle.putSerializable("recipient", this.a);
    super.onSaveInstanceState(paramBundle);
  }

  public static class a extends d<QuickPayRecipientDetailsActivity, Void, Void, QuickPayAddRecipientResponse>
  {
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.QuickPayRecipientDetailsActivity
 * JD-Core Version:    0.6.2
 */