package com.chase.sig.android.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.chase.sig.android.domain.QuickPayTransaction;
import com.chase.sig.android.view.JPSpinner;

public class QuickPayRequestMoneyActivity extends iy
{
  public final void a(Bundle paramBundle)
  {
    super.a(paramBundle);
    f();
    setTitle(2131165682);
    ((JPSpinner)findViewById(2131296823)).setVisibility(8);
    findViewById(2131296821).setVisibility(8);
    findViewById(2131296829).setVisibility(8);
    ((TextView)findViewById(2131296830)).setText("Due date");
    ((TextView)findViewById(2131296817)).setText("From");
  }

  protected final QuickPayTransaction d()
  {
    QuickPayTransaction localQuickPayTransaction = super.d();
    localQuickPayTransaction.b(true);
    localQuickPayTransaction.k(((Button)findViewById(2131296831)).getText().toString());
    return localQuickPayTransaction;
  }

  protected final boolean e()
  {
    return true;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.QuickPayRequestMoneyActivity
 * JD-Core Version:    0.6.2
 */