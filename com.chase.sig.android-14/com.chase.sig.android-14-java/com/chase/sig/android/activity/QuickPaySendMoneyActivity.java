package com.chase.sig.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.d;
import com.chase.sig.android.domain.LabeledValue;
import com.chase.sig.android.domain.QuickPayTransaction;
import com.chase.sig.android.domain.m;
import com.chase.sig.android.domain.o;
import com.chase.sig.android.service.quickpay.PayFromAccount;
import com.chase.sig.android.service.quickpay.QuickPayTransactionStartResponse;
import com.chase.sig.android.service.quickpay.TodoListResponse;
import com.chase.sig.android.util.a;
import com.chase.sig.android.util.s;
import com.chase.sig.android.view.JPSpinner;
import com.chase.sig.android.view.x;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

public class QuickPaySendMoneyActivity extends iy
{
  JPSpinner a;
  TextView b;
  TextView c;
  protected x d = new iv(this);
  private ArrayList<PayFromAccount> n;
  private String o;
  private CheckBox p;
  private CheckBox q;
  private EditText r;

  private JPSpinner G()
  {
    return (JPSpinner)findViewById(2131296833);
  }

  private void H()
  {
    this.p.setOnCheckedChangeListener(new it(this));
    this.q.setOnCheckedChangeListener(new iu(this));
  }

  private int I()
  {
    try
    {
      int i = Integer.parseInt(this.r.getText().toString());
      return i;
    }
    catch (NumberFormatException localNumberFormatException)
    {
    }
    return 0;
  }

  private static int a(ArrayList<PayFromAccount> paramArrayList, String paramString)
  {
    Iterator localIterator = paramArrayList.iterator();
    int i = -1;
    PayFromAccount localPayFromAccount;
    do
    {
      if (!localIterator.hasNext())
        break;
      localPayFromAccount = (PayFromAccount)localIterator.next();
      i++;
    }
    while (!paramString.equals(localPayFromAccount.b()));
    return i;
  }

  private void a(String paramString, boolean paramBoolean)
  {
    if (s.m(this.o))
      return;
    this.o = paramString;
    a(2131165318, paramString, paramBoolean);
  }

  private void a(ArrayList<PayFromAccount> paramArrayList)
  {
    this.n = paramArrayList;
    this.a.removeAllViews();
    this.a.a(this.d);
    this.a.setAdapter(a.a(this, paramArrayList));
    if (this.k.p() != null)
    {
      this.a.setSelection(a(paramArrayList, this.k.p()));
      return;
    }
    if (((ChaseApplication)getApplication()).b().c.n() != null)
    {
      this.a.setSelection(b(paramArrayList));
      k(b(paramArrayList));
      return;
    }
    this.a.setSelection(-1);
  }

  private void a(boolean paramBoolean)
  {
    int i = 8;
    if (this.k.R())
      this.p.setVisibility(i);
    if (paramBoolean)
      i = 0;
    findViewById(2131296833).setVisibility(i);
    findViewById(2131296832).setVisibility(i);
    findViewById(2131296834).setVisibility(i);
    findViewById(2131296413).setVisibility(i);
    TextView localTextView = (TextView)findViewById(2131296830);
    if (paramBoolean);
    for (String str = getString(2131165740); ; str = getString(2131165739))
    {
      localTextView.setText(str);
      return;
    }
  }

  private int b(ArrayList<PayFromAccount> paramArrayList)
  {
    return a(paramArrayList, ((ChaseApplication)getApplication()).b().c.n().b());
  }

  public final void a(Bundle paramBundle)
  {
    super.a(paramBundle);
    if ((paramBundle != null) && (paramBundle.containsKey("FOOTNOTE")))
      a(paramBundle.getString("FOOTNOTE"), false);
    if ((paramBundle != null) && (paramBundle.containsKey("PAY_FROM_ACCOUNTS")) && (paramBundle.getSerializable("PAY_FROM_ACCOUNTS") != null))
    {
      this.n = ((ArrayList)paramBundle.getSerializable("PAY_FROM_ACCOUNTS"));
      a(this.n);
      H();
      return;
    }
    a(a.class, new Void[0]);
  }

  protected final QuickPayTransaction d()
  {
    QuickPayTransaction localQuickPayTransaction = super.d();
    localQuickPayTransaction.b(false);
    int i = this.a.getSelectedItemPosition();
    PayFromAccount localPayFromAccount;
    label71: Date localDate;
    String str1;
    m localm;
    int j;
    if (i < 0)
    {
      localPayFromAccount = null;
      if (localPayFromAccount != null)
      {
        localQuickPayTransaction.b(localPayFromAccount.b());
        localQuickPayTransaction.c(localPayFromAccount.a());
        if (!localPayFromAccount.d())
          break label286;
        localQuickPayTransaction.i(((TextView)findViewById(2131296825)).getText().toString());
      }
      localDate = j();
      localQuickPayTransaction.h(getIntent().getBooleanExtra("is_editing", false));
      boolean bool1 = this.p.isChecked();
      localQuickPayTransaction.d(bool1);
      if (bool1)
      {
        boolean bool2 = this.q.isChecked();
        localQuickPayTransaction.c(bool2);
        if ((!bool2) && (s.m(this.r.getText().toString())))
          localQuickPayTransaction.a(Integer.parseInt(this.r.getText().toString()));
        if (G().b())
        {
          str1 = (String)G().getSelectedItem();
          localm = this.m;
          j = 0;
          label196: if (j >= localm.a.size())
            break label300;
          if (!((LabeledValue)localm.a.get(j)).a().equalsIgnoreCase(str1))
            break label294;
        }
      }
    }
    label286: label294: label300: for (String str2 = ((LabeledValue)localm.a.get(j)).b(); ; str2 = null)
    {
      localQuickPayTransaction.v(str2);
      localQuickPayTransaction.w(str1);
      localQuickPayTransaction.b(localDate);
      return localQuickPayTransaction;
      localPayFromAccount = (PayFromAccount)this.n.get(i);
      break;
      localQuickPayTransaction.i(null);
      break label71;
      j++;
      break label196;
    }
  }

  protected final boolean e()
  {
    return false;
  }

  protected final void f()
  {
    int i = 0;
    super.f();
    this.a = ((JPSpinner)findViewById(2131296823));
    this.b = ((TextView)findViewById(2131296822));
    this.c = ((TextView)findViewById(2131296818));
    this.p = ((CheckBox)findViewById(2131296829));
    this.r = ((EditText)findViewById(2131296835));
    this.q = ((CheckBox)findViewById(2131296836));
    if (getIntent().getBooleanExtra("send_money_for_request", false))
      ((Button)findViewById(2131296838)).setText("Reset");
    String str1;
    label292: boolean bool;
    label316: JPSpinner localJPSpinner;
    if (h())
    {
      this.p.setVisibility(8);
      findViewById(2131296819).setVisibility(8);
      this.a.setVisibility(8);
      this.c.setVisibility(0);
      this.b.setVisibility(0);
      this.b.setText(this.k.G());
      setTitle(2131165746);
      ((Button)findViewById(2131296838)).setText("Reset");
      this.b.setText(this.k.q());
      a(this.k.D());
      this.p.setChecked(this.k.D());
      if (this.k.D())
      {
        this.q.setChecked(this.k.C());
        EditText localEditText1 = this.r;
        if (this.k.H() != 0)
          break label426;
        str1 = "";
        localEditText1.setText(str1);
        EditText localEditText2 = this.r;
        if (this.k.C())
          break label440;
        bool = true;
        localEditText2.setEnabled(bool);
        localJPSpinner = G();
        m localm = this.m;
        String str2 = this.k.X();
        label344: if (i >= localm.a.size())
          break label452;
        if (!((LabeledValue)localm.a.get(i)).b().equalsIgnoreCase(str2))
          break label446;
      }
    }
    while (true)
    {
      localJPSpinner.setSelection(i);
      if (this.k.x() != null)
        ((TextView)findViewById(2131296825)).setText(this.k.x());
      return;
      setTitle(2131165683);
      break;
      label426: str1 = Integer.toString(this.k.H());
      break label292;
      label440: bool = false;
      break label316;
      label446: i++;
      break label344;
      label452: i = -1;
    }
  }

  public final boolean g()
  {
    boolean bool1 = super.g();
    if ((!h()) && (!((JPSpinner)findViewById(2131296823)).b()))
      e(2131296821);
    for (boolean bool2 = false; ; bool2 = bool1)
    {
      if (this.p.isChecked())
      {
        if ((!this.q.isChecked()) && (I() <= 0))
        {
          e(2131296834);
          bool2 = false;
        }
        if (G().getSelectedItemPosition() == -1)
        {
          e(2131296832);
          bool2 = false;
        }
      }
      return bool2;
    }
  }

  public final void k()
  {
    super.k();
    a(this.n);
  }

  public final void k(int paramInt)
  {
    if (((PayFromAccount)this.n.get(paramInt)).d())
    {
      findViewById(2131296824).setVisibility(0);
      findViewById(2131296825).setVisibility(0);
      findViewById(2131296831).setEnabled(false);
      ((Button)findViewById(2131296831)).setText(s.a(((ChaseApplication)getApplication()).b().c.j()));
      return;
    }
    findViewById(2131296824).setVisibility(8);
    findViewById(2131296825).setVisibility(8);
    findViewById(2131296831).setEnabled(true);
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    paramBundle.putString("FOOTNOTE", this.o);
    paramBundle.putSerializable("PAY_FROM_ACCOUNTS", this.n);
  }

  public static class a extends d<QuickPaySendMoneyActivity, Void, Void, QuickPayTransactionStartResponse>
  {
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.QuickPaySendMoneyActivity
 * JD-Core Version:    0.6.2
 */