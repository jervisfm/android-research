package com.chase.sig.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.ViewGroup;
import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.a.a;
import com.chase.sig.android.c;
import com.chase.sig.android.domain.QuickPayTransaction;
import com.chase.sig.android.service.quickpay.QuickPaySendTransactionConfirmResponse;
import com.chase.sig.android.util.s;
import com.chase.sig.android.view.aa;

public class QuickPaySendMoneyConfirmationActivity extends hm
{
  public final void a(Bundle paramBundle)
  {
    c.a();
    b(2130903170);
    setTitle(2131165300);
    QuickPaySendTransactionConfirmResponse localQuickPaySendTransactionConfirmResponse = (QuickPaySendTransactionConfirmResponse)getIntent().getExtras().getSerializable("quickPayConfirm");
    QuickPayTransaction localQuickPayTransaction = (QuickPayTransaction)getIntent().getExtras().getSerializable("sendTransaction");
    if ((s.m(localQuickPayTransaction.T())) && (!localQuickPayTransaction.U()) && (!localQuickPayTransaction.V()));
    for (int i = 1; ; i = 0)
    {
      if ((i != 0) && (paramBundle == null))
        f(localQuickPayTransaction.T());
      localQuickPayTransaction.z();
      localQuickPayTransaction.g(localQuickPaySendTransactionConfirmResponse.b());
      localQuickPayTransaction.h(localQuickPaySendTransactionConfirmResponse.c());
      localQuickPayTransaction.f(localQuickPaySendTransactionConfirmResponse.a());
      aa localaa = new aa(getBaseContext(), localQuickPayTransaction, (ChaseApplication)getApplication());
      ((ViewGroup)findViewById(2131296718)).addView(localaa);
      if ((!getIntent().hasExtra("qp_edit_payment")) || (localQuickPayTransaction.R()))
        break;
      a(QuickPayPendingTransactionsActivity.class, "Pending Transactions");
      return;
    }
    d();
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if (j(paramInt))
      return true;
    if (paramInt == 4)
    {
      a.a(paramInt, this, QuickPayHomeActivity.class);
      return true;
    }
    return super.onKeyDown(paramInt, paramKeyEvent);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.QuickPaySendMoneyConfirmationActivity
 * JD-Core Version:    0.6.2
 */