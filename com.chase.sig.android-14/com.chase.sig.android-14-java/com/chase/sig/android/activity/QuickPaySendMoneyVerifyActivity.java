package com.chase.sig.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.c.a;
import com.chase.sig.android.d;
import com.chase.sig.android.domain.QuickPayTransaction;
import com.chase.sig.android.service.quickpay.QuickPaySendTransactionConfirmResponse;
import com.chase.sig.android.util.s;
import com.chase.sig.android.view.aa;

public class QuickPaySendMoneyVerifyActivity extends ai
  implements fk.e, c.a
{
  QuickPayTransaction a = null;
  private String b;

  public final void a()
  {
    Intent localIntent = new Intent(this, QuickPayHomeActivity.class);
    localIntent.setFlags(67108864);
    startActivity(localIntent);
  }

  public final void a(Bundle paramBundle)
  {
    b(2130903183);
    setTitle(2131165695);
    this.a = ((QuickPayTransaction)getIntent().getExtras().getSerializable("sendTransaction"));
    aa localaa = new aa(getBaseContext(), this.a, (ChaseApplication)getApplication());
    this.b = ((String)getIntent().getExtras().getSerializable("FOOTNOTE"));
    if (s.m(this.b))
    {
      findViewById(2131296840).setPadding(n().getPaddingLeft(), n().getPaddingTop(), n().getPaddingRight(), 33);
      a(2131165674, this.b, true);
    }
    ((ViewGroup)findViewById(2131296718)).addView(localaa);
    Button localButton = (Button)findViewById(2131296842);
    localButton.setOnClickListener(new iw(this));
    if (this.a.B())
      localButton.setText(getString(2131165699));
    while (true)
    {
      ((Button)findViewById(2131296841)).setOnClickListener(new ix(this));
      return;
      localButton.setText(getString(2131165683));
    }
  }

  public static class a extends d<QuickPaySendMoneyVerifyActivity, Void, Void, QuickPaySendTransactionConfirmResponse>
  {
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.QuickPaySendMoneyVerifyActivity
 * JD-Core Version:    0.6.2
 */