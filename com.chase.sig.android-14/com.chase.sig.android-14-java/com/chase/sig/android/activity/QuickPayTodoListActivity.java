package com.chase.sig.android.activity;

import android.os.Bundle;
import android.text.TextUtils.TruncateAt;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.chase.sig.android.d;
import com.chase.sig.android.domain.QuickPayAcceptMoneyTransaction;
import com.chase.sig.android.domain.QuickPayActivityType;
import com.chase.sig.android.domain.QuickPayDeclineTransaction;
import com.chase.sig.android.domain.QuickPayTransaction;
import com.chase.sig.android.domain.o;
import com.chase.sig.android.service.TodoItem;
import com.chase.sig.android.service.quickpay.QuickPayTransactionListResponse;
import com.chase.sig.android.service.quickpay.TodoListResponse;
import com.chase.sig.android.util.Dollar;
import com.chase.sig.android.util.s;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class QuickPayTodoListActivity extends eb
  implements fk.e
{
  private View a;
  private List<TodoItem> b = new ArrayList();
  private ListView c;
  private boolean d;

  private String f()
  {
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = Integer.valueOf(r().c.d());
    return String.format("To Do List (%d)", arrayOfObject);
  }

  public final void a(Bundle paramBundle)
  {
    b(2130903185);
    this.c = ((ListView)findViewById(2131296849));
    if (paramBundle != null)
    {
      ((TextView)findViewById(2131296847)).setText(f());
      this.b = ((List)paramBundle.getSerializable("todoItems"));
      this.d = false;
      e();
    }
    while (true)
    {
      a(2131296846, a(QuickPayHomeActivity.class));
      return;
      b localb = (b)this.e.a(b.class);
      if (!localb.d())
      {
        Object[] arrayOfObject = new Object[1];
        arrayOfObject[0] = Integer.valueOf(1);
        localb.execute(arrayOfObject);
      }
    }
  }

  public final TodoListResponse d()
  {
    return r().c;
  }

  public final void e()
  {
    if ((r().c != null) && (r().c.d() > 0))
    {
      TextView localTextView2 = (TextView)findViewById(2131296847);
      localTextView2.setText(f());
      localTextView2.setVisibility(0);
    }
    if (r().c.d() <= 0)
    {
      TextView localTextView1 = (TextView)findViewById(2131296848);
      localTextView1.setText(r().c.m());
      localTextView1.setVisibility(0);
      return;
    }
    int i;
    if ((r().c.l() == 1) || (r().c.l() == 0))
    {
      this.b = r().c.c();
      i = r().c.d();
      if ((i <= 25) || (r().c.k() == 0))
        break label499;
      if (this.a != null)
        break label435;
      this.a = getLayoutInflater().inflate(2130903233, null);
      ((TextView)this.a.findViewById(2131296731)).setText(getString(2131165692));
      ((TextView)this.a.findViewById(2131296732)).setText(i + " items total,  " + (i - 25) + "  to load");
      this.a.setOnClickListener(new jh(this));
      this.c.addFooterView(this.a);
    }
    while (true)
    {
      this.c.setAdapter(new c(this.b));
      this.c.setItemsCanFocus(true);
      this.c.setSelectionFromTop(this.b.size() - r().c.d(), 0);
      String str = r().c.s();
      if (s.m(str))
      {
        d(str);
        ViewGroup localViewGroup = (ViewGroup)findViewById(2131296845);
        localViewGroup.setPadding(localViewGroup.getPaddingLeft(), localViewGroup.getPaddingTop(), localViewGroup.getPaddingRight(), 5 * localViewGroup.getPaddingLeft());
      }
      this.c.setVisibility(0);
      return;
      if (!this.d)
        break;
      this.b.addAll(r().c.c());
      break;
      label435: int j = i - 25 * r().c.l();
      ((TextView)this.a.findViewById(2131296732)).setText(i + " items total,  " + j + "  to load");
      continue;
      label499: this.c.removeFooterView(this.a);
    }
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    paramBundle.putSerializable("todoItems", (Serializable)this.b);
  }

  public static class a extends d<QuickPayTodoListActivity, Object, Void, QuickPayTransactionListResponse>
  {
    QuickPayTransaction a;
    TodoItem e;
    Class<?> f;
  }

  public static class b extends d<QuickPayTodoListActivity, Object, Void, TodoListResponse>
  {
  }

  public final class c extends BaseAdapter
  {
    List<TodoItem> a;

    public c()
    {
      Object localObject;
      this.a = localObject;
    }

    private View a(View paramView, TodoItem paramTodoItem, int paramInt1, String paramString, Class<?> paramClass, QuickPayTransaction paramQuickPayTransaction, boolean paramBoolean, int paramInt2)
    {
      View localView = paramView.findViewById(paramInt1);
      localView.setFocusable(true);
      localView.findViewById(2131296265).setVisibility(0);
      ((TextView)localView.findViewById(2131296938)).setText(paramString);
      ((TextView)localView.findViewById(2131296938)).setSingleLine(true);
      ((TextView)localView.findViewById(2131296938)).setEllipsize(TextUtils.TruncateAt.END);
      localView.setOnClickListener(new ji(this, paramBoolean, paramQuickPayTransaction, paramTodoItem, paramClass, paramInt2));
      if (s.m(paramString))
        localView.setContentDescription(paramString);
      return localView;
    }

    public final boolean areAllItemsEnabled()
    {
      return false;
    }

    public final int getCount()
    {
      return this.a.size();
    }

    public final Object getItem(int paramInt)
    {
      return this.a.get(paramInt);
    }

    public final long getItemId(int paramInt)
    {
      return Long.parseLong(((TodoItem)this.a.get(paramInt)).f());
    }

    public final View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      TodoItem localTodoItem = (TodoItem)this.a.get(paramInt);
      if (paramView == null);
      for (View localView1 = QuickPayTodoListActivity.this.getLayoutInflater().inflate(2130903232, paramViewGroup, false); ; localView1 = paramView)
      {
        String str = localTodoItem.a();
        Object localObject;
        QuickPayTransaction localQuickPayTransaction;
        if (QuickPayActivityType.d.a(localTodoItem.g()))
        {
          localObject = new QuickPayTransaction();
          a(localView1, localTodoItem, 2131296896, str, QuickPayViewTodoListDetailActivity.class, (QuickPayTransaction)localObject, true, -7);
          if (!QuickPayActivityType.d.a(localTodoItem.g()))
            break label258;
          localQuickPayTransaction = new QuickPayTransaction();
          localQuickPayTransaction.e(true);
        }
        label258: for (View localView2 = a(localView1, localTodoItem, 2131296954, localTodoItem.b(), QuickPaySendMoneyActivity.class, localQuickPayTransaction, QuickPayTodoListActivity.this.r().c.q(), -8); ; localView2 = a(localView1, localTodoItem, 2131296954, localTodoItem.b(), QuickPayAcceptMoneyVerifyActivity.class, new QuickPayAcceptMoneyTransaction(), QuickPayTodoListActivity.this.r().c.r(), -9))
        {
          TextView localTextView = (TextView)localView2.findViewById(2131296940);
          localTextView.setTextColor(-16750029);
          localTextView.setText(localTodoItem.c().toString());
          ((TextView)localView2.findViewById(2131296939)).setText(localTodoItem.d());
          ((TextView)localView2.findViewById(2131296938)).setTextColor(-16750029);
          a(localView1, localTodoItem, 2131296955, localTodoItem.e(), QuickPayDeclineVerifyActivity.class, new QuickPayDeclineTransaction(), true, -7);
          return localView1;
          localObject = new QuickPayAcceptMoneyTransaction();
          break;
        }
      }
    }

    public final boolean isEnabled(int paramInt)
    {
      return false;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.QuickPayTodoListActivity
 * JD-Core Version:    0.6.2
 */