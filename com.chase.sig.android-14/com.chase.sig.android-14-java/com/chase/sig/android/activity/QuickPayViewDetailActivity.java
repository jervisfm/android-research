package com.chase.sig.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.Button;
import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.d;
import com.chase.sig.android.domain.QuickPayActivityItem;
import com.chase.sig.android.domain.QuickPayActivityType;
import com.chase.sig.android.service.quickpay.QuickPayTransactionListResponse;
import com.chase.sig.android.view.aa;
import com.chase.sig.android.view.k;
import com.chase.sig.android.view.k.a;
import java.util.ArrayList;
import java.util.List;

public class QuickPayViewDetailActivity extends eb
  implements fk.e
{
  protected ArrayList<QuickPayActivityItem> a;
  protected boolean b;
  protected boolean c = false;
  protected QuickPayActivityItem d;

  public final void a(Bundle paramBundle)
  {
    b(2130903173);
    this.b = getIntent().getExtras().getBoolean("ACTIVITY_DETAIL_ORIGIN");
    this.c = getIntent().getExtras().getBoolean("ACTIVITY_DETAIL_OLD");
    if (paramBundle != null)
    {
      this.a = ((ArrayList)paramBundle.getSerializable("bundle_details"));
      a(this.a);
    }
    a locala;
    do
    {
      return;
      locala = (a)this.e.a(a.class);
    }
    while (locala.d());
    locala.execute(new Void[0]);
  }

  public final void a(String paramString)
  {
    k.a locala = new k.a(this);
    locala.a(paramString);
    locala.a("OK", new jk(this));
    locala.d(-1).show();
  }

  public final void a(List<QuickPayActivityItem> paramList)
  {
    QuickPayActivityItem localQuickPayActivityItem;
    if ((paramList != null) && (paramList.size() > 0))
    {
      localQuickPayActivityItem = (QuickPayActivityItem)paramList.get(0);
      if ((!QuickPayActivityType.d.a(localQuickPayActivityItem)) || (!this.c))
        break label154;
      setTitle(2131165670);
    }
    while (true)
    {
      this.d = ((QuickPayActivityItem)paramList.get(0));
      aa localaa = new aa(getBaseContext(), (QuickPayActivityItem)paramList.get(0), this.b, (ChaseApplication)getApplication());
      ViewGroup localViewGroup = (ViewGroup)findViewById(2131296718);
      if (localViewGroup.getChildCount() > 0)
        localViewGroup.removeAllViews();
      localViewGroup.addView(localaa);
      if (localaa.findViewById(2131296932) != null)
        ((Button)findViewById(2131296932)).setOnClickListener(new jj(this));
      return;
      label154: if (QuickPayActivityType.d.a(localQuickPayActivityItem))
        setTitle(2131165668);
      else if ((QuickPayActivityType.a.a(localQuickPayActivityItem)) && (this.c))
        setTitle(2131165669);
      else if (QuickPayActivityType.a.a(localQuickPayActivityItem))
        setTitle(2131165667);
      else if (QuickPayActivityType.b.a(localQuickPayActivityItem))
        setTitle(2131165742);
      else if (QuickPayActivityType.c.a(localQuickPayActivityItem))
        setTitle(2131165743);
    }
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    paramBundle.putSerializable("bundle_details", this.a);
  }

  public static class a extends d<QuickPayViewDetailActivity, Void, Void, QuickPayTransactionListResponse>
  {
  }

  public static class b extends d<QuickPayViewDetailActivity, QuickPayActivityItem, Void, QuickPayActivityItem>
  {
    private QuickPayActivityItem a;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.QuickPayViewDetailActivity
 * JD-Core Version:    0.6.2
 */