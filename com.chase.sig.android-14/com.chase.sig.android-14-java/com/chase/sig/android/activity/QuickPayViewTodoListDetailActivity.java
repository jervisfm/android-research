package com.chase.sig.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.Button;
import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.d;
import com.chase.sig.android.domain.LabeledValue;
import com.chase.sig.android.domain.QuickPayAcceptMoneyTransaction;
import com.chase.sig.android.domain.QuickPayActivityItem;
import com.chase.sig.android.domain.QuickPayActivityType;
import com.chase.sig.android.domain.QuickPayRecipient;
import com.chase.sig.android.domain.QuickPayTransaction;
import com.chase.sig.android.service.quickpay.QuickPayAcceptMoneyResponse;
import com.chase.sig.android.view.aa;
import java.util.ArrayList;
import java.util.List;

public class QuickPayViewTodoListDetailActivity extends eb
  implements fk.e
{
  protected ArrayList<QuickPayActivityItem> a;
  protected boolean b;
  protected boolean c = false;
  protected QuickPayActivityItem d;

  public static QuickPayRecipient a(ArrayList<QuickPayActivityItem> paramArrayList)
  {
    QuickPayRecipient localQuickPayRecipient = new QuickPayRecipient();
    QuickPayActivityItem localQuickPayActivityItem;
    if ((paramArrayList.size() > 0) && (paramArrayList.get(0) != null))
    {
      localQuickPayActivityItem = (QuickPayActivityItem)paramArrayList.get(0);
      localQuickPayRecipient.d(localQuickPayActivityItem.b());
      localQuickPayRecipient.a(localQuickPayActivityItem.o().b());
      if (localQuickPayActivityItem.z() != null)
        break label68;
    }
    label68: for (String str = ""; ; str = localQuickPayActivityItem.z().b())
    {
      localQuickPayRecipient.b(str);
      return localQuickPayRecipient;
    }
  }

  public static QuickPayTransaction a(List<QuickPayActivityItem> paramList, QuickPayTransaction paramQuickPayTransaction)
  {
    if ((paramList.size() > 0) && (paramList.get(0) != null))
    {
      QuickPayActivityItem localQuickPayActivityItem = (QuickPayActivityItem)paramList.get(0);
      paramQuickPayTransaction.d(localQuickPayActivityItem.d());
      if (localQuickPayActivityItem.y() != null)
        paramQuickPayTransaction.p(localQuickPayActivityItem.y().b());
      paramQuickPayTransaction.c(localQuickPayActivityItem.c());
      paramQuickPayTransaction.g(localQuickPayActivityItem.g());
      paramQuickPayTransaction.a(localQuickPayActivityItem.j());
      paramQuickPayTransaction.m(localQuickPayActivityItem.u().b());
      LabeledValue localLabeledValue = localQuickPayActivityItem.C();
      boolean bool1 = false;
      if (localLabeledValue != null)
      {
        boolean bool2 = "true".equalsIgnoreCase(localQuickPayActivityItem.C().b());
        bool1 = false;
        if (bool2)
          bool1 = true;
      }
      paramQuickPayTransaction.f(bool1);
      if ((localQuickPayActivityItem.A() != null) && ((paramQuickPayTransaction instanceof QuickPayAcceptMoneyTransaction)))
        ((QuickPayAcceptMoneyTransaction)paramQuickPayTransaction).a(localQuickPayActivityItem.A().b());
      if (localQuickPayActivityItem.w() != null)
        paramQuickPayTransaction.j(localQuickPayActivityItem.w().b());
      paramQuickPayTransaction.l(localQuickPayActivityItem.r().b());
      paramQuickPayTransaction.e(localQuickPayActivityItem.e());
    }
    return paramQuickPayTransaction;
  }

  private void a(List<QuickPayActivityItem> paramList)
  {
    QuickPayActivityItem localQuickPayActivityItem;
    Button localButton2;
    if ((paramList != null) && (paramList.size() > 0))
    {
      localQuickPayActivityItem = (QuickPayActivityItem)paramList.get(0);
      if ((!QuickPayActivityType.d.a(localQuickPayActivityItem)) || (!this.c))
        break label180;
      setTitle(2131165670);
      this.d = ((QuickPayActivityItem)paramList.get(0));
      aa localaa = new aa(getBaseContext(), (QuickPayActivityItem)paramList.get(0), this.b, (ChaseApplication)getApplication());
      ((ViewGroup)findViewById(2131296718)).addView(localaa);
      Button localButton1 = (Button)findViewById(2131296843);
      localButton2 = (Button)findViewById(2131296844);
      QuickPayRecipient localQuickPayRecipient = a(this.a);
      localButton1.setOnClickListener(new jl(this));
      if (!QuickPayActivityType.d.a(this.d))
        break label244;
      localButton2.setOnClickListener(new jm(this, localQuickPayRecipient));
    }
    label180: label244: 
    while (!QuickPayActivityType.a.a(this.d))
    {
      return;
      if (QuickPayActivityType.d.a(localQuickPayActivityItem))
      {
        setTitle(2131165668);
        break;
      }
      if ((QuickPayActivityType.a.a(localQuickPayActivityItem)) && (this.c))
      {
        setTitle(2131165669);
        break;
      }
      if (!QuickPayActivityType.a.a(localQuickPayActivityItem))
        break;
      setTitle(2131165667);
      break;
    }
    localButton2.setText("Accept Money");
    localButton2.setOnClickListener(new jn(this));
  }

  public final void a(Bundle paramBundle)
  {
    b(2130903184);
    this.b = getIntent().getExtras().getBoolean("ACTIVITY_DETAIL_ORIGIN");
    this.c = getIntent().getExtras().getBoolean("ACTIVITY_DETAIL_OLD");
    if ((paramBundle != null) && (paramBundle.containsKey("bundle_details")))
    {
      this.a = ((ArrayList)paramBundle.getSerializable("bundle_details"));
      a(this.a);
    }
    while (!getIntent().hasExtra("quick_pay_details"))
      return;
    this.a = ((ArrayList)getIntent().getSerializableExtra("quick_pay_details"));
    a(this.a);
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    paramBundle.putSerializable("bundle_details", this.a);
  }

  public static class a extends d<QuickPayViewTodoListDetailActivity, QuickPayAcceptMoneyTransaction, Void, QuickPayAcceptMoneyResponse>
  {
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.QuickPayViewTodoListDetailActivity
 * JD-Core Version:    0.6.2
 */