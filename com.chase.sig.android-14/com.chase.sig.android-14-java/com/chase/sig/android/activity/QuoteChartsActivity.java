package com.chase.sig.android.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.AsyncTask.Status;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.View;
import android.widget.TextView;
import com.chase.sig.android.domain.Chart;
import com.chase.sig.android.domain.ImageDownloadResponse;
import com.chase.sig.android.domain.Quote;
import com.chase.sig.android.util.s;
import com.chase.sig.android.view.JPTabWidget;
import com.chase.sig.android.view.k.a;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class QuoteChartsActivity extends b
{
  protected Quote b;
  private int c = 0;
  private JPTabWidget d;

  public QuoteChartsActivity()
  {
    super(2130837622, 2131296593);
  }

  private Chart k(int paramInt)
  {
    switch (paramInt)
    {
    default:
      return Chart.a;
    case 0:
      return this.b.q();
    case 1:
    }
    return this.b.r();
  }

  public final void a(Bundle paramBundle)
  {
    b(2130903189);
    findViewById(2131296300).setVisibility(8);
    this.b = ((Quote)getIntent().getExtras().get("transaction_object"));
    if (paramBundle != null)
    {
      this.c = paramBundle.getInt("tabSelection");
      b(paramBundle);
    }
    while (true)
    {
      this.d = ((JPTabWidget)findViewById(2131296590));
      this.d.a(getIntent().getStringExtra("exchangeName"));
      this.d.a(0, 2131165888);
      this.d.a(1, 2131165889);
      this.d.setTabListener(new jp(this));
      this.d.a(this.c, true);
      this.d = ((JPTabWidget)findViewById(2131296590));
      GestureDetector localGestureDetector = new GestureDetector(new jq(this));
      findViewById(2131296593).setOnTouchListener(new jr(this, localGestureDetector));
      String str1 = getString(2131165890);
      String str2 = s.w(this.b.j());
      ((TextView)findViewById(2131296856)).setText(str1 + " " + str2);
      return;
      Chart[] arrayOfChart = new Chart[2];
      arrayOfChart[0] = k(0);
      arrayOfChart[1] = k(1);
      List localList = Arrays.asList(arrayOfChart);
      Chart localChart = (Chart)localList.get(0);
      a locala = (a)this.e.a(a.class);
      if (locala.getStatus() != AsyncTask.Status.RUNNING)
        locala.execute(new Object[] { localList, localChart });
    }
  }

  protected final void a(Map<Chart, ImageDownloadResponse> paramMap, Chart paramChart)
  {
    Iterator localIterator = paramMap.entrySet().iterator();
    while (localIterator.hasNext())
      if (((Map.Entry)localIterator.next()).getValue() == ImageDownloadResponse.a)
        showDialog(35);
    super.a(paramMap, paramChart);
  }

  protected final void c_()
  {
    super.c_();
    finish();
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    k.a locala1 = new k.a(this);
    switch (paramInt)
    {
    default:
      return super.onCreateDialog(paramInt);
    case 35:
    }
    k.a locala2 = locala1.b(2131165894);
    locala2.i = false;
    locala2.a(2131165288, new jo(this)).c(2131165893);
    return locala1.d(-1);
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    c(paramBundle);
    paramBundle.putInt("tabSelection", this.c);
  }

  protected static class a extends com.chase.sig.android.b<b, Object, Void, Map<Chart, ImageDownloadResponse>>
  {
    protected List<Chart> a;
    protected Chart e;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.QuoteChartsActivity
 * JD-Core Version:    0.6.2
 */