package com.chase.sig.android.activity;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask.Status;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.chase.sig.android.activity.a.f;
import com.chase.sig.android.d;
import com.chase.sig.android.domain.Quote;
import com.chase.sig.android.domain.n;
import com.chase.sig.android.service.QuoteNewsResponse;
import com.chase.sig.android.util.Dollar;
import com.chase.sig.android.util.e;
import com.chase.sig.android.util.s;
import com.chase.sig.android.view.TickerValueTextView;
import com.chase.sig.android.view.detail.DetailView;

public class QuoteDetailsActivity extends b
{
  private String b;
  private String c;
  private String d;
  private Dollar k;
  private Quote l;
  private String m;
  private DetailView n;
  private QuoteNewsResponse o;
  private ag<QuoteDetailsActivity> p;
  private boolean q;

  public QuoteDetailsActivity()
  {
    super(2130837623, 2131296593);
  }

  private void a(int paramInt, CharSequence paramCharSequence)
  {
    ((TextView)findViewById(paramInt)).setText(paramCharSequence);
  }

  public static void a(Context paramContext, String paramString)
  {
    Intent localIntent = new Intent(paramContext, QuoteDetailsActivity.class);
    localIntent.putExtra("ticker_symbol", paramString);
    localIntent.putExtra("quantity", "0");
    localIntent.putExtra("from_quote_search", true);
    paramContext.startActivity(localIntent);
  }

  private void e()
  {
    b localb = (b)this.e.a(b.class);
    Object[] arrayOfObject;
    if (localb.getStatus() != AsyncTask.Status.RUNNING)
    {
      arrayOfObject = new Object[2];
      if (this.c == null)
        break label58;
    }
    label58: for (String str = this.c; ; str = this.b)
    {
      arrayOfObject[0] = str;
      arrayOfObject[1] = this.d;
      localb.execute(arrayOfObject);
      return;
    }
  }

  private void f()
  {
    findViewById(2131296593).setOnClickListener(new jt(this));
  }

  public final void a(Bundle paramBundle)
  {
    setTitle(2131165874);
    b(2130903190);
    findViewById(2131296854).setVisibility(4);
    Bundle localBundle = getIntent().getExtras();
    Button localButton = (Button)findViewById(2131296435);
    if (localBundle.containsKey("ticker_symbol"))
      this.b = localBundle.getString("ticker_symbol");
    if (localBundle.containsKey("quote_code"))
      this.c = ((String)e.a(localBundle, "quote_code", null));
    if (localBundle.containsKey("quantity"))
      this.d = localBundle.getString("quantity");
    if (localBundle.containsKey("gain_loss"))
    {
      this.m = localBundle.getString("gain_loss");
      if (s.m(this.m))
        this.k = new Dollar(this.m);
    }
    if (localBundle.containsKey("from_quote_search"))
      this.q = localBundle.getBoolean("from_quote_search");
    localButton.setOnClickListener(a(PrivateBankingDisclosuresActivity.class).a("title", localButton.getText().toString()));
    if (e.a(paramBundle, "quote"))
    {
      this.l = ((Quote)paramBundle.getSerializable("quote"));
      d();
      b(paramBundle);
      b(this.l.q());
      f();
      findViewById(2131296854).setVisibility(0);
    }
    while (true)
    {
      a(1000, 2130837565, 2130837564, new js(this));
      a(1001, 2130837571, 2130837570, new f(this, QuoteSearchActivity.class).d());
      ((TextView)findViewById(2131296594)).setText(2131165881);
      this.n = ((DetailView)findViewById(2131296533));
      if (this.p == null)
        this.p = new ag();
      if (paramBundle != null)
        this.o = ag.a(paramBundle);
      ag localag = this.p;
      QuoteNewsResponse localQuoteNewsResponse = this.o;
      DetailView localDetailView = this.n;
      String[] arrayOfString = new String[1];
      arrayOfString[0] = this.b;
      localag.a(this, localQuoteNewsResponse, localDetailView, a.class, arrayOfString);
      return;
      e();
    }
  }

  public final void d()
  {
    if (this.l == null)
      return;
    if (this.k == null)
      this.k = this.l.g();
    TickerValueTextView localTickerValueTextView1 = (TickerValueTextView)findViewById(2131296871);
    TextView localTextView = (TextView)findViewById(2131296872);
    View localView = findViewById(2131296869);
    if (this.q)
    {
      localTickerValueTextView1.setVisibility(4);
      localTextView.setVisibility(4);
      localView.setVisibility(4);
    }
    while (true)
    {
      TickerValueTextView localTickerValueTextView2 = (TickerValueTextView)findViewById(2131296867);
      a(2131296864, this.l.i().h());
      localTickerValueTextView2.setDisplayPlaceholder(true);
      localTickerValueTextView2.setShowNotApplicable(true);
      localTickerValueTextView2.a(this.l.b(), s.y(this.l.d()));
      a(2131296625, "As of " + s.w(this.l.j()));
      a(2131296859, this.l.p());
      a(2131296875, s.a(this.l.m(), "###,###,###") + "/" + s.a(this.l.a(), "###,###,###"));
      a(2131296880, "High: " + s.a(this.l.h()));
      a(2131296881, "Low: " + s.a(this.l.k()));
      a(2131296882, "High: " + s.a(this.l.n()));
      a(2131296883, "Low: " + s.a(this.l.o()));
      return;
      if (this.k != null)
      {
        localTickerValueTextView1.setDisplayPlaceholder(true);
        localTickerValueTextView1.setShowNotApplicable(true);
        localTickerValueTextView1.setTickerValue(this.l.g());
        localTickerValueTextView1.setVisibility(0);
        localTextView.setText(s.a(Double.valueOf(this.d), "#,##0.00"));
        localTextView.setTextColor(localTickerValueTextView1.getCurrentTextColor());
        localTextView.setVisibility(0);
        localView.setVisibility(0);
      }
    }
  }

  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    if ((paramInt1 == 35) && (paramInt2 == 35))
      e();
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    if (this.l != null)
    {
      paramBundle.putSerializable("quote", this.l);
      c(paramBundle);
    }
    paramBundle.putSerializable("quote_news_response", this.o);
  }

  public static class a extends d<QuoteDetailsActivity, String, Void, QuoteNewsResponse>
  {
  }

  public static class b extends d<QuoteDetailsActivity, Object, Void, n>
  {
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.QuoteDetailsActivity
 * JD-Core Version:    0.6.2
 */