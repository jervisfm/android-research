package com.chase.sig.android.activity;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import com.chase.sig.android.domain.Quote;
import com.chase.sig.android.domain.QuoteResponse;
import com.chase.sig.android.service.IServiceError;
import com.chase.sig.android.util.e;
import com.chase.sig.android.util.s;
import java.util.ArrayList;
import java.util.Iterator;

public class QuoteSearchActivity extends ai
  implements fk.c
{
  private EditText a;
  private ListView b;
  private ArrayAdapter<c> c;
  private QuoteResponse d;
  private String k;
  private TextView l;
  private final Handler m = new Handler();

  private void a(QuoteResponse paramQuoteResponse)
  {
    this.c.clear();
    if ((paramQuoteResponse.e()) && (paramQuoteResponse.c("20160")));
    for (String str = paramQuoteResponse.d("20160").a(); ; str = "")
    {
      this.l.setText(str);
      if (paramQuoteResponse.a() == null)
        break;
      Iterator localIterator = paramQuoteResponse.a().iterator();
      while (localIterator.hasNext())
      {
        Quote localQuote = (Quote)localIterator.next();
        this.c.add(new c(localQuote, (byte)0));
      }
    }
    this.b.setSelectionAfterHeaderView();
    e();
  }

  private void a(String paramString, QuoteResponse paramQuoteResponse)
  {
    this.k = paramString;
    this.d = paramQuoteResponse;
  }

  private void d()
  {
    View localView = findViewById(2131296475);
    if ((localView != null) && (localView.getVisibility() == 8))
    {
      Animation localAnimation = AnimationUtils.loadAnimation(this, 2130968580);
      localView.setVisibility(0);
      localView.startAnimation(localAnimation);
    }
  }

  private void e()
  {
    View localView = findViewById(2131296475);
    if ((localView != null) && (localView.getVisibility() == 0))
    {
      localView.clearAnimation();
      localView.setVisibility(8);
    }
  }

  public final void a(Bundle paramBundle)
  {
    this.f = getString(2131165855);
    setTitle(2131165895);
    b(2130903191);
    this.d = ((QuoteResponse)e.a(paramBundle, "QuoteSearchResponse", new QuoteResponse()));
    this.k = ((String)e.a(paramBundle, "QuoteSearchTerm", null));
    this.a = ((EditText)findViewById(2131296887));
    this.l = ((TextView)findViewById(2131296892));
    this.b = ((ListView)findViewById(2131296890));
    this.b.setEmptyView(findViewById(2131296891));
    this.c = new ArrayAdapter(this, 2130903192, 2131296277);
    this.c.setNotifyOnChange(true);
    a(this.b);
    this.b.setAdapter(this.c);
    this.b.setOnItemClickListener(new ju(this));
    EditText localEditText = this.a;
    Drawable localDrawable = getResources().getDrawable(2130837692);
    localDrawable.setBounds(0, 0, localDrawable.getIntrinsicWidth(), localDrawable.getIntrinsicHeight());
    localEditText.setCompoundDrawables(null, null, localDrawable, null);
    this.a.addTextChangedListener(new b(this.m));
    a(this.d);
  }

  public final void a(String paramString)
  {
    if (s.l(paramString))
    {
      QuoteResponse localQuoteResponse = new QuoteResponse();
      a(paramString, localQuoteResponse);
      a(localQuoteResponse);
      return;
    }
    if ((paramString.equals(this.k)) && (this.d != null))
    {
      a(this.d);
      return;
    }
    if (a.a(this, paramString))
    {
      d();
      return;
    }
    a.a(this);
    a(a.class, new String[] { paramString });
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    paramBundle.putSerializable("QuoteSearchResponse", this.d);
    paramBundle.putString("QuoteSearchTerm", this.k);
  }

  public static class a extends ae<QuoteSearchActivity, String, Void, QuoteResponse>
  {
    private String a;

    public static void a(QuoteSearchActivity paramQuoteSearchActivity)
    {
      a locala = (a)paramQuoteSearchActivity.e.a(a.class);
      if ((locala != null) && (locala.d()))
        locala.cancel(true);
    }

    public static boolean a(QuoteSearchActivity paramQuoteSearchActivity, String paramString)
    {
      a locala = (a)paramQuoteSearchActivity.e.a(a.class);
      return (locala != null) && (paramString.equals(locala.a));
    }

    protected void onPreExecute()
    {
      QuoteSearchActivity.a((QuoteSearchActivity)this.b);
    }
  }

  private final class b
    implements TextWatcher
  {
    private Runnable b;
    private Handler c;

    public b(Handler arg2)
    {
      Object localObject;
      this.c = localObject;
    }

    public final void afterTextChanged(Editable paramEditable)
    {
      String str = paramEditable.toString();
      this.c.removeCallbacks(this.b);
      this.b = new jv(this, str);
      this.c.postDelayed(this.b, 500L);
    }

    public final void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
    {
    }

    public final void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
    {
    }
  }

  private final class c
  {
    final String a;
    private final String c;
    private String d;

    private c(Quote arg2)
    {
      Object localObject;
      this.d = localObject.e();
      this.a = localObject.l();
      this.c = localObject.f();
    }

    public final String toString()
    {
      Object[] arrayOfObject = new Object[3];
      arrayOfObject[0] = this.a;
      arrayOfObject[1] = this.d;
      arrayOfObject[2] = this.c;
      return String.format("%s - %s (%s)", arrayOfObject);
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.QuoteSearchActivity
 * JD-Core Version:    0.6.2
 */