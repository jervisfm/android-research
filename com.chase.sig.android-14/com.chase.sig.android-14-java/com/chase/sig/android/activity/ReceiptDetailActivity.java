package com.chase.sig.android.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.chase.sig.android.d;
import com.chase.sig.android.domain.IAccount;
import com.chase.sig.android.domain.LabeledValue;
import com.chase.sig.android.domain.Receipt;
import com.chase.sig.android.domain.g;
import com.chase.sig.android.domain.o;
import com.chase.sig.android.service.l;
import com.chase.sig.android.util.Dollar;
import com.chase.sig.android.util.e;
import com.chase.sig.android.util.s;
import com.chase.sig.android.view.detail.DetailRow;
import com.chase.sig.android.view.detail.DetailView;
import com.chase.sig.android.view.detail.a;
import com.chase.sig.android.view.k.a;
import java.util.ArrayList;

public class ReceiptDetailActivity extends ai
{
  private Button a;
  private Button b;
  private Receipt c;
  private boolean d = false;

  public final void a(Bundle paramBundle)
  {
    b(2130903193);
    setTitle(2131165797);
    this.c = ((Receipt)e.a(paramBundle, getIntent(), "receipt"));
    this.d = getIntent().getBooleanExtra("is_browsing_receipts", false);
    Receipt localReceipt = this.c;
    DetailView localDetailView = (DetailView)findViewById(2131296893);
    ArrayList localArrayList = new ArrayList();
    localArrayList.add(new DetailRow("Description", localReceipt.c()));
    localArrayList.add(new DetailRow("Amount", localReceipt.b().toString()));
    IAccount localIAccount = r().b.a(localReceipt.a());
    if (getIntent().getExtras().getBoolean("show_all_details"))
    {
      localArrayList.add(new DetailRow("Transaction date", s.i(localReceipt.e())));
      if (localIAccount == null)
        break label419;
    }
    label419: for (String str = localIAccount.w().toString(); ; str = "NA")
    {
      localArrayList.add(new DetailRow("Account", str));
      localArrayList.add(new DetailRow("Status", localReceipt.d()));
      if (localReceipt.k() != null)
        localArrayList.add(new DetailRow("Category", localReceipt.k().a()));
      if (localReceipt.l() != null)
        localArrayList.add(new DetailRow("Expense type", localReceipt.l().a()));
      if (localReceipt.m() != null)
        localArrayList.add(new DetailRow("Tax deduction", localReceipt.m().a()));
      localDetailView.setRows((a[])localArrayList.toArray(new a[localArrayList.size()]));
      localDetailView.c();
      this.a = ((Button)findViewById(2131296894));
      this.a.setText(2131165804);
      this.a.setOnClickListener(new jx(this));
      this.b = ((Button)findViewById(2131296895));
      this.b.setText(2131165803);
      this.b.setOnClickListener(new ka(this));
      findViewById(2131296418).setOnClickListener(new jw(this));
      return;
    }
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    k.a locala1 = new k.a(this);
    switch (paramInt)
    {
    default:
      return super.onCreateDialog(paramInt);
    case 0:
    }
    k.a locala2 = locala1.b(2131165814);
    locala2.i = true;
    locala2.a(2131165805, new jz(this)).b(2131165815, new jy(this));
    return locala1.d(-1);
  }

  public static class a extends d<ReceiptDetailActivity, Receipt, Void, l>
  {
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.ReceiptDetailActivity
 * JD-Core Version:    0.6.2
 */