package com.chase.sig.android.activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.ProgressBar;
import com.chase.sig.android.domain.ReceiptPhotoList;
import com.chase.sig.android.domain.ReceiptPhotoList.ReceiptPhoto;
import com.chase.sig.android.e.a;
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ReceiptPhotosViewActivity extends ai
{
  private static int l = 2130837636;
  private static int m = 2130837512;
  private static int n = 2130837512;
  d a;
  Gallery b;
  ImageView c;
  ProgressBar d;
  ReceiptPhotoList.ReceiptPhoto k;

  public final void a(Bundle paramBundle)
  {
    b(2130903196);
    Bundle localBundle = getIntent().getExtras();
    this.a = ((d)getLastNonConfigurationInstance());
    if ((this.a == null) && (localBundle != null))
    {
      this.a = new d((byte)0);
      this.a.a = ((ReceiptPhotoList)localBundle.getSerializable("receipt_photo_list"));
    }
    Iterator localIterator = this.a.a.a().iterator();
    while (localIterator.hasNext())
    {
      ReceiptPhotoList.ReceiptPhoto localReceiptPhoto = (ReceiptPhotoList.ReceiptPhoto)localIterator.next();
      new b((byte)0).execute(new ReceiptPhotoList.ReceiptPhoto[] { localReceiptPhoto });
    }
    this.b = ((Gallery)findViewById(2131296901));
    this.c = ((ImageView)findViewById(2131296489));
    this.d = ((ProgressBar)findViewById(2131296902));
    this.b.setOnItemSelectedListener(new kb(this));
  }

  protected final void m()
  {
  }

  protected void onDestroy()
  {
    BitmapDrawable localBitmapDrawable = (BitmapDrawable)this.c.getDrawable();
    if (localBitmapDrawable != null)
    {
      localBitmapDrawable.getBitmap().recycle();
      System.gc();
    }
    super.onDestroy();
  }

  protected void onResume()
  {
    this.b.setAdapter(new c(this, this.a));
    super.onResume();
  }

  public Object onRetainNonConfigurationInstance()
  {
    return this.a;
  }

  final class a extends AsyncTask<ReceiptPhotoList.ReceiptPhoto, Void, Bitmap>
  {
    private final WeakReference<ImageView> b;
    private final WeakReference<ProgressBar> c;

    public a(ImageView paramProgressBar, ProgressBar arg3)
    {
      this.b = new WeakReference(paramProgressBar);
      Object localObject;
      this.c = new WeakReference(localObject);
    }
  }

  private final class b extends AsyncTask<ReceiptPhotoList.ReceiptPhoto, Void, ReceiptPhotoList.ReceiptPhoto>
  {
    private final int b = 96;
    private final int c = 96;

    private b()
    {
    }
  }

  private static final class c extends BaseAdapter
  {
    ReceiptPhotosViewActivity.d a;
    List<ReceiptPhotoList.ReceiptPhoto> b;
    Context c;
    Gallery d;
    int e;

    public c(Context paramContext, ReceiptPhotosViewActivity.d paramd)
    {
      this.c = paramContext;
      this.a = paramd;
      this.b = paramd.a.a();
      TypedArray localTypedArray = this.c.obtainStyledAttributes(e.a.Gallery1);
      this.e = localTypedArray.getResourceId(0, 0);
      localTypedArray.recycle();
    }

    public final int getCount()
    {
      return this.b.size();
    }

    public final Object getItem(int paramInt)
    {
      return this.b.get(paramInt);
    }

    public final long getItemId(int paramInt)
    {
      return paramInt;
    }

    public final View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      a locala;
      if (paramView == null)
      {
        paramView = LayoutInflater.from(this.c).inflate(2130903195, paramViewGroup, false);
        locala = new a(paramView);
        paramView.setTag(locala);
        this.d = ((Gallery)paramViewGroup.findViewById(2131296901));
        ReceiptPhotoList.ReceiptPhoto localReceiptPhoto = (ReceiptPhotoList.ReceiptPhoto)this.b.get(paramInt);
        Bitmap localBitmap = (Bitmap)this.a.b.get(localReceiptPhoto.a());
        if (localBitmap == null)
          break label144;
        locala.b.setVisibility(4);
        locala.a.setVisibility(0);
        locala.a.setImageBitmap(localBitmap);
      }
      while (true)
      {
        locala.a.setBackgroundResource(this.e);
        return paramView;
        locala = (a)paramView.getTag();
        break;
        label144: locala.b.setVisibility(0);
        locala.a.setVisibility(4);
      }
    }

    private static final class a
    {
      public ImageView a;
      public ProgressBar b;

      public a(View paramView)
      {
        this.a = ((ImageView)paramView.findViewById(2131296899));
        this.b = ((ProgressBar)paramView.findViewById(2131296900));
      }
    }
  }

  private static final class d
  {
    public ReceiptPhotoList a;
    public Map<String, Bitmap> b = new ConcurrentHashMap();
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.ReceiptPhotosViewActivity
 * JD-Core Version:    0.6.2
 */