package com.chase.sig.android.activity;

import android.content.Intent;
import android.os.AsyncTask.Status;
import android.os.Bundle;
import com.chase.sig.android.d;
import com.chase.sig.android.domain.ReceiptAccount;
import com.chase.sig.android.domain.ReceiptAccountSummary;
import com.chase.sig.android.service.ListContentResponse;
import com.chase.sig.android.util.e;
import com.chase.sig.android.view.detail.DetailRowWithSubItem;
import com.chase.sig.android.view.detail.DetailView;
import com.chase.sig.android.view.detail.a;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ReceiptsBrowseActivity extends ai
{
  private DetailView a;
  private ListContentResponse b;
  private ReceiptAccountSummary c;

  public final void a(Bundle paramBundle)
  {
    b(2130903197);
    setTitle(2131165808);
    Bundle localBundle = getIntent().getExtras();
    if (localBundle.containsKey("acctSummary"))
    {
      this.c = ((ReceiptAccountSummary)localBundle.getSerializable("acctSummary"));
      this.a = ((DetailView)findViewById(2131296903));
      ReceiptAccountSummary localReceiptAccountSummary = this.c;
      ArrayList localArrayList = new ArrayList();
      Iterator localIterator = localReceiptAccountSummary.c().iterator();
      while (localIterator.hasNext())
      {
        ReceiptAccount localReceiptAccount = (ReceiptAccount)localIterator.next();
        String str = localReceiptAccount.b();
        a locala1 = new DetailRowWithSubItem(str, localReceiptAccount.d() + " receipts / " + localReceiptAccount.a() + " unmatched", null).d();
        locala1.h = new kc(this, str, localReceiptAccount);
        localArrayList.add(locala1);
      }
      a[] arrayOfa = new a[localArrayList.size()];
      this.a.setRows((a[])localArrayList.toArray(arrayOfa));
    }
    this.b = ((ListContentResponse)e.a(paramBundle, getIntent(), "categories"));
    if (this.b == null)
    {
      a locala = (a)this.e.a(a.class);
      if (locala.getStatus() != AsyncTask.Status.RUNNING)
        locala.execute(new String[0]);
    }
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    if (this.b != null)
      paramBundle.putSerializable("categories", this.b);
  }

  public static class a extends d<ReceiptsBrowseActivity, String, Void, ListContentResponse>
  {
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.ReceiptsBrowseActivity
 * JD-Core Version:    0.6.2
 */