package com.chase.sig.android.activity;

import android.content.Intent;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.ShutterCallback;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.view.KeyEvent;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

public class ReceiptsCameraCaptureActivity extends ai
  implements fk.a
{
  Camera.ShutterCallback a = new kg(this);
  Camera.PictureCallback b = new kh(this);
  Camera.PictureCallback c = new ki(this);
  private ImageView d;
  private FrameLayout k;

  private void d()
  {
    ((TextView)findViewById(2131296308)).setText(2131165430);
  }

  public final void a(Bundle paramBundle)
  {
    CapturePreview localCapturePreview = new CapturePreview(this, 30, CapturePreview.Orientation.b);
    this.k = ((FrameLayout)findViewById(2131296319));
    ((FrameLayout)findViewById(2131296306)).addView(localCapturePreview);
    Button localButton = (Button)findViewById(2131296310);
    localButton.setVisibility(0);
    Spanned localSpanned = Html.fromHtml(getText(2131165819).toString());
    ((TextView)findViewById(2131296318)).setText(localSpanned);
    this.d = ((ImageView)findViewById(2131296316));
    localButton.setOnClickListener(new kd(this, localButton));
    this.d.setVisibility(0);
    this.d.setOnClickListener(new ke(this, localCapturePreview));
  }

  protected final void a_()
  {
    requestWindowFeature(1);
    setContentView(2130903199);
  }

  public void onBackPressed()
  {
    if (getIntent().getSerializableExtra("receipt_photo_list") != null)
    {
      showDialog(-4);
      return;
    }
    super.onBackPressed();
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if (paramInt == 27)
    {
      if (paramKeyEvent.getRepeatCount() == 0)
        this.d.performClick();
      return true;
    }
    return super.onKeyDown(paramInt, paramKeyEvent);
  }

  protected void onResume()
  {
    super.onResume();
    d();
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.ReceiptsCameraCaptureActivity
 * JD-Core Version:    0.6.2
 */