package com.chase.sig.android.activity;

import android.app.Dialog;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import com.chase.sig.android.b;
import com.chase.sig.android.d;
import com.chase.sig.android.service.ReceiptsEnrollmentResponse;
import com.chase.sig.android.service.content.ContentResponse;
import com.chase.sig.android.util.e;
import com.chase.sig.android.view.k.a;

public class ReceiptsCancelActivity extends ai
{
  private static int a = 0;
  private String b;

  public final void a(Bundle paramBundle)
  {
    b(2130903198);
    setTitle(2131165793);
    this.b = ((String)e.a(paramBundle, getIntent(), "resultMessage"));
    WebView localWebView = (WebView)findViewById(2131296908);
    localWebView.getSettings().setSaveFormData(false);
    localWebView.setWebViewClient(new kj(this));
    a(2131296906, C());
    ((Button)findViewById(2131296907)).setOnClickListener(new kk(this));
    a(b.class, new Void[0]);
  }

  public final void a(String paramString)
  {
    this.b = paramString;
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    if (paramInt == a)
    {
      k.a locala = new k.a(this);
      locala.a(this.b).a(2131165288, new kl(this));
      locala.g = new km(this);
      return locala.d(-1);
    }
    return super.onCreateDialog(paramInt);
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    paramBundle.putString("resultMessage", this.b);
  }

  public static class a extends d<ReceiptsCancelActivity, Void, Void, ReceiptsEnrollmentResponse>
  {
  }

  public static class b extends b<ReceiptsCancelActivity, Void, Void, ContentResponse>
  {
    protected void onCancelled()
    {
      ((ReceiptsCancelActivity)this.b).finish();
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.ReceiptsCancelActivity
 * JD-Core Version:    0.6.2
 */