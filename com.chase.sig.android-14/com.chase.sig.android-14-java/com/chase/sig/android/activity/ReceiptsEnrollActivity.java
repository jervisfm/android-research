package com.chase.sig.android.activity;

import android.content.res.Resources;
import android.os.Bundle;
import android.text.Html;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import com.chase.sig.android.b;
import com.chase.sig.android.domain.o;
import com.chase.sig.android.service.SplashResponse;
import com.chase.sig.android.service.content.ContentResponse;
import com.chase.sig.android.util.s;

public class ReceiptsEnrollActivity extends ai
{
  public final void a(Bundle paramBundle)
  {
    b(2130903200);
    setTitle(Html.fromHtml(getResources().getString(2131165783) + s.z(getResources().getString(2131165784))));
    if ((r().d != null) && (r().d.l()))
      e(r().d.s());
    for (int i = 1; ; i = 0)
    {
      if (i == 0)
      {
        WebView localWebView = (WebView)findViewById(2131296911);
        localWebView.getSettings().setSaveFormData(false);
        localWebView.setWebViewClient(new kn(this));
        a(a.class, new Void[0]);
        ((Button)findViewById(2131296681)).setText(Html.fromHtml(getString(2131165782)));
        a(2131296681, a(ReceiptsSelectPlanActivity.class));
      }
      return;
    }
  }

  public static class a extends b<ReceiptsEnrollActivity, Void, Void, ContentResponse>
  {
    protected void onCancelled()
    {
      ((ReceiptsEnrollActivity)this.b).finish();
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.ReceiptsEnrollActivity
 * JD-Core Version:    0.6.2
 */