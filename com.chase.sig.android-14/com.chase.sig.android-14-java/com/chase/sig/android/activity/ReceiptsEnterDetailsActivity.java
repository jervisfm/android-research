package com.chase.sig.android.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.chase.sig.android.domain.AccountActivity;
import com.chase.sig.android.domain.ActivityValue;
import com.chase.sig.android.domain.CacheActivityData;
import com.chase.sig.android.domain.IAccount;
import com.chase.sig.android.domain.LabeledValue;
import com.chase.sig.android.domain.Receipt;
import com.chase.sig.android.domain.ReceiptPhotoList;
import com.chase.sig.android.domain.g;
import com.chase.sig.android.domain.o;
import com.chase.sig.android.service.ListContentResponse;
import com.chase.sig.android.service.ReceiptsAddResponse;
import com.chase.sig.android.util.Dollar;
import com.chase.sig.android.util.s;
import com.chase.sig.android.view.JPSpinner;
import com.chase.sig.android.view.b.a;
import com.chase.sig.android.view.detail.DetailRow;
import com.chase.sig.android.view.detail.DetailView;
import com.chase.sig.android.view.detail.c;
import com.chase.sig.android.view.detail.q;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ReceiptsEnterDetailsActivity extends a
{
  private String A = s.a(Calendar.getInstance().getTime());
  private b.a B = new kp(this);
  private List<IAccount> a;
  private List<LabeledValue> b;
  private List<LabeledValue> c;
  private List<LabeledValue> d;
  private String k = "";
  private String l;
  private String m = "";
  private String n = "";
  private String o;
  private Receipt p;
  private ReceiptPhotoList q;
  private TextView r;
  private DetailView s;
  private Button t;
  private Button u;
  private boolean v = false;
  private boolean w = false;
  private boolean x = false;
  private boolean y = true;
  private final int z = 25;

  private static int a(LabeledValue paramLabeledValue, List<LabeledValue> paramList)
  {
    int i = paramList.size();
    if (paramLabeledValue != null)
      for (int j = 0; j < i; j++)
      {
        LabeledValue localLabeledValue = (LabeledValue)paramList.get(j);
        if (paramLabeledValue.b().equalsIgnoreCase(localLabeledValue.b()))
          return j;
      }
    return -1;
  }

  private void a(String paramString, List<LabeledValue> paramList)
  {
    b(paramString).q().setAdapter(com.chase.sig.android.util.a.g(this, paramList));
  }

  private void b(String paramString, List<IAccount> paramList)
  {
    b(paramString).q().setAdapter(com.chase.sig.android.util.a.a(this, paramList));
  }

  private boolean d()
  {
    boolean bool1 = true;
    y();
    try
    {
      JPSpinner localJPSpinner5 = ((q)a("ACCOUNT_NUMBER")).q();
      localJPSpinner1 = localJPSpinner5;
      JPSpinner localJPSpinner2 = ((q)a("CATEGORY")).q();
      JPSpinner localJPSpinner3 = ((q)a("EXPENSE_TYPE")).q();
      JPSpinner localJPSpinner4 = ((q)a("TAX_EXEMPT")).q();
      if ((localJPSpinner1 != null) && (this.a != null) && (!localJPSpinner1.b()))
      {
        a().a("ACCOUNT_NUMBER");
        bool2 = false;
        if (!localJPSpinner2.b())
        {
          a().a("CATEGORY");
          bool2 = false;
        }
        if (!localJPSpinner3.b())
        {
          a().a("EXPENSE_TYPE");
          bool2 = false;
        }
        if (!localJPSpinner4.b())
        {
          a().a("TAX_EXEMPT");
          bool3 = false;
          if ((!this.w) && (s.l(((com.chase.sig.android.view.detail.d)a("DATE")).p())))
          {
            a().a("DATE");
            bool3 = false;
          }
          if (!((c)a("AMOUNT")).n())
          {
            a().a("AMOUNT");
            bool3 = false;
          }
          if (((com.chase.sig.android.view.detail.l)a("DESCRIPTION")).g().contains("%"));
          while (true)
          {
            if ((bool1) || (s.l(((com.chase.sig.android.view.detail.l)a("DESCRIPTION")).g())))
            {
              a().a("DESCRIPTION");
              bool3 = false;
            }
            return bool3;
            bool1 = false;
          }
        }
      }
    }
    catch (ClassCastException localClassCastException)
    {
      while (true)
      {
        JPSpinner localJPSpinner1 = null;
        continue;
        boolean bool3 = bool2;
        continue;
        boolean bool2 = bool1;
      }
    }
  }

  protected final DetailView a()
  {
    return (DetailView)findViewById(2131296420);
  }

  public final void a(Bundle paramBundle)
  {
    b(2130903085);
    if (!r().b().b())
    {
      String str6 = (String)r().b().a("selectedAccountId");
      getIntent().putExtra("selectedAccountId", str6);
      AccountActivity localAccountActivity2 = (AccountActivity)r().b().a("transaction_details");
      getIntent().putExtra("transaction_details", localAccountActivity2);
    }
    Bundle localBundle = getIntent().getExtras();
    this.x = localBundle.getBoolean("is_browsing_receipts", false);
    if (localBundle.containsKey("receipt_photo_list"))
      this.q = ((ReceiptPhotoList)localBundle.getSerializable("receipt_photo_list"));
    if (localBundle.containsKey("selectedAccountId"))
      this.k = localBundle.getString("selectedAccountId");
    if (localBundle.containsKey("transaction_details"))
    {
      AccountActivity localAccountActivity1 = (AccountActivity)localBundle.getSerializable("transaction_details");
      this.l = localAccountActivity1.c();
      this.m = localAccountActivity1.a();
      ArrayList localArrayList = localAccountActivity1.b();
      int i = localAccountActivity1.b().size();
      int j = 0;
      if (j < i)
      {
        if (((ActivityValue)localArrayList.get(j)).b())
          this.n = s.b(((ActivityValue)localArrayList.get(j)).value, "-");
        if (((ActivityValue)localArrayList.get(j)).c())
        {
          String str5 = ((ActivityValue)localArrayList.get(j)).value;
          if ((str5 == null) || (str5.equalsIgnoreCase("pending")))
            break label318;
          this.o = s.b(s.t(str5));
        }
        while (true)
        {
          j++;
          break;
          label318: this.y = false;
        }
      }
      this.v = true;
    }
    if (localBundle.containsKey("receipt"))
    {
      this.w = true;
      this.p = ((Receipt)localBundle.getSerializable("receipt"));
      this.k = this.p.a();
      this.p.a(new Dollar(this.p.b().b().abs()));
      if (this.p.f() != null)
      {
        this.l = this.p.f();
        this.v = true;
      }
    }
    boolean bool3;
    boolean bool4;
    label729: String str4;
    if (this.p == null)
    {
      setTitle(2131165800);
      this.s = ((DetailView)findViewById(2131296420));
      DetailView localDetailView2 = this.s;
      com.chase.sig.android.view.detail.a[] arrayOfa2 = new com.chase.sig.android.view.detail.a[7];
      com.chase.sig.android.view.detail.l locall4 = new com.chase.sig.android.view.detail.l("Description", this.m);
      locall4.b = "DESCRIPTION";
      com.chase.sig.android.view.detail.l locall5 = (com.chase.sig.android.view.detail.l)locall4;
      locall5.a = 25;
      com.chase.sig.android.view.detail.l locall6 = (com.chase.sig.android.view.detail.l)locall5.a("Required");
      locall6.o = true;
      arrayOfa2[0] = locall6;
      c localc3 = new c("Amount $", new Dollar(this.n));
      localc3.b = "AMOUNT";
      c localc4 = (c)((c)localc3).a("Enter amount");
      localc4.o = true;
      arrayOfa2[1] = localc4;
      com.chase.sig.android.view.detail.d locald1 = new com.chase.sig.android.view.detail.d("Transaction date", this.A);
      locald1.b = "DATE";
      com.chase.sig.android.view.detail.d locald2 = (com.chase.sig.android.view.detail.d)locald1;
      locald2.a = i(0);
      com.chase.sig.android.view.detail.d locald3 = locald2.q();
      if ((this.v) && (this.y))
      {
        bool3 = true;
        locald3.j = bool3;
        arrayOfa2[2] = locald3;
        q localq7 = new q("Account");
        localq7.b = "ACCOUNT_NUMBER";
        q localq8 = (q)((q)localq7).a("Select account");
        localq8.o = s.l(this.k);
        q localq9 = (q)localq8;
        if (s.l(this.k))
          break label1319;
        bool4 = true;
        localq9.j = bool4;
        arrayOfa2[3] = localq9;
        q localq10 = new q("Category");
        localq10.b = "CATEGORY";
        q localq11 = (q)((q)localq10).a("Select category");
        localq11.o = true;
        arrayOfa2[4] = localq11;
        q localq12 = new q("Expense type");
        localq12.b = "EXPENSE_TYPE";
        q localq13 = (q)((q)localq12).a("Select expense type");
        localq13.o = true;
        arrayOfa2[5] = localq13;
        q localq14 = new q("Tax deduction");
        localq14.b = "TAX_EXEMPT";
        q localq15 = (q)((q)localq14).a("Select tax deduction");
        localq15.o = true;
        arrayOfa2[6] = localq15;
        localDetailView2.setRows(arrayOfa2);
        a(this.s, 2131296422);
        String str3 = getString(2131165807);
        this.r = ((TextView)findViewById(2131296419));
        this.r.setText(str3);
        this.t = ((Button)findViewById(2131296421));
        this.t.setText(getString(2131165291));
        this.t.setOnClickListener(new kq(this));
        this.u = ((Button)findViewById(2131296422));
        if ((this.p != null) && (this.p.j() != null))
          break label1862;
        if (!this.v)
          break label1850;
        str4 = getString(2131165801);
        label1024: this.u.setText(str4);
        this.u.setOnClickListener(new kr(this));
        if (paramBundle == null)
          break label1895;
        if (paramBundle.containsKey("DESCRIPTION"))
          ((com.chase.sig.android.view.detail.l)a("DESCRIPTION")).b(paramBundle.getString("DESCRIPTION"));
        if (paramBundle.containsKey("AMOUNT"))
          ((c)a("AMOUNT")).b(paramBundle.getString("AMOUNT"));
        if (paramBundle.containsKey("DATE"))
        {
          if (this.p == null)
            break label1874;
          ((DetailRow)a("DATE")).c = paramBundle.getString("DATE");
        }
        label1140: JPSpinner localJPSpinner1 = b("CATEGORY").q();
        JPSpinner localJPSpinner2 = b("EXPENSE_TYPE").q();
        JPSpinner localJPSpinner3 = b("TAX_EXEMPT").q();
        if (paramBundle.containsKey("account_list_key"))
        {
          this.a = ((ArrayList)paramBundle.getSerializable("account_list_key"));
          b("ACCOUNT_NUMBER", this.a);
        }
        this.b = ((List)paramBundle.getSerializable("category_list_key"));
        a("CATEGORY", this.b);
        localJPSpinner1.setSelection(paramBundle.getInt("CATEGORY"));
        this.d = ((List)paramBundle.getSerializable("expense_list_key"));
        a("EXPENSE_TYPE", this.d);
        localJPSpinner2.setSelection(paramBundle.getInt("EXPENSE_TYPE"));
        this.c = ((List)paramBundle.getSerializable("tax_exempt_list_key"));
        a("TAX_EXEMPT", this.c);
        localJPSpinner3.setSelection(paramBundle.getInt("TAX_EXEMPT"));
      }
    }
    label1319: label1349: label1862: label1874: label1895: 
    do
    {
      return;
      bool3 = false;
      break;
      bool4 = false;
      break label729;
      setTitle(2131165802);
      Receipt localReceipt = this.p;
      String str1;
      DetailView localDetailView1;
      com.chase.sig.android.view.detail.a[] arrayOfa1;
      boolean bool1;
      String str2;
      DetailRow localDetailRow4;
      if (localReceipt.e() == null)
      {
        str1 = "--";
        IAccount localIAccount = r().b.a(localReceipt.a());
        this.s = ((DetailView)findViewById(2131296420));
        localDetailView1 = this.s;
        arrayOfa1 = new com.chase.sig.android.view.detail.a[7];
        com.chase.sig.android.view.detail.l locall1 = new com.chase.sig.android.view.detail.l("Description", localReceipt.c());
        locall1.b = "DESCRIPTION";
        com.chase.sig.android.view.detail.l locall2 = (com.chase.sig.android.view.detail.l)locall1;
        locall2.a = 25;
        com.chase.sig.android.view.detail.l locall3 = (com.chase.sig.android.view.detail.l)locall2.a("Required");
        locall3.o = true;
        arrayOfa1[0] = locall3;
        c localc1 = new c("Amount $", localReceipt.b());
        localc1.b = "AMOUNT";
        c localc2 = (c)((c)localc1).a("Enter amount");
        localc2.o = true;
        arrayOfa1[1] = localc2;
        DetailRow localDetailRow1 = new DetailRow("Transaction date", str1);
        localDetailRow1.b = "DATE";
        DetailRow localDetailRow2 = (DetailRow)localDetailRow1;
        if (this.x)
          break label1830;
        bool1 = true;
        localDetailRow2.j = bool1;
        arrayOfa1[2] = localDetailRow2;
        if (localIAccount == null)
          break label1836;
        str2 = localIAccount.w().toString();
        DetailRow localDetailRow3 = new DetailRow("Account", str2);
        localDetailRow3.b = "ACCOUNT_NUMBER";
        localDetailRow4 = (DetailRow)localDetailRow3;
        if (this.x)
          break label1844;
      }
      for (boolean bool2 = true; ; bool2 = false)
      {
        localDetailRow4.j = bool2;
        arrayOfa1[3] = localDetailRow4;
        q localq1 = new q("Category");
        localq1.b = "CATEGORY";
        q localq2 = (q)((q)localq1).a("Select category");
        localq2.o = true;
        arrayOfa1[4] = localq2;
        q localq3 = new q("Expense type");
        localq3.b = "EXPENSE_TYPE";
        q localq4 = (q)((q)localq3).a("Select expense type");
        localq4.o = true;
        arrayOfa1[5] = localq4;
        q localq5 = new q("Tax deduction");
        localq5.b = "TAX_EXEMPT";
        q localq6 = (q)((q)localq5).a("Select tax deduction");
        localq6.o = true;
        arrayOfa1[6] = localq6;
        localDetailView1.setRows(arrayOfa1);
        a(this.s, 2131296422);
        findViewById(2131296418).setVisibility(0);
        findViewById(2131296418).setOnClickListener(new ko(this));
        break;
        str1 = s.h(localReceipt.e());
        break label1349;
        bool1 = false;
        break label1545;
        str2 = "NA";
        break label1575;
      }
      str4 = getString(2131165297);
      break label1024;
      str4 = getString(2131165806);
      break label1024;
      ((com.chase.sig.android.view.detail.d)a("DATE")).b(paramBundle.getString("DATE"));
      break label1140;
      a(b.class, new String[0]);
    }
    while (!s.l(this.k));
    label1545: label1575: label1836: label1844: label1850: this.a = r().b.w();
    label1830: b("ACCOUNT_NUMBER", this.a);
  }

  public final void k()
  {
  }

  public void onBackPressed()
  {
    showDialog(-4);
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    Dialog localDialog = super.onCreateDialog(paramInt);
    switch (paramInt)
    {
    default:
      return localDialog;
    case 0:
    }
    Calendar localCalendar1 = Calendar.getInstance();
    localCalendar1.add(5, 1);
    Calendar localCalendar2 = Calendar.getInstance();
    localCalendar2.add(5, -730);
    return new com.chase.sig.android.view.b(this, this.B, false, false, localCalendar2, localCalendar1).a(s.t(this.A)).a();
  }

  protected void onRestoreInstanceState(Bundle paramBundle)
  {
    super.onRestoreInstanceState(paramBundle);
    if (paramBundle.containsKey("right_button_enable_state"))
    {
      this.u.setEnabled(paramBundle.getBoolean("right_button_enable_state"));
      b(this.s, 2131296422);
    }
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    String str1 = ((com.chase.sig.android.view.detail.l)a("DESCRIPTION")).g();
    if (!s.m(str1))
      paramBundle.putString("DESCRIPTION", str1);
    String str2 = ((c)a("AMOUNT")).g();
    if (!s.m(str2))
      paramBundle.putString("AMOUNT", str2);
    if (this.p != null)
      paramBundle.putString("DATE", this.p.e().toString());
    while (true)
    {
      if ((this.p != null) && (this.v))
        paramBundle.putBoolean("right_button_enable_state", this.u.isEnabled());
      if (this.p == null)
        paramBundle.putInt("ACCOUNT_NUMBER", ((q)a("ACCOUNT_NUMBER")).q().getPosition());
      JPSpinner localJPSpinner1 = ((q)a("CATEGORY")).q();
      JPSpinner localJPSpinner2 = ((q)a("EXPENSE_TYPE")).q();
      JPSpinner localJPSpinner3 = ((q)a("TAX_EXEMPT")).q();
      paramBundle.putInt("CATEGORY", localJPSpinner1.getPosition());
      paramBundle.putInt("EXPENSE_TYPE", localJPSpinner2.getPosition());
      paramBundle.putInt("TAX_EXEMPT", localJPSpinner3.getPosition());
      paramBundle.putSerializable("category_list_key", (Serializable)this.b);
      paramBundle.putSerializable("tax_exempt_list_key", (Serializable)this.c);
      paramBundle.putSerializable("expense_list_key", (Serializable)this.d);
      if (this.a != null)
        paramBundle.putSerializable("account_list_key", (Serializable)this.a);
      return;
      String str3 = ((com.chase.sig.android.view.detail.d)a("DATE")).g();
      if (!s.m(str3))
        paramBundle.putString("DATE", str3);
    }
  }

  public static class a extends com.chase.sig.android.d<ReceiptsEnterDetailsActivity, Receipt, Void, ReceiptsAddResponse>
  {
  }

  public static class b extends com.chase.sig.android.b<ReceiptsEnterDetailsActivity, String, Void, ListContentResponse>
  {
    protected void onCancelled()
    {
      super.onCancelled();
    }
  }

  public static class c extends com.chase.sig.android.d<ReceiptsEnterDetailsActivity, Receipt, Void, com.chase.sig.android.service.l>
  {
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.ReceiptsEnterDetailsActivity
 * JD-Core Version:    0.6.2
 */