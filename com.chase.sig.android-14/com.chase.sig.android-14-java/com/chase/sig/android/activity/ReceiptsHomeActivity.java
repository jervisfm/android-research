package com.chase.sig.android.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.net.Uri;
import android.os.AsyncTask.Status;
import android.os.Bundle;
import android.provider.MediaStore.Images.Media;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import com.chase.sig.android.domain.ReceiptAccountSummary;
import com.chase.sig.android.domain.ReceiptPhotoList;
import com.chase.sig.android.domain.o;
import com.chase.sig.android.service.ReceiptAccountSummaryResponse;
import com.chase.sig.android.service.SplashResponse;
import com.chase.sig.android.view.detail.DetailRow;
import com.chase.sig.android.view.detail.DetailRowWithSubItem;
import com.chase.sig.android.view.detail.DetailView;
import com.chase.sig.android.view.detail.a;
import com.chase.sig.android.view.detail.p;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

public class ReceiptsHomeActivity extends ai
{
  View.OnClickListener a = new ku(this);
  private DetailView b;
  private DetailView c;
  private ReceiptAccountSummary d;

  private void a(ReceiptAccountSummary paramReceiptAccountSummary)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(paramReceiptAccountSummary.b());
    localStringBuilder.append(" total receipts / " + paramReceiptAccountSummary.a());
    localStringBuilder.append(" limit");
    DetailRowWithSubItem localDetailRowWithSubItem = new DetailRowWithSubItem(getString(2131165808), localStringBuilder.toString(), null);
    if (paramReceiptAccountSummary.b() > 0)
    {
      localDetailRowWithSubItem.m = true;
      localDetailRowWithSubItem.h = this.a;
    }
    while (true)
    {
      this.c.setRows(new a[] { localDetailRowWithSubItem });
      return;
      localDetailRowWithSubItem.m = false;
      localDetailRowWithSubItem.q = false;
      localDetailRowWithSubItem.e = 2131099664;
    }
  }

  private void d()
  {
    Animation localAnimation = AnimationUtils.loadAnimation(this, 2130968580);
    View localView = findViewById(2131296475);
    localView.setVisibility(0);
    localView.startAnimation(localAnimation);
    a locala = (a)this.e.a(a.class);
    if (locala.getStatus() != AsyncTask.Status.RUNNING)
      locala.execute(new String[0]);
  }

  private a<DetailRow, String> e()
  {
    a locala = new DetailRow(getString(2131165795), "").d();
    locala.h = new kt(this);
    return locala;
  }

  public final void a(Bundle paramBundle)
  {
    b(2130903201);
    setTitle(Html.fromHtml(getString(2131165780)));
    int i;
    if ((r().d != null) && (r().d.l()))
    {
      e(r().d.s());
      i = 1;
      if (i == 0)
      {
        this.b = ((DetailView)findViewById(2131296913));
        if (!x())
          break label206;
        DetailView localDetailView2 = this.b;
        a[] arrayOfa2 = new a[2];
        a locala = new p(getString(2131165794)).d();
        locala.h = new ks(this);
        arrayOfa2[0] = locala;
        arrayOfa2[1] = e();
        localDetailView2.setRows(arrayOfa2);
      }
    }
    while (true)
    {
      this.c = ((DetailView)findViewById(2131296914));
      if ((paramBundle == null) || (!paramBundle.containsKey("account_summary_bundle_key")))
        break label239;
      this.d = ((ReceiptAccountSummary)paramBundle.getSerializable("account_summary_bundle_key"));
      if (this.d == null)
        break label234;
      a(this.d);
      return;
      i = 0;
      break;
      label206: DetailView localDetailView1 = this.b;
      a[] arrayOfa1 = new a[1];
      arrayOfa1[0] = e();
      localDetailView1.setRows(arrayOfa1);
    }
    label234: d();
    return;
    label239: d();
  }

  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    Intent localIntent;
    ReceiptPhotoList localReceiptPhotoList;
    ArrayList localArrayList;
    if ((paramInt2 == -1) && (paramInt1 == 1) && (paramIntent != null))
    {
      localIntent = new Intent(this, ReceiptsEnterDetailsActivity.class);
      localReceiptPhotoList = ReceiptPhotoList.a(getIntent());
      localArrayList = paramIntent.getParcelableArrayListExtra("image_uris");
      if (localArrayList == null)
        break label141;
    }
    try
    {
      Iterator localIterator = localArrayList.iterator();
      while (localIterator.hasNext())
      {
        Uri localUri = (Uri)localIterator.next();
        Bitmap localBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), localUri);
        ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
        localBitmap.compress(Bitmap.CompressFormat.JPEG, 30, localByteArrayOutputStream);
        localBitmap.recycle();
        System.gc();
        localReceiptPhotoList.a(localByteArrayOutputStream.toByteArray());
      }
    }
    catch (FileNotFoundException localFileNotFoundException)
    {
      while (true)
      {
        super.onActivityResult(paramInt1, paramInt2, paramIntent);
        return;
        localIntent.putExtra("receipt_photo_list", localReceiptPhotoList);
        startActivity(localIntent);
      }
    }
    catch (IOException localIOException)
    {
      label133: label141: break label133;
    }
  }

  protected void onResume()
  {
    super.onResume();
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    if (this.d != null)
      paramBundle.putSerializable("account_summary_bundle_key", this.d);
  }

  public static class a extends ae<ReceiptsHomeActivity, String, Void, ReceiptAccountSummaryResponse>
  {
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.ReceiptsHomeActivity
 * JD-Core Version:    0.6.2
 */