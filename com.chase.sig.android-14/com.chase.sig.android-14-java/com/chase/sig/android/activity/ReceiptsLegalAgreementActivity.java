package com.chase.sig.android.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.AsyncTask.Status;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import com.chase.sig.android.d;
import com.chase.sig.android.service.l;
import com.chase.sig.android.util.e;
import com.chase.sig.android.view.k.a;

public class ReceiptsLegalAgreementActivity extends ai
{
  private String a;
  private String b;
  private WebView c;
  private String d;
  private String k;
  private String l;
  private String m;

  private static void b(String paramString, WebView paramWebView)
  {
    paramWebView.loadDataWithBaseURL(null, paramString, "text/html", "utf-8", null);
  }

  public final void a(Bundle paramBundle)
  {
    b(2130903202);
    setTitle(2131165651);
    Bundle localBundle = getIntent().getExtras();
    this.a = ((String)localBundle.get("plan_code_id"));
    this.b = ((String)localBundle.get("funding_acct_id"));
    this.l = ((String)e.a(paramBundle, getIntent(), "resultMessage"));
    WebView localWebView = (WebView)findViewById(2131296918);
    localWebView.getSettings().setSaveFormData(false);
    localWebView.setWebViewClient(new kv(this));
    this.c = localWebView;
    if ((paramBundle != null) && (paramBundle.containsKey("formId")))
    {
      this.d = ((String)paramBundle.getSerializable("formId"));
      this.k = ((String)paramBundle.getSerializable("legalAgreementUrl"));
      this.m = ((String)paramBundle.getSerializable("content"));
      b(this.m, this.c);
    }
    while (true)
    {
      a(2131296917, new kx(this));
      a(2131296916, new kw(this, this));
      return;
      b localb = (b)this.e.a(b.class);
      if (localb.getStatus() != AsyncTask.Status.RUNNING)
        localb.execute(new String[0]);
    }
  }

  public final void a(String paramString)
  {
    this.d = paramString;
  }

  public final void b(String paramString)
  {
    this.k = paramString;
  }

  public final void c(String paramString)
  {
    this.l = paramString;
  }

  public final String d()
  {
    return this.d;
  }

  public final void h(String paramString)
  {
    this.m = paramString;
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    if (paramInt == 0)
    {
      k.a locala = new k.a(this);
      locala.a(this.l).a(2131165288, new ky(this));
      locala.g = new kz(this);
      return locala.d(-1);
    }
    return super.onCreateDialog(paramInt);
  }

  protected void onResume()
  {
    super.onResume();
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    String str = this.d;
    if (str != null)
    {
      paramBundle.putSerializable("formId", str);
      paramBundle.putSerializable("legalAgreementUrl", this.k);
      paramBundle.putSerializable("content", this.m);
      paramBundle.putString("resultMessage", this.l);
    }
  }

  public static class a extends d<ReceiptsLegalAgreementActivity, String, Void, l>
  {
  }

  public static class b extends d<ReceiptsLegalAgreementActivity, String, Void, l>
  {
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.ReceiptsLegalAgreementActivity
 * JD-Core Version:    0.6.2
 */