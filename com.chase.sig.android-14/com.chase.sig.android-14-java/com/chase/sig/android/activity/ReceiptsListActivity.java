package com.chase.sig.android.activity;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.b;
import com.chase.sig.android.domain.LabeledValue;
import com.chase.sig.android.domain.Receipt;
import com.chase.sig.android.domain.receipt.ReceiptFilterList;
import com.chase.sig.android.service.ListContentResponse;
import com.chase.sig.android.service.ReceiptFilter;
import com.chase.sig.android.service.ReceiptFilter.DateFilter;
import com.chase.sig.android.service.ReceiptListResponse;
import com.chase.sig.android.util.Dollar;
import com.chase.sig.android.util.e;
import com.chase.sig.android.util.o;
import com.chase.sig.android.util.s;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ReceiptsListActivity extends ai
{
  private ListView a;
  private List<Receipt> b;
  private ListContentResponse c;
  private String d;
  private ReceiptFilterList k;
  private ArrayList<String> l;
  private Date m;
  private Date n;
  private ReceiptFilter o;
  private String p;
  private ArrayList<LabeledValue> q;
  private ArrayList<String> r;
  private ArrayList<LabeledValue> s;
  private boolean t;

  private View.OnClickListener a(Button paramButton)
  {
    return new lh(this, paramButton);
  }

  private static String[] a(List<LabeledValue> paramList)
  {
    String[] arrayOfString = new String[paramList.size()];
    for (int i = 0; i < arrayOfString.length; i++)
      arrayOfString[i] = ((LabeledValue)paramList.get(i)).a();
    return arrayOfString;
  }

  private boolean[] a(String[] paramArrayOfString)
  {
    boolean[] arrayOfBoolean = new boolean[paramArrayOfString.length];
    for (int i = 0; i < paramArrayOfString.length; i++)
    {
      Iterator localIterator = this.s.iterator();
      while (localIterator.hasNext())
      {
        LabeledValue localLabeledValue = (LabeledValue)localIterator.next();
        if ((!this.r.contains(localLabeledValue.b())) && (paramArrayOfString[i].equals(localLabeledValue.a())))
          arrayOfBoolean[i] = true;
      }
    }
    return arrayOfBoolean;
  }

  private Dialog d()
  {
    AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
    ArrayList localArrayList = new ArrayList();
    localArrayList.add(new LabeledValue("No filter", ""));
    localArrayList.addAll(((ReceiptFilterList)this.c.a().get("dateFilter")).a());
    int i = localArrayList.size();
    String[] arrayOfString = new String[i + 1];
    for (int j = 0; j < i; j++)
      arrayOfString[j] = ((LabeledValue)localArrayList.get(j)).a();
    arrayOfString[i] = "From / To";
    localBuilder.setItems(arrayOfString, new le(this, i, localArrayList)).setTitle("Select a date range");
    return localBuilder.create();
  }

  private void d(List<Receipt> paramList)
  {
    this.b = paramList;
    String str = "";
    ArrayList localArrayList = new ArrayList();
    Map localMap = o.a(this.c.a("Category").a());
    Iterator localIterator = paramList.iterator();
    while (localIterator.hasNext())
    {
      Receipt localReceipt = (Receipt)localIterator.next();
      LinkedHashMap localLinkedHashMap = new LinkedHashMap();
      if (localReceipt.k() != null)
        str = (String)localMap.get(localReceipt.k().b());
      localLinkedHashMap.put("name", localReceipt.c());
      localLinkedHashMap.put("category", str);
      localLinkedHashMap.put("amount", localReceipt.b().h());
      localArrayList.add(localLinkedHashMap);
    }
    SimpleAdapter localSimpleAdapter = new SimpleAdapter(this, localArrayList, 2130903126, new String[] { "name", "category", "amount" }, new int[] { 2131296277, 2131296417, 2131296534 });
    this.a.setAdapter(localSimpleAdapter);
    this.a.setOnItemClickListener(new a((byte)0));
  }

  private void e()
  {
    ReceiptFilter[] arrayOfReceiptFilter = new ReceiptFilter[1];
    arrayOfReceiptFilter[0] = this.o;
    a(b.class, arrayOfReceiptFilter);
  }

  private void f()
  {
    Button localButton1 = (Button)findViewById(2131296897);
    Button localButton2 = (Button)findViewById(2131296499);
    if (!this.o.a().isEmpty())
    {
      StringBuilder localStringBuilder = new StringBuilder("Dates: ");
      String str = "";
      if (this.o.c() != null)
      {
        if (!this.o.c().getClass().equals(String.class))
          break label150;
        str = (String)o.a(this.c.a("Dates").a()).get(this.o.c());
      }
      while (true)
      {
        localButton2.setText(str);
        localButton1.setText("Filter: " + g());
        return;
        label150: if (this.o.c().getClass().equals(ReceiptFilter.DateFilter.class))
          str = "From/To";
      }
    }
    localButton2.setText("Dates: ");
    localButton1.setText("Filter: No filter");
  }

  private String g()
  {
    Object localObject;
    if (!this.l.isEmpty())
    {
      Iterator localIterator = this.l.iterator();
      localObject = "";
      int i = 0;
      if (localIterator.hasNext())
      {
        if (i == 0);
        String str1;
        for (String str2 = (String)localIterator.next(); ; str2 = (String)localObject + ", " + str1)
        {
          i++;
          localObject = str2;
          break;
          str1 = (String)localIterator.next();
        }
      }
    }
    else
    {
      localObject = "";
    }
    return localObject;
  }

  public final void a(Bundle paramBundle)
  {
    b(2130903194);
    setTitle(2131165798);
    TextView localTextView = (TextView)findViewById(2131296896);
    Bundle localBundle = getIntent().getExtras();
    localTextView.setText((CharSequence)e.a(localBundle, "header_text", "N/A"));
    localTextView.setText((CharSequence)e.a(getIntent().getExtras(), "header_text", "N/A"));
    ReceiptFilter localReceiptFilter = new ReceiptFilter();
    localReceiptFilter.a("ALL");
    this.o = ((ReceiptFilter)e.a(paramBundle, "current_filters", localReceiptFilter));
    this.p = ((String)e.a(localBundle, "account_id", null));
    this.c = ((ListContentResponse)e.a(localBundle, "filter_set", null));
    this.b = ((List)e.a(paramBundle, "receipts", null));
    this.a = ((ListView)findViewById(2131296898));
    if (this.b != null)
      d(this.b);
    while (true)
    {
      this.d = ((String)e.a(paramBundle, "parent_filter_label", null));
      this.k = ((ReceiptFilterList)e.a(paramBundle, "parent_filter", null));
      this.l = ((ArrayList)e.a(paramBundle, "selected_filter_labels", null));
      if (this.l == null)
      {
        this.l = new ArrayList();
        this.l.add("No filter");
      }
      this.s = ((ArrayList)e.a(paramBundle, "temp_filters", null));
      if (this.s == null)
        this.s = new ArrayList();
      this.m = ((Date)e.a(paramBundle, "selectedFromDate", null));
      this.n = ((Date)e.a(paramBundle, "selectedToDate", null));
      findViewById(2131296499).setOnClickListener(new la(this));
      ((Button)findViewById(2131296897)).setOnClickListener(new ld(this));
      f();
      return;
      e();
    }
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    Dialog localDialog = super.onCreateDialog(paramInt);
    String[] arrayOfString2;
    int i;
    int i4;
    switch (paramInt)
    {
    default:
      return localDialog;
    case 0:
      LinkedHashMap localLinkedHashMap = this.c.a();
      arrayOfString2 = new String[-1 + localLinkedHashMap.size()];
      Iterator localIterator1 = localLinkedHashMap.values().iterator();
      i = 0;
      if (localIterator1.hasNext())
      {
        ReceiptFilterList localReceiptFilterList2 = (ReceiptFilterList)localIterator1.next();
        if ("Dates".equalsIgnoreCase(localReceiptFilterList2.b()))
          break label1090;
        i4 = i + 1;
        arrayOfString2[i] = localReceiptFilterList2.b();
      }
      break;
    case 1:
    case 2:
    case 3:
    }
    label262: label402: label549: label559: label565: label1090: for (int i3 = i4; ; i3 = i)
    {
      i = i3;
      break;
      lb locallb = new lb(this, arrayOfString2);
      Object localObject2;
      String[] arrayOfString3;
      int j;
      int i1;
      label212: LabeledValue localLabeledValue;
      int i2;
      if (this.t)
      {
        localObject2 = this.o;
        this.s.clear();
        arrayOfString3 = (String[])arrayOfString2.clone();
        j = 0;
        if (j > -1 + arrayOfString3.length)
          break label565;
        Iterator localIterator3 = this.c.a().values().iterator();
        i1 = 0;
        if (!localIterator3.hasNext())
          break label559;
        ReceiptFilterList localReceiptFilterList1 = (ReceiptFilterList)localIterator3.next();
        if (localReceiptFilterList1.b().equals(arrayOfString2[j]))
        {
          Iterator localIterator4 = localReceiptFilterList1.a().iterator();
          if (localIterator4.hasNext())
          {
            localLabeledValue = (LabeledValue)localIterator4.next();
            Iterator localIterator5 = ((ReceiptFilter)localObject2).a().iterator();
            i2 = i1;
            while (true)
            {
              if (!localIterator5.hasNext())
                break label549;
              String str = (String)localIterator5.next();
              if (localLabeledValue.b().equals(str))
              {
                this.s.add(new LabeledValue(localLabeledValue.a(), localLabeledValue.b()));
                if (i2 != 0)
                  break;
                arrayOfString3[j] = (arrayOfString3[j] + ": " + localLabeledValue.a());
                i2++;
              }
            }
          }
        }
      }
      else if (this.s.isEmpty())
      {
        if ((this.r == null) || (this.r.isEmpty()));
      }
      ReceiptFilter localReceiptFilter;
      for (Object localObject1 = new ReceiptFilter(); ; localObject1 = localReceiptFilter)
      {
        localObject2 = localObject1;
        break;
        localObject2 = this.o;
        break;
        localReceiptFilter = new ReceiptFilter();
        Iterator localIterator2 = this.s.iterator();
        while (true)
          if (localIterator2.hasNext())
          {
            localReceiptFilter.a(((LabeledValue)localIterator2.next()).b());
            continue;
            arrayOfString3[j] = (arrayOfString3[j] + ", " + localLabeledValue.a());
            break label402;
            i1 = i2;
            break label262;
            break label212;
            j++;
            break;
            AlertDialog.Builder localBuilder3 = new AlertDialog.Builder(this).setItems(arrayOfString3, locallb);
            localBuilder3.setTitle("Select receipt filter");
            localBuilder3.setPositiveButton("OK", new lc(this));
            return localBuilder3.create();
            String[] arrayOfString1 = a(this.c.a(this.d).a());
            this.q = new ArrayList();
            this.r = new ArrayList();
            lj locallj = new lj(this);
            boolean[] arrayOfBoolean = a(arrayOfString1);
            AlertDialog.Builder localBuilder2 = new AlertDialog.Builder(this).setMultiChoiceItems(arrayOfString1, arrayOfBoolean, locallj);
            localBuilder2.setTitle("Select " + this.d);
            localBuilder2.setPositiveButton("OK", new lk(this));
            localBuilder2.setOnCancelListener(new ll(this));
            return localBuilder2.create();
            return d();
            View localView = ((LayoutInflater)getBaseContext().getSystemService("layout_inflater")).inflate(2130903073, null);
            Button localButton1 = (Button)localView.findViewById(2131296405);
            localButton1.setOnClickListener(a(localButton1));
            Button localButton2 = (Button)localView.findViewById(2131296406);
            localButton2.setOnClickListener(a(localButton2));
            if (this.o.c() == null)
            {
              if (this.m == null)
                localButton1.setText("From");
              if (this.n == null)
                localButton2.setText("To");
            }
            if (this.m != null)
            {
              localButton1.setText(s.a(this.m));
              if (this.n == null)
                break label1030;
              localButton2.setText(s.a(this.n));
            }
            while (true)
            {
              AlertDialog.Builder localBuilder1 = new AlertDialog.Builder(this);
              lf locallf = new lf(this, localView);
              lg locallg = new lg(this);
              localBuilder1.setView(localView).setPositiveButton("OK", locallf);
              localBuilder1.setView(localView).setOnCancelListener(locallg);
              localBuilder1.setTitle("Select a date range");
              return localBuilder1.create();
              if ((this.o.c() == null) || (!this.o.c().getClass().equals(ReceiptFilter.DateFilter.class)))
                break;
              localButton1.setText(s.i(((ReceiptFilter.DateFilter)this.o.c()).a()));
              break;
              if ((this.o.c() != null) && (this.o.c().getClass().equals(ReceiptFilter.DateFilter.class)))
                localButton2.setText(s.i(((ReceiptFilter.DateFilter)this.o.c()).b()));
            }
          }
      }
    }
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    paramBundle.putSerializable("receipts", (Serializable)this.b);
    paramBundle.putSerializable("current_filters", this.o);
    paramBundle.putSerializable("parent_filter", this.k);
    paramBundle.putString("parent_filter_label", this.d);
    paramBundle.putSerializable("selected_filter_labels", this.l);
    paramBundle.putSerializable("temp_filters", this.s);
    paramBundle.putSerializable("selectedFromDate", this.m);
    paramBundle.putSerializable("selectedToDate", this.n);
  }

  private final class a
    implements AdapterView.OnItemClickListener
  {
    private a()
    {
    }

    public final void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
    {
      try
      {
        Receipt localReceipt = (Receipt)ReceiptsListActivity.k(ReceiptsListActivity.this).get(paramInt);
        ReceiptsListActivity.a(ReceiptsListActivity.this, localReceipt);
        BehaviorAnalyticsAspect.a();
        BehaviorAnalyticsAspect.a(paramAdapterView, paramView, paramInt);
        return;
      }
      catch (Throwable localThrowable)
      {
        BehaviorAnalyticsAspect.a();
        BehaviorAnalyticsAspect.a(paramAdapterView, paramView, paramInt);
        throw localThrowable;
      }
    }
  }

  public static class b extends b<ReceiptsListActivity, ReceiptFilter, Void, ReceiptListResponse>
  {
    protected void onCancelled()
    {
      ((ReceiptsListActivity)this.b).finish();
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.ReceiptsListActivity
 * JD-Core Version:    0.6.2
 */