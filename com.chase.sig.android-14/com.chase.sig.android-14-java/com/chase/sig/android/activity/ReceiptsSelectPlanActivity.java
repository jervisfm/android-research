package com.chase.sig.android.activity;

import android.content.Intent;
import android.os.AsyncTask.Status;
import android.os.Bundle;
import android.text.Html;
import android.view.ViewGroup;
import android.widget.TextView;
import com.chase.sig.android.b;
import com.chase.sig.android.domain.EnrollmentOptions;
import com.chase.sig.android.domain.ReceiptsPricingPlan;
import com.chase.sig.android.domain.o;
import com.chase.sig.android.service.GenericResponse;
import com.chase.sig.android.service.SplashResponse;
import com.chase.sig.android.view.detail.DetailRow;
import com.chase.sig.android.view.detail.DetailRowWithSubItem;
import com.chase.sig.android.view.detail.DetailView;
import com.chase.sig.android.view.detail.a;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ReceiptsSelectPlanActivity extends ai
{
  private DetailView a;
  private TextView b;
  private EnrollmentOptions c;
  private boolean d = false;

  private void a(EnrollmentOptions paramEnrollmentOptions)
  {
    this.c = paramEnrollmentOptions;
    List localList = paramEnrollmentOptions.c();
    ArrayList localArrayList = new ArrayList();
    boolean bool = a(localList);
    Iterator localIterator = localList.iterator();
    if (localIterator.hasNext())
    {
      ReceiptsPricingPlan localReceiptsPricingPlan = (ReceiptsPricingPlan)localIterator.next();
      a locala = new DetailRowWithSubItem(localReceiptsPricingPlan.a(), localReceiptsPricingPlan.b(), null).d();
      if ((localReceiptsPricingPlan.d().equals("CRPLAN0")) && (!bool))
      {
        Intent localIntent2 = new Intent(this, ReceiptsLegalAgreementActivity.class);
        localIntent2.putExtra("plan_code_id", localReceiptsPricingPlan.d());
        a(locala, localIntent2);
      }
      while (true)
      {
        if (localReceiptsPricingPlan.e())
          a(locala);
        if (this.c.a() >= localReceiptsPricingPlan.c())
          a(locala);
        locala.m = false;
        localArrayList.add(locala);
        break;
        Intent localIntent1 = new Intent(this, ReceiptsSettingsActivity.class);
        localIntent1.putExtra("pricing_plan", localReceiptsPricingPlan);
        localIntent1.putExtra("enrollment_option", this.c);
        localIntent1.putExtra("receipts_settings_mode", bool);
        a(locala, localIntent1);
      }
    }
    a[] arrayOfa = new a[localArrayList.size()];
    this.a.setRows((a[])localArrayList.toArray(arrayOfa));
  }

  private static void a(a<DetailRow, String> parama)
  {
    parama.e = 2131099664;
    parama.h = null;
    parama.q = false;
  }

  private void a(a<DetailRow, String> parama, Intent paramIntent)
  {
    parama.h = new lr(this, paramIntent);
  }

  private static boolean a(List<ReceiptsPricingPlan> paramList)
  {
    Iterator localIterator = paramList.iterator();
    while (localIterator.hasNext())
      if (((ReceiptsPricingPlan)localIterator.next()).e())
        return true;
    return false;
  }

  public final void a(Bundle paramBundle)
  {
    b(2130903205);
    if ((r().d != null) && (r().d.l()))
      e(r().d.s());
    for (int i = 1; ; i = 0)
    {
      if (i == 0)
      {
        this.a = ((DetailView)findViewById(2131296924));
        this.b = ((TextView)findViewById(2131296308));
        Bundle localBundle = getIntent().getExtras();
        if (localBundle != null)
          this.d = localBundle.getBoolean("receipts_settings_mode", false);
        if ((paramBundle == null) || (!paramBundle.containsKey("enrollment_option")))
          break;
        this.c = ((EnrollmentOptions)paramBundle.getSerializable("enrollment_option"));
        a(this.c);
      }
      return;
    }
    if (this.d)
    {
      setTitle(2131165787);
      this.b.setText(2131165789);
    }
    while (true)
    {
      a locala = (a)this.e.a(a.class);
      if (locala.getStatus() == AsyncTask.Status.RUNNING)
        break;
      locala.execute(new String[0]);
      return;
      setTitle(2131165785);
      this.b.setText(2131165788);
    }
  }

  protected void onResume()
  {
    super.onResume();
    if (!this.d)
      ((ViewGroup)findViewById(2131296925)).setVisibility(8);
    ViewGroup localViewGroup = (ViewGroup)findViewById(2131296925);
    ((TextView)localViewGroup.findViewById(2131296926)).setText(Html.fromHtml(getString(2131165905)));
    localViewGroup.setOnClickListener(new ls(this));
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    if (this.c != null)
      paramBundle.putSerializable("enrollment_option", this.c);
  }

  public static class a extends b<ReceiptsSelectPlanActivity, String, Void, GenericResponse>
  {
    protected void onCancelled()
    {
      super.onCancelled();
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.ReceiptsSelectPlanActivity
 * JD-Core Version:    0.6.2
 */