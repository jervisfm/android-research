package com.chase.sig.android.activity;

import android.app.Dialog;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.widget.Button;
import android.widget.SimpleAdapter;
import com.chase.sig.android.d;
import com.chase.sig.android.domain.EnrollmentOptions;
import com.chase.sig.android.domain.FundingAccount;
import com.chase.sig.android.domain.ReceiptsPricingPlan;
import com.chase.sig.android.service.ReceiptsEnrollmentResponse;
import com.chase.sig.android.util.Dollar;
import com.chase.sig.android.view.JPSpinner;
import com.chase.sig.android.view.detail.DetailView;
import com.chase.sig.android.view.detail.a;
import com.chase.sig.android.view.detail.e;
import com.chase.sig.android.view.detail.q;
import com.chase.sig.android.view.k.a;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class ReceiptsSettingsActivity extends ai
{
  private DetailView a;
  private ReceiptsPricingPlan b;
  private Button c;
  private String d;
  private boolean k = false;
  private boolean l;

  private JPSpinner d()
  {
    return ((q)((DetailView)findViewById(2131296928)).e("PAY_FROM")).q();
  }

  public final void a(Bundle paramBundle)
  {
    b(2130903206);
    this.a = ((DetailView)findViewById(2131296928));
    Bundle localBundle = getIntent().getExtras();
    if (localBundle == null)
      finish();
    this.b = ((ReceiptsPricingPlan)localBundle.getSerializable("pricing_plan"));
    this.l = this.b.d().equals("CRPLAN0");
    this.k = localBundle.getBoolean("receipts_settings_mode");
    List localList = ((EnrollmentOptions)localBundle.getSerializable("enrollment_option")).b();
    e locale = new e(getResources().getString(2131165786), this.b.a(), this.b.b());
    q localq1 = new q("Pay From");
    localq1.b = "PAY_FROM";
    q localq2 = (q)((q)localq1).a(getString(2131165792));
    localq2.j = this.l;
    q localq3 = (q)localq2;
    this.a.setRows(new a[] { locale, localq3 });
    String[] arrayOfString = { "nicknameAndMask", "balance" };
    int[] arrayOfInt = { 2131296283, 2131296284 };
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = localList.iterator();
    while (localIterator.hasNext())
    {
      FundingAccount localFundingAccount = (FundingAccount)localIterator.next();
      HashMap localHashMap = new HashMap();
      String str = localFundingAccount.b().g();
      localHashMap.put("nicknameAndMask", localFundingAccount.c());
      localHashMap.put("balance", new Dollar(str).toString());
      localHashMap.put("accountId", Integer.toString(localFundingAccount.a()));
      localArrayList.add(localHashMap);
    }
    SimpleAdapter localSimpleAdapter = new SimpleAdapter(getBaseContext(), localArrayList, 2130903047, arrayOfString, arrayOfInt);
    JPSpinner localJPSpinner = d();
    localJPSpinner.setAdapter(localSimpleAdapter);
    localJPSpinner.a(new lw(this));
    if (this.k)
      setTitle(2131165796);
    while (true)
    {
      ((Button)findViewById(2131296330)).setOnClickListener(new lt(this));
      if (!this.k)
        break;
      this.c = ((Button)findViewById(2131296929));
      this.c.setText(2131165796);
      if (this.l)
        this.c.setBackgroundResource(2130837661);
      this.c.setOnClickListener(new lu(this));
      return;
      setTitle(2131165787);
    }
    this.c = ((Button)findViewById(2131296929));
    this.c.setOnClickListener(new lv(this));
  }

  public final void a(String paramString)
  {
    this.d = paramString;
  }

  protected final boolean a(JPSpinner paramJPSpinner)
  {
    y();
    if (paramJPSpinner.getSelectedItemPosition() == -1)
    {
      this.a.a("PAY_FROM");
      return false;
    }
    return true;
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    if (paramInt == 0)
    {
      k.a locala = new k.a(this);
      locala.a(this.d).a(2131165288, new lx(this));
      return locala.d(-1);
    }
    return super.onCreateDialog(paramInt);
  }

  protected void onResume()
  {
    super.onResume();
  }

  public static class a extends d<ReceiptsSettingsActivity, String, Void, ReceiptsEnrollmentResponse>
  {
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.ReceiptsSettingsActivity
 * JD-Core Version:    0.6.2
 */