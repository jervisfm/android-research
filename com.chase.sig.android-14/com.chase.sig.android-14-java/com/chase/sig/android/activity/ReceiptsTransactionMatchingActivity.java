package com.chase.sig.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import com.chase.sig.android.b;
import com.chase.sig.android.d;
import com.chase.sig.android.domain.Receipt;
import com.chase.sig.android.domain.TransactionForReceipt;
import com.chase.sig.android.service.ReceiptsAddResponse;
import com.chase.sig.android.service.ReceiptsTransactionMatchResponse;
import com.chase.sig.android.service.l;
import com.chase.sig.android.view.ae;
import com.chase.sig.android.view.af;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ReceiptsTransactionMatchingActivity extends ai
{
  private ListView a;
  private List<TransactionForReceipt> b;
  private Receipt c;
  private boolean d = false;
  private String k;

  private void a(String paramString)
  {
    TextView localTextView = (TextView)findViewById(2131296921);
    localTextView.setText(paramString);
    localTextView.setVisibility(0);
    ((Button)findViewById(2131296338)).setEnabled(false);
  }

  private void a(List<TransactionForReceipt> paramList)
  {
    if ((paramList == null) || (paramList.size() <= 0))
      return;
    this.a = ((ListView)findViewById(2131296920));
    ae localae = new ae(this, paramList);
    this.a.setAdapter(new af(localae));
    this.a.setVisibility(0);
  }

  public final void a(Bundle paramBundle)
  {
    b(2130903203);
    setTitle(2131165800);
    Bundle localBundle = getIntent().getExtras();
    if (localBundle.containsKey("receipt"))
      this.c = ((Receipt)localBundle.getSerializable("receipt"));
    if (localBundle.containsKey("is_editing"))
      this.d = localBundle.getBoolean("is_editing");
    if (paramBundle == null)
    {
      Receipt[] arrayOfReceipt = new Receipt[1];
      arrayOfReceipt[0] = this.c;
      a(b.class, arrayOfReceipt);
    }
    while (true)
    {
      String str1 = getString(2131165811);
      ((TextView)findViewById(2131296419)).setText(str1);
      String str2 = getString(2131165812);
      Button localButton1 = (Button)findViewById(2131296919);
      localButton1.setText(str2);
      localButton1.setOnClickListener(new ly(this));
      Button localButton2 = (Button)findViewById(2131296338);
      localButton2.setText(getString(2131165292));
      localButton2.setOnClickListener(new lz(this));
      return;
      this.c = ((Receipt)paramBundle.getSerializable("receipt_bundle_key"));
      this.b = ((ArrayList)paramBundle.getSerializable("trans_matches_bundle_key"));
      this.k = paramBundle.getString("no_transaction_found_message");
      if (this.k != null)
        a(this.k);
      a(this.b);
    }
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    if (this.b != null)
      paramBundle.putSerializable("trans_matches_bundle_key", (Serializable)this.b);
    if (this.k != null)
      paramBundle.putString("no_transaction_found_message", this.k);
    paramBundle.putSerializable("receipt_bundle_key", this.c);
  }

  public static class a extends d<ReceiptsTransactionMatchingActivity, Receipt, Void, ReceiptsAddResponse>
  {
  }

  public static class b extends b<ReceiptsTransactionMatchingActivity, Receipt, Void, ReceiptsTransactionMatchResponse>
  {
    protected void onCancelled()
    {
      super.onCancelled();
    }
  }

  public static class c extends d<ReceiptsTransactionMatchingActivity, Receipt, Void, l>
  {
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.ReceiptsTransactionMatchingActivity
 * JD-Core Version:    0.6.2
 */