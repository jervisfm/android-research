package com.chase.sig.android.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import com.chase.sig.android.d;
import com.chase.sig.android.domain.Advisor;
import com.chase.sig.android.service.TeamInfoResponse;
import com.chase.sig.android.util.e;
import com.chase.sig.android.util.s;
import com.chase.sig.android.view.k;
import com.chase.sig.android.view.k.a;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class TeamInfoActivity extends ai
  implements fk.b
{
  private static final cc a = cc.b();
  private final int b = 0;
  private final int c = 1;
  private final int d = 2;
  private ListView k;
  private EditText l;
  private TextView m;
  private Advisor n;
  private String o;
  private List<Advisor> p;

  private List<String> a(Advisor paramAdvisor)
  {
    ArrayList localArrayList = new ArrayList();
    if (paramAdvisor != null)
    {
      if (s.m(paramAdvisor.a()))
        localArrayList.add(CommunicationOptions.a.a());
      if (s.m(paramAdvisor.b()))
        localArrayList.add(CommunicationOptions.b.a());
      paramAdvisor.d();
      if (!a.a(this, paramAdvisor.d()))
        localArrayList.add(CommunicationOptions.c.a());
      localArrayList.add(CommunicationOptions.d.a());
    }
    return localArrayList;
  }

  public final void a(Bundle paramBundle)
  {
    setTitle(2131165902);
    b(2130903231);
    this.k = ((ListView)findViewById(2131296953));
    this.l = ((EditText)findViewById(2131296952));
    this.m = ((TextView)findViewById(2131296891));
    this.n = ((Advisor)e.a(paramBundle, "clickedContact", null));
    this.o = ((String)e.a(paramBundle, "companyName", null));
    this.p = ((List)e.a(paramBundle, "response", new ArrayList()));
    if (this.p.isEmpty())
      a(a.class, new String[0]);
    while (true)
    {
      Drawable localDrawable = getResources().getDrawable(2130837692);
      localDrawable.setBounds(0, 0, localDrawable.getIntrinsicWidth(), localDrawable.getIntrinsicHeight());
      this.l.setCompoundDrawables(null, null, localDrawable, null);
      this.l.addTextChangedListener(new b((byte)0));
      return;
      a(this.p, false);
    }
  }

  public final void a(List<Advisor> paramList, boolean paramBoolean)
  {
    if ((paramList != null) && (paramList.size() > 0))
    {
      d locald = new d(this, paramList);
      this.k.setAdapter(locald);
      this.k.setFooterDividersEnabled(true);
      if (paramBoolean)
        locald.notifyDataSetChanged();
      this.k.setOnItemClickListener(new md(this, paramList));
    }
  }

  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
    if ((paramInt1 == 0) && (paramInt2 == -1))
    {
      Intent localIntent = a.b(this.n, this.o);
      localIntent.fillIn(paramIntent, 2);
      startActivity(localIntent);
    }
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    k.a locala = new k.a(this);
    switch (paramInt)
    {
    default:
      return super.onCreateDialog(paramInt);
    case 0:
      locala.i = true;
      locala.b = "Communication Options";
      locala.d = null;
      ListView localListView = new ListView(this);
      localListView.setId(2);
      localListView.setClickable(true);
      localListView.setAdapter(new ArrayAdapter(this, 2130903121, a(this.n)));
      localListView.setOnItemClickListener(new me(this));
      locala.k = localListView;
      return locala.d(-1);
    case 1:
    }
    locala.i = true;
    locala.b(2131165291, new mg(this)).a(2131165288, new mf(this));
    locala.b(2131165903).c(2131165904);
    return locala.d(-1);
  }

  protected void onPrepareDialog(int paramInt, Dialog paramDialog)
  {
    switch (paramInt)
    {
    default:
      return;
    case 0:
    }
    ListView localListView = (ListView)((k)paramDialog).findViewById(2);
    localListView.invalidateViews();
    localListView.setAdapter(new ArrayAdapter(this, 17367043, a(this.n)));
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    paramBundle.putSerializable("response", (Serializable)this.p);
    paramBundle.putSerializable("clickedContact", this.n);
    paramBundle.putSerializable("companyName", this.o);
  }

  public static enum CommunicationOptions
  {
    private final String displayName;

    static
    {
      CommunicationOptions[] arrayOfCommunicationOptions = new CommunicationOptions[4];
      arrayOfCommunicationOptions[0] = a;
      arrayOfCommunicationOptions[1] = b;
      arrayOfCommunicationOptions[2] = c;
      arrayOfCommunicationOptions[3] = d;
    }

    private CommunicationOptions(String paramString)
    {
      this.displayName = paramString;
    }

    public static CommunicationOptions a(String paramString)
    {
      for (CommunicationOptions localCommunicationOptions : values())
        if (localCommunicationOptions.displayName.equals(paramString))
          return localCommunicationOptions;
      throw new IllegalArgumentException();
    }

    public final String a()
    {
      return this.displayName;
    }
  }

  public static class a extends d<TeamInfoActivity, String, Void, TeamInfoResponse>
  {
  }

  private final class b
    implements TextWatcher
  {
    private b()
    {
    }

    public final void afterTextChanged(Editable paramEditable)
    {
      String str = TeamInfoActivity.b(TeamInfoActivity.this).getText().toString().trim();
      ArrayList localArrayList = new ArrayList();
      Iterator localIterator = TeamInfoActivity.c(TeamInfoActivity.this).iterator();
      while (localIterator.hasNext())
      {
        Advisor localAdvisor = (Advisor)localIterator.next();
        if (localAdvisor.d().toLowerCase().contains(str.toLowerCase()))
          localArrayList.add(localAdvisor);
      }
      if (localArrayList.size() > 0)
      {
        TeamInfoActivity.d(TeamInfoActivity.this).setVisibility(8);
        TeamInfoActivity.e(TeamInfoActivity.this).setVisibility(0);
        TeamInfoActivity.this.a(localArrayList, true);
        return;
      }
      TeamInfoActivity.e(TeamInfoActivity.this).setVisibility(8);
      TeamInfoActivity.d(TeamInfoActivity.this).setVisibility(0);
    }

    public final void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
    {
    }

    public final void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
    {
    }
  }

  private static final class c
    implements Comparator<Advisor>
  {
  }

  private final class d extends ArrayAdapter<Advisor>
  {
    private final List<Advisor> b;
    private final LayoutInflater c;

    public d(int arg2)
    {
      super(2130903230, localList);
      this.b = localList;
      this.c = ((LayoutInflater)localContext.getSystemService("layout_inflater"));
    }

    public final View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      Advisor localAdvisor = (Advisor)this.b.get(paramInt);
      if (paramView == null)
        paramView = this.c.inflate(2130903230, paramViewGroup, false);
      ((TextView)paramView.findViewById(2131296948)).setText(localAdvisor.d());
      ((TextView)paramView.findViewById(2131296949)).setText(localAdvisor.a());
      ((TextView)paramView.findViewById(2131296950)).setText(localAdvisor.c());
      ((TextView)paramView.findViewById(2131296951)).setText(localAdvisor.b().toLowerCase());
      return paramView;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.TeamInfoActivity
 * JD-Core Version:    0.6.2
 */