package com.chase.sig.android.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore.Images.Media;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.d;
import com.chase.sig.android.domain.AccountActivity;
import com.chase.sig.android.domain.ActivityValue;
import com.chase.sig.android.domain.CacheActivityData;
import com.chase.sig.android.domain.LabeledValue;
import com.chase.sig.android.domain.Receipt;
import com.chase.sig.android.domain.ReceiptPhotoList;
import com.chase.sig.android.domain.o;
import com.chase.sig.android.service.ReceiptListResponse;
import com.chase.sig.android.util.Dollar;
import com.chase.sig.android.util.e;
import com.chase.sig.android.view.SeparatorView;
import com.chase.sig.android.view.detail.DetailRow;
import com.chase.sig.android.view.detail.DetailRowWithSubItem;
import com.chase.sig.android.view.detail.DetailView;
import com.chase.sig.android.view.detail.a;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class TransactionDetailActivity extends ai
{
  private AccountActivity a;
  private String b;
  private List<Receipt> c;
  private String d;

  private void d()
  {
    CacheActivityData localCacheActivityData = r().b();
    localCacheActivityData.a("transaction_details", this.a);
    localCacheActivityData.a("selectedAccountId", this.b);
    localCacheActivityData.a("account_nickname_mask", this.d);
  }

  public final void a(Bundle paramBundle)
  {
    setTitle(2131165426);
    b(2130903237);
    if (!r().b().b())
    {
      AccountActivity localAccountActivity2 = (AccountActivity)r().b().a("transaction_details");
      String str1 = (String)r().b().a("selectedAccountId");
      String str2 = (String)r().b().a("account_nickname_mask");
      r().b().a();
      getIntent().putExtra("selectedAccountId", str1);
      getIntent().putExtra("transaction_details", localAccountActivity2);
      getIntent().putExtra("account_nickname_mask", str2);
    }
    this.a = ((AccountActivity)e.a(paramBundle, getIntent(), "transaction_details"));
    this.b = ((String)e.a(paramBundle, getIntent(), "selectedAccountId"));
    this.d = ((String)e.a(paramBundle, getIntent(), "account_nickname_mask"));
    if ((x()) && (((ChaseApplication)getApplication()).t()))
    {
      LinearLayout localLinearLayout = (LinearLayout)findViewById(2131296323);
      localLinearLayout.setVisibility(0);
      localLinearLayout.setOnClickListener(new mh(this));
      ((SeparatorView)findViewById(2131296275)).setVisibility(0);
    }
    ((LinearLayout)findViewById(2131296972)).setOnClickListener(new mi(this));
    AccountActivity localAccountActivity1 = this.a;
    DetailView localDetailView = (DetailView)findViewById(2131296975);
    ArrayList localArrayList1 = new ArrayList();
    localArrayList1.add(new DetailRow("Description", localAccountActivity1.a()));
    int i = localAccountActivity1.b().size();
    ArrayList localArrayList2 = localAccountActivity1.b();
    for (int j = 0; j < i; j++)
      localArrayList1.add(new DetailRow(((ActivityValue)localArrayList2.get(j)).label, ((ActivityValue)localArrayList2.get(j)).value));
    localDetailView.setRows((a[])localArrayList1.toArray(new a[localArrayList1.size()]));
    this.c = ((List)e.a(paramBundle, getIntent(), "receipts"));
    if (this.c == null)
    {
      String[] arrayOfString = new String[2];
      arrayOfString[0] = this.a.c();
      arrayOfString[1] = this.b;
      a(a.class, arrayOfString);
    }
    while (true)
    {
      ((TextView)findViewById(2131296260)).setText(this.d);
      return;
      a(this.c);
    }
  }

  public final void a(List<Receipt> paramList)
  {
    int i = 0;
    this.c = paramList;
    DetailView localDetailView = (DetailView)findViewById(2131296974);
    a[] arrayOfa;
    if (this.c.size() > 0)
    {
      arrayOfa = new a[this.c.size()];
      ((TextView)findViewById(2131296973)).setVisibility(0);
    }
    while (true)
    {
      Receipt localReceipt;
      if (i < arrayOfa.length)
        localReceipt = (Receipt)this.c.get(i);
      try
      {
        arrayOfa[i] = new DetailRowWithSubItem(localReceipt.c(), localReceipt.k().a(), localReceipt.b().toString()).d();
        arrayOfa[i].h = new mj(this, localReceipt);
        label130: i++;
        continue;
        localDetailView.setRows(arrayOfa);
        localDetailView.c();
        return;
      }
      catch (Exception localException)
      {
        break label130;
      }
    }
  }

  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    Intent localIntent;
    ReceiptPhotoList localReceiptPhotoList;
    ArrayList localArrayList;
    if ((paramInt2 == -1) && (paramInt1 == 1) && (paramIntent != null))
    {
      d();
      localIntent = new Intent(this, ReceiptsEnterDetailsActivity.class);
      localReceiptPhotoList = ReceiptPhotoList.a(getIntent());
      localArrayList = paramIntent.getParcelableArrayListExtra("image_uris");
      if (localArrayList == null)
        break label147;
    }
    try
    {
      Iterator localIterator = localArrayList.iterator();
      while (localIterator.hasNext())
      {
        Uri localUri = (Uri)localIterator.next();
        Bitmap localBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), localUri);
        ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
        localBitmap.compress(Bitmap.CompressFormat.JPEG, 30, localByteArrayOutputStream);
        localBitmap.recycle();
        System.gc();
        localReceiptPhotoList.a(localByteArrayOutputStream.toByteArray());
      }
    }
    catch (FileNotFoundException localFileNotFoundException)
    {
      while (true)
      {
        super.onActivityResult(paramInt1, paramInt2, paramIntent);
        return;
        localIntent.putExtra("receipt_photo_list", localReceiptPhotoList);
        startActivity(localIntent);
      }
    }
    catch (IOException localIOException)
    {
      label139: label147: break label139;
    }
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    paramBundle.putSerializable("receipts", (Serializable)this.c);
  }

  public static class a extends d<TransactionDetailActivity, String, Void, ReceiptListResponse>
  {
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.TransactionDetailActivity
 * JD-Core Version:    0.6.2
 */