package com.chase.sig.android.activity;

import android.content.Intent;
import android.os.Bundle;
import com.chase.sig.android.c.a;
import com.chase.sig.android.d;
import com.chase.sig.android.domain.AccountTransfer;
import com.chase.sig.android.service.movemoney.RequestFlags;
import com.chase.sig.android.service.movemoney.ServiceResponse;
import com.chase.sig.android.service.n;
import com.chase.sig.android.service.transfer.a;

public class TransferAuthorizeActivity extends ai
  implements fk.e, c.a
{
  private AccountTransfer a;

  public final void a()
  {
    Intent localIntent = new Intent(this, PaymentsAndTransfersHomeActivity.class);
    localIntent.setFlags(67108864);
    startActivity(localIntent);
  }

  public final void a(Bundle paramBundle)
  {
    b(2130903136);
    setTitle(2131165516);
    this.a = ((AccountTransfer)getIntent().getExtras().getSerializable("transaction_object"));
    a(2131296600, new mk(this));
    a(2131296601, a(this));
  }

  public static class a extends d<TransferAuthorizeActivity, Void, Void, ServiceResponse>
  {
    private ServiceResponse a()
    {
      try
      {
        ((TransferAuthorizeActivity)this.b).A();
        ServiceResponse localServiceResponse = n.e().b(TransferAuthorizeActivity.a((TransferAuthorizeActivity)this.b), RequestFlags.e);
        ((TransferAuthorizeActivity)this.b).l();
        return localServiceResponse;
      }
      catch (Exception localException)
      {
      }
      return null;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.TransferAuthorizeActivity
 * JD-Core Version:    0.6.2
 */