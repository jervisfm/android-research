package com.chase.sig.android.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.chase.sig.android.activity.a.b;
import com.chase.sig.android.c;
import com.chase.sig.android.domain.AccountTransfer;
import com.chase.sig.android.service.movemoney.d;
import com.chase.sig.android.service.n;
import com.chase.sig.android.util.Dollar;
import com.chase.sig.android.util.s;
import com.chase.sig.android.view.detail.DetailRow;
import com.chase.sig.android.view.detail.DetailView;
import com.chase.sig.android.view.detail.a;
import com.chase.sig.android.view.k.a;

public class TransferCompleteActivity extends m<AccountTransfer>
{
  public final void a(Bundle paramBundle)
  {
    b(2130903134);
    setTitle(2131165300);
    c.a();
    a(paramBundle, a.class);
  }

  protected final void a(boolean paramBoolean)
  {
    AccountTransfer localAccountTransfer = (AccountTransfer)this.a;
    boolean bool = getIntent().getBooleanExtra("hideFrequency", true);
    DetailView localDetailView = (DetailView)findViewById(2131296595);
    a[] arrayOfa = new a[10];
    arrayOfa[0] = new DetailRow("Transaction Number", localAccountTransfer.n());
    arrayOfa[1] = new DetailRow("From", localAccountTransfer.r() + localAccountTransfer.s());
    arrayOfa[2] = new DetailRow("To", localAccountTransfer.d_() + localAccountTransfer.A());
    arrayOfa[3] = new DetailRow("Amount", localAccountTransfer.e().h());
    arrayOfa[4] = new DetailRow("Send On", s.i(localAccountTransfer.b()));
    arrayOfa[5] = new DetailRow("Deliver By", s.i(localAccountTransfer.q()));
    DetailRow localDetailRow1 = new DetailRow("Frequency", a(localAccountTransfer.i()));
    localDetailRow1.j = bool;
    arrayOfa[6] = localDetailRow1;
    DetailRow localDetailRow2 = new DetailRow("Remaining Transfers", localAccountTransfer.w());
    localDetailRow2.j = bool;
    arrayOfa[7] = localDetailRow2;
    arrayOfa[8] = new DetailRow("Status", localAccountTransfer.t());
    arrayOfa[9] = new DetailRow("Memo", localAccountTransfer.o());
    localDetailView.setRows(arrayOfa);
    a(2131165502, TransferHistoryActivity.class);
    b(2131165500, TransferStartActivity.class);
    b(paramBoolean);
  }

  protected final Class<? extends eb> b()
  {
    return TransferHomeActivity.class;
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    k.a locala = new k.a(this);
    switch (paramInt)
    {
    default:
      return super.onCreateDialog(paramInt);
    case 12:
      AccountTransfer localAccountTransfer = (AccountTransfer)this.a;
      Context localContext = getBaseContext();
      Object[] arrayOfObject = new Object[4];
      arrayOfObject[0] = a(localAccountTransfer.i()).toLowerCase();
      arrayOfObject[1] = (localAccountTransfer.r() + localAccountTransfer.s());
      arrayOfObject[2] = (localAccountTransfer.d_() + localAccountTransfer.A());
      arrayOfObject[3] = (localAccountTransfer.d_() + localAccountTransfer.A());
      String str = s.a(localContext, 2131165512, arrayOfObject);
      locala.i = true;
      locala.a(2131165288, new b()).c(2131165511).a(str);
      return locala.d(-1);
    case 13:
    }
    locala.i = true;
    locala.a(2131165288, new b()).b(2131165510);
    return locala.d(-1);
  }

  public static class a extends n.a<AccountTransfer, TransferCompleteActivity>
  {
    protected final d<AccountTransfer> a()
    {
      ((TransferCompleteActivity)this.b).A();
      return n.e();
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.TransferCompleteActivity
 * JD-Core Version:    0.6.2
 */