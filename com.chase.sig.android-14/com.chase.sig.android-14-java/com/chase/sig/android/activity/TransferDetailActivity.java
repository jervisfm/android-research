package com.chase.sig.android.activity;

import android.os.Bundle;
import com.chase.sig.android.domain.AccountTransfer;
import com.chase.sig.android.service.movemoney.d;
import com.chase.sig.android.service.n;

public class TransferDetailActivity extends c<AccountTransfer>
{
  public final int a(DetailResource paramDetailResource)
  {
    switch (1.a[paramDetailResource.ordinal()])
    {
    default:
      return 0;
    case 1:
      return 2131165521;
    case 2:
      return 2131165294;
    case 3:
      return 2131165293;
    case 4:
      return 2131165290;
    case 5:
      return 2131165509;
    case 6:
      return 2131165507;
    case 7:
      return 2131165508;
    case 8:
      return 2131165317;
    case 9:
      return 2131165514;
    case 10:
      return 2131165513;
    case 11:
    }
    return 2131165515;
  }

  public final void a(Bundle paramBundle)
  {
    b(2130903238);
    setTitle(2131165503);
    a(paramBundle, b.class);
  }

  protected final d<AccountTransfer> b()
  {
    A();
    return n.e();
  }

  protected final Class<? extends ae<?, Boolean, ?, ?>> c()
  {
    return a.class;
  }

  protected final Class<? extends eb> d()
  {
    return TransferCompleteActivity.class;
  }

  protected final Class<? extends eb> e()
  {
    return TransferEditActivity.class;
  }

  public static class a extends c.a<AccountTransfer>
  {
  }

  public static class b extends n.a<AccountTransfer, TransferDetailActivity>
  {
    protected final d<AccountTransfer> a()
    {
      ((TransferDetailActivity)this.b).A();
      return n.e();
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.TransferDetailActivity
 * JD-Core Version:    0.6.2
 */