package com.chase.sig.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.format.Time;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.domain.AccountTransfer;
import com.chase.sig.android.service.ServiceError;
import com.chase.sig.android.service.movemoney.RequestFlags;
import com.chase.sig.android.service.movemoney.ServiceResponse;
import com.chase.sig.android.service.n;
import com.chase.sig.android.util.s;
import com.chase.sig.android.view.AmountView;
import com.chase.sig.android.view.JPSpinner;
import com.chase.sig.android.view.detail.DetailRow;
import com.chase.sig.android.view.detail.DetailView;
import com.chase.sig.android.view.detail.a;
import com.chase.sig.android.view.detail.j;
import com.chase.sig.android.view.detail.o;
import com.chase.sig.android.view.detail.r;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class TransferEditActivity extends h<AccountTransfer>
  implements ma
{
  private AccountTransfer b = null;
  private com.chase.sig.android.view.detail.d c = null;
  private com.chase.sig.android.view.detail.c d = null;
  private com.chase.sig.android.view.detail.l k = null;
  private o l = null;
  private DetailView m = null;
  private j n = null;
  private CheckBox o = null;
  private EditText p = null;
  private boolean q;

  private int D()
  {
    try
    {
      int i = Integer.parseInt(this.p.getText().toString());
      return i;
    }
    catch (Exception localException)
    {
    }
    return 0;
  }

  public final void a(Bundle paramBundle)
  {
    super.a(paramBundle);
    setTitle(2131165521);
    this.b = ((AccountTransfer)d());
    this.q = getIntent().getExtras().getBoolean("edit_single");
    this.m = a();
    DetailRow localDetailRow1 = new DetailRow(getString(2131165302), this.b.r() + this.b.s());
    DetailRow localDetailRow2 = new DetailRow(getString(2131165301), this.b.d_() + this.b.A());
    com.chase.sig.android.view.detail.d locald1 = new com.chase.sig.android.view.detail.d(getString(2131165306), s.i(this.b.q()));
    locald1.b = "DATE";
    com.chase.sig.android.view.detail.d locald2 = (com.chase.sig.android.view.detail.d)locald1;
    locald2.a = i(0);
    this.c = locald2;
    if ((!this.b.l()) && (!this.q))
    {
      this.l = a(this.b);
      r localr = new r(getString(2131165310), this.b.x());
      localr.b = "REMAINING";
      this.n = ((j)localr);
    }
    com.chase.sig.android.view.detail.c localc = new com.chase.sig.android.view.detail.c(getString(2131165308), this.b.e());
    localc.b = "AMOUNT";
    this.d = ((com.chase.sig.android.view.detail.c)localc);
    String str1 = getString(2131165307);
    if (s.m(this.b.o()));
    for (String str2 = this.b.o(); ; str2 = "")
    {
      com.chase.sig.android.view.detail.l locall1 = new com.chase.sig.android.view.detail.l(str1, str2);
      locall1.b = "MEMO";
      com.chase.sig.android.view.detail.l locall2 = (com.chase.sig.android.view.detail.l)locall1;
      locall2.a = 32;
      this.k = ((com.chase.sig.android.view.detail.l)locall2.a(getString(2131165311)));
      DetailView localDetailView = this.m;
      a[] arrayOfa = new a[7];
      arrayOfa[0] = localDetailRow1;
      arrayOfa[1] = localDetailRow2;
      arrayOfa[2] = this.d;
      arrayOfa[3] = this.c;
      arrayOfa[4] = this.l;
      arrayOfa[5] = this.n;
      arrayOfa[6] = this.k;
      localDetailView.setRows(arrayOfa);
      if ((!this.b.l()) && (!this.q))
      {
        this.o = ((CheckBox)this.n.k().findViewById(2131296415));
        this.p = this.n.p();
      }
      return;
    }
  }

  protected final boolean g()
  {
    y();
    if (!((com.chase.sig.android.view.detail.c)this.m.e("AMOUNT")).p().a())
      this.m.a("AMOUNT");
    for (boolean bool = false; ; bool = true)
    {
      if (s.l(this.c.p()))
      {
        this.m.a("DATE");
        bool = false;
      }
      if (!this.b.l())
      {
        if ((this.o != null) && (!this.o.isChecked()) && (D() <= 0))
        {
          this.m.a("REMAINING");
          bool = false;
        }
        if ((this.l != null) && (this.l.q().getSelectedItemPosition() == -1))
        {
          this.m.a("REMAINING");
          bool = false;
        }
      }
      return bool;
    }
  }

  protected final Class<a> h()
  {
    return a.class;
  }

  protected final Class<? extends eb> i()
  {
    return TransferEditVerifyActivity.class;
  }

  protected final Date j()
  {
    Time localTime = new Time();
    localTime.hour = Integer.valueOf(((ChaseApplication)getApplication()).c(getString(2131165529))).intValue();
    return com.chase.sig.android.util.l.a(localTime.hour).getTime();
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    paramBundle.putSerializable("transaction_object", this.b);
  }

  public static class a extends h.a<AccountTransfer>
  {
    protected final com.chase.sig.android.service.movemoney.d<AccountTransfer> a()
    {
      ((h)this.b).A();
      return n.e();
    }

    protected final void a(ServiceResponse paramServiceResponse)
    {
      if (paramServiceResponse.c("70011"));
      ArrayList localArrayList;
      for (int i = 1; ; i = 0)
      {
        String str = ((h)this.b).getString(2131165525);
        localArrayList = new ArrayList();
        localArrayList.add(new ServiceError("70011", str, false));
        b(paramServiceResponse);
        if (((i != 0) || (!paramServiceResponse.f())) && ((i == 0) || (((AccountTransfer)this.a).l())))
          break;
        ((h)this.b).c(paramServiceResponse.g());
        return;
      }
      Intent localIntent1 = new Intent(((h)this.b).getApplicationContext(), PaymentsAndTransfersHomeActivity.class);
      localIntent1.addFlags(67108864);
      com.chase.sig.android.c.a(localIntent1);
      Intent localIntent2 = new Intent(((h)this.b).getBaseContext(), TransferEditVerifyActivity.class);
      localIntent2.putExtra("transaction_object", ((h)this.b).d());
      RequestFlags localRequestFlags;
      if (((h)this.b).c())
      {
        localRequestFlags = RequestFlags.d;
        localIntent2.putExtra("request_flags", localRequestFlags);
        if (i == 0)
          break label244;
        localIntent2.putExtra("queued_errors", (Serializable)localArrayList);
      }
      while (true)
      {
        ((h)this.b).startActivity(localIntent2);
        return;
        localRequestFlags = RequestFlags.c;
        break;
        label244: localIntent2.putExtra("queued_errors", (Serializable)paramServiceResponse.g());
      }
    }

    protected final int b()
    {
      return 2131165526;
    }

    protected final void b(ServiceResponse paramServiceResponse)
    {
      ((AccountTransfer)this.a).a_(paramServiceResponse.a());
      ((AccountTransfer)this.a).m(paramServiceResponse.j());
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.TransferEditActivity
 * JD-Core Version:    0.6.2
 */