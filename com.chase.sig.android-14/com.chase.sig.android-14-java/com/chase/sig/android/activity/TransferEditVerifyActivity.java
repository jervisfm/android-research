package com.chase.sig.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import com.chase.sig.android.domain.AccountTransfer;
import com.chase.sig.android.service.movemoney.RequestFlags;
import com.chase.sig.android.service.movemoney.d;
import com.chase.sig.android.service.n;
import com.chase.sig.android.util.Dollar;
import com.chase.sig.android.util.s;
import com.chase.sig.android.view.detail.DetailRow;
import com.chase.sig.android.view.detail.DetailView;
import com.chase.sig.android.view.detail.a;

public class TransferEditVerifyActivity extends k<AccountTransfer>
{
  public final void a(Bundle paramBundle)
  {
    b(2130903239);
    setTitle(2131165524);
    h();
    ((Button)findViewById(2131296977)).setText(2131165528);
    ((Button)findViewById(2131296978)).setText(2131165527);
  }

  protected final void a(boolean paramBoolean)
  {
    DetailView localDetailView = (DetailView)findViewById(2131296976);
    RequestFlags localRequestFlags = (RequestFlags)getIntent().getSerializableExtra("request_flags");
    if ((((AccountTransfer)this.a).l()) || (!localRequestFlags.b()));
    for (boolean bool = true; ; bool = false)
    {
      a[] arrayOfa = new a[8];
      arrayOfa[0] = new DetailRow("From", ((AccountTransfer)this.a).r() + ((AccountTransfer)this.a).s());
      arrayOfa[1] = new DetailRow("To", ((AccountTransfer)this.a).d_() + ((AccountTransfer)this.a).A());
      arrayOfa[2] = new DetailRow("Amount", ((AccountTransfer)this.a).e().h());
      arrayOfa[3] = new DetailRow("Send On", s.i(((AccountTransfer)this.a).b()));
      arrayOfa[4] = new DetailRow("Deliver By", s.i(((AccountTransfer)this.a).q()));
      DetailRow localDetailRow1 = new DetailRow("Frequency", a(((AccountTransfer)this.a).i()));
      localDetailRow1.j = bool;
      arrayOfa[5] = localDetailRow1;
      DetailRow localDetailRow2 = new DetailRow("Remaining Transfers", ((AccountTransfer)this.a).w());
      localDetailRow2.j = bool;
      arrayOfa[6] = localDetailRow2;
      arrayOfa[7] = new DetailRow("Memo", ((AccountTransfer)this.a).o());
      localDetailView.setRows(arrayOfa);
      a(2131296977, B());
      a(2131296978, new ml(this));
      return;
    }
  }

  protected final d<AccountTransfer> c()
  {
    A();
    return n.e();
  }

  protected final Class<TransferCompleteActivity> d()
  {
    return TransferCompleteActivity.class;
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    paramBundle.putSerializable("transaction_object", this.a);
  }

  public static class a extends k.a<AccountTransfer>
  {
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.TransferEditVerifyActivity
 * JD-Core Version:    0.6.2
 */