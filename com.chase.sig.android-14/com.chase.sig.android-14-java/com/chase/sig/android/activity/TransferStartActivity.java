package com.chase.sig.android.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.Time;
import android.widget.Button;
import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.domain.AccountTransfer;
import com.chase.sig.android.domain.IAccount;
import com.chase.sig.android.domain.g;
import com.chase.sig.android.domain.o;
import com.chase.sig.android.service.SplashResponse;
import com.chase.sig.android.service.movemoney.ServiceResponse;
import com.chase.sig.android.util.s;
import com.chase.sig.android.view.JPSpinner;
import com.chase.sig.android.view.b;
import com.chase.sig.android.view.b.a;
import com.chase.sig.android.view.detail.DetailView;
import com.chase.sig.android.view.detail.c;
import com.chase.sig.android.view.detail.q;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.List;

public class TransferStartActivity extends a
{
  private List<IAccount> a;
  private List<IAccount> b;
  private AccountTransfer c;
  private b.a d = new mo(this);

  private static int a(List<IAccount> paramList, String paramString)
  {
    for (int i = 0; i < paramList.size(); i++)
      if (((IAccount)paramList.get(i)).b().equals(paramString))
        return i;
    return 0;
  }

  private static boolean a(Bundle paramBundle, String paramString)
  {
    return (paramBundle != null) && (paramBundle.containsKey(paramString));
  }

  private Calendar d()
  {
    Time localTime = new Time();
    localTime.hour = Integer.valueOf(((ChaseApplication)getApplication()).c("transfer_cutoff_hour")).intValue();
    return com.chase.sig.android.util.l.a(localTime.hour);
  }

  public final void a(Bundle paramBundle)
  {
    int i = 1;
    super.a(paramBundle);
    setTitle(2131165498);
    if ((r().d != null) && (r().d.b()))
    {
      e(r().d.p());
      return;
    }
    g localg = r().b;
    this.b = localg.v();
    this.b.addAll(localg.y());
    this.a = localg.x();
    int k;
    int m;
    if ((this.b != null) && (this.a != null))
    {
      if (this.b.size() + this.a.size() == 0);
      for (int j = i; j != 0; j = 0)
      {
        f(2131165821);
        return;
      }
      if ((this.b.size() <= 0) || (this.a.size() <= 0))
      {
        k = i;
        if (k == 0)
        {
          if ((this.b.size() != i) || (this.a.size() != i) || (!((IAccount)this.b.get(0)).b().equals(((IAccount)this.a.get(0)).b())))
            break label273;
          m = i;
        }
      }
    }
    while (true)
      if (m != 0)
      {
        f(2131165822);
        return;
        k = 0;
        break;
        label273: m = 0;
        continue;
        f(2131165822);
        return;
      }
    DetailView localDetailView = (DetailView)findViewById(2131296969);
    com.chase.sig.android.view.detail.a[] arrayOfa = new com.chase.sig.android.view.detail.a[5];
    q localq1 = new q("From");
    localq1.b = "PAY_FROM";
    arrayOfa[0] = ((q)localq1).a(getString(2131165522));
    q localq2 = new q("To");
    localq2.b = "PAY_TO";
    arrayOfa[i] = ((q)localq2).a(getString(2131165523));
    c localc = new c("Amount $");
    localc.b = "AMOUNT";
    arrayOfa[2] = ((c)localc).a("Enter Amount");
    String str1;
    JPSpinner localJPSpinner2;
    Bundle localBundle;
    label621: JPSpinner localJPSpinner3;
    label629: String str2;
    if ((paramBundle != null) && (s.m(paramBundle.getString("deliverBy"))))
    {
      str1 = paramBundle.getString("deliverBy");
      com.chase.sig.android.view.detail.d locald1 = new com.chase.sig.android.view.detail.d("Deliver By", str1);
      locald1.b = "DATE";
      com.chase.sig.android.view.detail.d locald2 = (com.chase.sig.android.view.detail.d)locald1;
      locald2.a = i(0);
      arrayOfa[3] = locald2.q();
      com.chase.sig.android.view.detail.l locall1 = new com.chase.sig.android.view.detail.l("Memo");
      locall1.b = "MEMO";
      com.chase.sig.android.view.detail.l locall2 = (com.chase.sig.android.view.detail.l)((com.chase.sig.android.view.detail.l)locall1).a("Optional");
      locall2.a = 32;
      arrayOfa[4] = locall2;
      localDetailView.setRows(arrayOfa);
      JPSpinner localJPSpinner1 = ((q)a("PAY_FROM")).q();
      localJPSpinner1.setAdapter(com.chase.sig.android.util.a.a(this, this.a));
      localJPSpinner2 = ((q)a("PAY_TO")).q();
      localJPSpinner2.setAdapter(com.chase.sig.android.util.a.e(this, this.b));
      localBundle = getIntent().getExtras();
      if ((!a(localBundle, "isDebitAccount")) || (!localBundle.getBoolean("isDebitAccount")))
        break label722;
      if (i == 0)
        break label727;
      localJPSpinner3 = localJPSpinner1;
      n = -1;
      if (a(localBundle, "selectedAccountId"))
      {
        str2 = localBundle.getString("selectedAccountId");
        if (i == 0)
          break label734;
      }
    }
    label722: label727: label734: int i1;
    for (int n = a(this.a, str2); ; n = i1)
    {
      localJPSpinner3.setSelection(n);
      ((Button)findViewById(2131296970)).setText(2131165531);
      a(2131296971, new mn(this));
      return;
      str1 = s.a(d().getTime());
      break;
      i = 0;
      break label621;
      localJPSpinner3 = localJPSpinner2;
      break label629;
      i1 = a(this.b, str2);
      String str3 = localBundle.getString("selectedAccountId");
      String str4 = (String)r().b.a(str3).e().get("nextPaymentAmount");
      ((c)a("AMOUNT")).b(str4);
    }
  }

  public final void k()
  {
    y();
    a().a();
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    Dialog localDialog = super.onCreateDialog(paramInt);
    switch (paramInt)
    {
    default:
      return localDialog;
    case 0:
    }
    return a(d(), this.d).a(s.t(a().e("DATE").g()));
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
  }

  public static class a extends com.chase.sig.android.d<TransferStartActivity, AccountTransfer, Void, ServiceResponse>
  {
    private AccountTransfer a;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.TransferStartActivity
 * JD-Core Version:    0.6.2
 */