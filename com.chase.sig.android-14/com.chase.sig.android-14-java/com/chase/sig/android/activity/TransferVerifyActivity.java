package com.chase.sig.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import com.chase.sig.android.c.a;
import com.chase.sig.android.d;
import com.chase.sig.android.domain.AccountTransfer;
import com.chase.sig.android.service.movemoney.RequestFlags;
import com.chase.sig.android.service.movemoney.ServiceResponse;
import com.chase.sig.android.service.n;
import com.chase.sig.android.util.Dollar;
import com.chase.sig.android.util.s;
import com.chase.sig.android.view.detail.DetailRow;
import com.chase.sig.android.view.detail.DetailView;

public class TransferVerifyActivity extends ai
  implements fk.e, c.a
{
  private DetailView a = null;
  private AccountTransfer b;

  public final void a()
  {
    Intent localIntent = new Intent(this, PaymentsAndTransfersHomeActivity.class);
    localIntent.setFlags(67108864);
    startActivity(localIntent);
  }

  public final void a(Bundle paramBundle)
  {
    b(2130903135);
    setTitle(2131165524);
    this.b = ((AccountTransfer)getIntent().getExtras().get("transaction_object"));
    this.a = ((DetailView)findViewById(2131296595));
    DetailView localDetailView = this.a;
    com.chase.sig.android.view.detail.a[] arrayOfa = new com.chase.sig.android.view.detail.a[6];
    arrayOfa[0] = new DetailRow("From", this.b.e_());
    arrayOfa[1] = new DetailRow("To", this.b.d());
    arrayOfa[2] = new DetailRow("Amount", this.b.e().h());
    arrayOfa[3] = new DetailRow("Send On", s.i(this.b.b()));
    arrayOfa[4] = new DetailRow("Deliver By", s.i(this.b.q()));
    if (s.m(this.b.o()));
    for (String str = this.b.o(); ; str = "")
    {
      arrayOfa[5] = new DetailRow("Memo ", str);
      localDetailView.setRows(arrayOfa);
      ((Button)findViewById(2131296599)).setText(2131165527);
      ((Button)findViewById(2131296598)).setText(2131165528);
      a(2131296599, new mp(this));
      a(2131296598, a(this));
      return;
    }
  }

  public static class a extends d<TransferVerifyActivity, Void, Void, ServiceResponse>
  {
    private ServiceResponse a()
    {
      try
      {
        ((TransferVerifyActivity)this.b).A();
        ServiceResponse localServiceResponse = n.e().b(TransferVerifyActivity.a((TransferVerifyActivity)this.b), RequestFlags.e);
        ((TransferVerifyActivity)this.b).l();
        return localServiceResponse;
      }
      catch (Exception localException)
      {
        localException.getMessage();
      }
      return null;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.TransferVerifyActivity
 * JD-Core Version:    0.6.2
 */