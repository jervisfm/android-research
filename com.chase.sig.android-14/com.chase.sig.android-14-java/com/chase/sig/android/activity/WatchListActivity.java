package com.chase.sig.android.activity;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.chase.sig.android.b;
import com.chase.sig.android.domain.Quote;
import com.chase.sig.android.domain.WatchList;
import com.chase.sig.android.service.WatchListResponse;
import com.chase.sig.android.util.Dollar;
import com.chase.sig.android.util.e;
import com.chase.sig.android.util.s;
import com.chase.sig.android.view.TickerValueTextView;
import java.util.ArrayList;
import java.util.List;

public class WatchListActivity extends ai
  implements fk.c
{
  private ListView a;
  private WatchListResponse b;
  private TextView c;

  private void a(List<WatchList> paramList)
  {
    if ((paramList == null) || ((paramList != null) && (paramList.size() == 0)))
    {
      this.a.setVisibility(8);
      this.c.setVisibility(0);
      return;
    }
    this.a.setVisibility(0);
    this.c.setVisibility(8);
    a(this.a);
    b localb = new b(this, paramList);
    this.a.setAdapter(localb);
  }

  public final void a(Bundle paramBundle)
  {
    setTitle(2131165901);
    b(2130903243);
    this.a = ((ListView)findViewById(2131296986));
    this.a.setItemsCanFocus(true);
    this.c = ((TextView)findViewById(2131296891));
    if (e.a(paramBundle, "watch_list_response"))
    {
      this.b = ((WatchListResponse)e.a(paramBundle, "watch_list_response", null));
      if (this.b != null);
      for (Object localObject = this.b.a(); ; localObject = new ArrayList())
      {
        a((List)localObject);
        return;
      }
    }
    a(a.class, new String[0]);
  }

  protected final void c_()
  {
    super.c_();
    finish();
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    paramBundle.putSerializable("watch_list_response", this.b);
  }

  public static class a extends b<WatchListActivity, String, Void, WatchListResponse>
  {
  }

  final class b extends ArrayAdapter<WatchList>
  {
    private final List<WatchList> b;
    private final LayoutInflater c;

    public b(int arg2)
    {
      super(2130903244, localList);
      this.b = localList;
      this.c = ((LayoutInflater)localContext.getSystemService("layout_inflater"));
    }

    private void a(View paramView, Quote paramQuote)
    {
      a(paramView, paramQuote, 2131296996, 2131296997, 2131296998, 2131296999);
    }

    private void a(View paramView, Quote paramQuote, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
    {
      ((TextView)paramView.findViewById(paramInt1)).setText(paramQuote.l());
      ((TextView)paramView.findViewById(paramInt2)).setText(paramQuote.i().h());
      Dollar localDollar = paramQuote.b();
      String str;
      if (localDollar == null)
      {
        ((TickerValueTextView)paramView.findViewById(paramInt3)).a(new Dollar("0.00").h(), false);
        str = paramQuote.d();
        if (str != null)
          break label127;
        ((TickerValueTextView)paramView.findViewById(paramInt4)).a("0.00", true);
      }
      while (true)
      {
        paramView.setOnClickListener(new mq(this, paramQuote));
        return;
        ((TickerValueTextView)paramView.findViewById(paramInt3)).setTickerValue(localDollar);
        break;
        label127: ((TickerValueTextView)paramView.findViewById(paramInt4)).a(s.C(str), paramQuote.c());
      }
    }

    private void b(View paramView, Quote paramQuote)
    {
      a(paramView, paramQuote, 2131297003, 2131297004, 2131297005, 2131297006);
    }

    public final View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      WatchList localWatchList = (WatchList)this.b.get(paramInt);
      if (paramView == null)
        paramView = this.c.inflate(2130903244, paramViewGroup, false);
      ((TextView)paramView.findViewById(2131296988)).setText(localWatchList.a());
      List localList = localWatchList.b();
      if ((localList != null) && (localList.size() > 0) && (localList.size() == 1))
      {
        paramView.findViewById(2131296995).setVisibility(0);
        paramView.findViewById(2131297001).setVisibility(8);
        a(paramView.findViewById(2131296995), (Quote)localList.get(0));
        return paramView;
      }
      if ((localList != null) && (localList.size() > 0) && (localList.size() == 2))
      {
        paramView.findViewById(2131296995).setVisibility(0);
        paramView.findViewById(2131297002).setVisibility(0);
        paramView.findViewById(2131297008).setVisibility(8);
        a(paramView.findViewById(2131296995), (Quote)localList.get(0));
        b(paramView.findViewById(2131297002), (Quote)localList.get(1));
        return paramView;
      }
      paramView.findViewById(2131296995).setVisibility(0);
      paramView.findViewById(2131297002).setVisibility(0);
      paramView.findViewById(2131297009).setVisibility(0);
      a(paramView.findViewById(2131296995), (Quote)localList.get(0));
      b(paramView.findViewById(2131297002), (Quote)localList.get(1));
      a(paramView.findViewById(2131297009), (Quote)localList.get(2), 2131297010, 2131297011, 2131297012, 2131297013);
      return paramView;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.WatchListActivity
 * JD-Core Version:    0.6.2
 */