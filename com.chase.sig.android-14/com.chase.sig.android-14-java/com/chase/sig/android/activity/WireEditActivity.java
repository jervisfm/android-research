package com.chase.sig.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.CheckBox;
import com.chase.sig.android.activity.a.g;
import com.chase.sig.android.activity.wire.WireEditVerifyActivity;
import com.chase.sig.android.domain.WireTransaction;
import com.chase.sig.android.service.ServiceError;
import com.chase.sig.android.service.movemoney.RequestFlags;
import com.chase.sig.android.service.movemoney.ServiceResponse;
import com.chase.sig.android.service.n;
import com.chase.sig.android.util.e;
import com.chase.sig.android.util.s;
import com.chase.sig.android.view.AmountView;
import com.chase.sig.android.view.detail.DetailRow;
import com.chase.sig.android.view.detail.DetailView;
import com.chase.sig.android.view.detail.a;
import com.chase.sig.android.view.detail.j;
import com.chase.sig.android.view.detail.l;
import com.chase.sig.android.view.detail.o;
import com.chase.sig.android.view.detail.q;
import com.chase.sig.android.view.detail.r;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class WireEditActivity extends h<WireTransaction>
{
  private Date b = null;

  private boolean D()
  {
    return ((r)a().e("REMAINING")).q().isChecked();
  }

  public final void a(Bundle paramBundle)
  {
    super.a(paramBundle);
    setTitle(2131165484);
    boolean bool1 = e.b(paramBundle, "memo_fields_visible");
    WireTransaction localWireTransaction = (WireTransaction)d();
    DetailView localDetailView = a();
    a[] arrayOfa = new a[10];
    arrayOfa[0] = new DetailRow("Recipient", localWireTransaction.d());
    arrayOfa[1] = new DetailRow("From", localWireTransaction.m());
    com.chase.sig.android.view.detail.c localc = new com.chase.sig.android.view.detail.c("Amount $ ", localWireTransaction.e());
    localc.b = "AMOUNT";
    arrayOfa[2] = localc;
    com.chase.sig.android.view.detail.d locald1 = new com.chase.sig.android.view.detail.d("Date ", s.i(localWireTransaction.q()));
    locald1.b = "DATE";
    com.chase.sig.android.view.detail.d locald2 = (com.chase.sig.android.view.detail.d)locald1;
    locald2.a = i(0);
    arrayOfa[3] = locald2;
    o localo = a(localWireTransaction);
    localo.b = "FREQUENCY";
    q localq = (q)localo;
    localq.j = b();
    arrayOfa[4] = localq;
    r localr = new r("Remaining Wires", localWireTransaction.x());
    localr.b = "REMAINING";
    j localj = (j)localr;
    localj.j = b();
    arrayOfa[5] = localj;
    com.chase.sig.android.view.detail.d locald3 = (com.chase.sig.android.view.detail.d)new com.chase.sig.android.view.detail.d("", "").a("Enter message or memo");
    locald3.b = "MEMO_FIELDS";
    com.chase.sig.android.view.detail.d locald4 = (com.chase.sig.android.view.detail.d)locald3;
    locald4.n = true;
    com.chase.sig.android.view.detail.d locald5 = (com.chase.sig.android.view.detail.d)locald4;
    locald5.a = new g(a(), new String[] { "MESSAGE_TO_RECIPIENT", "TO_RECIPIENT_BANK", "MEMO", "MEMO_FIELDS" });
    locald5.j = bool1;
    arrayOfa[6] = locald5;
    l locall1 = (l)new l("Message To Recipient ", s.s(localWireTransaction.f())).a("Optional");
    locall1.b = "MESSAGE_TO_RECIPIENT";
    l locall2 = (l)locall1;
    locall2.a = 140;
    boolean bool2;
    boolean bool3;
    label453: l locall6;
    if (!bool1)
    {
      bool2 = true;
      locall2.j = bool2;
      arrayOfa[7] = locall2;
      l locall3 = (l)new l("To Recipient's Bank ", s.s(localWireTransaction.h())).a("Optional");
      locall3.b = "TO_RECIPIENT_BANK";
      l locall4 = (l)locall3;
      locall4.a = 100;
      if (bool1)
        break label571;
      bool3 = true;
      locall4.j = bool3;
      arrayOfa[8] = locall4;
      l locall5 = (l)new l("Memo ", s.s(localWireTransaction.o())).a("Optional");
      locall5.b = "MEMO";
      locall6 = (l)locall5;
      locall6.a = 100;
      if (bool1)
        break label577;
    }
    label571: label577: for (boolean bool4 = true; ; bool4 = false)
    {
      locall6.j = bool4;
      arrayOfa[9] = locall6;
      localDetailView.setRows(arrayOfa);
      if (!e.a(paramBundle, "earliest_payment_date"))
        break label583;
      this.b = s.t(paramBundle.getString("earliest_payment_date"));
      return;
      bool2 = false;
      break;
      bool3 = false;
      break label453;
    }
    label583: a(a.class, new WireTransaction[] { localWireTransaction });
  }

  protected final boolean g()
  {
    boolean bool = true;
    y();
    if (!((com.chase.sig.android.view.detail.c)a().e("AMOUNT")).p().a())
    {
      a().a("AMOUNT");
      bool = false;
    }
    if ((!b()) && (!D()) && (!((r)a().e("REMAINING")).n()))
    {
      a().a("REMAINING");
      bool = false;
    }
    return bool;
  }

  protected final Class<b> h()
  {
    return b.class;
  }

  protected final Class<? extends eb> i()
  {
    return WireEditVerifyActivity.class;
  }

  protected final Date j()
  {
    return this.b;
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    paramBundle.putBoolean("memo_fields_visible", a().e("MEMO_FIELDS").j);
    if (this.b != null)
      paramBundle.putString("earliest_payment_date", s.a(this.b));
  }

  public static class a extends com.chase.sig.android.d<WireEditActivity, WireTransaction, Void, Void>
  {
  }

  public static class b extends h.a<WireTransaction>
  {
    protected final com.chase.sig.android.service.movemoney.d<WireTransaction> a()
    {
      ((h)this.b).A();
      return n.f();
    }

    protected final void a(ServiceResponse paramServiceResponse)
    {
      if (paramServiceResponse.c("70011"));
      ArrayList localArrayList;
      for (int i = 1; ; i = 0)
      {
        String str = ((h)this.b).getString(2131165471);
        localArrayList = new ArrayList();
        localArrayList.add(new ServiceError("70011", str, false));
        b(paramServiceResponse);
        if (((i != 0) || (!paramServiceResponse.f())) && ((i == 0) || (((WireTransaction)this.a).l())))
          break;
        ((h)this.b).c(paramServiceResponse.g());
        return;
      }
      Intent localIntent1 = new Intent(((h)this.b).getApplicationContext(), PaymentsAndTransfersHomeActivity.class);
      localIntent1.addFlags(67108864);
      com.chase.sig.android.c.a(localIntent1);
      Intent localIntent2 = new Intent(((h)this.b).getBaseContext(), WireEditVerifyActivity.class);
      localIntent2.putExtra("transaction_object", ((h)this.b).d());
      RequestFlags localRequestFlags;
      if (((h)this.b).c())
      {
        localRequestFlags = RequestFlags.d;
        localIntent2.putExtra("request_flags", localRequestFlags);
        if (i == 0)
          break label244;
        localIntent2.putExtra("queued_errors", (Serializable)localArrayList);
      }
      while (true)
      {
        ((h)this.b).startActivity(localIntent2);
        return;
        localRequestFlags = RequestFlags.c;
        break;
        label244: localIntent2.putExtra("queued_errors", (Serializable)paramServiceResponse.g());
      }
    }

    protected final int b()
    {
      return 2131165495;
    }

    protected final void b(ServiceResponse paramServiceResponse)
    {
      ((WireTransaction)this.a).m(paramServiceResponse.j());
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.WireEditActivity
 * JD-Core Version:    0.6.2
 */