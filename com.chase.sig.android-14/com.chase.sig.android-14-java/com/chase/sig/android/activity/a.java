package com.chase.sig.android.activity;

import android.app.Dialog;
import android.os.Bundle;
import com.chase.sig.android.c;
import com.chase.sig.android.util.s;
import com.chase.sig.android.view.b;
import com.chase.sig.android.view.detail.DetailView;
import com.chase.sig.android.view.detail.q;
import java.util.Date;

public abstract class a extends ai
  implements fk.e, ma
{
  protected DetailView a()
  {
    return (DetailView)findViewById(2131296969);
  }

  protected final <T extends com.chase.sig.android.view.detail.a> T a(String paramString)
  {
    return a().e(paramString);
  }

  public void a(Bundle paramBundle)
  {
    b(2130903236);
    a(2131296970, new com.chase.sig.android.activity.a.a(this));
  }

  protected final q b(String paramString)
  {
    return (q)a(paramString);
  }

  protected void onPrepareDialog(int paramInt, Dialog paramDialog)
  {
    String str;
    if (paramInt == 0)
    {
      str = a().e("DATE").g();
      if (!s.l(str))
        break label38;
    }
    label38: for (Date localDate = null; ; localDate = s.t(str))
    {
      ((b)paramDialog).a(localDate);
      return;
    }
  }

  protected void onRestart()
  {
    super.onRestart();
    c.a();
  }

  protected void onResume()
  {
    super.onResume();
    c.a();
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.a
 * JD-Core Version:    0.6.2
 */