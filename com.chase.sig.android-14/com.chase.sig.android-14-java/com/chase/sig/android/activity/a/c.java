package com.chase.sig.android.activity.a;

import android.app.Activity;
import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;

public final class c
  implements View.OnClickListener
{
  private Activity a;

  public c(Activity paramActivity)
  {
    this.a = paramActivity;
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    this.a.finish();
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.a.c
 * JD-Core Version:    0.6.2
 */