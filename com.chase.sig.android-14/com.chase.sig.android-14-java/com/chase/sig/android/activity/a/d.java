package com.chase.sig.android.activity.a;

import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.activity.eb;
import com.chase.sig.android.c;
import com.chase.sig.android.c.a;

public final class d
  implements View.OnClickListener
{
  private eb a;
  private c.a b;

  public d(eb parameb)
  {
    this.a = parameb;
  }

  public d(eb parameb, c.a parama)
  {
    this.a = parameb;
    this.b = parama;
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    if (this.b != null)
    {
      c.a(this.a, this.b);
      return;
    }
    this.a.showDialog(-4);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.a.d
 * JD-Core Version:    0.6.2
 */