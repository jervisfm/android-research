package com.chase.sig.android.activity.a;

import android.app.Activity;
import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;

public final class e
  implements View.OnClickListener
{
  private int a;
  private Activity b;

  public e(Activity paramActivity, int paramInt)
  {
    this.a = paramInt;
    this.b = paramActivity;
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    this.b.showDialog(this.a);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.a.e
 * JD-Core Version:    0.6.2
 */