package com.chase.sig.android.activity.a;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.activity.eb;
import java.io.Serializable;

public final class f
  implements View.OnClickListener
{
  private eb a;
  private Class<? extends eb> b;
  private boolean c = false;
  private boolean d = false;
  private Bundle e = new Bundle();
  private boolean f;
  private boolean g;

  public f(eb parameb, Class<? extends eb> paramClass)
  {
    this.a = parameb;
    this.b = paramClass;
  }

  public final f a()
  {
    this.c = true;
    return this;
  }

  public final f a(String paramString, Serializable paramSerializable)
  {
    this.e.putSerializable(paramString, paramSerializable);
    return this;
  }

  public final f a(String paramString1, String paramString2)
  {
    this.e.putString(paramString1, paramString2);
    return this;
  }

  public final f b()
  {
    this.g = true;
    return this;
  }

  public final f c()
  {
    this.d = true;
    return this;
  }

  public final f d()
  {
    this.f = true;
    return this;
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    Intent localIntent = new Intent(this.a, this.b);
    if (this.f)
      localIntent.addFlags(1073741824);
    if (this.g)
      localIntent.addFlags(67108864);
    if (this.d)
      localIntent.putExtras(this.a.getIntent());
    if ((this.e != null) && (!this.e.isEmpty()))
      localIntent.putExtras(this.e);
    this.a.startActivity(localIntent);
    if (this.c)
      this.a.finish();
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.a.f
 * JD-Core Version:    0.6.2
 */