package com.chase.sig.android.activity.a;

import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.view.detail.DetailView;
import com.chase.sig.android.view.detail.a;

public final class g
  implements View.OnClickListener
{
  private final DetailView a;
  private final String[] b;

  public g(DetailView paramDetailView, String[] paramArrayOfString)
  {
    this.a = paramDetailView;
    this.b = paramArrayOfString;
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    for (String str : this.b)
      this.a.e(str).b();
    this.a.c();
    this.a.e(this.b[0]).k().requestFocus();
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.a.g
 * JD-Core Version:    0.6.2
 */