package com.chase.sig.android.activity;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.domain.MobileAdResponse;

final class ab
  implements View.OnClickListener
{
  ab(AccountsActivity paramAccountsActivity, MobileAdResponse paramMobileAdResponse)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    Intent localIntent = new Intent(this.b, MobileAdOfferDetailsActivity.class);
    localIntent.putExtra("ad_offer", this.a);
    localIntent.setFlags(1073741824);
    this.b.startActivityForResult(localIntent, 1010);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.ab
 * JD-Core Version:    0.6.2
 */