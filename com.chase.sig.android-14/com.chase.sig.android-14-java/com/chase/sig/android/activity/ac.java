package com.chase.sig.android.activity;

import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;

final class ac
  implements View.OnClickListener
{
  ac(AccountsActivity paramAccountsActivity)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    TranslateAnimation localTranslateAnimation = new TranslateAnimation(1, 0.0F, 1, 0.0F, 1, 0.0F, 1, 1.0F);
    localTranslateAnimation.setDuration(300L);
    localTranslateAnimation.setAnimationListener(new ad(this));
    AccountsActivity.b(this.a).startAnimation(localTranslateAnimation);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.ac
 * JD-Core Version:    0.6.2
 */