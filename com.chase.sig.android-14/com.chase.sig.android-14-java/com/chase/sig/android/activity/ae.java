package com.chase.sig.android.activity;

import android.os.AsyncTask;
import android.os.AsyncTask.Status;
import android.os.Handler;
import com.chase.sig.android.service.JPService.CrossSiteRequestForgeryTokenFailureException;
import com.chase.sig.android.util.ChaseException;
import java.util.Hashtable;

public abstract class ae<ActivityType extends eb, Params, Progress, Result> extends AsyncTask<Params, Progress, Result>
{
  private int a = 0;
  public ActivityType b;
  mc c;
  public boolean d;

  protected abstract Result a(Params[] paramArrayOfParams);

  public final void a(ActivityType paramActivityType)
  {
    this.b = paramActivityType;
  }

  public final void a(mc parammc)
  {
    this.c = parammc;
  }

  protected void a(Result paramResult)
  {
  }

  public final void c()
  {
    this.d = true;
    if (this.c != null)
    {
      mc localmc = this.c;
      Class localClass = getClass();
      localmc.a.remove(localClass);
    }
  }

  public final boolean d()
  {
    return getStatus() == AsyncTask.Status.RUNNING;
  }

  protected Result doInBackground(Params[] paramArrayOfParams)
  {
    try
    {
      Object localObject = a(paramArrayOfParams);
      return localObject;
    }
    catch (JPService.CrossSiteRequestForgeryTokenFailureException localCrossSiteRequestForgeryTokenFailureException)
    {
      this.b.d(false);
      return null;
    }
    catch (ChaseException localChaseException)
    {
      label19: break label19;
    }
  }

  public boolean e()
  {
    return true;
  }

  public final void f()
  {
    this.d = true;
  }

  public void onPostExecute(Result paramResult)
  {
    int i = 1;
    super.onPostExecute(paramResult);
    if (this.d)
    {
      new StringBuilder("Application has told %s to stop.").append(this.d).toString();
      new Object[i][0] = getClass().getName();
    }
    while (true)
    {
      return;
      if (!this.b.g)
        break;
      if (e())
      {
        new Object[i][0] = getClass().getName();
        this.a = (1 + this.a);
        if (this.a > 710);
        while (i == 0)
        {
          new Handler().postDelayed(new af(this, paramResult), 1000L);
          return;
          i = 0;
        }
      }
    }
    a(paramResult);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.ae
 * JD-Core Version:    0.6.2
 */