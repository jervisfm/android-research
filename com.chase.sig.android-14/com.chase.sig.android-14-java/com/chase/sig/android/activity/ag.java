package com.chase.sig.android.activity;

import android.content.res.Resources;
import android.os.Bundle;
import android.text.Html;
import com.chase.sig.android.domain.QuoteNewsArticle;
import com.chase.sig.android.service.IServiceError;
import com.chase.sig.android.service.QuoteNewsResponse;
import com.chase.sig.android.view.detail.DetailView;
import com.chase.sig.android.view.detail.a;
import com.chase.sig.android.view.detail.t;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class ag<T extends eb>
{
  public static QuoteNewsResponse a(Bundle paramBundle)
  {
    return (QuoteNewsResponse)paramBundle.getSerializable("quote_news_response");
  }

  public final void a(T paramT, QuoteNewsResponse paramQuoteNewsResponse, DetailView paramDetailView, Class<? extends ae<?, String, ?, ?>> paramClass, String[] paramArrayOfString)
  {
    paramDetailView.d();
    if (paramQuoteNewsResponse == null)
    {
      paramT.a(paramClass, paramArrayOfString);
      return;
    }
    a(paramT, paramDetailView, paramQuoteNewsResponse);
  }

  public final void a(T paramT, DetailView paramDetailView, QuoteNewsResponse paramQuoteNewsResponse)
  {
    com.chase.sig.android.view.detail.s[] arrayOfs;
    String str2;
    if (paramQuoteNewsResponse.e())
    {
      arrayOfs = new com.chase.sig.android.view.detail.s[1];
      if ((paramQuoteNewsResponse.e()) && (paramQuoteNewsResponse.c("20165")))
      {
        str2 = paramQuoteNewsResponse.d("20165").a();
        arrayOfs[0] = new com.chase.sig.android.view.detail.s(str2);
      }
    }
    ArrayList localArrayList2;
    for (Object localObject = arrayOfs; ; localObject = (t[])localArrayList2.toArray(new t[localArrayList2.size()]))
    {
      paramDetailView.setRows((a[])localObject);
      paramDetailView.setVisibility(0);
      return;
      str2 = paramT.getResources().getText(2131165898).toString();
      break;
      ArrayList localArrayList1 = paramQuoteNewsResponse.a();
      localArrayList2 = new ArrayList();
      Iterator localIterator = localArrayList1.iterator();
      while (localIterator.hasNext())
      {
        QuoteNewsArticle localQuoteNewsArticle = (QuoteNewsArticle)localIterator.next();
        if (localQuoteNewsArticle != null)
        {
          String str1 = Html.fromHtml(localQuoteNewsArticle.c().trim()).toString();
          String[] arrayOfString = new String[2];
          arrayOfString[0] = com.chase.sig.android.util.s.s(localQuoteNewsArticle.b());
          arrayOfString[1] = com.chase.sig.android.util.s.c(localQuoteNewsArticle.d());
          t localt = new t(str1, com.chase.sig.android.util.s.a(" ", arrayOfString).trim());
          localt.m = true;
          localt.h = new ah(this, paramT, localQuoteNewsArticle);
          localArrayList2.add(localt);
        }
      }
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.ag
 * JD-Core Version:    0.6.2
 */