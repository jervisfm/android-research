package com.chase.sig.android.activity;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.domain.QuoteNewsArticle;

final class ah
  implements View.OnClickListener
{
  ah(ag paramag, eb parameb, QuoteNewsArticle paramQuoteNewsArticle)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    Intent localIntent = new Intent(this.a, ArticleActivity.class);
    localIntent.putExtra("article_content", this.b);
    this.a.startActivity(localIntent);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.ah
 * JD-Core Version:    0.6.2
 */