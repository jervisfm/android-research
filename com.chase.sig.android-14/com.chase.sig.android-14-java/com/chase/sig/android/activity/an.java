package com.chase.sig.android.activity;

import android.os.AsyncTask.Status;
import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.domain.BillPayTransaction;

final class an
  implements View.OnClickListener
{
  an(BillPayAddVerifyActivity paramBillPayAddVerifyActivity, BillPayTransaction paramBillPayTransaction)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    BillPayAddVerifyActivity.a locala = (BillPayAddVerifyActivity.a)this.b.e.a(BillPayAddVerifyActivity.a.class);
    if (locala.getStatus() != AsyncTask.Status.RUNNING)
    {
      BillPayTransaction[] arrayOfBillPayTransaction = new BillPayTransaction[1];
      arrayOfBillPayTransaction[0] = this.a;
      locala.execute(arrayOfBillPayTransaction);
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.an
 * JD-Core Version:    0.6.2
 */