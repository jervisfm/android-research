package com.chase.sig.android.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.service.SplashResponse;

final class ar
  implements View.OnClickListener
{
  ar(BillPayHomeActivity paramBillPayHomeActivity)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    if (!this.a.z().t())
    {
      this.a.g(2131165556);
      return;
    }
    if ((this.a.s() != null) && (this.a.s().m()))
    {
      this.a.f(this.a.s().u());
      return;
    }
    Intent localIntent = new Intent(this.a, BillPayPayeeImageCaptureActivity.class);
    localIntent.setFlags(1073741824);
    localIntent.putExtra("selectedAccountId", BillPayHomeActivity.a(this.a));
    if (this.a.getPreferences(0).getBoolean("photoBillPayPayeeShouldShowInstructions", true))
    {
      Bundle localBundle = new Bundle();
      localBundle.putParcelable("FORWARDING_INTENT_KEY", localIntent);
      this.a.showDialog(42, localBundle);
      return;
    }
    this.a.startActivity(localIntent);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.ar
 * JD-Core Version:    0.6.2
 */