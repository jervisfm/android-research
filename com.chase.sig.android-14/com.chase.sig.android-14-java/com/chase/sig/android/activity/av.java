package com.chase.sig.android.activity;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.domain.Payee;
import java.util.List;

final class av
  implements View.OnClickListener
{
  av(BillPayHomeActivity.a parama, Payee paramPayee)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    Intent localIntent = new Intent(this.b.a, BillPayAddStartActivity.class);
    localIntent.putExtra("payee", this.a);
    localIntent.putExtra("selectedAccountId", BillPayHomeActivity.a(this.b.a));
    localIntent.putExtra("payeeCount", BillPayHomeActivity.a.a(this.b).size());
    this.b.a.startActivity(localIntent);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.av
 * JD-Core Version:    0.6.2
 */