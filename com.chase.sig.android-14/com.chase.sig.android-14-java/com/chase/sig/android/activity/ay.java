package com.chase.sig.android.activity;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.domain.BillPayVerifyPayeeInfo;
import com.chase.sig.android.domain.MerchantPayee;

final class ay
  implements View.OnClickListener
{
  ay(BillPayPayeeAddActivity paramBillPayPayeeAddActivity)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    if (this.a.a())
    {
      BillPayPayeeAddActivity.a(this.a);
      BillPayPayeeAddActivity.b(this.a).c().a(null);
      Intent localIntent = new Intent(this.a, BillPayPayeeAddVerifyActivity.class);
      localIntent.putExtra("payee_info", BillPayPayeeAddActivity.b(this.a));
      localIntent.putExtra("from_merchant_directory", BillPayPayeeAddActivity.b(this.a).a());
      this.a.startActivityForResult(localIntent, 10);
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.ay
 * JD-Core Version:    0.6.2
 */