package com.chase.sig.android.activity;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.ImageView;
import com.chase.sig.android.domain.Chart;
import com.chase.sig.android.domain.ChartImageCache;
import com.chase.sig.android.domain.ImageDownloadResponse;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public abstract class b extends ai
  implements fk.c
{
  protected ChartImageCache a = new ChartImageCache();
  private final int b;
  private final int c;

  protected b(int paramInt1, int paramInt2)
  {
    this.b = paramInt1;
    this.c = paramInt2;
  }

  private void a(Bitmap paramBitmap)
  {
    ((ImageView)findViewById(this.c)).setImageBitmap(paramBitmap);
  }

  private void a(Bitmap paramBitmap, Chart paramChart)
  {
    a(paramBitmap);
    this.a.a(paramChart, paramBitmap);
  }

  public final void a(Chart paramChart)
  {
    b(paramChart);
  }

  protected final void a(Chart paramChart, ImageDownloadResponse paramImageDownloadResponse)
  {
    HashMap localHashMap = new HashMap();
    localHashMap.put(paramChart, paramImageDownloadResponse);
    a(localHashMap, paramChart);
  }

  protected void a(Map<Chart, ImageDownloadResponse> paramMap, Chart paramChart)
  {
    Iterator localIterator = paramMap.entrySet().iterator();
    while (localIterator.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      Chart localChart = (Chart)localEntry.getKey();
      ImageDownloadResponse localImageDownloadResponse = (ImageDownloadResponse)localEntry.getValue();
      if (localImageDownloadResponse.b())
        this.a.a(localChart, localImageDownloadResponse.a());
      else
        this.a.a(localChart, BitmapFactory.decodeResource(getApplicationContext().getResources(), this.b));
    }
    a(this.a.b(paramChart));
  }

  protected final boolean a(ImageDownloadResponse paramImageDownloadResponse, Chart paramChart)
  {
    if (paramImageDownloadResponse.b())
    {
      a(paramImageDownloadResponse.a(), paramChart);
      return true;
    }
    a(BitmapFactory.decodeResource(getApplicationContext().getResources(), this.b), paramChart);
    return false;
  }

  protected final void b(Bundle paramBundle)
  {
    this.a = ((ChartImageCache)paramBundle.getParcelable("chart_images"));
  }

  protected final void b(Chart paramChart)
  {
    if (this.a.a(paramChart))
      a(this.a.b(paramChart));
  }

  protected final void c(Bundle paramBundle)
  {
    paramBundle.putParcelable("chart_images", this.a);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.b
 * JD-Core Version:    0.6.2
 */