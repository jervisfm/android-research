package com.chase.sig.android.activity;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;

final class ba
  implements View.OnClickListener
{
  ba(BillPayPayeeAddVerifyActivity paramBillPayPayeeAddVerifyActivity)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    Intent localIntent = new Intent(this.a, BillPayPayeeAddActivity.class);
    localIntent.putExtra("payee_info", BillPayPayeeAddVerifyActivity.a(this.a));
    this.a.startActivityForResult(localIntent, 50);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.ba
 * JD-Core Version:    0.6.2
 */