package com.chase.sig.android.activity;

import com.chase.sig.android.domain.State;
import com.chase.sig.android.view.detail.l;
import com.chase.sig.android.view.detail.q;

public final class bd
{
  public static l a(String paramString)
  {
    l locall1 = new l(2131165336, paramString);
    locall1.b = "BILLPAY_MESSAGE";
    l locall2 = (l)((l)locall1).c(2131165337);
    locall2.p = true;
    l locall3 = ((l)locall2).q();
    locall3.a = 32;
    return locall3;
  }

  public static l b(String paramString)
  {
    l locall1 = new l(2131165335, paramString);
    locall1.b = "PHONE_NUMBER";
    l locall2 = (l)((l)locall1).c(2131165311);
    locall2.u = 5;
    return (l)locall2.c();
  }

  public static l c(String paramString)
  {
    l locall1 = new l(2131165324, paramString);
    locall1.b = "ZIP_CODE";
    l locall2 = (l)locall1;
    locall2.r = "zipCode";
    l locall3 = (l)((l)locall2).c(2131165325);
    locall3.o = true;
    l locall4 = (l)locall3;
    locall4.u = 4;
    locall4.a = 10;
    return (l)locall4.c();
  }

  public static l d(String paramString)
  {
    l locall1 = new l(2131165327, paramString);
    locall1.b = "CITY";
    l locall2 = (l)((l)locall1).c(2131165328);
    locall2.o = true;
    l locall3 = (l)locall2;
    locall3.v = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890 '.,-";
    locall3.a = 30;
    return (l)locall3.c();
  }

  public static l e(String paramString)
  {
    l locall1 = new l(2131165330, paramString);
    locall1.b = "ADDRESS2";
    l locall2 = (l)((l)locall1).c(2131165331);
    locall2.v = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890  .,:/$&#'-";
    locall2.a = 32;
    return (l)locall2.c();
  }

  public static l f(String paramString)
  {
    l locall1 = new l(2131165330, paramString);
    locall1.b = "ADDRESS1";
    l locall2 = (l)((l)locall1).c(2131165334);
    locall2.o = true;
    l locall3 = (l)locall2;
    locall3.v = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890  .,:/$&#'-";
    locall3.a = 32;
    return (l)locall3.c();
  }

  public static l g(String paramString)
  {
    l locall1 = new l(2131165326, paramString);
    locall1.b = "ACCOUNT_NUMBER";
    l locall2 = (l)((l)locall1).c(2131165312);
    locall2.p = true;
    l locall3 = (l)locall2;
    locall3.u = 3;
    locall3.a = 32;
    return (l)locall3.c();
  }

  public static l h(String paramString)
  {
    l locall1 = new l(2131165591, paramString);
    locall1.b = "PAYEE_NICKNAME";
    l locall2 = (l)((l)locall1).c(2131165311);
    locall2.a = 32;
    locall2.v = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890 .,&()-/'";
    return (l)locall2.c();
  }

  public static l i(String paramString)
  {
    l locall1 = new l(2131165329, paramString);
    locall1.b = "PAYEE";
    l locall2 = (l)((l)locall1).c(2131165590);
    locall2.o = true;
    l locall3 = (l)locall2;
    locall3.a = 32;
    locall3.v = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890 .,&()-/'";
    return (l)locall3.c();
  }

  public final q a(State paramState)
  {
    if (paramState != null);
    for (int i = paramState.ordinal(); ; i = -1)
    {
      be localbe = new be(this, i);
      localbe.o = true;
      q localq1 = (q)localbe;
      localq1.b = "STATE";
      q localq2 = (q)localq1;
      localq2.r = "state";
      return (q)((q)((q)localq2).c(2131165333)).c();
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.bd
 * JD-Core Version:    0.6.2
 */