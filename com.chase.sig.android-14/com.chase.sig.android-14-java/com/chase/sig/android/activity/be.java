package com.chase.sig.android.activity;

import android.content.Context;
import android.widget.ArrayAdapter;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.domain.State;
import com.chase.sig.android.view.JPSpinner;
import com.chase.sig.android.view.detail.q;

final class be extends q
{
  be(bd parambd, int paramInt)
  {
    super(paramInt);
  }

  protected final void a(Context paramContext)
  {
    BehaviorAnalyticsAspect.a().a(this);
    super.a(paramContext);
    ArrayAdapter localArrayAdapter = new ArrayAdapter(paramContext, 2130903213, State.values());
    q().setAdapter(localArrayAdapter);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.be
 * JD-Core Version:    0.6.2
 */