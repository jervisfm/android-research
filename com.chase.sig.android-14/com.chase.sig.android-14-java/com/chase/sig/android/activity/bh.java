package com.chase.sig.android.activity;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;

final class bh
  implements View.OnClickListener
{
  bh(BillPayPayeeEditActivity paramBillPayPayeeEditActivity)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    if (this.a.a())
    {
      BillPayPayeeEditActivity.a(this.a);
      Intent localIntent = new Intent(this.a, BillPayPayeeEditVerifyActivity.class);
      localIntent.putExtra("payee", BillPayPayeeEditActivity.b(this.a));
      this.a.startActivityForResult(localIntent, 12);
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.bh
 * JD-Core Version:    0.6.2
 */