package com.chase.sig.android.activity;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;

final class bn
  implements View.OnClickListener
{
  bn(BillPayPayeeImageCaptureActivity paramBillPayPayeeImageCaptureActivity, Button paramButton)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    View localView = this.b.findViewById(2131296317);
    if (localView.getVisibility() == 0)
    {
      this.a.setText("Help");
      ((TextView)this.b.findViewById(2131296308)).setText(2131165602);
      localView.setVisibility(4);
      BillPayPayeeImageCaptureActivity.a(this.b).setEnabled(true);
      return;
    }
    this.a.setText("Done");
    ((TextView)this.b.findViewById(2131296308)).setText(2131165603);
    localView.setVisibility(0);
    BillPayPayeeImageCaptureActivity.a(this.b).setEnabled(false);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.bn
 * JD-Core Version:    0.6.2
 */