package com.chase.sig.android.activity;

import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;

final class bo
  implements View.OnClickListener
{
  bo(BillPayPayeeImageCaptureActivity paramBillPayPayeeImageCaptureActivity, CapturePreview paramCapturePreview)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    BillPayPayeeImageCaptureActivity.a(this.b).setEnabled(false);
    BillPayPayeeImageCaptureActivity.b(this.b).setVisibility(0);
    BillPayPayeeImageCaptureActivity.b(this.b).requestLayout();
    Camera.Parameters localParameters = this.a.b.getParameters();
    localParameters.setRotation(0);
    this.a.b.setParameters(localParameters);
    this.a.b.autoFocus(new bp(this));
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.bo
 * JD-Core Version:    0.6.2
 */