package com.chase.sig.android.activity;

import android.content.Intent;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;

final class bs
  implements Camera.PictureCallback
{
  bs(BillPayPayeeImageCaptureActivity paramBillPayPayeeImageCaptureActivity)
  {
  }

  public final void onPictureTaken(byte[] paramArrayOfByte, Camera paramCamera)
  {
    Intent localIntent = new Intent(this.a, BillPayPayeeImageReviewActivity.class);
    localIntent.putExtra("image_data", BillPayPayeeImageCaptureActivity.a(paramArrayOfByte));
    localIntent.putExtra("selectedAccountId", BillPayPayeeImageCaptureActivity.c(this.a));
    localIntent.setFlags(1073741824);
    this.a.startActivity(localIntent);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.bs
 * JD-Core Version:    0.6.2
 */