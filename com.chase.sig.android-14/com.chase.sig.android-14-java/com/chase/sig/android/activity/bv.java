package com.chase.sig.android.activity;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;

final class bv
  implements View.OnClickListener
{
  bv(BillPayPayeeImageReviewActivity paramBillPayPayeeImageReviewActivity)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    Intent localIntent = new Intent(this.a, BillPayPayeeImageCaptureActivity.class);
    localIntent.addFlags(1073741824);
    localIntent.putExtra("selectedAccountId", BillPayPayeeImageReviewActivity.c(this.a));
    this.a.startActivity(localIntent);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.bv
 * JD-Core Version:    0.6.2
 */