package com.chase.sig.android.activity;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;

final class bx
  implements DialogInterface.OnClickListener
{
  bx(BillPayPayeeImageReviewActivity paramBillPayPayeeImageReviewActivity)
  {
  }

  public final void onClick(DialogInterface paramDialogInterface, int paramInt)
  {
    Intent localIntent = new Intent(this.a, BillPayPayeeSearchManuallyActivity.class);
    localIntent.putExtra("selectedAccountId", BillPayPayeeImageReviewActivity.c(this.a));
    this.a.startActivity(localIntent);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.bx
 * JD-Core Version:    0.6.2
 */