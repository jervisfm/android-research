package com.chase.sig.android.activity;

import android.app.Dialog;
import android.content.DialogInterface.OnClickListener;
import android.widget.Button;
import com.chase.sig.android.activity.a.b;
import com.chase.sig.android.domain.Transaction;
import com.chase.sig.android.service.movemoney.ServiceResponse;
import com.chase.sig.android.view.detail.DetailView;
import com.chase.sig.android.view.k.a;

public abstract class c<T extends Transaction> extends n<T>
{
  private DialogInterface.OnClickListener b(boolean paramBoolean)
  {
    return new g(this, paramBoolean);
  }

  public abstract int a(DetailResource paramDetailResource);

  public void a()
  {
    Button localButton1 = (Button)findViewById(2131296894);
    int i;
    int j;
    label40: Button localButton2;
    int k;
    if (this.a.u())
    {
      i = 0;
      localButton1.setVisibility(i);
      if (!this.a.l())
        break label127;
      j = 10;
      localButton1.setOnClickListener(i(j));
      localButton1.setText(a(DetailResource.a));
      localButton2 = (Button)findViewById(2131296895);
      localButton2.setText(a(DetailResource.h));
      boolean bool = this.a.v();
      k = 0;
      if (!bool)
        break label133;
    }
    while (true)
    {
      localButton2.setVisibility(k);
      localButton2.setOnClickListener(new d(this));
      return;
      i = 8;
      break;
      label127: j = 11;
      break label40;
      label133: k = 8;
    }
  }

  protected abstract void a(DetailView paramDetailView, T paramT);

  protected final void a(boolean paramBoolean)
  {
    a((DetailView)findViewById(2131296595), this.a);
  }

  protected abstract com.chase.sig.android.service.movemoney.d<T> b();

  protected abstract Class<? extends ae<?, Boolean, ?, ?>> c();

  protected abstract Class<? extends eb> d();

  protected abstract Class<? extends eb> e();

  protected Dialog onCreateDialog(int paramInt)
  {
    k.a locala = new k.a(this);
    switch (paramInt)
    {
    case 12:
    case 13:
    default:
      return super.onCreateDialog(paramInt);
    case 10:
      locala.i = true;
      locala.a(a(DetailResource.a), b(true)).b(a(DetailResource.b), new b()).b(a(DetailResource.c));
      return locala.d(-1);
    case 11:
      locala.i = true;
      locala.a(a(DetailResource.e), b(true)).c(a(DetailResource.f), b(false)).b(a(DetailResource.g), new b()).b(a(DetailResource.d));
      return locala.d(-1);
    case 14:
    }
    locala.i = true;
    locala.a(a(DetailResource.i), new f(this)).c(a(DetailResource.j), new e(this)).b(a(DetailResource.g), new b()).b(a(DetailResource.k));
    return locala.d(-1);
  }

  public static abstract class a<T extends Transaction> extends com.chase.sig.android.d<c<T>, Boolean, Void, ServiceResponse>
  {
    private boolean a;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.c
 * JD-Core Version:    0.6.2
 */