package com.chase.sig.android.activity;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.SimpleAdapter;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.domain.BillPayVerifyPayeeInfo;
import com.chase.sig.android.domain.MerchantPayee;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

final class cb
  implements AdapterView.OnItemClickListener
{
  cb(BillPayPayeeSearchResultActivity paramBillPayPayeeSearchResultActivity)
  {
  }

  public final void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    try
    {
      BillPayVerifyPayeeInfo localBillPayVerifyPayeeInfo = new BillPayVerifyPayeeInfo();
      String str = (String)((HashMap)BillPayPayeeSearchResultActivity.a(this.a).getItem(paramInt)).get("optionId");
      Iterator localIterator = BillPayPayeeSearchResultActivity.b(this.a).iterator();
      while (localIterator.hasNext())
      {
        MerchantPayee localMerchantPayee = (MerchantPayee)localIterator.next();
        if (localMerchantPayee.a().equals(str))
          localBillPayVerifyPayeeInfo.a(localMerchantPayee);
      }
      localBillPayVerifyPayeeInfo.b(BillPayPayeeSearchResultActivity.c(this.a));
      localBillPayVerifyPayeeInfo.a(BillPayPayeeSearchResultActivity.d(this.a));
      localBillPayVerifyPayeeInfo.f(BillPayPayeeSearchResultActivity.e(this.a));
      localBillPayVerifyPayeeInfo.g(BillPayPayeeSearchResultActivity.f(this.a));
      localBillPayVerifyPayeeInfo.a(true);
      Intent localIntent = new Intent(this.a, BillPayPayeeAddVerifyActivity.class);
      localIntent.putExtra("payee_info", localBillPayVerifyPayeeInfo);
      this.a.startActivityForResult(localIntent, 25);
      BehaviorAnalyticsAspect.a();
      BehaviorAnalyticsAspect.a(paramAdapterView, paramView, paramInt);
      return;
    }
    catch (Throwable localThrowable)
    {
      BehaviorAnalyticsAspect.a();
      BehaviorAnalyticsAspect.a(paramAdapterView, paramView, paramInt);
      throw localThrowable;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.cb
 * JD-Core Version:    0.6.2
 */