package com.chase.sig.android.activity;

import android.content.Context;
import android.content.Intent;
import com.chase.sig.android.domain.Advisor;
import com.chase.sig.android.domain.PhoneBookContact;

public abstract class cc
{
  private static cc a;

  public static cc b()
  {
    if (a == null)
      a = new cd();
    return a;
  }

  public abstract Intent a();

  public abstract Intent a(Advisor paramAdvisor, String paramString);

  public abstract PhoneBookContact a(Context paramContext, long paramLong);

  public abstract String a(Context paramContext);

  public abstract boolean a(Context paramContext, String paramString);

  public abstract Intent b(Advisor paramAdvisor, String paramString);
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.cc
 * JD-Core Version:    0.6.2
 */