package com.chase.sig.android.activity;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract.CommonDataKinds.Email;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.Contacts;
import com.chase.sig.android.domain.Advisor;
import com.chase.sig.android.domain.PhoneBookContact;
import com.chase.sig.android.domain.RecipientContact;
import java.util.ArrayList;
import java.util.List;

public final class cd extends cc
{
  private static void a(Advisor paramAdvisor, String paramString, Intent paramIntent)
  {
    paramIntent.putExtra("phone", paramAdvisor.a());
    paramIntent.putExtra("phone_type", 12);
    paramIntent.putExtra("email", paramAdvisor.b());
    paramIntent.putExtra("email_type", 2);
    paramIntent.putExtra("notes", paramAdvisor.c());
    paramIntent.putExtra("company", paramString);
  }

  private static List<RecipientContact> b(Context paramContext, String paramString)
  {
    ArrayList localArrayList = new ArrayList();
    Cursor localCursor = paramContext.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, "contact_id = ?", new String[] { paramString }, null);
    while (localCursor.moveToNext())
    {
      String str = localCursor.getString(localCursor.getColumnIndex("data1"));
      int i = Integer.parseInt(localCursor.getString(localCursor.getColumnIndex("data2")));
      RecipientContact localRecipientContact = new RecipientContact();
      localRecipientContact.d(paramString);
      localRecipientContact.c(str);
      localRecipientContact.b(ContactsContract.CommonDataKinds.Phone.getTypeLabel(paramContext.getResources(), i, "").toString());
      localArrayList.add(localRecipientContact);
    }
    localCursor.close();
    return localArrayList;
  }

  public final Intent a()
  {
    return new Intent("android.intent.action.PICK", ContactsContract.Contacts.CONTENT_URI);
  }

  public final Intent a(Advisor paramAdvisor, String paramString)
  {
    Intent localIntent = new Intent("android.intent.action.INSERT");
    localIntent.setType("vnd.android.cursor.dir/raw_contact");
    a(paramAdvisor, paramString, localIntent);
    localIntent.putExtra("name", paramAdvisor.d());
    return localIntent;
  }

  public final PhoneBookContact a(Context paramContext, long paramLong)
  {
    PhoneBookContact localPhoneBookContact = new PhoneBookContact();
    String[] arrayOfString = { "_id", "display_name" };
    Uri localUri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, paramLong);
    Cursor localCursor1 = paramContext.getContentResolver().query(localUri, arrayOfString, null, null, null);
    if (localCursor1.moveToFirst())
    {
      localPhoneBookContact.a(localCursor1.getString(localCursor1.getColumnIndexOrThrow("display_name")));
      String str1 = Long.toString(paramLong);
      ArrayList localArrayList = new ArrayList();
      Cursor localCursor2 = paramContext.getContentResolver().query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null, "contact_id = ?", new String[] { str1 }, null);
      while (localCursor2.moveToNext())
      {
        String str2 = localCursor2.getString(localCursor2.getColumnIndex("_id"));
        String str3 = localCursor2.getString(localCursor2.getColumnIndex("data1"));
        String str4 = localCursor2.getString(localCursor2.getColumnIndex("data2"));
        RecipientContact localRecipientContact = new RecipientContact();
        localRecipientContact.d(str2);
        localRecipientContact.c(str3);
        localRecipientContact.b(str4);
        localArrayList.add(localRecipientContact);
      }
      localCursor2.close();
      localPhoneBookContact.a(localArrayList);
      localPhoneBookContact.b(b(paramContext, Long.toString(paramLong)));
    }
    return localPhoneBookContact;
  }

  public final String a(Context paramContext)
  {
    return ContactsContract.CommonDataKinds.Phone.getTypeLabel(paramContext.getResources(), 2, "").toString();
  }

  public final boolean a(Context paramContext, String paramString)
  {
    ContentResolver localContentResolver = paramContext.getContentResolver();
    Uri localUri = ContactsContract.Contacts.CONTENT_URI;
    String[] arrayOfString = new String[1];
    arrayOfString[0] = ("%" + paramString + "%");
    return localContentResolver.query(localUri, null, "display_name like ?", arrayOfString, null).moveToFirst();
  }

  public final Intent b(Advisor paramAdvisor, String paramString)
  {
    Intent localIntent = new Intent("android.intent.action.EDIT");
    localIntent.setType("vnd.android.cursor.item/raw_contact");
    a(paramAdvisor, paramString, localIntent);
    return localIntent;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.cd
 * JD-Core Version:    0.6.2
 */