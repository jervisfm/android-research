package com.chase.sig.android.activity;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ListView;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.domain.PhoneBookContact;

final class ce
  implements View.OnClickListener
{
  ce(ContactEmailAddressesActivity paramContactEmailAddressesActivity)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    this.a.b();
    if ((ContactEmailAddressesActivity.a(ContactEmailAddressesActivity.a(this.a).getAdapter(), ContactEmailAddressesActivity.a(this.a).getCheckedItemPositions()).length <= 0) && (ContactEmailAddressesActivity.a(ContactEmailAddressesActivity.a(this.a).getAdapter(), ContactEmailAddressesActivity.b(this.a).getCheckedItemPositions()).length <= 0))
    {
      this.a.g(2131165767);
      return;
    }
    if (ContactEmailAddressesActivity.a(ContactEmailAddressesActivity.a(this.a).getAdapter(), ContactEmailAddressesActivity.a(this.a).getCheckedItemPositions()).length > 5)
    {
      this.a.g(2131165766);
      return;
    }
    if (ContactEmailAddressesActivity.c(this.a).e())
    {
      Intent localIntent1 = new Intent(this.a, ContactPhoneNumbersActivity.class);
      localIntent1.putExtras(this.a.getIntent());
      localIntent1.putExtra("recipient", ContactEmailAddressesActivity.d(this.a));
      localIntent1.putExtra("contact", ContactEmailAddressesActivity.c(this.a));
      this.a.startActivity(localIntent1);
      return;
    }
    Intent localIntent2 = new Intent(this.a, QuickPayAddRecipientActivity.class);
    localIntent2.putExtras(this.a.getIntent());
    localIntent2.putExtra("recipient", ContactEmailAddressesActivity.d(this.a));
    this.a.startActivity(localIntent2);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.ce
 * JD-Core Version:    0.6.2
 */