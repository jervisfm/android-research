package com.chase.sig.android.activity;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.domain.MobilePhoneNumber;
import com.chase.sig.android.domain.QuickPayRecipient;
import com.chase.sig.android.domain.RecipientContact;

final class ci
  implements View.OnClickListener
{
  ci(ContactPhoneNumbersActivity paramContactPhoneNumbersActivity)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    ContactPhoneNumbersActivity.b(this.a).a(null);
    if (ContactPhoneNumbersActivity.a(this.a).b() != -1)
    {
      String str = ContactPhoneNumbersActivity.a(this.a).a().b();
      if (!"".equalsIgnoreCase(str))
      {
        ContactPhoneNumbersActivity.b(this.a).e(str);
        ContactPhoneNumbersActivity.b(this.a).i().c("Mobile");
        ContactPhoneNumbersActivity.b(this.a).i().b("0");
      }
    }
    Intent localIntent = new Intent(this.a, QuickPayAddRecipientActivity.class);
    localIntent.putExtras(this.a.getIntent());
    localIntent.putExtra("recipient", ContactPhoneNumbersActivity.b(this.a));
    this.a.startActivity(localIntent);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.ci
 * JD-Core Version:    0.6.2
 */