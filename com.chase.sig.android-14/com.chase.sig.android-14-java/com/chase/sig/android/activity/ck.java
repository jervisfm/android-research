package com.chase.sig.android.activity;

import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.domain.PhoneBookContact;
import java.util.List;

final class ck
  implements AdapterView.OnItemClickListener
{
  ck(ContactsActivity paramContactsActivity)
  {
  }

  public final void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    int i = 1;
    while (true)
    {
      PhoneBookContact localPhoneBookContact;
      int j;
      try
      {
        localPhoneBookContact = ContactsActivity.a(this.a).a(this.a, paramLong);
        if ((localPhoneBookContact.d()) && (localPhoneBookContact.a().size() > i))
        {
          j = i;
          break label201;
          if ((i != 0) && (!localPhoneBookContact.e()))
          {
            ContactsActivity localContactsActivity3 = this.a;
            ContactsActivity.a(localContactsActivity3, ContactsActivity.a(localPhoneBookContact), localPhoneBookContact, QuickPayAddRecipientActivity.class);
            BehaviorAnalyticsAspect.a();
            BehaviorAnalyticsAspect.a(paramAdapterView, paramView, paramInt);
          }
        }
        else
        {
          j = 0;
          break label201;
          i = 0;
          continue;
        }
        if ((i != 0) && (localPhoneBookContact.e()))
        {
          ContactsActivity localContactsActivity2 = this.a;
          ContactsActivity.a(localContactsActivity2, ContactsActivity.a(localPhoneBookContact), localPhoneBookContact, ContactPhoneNumbersActivity.class);
          continue;
        }
      }
      catch (Throwable localThrowable)
      {
        BehaviorAnalyticsAspect.a();
        BehaviorAnalyticsAspect.a(paramAdapterView, paramView, paramInt);
        throw localThrowable;
      }
      if (j != 0)
      {
        ContactsActivity localContactsActivity1 = this.a;
        ContactsActivity.a(localContactsActivity1, ContactsActivity.b(localPhoneBookContact), localPhoneBookContact, ContactEmailAddressesActivity.class);
        continue;
        label201: if (j != 0);
      }
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.ck
 * JD-Core Version:    0.6.2
 */