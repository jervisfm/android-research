package com.chase.sig.android.activity;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RelativeLayout;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;

final class cl
  implements View.OnClickListener
{
  cl(CreditFacilityAccountDetailActivity paramCreditFacilityAccountDetailActivity)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    if (CreditFacilityAccountDetailActivity.a(this.a))
    {
      CreditFacilityAccountDetailActivity.b(this.a).setVisibility(0);
      CreditFacilityAccountDetailActivity.c(this.a).setVisibility(0);
      CreditFacilityAccountDetailActivity.d(this.a).setBackgroundResource(2130837742);
      CreditFacilityAccountDetailActivity.a(this.a, false);
      return;
    }
    CreditFacilityAccountDetailActivity.b(this.a).setVisibility(8);
    CreditFacilityAccountDetailActivity.c(this.a).setVisibility(8);
    CreditFacilityAccountDetailActivity.d(this.a).setBackgroundResource(2130837741);
    CreditFacilityAccountDetailActivity.a(this.a, true);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.cl
 * JD-Core Version:    0.6.2
 */