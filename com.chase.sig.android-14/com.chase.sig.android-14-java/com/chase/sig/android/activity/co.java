package com.chase.sig.android.activity;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import java.io.Serializable;

final class co
  implements View.OnClickListener
{
  co(DeviceCodeNotFoundActivity paramDeviceCodeNotFoundActivity)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    Intent localIntent = new Intent(this.a.getApplicationContext(), DeviceCodeLoginActivity.class);
    localIntent.putExtra("otp_contacts", (Serializable)this.a.a);
    this.a.startActivity(localIntent);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.co
 * JD-Core Version:    0.6.2
 */