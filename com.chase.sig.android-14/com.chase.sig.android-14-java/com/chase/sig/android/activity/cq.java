package com.chase.sig.android.activity;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.domain.Disclosure;
import com.chase.sig.android.util.e;
import java.util.List;

final class cq
  implements AdapterView.OnItemClickListener
{
  cq(DisclosuresActivity paramDisclosuresActivity)
  {
  }

  public final void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    try
    {
      if (paramInt < DisclosuresActivity.a(this.a).size())
      {
        Disclosure localDisclosure = (Disclosure)DisclosuresActivity.a(this.a).get(paramInt);
        Intent localIntent2 = new Intent(this.a, DisclosuresDetailActivity.class);
        e.a(localIntent2, localDisclosure);
        this.a.startActivity(localIntent2);
      }
      while (true)
      {
        BehaviorAnalyticsAspect.a();
        BehaviorAnalyticsAspect.a(paramAdapterView, paramView, paramInt);
        return;
        TextView localTextView = (TextView)paramView.findViewById(2131296444);
        if ((localTextView != null) && (localTextView.getText().equals(this.a.getString(2131165836))))
        {
          Intent localIntent1 = new Intent(this.a, PrivacyNoticesActivity.class);
          localIntent1.putExtra("title", localTextView.getText().toString());
          this.a.startActivity(localIntent1);
        }
      }
    }
    catch (Throwable localThrowable)
    {
      BehaviorAnalyticsAspect.a();
      BehaviorAnalyticsAspect.a(paramAdapterView, paramView, paramInt);
      throw localThrowable;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.cq
 * JD-Core Version:    0.6.2
 */