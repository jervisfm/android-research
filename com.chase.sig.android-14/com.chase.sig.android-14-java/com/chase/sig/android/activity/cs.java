package com.chase.sig.android.activity;

import android.os.AsyncTask.Status;
import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.domain.EPayment;

final class cs
  implements View.OnClickListener
{
  cs(EPayAuthorizeActivity paramEPayAuthorizeActivity)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    EPayAuthorizeActivity.a locala = (EPayAuthorizeActivity.a)this.a.e.a(EPayAuthorizeActivity.a.class);
    if (locala.getStatus() != AsyncTask.Status.RUNNING)
    {
      EPayment[] arrayOfEPayment = new EPayment[1];
      arrayOfEPayment[0] = EPayAuthorizeActivity.a(this.a);
      locala.execute(arrayOfEPayment);
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.cs
 * JD-Core Version:    0.6.2
 */