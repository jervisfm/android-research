package com.chase.sig.android.activity;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;

final class cy
  implements View.OnClickListener
{
  cy(EPayHomeActivity paramEPayHomeActivity)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    Intent localIntent;
    if (EPayHomeActivity.a(this.a))
    {
      localIntent = new Intent(this.a, EPayStartActivity.class);
      localIntent.putExtra("selectedAccountId", EPayHomeActivity.b(this.a));
    }
    while (true)
    {
      this.a.startActivity(localIntent);
      return;
      localIntent = new Intent(this.a, EPaySelectToAccount.class).setFlags(1073741824);
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.cy
 * JD-Core Version:    0.6.2
 */