package com.chase.sig.android.activity;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.domain.IAccount;
import java.util.List;

final class cz
  implements AdapterView.OnItemClickListener
{
  cz(EPaySelectToAccount paramEPaySelectToAccount, List paramList, boolean paramBoolean)
  {
  }

  public final void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    try
    {
      IAccount localIAccount = (IAccount)this.a.get(paramInt);
      if (this.b)
      {
        Intent localIntent = new Intent();
        localIntent.putExtra("selectedAccountId", localIAccount.b());
        this.c.setResult(0, localIntent);
        this.c.finish();
      }
      while (true)
      {
        BehaviorAnalyticsAspect.a();
        BehaviorAnalyticsAspect.a(paramAdapterView, paramView, paramInt);
        return;
        EPaySelectToAccount.a(this.c, localIAccount.b());
      }
    }
    catch (Throwable localThrowable)
    {
      BehaviorAnalyticsAspect.a();
      BehaviorAnalyticsAspect.a(paramAdapterView, paramView, paramInt);
      throw localThrowable;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.cz
 * JD-Core Version:    0.6.2
 */