package com.chase.sig.android.activity;

import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.domain.Transaction;

final class d
  implements View.OnClickListener
{
  d(c paramc)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    if (this.a.a.l())
    {
      c.a(this.a, true);
      return;
    }
    this.a.showDialog(14);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.d
 * JD-Core Version:    0.6.2
 */