package com.chase.sig.android.activity;

import android.view.View;
import android.widget.SimpleAdapter;
import com.chase.sig.android.util.s;
import com.chase.sig.android.view.AmountView;
import com.chase.sig.android.view.detail.DetailView;
import com.chase.sig.android.view.detail.c;
import com.chase.sig.android.view.detail.q;
import com.chase.sig.android.view.x;
import java.util.Map;

final class db
  implements x
{
  db(EPayStartActivity paramEPayStartActivity)
  {
  }

  public final void a(int paramInt)
  {
    Map localMap = (Map)EPayStartActivity.b(this.a).getItem(paramInt);
    if (((String)localMap.get("id")).equals("-1"))
    {
      ((q)this.a.a("PAYMENT_OPTIONS")).n = true;
      c localc2 = (c)this.a.a("AMOUNT");
      localc2.j = false;
      localc2.p().requestFocus();
    }
    while (true)
    {
      if (s.l((String)localMap.get("note")))
        ((q)this.a.a("PAYMENT_OPTIONS")).k().findViewById(2131296455).setVisibility(8);
      this.a.a().c();
      return;
      ((q)this.a.a("PAYMENT_OPTIONS")).n = false;
      c localc1 = (c)this.a.a("AMOUNT");
      localc1.j = true;
      localc1.b("");
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.db
 * JD-Core Version:    0.6.2
 */