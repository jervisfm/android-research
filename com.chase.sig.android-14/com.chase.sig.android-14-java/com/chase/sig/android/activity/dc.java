package com.chase.sig.android.activity;

import android.os.AsyncTask.Status;
import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.domain.EPayAccount;
import com.chase.sig.android.domain.EPayment;
import com.chase.sig.android.domain.EPaymentOption;
import com.chase.sig.android.domain.IAccount;
import com.chase.sig.android.util.Dollar;
import com.chase.sig.android.view.JPSpinner;
import com.chase.sig.android.view.detail.c;
import java.util.Iterator;
import java.util.List;

final class dc
  implements View.OnClickListener
{
  dc(EPayStartActivity paramEPayStartActivity)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    if (!this.a.b())
      return;
    EPayment localEPayment = new EPayment();
    localEPayment.d(EPayStartActivity.c(this.a));
    localEPayment.e(EPayStartActivity.d(this.a).b());
    localEPayment.c(((EPayAccount)EPayStartActivity.f(this.a).get(EPayStartActivity.e(this.a).getSelectedItemPosition())).d());
    Iterator localIterator = EPayStartActivity.f(this.a).iterator();
    while (localIterator.hasNext())
    {
      EPayAccount localEPayAccount = (EPayAccount)localIterator.next();
      if (localEPayAccount.d().equalsIgnoreCase(((EPayAccount)EPayStartActivity.f(this.a).get(EPayStartActivity.e(this.a).getSelectedItemPosition())).d()))
      {
        localEPayment.h(localEPayAccount.b());
        localEPayment.i(localEPayAccount.c());
      }
    }
    localEPayment.d(EPayStartActivity.c(this.a));
    EPaymentOption localEPaymentOption = (EPaymentOption)EPayStartActivity.h(this.a).get(EPayStartActivity.g(this.a).getSelectedItemPosition());
    localEPayment.f(localEPaymentOption.d());
    if ("-1".equals(localEPayment.g()))
      localEPayment.a(((c)this.a.a("AMOUNT")).q());
    while (true)
    {
      EPayStartActivity.b localb = (EPayStartActivity.b)this.a.e.a(EPayStartActivity.b.class);
      if (localb.getStatus() == AsyncTask.Status.RUNNING)
        break;
      localb.execute(new EPayment[] { localEPayment });
      return;
      if ((localEPaymentOption.a() != null) && (localEPaymentOption.a().c()))
        localEPayment.a(localEPaymentOption.a());
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.dc
 * JD-Core Version:    0.6.2
 */