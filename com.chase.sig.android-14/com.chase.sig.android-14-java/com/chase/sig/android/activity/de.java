package com.chase.sig.android.activity;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;

final class de
  implements View.OnClickListener
{
  de(EPayVerifyActivity paramEPayVerifyActivity)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    Intent localIntent = new Intent(this.a.getBaseContext(), EPayAuthorizeActivity.class);
    localIntent.putExtra(EPayVerifyActivity.a, EPayVerifyActivity.a(this.a));
    this.a.startActivity(localIntent);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.de
 * JD-Core Version:    0.6.2
 */