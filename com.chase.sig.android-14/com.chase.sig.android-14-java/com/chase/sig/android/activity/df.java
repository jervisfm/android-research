package com.chase.sig.android.activity;

import android.graphics.Bitmap;
import android.webkit.WebView;
import android.webkit.WebViewClient;

final class df extends WebViewClient
{
  df(EyeOnTheMarketReportArticleActivity paramEyeOnTheMarketReportArticleActivity)
  {
  }

  public final void onPageFinished(WebView paramWebView, String paramString)
  {
    super.onPageFinished(paramWebView, paramString);
    EyeOnTheMarketReportArticleActivity.a(this.a).loadUrl("javascript:(function() { document.getElementById('view-in-desktop').setAttribute('style', 'display:none !important'); document.getElementById('dd-url').setAttribute('style', 'display:none !important'); var breaks = document.getElementById('page-footer').getElementsByTagName('br'); for (var i=0; i<breaks.length; i++) { breaks[i].setAttribute('style', 'display:none !important'); }; })()");
    if (!this.a.isFinishing())
      this.a.u();
  }

  public final void onPageStarted(WebView paramWebView, String paramString, Bitmap paramBitmap)
  {
    super.onPageStarted(paramWebView, paramString, paramBitmap);
    if (!this.a.isFinishing())
      this.a.c(true);
  }

  public final void onReceivedError(WebView paramWebView, int paramInt, String paramString1, String paramString2)
  {
    Object[] arrayOfObject = new Object[3];
    arrayOfObject[0] = paramString2;
    arrayOfObject[1] = Integer.valueOf(paramInt);
    arrayOfObject[2] = paramString1;
    if (!this.a.isFinishing())
    {
      EyeOnTheMarketReportArticleActivity.a(this.a).setVisibility(8);
      this.a.f(2131165829);
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.df
 * JD-Core Version:    0.6.2
 */