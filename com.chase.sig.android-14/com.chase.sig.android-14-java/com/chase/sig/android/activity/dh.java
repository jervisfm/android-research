package com.chase.sig.android.activity;

import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.widget.ArrayAdapter;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import java.util.List;

final class dh
  implements View.OnKeyListener
{
  dh(FindBranchActivity paramFindBranchActivity)
  {
  }

  public final boolean onKey(View paramView, int paramInt, KeyEvent paramKeyEvent)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView, paramInt, paramKeyEvent);
    if ((paramKeyEvent.getAction() == 0) && (paramInt == 66))
    {
      FindBranchActivity.a locala = (FindBranchActivity.a)this.a.e.a(FindBranchActivity.a.class);
      if (locala.d())
        locala.cancel(false);
      FindBranchActivity.c(this.a).add(FindBranchActivity.b(this.a));
      FindBranchActivity.d(this.a).notifyDataSetChanged();
      FindBranchActivity.d(this.a).add(FindBranchActivity.b(this.a));
      this.a.a(FindBranchActivity.a.class, new Void[0]);
      return true;
    }
    return false;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.dh
 * JD-Core Version:    0.6.2
 */