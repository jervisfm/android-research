package com.chase.sig.android.activity;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import java.io.Serializable;
import java.util.List;

final class di
  implements AdapterView.OnItemClickListener
{
  di(FindBranchActivity paramFindBranchActivity)
  {
  }

  public final void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    try
    {
      Intent localIntent = new Intent(paramAdapterView.getContext(), LocationInfoActivity.class);
      localIntent.putExtra("location", (Serializable)FindBranchActivity.e(this.a).get(paramInt));
      this.a.startActivity(localIntent);
      BehaviorAnalyticsAspect.a();
      BehaviorAnalyticsAspect.a(paramAdapterView, paramView, paramInt);
      return;
    }
    catch (Throwable localThrowable)
    {
      BehaviorAnalyticsAspect.a();
      BehaviorAnalyticsAspect.a(paramAdapterView, paramView, paramInt);
      throw localThrowable;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.di
 * JD-Core Version:    0.6.2
 */