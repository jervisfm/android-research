package com.chase.sig.android.activity;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.domain.QuoteNewsArticle;

final class dj
  implements View.OnClickListener
{
  dj(HeadlinesActivity paramHeadlinesActivity, QuoteNewsArticle paramQuoteNewsArticle)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    Intent localIntent = new Intent(this.b, ArticleActivity.class);
    localIntent.putExtra("article_content", this.a);
    this.b.startActivity(localIntent);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.dj
 * JD-Core Version:    0.6.2
 */