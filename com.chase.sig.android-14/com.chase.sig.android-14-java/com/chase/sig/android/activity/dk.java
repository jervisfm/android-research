package com.chase.sig.android.activity;

import android.app.Dialog;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.TextView;
import com.chase.sig.android.ChaseApplication;
import java.util.ArrayList;
import java.util.StringTokenizer;

final class dk
  implements View.OnLongClickListener
{
  dk(HomeActivity paramHomeActivity, ChaseApplication paramChaseApplication)
  {
  }

  public final boolean onLongClick(View paramView)
  {
    Dialog localDialog = new Dialog(this.b);
    localDialog.setContentView(2130903074);
    localDialog.setTitle("Debug Settings");
    ArrayList localArrayList = new ArrayList();
    StringTokenizer localStringTokenizer = new StringTokenizer(this.b.z().a("environments"), ",");
    while (localStringTokenizer.hasMoreElements())
      localArrayList.add(localStringTokenizer.nextToken());
    Spinner localSpinner = (Spinner)localDialog.findViewById(2131296407);
    ArrayAdapter localArrayAdapter = new ArrayAdapter(this.b, 17367048, localArrayList);
    localArrayAdapter.setDropDownViewResource(17367049);
    localSpinner.setAdapter(localArrayAdapter);
    localSpinner.setSelection(localArrayAdapter.getPosition(this.a.f()));
    TextView localTextView = (TextView)localDialog.findViewById(2131296409);
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = this.a.d();
    localTextView.setText(String.format("Current ID is '%s'", arrayOfObject));
    localDialog.findViewById(2131296408).setOnClickListener(new dl(this, localDialog, localSpinner));
    localDialog.findViewById(2131296411).setOnClickListener(new dn(this, localDialog));
    CheckBox localCheckBox = (CheckBox)localDialog.findViewById(2131296412);
    localCheckBox.setChecked(this.b.z().t());
    localCheckBox.setOnCheckedChangeListener(new do(this));
    localDialog.findViewById(2131296410).setOnClickListener(new dp(this, localDialog));
    localDialog.show();
    return true;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.dk
 * JD-Core Version:    0.6.2
 */