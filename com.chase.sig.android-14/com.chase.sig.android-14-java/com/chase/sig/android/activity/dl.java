package com.chase.sig.android.activity;

import android.app.Dialog;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.util.s;

final class dl
  implements View.OnClickListener
{
  dl(dk paramdk, Dialog paramDialog, Spinner paramSpinner)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    this.a.dismiss();
    String str1 = (String)this.b.getSelectedItem();
    Dialog localDialog;
    LinearLayout localLinearLayout;
    EditText localEditText;
    if (str1.equals("closed_net_mockey"))
    {
      localDialog = new Dialog(this.c.b);
      localDialog.setTitle("Enter Hostname[:Port]");
      localLinearLayout = new LinearLayout(this.c.b);
      localLinearLayout.setOrientation(1);
      localDialog.addContentView(localLinearLayout, new ViewGroup.LayoutParams(-1, -2));
      localEditText = new EditText(this.c.b);
      localEditText.setSingleLine();
      localLinearLayout.addView(localEditText);
      String str2 = this.c.b.z().g();
      if (!s.m(str2))
        break label248;
      localEditText.setText(str2);
    }
    while (true)
    {
      Button localButton = new Button(this.c.b);
      localButton.setText("OK");
      localLinearLayout.addView(localButton);
      localButton.setOnClickListener(new dm(this, localEditText, localDialog));
      localDialog.show();
      this.c.b.z().b(str1);
      new StringBuilder("Environment selected: ").append(this.c.a.f()).toString();
      return;
      label248: localEditText.setText("192.168.0.100:8080");
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.dl
 * JD-Core Version:    0.6.2
 */