package com.chase.sig.android.activity;

import android.app.Dialog;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.ChaseApplication;

final class dn
  implements View.OnClickListener
{
  dn(dk paramdk, Dialog paramDialog)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    SharedPreferences.Editor localEditor = this.b.a.e().edit();
    localEditor.remove("deviceId");
    localEditor.commit();
    this.a.dismiss();
    Toast.makeText(this.b.b, "App ID removed.", 1).show();
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.dn
 * JD-Core Version:    0.6.2
 */