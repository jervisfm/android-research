package com.chase.sig.android.activity;

import android.app.Dialog;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.ChaseApplication;

final class dp
  implements View.OnClickListener
{
  dp(dk paramdk, Dialog paramDialog)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    this.a.dismiss();
    SharedPreferences.Editor localEditor = this.b.a.e().edit();
    localEditor.putString("deviceId", "1234567890");
    localEditor.commit();
    HomeActivity localHomeActivity = this.b.b;
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = this.b.a.d();
    Toast.makeText(localHomeActivity, String.format("ID saved as '%s'", arrayOfObject), 1).show();
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.dp
 * JD-Core Version:    0.6.2
 */