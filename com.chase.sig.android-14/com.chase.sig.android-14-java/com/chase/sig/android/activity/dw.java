package com.chase.sig.android.activity;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.service.InvestmentTransactionsResponse;
import java.io.Serializable;

final class dw
  implements View.OnClickListener
{
  dw(InvestmentTransactionsActivity paramInvestmentTransactionsActivity)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    Intent localIntent = new Intent(this.a, InvestmentTransactionsFilterActivity.class);
    localIntent.putExtra("filter_types", (Serializable)InvestmentTransactionsActivity.a(this.a).b());
    localIntent.putExtra("range", InvestmentTransactionsActivity.b(this.a));
    localIntent.putExtra("filter_type", InvestmentTransactionsActivity.c(this.a));
    this.a.startActivityForResult(localIntent, 0);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.dw
 * JD-Core Version:    0.6.2
 */