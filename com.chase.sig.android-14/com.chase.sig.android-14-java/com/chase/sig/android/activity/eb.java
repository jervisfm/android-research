package com.chase.sig.android.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.drawable.StateListDrawable;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.text.Html;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.ScrollView;
import android.widget.SlidingDrawer;
import android.widget.TextView;
import android.widget.Toast;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.a;
import com.chase.sig.android.c.a;
import com.chase.sig.android.domain.Announcement;
import com.chase.sig.android.domain.CacheActivityData;
import com.chase.sig.android.domain.IAccount;
import com.chase.sig.android.domain.QuickPayRecipient;
import com.chase.sig.android.domain.SimpleDialogInfo;
import com.chase.sig.android.domain.o;
import com.chase.sig.android.service.IServiceError;
import com.chase.sig.android.service.SplashResponse;
import com.chase.sig.android.service.af;
import com.chase.sig.android.service.n;
import com.chase.sig.android.service.quickpay.TodoListResponse;
import com.chase.sig.android.util.Dollar;
import com.chase.sig.android.util.s;
import com.chase.sig.android.view.JPSpinner;
import com.chase.sig.android.view.MenuView;
import com.chase.sig.android.view.an;
import com.chase.sig.android.view.b.a;
import com.chase.sig.android.view.detail.DetailView;
import com.chase.sig.android.view.k;
import com.chase.sig.android.view.k.a;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

public abstract class eb extends Activity
{
  private static final Object a = "cpcLogo";
  private List<SimpleDialogInfo> b = new ArrayList();
  private MenuView c;
  protected mc e = null;
  String f = null;
  public boolean g = false;
  public boolean h = false;
  public String i = null;
  public String j = null;

  private void a(int paramInt, String paramString1, String paramString2)
  {
    a(new SimpleDialogInfo(paramInt, paramString1, paramString2));
  }

  private void a(Bundle paramBundle, ViewGroup paramViewGroup)
  {
    if (paramViewGroup == null);
    while (true)
    {
      return;
      for (int k = 0; k < paramViewGroup.getChildCount(); k++)
      {
        View localView = paramViewGroup.getChildAt(k);
        if (((localView instanceof TextView)) && (paramBundle.containsKey("ERROR_" + localView.getId())))
          a((TextView)localView);
        if (((localView instanceof ViewGroup)) && (!(localView instanceof DetailView)))
          a(paramBundle, (ViewGroup)localView);
      }
    }
  }

  private void a(View paramView)
  {
    if ((paramView instanceof JPSpinner))
      ((JPSpinner)paramView).c();
    if ((paramView instanceof ViewGroup))
    {
      ViewGroup localViewGroup = (ViewGroup)paramView;
      for (int k = 0; k < localViewGroup.getChildCount(); k++)
        a(localViewGroup.getChildAt(k));
    }
  }

  private void a(ViewGroup paramViewGroup)
  {
    for (int k = 0; k < paramViewGroup.getChildCount(); k++)
    {
      View localView = paramViewGroup.getChildAt(k);
      if ((localView instanceof TextView))
        ((TextView)localView).setError(null);
      if ((localView instanceof ViewGroup))
        a((ViewGroup)localView);
    }
  }

  private void a(MenuView paramMenuView, int[] paramArrayOfInt)
  {
    int k = paramArrayOfInt.length;
    for (int m = 0; m < k; m++)
    {
      int n = paramArrayOfInt[m];
      findViewById(n).setOnClickListener(new eq(this, paramMenuView, n));
    }
  }

  private void a(boolean paramBoolean, Button paramButton)
  {
    if (paramBoolean)
    {
      paramButton.setBackgroundDrawable(getResources().getDrawable(2130837661));
      paramButton.setTextColor(getResources().getColorStateList(2130837592));
      return;
    }
    paramButton.setBackgroundDrawable(getResources().getDrawable(2130837759));
    paramButton.setTextColor(getResources().getColorStateList(2130837593));
  }

  public static boolean a(TextView paramTextView)
  {
    paramTextView.setError("");
    return false;
  }

  private k b(SimpleDialogInfo paramSimpleDialogInfo)
  {
    k.a locala = new k.a(this);
    String str = paramSimpleDialogInfo.b();
    if (str.matches(".*\\<[^>]+>.*"))
    {
      locala.b(str).i = paramSimpleDialogInfo.f();
      if (s.m(paramSimpleDialogInfo.e()))
        locala.b = paramSimpleDialogInfo.e();
      if (!paramSimpleDialogInfo.d())
        break label110;
      locala.a(2131165288, new ec(this, paramSimpleDialogInfo));
    }
    for (locala.g = new en(this, paramSimpleDialogInfo); ; locala.g = new ep(this, paramSimpleDialogInfo))
    {
      return locala.d(-1);
      locala.a(str).i = paramSimpleDialogInfo.f();
      break;
      label110: locala.a(2131165288, new eo(this, paramSimpleDialogInfo));
    }
  }

  private void b(Bundle paramBundle, ViewGroup paramViewGroup)
  {
    for (int k = 0; k < paramViewGroup.getChildCount(); k++)
    {
      View localView = paramViewGroup.getChildAt(k);
      if ((localView instanceof TextView))
      {
        TextView localTextView = (TextView)localView;
        if (localTextView.getError() != null)
          paramBundle.putBoolean("ERROR_" + localTextView.getId(), true);
      }
      if ((localView instanceof ViewGroup))
        b(paramBundle, (ViewGroup)localView);
    }
  }

  private static void b(View paramView)
  {
    if (paramView != null)
    {
      RelativeLayout.LayoutParams localLayoutParams = new RelativeLayout.LayoutParams(paramView.getLayoutParams());
      localLayoutParams.addRule(1, 2131296301);
      localLayoutParams.addRule(15, -1);
      paramView.setLayoutParams(localLayoutParams);
    }
  }

  private boolean d()
  {
    return (this.b != null) && (!this.b.isEmpty());
  }

  private int e()
  {
    if ((this instanceof fk.d))
      return 2131297020;
    if ((this instanceof fk.e))
      return 2131297021;
    if ((this instanceof fk.c))
      return 2131296530;
    if ((this instanceof fk.b))
      return 2131296531;
    if ((this instanceof fk.a))
      return 2131297022;
    return 0;
  }

  private ViewGroup f()
  {
    return (ViewGroup)findViewById(2131296300);
  }

  protected static int g(String paramString)
  {
    if (s.m(paramString))
    {
      Dollar localDollar = new Dollar(paramString);
      if ((localDollar.a()) && (localDollar.d()))
        return 2131099677;
    }
    return 2131099676;
  }

  private boolean g()
  {
    return ((ChaseApplication)getApplication()).b().b.C();
  }

  private boolean h()
  {
    return r().b.a();
  }

  public final n A()
  {
    getApplication();
    return af.a();
  }

  protected final View.OnClickListener B()
  {
    return new com.chase.sig.android.activity.a.d(this);
  }

  protected final View.OnClickListener C()
  {
    return new com.chase.sig.android.activity.a.c(this);
  }

  public final Dialog a(Context paramContext, int paramInt)
  {
    ej localej = new ej(this, paramContext);
    localej.setContentView(2130903147);
    ((TextView)localej.findViewById(2131296515)).setText(paramInt);
    localej.setCancelable(true);
    localej.setOnCancelListener(new ek(this));
    return localej;
  }

  protected final View.OnClickListener a(c.a parama)
  {
    return new com.chase.sig.android.activity.a.d(this, parama);
  }

  protected final com.chase.sig.android.activity.a.f a(Class<? extends eb> paramClass)
  {
    return new com.chase.sig.android.activity.a.f(this, paramClass);
  }

  public final com.chase.sig.android.view.b a(Calendar paramCalendar, b.a parama)
  {
    Calendar localCalendar = Calendar.getInstance();
    localCalendar.add(5, 366);
    return new com.chase.sig.android.view.b(this, parama, true, true, paramCalendar, localCalendar);
  }

  protected void a(int paramInt)
  {
  }

  public final void a(int paramInt1, int paramInt2)
  {
    Resources localResources = getResources();
    a(paramInt1, localResources.getString(2131165375), localResources.getString(paramInt2));
  }

  public final void a(int paramInt1, int paramInt2, int paramInt3, View.OnClickListener paramOnClickListener)
  {
    ImageButton localImageButton = new ImageButton(this);
    localImageButton.setLayoutParams(new LinearLayout.LayoutParams(-2, -1));
    localImageButton.setId(paramInt1);
    StateListDrawable localStateListDrawable = new StateListDrawable();
    localStateListDrawable.addState(new int[] { 16842919 }, getResources().getDrawable(paramInt3));
    localStateListDrawable.addState(new int[] { 16842908 }, getResources().getDrawable(paramInt3));
    localStateListDrawable.addState(new int[0], getResources().getDrawable(paramInt2));
    localImageButton.setImageDrawable(localStateListDrawable);
    localImageButton.setBackgroundDrawable(getResources().getDrawable(2130837555));
    localImageButton.setOnClickListener(paramOnClickListener);
    ((ViewGroup)findViewById(2131296304)).addView(localImageButton);
  }

  protected final void a(int paramInt, View.OnClickListener paramOnClickListener)
  {
    findViewById(paramInt).setOnClickListener(paramOnClickListener);
  }

  public final void a(int paramInt, IServiceError paramIServiceError)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramIServiceError);
    a(paramInt, paramIServiceError.a());
  }

  public final void a(int paramInt, String paramString)
  {
    a(new SimpleDialogInfo(paramInt, paramString));
  }

  public final void a(int paramInt, String paramString, boolean paramBoolean)
  {
    a(getString(paramInt), paramString, paramBoolean);
  }

  public final void a(int paramInt, List<IServiceError> paramList)
  {
    int k = paramList.size();
    for (int m = 0; m < k; m++)
    {
      IServiceError localIServiceError = (IServiceError)paramList.get(m);
      if (m == 0)
        a(paramInt, localIServiceError);
    }
  }

  protected final void a(Intent paramIntent, QuickPayRecipient paramQuickPayRecipient)
  {
    Object localObject;
    if (paramQuickPayRecipient.k() > 1)
      localObject = QuickPayChooseRecipientNotificatonActivity.class;
    while (true)
    {
      Intent localIntent = new Intent(this, (Class)localObject);
      localIntent.putExtra("recipient", paramQuickPayRecipient);
      localIntent.putExtra("isRequestForMoney", paramIntent.getBooleanExtra("isRequestForMoney", false));
      localIntent.putExtra("quick_pay_transaction", paramIntent.getSerializableExtra("quick_pay_transaction"));
      localIntent.addFlags(paramIntent.getFlags());
      startActivity(localIntent);
      return;
      paramIntent.setFlags(67108864);
      if (paramIntent.getBooleanExtra("isRequestForMoney", false))
        localObject = QuickPayRequestMoneyActivity.class;
      else
        localObject = QuickPaySendMoneyActivity.class;
    }
  }

  @Deprecated
  public final void a(Intent paramIntent, String paramString)
  {
    SharedPreferences localSharedPreferences = getPreferences(0);
    if (localSharedPreferences.getBoolean(paramString, true))
    {
      k.a locala1 = new k.a(this);
      k.a locala2 = locala1.a(2131165644);
      locala2.i = false;
      locala2.c(2131165643).a("Hide", new em(this, localSharedPreferences, paramString, paramIntent)).b("Remind", new el(this, paramIntent));
      locala1.d(-1).show();
      return;
    }
    startActivity(paramIntent);
  }

  public abstract void a(Bundle paramBundle);

  protected final void a(View paramView, Announcement paramAnnouncement)
  {
    ((TextView)paramView.findViewById(2131296298)).setText(paramAnnouncement.b());
    paramView.setOnClickListener(a(ManagedContentActivity.class).a("documentId", paramAnnouncement.c()).a("viewTitle", Integer.valueOf(2131165910)));
    paramView.setVisibility(0);
  }

  protected final void a(ListView paramListView)
  {
    View localView1 = getLayoutInflater().inflate(2130903089, null);
    localView1.setBackgroundResource(2131099656);
    localView1.setPadding(5, 10, 5, 10);
    View localView2 = localView1.findViewById(2131296435);
    paramListView.addFooterView(localView1, null, true);
    TextView localTextView = (TextView)findViewById(2131296435);
    localView2.setOnClickListener(a(PrivateBankingDisclosuresActivity.class).a("title", localTextView.getText().toString()));
  }

  public final void a(SimpleDialogInfo paramSimpleDialogInfo)
  {
    this.b.add(paramSimpleDialogInfo);
    k localk = b(paramSimpleDialogInfo);
    try
    {
      localk.show();
      return;
    }
    catch (Exception localException)
    {
    }
  }

  protected final void a(DetailView paramDetailView, int paramInt)
  {
    new com.chase.sig.android.view.detail.g(paramDetailView, this, paramInt);
  }

  public final <Params> void a(Class<? extends ae<?, Params, ?, ?>> paramClass, Params[] paramArrayOfParams)
  {
    ae localae = this.e.a(paramClass);
    if (!localae.d())
      localae.execute(paramArrayOfParams);
  }

  public final void a(String paramString1, String paramString2, boolean paramBoolean)
  {
    View localView = n().findViewById(2131296476);
    if (localView != null)
      n().removeView(localView);
    SlidingDrawer localSlidingDrawer = (SlidingDrawer)getLayoutInflater().inflate(2130903106, null);
    LinearLayout localLinearLayout = (LinearLayout)((ScrollView)localSlidingDrawer.getContent()).getChildAt(0);
    TextView localTextView = new TextView(this);
    Resources localResources = getResources();
    localTextView.setTextColor(localResources.getColor(2131099724));
    if (s.A(paramString2))
      localTextView.setText(Html.fromHtml(paramString2));
    while (true)
    {
      localLinearLayout.addView(localTextView);
      Button localButton = (Button)localSlidingDrawer.getHandle();
      localButton.setText(paramString1);
      localButton.setTextColor(localResources.getColor(2131099721));
      RelativeLayout.LayoutParams localLayoutParams = new RelativeLayout.LayoutParams(-1, (int)TypedValue.applyDimension(1, 175.0F, localResources.getDisplayMetrics()));
      localLayoutParams.addRule(12, -1);
      localSlidingDrawer.setPadding(0, 0, 0, 0);
      n().addView(localSlidingDrawer, localLayoutParams);
      if (paramBoolean)
        localSlidingDrawer.startAnimation(AnimationUtils.loadAnimation(this, 2130968576));
      this.i = paramString2;
      this.j = paramString1;
      return;
      localTextView.setText(paramString2);
    }
  }

  public void a(boolean paramBoolean, int paramInt)
  {
    a(paramBoolean, (Button)findViewById(paramInt));
  }

  protected final boolean a(Intent paramIntent)
  {
    return getPackageManager().queryIntentActivities(paramIntent, 65536).size() > 0;
  }

  protected void a_()
  {
    int k = 2130837666;
    requestWindowFeature(1);
    setContentView(2130903054);
    if ((((ChaseApplication)getApplication()).b() != null) && (((ChaseApplication)getApplication()).b().b != null))
    {
      String str = ((ChaseApplication)getApplication()).b().b.D();
      if ((s.l(str)) || (!str.equals(a)))
        break label108;
    }
    label108: for (int m = 2130837665; ; m = k)
    {
      k = m;
      ((ImageView)f().findViewById(2131296302)).setImageResource(k);
      return;
    }
  }

  public final void b(int paramInt)
  {
    try
    {
      View localView1 = View.inflate(this, paramInt, null);
      ViewGroup localViewGroup = n();
      RelativeLayout.LayoutParams localLayoutParams = new RelativeLayout.LayoutParams(localViewGroup.getLayoutParams());
      localLayoutParams.addRule(3, 2131296300);
      localViewGroup.addView(localView1, localLayoutParams);
      View localView2 = localViewGroup.findViewById(2131296435);
      Object localObject;
      if (localView2 != null)
      {
        if (!ChaseApplication.a().i())
          break label115;
        localObject = a(PrivateBankingDisclosuresActivity.class).a("title", ((TextView)localView2.findViewById(2131296435)).getText().toString());
      }
      while (true)
      {
        localView2.setOnClickListener((View.OnClickListener)localObject);
        BehaviorAnalyticsAspect.a().c(this);
        return;
        label115: localObject = a(DisclosuresActivity.class);
        if (s.m(this.f))
        {
          com.chase.sig.android.activity.a.f localf = ((com.chase.sig.android.activity.a.f)localObject).a("dislosure id", this.f).d();
          localObject = localf;
        }
      }
    }
    catch (Throwable localThrowable)
    {
      BehaviorAnalyticsAspect.a().c(this);
      throw localThrowable;
    }
  }

  public final void b(IServiceError paramIServiceError)
  {
    if (s.l(paramIServiceError.b()))
    {
      a(-9999, paramIServiceError);
      return;
    }
    a(-9999, paramIServiceError.b(), paramIServiceError.a());
  }

  protected final void b(DetailView paramDetailView, int paramInt)
  {
    new com.chase.sig.android.view.detail.f(paramDetailView, this, paramInt);
  }

  public final void b(String paramString1, String paramString2, boolean paramBoolean)
  {
    Uri localUri = Uri.parse(String.format("tel:%s", new Object[] { paramString1 }));
    Intent localIntent = new Intent("android.intent.action.DIAL");
    localIntent.setData(localUri);
    if (a(localIntent))
    {
      startActivity(localIntent);
      return;
    }
    if (paramBoolean)
    {
      e(paramString2);
      return;
    }
    f(paramString2);
  }

  public final void b(List<IServiceError> paramList)
  {
    int k = paramList.size();
    int m = 0;
    if (m < k)
    {
      IServiceError localIServiceError = (IServiceError)paramList.get(m);
      if (m == 0)
      {
        BehaviorAnalyticsAspect.a();
        BehaviorAnalyticsAspect.b(localIServiceError);
        a(new SimpleDialogInfo(localIServiceError.b(), localIServiceError.a()));
      }
      while (true)
      {
        m++;
        break;
        b(localIServiceError);
      }
    }
  }

  public void b(boolean paramBoolean, int paramInt)
  {
    a(paramBoolean, (Button)findViewById(paramInt));
  }

  protected boolean b_()
  {
    return (ChaseApplication.a().c()) && (ChaseApplication.a().i());
  }

  public final void c(List<IServiceError> paramList)
  {
    if (paramList == null);
    while (true)
    {
      return;
      Iterator localIterator = paramList.iterator();
      while (localIterator.hasNext())
      {
        IServiceError localIServiceError = (IServiceError)localIterator.next();
        try
        {
          b(localIServiceError);
        }
        catch (Exception localException)
        {
        }
      }
    }
  }

  public final void c(boolean paramBoolean)
  {
    if (paramBoolean);
    for (int k = -10; ; k = -1)
    {
      showDialog(k);
      return;
    }
  }

  public final boolean c(int paramInt)
  {
    Object localObject;
    switch (paramInt)
    {
    default:
      localObject = null;
      if (localObject == null)
        break label745;
      ((Intent)localObject).setFlags(67108864);
      if (com.chase.sig.android.c.a)
        com.chase.sig.android.c.a(this, new et(this, (Intent)localObject));
      break;
    case 2131297020:
    case 2131297021:
    case 2131297026:
    case 2131297024:
    case 2131297023:
    case 2131297022:
    case 2131296531:
    case 2131297028:
    case 2131297027:
    case 2131297029:
    case 2131296530:
    case 2131297030:
    case 2131297025:
    }
    while (true)
    {
      ((ChaseApplication)getApplication()).b().b().a();
      return true;
      localObject = new Intent(getApplication(), AccountsActivity.class);
      break;
      localObject = new Intent(getApplication(), PaymentsAndTransfersHomeActivity.class);
      break;
      localObject = new Intent(getApplication(), ContactUsActivity.class);
      break;
      localObject = new Intent(getApplication(), FindBranchActivity.class);
      break;
      localObject = new Intent(getApplication(), ((ChaseApplication)getApplication()).b().b.e());
      break;
      if (!((ChaseApplication)getApplication()).t())
      {
        g(2131165627);
        localObject = null;
        break;
      }
      if ((r().d != null) && (r().d.d()))
      {
        f(r().d.r());
        localObject = null;
        break;
      }
      localObject = new Intent(getApplication(), ((ChaseApplication)getApplication()).b().b.d());
      break;
      localObject = new Intent(getApplication(), TeamInfoActivity.class);
      break;
      Intent localIntent2 = new Intent(getBaseContext(), ManagedContentActivity.class);
      localIntent2.putExtra("webUrl", ((ChaseApplication)getApplication()).a(new StringBuilder("environment.").append(((ChaseApplication)getApplication()).f()).append(".mbb").toString()) + getResources().getString(2131165276));
      localIntent2.putExtra("viewTitle", 2131165854);
      localObject = localIntent2;
      break;
      Intent localIntent1 = new Intent(getBaseContext(), ManagedContentActivity.class);
      localIntent1.putExtra("webUrl", ((ChaseApplication)getApplication()).a(new StringBuilder("environment.").append(((ChaseApplication)getApplication()).f()).append(".content").toString()) + getResources().getString(2131165285));
      localIntent1.putExtra("viewTitle", 2131165626);
      localObject = localIntent1;
      break;
      localObject = new Intent(getBaseContext(), DisclosuresActivity.class);
      break;
      localObject = new Intent(getBaseContext(), MarketsActivity.class);
      break;
      if (g())
      {
        localObject = new Intent(getBaseContext(), ReceiptsSelectPlanActivity.class);
        ((Intent)localObject).putExtra("receipts_settings_mode", g());
        break;
      }
      localObject = new Intent(getBaseContext(), ReceiptsEnrollActivity.class);
      break;
      es locales = new es(this);
      if (com.chase.sig.android.c.a)
        com.chase.sig.android.c.a(this, locales);
      while (true)
      {
        return true;
        d(false);
      }
      q();
      startActivity((Intent)localObject);
    }
    label745: return false;
  }

  protected void c_()
  {
    Iterator localIterator = this.e.a().iterator();
    while (localIterator.hasNext())
    {
      ae localae = (ae)localIterator.next();
      if ((localae instanceof com.chase.sig.android.b))
        ((com.chase.sig.android.b)localae).a();
    }
  }

  public final void d(int paramInt)
  {
    ((TextView)findViewById(paramInt)).setError(null);
  }

  public final void d(String paramString)
  {
    a(2131165318, paramString, true);
  }

  public final void d(boolean paramBoolean)
  {
    this.h = true;
    Intent localIntent = new Intent(getApplication(), LogOutActivity.class);
    localIntent.putExtra("wasBecauseSessionTimedOut", paramBoolean);
    startActivity(localIntent);
  }

  public final void e(String paramString)
  {
    a(new SimpleDialogInfo(paramString, true));
  }

  public final boolean e(int paramInt)
  {
    return a((TextView)findViewById(paramInt));
  }

  public final void f(int paramInt)
  {
    e(getResources().getString(paramInt));
  }

  public final void f(String paramString)
  {
    a(new SimpleDialogInfo(paramString));
  }

  public final void g(int paramInt)
  {
    f(getResources().getString(paramInt));
  }

  public final void h(int paramInt)
  {
    a(-9999, paramInt);
  }

  protected final View.OnClickListener i(int paramInt)
  {
    return new com.chase.sig.android.activity.a.e(this, paramInt);
  }

  public final boolean j(int paramInt)
  {
    if (paramInt == 4)
    {
      MenuView localMenuView;
      if (this.c != null)
        localMenuView = this.c;
      while ((localMenuView != null) && (localMenuView.d()))
      {
        localMenuView.a();
        return true;
        localMenuView = (MenuView)findViewById(2131296527);
        this.c = localMenuView;
      }
      SlidingDrawer localSlidingDrawer = (SlidingDrawer)findViewById(2131296476);
      if ((localSlidingDrawer != null) && (localSlidingDrawer.isOpened()))
      {
        localSlidingDrawer.animateClose();
        return true;
      }
    }
    return false;
  }

  public final void l()
  {
    ((ChaseApplication)getApplication()).b().a();
  }

  protected void m()
  {
    this.e = ((mc)getLastNonConfigurationInstance());
    if (this.e == null)
      this.e = new mc();
    this.e.a(this);
  }

  public final ViewGroup n()
  {
    return (ViewGroup)findViewById(2131296299);
  }

  public final TextView o()
  {
    return (TextView)findViewById(2131296303);
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    m();
    if (paramBundle != null)
      this.h = paramBundle.getBoolean("is_logging_out", false);
    a_();
    a(paramBundle);
    Bundle localBundle = getIntent().getExtras();
    if (paramBundle == null);
    for (int k = 1; ; k = 0)
    {
      if ((localBundle != null) && (localBundle.containsKey("queued_errors")) && (k != 0))
        c((List)localBundle.getSerializable("queued_errors"));
      if ((paramBundle == null) || (!paramBundle.containsKey("simple_dialogs")))
        break;
      this.b = ((List)paramBundle.getSerializable("simple_dialogs"));
      if ((!d()) || (!d()))
        break;
      Iterator localIterator = this.b.iterator();
      while (localIterator.hasNext())
      {
        k localk = b((SimpleDialogInfo)localIterator.next());
        try
        {
          localk.show();
        }
        catch (Exception localException)
        {
        }
      }
    }
    if (paramBundle != null)
      a(paramBundle, n());
    if (com.chase.sig.android.util.e.a(paramBundle, "footnote_message"))
    {
      this.i = paramBundle.getString("footnote_message");
      a(paramBundle.getString("footnote_header"), this.i, false);
    }
    ViewGroup localViewGroup1 = f();
    ViewGroup localViewGroup2 = n();
    MenuView localMenuView;
    if ((b_()) && (localViewGroup1 != null) && (localViewGroup2 != null))
    {
      View localView = View.inflate(this, 2130903123, null);
      ViewGroup localViewGroup3 = (ViewGroup)localViewGroup1.findViewById(2131296301);
      localViewGroup3.addView(localView, new LinearLayout.LayoutParams(-2, -1));
      localView.setOnClickListener(new er(this));
      localView.setVisibility(0);
      an.a(localViewGroup3, localView);
      RelativeLayout.LayoutParams localLayoutParams = new RelativeLayout.LayoutParams(localViewGroup2.getLayoutParams());
      localLayoutParams.addRule(3, 2131296300);
      localMenuView = (MenuView)View.inflate(this, 2130903124, null);
      localMenuView.setSelectedOptionId(e());
      localViewGroup2.addView(localMenuView, localLayoutParams);
      a(localMenuView, new int[] { 2131297020, 2131297021, 2131296530, 2131296531, 2131297022 });
      if ((((ChaseApplication)getApplication()).b().b == null) || (!x()) || (!h()))
        localMenuView.c();
      ImageView localImageView = (ImageView)findViewById(2131296302);
      localImageView.setScaleType(ImageView.ScaleType.FIT_START);
      b(localImageView);
      b((TextView)findViewById(2131296303));
    }
    while (true)
    {
      this.c = localMenuView;
      return;
      localMenuView = null;
    }
  }

  public Dialog onCreateDialog(int paramInt)
  {
    int k = 2131165290;
    Dialog localDialog = super.onCreateDialog(paramInt);
    if ((paramInt == -1) || (paramInt == -10))
    {
      boolean bool = false;
      if (paramInt == -10)
        bool = true;
      eu localeu = new eu(this, this);
      localeu.setOnCancelListener(new ed(this));
      localeu.setCancelable(bool);
      localeu.setContentView(2130903147);
      return localeu;
    }
    if (paramInt == -3)
      return a(this, 2131165361);
    if (paramInt == -4)
    {
      k.a locala1 = new k.a(this);
      if (com.chase.sig.android.c.b != null)
        k = com.chase.sig.android.c.b.getIntExtra("receipt_cancel_warning", k);
      k.a locala2 = locala1.b(k);
      locala2.i = false;
      locala2.a(2131165291, new ef(this)).b(2131165293, new ee(this));
      return locala1.d(-1);
    }
    if (paramInt == -2)
    {
      k.a locala3 = new k.a(this);
      k.a locala4 = locala3.b(2131165357).c(2131165358);
      locala4.i = false;
      locala4.a(2131165288, new eh(this)).b(2131165289, new eg(this));
      return locala3.d(-1);
    }
    if ((paramInt == -9) || (paramInt == -8))
    {
      k.a locala5 = new k.a(this);
      locala5.a(r().c.p());
      locala5.a("OK", new ei(this));
      return locala5.d(-1);
    }
    if (paramInt == -11)
    {
      k.a locala6 = new k.a(this);
      locala6.a(r().c.p());
      locala6.a("OK", new com.chase.sig.android.activity.a.b());
      return locala6.d(-1);
    }
    return localDialog;
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    if ((((ChaseApplication)getApplication()).b().a == null) || (((ChaseApplication)getApplication()).b().b == null))
      return false;
    getMenuInflater().inflate(2131361792, paramMenu);
    MenuItem localMenuItem = paramMenu.findItem(2131297030);
    if (localMenuItem != null)
    {
      localMenuItem.setTitle(Html.fromHtml(getString(2131165350)));
      localMenuItem.setTitleCondensed(getString(2131165343));
    }
    if ((!x()) || (!h()))
      paramMenu.removeItem(2131297022);
    if (!((ChaseApplication)getApplication()).b().b.b())
    {
      paramMenu.removeItem(2131297023);
      paramMenu.removeItem(2131297030);
    }
    if (ChaseApplication.a().i())
    {
      paramMenu.removeItem(2131297028);
      paramMenu.removeItem(2131297029);
    }
    if (!((ChaseApplication)getApplication()).b().b.c())
      paramMenu.removeItem(2131297021);
    return true;
  }

  protected void onDestroy()
  {
    if (isFinishing())
    {
      new Object[1][0] = getClass().getName();
      if (this.e != null)
      {
        Iterator localIterator = this.e.a.values().iterator();
        while (localIterator.hasNext())
        {
          ae localae = (ae)localIterator.next();
          new Object[1][0] = localae.getClass().getName();
          localae.f();
        }
      }
    }
    a(n());
    super.onDestroy();
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    return (j(paramInt)) || (super.onKeyDown(paramInt, paramKeyEvent));
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    return c(paramMenuItem.getItemId());
  }

  protected void onPause()
  {
    try
    {
      this.g = true;
      super.onPause();
      if (!this.h)
      {
        ChaseApplication.o().interrupt();
        ChaseApplication.a(null);
      }
      a locala = new a(this);
      ChaseApplication.a(locala);
      locala.start();
      BehaviorAnalyticsAspect.a();
      BehaviorAnalyticsAspect.b(this);
      return;
    }
    catch (Throwable localThrowable)
    {
      BehaviorAnalyticsAspect.a();
      BehaviorAnalyticsAspect.b(this);
      throw localThrowable;
    }
  }

  protected void onResume()
  {
    try
    {
      super.onResume();
      this.g = false;
      if (!this.h)
      {
        com.chase.sig.android.f localf = new com.chase.sig.android.f(this);
        localf.start();
        ChaseApplication.a(localf);
      }
      a locala = ChaseApplication.p();
      if (locala != null)
        locala.interrupt();
      BehaviorAnalyticsAspect.a();
      BehaviorAnalyticsAspect.a(this);
      return;
    }
    catch (Throwable localThrowable)
    {
      BehaviorAnalyticsAspect.a();
      BehaviorAnalyticsAspect.a(this);
      throw localThrowable;
    }
  }

  public Object onRetainNonConfigurationInstance()
  {
    this.e.a(null);
    return this.e;
  }

  public void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    if (n() != null)
      b(paramBundle, n());
    if (d())
      paramBundle.putSerializable("simple_dialogs", (Serializable)this.b);
    if (this.i != null)
    {
      paramBundle.putString("footnote_message", this.i);
      paramBundle.putString("footnote_header", this.j);
    }
    paramBundle.putBoolean("is_logging_out", this.h);
  }

  public boolean onSearchRequested()
  {
    return true;
  }

  protected final Intent p()
  {
    List localList = ((ChaseApplication)getApplication()).b().b.u();
    if (localList.size() == 1);
    for (int k = 1; k != 0; k = 0)
    {
      Intent localIntent = new Intent(this, EPayStartActivity.class);
      localIntent.putExtra("selectedAccountId", ((IAccount)localList.get(0)).b());
      return localIntent;
    }
    return new Intent(this, EPaySelectToAccount.class).setFlags(1073741824);
  }

  public final void q()
  {
    if (getClass() == AccountsActivity.class)
      return;
    Intent localIntent = new Intent(getBaseContext(), AccountsActivity.class);
    localIntent.setFlags(67108864);
    startActivity(localIntent);
  }

  public final o r()
  {
    return ((ChaseApplication)getApplication()).b();
  }

  public final SplashResponse s()
  {
    return r().d;
  }

  public void setTitle(int paramInt)
  {
    setTitle(getResources().getText(paramInt));
  }

  public void setTitle(CharSequence paramCharSequence)
  {
    o().setText(paramCharSequence);
    findViewById(2131296302).setVisibility(8);
    o().setVisibility(0);
  }

  public final void t()
  {
    showDialog(-1);
    c(false);
  }

  public final void u()
  {
    Iterator localIterator = this.e.a().iterator();
    int k = 0;
    if (localIterator.hasNext())
      if (!((ae)localIterator.next() instanceof com.chase.sig.android.d))
        break label65;
    label65: for (int m = k + 1; ; m = k)
    {
      k = m;
      break;
      if (k <= 1)
      {
        removeDialog(-1);
        removeDialog(-10);
      }
      return;
    }
  }

  public final void v()
  {
    f(2131165820);
  }

  public final void w()
  {
    Toast.makeText(this, 2131165680, 0).show();
  }

  protected final boolean x()
  {
    if (Build.VERSION.SDK_INT >= 4)
      return getPackageManager().hasSystemFeature("android.hardware.camera");
    return true;
  }

  protected final void y()
  {
    a(n());
  }

  public final ChaseApplication z()
  {
    return (ChaseApplication)getApplication();
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.eb
 * JD-Core Version:    0.6.2
 */