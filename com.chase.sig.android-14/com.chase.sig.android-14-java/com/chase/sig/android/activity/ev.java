package com.chase.sig.android.activity;

import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.domain.BranchLocation;

final class ev
  implements View.OnClickListener
{
  ev(LocationInfoActivity paramLocationInfoActivity, BranchLocation paramBranchLocation)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    Intent localIntent = new Intent("android.intent.action.VIEW", Uri.parse("geo:0,0?q=" + this.a.a().replace(" ", "+") + "+" + this.a.c().replace(" ", "+") + "+,+" + this.a.g()));
    try
    {
      this.b.startActivity(localIntent);
      return;
    }
    catch (Exception localException)
    {
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.ev
 * JD-Core Version:    0.6.2
 */