package com.chase.sig.android.activity;

import android.content.Intent;
import android.content.res.Resources;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.ChaseApplication;

final class ex
  implements View.OnClickListener
{
  ex(LoginActivity paramLoginActivity)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    Intent localIntent2;
    if (ChaseApplication.a().i())
    {
      Intent localIntent1 = new Intent(this.a.getBaseContext(), PrivacyNoticesActivity.class);
      localIntent1.putExtra("title", ((Button)this.a.findViewById(2131296586)).getText());
      localIntent2 = localIntent1;
    }
    while (true)
    {
      this.a.startActivity(localIntent2);
      return;
      localIntent2 = new Intent(this.a.getBaseContext(), ManagedContentActivity.class);
      localIntent2.putExtra("webUrl", this.a.z().a(new StringBuilder("environment.").append(this.a.z().f()).append(".mbb").toString()) + this.a.getResources().getString(2131165276));
      localIntent2.putExtra("viewTitle", 2131165854);
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.ex
 * JD-Core Version:    0.6.2
 */