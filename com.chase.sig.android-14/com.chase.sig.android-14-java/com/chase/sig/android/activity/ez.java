package com.chase.sig.android.activity;

import android.os.AsyncTask.Status;
import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;

final class ez
  implements View.OnClickListener
{
  ez(LoginActivity paramLoginActivity)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    LoginActivity.c localc = (LoginActivity.c)this.a.e.a(LoginActivity.c.class);
    if (localc.getStatus() != AsyncTask.Status.RUNNING)
      localc.execute(new Void[0]);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.ez
 * JD-Core Version:    0.6.2
 */