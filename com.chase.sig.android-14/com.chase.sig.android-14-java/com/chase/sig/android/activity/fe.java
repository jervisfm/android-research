package com.chase.sig.android.activity;

import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.net.Uri;
import com.chase.sig.android.ChaseApplication;

final class fe
  implements DialogInterface.OnClickListener
{
  fe(LoginActivity paramLoginActivity, PackageInfo paramPackageInfo)
  {
  }

  public final void onClick(DialogInterface paramDialogInterface, int paramInt)
  {
    if (this.a != null)
    {
      Intent localIntent1 = new Intent();
      localIntent1.setAction("android.intent.action.MAIN");
      localIntent1.addCategory("android.intent.category.LAUNCHER");
      localIntent1.setComponent(new ComponentName(this.a.packageName, "com.chase.sig.android.activity.HomeActivity"));
      this.b.startActivity(localIntent1);
    }
    while (true)
    {
      this.b.z().s();
      return;
      String str1 = this.b.getString(2131165189);
      boolean bool = str1.equals("Google_Android_Marketplace");
      String str2 = null;
      if (bool)
        str2 = "market://details?id=com.jpm.sig.android";
      if (str1.equals("Amazon_Appstore"))
        str2 = "http://www.amazon.com/gp/mas/dl/android?p=com.jpm.sig.android";
      Intent localIntent2 = new Intent("android.intent.action.VIEW", Uri.parse(str2));
      this.b.startActivity(localIntent2);
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.fe
 * JD-Core Version:    0.6.2
 */