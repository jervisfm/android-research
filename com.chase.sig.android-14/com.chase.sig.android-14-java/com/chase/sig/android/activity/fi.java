package com.chase.sig.android.activity;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.domain.Quote;
import com.chase.sig.android.view.JPTabWidget;

final class fi
  implements View.OnClickListener
{
  fi(MarketsActivity paramMarketsActivity)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    Quote localQuote = MarketsActivity.a(this.a, MarketsActivity.a(this.a).getSelectedTabId());
    if (localQuote != null)
    {
      Intent localIntent = new Intent(this.a, QuoteChartsActivity.class);
      localIntent.putExtra("transaction_object", localQuote);
      localIntent.putExtra("exchangeName", MarketsActivity.b(this.a, MarketsActivity.a(this.a).getSelectedTabId()));
      this.a.startActivityForResult(localIntent, 35);
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.fi
 * JD-Core Version:    0.6.2
 */