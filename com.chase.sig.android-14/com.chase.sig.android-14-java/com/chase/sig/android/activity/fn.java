package com.chase.sig.android.activity;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.domain.g;
import com.chase.sig.android.domain.o;
import com.chase.sig.android.service.SplashResponse;
import com.chase.sig.android.service.quickpay.TodoListResponse;

final class fn
  implements View.OnClickListener
{
  fn(PaymentsAndTransfersHomeActivity paramPaymentsAndTransfersHomeActivity)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    TodoListResponse localTodoListResponse = this.a.r().c;
    if (PaymentsAndTransfersHomeActivity.a(this.a))
    {
      this.a.f(this.a.s().t());
      return;
    }
    if (!PaymentsAndTransfersHomeActivity.b(this.a).g())
    {
      this.a.g(2131165671);
      return;
    }
    if (!PaymentsAndTransfersHomeActivity.b(this.a).h())
    {
      this.a.g(2131165672);
      return;
    }
    if (localTodoListResponse == null)
    {
      this.a.g(2131165673);
      return;
    }
    if (localTodoListResponse.e())
    {
      this.a.c(localTodoListResponse.g());
      return;
    }
    Intent localIntent;
    if (localTodoListResponse.d() == 0)
      localIntent = new Intent(this.a, QuickPayHomeActivity.class);
    while (true)
    {
      this.a.startActivity(localIntent);
      return;
      localIntent = new Intent(this.a, QuickPayTodoListActivity.class);
      localIntent.putExtra("quick_pay_skip_loading_todo_list", true);
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.fn
 * JD-Core Version:    0.6.2
 */