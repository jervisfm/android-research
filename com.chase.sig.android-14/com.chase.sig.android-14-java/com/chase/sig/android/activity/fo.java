package com.chase.sig.android.activity;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.domain.Position;
import com.chase.sig.android.util.Dollar;

final class fo
  implements View.OnClickListener
{
  fo(PositionDetailActivity paramPositionDetailActivity)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    Intent localIntent = new Intent(this.a, QuoteDetailsActivity.class);
    localIntent.putExtra("ticker_symbol", this.a.a.i());
    localIntent.putExtra("quote_code", this.a.a.y());
    localIntent.putExtra("quantity", this.a.a.f());
    localIntent.putExtra("gain_loss", this.a.a.d().h());
    this.a.startActivity(localIntent);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.fo
 * JD-Core Version:    0.6.2
 */