package com.chase.sig.android.activity;

import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;

final class fq
  implements View.OnClickListener
{
  fq(PositionsActivity paramPositionsActivity)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    if (PositionsActivity.a(this.a))
    {
      AlphaAnimation localAlphaAnimation1 = new AlphaAnimation(0.0F, 1.0F);
      localAlphaAnimation1.setDuration(250L);
      PositionsActivity.b(this.a).setVisibility(0);
      PositionsActivity.c(this.a).setVisibility(0);
      PositionsActivity.d(this.a).setVisibility(0);
      PositionsActivity.d(this.a).startAnimation(localAlphaAnimation1);
      PositionsActivity.b(this.a).startAnimation(localAlphaAnimation1);
      PositionsActivity.c(this.a).startAnimation(localAlphaAnimation1);
      PositionsActivity.e(this.a).setBackgroundResource(2130837742);
    }
    while (true)
    {
      PositionsActivity localPositionsActivity = this.a;
      boolean bool1 = PositionsActivity.a(this.a);
      boolean bool2 = false;
      if (!bool1)
        bool2 = true;
      PositionsActivity.a(localPositionsActivity, bool2);
      return;
      AlphaAnimation localAlphaAnimation2 = new AlphaAnimation(0.8F, 0.0F);
      localAlphaAnimation2.setDuration(200L);
      PositionsActivity.d(this.a).startAnimation(localAlphaAnimation2);
      PositionsActivity.b(this.a).startAnimation(localAlphaAnimation2);
      PositionsActivity.c(this.a).startAnimation(localAlphaAnimation2);
      localAlphaAnimation2.setAnimationListener(PositionsActivity.f(this.a));
      PositionsActivity.e(this.a).setBackgroundResource(2130837741);
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.fq
 * JD-Core Version:    0.6.2
 */