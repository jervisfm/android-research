package com.chase.sig.android.activity;

import android.content.Intent;
import android.net.Uri;
import android.webkit.WebView;
import android.webkit.WebViewClient;

final class fu extends WebViewClient
{
  fu(PrivacyNoticeDetailActivity paramPrivacyNoticeDetailActivity)
  {
  }

  public final boolean shouldOverrideUrlLoading(WebView paramWebView, String paramString)
  {
    if (paramString.startsWith("tel:"))
    {
      Intent localIntent1 = new Intent("android.intent.action.DIAL");
      localIntent1.setData(Uri.parse(paramString));
      if (this.a.a(localIntent1))
        this.a.startActivity(localIntent1);
      return true;
    }
    Intent localIntent2 = new Intent(this.a.getBaseContext(), ManagedContentActivity.class);
    localIntent2.putExtra("webUrl", paramString);
    this.a.startActivity(localIntent2);
    return true;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.fu
 * JD-Core Version:    0.6.2
 */