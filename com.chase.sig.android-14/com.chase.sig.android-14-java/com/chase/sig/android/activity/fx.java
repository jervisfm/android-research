package com.chase.sig.android.activity;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import java.util.Map;

final class fx
  implements AdapterView.OnItemClickListener
{
  fx(PrivateBankingDisclosuresActivity paramPrivateBankingDisclosuresActivity)
  {
  }

  public final void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    while (true)
    {
      try
      {
        PrivateBankingDisclosuresActivity.a(this.a).getItemAtPosition(paramInt);
        String str = (String)((Map)PrivateBankingDisclosuresActivity.a(this.a).getItemAtPosition(paramInt)).get("title");
        if (str.equals(this.a.getString(2131165836)))
        {
          TextView localTextView = (TextView)paramView.findViewById(2131296444);
          Intent localIntent2 = new Intent(this.a, PrivacyNoticesActivity.class);
          localIntent2.putExtra("title", localTextView.getText().toString());
          this.a.startActivity(localIntent2);
          BehaviorAnalyticsAspect.a();
          BehaviorAnalyticsAspect.a(paramAdapterView, paramView, paramInt);
          return;
        }
        if ((str.equals(this.a.getString(2131165840))) || (str.equals(this.a.getString(2131165841))) || (str.equals(this.a.getString(2131165842))))
          break label251;
        if (str.equals(this.a.getString(2131165843)))
        {
          break label251;
          if (i == -1)
            continue;
          Intent localIntent1 = new Intent(this.a, PrivateBankingDisclosureDetails.class);
          localIntent1.putExtra("disclosure_document_index", paramInt);
          this.a.startActivity(localIntent1);
          continue;
        }
      }
      catch (Throwable localThrowable)
      {
        BehaviorAnalyticsAspect.a();
        BehaviorAnalyticsAspect.a(paramAdapterView, paramView, paramInt);
        throw localThrowable;
      }
      int i = -1;
      continue;
      label251: i = paramInt;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.fx
 * JD-Core Version:    0.6.2
 */