package com.chase.sig.android.activity;

import android.content.Intent;
import android.webkit.WebView;
import android.webkit.WebViewClient;

final class fy extends WebViewClient
{
  fy(PrivateBankingDisclosuresActivity paramPrivateBankingDisclosuresActivity)
  {
  }

  public final void onLoadResource(WebView paramWebView, String paramString)
  {
  }

  public final boolean shouldOverrideUrlLoading(WebView paramWebView, String paramString)
  {
    Intent localIntent = new Intent(this.a.getBaseContext(), ManagedContentActivity.class);
    localIntent.putExtra("webUrl", paramString);
    this.a.startActivity(localIntent);
    return true;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.fy
 * JD-Core Version:    0.6.2
 */