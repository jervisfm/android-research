package com.chase.sig.android.activity;

import android.content.Context;
import android.content.res.Resources;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import com.chase.sig.android.util.s;
import java.util.List;
import java.util.Map;

final class fz extends SimpleAdapter
{
  fz(PrivateBankingDisclosuresActivity paramPrivateBankingDisclosuresActivity, Context paramContext, List paramList, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    super(paramContext, paramList, 2130903093, paramArrayOfString, paramArrayOfInt);
  }

  private boolean a(int paramInt)
  {
    return s.l((String)((Map)getItem(paramInt)).get("title"));
  }

  public final View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    View localView = super.getView(paramInt, paramView, paramViewGroup);
    ImageView localImageView = (ImageView)localView.findViewById(2131296445);
    if (a(paramInt))
    {
      localImageView.setVisibility(8);
      localView.setPadding(localView.getPaddingLeft(), 0, localView.getPaddingRight(), 0);
      localView.setBackgroundColor(this.a.getResources().getColor(2131099685));
    }
    while ((PrivateBankingDisclosuresActivity.b(this.a) == null) || (s.n(PrivateBankingDisclosuresActivity.b(this.a))))
    {
      PrivateBankingDisclosuresActivity.a(this.a).setFooterDividersEnabled(false);
      return localView;
      localImageView.setVisibility(0);
      localView.setPadding(localView.getPaddingLeft(), localView.getPaddingLeft(), localView.getPaddingLeft(), localView.getPaddingLeft());
      localView.setBackgroundColor(0);
    }
    PrivateBankingDisclosuresActivity.a(this.a).setFooterDividersEnabled(true);
    return localView;
  }

  public final boolean isEnabled(int paramInt)
  {
    return !a(paramInt);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.fz
 * JD-Core Version:    0.6.2
 */