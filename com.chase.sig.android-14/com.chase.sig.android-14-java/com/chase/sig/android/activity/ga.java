package com.chase.sig.android.activity;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;

final class ga
  implements View.OnClickListener
{
  ga(QuickDepositCaptureActivity paramQuickDepositCaptureActivity, Button paramButton)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    View localView = this.b.findViewById(2131296317);
    if (localView.getVisibility() == 0)
    {
      this.a.setText("Help");
      QuickDepositCaptureActivity.a(this.b);
      localView.setVisibility(4);
      return;
    }
    this.a.setText("Done");
    ((TextView)this.b.findViewById(2131296308)).setText(2131165648);
    localView.setVisibility(0);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.ga
 * JD-Core Version:    0.6.2
 */