package com.chase.sig.android.activity;

import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;

final class gb
  implements View.OnClickListener
{
  gb(QuickDepositCaptureActivity paramQuickDepositCaptureActivity, CapturePreview paramCapturePreview)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    QuickDepositCaptureActivity.b(this.b).setEnabled(false);
    Camera.Parameters localParameters = this.a.b.getParameters();
    localParameters.setRotation(0);
    this.a.b.setParameters(localParameters);
    this.a.b.autoFocus(new gc(this));
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.gb
 * JD-Core Version:    0.6.2
 */