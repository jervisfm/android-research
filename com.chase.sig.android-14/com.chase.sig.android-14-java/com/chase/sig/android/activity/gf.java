package com.chase.sig.android.activity;

import android.content.Intent;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.os.Bundle;

final class gf
  implements Camera.PictureCallback
{
  gf(QuickDepositCaptureActivity paramQuickDepositCaptureActivity)
  {
  }

  public final void onPictureTaken(byte[] paramArrayOfByte, Camera paramCamera)
  {
    Intent localIntent = new Intent(this.a, QuickDepositReviewImageActivity.class);
    localIntent.putExtra("qd_image_side", QuickDepositCaptureActivity.c(this.a));
    localIntent.putExtra("quick_deposit", this.a.getIntent().getExtras().getSerializable("quick_deposit"));
    localIntent.putExtra("image_data", QuickDepositCaptureActivity.a(paramArrayOfByte));
    this.a.startActivity(localIntent);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.gf
 * JD-Core Version:    0.6.2
 */