package com.chase.sig.android.activity;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;

final class gi
  implements View.OnClickListener
{
  gi(QuickDepositReviewImageActivity paramQuickDepositReviewImageActivity)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    Intent localIntent = new Intent(this.a, QuickDepositCaptureActivity.class);
    localIntent.putExtra("qd_image_side", QuickDepositReviewImageActivity.a(this.a));
    localIntent.putExtra("quick_deposit", QuickDepositReviewImageActivity.b(this.a));
    localIntent.addFlags(1073741824);
    this.a.startActivity(localIntent);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.gi
 * JD-Core Version:    0.6.2
 */