package com.chase.sig.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.domain.QuickDeposit;

final class gj
  implements View.OnClickListener
{
  gj(QuickDepositReviewImageActivity paramQuickDepositReviewImageActivity)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    QuickDeposit localQuickDeposit = QuickDepositReviewImageActivity.b(this.a);
    if ("qd_check_front_image".equals(this.a.getIntent().getExtras().getString("qd_image_side")))
      localQuickDeposit.b(this.a.getIntent().getExtras().getByteArray("image_data"));
    while (true)
    {
      Intent localIntent = new Intent(this.a, QuickDepositStartActivity.class);
      localIntent.putExtra("quick_deposit", localQuickDeposit);
      localIntent.addFlags(131072);
      this.a.startActivity(localIntent);
      this.a.finish();
      return;
      localQuickDeposit.a(this.a.getIntent().getExtras().getByteArray("image_data"));
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.gj
 * JD-Core Version:    0.6.2
 */