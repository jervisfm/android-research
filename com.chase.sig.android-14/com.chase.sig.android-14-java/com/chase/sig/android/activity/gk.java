package com.chase.sig.android.activity;

import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;

final class gk
  implements GestureDetector.OnGestureListener
{
  gk(QuickDepositReviewImageActivity paramQuickDepositReviewImageActivity)
  {
  }

  public final boolean onDown(MotionEvent paramMotionEvent)
  {
    return true;
  }

  public final boolean onFling(MotionEvent paramMotionEvent1, MotionEvent paramMotionEvent2, float paramFloat1, float paramFloat2)
  {
    return true;
  }

  public final void onLongPress(MotionEvent paramMotionEvent)
  {
  }

  public final boolean onScroll(MotionEvent paramMotionEvent1, MotionEvent paramMotionEvent2, float paramFloat1, float paramFloat2)
  {
    QuickDepositReviewImageActivity.d(this.a).a(paramFloat1, paramFloat2);
    return true;
  }

  public final void onShowPress(MotionEvent paramMotionEvent)
  {
  }

  public final boolean onSingleTapUp(MotionEvent paramMotionEvent)
  {
    QuickDepositReviewImageActivity.c(this.a);
    return true;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.gk
 * JD-Core Version:    0.6.2
 */