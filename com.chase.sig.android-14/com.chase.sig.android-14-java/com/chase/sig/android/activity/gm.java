package com.chase.sig.android.activity;

import android.os.AsyncTask.Status;
import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;

final class gm
  implements View.OnClickListener
{
  gm(QuickDepositServiceActivationActivity paramQuickDepositServiceActivationActivity)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    QuickDepositServiceActivationActivity.b localb = (QuickDepositServiceActivationActivity.b)this.a.e.a(QuickDepositServiceActivationActivity.b.class);
    if (localb.getStatus() != AsyncTask.Status.RUNNING)
      localb.execute(new Void[0]);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.gm
 * JD-Core Version:    0.6.2
 */