package com.chase.sig.android.activity;

import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AnimationUtils;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;

final class gt
  implements View.OnClickListener
{
  gt(QuickDepositStartActivity paramQuickDepositStartActivity)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    paramView.startAnimation(AnimationUtils.loadAnimation(this.a, 2130968577));
    QuickDepositStartActivity.a(this.a, "qd_check_back_image");
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.gt
 * JD-Core Version:    0.6.2
 */