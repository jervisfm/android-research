package com.chase.sig.android.activity;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;

final class gv
  implements View.OnClickListener
{
  gv(QuickDepositStartActivity paramQuickDepositStartActivity)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    Intent localIntent = new Intent(this.a, QuickDepositCaptureActivity.class);
    localIntent.putExtra("qd_image_side", "qd_check_back_image");
    localIntent.putExtra("quick_deposit", QuickDepositStartActivity.c(this.a));
    localIntent.setFlags(1073741824);
    this.a.startActivity(localIntent);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.gv
 * JD-Core Version:    0.6.2
 */