package com.chase.sig.android.activity;

import android.os.AsyncTask.Status;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ListAdapter;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.domain.QuickDeposit;
import com.chase.sig.android.domain.QuickDepositAccount;
import com.chase.sig.android.domain.QuickDepositAccount.Location;
import com.chase.sig.android.view.AmountView;
import com.chase.sig.android.view.JPSpinner;
import java.util.List;

final class gy
  implements View.OnClickListener
{
  gy(QuickDepositStartActivity paramQuickDepositStartActivity)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    if (QuickDepositStartActivity.f(this.a))
    {
      QuickDepositStartActivity.c(this.a).a(this.a.d());
      QuickDepositStartActivity.c(this.a).a(QuickDepositStartActivity.g(this.a).getDollarAmount());
      QuickDepositAccount localQuickDepositAccount = (QuickDepositAccount)QuickDepositStartActivity.i(this.a).get(QuickDepositStartActivity.h(this.a).getSelectedItemPosition());
      localQuickDepositAccount.d();
      QuickDepositStartActivity.c(this.a).a(localQuickDepositAccount);
      if (QuickDepositStartActivity.j(this.a).getCount() == 0)
        break label224;
      int i = QuickDepositStartActivity.a(this.a).getSelectedItemPosition();
      QuickDepositAccount.Location localLocation = (QuickDepositAccount.Location)localQuickDepositAccount.c().get(i);
      QuickDepositStartActivity.c(this.a).f(localLocation.b());
      QuickDepositStartActivity.c(this.a).e(localLocation.a());
    }
    while (true)
    {
      QuickDepositStartActivity.b localb = (QuickDepositStartActivity.b)this.a.e.a(QuickDepositStartActivity.b.class);
      if (localb.getStatus() != AsyncTask.Status.RUNNING)
      {
        QuickDeposit[] arrayOfQuickDeposit = new QuickDeposit[1];
        arrayOfQuickDeposit[0] = QuickDepositStartActivity.c(this.a);
        localb.execute(arrayOfQuickDeposit);
      }
      return;
      label224: QuickDepositStartActivity.c(this.a).f("N/A");
      QuickDepositStartActivity.c(this.a).e(null);
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.gy
 * JD-Core Version:    0.6.2
 */