package com.chase.sig.android.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import com.chase.sig.android.c;
import com.chase.sig.android.domain.Transaction;
import com.chase.sig.android.service.IServiceError;
import com.chase.sig.android.service.movemoney.RequestFlags;
import com.chase.sig.android.service.movemoney.ServiceResponse;
import com.chase.sig.android.util.s;
import com.chase.sig.android.view.b;
import com.chase.sig.android.view.b.a;
import com.chase.sig.android.view.detail.DetailView;
import com.chase.sig.android.view.detail.o;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public abstract class h<T extends Transaction> extends ai
  implements fk.e, ma
{
  protected static final String[] a = { "70011", "70033" };
  private b.a b = new j(this);

  protected final DetailView a()
  {
    return (DetailView)findViewById(2131296969);
  }

  protected final o a(Transaction paramTransaction)
  {
    if (paramTransaction.C() != null)
      return new o(getString(2131165309), paramTransaction.i(), paramTransaction.C());
    return new o(getString(2131165309), paramTransaction.i());
  }

  public void a(Bundle paramBundle)
  {
    b(2130903236);
    a(2131296971, new i(this));
    findViewById(2131296970).setOnClickListener(new com.chase.sig.android.activity.a.a(this));
  }

  public final boolean b()
  {
    boolean bool1;
    if (!d().l())
    {
      boolean bool2 = getIntent().getBooleanExtra("edit_single", false);
      bool1 = false;
      if (!bool2);
    }
    else
    {
      bool1 = true;
    }
    return bool1;
  }

  public final boolean c()
  {
    return getIntent().getBooleanExtra("edit_single", false);
  }

  protected final T d()
  {
    return (Transaction)getIntent().getSerializableExtra("transaction_object");
  }

  protected final void e()
  {
    if (g())
    {
      Class localClass = h();
      Transaction[] arrayOfTransaction = new Transaction[1];
      arrayOfTransaction[0] = f();
      a(localClass, arrayOfTransaction);
    }
  }

  protected abstract T f();

  protected abstract boolean g();

  protected abstract Class<? extends a<T>> h();

  protected abstract Class<? extends eb> i();

  protected abstract Date j();

  public final void k()
  {
    a().a();
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    if (paramInt == 0)
    {
      Calendar localCalendar = Calendar.getInstance();
      localCalendar.setTime(j());
      return a(localCalendar, this.b).a(s.t(a().e("DATE").g()));
    }
    return super.onCreateDialog(paramInt);
  }

  protected void onPrepareDialog(int paramInt, Dialog paramDialog)
  {
    if (paramInt == 0)
      ((b)paramDialog).a(s.t(a().e("DATE").g()));
  }

  protected void onRestart()
  {
    super.onRestart();
    c.a();
  }

  protected void onResume()
  {
    super.onResume();
    c.a();
  }

  public static abstract class a<T extends Transaction> extends com.chase.sig.android.d<h<T>, T, Void, ServiceResponse>
  {
    T a;

    protected abstract com.chase.sig.android.service.movemoney.d<T> a();

    protected void a(ServiceResponse paramServiceResponse)
    {
      if (paramServiceResponse.f())
      {
        ((h)this.b).c(paramServiceResponse.g());
        return;
      }
      int k;
      label78: int m;
      if (!this.a.l())
      {
        Iterator localIterator = paramServiceResponse.g().iterator();
        if (localIterator.hasNext())
        {
          IServiceError localIServiceError = (IServiceError)localIterator.next();
          String[] arrayOfString = h.a;
          int j = arrayOfString.length;
          k = 0;
          if (k < j)
            if (arrayOfString[k].equals(localIServiceError.c()))
            {
              m = 1;
              label106: if (m == 0)
                break label142;
            }
        }
      }
      for (int i = 1; ; i = 0)
      {
        if (i == 0)
          break label149;
        ((h)this.b).g(b());
        return;
        k++;
        break label78;
        m = 0;
        break label106;
        label142: break;
      }
      label149: b(paramServiceResponse);
      this.a.m(paramServiceResponse.j());
      Intent localIntent1 = new Intent(((h)this.b).getApplicationContext(), PaymentsAndTransfersHomeActivity.class);
      localIntent1.setFlags(67108864);
      c.a(localIntent1);
      Intent localIntent2 = new Intent(this.b, ((h)this.b).i());
      Bundle localBundle = ((h)this.b).getIntent().getExtras();
      Transaction localTransaction = this.a;
      if (localTransaction != null)
        localBundle.putSerializable(localTransaction.getClass().getCanonicalName(), localTransaction);
      localIntent2.putExtra("transaction_object", this.a);
      if (((h)this.b).c());
      for (RequestFlags localRequestFlags = RequestFlags.d; ; localRequestFlags = RequestFlags.c)
      {
        localIntent2.putExtra("request_flags", localRequestFlags);
        localIntent2.putExtra("queued_errors", (Serializable)paramServiceResponse.g());
        ((h)this.b).startActivity(localIntent2);
        return;
      }
    }

    protected abstract int b();

    protected void b(ServiceResponse paramServiceResponse)
    {
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.h
 * JD-Core Version:    0.6.2
 */