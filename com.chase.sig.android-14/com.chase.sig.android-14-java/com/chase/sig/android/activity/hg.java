package com.chase.sig.android.activity;

import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import java.util.Map;

final class hg
  implements AdapterView.OnItemClickListener
{
  hg(QuickPayActivityActivity paramQuickPayActivityActivity, int paramInt)
  {
  }

  public final void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    try
    {
      if (paramView == QuickPayActivityActivity.b(this.b))
      {
        QuickPayActivityActivity localQuickPayActivityActivity = this.b;
        Object[] arrayOfObject = new Object[2];
        arrayOfObject[0] = QuickPayActivityActivity.a(this.b);
        arrayOfObject[1] = Integer.valueOf(this.a);
        localQuickPayActivityActivity.a(QuickPayActivityActivity.a.class, arrayOfObject);
      }
      while (true)
      {
        BehaviorAnalyticsAspect.a();
        BehaviorAnalyticsAspect.a(paramAdapterView, paramView, paramInt);
        return;
        Map localMap = (Map)paramAdapterView.getItemAtPosition(paramInt);
        QuickPayActivityActivity.a(this.b, (String)localMap.get("id"), (String)localMap.get("type"), (String)localMap.get("isinvoicerequest"));
      }
    }
    catch (Throwable localThrowable)
    {
      BehaviorAnalyticsAspect.a();
      BehaviorAnalyticsAspect.a(paramAdapterView, paramView, paramInt);
      throw localThrowable;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.hg
 * JD-Core Version:    0.6.2
 */