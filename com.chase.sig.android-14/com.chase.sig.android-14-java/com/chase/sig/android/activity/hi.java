package com.chase.sig.android.activity;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.domain.QuickPayRecipient;
import java.util.ArrayList;

final class hi
  implements AdapterView.OnItemClickListener
{
  hi(QuickPayChooseRecipientActivity paramQuickPayChooseRecipientActivity, ArrayList paramArrayList)
  {
  }

  public final void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    try
    {
      QuickPayRecipient localQuickPayRecipient = (QuickPayRecipient)this.a.get(paramInt);
      if (localQuickPayRecipient.m())
      {
        if (!this.b.getIntent().getBooleanExtra("quick_pay_manage_recipient", false))
          break label57;
        QuickPayChooseRecipientActivity.a(this.b, localQuickPayRecipient);
      }
      while (true)
      {
        BehaviorAnalyticsAspect.a();
        BehaviorAnalyticsAspect.a(paramAdapterView, paramView, paramInt);
        return;
        label57: if (this.b.getIntent().hasExtra("quick_pay_transaction"))
          this.b.a(this.b.getIntent(), localQuickPayRecipient);
      }
    }
    catch (Throwable localThrowable)
    {
      BehaviorAnalyticsAspect.a();
      BehaviorAnalyticsAspect.a(paramAdapterView, paramView, paramInt);
      throw localThrowable;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.hi
 * JD-Core Version:    0.6.2
 */