package com.chase.sig.android.activity;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.domain.Email;
import com.chase.sig.android.domain.QuickPayRecipient;
import com.chase.sig.android.util.s;
import com.google.common.a.e;
import java.util.Iterator;
import java.util.List;

final class hj
  implements View.OnClickListener
{
  hj(QuickPayChooseRecipientNotificatonActivity paramQuickPayChooseRecipientNotificatonActivity)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    int i;
    QuickPayRecipient localQuickPayRecipient;
    if ((QuickPayChooseRecipientNotificatonActivity.a(this.a) != null) || (s.m(QuickPayChooseRecipientNotificatonActivity.b(this.a))))
    {
      i = 1;
      if (i != 0)
      {
        QuickPayChooseRecipientNotificatonActivity.c(this.a).e(QuickPayChooseRecipientNotificatonActivity.b(this.a));
        String str = QuickPayChooseRecipientNotificatonActivity.a(this.a);
        localQuickPayRecipient = QuickPayChooseRecipientNotificatonActivity.c(this.a);
        Iterator localIterator = localQuickPayRecipient.j().iterator();
        while (localIterator.hasNext())
        {
          Email localEmail1 = (Email)localIterator.next();
          if (e.a(str).equals(localEmail1.a()))
          {
            Email localEmail2 = new Email(str, localEmail1.b(), localEmail1.c());
            localQuickPayRecipient.h();
            localQuickPayRecipient.a(localEmail2);
            label155: if (!QuickPayChooseRecipientNotificatonActivity.d(this.a))
              break label271;
          }
        }
      }
    }
    else
    {
      label271: for (Object localObject = QuickPayRequestMoneyActivity.class; ; localObject = QuickPaySendMoneyActivity.class)
      {
        Intent localIntent = new Intent(this.a, (Class)localObject);
        localIntent.putExtras(this.a.getIntent());
        localIntent.putExtra("recipient", QuickPayChooseRecipientNotificatonActivity.c(this.a));
        localIntent.putExtra("is_editing", this.a.getIntent().getBooleanExtra("is_editing", false));
        localIntent.setFlags(67108864);
        this.a.startActivity(localIntent);
        this.a.finish();
        return;
        i = 0;
        break;
        localQuickPayRecipient.h();
        break label155;
      }
    }
    this.a.g(2131165769);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.hj
 * JD-Core Version:    0.6.2
 */