package com.chase.sig.android.activity;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;

final class hq
  implements DialogInterface.OnClickListener
{
  hq(QuickPayEditRecipientActivity paramQuickPayEditRecipientActivity)
  {
  }

  public final void onClick(DialogInterface paramDialogInterface, int paramInt)
  {
    Intent localIntent = new Intent(this.a, QuickPayChooseRecipientActivity.class);
    localIntent.setFlags(67108864);
    localIntent.putExtra("quick_pay_manage_recipient", true);
    this.a.startActivity(localIntent);
    this.a.finish();
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.hq
 * JD-Core Version:    0.6.2
 */