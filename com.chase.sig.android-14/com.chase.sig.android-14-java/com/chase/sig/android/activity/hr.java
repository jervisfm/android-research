package com.chase.sig.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.chase.sig.android.d;
import com.chase.sig.android.domain.Email;
import com.chase.sig.android.domain.MobilePhoneNumber;
import com.chase.sig.android.domain.QuickPayRecipient;
import com.chase.sig.android.service.IServiceError;
import com.chase.sig.android.service.quickpay.QuickPayAddRecipientResponse;
import com.chase.sig.android.util.s;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public abstract class hr extends ai
  implements fk.e
{
  protected static final HashMap<String, Integer> d = new HashMap();
  protected int[] a = null;
  protected int[] b = null;
  protected int[] c = null;

  private static String a(String paramString, List<Email> paramList)
  {
    Iterator localIterator = paramList.iterator();
    while (localIterator.hasNext())
    {
      Email localEmail = (Email)localIterator.next();
      if (localEmail.a().equals(paramString))
        return localEmail.c();
    }
    return "0";
  }

  private String k(int paramInt)
  {
    return ((EditText)findViewById(paramInt)).getText().toString();
  }

  public void a(Bundle paramBundle)
  {
    d();
    e();
    for (int i = 0; i < -1 + this.b.length; i++)
      ((EditText)findViewById(this.b[i])).setOnKeyListener(new hu(this, this.a[(i + 1)]));
    int[] arrayOfInt = this.c;
    int j = 0;
    if (arrayOfInt != null)
      while (j < this.c.length)
      {
        findViewById(this.c[j]).setOnClickListener(new ht(this, j));
        j++;
      }
    Button localButton1 = (Button)findViewById(2131296761);
    Button localButton2 = (Button)findViewById(2131296760);
    if (!getIntent().hasExtra("quick_pay_manage_recipient"))
    {
      localButton2.setVisibility(8);
      localButton1.setText(2131165701);
    }
    localButton1.setOnClickListener(new hs(this));
    localButton2.setOnClickListener(C());
  }

  public final void a(QuickPayRecipient paramQuickPayRecipient)
  {
    if (getIntent().hasExtra("quick_pay_transaction"))
      a(getIntent(), paramQuickPayRecipient);
    while ((!getIntent().getBooleanExtra("quick_pay_manage_recipient", false)) && (!getIntent().getBooleanExtra("fromContactsActivity", false)))
      return;
    Intent localIntent = new Intent(this, QuickPayChooseRecipientActivity.class);
    localIntent.putExtra("quick_pay_manage_recipient", getIntent().getBooleanExtra("quick_pay_manage_recipient", false));
    localIntent.setFlags(67108864);
    startActivity(localIntent);
  }

  protected abstract void d();

  protected abstract void e();

  protected final QuickPayRecipient g()
  {
    QuickPayRecipient localQuickPayRecipient = (QuickPayRecipient)getIntent().getExtras().getSerializable("recipient");
    if (localQuickPayRecipient == null)
      localQuickPayRecipient = new QuickPayRecipient();
    localQuickPayRecipient.a(k(2131296738));
    String str1 = MobilePhoneNumber.d(k(2131296743));
    String str2;
    ArrayList localArrayList;
    int j;
    label106: String str3;
    if (localQuickPayRecipient.i() == null)
    {
      str2 = "0";
      localQuickPayRecipient.a(new MobilePhoneNumber(str1, str2, ""));
      localArrayList = new ArrayList(localQuickPayRecipient.j());
      localQuickPayRecipient.h();
      int[] arrayOfInt = this.b;
      int i = arrayOfInt.length;
      j = 0;
      if (j >= i)
        break label192;
      str3 = k(arrayOfInt[j]);
      if (s.o(str3))
        if (localQuickPayRecipient.e().size() != 0)
          break label186;
    }
    label186: for (boolean bool = true; ; bool = false)
    {
      localQuickPayRecipient.a(new Email(str3, bool, a(str3, localArrayList)));
      j++;
      break label106;
      str2 = localQuickPayRecipient.i().c();
      break;
    }
    label192: return localQuickPayRecipient;
  }

  protected void onResume()
  {
    super.onResume();
    for (int i = 0; i < -1 + this.b.length; i++)
      if (s.o(((TextView)findViewById(this.b[i])).getText().toString()))
        findViewById(this.a[(i + 1)]).setVisibility(0);
  }

  public static class a extends d<hr, Void, Void, QuickPayAddRecipientResponse>
  {
    private static boolean a(IServiceError paramIServiceError)
    {
      return (paramIServiceError.c() != null) && (hr.d.get(paramIServiceError.c().toLowerCase()) != null);
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.hr
 * JD-Core Version:    0.6.2
 */