package com.chase.sig.android.activity;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.util.s;

final class ht
  implements View.OnClickListener
{
  ht(hr paramhr, int paramInt)
  {
  }

  private void a(int paramInt)
  {
    this.b.findViewById(this.b.a[paramInt]).setVisibility(8);
    ((EditText)this.b.findViewById(this.b.b[paramInt])).setText("");
  }

  private boolean b(int paramInt)
  {
    return paramInt == -1 + this.b.a.length;
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    if (!b(this.a))
    {
      int i = this.a;
      if (this.b.findViewById(this.b.a[(i + 1)]).getVisibility() != 8)
        break label86;
    }
    label86: for (int j = 1; j != 0; j = 0)
    {
      ((EditText)this.b.findViewById(this.b.b[this.a])).setText("");
      return;
    }
    for (int k = this.a; k < -1 + this.b.b.length; k++)
      ((EditText)this.b.findViewById(this.b.b[k])).setText(((EditText)this.b.findViewById(this.b.b[(k + 1)])).getText());
    int m = this.a;
    label173: EditText localEditText;
    if (m < this.b.a.length)
    {
      if (8 == this.b.findViewById(this.b.a[m]).getVisibility())
      {
        a(m - 1);
        return;
      }
      if (b(m))
      {
        localEditText = (EditText)this.b.findViewById(this.b.b[m]);
        if (!s.n(localEditText.getText().toString()))
          break label277;
        a(m);
      }
    }
    while (true)
    {
      m++;
      break label173;
      break;
      label277: localEditText.setText("");
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.ht
 * JD-Core Version:    0.6.2
 */