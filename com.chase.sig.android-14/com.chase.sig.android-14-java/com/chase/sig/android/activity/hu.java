package com.chase.sig.android.activity;

import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.util.s;

final class hu
  implements View.OnKeyListener
{
  hu(hr paramhr, int paramInt)
  {
  }

  public final boolean onKey(View paramView, int paramInt, KeyEvent paramKeyEvent)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView, paramInt, paramKeyEvent);
    String str = ((TextView)paramView).getText().toString();
    LinearLayout localLinearLayout = (LinearLayout)this.b.findViewById(this.a);
    if (s.o(str))
      localLinearLayout.setVisibility(0);
    return false;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.hu
 * JD-Core Version:    0.6.2
 */