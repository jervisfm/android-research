package com.chase.sig.android.activity;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.domain.o;
import com.chase.sig.android.service.quickpay.TodoListResponse;

final class hw
  implements View.OnClickListener
{
  hw(QuickPayHomeActivity paramQuickPayHomeActivity)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    if (this.a.r().c.o())
    {
      Intent localIntent = new Intent(this.a, QuickPayRequestMoneyActivity.class);
      this.a.startActivity(localIntent);
      return;
    }
    this.a.showDialog(-11);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.hw
 * JD-Core Version:    0.6.2
 */