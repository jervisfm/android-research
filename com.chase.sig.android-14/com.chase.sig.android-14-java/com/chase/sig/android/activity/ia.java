package com.chase.sig.android.activity;

import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;

final class ia
  implements View.OnClickListener
{
  ia(QuickPayPendingTransactionsActivity paramQuickPayPendingTransactionsActivity, int paramInt)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    QuickPayPendingTransactionsActivity.a locala = (QuickPayPendingTransactionsActivity.a)this.b.e.a(QuickPayPendingTransactionsActivity.a.class);
    if (!locala.d())
    {
      Object[] arrayOfObject = new Object[2];
      arrayOfObject[0] = QuickPayPendingTransactionsActivity.c(this.b);
      arrayOfObject[1] = Integer.valueOf(this.a);
      locala.execute(arrayOfObject);
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.ia
 * JD-Core Version:    0.6.2
 */