package com.chase.sig.android.activity;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.domain.QuickPayPendingTransaction;
import java.util.Map;

final class ib
  implements AdapterView.OnItemClickListener
{
  ib(QuickPayPendingTransactionsActivity paramQuickPayPendingTransactionsActivity)
  {
  }

  public final void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    try
    {
      Map localMap = (Map)paramAdapterView.getItemAtPosition(paramInt);
      Intent localIntent = new Intent(this.a, QuickPayPendingTransactionsDetailActivity.class);
      localIntent.putExtra("sendTransaction", (QuickPayPendingTransaction)localMap.get("transaction"));
      this.a.startActivity(localIntent);
      BehaviorAnalyticsAspect.a();
      BehaviorAnalyticsAspect.a(paramAdapterView, paramView, paramInt);
      return;
    }
    catch (Throwable localThrowable)
    {
      BehaviorAnalyticsAspect.a();
      BehaviorAnalyticsAspect.a(paramAdapterView, paramView, paramInt);
      throw localThrowable;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.ib
 * JD-Core Version:    0.6.2
 */