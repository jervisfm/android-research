package com.chase.sig.android.activity;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;

final class ic
  implements View.OnClickListener
{
  ic(QuickPayPendingTransactionsDetailActivity paramQuickPayPendingTransactionsDetailActivity, boolean paramBoolean)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    if (this.a)
    {
      this.b.showDialog(5);
      return;
    }
    Intent localIntent = new Intent(this.b, QuickPaySendMoneyActivity.class);
    localIntent.putExtra("qp_edit_one_payment", true);
    localIntent.putExtra("is_editing", true);
    localIntent.putExtra("quick_pay_transaction", QuickPayPendingTransactionsDetailActivity.a(this.b));
    this.b.startActivity(localIntent);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.ic
 * JD-Core Version:    0.6.2
 */