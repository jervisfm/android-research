package com.chase.sig.android.activity;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;

final class ih
  implements DialogInterface.OnClickListener
{
  ih(QuickPayPendingTransactionsDetailActivity paramQuickPayPendingTransactionsDetailActivity)
  {
  }

  public final void onClick(DialogInterface paramDialogInterface, int paramInt)
  {
    Intent localIntent = new Intent(this.a, QuickPaySendMoneyActivity.class);
    localIntent.putExtra("quick_pay_transaction", QuickPayPendingTransactionsDetailActivity.a(this.a));
    localIntent.putExtra("qp_edit_one_payment", true);
    localIntent.putExtra("is_editing", true);
    this.a.startActivity(localIntent);
    paramDialogInterface.dismiss();
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.ih
 * JD-Core Version:    0.6.2
 */