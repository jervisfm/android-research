package com.chase.sig.android.activity;

import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.domain.QuickPayPendingTransaction;
import com.chase.sig.android.util.s;

final class ii
  implements View.OnClickListener
{
  ii(QuickPayPendingTransactionsDetailActivity paramQuickPayPendingTransactionsDetailActivity, boolean paramBoolean)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    if ((s.o(QuickPayPendingTransactionsDetailActivity.a(this.b).X())) && (this.a))
    {
      this.b.showDialog(1);
      return;
    }
    this.b.showDialog(0);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.ii
 * JD-Core Version:    0.6.2
 */