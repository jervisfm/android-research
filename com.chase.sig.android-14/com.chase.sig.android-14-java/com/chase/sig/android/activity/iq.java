package com.chase.sig.android.activity;

import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import com.chase.sig.android.domain.Email;
import com.chase.sig.android.domain.QuickPayRecipient;
import java.util.Iterator;
import java.util.List;

final class iq
  implements CompoundButton.OnCheckedChangeListener
{
  iq(QuickPayRecipientDetailsActivity paramQuickPayRecipientDetailsActivity)
  {
  }

  public final void onCheckedChanged(CompoundButton paramCompoundButton, boolean paramBoolean)
  {
    if (paramBoolean)
    {
      String str = QuickPayRecipientDetailsActivity.a(this.a).d();
      Iterator localIterator = QuickPayRecipientDetailsActivity.a(this.a).j().iterator();
      while (localIterator.hasNext())
      {
        Email localEmail = (Email)localIterator.next();
        localEmail.a(localEmail.a().equals(paramCompoundButton.getText()));
      }
      if (str.equals(QuickPayRecipientDetailsActivity.a(this.a).d()))
        break label135;
    }
    label135: for (int i = 1; ; i = 0)
    {
      QuickPayRecipientDetailsActivity.a locala = (QuickPayRecipientDetailsActivity.a)this.a.e.a(QuickPayRecipientDetailsActivity.a.class);
      if ((!locala.d()) && (i != 0))
        locala.execute(new Void[0]);
      return;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.iq
 * JD-Core Version:    0.6.2
 */