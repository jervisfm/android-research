package com.chase.sig.android.activity;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;

final class ir
  implements View.OnClickListener
{
  ir(QuickPayRecipientDetailsActivity paramQuickPayRecipientDetailsActivity)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    Intent localIntent = new Intent(this.a, QuickPayEditRecipientActivity.class);
    localIntent.putExtra("quick_pay_manage_recipient", true);
    localIntent.putExtra("recipient", QuickPayRecipientDetailsActivity.a(this.a));
    this.a.startActivity(localIntent);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.ir
 * JD-Core Version:    0.6.2
 */