package com.chase.sig.android.activity;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.domain.QuickPayRecipient;
import com.chase.sig.android.domain.o;
import com.chase.sig.android.service.quickpay.TodoListResponse;

final class is
  implements View.OnClickListener
{
  is(QuickPayRecipientDetailsActivity paramQuickPayRecipientDetailsActivity, boolean paramBoolean, QuickPayRecipient paramQuickPayRecipient)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    if ((this.a) && (!this.c.r().c.o()))
    {
      this.c.showDialog(-11);
      return;
    }
    if ((!this.a) && (!this.c.r().c.q()))
    {
      this.c.showDialog(-8);
      return;
    }
    Intent localIntent = this.c.getIntent();
    localIntent.putExtra("isRequestForMoney", this.a);
    this.c.a(localIntent, this.b);
    this.c.finish();
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.is
 * JD-Core Version:    0.6.2
 */