package com.chase.sig.android.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.activity.a.a;
import com.chase.sig.android.c;
import com.chase.sig.android.d;
import com.chase.sig.android.domain.Email;
import com.chase.sig.android.domain.QuickPayRecipient;
import com.chase.sig.android.domain.QuickPayTransaction;
import com.chase.sig.android.domain.m;
import com.chase.sig.android.domain.o;
import com.chase.sig.android.service.IServiceError;
import com.chase.sig.android.service.quickpay.QuickPayRecipientListResponse;
import com.chase.sig.android.service.quickpay.QuickPaySendTransactionVerifyResponse;
import com.chase.sig.android.service.quickpay.TodoListResponse;
import com.chase.sig.android.util.s;
import com.chase.sig.android.view.AmountView;
import com.chase.sig.android.view.JPSpinner;
import com.chase.sig.android.view.b;
import com.chase.sig.android.view.b.a;
import com.chase.sig.android.view.k;
import com.chase.sig.android.view.k.a;
import com.google.common.a.e;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public abstract class iy extends ai
  implements fk.e, ma
{
  private static final String a = null;
  private Button b;
  private Date c;
  private TextView d;
  protected QuickPayTransaction k;
  protected QuickPayRecipient l;
  protected m m;
  private Date n;
  private Object o;
  private QuickPaySendTransactionVerifyResponse p;
  private b.a q = new je(this);

  private Date G()
  {
    if (this.c == null)
      this.c = ((ChaseApplication)getApplication()).b().c.j();
    return this.c;
  }

  protected static void i()
  {
  }

  public final void D()
  {
    Intent localIntent1 = new Intent(getBaseContext(), PaymentsAndTransfersHomeActivity.class);
    localIntent1.setFlags(67108864);
    c.a(localIntent1);
    Intent localIntent2 = new Intent(this, QuickPaySendMoneyVerifyActivity.class);
    QuickPayTransaction localQuickPayTransaction = d();
    localQuickPayTransaction.a(this.p.o());
    localQuickPayTransaction.q(this.p.d());
    localQuickPayTransaction.a(this.p.l());
    localQuickPayTransaction.t(this.p.p());
    localQuickPayTransaction.g(this.p.s());
    localIntent2.putExtra("FOOTNOTE", this.p.n());
    localIntent2.putExtra("sendTransaction", localQuickPayTransaction);
    this.p = null;
    startActivity(localIntent2);
  }

  public final void E()
  {
    Iterator localIterator = this.p.g().iterator();
    while (localIterator.hasNext())
    {
      IServiceError localIServiceError = (IServiceError)localIterator.next();
      String str = this.p.o().d();
      k.a locala1 = new k.a(this);
      k.a locala2 = locala1.a(localIServiceError.a()).a(2131165288, new jg(this, str));
      locala2.i = true;
      locala2.g = new jf(this);
      locala1.d(-1).show();
    }
  }

  protected final void F()
  {
    a locala = (a)this.e.a(a.class);
    if (!locala.d())
    {
      String[] arrayOfString = new String[1];
      arrayOfString[0] = this.l.d();
      locala.execute(arrayOfString);
    }
  }

  public void a(Bundle paramBundle)
  {
    b(2130903182);
    this.b = ((Button)findViewById(2131296831));
    this.d = ((TextView)findViewById(2131296500));
    if (paramBundle == null)
      paramBundle = getIntent().getExtras();
    if (paramBundle != null)
    {
      this.k = ((QuickPayTransaction)paramBundle.getSerializable("quick_pay_transaction"));
      this.l = ((QuickPayRecipient)paramBundle.getSerializable("recipient"));
      this.n = ((Date)paramBundle.getSerializable("saved date"));
      this.o = paramBundle.getString(a);
      this.p = ((QuickPaySendTransactionVerifyResponse)paramBundle.getSerializable("quick pay verify response"));
    }
    if (this.k == null)
      this.k = new QuickPayTransaction();
    if (this.l == null)
      this.l = this.k.j();
    if (getIntent().hasExtra("qp_edit_one_payment"))
      this.k.d(false);
    if (!getIntent().getBooleanExtra("send_money_for_request", false))
      a(2131296819, new iz(this));
    this.b.setOnClickListener(i(0));
    ((Button)findViewById(2131296839)).setOnClickListener(new ja(this));
    ((Button)findViewById(2131296838)).setOnClickListener(new a(this));
    f();
    if ((this.p != null) && (this.p.r()))
      E();
  }

  protected QuickPayTransaction d()
  {
    QuickPayTransaction localQuickPayTransaction = new QuickPayTransaction();
    localQuickPayTransaction.g(this.k.w());
    localQuickPayTransaction.o(this.k.L());
    localQuickPayTransaction.m(this.k.J());
    localQuickPayTransaction.n(this.k.K());
    localQuickPayTransaction.a(this.l);
    localQuickPayTransaction.d(((TextView)findViewById(2131296500)).getText().toString());
    localQuickPayTransaction.b(j());
    localQuickPayTransaction.e(((TextView)findViewById(2131296837)).getText().toString());
    localQuickPayTransaction.e(this.k.R());
    localQuickPayTransaction.f(this.k.S());
    localQuickPayTransaction.u(this.k.W());
    return localQuickPayTransaction;
  }

  protected abstract boolean e();

  protected void f()
  {
    this.m = r().c.t();
    String str3;
    String str4;
    label52: String str5;
    label67: String str2;
    label157: Date localDate;
    label180: label187: Object localObject;
    if (h())
      if (this.l == null)
      {
        str3 = getString(2131165744);
        if (this.l.d() != null)
          break label255;
        str4 = "";
        if (this.l.f() != null)
          break label283;
        str5 = "";
        Spanned localSpanned = Html.fromHtml(str3 + str4 + str5);
        ((TextView)findViewById(2131296818)).setText(localSpanned);
        ((JPSpinner)findViewById(2131296833)).setAdapter(new ArrayAdapter(this, 2130903213, this.m.a()));
        if (this.k.r() != null)
          break label357;
        str2 = "";
        this.d.setText(str2);
        if (this.k.s() != null)
          break label368;
        localDate = G();
        if (this.n != null)
          break label379;
        this.b.setText(s.a(localDate));
        if (this.k.t() != null)
          break label387;
        localObject = "";
        label213: if (this.o != null)
          break label399;
      }
    while (true)
    {
      CharSequence localCharSequence = (CharSequence)localObject;
      ((TextView)findViewById(2131296837)).setText(localCharSequence);
      return;
      str3 = this.l.a();
      break;
      label255: str4 = "<br><font color=\"#696969\">" + this.l.d();
      break label52;
      label283: str5 = "<br><font color=\"#696969\">" + s.f(this.l.f());
      break label67;
      if (this.l == null);
      for (String str1 = getString(2131165744); ; str1 = this.l.a())
      {
        ((Button)findViewById(2131296819)).setText(str1);
        break;
      }
      label357: str2 = this.k.r();
      break label157;
      label368: localDate = this.k.s();
      break label180;
      label379: localDate = this.n;
      break label187;
      label387: localObject = this.k.t();
      break label213;
      label399: localObject = this.o;
    }
  }

  public boolean g()
  {
    y();
    if (!h())
      if (this.l == null)
      {
        bool = e(2131296817);
        if (!((Button)findViewById(2131296819)).getText().equals(getString(2131165744)));
      }
    for (boolean bool = e(2131296817); ; bool = true)
    {
      if (!((AmountView)findViewById(2131296500)).a())
        bool = e(2131296826);
      return bool;
      bool = true;
      break;
    }
  }

  public final boolean h()
  {
    return getIntent().getBooleanExtra("is_editing", false);
  }

  protected final Date j()
  {
    return s.e(((Button)findViewById(2131296831)).getText().toString());
  }

  public void k()
  {
    if ((getIntent().getBooleanExtra("is_editing", false)) || (getIntent().getBooleanExtra("send_money_for_request", false)))
    {
      this.k = ((QuickPayTransaction)getIntent().getSerializableExtra("quick_pay_transaction"));
      if (getIntent().hasExtra("qp_edit_one_payment"))
        this.k.d(false);
    }
    while (true)
    {
      this.n = this.k.s();
      this.o = this.k.t();
      this.l = this.k.j();
      y();
      f();
      return;
      this.k = new QuickPayTransaction();
    }
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    Dialog localDialog = super.onCreateDialog(paramInt);
    k.a locala = new k.a(this);
    switch (paramInt)
    {
    default:
      return localDialog;
    case 0:
      Calendar localCalendar1 = Calendar.getInstance();
      localCalendar1.add(5, 366);
      Calendar localCalendar2 = Calendar.getInstance();
      if ((this.k.s() != null) && (!h()));
      for (Date localDate = this.k.s(); ; localDate = G())
      {
        localCalendar2.setTime(localDate);
        return new b(this, this.q, false, false, localCalendar2, localCalendar1);
      }
    case 4:
    }
    locala.i = true;
    locala.b(2131165291, new jb(this));
    locala.a(2131165696, new jc(this));
    locala.g = new jd(this);
    locala.a(this.p.k());
    return locala.d(-1);
  }

  protected void onResume()
  {
    super.onResume();
    c.a();
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    paramBundle.putString(a, ((TextView)findViewById(2131296837)).getText().toString());
    paramBundle.putSerializable("saved date", j());
    paramBundle.putSerializable("recipient", this.l);
    paramBundle.putSerializable("quick_pay_transaction", this.k);
    paramBundle.putSerializable("quick pay verify response", this.p);
    super.onSaveInstanceState(paramBundle);
  }

  public static class a extends iy.b
  {
    protected final QuickPayRecipient a(QuickPayRecipientListResponse paramQuickPayRecipientListResponse)
    {
      Iterator localIterator1 = paramQuickPayRecipientListResponse.a().iterator();
      while (localIterator1.hasNext())
      {
        QuickPayRecipient localQuickPayRecipient = (QuickPayRecipient)localIterator1.next();
        List localList = localQuickPayRecipient.j();
        String str = this.a;
        Iterator localIterator2 = localList.iterator();
        Email localEmail;
        do
        {
          if (!localIterator2.hasNext())
            break;
          localEmail = (Email)localIterator2.next();
        }
        while (!e.a(str).equalsIgnoreCase(localEmail.a()));
        for (int i = 1; i != 0; i = 0)
          return localQuickPayRecipient;
      }
      return null;
    }

    protected final void b(QuickPayRecipientListResponse paramQuickPayRecipientListResponse)
    {
      if (paramQuickPayRecipientListResponse.e())
        ((iy)this.b).b(paramQuickPayRecipientListResponse.g());
      iy.c localc;
      do
      {
        return;
        ((iy)this.b).l = a(paramQuickPayRecipientListResponse);
        localc = (iy.c)((iy)this.b).e.a(iy.c.class);
      }
      while (localc.d());
      localc.execute(new Void[0]);
    }
  }

  public static class b extends d<iy, String, Void, QuickPayRecipientListResponse>
  {
    protected String a;

    protected QuickPayRecipient a(QuickPayRecipientListResponse paramQuickPayRecipientListResponse)
    {
      Iterator localIterator = paramQuickPayRecipientListResponse.a().iterator();
      while (localIterator.hasNext())
      {
        QuickPayRecipient localQuickPayRecipient = (QuickPayRecipient)localIterator.next();
        if (e.a(this.a).equalsIgnoreCase(localQuickPayRecipient.b()))
          return localQuickPayRecipient;
      }
      return null;
    }

    protected void b(QuickPayRecipientListResponse paramQuickPayRecipientListResponse)
    {
      if (paramQuickPayRecipientListResponse.e())
      {
        ((iy)this.b).b(paramQuickPayRecipientListResponse.g());
        return;
      }
      Intent localIntent = new Intent(this.b, QuickPayChooseRecipientNotificatonActivity.class);
      localIntent.putExtra("recipient", a(paramQuickPayRecipientListResponse));
      localIntent.putExtra("saved_recipient", ((iy)this.b).l);
      localIntent.putExtra("quick_pay_transaction", ((iy)this.b).d());
      localIntent.putExtra("is_editing", true);
      localIntent.putExtra("isRequestForMoney", ((iy)this.b).e());
      ((iy)this.b).startActivity(localIntent);
    }
  }

  public static class c extends iy.d
  {
    public final void a(QuickPaySendTransactionVerifyResponse paramQuickPaySendTransactionVerifyResponse)
    {
      this.a = true;
    }

    protected final void b(QuickPaySendTransactionVerifyResponse paramQuickPaySendTransactionVerifyResponse)
    {
      iy.a((iy)this.b, paramQuickPaySendTransactionVerifyResponse);
      super.b(paramQuickPaySendTransactionVerifyResponse);
    }
  }

  public static class d extends d<iy, Void, Void, QuickPaySendTransactionVerifyResponse>
  {
    protected boolean a;

    public void a(QuickPaySendTransactionVerifyResponse paramQuickPaySendTransactionVerifyResponse)
    {
      if (paramQuickPaySendTransactionVerifyResponse.r())
        ((iy)this.b).E();
    }

    protected void b(QuickPaySendTransactionVerifyResponse paramQuickPaySendTransactionVerifyResponse)
    {
      iy.a((iy)this.b, paramQuickPaySendTransactionVerifyResponse);
      if (paramQuickPaySendTransactionVerifyResponse.e())
      {
        if (paramQuickPaySendTransactionVerifyResponse.q())
        {
          iy localiy = (iy)this.b;
          Context localContext = ((iy)this.b).getBaseContext();
          Object[] arrayOfObject = new Object[2];
          arrayOfObject[0] = paramQuickPaySendTransactionVerifyResponse.o().a();
          arrayOfObject[1] = paramQuickPaySendTransactionVerifyResponse.o().d();
          localiy.f(s.a(localContext, 2131165775, arrayOfObject));
        }
        while (!this.a)
        {
          return;
          if (paramQuickPaySendTransactionVerifyResponse.r())
            a(paramQuickPaySendTransactionVerifyResponse);
          else
            ((iy)this.b).c(paramQuickPaySendTransactionVerifyResponse.g());
        }
      }
      if (paramQuickPaySendTransactionVerifyResponse.j())
      {
        ((iy)this.b).showDialog(4);
        return;
      }
      ((iy)this.b).D();
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.iy
 * JD-Core Version:    0.6.2
 */