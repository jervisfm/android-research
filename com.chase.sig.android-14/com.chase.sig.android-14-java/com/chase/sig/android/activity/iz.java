package com.chase.sig.android.activity;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;

final class iz
  implements View.OnClickListener
{
  iz(iy paramiy)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    if (this.a.h())
    {
      this.a.a(iy.b.class, new String[0]);
      return;
    }
    Intent localIntent = new Intent(this.a, QuickPayChooseRecipientActivity.class);
    localIntent.putExtra("quick_pay_transaction", this.a.d());
    localIntent.putExtra("isRequestForMoney", this.a.e());
    this.a.startActivity(localIntent);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.iz
 * JD-Core Version:    0.6.2
 */