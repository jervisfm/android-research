package com.chase.sig.android.activity;

import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.service.quickpay.TodoListResponse;

final class jh
  implements View.OnClickListener
{
  jh(QuickPayTodoListActivity paramQuickPayTodoListActivity)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    QuickPayTodoListActivity.b localb = (QuickPayTodoListActivity.b)this.a.e.a(QuickPayTodoListActivity.b.class);
    if (!localb.d())
    {
      Object[] arrayOfObject = new Object[1];
      arrayOfObject[0] = Integer.valueOf(this.a.d().k());
      localb.execute(arrayOfObject);
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.jh
 * JD-Core Version:    0.6.2
 */