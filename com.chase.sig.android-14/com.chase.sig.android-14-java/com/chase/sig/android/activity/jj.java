package com.chase.sig.android.activity;

import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.domain.QuickPayActivityItem;

final class jj
  implements View.OnClickListener
{
  jj(QuickPayViewDetailActivity paramQuickPayViewDetailActivity)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    QuickPayViewDetailActivity.b localb = (QuickPayViewDetailActivity.b)this.a.e.a(QuickPayViewDetailActivity.b.class);
    if (!localb.d())
    {
      QuickPayActivityItem[] arrayOfQuickPayActivityItem = new QuickPayActivityItem[1];
      arrayOfQuickPayActivityItem[0] = this.a.d;
      localb.execute(arrayOfQuickPayActivityItem);
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.jj
 * JD-Core Version:    0.6.2
 */