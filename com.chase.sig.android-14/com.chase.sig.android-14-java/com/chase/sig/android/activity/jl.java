package com.chase.sig.android.activity;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.domain.QuickPayDeclineTransaction;

final class jl
  implements View.OnClickListener
{
  jl(QuickPayViewTodoListDetailActivity paramQuickPayViewTodoListDetailActivity)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    QuickPayDeclineTransaction localQuickPayDeclineTransaction = new QuickPayDeclineTransaction();
    QuickPayViewTodoListDetailActivity.a(this.a.a, localQuickPayDeclineTransaction);
    Intent localIntent = new Intent(this.a, QuickPayDeclineVerifyActivity.class);
    localIntent.putExtra("quick_pay_transaction", localQuickPayDeclineTransaction);
    this.a.startActivity(localIntent);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.jl
 * JD-Core Version:    0.6.2
 */