package com.chase.sig.android.activity;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.domain.QuickPayRecipient;
import com.chase.sig.android.domain.QuickPayTransaction;
import com.chase.sig.android.domain.o;
import com.chase.sig.android.service.quickpay.TodoListResponse;

final class jm
  implements View.OnClickListener
{
  jm(QuickPayViewTodoListDetailActivity paramQuickPayViewTodoListDetailActivity, QuickPayRecipient paramQuickPayRecipient)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    if (this.b.r().c.q())
    {
      QuickPayTransaction localQuickPayTransaction = new QuickPayTransaction();
      QuickPayViewTodoListDetailActivity.a(this.b.a, localQuickPayTransaction);
      localQuickPayTransaction.e(true);
      Intent localIntent = new Intent(this.b, QuickPaySendMoneyActivity.class);
      localIntent.putExtra("quick_pay_transaction", localQuickPayTransaction);
      localIntent.putExtra("recipient", this.a);
      localIntent.putExtra("send_money_for_request", true);
      this.b.startActivity(localIntent);
      return;
    }
    this.b.showDialog(-8);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.jm
 * JD-Core Version:    0.6.2
 */