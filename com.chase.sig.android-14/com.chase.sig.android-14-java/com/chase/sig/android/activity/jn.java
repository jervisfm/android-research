package com.chase.sig.android.activity;

import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.domain.QuickPayAcceptMoneyTransaction;
import com.chase.sig.android.domain.o;
import com.chase.sig.android.service.quickpay.TodoListResponse;

final class jn
  implements View.OnClickListener
{
  jn(QuickPayViewTodoListDetailActivity paramQuickPayViewTodoListDetailActivity)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    if (this.a.r().c.r())
    {
      QuickPayAcceptMoneyTransaction localQuickPayAcceptMoneyTransaction = new QuickPayAcceptMoneyTransaction();
      QuickPayViewTodoListDetailActivity.a(this.a.a, localQuickPayAcceptMoneyTransaction);
      QuickPayViewTodoListDetailActivity.a locala = (QuickPayViewTodoListDetailActivity.a)this.a.e.a(QuickPayViewTodoListDetailActivity.a.class);
      if (!locala.d())
        locala.execute(new QuickPayAcceptMoneyTransaction[] { localQuickPayAcceptMoneyTransaction });
      return;
    }
    this.a.showDialog(-9);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.jn
 * JD-Core Version:    0.6.2
 */