package com.chase.sig.android.activity;

import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;

final class jr
  implements View.OnTouchListener
{
  jr(QuoteChartsActivity paramQuoteChartsActivity, GestureDetector paramGestureDetector)
  {
  }

  public final boolean onTouch(View paramView, MotionEvent paramMotionEvent)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView, paramMotionEvent);
    return this.a.onTouchEvent(paramMotionEvent);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.jr
 * JD-Core Version:    0.6.2
 */