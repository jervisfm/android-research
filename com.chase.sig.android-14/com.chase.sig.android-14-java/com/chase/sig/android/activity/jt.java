package com.chase.sig.android.activity;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.domain.Quote;

final class jt
  implements View.OnClickListener
{
  jt(QuoteDetailsActivity paramQuoteDetailsActivity)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    Intent localIntent = new Intent(this.a, QuoteChartsActivity.class);
    localIntent.putExtra("exchangeName", QuoteDetailsActivity.b(this.a).l());
    localIntent.putExtra("transaction_object", QuoteDetailsActivity.b(this.a));
    this.a.startActivityForResult(localIntent, 35);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.jt
 * JD-Core Version:    0.6.2
 */