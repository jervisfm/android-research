package com.chase.sig.android.activity;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.domain.Receipt;
import com.chase.sig.android.domain.ReceiptPhotoList;
import com.chase.sig.android.domain.ReceiptPhotoList.ReceiptPhoto;
import java.util.Iterator;
import java.util.List;

final class jw
  implements View.OnClickListener
{
  jw(ReceiptDetailActivity paramReceiptDetailActivity)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    List localList = ReceiptDetailActivity.a(this.a).h().a();
    ReceiptPhotoList localReceiptPhotoList = new ReceiptPhotoList();
    Iterator localIterator = localList.iterator();
    while (localIterator.hasNext())
      localReceiptPhotoList.a(((ReceiptPhotoList.ReceiptPhoto)localIterator.next()).a());
    Intent localIntent = new Intent(this.a, ReceiptPhotosViewActivity.class);
    localIntent.putExtra("receipt_photo_list", localReceiptPhotoList);
    this.a.startActivity(localIntent);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.jw
 * JD-Core Version:    0.6.2
 */