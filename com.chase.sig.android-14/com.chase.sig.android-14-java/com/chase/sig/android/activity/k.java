package com.chase.sig.android.activity;

import com.chase.sig.android.domain.Transaction;
import com.chase.sig.android.service.movemoney.RequestFlags;
import com.chase.sig.android.service.movemoney.ServiceResponse;

public abstract class k<T extends Transaction> extends n<T>
{
  public final boolean b()
  {
    return (f().l()) || (!g().b());
  }

  protected abstract com.chase.sig.android.service.movemoney.d<T> c();

  protected abstract Class<? extends eb> d();

  public static class a<T extends Transaction> extends com.chase.sig.android.d<k<T>, Void, Void, ServiceResponse>
  {
    private ServiceResponse a()
    {
      try
      {
        Transaction localTransaction = ((k)this.b).f();
        ServiceResponse localServiceResponse = ((k)this.b).c().a(localTransaction, ((k)this.b).g());
        ((k)this.b).l();
        return localServiceResponse;
      }
      catch (Exception localException)
      {
      }
      return null;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.k
 * JD-Core Version:    0.6.2
 */