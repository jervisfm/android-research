package com.chase.sig.android.activity;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;

final class ka
  implements View.OnClickListener
{
  ka(ReceiptDetailActivity paramReceiptDetailActivity)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    Intent localIntent = new Intent(this.a, ReceiptsEnterDetailsActivity.class);
    localIntent.putExtra("receipt", ReceiptDetailActivity.a(this.a));
    localIntent.putExtra("is_browsing_receipts", ReceiptDetailActivity.b(this.a));
    this.a.startActivity(localIntent);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.ka
 * JD-Core Version:    0.6.2
 */