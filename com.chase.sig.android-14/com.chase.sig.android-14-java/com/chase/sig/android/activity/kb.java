package com.chase.sig.android.activity;

import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import com.chase.sig.android.domain.ReceiptPhotoList.ReceiptPhoto;

final class kb
  implements AdapterView.OnItemSelectedListener
{
  kb(ReceiptPhotosViewActivity paramReceiptPhotosViewActivity)
  {
  }

  public final void onItemSelected(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    ReceiptPhotoList.ReceiptPhoto localReceiptPhoto = (ReceiptPhotoList.ReceiptPhoto)paramAdapterView.getItemAtPosition(paramInt);
    this.a.k = localReceiptPhoto;
    ReceiptPhotosViewActivity.a(this.a);
  }

  public final void onNothingSelected(AdapterView<?> paramAdapterView)
  {
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.kb
 * JD-Core Version:    0.6.2
 */