package com.chase.sig.android.activity;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.domain.ReceiptAccount;

final class kc
  implements View.OnClickListener
{
  kc(ReceiptsBrowseActivity paramReceiptsBrowseActivity, String paramString, ReceiptAccount paramReceiptAccount)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    Intent localIntent = new Intent(this.c, ReceiptsListActivity.class);
    localIntent.putExtra("header_text", this.a);
    localIntent.putExtra("account_id", this.b.c());
    localIntent.putExtra("filter_set", ReceiptsBrowseActivity.a(this.c));
    this.c.startActivity(localIntent);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.kc
 * JD-Core Version:    0.6.2
 */