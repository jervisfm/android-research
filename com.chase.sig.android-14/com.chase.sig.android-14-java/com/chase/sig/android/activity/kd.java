package com.chase.sig.android.activity;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;

final class kd
  implements View.OnClickListener
{
  kd(ReceiptsCameraCaptureActivity paramReceiptsCameraCaptureActivity, Button paramButton)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    View localView = this.b.findViewById(2131296317);
    if (localView.getVisibility() == 0)
    {
      this.a.setText(this.b.getString(2131165299));
      ReceiptsCameraCaptureActivity.a(this.b);
      localView.setVisibility(4);
      ReceiptsCameraCaptureActivity.b(this.b).setClickable(true);
      return;
    }
    this.a.setText(this.b.getString(2131165292));
    ((TextView)this.b.findViewById(2131296308)).setText(2131165818);
    localView.setVisibility(0);
    ReceiptsCameraCaptureActivity.b(this.b).setClickable(false);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.kd
 * JD-Core Version:    0.6.2
 */