package com.chase.sig.android.activity;

import android.hardware.Camera;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;

final class ke
  implements View.OnClickListener
{
  ke(ReceiptsCameraCaptureActivity paramReceiptsCameraCaptureActivity, CapturePreview paramCapturePreview)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    ReceiptsCameraCaptureActivity.b(this.b).setEnabled(false);
    ReceiptsCameraCaptureActivity.c(this.b).setVisibility(0);
    ReceiptsCameraCaptureActivity.c(this.b).requestLayout();
    this.a.b.autoFocus(new kf(this));
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.ke
 * JD-Core Version:    0.6.2
 */