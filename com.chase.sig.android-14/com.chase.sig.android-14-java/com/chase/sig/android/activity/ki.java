package com.chase.sig.android.activity;

import android.content.Intent;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.os.Bundle;
import com.chase.sig.android.domain.Receipt;

final class ki
  implements Camera.PictureCallback
{
  ki(ReceiptsCameraCaptureActivity paramReceiptsCameraCaptureActivity)
  {
  }

  public final void onPictureTaken(byte[] paramArrayOfByte, Camera paramCamera)
  {
    Intent localIntent = new Intent(this.a, ReceiptsReviewImageActivity.class);
    Bundle localBundle = this.a.getIntent().getExtras();
    if (localBundle != null)
    {
      if (localBundle.containsKey("receipt_curr_photo_number"))
        localIntent.putExtra("receipt_curr_photo_number", localBundle.getInt("receipt_curr_photo_number"));
      if (localBundle.getSerializable("receipt_photo_list") != null)
        localIntent.putExtra("receipt_photo_list", localBundle.getSerializable("receipt_photo_list"));
    }
    Receipt localReceipt = (Receipt)this.a.getIntent().getSerializableExtra("receipt");
    if (localReceipt != null)
    {
      boolean bool = this.a.getIntent().getBooleanExtra("is_browsing_receipts", false);
      localIntent.putExtra("receipt", localReceipt);
      localIntent.putExtra("is_browsing_receipts", bool);
    }
    localIntent.setFlags(1073741824);
    localIntent.putExtra("image_data", ReceiptsCameraCaptureActivity.a(paramArrayOfByte));
    this.a.startActivity(localIntent);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.ki
 * JD-Core Version:    0.6.2
 */