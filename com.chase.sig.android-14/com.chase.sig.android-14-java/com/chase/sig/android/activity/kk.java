package com.chase.sig.android.activity;

import android.os.AsyncTask.Status;
import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;

final class kk
  implements View.OnClickListener
{
  kk(ReceiptsCancelActivity paramReceiptsCancelActivity)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    ReceiptsCancelActivity.a locala = (ReceiptsCancelActivity.a)this.a.e.a(ReceiptsCancelActivity.a.class);
    if (locala.getStatus() != AsyncTask.Status.RUNNING)
      locala.execute(new Void[0]);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.kk
 * JD-Core Version:    0.6.2
 */