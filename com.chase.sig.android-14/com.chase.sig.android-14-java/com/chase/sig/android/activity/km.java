package com.chase.sig.android.activity;

import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;

final class km
  implements DialogInterface.OnCancelListener
{
  km(ReceiptsCancelActivity paramReceiptsCancelActivity)
  {
  }

  public final void onCancel(DialogInterface paramDialogInterface)
  {
    Intent localIntent = new Intent(this.a, AccountsActivity.class);
    this.a.startActivity(localIntent);
    this.a.finish();
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.km
 * JD-Core Version:    0.6.2
 */