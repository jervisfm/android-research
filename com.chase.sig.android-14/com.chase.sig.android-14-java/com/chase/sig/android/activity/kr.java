package com.chase.sig.android.activity;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.domain.Receipt;

final class kr
  implements View.OnClickListener
{
  kr(ReceiptsEnterDetailsActivity paramReceiptsEnterDetailsActivity)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    if (ReceiptsEnterDetailsActivity.b(this.a))
    {
      if ((ReceiptsEnterDetailsActivity.a(this.a) != null) && (ReceiptsEnterDetailsActivity.a(this.a).j() != null))
        break label129;
      if (ReceiptsEnterDetailsActivity.c(this.a))
      {
        ReceiptsEnterDetailsActivity localReceiptsEnterDetailsActivity1 = this.a;
        Receipt[] arrayOfReceipt1 = new Receipt[1];
        arrayOfReceipt1[0] = ReceiptsEnterDetailsActivity.d(this.a);
        localReceiptsEnterDetailsActivity1.a(ReceiptsEnterDetailsActivity.a.class, arrayOfReceipt1);
      }
    }
    else
    {
      return;
    }
    Intent localIntent = new Intent(this.a, ReceiptsTransactionMatchingActivity.class);
    localIntent.putExtra("receipt", ReceiptsEnterDetailsActivity.d(this.a));
    localIntent.putExtra("is_editing", false);
    this.a.startActivity(localIntent);
    return;
    label129: ReceiptsEnterDetailsActivity localReceiptsEnterDetailsActivity2 = this.a;
    Receipt[] arrayOfReceipt2 = new Receipt[1];
    arrayOfReceipt2[0] = ReceiptsEnterDetailsActivity.d(this.a);
    localReceiptsEnterDetailsActivity2.a(ReceiptsEnterDetailsActivity.c.class, arrayOfReceipt2);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.kr
 * JD-Core Version:    0.6.2
 */