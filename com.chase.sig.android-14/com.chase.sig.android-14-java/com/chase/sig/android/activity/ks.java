package com.chase.sig.android.activity;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.c;

final class ks
  implements View.OnClickListener
{
  ks(ReceiptsHomeActivity paramReceiptsHomeActivity)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    Intent localIntent1 = new Intent(this.a, ReceiptsCameraCaptureActivity.class);
    localIntent1.setFlags(1073741824);
    this.a.startActivity(localIntent1);
    Intent localIntent2 = new Intent(this.a, ReceiptsHomeActivity.class);
    localIntent2.putExtra("receipt_cancel_warning", 2131165816);
    c.a(localIntent2);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.ks
 * JD-Core Version:    0.6.2
 */