package com.chase.sig.android.activity;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;

final class ku
  implements View.OnClickListener
{
  ku(ReceiptsHomeActivity paramReceiptsHomeActivity)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    Intent localIntent = new Intent(this.a, ReceiptsBrowseActivity.class);
    if (ReceiptsHomeActivity.a(this.a) != null)
      localIntent.putExtra("acctSummary", ReceiptsHomeActivity.a(this.a));
    this.a.startActivity(localIntent);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.ku
 * JD-Core Version:    0.6.2
 */