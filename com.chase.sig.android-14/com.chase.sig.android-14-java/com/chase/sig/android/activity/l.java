package com.chase.sig.android.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.domain.LabeledValue;
import com.chase.sig.android.domain.Transaction;
import com.chase.sig.android.service.movemoney.ListServiceResponse;
import com.chase.sig.android.view.h;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public abstract class l<T extends Transaction> extends ai
  implements fk.e
{
  protected List<T> a = new ArrayList();
  protected ArrayList<LabeledValue> b;
  protected int c;
  protected int d;
  private ListView k;
  private TextView l;
  private View m;

  protected abstract int a();

  public void a(Bundle paramBundle)
  {
    b(2130903240);
    setTitle(a());
    this.k = ((ListView)findViewById(2131296981));
    this.l = ((TextView)findViewById(2131296261));
  }

  protected void a(T paramT)
  {
    Intent localIntent = new Intent(getBaseContext(), c());
    localIntent.putExtra("transaction_object", paramT);
    localIntent.putExtra("frequency_list", this.b);
    startActivity(localIntent);
  }

  protected final void a(List<T> paramList, int paramInt1, int paramInt2)
  {
    int i = 8;
    this.a.addAll(paramList);
    this.c = paramInt1;
    this.d = paramInt2;
    int j;
    label88: TextView localTextView;
    if (paramInt1 != this.d)
    {
      if (this.m == null)
      {
        this.m = ((LayoutInflater)getBaseContext().getSystemService("layout_inflater")).inflate(2130903137, null);
        this.k.addFooterView(this.m);
      }
      if (this.a.isEmpty())
        break label213;
      j = 1;
      localTextView = this.l;
      if (j == 0)
        break label219;
    }
    label213: label219: for (int n = i; ; n = 0)
    {
      localTextView.setVisibility(n);
      ListView localListView = this.k;
      if (j != 0)
        i = 0;
      localListView.setVisibility(i);
      this.k.setOnItemClickListener(new b((byte)0));
      this.k.setAdapter(new h(this, this.a));
      this.k.setSelectionFromTop(this.a.size() - paramList.size(), 0);
      return;
      if (this.m == null)
        break;
      this.k.removeFooterView(this.m);
      break;
      j = 0;
      break label88;
    }
  }

  protected abstract Class<? extends a<T>> b();

  protected final void b(Bundle paramBundle)
  {
    if ((paramBundle != null) && (paramBundle.containsKey("payment_items")))
    {
      c(paramBundle);
      return;
    }
    Class localClass = b();
    Integer[] arrayOfInteger = new Integer[1];
    arrayOfInteger[0] = Integer.valueOf(1);
    a(localClass, arrayOfInteger);
  }

  protected abstract Class<? extends eb> c();

  protected final void c(Bundle paramBundle)
  {
    this.a.clear();
    List localList = (List)paramBundle.getSerializable("payment_items");
    if (!((a)this.e.a(b())).d())
      a(localList, paramBundle.getInt("payment_current_page"), paramBundle.getInt("payment_total_page"));
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    paramBundle.putSerializable("payment_items", (Serializable)this.a);
    paramBundle.putInt("payment_current_page", this.c);
    paramBundle.putInt("payment_total_page", this.d);
    paramBundle.putSerializable("frequency_list", this.b);
  }

  public static abstract class a<T extends Transaction> extends com.chase.sig.android.d<l<T>, Integer, Void, ListServiceResponse<T>>
  {
    protected ListServiceResponse<T> a(Integer[] paramArrayOfInteger)
    {
      return a().c(paramArrayOfInteger[0].intValue());
    }

    protected abstract com.chase.sig.android.service.movemoney.d<T> a();

    protected void a(ListServiceResponse<T> paramListServiceResponse)
    {
      if (paramListServiceResponse.e())
      {
        ((l)this.b).b(paramListServiceResponse.g());
        return;
      }
      ((l)this.b).b = ((ArrayList)paramListServiceResponse.d());
      ((l)this.b).a(paramListServiceResponse.a(), paramListServiceResponse.b(), paramListServiceResponse.c());
    }
  }

  private final class b
    implements AdapterView.OnItemClickListener
  {
    private b()
    {
    }

    public final void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
    {
      int i = 1;
      try
      {
        if (paramAdapterView.getItemAtPosition(paramInt) == null)
        {
          if (i == 0)
            break label79;
          l locall = l.this;
          Class localClass = l.this.b();
          Integer[] arrayOfInteger = new Integer[1];
          arrayOfInteger[0] = Integer.valueOf(1 + l.this.c);
          locall.a(localClass, arrayOfInteger);
        }
        while (true)
        {
          BehaviorAnalyticsAspect.a();
          BehaviorAnalyticsAspect.a(paramAdapterView, paramView, paramInt);
          return;
          i = 0;
          break;
          label79: l.this.a((Transaction)paramAdapterView.getItemAtPosition(paramInt));
        }
      }
      catch (Throwable localThrowable)
      {
        BehaviorAnalyticsAspect.a();
        BehaviorAnalyticsAspect.a(paramAdapterView, paramView, paramInt);
        throw localThrowable;
      }
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.l
 * JD-Core Version:    0.6.2
 */