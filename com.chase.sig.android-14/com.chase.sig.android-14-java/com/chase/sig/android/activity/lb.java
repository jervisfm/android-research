package com.chase.sig.android.activity;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import com.chase.sig.android.domain.receipt.ReceiptFilterList;
import com.chase.sig.android.service.ReceiptFilter;
import java.util.ArrayList;

final class lb
  implements DialogInterface.OnClickListener
{
  lb(ReceiptsListActivity paramReceiptsListActivity, String[] paramArrayOfString)
  {
  }

  public final void onClick(DialogInterface paramDialogInterface, int paramInt)
  {
    ReceiptsListActivity.a(this.b, false);
    ReceiptsListActivity.a(this.b, paramInt, this.a);
    if ("ALL".equals(ReceiptsListActivity.g(this.b).c()))
    {
      ReceiptsListActivity.c(this.b).b();
      ReceiptsListActivity.c(this.b).a("ALL");
      ReceiptsListActivity.d(this.b).clear();
      ReceiptsListActivity.d(this.b).add("No filter");
      ReceiptsListActivity.f(this.b);
      ReceiptsListActivity localReceiptsListActivity = this.b;
      ReceiptFilter[] arrayOfReceiptFilter = new ReceiptFilter[1];
      arrayOfReceiptFilter[0] = ReceiptsListActivity.c(this.b);
      localReceiptsListActivity.a(ReceiptsListActivity.b.class, arrayOfReceiptFilter);
      paramDialogInterface.dismiss();
      this.b.removeDialog(0);
      return;
    }
    this.b.showDialog(1);
    paramDialogInterface.dismiss();
    this.b.removeDialog(0);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.lb
 * JD-Core Version:    0.6.2
 */