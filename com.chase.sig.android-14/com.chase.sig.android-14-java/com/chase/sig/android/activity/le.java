package com.chase.sig.android.activity;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.widget.Button;
import com.chase.sig.android.domain.LabeledValue;
import com.chase.sig.android.util.s;
import java.util.List;

final class le
  implements DialogInterface.OnClickListener
{
  le(ReceiptsListActivity paramReceiptsListActivity, int paramInt, List paramList)
  {
  }

  public final void onClick(DialogInterface paramDialogInterface, int paramInt)
  {
    if (paramInt == this.a)
    {
      this.c.showDialog(3);
      return;
    }
    if (s.n(((LabeledValue)this.b.get(paramInt)).b()))
    {
      ((Button)this.c.findViewById(2131296499)).setText("Dates: ");
      ReceiptsListActivity.a(this.c, null, null, null);
      this.c.removeDialog(3);
      return;
    }
    ((Button)this.c.findViewById(2131296499)).setText("Dates: " + ((LabeledValue)this.b.get(paramInt)).toString());
    ReceiptsListActivity.a(this.c, ((LabeledValue)this.b.get(paramInt)).b(), null, null);
    this.c.removeDialog(3);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.le
 * JD-Core Version:    0.6.2
 */