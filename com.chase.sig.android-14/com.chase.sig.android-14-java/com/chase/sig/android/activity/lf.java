package com.chase.sig.android.activity;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.view.View;
import android.widget.Button;
import com.chase.sig.android.util.s;
import java.util.Date;

final class lf
  implements DialogInterface.OnClickListener
{
  lf(ReceiptsListActivity paramReceiptsListActivity, View paramView)
  {
  }

  public final void onClick(DialogInterface paramDialogInterface, int paramInt)
  {
    Button localButton1 = (Button)this.a.findViewById(2131296405);
    Button localButton2 = (Button)this.a.findViewById(2131296406);
    Date localDate1 = s.e(localButton1.getText().toString());
    Date localDate2 = s.e(localButton2.getText().toString());
    if ((localDate1 != null) && (localDate2 != null))
      ReceiptsListActivity.a(this.b, null, localDate1, localDate2);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.lf
 * JD-Core Version:    0.6.2
 */