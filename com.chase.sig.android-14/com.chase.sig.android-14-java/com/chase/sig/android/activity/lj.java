package com.chase.sig.android.activity;

import android.content.DialogInterface;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import com.chase.sig.android.service.ReceiptFilter;
import java.util.ArrayList;

final class lj
  implements DialogInterface.OnMultiChoiceClickListener
{
  lj(ReceiptsListActivity paramReceiptsListActivity)
  {
  }

  public final void onClick(DialogInterface paramDialogInterface, int paramInt, boolean paramBoolean)
  {
    if (paramBoolean)
    {
      ReceiptsListActivity.c(this.a).b("ALL");
      ReceiptsListActivity.d(this.a).remove("No filter");
      ReceiptsListActivity.a(this.a, paramInt);
      return;
    }
    ReceiptsListActivity.b(this.a, paramInt);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.lj
 * JD-Core Version:    0.6.2
 */