package com.chase.sig.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.domain.ReceiptPhotoList;

final class ln
  implements View.OnClickListener
{
  ln(ReceiptsReviewImageActivity paramReceiptsReviewImageActivity, Bundle paramBundle)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    Intent localIntent = new Intent(this.b, ReceiptsCameraCaptureActivity.class);
    localIntent.putExtra("receipt_photo_list", ReceiptPhotoList.a(this.b.getIntent()));
    if ((this.a != null) && (this.a.containsKey("receipt_curr_photo_number")))
      localIntent.putExtra("receipt_curr_photo_number", this.a.getInt("receipt_curr_photo_number"));
    ReceiptsReviewImageActivity.a(this.b, localIntent);
    localIntent.addFlags(1073741824);
    this.b.startActivity(localIntent);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.ln
 * JD-Core Version:    0.6.2
 */