package com.chase.sig.android.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.domain.ReceiptPhotoList;

final class lp
  implements View.OnClickListener
{
  lp(ReceiptsReviewImageActivity paramReceiptsReviewImageActivity, Bundle paramBundle)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    Intent localIntent = new Intent(this.b, ReceiptsEnterDetailsActivity.class);
    ReceiptPhotoList localReceiptPhotoList = ReceiptPhotoList.a(this.b.getIntent());
    if ((this.a != null) && (this.a.containsKey("image_data")))
      localReceiptPhotoList.a(this.a.getByteArray("image_data"));
    ReceiptsReviewImageActivity.a(this.b, localIntent);
    localIntent.putExtra("receipt_photo_list", localReceiptPhotoList);
    localIntent.putExtra("selectedAccountId", this.a.getString("selectedAccountId"));
    this.b.startActivity(localIntent);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.lp
 * JD-Core Version:    0.6.2
 */