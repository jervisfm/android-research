package com.chase.sig.android.activity;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;

final class lt
  implements View.OnClickListener
{
  lt(ReceiptsSettingsActivity paramReceiptsSettingsActivity)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    Intent localIntent;
    if (ReceiptsSettingsActivity.a(this.a))
    {
      localIntent = new Intent(this.a, ReceiptsSelectPlanActivity.class);
      localIntent.putExtra("receipts_settings_mode", true);
    }
    while (true)
    {
      localIntent.addFlags(67108864);
      this.a.startActivity(localIntent);
      return;
      localIntent = new Intent(this.a, ReceiptsEnrollActivity.class);
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.lt
 * JD-Core Version:    0.6.2
 */