package com.chase.sig.android.activity;

import android.os.AsyncTask.Status;
import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.domain.ReceiptsPricingPlan;
import com.chase.sig.android.view.JPSpinner;

final class lu
  implements View.OnClickListener
{
  lu(ReceiptsSettingsActivity paramReceiptsSettingsActivity)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    JPSpinner localJPSpinner = ReceiptsSettingsActivity.b(this.a);
    if ((this.a.a(localJPSpinner)) || (ReceiptsSettingsActivity.c(this.a).d().equals("CRPLAN0")))
    {
      ReceiptsSettingsActivity.a locala = (ReceiptsSettingsActivity.a)this.a.e.a(ReceiptsSettingsActivity.a.class);
      if (locala.getStatus() != AsyncTask.Status.RUNNING)
        locala.execute(new String[0]);
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.lu
 * JD-Core Version:    0.6.2
 */