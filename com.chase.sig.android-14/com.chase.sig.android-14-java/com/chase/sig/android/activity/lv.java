package com.chase.sig.android.activity;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.domain.ReceiptsPricingPlan;
import com.chase.sig.android.view.JPSpinner;
import java.util.HashMap;

final class lv
  implements View.OnClickListener
{
  lv(ReceiptsSettingsActivity paramReceiptsSettingsActivity)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    JPSpinner localJPSpinner = ReceiptsSettingsActivity.b(this.a);
    HashMap localHashMap;
    if (this.a.a(localJPSpinner))
    {
      localHashMap = (HashMap)localJPSpinner.getSelectedItem();
      if (localHashMap != null);
    }
    else
    {
      return;
    }
    String str = (String)localHashMap.get("accountId");
    Intent localIntent = new Intent(this.a, ReceiptsLegalAgreementActivity.class);
    localIntent.putExtra("plan_code_id", ReceiptsSettingsActivity.c(this.a).d());
    localIntent.putExtra("funding_acct_id", str);
    this.a.startActivity(localIntent);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.lv
 * JD-Core Version:    0.6.2
 */