package com.chase.sig.android.activity;

import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.domain.Receipt;

final class ly
  implements View.OnClickListener
{
  ly(ReceiptsTransactionMatchingActivity paramReceiptsTransactionMatchingActivity)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    if (ReceiptsTransactionMatchingActivity.a(this.a))
    {
      ReceiptsTransactionMatchingActivity localReceiptsTransactionMatchingActivity2 = this.a;
      Receipt[] arrayOfReceipt2 = new Receipt[1];
      arrayOfReceipt2[0] = ReceiptsTransactionMatchingActivity.b(this.a);
      localReceiptsTransactionMatchingActivity2.a(ReceiptsTransactionMatchingActivity.c.class, arrayOfReceipt2);
      return;
    }
    ReceiptsTransactionMatchingActivity localReceiptsTransactionMatchingActivity1 = this.a;
    Receipt[] arrayOfReceipt1 = new Receipt[1];
    arrayOfReceipt1[0] = ReceiptsTransactionMatchingActivity.b(this.a);
    localReceiptsTransactionMatchingActivity1.a(ReceiptsTransactionMatchingActivity.a.class, arrayOfReceipt1);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.ly
 * JD-Core Version:    0.6.2
 */