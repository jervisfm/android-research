package com.chase.sig.android.activity;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.domain.Receipt;
import com.chase.sig.android.domain.TransactionForReceipt;

final class lz
  implements View.OnClickListener
{
  lz(ReceiptsTransactionMatchingActivity paramReceiptsTransactionMatchingActivity)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    int i = ReceiptsTransactionMatchingActivity.c(this.a).getCheckedItemPosition();
    if (i < 0)
    {
      this.a.g(2131165813);
      return;
    }
    TransactionForReceipt localTransactionForReceipt = (TransactionForReceipt)ReceiptsTransactionMatchingActivity.c(this.a).getAdapter().getItem(i);
    ReceiptsTransactionMatchingActivity.b(this.a).d(localTransactionForReceipt.a());
    if (ReceiptsTransactionMatchingActivity.a(this.a))
    {
      ReceiptsTransactionMatchingActivity localReceiptsTransactionMatchingActivity2 = this.a;
      Receipt[] arrayOfReceipt2 = new Receipt[1];
      arrayOfReceipt2[0] = ReceiptsTransactionMatchingActivity.b(this.a);
      localReceiptsTransactionMatchingActivity2.a(ReceiptsTransactionMatchingActivity.c.class, arrayOfReceipt2);
      return;
    }
    ReceiptsTransactionMatchingActivity localReceiptsTransactionMatchingActivity1 = this.a;
    Receipt[] arrayOfReceipt1 = new Receipt[1];
    arrayOfReceipt1[0] = ReceiptsTransactionMatchingActivity.b(this.a);
    localReceiptsTransactionMatchingActivity1.a(ReceiptsTransactionMatchingActivity.a.class, arrayOfReceipt1);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.lz
 * JD-Core Version:    0.6.2
 */