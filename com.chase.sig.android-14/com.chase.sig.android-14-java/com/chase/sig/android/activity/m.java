package com.chase.sig.android.activity;

import android.content.Intent;
import android.view.KeyEvent;
import android.widget.Button;
import com.chase.sig.android.a.a;
import com.chase.sig.android.domain.Transaction;

public abstract class m<T extends Transaction> extends n<T>
{
  protected final void a(int paramInt, Class<? extends eb> paramClass)
  {
    Button localButton = (Button)findViewById(2131296596);
    localButton.setVisibility(0);
    localButton.setText(paramInt);
    localButton.setOnClickListener(a(paramClass));
  }

  protected abstract Class<? extends eb> b();

  protected final void b(int paramInt, Class<? extends eb> paramClass)
  {
    Button localButton = (Button)findViewById(2131296597);
    localButton.setVisibility(0);
    localButton.setText(paramInt);
    localButton.setOnClickListener(a(paramClass));
  }

  protected final void b(boolean paramBoolean)
  {
    int i = getIntent().getIntExtra("alert_type", 0);
    if ((paramBoolean) && (i != 0))
      showDialog(i);
  }

  protected final boolean c()
  {
    return getIntent().getBooleanExtra("hideFrequency", false);
  }

  public final boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if (j(paramInt))
      return true;
    a.a(paramInt, this, b());
    return super.onKeyDown(paramInt, paramKeyEvent);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.m
 * JD-Core Version:    0.6.2
 */