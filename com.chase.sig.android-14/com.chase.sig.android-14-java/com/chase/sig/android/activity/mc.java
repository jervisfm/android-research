package com.chase.sig.android.activity;

import android.os.AsyncTask.Status;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

public final class mc
{
  Hashtable<Class<?>, ae> a = new Hashtable();
  private eb b = null;

  public final <T extends ae> T a(Class<T> paramClass)
  {
    int i;
    if ((this.a.get(paramClass) != null) && (((ae)this.a.get(paramClass)).getStatus() != AsyncTask.Status.FINISHED) && (!((ae)this.a.get(paramClass)).isCancelled()))
      i = 1;
    while (true)
    {
      if (i == 0);
      try
      {
        ae localae = (ae)paramClass.newInstance();
        localae.a(this.b);
        localae.a(this);
        this.a.put(localae.getClass(), localae);
        label93: return (ae)this.a.get(paramClass);
        i = 0;
      }
      catch (Exception localException)
      {
        break label93;
      }
    }
  }

  public final <T extends ae> List<T> a()
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = this.a.values().iterator();
    while (localIterator.hasNext())
    {
      ae localae = (ae)localIterator.next();
      if (localae.d())
        localArrayList.add(localae);
    }
    return localArrayList;
  }

  public final void a(eb parameb)
  {
    this.b = parameb;
    Iterator localIterator = this.a.values().iterator();
    while (localIterator.hasNext())
      ((ae)localIterator.next()).a(this.b);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.mc
 * JD-Core Version:    0.6.2
 */