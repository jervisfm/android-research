package com.chase.sig.android.activity;

import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;

final class me
  implements AdapterView.OnItemClickListener
{
  me(TeamInfoActivity paramTeamInfoActivity)
  {
  }

  public final void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    try
    {
      String str = (String)paramAdapterView.getItemAtPosition(paramInt);
      TeamInfoActivity.a(this.a, TeamInfoActivity.CommunicationOptions.a(str));
      BehaviorAnalyticsAspect.a();
      BehaviorAnalyticsAspect.a(paramAdapterView, paramView, paramInt);
      return;
    }
    catch (Throwable localThrowable)
    {
      BehaviorAnalyticsAspect.a();
      BehaviorAnalyticsAspect.a(paramAdapterView, paramView, paramInt);
      throw localThrowable;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.me
 * JD-Core Version:    0.6.2
 */