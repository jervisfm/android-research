package com.chase.sig.android.activity;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import com.chase.sig.android.domain.Advisor;

final class mf
  implements DialogInterface.OnClickListener
{
  mf(TeamInfoActivity paramTeamInfoActivity)
  {
  }

  public final void onClick(DialogInterface paramDialogInterface, int paramInt)
  {
    this.a.dismissDialog(0);
    Intent localIntent = new Intent("android.intent.action.SEND");
    localIntent.setType("plain/text");
    String[] arrayOfString = new String[1];
    arrayOfString[0] = TeamInfoActivity.a(this.a).b();
    localIntent.putExtra("android.intent.extra.EMAIL", arrayOfString);
    localIntent.putExtra("android.intent.extra.SUBJECT", "Subject");
    localIntent.putExtra("android.intent.extra.TEXT", "Text");
    this.a.startActivity(Intent.createChooser(localIntent, "Send mail..."));
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.mf
 * JD-Core Version:    0.6.2
 */