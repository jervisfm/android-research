package com.chase.sig.android.activity;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.util.imagebrowser.ImageBrowserActivity;

final class mi
  implements View.OnClickListener
{
  mi(TransactionDetailActivity paramTransactionDetailActivity)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    Intent localIntent = new Intent(this.a, ImageBrowserActivity.class);
    localIntent.putExtra("extra_key_max_sel_allowed", 3);
    this.a.startActivityForResult(localIntent, 1);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.mi
 * JD-Core Version:    0.6.2
 */