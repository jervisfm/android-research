package com.chase.sig.android.activity;

import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.domain.AccountTransfer;
import com.chase.sig.android.util.s;
import com.chase.sig.android.view.JPSpinner;
import com.chase.sig.android.view.detail.DetailView;
import com.chase.sig.android.view.detail.d;
import com.chase.sig.android.view.detail.q;

final class mn
  implements View.OnClickListener
{
  mn(TransferStartActivity paramTransferStartActivity)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    JPSpinner localJPSpinner1;
    JPSpinner localJPSpinner2;
    AccountTransfer localAccountTransfer;
    if (TransferStartActivity.a(this.a))
    {
      localJPSpinner1 = ((q)this.a.a("PAY_TO")).q();
      localJPSpinner2 = ((q)this.a.a("PAY_FROM")).q();
      localAccountTransfer = new AccountTransfer();
      localAccountTransfer.a(TransferStartActivity.b(this.a));
      localAccountTransfer.a(s.t(((d)this.a.a("DATE")).p()));
      if (!s.m(this.a.a().d("MEMO")))
        break label345;
    }
    label345: for (String str = this.a.a().d("MEMO"); ; str = "")
    {
      localAccountTransfer.l(str);
      localAccountTransfer.b(TransferStartActivity.a(TransferStartActivity.c(this.a), localJPSpinner2));
      localAccountTransfer.p(TransferStartActivity.b(TransferStartActivity.c(this.a), localJPSpinner2));
      localAccountTransfer.a(TransferStartActivity.a(TransferStartActivity.d(this.a), localJPSpinner1));
      localAccountTransfer.t(TransferStartActivity.b(TransferStartActivity.d(this.a), localJPSpinner1));
      localAccountTransfer.u(TransferStartActivity.c(TransferStartActivity.d(this.a), localJPSpinner1));
      localAccountTransfer.c(TransferStartActivity.d(TransferStartActivity.d(this.a), localJPSpinner1));
      localAccountTransfer.o(TransferStartActivity.d(TransferStartActivity.c(this.a), localJPSpinner2));
      TransferStartActivity.a(this.a, localAccountTransfer);
      TransferStartActivity.a locala = (TransferStartActivity.a)this.a.e.a(TransferStartActivity.a.class);
      if (!locala.d())
      {
        AccountTransfer[] arrayOfAccountTransfer = new AccountTransfer[1];
        arrayOfAccountTransfer[0] = TransferStartActivity.e(this.a);
        locala.execute(arrayOfAccountTransfer);
      }
      return;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.mn
 * JD-Core Version:    0.6.2
 */