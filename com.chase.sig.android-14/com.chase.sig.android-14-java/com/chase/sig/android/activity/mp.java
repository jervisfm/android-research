package com.chase.sig.android.activity;

import android.content.Intent;
import android.os.AsyncTask.Status;
import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;

final class mp
  implements View.OnClickListener
{
  mp(TransferVerifyActivity paramTransferVerifyActivity)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    if (this.a.getIntent().getBooleanExtra("INCLUDES_OPTIONAL_PRODUCT_FEE", false))
    {
      Intent localIntent = new Intent(this.a, TransferAuthorizeActivity.class);
      localIntent.putExtra("transaction_object", TransferVerifyActivity.a(this.a));
      this.a.startActivity(localIntent);
    }
    TransferVerifyActivity.a locala;
    do
    {
      return;
      locala = (TransferVerifyActivity.a)this.a.e.a(TransferVerifyActivity.a.class);
    }
    while (locala.getStatus() == AsyncTask.Status.RUNNING);
    locala.execute(new Void[0]);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.mp
 * JD-Core Version:    0.6.2
 */