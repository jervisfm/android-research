package com.chase.sig.android.activity;

import android.content.Intent;
import android.os.Bundle;
import com.chase.sig.android.domain.LabeledValue;
import com.chase.sig.android.domain.Transaction;
import com.chase.sig.android.service.movemoney.ListServiceResponse;
import com.chase.sig.android.service.movemoney.RequestFlags;
import com.chase.sig.android.util.e;
import com.chase.sig.android.view.detail.DetailColoredValueRow;
import java.util.ArrayList;
import java.util.Iterator;

public abstract class n<T extends Transaction> extends ai
  implements fk.e
{
  protected T a;
  protected int b;
  protected ArrayList<LabeledValue> c;

  protected static DetailColoredValueRow a(String paramString1, String paramString2)
  {
    if (paramString2 == null)
      return null;
    return new DetailColoredValueRow(paramString1, paramString2);
  }

  private ArrayList<LabeledValue> d()
  {
    return (ArrayList)e.a(getIntent().getExtras(), "frequency_list", null);
  }

  protected final String a(String paramString)
  {
    if (!this.a.l())
    {
      Iterator localIterator = this.c.iterator();
      while (localIterator.hasNext())
      {
        LabeledValue localLabeledValue = (LabeledValue)localIterator.next();
        if (paramString.equalsIgnoreCase(localLabeledValue.b()))
          return localLabeledValue.a();
      }
    }
    return "One-Time";
  }

  protected void a()
  {
  }

  protected final void a(Bundle paramBundle, Class<? extends a<T, ?>> paramClass)
  {
    if (e.a(paramBundle, "alert_type"));
    for (this.b = paramBundle.getInt("alert_type"); e.a(paramBundle, "transaction_object"); this.b = getIntent().getIntExtra("alert_type", 0))
    {
      a((Transaction)paramBundle.getSerializable("transaction_object"), false);
      this.c = ((ArrayList)e.a(paramBundle, "frequency_list", null));
      a();
      return;
    }
    this.a = f();
    if (d() != null);
    for (ArrayList localArrayList = d(); ; localArrayList = this.a.C())
    {
      this.c = localArrayList;
      String[] arrayOfString = new String[1];
      arrayOfString[0] = this.a.n();
      a(paramClass, arrayOfString);
      return;
    }
  }

  public final void a(T paramT, boolean paramBoolean)
  {
    this.a = paramT;
    if (this.c != null)
      this.a.a(this.c);
    while (true)
    {
      a(paramBoolean);
      return;
      this.c = this.a.C();
    }
  }

  protected abstract void a(boolean paramBoolean);

  protected final T f()
  {
    return (Transaction)getIntent().getExtras().getSerializable("transaction_object");
  }

  protected final RequestFlags g()
  {
    return (RequestFlags)getIntent().getSerializableExtra("request_flags");
  }

  protected final void h()
  {
    a(f(), true);
    a();
  }

  public void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    paramBundle.putSerializable("transaction_object", this.a);
    paramBundle.putSerializable("frequency_list", this.c);
    paramBundle.putInt("alert_type", this.b);
  }

  public static abstract class a<T extends Transaction, S extends n<T>> extends com.chase.sig.android.d<S, String, Void, ListServiceResponse<T>>
  {
    protected abstract com.chase.sig.android.service.movemoney.d<T> a();
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.n
 * JD-Core Version:    0.6.2
 */