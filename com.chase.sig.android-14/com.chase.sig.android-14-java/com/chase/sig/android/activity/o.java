package com.chase.sig.android.activity;

import android.os.AsyncTask.Status;
import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;

final class o
  implements View.OnClickListener
{
  o(AccountActivityActivity paramAccountActivityActivity)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    AccountActivityActivity.c localc = (AccountActivityActivity.c)this.a.e.a(AccountActivityActivity.c.class);
    if (localc.getStatus() != AsyncTask.Status.RUNNING)
    {
      Object[] arrayOfObject = new Object[2];
      arrayOfObject[0] = AccountActivityActivity.c(this.a);
      arrayOfObject[1] = Boolean.valueOf(AccountActivityActivity.d(this.a));
      localc.execute(arrayOfObject);
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.o
 * JD-Core Version:    0.6.2
 */