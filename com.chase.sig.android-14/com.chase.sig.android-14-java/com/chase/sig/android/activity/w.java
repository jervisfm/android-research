package com.chase.sig.android.activity;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.view.ai;

final class w
  implements AdapterView.OnItemClickListener
{
  w(AccountsActivity paramAccountsActivity)
  {
  }

  public final void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    try
    {
      ai localai = (ai)paramAdapterView.getItemAtPosition(paramInt);
      Intent localIntent = new Intent(this.a, AccountsActivity.class);
      localIntent.putExtra("showBusinessAccounts", localai.d);
      this.a.startActivity(localIntent);
      BehaviorAnalyticsAspect.a();
      BehaviorAnalyticsAspect.a(paramAdapterView, paramView, paramInt);
      return;
    }
    catch (Throwable localThrowable)
    {
      BehaviorAnalyticsAspect.a();
      BehaviorAnalyticsAspect.a(paramAdapterView, paramView, paramInt);
      throw localThrowable;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.w
 * JD-Core Version:    0.6.2
 */