package com.chase.sig.android.activity.wire;

import android.os.Bundle;
import com.chase.sig.android.activity.eb;
import com.chase.sig.android.activity.m;
import com.chase.sig.android.c;
import com.chase.sig.android.domain.WireTransaction;
import com.chase.sig.android.util.Dollar;
import com.chase.sig.android.util.s;
import com.chase.sig.android.view.detail.DetailRow;
import com.chase.sig.android.view.detail.DetailView;
import com.chase.sig.android.view.detail.a;

public class WireAddCompleteActivity extends m<WireTransaction>
{
  public final void a(Bundle paramBundle)
  {
    b(2130903134);
    setTitle(2131165300);
    c.a();
    h();
  }

  protected final void a(boolean paramBoolean)
  {
    DetailView localDetailView = (DetailView)findViewById(2131296595);
    WireTransaction localWireTransaction = (WireTransaction)this.a;
    a[] arrayOfa = new a[7];
    arrayOfa[0] = new DetailRow("Transaction Number", localWireTransaction.n());
    arrayOfa[1] = new DetailRow("Recipient", localWireTransaction.a());
    arrayOfa[2] = new DetailRow("From", localWireTransaction.m());
    arrayOfa[3] = new DetailRow("Amount", localWireTransaction.e().h());
    arrayOfa[4] = new DetailRow("Date", s.i(localWireTransaction.q()));
    arrayOfa[5] = new DetailRow("Status", localWireTransaction.t());
    arrayOfa[6] = new DetailRow("Memo", localWireTransaction.o());
    localDetailView.setRows(arrayOfa);
    a(2131165474, WireHistoryActivity.class);
    b(2131165475, WireAddStartActivity.class);
  }

  protected final Class<? extends eb> b()
  {
    return WireHomeActivity.class;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.wire.WireAddCompleteActivity
 * JD-Core Version:    0.6.2
 */