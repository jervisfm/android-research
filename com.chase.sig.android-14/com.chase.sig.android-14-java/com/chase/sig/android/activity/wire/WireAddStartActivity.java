package com.chase.sig.android.activity.wire;

import android.app.Dialog;
import android.os.Bundle;
import android.widget.Button;
import com.chase.sig.android.domain.IAccount;
import com.chase.sig.android.domain.Payee;
import com.chase.sig.android.domain.WireTransaction;
import com.chase.sig.android.domain.g;
import com.chase.sig.android.domain.o;
import com.chase.sig.android.service.movemoney.ServiceResponse;
import com.chase.sig.android.service.wire.WireGetPayeesResponse;
import com.chase.sig.android.util.s;
import com.chase.sig.android.view.JPSpinner;
import com.chase.sig.android.view.detail.DetailView;
import com.chase.sig.android.view.detail.l;
import com.chase.sig.android.view.detail.q;
import com.chase.sig.android.view.k.a;
import com.chase.sig.android.view.x;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class WireAddStartActivity extends com.chase.sig.android.activity.a
{
  x a = new c(this);
  private ArrayList<Payee> b;

  private JPSpinner e()
  {
    return b("PAY_FROM").q();
  }

  private ArrayList<IAccount> f()
  {
    return (ArrayList)r().b.j();
  }

  private Calendar g()
  {
    Calendar localCalendar = Calendar.getInstance();
    try
    {
      SimpleDateFormat localSimpleDateFormat = new SimpleDateFormat("yyyyMMdd");
      if (i().b())
        localCalendar.setTime(localSimpleDateFormat.parse(((Payee)this.b.get(i().getSelectedItemPosition())).e()));
      return localCalendar;
    }
    catch (ParseException localParseException)
    {
    }
    return localCalendar;
  }

  private void h()
  {
    e().setAdapter(com.chase.sig.android.util.a.a(this, f()));
    e().setSelection(-1);
    i().setAdapter(com.chase.sig.android.util.a.f(this, this.b));
    i().setSelection(-1);
  }

  private JPSpinner i()
  {
    return b("PAY_TO").q();
  }

  public final void a(Bundle paramBundle)
  {
    boolean bool1 = true;
    super.a(paramBundle);
    setTitle(2131165466);
    ((Button)findViewById(2131296970)).setText(2131165846);
    boolean bool2 = com.chase.sig.android.util.e.b(paramBundle, "memo_fields_visible");
    a locala = new a(this);
    DetailView localDetailView = a();
    com.chase.sig.android.view.detail.a[] arrayOfa = new com.chase.sig.android.view.detail.a[8];
    q localq1 = new q("Recipient");
    localq1.b = "PAY_TO";
    arrayOfa[0] = ((q)localq1).a(getString(2131165467));
    q localq2 = new q("From");
    localq2.b = "PAY_FROM";
    arrayOfa[bool1] = ((q)localq2).a(getString(2131165468));
    com.chase.sig.android.view.detail.c localc = new com.chase.sig.android.view.detail.c("Amount $");
    localc.b = "AMOUNT";
    arrayOfa[2] = ((com.chase.sig.android.view.detail.c)localc).a("Enter amount");
    com.chase.sig.android.view.detail.d locald1 = (com.chase.sig.android.view.detail.d)new com.chase.sig.android.view.detail.d("Date", "").a("Enter date");
    locald1.b = "DATE";
    com.chase.sig.android.view.detail.d locald2 = (com.chase.sig.android.view.detail.d)locald1;
    locald2.a = i(0);
    arrayOfa[3] = locald2.q();
    com.chase.sig.android.view.detail.d locald3 = (com.chase.sig.android.view.detail.d)new com.chase.sig.android.view.detail.d("", "").a("Enter message or memo");
    locald3.b = "MEMO_FIELDS";
    com.chase.sig.android.view.detail.d locald4 = (com.chase.sig.android.view.detail.d)locald3;
    locald4.n = bool1;
    com.chase.sig.android.view.detail.d locald5 = (com.chase.sig.android.view.detail.d)locald4;
    locald5.a = locala;
    locald5.j = bool2;
    arrayOfa[4] = locald5;
    l locall1 = (l)new l("Message To Recipient ", "").a("Optional");
    locall1.b = "MESSAGE_TO_RECIPIENT";
    l locall2 = (l)locall1;
    locall2.a = 140;
    boolean bool3;
    boolean bool4;
    if (!bool2)
    {
      bool3 = bool1;
      locall2.j = bool3;
      arrayOfa[5] = locall2;
      l locall3 = (l)new l("To Recipient's Bank ", "").a("Optional");
      locall3.b = "TO_RECIPIENT_BANK";
      l locall4 = (l)locall3;
      locall4.a = 100;
      if (bool2)
        break label543;
      bool4 = bool1;
      label400: locall4.j = bool4;
      arrayOfa[6] = locall4;
      l locall5 = (l)new l("Memo ", "").a("Optional");
      locall5.b = "MEMO";
      l locall6 = (l)locall5;
      locall6.a = 100;
      if (bool2)
        break label549;
      label462: locall6.j = bool1;
      arrayOfa[7] = locall6;
      localDetailView.setRows(arrayOfa);
      i().a(this.a);
      if (!com.chase.sig.android.util.e.a(paramBundle, "wirePayees"))
        break label554;
      this.b = ((ArrayList)paramBundle.getSerializable("wirePayees"));
      h();
    }
    while (true)
    {
      a(2131296971, new b(this));
      return;
      bool3 = false;
      break;
      label543: bool4 = false;
      break label400;
      label549: bool1 = false;
      break label462;
      label554: e().setSelection(-1);
      i().setSelection(-1);
      a(a.class, new Void[0]);
    }
  }

  protected final boolean d()
  {
    boolean bool1 = true;
    y();
    DetailView localDetailView = a();
    if (!i().b())
    {
      localDetailView.a("PAY_TO");
      bool1 = false;
    }
    if (!e().b())
      localDetailView.a("PAY_FROM");
    for (boolean bool2 = false; ; bool2 = bool1)
    {
      if (s.l(((com.chase.sig.android.view.detail.d)a("DATE")).p()))
      {
        localDetailView.a("DATE");
        bool2 = false;
      }
      if (!((com.chase.sig.android.view.detail.c)a("AMOUNT")).n())
      {
        localDetailView.a("AMOUNT");
        return false;
      }
      return bool2;
    }
  }

  public final void k()
  {
    y();
    a().e("MESSAGE_TO_RECIPIENT").j = true;
    a().e("TO_RECIPIENT_BANK").j = true;
    a().e("MEMO").j = true;
    a().e("MEMO_FIELDS").j = false;
    a().a();
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    Dialog localDialog = super.onCreateDialog(paramInt);
    switch (paramInt)
    {
    case 1:
    default:
      return localDialog;
    case 0:
      d locald = new d(this);
      Calendar localCalendar = Calendar.getInstance();
      localCalendar.add(2, 12);
      return new com.chase.sig.android.view.b(this, locald, true, true, g(), localCalendar, null);
    case 2:
    }
    k.a locala1 = new k.a(this);
    k.a locala2 = locala1.b(2131165469);
    locala2.i = false;
    locala2.a("OK", new e(this));
    return locala1.d(-1);
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    paramBundle.putBoolean("memo_fields_visible", a().e("MEMO_FIELDS").j);
    if (this.b != null)
      paramBundle.putSerializable("wirePayees", this.b);
  }

  public static class a extends com.chase.sig.android.d<WireAddStartActivity, Void, Void, WireGetPayeesResponse>
  {
  }

  public static class b extends com.chase.sig.android.d<WireAddStartActivity, WireTransaction, Void, ServiceResponse>
  {
    private WireTransaction a;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.wire.WireAddStartActivity
 * JD-Core Version:    0.6.2
 */