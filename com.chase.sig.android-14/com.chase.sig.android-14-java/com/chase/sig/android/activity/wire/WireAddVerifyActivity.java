package com.chase.sig.android.activity.wire;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import com.chase.sig.android.activity.PaymentsAndTransfersHomeActivity;
import com.chase.sig.android.activity.ai;
import com.chase.sig.android.activity.fk.e;
import com.chase.sig.android.c.a;
import com.chase.sig.android.d;
import com.chase.sig.android.domain.WireTransaction;
import com.chase.sig.android.service.movemoney.ServiceResponse;
import com.chase.sig.android.util.Dollar;
import com.chase.sig.android.util.s;
import com.chase.sig.android.view.detail.DetailRow;
import com.chase.sig.android.view.detail.DetailView;
import com.chase.sig.android.view.detail.a;

public class WireAddVerifyActivity extends ai
  implements fk.e, c.a
{
  private String a = "";

  private WireTransaction d()
  {
    return (WireTransaction)getIntent().getSerializableExtra("transaction_object");
  }

  public final void a()
  {
    Intent localIntent = new Intent(this, PaymentsAndTransfersHomeActivity.class);
    localIntent.setFlags(67108864);
    startActivity(localIntent);
  }

  public final void a(Bundle paramBundle)
  {
    b(2130903135);
    setTitle(2131165470);
    WireTransaction localWireTransaction = d();
    DetailView localDetailView = (DetailView)findViewById(2131296595);
    a[] arrayOfa = new a[7];
    arrayOfa[0] = new DetailRow("Recipient", localWireTransaction.d());
    arrayOfa[1] = new DetailRow("From", localWireTransaction.m());
    arrayOfa[2] = new DetailRow("Amount", localWireTransaction.e().h());
    arrayOfa[3] = new DetailRow("Date", s.i(localWireTransaction.q()));
    arrayOfa[4] = new DetailRow("Message To Recipient", localWireTransaction.f());
    arrayOfa[5] = new DetailRow("To Recipient's Bank", localWireTransaction.h());
    arrayOfa[6] = new DetailRow("Memo", localWireTransaction.o());
    localDetailView.setRows(arrayOfa);
    if ((paramBundle != null) && (paramBundle.containsKey("AGREEMENT")))
      this.a = paramBundle.getString("AGREEMENT");
    ((Button)findViewById(2131296599)).setText(2131165472);
    a(2131296599, i(0));
    ((Button)findViewById(2131296598)).setText(2131165473);
    a(2131296598, a(this));
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    switch (paramInt)
    {
    default:
      return super.onCreateDialog(paramInt);
    case 0:
      return f.a(this, this.a, b.class, a.class);
    case 1:
    }
    return f.a(this, this.a);
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    paramBundle.putString("AGREEMENT", this.a);
  }

  public static class a extends f.a<WireAddVerifyActivity>
  {
  }

  public static class b extends d<WireAddVerifyActivity, Void, Void, ServiceResponse>
  {
    WireTransaction a;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.wire.WireAddVerifyActivity
 * JD-Core Version:    0.6.2
 */