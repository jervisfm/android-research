package com.chase.sig.android.activity.wire;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import com.chase.sig.android.activity.a.b;
import com.chase.sig.android.activity.ae;
import com.chase.sig.android.activity.eb;
import com.chase.sig.android.activity.m;
import com.chase.sig.android.activity.n.a;
import com.chase.sig.android.c;
import com.chase.sig.android.domain.WireTransaction;
import com.chase.sig.android.service.movemoney.d;
import com.chase.sig.android.service.n;
import com.chase.sig.android.util.Dollar;
import com.chase.sig.android.util.s;
import com.chase.sig.android.view.detail.DetailRow;
import com.chase.sig.android.view.detail.DetailView;
import com.chase.sig.android.view.detail.a;
import com.chase.sig.android.view.k.a;

public class WireCompleteActivity extends m<WireTransaction>
{
  public final void a(Bundle paramBundle)
  {
    b(2130903134);
    setTitle(2131165300);
    c.a();
    a(paramBundle, a.class);
  }

  protected final void a(boolean paramBoolean)
  {
    WireTransaction localWireTransaction = (WireTransaction)this.a;
    DetailView localDetailView = (DetailView)findViewById(2131296595);
    a[] arrayOfa = new a[8];
    arrayOfa[0] = new DetailRow("Transaction Number", localWireTransaction.n());
    arrayOfa[1] = new DetailRow("Recipient", localWireTransaction.a());
    arrayOfa[2] = new DetailRow("From", localWireTransaction.m());
    arrayOfa[3] = new DetailRow("Amount", localWireTransaction.e().h());
    arrayOfa[4] = new DetailRow("Date", s.i(localWireTransaction.q()));
    DetailRow localDetailRow1 = new DetailRow("Frequency", a(localWireTransaction.i()));
    localDetailRow1.j = c();
    arrayOfa[5] = localDetailRow1;
    DetailRow localDetailRow2 = new DetailRow("Remaining Wires", localWireTransaction.w());
    localDetailRow2.j = c();
    arrayOfa[6] = localDetailRow2;
    arrayOfa[7] = new DetailRow("Status", localWireTransaction.t());
    localDetailView.setRows(arrayOfa);
    a(2131165474, WireHistoryActivity.class);
    b(2131165475, WireAddStartActivity.class);
    b(paramBoolean);
  }

  protected final Class<? extends eb> b()
  {
    return WireHomeActivity.class;
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    k.a locala = new k.a(this);
    switch (paramInt)
    {
    default:
      return super.onCreateDialog(paramInt);
    case 12:
      WireTransaction localWireTransaction = (WireTransaction)this.a;
      Context localContext = getBaseContext();
      Object[] arrayOfObject = new Object[4];
      arrayOfObject[0] = a(localWireTransaction.i());
      arrayOfObject[1] = (localWireTransaction.r() + localWireTransaction.s());
      arrayOfObject[2] = localWireTransaction.d();
      arrayOfObject[3] = localWireTransaction.d();
      String str = s.a(localContext, 2131165478, arrayOfObject);
      locala.i = true;
      locala.a(2131165288, new b()).c(2131165477).a(str);
      return locala.d(-1);
    case 13:
    }
    locala.i = true;
    locala.a(2131165288, new b()).b(2131165476);
    return locala.d(-1);
  }

  public static class a extends n.a<WireTransaction, WireCompleteActivity>
  {
    protected final d<WireTransaction> a()
    {
      ((WireCompleteActivity)this.b).A();
      return n.f();
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.wire.WireCompleteActivity
 * JD-Core Version:    0.6.2
 */