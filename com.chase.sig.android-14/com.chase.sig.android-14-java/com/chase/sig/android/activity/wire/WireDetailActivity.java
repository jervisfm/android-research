package com.chase.sig.android.activity.wire;

import android.os.Bundle;
import com.chase.sig.android.activity.DetailResource;
import com.chase.sig.android.activity.WireEditActivity;
import com.chase.sig.android.activity.ae;
import com.chase.sig.android.activity.c;
import com.chase.sig.android.activity.c.a;
import com.chase.sig.android.activity.eb;
import com.chase.sig.android.activity.n.a;
import com.chase.sig.android.domain.WireTransaction;
import com.chase.sig.android.service.movemoney.d;
import com.chase.sig.android.service.n;

public class WireDetailActivity extends c<WireTransaction>
{
  public final int a(DetailResource paramDetailResource)
  {
    switch (1.a[paramDetailResource.ordinal()])
    {
    default:
      return 0;
    case 1:
      return 2131165484;
    case 2:
      return 2131165486;
    case 3:
      return 2131165487;
    case 4:
      return 2131165488;
    case 5:
      return 2131165489;
    case 6:
      return 2131165490;
    case 7:
      return 2131165491;
    case 8:
      return 2131165317;
    case 9:
      return 2131165493;
    case 10:
      return 2131165492;
    case 11:
    }
    return 2131165494;
  }

  protected final void a()
  {
    super.a();
  }

  public final void a(Bundle paramBundle)
  {
    b(2130903238);
    setTitle(2131165483);
    a(paramBundle, b.class);
  }

  protected final d<WireTransaction> b()
  {
    A();
    return n.f();
  }

  protected final Class<? extends ae<?, Boolean, ?, ?>> c()
  {
    return a.class;
  }

  protected final Class<? extends eb> d()
  {
    return WireCompleteActivity.class;
  }

  protected final Class<? extends eb> e()
  {
    return WireEditActivity.class;
  }

  public static class a extends c.a<WireTransaction>
  {
  }

  public static class b extends n.a<WireTransaction, WireDetailActivity>
  {
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.wire.WireDetailActivity
 * JD-Core Version:    0.6.2
 */