package com.chase.sig.android.activity.wire;

import android.app.Dialog;
import android.os.Bundle;
import android.widget.Button;
import com.chase.sig.android.activity.k;
import com.chase.sig.android.activity.k.a;
import com.chase.sig.android.domain.WireTransaction;
import com.chase.sig.android.service.movemoney.d;
import com.chase.sig.android.service.n;
import com.chase.sig.android.util.Dollar;
import com.chase.sig.android.util.e;
import com.chase.sig.android.util.s;
import com.chase.sig.android.view.detail.DetailRow;
import com.chase.sig.android.view.detail.DetailView;
import com.chase.sig.android.view.detail.a;

public class WireEditVerifyActivity extends k<WireTransaction>
{
  private String d;

  public final void a(Bundle paramBundle)
  {
    setTitle(2131165485);
    b(2130903239);
    h();
    ((Button)findViewById(2131296977)).setText(2131165473);
    ((Button)findViewById(2131296978)).setText(2131165472);
    if (e.a(paramBundle, "WIRE_AGREEMENT_MESSAGE"))
      this.d = paramBundle.getString("WIRE_AGREEMENT_MESSAGE");
  }

  protected final void a(boolean paramBoolean)
  {
    WireTransaction localWireTransaction = (WireTransaction)this.a;
    DetailView localDetailView = (DetailView)findViewById(2131296976);
    a[] arrayOfa = new a[9];
    arrayOfa[0] = new DetailRow("Recipient", localWireTransaction.d());
    arrayOfa[1] = new DetailRow("From", localWireTransaction.m());
    arrayOfa[2] = new DetailRow("Amount", localWireTransaction.e().h());
    arrayOfa[3] = new DetailRow("Date", s.i(localWireTransaction.q()));
    DetailRow localDetailRow1 = new DetailRow("Frequency", a(localWireTransaction.i()));
    localDetailRow1.j = b();
    arrayOfa[4] = localDetailRow1;
    DetailRow localDetailRow2 = new DetailRow("Remaining Wires", localWireTransaction.w());
    localDetailRow2.j = b();
    arrayOfa[5] = localDetailRow2;
    arrayOfa[6] = new DetailRow("Message To Recipient", localWireTransaction.f());
    arrayOfa[7] = new DetailRow("To Recipient's Bank", localWireTransaction.h());
    arrayOfa[8] = new DetailRow("Memo", localWireTransaction.o());
    localDetailView.setRows(arrayOfa);
    a(2131296978, i(0));
    a(2131296977, B());
  }

  protected final d<WireTransaction> c()
  {
    A();
    return n.f();
  }

  protected final Class<WireCompleteActivity> d()
  {
    return WireCompleteActivity.class;
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    switch (paramInt)
    {
    default:
      return super.onCreateDialog(paramInt);
    case 0:
      return f.a(this, this.d, b.class, a.class);
    case 1:
    }
    return f.a(this, this.d);
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    if (s.m(this.d))
      paramBundle.putString("WIRE_AGREEMENT_MESSAGE", this.d);
  }

  public static class a extends f.a<WireEditVerifyActivity>
  {
  }

  public static class b extends k.a<WireTransaction>
  {
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.wire.WireEditVerifyActivity
 * JD-Core Version:    0.6.2
 */