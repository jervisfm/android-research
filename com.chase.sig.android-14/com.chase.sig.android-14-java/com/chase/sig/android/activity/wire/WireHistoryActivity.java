package com.chase.sig.android.activity.wire;

import android.os.Bundle;
import com.chase.sig.android.activity.ae;
import com.chase.sig.android.activity.eb;
import com.chase.sig.android.activity.l;
import com.chase.sig.android.activity.l.a;
import com.chase.sig.android.domain.WireTransaction;
import com.chase.sig.android.service.movemoney.d;
import com.chase.sig.android.service.n;

public class WireHistoryActivity extends l<WireTransaction>
{
  protected final int a()
  {
    return 2131165482;
  }

  public final void a(Bundle paramBundle)
  {
    super.a(paramBundle);
    b(paramBundle);
  }

  protected final Class<? extends l.a<WireTransaction>> b()
  {
    return a.class;
  }

  protected final Class<? extends eb> c()
  {
    return WireDetailActivity.class;
  }

  public static class a extends l.a<WireTransaction>
  {
    protected final d<WireTransaction> a()
    {
      ((l)this.b).A();
      return n.f();
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.wire.WireHistoryActivity
 * JD-Core Version:    0.6.2
 */