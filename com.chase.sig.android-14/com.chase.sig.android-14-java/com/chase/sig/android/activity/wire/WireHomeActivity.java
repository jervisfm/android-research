package com.chase.sig.android.activity.wire;

import android.app.Dialog;
import android.os.Bundle;
import com.chase.sig.android.activity.a.b;
import com.chase.sig.android.activity.ai;
import com.chase.sig.android.activity.eb;
import com.chase.sig.android.activity.fk.e;
import com.chase.sig.android.domain.o;
import com.chase.sig.android.service.SplashResponse;
import com.chase.sig.android.view.k.a;

public class WireHomeActivity extends ai
  implements fk.e
{
  public final void a(Bundle paramBundle)
  {
    b(2130903250);
    setTitle(2131165479);
    if ((r().d != null) && (r().d.k()))
    {
      e(r().d.q());
      return;
    }
    a(2131297018, new j(this));
    a(2131297019, a(WireHistoryActivity.class));
  }

  protected Dialog onCreateDialog(int paramInt)
  {
    Object localObject = super.onCreateDialog(paramInt);
    if (paramInt == 0)
    {
      k.a locala = new k.a(this);
      locala.b(2131165825).a("OK", new b());
      localObject = locala.d(-1);
    }
    return localObject;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.wire.WireHomeActivity
 * JD-Core Version:    0.6.2
 */