package com.chase.sig.android.activity.wire;

import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.domain.IAccount;
import com.chase.sig.android.domain.Payee;
import com.chase.sig.android.domain.WireTransaction;
import com.chase.sig.android.util.Dollar;
import com.chase.sig.android.util.s;
import com.chase.sig.android.view.JPSpinner;
import com.chase.sig.android.view.detail.DetailView;
import java.util.ArrayList;

final class b
  implements View.OnClickListener
{
  b(WireAddStartActivity paramWireAddStartActivity)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    if (this.a.d())
    {
      WireTransaction localWireTransaction = new WireTransaction();
      localWireTransaction.a((Dollar)WireAddStartActivity.b(this.a).c("AMOUNT"));
      localWireTransaction.n(s.b(s.t((String)WireAddStartActivity.c(this.a).c("DATE"))));
      localWireTransaction.e(s.s(WireAddStartActivity.d(this.a).d("MESSAGE_TO_RECIPIENT")));
      localWireTransaction.f(s.s(WireAddStartActivity.e(this.a).d("TO_RECIPIENT_BANK")));
      localWireTransaction.l(s.s(WireAddStartActivity.f(this.a).d("MEMO")));
      Payee localPayee = (Payee)WireAddStartActivity.h(this.a).get(WireAddStartActivity.g(this.a).getSelectedItemPosition());
      localWireTransaction.v(localPayee.j());
      localWireTransaction.a(localPayee.h());
      IAccount localIAccount = (IAccount)WireAddStartActivity.j(this.a).get(WireAddStartActivity.i(this.a).getSelectedItemPosition());
      localWireTransaction.b(localIAccount.b());
      localWireTransaction.o(localIAccount.a());
      localWireTransaction.p(localIAccount.y().toString());
      this.a.a(WireAddStartActivity.b.class, new WireTransaction[] { localWireTransaction });
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.wire.b
 * JD-Core Version:    0.6.2
 */