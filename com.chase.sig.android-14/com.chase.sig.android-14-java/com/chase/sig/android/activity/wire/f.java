package com.chase.sig.android.activity.wire;

import android.app.Dialog;
import android.content.Context;
import com.chase.sig.android.activity.a.b;
import com.chase.sig.android.activity.ae;
import com.chase.sig.android.activity.eb;
import com.chase.sig.android.d;
import com.chase.sig.android.domain.o;
import com.chase.sig.android.service.content.ContentResponse;
import com.chase.sig.android.view.k.a;

public final class f
{
  static Dialog a(Context paramContext, String paramString)
  {
    k.a locala = new k.a(paramContext);
    locala.i = true;
    locala.a("Close", new b());
    locala.b = "Wire Agreement";
    locala.a(paramString);
    return locala.d(-1);
  }

  static Dialog a(eb parameb, String paramString, Class<? extends ae<?, Void, ?, ?>> paramClass1, Class<? extends ae<?, Void, ?, ?>> paramClass2)
  {
    k.a locala = new k.a(parameb);
    locala.i = false;
    locala.a("Schedule Wire", new g(parameb, paramClass1));
    if (parameb.r().b.A())
    {
      h localh = new h(paramString, parameb, paramClass2);
      locala.j = "See Wire Agreement";
      locala.f = localh;
    }
    locala.b("Cancel Wire", new i(parameb));
    locala.a("Note: By selecting 'Schedule Wire' you agree your wire will be governed- by the terms of the Wire Agreement.");
    return locala.d(-1);
  }

  public static abstract class a<T extends eb> extends d<T, Void, Void, ContentResponse>
  {
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.wire.f
 * JD-Core Version:    0.6.2
 */