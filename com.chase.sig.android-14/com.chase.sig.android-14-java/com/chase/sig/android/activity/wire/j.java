package com.chase.sig.android.activity.wire;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.domain.g;
import com.chase.sig.android.domain.o;
import java.util.ArrayList;

final class j
  implements View.OnClickListener
{
  j(WireHomeActivity paramWireHomeActivity)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    if (((ArrayList)this.a.r().b.j()).size() == 0)
    {
      this.a.showDialog(0);
      return;
    }
    Intent localIntent = new Intent(this.a.getBaseContext(), WireAddStartActivity.class);
    this.a.startActivity(localIntent);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.activity.wire.j
 * JD-Core Version:    0.6.2
 */