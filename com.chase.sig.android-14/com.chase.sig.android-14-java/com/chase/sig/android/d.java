package com.chase.sig.android;

import com.chase.sig.android.activity.ae;
import com.chase.sig.android.activity.eb;

public abstract class d<ActivityType extends eb, Params, Progress, Result> extends ae<ActivityType, Params, Progress, Result>
{
  public void onCancelled()
  {
    super.onCancelled();
    this.b.u();
  }

  public final void onPostExecute(Result paramResult)
  {
    this.b.u();
    super.onPostExecute(paramResult);
  }

  public void onPreExecute()
  {
    this.b.t();
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.d
 * JD-Core Version:    0.6.2
 */