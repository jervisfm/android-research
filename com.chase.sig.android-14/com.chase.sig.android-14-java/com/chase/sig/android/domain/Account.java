package com.chase.sig.android.domain;

import com.chase.sig.android.util.Dollar;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class Account
  implements IAccount
{
  private static final List<String> a = Arrays.asList(new String[] { "BRC", "BCL" });
  private static final List<String> b = Arrays.asList(new String[] { "ALA", "ALS" });
  private static final List<String> c = Arrays.asList(new String[] { "HMG", "HEO", "HEL", "RCA", "ILA" });
  private static final List<String> d = Arrays.asList(new String[] { "PAC", "BAC", "OLC", "PCC", "BCC", "SCC" });
  private static final List<String> e = Arrays.asList(new String[] { "BRK", "WRP", "BR2", "WR2", "TRS", "JPF", "ANU", "OMN" });
  private static final List<String> f = Arrays.asList(new String[] { "CHK", "AMA", "SAV", "MMA", "CDA", "IRA" });
  private static final List<String> g = Arrays.asList(new String[] { "OFF", "MAN", "MUT", "MAR", "BR2", "WR2" });
  private static final List<String> h = Arrays.asList(new String[] { "CRF" });
  private static final List<String> i = Arrays.asList(new String[] { "PPA", "PPX" });
  private List<f> accountDetailsList;
  private Hashtable<String, String> details;
  private Set<String> disclosureIds;
  private String id;
  private boolean isBCCControl;
  private boolean isBCCEmployee;
  private boolean isBCCOfficer;
  private boolean isOmni;
  private String mask;
  private String nickname;
  private Set<String> privileges;
  private String standInAsofDate;
  private List<IAccount> subAccountsList;
  private String type;

  public final boolean A()
  {
    return (!this.type.equals("RWA")) && (!this.type.equals("ATM")) && (e().get("rewardsProgramName") != null);
  }

  public final CharSequence B()
  {
    Object[] arrayOfObject = new Object[2];
    arrayOfObject[0] = this.nickname;
    arrayOfObject[1] = this.mask;
    return String.format("%s (%s)", arrayOfObject);
  }

  public final boolean C()
  {
    return !G();
  }

  public final boolean D()
  {
    return "CRF".equals(this.type);
  }

  public final boolean E()
  {
    return "SCC".equalsIgnoreCase(this.type);
  }

  public final String F()
  {
    boolean bool = "CRF".equals(this.type);
    String str = null;
    if (bool)
    {
      Hashtable localHashtable = this.details;
      str = null;
      if (localHashtable != null)
        str = (String)this.details.get("outstanding");
    }
    return str;
  }

  public final boolean G()
  {
    return (1 == s()) || (8 == s());
  }

  public final boolean H()
  {
    return f("feature_positions");
  }

  public final boolean I()
  {
    return (e().containsKey("txnCounter")) || (c("Withdrawals this period") != null);
  }

  public final Set<String> J()
  {
    return this.privileges;
  }

  public final List<f> K()
  {
    return this.accountDetailsList;
  }

  public final String L()
  {
    return this.standInAsofDate;
  }

  public final boolean M()
  {
    return s() == 10;
  }

  public final String a()
  {
    return this.nickname;
  }

  public final String a(boolean paramBoolean)
  {
    String str1;
    Object[] arrayOfObject;
    if ((this.disclosureIds != null) && (this.disclosureIds.size() > 0) && (!paramBoolean))
    {
      str1 = "%s* (%s)";
      arrayOfObject = new Object[2];
      if (this.nickname != null)
        break label66;
    }
    label66: for (String str2 = ""; ; str2 = this.nickname)
    {
      arrayOfObject[0] = str2;
      arrayOfObject[1] = this.mask;
      return String.format(str1, arrayOfObject);
      str1 = "%s (%s)";
      break;
    }
  }

  public final void a(String paramString)
  {
    this.mask = paramString;
  }

  public final void a(Hashtable<String, String> paramHashtable)
  {
    this.details = paramHashtable;
  }

  public final void a(List<IAccount> paramList)
  {
    this.subAccountsList = paramList;
  }

  public final void a(Set<String> paramSet)
  {
    this.disclosureIds = paramSet;
  }

  public final boolean a(String[] paramArrayOfString)
  {
    int j = paramArrayOfString.length;
    for (int k = 0; ; k++)
    {
      boolean bool = false;
      if (k < j)
      {
        if (paramArrayOfString[k].equals(this.type))
          bool = true;
      }
      else
        return bool;
    }
  }

  public final String b()
  {
    return this.id;
  }

  public final void b(String paramString)
  {
    this.id = paramString;
  }

  public final void b(List<f> paramList)
  {
    this.accountDetailsList = paramList;
  }

  public final void b(Set<String> paramSet)
  {
    this.privileges = paramSet;
  }

  public final void b(boolean paramBoolean)
  {
    this.isBCCOfficer = paramBoolean;
  }

  public final f c(String paramString)
  {
    Iterator localIterator = this.accountDetailsList.iterator();
    while (localIterator.hasNext())
    {
      f localf = (f)localIterator.next();
      if (localf.a().equals(paramString))
        return localf;
    }
    return null;
  }

  public final String c()
  {
    return this.mask;
  }

  public final void c(boolean paramBoolean)
  {
    this.isBCCControl = paramBoolean;
  }

  public final String d()
  {
    return this.type;
  }

  public final void d(String paramString)
  {
    this.type = paramString;
  }

  public final void d(boolean paramBoolean)
  {
    this.isBCCEmployee = paramBoolean;
  }

  public final Hashtable<String, String> e()
  {
    if (this.details == null)
      return new Hashtable();
    return this.details;
  }

  public final void e(String paramString)
  {
    this.nickname = paramString;
  }

  public final void e(boolean paramBoolean)
  {
    this.isOmni = paramBoolean;
  }

  public final boolean f()
  {
    if (G())
      return f("feature_activity");
    return f("mobile_activity");
  }

  protected final boolean f(String paramString)
  {
    if (this.privileges == null)
      return false;
    return this.privileges.contains(paramString);
  }

  public final void g(String paramString)
  {
    this.standInAsofDate = paramString;
  }

  public final boolean g()
  {
    return this.isBCCControl;
  }

  public final boolean h()
  {
    return this.isBCCEmployee;
  }

  public final boolean i()
  {
    return this.isBCCOfficer;
  }

  public final Dollar j()
  {
    if (this.details != null);
    for (String str = (String)this.details.get("available"); ; str = null)
    {
      if (str == null)
        return null;
      return new Dollar(str);
    }
  }

  public final Dollar k()
  {
    if (this.details != null);
    for (String str = (String)this.details.get("current"); ; str = null)
    {
      if (str == null)
        return null;
      return new Dollar(str);
    }
  }

  public final boolean l()
  {
    return this.privileges.contains("mobile_blueprint");
  }

  public final boolean m()
  {
    return this.privileges.contains("feature_chasereceipts");
  }

  public final boolean n()
  {
    return this.privileges.contains("mobile_mortgage_bankruptcy");
  }

  public final boolean o()
  {
    return this.privileges.contains("mobile_internal_xfer");
  }

  public final boolean p()
  {
    return this.privileges.contains("mobile_internal_xfer_credit");
  }

  public final boolean q()
  {
    return this.privileges.contains("mobile_epay");
  }

  public final boolean r()
  {
    return this.privileges.contains("mobile_debitcardrewards");
  }

  public final int s()
  {
    if (f.contains(this.type))
      return 7;
    if (d.contains(this.type))
    {
      if ((this.type.equals("BCC")) && (this.isBCCEmployee))
        return 3;
      return 2;
    }
    if (b.contains(this.type))
      return 5;
    if (g.contains(this.type))
      return 8;
    if (a.contains(this.type))
      return 6;
    if (c.contains(this.type))
      return 4;
    if (e.contains(this.type))
      return 1;
    if (h.contains(this.type))
      return 9;
    if (i.contains(this.type))
      return 10;
    return 0;
  }

  public final boolean t()
  {
    return this.privileges.contains("mobile_paybills");
  }

  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("Account [type=");
    localStringBuilder.append(this.type);
    localStringBuilder.append(", isOmni=");
    localStringBuilder.append(this.isOmni);
    localStringBuilder.append(", isBCCControl=");
    localStringBuilder.append(this.isBCCControl);
    localStringBuilder.append(", isBCCEmployee=");
    localStringBuilder.append(this.isBCCEmployee);
    localStringBuilder.append(", isBCCOfficer=");
    localStringBuilder.append(this.isBCCOfficer);
    localStringBuilder.append(", mask=");
    localStringBuilder.append(this.mask);
    localStringBuilder.append(", disclosureIds=");
    localStringBuilder.append(this.disclosureIds);
    localStringBuilder.append(", id=");
    localStringBuilder.append(this.id);
    localStringBuilder.append(", nickname=");
    localStringBuilder.append(this.nickname);
    localStringBuilder.append(", subAccountsList=");
    localStringBuilder.append(this.subAccountsList);
    localStringBuilder.append(", accountDetailsList=");
    localStringBuilder.append(this.accountDetailsList);
    localStringBuilder.append(", details=");
    localStringBuilder.append(this.details);
    localStringBuilder.append(", privileges=");
    localStringBuilder.append(this.privileges);
    localStringBuilder.append("]");
    return localStringBuilder.toString();
  }

  public final boolean u()
  {
    return this.isOmni;
  }

  public final List<IAccount> v()
  {
    return this.subAccountsList;
  }

  public final CharSequence w()
  {
    Object[] arrayOfObject = new Object[2];
    arrayOfObject[0] = this.nickname;
    arrayOfObject[1] = this.mask;
    return String.format("%s (%s)", arrayOfObject);
  }

  public final IAccount x()
  {
    if ((this.isBCCOfficer) && (this.subAccountsList != null))
    {
      Iterator localIterator = this.subAccountsList.iterator();
      while (localIterator.hasNext())
      {
        IAccount localIAccount = (IAccount)localIterator.next();
        if (localIAccount.g())
          return localIAccount;
      }
    }
    return null;
  }

  public final CharSequence y()
  {
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = this.mask;
    return String.format("(%s)", arrayOfObject);
  }

  public final boolean z()
  {
    return (this.type.equals("RWA")) || (this.type.equals("ATM"));
  }

  public static class Value
    implements Serializable
  {
    private String text;
    private String type;

    public final String a()
    {
      return this.type;
    }

    public final String b()
    {
      return this.text;
    }

    public String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("Value [type=");
      localStringBuilder.append(this.type);
      localStringBuilder.append(", text=");
      localStringBuilder.append(this.text);
      localStringBuilder.append("]");
      return localStringBuilder.toString();
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.Account
 * JD-Core Version:    0.6.2
 */