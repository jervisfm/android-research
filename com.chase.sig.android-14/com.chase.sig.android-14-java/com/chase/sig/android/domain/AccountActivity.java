package com.chase.sig.android.domain;

import java.io.Serializable;
import java.util.ArrayList;

public class AccountActivity
  implements Serializable
{
  private String description;
  private String transactionId;
  private ArrayList<ActivityValue> values = new ArrayList();

  public final String a()
  {
    return this.description;
  }

  public final void a(String paramString)
  {
    this.description = paramString;
  }

  public final ArrayList<ActivityValue> b()
  {
    return this.values;
  }

  public final void b(String paramString)
  {
    this.transactionId = paramString;
  }

  public final String c()
  {
    return this.transactionId;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.AccountActivity
 * JD-Core Version:    0.6.2
 */