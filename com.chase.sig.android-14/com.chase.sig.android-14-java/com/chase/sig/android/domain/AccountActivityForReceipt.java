package com.chase.sig.android.domain;

import com.google.gson.chase.annotations.SerializedName;
import java.io.Serializable;

public class AccountActivityForReceipt
  implements Serializable
{
  private long accountId;

  @SerializedName(a="page")
  private TransactionsForReceiptPage transForReceiptPage;

  public final TransactionsForReceiptPage a()
  {
    return this.transForReceiptPage;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.AccountActivityForReceipt
 * JD-Core Version:    0.6.2
 */