package com.chase.sig.android.domain;

import java.io.Serializable;
import java.util.Hashtable;

public class AccountDetail
  implements Serializable
{
  private boolean failed;
  private String id;
  private boolean loaded;
  private Hashtable<String, String> values;

  public final void a(String paramString)
  {
    this.id = paramString;
  }

  public final void a(Hashtable<String, String> paramHashtable)
  {
    this.values = paramHashtable;
  }

  public final void a(boolean paramBoolean)
  {
    this.failed = paramBoolean;
  }

  public final void b(boolean paramBoolean)
  {
    this.loaded = paramBoolean;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.AccountDetail
 * JD-Core Version:    0.6.2
 */