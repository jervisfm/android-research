package com.chase.sig.android.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Hashtable;

public class AccountRewardsDetail
  implements Serializable
{
  String accountId;
  ArrayList<RewardDetailValue> allRewardsValuePairs;
  Hashtable<String, String> allRewardsValuePairsHash;
  boolean isFailed;

  public final Hashtable<String, String> a()
  {
    return this.allRewardsValuePairsHash;
  }

  public final void a(ArrayList<RewardDetailValue> paramArrayList)
  {
    this.allRewardsValuePairs = paramArrayList;
  }

  public final void a(Hashtable<String, String> paramHashtable)
  {
    this.allRewardsValuePairsHash = paramHashtable;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.AccountRewardsDetail
 * JD-Core Version:    0.6.2
 */