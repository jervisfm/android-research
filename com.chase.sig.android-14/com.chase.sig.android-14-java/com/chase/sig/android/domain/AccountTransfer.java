package com.chase.sig.android.domain;

import java.util.Date;

public class AccountTransfer extends Transaction
{
  private Date processDate;
  private String sendOnDate;
  private String toAccount;
  private String toAccountNickName;

  public final String a()
  {
    return this.toAccount;
  }

  public final void a(String paramString)
  {
    this.toAccount = paramString;
  }

  public final void a_(String paramString)
  {
    this.sendOnDate = paramString;
  }

  public final String b()
  {
    return this.sendOnDate;
  }

  public final void c(String paramString)
  {
    this.toAccountNickName = paramString;
  }

  public final String d()
  {
    Object[] arrayOfObject = new Object[2];
    arrayOfObject[0] = this.toAccountNickName;
    arrayOfObject[1] = this.toAccountNumber;
    return String.format("%s %s", arrayOfObject).trim();
  }

  public final String d_()
  {
    return this.toAccountNickName;
  }

  public final String e_()
  {
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = this.fromAccountMask;
    return String.format("%s", arrayOfObject).trim();
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.AccountTransfer
 * JD-Core Version:    0.6.2
 */