package com.chase.sig.android.domain;

import com.chase.sig.android.util.s;
import java.io.Serializable;

public class ActivityValue
  implements Serializable
{
  public String label;
  public String value;

  public ActivityValue(String paramString1, String paramString2)
  {
    this.label = paramString1;
    this.value = paramString2;
  }

  public final boolean a()
  {
    return ("Balance".equalsIgnoreCase(this.label)) && (s.l(this.value));
  }

  public final boolean b()
  {
    return (("Debit/Credit amount".equalsIgnoreCase(this.label)) || ("Debit/Credit".equalsIgnoreCase(this.label))) && (s.m(this.value));
  }

  public final boolean c()
  {
    return (("Transaction date".equalsIgnoreCase(this.label)) || ("Date".equalsIgnoreCase(this.label))) && (s.m(this.value));
  }

  public final boolean d()
  {
    boolean bool1 = s.m(this.value);
    boolean bool2 = false;
    if (bool1)
    {
      boolean bool3 = this.value.trim().startsWith("-$");
      bool2 = false;
      if (bool3)
        bool2 = true;
    }
    return bool2;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.ActivityValue
 * JD-Core Version:    0.6.2
 */