package com.chase.sig.android.domain;

import java.io.Serializable;

public class Advisor
  implements Serializable
{
  private String displayName;
  private String email;
  private String firstName;
  private String lastName;
  private String middleInitial;
  private String phone;
  private String relationship;
  private boolean support;

  public Advisor()
  {
  }

  public Advisor(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, boolean paramBoolean)
  {
    this.phone = paramString1;
    this.email = paramString2;
    this.middleInitial = paramString3;
    this.relationship = paramString4;
    this.firstName = paramString5;
    this.lastName = paramString6;
    this.displayName = paramString7;
    this.support = paramBoolean;
  }

  public final String a()
  {
    return this.phone;
  }

  public final String b()
  {
    return this.email;
  }

  public final String c()
  {
    return this.relationship;
  }

  public final String d()
  {
    return this.displayName;
  }

  public final boolean e()
  {
    return this.support;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.Advisor
 * JD-Core Version:    0.6.2
 */