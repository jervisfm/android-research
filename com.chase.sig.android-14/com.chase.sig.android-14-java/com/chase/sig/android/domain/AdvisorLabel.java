package com.chase.sig.android.domain;

import java.io.Serializable;

public class AdvisorLabel
  implements Serializable
{
  private String actionEmail;
  private String advisor;
  private String email;
  private String phone;
  private String relationship;
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.AdvisorLabel
 * JD-Core Version:    0.6.2
 */