package com.chase.sig.android.domain;

import com.google.gson.chase.annotations.SerializedName;
import java.io.Serializable;

public class Announcement
  implements Serializable
{

  @SerializedName(a="flag")
  private boolean announcementRequired;
  private String message;
  private String url;

  public final boolean a()
  {
    return this.announcementRequired;
  }

  public final String b()
  {
    return this.message;
  }

  public final String c()
  {
    return this.url;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.Announcement
 * JD-Core Version:    0.6.2
 */