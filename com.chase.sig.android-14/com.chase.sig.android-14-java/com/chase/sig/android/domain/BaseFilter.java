package com.chase.sig.android.domain;

import com.google.gson.chase.annotations.SerializedName;
import java.io.Serializable;
import java.util.ArrayList;

public class BaseFilter
  implements Serializable
{
  String label;

  @SerializedName(a="valueOptionsList")
  ArrayList<BaseFilterOption> optionList;
  String value;
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.BaseFilter
 * JD-Core Version:    0.6.2
 */