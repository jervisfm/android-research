package com.chase.sig.android.domain;

import com.chase.sig.android.util.Dollar;
import java.io.Serializable;

public class BillPayTransaction extends Transaction
  implements Serializable
{
  private Dollar amount;
  private String frequency;
  private String fromAccountId;
  private Payee payee;
  private String payeeId;
  private String paymentModelId;
  private String paymentModelToken;
  private String paymentToken;
  private String sendOnDate;
  private String toAccount;
  private String toAccountNickName;
  private String transactionNumber;

  public final Payee a()
  {
    return this.payee;
  }

  public final void a(Payee paramPayee)
  {
    this.payee = paramPayee;
  }

  public final void a(Dollar paramDollar)
  {
    this.amount = paramDollar;
  }

  public final void a(String paramString)
  {
    this.payeeId = paramString;
  }

  public final String b()
  {
    return this.payeeId;
  }

  public final void b(String paramString)
  {
    this.fromAccountId = paramString;
  }

  public final String c()
  {
    return this.fromAccountId;
  }

  public final void c(String paramString)
  {
    this.sendOnDate = paramString;
  }

  public final String d()
  {
    return this.toAccountNickName;
  }

  public final void d(String paramString)
  {
    this.paymentToken = paramString;
  }

  public final Dollar e()
  {
    return this.amount;
  }

  public final void e(String paramString)
  {
    this.toAccount = paramString;
  }

  public final String f()
  {
    return this.sendOnDate;
  }

  public final void f(String paramString)
  {
    this.toAccountNickName = paramString;
  }

  public final String g()
  {
    return this.paymentToken;
  }

  public final void g(String paramString)
  {
    this.frequency = paramString;
  }

  public final String h()
  {
    return this.toAccountNickName;
  }

  public final void h(String paramString)
  {
    this.paymentModelId = paramString;
  }

  public final String i()
  {
    return this.frequency;
  }

  public final void i(String paramString)
  {
    this.paymentModelToken = paramString;
  }

  public final String j()
  {
    return this.paymentModelId;
  }

  public final String k()
  {
    return this.paymentModelToken;
  }

  public final boolean l()
  {
    return (this.frequency == null) || (this.frequency.equalsIgnoreCase("One-Time"));
  }

  public final String m()
  {
    return this.fromAccountNickName + this.fromAccountMask;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.BillPayTransaction
 * JD-Core Version:    0.6.2
 */