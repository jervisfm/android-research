package com.chase.sig.android.domain;

import java.io.Serializable;

public class BillPayVerifyPayeeInfo
  implements Serializable
{
  private String accountNumber;
  private String amountDue;
  private String amountDueDate;
  private boolean isFromMerchantDirectory;
  private String maskedAccountNumber;
  private MerchantPayee merchantPayee = new MerchantPayee();
  private String message;
  private String payeeNickName;
  private String selectedAccountId;

  public final void a(MerchantPayee paramMerchantPayee)
  {
    this.merchantPayee = paramMerchantPayee;
  }

  public final void a(String paramString)
  {
    this.selectedAccountId = paramString;
  }

  public final void a(boolean paramBoolean)
  {
    this.isFromMerchantDirectory = paramBoolean;
  }

  public final boolean a()
  {
    return this.isFromMerchantDirectory;
  }

  public final String b()
  {
    return this.selectedAccountId;
  }

  public final void b(String paramString)
  {
    this.accountNumber = paramString;
  }

  public final MerchantPayee c()
  {
    return this.merchantPayee;
  }

  public final void c(String paramString)
  {
    this.maskedAccountNumber = paramString;
  }

  public final String d()
  {
    return this.accountNumber;
  }

  public final void d(String paramString)
  {
    this.payeeNickName = paramString;
  }

  public final String e()
  {
    return this.maskedAccountNumber;
  }

  public final void e(String paramString)
  {
    this.message = paramString;
  }

  public final String f()
  {
    return this.payeeNickName;
  }

  public final void f(String paramString)
  {
    this.amountDue = paramString;
  }

  public final String g()
  {
    return this.message;
  }

  public final void g(String paramString)
  {
    this.amountDueDate = paramString;
  }

  public final String h()
  {
    return this.amountDue;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.BillPayVerifyPayeeInfo
 * JD-Core Version:    0.6.2
 */