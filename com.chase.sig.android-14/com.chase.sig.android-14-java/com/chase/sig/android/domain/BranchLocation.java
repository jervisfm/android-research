package com.chase.sig.android.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class BranchLocation
  implements Serializable
{
  private static final String[] a = { "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" };
  private String accessType;
  private String address;
  private String atms;
  private String bank;
  private String city;
  private double distance;
  private List<String> driveUpHours;
  private String label;
  private List<String> languages;
  private double latitude;
  private List<String> lobbyHours;
  private String locationType;
  private double longitude;
  private String name;
  private String phone;
  private List<String> services;
  private String state;
  private String type;
  private String zip;

  private static List<String> e(List<String> paramList)
  {
    ArrayList localArrayList = new ArrayList();
    for (int i = 0; i < a.length; i++)
    {
      String str = (String)paramList.get(i);
      if ("".equalsIgnoreCase(str))
        str = "Closed";
      localArrayList.add(a[i] + " " + str);
    }
    return localArrayList;
  }

  public final String a()
  {
    return this.address;
  }

  public final void a(double paramDouble)
  {
    this.distance = paramDouble;
  }

  public final void a(String paramString)
  {
    this.address = paramString;
  }

  public final void a(List<String> paramList)
  {
    this.driveUpHours = paramList;
  }

  public final String b()
  {
    return this.atms;
  }

  public final void b(double paramDouble)
  {
    this.latitude = paramDouble;
  }

  public final void b(String paramString)
  {
    this.atms = paramString;
  }

  public final void b(List<String> paramList)
  {
    this.lobbyHours = paramList;
  }

  public final String c()
  {
    return this.city;
  }

  public final void c(double paramDouble)
  {
    this.longitude = paramDouble;
  }

  public final void c(String paramString)
  {
    this.bank = paramString;
  }

  public final void c(List<String> paramList)
  {
    this.services = paramList;
  }

  public final String d()
  {
    return this.locationType;
  }

  public final void d(String paramString)
  {
    this.city = paramString;
  }

  public final void d(List<String> paramList)
  {
    this.languages = paramList;
  }

  public final String e()
  {
    return this.name;
  }

  public final void e(String paramString)
  {
    this.label = paramString;
  }

  public final String f()
  {
    return this.phone;
  }

  public final void f(String paramString)
  {
    this.locationType = paramString;
  }

  public final String g()
  {
    return this.state;
  }

  public final void g(String paramString)
  {
    this.name = paramString;
  }

  public final String h()
  {
    return this.zip;
  }

  public final void h(String paramString)
  {
    this.phone = paramString;
  }

  public final String i()
  {
    return this.accessType;
  }

  public final void i(String paramString)
  {
    this.state = paramString;
  }

  public final double j()
  {
    return this.distance;
  }

  public final void j(String paramString)
  {
    this.zip = paramString;
  }

  public final List<String> k()
  {
    return e(this.lobbyHours);
  }

  public final void k(String paramString)
  {
    this.accessType = paramString;
  }

  public final List<String> l()
  {
    return e(this.driveUpHours);
  }

  public final void l(String paramString)
  {
    this.type = paramString;
  }

  public final List<String> m()
  {
    return this.services;
  }

  public final List<String> n()
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = this.languages.iterator();
    while (localIterator.hasNext())
    {
      String str = i.a((String)localIterator.next());
      if (str != null)
        localArrayList.add(str);
    }
    return localArrayList;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.BranchLocation
 * JD-Core Version:    0.6.2
 */