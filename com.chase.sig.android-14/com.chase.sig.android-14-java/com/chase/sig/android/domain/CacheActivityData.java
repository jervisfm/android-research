package com.chase.sig.android.domain;

import java.io.Serializable;
import java.util.HashMap;

public class CacheActivityData
  implements Serializable
{
  HashMap<String, Object> cacheDataMap = new HashMap();

  public final Object a(String paramString)
  {
    return this.cacheDataMap.get(paramString);
  }

  public final void a()
  {
    this.cacheDataMap.clear();
  }

  public final void a(String paramString, Object paramObject)
  {
    this.cacheDataMap.put(paramString, paramObject);
  }

  public final boolean b()
  {
    return this.cacheDataMap.isEmpty();
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.CacheActivityData
 * JD-Core Version:    0.6.2
 */