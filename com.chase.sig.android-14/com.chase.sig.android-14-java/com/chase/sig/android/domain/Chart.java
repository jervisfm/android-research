package com.chase.sig.android.domain;

import java.io.Serializable;

public class Chart
  implements Serializable
{
  public static final Chart a = new Chart("", "", "");
  private String label;
  private String type;
  private String url;

  private Chart(String paramString1, String paramString2, String paramString3)
  {
    this.label = paramString1;
    this.url = paramString2;
    this.type = paramString3;
  }

  public final String a()
  {
    return this.url;
  }

  public boolean equals(Object paramObject)
  {
    if (this == paramObject);
    Chart localChart;
    do
    {
      return true;
      if ((paramObject == null) || (getClass() != paramObject.getClass()))
        return false;
      localChart = (Chart)paramObject;
      if (this.label != null)
      {
        if (this.label.equals(localChart.label));
      }
      else
        while (localChart.label != null)
          return false;
      if (this.type != null)
      {
        if (this.type.equals(localChart.type));
      }
      else
        while (localChart.type != null)
          return false;
      if (this.url == null)
        break;
    }
    while (this.url.equals(localChart.url));
    while (true)
    {
      return false;
      if (localChart.url == null)
        break;
    }
  }

  public int hashCode()
  {
    int i;
    int j;
    if (this.label != null)
    {
      i = this.label.hashCode();
      j = i * 31;
      if (this.url == null)
        break label77;
    }
    label77: for (int k = this.url.hashCode(); ; k = 0)
    {
      int m = 31 * (k + j);
      String str = this.type;
      int n = 0;
      if (str != null)
        n = this.type.hashCode();
      return m + n;
      i = 0;
      break;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.Chart
 * JD-Core Version:    0.6.2
 */