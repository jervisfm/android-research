package com.chase.sig.android.domain;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.util.HashMap;
import java.util.Map;

public class ChartImageCache
  implements Parcelable
{
  public static final Parcelable.Creator<ChartImageCache> CREATOR = new b();
  private Map<Chart, Bitmap> a = new HashMap();

  public ChartImageCache()
  {
  }

  public ChartImageCache(Parcel paramParcel)
  {
    this();
    paramParcel.readMap(this.a, ChartImageCache.class.getClassLoader());
  }

  public final void a(Chart paramChart, Bitmap paramBitmap)
  {
    this.a.put(paramChart, paramBitmap);
  }

  public final boolean a(Chart paramChart)
  {
    return this.a.containsKey(paramChart);
  }

  public final Bitmap b(Chart paramChart)
  {
    return (Bitmap)this.a.get(paramChart);
  }

  public int describeContents()
  {
    return 0;
  }

  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramParcel.writeMap(this.a);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.ChartImageCache
 * JD-Core Version:    0.6.2
 */