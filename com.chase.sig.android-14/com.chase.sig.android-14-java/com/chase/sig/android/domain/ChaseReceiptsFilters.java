package com.chase.sig.android.domain;

import com.google.gson.chase.annotations.SerializedName;
import java.io.Serializable;

public class ChaseReceiptsFilters
  implements Serializable
{

  @SerializedName(a="categoryFilter")
  BaseFilter categoryFilter;

  @SerializedName(a="dateFilter")
  BaseFilter dateFilter;

  @SerializedName(a="expenseTypeFilter")
  BaseFilter expenseTypeFilter;

  @SerializedName(a="matchFilter")
  BaseFilter matchFilter;

  @SerializedName(a="receiptsFilter")
  BaseFilter receiptFilter;

  @SerializedName(a="taxDeductibleFilter")
  BaseFilter taxDeductibleFilter;
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.ChaseReceiptsFilters
 * JD-Core Version:    0.6.2
 */