package com.chase.sig.android.domain;

import com.chase.sig.android.util.Dollar;
import com.chase.sig.android.util.s;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;

public class CreditFacilityAccountDetail
  implements Serializable
{
  private Dollar available;
  private Dollar creditLimit;
  private String displayType;
  private String mask;
  private String name;
  private Dollar outstandingValue;
  private List<CreditFacilityAccountLoanDetail> sortedLoanDetails;

  public final String a()
  {
    return s.a(this.creditLimit);
  }

  public final void a(Dollar paramDollar)
  {
    this.creditLimit = paramDollar;
  }

  public final void a(String paramString)
  {
    this.displayType = paramString;
  }

  public final void a(List<CreditFacilityAccountLoanDetail> paramList)
  {
    this.sortedLoanDetails = paramList;
    Collections.sort(this.sortedLoanDetails);
  }

  public final String b()
  {
    return s.a(this.available);
  }

  public final void b(Dollar paramDollar)
  {
    this.available = paramDollar;
  }

  public final void b(String paramString)
  {
    this.mask = paramString;
  }

  public final String c()
  {
    return s.a(this.displayType);
  }

  public final void c(Dollar paramDollar)
  {
    this.outstandingValue = paramDollar;
  }

  public final void c(String paramString)
  {
    this.name = paramString;
  }

  public final String d()
  {
    return this.name;
  }

  public final String e()
  {
    return s.a(this.outstandingValue);
  }

  public final List<CreditFacilityAccountLoanDetail> f()
  {
    return this.sortedLoanDetails;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.CreditFacilityAccountDetail
 * JD-Core Version:    0.6.2
 */