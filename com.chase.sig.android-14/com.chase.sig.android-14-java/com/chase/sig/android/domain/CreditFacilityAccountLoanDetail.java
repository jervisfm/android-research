package com.chase.sig.android.domain;

import com.chase.sig.android.util.Dollar;
import com.chase.sig.android.util.s;
import java.io.Serializable;
import java.util.Hashtable;

public class CreditFacilityAccountLoanDetail
  implements Serializable, Comparable<CreditFacilityAccountLoanDetail>
{
  private a accruedInt;
  private Double intRate;
  private String loanNumMask;
  private String loanType;
  private String maturityDate;
  private a outstanding;

  private Integer g()
  {
    return Integer.valueOf(Integer.parseInt(this.loanNumMask.replace("...", "")));
  }

  public final String a()
  {
    return s.a(this.loanNumMask);
  }

  public final void a(Double paramDouble)
  {
    this.intRate = paramDouble;
  }

  public final void a(String paramString)
  {
    this.loanNumMask = paramString;
  }

  public final void a(Hashtable<String, String> paramHashtable)
  {
    this.outstanding = new a(paramHashtable);
  }

  public final String b()
  {
    return s.a(this.loanType);
  }

  public final void b(String paramString)
  {
    this.loanType = paramString;
  }

  public final void b(Hashtable<String, String> paramHashtable)
  {
    this.accruedInt = new a(paramHashtable);
  }

  public final String c()
  {
    return s.a(this.intRate);
  }

  public final void c(String paramString)
  {
    this.maturityDate = paramString;
  }

  public final String d()
  {
    return s.k(this.maturityDate);
  }

  public final String e()
  {
    return s.a(this.outstanding.a);
  }

  public final String f()
  {
    return s.a(this.accruedInt.a);
  }

  private final class a
    implements Serializable
  {
    final Dollar a;

    public a()
    {
      Object localObject;
      this.a = new Dollar((String)localObject.get("value"));
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.CreditFacilityAccountLoanDetail
 * JD-Core Version:    0.6.2
 */