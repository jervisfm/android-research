package com.chase.sig.android.domain;

import java.io.Serializable;

public class Disclosure
  implements Serializable
{
  private String disclosureId;
  private String disclosureText;
  private String disclosureTitle;

  public final String a()
  {
    return this.disclosureId;
  }

  public final void a(String paramString)
  {
    this.disclosureId = paramString;
  }

  public final String b()
  {
    return this.disclosureText;
  }

  public final void b(String paramString)
  {
    this.disclosureText = paramString;
  }

  public final String c()
  {
    return this.disclosureTitle;
  }

  public final void c(String paramString)
  {
    this.disclosureTitle = paramString;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.Disclosure
 * JD-Core Version:    0.6.2
 */