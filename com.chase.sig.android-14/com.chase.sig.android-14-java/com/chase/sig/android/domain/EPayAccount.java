package com.chase.sig.android.domain;

import java.io.Serializable;

public class EPayAccount
  implements Serializable
{
  private String id;
  private String nicknameAndMask;

  public final String a()
  {
    return this.nicknameAndMask;
  }

  public final void a(String paramString)
  {
    this.nicknameAndMask = paramString;
  }

  public final String b()
  {
    if ((this.nicknameAndMask == null) || (this.nicknameAndMask.indexOf("(...") < 0));
    for (int i = 1; i != 0; i = 0)
      return "";
    int j = this.nicknameAndMask.indexOf("(...");
    return this.nicknameAndMask.substring(j + 1, -1 + this.nicknameAndMask.length());
  }

  public final void b(String paramString)
  {
    this.id = paramString;
  }

  public final String c()
  {
    if (this.nicknameAndMask == null)
      return "";
    int i = this.nicknameAndMask.indexOf("(...");
    if (i < 0)
      return this.nicknameAndMask;
    return this.nicknameAndMask.substring(0, i);
  }

  public final String d()
  {
    return this.id;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.EPayAccount
 * JD-Core Version:    0.6.2
 */