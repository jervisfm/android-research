package com.chase.sig.android.domain;

import com.chase.sig.android.util.Dollar;
import java.io.Serializable;

public class EPayment
  implements Serializable
{
  private String agreement;
  private Dollar amount;
  private String confirmationNumber;
  private String deliverByDate;
  private String formId;
  private String fromAccountId;
  private String fromAccountMask;
  private String fromAccountNickname;
  private String optionId;
  private String toAccountId;

  public final String a()
  {
    return this.confirmationNumber;
  }

  public final void a(Dollar paramDollar)
  {
    this.amount = paramDollar;
  }

  public final void a(String paramString)
  {
    this.confirmationNumber = paramString;
  }

  public final String b()
  {
    return this.formId;
  }

  public final void b(String paramString)
  {
    this.formId = paramString;
  }

  public final String c()
  {
    return this.fromAccountId;
  }

  public final void c(String paramString)
  {
    this.fromAccountId = paramString;
  }

  public final Dollar d()
  {
    return this.amount;
  }

  public final void d(String paramString)
  {
    this.deliverByDate = paramString;
  }

  public final String e()
  {
    return this.deliverByDate;
  }

  public final void e(String paramString)
  {
    this.toAccountId = paramString;
  }

  public final String f()
  {
    return this.toAccountId;
  }

  public final void f(String paramString)
  {
    this.optionId = paramString;
  }

  public final String g()
  {
    return this.optionId;
  }

  public final void g(String paramString)
  {
    this.agreement = paramString;
  }

  public final String h()
  {
    return this.agreement;
  }

  public final void h(String paramString)
  {
    this.fromAccountMask = paramString;
  }

  public final CharSequence i()
  {
    Object[] arrayOfObject = new Object[2];
    arrayOfObject[0] = this.fromAccountNickname;
    arrayOfObject[1] = this.fromAccountMask;
    return String.format("%s (%s)", arrayOfObject);
  }

  public final void i(String paramString)
  {
    this.fromAccountNickname = paramString;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.EPayment
 * JD-Core Version:    0.6.2
 */