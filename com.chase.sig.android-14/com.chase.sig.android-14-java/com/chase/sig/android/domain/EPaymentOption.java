package com.chase.sig.android.domain;

import com.chase.sig.android.util.Dollar;
import java.io.Serializable;

public class EPaymentOption
  implements Serializable
{
  private Dollar amount;
  private boolean amountRequired;
  private String description;
  private String id;
  private String label;
  private String note;
  private String shortDescription;

  public final Dollar a()
  {
    return this.amount;
  }

  public final void a(Dollar paramDollar)
  {
    this.amount = paramDollar;
  }

  public final void a(String paramString)
  {
    this.shortDescription = paramString;
  }

  public final void a(boolean paramBoolean)
  {
    this.amountRequired = paramBoolean;
  }

  public final String b()
  {
    return this.label;
  }

  public final void b(String paramString)
  {
    this.label = paramString;
  }

  public final String c()
  {
    return this.note;
  }

  public final void c(String paramString)
  {
    this.description = paramString;
  }

  public final String d()
  {
    return this.id;
  }

  public final void d(String paramString)
  {
    this.note = paramString;
  }

  public final void e(String paramString)
  {
    this.id = paramString;
  }

  public String toString()
  {
    return this.label;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.EPaymentOption
 * JD-Core Version:    0.6.2
 */