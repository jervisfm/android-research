package com.chase.sig.android.domain;

import java.io.Serializable;

public class Email
  implements Serializable
{
  private String email;
  private String id;
  private boolean isDefault;

  public Email(String paramString1, boolean paramBoolean, String paramString2)
  {
    this.email = paramString1;
    this.isDefault = paramBoolean;
    this.id = paramString2;
  }

  public final String a()
  {
    return this.email;
  }

  public final void a(boolean paramBoolean)
  {
    this.isDefault = paramBoolean;
  }

  public final boolean b()
  {
    return this.isDefault;
  }

  public final String c()
  {
    return this.id;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.Email
 * JD-Core Version:    0.6.2
 */