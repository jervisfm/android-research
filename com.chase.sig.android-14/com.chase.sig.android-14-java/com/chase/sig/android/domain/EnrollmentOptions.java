package com.chase.sig.android.domain;

import java.io.Serializable;
import java.util.List;

public class EnrollmentOptions
  implements Serializable
{
  int fundingAccountId;
  List<FundingAccount> fundingAccountsList;
  int imageCount;
  List<ReceiptsPricingPlan> pricingPlansList;

  public final int a()
  {
    return this.imageCount;
  }

  public final List<FundingAccount> b()
  {
    return this.fundingAccountsList;
  }

  public final List<ReceiptsPricingPlan> c()
  {
    return this.pricingPlansList;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.EnrollmentOptions
 * JD-Core Version:    0.6.2
 */