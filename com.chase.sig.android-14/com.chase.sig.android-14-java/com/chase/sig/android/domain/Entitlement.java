package com.chase.sig.android.domain;

import java.io.Serializable;

public class Entitlement
  implements Serializable
{
  private String id;
  private Privilege priviledges;

  public String toString()
  {
    Object[] arrayOfObject = new Object[2];
    arrayOfObject[0] = this.id;
    arrayOfObject[1] = this.priviledges.toString();
    return String.format("[id=%s, priviledges=%s]", arrayOfObject);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.Entitlement
 * JD-Core Version:    0.6.2
 */