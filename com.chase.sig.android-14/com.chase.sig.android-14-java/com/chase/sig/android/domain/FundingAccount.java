package com.chase.sig.android.domain;

import com.chase.sig.android.util.Dollar;
import java.io.Serializable;

public class FundingAccount
  implements Serializable
{
  int accountId;
  Dollar availableBalance;
  String mask;
  String nickname;

  public final int a()
  {
    return this.accountId;
  }

  public final Dollar b()
  {
    return this.availableBalance;
  }

  public final String c()
  {
    return this.nickname + "(" + this.mask + ")";
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.FundingAccount
 * JD-Core Version:    0.6.2
 */