package com.chase.sig.android.domain;

import com.chase.sig.android.util.Dollar;
import java.io.Serializable;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;

public abstract interface IAccount extends Serializable
{
  public abstract boolean A();

  public abstract CharSequence B();

  public abstract boolean C();

  public abstract boolean D();

  public abstract boolean E();

  public abstract String F();

  public abstract boolean G();

  public abstract boolean H();

  public abstract boolean I();

  public abstract Set<String> J();

  public abstract List<f> K();

  public abstract String L();

  public abstract boolean M();

  public abstract String a();

  public abstract String a(boolean paramBoolean);

  public abstract void a(String paramString);

  public abstract void a(Hashtable<String, String> paramHashtable);

  public abstract void a(List<IAccount> paramList);

  public abstract void a(Set<String> paramSet);

  public abstract boolean a(String[] paramArrayOfString);

  public abstract String b();

  public abstract void b(String paramString);

  public abstract void b(List<f> paramList);

  public abstract void b(Set<String> paramSet);

  public abstract void b(boolean paramBoolean);

  public abstract f c(String paramString);

  public abstract String c();

  public abstract void c(boolean paramBoolean);

  public abstract String d();

  public abstract void d(String paramString);

  public abstract void d(boolean paramBoolean);

  public abstract Hashtable<String, String> e();

  public abstract void e(String paramString);

  public abstract void e(boolean paramBoolean);

  public abstract boolean f();

  public abstract void g(String paramString);

  public abstract boolean g();

  public abstract boolean h();

  public abstract boolean i();

  public abstract Dollar j();

  public abstract Dollar k();

  public abstract boolean l();

  public abstract boolean m();

  public abstract boolean n();

  public abstract boolean o();

  public abstract boolean p();

  public abstract boolean q();

  public abstract boolean r();

  public abstract int s();

  public abstract boolean t();

  public abstract boolean u();

  public abstract List<IAccount> v();

  public abstract CharSequence w();

  public abstract IAccount x();

  public abstract CharSequence y();

  public abstract boolean z();
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.IAccount
 * JD-Core Version:    0.6.2
 */