package com.chase.sig.android.domain;

import java.io.Serializable;

public class ImageCaptureData
  implements Serializable
{
  private String accountNumber;
  private String addressLine1;
  private String addressLine2;
  private String amountDue;
  private String balanceDueAmount;
  private String city;
  private String dueDate;
  private String invoiceNumber;
  private String minimumAmountDue;
  private String name;
  private String postalCode;
  private State state;

  public final String a()
  {
    return this.name;
  }

  public final void a(State paramState)
  {
    this.state = paramState;
  }

  public final void a(String paramString)
  {
    this.name = paramString;
  }

  public final String b()
  {
    return this.accountNumber;
  }

  public final void b(String paramString)
  {
    this.accountNumber = paramString;
  }

  public final String c()
  {
    return this.amountDue;
  }

  public final void c(String paramString)
  {
    this.balanceDueAmount = paramString;
  }

  public final String d()
  {
    return this.dueDate;
  }

  public final void d(String paramString)
  {
    this.amountDue = paramString;
  }

  public final String e()
  {
    return this.addressLine1;
  }

  public final void e(String paramString)
  {
    this.minimumAmountDue = paramString;
  }

  public final String f()
  {
    return this.addressLine2;
  }

  public final void f(String paramString)
  {
    this.invoiceNumber = paramString;
  }

  public final String g()
  {
    return this.city;
  }

  public final void g(String paramString)
  {
    this.dueDate = paramString;
  }

  public final State h()
  {
    return this.state;
  }

  public final void h(String paramString)
  {
    this.addressLine1 = paramString;
  }

  public final String i()
  {
    return this.postalCode;
  }

  public final void i(String paramString)
  {
    this.addressLine2 = paramString;
  }

  public final void j(String paramString)
  {
    this.city = paramString;
  }

  public final void k(String paramString)
  {
    this.postalCode = paramString;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.ImageCaptureData
 * JD-Core Version:    0.6.2
 */