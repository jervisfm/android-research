package com.chase.sig.android.domain;

import android.graphics.Bitmap;
import com.chase.sig.android.service.l;

public class ImageDownloadResponse extends l
{
  public static ImageDownloadResponse a = new ImageDownloadResponse();
  private Bitmap bitmap;

  public final Bitmap a()
  {
    return this.bitmap;
  }

  public final void a(Bitmap paramBitmap)
  {
    this.bitmap = paramBitmap;
  }

  public final boolean b()
  {
    return (!e()) && (this.bitmap != null);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.ImageDownloadResponse
 * JD-Core Version:    0.6.2
 */