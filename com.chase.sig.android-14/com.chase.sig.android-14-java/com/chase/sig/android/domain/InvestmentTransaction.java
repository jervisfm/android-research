package com.chase.sig.android.domain;

import com.chase.sig.android.util.Dollar;
import java.io.Serializable;

public class InvestmentTransaction
  implements Serializable
{
  private Dollar amount;
  private String amountCurrency;
  private String asOfDate;
  private Dollar cost;
  private String costCurrency;
  private String date;
  private String description;
  private boolean isIntraDay;
  private boolean isTrade;
  private Dollar price;
  private double quantity;
  private String settleDate;
  private String symbol;
  private String tradeDate;
  private String transactionType;

  public final Dollar a()
  {
    return this.amount;
  }

  public final void a(double paramDouble)
  {
    this.quantity = paramDouble;
  }

  public final void a(Dollar paramDollar)
  {
    this.amount = paramDollar;
  }

  public final void a(String paramString)
  {
    this.date = paramString;
  }

  public final void a(boolean paramBoolean)
  {
    this.isIntraDay = paramBoolean;
  }

  public final String b()
  {
    return this.date;
  }

  public final void b(Dollar paramDollar)
  {
    this.price = paramDollar;
  }

  public final void b(String paramString)
  {
    this.asOfDate = paramString;
  }

  public final void b(boolean paramBoolean)
  {
    this.isTrade = paramBoolean;
  }

  public final double c()
  {
    return this.quantity;
  }

  public final void c(Dollar paramDollar)
  {
    this.cost = paramDollar;
  }

  public final void c(String paramString)
  {
    this.transactionType = paramString;
  }

  public final void d(String paramString)
  {
    this.description = paramString;
  }

  public final boolean d()
  {
    return this.isTrade;
  }

  public final String e()
  {
    return this.transactionType;
  }

  public final void e(String paramString)
  {
    this.tradeDate = paramString;
  }

  public final String f()
  {
    return this.description;
  }

  public final void f(String paramString)
  {
    this.settleDate = paramString;
  }

  public final String g()
  {
    return this.tradeDate;
  }

  public final void g(String paramString)
  {
    this.symbol = paramString;
  }

  public final String h()
  {
    return this.settleDate;
  }

  public final void h(String paramString)
  {
    this.amountCurrency = paramString;
  }

  public final String i()
  {
    return this.symbol;
  }

  public final void i(String paramString)
  {
    this.costCurrency = paramString;
  }

  public final Dollar j()
  {
    return this.price;
  }

  public final Dollar k()
  {
    return this.cost;
  }

  public final String l()
  {
    return this.amountCurrency;
  }

  public final String m()
  {
    return this.costCurrency;
  }

  public class FilterType
    implements Serializable
  {
    private String code;
    private String name;
    private int sortOrder;
    private String value;

    static
    {
      if (!InvestmentTransaction.class.desiredAssertionStatus());
      for (boolean bool = true; ; bool = false)
      {
        a = bool;
        return;
      }
    }

    public FilterType()
    {
    }

    public final String a()
    {
      return this.code;
    }

    public final void a(int paramInt)
    {
      this.sortOrder = paramInt;
    }

    public final void a(String paramString)
    {
      this.name = paramString;
    }

    public final String b()
    {
      return this.value;
    }

    public final void b(String paramString)
    {
      this.code = paramString;
    }

    public final void c(String paramString)
    {
      this.value = paramString;
    }

    public boolean equals(Object paramObject)
    {
      if ((paramObject instanceof FilterType))
      {
        FilterType localFilterType = (FilterType)paramObject;
        return (localFilterType.code.equals(this.code)) && (localFilterType.value.equals(this.value));
      }
      return super.equals(paramObject);
    }

    public int hashCode()
    {
      if (!a)
        throw new AssertionError("hashCode not designed");
      return 42;
    }

    public String toString()
    {
      return this.name;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.InvestmentTransaction
 * JD-Core Version:    0.6.2
 */