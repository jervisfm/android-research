package com.chase.sig.android.domain;

import com.chase.sig.android.domain.ui.Action;
import java.io.Serializable;
import java.util.List;

public class Item
  implements f, Serializable
{
  private List<Action> actions;
  private String label;
  private String subLabel;
  private String type;
  private Account.Value value;

  public final String a()
  {
    return this.label;
  }

  public final String b()
  {
    return this.subLabel;
  }

  public final Account.Value c()
  {
    return this.value;
  }

  public final String d()
  {
    return this.type;
  }

  public final List<Action> e()
  {
    return this.actions;
  }

  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("Item [label=");
    localStringBuilder.append(this.label);
    localStringBuilder.append(", subLabel=");
    localStringBuilder.append(this.subLabel);
    localStringBuilder.append(", value=");
    localStringBuilder.append(this.value);
    localStringBuilder.append("]");
    return localStringBuilder.toString();
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.Item
 * JD-Core Version:    0.6.2
 */