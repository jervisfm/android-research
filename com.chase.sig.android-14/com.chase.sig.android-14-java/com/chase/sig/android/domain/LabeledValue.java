package com.chase.sig.android.domain;

import java.io.Serializable;

public class LabeledValue
  implements Serializable
{
  private String label;
  private String value;

  public LabeledValue()
  {
  }

  public LabeledValue(String paramString1, String paramString2)
  {
    this.label = paramString1;
    this.value = paramString2;
  }

  public final String a()
  {
    return this.label;
  }

  public final void a(String paramString)
  {
    this.label = paramString;
  }

  public final String b()
  {
    return this.value;
  }

  public final void b(String paramString)
  {
    this.value = paramString;
  }

  public String toString()
  {
    return this.label;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.LabeledValue
 * JD-Core Version:    0.6.2
 */