package com.chase.sig.android.domain;

public enum MMBFrequency
{
  private String formattedValue;
  private String labelValue;

  static
  {
    MMBFrequency[] arrayOfMMBFrequency = new MMBFrequency[6];
    arrayOfMMBFrequency[0] = a;
    arrayOfMMBFrequency[1] = b;
    arrayOfMMBFrequency[2] = c;
    arrayOfMMBFrequency[3] = d;
    arrayOfMMBFrequency[4] = e;
    arrayOfMMBFrequency[5] = f;
  }

  private MMBFrequency(String paramString1, String paramString2)
  {
    this.labelValue = paramString1;
    this.formattedValue = paramString2;
  }

  public static int a(MMBFrequency paramMMBFrequency)
  {
    if (paramMMBFrequency == null)
      return -1;
    return paramMMBFrequency.ordinal();
  }

  public static MMBFrequency a(String paramString)
  {
    for (MMBFrequency localMMBFrequency : values())
      if (localMMBFrequency.labelValue.equals(paramString))
        return localMMBFrequency;
    return null;
  }

  public static String[] a()
  {
    String[] arrayOfString = new String[6];
    arrayOfString[0] = a.formattedValue;
    arrayOfString[1] = b.formattedValue;
    arrayOfString[2] = c.formattedValue;
    arrayOfString[3] = d.formattedValue;
    arrayOfString[4] = e.formattedValue;
    arrayOfString[5] = f.formattedValue;
    return arrayOfString;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.MMBFrequency
 * JD-Core Version:    0.6.2
 */