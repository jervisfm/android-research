package com.chase.sig.android.domain;

import java.io.Serializable;
import java.util.ArrayList;

public class MaintenanceInfo
  implements Serializable
{
  private Announcement announcement;
  private ArrayList<SplashInfo> featuresBlocked;

  public final Announcement a()
  {
    return this.announcement;
  }

  public final ArrayList<SplashInfo> b()
  {
    return this.featuresBlocked;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.MaintenanceInfo
 * JD-Core Version:    0.6.2
 */