package com.chase.sig.android.domain;

import java.io.Serializable;

public class MerchantPayee
  implements Serializable
{
  private String addressLine1;
  private String addressLine2;
  private String city;
  private String name;
  private String optionId;
  private String phone;
  private State state;
  private String zipCode;

  public final String a()
  {
    return this.optionId;
  }

  public final void a(State paramState)
  {
    this.state = paramState;
  }

  public final void a(String paramString)
  {
    this.optionId = paramString;
  }

  public final String b()
  {
    return this.name;
  }

  public final void b(String paramString)
  {
    this.name = paramString;
  }

  public final String c()
  {
    return this.phone;
  }

  public final void c(String paramString)
  {
    this.phone = paramString;
  }

  public final String d()
  {
    return this.addressLine1;
  }

  public final void d(String paramString)
  {
    this.addressLine1 = paramString;
  }

  public final String e()
  {
    return this.addressLine2;
  }

  public final void e(String paramString)
  {
    this.addressLine2 = paramString;
  }

  public final String f()
  {
    return this.city;
  }

  public final void f(String paramString)
  {
    this.city = paramString;
  }

  public final State g()
  {
    return this.state;
  }

  public final void g(String paramString)
  {
    this.zipCode = paramString;
  }

  public final String h()
  {
    return this.zipCode;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.MerchantPayee
 * JD-Core Version:    0.6.2
 */