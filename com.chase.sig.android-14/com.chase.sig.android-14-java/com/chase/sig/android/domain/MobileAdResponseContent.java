package com.chase.sig.android.domain;

import java.io.Serializable;

public class MobileAdResponseContent
  implements Serializable
{
  private ResponseActions actions;
  private String phoneNumber;
  private String url;

  public final String a()
  {
    return this.url;
  }

  public final ResponseActions b()
  {
    return this.actions;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.MobileAdResponseContent
 * JD-Core Version:    0.6.2
 */