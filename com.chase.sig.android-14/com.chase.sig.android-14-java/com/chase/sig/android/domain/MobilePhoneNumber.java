package com.chase.sig.android.domain;

import java.io.Serializable;

public class MobilePhoneNumber
  implements Serializable
{
  private String id;
  private String label;
  private String mobileNumber;

  public MobilePhoneNumber(String paramString1, String paramString2, String paramString3)
  {
    this.mobileNumber = paramString1;
    this.id = paramString2;
    this.label = paramString3;
  }

  public static String d(String paramString)
  {
    if (paramString == null)
      return paramString;
    String str = "";
    int i = 0;
    int j = 1;
    if (i < paramString.length())
    {
      char c = paramString.charAt(i);
      if ((c == '0') && (j == 0))
        str = str + c;
      while (true)
      {
        i++;
        break;
        if ((c >= '1') && (c <= '9'))
        {
          str = str + c;
          j = 0;
        }
      }
    }
    if ((str.startsWith("1")) && (str.length() > 9))
      str = str.substring(1);
    return str;
  }

  public final String a()
  {
    return this.mobileNumber;
  }

  public final void a(String paramString)
  {
    this.mobileNumber = paramString;
  }

  public final String b()
  {
    return d(this.mobileNumber);
  }

  public final void b(String paramString)
  {
    this.id = paramString;
  }

  public final String c()
  {
    return this.id;
  }

  public final void c(String paramString)
  {
    this.label = paramString;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.MobilePhoneNumber
 * JD-Core Version:    0.6.2
 */