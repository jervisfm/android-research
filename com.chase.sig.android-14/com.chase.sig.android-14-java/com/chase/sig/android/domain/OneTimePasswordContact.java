package com.chase.sig.android.domain;

import java.io.Serializable;

public class OneTimePasswordContact
  implements Serializable
{
  private String id;
  private String label;
  private String mask;
  private String type;

  public OneTimePasswordContact(String paramString1, String paramString2, String paramString3)
  {
    this.id = paramString1;
    this.mask = paramString2;
    this.type = paramString3;
  }

  public final String a()
  {
    return this.id;
  }

  public final String b()
  {
    return this.mask;
  }

  public final String c()
  {
    return this.type;
  }

  public final String d()
  {
    if (this.type.equals("EMAIL"))
      this.label = "Email";
    while (true)
    {
      return this.label;
      if (this.type.equals("PHONE"))
        this.label = "Call";
      else if (this.type.equals("TEXT"))
        this.label = "Text";
    }
  }

  public String toString()
  {
    String str = "???";
    if (this.type.equals("EMAIL"))
      str = "EMAIL";
    while (true)
    {
      return str + " : " + this.mask;
      if (this.type.equals("PHONE"))
        str = "CALL";
      else if (this.type.equals("TEXT"))
        str = "TEXT";
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.OneTimePasswordContact
 * JD-Core Version:    0.6.2
 */