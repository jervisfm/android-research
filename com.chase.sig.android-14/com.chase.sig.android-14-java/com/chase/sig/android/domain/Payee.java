package com.chase.sig.android.domain;

import com.chase.sig.android.util.Dollar;
import com.chase.sig.android.util.s;
import java.io.Serializable;
import java.util.Date;

public class Payee
  implements Serializable
{
  private String accountNumber;
  private String accountNumberMask;
  private PayeeAddress address = new PayeeAddress();
  private String bankInstructions;
  private String earliestPaymentDeliveryDate;
  private String earliestPaymentSendDate;
  private Dollar lastPaymentAmount;
  private String lastPaymentDate;
  private int leadTime = -1;
  private String memo;
  private String name;
  private String nickName;
  private String payeeId;
  private String payeeInstructions;
  private String phoneNumber;
  private String status;
  private String token;

  public final int a()
  {
    return this.leadTime;
  }

  public final void a(PayeeAddress paramPayeeAddress)
  {
    this.address = paramPayeeAddress;
  }

  public final void a(String paramString)
  {
    this.accountNumber = paramString;
  }

  public final String b()
  {
    switch (this.leadTime)
    {
    case 3:
    case 4:
    default:
      return "";
    case 0:
      return "Same day";
    case 1:
      return "1-day";
    case 2:
      return "2-day";
    case 5:
    }
    return "5-day";
  }

  public final void b(String paramString)
  {
    this.bankInstructions = paramString;
  }

  public final String c()
  {
    return this.accountNumber;
  }

  public final void c(String paramString)
  {
    this.earliestPaymentDeliveryDate = paramString;
  }

  public final String d()
  {
    return this.accountNumberMask;
  }

  public final void d(String paramString)
  {
    this.earliestPaymentSendDate = paramString;
  }

  public final String e()
  {
    return this.earliestPaymentDeliveryDate;
  }

  public final void e(String paramString)
  {
    this.memo = paramString;
  }

  public final Date f()
  {
    return s.g(this.earliestPaymentDeliveryDate);
  }

  public final void f(String paramString)
  {
    this.nickName = paramString;
  }

  public final String g()
  {
    return this.memo;
  }

  public final void g(String paramString)
  {
    this.name = paramString;
  }

  public final String h()
  {
    return this.nickName;
  }

  public final void h(String paramString)
  {
    this.payeeId = paramString;
  }

  public final String i()
  {
    return this.name;
  }

  public final void i(String paramString)
  {
    this.payeeInstructions = paramString;
  }

  public final String j()
  {
    return this.payeeId;
  }

  public final void j(String paramString)
  {
    this.phoneNumber = paramString;
  }

  public final String k()
  {
    String str = this.lastPaymentDate;
    if ((s.l(str)) || (str.equals("null")))
      return "No activity";
    return s.h(str);
  }

  public final String l()
  {
    if ((this.lastPaymentAmount == null) || (s.l(this.lastPaymentAmount.g())))
      return "--";
    return this.lastPaymentAmount.h();
  }

  public final String m()
  {
    return this.status;
  }

  public final String n()
  {
    if (this.token != null)
      return this.token;
    return "";
  }

  public final PayeeAddress o()
  {
    return this.address;
  }

  public final String p()
  {
    return this.phoneNumber;
  }

  public String toString()
  {
    return this.nickName;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.Payee
 * JD-Core Version:    0.6.2
 */