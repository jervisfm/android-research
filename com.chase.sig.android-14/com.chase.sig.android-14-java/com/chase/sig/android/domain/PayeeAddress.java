package com.chase.sig.android.domain;

import java.io.Serializable;

public class PayeeAddress
  implements Serializable
{
  private String addressLine1;
  private String addressLine2;
  private String city;
  private String countryCode;
  private String postalCode;
  private State state;

  public final String a()
  {
    return this.addressLine1;
  }

  public final void a(State paramState)
  {
    this.state = paramState;
  }

  public final void a(String paramString)
  {
    this.addressLine1 = paramString;
  }

  public final String b()
  {
    return this.addressLine2;
  }

  public final void b(String paramString)
  {
    this.addressLine2 = paramString;
  }

  public final String c()
  {
    return this.city;
  }

  public final void c(String paramString)
  {
    this.city = paramString;
  }

  public final String d()
  {
    return this.postalCode;
  }

  public final void d(String paramString)
  {
    this.postalCode = paramString;
  }

  public final State e()
  {
    return this.state;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.PayeeAddress
 * JD-Core Version:    0.6.2
 */