package com.chase.sig.android.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class PhoneBookContact
  implements Serializable
{
  private int contactId;
  private String displayName;
  private List<RecipientContact> emailAddresses = new ArrayList();
  private List<RecipientContact> phoneNumbers = new ArrayList();

  private static List<RecipientContact> c(List<RecipientContact> paramList)
  {
    Hashtable localHashtable = new Hashtable();
    Iterator localIterator = paramList.iterator();
    while (localIterator.hasNext())
    {
      RecipientContact localRecipientContact = (RecipientContact)localIterator.next();
      localHashtable.put(localRecipientContact.b(), localRecipientContact);
    }
    return new ArrayList(localHashtable.values());
  }

  public final List<RecipientContact> a()
  {
    return this.emailAddresses;
  }

  public final void a(String paramString)
  {
    this.displayName = paramString;
  }

  public final void a(List<RecipientContact> paramList)
  {
    this.emailAddresses = c(paramList);
  }

  public final List<RecipientContact> b()
  {
    return this.phoneNumbers;
  }

  public final void b(List<RecipientContact> paramList)
  {
    this.phoneNumbers = c(paramList);
  }

  public final String c()
  {
    return this.displayName;
  }

  public final boolean d()
  {
    return (this.emailAddresses != null) && (this.emailAddresses.size() > 0);
  }

  public final boolean e()
  {
    return (this.phoneNumbers != null) && (this.phoneNumbers.size() > 0);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.PhoneBookContact
 * JD-Core Version:    0.6.2
 */