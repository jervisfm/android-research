package com.chase.sig.android.domain;

import com.chase.sig.android.util.Dollar;
import com.chase.sig.android.util.s;
import java.io.Serializable;

public class Position
  implements Serializable, Comparable<Position>
{
  private String assetClass1;
  private String assetClass2;
  private Dollar cost;
  private String cusip;
  private String description;
  private boolean isShowAssetClass1;
  private boolean isShowCost;
  private boolean isShowDescription;
  private boolean isShowPrice;
  private boolean isShowPriceDate;
  private boolean isShowQuantity;
  private boolean isShowShortName;
  private boolean isShowSymbol;
  private boolean isShowUnrealizedChange;
  private boolean isShowValue;
  private Dollar price;
  private String priceAsOfDate;
  private String quantity;
  private String quoteCode;
  private String shortName;
  private String tickerSymbol;
  private Dollar unrealizedChange;
  private Dollar value;
  private Dollar valueChange;

  public final Dollar a()
  {
    return this.value;
  }

  public final void a(Dollar paramDollar)
  {
    this.value = paramDollar;
  }

  public final void a(String paramString)
  {
    this.shortName = paramString;
  }

  public final void a(boolean paramBoolean)
  {
    this.isShowAssetClass1 = paramBoolean;
  }

  public final String b()
  {
    return this.shortName;
  }

  public final void b(Dollar paramDollar)
  {
    this.price = paramDollar;
  }

  public final void b(String paramString)
  {
    this.quantity = paramString;
  }

  public final void b(boolean paramBoolean)
  {
    this.isShowCost = paramBoolean;
  }

  public final Dollar c()
  {
    return this.price;
  }

  public final void c(Dollar paramDollar)
  {
    this.unrealizedChange = paramDollar;
  }

  public final void c(String paramString)
  {
    this.description = paramString;
  }

  public final void c(boolean paramBoolean)
  {
    this.isShowDescription = paramBoolean;
  }

  public final Dollar d()
  {
    return this.unrealizedChange;
  }

  public final void d(Dollar paramDollar)
  {
    this.cost = paramDollar;
  }

  public final void d(String paramString)
  {
    this.tickerSymbol = paramString;
  }

  public final void d(boolean paramBoolean)
  {
    this.isShowPrice = paramBoolean;
  }

  public final Dollar e()
  {
    return this.cost;
  }

  public final void e(Dollar paramDollar)
  {
    this.valueChange = paramDollar;
  }

  public final void e(String paramString)
  {
    this.cusip = paramString;
  }

  public final void e(boolean paramBoolean)
  {
    this.isShowPriceDate = paramBoolean;
  }

  public final String f()
  {
    return this.quantity;
  }

  public final void f(String paramString)
  {
    this.assetClass1 = paramString;
  }

  public final void f(boolean paramBoolean)
  {
    this.isShowQuantity = paramBoolean;
  }

  public final String g()
  {
    return this.description;
  }

  public final void g(String paramString)
  {
    this.assetClass2 = paramString;
  }

  public final void g(boolean paramBoolean)
  {
    this.isShowShortName = paramBoolean;
  }

  public final void h(String paramString)
  {
    this.priceAsOfDate = paramString;
  }

  public final void h(boolean paramBoolean)
  {
    this.isShowSymbol = paramBoolean;
  }

  public final boolean h()
  {
    return !s.l(this.tickerSymbol);
  }

  public final String i()
  {
    if (h())
      return this.tickerSymbol;
    return this.cusip;
  }

  public final void i(String paramString)
  {
    this.quoteCode = paramString;
  }

  public final void i(boolean paramBoolean)
  {
    this.isShowUnrealizedChange = paramBoolean;
  }

  public final String j()
  {
    if (h())
      return "Symbol";
    return "CUSIP";
  }

  public final void j(boolean paramBoolean)
  {
    this.isShowValue = paramBoolean;
  }

  public final String k()
  {
    if (this.assetClass1 == null)
      return "--";
    if (this.assetClass2 == null)
      return this.assetClass1;
    Object[] arrayOfObject = new Object[2];
    arrayOfObject[0] = this.assetClass1;
    arrayOfObject[1] = this.assetClass2;
    return String.format("%s - %s", arrayOfObject);
  }

  public final PositionType l()
  {
    return PositionType.a(this.assetClass1);
  }

  public final String m()
  {
    return this.priceAsOfDate;
  }

  public final boolean n()
  {
    return this.isShowAssetClass1;
  }

  public final boolean o()
  {
    return this.isShowCost;
  }

  public final boolean p()
  {
    return this.isShowDescription;
  }

  public final boolean q()
  {
    return this.isShowPrice;
  }

  public final boolean r()
  {
    return this.isShowPriceDate;
  }

  public final boolean s()
  {
    return this.isShowQuantity;
  }

  public final boolean t()
  {
    return this.isShowShortName;
  }

  public final boolean u()
  {
    return this.isShowSymbol;
  }

  public final boolean v()
  {
    return this.isShowUnrealizedChange;
  }

  public final boolean w()
  {
    return this.isShowValue;
  }

  public final Dollar x()
  {
    return this.valueChange;
  }

  public final String y()
  {
    return this.quoteCode;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.Position
 * JD-Core Version:    0.6.2
 */