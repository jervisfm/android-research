package com.chase.sig.android.domain;

public enum PositionType
{
  private final String type;

  static
  {
    PositionType[] arrayOfPositionType = new PositionType[6];
    arrayOfPositionType[0] = a;
    arrayOfPositionType[1] = b;
    arrayOfPositionType[2] = c;
    arrayOfPositionType[3] = d;
    arrayOfPositionType[4] = e;
    arrayOfPositionType[5] = f;
  }

  private PositionType(String paramString)
  {
    this.type = paramString;
  }

  public static PositionType a(String paramString)
  {
    for (PositionType localPositionType : values())
      if (localPositionType.type.equals(paramString))
        return localPositionType;
    throw new IllegalArgumentException(paramString);
  }

  public final String a()
  {
    return this.type;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.PositionType
 * JD-Core Version:    0.6.2
 */