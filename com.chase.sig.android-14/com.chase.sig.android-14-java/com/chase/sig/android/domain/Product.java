package com.chase.sig.android.domain;

import java.io.Serializable;

public class Product
  implements Serializable
{
  private String code;
  private String id;
  private String state;
  private String status;
  private String type;
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.Product
 * JD-Core Version:    0.6.2
 */