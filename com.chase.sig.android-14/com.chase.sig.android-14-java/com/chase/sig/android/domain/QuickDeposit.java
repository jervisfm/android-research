package com.chase.sig.android.domain;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Matrix;
import android.os.Bundle;
import com.chase.sig.android.util.Dollar;
import com.chase.sig.android.util.s;
import java.io.Serializable;
import java.util.Date;

public class QuickDeposit
  implements Serializable
{
  private String accountNumber;
  private Dollar amount;
  private Dollar availableBalance;
  private byte[] checkImageBack;
  private byte[] checkImageFront;
  private Dollar delayedAmount1;
  private Dollar delayedAmount2;
  private String delayedAmountAvailableDate1;
  private String delayedAmountAvailableDate2;
  private QuickDepositAccount depositToAccount;
  private Date effectiveDate;
  private int imageHeightUsed;
  private int imageWidthUsed;
  private String itemSequenceNumber;
  private String largeDollar;
  private String locationId;
  private String locationName;
  private String onus;
  private String payingBank;
  private Dollar presentBalance;
  private Dollar readAmount;
  private String routingNumber;
  private String standardBottom;
  private int submitCounter;
  private String transactionId;
  private String updateRequired;
  private String variableBottom;

  public static QuickDeposit a(Intent paramIntent)
  {
    if (paramIntent.getExtras() == null)
      return new QuickDeposit();
    return (QuickDeposit)paramIntent.getExtras().getSerializable("quick_deposit");
  }

  private static Bitmap c(byte[] paramArrayOfByte)
  {
    BitmapFactory.Options localOptions = new BitmapFactory.Options();
    localOptions.inSampleSize = 4;
    Bitmap localBitmap1 = BitmapFactory.decodeByteArray(paramArrayOfByte, 0, paramArrayOfByte.length, localOptions);
    int i = localBitmap1.getWidth();
    int j = localBitmap1.getHeight();
    if (j < 50)
      return localBitmap1;
    float f = 50.0F / j;
    Matrix localMatrix = new Matrix();
    localMatrix.postScale(f, f);
    Bitmap localBitmap2 = Bitmap.createBitmap(localBitmap1, 0, 0, i, j, localMatrix, true);
    localBitmap1.recycle();
    return localBitmap2;
  }

  public final int a()
  {
    return this.submitCounter;
  }

  public final void a(int paramInt)
  {
    this.submitCounter = paramInt;
  }

  public final void a(QuickDepositAccount paramQuickDepositAccount)
  {
    this.depositToAccount = paramQuickDepositAccount;
  }

  public final void a(Dollar paramDollar)
  {
    this.amount = paramDollar;
  }

  public final void a(String paramString)
  {
    this.transactionId = paramString;
  }

  public final void a(byte[] paramArrayOfByte)
  {
    this.checkImageBack = paramArrayOfByte;
  }

  public final String b()
  {
    return this.transactionId;
  }

  public final void b(String paramString)
  {
    this.routingNumber = paramString;
  }

  public final void b(byte[] paramArrayOfByte)
  {
    this.checkImageFront = paramArrayOfByte;
  }

  public final String c()
  {
    return this.accountNumber;
  }

  public final void c(String paramString)
  {
    this.accountNumber = paramString;
  }

  public final void d(String paramString)
  {
    if (s.m(paramString))
    {
      this.effectiveDate = s.a(paramString);
      return;
    }
    this.effectiveDate = null;
  }

  public final byte[] d()
  {
    return this.checkImageBack;
  }

  public final Bitmap e()
  {
    return c(this.checkImageBack);
  }

  public final void e(String paramString)
  {
    this.locationId = paramString;
  }

  public final void f(String paramString)
  {
    this.locationName = paramString;
  }

  public final byte[] f()
  {
    return this.checkImageFront;
  }

  public final Bitmap g()
  {
    return c(this.checkImageFront);
  }

  public final void g(String paramString)
  {
    this.itemSequenceNumber = paramString;
  }

  public final String h()
  {
    return this.routingNumber;
  }

  public final Dollar i()
  {
    return this.amount;
  }

  public final boolean j()
  {
    return this.checkImageFront != null;
  }

  public final boolean k()
  {
    return this.checkImageBack != null;
  }

  public final Dollar l()
  {
    return this.readAmount;
  }

  public final String m()
  {
    return this.locationId;
  }

  public final String n()
  {
    return this.locationName;
  }

  public final QuickDepositAccount o()
  {
    return this.depositToAccount;
  }

  public final String p()
  {
    return this.itemSequenceNumber;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.QuickDeposit
 * JD-Core Version:    0.6.2
 */