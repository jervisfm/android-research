package com.chase.sig.android.domain;

import com.chase.sig.android.util.Dollar;
import com.google.gson.chase.annotations.SerializedName;
import java.io.Serializable;
import java.util.List;

public class QuickDepositAccount
  implements Serializable
{

  @SerializedName(a="id")
  private String accountId;

  @SerializedName(a="nickname")
  private String accountNickname;
  private Authorization authorization;
  private Dollar availableBalance;

  @SerializedName(a="ulidDetails")
  private List<Location> locations;
  private String mask;

  public final String a()
  {
    return this.accountId;
  }

  public final String b()
  {
    return this.accountNickname;
  }

  public final List<Location> c()
  {
    return this.locations;
  }

  public final String d()
  {
    return this.mask;
  }

  public final Authorization e()
  {
    return this.authorization;
  }

  public final Dollar f()
  {
    return this.availableBalance;
  }

  public class Authorization
    implements Serializable
  {
    private boolean authorized;
    private String deniedReasonCode;
    private String deniedReasonMessage;

    public final String a()
    {
      return this.deniedReasonMessage;
    }

    public final boolean b()
    {
      return this.authorized;
    }
  }

  public class Location
    implements Serializable
  {

    @SerializedName(a="ulid")
    private String id;

    @SerializedName(a="ulidName")
    private String name;

    public final String a()
    {
      return this.id;
    }

    public final String b()
    {
      return this.name;
    }

    public String toString()
    {
      return this.name;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.QuickDepositAccount
 * JD-Core Version:    0.6.2
 */