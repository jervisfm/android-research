package com.chase.sig.android.domain;

import com.chase.sig.android.util.s;

public class QuickPayAcceptMoneyTransaction extends QuickPayTransaction
{
  private String recipientCode;

  public final void a(String paramString)
  {
    this.recipientCode = paramString;
  }

  public final String m()
  {
    return this.recipientCode;
  }

  public final boolean n()
  {
    return s.o(this.recipientCode);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.QuickPayAcceptMoneyTransaction
 * JD-Core Version:    0.6.2
 */