package com.chase.sig.android.domain;

import java.io.Serializable;

public class QuickPayActivityItem
  implements h, Serializable
{
  private LabeledValue acceptedOn;
  private LabeledValue accountName;
  private LabeledValue action;
  private LabeledValue amount;
  private LabeledValue atmEligible;
  private LabeledValue date;
  private LabeledValue decline;
  private LabeledValue declineReason;
  private LabeledValue dueDate;
  private LabeledValue frequency;
  private LabeledValue id;
  private LabeledValue invoice;
  private LabeledValue isInvoiceRequest;
  private LabeledValue isSmsEligible;
  private boolean isSuccessfulResentNotification;
  private LabeledValue memo;
  private LabeledValue nextdeliverbydate;
  private LabeledValue nextsendondate;
  private LabeledValue numberofremainingpayments;
  private LabeledValue openended;
  private LabeledValue receivedOn;
  private QuickPayRecipient recipient = new QuickPayRecipient();
  private LabeledValue recipientCode;
  private LabeledValue repeatingid;
  private String resentNotificationMessage;
  private LabeledValue senderCode;
  private LabeledValue sentOn;
  private String smsReason;
  private LabeledValue status;
  private LabeledValue token;
  private LabeledValue type;

  public final LabeledValue A()
  {
    return this.recipientCode;
  }

  public final String B()
  {
    return this.resentNotificationMessage;
  }

  public final LabeledValue C()
  {
    return this.isInvoiceRequest;
  }

  public final String D()
  {
    if (this.declineReason != null)
      return this.declineReason.b();
    return null;
  }

  public final String a()
  {
    return o().b();
  }

  public final void a(LabeledValue paramLabeledValue)
  {
    this.date = paramLabeledValue;
  }

  public final void a(QuickPayRecipient paramQuickPayRecipient)
  {
    this.recipient = paramQuickPayRecipient;
  }

  public final void a(String paramString)
  {
    this.resentNotificationMessage = paramString;
  }

  public final void a(boolean paramBoolean)
  {
    this.isSuccessfulResentNotification = paramBoolean;
  }

  public final String b()
  {
    return new LabeledValue("", this.recipient.d()).b();
  }

  public final void b(LabeledValue paramLabeledValue)
  {
    this.amount = paramLabeledValue;
  }

  public final String c()
  {
    return this.accountName.b();
  }

  public final void c(LabeledValue paramLabeledValue)
  {
    this.status = paramLabeledValue;
  }

  public final String d()
  {
    return this.amount.b();
  }

  public final void d(LabeledValue paramLabeledValue)
  {
    this.id = paramLabeledValue;
  }

  public final String e()
  {
    return this.memo.b();
  }

  public final void e(LabeledValue paramLabeledValue)
  {
    this.type = paramLabeledValue;
  }

  public final void f(LabeledValue paramLabeledValue)
  {
    this.action = paramLabeledValue;
  }

  public final boolean f()
  {
    if (this.isInvoiceRequest != null)
      return "true".equalsIgnoreCase(this.isInvoiceRequest.b());
    return false;
  }

  public final String g()
  {
    return this.id.b();
  }

  public final void g(LabeledValue paramLabeledValue)
  {
    this.decline = paramLabeledValue;
  }

  public final String h()
  {
    return this.status.b();
  }

  public final void h(LabeledValue paramLabeledValue)
  {
    this.accountName = paramLabeledValue;
  }

  public final String i()
  {
    if (this.sentOn == null)
      return null;
    return this.sentOn.b();
  }

  public final void i(LabeledValue paramLabeledValue)
  {
    this.memo = paramLabeledValue;
  }

  public final QuickPayRecipient j()
  {
    return this.recipient;
  }

  public final void j(LabeledValue paramLabeledValue)
  {
    this.receivedOn = paramLabeledValue;
  }

  public final void k(LabeledValue paramLabeledValue)
  {
    this.acceptedOn = paramLabeledValue;
  }

  public final boolean k()
  {
    if (this.atmEligible == null)
      return (this.senderCode != null) && (Integer.valueOf(this.senderCode.b()).intValue() > 0);
    return this.atmEligible.b().equalsIgnoreCase("true");
  }

  public final String l()
  {
    return this.senderCode.b();
  }

  public final void l(LabeledValue paramLabeledValue)
  {
    this.token = paramLabeledValue;
  }

  public final LabeledValue m()
  {
    return this.date;
  }

  public final void m(LabeledValue paramLabeledValue)
  {
    this.dueDate = paramLabeledValue;
  }

  public final LabeledValue n()
  {
    return this.amount;
  }

  public final void n(LabeledValue paramLabeledValue)
  {
    this.invoice = paramLabeledValue;
  }

  public final LabeledValue o()
  {
    return new LabeledValue("", this.recipient.a());
  }

  public final void o(LabeledValue paramLabeledValue)
  {
    this.sentOn = paramLabeledValue;
  }

  public final LabeledValue p()
  {
    return this.status;
  }

  public final void p(LabeledValue paramLabeledValue)
  {
    this.atmEligible = paramLabeledValue;
  }

  public final LabeledValue q()
  {
    return this.id;
  }

  public final void q(LabeledValue paramLabeledValue)
  {
    this.senderCode = paramLabeledValue;
  }

  public final LabeledValue r()
  {
    return this.type;
  }

  public final void r(LabeledValue paramLabeledValue)
  {
    this.frequency = paramLabeledValue;
  }

  public final LabeledValue s()
  {
    return this.receivedOn;
  }

  public final void s(LabeledValue paramLabeledValue)
  {
    this.numberofremainingpayments = paramLabeledValue;
  }

  public final LabeledValue t()
  {
    return this.acceptedOn;
  }

  public final void t(LabeledValue paramLabeledValue)
  {
    this.nextsendondate = paramLabeledValue;
  }

  public final LabeledValue u()
  {
    return this.token;
  }

  public final void u(LabeledValue paramLabeledValue)
  {
    this.nextdeliverbydate = paramLabeledValue;
  }

  public final LabeledValue v()
  {
    return this.dueDate;
  }

  public final void v(LabeledValue paramLabeledValue)
  {
    this.recipientCode = paramLabeledValue;
  }

  public final LabeledValue w()
  {
    return this.invoice;
  }

  public final void w(LabeledValue paramLabeledValue)
  {
    this.isInvoiceRequest = paramLabeledValue;
  }

  public final void x(LabeledValue paramLabeledValue)
  {
    this.declineReason = paramLabeledValue;
  }

  public final boolean x()
  {
    return ((this.receivedOn.b() == null) || (this.receivedOn.b().equals(""))) && ((this.acceptedOn.b() == null) || (this.acceptedOn.b().equals("")));
  }

  public final LabeledValue y()
  {
    return this.sentOn;
  }

  public final LabeledValue z()
  {
    return new LabeledValue("", this.recipient.b());
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.QuickPayActivityItem
 * JD-Core Version:    0.6.2
 */