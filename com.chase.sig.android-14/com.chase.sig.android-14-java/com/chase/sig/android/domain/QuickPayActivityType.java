package com.chase.sig.android.domain;

public enum QuickPayActivityType
{
  static
  {
    QuickPayActivityType[] arrayOfQuickPayActivityType = new QuickPayActivityType[5];
    arrayOfQuickPayActivityType[0] = a;
    arrayOfQuickPayActivityType[1] = b;
    arrayOfQuickPayActivityType[2] = c;
    arrayOfQuickPayActivityType[3] = d;
    arrayOfQuickPayActivityType[4] = e;
  }

  public final boolean a(QuickPayActivityItem paramQuickPayActivityItem)
  {
    if (paramQuickPayActivityItem == null);
    while (paramQuickPayActivityItem.r() == null)
      return false;
    return a(paramQuickPayActivityItem.r().b());
  }

  public final boolean a(String paramString)
  {
    return equals(valueOf(paramString));
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.QuickPayActivityType
 * JD-Core Version:    0.6.2
 */