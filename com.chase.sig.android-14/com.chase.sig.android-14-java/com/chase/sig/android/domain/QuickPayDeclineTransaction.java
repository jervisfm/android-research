package com.chase.sig.android.domain;

public class QuickPayDeclineTransaction extends QuickPayTransaction
{
  private String declineReason;

  public final void a(String paramString)
  {
    this.declineReason = paramString;
  }

  public final String m()
  {
    return this.declineReason.trim();
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.QuickPayDeclineTransaction
 * JD-Core Version:    0.6.2
 */