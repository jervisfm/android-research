package com.chase.sig.android.domain;

import com.chase.sig.android.util.s;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class QuickPayRecipient
  implements Serializable
{
  private String email;
  private String id = "0";
  private String mobileNumber;
  private MobilePhoneNumber mobilePhoneNumberObj;
  private String name;
  private List<Email> recipientEmails = new ArrayList();
  private String recipientId;
  private String status;
  private String token = "0";

  public final String a()
  {
    return this.name;
  }

  public final void a(Email paramEmail)
  {
    this.recipientEmails.add(paramEmail);
  }

  public final void a(MobilePhoneNumber paramMobilePhoneNumber)
  {
    this.mobilePhoneNumberObj = paramMobilePhoneNumber;
  }

  public final void a(String paramString)
  {
    this.name = paramString;
  }

  public final String b()
  {
    return this.id;
  }

  public final void b(String paramString)
  {
    this.id = paramString;
  }

  public final String c()
  {
    return this.token;
  }

  public final void c(String paramString)
  {
    this.token = paramString;
  }

  public final String d()
  {
    if (s.o(this.email))
      return this.email;
    if (this.recipientEmails.size() == 1)
      return ((Email)this.recipientEmails.get(0)).a();
    Iterator localIterator = this.recipientEmails.iterator();
    while (localIterator.hasNext())
    {
      Email localEmail = (Email)localIterator.next();
      if (localEmail.b())
        return localEmail.a();
    }
    return null;
  }

  public final void d(String paramString)
  {
    this.email = paramString;
  }

  public final List<Email> e()
  {
    return new ArrayList(this.recipientEmails);
  }

  public final void e(String paramString)
  {
    if (this.mobilePhoneNumberObj == null)
    {
      this.mobilePhoneNumberObj = new MobilePhoneNumber(paramString, "", "");
      return;
    }
    this.mobilePhoneNumberObj.a(paramString);
  }

  public final String f()
  {
    if (this.mobilePhoneNumberObj == null)
      return null;
    return this.mobilePhoneNumberObj.a();
  }

  public final void f(String paramString)
  {
    this.status = paramString;
  }

  public final String g()
  {
    if (this.mobilePhoneNumberObj == null)
      return null;
    return this.mobilePhoneNumberObj.b();
  }

  public final void g(String paramString)
  {
    this.recipientId = paramString;
  }

  public final void h()
  {
    this.recipientEmails.clear();
  }

  public final MobilePhoneNumber i()
  {
    return this.mobilePhoneNumberObj;
  }

  public final List<Email> j()
  {
    return this.recipientEmails;
  }

  public final int k()
  {
    if (this.mobilePhoneNumberObj == null);
    for (int i = 0; ; i = 1)
      return i + this.recipientEmails.size();
  }

  public final String l()
  {
    return this.status;
  }

  public final boolean m()
  {
    return "ACTIVE".equals(this.status);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.QuickPayRecipient
 * JD-Core Version:    0.6.2
 */