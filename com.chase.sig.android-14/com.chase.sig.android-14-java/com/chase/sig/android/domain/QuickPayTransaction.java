package com.chase.sig.android.domain;

import com.chase.sig.android.util.s;
import java.io.Serializable;
import java.util.Date;

public class QuickPayTransaction
  implements h, Serializable
{
  private String CVV;
  private String accountId;
  private String accountName;
  private String accountTypeIndicator;
  private String amount;
  private boolean atmEligible;
  private boolean canCancel = true;
  private boolean canEdit = true;
  private Date deliverByDate;
  private boolean displayConfirmation;
  private Date dueDate;
  private String formId;
  private String frequency;
  private String frequencyLabel;
  private String invoiceId;
  private boolean isEditing;
  private boolean isInvoiceRequest = false;
  private boolean isRequest;
  private boolean isResponseToARequest = false;
  private boolean isSubmitted = false;
  private String memo;
  private Date nextPaymentDate;
  private Date nextSendOnDate;
  private int numberOfRemainingPayments = 0;
  private String payFromAccountName;
  private QuickPayRecipient recipient;
  private boolean repeating = false;
  private String repeatingId;
  private String repeatingToken;
  private String requestTransactionId;
  private Date sendOnDate;
  private String senderCode;
  private String sentOn;
  private boolean smsEligible;
  private String smsReason;
  private String status;
  private String token;
  private String transactionId;
  private String type;
  private boolean unlimited = false;

  public final String A()
  {
    return this.invoiceId;
  }

  public final boolean B()
  {
    return this.isRequest;
  }

  public final boolean C()
  {
    return this.unlimited;
  }

  public final boolean D()
  {
    return this.repeating;
  }

  public final Date E()
  {
    return this.dueDate;
  }

  public final Date F()
  {
    return this.nextPaymentDate;
  }

  public final String G()
  {
    return this.payFromAccountName;
  }

  public final int H()
  {
    return this.numberOfRemainingPayments;
  }

  public final String I()
  {
    return this.type;
  }

  public final String J()
  {
    return this.token;
  }

  public final String K()
  {
    return this.repeatingToken;
  }

  public final String L()
  {
    return this.repeatingId;
  }

  public final void M()
  {
    this.displayConfirmation = true;
  }

  public final boolean N()
  {
    return this.displayConfirmation;
  }

  public final String O()
  {
    return this.formId;
  }

  public final boolean P()
  {
    return this.canEdit;
  }

  public final boolean Q()
  {
    return this.canCancel;
  }

  public final boolean R()
  {
    return this.isResponseToARequest;
  }

  public final boolean S()
  {
    return this.isInvoiceRequest;
  }

  public final String T()
  {
    return this.smsReason;
  }

  public final boolean U()
  {
    return this.smsEligible;
  }

  public final boolean V()
  {
    return this.isEditing;
  }

  public final String W()
  {
    return this.accountTypeIndicator;
  }

  public final String X()
  {
    return this.frequency;
  }

  public final String Y()
  {
    return this.frequencyLabel;
  }

  public final String a()
  {
    return this.recipient.a();
  }

  public final void a(int paramInt)
  {
    this.numberOfRemainingPayments = paramInt;
  }

  public final void a(QuickPayRecipient paramQuickPayRecipient)
  {
    this.recipient = paramQuickPayRecipient;
  }

  public final void a(Date paramDate)
  {
    this.nextPaymentDate = paramDate;
  }

  public final void a(boolean paramBoolean)
  {
    this.atmEligible = paramBoolean;
  }

  public final String b()
  {
    return this.recipient.d();
  }

  public final void b(String paramString)
  {
    this.accountId = paramString;
  }

  public final void b(Date paramDate)
  {
    this.sendOnDate = paramDate;
  }

  public final void b(boolean paramBoolean)
  {
    this.isRequest = paramBoolean;
  }

  public final String c()
  {
    return this.accountName;
  }

  public final void c(String paramString)
  {
    this.accountName = paramString;
  }

  public final void c(Date paramDate)
  {
    this.nextSendOnDate = paramDate;
  }

  public final void c(boolean paramBoolean)
  {
    this.unlimited = paramBoolean;
  }

  public final String d()
  {
    return this.amount;
  }

  public final void d(String paramString)
  {
    this.amount = paramString;
  }

  public final void d(boolean paramBoolean)
  {
    this.repeating = paramBoolean;
  }

  public final String e()
  {
    return this.memo;
  }

  public final void e(String paramString)
  {
    this.memo = paramString;
  }

  public final void e(boolean paramBoolean)
  {
    this.isResponseToARequest = paramBoolean;
  }

  public final void f(String paramString)
  {
    this.status = paramString;
  }

  public final void f(boolean paramBoolean)
  {
    this.isInvoiceRequest = paramBoolean;
  }

  public final boolean f()
  {
    return s.o(this.invoiceId);
  }

  public final String g()
  {
    return this.transactionId;
  }

  public final void g(String paramString)
  {
    this.transactionId = paramString;
  }

  public final void g(boolean paramBoolean)
  {
    this.smsEligible = paramBoolean;
  }

  public final String h()
  {
    return this.status;
  }

  public final void h(String paramString)
  {
    this.senderCode = paramString;
  }

  public final void h(boolean paramBoolean)
  {
    this.isEditing = paramBoolean;
  }

  public final String i()
  {
    return this.sentOn;
  }

  public final void i(String paramString)
  {
    this.CVV = paramString;
  }

  public final QuickPayRecipient j()
  {
    return this.recipient;
  }

  public final void j(String paramString)
  {
    this.invoiceId = paramString;
  }

  public final void k(String paramString)
  {
    this.dueDate = s.t(paramString);
  }

  public final boolean k()
  {
    return this.atmEligible;
  }

  public final String l()
  {
    return this.senderCode;
  }

  public final void l(String paramString)
  {
    this.type = paramString;
  }

  public final void m(String paramString)
  {
    this.token = paramString;
  }

  public final void n(String paramString)
  {
    this.repeatingToken = paramString;
  }

  public final String o()
  {
    return this.recipient.b();
  }

  public final void o(String paramString)
  {
    this.repeatingId = paramString;
  }

  public final String p()
  {
    return this.accountId;
  }

  public final void p(String paramString)
  {
    this.sentOn = paramString;
  }

  public final String q()
  {
    return this.accountName;
  }

  public final void q(String paramString)
  {
    this.formId = paramString;
  }

  public final String r()
  {
    return this.amount;
  }

  public final void r(String paramString)
  {
    if (paramString.equalsIgnoreCase("true"))
    {
      this.canEdit = true;
      return;
    }
    this.canEdit = false;
  }

  public final Date s()
  {
    return this.sendOnDate;
  }

  public final void s(String paramString)
  {
    if ("true".equalsIgnoreCase(paramString))
    {
      this.canCancel = true;
      return;
    }
    this.canCancel = false;
  }

  public final String t()
  {
    return this.memo;
  }

  public final void t(String paramString)
  {
    this.smsReason = paramString;
  }

  public final String u()
  {
    return this.recipient.d();
  }

  public final void u(String paramString)
  {
    this.accountTypeIndicator = paramString;
  }

  public final String v()
  {
    return this.recipient.a();
  }

  public final void v(String paramString)
  {
    this.frequency = paramString;
  }

  public final String w()
  {
    return this.transactionId;
  }

  public final void w(String paramString)
  {
    this.frequencyLabel = paramString;
  }

  public final String x()
  {
    return this.CVV;
  }

  public final boolean y()
  {
    return this.isSubmitted;
  }

  public final void z()
  {
    this.isSubmitted = true;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.QuickPayTransaction
 * JD-Core Version:    0.6.2
 */