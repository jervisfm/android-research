package com.chase.sig.android.domain;

import com.chase.sig.android.util.Dollar;
import com.chase.sig.android.util.s;
import com.google.gson.chase.annotations.SerializedName;
import java.io.Serializable;

public class Quote
  implements Serializable
{
  private Double averageVolume;
  private Dollar change;
  private String changeDirection;
  private String changePercent;
  private String companyName;
  private String cusip;
  private String exchange;
  private Dollar gainLossToday;
  private Dollar high;

  @SerializedName(a="recent")
  private Dollar last;
  private String lastTime;
  private Dollar low;

  @SerializedName(a="chart1")
  private Chart oneDayChart;

  @SerializedName(a="chart2")
  private Chart oneYearChart;
  private Dollar open;
  private String tickerSymbol;
  private String type;
  private Double volume;
  private Dollar week52High;
  private Dollar week52Low;

  public final Double a()
  {
    return this.averageVolume;
  }

  public final Dollar b()
  {
    return this.change;
  }

  public final String c()
  {
    return this.changeDirection;
  }

  public final String d()
  {
    return this.changePercent;
  }

  public final String e()
  {
    return this.companyName;
  }

  public final String f()
  {
    return this.exchange;
  }

  public final Dollar g()
  {
    return this.gainLossToday;
  }

  public final Dollar h()
  {
    return this.high;
  }

  public final Dollar i()
  {
    return this.last;
  }

  public final String j()
  {
    return this.lastTime;
  }

  public final Dollar k()
  {
    return this.low;
  }

  public final String l()
  {
    return this.tickerSymbol;
  }

  public final Double m()
  {
    return this.volume;
  }

  public final Dollar n()
  {
    return this.week52High;
  }

  public final Dollar o()
  {
    return this.week52Low;
  }

  public final String p()
  {
    return s.a(this.companyName) + " (" + s.a(this.tickerSymbol) + ")";
  }

  public final Chart q()
  {
    if (this.oneDayChart != null)
      return this.oneDayChart;
    return Chart.a;
  }

  public final Chart r()
  {
    if (this.oneYearChart != null)
      return this.oneYearChart;
    return Chart.a;
  }

  public String toString()
  {
    return p();
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.Quote
 * JD-Core Version:    0.6.2
 */