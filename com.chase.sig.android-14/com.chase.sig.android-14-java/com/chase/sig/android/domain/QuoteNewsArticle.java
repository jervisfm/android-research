package com.chase.sig.android.domain;

import java.io.Serializable;

public class QuoteNewsArticle
  implements Serializable
{
  private String headline;
  private String headlineId;
  private String newsContent;
  private String newsDate;
  private String source;
  private String tickerSymbol;

  public final String a()
  {
    return this.tickerSymbol;
  }

  public final String b()
  {
    return this.source;
  }

  public final String c()
  {
    return this.headline;
  }

  public final String d()
  {
    return this.newsDate;
  }

  public final String e()
  {
    return this.newsContent;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.QuoteNewsArticle
 * JD-Core Version:    0.6.2
 */