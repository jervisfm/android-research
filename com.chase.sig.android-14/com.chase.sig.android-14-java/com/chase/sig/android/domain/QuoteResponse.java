package com.chase.sig.android.domain;

import com.chase.sig.android.service.l;
import com.google.gson.chase.annotations.SerializedName;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

public class QuoteResponse extends l
  implements Serializable
{

  @SerializedName(a="stocks")
  private ArrayList<Quote> quotes = new ArrayList();

  public final Quote a(String paramString)
  {
    Iterator localIterator = this.quotes.iterator();
    while (localIterator.hasNext())
    {
      Quote localQuote = (Quote)localIterator.next();
      if (localQuote.l().equalsIgnoreCase(paramString))
        return localQuote;
    }
    return null;
  }

  public final ArrayList<Quote> a()
  {
    return this.quotes;
  }

  public final void a(Quote paramQuote)
  {
    Quote localQuote = a(paramQuote.l());
    if (localQuote != null)
      this.quotes.remove(localQuote);
    this.quotes.add(paramQuote);
  }

  public final Quote b()
  {
    if (this.quotes.isEmpty())
      return null;
    return (Quote)this.quotes.get(0);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.QuoteResponse
 * JD-Core Version:    0.6.2
 */