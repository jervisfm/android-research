package com.chase.sig.android.domain;

import com.chase.sig.android.util.Dollar;
import com.google.gson.chase.annotations.SerializedName;
import java.io.Serializable;

public class Receipt
  implements Serializable
{
  private String accountId;
  private Dollar amount;
  private LabeledValue category;
  private String collectionId;
  private String description;
  private LabeledValue expenseType;

  @SerializedName(a="quickReceiptId")
  private String id;

  @SerializedName(a="accountMask")
  private String maskedName;

  @SerializedName(a="quickReceiptIds")
  private String[] photoIds;
  private String profileId;
  private ReceiptPhotoList receiptPhotoList;
  private String status;

  @SerializedName(a="taxDeductable")
  private LabeledValue taxDeductible;
  private String transactionDate;

  @SerializedName(a="transactionKey")
  private String transactionId;

  public final String a()
  {
    return this.accountId;
  }

  public final void a(LabeledValue paramLabeledValue)
  {
    this.category = paramLabeledValue;
  }

  public final void a(ReceiptPhotoList paramReceiptPhotoList)
  {
    this.receiptPhotoList = paramReceiptPhotoList;
  }

  public final void a(Dollar paramDollar)
  {
    this.amount = paramDollar;
  }

  public final void a(String paramString)
  {
    this.accountId = paramString;
  }

  public final Dollar b()
  {
    return this.amount;
  }

  public final void b(LabeledValue paramLabeledValue)
  {
    this.expenseType = paramLabeledValue;
  }

  public final void b(String paramString)
  {
    this.description = paramString;
  }

  public final String c()
  {
    return this.description;
  }

  public final void c(LabeledValue paramLabeledValue)
  {
    this.taxDeductible = paramLabeledValue;
  }

  public final void c(String paramString)
  {
    this.transactionDate = paramString;
  }

  public final String d()
  {
    return this.status;
  }

  public final void d(String paramString)
  {
    this.transactionId = paramString;
  }

  public final String e()
  {
    return this.transactionDate;
  }

  public final void e(String paramString)
  {
    this.collectionId = paramString;
  }

  public final String f()
  {
    return this.transactionId;
  }

  public final void f(String paramString)
  {
    this.id = paramString;
  }

  public final String g()
  {
    return this.collectionId;
  }

  public final ReceiptPhotoList h()
  {
    return this.receiptPhotoList;
  }

  public final String i()
  {
    return this.id;
  }

  public final String[] j()
  {
    return this.photoIds;
  }

  public final LabeledValue k()
  {
    return this.category;
  }

  public final LabeledValue l()
  {
    return this.expenseType;
  }

  public final LabeledValue m()
  {
    return this.taxDeductible;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.Receipt
 * JD-Core Version:    0.6.2
 */