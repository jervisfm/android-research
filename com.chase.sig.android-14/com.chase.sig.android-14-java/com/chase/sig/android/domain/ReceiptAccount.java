package com.chase.sig.android.domain;

import java.io.Serializable;

public class ReceiptAccount
  implements Serializable
{
  private String accountId;
  private String accountName;
  private String maskedName;
  private int matchedReceiptCount;
  private int unMatchedReceiptCount;

  public final int a()
  {
    return this.unMatchedReceiptCount;
  }

  public final String b()
  {
    return this.maskedName;
  }

  public final String c()
  {
    return this.accountId;
  }

  public final int d()
  {
    return this.unMatchedReceiptCount + this.matchedReceiptCount;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.ReceiptAccount
 * JD-Core Version:    0.6.2
 */