package com.chase.sig.android.domain;

import java.io.Serializable;
import java.util.List;

public class ReceiptAccountSummary
  implements Serializable
{
  private List<ReceiptAccount> accounts;
  private int receiptLimit;
  private int totalReceiptCount;

  public final int a()
  {
    return this.receiptLimit;
  }

  public final int b()
  {
    return this.totalReceiptCount;
  }

  public final List<ReceiptAccount> c()
  {
    return this.accounts;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.ReceiptAccountSummary
 * JD-Core Version:    0.6.2
 */