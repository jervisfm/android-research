package com.chase.sig.android.domain;

import android.content.Intent;
import com.chase.sig.android.util.e;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ReceiptPhotoList
  implements Serializable
{
  private ArrayList<ReceiptPhoto> photoList = new ArrayList();

  public static ReceiptPhotoList a(Intent paramIntent)
  {
    return (ReceiptPhotoList)e.a(paramIntent.getExtras(), "receipt_photo_list", new ReceiptPhotoList());
  }

  public final List<ReceiptPhoto> a()
  {
    return Collections.unmodifiableList(this.photoList);
  }

  public final void a(String paramString)
  {
    this.photoList.add(new ReceiptPhoto(paramString));
  }

  public final void a(byte[] paramArrayOfByte)
  {
    this.photoList.add(new ReceiptPhoto(paramArrayOfByte));
  }

  public static class ReceiptPhoto
    implements Serializable
  {
    String id;
    byte[] photoData;

    public ReceiptPhoto(String paramString)
    {
      this.id = paramString;
      this.photoData = null;
    }

    public ReceiptPhoto(byte[] paramArrayOfByte)
    {
      this.photoData = paramArrayOfByte;
    }

    public final String a()
    {
      return this.id;
    }

    public final void a(byte[] paramArrayOfByte)
    {
      this.photoData = paramArrayOfByte;
    }

    public final byte[] b()
    {
      return this.photoData;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.ReceiptPhotoList
 * JD-Core Version:    0.6.2
 */