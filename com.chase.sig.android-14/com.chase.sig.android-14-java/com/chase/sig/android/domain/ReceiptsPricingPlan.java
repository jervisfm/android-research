package com.chase.sig.android.domain;

import com.chase.sig.android.util.Dollar;
import java.io.Serializable;

public class ReceiptsPricingPlan
  implements Serializable
{
  String description;
  boolean enrolled = false;
  Dollar fee;
  String label;
  int limit;
  boolean limitExceeded = false;
  String planCode;

  public final String a()
  {
    return this.label;
  }

  public final String b()
  {
    return this.description;
  }

  public final int c()
  {
    return this.limit;
  }

  public final String d()
  {
    return this.planCode;
  }

  public final boolean e()
  {
    return this.enrolled;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.ReceiptsPricingPlan
 * JD-Core Version:    0.6.2
 */