package com.chase.sig.android.domain;

import java.io.Serializable;

public class RecipientContact
  implements Serializable
{
  private String contactType;
  private String id = "0";
  private String label;
  private String value;

  public final String a()
  {
    return this.label;
  }

  public final void a(String paramString)
  {
    this.contactType = paramString;
  }

  public final String b()
  {
    return this.value;
  }

  public final void b(String paramString)
  {
    this.label = paramString;
  }

  public final String c()
  {
    return this.id;
  }

  public final void c(String paramString)
  {
    this.value = paramString;
  }

  public final void d(String paramString)
  {
    this.id = paramString;
  }

  public String toString()
  {
    return this.value;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.RecipientContact
 * JD-Core Version:    0.6.2
 */