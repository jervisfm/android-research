package com.chase.sig.android.domain;

import com.google.gson.chase.annotations.SerializedName;
import java.io.Serializable;

public class ResponseActions
  implements Serializable
{

  @SerializedName(a="cancel")
  private Action cancelAction;

  @SerializedName(a="advance")
  private Action continueAction;

  public final Action a()
  {
    return this.cancelAction;
  }

  public final Action b()
  {
    return this.continueAction;
  }

  public class Action
    implements Serializable
  {
    private String label;
    private String url;

    public final String a()
    {
      return this.url;
    }

    public final String b()
    {
      return this.label;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.ResponseActions
 * JD-Core Version:    0.6.2
 */