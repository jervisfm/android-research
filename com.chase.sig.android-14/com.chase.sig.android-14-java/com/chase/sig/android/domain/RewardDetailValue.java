package com.chase.sig.android.domain;

import java.io.Serializable;

public class RewardDetailValue
  implements Serializable
{
  private String name;
  private String value;

  public RewardDetailValue()
  {
    this.name = null;
    this.value = null;
  }

  public RewardDetailValue(String paramString1, String paramString2)
  {
    this.name = paramString1;
    this.value = paramString2;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.RewardDetailValue
 * JD-Core Version:    0.6.2
 */