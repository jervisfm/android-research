package com.chase.sig.android.domain;

import java.io.Serializable;

public class SimpleDialogInfo
  implements Serializable
{
  private boolean cancellable = true;
  private boolean finishOnDismiss;
  private int id = -1;
  private String message;
  private String title;

  public SimpleDialogInfo()
  {
    this(null, false);
  }

  public SimpleDialogInfo(int paramInt, String paramString)
  {
    this(paramInt, null, paramString, false);
  }

  public SimpleDialogInfo(int paramInt, String paramString1, String paramString2)
  {
    this(paramInt, paramString1, paramString2, false);
  }

  private SimpleDialogInfo(int paramInt, String paramString1, String paramString2, boolean paramBoolean)
  {
    this.id = paramInt;
    this.title = paramString1;
    this.message = paramString2;
    this.finishOnDismiss = paramBoolean;
  }

  public SimpleDialogInfo(String paramString)
  {
    this(paramString, false);
  }

  public SimpleDialogInfo(String paramString1, String paramString2)
  {
    this.title = paramString1;
    this.message = paramString2;
    this.finishOnDismiss = true;
  }

  public SimpleDialogInfo(String paramString, boolean paramBoolean)
  {
    this(-9999, null, paramString, paramBoolean);
  }

  public final int a()
  {
    return this.id;
  }

  public final String b()
  {
    return this.message;
  }

  public final void c()
  {
    this.finishOnDismiss = true;
  }

  public final boolean d()
  {
    return this.finishOnDismiss;
  }

  public final String e()
  {
    return this.title;
  }

  public final boolean f()
  {
    return this.cancellable;
  }

  public final void g()
  {
    this.cancellable = false;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.SimpleDialogInfo
 * JD-Core Version:    0.6.2
 */