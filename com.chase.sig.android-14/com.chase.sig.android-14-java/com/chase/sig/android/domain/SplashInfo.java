package com.chase.sig.android.domain;

import java.io.Serializable;

public class SplashInfo
  implements Serializable
{
  private String message;
  private String name;

  public final String a()
  {
    return this.name;
  }

  public final String b()
  {
    return this.message;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.SplashInfo
 * JD-Core Version:    0.6.2
 */