package com.chase.sig.android.domain;

public enum State
{
  private final String displayName;

  static
  {
    A = new State("MS", 26, "Mississippi");
    B = new State("MO", 27, "Missouri");
    C = new State("MT", 28, "Montana");
    D = new State("NE", 29, "Nebraska");
    E = new State("NV", 30, "Nevada");
    F = new State("NH", 31, "New Hampshire");
    G = new State("NJ", 32, "New Jersey");
    H = new State("NM", 33, "New Mexico");
    I = new State("NY", 34, "New York");
    J = new State("NC", 35, "North Carolina");
    K = new State("ND", 36, "North Dakota");
    L = new State("MP", 37, "Northern Marianas Islands");
    M = new State("OH", 38, "Ohio");
    N = new State("OK", 39, "Oklahoma");
    O = new State("OR", 40, "Oregon");
    P = new State("PA", 41, "Pennsylvania");
    Q = new State("PR", 42, "Puerto Rico");
    R = new State("RI", 43, "Rhode Island");
    S = new State("SC", 44, "South Carolina");
    T = new State("SD", 45, "South Dakota");
    U = new State("TN", 46, "Tennessee");
    V = new State("TX", 47, "Texas");
    W = new State("UT", 48, "Utah");
    X = new State("VT", 49, "Vermont");
    Y = new State("VI", 50, "Virgin Islands");
    Z = new State("VA", 51, "Virginia");
    aa = new State("WA", 52, "Washington");
    ab = new State("WV", 53, "West Virginia");
    ac = new State("WI", 54, "Wisconsin");
    ad = new State("WY", 55, "Wyoming");
    State[] arrayOfState = new State[56];
    arrayOfState[0] = a;
    arrayOfState[1] = b;
    arrayOfState[2] = c;
    arrayOfState[3] = d;
    arrayOfState[4] = e;
    arrayOfState[5] = f;
    arrayOfState[6] = g;
    arrayOfState[7] = h;
    arrayOfState[8] = i;
    arrayOfState[9] = j;
    arrayOfState[10] = k;
    arrayOfState[11] = l;
    arrayOfState[12] = m;
    arrayOfState[13] = n;
    arrayOfState[14] = o;
    arrayOfState[15] = p;
    arrayOfState[16] = q;
    arrayOfState[17] = r;
    arrayOfState[18] = s;
    arrayOfState[19] = t;
    arrayOfState[20] = u;
    arrayOfState[21] = v;
    arrayOfState[22] = w;
    arrayOfState[23] = x;
    arrayOfState[24] = y;
    arrayOfState[25] = z;
    arrayOfState[26] = A;
    arrayOfState[27] = B;
    arrayOfState[28] = C;
    arrayOfState[29] = D;
    arrayOfState[30] = E;
    arrayOfState[31] = F;
    arrayOfState[32] = G;
    arrayOfState[33] = H;
    arrayOfState[34] = I;
    arrayOfState[35] = J;
    arrayOfState[36] = K;
    arrayOfState[37] = L;
    arrayOfState[38] = M;
    arrayOfState[39] = N;
    arrayOfState[40] = O;
    arrayOfState[41] = P;
    arrayOfState[42] = Q;
    arrayOfState[43] = R;
    arrayOfState[44] = S;
    arrayOfState[45] = T;
    arrayOfState[46] = U;
    arrayOfState[47] = V;
    arrayOfState[48] = W;
    arrayOfState[49] = X;
    arrayOfState[50] = Y;
    arrayOfState[51] = Z;
    arrayOfState[52] = aa;
    arrayOfState[53] = ab;
    arrayOfState[54] = ac;
    arrayOfState[55] = ad;
  }

  private State(String paramString)
  {
    this.displayName = paramString;
  }

  public static State a(String paramString)
  {
    try
    {
      State localState = valueOf(paramString);
      return localState;
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
    }
    return null;
  }

  public static State b(String paramString)
  {
    for (State localState : values())
      if (paramString.equalsIgnoreCase(localState.displayName))
        return localState;
    return null;
  }

  public final String a()
  {
    return this.displayName;
  }

  public final String toString()
  {
    return this.displayName;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.State
 * JD-Core Version:    0.6.2
 */