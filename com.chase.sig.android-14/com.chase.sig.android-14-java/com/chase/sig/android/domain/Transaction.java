package com.chase.sig.android.domain;

import com.chase.sig.android.util.Dollar;
import com.chase.sig.android.util.s;
import com.chase.sig.android.view.f;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

public abstract class Transaction
  implements Serializable
{
  private Dollar amount;
  private boolean cancellable;
  public String deliverByDate;
  private boolean editable;
  private String formId;
  private String frequency;
  private String fromAccountId;
  public String fromAccountMask;
  String fromAccountNickName;
  private ArrayList<LabeledValue> gwoFrequency;
  private boolean isOpenEnded;
  private String memo;
  private MMBFrequency mmbfrequency;
  private String paymentModelId;
  private String paymentModelToken;
  private String paymentToken;
  private String remainingInstances;
  private String status;
  public String toAccountMask;
  String toAccountNumber;
  private String transactionId;

  public final String A()
  {
    return this.toAccountNumber;
  }

  public String B()
  {
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = d();
    return String.format("To %s", arrayOfObject);
  }

  public final ArrayList<LabeledValue> C()
  {
    return this.gwoFrequency;
  }

  public void a(Dollar paramDollar)
  {
    this.amount = paramDollar;
  }

  public final void a(ArrayList<LabeledValue> paramArrayList)
  {
    this.gwoFrequency = paramArrayList;
  }

  public final void a(Date paramDate)
  {
    this.deliverByDate = s.b(paramDate);
  }

  public final void a(boolean paramBoolean)
  {
    this.editable = paramBoolean;
  }

  public void b(String paramString)
  {
    this.fromAccountId = paramString;
  }

  public final void b(boolean paramBoolean)
  {
    this.cancellable = paramBoolean;
  }

  public String c()
  {
    return this.fromAccountId;
  }

  public abstract String d();

  public void d(String paramString)
  {
    this.paymentToken = paramString;
  }

  public Dollar e()
  {
    return this.amount;
  }

  public String g()
  {
    return this.paymentToken;
  }

  public void g(String paramString)
  {
    this.frequency = paramString;
  }

  public void h(String paramString)
  {
    this.paymentModelId = paramString;
  }

  public String i()
  {
    return this.frequency;
  }

  public void i(String paramString)
  {
    this.paymentModelToken = paramString;
  }

  public String j()
  {
    return this.paymentModelId;
  }

  public final void j(String paramString)
  {
    this.mmbfrequency = MMBFrequency.a(paramString);
  }

  public String k()
  {
    return this.paymentModelToken;
  }

  public final void k(String paramString)
  {
    this.transactionId = paramString;
  }

  public final void l(String paramString)
  {
    this.memo = paramString;
  }

  public boolean l()
  {
    return "One-Time".equals(this.frequency);
  }

  public final void m(String paramString)
  {
    this.formId = paramString;
  }

  public final String n()
  {
    return this.transactionId;
  }

  public final void n(String paramString)
  {
    this.deliverByDate = paramString;
  }

  public final String o()
  {
    return this.memo;
  }

  public final void o(String paramString)
  {
    this.fromAccountNickName = paramString;
  }

  public final String p()
  {
    return this.formId;
  }

  public final void p(String paramString)
  {
    this.fromAccountMask = paramString;
  }

  public final String q()
  {
    return this.deliverByDate;
  }

  public final void q(String paramString)
  {
    this.status = paramString;
  }

  public final String r()
  {
    return this.fromAccountNickName;
  }

  public final void r(String paramString)
  {
    if (paramString.equalsIgnoreCase("null"))
    {
      this.remainingInstances = null;
      return;
    }
    this.remainingInstances = paramString;
  }

  public final String s()
  {
    return this.fromAccountMask;
  }

  public final void s(String paramString)
  {
    this.isOpenEnded = "true".equalsIgnoreCase(paramString);
  }

  public final String t()
  {
    return this.status;
  }

  public final void t(String paramString)
  {
    this.toAccountMask = paramString;
  }

  public final void u(String paramString)
  {
    this.toAccountNumber = paramString;
  }

  public final boolean u()
  {
    return this.cancellable;
  }

  public final boolean v()
  {
    return this.editable;
  }

  public final String w()
  {
    if (this.isOpenEnded)
      return "Unlimited";
    return this.remainingInstances;
  }

  public final f x()
  {
    f localf = new f();
    localf.b = this.isOpenEnded;
    localf.a = this.remainingInstances;
    return localf;
  }

  public final boolean y()
  {
    return this.isOpenEnded;
  }

  public final String z()
  {
    return this.toAccountMask;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.Transaction
 * JD-Core Version:    0.6.2
 */