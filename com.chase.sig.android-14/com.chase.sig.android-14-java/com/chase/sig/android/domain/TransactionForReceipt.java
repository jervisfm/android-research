package com.chase.sig.android.domain;

import com.chase.sig.android.util.Dollar;
import java.io.Serializable;

public class TransactionForReceipt
  implements Serializable
{
  private Dollar amount;
  private String date;
  private String description;
  private String transactionId;

  public final String a()
  {
    return this.transactionId;
  }

  public final String b()
  {
    return this.description;
  }

  public final String c()
  {
    return this.date;
  }

  public final Dollar d()
  {
    return this.amount;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.TransactionForReceipt
 * JD-Core Version:    0.6.2
 */