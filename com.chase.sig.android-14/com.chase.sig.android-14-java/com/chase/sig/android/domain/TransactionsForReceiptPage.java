package com.chase.sig.android.domain;

import com.google.gson.chase.annotations.SerializedName;
import java.io.Serializable;
import java.util.ArrayList;

public class TransactionsForReceiptPage
  implements Serializable
{
  private String end;
  private boolean more;

  @SerializedName(a="transactions")
  private ArrayList<TransactionForReceipt> transForReceipt;

  public final ArrayList<TransactionForReceipt> a()
  {
    return this.transForReceipt;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.TransactionsForReceiptPage
 * JD-Core Version:    0.6.2
 */