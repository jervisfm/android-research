package com.chase.sig.android.domain;

import java.io.Serializable;
import java.util.List;

public class WatchList
  implements Serializable
{
  private String name;
  private List<Quote> symbols;

  public final String a()
  {
    return this.name;
  }

  public final List<Quote> b()
  {
    return this.symbols;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.WatchList
 * JD-Core Version:    0.6.2
 */