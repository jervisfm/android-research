package com.chase.sig.android.domain;

import com.chase.sig.android.util.s;

public class WireTransaction extends Transaction
{
  private String bankMessage;
  private String fedReference;
  private String payeeId;
  private String payeeNickName;
  private String recipientMessage;

  public final String D()
  {
    return this.payeeId;
  }

  public final String a()
  {
    return this.payeeNickName;
  }

  public final void a(String paramString)
  {
    this.payeeNickName = paramString;
  }

  public final String b()
  {
    return this.fedReference;
  }

  public final void c(String paramString)
  {
    this.fedReference = paramString;
  }

  public final String d()
  {
    return this.payeeNickName;
  }

  public final void e(String paramString)
  {
    this.recipientMessage = paramString;
  }

  public final String f()
  {
    return this.recipientMessage;
  }

  public final void f(String paramString)
  {
    this.bankMessage = paramString;
  }

  public final String h()
  {
    return this.bankMessage;
  }

  public final String m()
  {
    return s.s(this.fromAccountNickName) + s.s(this.fromAccountMask);
  }

  public final void v(String paramString)
  {
    this.payeeId = paramString;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.WireTransaction
 * JD-Core Version:    0.6.2
 */