package com.chase.sig.android.domain;

public final class a
{
  public String a;
  public String b;
  public String c;
  public String d;
  public String[] e;
  private String f;
  private boolean g;

  public a()
  {
  }

  public a(String paramString1, String paramString2, String paramString3, boolean paramBoolean, String paramString4, String[] paramArrayOfString)
  {
    this.c = paramString1;
    this.a = paramString2;
    this.f = paramString3;
    this.g = paramBoolean;
    this.b = paramString4;
    this.e = paramArrayOfString;
  }

  public final int a()
  {
    if (this.a.equals("success"))
      return 2;
    if (this.a.equals("secauth"))
      return 4;
    if (this.a.equals("rsasecondcode"))
      return 5;
    return 1;
  }

  public final boolean b()
  {
    return a() == 2;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.a
 * JD-Core Version:    0.6.2
 */