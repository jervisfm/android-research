package com.chase.sig.android.domain;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class c
  implements e
{
  private String a;
  private String b;
  private ArrayList<IAccount> c;

  public final ArrayList<IAccount> a()
  {
    return this.c;
  }

  public final void a(String paramString)
  {
    this.a = paramString;
  }

  public final void a(ArrayList<IAccount> paramArrayList)
  {
    this.c = paramArrayList;
  }

  public final String b()
  {
    return this.a;
  }

  public final void b(String paramString)
  {
    this.b = paramString;
  }

  public final String c()
  {
    return this.b;
  }

  public final boolean c(String paramString)
  {
    Iterator localIterator2;
    do
    {
      Iterator localIterator1 = this.c.iterator();
      while (!localIterator2.hasNext())
      {
        List localList;
        do
        {
          if (!localIterator1.hasNext())
            break;
          IAccount localIAccount = (IAccount)localIterator1.next();
          if (localIAccount.b().equals(paramString))
            return true;
          localList = localIAccount.v();
        }
        while ((localList == null) || (localList.size() <= 0));
        localIterator2 = localList.iterator();
      }
    }
    while (!((IAccount)localIterator2.next()).b().equals(paramString));
    return true;
    return false;
  }

  protected final List<IAccount> d(String paramString)
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = this.c.iterator();
    while (localIterator.hasNext())
    {
      Account localAccount = (Account)localIterator.next();
      if (localAccount.f(paramString))
        localArrayList.add(localAccount);
    }
    return localArrayList;
  }

  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("Customer [id=");
    localStringBuilder.append(this.a);
    localStringBuilder.append(", name=");
    localStringBuilder.append(this.b);
    localStringBuilder.append(", accounts=");
    localStringBuilder.append(this.c);
    localStringBuilder.append("]");
    return localStringBuilder.toString();
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.c
 * JD-Core Version:    0.6.2
 */