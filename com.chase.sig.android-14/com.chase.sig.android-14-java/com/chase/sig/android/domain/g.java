package com.chase.sig.android.domain;

import com.chase.sig.android.activity.eb;
import java.util.List;

public abstract interface g
{
  public abstract boolean A();

  public abstract boolean B();

  public abstract boolean C();

  public abstract String D();

  public abstract MaintenanceInfo E();

  public abstract boolean F();

  public abstract boolean G();

  public abstract boolean H();

  public abstract boolean I();

  public abstract boolean J();

  public abstract boolean K();

  public abstract IAccount a(String paramString);

  public abstract String a(IAccount paramIAccount);

  public abstract boolean a();

  public abstract e b(String paramString);

  public abstract boolean b();

  public abstract e c(String paramString);

  public abstract boolean c();

  public abstract e d(String paramString);

  public abstract Class<? extends eb> d();

  public abstract IAccount e(String paramString);

  public abstract Class<? extends eb> e();

  public abstract String f();

  public abstract boolean g();

  public abstract boolean h();

  public abstract List<IAccount> i();

  public abstract List<IAccount> j();

  public abstract boolean k();

  public abstract boolean l();

  public abstract boolean m();

  public abstract boolean n();

  public abstract List<e> o();

  public abstract List<IAccount> p();

  public abstract boolean q();

  public abstract boolean r();

  public abstract boolean s();

  public abstract List<IAccount> t();

  public abstract List<IAccount> u();

  public abstract List<IAccount> v();

  public abstract List<IAccount> w();

  public abstract List<IAccount> x();

  public abstract List<IAccount> y();

  public abstract boolean z();
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.g
 * JD-Core Version:    0.6.2
 */