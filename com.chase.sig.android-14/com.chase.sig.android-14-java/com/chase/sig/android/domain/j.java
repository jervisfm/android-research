package com.chase.sig.android.domain;

import java.util.HashMap;

final class j extends HashMap<String, String>
{
  j()
  {
    put("C", "Chinese");
    put("E", "English");
    put("F", "French");
    put("G", "German");
    put("H", "Greek");
    put("I", "Italian");
    put("J", "Japanese");
    put("K", "Korean");
    put("P", "Polish");
    put("Q", "Portuguese");
    put("R", "Russian");
    put("S", "Spanish");
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.j
 * JD-Core Version:    0.6.2
 */