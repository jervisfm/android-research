package com.chase.sig.android.domain;

import com.chase.sig.android.activity.QuickDepositSignUpActivity;
import com.chase.sig.android.activity.QuickDepositStartActivity;
import com.chase.sig.android.activity.ReceiptsEnrollActivity;
import com.chase.sig.android.activity.ReceiptsHomeActivity;
import com.chase.sig.android.activity.eb;
import com.chase.sig.android.util.r;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class k
  implements g
{
  private static final List<String> h = Arrays.asList(new String[] { "mobile_internal_xfer", "mobile_internal_xfer_eligible", "mobile_paybills", "mobile_paybills_eligible", "mobile_quickpay", "mobile_quickpay_eligible", "mobile_epay", "mobile_epay_eligible", "mobile_wires", "mobile_wires_eligible" });
  public String a;
  public List<e> b;
  public List<e> c;
  public List<e> d;
  public Set<String> e;
  public Map<String, String> f;
  public MaintenanceInfo g;

  private List<IAccount> a(boolean paramBoolean1, boolean paramBoolean2)
  {
    ArrayList localArrayList1 = new ArrayList();
    if (paramBoolean1)
      localArrayList1.addAll(this.c);
    if (paramBoolean2)
      localArrayList1.addAll(this.b);
    ArrayList localArrayList2 = new ArrayList();
    Iterator localIterator = localArrayList1.iterator();
    while (localIterator.hasNext())
      localArrayList2.addAll(((e)localIterator.next()).a());
    return localArrayList2;
  }

  private static boolean a(List<e> paramList)
  {
    if (!paramList.isEmpty())
    {
      Iterator localIterator = paramList.iterator();
      while (localIterator.hasNext())
      {
        e locale = (e)localIterator.next();
        if ((locale.a() != null) && (locale.a().size() > 0))
          return false;
      }
    }
    return true;
  }

  private boolean f(String paramString)
  {
    return (g(paramString + "_eligible")) || (g(paramString + "_pending_enrollment")) || (g(paramString));
  }

  private boolean g(String paramString)
  {
    if (this.e == null)
      return false;
    return this.e.contains(paramString);
  }

  private List<IAccount> h(String paramString)
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = this.d.iterator();
    while (localIterator.hasNext())
      localArrayList.addAll(((c)localIterator.next()).d(paramString));
    return localArrayList;
  }

  public final boolean A()
  {
    return g("mobile_wires_agreement");
  }

  public final boolean B()
  {
    return f("mobile_quickpay");
  }

  public final boolean C()
  {
    return g("feature_chasereceipts");
  }

  public final String D()
  {
    if ((this.f != null) && (this.f.containsKey("logoType")));
    for (int i = 1; i != 0; i = 0)
      return (String)this.f.get("logoType");
    return null;
  }

  public final MaintenanceInfo E()
  {
    return this.g;
  }

  public final boolean F()
  {
    return g("feature_jpm_brand");
  }

  public final boolean G()
  {
    return f("mobile_internal_xfer");
  }

  public final boolean H()
  {
    return f("mobile_paybills");
  }

  public final boolean I()
  {
    return f("mobile_wires");
  }

  public final boolean J()
  {
    return a(this.b);
  }

  public final boolean K()
  {
    return a(this.c);
  }

  public final IAccount a(String paramString)
  {
    Iterator localIterator1 = this.d.iterator();
    label10: if (localIterator1.hasNext())
    {
      label112: IAccount localIAccount2;
      do
      {
        Iterator localIterator2 = ((e)localIterator1.next()).a().iterator();
        break label112;
        if (!localIterator2.hasNext())
          break label10;
        IAccount localIAccount1 = (IAccount)localIterator2.next();
        if (localIAccount1.b().equals(paramString))
          return localIAccount1;
        List localList = localIAccount1.v();
        if ((localList == null) || (localList.size() <= 0))
          break;
        Iterator localIterator3 = localIAccount1.v().iterator();
        if (!localIterator3.hasNext())
          break;
        localIAccount2 = (IAccount)localIterator3.next();
      }
      while (!localIAccount2.b().equals(paramString));
      return localIAccount2;
    }
    return null;
  }

  public final String a(IAccount paramIAccount)
  {
    if (!paramIAccount.g())
      return paramIAccount.w().toString();
    IAccount localIAccount = e(paramIAccount.b());
    Object[] arrayOfObject = new Object[2];
    arrayOfObject[0] = paramIAccount.a();
    arrayOfObject[1] = localIAccount.c();
    return String.format("%s (%s)", arrayOfObject);
  }

  public final boolean a()
  {
    return (g("mobile_quick_deposit")) || (g("mobile_quick_deposit_eligible"));
  }

  public final e b(String paramString)
  {
    l locall = new l(this, paramString);
    new r();
    int i = r.a(this.d, locall);
    return (e)this.d.get(i);
  }

  public final boolean b()
  {
    return (g("feature_chasereceipts")) || (g("feature_chasereceiptsenroll"));
  }

  public final e c(String paramString)
  {
    Iterator localIterator = this.d.iterator();
    while (localIterator.hasNext())
    {
      e locale = (e)localIterator.next();
      if (locale.c(paramString))
        return locale;
    }
    return null;
  }

  public final boolean c()
  {
    Iterator localIterator1 = a(true, true).iterator();
    int i = 0;
    int j = 0;
    if (localIterator1.hasNext())
    {
      IAccount localIAccount = (IAccount)localIterator1.next();
      if (localIAccount.d().equals("PPX"))
        j = 1;
      if (localIAccount.d().equals("PPX"))
        break label159;
    }
    label159: for (int n = 1; ; n = i)
    {
      i = n;
      break;
      int k;
      if ((j != 0) && (i == 0))
      {
        k = 1;
        if (k != 0)
        {
          Iterator localIterator2 = h.iterator();
          do
            if (!localIterator2.hasNext())
              break;
          while (!g((String)localIterator2.next()));
        }
      }
      for (int m = 1; ; m = 0)
      {
        boolean bool = false;
        if (m != 0)
          bool = true;
        return bool;
        k = 0;
        break;
      }
    }
  }

  public final e d(String paramString)
  {
    Iterator localIterator = this.d.iterator();
    while (localIterator.hasNext())
    {
      e locale = (e)localIterator.next();
      if (locale.b().equals(paramString))
        return locale;
    }
    return null;
  }

  public final Class<? extends eb> d()
  {
    if (g("mobile_quick_deposit_eligible"))
      return QuickDepositSignUpActivity.class;
    if (g("mobile_quick_deposit"))
      return QuickDepositStartActivity.class;
    return null;
  }

  public final IAccount e(String paramString)
  {
    IAccount localIAccount;
    do
    {
      Iterator localIterator1 = this.d.iterator();
      Iterator localIterator2;
      while (!localIterator2.hasNext())
      {
        e locale;
        do
        {
          if (!localIterator1.hasNext())
            break;
          locale = (e)localIterator1.next();
        }
        while (!locale.c(paramString));
        localIterator2 = locale.a().iterator();
      }
      localIAccount = (IAccount)localIterator2.next();
    }
    while (!localIAccount.i());
    return localIAccount;
    return null;
  }

  public final Class<? extends eb> e()
  {
    if (g("feature_chasereceipts"))
      return ReceiptsHomeActivity.class;
    return ReceiptsEnrollActivity.class;
  }

  public final String f()
  {
    return this.a;
  }

  public final boolean g()
  {
    return (g("mobile_quickpay")) || (g("mobile_quickpay_pending_enrollment"));
  }

  public final boolean h()
  {
    return g("mobile_quickpay");
  }

  public final List<IAccount> i()
  {
    return a(true, true);
  }

  public final List<IAccount> j()
  {
    return h("mobile_wires");
  }

  public final boolean k()
  {
    return (g("mobile_paybills_eligible")) || (g("mobile_paybills"));
  }

  public final boolean l()
  {
    return g("mobile_add_payee");
  }

  public final boolean m()
  {
    return g("mobile_add_photo_payee");
  }

  public final boolean n()
  {
    return f("mobile_epay");
  }

  public final List<e> o()
  {
    return this.c;
  }

  public final List<IAccount> p()
  {
    return a(false, true);
  }

  public final boolean q()
  {
    return a(false, true).size() > 0;
  }

  public final boolean r()
  {
    return (this.c != null) && (this.c.size() > 0);
  }

  public final boolean s()
  {
    if (a(true, false).size() > 0);
    for (int i = 1; (i != 0) && (q()); i = 0)
      return true;
    return false;
  }

  public final List<IAccount> t()
  {
    return h("mobile_paybills");
  }

  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("AuthorizedProfile [type=");
    localStringBuilder.append(this.a);
    localStringBuilder.append(", personalCustomers=");
    localStringBuilder.append(this.b);
    localStringBuilder.append(", nonPersonalCustomers=");
    localStringBuilder.append(this.c);
    localStringBuilder.append(", privileges=");
    localStringBuilder.append(this.e);
    localStringBuilder.append("]");
    return localStringBuilder.toString();
  }

  public final List<IAccount> u()
  {
    ArrayList localArrayList1 = new ArrayList();
    localArrayList1.addAll(h("mobile_epay"));
    Iterator localIterator1 = this.c.iterator();
    break label66;
    label29: ArrayList localArrayList2;
    do
    {
      if (!localIterator1.hasNext())
        break;
      localArrayList2 = ((e)localIterator1.next()).a();
    }
    while (localArrayList2 == null);
    Iterator localIterator2 = localArrayList2.iterator();
    while (true)
    {
      label66: if (!localIterator2.hasNext())
        break label29;
      IAccount localIAccount1 = (IAccount)localIterator2.next();
      if (localIAccount1.v() == null)
        break;
      Iterator localIterator3 = localIAccount1.v().iterator();
      while (localIterator3.hasNext())
      {
        IAccount localIAccount2 = (IAccount)localIterator3.next();
        if (localIAccount2.J().contains("mobile_epay"))
          localArrayList1.add(localIAccount2);
      }
    }
    return localArrayList1;
  }

  public final List<IAccount> v()
  {
    return h("mobile_internal_xfer_credit");
  }

  public final List<IAccount> w()
  {
    return h("feature_chasereceipts");
  }

  public final List<IAccount> x()
  {
    return h("mobile_internal_xfer_debit");
  }

  public final List<IAccount> y()
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator1 = this.d.iterator();
    while (localIterator1.hasNext())
    {
      Iterator localIterator2 = ((e)localIterator1.next()).a().iterator();
      while (localIterator2.hasNext())
      {
        IAccount localIAccount = (IAccount)localIterator2.next();
        String str = localIAccount.d();
        boolean bool = ((Account)localIAccount).f("mobile_internal_xfer_credit");
        if (((str.equals("HEL")) || (str.equals("HEO"))) && (!bool))
          localArrayList.add(localIAccount);
      }
    }
    return localArrayList;
  }

  public final boolean z()
  {
    return g("feature_eotm");
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.k
 * JD-Core Version:    0.6.2
 */