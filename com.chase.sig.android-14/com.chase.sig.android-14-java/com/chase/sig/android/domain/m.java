package com.chase.sig.android.domain;

import java.util.ArrayList;
import java.util.Iterator;

public final class m
{
  public ArrayList<LabeledValue> a;

  public m(ArrayList<LabeledValue> paramArrayList)
  {
    this.a = paramArrayList;
  }

  public final String a(String paramString)
  {
    for (int i = 0; i < this.a.size(); i++)
      if (((LabeledValue)this.a.get(i)).b().equalsIgnoreCase(paramString))
        return ((LabeledValue)this.a.get(i)).a();
    return null;
  }

  public final String[] a()
  {
    String[] arrayOfString = new String[this.a.size()];
    Iterator localIterator = this.a.iterator();
    int j;
    for (int i = 0; localIterator.hasNext(); i = j)
    {
      j = i + 1;
      arrayOfString[i] = ((LabeledValue)localIterator.next()).a();
    }
    return arrayOfString;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.m
 * JD-Core Version:    0.6.2
 */