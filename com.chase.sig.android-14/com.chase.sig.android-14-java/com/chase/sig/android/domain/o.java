package com.chase.sig.android.domain;

import com.chase.sig.android.service.ProfileResponse;
import com.chase.sig.android.service.SplashResponse;
import com.chase.sig.android.service.n;
import com.chase.sig.android.service.quickpay.TodoListResponse;
import com.chase.sig.android.service.w;
import java.util.ArrayList;

public final class o
{
  public a a;
  public g b;
  public TodoListResponse c;
  public SplashResponse d;
  private CacheActivityData e;

  public o()
  {
  }

  public o(a parama, g paramg)
  {
    this.a = parama;
    this.b = paramg;
    if (c())
      a(paramg.E().b());
  }

  private void a(ArrayList<SplashInfo> paramArrayList)
  {
    this.d = new SplashResponse(paramArrayList);
  }

  private boolean c()
  {
    return (this.b != null) && (this.b.E() != null);
  }

  public final ProfileResponse a()
  {
    new n().c();
    ProfileResponse localProfileResponse = w.a();
    if ((localProfileResponse == null) || (localProfileResponse.a() == null) || (localProfileResponse.e()));
    do
    {
      return localProfileResponse;
      this.b = localProfileResponse.a();
    }
    while (!c());
    a(this.b.E().b());
    return localProfileResponse;
  }

  public final CacheActivityData b()
  {
    if (this.e == null)
      this.e = new CacheActivityData();
    return this.e;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.o
 * JD-Core Version:    0.6.2
 */