package com.chase.sig.android.domain.receipt;

import com.chase.sig.android.domain.LabeledValue;
import java.io.Serializable;
import java.util.List;

public class ReceiptFilterList
  implements Serializable
{
  private String label;
  private String value;
  private List<LabeledValue> valueOptionsList;

  public final List<LabeledValue> a()
  {
    return this.valueOptionsList;
  }

  public final String b()
  {
    return this.label;
  }

  public final String c()
  {
    return this.value;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.receipt.ReceiptFilterList
 * JD-Core Version:    0.6.2
 */