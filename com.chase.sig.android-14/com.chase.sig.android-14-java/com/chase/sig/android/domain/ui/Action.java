package com.chase.sig.android.domain.ui;

import java.io.Serializable;
import java.util.Hashtable;

public class Action
  implements Serializable
{
  public String label;
  public Url url;

  public final String a()
  {
    return this.label;
  }

  public final Url b()
  {
    return this.url;
  }

  public class Url
    implements Serializable
  {
    private Hashtable<String, String> parameters;
    private String rootUriPath;
    private String uri;

    public final String a()
    {
      return this.uri;
    }

    public final Hashtable<String, String> b()
    {
      return this.parameters;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.domain.ui.Action
 * JD-Core Version:    0.6.2
 */