package com.chase.sig.android;

import com.chase.sig.android.activity.eb;
import java.util.Calendar;

public final class f extends Thread
{
  private static Calendar a;
  private eb b;

  public f(eb parameb)
  {
    this.b = parameb;
  }

  public static void a()
  {
    a = Calendar.getInstance();
  }

  public final void run()
  {
    ChaseApplication localChaseApplication = ChaseApplication.a();
    try
    {
      Calendar localCalendar1;
      Calendar localCalendar2;
      if ((localChaseApplication.c()) && (!this.b.isFinishing()))
      {
        localCalendar1 = Calendar.getInstance();
        localCalendar2 = (Calendar)a.clone();
        localCalendar2.add(13, 650);
        Calendar localCalendar3 = (Calendar)a.clone();
        localCalendar3.add(13, 710);
        if (!localCalendar1.after(localCalendar3))
          break label121;
        this.b.runOnUiThread(new g(this));
      }
      while (true)
      {
        while (true)
        {
          if (Thread.currentThread().isInterrupted())
            return;
          try
          {
            wait(5000L);
            break;
          }
          finally
          {
            localObject = finally;
            throw localObject;
          }
        }
        label121: if (localCalendar1.after(localCalendar2))
          this.b.runOnUiThread(new h(this));
      }
    }
    catch (InterruptedException localInterruptedException)
    {
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.f
 * JD-Core Version:    0.6.2
 */