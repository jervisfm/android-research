package com.chase.sig.android.service;

import com.chase.sig.android.domain.AccountActivity;
import java.util.List;

public class AccountActivityResponse extends l
{
  private List<AccountActivity> activity;
  private String end;
  private boolean hasMore;

  public final List<AccountActivity> a()
  {
    return this.activity;
  }

  public final void a(String paramString)
  {
    this.end = paramString;
  }

  public final void a(List<AccountActivity> paramList)
  {
    this.activity = paramList;
  }

  public final void a(boolean paramBoolean)
  {
    this.hasMore = paramBoolean;
  }

  public final boolean b()
  {
    return this.hasMore;
  }

  public final String c()
  {
    return this.end;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.AccountActivityResponse
 * JD-Core Version:    0.6.2
 */