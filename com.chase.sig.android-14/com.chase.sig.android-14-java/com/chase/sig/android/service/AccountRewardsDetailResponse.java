package com.chase.sig.android.service;

import com.chase.sig.android.domain.AccountRewardsDetail;
import java.util.List;

public class AccountRewardsDetailResponse extends l
{
  private AccountRewardsDetail accountRewardsDetail;
  private List<AccountRewardsDetail> accountRewardsDetailList;
  private String footNote;

  public final AccountRewardsDetail a()
  {
    return this.accountRewardsDetail;
  }

  public final void a(String paramString)
  {
    this.footNote = paramString;
  }

  public final void a(List<AccountRewardsDetail> paramList)
  {
    if (paramList.isEmpty())
    {
      this.accountRewardsDetailList = null;
      return;
    }
    this.accountRewardsDetailList = paramList;
  }

  public final List<AccountRewardsDetail> b()
  {
    return this.accountRewardsDetailList;
  }

  public final String c()
  {
    return this.footNote;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.AccountRewardsDetailResponse
 * JD-Core Version:    0.6.2
 */