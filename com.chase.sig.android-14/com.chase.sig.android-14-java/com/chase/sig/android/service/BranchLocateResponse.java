package com.chase.sig.android.service;

import com.chase.sig.android.domain.BranchLocation;
import java.util.List;

public class BranchLocateResponse extends l
{
  private List<BranchLocation> locations;

  public final List<BranchLocation> a()
  {
    return this.locations;
  }

  public final void a(List<BranchLocation> paramList)
  {
    this.locations = paramList;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.BranchLocateResponse
 * JD-Core Version:    0.6.2
 */