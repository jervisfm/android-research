package com.chase.sig.android.service;

import com.chase.sig.android.domain.CreditFacilityAccountDetail;

public class CreditFacilityAccountDetailResponse extends l
{
  private CreditFacilityAccountDetail details;

  public final CreditFacilityAccountDetail a()
  {
    return this.details;
  }

  public final void a(CreditFacilityAccountDetail paramCreditFacilityAccountDetail)
  {
    this.details = paramCreditFacilityAccountDetail;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.CreditFacilityAccountDetailResponse
 * JD-Core Version:    0.6.2
 */