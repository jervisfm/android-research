package com.chase.sig.android.service;

import java.io.Serializable;

public class EyeOnTheMarketReport
  implements Serializable
{
  private String day;
  private String description;
  private String url;

  public final String a()
  {
    return this.day;
  }

  public final String b()
  {
    return this.description;
  }

  public final String c()
  {
    return this.url;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.EyeOnTheMarketReport
 * JD-Core Version:    0.6.2
 */