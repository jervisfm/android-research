package com.chase.sig.android.service;

import android.location.Location;
import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.util.m;
import java.net.URLEncoder;
import org.json.JSONArray;
import org.json.JSONObject;

public final class GeocoderService extends JPService
{
  public final GeoLocationResponse a(String paramString)
  {
    GeoLocationResponse localGeoLocationResponse = new GeoLocationResponse();
    try
    {
      Object[] arrayOfObject1 = new Object[3];
      ChaseApplication localChaseApplication = ChaseApplication.a();
      Object[] arrayOfObject2 = new Object[2];
      arrayOfObject2[0] = localChaseApplication.c("google_apis");
      arrayOfObject2[1] = localChaseApplication.getString(2131165281);
      arrayOfObject1[0] = String.format("%s%s", arrayOfObject2);
      arrayOfObject1[1] = "true";
      arrayOfObject1[2] = URLEncoder.encode(paramString, "utf-8");
      String str = String.format("%s?sensor=%s&address=%s", arrayOfObject1);
      JSONArray localJSONArray = m.a(ChaseApplication.a(), str, "").optJSONArray("results");
      if ((localJSONArray != null) && (localJSONArray.length() > 0))
      {
        JSONObject localJSONObject = ((JSONObject)localJSONArray.get(0)).getJSONObject("geometry").getJSONObject("location");
        Location localLocation = new Location("passive");
        localLocation.setLatitude(localJSONObject.getDouble("lat"));
        localLocation.setLongitude(localJSONObject.getDouble("lng"));
        localGeoLocationResponse.a(localLocation);
        return localGeoLocationResponse;
      }
    }
    catch (Exception localException)
    {
      while (true)
        localGeoLocationResponse.a(ChaseApplication.a().getApplicationContext());
    }
    return localGeoLocationResponse;
  }

  public class GeoLocationResponse extends l
  {
    private Location location;

    public GeoLocationResponse()
    {
    }

    public final Location a()
    {
      return this.location;
    }

    public final void a(Location paramLocation)
    {
      this.location = paramLocation;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.GeocoderService
 * JD-Core Version:    0.6.2
 */