package com.chase.sig.android.service;

import java.io.Serializable;
import java.util.List;

public abstract interface IServiceError extends Serializable
{
  public abstract String a();

  public abstract String b();

  public abstract String c();

  public abstract boolean d();

  public abstract List<ServiceErrorAttribute> e();
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.IServiceError
 * JD-Core Version:    0.6.2
 */