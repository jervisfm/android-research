package com.chase.sig.android.service;

import com.chase.sig.android.domain.InvestmentTransaction;
import com.chase.sig.android.domain.InvestmentTransaction.FilterType;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class InvestmentTransactionsResponse extends l
  implements Serializable
{
  private String dateRange;
  private String filterValue;
  private List<InvestmentTransaction> investmentTransactions;
  private List<InvestmentTransaction.FilterType> transactionFilters = new ArrayList();

  public final List<InvestmentTransaction> a()
  {
    return this.investmentTransactions;
  }

  public final void a(InvestmentTransaction.FilterType paramFilterType)
  {
    this.transactionFilters.add(paramFilterType);
  }

  public final void a(String paramString)
  {
    this.dateRange = paramString;
  }

  public final void a(List<InvestmentTransaction> paramList)
  {
    this.investmentTransactions = paramList;
  }

  public final List<InvestmentTransaction.FilterType> b()
  {
    return this.transactionFilters;
  }

  public final void b(String paramString)
  {
    this.filterValue = paramString;
  }

  public final String c()
  {
    return this.dateRange;
  }

  public final String d()
  {
    return this.filterValue;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.InvestmentTransactionsResponse
 * JD-Core Version:    0.6.2
 */