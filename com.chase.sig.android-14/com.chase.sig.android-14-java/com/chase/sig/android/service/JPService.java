package com.chase.sig.android.service;

import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.os.Build.VERSION;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.domain.a;
import com.chase.sig.android.domain.d;
import com.chase.sig.android.domain.o;
import com.chase.sig.android.util.ChaseException;
import com.chase.sig.android.util.Dollar;
import com.chase.sig.android.util.f;
import com.chase.sig.android.util.s;
import com.google.gson.chase.Gson;
import com.google.gson.chase.GsonBuilder;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.http.cookie.Cookie;

public class JPService
{
  protected static <T> T a(String paramString, Hashtable<String, String> paramHashtable, Class<T> paramClass)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.c(paramString, paramHashtable);
    return a.a(a(paramString, paramHashtable), paramClass);
  }

  public static String a(int paramInt)
  {
    String str = ChaseApplication.a().c("gws");
    return str + b(paramInt);
  }

  protected static String a(String paramString, Map<String, String> paramMap)
  {
    f localf = new f(ChaseApplication.a());
    Hashtable localHashtable = c();
    localHashtable.putAll(paramMap);
    String str = localf.a(paramString, f.a(localHashtable));
    new Object[] { str };
    if (str == null)
      throw new ChaseException(ChaseException.b, String.format("Did not recieve JSON response from %s", new Object[] { paramString }));
    return str;
  }

  public static void a(Hashtable<String, String> paramHashtable, String paramString1, String paramString2)
  {
    if (paramString2 != null)
      paramHashtable.put(paramString1, paramString2);
  }

  public static String b(int paramInt)
  {
    return ChaseApplication.a().getApplicationContext().getResources().getString(paramInt);
  }

  public static String b(String paramString)
  {
    Object[] arrayOfObject = new Object[2];
    arrayOfObject[0] = ChaseApplication.a().c("gws");
    arrayOfObject[1] = paramString;
    return String.format("%s%s", arrayOfObject);
  }

  public static HashMap<String, String> b()
  {
    HashMap localHashMap = new HashMap();
    localHashMap.put("User-Agent", "plop");
    localHashMap.put("http_referrer", "mobilebanking.chase.com");
    return localHashMap;
  }

  public static Hashtable<String, String> c()
  {
    new f(ChaseApplication.a());
    Iterator localIterator = f.b().iterator();
    Cookie localCookie;
    do
    {
      if (!localIterator.hasNext())
        break;
      localCookie = (Cookie)localIterator.next();
    }
    while (!localCookie.getName().equals("csrfTokenCookie"));
    for (String str = localCookie.getValue(); ; str = null)
    {
      new Object[] { str };
      Hashtable localHashtable = new Hashtable();
      ChaseApplication localChaseApplication = ChaseApplication.a();
      localHashtable.put("deviceVersion", Build.VERSION.RELEASE);
      localHashtable.put("deviceType", Build.MODEL);
      localHashtable.put("deviceTimeStamp", s.d(new Date()));
      localHashtable.put("deviceAppVersion", localChaseApplication.k());
      localHashtable.put("deviceId", localChaseApplication.d());
      localHashtable.put("deviceOS", Build.VERSION.RELEASE);
      localHashtable.put("type", "json");
      localHashtable.put("version", "1");
      localHashtable.put("cache", "true");
      localHashtable.put("applId", "gateway");
      localHashtable.put("channelId", ChaseApplication.a().h());
      if (localChaseApplication.b().a != null)
        localHashtable.put("userId", localChaseApplication.b().a.c);
      if (s.o(str))
        localHashtable.put("csrfToken", str);
      return localHashtable;
    }
  }

  public static class CrossSiteRequestForgeryTokenFailureException extends ChaseException
  {
    public CrossSiteRequestForgeryTokenFailureException()
    {
      super("");
    }
  }

  private static final class a
  {
    private static Gson a = new GsonBuilder().a(Dollar.class, new d()).a(IServiceError.class, new ae()).b();

    public static <T> T a(String paramString, Class<T> paramClass)
    {
      return a.a(paramString, paramClass);
    }

    public static String a(Object paramObject)
    {
      return a.a(paramObject);
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.JPService
 * JD-Core Version:    0.6.2
 */