package com.chase.sig.android.service;

public class LegalAgreementGetDetailsResponse extends l
{
  private String code;
  private String content;
  private String status;
  private String version;

  public final String a()
  {
    return this.content;
  }

  public final void a(String paramString)
  {
    this.code = paramString;
  }

  public final void b(String paramString)
  {
    this.status = paramString;
  }

  public final void e(String paramString)
  {
    this.version = paramString;
  }

  public final void f(String paramString)
  {
    this.content = paramString;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.LegalAgreementGetDetailsResponse
 * JD-Core Version:    0.6.2
 */