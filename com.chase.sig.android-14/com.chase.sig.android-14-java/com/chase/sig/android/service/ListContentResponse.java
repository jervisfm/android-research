package com.chase.sig.android.service;

import com.chase.sig.android.domain.receipt.ReceiptFilterList;
import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;

public class ListContentResponse extends l
  implements Serializable
{
  private LinkedHashMap<String, ReceiptFilterList> filters;

  public final ReceiptFilterList a(String paramString)
  {
    Iterator localIterator = this.filters.values().iterator();
    while (localIterator.hasNext())
    {
      ReceiptFilterList localReceiptFilterList = (ReceiptFilterList)localIterator.next();
      if (localReceiptFilterList.b().equals(paramString))
        return localReceiptFilterList;
    }
    return null;
  }

  public final LinkedHashMap<String, ReceiptFilterList> a()
  {
    return this.filters;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.ListContentResponse
 * JD-Core Version:    0.6.2
 */