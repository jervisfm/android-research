package com.chase.sig.android.service;

import com.chase.sig.android.domain.OneTimePasswordContact;
import java.io.Serializable;
import java.util.ArrayList;

public class OneTimePasswordContactsResponse extends l
  implements Serializable
{
  ArrayList<OneTimePasswordContact> contacts;

  public final ArrayList<OneTimePasswordContact> a()
  {
    return this.contacts;
  }

  public final void a(ArrayList<OneTimePasswordContact> paramArrayList)
  {
    this.contacts = paramArrayList;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.OneTimePasswordContactsResponse
 * JD-Core Version:    0.6.2
 */