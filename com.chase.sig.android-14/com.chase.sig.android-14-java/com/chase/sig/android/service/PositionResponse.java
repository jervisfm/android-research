package com.chase.sig.android.service;

import com.chase.sig.android.domain.Position;
import com.chase.sig.android.domain.PositionType;
import java.io.Serializable;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class PositionResponse extends l
  implements Serializable
{
  private List<PositionsAccount> accounts;

  public final List<PositionsAccount> a()
  {
    return this.accounts;
  }

  public final void a(List<PositionsAccount> paramList)
  {
    this.accounts = paramList;
  }

  public final Map<PositionType, ? extends Set<Position>> b()
  {
    if (!c())
      return Collections.emptyMap();
    return ((PositionsAccount)this.accounts.get(0)).f();
  }

  public final boolean c()
  {
    if (this.accounts != null)
    {
      Iterator localIterator = this.accounts.iterator();
      while (localIterator.hasNext())
      {
        PositionsAccount localPositionsAccount = (PositionsAccount)localIterator.next();
        if ((localPositionsAccount.a() != null) && (!localPositionsAccount.a().isEmpty()))
          return true;
      }
    }
    return false;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.PositionResponse
 * JD-Core Version:    0.6.2
 */