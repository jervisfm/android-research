package com.chase.sig.android.service;

import com.chase.sig.android.domain.Position;
import com.chase.sig.android.domain.PositionType;
import com.chase.sig.android.util.Dollar;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public class PositionsAccount
  implements Serializable
{
  private String accountUnrealizedChange;
  private String accountValue;
  private String asOfDate;
  private String displayName;
  private Dollar marketValue;
  private Dollar marketValueChange;
  private List<Position> positions = new ArrayList();
  private LinkedHashMap<PositionType, TreeSet<Position>> sortedPositions;

  public final List<Position> a()
  {
    return this.positions;
  }

  public final void a(Dollar paramDollar)
  {
    this.marketValue = paramDollar;
  }

  public final void a(String paramString)
  {
    this.displayName = paramString;
  }

  public final void a(List<Position> paramList)
  {
    this.positions = paramList;
    this.sortedPositions = new LinkedHashMap();
    for (PositionType localPositionType : PositionType.values())
      this.sortedPositions.put(localPositionType, new TreeSet());
    Iterator localIterator = paramList.iterator();
    while (localIterator.hasNext())
    {
      Position localPosition = (Position)localIterator.next();
      ((TreeSet)this.sortedPositions.get(localPosition.l())).add(localPosition);
    }
  }

  public final String b()
  {
    return this.displayName;
  }

  public final void b(Dollar paramDollar)
  {
    this.marketValueChange = paramDollar;
  }

  public final void b(String paramString)
  {
    this.asOfDate = paramString;
  }

  public final String c()
  {
    return this.asOfDate;
  }

  public final void c(String paramString)
  {
    this.accountUnrealizedChange = paramString;
  }

  public final String d()
  {
    return this.accountUnrealizedChange;
  }

  public final void d(String paramString)
  {
    this.accountValue = paramString;
  }

  public final String e()
  {
    return this.accountValue;
  }

  public final Map<PositionType, ? extends Set<Position>> f()
  {
    return this.sortedPositions;
  }

  public final Dollar g()
  {
    return this.marketValueChange;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.PositionsAccount
 * JD-Core Version:    0.6.2
 */