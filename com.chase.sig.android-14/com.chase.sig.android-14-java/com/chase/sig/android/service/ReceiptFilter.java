package com.chase.sig.android.service;

import com.google.gson.chase.annotations.SerializedName;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class ReceiptFilter
  implements Serializable
{
  private Object dateFilter;

  @SerializedName(a="filters")
  private Set<String> filterKeys;

  public final Set<String> a()
  {
    if (this.filterKeys == null)
      this.filterKeys = new HashSet();
    return this.filterKeys;
  }

  public final void a(String paramString)
  {
    a().add(paramString);
  }

  public final void a(String paramString1, String paramString2)
  {
    if (this.dateFilter == null)
      this.dateFilter = new DateFilter();
    DateFilter localDateFilter = new DateFilter();
    localDateFilter.a(paramString1);
    localDateFilter.b(paramString2);
    this.dateFilter = localDateFilter;
  }

  public final void b()
  {
    this.filterKeys = null;
  }

  public final void b(String paramString)
  {
    a().remove(paramString);
  }

  public final Object c()
  {
    return this.dateFilter;
  }

  public final void c(String paramString)
  {
    if (this.dateFilter == null)
      this.dateFilter = new String();
    this.dateFilter = paramString;
  }

  public class DateFilter
    implements Serializable
  {
    private String endDate;
    private String startDate;

    public DateFilter()
    {
    }

    public final String a()
    {
      return this.startDate;
    }

    public final void a(String paramString)
    {
      this.startDate = paramString;
    }

    public final String b()
    {
      return this.endDate;
    }

    public final void b(String paramString)
    {
      this.endDate = paramString;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.ReceiptFilter
 * JD-Core Version:    0.6.2
 */