package com.chase.sig.android.service;

import com.chase.sig.android.domain.Receipt;
import com.chase.sig.android.domain.ReceiptPhotoList;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ReceiptListResponse extends l
{
  private ArrayList<Receipt> receiptCollection;

  public final List<Receipt> a()
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = this.receiptCollection.iterator();
    while (localIterator.hasNext())
    {
      Receipt localReceipt = (Receipt)localIterator.next();
      String[] arrayOfString = localReceipt.j();
      ReceiptPhotoList localReceiptPhotoList = new ReceiptPhotoList();
      int i = arrayOfString.length;
      for (int j = 0; j < i; j++)
        localReceiptPhotoList.a(arrayOfString[j]);
      localReceipt.a(localReceiptPhotoList);
      localArrayList.add(localReceipt);
    }
    return localArrayList;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.ReceiptListResponse
 * JD-Core Version:    0.6.2
 */