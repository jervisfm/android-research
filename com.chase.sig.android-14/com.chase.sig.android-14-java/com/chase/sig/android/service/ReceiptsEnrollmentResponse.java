package com.chase.sig.android.service;

import com.chase.sig.android.domain.Agreement;
import com.chase.sig.android.domain.EnrollmentForm;

public class ReceiptsEnrollmentResponse extends GenericResponse
{
  Agreement agreement;
  EnrollmentForm enrollmentForm;
  String successMsg;

  public final String b()
  {
    return this.successMsg;
  }

  public final EnrollmentForm c()
  {
    return this.enrollmentForm;
  }

  public final Agreement d()
  {
    return this.agreement;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.ReceiptsEnrollmentResponse
 * JD-Core Version:    0.6.2
 */