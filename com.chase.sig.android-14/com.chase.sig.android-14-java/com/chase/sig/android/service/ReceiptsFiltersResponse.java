package com.chase.sig.android.service;

import com.chase.sig.android.domain.ChaseReceiptsFilters;
import com.google.gson.chase.annotations.SerializedName;

public class ReceiptsFiltersResponse extends GenericResponse
{

  @SerializedName(a="filters")
  ChaseReceiptsFilters chaseReceiptsFilters;
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.ReceiptsFiltersResponse
 * JD-Core Version:    0.6.2
 */