package com.chase.sig.android.service;

import com.chase.sig.android.domain.AccountActivityForReceipt;
import com.google.gson.chase.annotations.SerializedName;

public class ReceiptsTransactionMatchResponse extends GenericResponse
{

  @SerializedName(a="accountActivityRes")
  private AccountActivityForReceipt acctActivityForReceipt;

  @SerializedName(a="contentEmptyMessage")
  private String emptyListMessage;

  public final String b()
  {
    return this.emptyListMessage;
  }

  public final AccountActivityForReceipt c()
  {
    return this.acctActivityForReceipt;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.ReceiptsTransactionMatchResponse
 * JD-Core Version:    0.6.2
 */