package com.chase.sig.android.service;

import java.util.ArrayList;
import java.util.List;

public class ServiceError
  implements IServiceError
{
  private String code;
  private List<ServiceErrorAttribute> errorAttributes = new ArrayList();
  private boolean isFatal = true;
  private String message;
  private String title;

  public ServiceError(String paramString1, String paramString2, List<ServiceErrorAttribute> paramList, boolean paramBoolean)
  {
    this(paramString1, paramString2, paramBoolean);
    this.errorAttributes = paramList;
  }

  public ServiceError(String paramString1, String paramString2, boolean paramBoolean)
  {
    this.code = paramString1;
    this.message = paramString2;
    this.isFatal = paramBoolean;
  }

  public ServiceError(String paramString, boolean paramBoolean)
  {
    this.code = "LOCAL";
    this.message = paramString;
    this.isFatal = paramBoolean;
  }

  public final String a()
  {
    return this.message;
  }

  public final String b()
  {
    return this.title;
  }

  public final String c()
  {
    return this.code;
  }

  public final boolean d()
  {
    String str = this.code.trim();
    if ((str != null) && ((str.equals("10047")) || (str.equals("70033")) || (str.equals("50532"))))
      return false;
    return this.isFatal;
  }

  public final List<ServiceErrorAttribute> e()
  {
    return this.errorAttributes;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.ServiceError
 * JD-Core Version:    0.6.2
 */