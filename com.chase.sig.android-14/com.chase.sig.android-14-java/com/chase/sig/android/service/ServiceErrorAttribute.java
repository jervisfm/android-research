package com.chase.sig.android.service;

import java.io.Serializable;

public class ServiceErrorAttribute
  implements Serializable
{
  private final String key;
  private final String value;

  public ServiceErrorAttribute(String paramString1, String paramString2)
  {
    this.key = paramString1;
    this.value = paramString2;
  }

  public final String a()
  {
    return this.key;
  }

  public final boolean b()
  {
    if (this.value == null)
      return false;
    return "invalid".equals(this.value.toLowerCase());
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.ServiceErrorAttribute
 * JD-Core Version:    0.6.2
 */