package com.chase.sig.android.service;

import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.domain.Item;
import com.chase.sig.android.domain.ui.Action;
import com.chase.sig.android.domain.ui.Action.Url;
import java.io.Serializable;

public final class SmartService extends JPService
{
  public final SmartResponse a(Action.Url paramUrl)
  {
    try
    {
      SmartResponse localSmartResponse = (SmartResponse)a(b(paramUrl.a()), paramUrl.b(), SmartResponse.class);
      return localSmartResponse;
    }
    catch (Exception localException)
    {
    }
    return (SmartResponse)new SmartResponse().a(ChaseApplication.a().getApplicationContext());
  }

  public class SmartContent
    implements Serializable
  {
    private SmartService.SmartSection[] sections;

    public final SmartService.SmartSection[] a()
    {
      return this.sections;
    }
  }

  public class SmartResponse extends l
    implements Serializable
  {
    private SmartService.SmartContent content;

    public SmartResponse()
    {
    }

    public final SmartService.SmartContent a()
    {
      return this.content;
    }
  }

  public class SmartSection
    implements Serializable
  {
    private Action[] actions;
    private String id;
    private Item[] items;

    public final Item[] a()
    {
      return this.items;
    }

    public final Action[] b()
    {
      return this.actions;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.SmartService
 * JD-Core Version:    0.6.2
 */