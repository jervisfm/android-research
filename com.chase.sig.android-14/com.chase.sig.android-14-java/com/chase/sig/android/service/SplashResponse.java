package com.chase.sig.android.service;

import com.chase.sig.android.domain.SplashInfo;
import java.util.ArrayList;
import java.util.Iterator;

public class SplashResponse extends l
{
  private boolean alertsManagementBlocked = false;
  private String alertsManagementMessage;
  private boolean billPayBlocked = false;
  private String billPayMessage;
  private boolean ePayBlocked = false;
  private String ePayMessage;
  private boolean isSplashEnabled = false;
  private boolean photoBillPayBlocked = false;
  private String photoBillPayMessage;
  private boolean quickDepositBlocked = false;
  private String quickDepositMessage;
  private boolean quickPayBlocked = false;
  private String quickPayMessage;
  private boolean receiptBlocked = false;
  private String receiptMessage;
  private String splashUrl;
  private boolean transferBlocked = false;
  private String transferMessage;
  private String url;
  private boolean wiresBlocked = false;
  private String wiresMessage;

  public SplashResponse()
  {
  }

  public SplashResponse(ArrayList<SplashInfo> paramArrayList)
  {
    Iterator localIterator = paramArrayList.iterator();
    while (localIterator.hasNext())
    {
      SplashInfo localSplashInfo = (SplashInfo)localIterator.next();
      if (localSplashInfo != null)
        if (localSplashInfo.a().equalsIgnoreCase("billpay"))
        {
          this.billPayBlocked = true;
          this.billPayMessage = localSplashInfo.b();
        }
        else if (localSplashInfo.a().equalsIgnoreCase("epay"))
        {
          this.ePayBlocked = true;
          this.ePayMessage = localSplashInfo.b();
        }
        else if (localSplashInfo.a().equalsIgnoreCase("transfer"))
        {
          this.transferBlocked = true;
          this.transferMessage = localSplashInfo.b();
        }
        else if (localSplashInfo.a().equalsIgnoreCase("wires"))
        {
          this.wiresBlocked = true;
          this.wiresMessage = localSplashInfo.b();
        }
        else if (localSplashInfo.a().equalsIgnoreCase("quickdeposit"))
        {
          this.quickDepositBlocked = true;
          this.quickDepositMessage = localSplashInfo.b();
        }
        else if (localSplashInfo.a().equalsIgnoreCase("quickreceipt"))
        {
          this.receiptBlocked = true;
          this.receiptMessage = localSplashInfo.b();
        }
        else if (localSplashInfo.a().equalsIgnoreCase("quickpay"))
        {
          this.quickPayBlocked = true;
          this.quickPayMessage = localSplashInfo.b();
        }
        else if (localSplashInfo.a().equalsIgnoreCase("photobillpay"))
        {
          this.photoBillPayBlocked = true;
          this.photoBillPayMessage = localSplashInfo.b();
        }
        else if (localSplashInfo.a().equalsIgnoreCase("alertsmanagement"))
        {
          this.alertsManagementBlocked = true;
          this.alertsManagementMessage = localSplashInfo.b();
        }
    }
  }

  public final boolean a()
  {
    return this.quickPayBlocked;
  }

  public final boolean b()
  {
    return this.transferBlocked;
  }

  public final boolean c()
  {
    return this.billPayBlocked;
  }

  public final boolean d()
  {
    return this.quickDepositBlocked;
  }

  public final boolean j()
  {
    return this.ePayBlocked;
  }

  public final boolean k()
  {
    return this.wiresBlocked;
  }

  public final boolean l()
  {
    return this.receiptBlocked;
  }

  public final boolean m()
  {
    return this.photoBillPayBlocked;
  }

  public final String n()
  {
    return this.billPayMessage;
  }

  public final String o()
  {
    return this.ePayMessage;
  }

  public final String p()
  {
    return this.transferMessage;
  }

  public final String q()
  {
    return this.wiresMessage;
  }

  public final String r()
  {
    return this.quickDepositMessage;
  }

  public final String s()
  {
    return this.receiptMessage;
  }

  public final String t()
  {
    return this.quickPayMessage;
  }

  public final String u()
  {
    return this.photoBillPayMessage;
  }

  public final String v()
  {
    return this.splashUrl;
  }

  public final boolean w()
  {
    return this.isSplashEnabled;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.SplashResponse
 * JD-Core Version:    0.6.2
 */