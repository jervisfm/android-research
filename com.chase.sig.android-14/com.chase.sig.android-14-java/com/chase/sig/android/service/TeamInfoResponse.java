package com.chase.sig.android.service;

import com.chase.sig.android.domain.Advisor;
import java.util.List;

public class TeamInfoResponse extends l
{
  private List<Advisor> advisors;
  private String contactCompanyField;

  public final List<Advisor> a()
  {
    return this.advisors;
  }

  public final void a(String paramString)
  {
    this.contactCompanyField = paramString;
  }

  public final void a(List<Advisor> paramList)
  {
    this.advisors = paramList;
  }

  public final String b()
  {
    return this.contactCompanyField;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.TeamInfoResponse
 * JD-Core Version:    0.6.2
 */