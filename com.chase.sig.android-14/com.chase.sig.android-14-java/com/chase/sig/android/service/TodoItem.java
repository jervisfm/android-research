package com.chase.sig.android.service;

import com.chase.sig.android.util.Dollar;
import java.io.Serializable;

public class TodoItem
  implements Serializable
{
  private String actionLabel;
  private Dollar amount;
  private String declineLabel;
  private String header;
  private String id;
  private String isInvoiceRequest;
  private String subLabel;
  private String typeLabel;
  private String typeString;

  public final String a()
  {
    return this.header;
  }

  public final void a(Dollar paramDollar)
  {
    this.amount = paramDollar;
  }

  public final void a(String paramString)
  {
    this.header = paramString;
  }

  public final String b()
  {
    return this.actionLabel;
  }

  public final void b(String paramString)
  {
    this.actionLabel = paramString;
  }

  public final Dollar c()
  {
    return this.amount;
  }

  public final void c(String paramString)
  {
    this.subLabel = paramString;
  }

  public final String d()
  {
    return this.subLabel;
  }

  public final void d(String paramString)
  {
    this.declineLabel = paramString;
  }

  public final String e()
  {
    return this.declineLabel;
  }

  public final void e(String paramString)
  {
    this.id = paramString;
  }

  public final String f()
  {
    return this.id;
  }

  public final void f(String paramString)
  {
    this.typeString = paramString;
  }

  public final String g()
  {
    return this.typeString;
  }

  public final void g(String paramString)
  {
    this.typeLabel = paramString;
  }

  public final String h()
  {
    return this.isInvoiceRequest;
  }

  public final void h(String paramString)
  {
    this.isInvoiceRequest = paramString;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.TodoItem
 * JD-Core Version:    0.6.2
 */