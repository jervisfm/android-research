package com.chase.sig.android.service;

import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.domain.AccountActivity;
import com.chase.sig.android.domain.ActivityValue;
import com.chase.sig.android.util.Dollar;
import com.chase.sig.android.util.l;
import com.chase.sig.android.util.m;
import com.chase.sig.android.util.s;
import java.util.ArrayList;
import java.util.Hashtable;
import org.json.JSONArray;
import org.json.JSONObject;

public final class a extends JPService
{
  private static AccountActivity a(JSONObject paramJSONObject)
  {
    AccountActivity localAccountActivity = new AccountActivity();
    String str1 = paramJSONObject.optString("description");
    if (s.l(str1))
      str1 = "";
    localAccountActivity.a(str1);
    JSONArray localJSONArray = paramJSONObject.optJSONArray("values");
    localAccountActivity.b(paramJSONObject.optString("transNumber"));
    int i = 0;
    if (i < localJSONArray.length())
    {
      JSONObject localJSONObject = (JSONObject)localJSONArray.opt(i);
      String str2 = localJSONObject.optString("field");
      String str3 = localJSONObject.optString("value");
      String str4 = localJSONObject.optString("format");
      String str5;
      if (str4.equals("string"))
        str5 = s.a(str3);
      while (true)
      {
        localAccountActivity.b().add(new ActivityValue(str2, str5));
        i++;
        break;
        if (str4.equals("dollar"))
          str5 = s.a(new Dollar(str3).h());
        else if (str4.equals("date"))
          str5 = s.k(str3);
        else if (str4.equals("rate"))
          str5 = s.a(Double.valueOf(Double.parseDouble(str3)), "00.000000");
        else
          str5 = str3;
      }
    }
    return localAccountActivity;
  }

  public static AccountActivityResponse a(String paramString1, String paramString2, boolean paramBoolean1, boolean paramBoolean2)
  {
    String str = a(2131165264);
    AccountActivityResponse localAccountActivityResponse = new AccountActivityResponse();
    try
    {
      Hashtable localHashtable = c();
      localHashtable.put("accountId", paramString1);
      localHashtable.put("rows", "25");
      if ((ChaseApplication.a().j()) && (paramBoolean2))
        localHashtable.put("numDaysBack", String.valueOf(l.b()));
      if (paramBoolean1)
        localHashtable.put("start", paramString2);
      JSONObject localJSONObject1 = m.a(ChaseApplication.a(), str, localHashtable);
      localAccountActivityResponse.b(localJSONObject1);
      if (localAccountActivityResponse.e())
        return localAccountActivityResponse;
      JSONObject localJSONObject2 = localJSONObject1.getJSONObject("page");
      JSONArray localJSONArray = localJSONObject2.getJSONArray("activity");
      ArrayList localArrayList = new ArrayList();
      for (int i = 0; i < localJSONArray.length(); i++)
        localArrayList.add(a(localJSONArray.getJSONObject(i)));
      localAccountActivityResponse.a(localArrayList);
      localAccountActivityResponse.a(localJSONObject2.optString("end"));
      localAccountActivityResponse.a(localJSONObject2.optBoolean("more"));
      return localAccountActivityResponse;
    }
    catch (Exception localException)
    {
      localAccountActivityResponse.a(ChaseApplication.a().getApplicationContext());
    }
    return localAccountActivityResponse;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.a
 * JD-Core Version:    0.6.2
 */