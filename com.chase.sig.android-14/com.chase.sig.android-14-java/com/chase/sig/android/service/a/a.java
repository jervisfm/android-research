package com.chase.sig.android.service.a;

import com.chase.sig.android.domain.Account;
import com.chase.sig.android.domain.IAccount;
import com.chase.sig.android.domain.MaintenanceInfo;
import com.chase.sig.android.domain.c;
import com.chase.sig.android.domain.e;
import com.chase.sig.android.domain.k;
import com.chase.sig.android.service.JPService.a;
import com.chase.sig.android.util.n;
import com.google.gson.chase.Gson;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class a extends k
{
  public a(JSONObject paramJSONObject)
  {
    try
    {
      JSONObject localJSONObject1 = paramJSONObject.optJSONObject("profile");
      if (localJSONObject1 == null)
        return;
      JSONObject localJSONObject2 = paramJSONObject.optJSONObject("maintenance");
      if (localJSONObject2 != null)
        this.g = ((MaintenanceInfo)JPService.a.a(localJSONObject2.toString(), MaintenanceInfo.class));
      this.a = localJSONObject1.getString("type");
      JSONObject localJSONObject3 = localJSONObject1.optJSONObject("miniProfileGroups");
      List localList1 = a(localJSONObject3, "personalCustomers");
      List localList2 = a(localJSONObject3, "nonPersonalCustomers");
      this.b = localList1;
      this.c = localList2;
      int i;
      int j;
      if (this.b == null)
      {
        i = 0;
        j = i + 0;
        if (this.c != null)
          break label226;
      }
      label226: int n;
      for (int k = 0; ; k = n)
      {
        int m = k + j;
        if (m > 0)
        {
          this.d = new ArrayList(m);
          if (this.b != null)
            this.d.addAll(this.b);
          if (this.c != null)
            this.d.addAll(this.c);
        }
        this.e = a(localJSONObject1, "profilePrivileges", "name");
        this.f = c(localJSONObject1, "profileFeatures");
        return;
        i = this.b.size();
        break;
        n = this.c.size();
      }
    }
    catch (JSONException localJSONException)
    {
    }
  }

  private List<e> a(JSONObject paramJSONObject, String paramString)
  {
    JSONArray localJSONArray = paramJSONObject.optJSONArray(paramString);
    Object localObject1 = null;
    if (localJSONArray != null)
    {
      ArrayList localArrayList = new ArrayList();
      int i = localJSONArray.length();
      int j = 0;
      if (j < i)
      {
        JSONObject localJSONObject = localJSONArray.getJSONObject(j);
        if (localJSONObject == null);
        c localc;
        for (Object localObject2 = null; ; localObject2 = localc)
        {
          localArrayList.add(localObject2);
          j++;
          break;
          localc = new c();
          localc.a(localJSONObject.getString("id"));
          localc.b(localJSONObject.getString("name"));
          localc.a((ArrayList)b(localJSONObject, "accounts"));
        }
      }
      localObject1 = localArrayList;
    }
    return localObject1;
  }

  private static Set<String> a(JSONObject paramJSONObject, String paramString1, String paramString2)
  {
    boolean bool = paramJSONObject.isNull(paramString1);
    Object localObject = null;
    if (bool);
    while (true)
    {
      return localObject;
      JSONArray localJSONArray = paramJSONObject.getJSONArray(paramString1);
      localObject = null;
      if (localJSONArray != null)
      {
        int i = localJSONArray.length();
        localObject = new HashSet();
        for (int j = 0; j < i; j++)
          ((Set)localObject).add(localJSONArray.getJSONObject(j).getString(paramString2));
      }
    }
  }

  private List<IAccount> b(JSONObject paramJSONObject, String paramString)
  {
    if (paramJSONObject.isNull(paramString));
    JSONArray localJSONArray1;
    do
    {
      return null;
      localJSONArray1 = paramJSONObject.getJSONArray(paramString);
    }
    while (localJSONArray1 == null);
    ArrayList localArrayList = new ArrayList();
    int i = localJSONArray1.length();
    int j = 0;
    JSONObject localJSONObject1;
    Object localObject;
    Account localAccount;
    List localList;
    label239: Hashtable localHashtable;
    while (true)
      if (j < i)
      {
        localJSONObject1 = localJSONArray1.getJSONObject(j);
        if (localJSONObject1 == null)
        {
          localObject = null;
          localArrayList.add(localObject);
          j++;
        }
        else
        {
          localAccount = new Account();
          localAccount.d(localJSONObject1.getString("type"));
          localAccount.e(localJSONObject1.optBoolean("omni", false));
          localAccount.c(localJSONObject1.getBoolean("BCCControl"));
          localAccount.d(localJSONObject1.getBoolean("BCCEmployee"));
          localAccount.b(localJSONObject1.getBoolean("BCCOfficer"));
          localAccount.a(localJSONObject1.getString("mask"));
          localAccount.a(d(localJSONObject1, "disclosureIds"));
          localAccount.b(localJSONObject1.getString("id"));
          localAccount.e(n.b(localJSONObject1, "nickname"));
          localAccount.a(b(localJSONObject1, "accounts"));
          if (localJSONObject1.isNull("accountDetailList"))
          {
            localList = null;
            localAccount.b(localList);
            if (!localJSONObject1.isNull("details"))
              break label374;
            localHashtable = null;
          }
        }
      }
    while (true)
    {
      localAccount.a(localHashtable);
      localAccount.b(a(localJSONObject1, "accountPrivileges", "name"));
      if ((localJSONObject1.has("standInAsofDate")) && (!localJSONObject1.isNull("standInAsofDate")))
        localAccount.g(localJSONObject1.getString("standInAsofDate"));
      localObject = localAccount;
      break;
      JSONArray localJSONArray2 = localJSONObject1.optJSONArray("accountDetailList");
      Type localType = new b(this).b();
      localList = (List)new Gson().a(localJSONArray2.toString(), localType);
      break label239;
      label374: JSONObject localJSONObject2 = localJSONObject1.getJSONObject("details");
      if (localJSONObject2 != null)
      {
        localHashtable = c(localJSONObject2, "values");
        continue;
        return localArrayList;
      }
      else
      {
        localHashtable = null;
      }
    }
  }

  private static Hashtable<String, String> c(JSONObject paramJSONObject, String paramString)
  {
    if (paramJSONObject.isNull(paramString));
    JSONObject localJSONObject;
    do
    {
      return null;
      localJSONObject = paramJSONObject.getJSONObject(paramString);
    }
    while (localJSONObject == null);
    Hashtable localHashtable = new Hashtable();
    Iterator localIterator = localJSONObject.keys();
    while (localIterator.hasNext())
    {
      String str = (String)localIterator.next();
      localHashtable.put(str, localJSONObject.getString(str));
    }
    return localHashtable;
  }

  private static Set<String> d(JSONObject paramJSONObject, String paramString)
  {
    boolean bool = paramJSONObject.isNull(paramString);
    Object localObject = null;
    if (bool);
    while (true)
    {
      return localObject;
      JSONArray localJSONArray = paramJSONObject.getJSONArray(paramString);
      localObject = null;
      if (localJSONArray != null)
      {
        int i = localJSONArray.length();
        localObject = new HashSet();
        for (int j = 0; j < i; j++)
          ((Set)localObject).add(localJSONArray.getString(j));
      }
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.a.a
 * JD-Core Version:    0.6.2
 */