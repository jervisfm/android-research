package com.chase.sig.android.service;

import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.domain.QuoteResponse;
import com.chase.sig.android.util.s;
import java.util.Hashtable;

public final class aa extends JPService
{
  public static QuoteResponse a(String paramString)
  {
    String str = a(2131165227);
    QuoteResponse localQuoteResponse1 = new QuoteResponse();
    try
    {
      Hashtable localHashtable = c();
      localHashtable.put("tickerSymbol", paramString);
      QuoteResponse localQuoteResponse2 = (QuoteResponse)a(str, localHashtable, QuoteResponse.class);
      return localQuoteResponse2;
    }
    catch (Exception localException)
    {
      localQuoteResponse1.a(ChaseApplication.a().getApplicationContext());
    }
    return localQuoteResponse1;
  }

  public static QuoteResponse a(String paramString1, String paramString2)
  {
    String str = a(2131165226);
    QuoteResponse localQuoteResponse1 = new QuoteResponse();
    try
    {
      Hashtable localHashtable = c();
      localHashtable.put("tickerSymbol", String.format("[\"%s\"]", new Object[] { paramString1 }));
      if (s.m(paramString2))
        localHashtable.put("quantity", paramString2.split("\\.")[0]);
      QuoteResponse localQuoteResponse2 = (QuoteResponse)a(str, localHashtable, QuoteResponse.class);
      return localQuoteResponse2;
    }
    catch (Exception localException)
    {
      localQuoteResponse1.a(ChaseApplication.a().getApplicationContext());
    }
    return localQuoteResponse1;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.aa
 * JD-Core Version:    0.6.2
 */