package com.chase.sig.android.service;

import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.domain.LabeledValue;
import com.chase.sig.android.domain.Receipt;
import com.chase.sig.android.domain.ReceiptPhotoList;
import com.chase.sig.android.domain.ReceiptPhotoList.ReceiptPhoto;
import com.chase.sig.android.util.ChaseException;
import com.chase.sig.android.util.Dollar;
import com.chase.sig.android.util.f;
import com.chase.sig.android.util.f.a;
import com.chase.sig.android.util.m;
import com.chase.sig.android.util.s;
import java.util.Hashtable;
import java.util.List;
import org.json.JSONObject;

public final class ac extends JPService
{
  private p a;

  public ac(p paramp)
  {
    this.a = paramp;
  }

  public static ReceiptListResponse a(Hashtable<String, String> paramHashtable)
  {
    ReceiptListResponse localReceiptListResponse1 = new ReceiptListResponse();
    String str = a(2131165246);
    try
    {
      paramHashtable.putAll(c());
      ReceiptListResponse localReceiptListResponse2 = (ReceiptListResponse)a(str, paramHashtable, ReceiptListResponse.class);
      return localReceiptListResponse2;
    }
    catch (Exception localException)
    {
      localReceiptListResponse1.a(ChaseApplication.a().getApplicationContext());
    }
    return localReceiptListResponse1;
  }

  public static l a(Receipt paramReceipt)
  {
    l locall1 = new l();
    String str = a(2131165251);
    Hashtable localHashtable = c(paramReceipt);
    localHashtable.put("quickReceiptId", paramReceipt.j()[0]);
    try
    {
      l locall2 = (l)a(str, localHashtable, l.class);
      return locall2;
    }
    catch (Exception localException)
    {
      locall1.a(ChaseApplication.a().getApplicationContext());
    }
    return locall1;
  }

  public static l a(Receipt paramReceipt, boolean paramBoolean)
  {
    l locall1 = new l();
    String str = a(2131165250);
    Hashtable localHashtable = c(paramReceipt);
    localHashtable.put("quickReceiptId", paramReceipt.i());
    if ((paramReceipt.f() != null) && (!paramBoolean) && (!s.B(paramReceipt.f())))
    {
      localHashtable.put("transactionId", paramReceipt.f());
      localHashtable.put("collectionId", paramReceipt.g());
    }
    try
    {
      l locall2 = (l)a(str, localHashtable, l.class);
      return locall2;
    }
    catch (Exception localException)
    {
      locall1.a(ChaseApplication.a().getApplicationContext());
    }
    return locall1;
  }

  public static byte[] a(String paramString)
  {
    String str1 = paramString.replaceAll("\\{", "").replaceAll("\\}", "");
    Hashtable localHashtable1 = new Hashtable();
    localHashtable1.put("quickReceiptId", str1);
    String str2 = a(2131165253);
    try
    {
      localHashtable1.putAll(c());
      f localf = new f(ChaseApplication.a());
      localf.a = null;
      Hashtable localHashtable2 = JPService.c();
      localHashtable2.putAll(localHashtable1);
      byte[] arrayOfByte = localf.a(str2, f.a(localHashtable2), false).b;
      if (arrayOfByte == null)
        throw new ChaseException(ChaseException.b, String.format("Did not recieve BYTE response from %s", new Object[] { str2 }));
      new StringBuilder("Service HTTP Response Size: ").append(arrayOfByte.length).toString();
      return arrayOfByte;
    }
    catch (Exception localException)
    {
    }
    return null;
  }

  public static ReceiptsTransactionMatchResponse b(Receipt paramReceipt)
  {
    ReceiptsTransactionMatchResponse localReceiptsTransactionMatchResponse1 = new ReceiptsTransactionMatchResponse();
    String str = a(2131165252);
    Hashtable localHashtable = c(paramReceipt);
    localHashtable.put("rows", "50");
    localHashtable.put("start", "1");
    try
    {
      ReceiptsTransactionMatchResponse localReceiptsTransactionMatchResponse2 = (ReceiptsTransactionMatchResponse)a(str, localHashtable, ReceiptsTransactionMatchResponse.class);
      return localReceiptsTransactionMatchResponse2;
    }
    catch (Exception localException)
    {
      localReceiptsTransactionMatchResponse1.a(ChaseApplication.a().getApplicationContext());
    }
    return localReceiptsTransactionMatchResponse1;
  }

  private static Hashtable<String, String> c(Receipt paramReceipt)
  {
    Hashtable localHashtable = c();
    localHashtable.put("accountId", paramReceipt.a());
    if (paramReceipt.e() == null);
    for (String str = ""; ; str = paramReceipt.e())
    {
      localHashtable.put("transactionDate", str);
      localHashtable.put("description", paramReceipt.c());
      localHashtable.put("amount", paramReceipt.b().g());
      localHashtable.put("category", paramReceipt.k().b());
      if (paramReceipt.l() != null)
        localHashtable.put("expenseType", paramReceipt.l().b());
      if (paramReceipt.m() != null)
        localHashtable.put("taxDeductable", paramReceipt.m().b());
      return localHashtable;
    }
  }

  public static ReceiptAccountSummaryResponse d()
  {
    ReceiptAccountSummaryResponse localReceiptAccountSummaryResponse1 = new ReceiptAccountSummaryResponse();
    String str = a(2131165248);
    try
    {
      ReceiptAccountSummaryResponse localReceiptAccountSummaryResponse2 = (ReceiptAccountSummaryResponse)a(str, c(), ReceiptAccountSummaryResponse.class);
      return localReceiptAccountSummaryResponse2;
    }
    catch (Exception localException)
    {
      localReceiptAccountSummaryResponse1.a(ChaseApplication.a().getApplicationContext());
    }
    return localReceiptAccountSummaryResponse1;
  }

  public final ListContentResponse a()
  {
    return p.a(new String[] { "quickreceipts-filters" });
  }

  public final ReceiptsAddResponse a(Receipt paramReceipt, int paramInt)
  {
    ReceiptsAddResponse localReceiptsAddResponse = new ReceiptsAddResponse();
    String str1 = a(2131165249);
    Hashtable localHashtable = c(paramReceipt);
    String str2 = paramReceipt.g();
    if (s.m(str2))
      localHashtable.put("collectionId", str2);
    String str3 = paramReceipt.f();
    if (s.m(str3))
      localHashtable.put("transactionId", str3);
    List localList = paramReceipt.h().a();
    String[] arrayOfString1 = { "image/jpeg" };
    String[] arrayOfString2 = { "receiptImage" };
    byte[][] arrayOfByte = new byte[1][];
    arrayOfByte[0] = ((ReceiptPhotoList.ReceiptPhoto)localList.get(paramInt)).b();
    try
    {
      JSONObject localJSONObject = m.a(ChaseApplication.a(), str1, localHashtable, arrayOfString2, arrayOfByte, arrayOfString1, ChaseApplication.a().h());
      localReceiptsAddResponse.b(localJSONObject);
      String str4 = localJSONObject.optString("collectionId");
      if (paramInt < 3)
      {
        int i = paramInt + 1;
        if (i < localList.size())
        {
          paramReceipt.e(str4);
          a(paramReceipt, i);
        }
      }
      return localReceiptsAddResponse;
    }
    catch (ChaseException localChaseException)
    {
      localReceiptsAddResponse.a(ChaseApplication.a().getApplicationContext());
    }
    return localReceiptsAddResponse;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.ac
 * JD-Core Version:    0.6.2
 */