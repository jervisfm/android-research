package com.chase.sig.android.service;

import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.util.m;
import java.util.Hashtable;
import org.json.JSONException;
import org.json.JSONObject;

public final class ad extends JPService
{
  public static ReceiptsEnrollmentResponse a()
  {
    String str = a(2131165243);
    ReceiptsEnrollmentResponse localReceiptsEnrollmentResponse1 = new ReceiptsEnrollmentResponse();
    try
    {
      Hashtable localHashtable = c();
      JSONObject localJSONObject = m.a(ChaseApplication.a(), str, localHashtable);
      localReceiptsEnrollmentResponse1.b(localJSONObject);
      if (localReceiptsEnrollmentResponse1.e())
        return localReceiptsEnrollmentResponse1;
      ReceiptsEnrollmentResponse localReceiptsEnrollmentResponse2 = a(localJSONObject.toString());
      return localReceiptsEnrollmentResponse2;
    }
    catch (JSONException localJSONException)
    {
      localReceiptsEnrollmentResponse1.a(ChaseApplication.a().getApplicationContext());
      return localReceiptsEnrollmentResponse1;
    }
    catch (Exception localException)
    {
      localReceiptsEnrollmentResponse1.a(ChaseApplication.a().getApplicationContext());
    }
    return localReceiptsEnrollmentResponse1;
  }

  private static ReceiptsEnrollmentResponse a(String paramString)
  {
    return (ReceiptsEnrollmentResponse)JPService.a.a(paramString, ReceiptsEnrollmentResponse.class);
  }

  public static ReceiptsEnrollmentResponse a(String paramString1, String paramString2)
  {
    String str = a(2131165242);
    ReceiptsEnrollmentResponse localReceiptsEnrollmentResponse1 = new ReceiptsEnrollmentResponse();
    try
    {
      Hashtable localHashtable = c();
      localHashtable.put("planCode", paramString2);
      if (paramString1 != null)
        localHashtable.put("accountId", paramString1);
      localHashtable.put("validateOnly", "true");
      JSONObject localJSONObject = m.a(ChaseApplication.a(), str, localHashtable);
      localReceiptsEnrollmentResponse1.b(localJSONObject);
      if (localReceiptsEnrollmentResponse1.e())
        return localReceiptsEnrollmentResponse1;
      ReceiptsEnrollmentResponse localReceiptsEnrollmentResponse2 = a(localJSONObject.toString());
      return localReceiptsEnrollmentResponse2;
    }
    catch (JSONException localJSONException)
    {
      localReceiptsEnrollmentResponse1.a(ChaseApplication.a().getApplicationContext());
      return localReceiptsEnrollmentResponse1;
    }
    catch (Exception localException)
    {
      localReceiptsEnrollmentResponse1.a(ChaseApplication.a().getApplicationContext());
    }
    return localReceiptsEnrollmentResponse1;
  }

  public static ReceiptsEnrollmentResponse b(String paramString1, String paramString2)
  {
    String str = a(2131165242);
    ReceiptsEnrollmentResponse localReceiptsEnrollmentResponse1 = new ReceiptsEnrollmentResponse();
    try
    {
      Hashtable localHashtable = c();
      localHashtable.put("formId", paramString1);
      localHashtable.put("action", paramString2);
      localHashtable.put("validateOnly", "false");
      JSONObject localJSONObject = m.a(ChaseApplication.a(), str, localHashtable);
      localReceiptsEnrollmentResponse1.b(localJSONObject);
      if (localReceiptsEnrollmentResponse1.e())
        return localReceiptsEnrollmentResponse1;
      ReceiptsEnrollmentResponse localReceiptsEnrollmentResponse2 = a(localJSONObject.toString());
      return localReceiptsEnrollmentResponse2;
    }
    catch (JSONException localJSONException)
    {
      localReceiptsEnrollmentResponse1.a(ChaseApplication.a().getApplicationContext());
      return localReceiptsEnrollmentResponse1;
    }
    catch (Exception localException)
    {
      localReceiptsEnrollmentResponse1.a(ChaseApplication.a().getApplicationContext());
    }
    return localReceiptsEnrollmentResponse1;
  }

  public static ReceiptsEnrollmentResponse c(String paramString1, String paramString2)
  {
    String str = a(2131165245);
    ReceiptsEnrollmentResponse localReceiptsEnrollmentResponse1 = new ReceiptsEnrollmentResponse();
    try
    {
      Hashtable localHashtable = c();
      localHashtable.put("planCode", paramString1);
      if (paramString2 != null)
        localHashtable.put("accountId", paramString2);
      JSONObject localJSONObject = m.a(ChaseApplication.a(), str, localHashtable);
      localReceiptsEnrollmentResponse1.b(localJSONObject);
      if (localReceiptsEnrollmentResponse1.e())
        return localReceiptsEnrollmentResponse1;
      ReceiptsEnrollmentResponse localReceiptsEnrollmentResponse2 = a(localJSONObject.toString());
      return localReceiptsEnrollmentResponse2;
    }
    catch (Exception localException)
    {
      localReceiptsEnrollmentResponse1.a(ChaseApplication.a().getApplicationContext());
    }
    return localReceiptsEnrollmentResponse1;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.ad
 * JD-Core Version:    0.6.2
 */