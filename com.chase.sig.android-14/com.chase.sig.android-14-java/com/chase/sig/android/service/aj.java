package com.chase.sig.android.service;

import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.domain.Advisor;
import com.chase.sig.android.util.m;
import com.chase.sig.android.util.n;
import com.chase.sig.android.util.s;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class aj extends JPService
{
  public static TeamInfoResponse a()
  {
    String str1 = a(2131165229);
    TeamInfoResponse localTeamInfoResponse = new TeamInfoResponse();
    Hashtable localHashtable = c();
    while (true)
    {
      int j;
      try
      {
        JSONObject localJSONObject1 = m.a(ChaseApplication.a(), str1, localHashtable);
        localTeamInfoResponse.b(localJSONObject1);
        if (localJSONObject1 != null)
        {
          localTeamInfoResponse.a(localJSONObject1.getString("contactCompanyField"));
          ArrayList localArrayList = new ArrayList();
          if (n.a(localJSONObject1, "advisorView"))
          {
            JSONArray localJSONArray = localJSONObject1.getJSONArray("advisorView");
            int i = localJSONArray.length();
            if (i > 0)
            {
              j = 0;
              if (j < i)
              {
                JSONObject localJSONObject2 = localJSONArray.getJSONObject(j);
                if (localJSONObject2 == null)
                  break label292;
                String str2 = localJSONObject2.optString("displayName");
                String str3 = n.b(localJSONObject2, "phone");
                String str4 = n.b(localJSONObject2, "email");
                String str5 = localJSONObject2.optString("middleInitial");
                String str6 = localJSONObject2.optString("relationship");
                String str7 = localJSONObject2.optString("firstName");
                String str8 = localJSONObject2.optString("lastName");
                if (!s.m(str2))
                  break label298;
                str9 = str2.trim();
                if ("Client Service Team".equals(str9))
                  break label286;
                if (!"Private Banking Mobile Support".equals(str9))
                  break label305;
                break label286;
                localArrayList.add(new Advisor(str3, str4, str5, str6, str7, str8, str2, bool));
                break label292;
              }
            }
          }
          localTeamInfoResponse.a(localArrayList);
        }
        else
        {
          return localTeamInfoResponse;
        }
      }
      catch (JSONException localJSONException)
      {
        localTeamInfoResponse.a(ChaseApplication.a().getApplicationContext());
        return localTeamInfoResponse;
      }
      catch (Exception localException)
      {
        localTeamInfoResponse.a(ChaseApplication.a().getApplicationContext());
        return localTeamInfoResponse;
      }
      label286: boolean bool = true;
      continue;
      label292: j++;
      continue;
      label298: String str9 = "";
      continue;
      label305: bool = false;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.aj
 * JD-Core Version:    0.6.2
 */