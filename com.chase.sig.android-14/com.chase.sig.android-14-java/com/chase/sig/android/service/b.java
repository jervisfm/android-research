package com.chase.sig.android.service;

import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.domain.AccountDetail;
import com.chase.sig.android.util.m;
import java.util.Hashtable;
import java.util.Iterator;
import org.json.JSONObject;

public final class b extends JPService
{
  public static AccountDetailResponse a(String paramString)
  {
    AccountDetailResponse localAccountDetailResponse = new AccountDetailResponse();
    AccountDetail localAccountDetail = new AccountDetail();
    String str1 = a(2131165198);
    Hashtable localHashtable1 = c();
    localHashtable1.put("accountId", paramString);
    Hashtable localHashtable2;
    try
    {
      JSONObject localJSONObject1 = m.a(ChaseApplication.a(), str1, localHashtable1);
      localAccountDetailResponse.b(localJSONObject1);
      JSONObject localJSONObject2 = localJSONObject1.optJSONObject("detail");
      if (localJSONObject2 != null)
      {
        localAccountDetail.a(localJSONObject2.optString("id"));
        localAccountDetail.a(localJSONObject2.optBoolean("failed"));
        localAccountDetail.b(localJSONObject2.optBoolean("loaded"));
        localHashtable2 = new Hashtable();
        JSONObject localJSONObject3 = localJSONObject2.optJSONObject("values");
        if (localJSONObject3 == null)
          break label194;
        Iterator localIterator = (Iterator)localJSONObject3.keys();
        while (localIterator.hasNext())
        {
          String str2 = (String)localIterator.next();
          localHashtable2.put(str2, localJSONObject3.getString(str2));
        }
      }
    }
    catch (Exception localException)
    {
      localAccountDetailResponse.a(ChaseApplication.a().getApplicationContext());
    }
    return localAccountDetailResponse;
    localAccountDetail.a(localHashtable2);
    label194: localAccountDetailResponse.a(localAccountDetail);
    return localAccountDetailResponse;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.b
 * JD-Core Version:    0.6.2
 */