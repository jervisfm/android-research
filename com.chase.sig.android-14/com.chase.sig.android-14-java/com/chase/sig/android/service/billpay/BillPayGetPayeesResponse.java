package com.chase.sig.android.service.billpay;

import com.chase.sig.android.domain.Payee;
import com.chase.sig.android.service.l;
import java.util.List;

public class BillPayGetPayeesResponse extends l
{
  private List<Payee> payees;

  public final List<Payee> a()
  {
    return this.payees;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.billpay.BillPayGetPayeesResponse
 * JD-Core Version:    0.6.2
 */