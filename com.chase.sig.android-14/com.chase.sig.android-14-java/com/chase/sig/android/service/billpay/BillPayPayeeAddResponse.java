package com.chase.sig.android.service.billpay;

import com.chase.sig.android.domain.Payee;
import com.chase.sig.android.service.l;
import java.io.Serializable;

public class BillPayPayeeAddResponse extends l
  implements Serializable
{
  private String formId;
  private Payee payee;

  public final String a()
  {
    return this.formId;
  }

  public final Payee b()
  {
    return this.payee;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.billpay.BillPayPayeeAddResponse
 * JD-Core Version:    0.6.2
 */