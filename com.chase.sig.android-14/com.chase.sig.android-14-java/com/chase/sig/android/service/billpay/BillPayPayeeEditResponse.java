package com.chase.sig.android.service.billpay;

import com.chase.sig.android.service.l;
import java.io.Serializable;

public class BillPayPayeeEditResponse extends l
  implements Serializable
{
  private String accountNumberMask;
  private String formId;
  private int leadTime = -1;
  private String payeeId;
  private String status;

  public final String a()
  {
    return this.formId;
  }

  public final void a(int paramInt)
  {
    this.leadTime = paramInt;
  }

  public final void a(String paramString)
  {
    this.formId = paramString;
  }

  public final String b()
  {
    return this.status;
  }

  public final void b(String paramString)
  {
    this.payeeId = paramString;
  }

  public final String c()
  {
    switch (this.leadTime)
    {
    case 3:
    case 4:
    default:
      return "";
    case 0:
      return "Same day";
    case 1:
      return "1-day";
    case 2:
      return "2-day";
    case 5:
    }
    return "5-day";
  }

  public final String d()
  {
    return this.accountNumberMask;
  }

  public final void e(String paramString)
  {
    this.status = paramString;
  }

  public final void f(String paramString)
  {
    this.accountNumberMask = paramString;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.billpay.BillPayPayeeEditResponse
 * JD-Core Version:    0.6.2
 */