package com.chase.sig.android.service.billpay;

import com.chase.sig.android.domain.ImageCaptureData;
import com.chase.sig.android.domain.MerchantPayee;
import com.chase.sig.android.service.l;
import java.util.List;

public class BillPayPayeeSearchResponse extends l
{
  private ImageCaptureData capturedData = new ImageCaptureData();
  private String flowIndicator;
  private List<MerchantPayee> merchantPayees;

  public final String a()
  {
    return this.flowIndicator;
  }

  public final void a(ImageCaptureData paramImageCaptureData)
  {
    this.capturedData = paramImageCaptureData;
  }

  public final void a(String paramString)
  {
    this.flowIndicator = paramString;
  }

  public final void a(List<MerchantPayee> paramList)
  {
    this.merchantPayees = paramList;
  }

  public final ImageCaptureData b()
  {
    return this.capturedData;
  }

  public final List<MerchantPayee> c()
  {
    return this.merchantPayees;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.billpay.BillPayPayeeSearchResponse
 * JD-Core Version:    0.6.2
 */