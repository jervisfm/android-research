package com.chase.sig.android.service.billpay;

import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.domain.BillPayTransaction;
import com.chase.sig.android.domain.ImageCaptureData;
import com.chase.sig.android.domain.MerchantPayee;
import com.chase.sig.android.domain.Payee;
import com.chase.sig.android.domain.PayeeAddress;
import com.chase.sig.android.domain.State;
import com.chase.sig.android.service.movemoney.e;
import com.chase.sig.android.service.movemoney.f;
import com.chase.sig.android.service.movemoney.g;
import com.chase.sig.android.util.Dollar;
import com.chase.sig.android.util.m;
import com.chase.sig.android.util.s;
import com.google.gson.chase.Gson;
import com.google.gson.chase.GsonBuilder;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public final class b extends com.chase.sig.android.service.movemoney.d<BillPayTransaction>
{
  public b()
  {
  }

  public b(g paramg, e parame, f<BillPayTransaction> paramf)
  {
    super(paramg, parame, paramf);
  }

  private static MerchantPayee a(JSONObject paramJSONObject)
  {
    MerchantPayee localMerchantPayee = new MerchantPayee();
    localMerchantPayee.a(paramJSONObject.optString("optionId"));
    localMerchantPayee.b(paramJSONObject.optString("name"));
    localMerchantPayee.c(paramJSONObject.optString("phone"));
    JSONObject localJSONObject;
    if (paramJSONObject.has("address"))
    {
      localJSONObject = (JSONObject)paramJSONObject.opt("address");
      if (localJSONObject != null)
      {
        localMerchantPayee.d(localJSONObject.optString("addressLine1"));
        if (!s.l(localJSONObject.optString("addressLine2")))
          break label126;
      }
    }
    label126: for (String str = ""; ; str = localJSONObject.optString("addressLine2"))
    {
      localMerchantPayee.e(str);
      localMerchantPayee.f(localJSONObject.optString("city"));
      localMerchantPayee.g(localJSONObject.optString("postalCode"));
      localMerchantPayee.a(State.a(localJSONObject.optString("state")));
      return localMerchantPayee;
    }
  }

  public static BillPayGetPayeesResponse a()
  {
    BillPayGetPayeesResponse localBillPayGetPayeesResponse1 = new BillPayGetPayeesResponse();
    try
    {
      String str1 = a(2131165262);
      Hashtable localHashtable = c();
      localHashtable.put("status", "[\"Active\",\"Rejected\",\"Pending Review\"]");
      JSONObject localJSONObject = m.a(ChaseApplication.a(), str1, localHashtable);
      localBillPayGetPayeesResponse1.b(localJSONObject);
      if (localBillPayGetPayeesResponse1.e())
        return localBillPayGetPayeesResponse1;
      String str2 = localJSONObject.toString();
      localBillPayGetPayeesResponse2 = (BillPayGetPayeesResponse)new GsonBuilder().a(Dollar.class, new com.chase.sig.android.domain.d()).b().a(str2, BillPayGetPayeesResponse.class);
      return localBillPayGetPayeesResponse2;
    }
    catch (Exception localException)
    {
      while (true)
      {
        localBillPayGetPayeesResponse1.a(ChaseApplication.a().getApplicationContext());
        BillPayGetPayeesResponse localBillPayGetPayeesResponse2 = localBillPayGetPayeesResponse1;
      }
    }
  }

  public static BillPayPayeeAddResponse a(a parama)
  {
    BillPayPayeeAddResponse localBillPayPayeeAddResponse1 = new BillPayPayeeAddResponse();
    try
    {
      String str = a(2131165259);
      Hashtable localHashtable = c();
      localHashtable.put("validateOnly", Boolean.toString(true));
      localHashtable.put("name", parama.a);
      localHashtable.put("nickName", parama.b);
      b(localHashtable, "accountNumber", parama.c);
      localHashtable.put("addressLine1", parama.d);
      b(localHashtable, "addressLine2", parama.e);
      b(localHashtable, "addressLine3", parama.f);
      localHashtable.put("city", parama.g);
      localHashtable.put("state", parama.h);
      localHashtable.put("zipCode", parama.i.replace("-", ""));
      if (s.m(parama.j))
        localHashtable.put("phone", parama.j.replaceAll("-", ""));
      b(localHashtable, "optionId", parama.k);
      b(localHashtable, "memo", parama.l);
      JSONObject localJSONObject = m.a(ChaseApplication.a(), str, localHashtable);
      localBillPayPayeeAddResponse1.b(localJSONObject);
      if (localBillPayPayeeAddResponse1.e())
        return localBillPayPayeeAddResponse1;
      localBillPayPayeeAddResponse2 = (BillPayPayeeAddResponse)new Gson().a(localJSONObject.toString(), BillPayPayeeAddResponse.class);
      return localBillPayPayeeAddResponse2;
    }
    catch (Exception localException)
    {
      while (true)
      {
        localBillPayPayeeAddResponse1.a(ChaseApplication.a().getApplicationContext());
        BillPayPayeeAddResponse localBillPayPayeeAddResponse2 = localBillPayPayeeAddResponse1;
      }
    }
  }

  public static BillPayPayeeAddResponse a(String paramString1, String paramString2)
  {
    BillPayPayeeAddResponse localBillPayPayeeAddResponse1 = new BillPayPayeeAddResponse();
    try
    {
      String str = a(2131165259);
      Hashtable localHashtable = c();
      localHashtable.put("validateOnly", Boolean.toString(false));
      localHashtable.put("formId", paramString2);
      b(localHashtable, "optionId", paramString1);
      JSONObject localJSONObject = m.a(ChaseApplication.a(), str, localHashtable);
      localBillPayPayeeAddResponse1.b(localJSONObject);
      if (localBillPayPayeeAddResponse1.e())
        return localBillPayPayeeAddResponse1;
      localBillPayPayeeAddResponse2 = (BillPayPayeeAddResponse)new Gson().a(localJSONObject.toString(), BillPayPayeeAddResponse.class);
      return localBillPayPayeeAddResponse2;
    }
    catch (Exception localException)
    {
      while (true)
      {
        localBillPayPayeeAddResponse1.a(ChaseApplication.a().getApplicationContext());
        BillPayPayeeAddResponse localBillPayPayeeAddResponse2 = localBillPayPayeeAddResponse1;
      }
    }
  }

  public static BillPayPayeeEditResponse a(Payee paramPayee)
  {
    BillPayPayeeEditResponse localBillPayPayeeEditResponse = new BillPayPayeeEditResponse();
    try
    {
      String str = a(2131165263);
      Hashtable localHashtable = c();
      PayeeAddress localPayeeAddress = paramPayee.o();
      localHashtable.put("payeeId", paramPayee.j());
      localHashtable.put("token", paramPayee.n());
      localHashtable.put("name", paramPayee.i());
      localHashtable.put("nickName", paramPayee.h());
      localHashtable.put("accountNumber", s.s(paramPayee.c()));
      localHashtable.put("addressLine1", localPayeeAddress.a());
      localHashtable.put("addressLine2", s.s(localPayeeAddress.b()));
      localHashtable.put("city", localPayeeAddress.c());
      localHashtable.put("state", localPayeeAddress.e().name());
      localHashtable.put("zipCode", localPayeeAddress.d().replace("-", ""));
      localHashtable.put("phone", s.s(paramPayee.p().replace("-", "")));
      localHashtable.put("memo", s.s(paramPayee.g()));
      localHashtable.put("validateOnly", Boolean.toString(true));
      localHashtable.put("addressLine3", "");
      JSONObject localJSONObject1 = m.a(ChaseApplication.a(), str, localHashtable);
      localBillPayPayeeEditResponse.b(localJSONObject1);
      if (localBillPayPayeeEditResponse.e())
        return localBillPayPayeeEditResponse;
      localBillPayPayeeEditResponse.a(localJSONObject1.getString("formId"));
      JSONObject localJSONObject2 = localJSONObject1.getJSONObject("payee");
      localBillPayPayeeEditResponse.b(localJSONObject2.optString("payeeId"));
      localBillPayPayeeEditResponse.e(localJSONObject2.optString("status"));
      localBillPayPayeeEditResponse.a(localJSONObject2.getInt("leadTime"));
      localBillPayPayeeEditResponse.f(localJSONObject2.optString("accountNumberMask"));
      return localBillPayPayeeEditResponse;
    }
    catch (Exception localException)
    {
      localBillPayPayeeEditResponse.a(ChaseApplication.a().getApplicationContext());
    }
    return localBillPayPayeeEditResponse;
  }

  public static BillPayPayeeSearchResponse a(String paramString1, String paramString2, String paramString3)
  {
    ArrayList localArrayList = new ArrayList();
    BillPayPayeeSearchResponse localBillPayPayeeSearchResponse = new BillPayPayeeSearchResponse();
    try
    {
      String str = a(2131165258);
      Hashtable localHashtable = c();
      localHashtable.put("name", paramString1);
      localHashtable.put("postalCode", paramString2);
      if (s.m(paramString3))
        localHashtable.put("accountNumber", paramString3);
      JSONObject localJSONObject = m.a(ChaseApplication.a(), str, localHashtable);
      localBillPayPayeeSearchResponse.b(localJSONObject);
      localBillPayPayeeSearchResponse.a(localJSONObject.optString("flowIndicator"));
      if (localBillPayPayeeSearchResponse.e())
        return localBillPayPayeeSearchResponse;
      JSONArray localJSONArray = localJSONObject.getJSONArray("payees");
      for (int i = 0; i < localJSONArray.length(); i++)
        localArrayList.add(a(localJSONArray.getJSONObject(i)));
      localBillPayPayeeSearchResponse.a(localArrayList);
      return localBillPayPayeeSearchResponse;
    }
    catch (Exception localException)
    {
      localBillPayPayeeSearchResponse.a(ChaseApplication.a().getApplicationContext());
    }
    return localBillPayPayeeSearchResponse;
  }

  public static BillPayPayeeSearchResponse a(byte[] paramArrayOfByte)
  {
    ArrayList localArrayList = new ArrayList();
    BillPayPayeeSearchResponse localBillPayPayeeSearchResponse = new BillPayPayeeSearchResponse();
    try
    {
      String str1 = a(2131165260);
      Hashtable localHashtable = c();
      String[] arrayOfString1 = { "frontImageData" };
      String[] arrayOfString2 = { "image/jpeg" };
      byte[][] arrayOfByte = { paramArrayOfByte };
      localHashtable.put("frontImageType", "JPEG");
      JSONObject localJSONObject1 = m.a(ChaseApplication.a(), str1, localHashtable, arrayOfString1, arrayOfByte, arrayOfString2, ChaseApplication.a().h());
      localBillPayPayeeSearchResponse.b(localJSONObject1);
      localBillPayPayeeSearchResponse.a(localJSONObject1.optString("flowIndicator"));
      JSONObject localJSONObject2 = localJSONObject1.optJSONObject("detail");
      ImageCaptureData localImageCaptureData;
      JSONObject localJSONObject3;
      if (localJSONObject2 != null)
      {
        localImageCaptureData = new ImageCaptureData();
        localImageCaptureData.c(localJSONObject2.optString("balDueAmount"));
        localImageCaptureData.d(localJSONObject2.optString("amountDue"));
        localImageCaptureData.a(localJSONObject2.optString("name"));
        localImageCaptureData.e(localJSONObject2.optString("minDueAmount"));
        localImageCaptureData.f(localJSONObject2.optString("invoiceNum"));
        localImageCaptureData.b(localJSONObject2.optString("accountNumber"));
        localImageCaptureData.g(localJSONObject2.optString("dueDate"));
        localJSONObject3 = localJSONObject2.optJSONObject("address");
        localImageCaptureData.h(localJSONObject3.optString("addressLine1"));
        if (!s.l(localJSONObject3.optString("addressLine2")))
          break label329;
      }
      label329: for (String str2 = ""; ; str2 = localJSONObject3.optString("addressLine2"))
      {
        localImageCaptureData.i(str2);
        localImageCaptureData.j(localJSONObject3.optString("city"));
        localImageCaptureData.a(State.a(localJSONObject3.optString("state")));
        localImageCaptureData.k(localJSONObject3.optString("postalCode"));
        localBillPayPayeeSearchResponse.a(localImageCaptureData);
        if (!localBillPayPayeeSearchResponse.e())
          break;
        return localBillPayPayeeSearchResponse;
      }
      JSONArray localJSONArray = localJSONObject1.getJSONArray("payees");
      for (int i = 0; i < localJSONArray.length(); i++)
        localArrayList.add(a(localJSONArray.getJSONObject(i)));
      localBillPayPayeeSearchResponse.a(localArrayList);
      return localBillPayPayeeSearchResponse;
    }
    catch (Exception localException)
    {
      while (true)
        localBillPayPayeeSearchResponse.a(ChaseApplication.a().getApplicationContext());
    }
  }

  public static DeletePayeeResponse b(String paramString1, String paramString2)
  {
    DeletePayeeResponse localDeletePayeeResponse = new DeletePayeeResponse();
    try
    {
      String str = a(2131165261);
      Hashtable localHashtable = c();
      localHashtable.put("payeeId", paramString1);
      localHashtable.put("token", paramString2);
      localDeletePayeeResponse.b(m.a(ChaseApplication.a(), str, localHashtable));
      return localDeletePayeeResponse;
    }
    catch (Exception localException)
    {
      localDeletePayeeResponse.a(ChaseApplication.a().getApplicationContext());
    }
    return localDeletePayeeResponse;
  }

  private static void b(Hashtable<String, String> paramHashtable, String paramString1, String paramString2)
  {
    if (s.m(paramString2))
      paramHashtable.put(paramString1, paramString2);
  }

  public static BillPayPayeeEditResponse c(String paramString1, String paramString2)
  {
    BillPayPayeeEditResponse localBillPayPayeeEditResponse = new BillPayPayeeEditResponse();
    try
    {
      String str = a(2131165263);
      Hashtable localHashtable = c();
      localHashtable.put("validateOnly", Boolean.toString(false));
      localHashtable.put("formId", paramString1);
      localHashtable.put("token", paramString2);
      JSONObject localJSONObject1 = m.a(ChaseApplication.a(), str, localHashtable);
      localBillPayPayeeEditResponse.b(localJSONObject1);
      if (localBillPayPayeeEditResponse.e())
        return localBillPayPayeeEditResponse;
      localBillPayPayeeEditResponse.a(localJSONObject1.optString("formId"));
      JSONObject localJSONObject2 = localJSONObject1.getJSONObject("payee");
      localBillPayPayeeEditResponse.b(localJSONObject2.optString("payeeId"));
      localBillPayPayeeEditResponse.e(localJSONObject2.getString("status"));
      localBillPayPayeeEditResponse.a(localJSONObject2.optInt("leadTime"));
      return localBillPayPayeeEditResponse;
    }
    catch (Exception localException)
    {
      localBillPayPayeeEditResponse.a(ChaseApplication.a().getApplicationContext());
    }
    return localBillPayPayeeEditResponse;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.billpay.b
 * JD-Core Version:    0.6.2
 */