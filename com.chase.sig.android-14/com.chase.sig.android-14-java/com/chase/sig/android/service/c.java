package com.chase.sig.android.service;

import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.domain.AccountRewardsDetail;
import com.chase.sig.android.domain.RewardDetailValue;
import com.chase.sig.android.util.m;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONObject;

public final class c extends JPService
{
  public static AccountRewardsDetailResponse a(String paramString)
  {
    AccountRewardsDetailResponse localAccountRewardsDetailResponse = new AccountRewardsDetailResponse();
    String str1 = a(2131165200);
    Hashtable localHashtable1 = c();
    ArrayList localArrayList1 = new ArrayList();
    localArrayList1.add(paramString);
    localHashtable1.put("accountIds", new JSONArray(localArrayList1).toString());
    while (true)
    {
      ArrayList localArrayList2;
      int j;
      AccountRewardsDetail localAccountRewardsDetail;
      Hashtable localHashtable2;
      JSONObject localJSONObject2;
      ArrayList localArrayList3;
      String str2;
      String str3;
      try
      {
        JSONObject localJSONObject1 = m.a(ChaseApplication.a(), str1, localHashtable1);
        localAccountRewardsDetailResponse.b(localJSONObject1);
        if (localAccountRewardsDetailResponse.e())
          return localAccountRewardsDetailResponse;
        localAccountRewardsDetailResponse.a(localJSONObject1.optString("footnote"));
        JSONArray localJSONArray = localJSONObject1.getJSONArray("rewards");
        int i = localJSONArray.length();
        localArrayList2 = new ArrayList();
        j = 0;
        if (j >= i)
          break label331;
        localAccountRewardsDetail = new AccountRewardsDetail();
        localHashtable2 = new Hashtable();
        localJSONObject2 = localJSONArray.getJSONObject(j);
        localArrayList3 = new ArrayList();
        if (localJSONObject2 == null)
          break label347;
        Iterator localIterator = (Iterator)localJSONObject2.keys();
        if (!localIterator.hasNext())
          break label306;
        str2 = (String)localIterator.next();
        if (str2.equals("displayFormat"))
        {
          str3 = Integer.valueOf(localJSONObject2.getInt(str2)).toString();
          localHashtable2.put(str2, str3);
          localArrayList3.add(new RewardDetailValue(str2, str3));
          continue;
        }
      }
      catch (Exception localException)
      {
        localAccountRewardsDetailResponse.a(ChaseApplication.a().getApplicationContext());
        return localAccountRewardsDetailResponse;
      }
      if (str2.equals("failed"))
      {
        if (localJSONObject2.getBoolean(str2))
          str3 = "true";
      }
      else
      {
        str3 = localJSONObject2.getString(str2);
        continue;
        label306: localAccountRewardsDetail.a(localHashtable2);
        localAccountRewardsDetail.a(localArrayList3);
        localArrayList2.add(localAccountRewardsDetail);
        break label347;
        label331: localAccountRewardsDetailResponse.a(localArrayList2);
        continue;
        str3 = "false";
        continue;
        label347: j++;
      }
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.c
 * JD-Core Version:    0.6.2
 */