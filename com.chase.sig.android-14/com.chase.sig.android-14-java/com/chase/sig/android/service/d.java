package com.chase.sig.android.service;

import android.location.Location;
import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.domain.BranchLocation;
import com.chase.sig.android.util.ChaseException;
import com.chase.sig.android.util.m;
import com.chase.sig.android.util.s;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class d extends JPService
{
  public static String a = "25";

  private static Location a(String paramString)
  {
    ChaseApplication.a();
    n localn = af.a();
    if (localn.p == null)
      localn.p = new GeocoderService();
    GeocoderService localGeocoderService = localn.p;
    try
    {
      GeocoderService.GeoLocationResponse localGeoLocationResponse2 = localGeocoderService.a(paramString);
      localGeoLocationResponse1 = localGeoLocationResponse2;
      Location localLocation = localGeoLocationResponse1.a();
      if ((localLocation instanceof Location))
        return (Location)localLocation;
    }
    catch (Exception localException)
    {
      while (true)
        GeocoderService.GeoLocationResponse localGeoLocationResponse1 = null;
    }
    return null;
  }

  public static BranchLocateResponse a(Location paramLocation, String paramString)
  {
    BranchLocateResponse localBranchLocateResponse = new BranchLocateResponse();
    if (!s.n(paramString))
      paramLocation = a(paramString);
    if (paramLocation == null)
      return localBranchLocateResponse;
    try
    {
      String str = a(2131165275);
      Hashtable localHashtable = new Hashtable();
      if (paramLocation != null)
      {
        localHashtable.put("lat", new Float(paramLocation.getLatitude()).toString());
        localHashtable.put("lng", new Float(paramLocation.getLongitude()).toString());
      }
      localHashtable.put("maxCount", a);
      if (s.m(paramString))
        localHashtable.put("address", paramString);
      JSONObject localJSONObject1 = m.a(ChaseApplication.a(), str, localHashtable);
      localBranchLocateResponse.b(localJSONObject1);
      ArrayList localArrayList = new ArrayList();
      JSONArray localJSONArray = localJSONObject1.optJSONArray("locations");
      if (localJSONArray != null)
        for (int i = 0; i < localJSONArray.length(); i++)
        {
          JSONObject localJSONObject2 = localJSONArray.getJSONObject(i);
          BranchLocation localBranchLocation = new BranchLocation();
          localBranchLocation.k(localJSONObject2.optString("access"));
          localBranchLocation.a(localJSONObject2.optString("address"));
          localBranchLocation.b(localJSONObject2.optString("atms"));
          localBranchLocation.c(localJSONObject2.optString("bank"));
          localBranchLocation.d(localJSONObject2.optString("city"));
          localBranchLocation.a(localJSONObject2.optDouble("distance"));
          localBranchLocation.e(localJSONObject2.optString("label"));
          localBranchLocation.b(localJSONObject2.optDouble("lat"));
          localBranchLocation.c(localJSONObject2.optDouble("lng"));
          localBranchLocation.f(localJSONObject2.optString("locType"));
          localBranchLocation.g(localJSONObject2.optString("name"));
          localBranchLocation.h(localJSONObject2.optString("phone"));
          localBranchLocation.i(localJSONObject2.optString("state"));
          localBranchLocation.l(localJSONObject2.optString("type"));
          localBranchLocation.j(localJSONObject2.optString("zip"));
          localBranchLocation.b(localJSONObject2.optString("atms"));
          localBranchLocation.a(a(localJSONObject2.optJSONArray("driveUpHrs")));
          localBranchLocation.b(a(localJSONObject2.optJSONArray("lobbyHrs")));
          localBranchLocation.c(a(localJSONObject2.optJSONArray("services")));
          localBranchLocation.d(a(localJSONObject2.optJSONArray("languages")));
          localArrayList.add(localBranchLocation);
        }
      localBranchLocateResponse.a(localArrayList);
      return localBranchLocateResponse;
    }
    catch (JSONException localJSONException)
    {
      localBranchLocateResponse.a(ChaseApplication.a());
      return localBranchLocateResponse;
    }
    catch (ChaseException localChaseException)
    {
      localBranchLocateResponse.a(ChaseApplication.a());
    }
    return localBranchLocateResponse;
  }

  private static List<String> a(JSONArray paramJSONArray)
  {
    ArrayList localArrayList = new ArrayList();
    if (paramJSONArray == null)
      return localArrayList;
    for (int i = 0; i < paramJSONArray.length(); i++)
      localArrayList.add(paramJSONArray.optString(i, ""));
    return localArrayList;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.d
 * JD-Core Version:    0.6.2
 */