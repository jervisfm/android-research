package com.chase.sig.android.service.disclosures;

import com.chase.sig.android.domain.Disclosure;
import com.chase.sig.android.service.l;
import java.io.Serializable;
import java.util.List;

public class DisclosuresResponse extends l
  implements Serializable
{
  private List<Disclosure> disclosures;

  public final List<Disclosure> a()
  {
    return this.disclosures;
  }

  public final void a(List<Disclosure> paramList)
  {
    this.disclosures = paramList;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.disclosures.DisclosuresResponse
 * JD-Core Version:    0.6.2
 */