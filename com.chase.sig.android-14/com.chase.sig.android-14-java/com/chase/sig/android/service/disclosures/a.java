package com.chase.sig.android.service.disclosures;

import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.domain.Disclosure;
import com.chase.sig.android.service.JPService;
import com.chase.sig.android.util.m;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public final class a extends JPService
{
  public static DisclosuresResponse a()
  {
    DisclosuresResponse localDisclosuresResponse = new DisclosuresResponse();
    ArrayList localArrayList = new ArrayList();
    String str = a(2131165268);
    try
    {
      JSONObject localJSONObject1 = m.a(ChaseApplication.a(), str, c());
      localDisclosuresResponse.b(localJSONObject1);
      JSONArray localJSONArray = localJSONObject1.optJSONArray("disclosures");
      int i = localJSONArray.length();
      for (int j = 0; j < i; j++)
      {
        JSONObject localJSONObject2 = localJSONArray.getJSONObject(j);
        Disclosure localDisclosure = new Disclosure();
        localDisclosure.a(localJSONObject2.optString("id"));
        localDisclosure.c(localJSONObject2.optString("title"));
        localDisclosure.b(localJSONObject2.optString("text"));
        localArrayList.add(localDisclosure);
      }
      localDisclosuresResponse.a(localArrayList);
      return localDisclosuresResponse;
    }
    catch (Exception localException)
    {
      localDisclosuresResponse.a(ChaseApplication.a().getApplicationContext());
    }
    return localDisclosuresResponse;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.disclosures.a
 * JD-Core Version:    0.6.2
 */