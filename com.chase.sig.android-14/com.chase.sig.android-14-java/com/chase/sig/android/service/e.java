package com.chase.sig.android.service;

import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.domain.CreditFacilityAccountDetail;
import com.chase.sig.android.domain.CreditFacilityAccountLoanDetail;
import com.chase.sig.android.util.Dollar;
import com.chase.sig.android.util.m;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public final class e extends JPService
{
  public static CreditFacilityAccountDetailResponse a(String paramString)
  {
    CreditFacilityAccountDetailResponse localCreditFacilityAccountDetailResponse = new CreditFacilityAccountDetailResponse();
    CreditFacilityAccountDetail localCreditFacilityAccountDetail = new CreditFacilityAccountDetail();
    String str = a(2131165199);
    Hashtable localHashtable = c();
    localHashtable.put("accountId", paramString);
    try
    {
      JSONObject localJSONObject1 = m.a(ChaseApplication.a(), str, localHashtable);
      localCreditFacilityAccountDetailResponse.b(localJSONObject1);
      if (localCreditFacilityAccountDetailResponse.e())
        return localCreditFacilityAccountDetailResponse;
      JSONObject localJSONObject2 = localJSONObject1.getJSONObject("creditFacilitySummaryRes");
      localCreditFacilityAccountDetail.a(new Dollar(localJSONObject2.getString("creditLimit")));
      localCreditFacilityAccountDetail.b(new Dollar(localJSONObject2.getString("available")));
      localCreditFacilityAccountDetail.a(localJSONObject2.getString("displayType"));
      localCreditFacilityAccountDetail.b(localJSONObject2.getString("mask"));
      localCreditFacilityAccountDetail.c(localJSONObject2.getString("name"));
      ArrayList localArrayList = new ArrayList();
      JSONArray localJSONArray = localJSONObject2.getJSONArray("details");
      for (int i = 0; i < localJSONArray.length(); i++)
      {
        JSONObject localJSONObject3 = localJSONArray.getJSONObject(i);
        CreditFacilityAccountLoanDetail localCreditFacilityAccountLoanDetail = new CreditFacilityAccountLoanDetail();
        localCreditFacilityAccountLoanDetail.a(localJSONObject3.getString("loanNumMask"));
        localCreditFacilityAccountLoanDetail.b(localJSONObject3.getString("loanType"));
        localCreditFacilityAccountLoanDetail.a(Double.valueOf(localJSONObject3.optDouble("intRate")));
        localCreditFacilityAccountLoanDetail.c(localJSONObject3.getString("maturityDate"));
        localCreditFacilityAccountLoanDetail.a(a(localJSONObject3, "outstanding"));
        localCreditFacilityAccountLoanDetail.b(a(localJSONObject3, "accruedInt"));
        localCreditFacilityAccountLoanDetail.b(localJSONObject3.getString("loanType"));
        localArrayList.add(localCreditFacilityAccountLoanDetail);
      }
      localCreditFacilityAccountDetail.a(localArrayList);
      localCreditFacilityAccountDetailResponse.a(localCreditFacilityAccountDetail);
      return localCreditFacilityAccountDetailResponse;
    }
    catch (Exception localException)
    {
      localCreditFacilityAccountDetailResponse.a(ChaseApplication.a().getApplicationContext());
    }
    return localCreditFacilityAccountDetailResponse;
  }

  private static Hashtable<String, String> a(JSONObject paramJSONObject, String paramString)
  {
    Hashtable localHashtable = new Hashtable();
    JSONObject localJSONObject = paramJSONObject.optJSONObject(paramString);
    if (localJSONObject != null)
    {
      Iterator localIterator = (Iterator)localJSONObject.keys();
      while (localIterator.hasNext())
      {
        String str = (String)localIterator.next();
        localHashtable.put(str, localJSONObject.getString(str));
      }
    }
    return localHashtable;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.e
 * JD-Core Version:    0.6.2
 */