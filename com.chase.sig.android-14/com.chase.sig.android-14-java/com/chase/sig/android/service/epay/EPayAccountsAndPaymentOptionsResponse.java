package com.chase.sig.android.service.epay;

import com.chase.sig.android.domain.EPayAccount;
import com.chase.sig.android.domain.EPaymentOption;
import com.chase.sig.android.service.l;
import java.io.Serializable;
import java.util.List;

public class EPayAccountsAndPaymentOptionsResponse extends l
  implements Serializable
{
  private List<EPayAccount> accounts;
  private String date;
  private List<EPaymentOption> paymentOptions;

  public final String a()
  {
    return this.date;
  }

  public final void a(String paramString)
  {
    this.date = paramString;
  }

  public final void a(List<EPayAccount> paramList)
  {
    this.accounts = paramList;
  }

  public final List<EPayAccount> b()
  {
    return this.accounts;
  }

  public final List<EPaymentOption> c()
  {
    return this.paymentOptions;
  }

  public final void c(List<EPaymentOption> paramList)
  {
    this.paymentOptions = paramList;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.epay.EPayAccountsAndPaymentOptionsResponse
 * JD-Core Version:    0.6.2
 */