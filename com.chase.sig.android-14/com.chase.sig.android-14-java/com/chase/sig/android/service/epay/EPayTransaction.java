package com.chase.sig.android.service.epay;

import com.chase.sig.android.domain.Transaction;

public class EPayTransaction extends Transaction
{
  private String description;

  public final String B()
  {
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = this.fromAccountMask;
    return String.format("From %s", arrayOfObject);
  }

  public final String a()
  {
    return this.description;
  }

  public final void a(String paramString)
  {
    this.description = paramString;
  }

  public final void c(String paramString)
  {
    this.deliverByDate = paramString;
  }

  public final String d()
  {
    return this.toAccountMask;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.epay.EPayTransaction
 * JD-Core Version:    0.6.2
 */