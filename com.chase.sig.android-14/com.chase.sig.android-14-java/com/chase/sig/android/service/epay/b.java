package com.chase.sig.android.service.epay;

import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.domain.EPayment;
import com.chase.sig.android.domain.a;
import com.chase.sig.android.domain.o;
import com.chase.sig.android.service.GenericResponse;
import com.chase.sig.android.service.JPService;
import com.chase.sig.android.util.ChaseException;
import com.chase.sig.android.util.Dollar;
import com.chase.sig.android.util.m;
import com.chase.sig.android.util.s;
import java.math.BigDecimal;
import java.util.Hashtable;
import org.json.JSONObject;

public final class b extends JPService
{
  public static GenericResponse a(EPayment paramEPayment)
  {
    String str = a(2131165235);
    Hashtable localHashtable = c();
    localHashtable.put("userId", ChaseApplication.a().b().a.c);
    localHashtable.put("accountId", paramEPayment.f());
    localHashtable.put("amount", paramEPayment.d().b().toPlainString());
    localHashtable.put("date", s.b(s.t(paramEPayment.e())));
    localHashtable.put("epayAccountId", paramEPayment.c());
    localHashtable.put("optionId", paramEPayment.g());
    GenericResponse localGenericResponse = new GenericResponse();
    try
    {
      JSONObject localJSONObject1 = m.a(ChaseApplication.a(), str, localHashtable);
      localGenericResponse.b(localJSONObject1);
      paramEPayment.b(localJSONObject1.optString("formId"));
      JSONObject localJSONObject2 = localJSONObject1.optJSONObject("agreement");
      if (localJSONObject2 != null)
        paramEPayment.g(localJSONObject2.optString("content").replace("\r", ""));
      return localGenericResponse;
    }
    catch (Exception localException)
    {
      localGenericResponse.a(ChaseApplication.a().getApplicationContext());
    }
    return localGenericResponse;
  }

  // ERROR //
  public static EPayAccountsAndPaymentOptionsResponse a(String paramString)
  {
    // Byte code:
    //   0: iconst_0
    //   1: istore_1
    //   2: ldc 149
    //   4: invokestatic 16	com/chase/sig/android/service/epay/b:a	(I)Ljava/lang/String;
    //   7: astore_2
    //   8: new 151	java/util/ArrayList
    //   11: dup
    //   12: invokespecial 152	java/util/ArrayList:<init>	()V
    //   15: astore_3
    //   16: new 151	java/util/ArrayList
    //   19: dup
    //   20: invokespecial 152	java/util/ArrayList:<init>	()V
    //   23: astore 4
    //   25: invokestatic 20	com/chase/sig/android/service/epay/b:c	()Ljava/util/Hashtable;
    //   28: astore 5
    //   30: aload 5
    //   32: ldc 49
    //   34: aload_0
    //   35: invokevirtual 47	java/util/Hashtable:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   38: pop
    //   39: new 154	com/chase/sig/android/service/epay/EPayAccountsAndPaymentOptionsResponse
    //   42: dup
    //   43: invokespecial 155	com/chase/sig/android/service/epay/EPayAccountsAndPaymentOptionsResponse:<init>	()V
    //   46: astore 7
    //   48: invokestatic 27	com/chase/sig/android/ChaseApplication:a	()Lcom/chase/sig/android/ChaseApplication;
    //   51: aload_2
    //   52: aload 5
    //   54: invokestatic 102	com/chase/sig/android/util/m:a	(Lcom/chase/sig/android/ChaseApplication;Ljava/lang/String;Ljava/util/Hashtable;)Lorg/json/JSONObject;
    //   57: astore 10
    //   59: aload 7
    //   61: aload 10
    //   63: invokevirtual 156	com/chase/sig/android/service/epay/EPayAccountsAndPaymentOptionsResponse:b	(Lorg/json/JSONObject;)V
    //   66: aload 7
    //   68: invokevirtual 159	com/chase/sig/android/service/epay/EPayAccountsAndPaymentOptionsResponse:e	()Z
    //   71: istore 13
    //   73: iload 13
    //   75: ifeq +23 -> 98
    //   78: aload 7
    //   80: areturn
    //   81: astore 8
    //   83: aload 7
    //   85: invokestatic 27	com/chase/sig/android/ChaseApplication:a	()Lcom/chase/sig/android/ChaseApplication;
    //   88: invokevirtual 140	com/chase/sig/android/ChaseApplication:getApplicationContext	()Landroid/content/Context;
    //   91: invokevirtual 160	com/chase/sig/android/service/epay/EPayAccountsAndPaymentOptionsResponse:a	(Landroid/content/Context;)Lcom/chase/sig/android/service/l;
    //   94: pop
    //   95: aload 7
    //   97: areturn
    //   98: aload 7
    //   100: aload 10
    //   102: ldc 73
    //   104: invokevirtual 163	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   107: invokevirtual 165	com/chase/sig/android/service/epay/EPayAccountsAndPaymentOptionsResponse:a	(Ljava/lang/String;)V
    //   110: aload 10
    //   112: ldc 167
    //   114: invokevirtual 171	org/json/JSONObject:getJSONArray	(Ljava/lang/String;)Lorg/json/JSONArray;
    //   117: astore 14
    //   119: aload 14
    //   121: invokevirtual 177	org/json/JSONArray:length	()I
    //   124: istore 15
    //   126: iconst_0
    //   127: istore 16
    //   129: iload 16
    //   131: iload 15
    //   133: if_icmpge +58 -> 191
    //   136: aload 14
    //   138: iload 16
    //   140: invokevirtual 181	org/json/JSONArray:getJSONObject	(I)Lorg/json/JSONObject;
    //   143: astore 17
    //   145: new 183	com/chase/sig/android/domain/EPayAccount
    //   148: dup
    //   149: invokespecial 184	com/chase/sig/android/domain/EPayAccount:<init>	()V
    //   152: astore 18
    //   154: aload 18
    //   156: aload 17
    //   158: ldc 186
    //   160: invokevirtual 163	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   163: invokevirtual 187	com/chase/sig/android/domain/EPayAccount:b	(Ljava/lang/String;)V
    //   166: aload 18
    //   168: aload 17
    //   170: ldc 189
    //   172: invokevirtual 163	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   175: invokevirtual 190	com/chase/sig/android/domain/EPayAccount:a	(Ljava/lang/String;)V
    //   178: aload_3
    //   179: aload 18
    //   181: invokevirtual 194	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   184: pop
    //   185: iinc 16 1
    //   188: goto -59 -> 129
    //   191: aload 10
    //   193: ldc 196
    //   195: invokevirtual 171	org/json/JSONObject:getJSONArray	(Ljava/lang/String;)Lorg/json/JSONArray;
    //   198: astore 20
    //   200: aload 20
    //   202: invokevirtual 177	org/json/JSONArray:length	()I
    //   205: istore 21
    //   207: iload_1
    //   208: iload 21
    //   210: if_icmpge +154 -> 364
    //   213: aload 20
    //   215: iload_1
    //   216: invokevirtual 181	org/json/JSONArray:getJSONObject	(I)Lorg/json/JSONObject;
    //   219: astore 22
    //   221: new 198	com/chase/sig/android/domain/EPaymentOption
    //   224: dup
    //   225: invokespecial 199	com/chase/sig/android/domain/EPaymentOption:<init>	()V
    //   228: astore 23
    //   230: aload 23
    //   232: aload 22
    //   234: ldc 201
    //   236: invokevirtual 205	org/json/JSONObject:getBoolean	(Ljava/lang/String;)Z
    //   239: invokevirtual 208	com/chase/sig/android/domain/EPaymentOption:a	(Z)V
    //   242: aload 23
    //   244: aload 22
    //   246: ldc 210
    //   248: invokevirtual 163	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   251: invokevirtual 212	com/chase/sig/android/domain/EPaymentOption:c	(Ljava/lang/String;)V
    //   254: aload 23
    //   256: aload 22
    //   258: ldc 214
    //   260: invokevirtual 163	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   263: invokevirtual 215	com/chase/sig/android/domain/EPaymentOption:a	(Ljava/lang/String;)V
    //   266: aload 23
    //   268: aload 22
    //   270: ldc 217
    //   272: invokevirtual 163	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   275: invokevirtual 218	com/chase/sig/android/domain/EPaymentOption:b	(Ljava/lang/String;)V
    //   278: aload 23
    //   280: aload 22
    //   282: ldc 220
    //   284: invokestatic 223	com/chase/sig/android/util/s:a	(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;
    //   287: invokevirtual 225	com/chase/sig/android/domain/EPaymentOption:d	(Ljava/lang/String;)V
    //   290: aload 23
    //   292: aload 22
    //   294: ldc 186
    //   296: invokevirtual 163	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   299: invokevirtual 227	com/chase/sig/android/domain/EPaymentOption:e	(Ljava/lang/String;)V
    //   302: aload 22
    //   304: ldc 57
    //   306: invokevirtual 163	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   309: astore 24
    //   311: aload 24
    //   313: invokestatic 230	com/chase/sig/android/util/s:m	(Ljava/lang/String;)Z
    //   316: ifeq +17 -> 333
    //   319: aload 23
    //   321: new 63	com/chase/sig/android/util/Dollar
    //   324: dup
    //   325: aload 24
    //   327: invokespecial 232	com/chase/sig/android/util/Dollar:<init>	(Ljava/lang/String;)V
    //   330: invokevirtual 235	com/chase/sig/android/domain/EPaymentOption:a	(Lcom/chase/sig/android/util/Dollar;)V
    //   333: aload 4
    //   335: aload 23
    //   337: invokevirtual 194	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   340: pop
    //   341: iinc 1 1
    //   344: goto -137 -> 207
    //   347: astore 11
    //   349: aload 7
    //   351: invokestatic 27	com/chase/sig/android/ChaseApplication:a	()Lcom/chase/sig/android/ChaseApplication;
    //   354: invokevirtual 140	com/chase/sig/android/ChaseApplication:getApplicationContext	()Landroid/content/Context;
    //   357: invokevirtual 160	com/chase/sig/android/service/epay/EPayAccountsAndPaymentOptionsResponse:a	(Landroid/content/Context;)Lcom/chase/sig/android/service/l;
    //   360: pop
    //   361: aload 7
    //   363: areturn
    //   364: aload 7
    //   366: aload_3
    //   367: invokevirtual 238	com/chase/sig/android/service/epay/EPayAccountsAndPaymentOptionsResponse:a	(Ljava/util/List;)V
    //   370: aload 7
    //   372: aload 4
    //   374: invokevirtual 240	com/chase/sig/android/service/epay/EPayAccountsAndPaymentOptionsResponse:c	(Ljava/util/List;)V
    //   377: aload 7
    //   379: areturn
    //
    // Exception table:
    //   from	to	target	type
    //   48	59	81	com/chase/sig/android/util/ChaseException
    //   59	73	347	org/json/JSONException
    //   98	126	347	org/json/JSONException
    //   136	185	347	org/json/JSONException
    //   191	207	347	org/json/JSONException
    //   213	333	347	org/json/JSONException
    //   333	341	347	org/json/JSONException
  }

  public static GenericResponse b(EPayment paramEPayment)
  {
    String str = a(2131165232);
    Hashtable localHashtable = c();
    localHashtable.put("formId", paramEPayment.b());
    GenericResponse localGenericResponse = new GenericResponse();
    try
    {
      JSONObject localJSONObject = m.a(ChaseApplication.a(), str, localHashtable);
      localGenericResponse.b(localJSONObject);
      paramEPayment.a(localJSONObject.optString("confNum"));
      return localGenericResponse;
    }
    catch (ChaseException localChaseException)
    {
      localGenericResponse.a(ChaseApplication.a().getApplicationContext());
    }
    return localGenericResponse;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.epay.b
 * JD-Core Version:    0.6.2
 */