package com.chase.sig.android.service;

import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import com.chase.sig.android.ChaseApplication;
import java.util.List;

public final class f
  implements LocationListener
{
  public final Object a = new Object();
  private LocationManager b = (LocationManager)ChaseApplication.a().getSystemService("location");
  private Location c;

  public final Location a()
  {
    List localList = this.b.getProviders(true);
    String str1 = this.b.getBestProvider(new Criteria(), true);
    if ((localList.size() == 0) || ((localList.size() == 1) && (((String)localList.get(0)).equals("passive"))))
      return null;
    if (str1 != null)
      this.c = this.b.getLastKnownLocation(str1);
    if (localList.contains("gps"))
      this.b.requestLocationUpdates("gps", 0L, 0.0F, this, Looper.getMainLooper());
    if (localList.contains("network"))
      this.b.requestLocationUpdates("network", 0L, 0.0F, this, Looper.getMainLooper());
    if ((str1 != null) && (str1 != "gps") && (str1 != "network"))
      this.b.requestLocationUpdates(str1, 0L, 0.0F, this, Looper.getMainLooper());
    try
    {
      Object localObject1 = this.a;
      i = 0;
      j = i + 1;
      if (i < 20);
      try
      {
        this.a.wait(1000L);
        Object[] arrayOfObject1 = new Object[2];
        arrayOfObject1[0] = Integer.valueOf(1);
        if (this.c == null);
        for (String str2 = "YES"; ; str2 = "NO")
        {
          arrayOfObject1[1] = str2;
          if ((this.c == null) || (!this.c.hasAccuracy()))
            break;
          float f = this.c.getAccuracy();
          Object[] arrayOfObject2 = new Object[4];
          arrayOfObject2[0] = Float.valueOf(f);
          arrayOfObject2[1] = Double.valueOf(this.c.getLatitude());
          arrayOfObject2[2] = Double.valueOf(this.c.getLongitude());
          arrayOfObject2[3] = Double.valueOf(this.c.getAltitude());
          if (f >= 402.0F)
            break;
          this.b.removeUpdates(this);
          return this.c;
        }
      }
      finally
      {
      }
    }
    catch (InterruptedException localInterruptedException)
    {
      while (true)
      {
        int j;
        continue;
        int i = j;
      }
    }
  }

  public final void onLocationChanged(Location paramLocation)
  {
    synchronized (this.a)
    {
      this.c = paramLocation;
      this.a.notify();
      return;
    }
  }

  public final void onProviderDisabled(String paramString)
  {
  }

  public final void onProviderEnabled(String paramString)
  {
  }

  public final void onStatusChanged(String paramString, int paramInt, Bundle paramBundle)
  {
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.f
 * JD-Core Version:    0.6.2
 */