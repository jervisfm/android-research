package com.chase.sig.android.service;

import android.os.Bundle;
import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.util.m;
import java.util.Hashtable;
import org.json.JSONObject;

public final class i extends JPService
{
  public static GenericResponse a()
  {
    GenericResponse localGenericResponse = new GenericResponse();
    Hashtable localHashtable = c();
    try
    {
      ChaseApplication localChaseApplication = ChaseApplication.a();
      String str = a(2131165279);
      localHashtable.put("deviceId", localChaseApplication.d());
      localHashtable.put("deviceType", "Android");
      localHashtable.put("deviceAppVersion", localChaseApplication.k());
      localHashtable.put("deviceAppSource", localChaseApplication.getString(2131165189));
      JSONObject localJSONObject = m.a(localChaseApplication, str, localHashtable);
      Bundle localBundle = new Bundle();
      localBundle.putBoolean("extra_upgrade_required", localJSONObject.optBoolean("updateIsMandatory", false));
      localBundle.putString("extra_upgrade_recommended", localJSONObject.optString("updateMessage", ""));
      localBundle.putString("extra_upgrade_url", localJSONObject.optString("appStoreUpdateUrl", ""));
      localBundle.putBoolean("extra_os_not_supported", localJSONObject.optBoolean("operatingSystemNotSupported", false));
      localGenericResponse.a(localBundle);
      return localGenericResponse;
    }
    catch (Exception localException)
    {
      localGenericResponse.a(ChaseApplication.a().getApplicationContext());
    }
    return localGenericResponse;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.i
 * JD-Core Version:    0.6.2
 */