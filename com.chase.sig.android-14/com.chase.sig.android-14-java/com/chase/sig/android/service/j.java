package com.chase.sig.android.service;

import java.io.Closeable;
import org.apache.http.HttpEntity;

public final class j extends JPService
{
  // ERROR //
  public static com.chase.sig.android.domain.ImageDownloadResponse a(java.lang.String paramString, boolean paramBoolean, java.util.HashMap<java.lang.String, java.lang.String> paramHashMap)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_3
    //   2: iconst_1
    //   3: istore 4
    //   5: new 16	com/chase/sig/android/domain/ImageDownloadResponse
    //   8: dup
    //   9: invokespecial 17	com/chase/sig/android/domain/ImageDownloadResponse:<init>	()V
    //   12: astore 5
    //   14: new 19	com/chase/sig/android/util/f
    //   17: dup
    //   18: invokestatic 24	com/chase/sig/android/ChaseApplication:a	()Lcom/chase/sig/android/ChaseApplication;
    //   21: aload_2
    //   22: invokespecial 27	com/chase/sig/android/util/f:<init>	(Lcom/chase/sig/android/ChaseApplication;Ljava/util/HashMap;)V
    //   25: aload_0
    //   26: invokevirtual 30	com/chase/sig/android/util/f:a	(Ljava/lang/String;)Lorg/apache/http/HttpResponse;
    //   29: astore 8
    //   31: iconst_1
    //   32: anewarray 32	java/lang/Object
    //   35: dup
    //   36: iconst_0
    //   37: aload 8
    //   39: aastore
    //   40: pop
    //   41: aload 8
    //   43: ifnonnull +43 -> 86
    //   46: new 12	com/chase/sig/android/util/ChaseException
    //   49: dup
    //   50: getstatic 36	com/chase/sig/android/util/ChaseException:b	I
    //   53: ldc 38
    //   55: iconst_1
    //   56: anewarray 32	java/lang/Object
    //   59: dup
    //   60: iconst_0
    //   61: aload_0
    //   62: aastore
    //   63: invokestatic 44	java/lang/String:format	(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    //   66: invokespecial 47	com/chase/sig/android/util/ChaseException:<init>	(ILjava/lang/String;)V
    //   69: athrow
    //   70: astore 7
    //   72: aload 5
    //   74: invokestatic 24	com/chase/sig/android/ChaseApplication:a	()Lcom/chase/sig/android/ChaseApplication;
    //   77: invokevirtual 51	com/chase/sig/android/ChaseApplication:getApplicationContext	()Landroid/content/Context;
    //   80: invokevirtual 54	com/chase/sig/android/domain/ImageDownloadResponse:b	(Landroid/content/Context;)V
    //   83: aload 5
    //   85: areturn
    //   86: aload 8
    //   88: invokeinterface 60 1 0
    //   93: invokeinterface 66 1 0
    //   98: sipush 200
    //   101: if_icmpeq +33 -> 134
    //   104: aload 5
    //   106: invokestatic 24	com/chase/sig/android/ChaseApplication:a	()Lcom/chase/sig/android/ChaseApplication;
    //   109: invokevirtual 51	com/chase/sig/android/ChaseApplication:getApplicationContext	()Landroid/content/Context;
    //   112: invokevirtual 54	com/chase/sig/android/domain/ImageDownloadResponse:b	(Landroid/content/Context;)V
    //   115: aload 5
    //   117: areturn
    //   118: astore 6
    //   120: aload 5
    //   122: invokestatic 24	com/chase/sig/android/ChaseApplication:a	()Lcom/chase/sig/android/ChaseApplication;
    //   125: invokevirtual 51	com/chase/sig/android/ChaseApplication:getApplicationContext	()Landroid/content/Context;
    //   128: invokevirtual 54	com/chase/sig/android/domain/ImageDownloadResponse:b	(Landroid/content/Context;)V
    //   131: aload 5
    //   133: areturn
    //   134: aload 8
    //   136: invokeinterface 70 1 0
    //   141: astore 12
    //   143: aload 12
    //   145: astore 11
    //   147: iload_1
    //   148: ifeq +35 -> 183
    //   151: aload 11
    //   153: ifnull +59 -> 212
    //   156: aload 11
    //   158: invokeinterface 76 1 0
    //   163: invokeinterface 82 1 0
    //   168: ldc 84
    //   170: invokevirtual 88	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   173: ifeq +39 -> 212
    //   176: iload 4
    //   178: istore 13
    //   180: goto +97 -> 277
    //   183: aload 11
    //   185: invokeinterface 92 1 0
    //   190: astore_3
    //   191: aload 5
    //   193: aload_3
    //   194: invokestatic 98	android/graphics/BitmapFactory:decodeStream	(Ljava/io/InputStream;)Landroid/graphics/Bitmap;
    //   197: invokevirtual 101	com/chase/sig/android/domain/ImageDownloadResponse:a	(Landroid/graphics/Bitmap;)V
    //   200: aload_3
    //   201: invokestatic 104	com/chase/sig/android/service/j:a	(Ljava/io/Closeable;)V
    //   204: aload 11
    //   206: invokestatic 107	com/chase/sig/android/service/j:a	(Lorg/apache/http/HttpEntity;)V
    //   209: aload 5
    //   211: areturn
    //   212: iconst_0
    //   213: istore 13
    //   215: goto +62 -> 277
    //   218: aload 11
    //   220: ifnull +65 -> 285
    //   223: aload 11
    //   225: invokestatic 113	org/apache/http/util/EntityUtils:toString	(Lorg/apache/http/HttpEntity;)Ljava/lang/String;
    //   228: ldc 115
    //   230: invokevirtual 88	java/lang/String:contains	(Ljava/lang/CharSequence;)Z
    //   233: ifeq +52 -> 285
    //   236: aconst_null
    //   237: astore_3
    //   238: iload 4
    //   240: ifeq -40 -> 200
    //   243: getstatic 118	com/chase/sig/android/domain/ImageDownloadResponse:a	Lcom/chase/sig/android/domain/ImageDownloadResponse;
    //   246: astore 14
    //   248: aconst_null
    //   249: invokestatic 104	com/chase/sig/android/service/j:a	(Ljava/io/Closeable;)V
    //   252: aload 11
    //   254: invokestatic 107	com/chase/sig/android/service/j:a	(Lorg/apache/http/HttpEntity;)V
    //   257: aload 14
    //   259: areturn
    //   260: aload_3
    //   261: invokestatic 104	com/chase/sig/android/service/j:a	(Ljava/io/Closeable;)V
    //   264: aload 11
    //   266: invokestatic 107	com/chase/sig/android/service/j:a	(Lorg/apache/http/HttpEntity;)V
    //   269: aload 10
    //   271: athrow
    //   272: astore 10
    //   274: goto -14 -> 260
    //   277: iload 13
    //   279: ifeq -61 -> 218
    //   282: goto -99 -> 183
    //   285: iconst_0
    //   286: istore 4
    //   288: goto -52 -> 236
    //   291: astore 10
    //   293: aconst_null
    //   294: astore_3
    //   295: aconst_null
    //   296: astore 11
    //   298: goto -38 -> 260
    //
    // Exception table:
    //   from	to	target	type
    //   14	41	70	com/chase/sig/android/util/ChaseException
    //   46	70	70	com/chase/sig/android/util/ChaseException
    //   86	115	70	com/chase/sig/android/util/ChaseException
    //   200	209	70	com/chase/sig/android/util/ChaseException
    //   248	257	70	com/chase/sig/android/util/ChaseException
    //   260	272	70	com/chase/sig/android/util/ChaseException
    //   14	41	118	java/io/IOException
    //   46	70	118	java/io/IOException
    //   86	115	118	java/io/IOException
    //   200	209	118	java/io/IOException
    //   248	257	118	java/io/IOException
    //   260	272	118	java/io/IOException
    //   156	176	272	finally
    //   183	200	272	finally
    //   223	236	272	finally
    //   243	248	272	finally
    //   134	143	291	finally
  }

  private static void a(Closeable paramCloseable)
  {
    if (paramCloseable != null)
      paramCloseable.close();
  }

  private static void a(HttpEntity paramHttpEntity)
  {
    if (paramHttpEntity != null)
      paramHttpEntity.consumeContent();
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.j
 * JD-Core Version:    0.6.2
 */