package com.chase.sig.android.service;

import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.domain.InvestmentTransaction;
import com.chase.sig.android.domain.InvestmentTransaction.FilterType;
import com.chase.sig.android.util.Dollar;
import com.chase.sig.android.util.l;
import com.chase.sig.android.util.m;
import com.chase.sig.android.util.n;
import com.chase.sig.android.util.s;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class k extends JPService
{
  private static InvestmentTransaction a(JSONObject paramJSONObject)
  {
    InvestmentTransaction localInvestmentTransaction = new InvestmentTransaction();
    String str = paramJSONObject.optString("description");
    if (s.l(str))
      str = "";
    localInvestmentTransaction.d(str);
    JSONArray localJSONArray = paramJSONObject.optJSONArray("values");
    for (int i = 0; i < localJSONArray.length(); i++)
    {
      JSONObject localJSONObject = (JSONObject)localJSONArray.opt(i);
      if (localJSONObject.optString("fieldId").equals("amount"))
      {
        localInvestmentTransaction.a(new Dollar(b(localJSONObject).optString("value")));
        localInvestmentTransaction.h(localJSONObject.getJSONObject("value").optString("currency"));
      }
      if (localJSONObject.optString("fieldId").equals("date"))
        localInvestmentTransaction.a(localJSONObject.optString("value"));
      if (localJSONObject.optString("fieldId").equals("is-intraday"))
        localInvestmentTransaction.a(localJSONObject.optBoolean("value"));
      if (localJSONObject.optString("fieldId").equals("as-of-date"))
        localInvestmentTransaction.b(localJSONObject.optString("value"));
      if (localJSONObject.optString("fieldId").equals("quantity"))
        localInvestmentTransaction.a(localJSONObject.optDouble("value"));
      if (localJSONObject.optString("fieldId").equals("is-trade"))
        localInvestmentTransaction.b(localJSONObject.optBoolean("value"));
      if (localJSONObject.optString("fieldId").equals("trans-type"))
        localInvestmentTransaction.c(localJSONObject.optString("value"));
      if (localJSONObject.optString("fieldId").equals("settle-date"))
        localInvestmentTransaction.f(localJSONObject.optString("value"));
      if (localJSONObject.optString("fieldId").equals("trade-date"))
        localInvestmentTransaction.e(localJSONObject.optString("value"));
      if (localJSONObject.optString("fieldId").equals("symbol"))
        localInvestmentTransaction.g(localJSONObject.optString("value"));
      if (localJSONObject.optString("fieldId").equals("price"))
        localInvestmentTransaction.b(new Dollar(b(localJSONObject).optString("value")));
      if (localJSONObject.optString("fieldId").equals("cost"))
      {
        localInvestmentTransaction.c(new Dollar(b(localJSONObject).optString("value")));
        localInvestmentTransaction.i(localJSONObject.getJSONObject("value").optString("currency"));
      }
    }
    return localInvestmentTransaction;
  }

  public static InvestmentTransactionsResponse a(String paramString1, String paramString2, InvestmentTransaction.FilterType paramFilterType)
  {
    String str1 = a(2131165264);
    InvestmentTransactionsResponse localInvestmentTransactionsResponse = new InvestmentTransactionsResponse();
    Hashtable localHashtable = c();
    if (ChaseApplication.a().j())
      localHashtable.put("numDaysBack", String.valueOf(l.b()));
    localHashtable.put("accountId", paramString1);
    localHashtable.put("rows", "25");
    if (s.m(paramString2))
      localHashtable.put("dateRange", paramString2);
    if (paramFilterType != null)
    {
      String str2 = paramFilterType.a();
      if (s.m(str2))
        localHashtable.put("filterCode", str2);
      String str3 = paramFilterType.b();
      if (s.m(str3))
        localHashtable.put("filterValue", str3);
    }
    try
    {
      JSONObject localJSONObject1 = m.a(ChaseApplication.a(), str1, localHashtable);
      localInvestmentTransactionsResponse.b(localJSONObject1);
      if (n.a(localJSONObject1, "investmentTypes"))
      {
        JSONArray localJSONArray2 = localJSONObject1.optJSONArray("investmentTypes");
        if (localJSONArray2 != null)
        {
          int k = localJSONArray2.length();
          for (int m = 0; m < k; m++)
          {
            JSONObject localJSONObject2 = localJSONArray2.getJSONObject(m);
            InvestmentTransaction localInvestmentTransaction = new InvestmentTransaction();
            localInvestmentTransaction.getClass();
            InvestmentTransaction.FilterType localFilterType = new InvestmentTransaction.FilterType(localInvestmentTransaction);
            localFilterType.b(localJSONObject2.optString("filterCode"));
            localFilterType.a(localJSONObject2.optString("filterName"));
            localFilterType.a(localJSONObject2.optInt("sortOrdNo", -1));
            localFilterType.c(localJSONObject2.optString("filterValue"));
            localInvestmentTransactionsResponse.a(localFilterType);
          }
        }
      }
      if (n.a(localJSONObject1, "page"))
      {
        JSONArray localJSONArray1 = localJSONObject1.getJSONObject("page").optJSONArray("activity");
        ArrayList localArrayList = new ArrayList();
        if (localJSONArray1 != null)
        {
          int i = localJSONArray1.length();
          int j = 0;
          if (i > 0)
            while (j < localJSONArray1.length())
            {
              localArrayList.add(a(localJSONArray1.getJSONObject(j)));
              j++;
            }
        }
        localInvestmentTransactionsResponse.a(localArrayList);
      }
      localInvestmentTransactionsResponse.a(localJSONObject1.optString("dateRange"));
      localInvestmentTransactionsResponse.b(localJSONObject1.optString("filterValue"));
      return localInvestmentTransactionsResponse;
    }
    catch (JSONException localJSONException)
    {
      localInvestmentTransactionsResponse.a(ChaseApplication.a().getApplicationContext());
      return localInvestmentTransactionsResponse;
    }
    catch (Exception localException)
    {
      localInvestmentTransactionsResponse.a(ChaseApplication.a().getApplicationContext());
    }
    return localInvestmentTransactionsResponse;
  }

  private static JSONObject b(JSONObject paramJSONObject)
  {
    try
    {
      JSONObject localJSONObject = paramJSONObject.getJSONObject("value");
      return localJSONObject;
    }
    catch (JSONException localJSONException)
    {
      localJSONException.printStackTrace();
    }
    return null;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.k
 * JD-Core Version:    0.6.2
 */