package com.chase.sig.android.service;

import android.content.Context;
import android.content.res.Resources;
import com.chase.sig.android.util.r;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class l
{
  private List<IServiceError> errors;

  private static List<ServiceErrorAttribute> c(JSONObject paramJSONObject)
  {
    ArrayList localArrayList = new ArrayList();
    JSONArray localJSONArray = paramJSONObject.optJSONArray("attributes");
    if (localJSONArray != null)
      for (int i = 0; i < localJSONArray.length(); i++)
      {
        JSONObject localJSONObject = (JSONObject)localJSONArray.opt(i);
        localArrayList.add(new ServiceErrorAttribute(localJSONObject.optString("key"), localJSONObject.optString("value")));
      }
    return localArrayList;
  }

  public final l a(Context paramContext)
  {
    a(new ServiceError("2000", paramContext.getResources().getString(2131165820), true));
    return this;
  }

  public final void a(IServiceError paramIServiceError)
  {
    if (this.errors == null)
      this.errors = new ArrayList();
    this.errors.add(paramIServiceError);
  }

  public final void a(JSONObject paramJSONObject)
  {
    this.errors = new ArrayList();
    if (!paramJSONObject.has("errors"));
    while (true)
    {
      return;
      JSONObject localJSONObject1 = paramJSONObject.optJSONObject("errors");
      if (localJSONObject1 != null)
      {
        Iterator localIterator = localJSONObject1.keys();
        while (localIterator.hasNext())
          this.errors.add(new ServiceError("", localJSONObject1.optString((String)localIterator.next()), false));
      }
      JSONArray localJSONArray = paramJSONObject.optJSONArray("errors");
      if ((localJSONArray != null) && (localJSONArray.length() > 0))
        for (int i = 0; i < localJSONArray.length(); i++)
        {
          JSONObject localJSONObject2 = localJSONArray.optJSONObject(i);
          this.errors.add(new ServiceError("", localJSONObject2.toString(), false));
        }
    }
  }

  public final void b(Context paramContext)
  {
    a(paramContext);
  }

  public final void b(List<IServiceError> paramList)
  {
    this.errors = paramList;
  }

  public final void b(JSONObject paramJSONObject)
  {
    if (paramJSONObject == null);
    JSONArray localJSONArray;
    do
    {
      return;
      localJSONArray = paramJSONObject.optJSONArray("errors");
    }
    while (localJSONArray == null);
    this.errors = new ArrayList();
    int i = 0;
    label29: JSONObject localJSONObject;
    if (i < localJSONArray.length())
    {
      localJSONObject = (JSONObject)localJSONArray.opt(i);
      if (localJSONObject != null)
        if (localJSONObject.optInt("severity", 300) != 300)
          break label119;
    }
    label119: for (boolean bool = true; ; bool = false)
    {
      ServiceError localServiceError = new ServiceError(localJSONObject.optString("code"), localJSONObject.optString("message"), c(localJSONObject), bool);
      this.errors.add(localServiceError);
      i++;
      break label29;
      break;
    }
  }

  public final boolean c(String paramString)
  {
    return d(paramString) != null;
  }

  public final IServiceError d(String paramString)
  {
    m localm = new m(this, paramString);
    new r();
    if (r.a(this.errors, localm) >= 0)
    {
      List localList = this.errors;
      new r();
      return (IServiceError)localList.get(r.a(this.errors, localm));
    }
    return null;
  }

  public final boolean e()
  {
    return (this.errors != null) && (this.errors.size() > 0);
  }

  public final boolean f()
  {
    if (this.errors == null);
    while (true)
    {
      return false;
      for (int i = 0; i < this.errors.size(); i++)
        if (((IServiceError)this.errors.get(i)).d())
          return true;
    }
  }

  public final List<IServiceError> g()
  {
    return this.errors;
  }

  public final boolean h()
  {
    return d("2000") != null;
  }

  public final List<IServiceError> i()
  {
    return this.errors;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.l
 * JD-Core Version:    0.6.2
 */