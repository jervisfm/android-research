package com.chase.sig.android.service.movemoney;

import com.chase.sig.android.domain.LabeledValue;
import com.chase.sig.android.domain.Transaction;
import com.chase.sig.android.service.l;
import java.util.ArrayList;
import java.util.List;

public class ListServiceResponse<T extends Transaction> extends l
{
  private int currentPage;
  private List<LabeledValue> frequencyList;
  private List<T> list;
  private int totalPages;

  public final List<T> a()
  {
    return new ArrayList(this.list);
  }

  public final void a(int paramInt)
  {
    this.currentPage = paramInt;
  }

  public final void a(List<T> paramList)
  {
    this.list = paramList;
  }

  public final int b()
  {
    return this.currentPage;
  }

  public final void b(int paramInt)
  {
    this.totalPages = paramInt;
  }

  public final int c()
  {
    return this.totalPages;
  }

  public final void c(List<LabeledValue> paramList)
  {
    this.frequencyList = paramList;
  }

  public final List<LabeledValue> d()
  {
    return this.frequencyList;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.movemoney.ListServiceResponse
 * JD-Core Version:    0.6.2
 */