package com.chase.sig.android.service.movemoney;

import java.io.Serializable;

public class RequestFlags
  implements Serializable
{
  public static final RequestFlags a;
  public static final RequestFlags b;
  public static final RequestFlags c;
  public static final RequestFlags d;
  public static final RequestFlags e = localRequestFlags5;
  private boolean forValidation;
  private boolean updateModel;

  static
  {
    RequestFlags localRequestFlags1 = new RequestFlags();
    localRequestFlags1.forValidation = true;
    localRequestFlags1.updateModel = false;
    a = localRequestFlags1;
    RequestFlags localRequestFlags2 = new RequestFlags();
    localRequestFlags2.forValidation = true;
    localRequestFlags2.updateModel = true;
    b = localRequestFlags2;
    RequestFlags localRequestFlags3 = new RequestFlags();
    localRequestFlags3.forValidation = false;
    localRequestFlags3.updateModel = true;
    c = localRequestFlags3;
    RequestFlags localRequestFlags4 = new RequestFlags();
    localRequestFlags4.forValidation = false;
    localRequestFlags4.updateModel = false;
    d = localRequestFlags4;
    RequestFlags localRequestFlags5 = new RequestFlags();
    localRequestFlags5.forValidation = false;
    localRequestFlags5.updateModel = false;
  }

  public final boolean a()
  {
    return this.forValidation;
  }

  public final boolean b()
  {
    return this.updateModel;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.movemoney.RequestFlags
 * JD-Core Version:    0.6.2
 */