package com.chase.sig.android.service.movemoney;

import com.chase.sig.android.domain.Transaction;
import com.chase.sig.android.service.l;

public class ServiceResponse extends l
{
  private String dueDate;
  private String formId;
  private boolean includesOptionalProductFee;
  private String paymentId;
  private String processDate;
  private String status;
  private Transaction transaction;

  public final String a()
  {
    return this.processDate;
  }

  public final void a(String paramString)
  {
    this.processDate = paramString;
  }

  public final void a(boolean paramBoolean)
  {
    this.includesOptionalProductFee = paramBoolean;
  }

  public final String b()
  {
    return this.dueDate;
  }

  public final void b(String paramString)
  {
    this.dueDate = paramString;
  }

  public final String c()
  {
    return this.paymentId;
  }

  public final String d()
  {
    return this.status;
  }

  public final void e(String paramString)
  {
    this.paymentId = paramString;
  }

  public final void f(String paramString)
  {
    this.status = paramString;
  }

  public final void g(String paramString)
  {
    this.formId = paramString;
  }

  public final String j()
  {
    return this.formId;
  }

  public final boolean k()
  {
    return this.includesOptionalProductFee;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.movemoney.ServiceResponse
 * JD-Core Version:    0.6.2
 */