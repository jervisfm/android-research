package com.chase.sig.android.service.movemoney;

import com.chase.sig.android.domain.BillPayTransaction;
import com.chase.sig.android.domain.Payee;
import com.chase.sig.android.domain.Transaction;
import com.chase.sig.android.util.Dollar;
import com.chase.sig.android.util.s;
import java.math.BigDecimal;
import java.util.Hashtable;

public final class a extends e
{
  public final Hashtable<String, String> a(Transaction paramTransaction, String paramString)
  {
    Hashtable localHashtable = super.a(paramTransaction, paramString);
    BillPayTransaction localBillPayTransaction = (BillPayTransaction)paramTransaction;
    localHashtable.put("accountId", localBillPayTransaction.c());
    localHashtable.put("paymentId", localBillPayTransaction.n());
    localHashtable.put("paymentToken", localBillPayTransaction.g());
    String str = "false";
    if ((!localBillPayTransaction.l()) && (s.m(localBillPayTransaction.k())))
    {
      str = "true";
      localHashtable.put("paymentModelToken", localBillPayTransaction.k());
      localHashtable.put("paymentModelId", localBillPayTransaction.j());
    }
    localHashtable.put("cancelModel", str);
    return localHashtable;
  }

  public final Hashtable<String, String> a(Transaction paramTransaction, String paramString, RequestFlags paramRequestFlags)
  {
    Hashtable localHashtable = super.a(paramTransaction, paramString, paramRequestFlags);
    BillPayTransaction localBillPayTransaction = (BillPayTransaction)paramTransaction;
    localHashtable.put("validateOnly", String.valueOf(paramRequestFlags.a()));
    localHashtable.put("amount", localBillPayTransaction.e().b().toPlainString());
    localHashtable.put("dueDate", localBillPayTransaction.q());
    if (localBillPayTransaction.f() != null);
    for (String str = localBillPayTransaction.f(); ; str = localBillPayTransaction.q())
    {
      localHashtable.put("processDate", str);
      localHashtable.put("accountId", localBillPayTransaction.c());
      localHashtable.put("payeeId", localBillPayTransaction.a().j());
      localHashtable.put("memo", localBillPayTransaction.o());
      if (!paramRequestFlags.a())
        localHashtable.put("formId", paramTransaction.p());
      return localHashtable;
    }
  }

  public final Hashtable<String, String> b(Transaction paramTransaction, String paramString, RequestFlags paramRequestFlags)
  {
    Hashtable localHashtable = super.b(paramTransaction, paramString, paramRequestFlags);
    BillPayTransaction localBillPayTransaction = (BillPayTransaction)paramTransaction;
    localHashtable.put("paymentId", localBillPayTransaction.n());
    localHashtable.put("paymentToken", localBillPayTransaction.g());
    localHashtable.put("amount", localBillPayTransaction.e().e());
    localHashtable.put("dueDate", localBillPayTransaction.q());
    if (localBillPayTransaction.o() == null);
    for (String str = ""; ; str = localBillPayTransaction.o())
    {
      localHashtable.put("memo", str);
      localHashtable.put("processDate", localBillPayTransaction.q());
      localHashtable.put("accountId", localBillPayTransaction.c());
      localHashtable.put("payeeId", localBillPayTransaction.b());
      localHashtable.put("validateOnly", String.valueOf(paramRequestFlags.a()));
      localHashtable.put("updateModel", String.valueOf(paramRequestFlags.b()));
      if (!paramRequestFlags.a())
        localHashtable.put("formId", localBillPayTransaction.p());
      if (!localBillPayTransaction.l())
        a(localHashtable, localBillPayTransaction);
      return localHashtable;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.movemoney.a
 * JD-Core Version:    0.6.2
 */