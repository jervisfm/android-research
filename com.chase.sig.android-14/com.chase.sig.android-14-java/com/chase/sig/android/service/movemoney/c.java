package com.chase.sig.android.service.movemoney;

import com.chase.sig.android.domain.Transaction;
import com.chase.sig.android.service.JPService;
import java.util.Hashtable;

public final class c extends e
{
  public final Hashtable<String, String> a(int paramInt)
  {
    Hashtable localHashtable = JPService.c();
    localHashtable.put("accountId", Integer.valueOf(paramInt).toString());
    return localHashtable;
  }

  public final Hashtable<String, String> a(Transaction paramTransaction, String paramString)
  {
    Hashtable localHashtable = JPService.c();
    localHashtable.put("accountId", paramTransaction.A());
    localHashtable.put("confNum", paramTransaction.n());
    return localHashtable;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.movemoney.c
 * JD-Core Version:    0.6.2
 */