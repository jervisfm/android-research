package com.chase.sig.android.service.movemoney;

import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.domain.Transaction;
import com.chase.sig.android.domain.a;
import com.chase.sig.android.domain.o;
import com.chase.sig.android.service.JPService;
import com.chase.sig.android.service.JPService.CrossSiteRequestForgeryTokenFailureException;
import com.chase.sig.android.util.ChaseException;
import com.chase.sig.android.util.m;
import java.util.Hashtable;
import org.json.JSONException;
import org.json.JSONObject;

public class d<T extends Transaction> extends JPService
{
  public g a;
  public e b;
  private f<T> c;

  public d()
  {
  }

  public d(g paramg, e parame, f<T> paramf)
  {
    this.a = paramg;
    this.b = parame;
    this.c = paramf;
  }

  private static String a()
  {
    return ChaseApplication.a().b().a.c;
  }

  private ServiceResponse b(Hashtable<String, String> paramHashtable, String paramString)
  {
    ServiceResponse localServiceResponse1 = new ServiceResponse();
    try
    {
      JSONObject localJSONObject = m.a(ChaseApplication.a(), paramString, paramHashtable);
      localServiceResponse1.a(localJSONObject);
      localServiceResponse1.b(localJSONObject);
      ServiceResponse localServiceResponse2 = this.c.a(localServiceResponse1, localJSONObject);
      return localServiceResponse2;
    }
    catch (ChaseException localChaseException)
    {
      localServiceResponse1.a(ChaseApplication.a());
      return localServiceResponse1;
    }
    catch (JSONException localJSONException)
    {
      localServiceResponse1.a(ChaseApplication.a());
    }
    return localServiceResponse1;
  }

  protected final ListServiceResponse<T> a(Hashtable<String, String> paramHashtable)
  {
    return a(paramHashtable, this.a.d);
  }

  public final ListServiceResponse<T> a(Hashtable<String, String> paramHashtable, String paramString)
  {
    try
    {
      JSONObject localJSONObject = m.a(ChaseApplication.a(), paramString, paramHashtable);
      ListServiceResponse localListServiceResponse2 = this.c.b(localJSONObject);
      localListServiceResponse2.b(localJSONObject);
      return localListServiceResponse2;
    }
    catch (Exception localException)
    {
      ListServiceResponse localListServiceResponse1 = new ListServiceResponse();
      localListServiceResponse1.a(ChaseApplication.a());
      return localListServiceResponse1;
    }
  }

  public final ServiceResponse a(Transaction paramTransaction)
  {
    return b(this.b.a(paramTransaction, a()), this.a.a);
  }

  public final ServiceResponse a(T paramT, RequestFlags paramRequestFlags)
  {
    return b(this.b.b(paramT, a(), paramRequestFlags), this.a.b);
  }

  public final ServiceResponse b(Transaction paramTransaction, RequestFlags paramRequestFlags)
  {
    try
    {
      Hashtable localHashtable = this.b.a(paramTransaction, a(), paramRequestFlags);
      return b(localHashtable, this.a.c);
    }
    catch (JPService.CrossSiteRequestForgeryTokenFailureException localCrossSiteRequestForgeryTokenFailureException)
    {
      ServiceResponse localServiceResponse = new ServiceResponse();
      localServiceResponse.a(ChaseApplication.a().getApplicationContext());
      return localServiceResponse;
    }
  }

  public ListServiceResponse<T> c(int paramInt)
  {
    return a(this.b.a(paramInt));
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.movemoney.d
 * JD-Core Version:    0.6.2
 */