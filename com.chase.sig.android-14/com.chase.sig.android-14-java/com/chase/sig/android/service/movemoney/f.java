package com.chase.sig.android.service.movemoney;

import com.chase.sig.android.domain.LabeledValue;
import com.chase.sig.android.domain.Transaction;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public abstract class f<T extends Transaction>
{
  protected abstract T a(JSONObject paramJSONObject);

  public abstract ServiceResponse a(ServiceResponse paramServiceResponse, JSONObject paramJSONObject);

  public final ListServiceResponse<T> b(JSONObject paramJSONObject)
  {
    int i = 0;
    ListServiceResponse localListServiceResponse = new ListServiceResponse();
    if (!paramJSONObject.isNull("frequencyList"))
    {
      JSONArray localJSONArray2 = paramJSONObject.getJSONArray("frequencyList");
      ArrayList localArrayList2 = new ArrayList();
      for (int j = 0; j < localJSONArray2.length(); j++)
      {
        JSONObject localJSONObject3 = localJSONArray2.getJSONObject(j);
        localArrayList2.add(new LabeledValue(localJSONObject3.getString("label"), localJSONObject3.getString("value")));
      }
      localListServiceResponse.c(localArrayList2);
    }
    if (!paramJSONObject.isNull("paymentsPage"))
    {
      JSONObject localJSONObject1 = paramJSONObject.getJSONObject("paymentsPage");
      if (localJSONObject1.has("pageItems"))
      {
        ArrayList localArrayList1 = new ArrayList();
        JSONArray localJSONArray1 = localJSONObject1.getJSONArray("pageItems");
        while (i < localJSONArray1.length())
        {
          localArrayList1.add(a(localJSONArray1.getJSONObject(i)));
          i++;
        }
        localListServiceResponse.a(localArrayList1);
      }
      JSONObject localJSONObject2 = localJSONObject1.optJSONObject("pageInfo");
      if (localJSONObject2 != null)
      {
        localListServiceResponse.a(localJSONObject2.optInt("currentPage"));
        localListServiceResponse.b(localJSONObject2.optInt("totalPages"));
      }
    }
    return localListServiceResponse;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.movemoney.f
 * JD-Core Version:    0.6.2
 */