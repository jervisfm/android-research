package com.chase.sig.android.service.movemoney;

import com.chase.sig.android.domain.AccountTransfer;
import com.chase.sig.android.domain.Transaction;
import com.chase.sig.android.util.Dollar;
import com.chase.sig.android.util.s;
import java.util.Hashtable;

public final class h extends e
{
  public final Hashtable<String, String> a(Transaction paramTransaction, String paramString)
  {
    Hashtable localHashtable = super.a(paramTransaction, paramString);
    AccountTransfer localAccountTransfer = (AccountTransfer)paramTransaction;
    localHashtable.put("accountId", localAccountTransfer.c());
    localHashtable.put("paymentId", localAccountTransfer.n());
    localHashtable.put("paymentToken", localAccountTransfer.g());
    String str = "false";
    if ((!localAccountTransfer.l()) && (s.m(localAccountTransfer.k())))
    {
      str = "true";
      localHashtable.put("paymentModelToken", localAccountTransfer.k());
      localHashtable.put("paymentModelId", localAccountTransfer.j());
    }
    localHashtable.put("cancelModel", str);
    return localHashtable;
  }

  public final Hashtable<String, String> a(Transaction paramTransaction, String paramString, RequestFlags paramRequestFlags)
  {
    Hashtable localHashtable = super.a(paramTransaction, paramString, paramRequestFlags);
    AccountTransfer localAccountTransfer = (AccountTransfer)paramTransaction;
    if (paramRequestFlags.a())
    {
      localHashtable.put("validateOnly", String.valueOf(paramRequestFlags.a()));
      localHashtable.put("amount", localAccountTransfer.e().e());
      localHashtable.put("dueDate", localAccountTransfer.q());
      localHashtable.put("memo", localAccountTransfer.o());
      localHashtable.put("processDate", localAccountTransfer.q());
      localHashtable.put("creditAccountId", localAccountTransfer.a());
      localHashtable.put("accountId", localAccountTransfer.c());
      return localHashtable;
    }
    localHashtable.put("formId", localAccountTransfer.p());
    localHashtable.put("accountId", localAccountTransfer.c());
    return localHashtable;
  }

  public final Hashtable<String, String> b(Transaction paramTransaction, String paramString, RequestFlags paramRequestFlags)
  {
    Hashtable localHashtable = super.b(paramTransaction, paramString, paramRequestFlags);
    AccountTransfer localAccountTransfer = (AccountTransfer)paramTransaction;
    localHashtable.put("validateOnly", String.valueOf(paramRequestFlags.a()));
    localHashtable.put("amount", localAccountTransfer.e().e());
    localHashtable.put("creditAccountNickName", localAccountTransfer.r());
    localHashtable.put("dueDate", localAccountTransfer.q());
    localHashtable.put("fundingAccountId", localAccountTransfer.c());
    localHashtable.put("memo", localAccountTransfer.o());
    localHashtable.put("paymentId", localAccountTransfer.n());
    localHashtable.put("paymentToken", localAccountTransfer.g());
    localHashtable.put("updateModel", String.valueOf(paramRequestFlags.b()));
    if (!localAccountTransfer.l())
      a(localHashtable, localAccountTransfer);
    localHashtable.put("processDate", localAccountTransfer.q());
    localHashtable.put("creditAccountId", localAccountTransfer.a());
    localHashtable.put("accountId", localAccountTransfer.c());
    if (!paramRequestFlags.a())
      localHashtable.put("formId", localAccountTransfer.p());
    return localHashtable;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.movemoney.h
 * JD-Core Version:    0.6.2
 */