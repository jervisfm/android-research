package com.chase.sig.android.service.movemoney;

import com.chase.sig.android.domain.AccountTransfer;
import org.json.JSONObject;

public final class i extends f<AccountTransfer>
{
  public final ServiceResponse a(ServiceResponse paramServiceResponse, JSONObject paramJSONObject)
  {
    if (!paramJSONObject.isNull("payment"))
    {
      JSONObject localJSONObject = (JSONObject)paramJSONObject.opt("payment");
      paramServiceResponse.a(localJSONObject.optString("processDate"));
      paramServiceResponse.b(localJSONObject.optString("dueDate"));
      paramServiceResponse.e(localJSONObject.optString("paymentId"));
      paramServiceResponse.f(localJSONObject.optString("status"));
      paramServiceResponse.a(localJSONObject.optBoolean("includesOptionalProductFee"));
    }
    if (paramJSONObject.has("formId"))
      paramServiceResponse.g(paramJSONObject.optString("formId"));
    return paramServiceResponse;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.movemoney.i
 * JD-Core Version:    0.6.2
 */