package com.chase.sig.android.service.movemoney;

import com.chase.sig.android.domain.Transaction;
import com.chase.sig.android.domain.WireTransaction;
import com.chase.sig.android.util.Dollar;
import com.chase.sig.android.util.s;
import java.util.Hashtable;

public final class j extends e
{
  public final Hashtable<String, String> a(Transaction paramTransaction, String paramString)
  {
    Hashtable localHashtable = super.a(paramTransaction, paramString);
    WireTransaction localWireTransaction = (WireTransaction)paramTransaction;
    localHashtable.put("paymentId", localWireTransaction.n());
    localHashtable.put("paymentToken", localWireTransaction.g());
    String str = "false";
    if ((!localWireTransaction.l()) && (s.m(localWireTransaction.k())))
    {
      str = "true";
      localHashtable.put("paymentModelToken", localWireTransaction.k());
      localHashtable.put("paymentModelId", localWireTransaction.j());
    }
    localHashtable.put("cancelModel", str);
    return localHashtable;
  }

  public final Hashtable<String, String> a(Transaction paramTransaction, String paramString, RequestFlags paramRequestFlags)
  {
    Hashtable localHashtable = super.a(paramTransaction, paramString, paramRequestFlags);
    WireTransaction localWireTransaction = (WireTransaction)paramTransaction;
    localHashtable.put("validateOnly", String.valueOf(paramRequestFlags.a()));
    if (paramRequestFlags.a())
    {
      localHashtable.put("amount", localWireTransaction.e().e());
      localHashtable.put("dueDate", localWireTransaction.q());
      localHashtable.put("recipientMessage", localWireTransaction.f());
      localHashtable.put("bankMessage", localWireTransaction.h());
      localHashtable.put("memo", localWireTransaction.o());
      localHashtable.put("processDate", localWireTransaction.q());
      localHashtable.put("accountId", localWireTransaction.c());
      localHashtable.put("payeeId", localWireTransaction.D());
      return localHashtable;
    }
    localHashtable.put("formId", localWireTransaction.p());
    localHashtable.put("accountId", localWireTransaction.c());
    return localHashtable;
  }

  public final Hashtable<String, String> b(Transaction paramTransaction, String paramString, RequestFlags paramRequestFlags)
  {
    Hashtable localHashtable = super.b(paramTransaction, paramString, paramRequestFlags);
    WireTransaction localWireTransaction = (WireTransaction)paramTransaction;
    localHashtable.put("amount", localWireTransaction.e().e());
    localHashtable.put("accountId", localWireTransaction.c());
    localHashtable.put("dueDate", localWireTransaction.q());
    localHashtable.put("processDate", localWireTransaction.q());
    if (s.m(localWireTransaction.b()))
      localHashtable.put("fedReference", localWireTransaction.b());
    if (!localWireTransaction.l())
      a(localHashtable, localWireTransaction);
    localHashtable.put("recipientMessage", localWireTransaction.f());
    localHashtable.put("bankMessage", localWireTransaction.h());
    localHashtable.put("memo", localWireTransaction.o());
    localHashtable.put("paymentId", localWireTransaction.n());
    localHashtable.put("payeeId", localWireTransaction.D());
    localHashtable.put("paymentToken", localWireTransaction.g());
    localHashtable.put("updateModel", String.valueOf(paramRequestFlags.b()));
    localHashtable.put("validateOnly", String.valueOf(paramRequestFlags.a()));
    if (!paramRequestFlags.a())
      localHashtable.put("formId", localWireTransaction.p());
    return localHashtable;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.movemoney.j
 * JD-Core Version:    0.6.2
 */