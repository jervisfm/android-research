package com.chase.sig.android.service;

import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.service.movemoney.i;

public final class n
{
  private z A;
  private aa B;
  private j C;
  private s D;
  private com.chase.sig.android.service.content.a E;
  private ad F;
  private ac G;
  private p H;
  public q a;
  public t b;
  public a c;
  public b d;
  public e e;
  public c f;
  public ag g;
  public d h;
  public com.chase.sig.android.service.disclosures.a i;
  public f j;
  public v k;
  public k l;
  public h m;
  public aj n;
  public ak o;
  GeocoderService p;
  public ab q;
  public SmartService r;
  public ai s;
  private r t;
  private w u;
  private com.chase.sig.android.service.epay.b v;
  private o w;
  private com.chase.sig.android.service.quickdeposit.a x;
  private y y;
  private x z;

  public static com.chase.sig.android.service.transfer.a e()
  {
    return new com.chase.sig.android.service.transfer.a(new com.chase.sig.android.service.movemoney.g(ChaseApplication.a().c("gws"), 2131165219, 2131165220, 2131165221, 2131165222), new com.chase.sig.android.service.movemoney.h(), new i());
  }

  public static com.chase.sig.android.service.wire.a f()
  {
    return new com.chase.sig.android.service.wire.a(new com.chase.sig.android.service.movemoney.g(ChaseApplication.a().c("gws"), 2131165240, 2131165237, 2131165238, 2131165239), new com.chase.sig.android.service.movemoney.j(), new com.chase.sig.android.service.wire.b());
  }

  public static com.chase.sig.android.service.billpay.b g()
  {
    return new com.chase.sig.android.service.billpay.b(new com.chase.sig.android.service.movemoney.g(ChaseApplication.a().c("gws"), 2131165256, 2131165257, 2131165255, 2131165254), new com.chase.sig.android.service.movemoney.a(), new com.chase.sig.android.service.movemoney.b());
  }

  public static com.chase.sig.android.service.epay.a h()
  {
    return new com.chase.sig.android.service.epay.a(new com.chase.sig.android.service.movemoney.g(ChaseApplication.a().c("gws"), 2131165236, 2131165231, 2131165232, 2131165233), new com.chase.sig.android.service.movemoney.c(), new g());
  }

  public final com.chase.sig.android.service.epay.b a()
  {
    if (this.v == null)
      this.v = new com.chase.sig.android.service.epay.b();
    return this.v;
  }

  public final r b()
  {
    if (this.t == null)
      this.t = new r(c());
    return this.t;
  }

  public final w c()
  {
    if (this.u == null)
      this.u = new w();
    return this.u;
  }

  public final com.chase.sig.android.service.quickdeposit.a d()
  {
    if (this.x == null)
      this.x = new com.chase.sig.android.service.quickdeposit.a();
    return this.x;
  }

  public final o i()
  {
    if (this.w == null)
      this.w = new o();
    return this.w;
  }

  public final y j()
  {
    if (this.y == null)
      this.y = new y();
    return this.y;
  }

  public final x k()
  {
    if (this.z == null)
      this.z = new x();
    return this.z;
  }

  public final z l()
  {
    if (this.A == null)
      this.A = new z();
    return this.A;
  }

  public final aa m()
  {
    if (this.B == null)
      this.B = new aa();
    return this.B;
  }

  public final j n()
  {
    if (this.C == null)
      this.C = new j();
    return this.C;
  }

  public final s o()
  {
    if (this.D == null)
      this.D = new s();
    return this.D;
  }

  public final com.chase.sig.android.service.content.a p()
  {
    if (this.E == null)
      this.E = new com.chase.sig.android.service.content.a();
    return this.E;
  }

  public final ad q()
  {
    if (this.F == null)
      this.F = new ad();
    return this.F;
  }

  public final ac r()
  {
    if (this.G == null)
    {
      if (this.H == null)
        this.H = new p();
      this.G = new ac(this.H);
    }
    return this.G;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.n
 * JD-Core Version:    0.6.2
 */