package com.chase.sig.android.service;

import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.domain.g;
import com.chase.sig.android.util.m;
import java.util.Hashtable;
import org.json.JSONObject;

public final class o extends JPService
{
  public static LegalAgreementGetDetailsResponse a(String paramString)
  {
    String str = a(2131165269);
    LegalAgreementGetDetailsResponse localLegalAgreementGetDetailsResponse = new LegalAgreementGetDetailsResponse();
    Hashtable localHashtable = c();
    try
    {
      localHashtable.put("productId", paramString);
      localHashtable.put("segmentId", ChaseApplication.a().b().b.f());
      JSONObject localJSONObject = m.a(ChaseApplication.a(), str, localHashtable);
      localLegalAgreementGetDetailsResponse.b(localJSONObject);
      localLegalAgreementGetDetailsResponse.a(localJSONObject.getString("code"));
      localLegalAgreementGetDetailsResponse.b(localJSONObject.getString("status"));
      localLegalAgreementGetDetailsResponse.f(localJSONObject.getString("content"));
      localLegalAgreementGetDetailsResponse.e(localJSONObject.getString("version"));
      return localLegalAgreementGetDetailsResponse;
    }
    catch (Exception localException)
    {
      localLegalAgreementGetDetailsResponse.a(ChaseApplication.a().getApplicationContext());
    }
    return localLegalAgreementGetDetailsResponse;
  }

  public static LegalAgreementUpdateResponse a(String paramString, boolean paramBoolean)
  {
    String str1 = a(2131165270);
    LegalAgreementUpdateResponse localLegalAgreementUpdateResponse = new LegalAgreementUpdateResponse();
    Hashtable localHashtable = c();
    try
    {
      localHashtable.put("productId", paramString);
      localHashtable.put("segmentId", ChaseApplication.a().b().b.f());
      localHashtable.put("accepted", "1");
      localHashtable.put("denied", "0");
      if (paramBoolean);
      for (String str2 = "1"; ; str2 = "0")
      {
        localHashtable.put("viewed", str2);
        JSONObject localJSONObject = m.a(ChaseApplication.a(), str1, localHashtable);
        localLegalAgreementUpdateResponse.b(localJSONObject);
        localLegalAgreementUpdateResponse.a(localJSONObject.getString("agreementId"));
        return localLegalAgreementUpdateResponse;
      }
    }
    catch (Exception localException)
    {
      localLegalAgreementUpdateResponse.a(ChaseApplication.a().getApplicationContext());
    }
    return localLegalAgreementUpdateResponse;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.o
 * JD-Core Version:    0.6.2
 */