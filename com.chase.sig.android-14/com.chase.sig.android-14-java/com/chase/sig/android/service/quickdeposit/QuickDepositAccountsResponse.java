package com.chase.sig.android.service.quickdeposit;

import com.chase.sig.android.domain.QuickDepositAccount;
import com.chase.sig.android.service.l;
import java.io.Serializable;
import java.util.List;

public class QuickDepositAccountsResponse extends l
  implements Serializable
{
  private List<QuickDepositAccount> accounts;

  public final List<QuickDepositAccount> a()
  {
    return this.accounts;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.quickdeposit.QuickDepositAccountsResponse
 * JD-Core Version:    0.6.2
 */