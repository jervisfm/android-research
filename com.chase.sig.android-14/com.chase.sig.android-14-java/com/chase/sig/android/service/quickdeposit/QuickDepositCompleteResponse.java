package com.chase.sig.android.service.quickdeposit;

import com.chase.sig.android.service.l;
import com.chase.sig.android.util.Dollar;

public class QuickDepositCompleteResponse extends l
{
  private Dollar availableBalance;
  private Dollar delayedAmount1;
  private Dollar delayedAmount2;
  private String delayedAmountAvailableDate1;
  private String delayedAmountAvailableDate2;
  private String effectiveDate;
  private String largeDollar;
  private String onus;
  private String payingBank;
  private Dollar presentBalance;
  private String standardBottom;
  private String variableBottom;

  public final String a()
  {
    return this.effectiveDate;
  }

  public final void a(Dollar paramDollar)
  {
    this.availableBalance = paramDollar;
  }

  public final void a(String paramString)
  {
    this.effectiveDate = paramString;
  }

  public final void b(Dollar paramDollar)
  {
    this.presentBalance = paramDollar;
  }

  public final void b(String paramString)
  {
    this.delayedAmountAvailableDate1 = paramString;
  }

  public final void c(Dollar paramDollar)
  {
    this.delayedAmount1 = paramDollar;
  }

  public final void d(Dollar paramDollar)
  {
    this.delayedAmount2 = paramDollar;
  }

  public final void e(String paramString)
  {
    this.delayedAmountAvailableDate2 = paramString;
  }

  public final void f(String paramString)
  {
    this.largeDollar = paramString;
  }

  public final void g(String paramString)
  {
    this.payingBank = paramString;
  }

  public final void h(String paramString)
  {
    this.onus = paramString;
  }

  public final void i(String paramString)
  {
    this.standardBottom = paramString;
  }

  public final void j(String paramString)
  {
    this.variableBottom = paramString;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.quickdeposit.QuickDepositCompleteResponse
 * JD-Core Version:    0.6.2
 */