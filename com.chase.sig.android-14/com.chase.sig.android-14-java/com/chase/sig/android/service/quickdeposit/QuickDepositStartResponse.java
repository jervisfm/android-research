package com.chase.sig.android.service.quickdeposit;

import com.chase.sig.android.service.l;
import com.chase.sig.android.util.Dollar;

public class QuickDepositStartResponse extends l
{
  private Dollar DeltaAmount;
  private boolean hasErrorWithAmount;
  private boolean hasErrorWithDepositAccount;
  private boolean isApprovedForDeposit;
  private Dollar maxAmount;
  private Dollar minAmount;

  public final void a(boolean paramBoolean)
  {
    this.hasErrorWithAmount = paramBoolean;
  }

  public final boolean a()
  {
    return this.hasErrorWithAmount;
  }

  public final void b(boolean paramBoolean)
  {
    this.hasErrorWithDepositAccount = paramBoolean;
  }

  public final boolean b()
  {
    return this.hasErrorWithDepositAccount;
  }

  public final void c(boolean paramBoolean)
  {
    this.isApprovedForDeposit = paramBoolean;
  }

  public final boolean c()
  {
    return this.isApprovedForDeposit;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.quickdeposit.QuickDepositStartResponse
 * JD-Core Version:    0.6.2
 */