package com.chase.sig.android.service.quickdeposit;

import com.chase.sig.android.service.l;
import com.chase.sig.android.util.Dollar;
import java.io.Serializable;

public class QuickDepositVerifyResponse extends l
  implements Serializable
{
  private String footNote;
  private boolean hasErrorWithAccountNumber;
  private boolean hasErrorWithAmount;
  private boolean hasErrorWithCheckFrontImage;
  private boolean hasErrorWithRoutingNumber;
  private boolean isAccountNumberEditable;
  private boolean isAmountEditable;
  private boolean isRoutingNumberEditable;
  private String itemSequenceNumber;
  private Dollar readAmount;
  private boolean success;

  public final void a(Dollar paramDollar)
  {
    this.readAmount = paramDollar;
  }

  public final void a(String paramString)
  {
    this.itemSequenceNumber = paramString;
  }

  public final void a(boolean paramBoolean)
  {
    this.hasErrorWithAmount = paramBoolean;
  }

  public final boolean a()
  {
    return this.hasErrorWithAmount;
  }

  public final void b(String paramString)
  {
    this.footNote = paramString;
  }

  public final void b(boolean paramBoolean)
  {
    this.hasErrorWithCheckFrontImage = paramBoolean;
  }

  public final boolean b()
  {
    return this.hasErrorWithCheckFrontImage;
  }

  public final void c(boolean paramBoolean)
  {
    this.hasErrorWithRoutingNumber = paramBoolean;
  }

  public final boolean c()
  {
    return this.hasErrorWithCheckFrontImage;
  }

  public final void d(boolean paramBoolean)
  {
    this.hasErrorWithAccountNumber = paramBoolean;
  }

  public final boolean d()
  {
    return this.hasErrorWithRoutingNumber;
  }

  public final void e(boolean paramBoolean)
  {
    this.isAmountEditable = paramBoolean;
  }

  public final void f(boolean paramBoolean)
  {
    this.isRoutingNumberEditable = paramBoolean;
  }

  public final void g(boolean paramBoolean)
  {
    this.isAccountNumberEditable = paramBoolean;
  }

  public final void h(boolean paramBoolean)
  {
    this.success = paramBoolean;
  }

  public final boolean j()
  {
    return this.hasErrorWithAccountNumber;
  }

  public final boolean k()
  {
    return this.isRoutingNumberEditable;
  }

  public final boolean l()
  {
    return this.isAccountNumberEditable;
  }

  public final Dollar m()
  {
    return this.readAmount;
  }

  public final boolean n()
  {
    return this.success;
  }

  public final String o()
  {
    return this.itemSequenceNumber;
  }

  public final String p()
  {
    return this.footNote;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.quickdeposit.QuickDepositVerifyResponse
 * JD-Core Version:    0.6.2
 */