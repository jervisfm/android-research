package com.chase.sig.android.service.quickdeposit;

import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.domain.QuickDeposit;
import com.chase.sig.android.domain.QuickDepositAccount;
import com.chase.sig.android.domain.d;
import com.chase.sig.android.service.JPService;
import com.chase.sig.android.util.ChaseException;
import com.chase.sig.android.util.Dollar;
import com.chase.sig.android.util.m;
import com.chase.sig.android.util.s;
import com.google.gson.chase.Gson;
import com.google.gson.chase.GsonBuilder;
import java.math.BigDecimal;
import java.util.Hashtable;
import org.json.JSONException;
import org.json.JSONObject;

public final class a extends JPService
{
  public static QuickDepositAccountsResponse a()
  {
    QuickDepositAccountsResponse localQuickDepositAccountsResponse1 = new QuickDepositAccountsResponse();
    try
    {
      Hashtable localHashtable = c();
      String str = a(2131165271);
      JSONObject localJSONObject = m.a(ChaseApplication.a(), str, localHashtable);
      localQuickDepositAccountsResponse1.b(localJSONObject);
      if (localQuickDepositAccountsResponse1.e())
        return localQuickDepositAccountsResponse1;
      localQuickDepositAccountsResponse2 = (QuickDepositAccountsResponse)new GsonBuilder().a(Dollar.class, new d()).b().a(localJSONObject.toString(), QuickDepositAccountsResponse.class);
      return localQuickDepositAccountsResponse2;
    }
    catch (Exception localException)
    {
      while (true)
      {
        localQuickDepositAccountsResponse1.a(ChaseApplication.a().getApplicationContext());
        QuickDepositAccountsResponse localQuickDepositAccountsResponse2 = localQuickDepositAccountsResponse1;
      }
    }
  }

  public static QuickDepositCompleteResponse a(QuickDeposit paramQuickDeposit, String paramString1, String paramString2, String paramString3)
  {
    QuickDepositCompleteResponse localQuickDepositCompleteResponse = new QuickDepositCompleteResponse();
    try
    {
      Hashtable localHashtable = c();
      if ((s.m(paramString2)) || (s.m(paramString1)) || (s.m(paramString3)))
      {
        localHashtable.put("updateRequired", "true");
        localHashtable.put("itemSequenceNumber", paramQuickDeposit.p());
        a(localHashtable, "correctedAccountNumber", paramString2);
        a(localHashtable, "correctedRoutingNumber", paramString1);
        a(localHashtable, "correctedDepositAmount", paramString3);
      }
      localHashtable.put("transactionId", paramQuickDeposit.b());
      localHashtable.put("startDepositAmount", paramQuickDeposit.i().b().toPlainString());
      String str = a(2131165274);
      JSONObject localJSONObject = m.a(ChaseApplication.a(), str, localHashtable);
      localQuickDepositCompleteResponse.b(localJSONObject);
      localQuickDepositCompleteResponse.a(localJSONObject.getString("effectiveDate"));
      localQuickDepositCompleteResponse.a(new Dollar(localJSONObject.optString("availableBalance")));
      localQuickDepositCompleteResponse.b(new Dollar(localJSONObject.optString("presentBalance")));
      localQuickDepositCompleteResponse.a(localJSONObject.getString("effectiveDate"));
      localQuickDepositCompleteResponse.c(new Dollar(localJSONObject.optString("delayedAmount1")));
      localQuickDepositCompleteResponse.b(localJSONObject.optString("delayedAmountAvailableDate1"));
      localQuickDepositCompleteResponse.d(new Dollar(localJSONObject.optString("delayedAmount2")));
      localQuickDepositCompleteResponse.e(localJSONObject.optString("delayedAmountAvailableDate2"));
      localQuickDepositCompleteResponse.g(localJSONObject.optString("payingBank"));
      localQuickDepositCompleteResponse.h(localJSONObject.optString("onus"));
      localQuickDepositCompleteResponse.i(localJSONObject.optString("standardBottom"));
      localQuickDepositCompleteResponse.j(localJSONObject.optString("variableBottom"));
      localQuickDepositCompleteResponse.f(localJSONObject.optString("largeDollar"));
      return localQuickDepositCompleteResponse;
    }
    catch (ChaseException localChaseException)
    {
      localQuickDepositCompleteResponse.a(ChaseApplication.a().getApplicationContext());
      return localQuickDepositCompleteResponse;
    }
    catch (JSONException localJSONException)
    {
      localQuickDepositCompleteResponse.a(ChaseApplication.a().getApplicationContext());
    }
    return localQuickDepositCompleteResponse;
  }

  public static QuickDepositStartResponse a(QuickDeposit paramQuickDeposit)
  {
    QuickDepositStartResponse localQuickDepositStartResponse = new QuickDepositStartResponse();
    try
    {
      Hashtable localHashtable = c();
      localHashtable.put("amount", paramQuickDeposit.i().b().toPlainString());
      localHashtable.put("accountId", paramQuickDeposit.o().a());
      if (s.m(paramQuickDeposit.m()))
        localHashtable.put("ulid", String.valueOf(paramQuickDeposit.m()));
      JSONObject localJSONObject = m.a(ChaseApplication.a(), a(2131165272), localHashtable);
      localQuickDepositStartResponse.b(localJSONObject);
      localQuickDepositStartResponse.c(localJSONObject.optBoolean("approvedForDeposit"));
      localQuickDepositStartResponse.a(localJSONObject.optBoolean("hasErrorWithAmount"));
      localQuickDepositStartResponse.b(localJSONObject.optBoolean("hasErrorWitDepositAccount"));
      paramQuickDeposit.a(localJSONObject.optString("transactionId"));
      return localQuickDepositStartResponse;
    }
    catch (ChaseException localChaseException)
    {
      localQuickDepositStartResponse.a(ChaseApplication.a().getApplicationContext());
    }
    return localQuickDepositStartResponse;
  }

  public static QuickDepositVerifyResponse b(QuickDeposit paramQuickDeposit)
  {
    QuickDepositVerifyResponse localQuickDepositVerifyResponse = new QuickDepositVerifyResponse();
    while (true)
    {
      try
      {
        Hashtable localHashtable = c();
        localHashtable.put("transactionId", paramQuickDeposit.b());
        localHashtable.put("submitCounter", Integer.valueOf(paramQuickDeposit.a()).toString());
        localHashtable.put("amount", paramQuickDeposit.i().b().toPlainString());
        String str1 = a(2131165273);
        String[] arrayOfString1 = { "checkImageFront", "checkImageBack" };
        String[] arrayOfString2 = { "image/jpeg", "image/jpeg" };
        byte[][] arrayOfByte = new byte[2][];
        arrayOfByte[0] = paramQuickDeposit.f();
        arrayOfByte[1] = paramQuickDeposit.d();
        JSONObject localJSONObject = m.a(ChaseApplication.a(), str1, localHashtable, arrayOfString1, arrayOfByte, arrayOfString2, ChaseApplication.a().h());
        localQuickDepositVerifyResponse.b(localJSONObject);
        boolean bool1 = localJSONObject.optBoolean("hasErrorWithAccountNumber");
        localQuickDepositVerifyResponse.d(bool1);
        if (!localJSONObject.optBoolean("isAccountNumberEditable"))
        {
          if (bool1)
          {
            break label444;
            localQuickDepositVerifyResponse.g(bool2);
            localQuickDepositVerifyResponse.e(localJSONObject.optBoolean("isAmountEditable"));
            boolean bool3 = localJSONObject.optBoolean("hasErrorWithRoutingNumber");
            localQuickDepositVerifyResponse.c(bool3);
            if (localJSONObject.optBoolean("isRoutingNumberEditable"))
              break label450;
            if (bool3)
            {
              break label450;
              localQuickDepositVerifyResponse.f(bool4);
              localQuickDepositVerifyResponse.a(localJSONObject.optBoolean("hasErrorWithAmount"));
              localQuickDepositVerifyResponse.b(localJSONObject.optBoolean("hasErrorWithCheckImages"));
              localQuickDepositVerifyResponse.a(localJSONObject.optString("itemSequenceNumber"));
              localQuickDepositVerifyResponse.h(localJSONObject.optBoolean("success", false));
              localQuickDepositVerifyResponse.b(localJSONObject.optString("trayContent"));
              paramQuickDeposit.b(localJSONObject.optString("readRoutingNumber"));
              paramQuickDeposit.c(localJSONObject.optString("readAccountNumber"));
              String str2 = localJSONObject.optString("depositAmount");
              if (s.m(str2))
                paramQuickDeposit.a(new Dollar(str2));
              String str3 = localJSONObject.optString("readAmount");
              if (s.m(str3))
                localQuickDepositVerifyResponse.a(new Dollar(str3));
              if (localQuickDepositVerifyResponse.c("20452"))
                paramQuickDeposit.a(1 + paramQuickDeposit.a());
              return localQuickDepositVerifyResponse;
            }
          }
          else
          {
            bool2 = false;
            continue;
          }
          bool4 = false;
          continue;
        }
      }
      catch (ChaseException localChaseException)
      {
        localQuickDepositVerifyResponse.a(ChaseApplication.a().getApplicationContext());
        return localQuickDepositVerifyResponse;
      }
      label444: boolean bool2 = true;
      continue;
      label450: boolean bool4 = true;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.quickdeposit.a
 * JD-Core Version:    0.6.2
 */