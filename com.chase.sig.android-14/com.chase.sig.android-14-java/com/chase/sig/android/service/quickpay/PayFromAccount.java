package com.chase.sig.android.service.quickpay;

import com.chase.sig.android.util.Dollar;
import java.io.Serializable;

public class PayFromAccount
  implements Serializable
{
  private Dollar balance;
  private String displayName;
  private String id;
  private boolean isDDA = false;
  private String maskedAccountNumber;
  private String nickname;
  private boolean requiresCvv;
  private String sourceIndicator;

  public final String a()
  {
    return this.displayName;
  }

  public final void a(Dollar paramDollar)
  {
    this.balance = paramDollar;
  }

  public final void a(String paramString)
  {
    this.displayName = paramString;
  }

  public final void a(boolean paramBoolean)
  {
    this.requiresCvv = paramBoolean;
  }

  public final String b()
  {
    return this.id;
  }

  public final void b(String paramString)
  {
    this.maskedAccountNumber = paramString;
  }

  public final void b(boolean paramBoolean)
  {
    this.isDDA = paramBoolean;
  }

  public final Dollar c()
  {
    return this.balance;
  }

  public final void c(String paramString)
  {
    this.id = paramString;
  }

  public final void d(String paramString)
  {
    this.nickname = paramString;
  }

  public final boolean d()
  {
    return this.requiresCvv;
  }

  public final void e(String paramString)
  {
    this.sourceIndicator = paramString;
  }

  public final boolean e()
  {
    return this.isDDA;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.quickpay.PayFromAccount
 * JD-Core Version:    0.6.2
 */