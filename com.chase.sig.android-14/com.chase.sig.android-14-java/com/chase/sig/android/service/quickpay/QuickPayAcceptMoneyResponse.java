package com.chase.sig.android.service.quickpay;

import com.chase.sig.android.service.l;
import java.io.Serializable;

public class QuickPayAcceptMoneyResponse extends l
  implements Serializable
{
  private String status;

  public final String a()
  {
    return this.status;
  }

  public final void a(String paramString)
  {
    this.status = paramString;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.quickpay.QuickPayAcceptMoneyResponse
 * JD-Core Version:    0.6.2
 */