package com.chase.sig.android.service.quickpay;

import com.chase.sig.android.domain.QuickPayRecipient;
import com.chase.sig.android.service.l;
import java.io.Serializable;

public class QuickPayAddRecipientResponse extends l
  implements Serializable
{
  private QuickPayRecipient recipient;

  public final QuickPayRecipient a()
  {
    return this.recipient;
  }

  public final void a(QuickPayRecipient paramQuickPayRecipient)
  {
    this.recipient = paramQuickPayRecipient;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.quickpay.QuickPayAddRecipientResponse
 * JD-Core Version:    0.6.2
 */