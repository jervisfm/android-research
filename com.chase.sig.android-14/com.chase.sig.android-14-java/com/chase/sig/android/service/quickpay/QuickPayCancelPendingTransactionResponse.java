package com.chase.sig.android.service.quickpay;

import com.chase.sig.android.domain.QuickPayPendingTransaction;
import com.chase.sig.android.service.l;

public class QuickPayCancelPendingTransactionResponse extends l
{
  private QuickPayPendingTransaction pendingTransaction;

  public final QuickPayPendingTransaction a()
  {
    return this.pendingTransaction;
  }

  public final void a(QuickPayPendingTransaction paramQuickPayPendingTransaction)
  {
    this.pendingTransaction = paramQuickPayPendingTransaction;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.quickpay.QuickPayCancelPendingTransactionResponse
 * JD-Core Version:    0.6.2
 */