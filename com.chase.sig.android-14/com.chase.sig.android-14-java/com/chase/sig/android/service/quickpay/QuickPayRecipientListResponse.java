package com.chase.sig.android.service.quickpay;

import com.chase.sig.android.domain.QuickPayRecipient;
import com.chase.sig.android.service.l;
import java.io.Serializable;
import java.util.ArrayList;

public class QuickPayRecipientListResponse extends l
  implements Serializable
{
  private String contentHeader;
  private String contentTitle;
  private ArrayList<QuickPayRecipient> recipients;

  public final ArrayList<QuickPayRecipient> a()
  {
    return this.recipients;
  }

  public final void a(String paramString)
  {
    this.contentTitle = paramString;
  }

  public final void a(ArrayList<QuickPayRecipient> paramArrayList)
  {
    this.recipients = paramArrayList;
  }

  public final void b(String paramString)
  {
    this.contentHeader = paramString;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.quickpay.QuickPayRecipientListResponse
 * JD-Core Version:    0.6.2
 */