package com.chase.sig.android.service.quickpay;

public class QuickPaySendTransactionConfirmResponse extends QuickPaySendTransactionVerifyResponse
{
  private String senderCode;
  private String status;
  private String transactionId;

  public final String a()
  {
    return this.status;
  }

  public final void a(String paramString)
  {
    this.status = paramString;
  }

  public final String b()
  {
    return this.transactionId;
  }

  public final void b(String paramString)
  {
    this.transactionId = paramString;
  }

  public final String c()
  {
    return this.senderCode;
  }

  public final void e(String paramString)
  {
    this.senderCode = paramString;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.quickpay.QuickPaySendTransactionConfirmResponse
 * JD-Core Version:    0.6.2
 */