package com.chase.sig.android.service.quickpay;

import com.chase.sig.android.domain.QuickPayRecipient;
import com.chase.sig.android.service.l;
import com.chase.sig.android.util.s;
import java.io.Serializable;

public class QuickPaySendTransactionVerifyResponse extends l
  implements Serializable
{
  private String accountName;
  private String amount;
  private String dueDate;
  private String duplicateMessage;
  private String feeDisclaimer;
  private String formId;
  private String frequency;
  private String invoiceId;
  private boolean isAtmEligible;
  private boolean isDuplicate;
  private String isNickNameMatched = "";
  private boolean isWarning;
  private String memo;
  private int numberOfPayments;
  private QuickPayRecipient recipient;
  private boolean repeatingPayment;
  private String sendOnDate;
  private boolean smsEligible;
  private String smsReason;
  private boolean unlimitedPayments;

  public final void a(int paramInt)
  {
    this.numberOfPayments = paramInt;
  }

  public final void a(QuickPayRecipient paramQuickPayRecipient)
  {
    this.recipient = paramQuickPayRecipient;
  }

  public final void a(boolean paramBoolean)
  {
    this.isDuplicate = paramBoolean;
  }

  public final void b(boolean paramBoolean)
  {
    this.repeatingPayment = paramBoolean;
  }

  public final void c(boolean paramBoolean)
  {
    this.unlimitedPayments = paramBoolean;
  }

  public final String d()
  {
    return this.formId;
  }

  public final void d(boolean paramBoolean)
  {
    this.isWarning = paramBoolean;
  }

  public final void e(boolean paramBoolean)
  {
    this.smsEligible = paramBoolean;
  }

  public final void f(String paramString)
  {
    this.accountName = paramString;
  }

  public final void g(String paramString)
  {
    this.amount = paramString;
  }

  public final void h(String paramString)
  {
    this.invoiceId = paramString;
  }

  public final void i(String paramString)
  {
    this.sendOnDate = paramString;
  }

  public final void j(String paramString)
  {
    this.memo = paramString;
  }

  public final boolean j()
  {
    return this.isDuplicate;
  }

  public final String k()
  {
    return this.duplicateMessage;
  }

  public final void k(String paramString)
  {
    this.duplicateMessage = paramString;
  }

  public final void l(String paramString)
  {
    this.dueDate = paramString;
  }

  public final boolean l()
  {
    return this.isAtmEligible;
  }

  public final void m()
  {
    this.isAtmEligible = false;
  }

  public final void m(String paramString)
  {
    this.frequency = paramString;
  }

  public final String n()
  {
    return this.feeDisclaimer;
  }

  public final void n(String paramString)
  {
    this.feeDisclaimer = paramString;
  }

  public final QuickPayRecipient o()
  {
    return this.recipient;
  }

  public final void o(String paramString)
  {
    this.formId = paramString;
  }

  public final String p()
  {
    return this.smsReason;
  }

  public final void p(String paramString)
  {
    this.smsReason = paramString;
  }

  public final void q(String paramString)
  {
    this.isNickNameMatched = paramString;
  }

  public final boolean q()
  {
    return (s.m(this.isNickNameMatched)) && (this.isNickNameMatched.equalsIgnoreCase("yes"));
  }

  public final boolean r()
  {
    return this.isWarning;
  }

  public final boolean s()
  {
    return this.smsEligible;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.quickpay.QuickPaySendTransactionVerifyResponse
 * JD-Core Version:    0.6.2
 */