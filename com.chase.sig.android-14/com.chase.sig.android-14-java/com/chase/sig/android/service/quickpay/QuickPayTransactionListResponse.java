package com.chase.sig.android.service.quickpay;

import com.chase.sig.android.domain.QuickPayActivityItem;
import com.chase.sig.android.domain.QuickPayActivityType;
import com.chase.sig.android.domain.QuickPayPendingTransaction;
import com.chase.sig.android.service.l;
import java.io.Serializable;
import java.util.ArrayList;

public class QuickPayTransactionListResponse extends l
  implements Serializable
{
  private ArrayList<QuickPayActivityItem> activities = new ArrayList();
  private String contentEmptyMessage;
  private String contentHeader1;
  private String contentSubtitle;
  private String contentTitle;
  private QuickPayActivityType currentActivityType;
  private int currentPage;
  private int nextPage;
  private int rowsLeftToShow;
  private String showNext;
  private int totalRows;
  private ArrayList<QuickPayPendingTransaction> transactions = new ArrayList();

  public final String a()
  {
    return this.contentSubtitle;
  }

  public final void a(int paramInt)
  {
    this.totalRows = paramInt;
  }

  public final void a(QuickPayActivityType paramQuickPayActivityType)
  {
    this.currentActivityType = paramQuickPayActivityType;
  }

  public final void a(String paramString)
  {
    this.contentTitle = paramString;
  }

  public final void a(ArrayList<QuickPayActivityItem> paramArrayList)
  {
    this.activities = paramArrayList;
  }

  public final String b()
  {
    return this.contentHeader1;
  }

  public final void b(int paramInt)
  {
    this.nextPage = paramInt;
  }

  public final void b(String paramString)
  {
    this.contentSubtitle = paramString;
  }

  public final void b(ArrayList<QuickPayPendingTransaction> paramArrayList)
  {
    this.transactions = paramArrayList;
  }

  public final ArrayList<QuickPayActivityItem> c()
  {
    return this.activities;
  }

  public final void c(int paramInt)
  {
    this.currentPage = paramInt;
  }

  public final String d()
  {
    return this.showNext;
  }

  public final void e(String paramString)
  {
    this.contentHeader1 = paramString;
  }

  public final void f(String paramString)
  {
    this.showNext = paramString;
  }

  public final void g(String paramString)
  {
    this.contentEmptyMessage = paramString;
  }

  public final int j()
  {
    return this.totalRows;
  }

  public final int k()
  {
    return this.nextPage;
  }

  public final int l()
  {
    return this.currentPage;
  }

  public final ArrayList<QuickPayPendingTransaction> m()
  {
    return this.transactions;
  }

  public final String n()
  {
    return this.contentEmptyMessage;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.quickpay.QuickPayTransactionListResponse
 * JD-Core Version:    0.6.2
 */