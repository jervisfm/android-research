package com.chase.sig.android.service.quickpay;

import com.chase.sig.android.service.l;
import java.io.Serializable;
import java.util.ArrayList;

public class QuickPayTransactionStartResponse extends l
  implements Serializable
{
  private String cutoffNotice;
  ArrayList<PayFromAccount> payFromAccounts = new ArrayList();

  public final ArrayList<PayFromAccount> a()
  {
    return this.payFromAccounts;
  }

  public final void a(String paramString)
  {
    this.cutoffNotice = paramString;
  }

  public final void a(ArrayList<PayFromAccount> paramArrayList)
  {
    this.payFromAccounts = paramArrayList;
  }

  public final String b()
  {
    return this.cutoffNotice;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.quickpay.QuickPayTransactionStartResponse
 * JD-Core Version:    0.6.2
 */