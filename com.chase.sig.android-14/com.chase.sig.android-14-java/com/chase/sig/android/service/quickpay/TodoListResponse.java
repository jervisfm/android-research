package com.chase.sig.android.service.quickpay;

import com.chase.sig.android.domain.m;
import com.chase.sig.android.service.TodoItem;
import com.chase.sig.android.service.l;
import com.chase.sig.android.util.s;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TodoListResponse extends l
  implements Serializable
{
  private String accountMessage;
  private boolean canAcceptMoney;
  private boolean canRequestMoney;
  private boolean canSeeAtm;
  private boolean canSendMoney;
  private boolean canViewActivity;
  private boolean canViewTodo;
  private String contentEmptyMessage;
  private String contentHeader;
  private String contentTitle;
  private PayFromAccount defaultQuickPayAccount;
  private String earliestSendOnDate;
  private m frequencyList;
  private String missingPmtMessage;
  private int nextPage;
  private int thisPage;
  private List<TodoItem> todoItems = new ArrayList();
  private int totalItems;

  public final void a(int paramInt)
  {
    this.totalItems = paramInt;
  }

  public final void a(m paramm)
  {
    this.frequencyList = paramm;
  }

  public final void a(PayFromAccount paramPayFromAccount)
  {
    this.defaultQuickPayAccount = paramPayFromAccount;
  }

  public final void a(String paramString)
  {
    this.contentTitle = paramString;
  }

  public final void a(boolean paramBoolean)
  {
    this.canRequestMoney = paramBoolean;
  }

  public final boolean a()
  {
    return this.canSeeAtm;
  }

  public final void b()
  {
    this.canSeeAtm = false;
  }

  public final void b(int paramInt)
  {
    this.nextPage = paramInt;
  }

  public final void b(String paramString)
  {
    this.contentHeader = paramString;
  }

  public final void b(boolean paramBoolean)
  {
    this.canSendMoney = paramBoolean;
  }

  public final List<TodoItem> c()
  {
    return this.todoItems;
  }

  public final void c(int paramInt)
  {
    this.thisPage = paramInt;
  }

  public final void c(boolean paramBoolean)
  {
    this.canAcceptMoney = paramBoolean;
  }

  public final int d()
  {
    return this.totalItems;
  }

  public final void d(boolean paramBoolean)
  {
    this.canViewTodo = paramBoolean;
  }

  public final void e(String paramString)
  {
    this.earliestSendOnDate = paramString;
  }

  public final void e(boolean paramBoolean)
  {
    this.canViewActivity = paramBoolean;
  }

  public final void f(String paramString)
  {
    this.contentEmptyMessage = paramString;
  }

  public final void g(String paramString)
  {
    this.accountMessage = paramString;
  }

  public final void h(String paramString)
  {
    this.missingPmtMessage = paramString;
  }

  public final Date j()
  {
    return s.g(this.earliestSendOnDate);
  }

  public final int k()
  {
    return this.nextPage;
  }

  public final int l()
  {
    return this.thisPage;
  }

  public final String m()
  {
    return this.contentEmptyMessage;
  }

  public final PayFromAccount n()
  {
    return this.defaultQuickPayAccount;
  }

  public final boolean o()
  {
    return this.canRequestMoney;
  }

  public final String p()
  {
    return this.accountMessage;
  }

  public final boolean q()
  {
    return this.canSendMoney;
  }

  public final boolean r()
  {
    return this.canAcceptMoney;
  }

  public final String s()
  {
    return this.missingPmtMessage;
  }

  public final m t()
  {
    return this.frequencyList;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.quickpay.TodoListResponse
 * JD-Core Version:    0.6.2
 */