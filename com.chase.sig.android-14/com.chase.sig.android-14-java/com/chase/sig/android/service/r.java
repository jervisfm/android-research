package com.chase.sig.android.service;

import android.os.Build;
import android.os.Build.VERSION;
import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.domain.a;
import com.chase.sig.android.util.ChaseException;
import com.chase.sig.android.util.f;
import com.chase.sig.android.util.m;
import com.chase.sig.android.util.s;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import org.apache.http.cookie.Cookie;
import org.json.JSONObject;

public final class r extends JPService
{
  private w a;

  public r(w paramw)
  {
    this.a = paramw;
  }

  private static String a()
  {
    new f(ChaseApplication.a());
    List localList = f.b();
    if (localList == null)
      return null;
    Iterator localIterator = localList.iterator();
    while (localIterator.hasNext())
    {
      Cookie localCookie = (Cookie)localIterator.next();
      if (localCookie.getName().startsWith("adtoken"))
        return localCookie.getValue();
    }
    return null;
  }

  private static a b(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6)
  {
    while (true)
    {
      try
      {
        String str1 = ChaseApplication.a().c("auth") + b(2131165192);
        Hashtable localHashtable = c();
        ChaseApplication localChaseApplication = ChaseApplication.a();
        Object[] arrayOfObject = new Object[8];
        arrayOfObject[0] = Build.BRAND;
        arrayOfObject[1] = localChaseApplication.d();
        arrayOfObject[2] = "Android";
        arrayOfObject[3] = Build.PRODUCT;
        arrayOfObject[4] = Build.VERSION.RELEASE;
        arrayOfObject[5] = Build.MODEL;
        arrayOfObject[6] = s.b(localChaseApplication.l());
        arrayOfObject[7] = localChaseApplication.k();
        localHashtable.put("auth_mobile_mis", String.format("DEVMAKE=%s&DEVID=%s&DEVOS=%s&DEVMODELVER=%s&DEVOSVER=%s&DEVMODEL=%s&DEVAPPINSTALL=%s&DEVAPPVER=%s", arrayOfObject));
        localHashtable.put("LOB", "RBGLogon");
        localHashtable.put("Referer", "https://www.chase.com");
        localHashtable.put("auth_contextId", "login");
        localHashtable.put("auth_deviceCookie", "adtoken");
        String str2 = a();
        if (s.m(str2))
          localHashtable.put("auth_deviceId", str2);
        if (s.m(paramString5))
          localHashtable.put("auth_tokencode", paramString5);
        if (s.m(paramString6))
          localHashtable.put("auth_nexttokencode", paramString6);
        localHashtable.put("auth_deviceSignature", "");
        localHashtable.put("auth_externalData", "LOB=RBGLogon");
        if (paramString3 == null)
        {
          str3 = "";
          localHashtable.put("auth_otp", str3);
          if (paramString4 == null)
            paramString4 = "";
          localHashtable.put("auth_otpprefix", paramString4);
          if (paramString3 == null)
          {
            str4 = "";
            localHashtable.put("auth_otpreason", str4);
            localHashtable.put("auth_userId", paramString1.toLowerCase());
            localHashtable.put("auth_passwd", paramString2.toLowerCase());
            localHashtable.put("auth_siteId", ChaseApplication.a().h());
            localHashtable.put("type", "json");
            new f(ChaseApplication.a()).a();
            JSONObject localJSONObject = m.a(ChaseApplication.a(), str1, localHashtable);
            String str5 = localJSONObject.getString("response");
            String str6 = localJSONObject.getString("smtoken");
            boolean bool1 = new Boolean(localJSONObject.getString("newstoken")).booleanValue();
            boolean bool2 = localJSONObject.has("spid");
            String str7 = null;
            if (bool2)
              str7 = localJSONObject.getString("spid");
            return new a(paramString1, str5, str6, bool1, str7, new String[] { paramString5, paramString6 });
          }
          String str4 = "2";
          continue;
        }
      }
      catch (Exception localException)
      {
        throw new ChaseException(ChaseException.b, localException, "Login service failure.");
      }
      String str3 = paramString3;
    }
  }

  // ERROR //
  public final GenericResponse a(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6)
  {
    // Byte code:
    //   0: new 242	com/chase/sig/android/service/GenericResponse
    //   3: dup
    //   4: invokespecial 243	com/chase/sig/android/service/GenericResponse:<init>	()V
    //   7: astore 7
    //   9: aload_1
    //   10: aload_2
    //   11: aload_3
    //   12: aload 4
    //   14: aload 5
    //   16: aload 6
    //   18: invokestatic 245	com/chase/sig/android/service/r:b	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/domain/a;
    //   21: astore 16
    //   23: aload 16
    //   25: astore 9
    //   27: aload 9
    //   29: getfield 247	com/chase/sig/android/domain/a:a	Ljava/lang/String;
    //   32: ldc 249
    //   34: invokevirtual 253	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   37: ifeq +82 -> 119
    //   40: aload 7
    //   42: new 255	com/chase/sig/android/service/ServiceError
    //   45: dup
    //   46: invokestatic 21	com/chase/sig/android/ChaseApplication:a	()Lcom/chase/sig/android/ChaseApplication;
    //   49: invokevirtual 259	com/chase/sig/android/ChaseApplication:getResources	()Landroid/content/res/Resources;
    //   52: ldc_w 260
    //   55: invokevirtual 264	android/content/res/Resources:getString	(I)Ljava/lang/String;
    //   58: iconst_1
    //   59: invokespecial 267	com/chase/sig/android/service/ServiceError:<init>	(Ljava/lang/String;Z)V
    //   62: invokevirtual 270	com/chase/sig/android/service/GenericResponse:a	(Lcom/chase/sig/android/service/IServiceError;)V
    //   65: aconst_null
    //   66: astore 12
    //   68: aload 9
    //   70: ifnull +134 -> 204
    //   73: aload 9
    //   75: invokevirtual 272	com/chase/sig/android/domain/a:b	()Z
    //   78: istore 13
    //   80: aconst_null
    //   81: astore 12
    //   83: iload 13
    //   85: ifeq +119 -> 204
    //   88: aload_0
    //   89: getfield 13	com/chase/sig/android/service/r:a	Lcom/chase/sig/android/service/w;
    //   92: pop
    //   93: invokestatic 277	com/chase/sig/android/service/w:a	()Lcom/chase/sig/android/service/ProfileResponse;
    //   96: astore 15
    //   98: aload 15
    //   100: invokevirtual 282	com/chase/sig/android/service/ProfileResponse:e	()Z
    //   103: ifeq +57 -> 160
    //   106: aload 7
    //   108: aload 15
    //   110: invokevirtual 285	com/chase/sig/android/service/ProfileResponse:g	()Ljava/util/List;
    //   113: invokevirtual 288	com/chase/sig/android/service/GenericResponse:b	(Ljava/util/List;)V
    //   116: aload 7
    //   118: areturn
    //   119: aload 7
    //   121: invokevirtual 289	com/chase/sig/android/service/GenericResponse:e	()Z
    //   124: istore 17
    //   126: iload 17
    //   128: ifeq -63 -> 65
    //   131: aload 7
    //   133: areturn
    //   134: astore 8
    //   136: aconst_null
    //   137: astore 9
    //   139: aload 8
    //   141: invokevirtual 290	com/chase/sig/android/util/ChaseException:a	()Ljava/lang/String;
    //   144: pop
    //   145: aload 7
    //   147: invokestatic 21	com/chase/sig/android/ChaseApplication:a	()Lcom/chase/sig/android/ChaseApplication;
    //   150: invokevirtual 294	com/chase/sig/android/ChaseApplication:getApplicationContext	()Landroid/content/Context;
    //   153: invokevirtual 297	com/chase/sig/android/service/GenericResponse:a	(Landroid/content/Context;)Lcom/chase/sig/android/service/l;
    //   156: pop
    //   157: goto -92 -> 65
    //   160: aload 15
    //   162: invokevirtual 300	com/chase/sig/android/service/ProfileResponse:a	()Lcom/chase/sig/android/domain/g;
    //   165: astore 12
    //   167: aload 12
    //   169: invokeinterface 305 1 0
    //   174: invokeinterface 309 1 0
    //   179: ifgt +25 -> 204
    //   182: aload 7
    //   184: new 255	com/chase/sig/android/service/ServiceError
    //   187: dup
    //   188: invokestatic 21	com/chase/sig/android/ChaseApplication:a	()Lcom/chase/sig/android/ChaseApplication;
    //   191: ldc_w 310
    //   194: invokevirtual 311	com/chase/sig/android/ChaseApplication:getString	(I)Ljava/lang/String;
    //   197: iconst_1
    //   198: invokespecial 267	com/chase/sig/android/service/ServiceError:<init>	(Ljava/lang/String;Z)V
    //   201: invokevirtual 270	com/chase/sig/android/service/GenericResponse:a	(Lcom/chase/sig/android/service/IServiceError;)V
    //   204: invokestatic 21	com/chase/sig/android/ChaseApplication:a	()Lcom/chase/sig/android/ChaseApplication;
    //   207: new 313	com/chase/sig/android/domain/o
    //   210: dup
    //   211: aload 9
    //   213: aload 12
    //   215: invokespecial 316	com/chase/sig/android/domain/o:<init>	(Lcom/chase/sig/android/domain/a;Lcom/chase/sig/android/domain/g;)V
    //   218: invokevirtual 319	com/chase/sig/android/ChaseApplication:a	(Lcom/chase/sig/android/domain/o;)V
    //   221: aload 7
    //   223: areturn
    //   224: astore 8
    //   226: goto -87 -> 139
    //
    // Exception table:
    //   from	to	target	type
    //   9	23	134	com/chase/sig/android/util/ChaseException
    //   27	65	224	com/chase/sig/android/util/ChaseException
    //   119	126	224	com/chase/sig/android/util/ChaseException
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.r
 * JD-Core Version:    0.6.2
 */