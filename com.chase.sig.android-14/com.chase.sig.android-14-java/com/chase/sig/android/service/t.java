package com.chase.sig.android.service;

import android.content.Context;
import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.domain.OneTimePasswordContact;
import com.chase.sig.android.domain.a;
import com.chase.sig.android.domain.o;
import com.chase.sig.android.util.ChaseException;
import com.chase.sig.android.util.m;
import com.chase.sig.android.util.s;
import java.util.ArrayList;
import java.util.Hashtable;
import org.json.JSONArray;
import org.json.JSONObject;

public final class t extends JPService
{
  public static GenericResponse a(o paramo, String paramString1, String paramString2)
  {
    String str1 = a(2131165195);
    Hashtable localHashtable = new Hashtable();
    localHashtable.put("applId", "GATEWAY");
    localHashtable.put("reason", "2");
    String str2;
    if (paramString1.equals("EMAIL"))
      str2 = "T";
    GenericResponse localGenericResponse;
    while (true)
    {
      localHashtable.put("method", str2);
      localHashtable.put("userId", paramo.a.c);
      localHashtable.put("spid", paramo.a.b);
      localHashtable.put("type", "json");
      localHashtable.put("version", "1");
      localHashtable.put("contactId", paramString2);
      localGenericResponse = new GenericResponse();
      try
      {
        Context localContext = ChaseApplication.a().getApplicationContext();
        JSONObject localJSONObject = m.a(ChaseApplication.a(), str1, localHashtable);
        String str3 = localJSONObject.getString("prefix");
        new Object[] { str3 };
        paramo.a.d = str3;
        localGenericResponse.b(localJSONObject);
        if (s.l(str3))
          localGenericResponse.b(localContext);
        return localGenericResponse;
        if (paramString1.equals("PHONE"))
          str2 = "V";
        else if (paramString1.equals("TEXT"))
          str2 = "S";
        else
          str2 = null;
      }
      catch (Exception localException)
      {
        localGenericResponse.a(ChaseApplication.a().getApplicationContext());
      }
    }
    return localGenericResponse;
  }

  public static OneTimePasswordContactsResponse a(o paramo)
  {
    String str1 = a(2131165194);
    Hashtable localHashtable = c();
    localHashtable.put("spid", paramo.a.b);
    localHashtable.put("reason", "2");
    try
    {
      JSONObject localJSONObject1 = m.a(ChaseApplication.a(), str1, localHashtable);
      OneTimePasswordContactsResponse localOneTimePasswordContactsResponse = new OneTimePasswordContactsResponse();
      String str2 = localJSONObject1.getString("prefix");
      localOneTimePasswordContactsResponse.b(localJSONObject1);
      paramo.a.d = str2;
      JSONArray localJSONArray = localJSONObject1.optJSONArray("contacts");
      ArrayList localArrayList = new ArrayList();
      if (localJSONArray != null)
      {
        for (int i = 0; i < localJSONArray.length(); i++)
        {
          JSONObject localJSONObject2 = localJSONArray.getJSONObject(i);
          localArrayList.add(new OneTimePasswordContact(localJSONObject2.getString("id"), localJSONObject2.getString("mask"), localJSONObject2.getString("type")));
        }
        localOneTimePasswordContactsResponse.a(localArrayList);
      }
      return localOneTimePasswordContactsResponse;
    }
    catch (Exception localException)
    {
      throw new ChaseException(ChaseException.b, localException, "Failed to communicate with auth GWS.");
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.t
 * JD-Core Version:    0.6.2
 */