package com.chase.sig.android.service.transfer;

import com.chase.sig.android.service.l;
import java.io.Serializable;

public class TransferAddResponse extends l
  implements Serializable
{
  private String dueDate;
  private String paymentId;
  private String processDate;
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.transfer.TransferAddResponse
 * JD-Core Version:    0.6.2
 */