package com.chase.sig.android.service;

import java.util.Hashtable;

public final class u extends JPService
{
  private String a;
  private Hashtable<String, String> b;

  public u(String paramString, Hashtable<String, String> paramHashtable)
  {
    this.a = paramString;
    this.b = paramHashtable;
  }

  public final void a()
  {
    try
    {
      String str = a(b(this.a), this.b);
      new StringBuilder("This is the response from the Ping Service...  ").append(str).toString();
      return;
    }
    catch (Exception localException)
    {
      new StringBuilder("Error has occurred in the Ping Service...  \n").append(localException.getMessage()).toString();
      localException.printStackTrace();
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.u
 * JD-Core Version:    0.6.2
 */