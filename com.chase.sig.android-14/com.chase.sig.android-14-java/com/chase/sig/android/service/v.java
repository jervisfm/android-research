package com.chase.sig.android.service;

import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.domain.Position;
import com.chase.sig.android.util.Dollar;
import com.chase.sig.android.util.m;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class v extends JPService
{
  public static PositionResponse a(String paramString)
  {
    int i = 0;
    String str = a(2131165223);
    PositionResponse localPositionResponse = new PositionResponse();
    try
    {
      Hashtable localHashtable = c();
      localHashtable.put("accountId", paramString);
      JSONObject localJSONObject1 = m.a(ChaseApplication.a(), str, localHashtable);
      JSONArray localJSONArray1 = localJSONObject1.optJSONArray("accounts");
      localPositionResponse.b(localJSONObject1);
      if ((localJSONArray1 != null) && (!localJSONArray1.isNull(0)))
      {
        JSONObject localJSONObject2 = localJSONArray1.getJSONObject(0);
        JSONArray localJSONArray2 = localJSONObject2.getJSONArray("positions");
        PositionsAccount localPositionsAccount = new PositionsAccount();
        localPositionsAccount.c(localJSONObject2.getString("accountUnrealizedChange"));
        localPositionsAccount.d(localJSONObject2.getString("accountValue"));
        localPositionsAccount.b(localJSONObject2.getString("asOfDate"));
        localPositionsAccount.a(localJSONObject2.getString("displayName"));
        if (!localJSONObject2.isNull("marketValue"))
          localPositionsAccount.a(a(localJSONObject2, "marketValue"));
        if (!localJSONObject2.isNull("marketValueChange"))
          localPositionsAccount.b(a(localJSONObject2, "marketValueChange"));
        ArrayList localArrayList1 = new ArrayList();
        while (i < localJSONArray2.length())
        {
          JSONObject localJSONObject3 = localJSONArray2.getJSONObject(i);
          Position localPosition = new Position();
          localPosition.f(localJSONObject3.optString("assetClass1"));
          localPosition.g(localJSONObject3.optString("assetClass2"));
          localPosition.h(localJSONObject3.getString("priceAsOfDate"));
          localPosition.b(a(localJSONObject3, "price", "value"));
          localPosition.a(localJSONObject3.getString("shortName"));
          localPosition.c(a(localJSONObject3, "unrealizedChange", "value"));
          localPosition.a(a(localJSONObject3, "value", "value"));
          localPosition.e(a(localJSONObject3, "value", "change"));
          localPosition.d(a(localJSONObject3, "cost", "value"));
          localPosition.b(localJSONObject3.getString("quantity"));
          localPosition.d(localJSONObject3.optString("tickerSymbol"));
          localPosition.e(localJSONObject3.optString("cusip"));
          localPosition.c(localJSONObject3.getString("description"));
          if (localJSONObject3.has("quoteCode"))
            localPosition.i(localJSONObject3.optString("quoteCode"));
          localPosition.g(localJSONObject3.getBoolean("isShowShortName"));
          localPosition.e(localJSONObject3.getBoolean("isShowPriceDate"));
          localPosition.d(localJSONObject3.getBoolean("isShowPrice"));
          localPosition.b(localJSONObject3.getBoolean("isShowCost"));
          localPosition.f(localJSONObject3.getBoolean("isShowQuantity"));
          localPosition.j(localJSONObject3.getBoolean("isShowValue"));
          localPosition.h(localJSONObject3.getBoolean("isShowSymbol"));
          localPosition.c(localJSONObject3.getBoolean("isShowDescription"));
          localPosition.i(localJSONObject3.getBoolean("isShowUnrealizedChange"));
          localPosition.a(localJSONObject3.getBoolean("isShowAssetClass1"));
          localArrayList1.add(localPosition);
          i++;
        }
        localPositionsAccount.a(localArrayList1);
        ArrayList localArrayList2 = new ArrayList();
        localArrayList2.add(localPositionsAccount);
        localPositionResponse.a(localArrayList2);
      }
      return localPositionResponse;
    }
    catch (JSONException localJSONException)
    {
      localPositionResponse.a(ChaseApplication.a().getApplicationContext());
      return localPositionResponse;
    }
    catch (Exception localException)
    {
      localPositionResponse.a(ChaseApplication.a().getApplicationContext());
    }
    return localPositionResponse;
  }

  private static Dollar a(JSONObject paramJSONObject, String paramString)
  {
    String str = paramJSONObject.getString(paramString);
    if (str == null)
      str = "";
    return new Dollar(str);
  }

  private static Dollar a(JSONObject paramJSONObject, String paramString1, String paramString2)
  {
    JSONObject localJSONObject = paramJSONObject.optJSONObject(paramString1);
    if (localJSONObject == null);
    for (String str = ""; ; str = localJSONObject.getString(paramString2))
      return new Dollar(str);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.v
 * JD-Core Version:    0.6.2
 */