package com.chase.sig.android.service.wire;

import com.chase.sig.android.domain.Payee;
import com.chase.sig.android.service.l;
import java.util.List;

public class WireGetPayeesResponse extends l
{
  private List<Payee> payees;

  public final List<Payee> a()
  {
    return this.payees;
  }

  public final void a(List<Payee> paramList)
  {
    this.payees = paramList;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.wire.WireGetPayeesResponse
 * JD-Core Version:    0.6.2
 */