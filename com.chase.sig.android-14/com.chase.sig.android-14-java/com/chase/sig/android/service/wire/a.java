package com.chase.sig.android.service.wire;

import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.domain.Payee;
import com.chase.sig.android.domain.WireTransaction;
import com.chase.sig.android.service.movemoney.d;
import com.chase.sig.android.service.movemoney.e;
import com.chase.sig.android.service.movemoney.g;
import com.chase.sig.android.util.m;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public final class a extends d<WireTransaction>
{
  public a(g paramg, e parame, b paramb)
  {
    super(paramg, parame, paramb);
  }

  public static WireGetPayeesResponse a()
  {
    WireGetPayeesResponse localWireGetPayeesResponse = new WireGetPayeesResponse();
    try
    {
      String str = a(2131165266);
      JSONObject localJSONObject1 = m.a(ChaseApplication.a(), str, c());
      ArrayList localArrayList = new ArrayList();
      localWireGetPayeesResponse.b(localJSONObject1);
      JSONArray localJSONArray = localJSONObject1.optJSONArray("payees");
      if ((localJSONArray != null) && (localJSONArray.length() > 0))
        for (int i = 0; i < localJSONArray.length(); i++)
        {
          JSONObject localJSONObject2 = localJSONArray.getJSONObject(i);
          Payee localPayee = new Payee();
          localPayee.a(localJSONObject2.optString("accountNumber"));
          localPayee.b(localJSONObject2.optString("bankInstructions"));
          localPayee.c(localJSONObject2.optString("earliestPaymentDeliveryDate"));
          localPayee.d(localJSONObject2.optString("earliestPaymentSendDate"));
          localPayee.e(localJSONObject2.optString("memo"));
          localPayee.f(localJSONObject2.optString("nickName"));
          localPayee.h(localJSONObject2.optString("payeeId"));
          localPayee.i(localJSONObject2.optString("payeeInstructions"));
          localArrayList.add(localPayee);
        }
      localWireGetPayeesResponse.a(localArrayList);
      return localWireGetPayeesResponse;
    }
    catch (Exception localException)
    {
      localWireGetPayeesResponse.a(ChaseApplication.a().getApplicationContext());
    }
    return localWireGetPayeesResponse;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.wire.a
 * JD-Core Version:    0.6.2
 */