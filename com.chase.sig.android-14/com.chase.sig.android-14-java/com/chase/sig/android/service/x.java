package com.chase.sig.android.service;

import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.domain.Email;
import com.chase.sig.android.domain.MobilePhoneNumber;
import com.chase.sig.android.domain.QuickPayRecipient;
import com.chase.sig.android.service.quickpay.QuickPayAddRecipientResponse;
import com.chase.sig.android.service.quickpay.QuickPayDeleteRecipientResponse;
import com.chase.sig.android.util.m;
import com.chase.sig.android.util.s;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public final class x extends JPService
{
  public static QuickPayAddRecipientResponse a(QuickPayRecipient paramQuickPayRecipient)
  {
    QuickPayAddRecipientResponse localQuickPayAddRecipientResponse = new QuickPayAddRecipientResponse();
    String str1 = a(2131165205);
    if ("0".equalsIgnoreCase(paramQuickPayRecipient.b()));
    for (String str2 = a(2131165204); ; str2 = str1)
    {
      JSONObject localJSONObject3;
      do
      {
        Hashtable localHashtable;
        JSONObject localJSONObject1;
        JSONArray localJSONArray;
        while (true)
        {
          JSONObject localJSONObject5;
          try
          {
            localHashtable = c();
            localJSONObject1 = new JSONObject();
            localJSONArray = new JSONArray();
            if (s.o(paramQuickPayRecipient.g()))
            {
              JSONObject localJSONObject2 = new JSONObject();
              localJSONObject2.put("contact", paramQuickPayRecipient.g());
              if (s.o(paramQuickPayRecipient.i().c()))
                localJSONObject2.put("payeeContactId", paramQuickPayRecipient.i().c());
              localJSONObject2.put("type", "PHONE");
              localJSONObject2.put("label", "SMS");
              localJSONArray.put(localJSONObject2);
            }
            Iterator localIterator = paramQuickPayRecipient.e().iterator();
            if (!localIterator.hasNext())
              break;
            Email localEmail = (Email)localIterator.next();
            localJSONObject5 = new JSONObject();
            localJSONObject5.put("contact", localEmail.a());
            localJSONObject5.put("payeeContactId", localEmail.c());
            localJSONObject5.put("type", "EMAIL");
            if (localEmail.b())
            {
              localJSONObject5.put("label", "PRIMARY");
              localJSONArray.put(localJSONObject5);
              continue;
            }
          }
          catch (Exception localException)
          {
            localQuickPayAddRecipientResponse.a(ChaseApplication.a().getApplicationContext());
            return localQuickPayAddRecipientResponse;
          }
          localJSONObject5.put("label", "ADDITIONAL");
        }
        localJSONObject1.put("id", paramQuickPayRecipient.b());
        localJSONObject1.put("name", paramQuickPayRecipient.a());
        localJSONObject1.put("token", paramQuickPayRecipient.c());
        localJSONObject1.put("contacts", localJSONArray);
        localHashtable.put("payload", localJSONObject1.toString());
        localJSONObject3 = m.a(ChaseApplication.a(), str2, localHashtable);
        localQuickPayAddRecipientResponse.b(localJSONObject3);
      }
      while (localQuickPayAddRecipientResponse.e());
      JSONObject localJSONObject4 = localJSONObject3.optJSONObject("recipient");
      new y();
      localQuickPayAddRecipientResponse.a(y.a(localJSONObject4));
      return localQuickPayAddRecipientResponse;
    }
  }

  public static QuickPayDeleteRecipientResponse b(QuickPayRecipient paramQuickPayRecipient)
  {
    QuickPayDeleteRecipientResponse localQuickPayDeleteRecipientResponse = new QuickPayDeleteRecipientResponse();
    try
    {
      Hashtable localHashtable = c();
      localHashtable.put("recipientId", paramQuickPayRecipient.b());
      localHashtable.put("token", paramQuickPayRecipient.c());
      String str = a(2131165206);
      localQuickPayDeleteRecipientResponse.b(m.a(ChaseApplication.a(), str, localHashtable));
      return localQuickPayDeleteRecipientResponse;
    }
    catch (Exception localException)
    {
      localQuickPayDeleteRecipientResponse.a(ChaseApplication.a().getApplicationContext());
    }
    return localQuickPayDeleteRecipientResponse;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.x
 * JD-Core Version:    0.6.2
 */