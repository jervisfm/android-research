package com.chase.sig.android.service;

import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.domain.LabeledValue;
import com.chase.sig.android.domain.QuickPayAcceptMoneyTransaction;
import com.chase.sig.android.domain.QuickPayActivityItem;
import com.chase.sig.android.domain.QuickPayActivityType;
import com.chase.sig.android.domain.QuickPayDeclineTransaction;
import com.chase.sig.android.domain.QuickPayPendingTransaction;
import com.chase.sig.android.domain.QuickPayRecipient;
import com.chase.sig.android.domain.QuickPayTransaction;
import com.chase.sig.android.service.quickpay.PayFromAccount;
import com.chase.sig.android.service.quickpay.QuickPayAcceptMoneyResponse;
import com.chase.sig.android.service.quickpay.QuickPayCancelPendingTransactionResponse;
import com.chase.sig.android.service.quickpay.QuickPayDeclineResponse;
import com.chase.sig.android.service.quickpay.QuickPayRecipientListResponse;
import com.chase.sig.android.service.quickpay.QuickPaySendTransactionConfirmResponse;
import com.chase.sig.android.service.quickpay.QuickPaySendTransactionVerifyResponse;
import com.chase.sig.android.service.quickpay.QuickPayTransactionListResponse;
import com.chase.sig.android.service.quickpay.QuickPayTransactionStartResponse;
import com.chase.sig.android.service.quickpay.TodoListResponse;
import com.chase.sig.android.util.ChaseException;
import com.chase.sig.android.util.Dollar;
import com.chase.sig.android.util.n;
import com.chase.sig.android.util.s;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class y extends JPService
{
  public static QuickPayActivityItem a(QuickPayActivityItem paramQuickPayActivityItem)
  {
    try
    {
      Hashtable localHashtable = c();
      localHashtable.put("TransactionId", paramQuickPayActivityItem.g());
      String str1 = ChaseApplication.a().c("mbb");
      String str2 = str1 + JPService.b(2131165217);
      QuickPayActivityItem localQuickPayActivityItem = g(com.chase.sig.android.util.m.a(ChaseApplication.a(), str2, localHashtable));
      return localQuickPayActivityItem;
    }
    catch (ChaseException localChaseException)
    {
    }
    return null;
  }

  // ERROR //
  public static QuickPayRecipient a(JSONObject paramJSONObject)
  {
    // Byte code:
    //   0: new 68	com/chase/sig/android/domain/QuickPayRecipient
    //   3: dup
    //   4: invokespecial 69	com/chase/sig/android/domain/QuickPayRecipient:<init>	()V
    //   7: astore_1
    //   8: aload_1
    //   9: aload_0
    //   10: ldc 71
    //   12: invokevirtual 76	org/json/JSONObject:optString	(Ljava/lang/String;)Ljava/lang/String;
    //   15: invokevirtual 79	com/chase/sig/android/domain/QuickPayRecipient:a	(Ljava/lang/String;)V
    //   18: aload_1
    //   19: aload_0
    //   20: ldc 81
    //   22: invokevirtual 76	org/json/JSONObject:optString	(Ljava/lang/String;)Ljava/lang/String;
    //   25: invokevirtual 83	com/chase/sig/android/domain/QuickPayRecipient:b	(Ljava/lang/String;)V
    //   28: aload_1
    //   29: aload_0
    //   30: ldc 85
    //   32: invokevirtual 76	org/json/JSONObject:optString	(Ljava/lang/String;)Ljava/lang/String;
    //   35: invokevirtual 87	com/chase/sig/android/domain/QuickPayRecipient:c	(Ljava/lang/String;)V
    //   38: aload_1
    //   39: aload_0
    //   40: ldc 89
    //   42: invokevirtual 76	org/json/JSONObject:optString	(Ljava/lang/String;)Ljava/lang/String;
    //   45: invokevirtual 92	com/chase/sig/android/domain/QuickPayRecipient:f	(Ljava/lang/String;)V
    //   48: aload_1
    //   49: aload_0
    //   50: ldc 94
    //   52: invokevirtual 76	org/json/JSONObject:optString	(Ljava/lang/String;)Ljava/lang/String;
    //   55: invokevirtual 96	com/chase/sig/android/domain/QuickPayRecipient:g	(Ljava/lang/String;)V
    //   58: aload_0
    //   59: ldc 98
    //   61: invokevirtual 102	org/json/JSONObject:getJSONArray	(Ljava/lang/String;)Lorg/json/JSONArray;
    //   64: astore 10
    //   66: aload 10
    //   68: astore_3
    //   69: iconst_0
    //   70: istore 4
    //   72: iload 4
    //   74: aload_3
    //   75: invokevirtual 108	org/json/JSONArray:length	()I
    //   78: if_icmpge +16 -> 94
    //   81: aload_3
    //   82: iload 4
    //   84: invokevirtual 112	org/json/JSONArray:isNull	(I)Z
    //   87: istore 8
    //   89: iload 8
    //   91: ifeq +17 -> 108
    //   94: aload_1
    //   95: areturn
    //   96: astore_2
    //   97: new 104	org/json/JSONArray
    //   100: dup
    //   101: invokespecial 113	org/json/JSONArray:<init>	()V
    //   104: astore_3
    //   105: goto -36 -> 69
    //   108: aload_3
    //   109: iload 4
    //   111: invokevirtual 117	org/json/JSONArray:getJSONObject	(I)Lorg/json/JSONObject;
    //   114: astore 9
    //   116: aload 9
    //   118: astore 6
    //   120: ldc 119
    //   122: aload 6
    //   124: ldc 121
    //   126: invokevirtual 76	org/json/JSONObject:optString	(Ljava/lang/String;)Ljava/lang/String;
    //   129: invokevirtual 127	java/lang/String:equalsIgnoreCase	(Ljava/lang/String;)Z
    //   132: ifeq +55 -> 187
    //   135: aload_1
    //   136: new 129	com/chase/sig/android/domain/MobilePhoneNumber
    //   139: dup
    //   140: aload 6
    //   142: ldc 131
    //   144: invokevirtual 76	org/json/JSONObject:optString	(Ljava/lang/String;)Ljava/lang/String;
    //   147: aload 6
    //   149: ldc 81
    //   151: invokevirtual 76	org/json/JSONObject:optString	(Ljava/lang/String;)Ljava/lang/String;
    //   154: aload 6
    //   156: ldc 133
    //   158: invokevirtual 76	org/json/JSONObject:optString	(Ljava/lang/String;)Ljava/lang/String;
    //   161: invokespecial 136	com/chase/sig/android/domain/MobilePhoneNumber:<init>	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    //   164: invokevirtual 139	com/chase/sig/android/domain/QuickPayRecipient:a	(Lcom/chase/sig/android/domain/MobilePhoneNumber;)V
    //   167: iinc 4 1
    //   170: goto -98 -> 72
    //   173: astore 5
    //   175: new 73	org/json/JSONObject
    //   178: dup
    //   179: invokespecial 140	org/json/JSONObject:<init>	()V
    //   182: astore 6
    //   184: goto -64 -> 120
    //   187: ldc 142
    //   189: aload 6
    //   191: ldc 133
    //   193: invokevirtual 76	org/json/JSONObject:optString	(Ljava/lang/String;)Ljava/lang/String;
    //   196: invokevirtual 127	java/lang/String:equalsIgnoreCase	(Ljava/lang/String;)Z
    //   199: invokestatic 148	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   202: astore 7
    //   204: aload_1
    //   205: new 150	com/chase/sig/android/domain/Email
    //   208: dup
    //   209: aload 6
    //   211: ldc 131
    //   213: invokevirtual 76	org/json/JSONObject:optString	(Ljava/lang/String;)Ljava/lang/String;
    //   216: aload 7
    //   218: invokevirtual 154	java/lang/Boolean:booleanValue	()Z
    //   221: aload 6
    //   223: ldc 81
    //   225: invokevirtual 76	org/json/JSONObject:optString	(Ljava/lang/String;)Ljava/lang/String;
    //   228: invokespecial 157	com/chase/sig/android/domain/Email:<init>	(Ljava/lang/String;ZLjava/lang/String;)V
    //   231: invokevirtual 160	com/chase/sig/android/domain/QuickPayRecipient:a	(Lcom/chase/sig/android/domain/Email;)V
    //   234: goto -67 -> 167
    //
    // Exception table:
    //   from	to	target	type
    //   58	66	96	org/json/JSONException
    //   81	89	173	org/json/JSONException
    //   108	116	173	org/json/JSONException
  }

  public static QuickPayAcceptMoneyResponse a(QuickPayAcceptMoneyTransaction paramQuickPayAcceptMoneyTransaction)
  {
    QuickPayAcceptMoneyResponse localQuickPayAcceptMoneyResponse1 = new QuickPayAcceptMoneyResponse();
    String str = a(2131165211);
    Hashtable localHashtable = c();
    localHashtable.put("transactionId", paramQuickPayAcceptMoneyTransaction.w());
    localHashtable.put("token", paramQuickPayAcceptMoneyTransaction.J());
    try
    {
      JSONObject localJSONObject = com.chase.sig.android.util.m.a(ChaseApplication.a(), str, localHashtable);
      QuickPayAcceptMoneyResponse localQuickPayAcceptMoneyResponse2 = new QuickPayAcceptMoneyResponse();
      localQuickPayAcceptMoneyResponse2.a(localJSONObject.optString("status"));
      localQuickPayAcceptMoneyResponse2.a(localJSONObject);
      return localQuickPayAcceptMoneyResponse2;
    }
    catch (ChaseException localChaseException)
    {
      localQuickPayAcceptMoneyResponse1.a(ChaseApplication.a().getApplicationContext());
    }
    return localQuickPayAcceptMoneyResponse1;
  }

  public static QuickPayCancelPendingTransactionResponse a(QuickPayTransaction paramQuickPayTransaction, Boolean paramBoolean)
  {
    QuickPayCancelPendingTransactionResponse localQuickPayCancelPendingTransactionResponse1 = new QuickPayCancelPendingTransactionResponse();
    while (true)
    {
      QuickPayCancelPendingTransactionResponse localQuickPayCancelPendingTransactionResponse2;
      try
      {
        Hashtable localHashtable = c();
        if (!paramQuickPayTransaction.D())
          break label243;
        str1 = "true";
        localHashtable.put("isRepeatingPayment", str1);
        if (paramQuickPayTransaction.D())
        {
          String str4;
          if (paramBoolean.booleanValue())
          {
            str4 = paramQuickPayTransaction.w();
            localHashtable.put("transactionId", str4);
            if (paramBoolean.booleanValue())
            {
              str5 = paramQuickPayTransaction.J();
              localHashtable.put("token", str5);
              if (!paramBoolean.booleanValue())
                break label233;
              str2 = "false";
              localHashtable.put("cancelEntireSeries", str2);
              String str3 = a(2131165216);
              JSONObject localJSONObject = com.chase.sig.android.util.m.a(ChaseApplication.a(), str3, localHashtable);
              localQuickPayCancelPendingTransactionResponse2 = new QuickPayCancelPendingTransactionResponse();
              localQuickPayCancelPendingTransactionResponse2.b(localJSONObject.optJSONObject("paymentDetails"));
              if (localQuickPayCancelPendingTransactionResponse2.e())
                break label240;
              localQuickPayCancelPendingTransactionResponse2.a(f(localJSONObject.getJSONObject("paymentDetails")));
              return localQuickPayCancelPendingTransactionResponse2;
            }
          }
          else
          {
            str4 = paramQuickPayTransaction.L();
            continue;
          }
          String str5 = paramQuickPayTransaction.K();
          continue;
        }
        else
        {
          localHashtable.put("transactionId", paramQuickPayTransaction.w());
          localHashtable.put("token", paramQuickPayTransaction.J());
          continue;
        }
      }
      catch (Exception localException)
      {
        localQuickPayCancelPendingTransactionResponse1.a(ChaseApplication.a().getApplicationContext());
        return localQuickPayCancelPendingTransactionResponse1;
      }
      label233: String str2 = "true";
      continue;
      label240: return localQuickPayCancelPendingTransactionResponse2;
      label243: String str1 = "false";
    }
  }

  public static QuickPayDeclineResponse a(QuickPayDeclineTransaction paramQuickPayDeclineTransaction)
  {
    QuickPayDeclineResponse localQuickPayDeclineResponse1 = new QuickPayDeclineResponse();
    String str1 = a(2131165210);
    Hashtable localHashtable = c();
    localHashtable.put("transactionId", paramQuickPayDeclineTransaction.w());
    localHashtable.put("activityType", paramQuickPayDeclineTransaction.I());
    localHashtable.put("declineReason", paramQuickPayDeclineTransaction.m());
    String str2;
    if (paramQuickPayDeclineTransaction.S())
      str2 = "true";
    while (true)
    {
      localHashtable.put("isInvoiceRequest", str2);
      localHashtable.put("token", paramQuickPayDeclineTransaction.J());
      try
      {
        JSONObject localJSONObject = com.chase.sig.android.util.m.a(ChaseApplication.a(), str1, localHashtable);
        QuickPayDeclineResponse localQuickPayDeclineResponse2 = new QuickPayDeclineResponse();
        localQuickPayDeclineResponse2.a(localJSONObject.optString("status").equals("Success"));
        localQuickPayDeclineResponse2.a(localJSONObject);
        return localQuickPayDeclineResponse2;
        str2 = "false";
      }
      catch (ChaseException localChaseException)
      {
        localQuickPayDeclineResponse1.a(ChaseApplication.a().getApplicationContext());
      }
    }
    return localQuickPayDeclineResponse1;
  }

  public static QuickPayRecipientListResponse a()
  {
    QuickPayRecipientListResponse localQuickPayRecipientListResponse1 = new QuickPayRecipientListResponse();
    try
    {
      String str = a(2131165218);
      Hashtable localHashtable = c();
      localHashtable.put("pageNumber", "1");
      JSONObject localJSONObject = com.chase.sig.android.util.m.a(ChaseApplication.a(), str, localHashtable);
      QuickPayRecipientListResponse localQuickPayRecipientListResponse2 = new QuickPayRecipientListResponse();
      localQuickPayRecipientListResponse2.b(localJSONObject);
      if (!localQuickPayRecipientListResponse2.e())
      {
        JSONArray localJSONArray = localJSONObject.getJSONArray("recipients");
        ArrayList localArrayList = new ArrayList();
        for (int i = 0; i < localJSONArray.length(); i++)
          localArrayList.add(a(localJSONArray.getJSONObject(i)));
        localQuickPayRecipientListResponse2.b(localJSONObject.optString("contentHeader"));
        localQuickPayRecipientListResponse2.a(localJSONObject.optString("contentTitle"));
        localQuickPayRecipientListResponse2.a(localArrayList);
      }
      return localQuickPayRecipientListResponse2;
    }
    catch (Exception localException)
    {
      localQuickPayRecipientListResponse1.a(ChaseApplication.a().getApplicationContext());
    }
    return localQuickPayRecipientListResponse1;
  }

  public static QuickPaySendTransactionVerifyResponse a(QuickPayTransaction paramQuickPayTransaction)
  {
    QuickPaySendTransactionVerifyResponse localQuickPaySendTransactionVerifyResponse1 = new QuickPaySendTransactionVerifyResponse();
    String str1 = a(2131165212);
    Hashtable localHashtable = c();
    a(localHashtable, paramQuickPayTransaction);
    localHashtable.put("selectedRecipientId", paramQuickPayTransaction.o());
    String str6;
    if (paramQuickPayTransaction.D())
    {
      a(localHashtable, "repeatingId", paramQuickPayTransaction.L());
      localHashtable.put("frequency", paramQuickPayTransaction.X());
      localHashtable.put("noOfPayments", Integer.toString(paramQuickPayTransaction.H()));
      if (paramQuickPayTransaction.C())
        str6 = "true";
    }
    while (true)
    {
      localHashtable.put("unlimitedPayments", str6);
      a(localHashtable, "token", paramQuickPayTransaction.K());
      label112: a(localHashtable, "selectedAccountId", paramQuickPayTransaction.p());
      a(localHashtable, "accountType", paramQuickPayTransaction.W());
      localHashtable.put("amount", paramQuickPayTransaction.r());
      if (paramQuickPayTransaction.s() != null)
        localHashtable.put("sendOnDate", s.b(paramQuickPayTransaction.s()));
      a(localHashtable, "recipientEmail", paramQuickPayTransaction.j().d());
      a(localHashtable, "recipientNickname", paramQuickPayTransaction.j().a());
      a(localHashtable, "phoneNumber", paramQuickPayTransaction.j().g());
      if (paramQuickPayTransaction.E() != null)
        localHashtable.put("dueDate", s.b(paramQuickPayTransaction.E()));
      localHashtable.put("memo", paramQuickPayTransaction.t());
      a(localHashtable, "cvv", paramQuickPayTransaction.x());
      String str2;
      label266: String str3;
      label286: String str4;
      if (paramQuickPayTransaction.D())
      {
        str2 = "true";
        localHashtable.put("isRepeatingPayment", str2);
        if (!paramQuickPayTransaction.S())
          break label694;
        str3 = "true";
        localHashtable.put("invoiceRequest", str3);
        if (!paramQuickPayTransaction.B())
          break label701;
        str4 = "inbound";
        localHashtable.put("direction", str4);
      }
      try
      {
        JSONObject localJSONObject = com.chase.sig.android.util.m.a(ChaseApplication.a(), str1, localHashtable);
        QuickPaySendTransactionVerifyResponse localQuickPaySendTransactionVerifyResponse2 = new QuickPaySendTransactionVerifyResponse();
        localQuickPaySendTransactionVerifyResponse2.b(localJSONObject);
        if (!localQuickPaySendTransactionVerifyResponse2.e())
        {
          if (n.a(localJSONObject, "recipient"))
            localQuickPaySendTransactionVerifyResponse2.a(a(localJSONObject.optJSONObject("recipient")));
          localQuickPaySendTransactionVerifyResponse2.o(localJSONObject.optString("formId"));
          localQuickPaySendTransactionVerifyResponse2.f(localJSONObject.optString("accountDisplayName"));
          localQuickPaySendTransactionVerifyResponse2.g(localJSONObject.optString("amount"));
          localQuickPaySendTransactionVerifyResponse2.i(localJSONObject.optString("outboundSendOnDate"));
          localQuickPaySendTransactionVerifyResponse2.l(localJSONObject.optString("inboundDueDate"));
          localQuickPaySendTransactionVerifyResponse2.j(localJSONObject.optString("outboundMemo"));
          localQuickPaySendTransactionVerifyResponse2.h(localJSONObject.optString("invoiceId"));
          localQuickPaySendTransactionVerifyResponse2.b(localJSONObject.optBoolean("isRepeatingPayment", false));
          localQuickPaySendTransactionVerifyResponse2.m(localJSONObject.optString("frequency"));
          localQuickPaySendTransactionVerifyResponse2.a(localJSONObject.optInt("noOfPayments", 0));
          localQuickPaySendTransactionVerifyResponse2.c(localJSONObject.optBoolean("unlimitedPayments", false));
          localQuickPaySendTransactionVerifyResponse2.a(localJSONObject.optBoolean("duplicate"));
          localQuickPaySendTransactionVerifyResponse2.m();
          localQuickPaySendTransactionVerifyResponse2.k(localJSONObject.optString("duplicateMessage"));
          localQuickPaySendTransactionVerifyResponse2.b(localJSONObject);
          localQuickPaySendTransactionVerifyResponse2.p(localJSONObject.optString("smsReason"));
          localQuickPaySendTransactionVerifyResponse2.e("true".equalsIgnoreCase(localJSONObject.optString("iSsmsEligible")));
          String str5 = localJSONObject.optString("feeDisclaimer");
          localQuickPaySendTransactionVerifyResponse2.q(localJSONObject.optString("isNickameMatched", null));
          localQuickPaySendTransactionVerifyResponse2.d("true".equalsIgnoreCase(localJSONObject.optString("isWarning", "false")));
          if (s.m(str5))
            localQuickPaySendTransactionVerifyResponse2.n(str5.replace("\r", "").trim());
        }
        return localQuickPaySendTransactionVerifyResponse2;
        str6 = "false";
        continue;
        a(localHashtable, "token", paramQuickPayTransaction.J());
        break label112;
        str2 = "false";
        break label266;
        label694: str3 = "false";
        break label286;
        label701: str4 = "outbound";
      }
      catch (ChaseException localChaseException)
      {
        localQuickPaySendTransactionVerifyResponse1.a(ChaseApplication.a().getApplicationContext());
      }
    }
    return localQuickPaySendTransactionVerifyResponse1;
  }

  // ERROR //
  public static QuickPayTransactionListResponse a(QuickPayActivityType paramQuickPayActivityType, int paramInt)
  {
    // Byte code:
    //   0: new 518	com/chase/sig/android/service/quickpay/QuickPayTransactionListResponse
    //   3: dup
    //   4: invokespecial 519	com/chase/sig/android/service/quickpay/QuickPayTransactionListResponse:<init>	()V
    //   7: astore_2
    //   8: invokestatic 16	com/chase/sig/android/service/y:c	()Ljava/util/Hashtable;
    //   11: astore 9
    //   13: aload 9
    //   15: ldc 246
    //   17: aload_0
    //   18: invokevirtual 523	com/chase/sig/android/domain/QuickPayActivityType:name	()Ljava/lang/String;
    //   21: invokevirtual 30	java/util/Hashtable:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   24: pop
    //   25: aload 9
    //   27: ldc_w 278
    //   30: iload_1
    //   31: invokestatic 334	java/lang/Integer:toString	(I)Ljava/lang/String;
    //   34: invokevirtual 30	java/util/Hashtable:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   37: pop
    //   38: aload 9
    //   40: ldc_w 525
    //   43: ldc_w 527
    //   46: invokevirtual 30	java/util/Hashtable:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   49: pop
    //   50: ldc_w 528
    //   53: invokestatic 167	com/chase/sig/android/service/y:a	(I)Ljava/lang/String;
    //   56: astore 13
    //   58: invokestatic 35	com/chase/sig/android/ChaseApplication:a	()Lcom/chase/sig/android/ChaseApplication;
    //   61: aload 13
    //   63: aload 9
    //   65: invokestatic 60	com/chase/sig/android/util/m:a	(Lcom/chase/sig/android/ChaseApplication;Ljava/lang/String;Ljava/util/Hashtable;)Lorg/json/JSONObject;
    //   68: astore 14
    //   70: new 518	com/chase/sig/android/service/quickpay/QuickPayTransactionListResponse
    //   73: dup
    //   74: invokespecial 519	com/chase/sig/android/service/quickpay/QuickPayTransactionListResponse:<init>	()V
    //   77: astore 15
    //   79: aload 15
    //   81: aload 14
    //   83: ldc_w 530
    //   86: invokevirtual 76	org/json/JSONObject:optString	(Ljava/lang/String;)Ljava/lang/String;
    //   89: invokevirtual 532	com/chase/sig/android/service/quickpay/QuickPayTransactionListResponse:e	(Ljava/lang/String;)V
    //   92: aload 15
    //   94: aload 14
    //   96: ldc_w 297
    //   99: invokevirtual 76	org/json/JSONObject:optString	(Ljava/lang/String;)Ljava/lang/String;
    //   102: invokevirtual 533	com/chase/sig/android/service/quickpay/QuickPayTransactionListResponse:b	(Ljava/lang/String;)V
    //   105: aload 15
    //   107: aload 14
    //   109: ldc_w 297
    //   112: invokevirtual 76	org/json/JSONObject:optString	(Ljava/lang/String;)Ljava/lang/String;
    //   115: invokevirtual 534	com/chase/sig/android/service/quickpay/QuickPayTransactionListResponse:a	(Ljava/lang/String;)V
    //   118: aload 15
    //   120: aload 14
    //   122: ldc_w 536
    //   125: invokevirtual 76	org/json/JSONObject:optString	(Ljava/lang/String;)Ljava/lang/String;
    //   128: invokevirtual 537	com/chase/sig/android/service/quickpay/QuickPayTransactionListResponse:g	(Ljava/lang/String;)V
    //   131: aload 14
    //   133: ldc_w 539
    //   136: invokevirtual 542	org/json/JSONObject:has	(Ljava/lang/String;)Z
    //   139: ifeq +77 -> 216
    //   142: aload 14
    //   144: ldc_w 539
    //   147: invokevirtual 545	org/json/JSONObject:optJSONArray	(Ljava/lang/String;)Lorg/json/JSONArray;
    //   150: astore 19
    //   152: new 286	java/util/ArrayList
    //   155: dup
    //   156: invokespecial 287	java/util/ArrayList:<init>	()V
    //   159: astore 20
    //   161: aload 19
    //   163: invokevirtual 108	org/json/JSONArray:length	()I
    //   166: istore 21
    //   168: iconst_0
    //   169: istore 22
    //   171: iload 22
    //   173: iload 21
    //   175: if_icmpge +34 -> 209
    //   178: aload 19
    //   180: iload 22
    //   182: invokevirtual 549	org/json/JSONArray:opt	(I)Ljava/lang/Object;
    //   185: astore 23
    //   187: aload 23
    //   189: ifnull +158 -> 347
    //   192: aload 20
    //   194: aload 23
    //   196: checkcast 73	org/json/JSONObject
    //   199: invokestatic 551	com/chase/sig/android/service/y:c	(Lorg/json/JSONObject;)Lcom/chase/sig/android/domain/QuickPayActivityItem;
    //   202: invokevirtual 292	java/util/ArrayList:add	(Ljava/lang/Object;)Z
    //   205: pop
    //   206: goto +141 -> 347
    //   209: aload 15
    //   211: aload 20
    //   213: invokevirtual 552	com/chase/sig/android/service/quickpay/QuickPayTransactionListResponse:a	(Ljava/util/ArrayList;)V
    //   216: aload 14
    //   218: ldc_w 554
    //   221: invokevirtual 542	org/json/JSONObject:has	(Ljava/lang/String;)Z
    //   224: ifeq +57 -> 281
    //   227: aload 14
    //   229: ldc_w 554
    //   232: invokevirtual 216	org/json/JSONObject:optJSONObject	(Ljava/lang/String;)Lorg/json/JSONObject;
    //   235: astore 18
    //   237: aload 18
    //   239: ifnull +42 -> 281
    //   242: aload 15
    //   244: aload 18
    //   246: ldc_w 556
    //   249: invokevirtual 76	org/json/JSONObject:optString	(Ljava/lang/String;)Ljava/lang/String;
    //   252: invokevirtual 557	com/chase/sig/android/service/quickpay/QuickPayTransactionListResponse:f	(Ljava/lang/String;)V
    //   255: aload 15
    //   257: aload 18
    //   259: ldc_w 559
    //   262: invokevirtual 562	org/json/JSONObject:optInt	(Ljava/lang/String;)I
    //   265: invokevirtual 563	com/chase/sig/android/service/quickpay/QuickPayTransactionListResponse:a	(I)V
    //   268: aload 15
    //   270: aload 18
    //   272: ldc_w 565
    //   275: invokevirtual 562	org/json/JSONObject:optInt	(Ljava/lang/String;)I
    //   278: invokevirtual 567	com/chase/sig/android/service/quickpay/QuickPayTransactionListResponse:b	(I)V
    //   281: aload 15
    //   283: iload_1
    //   284: invokevirtual 569	com/chase/sig/android/service/quickpay/QuickPayTransactionListResponse:c	(I)V
    //   287: aload 15
    //   289: areturn
    //   290: astore 6
    //   292: aload_2
    //   293: astore 7
    //   295: aload 7
    //   297: invokestatic 35	com/chase/sig/android/ChaseApplication:a	()Lcom/chase/sig/android/ChaseApplication;
    //   300: invokevirtual 185	com/chase/sig/android/ChaseApplication:getApplicationContext	()Landroid/content/Context;
    //   303: invokevirtual 570	com/chase/sig/android/service/quickpay/QuickPayTransactionListResponse:a	(Landroid/content/Context;)Lcom/chase/sig/android/service/l;
    //   306: pop
    //   307: aload 7
    //   309: areturn
    //   310: astore_3
    //   311: aload_2
    //   312: astore 4
    //   314: aload 4
    //   316: invokestatic 35	com/chase/sig/android/ChaseApplication:a	()Lcom/chase/sig/android/ChaseApplication;
    //   319: invokevirtual 185	com/chase/sig/android/ChaseApplication:getApplicationContext	()Landroid/content/Context;
    //   322: invokevirtual 570	com/chase/sig/android/service/quickpay/QuickPayTransactionListResponse:a	(Landroid/content/Context;)Lcom/chase/sig/android/service/l;
    //   325: pop
    //   326: aload 4
    //   328: areturn
    //   329: astore 17
    //   331: aload 15
    //   333: astore 4
    //   335: goto -21 -> 314
    //   338: astore 16
    //   340: aload 15
    //   342: astore 7
    //   344: goto -49 -> 295
    //   347: iinc 22 1
    //   350: goto -179 -> 171
    //
    // Exception table:
    //   from	to	target	type
    //   8	168	290	org/json/JSONException
    //   178	187	290	org/json/JSONException
    //   192	206	290	org/json/JSONException
    //   209	216	290	org/json/JSONException
    //   216	237	290	org/json/JSONException
    //   242	281	290	org/json/JSONException
    //   8	168	310	java/lang/Exception
    //   178	187	310	java/lang/Exception
    //   192	206	310	java/lang/Exception
    //   209	216	310	java/lang/Exception
    //   216	237	310	java/lang/Exception
    //   242	281	310	java/lang/Exception
    //   281	287	329	java/lang/Exception
    //   281	287	338	org/json/JSONException
  }

  public static QuickPayTransactionListResponse a(String paramString, int paramInt)
  {
    QuickPayTransactionListResponse localQuickPayTransactionListResponse1 = new QuickPayTransactionListResponse();
    while (true)
    {
      int j;
      try
      {
        Hashtable localHashtable = c();
        localHashtable.put("sortByColumn", paramString);
        localHashtable.put("pageNumber", Integer.toString(paramInt));
        localHashtable.put("listPending", "true");
        String str = a(2131165215);
        JSONObject localJSONObject1 = com.chase.sig.android.util.m.a(ChaseApplication.a(), str, localHashtable);
        QuickPayTransactionListResponse localQuickPayTransactionListResponse2 = new QuickPayTransactionListResponse();
        localQuickPayTransactionListResponse2.e(localJSONObject1.optString("contentHeader"));
        localQuickPayTransactionListResponse2.a(localJSONObject1.optString("contentTitle"));
        localQuickPayTransactionListResponse2.g(localJSONObject1.optString("contentEmptyMessage"));
        if (localJSONObject1.has("transactions"))
        {
          JSONArray localJSONArray = localJSONObject1.optJSONArray("transactions");
          ArrayList localArrayList = new ArrayList();
          int i = localJSONArray.length();
          j = 0;
          if (j < i)
          {
            Object localObject = localJSONArray.opt(j);
            if (localObject != null)
              localArrayList.add(f((JSONObject)localObject));
          }
          else
          {
            localQuickPayTransactionListResponse2.b(localArrayList);
          }
        }
        else
        {
          if (localJSONObject1.has("pageInfo"))
          {
            JSONObject localJSONObject2 = localJSONObject1.optJSONObject("pageInfo");
            if (localJSONObject2 != null)
            {
              localQuickPayTransactionListResponse2.f(localJSONObject2.optString("showNext"));
              localQuickPayTransactionListResponse2.a(localJSONObject2.optInt("totalRows"));
              localQuickPayTransactionListResponse2.b(localJSONObject2.optInt("nextPage"));
              if ("true".equalsIgnoreCase(localQuickPayTransactionListResponse2.d()))
                localQuickPayTransactionListResponse2.c(-1 + localQuickPayTransactionListResponse2.k());
            }
          }
          return localQuickPayTransactionListResponse2;
        }
      }
      catch (JSONException localJSONException)
      {
        localQuickPayTransactionListResponse1.a(ChaseApplication.a().getApplicationContext());
        return localQuickPayTransactionListResponse1;
      }
      catch (Exception localException)
      {
        localQuickPayTransactionListResponse1.a(ChaseApplication.a().getApplicationContext());
        return localQuickPayTransactionListResponse1;
      }
      j++;
    }
  }

  public static QuickPayTransactionListResponse a(String paramString1, QuickPayActivityType paramQuickPayActivityType, String paramString2)
  {
    QuickPayTransactionListResponse localQuickPayTransactionListResponse1 = new QuickPayTransactionListResponse();
    try
    {
      String str = a(2131165208);
      Hashtable localHashtable = c();
      localHashtable.put("transactionId", paramString1);
      localHashtable.put("activityType", paramQuickPayActivityType.name());
      if (paramString2 != null)
        localHashtable.put("invoiceRequest", paramString2);
      JSONObject localJSONObject = com.chase.sig.android.util.m.a(ChaseApplication.a(), str, localHashtable);
      QuickPayTransactionListResponse localQuickPayTransactionListResponse2 = new QuickPayTransactionListResponse();
      localQuickPayTransactionListResponse2.e(localJSONObject.optString("contentHeader1"));
      localQuickPayTransactionListResponse2.b(localJSONObject.optString("contentTitle"));
      localQuickPayTransactionListResponse2.a(localJSONObject.optString("contentTitle"));
      if (localJSONObject.has("contentData"))
      {
        QuickPayActivityItem localQuickPayActivityItem = c(localJSONObject.optJSONObject("contentData"));
        ArrayList localArrayList = new ArrayList();
        localArrayList.add(localQuickPayActivityItem);
        localQuickPayTransactionListResponse2.a(localArrayList);
      }
      return localQuickPayTransactionListResponse2;
    }
    catch (Exception localException)
    {
      localQuickPayTransactionListResponse1.a(ChaseApplication.a().getApplicationContext());
    }
    return localQuickPayTransactionListResponse1;
  }

  public static QuickPayTransactionStartResponse a(String paramString1, String paramString2)
  {
    QuickPayTransactionStartResponse localQuickPayTransactionStartResponse1 = new QuickPayTransactionStartResponse();
    while (true)
    {
      int i;
      try
      {
        String str1 = a(2131165209);
        Hashtable localHashtable = c();
        localHashtable.put("direction", "outbound");
        if (s.m(paramString1))
          localHashtable.put("transactionid", paramString1);
        if (s.m(paramString2))
          localHashtable.put("invoiceId", paramString2);
        JSONObject localJSONObject = com.chase.sig.android.util.m.a(ChaseApplication.a(), str1, localHashtable);
        QuickPayTransactionStartResponse localQuickPayTransactionStartResponse2 = new QuickPayTransactionStartResponse();
        String str2 = localJSONObject.optString("cutoffNotice");
        if (s.m(str2))
          localQuickPayTransactionStartResponse2.a(str2.replace("\r", "").trim());
        if (localJSONObject.has("payFromAccounts"))
        {
          ArrayList localArrayList = new ArrayList();
          JSONArray localJSONArray = localJSONObject.optJSONArray("payFromAccounts");
          i = 0;
          if (i < localJSONArray.length())
          {
            Object localObject = localJSONArray.opt(i);
            if (localObject != null)
              localArrayList.add(b((JSONObject)localObject));
          }
          else
          {
            localQuickPayTransactionStartResponse2.a(localArrayList);
          }
        }
        else
        {
          return localQuickPayTransactionStartResponse2;
        }
      }
      catch (Exception localException)
      {
        localQuickPayTransactionStartResponse1.a(ChaseApplication.a().getApplicationContext());
        return localQuickPayTransactionStartResponse1;
      }
      i++;
    }
  }

  private static void a(Hashtable<String, String> paramHashtable, QuickPayTransaction paramQuickPayTransaction)
  {
    String str8;
    Object localObject;
    String str2;
    String str3;
    if (paramQuickPayTransaction.R())
      if (paramQuickPayTransaction.S())
      {
        str8 = paramQuickPayTransaction.A();
        String str9 = paramQuickPayTransaction.w();
        localObject = "0";
        str2 = str8;
        str3 = str9;
      }
    while (true)
    {
      a(paramHashtable, "transactionId", (String)localObject);
      a(paramHashtable, "invoiceId", str2);
      a(paramHashtable, "requestTransactionId", str3);
      return;
      str8 = "0";
      break;
      if ((paramQuickPayTransaction.D()) && (paramQuickPayTransaction.V()))
      {
        String str6 = paramQuickPayTransaction.w();
        paramHashtable.put("noOfPayments", Integer.toString(paramQuickPayTransaction.H()));
        if (paramQuickPayTransaction.V());
        for (String str7 = "true"; ; str7 = "false")
        {
          paramHashtable.put("editing", str7);
          localObject = str6;
          str2 = "0";
          str3 = null;
          break;
        }
      }
      if (paramQuickPayTransaction.V())
      {
        String str4 = paramQuickPayTransaction.w();
        if (paramQuickPayTransaction.V());
        for (String str5 = "true"; ; str5 = "false")
        {
          paramHashtable.put("editing", str5);
          localObject = str4;
          str2 = "0";
          str3 = null;
          break;
        }
      }
      String str1 = paramQuickPayTransaction.w();
      str2 = paramQuickPayTransaction.A();
      localObject = str1;
      str3 = null;
    }
  }

  private static PayFromAccount b(JSONObject paramJSONObject)
  {
    PayFromAccount localPayFromAccount = new PayFromAccount();
    localPayFromAccount.a(paramJSONObject.optString("displayName"));
    localPayFromAccount.c(paramJSONObject.optString("id"));
    localPayFromAccount.b(paramJSONObject.optString("maskedAccountNumber"));
    localPayFromAccount.d(paramJSONObject.optString("nickname"));
    localPayFromAccount.e(paramJSONObject.optString("sourceIndicator"));
    localPayFromAccount.a(paramJSONObject.optBoolean("requiresCVV"));
    localPayFromAccount.b(paramJSONObject.optString("sourceIndicator").equalsIgnoreCase("ExternalDDA"));
    return localPayFromAccount;
  }

  public static QuickPaySendTransactionConfirmResponse b(QuickPayTransaction paramQuickPayTransaction)
  {
    QuickPaySendTransactionConfirmResponse localQuickPaySendTransactionConfirmResponse1 = new QuickPaySendTransactionConfirmResponse();
    String str1;
    if (paramQuickPayTransaction.V())
      str1 = a(2131165214);
    while (true)
    {
      Hashtable localHashtable = c();
      a(localHashtable, paramQuickPayTransaction);
      localHashtable.put("formId", paramQuickPayTransaction.O());
      localHashtable.put("amount", paramQuickPayTransaction.r());
      localHashtable.put("memo", paramQuickPayTransaction.t());
      a(localHashtable, "selectedAccountId", paramQuickPayTransaction.p());
      localHashtable.put("selectedRecipientId", paramQuickPayTransaction.o());
      a(localHashtable, "cvv", paramQuickPayTransaction.x());
      String str2;
      label113: String str3;
      label134: String str4;
      label236: String str5;
      if (paramQuickPayTransaction.B())
      {
        str2 = "inbound";
        localHashtable.put("direction", str2);
        if (!paramQuickPayTransaction.S())
          break label554;
        str3 = "true";
        localHashtable.put("invoiceRequest", str3);
        if (paramQuickPayTransaction.s() != null)
          localHashtable.put("sendOnDate", s.b(paramQuickPayTransaction.s()));
        if (paramQuickPayTransaction.E() != null)
          localHashtable.put("dueDate", s.b(paramQuickPayTransaction.E()));
        a(localHashtable, "recipientEmail", paramQuickPayTransaction.u());
        a(localHashtable, "phoneNumber", paramQuickPayTransaction.j().f());
        localHashtable.put("recipientNickname", paramQuickPayTransaction.v());
        if (!paramQuickPayTransaction.D())
          break label561;
        str4 = "true";
        localHashtable.put("isRepeatingPayment", str4);
        if (!paramQuickPayTransaction.D())
          break label575;
        localHashtable.put("frequency", paramQuickPayTransaction.X());
        localHashtable.put("noOfPayments", Integer.toString(paramQuickPayTransaction.H()));
        if (!paramQuickPayTransaction.C())
          break label568;
        str5 = "true";
        label290: localHashtable.put("unlimitedPayments", str5);
        a(localHashtable, "repeatingId", paramQuickPayTransaction.L());
        a(localHashtable, "token", paramQuickPayTransaction.K());
      }
      try
      {
        while (true)
        {
          JSONObject localJSONObject = com.chase.sig.android.util.m.a(ChaseApplication.a(), str1, localHashtable);
          QuickPaySendTransactionConfirmResponse localQuickPaySendTransactionConfirmResponse2 = new QuickPaySendTransactionConfirmResponse();
          localQuickPaySendTransactionConfirmResponse2.b(localJSONObject);
          if (!localQuickPaySendTransactionConfirmResponse2.e())
          {
            if (n.a(localJSONObject, "recipient"))
              localQuickPaySendTransactionConfirmResponse2.a(a(localJSONObject.optJSONObject("recipient")));
            localQuickPaySendTransactionConfirmResponse2.b(localJSONObject);
            localQuickPaySendTransactionConfirmResponse2.f(localJSONObject.optString("accountDisplayname"));
            localQuickPaySendTransactionConfirmResponse2.g(localJSONObject.optString("amount"));
            localQuickPaySendTransactionConfirmResponse2.i(localJSONObject.optString("outboundSendOnDate"));
            localQuickPaySendTransactionConfirmResponse2.j(localJSONObject.optString("memo"));
            localQuickPaySendTransactionConfirmResponse2.b(localJSONObject.optString("transactionNumber"));
            localQuickPaySendTransactionConfirmResponse2.a(localJSONObject.optString("status"));
            localQuickPaySendTransactionConfirmResponse2.e(localJSONObject.optString("senderCode"));
            localQuickPaySendTransactionConfirmResponse2.b(localJSONObject.optBoolean("isRepeatingPayment", false));
            localQuickPaySendTransactionConfirmResponse2.m(localJSONObject.optString("frequency"));
            localQuickPaySendTransactionConfirmResponse2.a(localJSONObject.optInt("noOfPayments", 0));
            localQuickPaySendTransactionConfirmResponse2.c(localJSONObject.optBoolean("unlimitedPayments", false));
          }
          return localQuickPaySendTransactionConfirmResponse2;
          str1 = a(2131165213);
          break;
          str2 = "outbound";
          break label113;
          label554: str3 = "false";
          break label134;
          label561: str4 = "false";
          break label236;
          label568: str5 = "false";
          break label290;
          label575: a(localHashtable, "token", paramQuickPayTransaction.J());
        }
      }
      catch (ChaseException localChaseException)
      {
        localQuickPaySendTransactionConfirmResponse1.a(ChaseApplication.a().getApplicationContext());
      }
    }
    return localQuickPaySendTransactionConfirmResponse1;
  }

  private static QuickPayActivityItem c(JSONObject paramJSONObject)
  {
    QuickPayActivityItem localQuickPayActivityItem = new QuickPayActivityItem();
    localQuickPayActivityItem.a(d(paramJSONObject.optJSONObject("date")));
    localQuickPayActivityItem.b(d(paramJSONObject.optJSONObject("amount")));
    localQuickPayActivityItem.c(d(paramJSONObject.optJSONObject("status")));
    localQuickPayActivityItem.d(d(paramJSONObject.optJSONObject("id")));
    localQuickPayActivityItem.e(d(paramJSONObject.optJSONObject("type")));
    localQuickPayActivityItem.f(d(paramJSONObject.optJSONObject("action")));
    localQuickPayActivityItem.g(d(paramJSONObject.optJSONObject("decline")));
    localQuickPayActivityItem.h(d(paramJSONObject.optJSONObject("accountName")));
    localQuickPayActivityItem.j(d(paramJSONObject.optJSONObject("receivedOn")));
    localQuickPayActivityItem.k(d(paramJSONObject.optJSONObject("acceptedOn")));
    localQuickPayActivityItem.i(d(paramJSONObject.optJSONObject("memo")));
    if ((paramJSONObject.has("declineReason")) && (s.m(paramJSONObject.optJSONObject("declineReason").optString("value"))))
      localQuickPayActivityItem.x(d(paramJSONObject.optJSONObject("declineReason")));
    if (paramJSONObject.has("transactionId"))
      localQuickPayActivityItem.d(d(paramJSONObject.optJSONObject("transactionId")));
    localQuickPayActivityItem.l(d(paramJSONObject.optJSONObject("token")));
    localQuickPayActivityItem.m(d(paramJSONObject.optJSONObject("dueDate")));
    localQuickPayActivityItem.n(d(paramJSONObject.optJSONObject("invoiceNumber")));
    if (paramJSONObject.has("sentOn"))
      localQuickPayActivityItem.o(d(paramJSONObject.optJSONObject("sentOn")));
    JSONObject localJSONObject1 = paramJSONObject.optJSONObject("displayDate");
    if (localJSONObject1 != null)
    {
      LabeledValue localLabeledValue1 = new LabeledValue();
      localLabeledValue1.a(localJSONObject1.optString("label"));
      localLabeledValue1.b(localJSONObject1.optString("value"));
      localQuickPayActivityItem.a(localLabeledValue1);
    }
    LabeledValue localLabeledValue2 = new LabeledValue();
    localLabeledValue2.a("isatmeligible");
    if (paramJSONObject.has("isAtmEligible"))
    {
      localLabeledValue2.b(d(paramJSONObject.optJSONObject("isAtmEligible")).b());
      localQuickPayActivityItem.p(localLabeledValue2);
    }
    if (paramJSONObject.has("senderCode"))
      localQuickPayActivityItem.q(d(paramJSONObject.optJSONObject("senderCode")));
    if (paramJSONObject.has("recipientCode"))
      localQuickPayActivityItem.v(d(paramJSONObject.optJSONObject("recipientCode")));
    if (paramJSONObject.has("isInvoiceRequest"))
      localQuickPayActivityItem.w(d(paramJSONObject.optJSONObject("isInvoiceRequest")));
    if ((paramJSONObject.has("sender")) && (!paramJSONObject.isNull("sender")))
      localQuickPayActivityItem.a(a(paramJSONObject.getJSONObject("sender")));
    while (true)
    {
      if ((paramJSONObject.has("repeatingInfo")) && (!paramJSONObject.isNull("repeatingInfo")))
      {
        localQuickPayActivityItem.r(new LabeledValue("frequency", paramJSONObject.optJSONObject("repeatingInfo").optString("frequency")));
        localQuickPayActivityItem.s(new LabeledValue("numberOfRemainingPayments", paramJSONObject.optJSONObject("repeatingInfo").optString("numberOfRemainingPayments")));
        localQuickPayActivityItem.u(new LabeledValue("nextDeliverByDate", paramJSONObject.optJSONObject("repeatingInfo").optString("nextDeliverByDate")));
        localQuickPayActivityItem.t(new LabeledValue("nextSendonDate", paramJSONObject.optJSONObject("repeatingInfo").optString("nextSendonDate")));
      }
      JSONObject localJSONObject4 = paramJSONObject.optJSONObject("payFromAccount");
      if (localJSONObject4 != null)
        localQuickPayActivityItem.h(new LabeledValue("Pay To", localJSONObject4.optString("displayName")));
      return localQuickPayActivityItem;
      if ((paramJSONObject.has("recipient")) && (!paramJSONObject.isNull("recipient")))
      {
        localQuickPayActivityItem.a(a(paramJSONObject.getJSONObject("recipient")));
      }
      else
      {
        JSONObject localJSONObject2 = paramJSONObject.optJSONObject("nickName");
        if (localJSONObject2 != null)
          localQuickPayActivityItem.j().a(localJSONObject2.optString("value"));
        JSONObject localJSONObject3 = paramJSONObject.optJSONObject("email");
        if (localJSONObject3 != null)
          localQuickPayActivityItem.j().d(localJSONObject3.optString("value"));
      }
    }
  }

  public static TodoListResponse c(int paramInt)
  {
    int i = 0;
    TodoListResponse localTodoListResponse1 = new TodoListResponse();
    while (true)
    {
      try
      {
        Hashtable localHashtable = c();
        localHashtable.put("pageNumber", Integer.toString(paramInt));
        localHashtable.put("includeFrequencyList", "true");
        String str = a(2131165203);
        JSONObject localJSONObject1 = com.chase.sig.android.util.m.a(ChaseApplication.a(), str, localHashtable);
        localTodoListResponse1.b(localJSONObject1);
        if (!localTodoListResponse1.e())
        {
          localTodoListResponse2 = new TodoListResponse();
          localTodoListResponse2.a(localJSONObject1.optString("contentTitle"));
          localTodoListResponse2.b(localJSONObject1.optString("contentHeader1"));
          localTodoListResponse2.a(localJSONObject1.optInt("totalItems"));
          localTodoListResponse2.f(localJSONObject1.optString("contentEmptyMessage"));
          localTodoListResponse2.g(localJSONObject1.optString("accountMessage"));
          localTodoListResponse2.h(localJSONObject1.optString("missingPmtMessage"));
          if (localJSONObject1.has("frequencyList"))
          {
            JSONArray localJSONArray2 = localJSONObject1.getJSONArray("frequencyList");
            ArrayList localArrayList = new ArrayList();
            int j = 0;
            if (j < localJSONArray2.length())
            {
              JSONObject localJSONObject5 = localJSONArray2.getJSONObject(j);
              localArrayList.add(new LabeledValue(localJSONObject5.getString("label"), localJSONObject5.getString("value")));
              j++;
              continue;
            }
            localTodoListResponse2.a(new com.chase.sig.android.domain.m(localArrayList));
          }
          if (localJSONObject1.has("actions"))
          {
            JSONObject localJSONObject4 = localJSONObject1.optJSONObject("actions");
            localTodoListResponse2.b();
            localTodoListResponse2.a("true".equalsIgnoreCase(localJSONObject4.optString("requestMoney")));
            localTodoListResponse2.b("true".equalsIgnoreCase(localJSONObject4.optString("sendMoney")));
            localTodoListResponse2.c("true".equalsIgnoreCase(localJSONObject4.optString("acceptMoney")));
            localTodoListResponse2.d("true".equalsIgnoreCase(localJSONObject4.optString("viewTodo")));
            localTodoListResponse2.e("true".equalsIgnoreCase(localJSONObject4.optString("viewActivity")));
          }
          JSONArray localJSONArray1 = localJSONObject1.getJSONArray("todoItem");
          if (i < localJSONArray1.length())
          {
            JSONObject localJSONObject3 = localJSONArray1.getJSONObject(i);
            TodoItem localTodoItem = new TodoItem();
            localTodoItem.a(localJSONObject3.optString("header"));
            localTodoItem.b(localJSONObject3.optString("actionLabel"));
            localTodoItem.a(new Dollar(localJSONObject3.optString("amount")));
            localTodoItem.c(localJSONObject3.optString("sublabel"));
            localTodoItem.d(localJSONObject3.optString("declineLabel"));
            localTodoItem.e(localJSONObject3.optString("id"));
            localTodoItem.f(localJSONObject3.optString("typeString"));
            localTodoItem.g(localJSONObject3.optString("typeLabel"));
            localTodoItem.h(localJSONObject3.optString("isInvoiceRequest"));
            localTodoListResponse2.c().add(localTodoItem);
            i++;
            continue;
          }
          if (localJSONObject1.optInt("totalItems") > 25)
          {
            localTodoListResponse2.b(localJSONObject1.optInt("nextPage"));
            localTodoListResponse2.c(localJSONObject1.optInt("thisPage"));
          }
          JSONObject localJSONObject2 = localJSONObject1.optJSONObject("defaultAccount");
          if (localJSONObject2 != null)
            localTodoListResponse2.a(b(localJSONObject2));
          localTodoListResponse2.e(localJSONObject1.optString("earliestSendOnDate"));
          return localTodoListResponse2;
        }
      }
      catch (JSONException localJSONException)
      {
        localTodoListResponse1.a(ChaseApplication.a().getApplicationContext());
        return localTodoListResponse1;
      }
      catch (Exception localException)
      {
        localTodoListResponse1.a(ChaseApplication.a().getApplicationContext());
        return localTodoListResponse1;
      }
      TodoListResponse localTodoListResponse2 = localTodoListResponse1;
    }
  }

  private static LabeledValue d(JSONObject paramJSONObject)
  {
    LabeledValue localLabeledValue = new LabeledValue();
    if (paramJSONObject != null)
    {
      localLabeledValue.a(paramJSONObject.optString("label"));
      localLabeledValue.b(s.s(paramJSONObject.optString("value")));
    }
    return localLabeledValue;
  }

  private static String e(JSONObject paramJSONObject)
  {
    if ((paramJSONObject == null) || (paramJSONObject.isNull("value")))
      return "";
    return paramJSONObject.optString("value");
  }

  private static QuickPayPendingTransaction f(JSONObject paramJSONObject)
  {
    QuickPayPendingTransaction localQuickPayPendingTransaction = new QuickPayPendingTransaction();
    if (paramJSONObject.has("transactionId"))
      localQuickPayPendingTransaction.g(e(paramJSONObject.optJSONObject("transactionId")));
    if (paramJSONObject.has("status"))
    {
      localQuickPayPendingTransaction.f(e(paramJSONObject.optJSONObject("status")));
      localQuickPayPendingTransaction.z();
    }
    if (paramJSONObject.has("canEdit"))
      localQuickPayPendingTransaction.r(e(paramJSONObject.optJSONObject("canEdit")));
    if (paramJSONObject.has("canCancel"))
      localQuickPayPendingTransaction.s(e(paramJSONObject.optJSONObject("canCancel")));
    if (paramJSONObject.has("recipient"))
      localQuickPayPendingTransaction.a(a(paramJSONObject.optJSONObject("recipient")));
    if (paramJSONObject.has("repeatingInfo"))
    {
      JSONObject localJSONObject2 = paramJSONObject.optJSONObject("repeatingInfo");
      if (localJSONObject2 != null)
      {
        localQuickPayPendingTransaction.d(true);
        localQuickPayPendingTransaction.v(localJSONObject2.optString("frequency"));
        localQuickPayPendingTransaction.c("true".equalsIgnoreCase(localJSONObject2.optString("openEnded")));
        localQuickPayPendingTransaction.a(Integer.parseInt(localJSONObject2.optString("numberOfRemainingPayments")));
        localQuickPayPendingTransaction.a(s.g(localJSONObject2.optString("nextDeliverByDate")));
        localQuickPayPendingTransaction.c(s.g(localJSONObject2.optString("nextSendonDate")));
        localQuickPayPendingTransaction.n(localJSONObject2.optString("token"));
        localQuickPayPendingTransaction.o(localJSONObject2.optString("repeatingId"));
      }
    }
    localQuickPayPendingTransaction.a(e(paramJSONObject.optJSONObject("displayDate")));
    localQuickPayPendingTransaction.d(e(paramJSONObject.optJSONObject("amount")));
    JSONObject localJSONObject1 = paramJSONObject.optJSONObject("payFromAccount");
    localQuickPayPendingTransaction.c(localJSONObject1.optString("displayName"));
    localQuickPayPendingTransaction.b(localJSONObject1.optString("id"));
    localQuickPayPendingTransaction.u(localJSONObject1.optString("accountType", null));
    localQuickPayPendingTransaction.m(e(paramJSONObject.optJSONObject("token")));
    localQuickPayPendingTransaction.e(e(paramJSONObject.optJSONObject("memo")));
    localQuickPayPendingTransaction.b(s.g(e(paramJSONObject.optJSONObject("sentOn"))));
    return localQuickPayPendingTransaction;
  }

  private static QuickPayActivityItem g(JSONObject paramJSONObject)
  {
    QuickPayActivityItem localQuickPayActivityItem = null;
    try
    {
      localQuickPayActivityItem = c(paramJSONObject.getJSONObject("contentdata"));
      JSONObject localJSONObject = paramJSONObject.getJSONObject("contentdata").getJSONObject("paymentnotificationresent");
      localQuickPayActivityItem.a(localJSONObject.optString("label"));
      localQuickPayActivityItem.a("true".equalsIgnoreCase(localJSONObject.optString("value")));
      return localQuickPayActivityItem;
    }
    catch (JSONException localJSONException)
    {
    }
    return localQuickPayActivityItem;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.y
 * JD-Core Version:    0.6.2
 */