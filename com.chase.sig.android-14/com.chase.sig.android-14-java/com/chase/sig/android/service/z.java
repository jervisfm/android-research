package com.chase.sig.android.service;

import com.chase.sig.android.ChaseApplication;
import java.net.URLEncoder;
import java.util.Hashtable;

public final class z extends JPService
{
  public static QuoteNewsResponse a(String paramString, String[] paramArrayOfString)
  {
    QuoteNewsResponse localQuoteNewsResponse1 = new QuoteNewsResponse();
    try
    {
      Hashtable localHashtable = c();
      if (paramArrayOfString.length > 0)
        localHashtable.put("tickerSymbol", URLEncoder.encode(paramArrayOfString[0]));
      QuoteNewsResponse localQuoteNewsResponse2 = (QuoteNewsResponse)a(paramString, localHashtable, QuoteNewsResponse.class);
      return localQuoteNewsResponse2;
    }
    catch (Exception localException)
    {
      localQuoteNewsResponse1.a(ChaseApplication.a().getApplicationContext());
    }
    return localQuoteNewsResponse1;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.service.z
 * JD-Core Version:    0.6.2
 */