package com.chase.sig.android.util;

public class ChaseException extends Exception
{
  public static int a = 0;
  public static int b = 1;
  private String description;
  private Exception exception;
  private int severity = a;

  public ChaseException(int paramInt, Exception paramException, String paramString)
  {
    this.severity = paramInt;
    this.exception = paramException;
    this.description = paramString;
  }

  public ChaseException(int paramInt, String paramString)
  {
    this(paramInt, null, paramString);
  }

  public ChaseException(Exception paramException, String paramString)
  {
    this(b, paramException, paramString);
  }

  public final String a()
  {
    return this.description;
  }

  public String toString()
  {
    if (this.exception == null)
    {
      Object[] arrayOfObject2 = new Object[2];
      arrayOfObject2[0] = Integer.valueOf(this.severity);
      arrayOfObject2[1] = this.description;
      return String.format("[Severity=%s; Description=%s", arrayOfObject2);
    }
    Object[] arrayOfObject1 = new Object[3];
    arrayOfObject1[0] = Integer.valueOf(this.severity);
    arrayOfObject1[1] = this.description;
    arrayOfObject1[2] = this.exception.toString();
    return String.format("[Severity=%s; Description=%s; InnerException=%s", arrayOfObject1);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.util.ChaseException
 * JD-Core Version:    0.6.2
 */