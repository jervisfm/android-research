package com.chase.sig.android.util;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DecimalFormat;

public class Dollar
  implements Serializable, Comparable
{
  private static DecimalFormat a;
  private static DecimalFormat b;
  private static DecimalFormat c;
  private BigDecimal amount;

  public Dollar(String paramString)
  {
    if (paramString == null)
    {
      this.amount = null;
      return;
    }
    BigDecimal localBigDecimal;
    try
    {
      localBigDecimal = new BigDecimal(paramString.replace(",", "").replace("$", ""));
      if (localBigDecimal.scale() == 0)
      {
        this.amount = localBigDecimal.setScale(2);
        return;
      }
    }
    catch (NumberFormatException localNumberFormatException)
    {
      new StringBuilder("Failed to format Dollar  ").append(localNumberFormatException.getMessage()).toString();
      return;
    }
    this.amount = localBigDecimal;
  }

  public Dollar(BigDecimal paramBigDecimal)
  {
    this.amount = paramBigDecimal;
  }

  private String a(DecimalFormat paramDecimalFormat)
  {
    if (this.amount == null)
      return "";
    return paramDecimalFormat.format(this.amount.doubleValue());
  }

  public final boolean a()
  {
    return this.amount != null;
  }

  public final BigDecimal b()
  {
    return this.amount;
  }

  public final boolean c()
  {
    BigDecimal localBigDecimal = this.amount;
    boolean bool = false;
    if (localBigDecimal != null)
    {
      int i = this.amount.compareTo(new BigDecimal(0));
      bool = false;
      if (i > 0)
        bool = true;
    }
    return bool;
  }

  public int compareTo(Object paramObject)
  {
    if (!(paramObject instanceof Dollar))
      return 0;
    Dollar localDollar = (Dollar)paramObject;
    return this.amount.compareTo(localDollar.amount);
  }

  public final boolean d()
  {
    BigDecimal localBigDecimal = this.amount;
    boolean bool = false;
    if (localBigDecimal != null)
    {
      int i = this.amount.compareTo(new BigDecimal(0));
      bool = false;
      if (i < 0)
        bool = true;
    }
    return bool;
  }

  public final String e()
  {
    if (this.amount == null)
      return null;
    return this.amount.toPlainString();
  }

  public final String f()
  {
    if (c == null)
      c = new DecimalFormat("###,###,###,##0.00");
    return a(c);
  }

  public final String g()
  {
    if (b == null)
      b = new DecimalFormat("###########0.00");
    return a(b);
  }

  public final String h()
  {
    if (a == null)
      a = new DecimalFormat("$###,###,###,##0.00");
    return a(a);
  }

  public String toString()
  {
    return h();
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.util.Dollar
 * JD-Core Version:    0.6.2
 */