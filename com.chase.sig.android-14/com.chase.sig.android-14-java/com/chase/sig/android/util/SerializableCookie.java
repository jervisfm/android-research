package com.chase.sig.android.util;

import java.io.Externalizable;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.Date;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.cookie.BasicClientCookie;

public class SerializableCookie
  implements Externalizable, Cookie
{
  private transient Cookie a;
  private transient int b = 0;

  public SerializableCookie()
  {
  }

  public SerializableCookie(Cookie paramCookie)
  {
    this.a = paramCookie;
  }

  public String getComment()
  {
    return this.a.getComment();
  }

  public String getCommentURL()
  {
    return this.a.getCommentURL();
  }

  public String getDomain()
  {
    return this.a.getDomain();
  }

  public Date getExpiryDate()
  {
    return this.a.getExpiryDate();
  }

  public String getName()
  {
    return this.a.getName();
  }

  public String getPath()
  {
    return this.a.getPath();
  }

  public int[] getPorts()
  {
    return this.a.getPorts();
  }

  public String getValue()
  {
    return this.a.getValue();
  }

  public int getVersion()
  {
    return this.a.getVersion();
  }

  public boolean isExpired(Date paramDate)
  {
    return this.a.isExpired(paramDate);
  }

  public boolean isPersistent()
  {
    return this.a.isPersistent();
  }

  public boolean isSecure()
  {
    return this.a.isSecure();
  }

  public void readExternal(ObjectInput paramObjectInput)
  {
    this.b = paramObjectInput.readInt();
    if ((0x1 & this.b) == 0);
    for (String str1 = paramObjectInput.readUTF(); ; str1 = null)
    {
      if ((0x2 & this.b) == 0);
      for (String str2 = paramObjectInput.readUTF(); ; str2 = null)
      {
        if ((0x10 & this.b) == 0);
        for (Date localDate = new Date(paramObjectInput.readLong()); ; localDate = null)
        {
          if ((0x20 & this.b) == 0);
          for (String str3 = paramObjectInput.readUTF(); ; str3 = null)
          {
            if ((0x40 & this.b) == 0);
            for (String str4 = paramObjectInput.readUTF(); ; str4 = null)
            {
              if ((0x80 & this.b) == 0)
              {
                int j = paramObjectInput.readInt();
                int[] arrayOfInt = new int[j];
                for (int k = 0; k < j; k++)
                  arrayOfInt[k] = paramObjectInput.readInt();
              }
              boolean bool = paramObjectInput.readBoolean();
              int i = paramObjectInput.readInt();
              BasicClientCookie localBasicClientCookie = new BasicClientCookie(str1, str2);
              localBasicClientCookie.setComment(null);
              localBasicClientCookie.setDomain(str3);
              localBasicClientCookie.setExpiryDate(localDate);
              localBasicClientCookie.setPath(str4);
              localBasicClientCookie.setSecure(bool);
              localBasicClientCookie.setVersion(i);
              this.a = localBasicClientCookie;
              return;
            }
          }
        }
      }
    }
  }

  public String toString()
  {
    if (this.a == null)
      return "null";
    return this.a.toString();
  }

  public void writeExternal(ObjectOutput paramObjectOutput)
  {
    int i = 0;
    paramObjectOutput.writeInt(this.b);
    if ((0x1 & this.b) == 0)
      paramObjectOutput.writeUTF(getName());
    if ((0x2 & this.b) == 0)
      paramObjectOutput.writeUTF(getValue());
    if ((0x10 & this.b) == 0)
      paramObjectOutput.writeLong(getExpiryDate().getTime());
    if ((0x20 & this.b) == 0)
      paramObjectOutput.writeUTF(getDomain());
    if ((0x40 & this.b) == 0)
      paramObjectOutput.writeUTF(getPath());
    if ((0x80 & this.b) == 0)
    {
      if (getPorts() == null);
      for (int j = 0; ; j = getPorts().length)
      {
        paramObjectOutput.writeInt(j);
        int[] arrayOfInt = getPorts();
        while (i < j)
        {
          paramObjectOutput.writeInt(arrayOfInt[i]);
          i++;
        }
      }
    }
    paramObjectOutput.writeBoolean(isSecure());
    paramObjectOutput.writeInt(getVersion());
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.util.SerializableCookie
 * JD-Core Version:    0.6.2
 */