package com.chase.sig.android.util;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.SimpleAdapter;
import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.domain.IAccount;
import com.chase.sig.android.domain.LabeledValue;
import com.chase.sig.android.domain.Payee;
import com.chase.sig.android.domain.QuickDepositAccount;
import com.chase.sig.android.domain.QuickDepositAccount.Authorization;
import com.chase.sig.android.domain.QuickDepositAccount.Location;
import com.chase.sig.android.domain.g;
import com.chase.sig.android.domain.o;
import com.chase.sig.android.service.quickpay.PayFromAccount;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public final class a
{
  public static final String[] a = { "nickname", "amount" };
  public static final String[] b = { "nickname", "mask", "amount" };
  public static final int[] c = { 2131296524, 2131296525 };
  public static final int[] d = { 2131296281, 2131296280, 2131296282 };
  public static final String[] e = { "label" };
  public static final int[] f = { 2131296524 };

  public static ArrayAdapter<QuickDepositAccount.Location> a(Context paramContext, List<QuickDepositAccount> paramList, String paramString)
  {
    new r();
    return new ArrayAdapter(paramContext, 2130903213, ((QuickDepositAccount)paramList.get(r.a(paramList, new c(paramString)))).c());
  }

  public static ListAdapter a(Context paramContext, ArrayList<PayFromAccount> paramArrayList)
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = paramArrayList.iterator();
    if (localIterator.hasNext())
    {
      PayFromAccount localPayFromAccount = (PayFromAccount)localIterator.next();
      String str;
      if (localPayFromAccount.e())
        str = "";
      while (true)
      {
        localArrayList.add(a(paramContext, localPayFromAccount.a(), str));
        break;
        if (localPayFromAccount.c() == null)
          str = null;
        else
          str = localPayFromAccount.c().h();
      }
    }
    return new SimpleAdapter(paramContext, localArrayList, 2130903122, a, c);
  }

  public static SimpleAdapter a(Context paramContext, List<IAccount> paramList)
  {
    return new SimpleAdapter(paramContext, h(paramContext, paramList), 2130903122, a, c);
  }

  private static List<Map<String, String>> a(List<Payee> paramList)
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = paramList.iterator();
    while (localIterator.hasNext())
    {
      Payee localPayee = (Payee)localIterator.next();
      HashMap localHashMap = new HashMap();
      localHashMap.put("nickname", localPayee.h());
      localArrayList.add(localHashMap);
    }
    return localArrayList;
  }

  private static Map<String, String> a(Context paramContext, String paramString1, String paramString2)
  {
    HashMap localHashMap = new HashMap();
    localHashMap.put("nickname", paramString1);
    if (paramString2 == null)
    {
      localHashMap.put("amount", "N/A");
      return localHashMap;
    }
    localHashMap.put("amount", s.a(paramContext, 2131165563, new Object[] { paramString2 }));
    return localHashMap;
  }

  public static SimpleAdapter b(Context paramContext, List<QuickDepositAccount> paramList)
  {
    return new b(paramContext, i(paramContext, paramList), b, d, paramList);
  }

  private static List<Map<String, String>> b(List<LabeledValue> paramList)
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = paramList.iterator();
    while (localIterator.hasNext())
    {
      LabeledValue localLabeledValue = (LabeledValue)localIterator.next();
      HashMap localHashMap = new HashMap();
      localHashMap.put("label", localLabeledValue.a());
      localArrayList.add(localHashMap);
    }
    return localArrayList;
  }

  public static SimpleAdapter c(Context paramContext, List<IAccount> paramList)
  {
    return new SimpleAdapter(paramContext, h(paramContext, paramList), 2130903122, a, c);
  }

  private static List<Map<String, String>> c(List<IAccount> paramList)
  {
    ChaseApplication localChaseApplication = ChaseApplication.a();
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = paramList.iterator();
    HashMap localHashMap;
    Dollar localDollar;
    if (localIterator.hasNext())
    {
      IAccount localIAccount = (IAccount)localIterator.next();
      localHashMap = new HashMap();
      localHashMap.put("nickname", localChaseApplication.b().b.a(localIAccount));
      String str1 = (String)localIAccount.e().get("nextPaymentAmount");
      if (!s.m(str1))
        break label149;
      localDollar = new Dollar(str1);
      if (!localDollar.a())
        break label149;
    }
    label149: for (String str2 = localDollar.h(); ; str2 = "N/A")
    {
      localHashMap.put("amount", str2);
      localArrayList.add(localHashMap);
      break;
      return localArrayList;
    }
  }

  public static SimpleAdapter d(Context paramContext, List<IAccount> paramList)
  {
    return new SimpleAdapter(paramContext, c(paramList), 2130903122, a, c);
  }

  public static SimpleAdapter e(Context paramContext, List<IAccount> paramList)
  {
    return new SimpleAdapter(paramContext, j(paramContext, paramList), 2130903122, a, c);
  }

  public static SimpleAdapter f(Context paramContext, List<Payee> paramList)
  {
    return new SimpleAdapter(paramContext, a(paramList), 2130903121, a, c);
  }

  public static SimpleAdapter g(Context paramContext, List<LabeledValue> paramList)
  {
    return new SimpleAdapter(paramContext, b(paramList), 2130903121, e, f);
  }

  private static List<Map<String, String>> h(Context paramContext, List<IAccount> paramList)
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = paramList.iterator();
    if (localIterator.hasNext())
    {
      IAccount localIAccount = (IAccount)localIterator.next();
      String str1 = localIAccount.a(ChaseApplication.a().i());
      if (localIAccount.j() == null);
      for (String str2 = null; ; str2 = localIAccount.j().h())
      {
        localArrayList.add(a(paramContext, str1, str2));
        break;
      }
    }
    return localArrayList;
  }

  private static List<Map<String, String>> i(Context paramContext, List<QuickDepositAccount> paramList)
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = paramList.iterator();
    if (localIterator.hasNext())
    {
      QuickDepositAccount localQuickDepositAccount = (QuickDepositAccount)localIterator.next();
      String str;
      if (localQuickDepositAccount.e().b())
        if (localQuickDepositAccount.f() == null)
          str = null;
      while (true)
      {
        Map localMap = a(paramContext, localQuickDepositAccount.b(), str);
        localMap.put("mask", "(" + localQuickDepositAccount.d() + ")");
        localArrayList.add(localMap);
        break;
        str = localQuickDepositAccount.f().h();
        continue;
        str = localQuickDepositAccount.e().a();
      }
    }
    return localArrayList;
  }

  private static List<Map<String, String>> j(Context paramContext, List<IAccount> paramList)
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = paramList.iterator();
    if (localIterator.hasNext())
    {
      IAccount localIAccount = (IAccount)localIterator.next();
      HashMap localHashMap = new HashMap();
      localHashMap.put("nickname", localIAccount.a(ChaseApplication.a().i()));
      Dollar localDollar;
      switch (localIAccount.s())
      {
      case 3:
      case 4:
      case 8:
      case 9:
      default:
        localDollar = localIAccount.k();
        label133: if (localDollar != null)
          break;
      case 7:
      case 10:
      case 2:
      case 5:
      case 6:
      }
      for (String str = "N/A"; ; str = localDollar.h())
      {
        if (localDollar != null)
          new Dollar("0");
        localHashMap.put("amount", s.a(paramContext, 2131165563, new Object[] { str }));
        localArrayList.add(localHashMap);
        break;
        localDollar = localIAccount.j();
        break label133;
        localDollar = localIAccount.k();
        break label133;
      }
    }
    return localArrayList;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.util.a
 * JD-Core Version:    0.6.2
 */