package com.chase.sig.android.util;

import android.content.Context;
import android.content.res.Resources;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.domain.QuickDepositAccount;
import com.chase.sig.android.domain.QuickDepositAccount.Authorization;
import java.util.List;

final class b extends SimpleAdapter
{
  b(Context paramContext, List paramList1, String[] paramArrayOfString, int[] paramArrayOfInt, List paramList2)
  {
    super(paramContext, paramList1, 2130903046, paramArrayOfString, paramArrayOfInt);
  }

  public final View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    View localView = super.getView(paramInt, paramView, paramViewGroup);
    TextView localTextView1 = (TextView)localView.findViewById(a.d[0]);
    TextView localTextView2 = (TextView)localView.findViewById(a.d[1]);
    TextView localTextView3 = (TextView)localView.findViewById(a.d[2]);
    Resources localResources;
    if (localTextView1 != null)
    {
      localResources = ChaseApplication.a().getResources();
      if (!isEnabled(paramInt))
        break label112;
    }
    label112: for (int i = 2131099653; ; i = 2131099655)
    {
      int j = localResources.getColor(i);
      localTextView1.setTextColor(j);
      localTextView2.setTextColor(j);
      localTextView3.setTextColor(j);
      return localView;
    }
  }

  public final boolean isEnabled(int paramInt)
  {
    return ((QuickDepositAccount)this.a.get(paramInt)).e().b();
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.util.b
 * JD-Core Version:    0.6.2
 */