package com.chase.sig.android.util;

import android.text.Editable;
import android.text.TextWatcher;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class d
  implements TextWatcher
{
  boolean a = true;
  CharSequence b = null;
  Matcher c = null;
  final double d = 9999999.9900000002D;

  public final void afterTextChanged(Editable paramEditable)
  {
    if (paramEditable.length() == 0);
    do
    {
      do
        return;
      while (this.a);
      if (Pattern.matches("\\d{0,7}[.]\\d{3}", paramEditable))
      {
        paramEditable.delete(-1 + paramEditable.length(), paramEditable.length());
        return;
      }
    }
    while (!Pattern.matches("[0-9]{10}+", paramEditable));
    paramEditable.delete(-3 + paramEditable.length(), paramEditable.length());
  }

  public final void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
  {
  }

  public final void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
  {
    if (Pattern.matches("\\d{1,7}", paramCharSequence) == true)
    {
      this.c = Pattern.compile("\\d{1,7}").matcher(paramCharSequence);
      if (Pattern.matches("[.]?\\d{0}", paramCharSequence.subSequence(this.c.regionEnd(), paramCharSequence.length())))
        this.a = true;
      return;
    }
    if (Pattern.matches("\\d{1,7}[.]\\d{0,2}", paramCharSequence) == true)
    {
      this.a = true;
      return;
    }
    this.a = false;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.util.d
 * JD-Core Version:    0.6.2
 */