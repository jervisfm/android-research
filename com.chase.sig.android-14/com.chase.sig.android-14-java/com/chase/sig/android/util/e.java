package com.chase.sig.android.util;

import android.content.Intent;
import android.os.Bundle;
import java.io.Serializable;

public final class e
{
  public static <T> T a(Bundle paramBundle, Intent paramIntent, String paramString)
  {
    if (a(paramBundle, paramString))
      return paramBundle.getSerializable(paramString);
    return paramIntent.getSerializableExtra(paramString);
  }

  public static <T> T a(Bundle paramBundle, String paramString, T paramT)
  {
    if (a(paramBundle, paramString))
      paramT = paramBundle.get(paramString);
    return paramT;
  }

  public static void a(Intent paramIntent, Serializable paramSerializable)
  {
    paramIntent.putExtra(paramSerializable.getClass().getCanonicalName(), paramSerializable);
  }

  public static boolean a(Bundle paramBundle, String paramString)
  {
    return (paramBundle != null) && (paramBundle.containsKey(paramString));
  }

  public static boolean b(Bundle paramBundle, String paramString)
  {
    boolean bool1 = a(paramBundle, paramString);
    boolean bool2 = false;
    if (bool1)
      bool2 = paramBundle.getBoolean(paramString, false);
    return bool2;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.util.e
 * JD-Core Version:    0.6.2
 */