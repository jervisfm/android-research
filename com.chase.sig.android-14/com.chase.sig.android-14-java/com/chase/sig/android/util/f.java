package com.chase.sig.android.util;

import android.content.Context;
import com.chase.sig.android.ChaseApplication;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.CookieStore;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.apache.http.cookie.Cookie;
import org.apache.http.cookie.CookieOrigin;
import org.apache.http.cookie.CookieSpec;
import org.apache.http.cookie.CookieSpecRegistry;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.impl.cookie.BrowserCompatSpec;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.BasicHttpContext;

public final class f
{
  private static DefaultHttpClient b;
  public String a;
  private BasicHttpContext c;
  private Context d;
  private ChaseApplication e;
  private HashMap<String, String> f;

  public f(ChaseApplication paramChaseApplication)
  {
    this.e = paramChaseApplication;
    this.d = this.e.getApplicationContext();
    this.c = new BasicHttpContext();
    new StringBuilder("Environment: ").append(this.e.f()).toString();
    if (b == null)
    {
      g localg = new g(this);
      BasicHttpParams localBasicHttpParams = new BasicHttpParams();
      HttpConnectionParams.setConnectionTimeout(localBasicHttpParams, 60000);
      HttpConnectionParams.setSoTimeout(localBasicHttpParams, 60000);
      localBasicHttpParams.setParameter("http.protocol.expect-continue", Boolean.valueOf(false));
      HttpProtocolParams.setVersion(localBasicHttpParams, HttpVersion.HTTP_1_1);
      SchemeRegistry localSchemeRegistry = new SchemeRegistry();
      SSLSocketFactory localSSLSocketFactory = SSLSocketFactory.getSocketFactory();
      localSSLSocketFactory.setHostnameVerifier((X509HostnameVerifier)SSLSocketFactory.STRICT_HOSTNAME_VERIFIER);
      localSchemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
      localSchemeRegistry.register(new Scheme("https", localSSLSocketFactory, 443));
      DefaultHttpClient localDefaultHttpClient = new DefaultHttpClient(new ThreadSafeClientConnManager(localBasicHttpParams, localSchemeRegistry), localBasicHttpParams);
      b = localDefaultHttpClient;
      localDefaultHttpClient.getCookieSpecs().register("jpspec", localg);
      b.getParams().setParameter("http.protocol.cookie-policy", "jpspec");
      b.setCookieStore(new k());
      b.getParams().setParameter("http.connection.stalecheck", Boolean.valueOf(true));
      b.setKeepAliveStrategy(new i(this));
      a(paramChaseApplication.getBaseContext());
    }
  }

  public f(ChaseApplication paramChaseApplication, HashMap<String, String> paramHashMap)
  {
    this(paramChaseApplication);
    this.f = paramHashMap;
  }

  public static String a(Hashtable<String, String> paramHashtable)
  {
    Enumeration localEnumeration = paramHashtable.keys();
    StringBuffer localStringBuffer = new StringBuffer();
    while (localEnumeration.hasMoreElements())
    {
      Object localObject = localEnumeration.nextElement();
      String str = (String)paramHashtable.get(localObject);
      try
      {
        localStringBuffer.append(localObject + "=" + URLEncoder.encode(str, "UTF-8") + "&");
      }
      catch (UnsupportedEncodingException localUnsupportedEncodingException)
      {
        String.format("Request parameter could not be URL encoded: %s", new Object[] { str });
      }
    }
    return localStringBuffer.toString();
  }

  private static void a(Context paramContext)
  {
    String[] arrayOfString = paramContext.fileList();
    if ((arrayOfString == null) || (arrayOfString.length <= 0));
    label20: label206: 
    while (true)
    {
      return;
      int i = arrayOfString.length;
      int j = 0;
      if (j < i)
        if (!arrayOfString[j].equals("CookieStore"));
      for (int k = 1; ; k = 0)
        while (true)
        {
          if (k == 0)
            break label206;
          try
          {
            ObjectInputStream localObjectInputStream = new ObjectInputStream(paramContext.openFileInput("CookieStore"));
            localArrayList = (ArrayList)localObjectInputStream.readObject();
            localObjectInputStream.close();
            CookieStore localCookieStore = b.getCookieStore();
            localCookieStore.clear();
            Iterator localIterator = localArrayList.iterator();
            while (localIterator.hasNext())
            {
              Cookie localCookie = (Cookie)localIterator.next();
              if (!localCookie.isExpired(new Date()))
                localCookieStore.addCookie(localCookie);
            }
          }
          catch (FileNotFoundException localFileNotFoundException)
          {
            ArrayList localArrayList;
            for (m = 0; m == 0; m = 1)
            {
              paramContext.deleteFile("CookieStore");
              return;
              j++;
              break label20;
              new Object[1][0] = Integer.valueOf(localArrayList.size());
            }
          }
          catch (Exception localException)
          {
            while (true)
              int m = 0;
          }
        }
    }
  }

  private static void a(HttpResponse paramHttpResponse)
  {
    Header[] arrayOfHeader = paramHttpResponse.getAllHeaders();
    int i = 0;
    while (true)
      if (i < arrayOfHeader.length)
      {
        Header localHeader = arrayOfHeader[i];
        Object[] arrayOfObject = new Object[2];
        arrayOfObject[0] = localHeader.getName();
        arrayOfObject[1] = localHeader.getValue();
        String.format("Response Header - %s: %s", arrayOfObject);
        String str4;
        label174: String str5;
        BasicClientCookie localBasicClientCookie;
        if ((localHeader.getName().equalsIgnoreCase("Set-Cookie")) && (localHeader.getValue().startsWith("adtoken.")))
        {
          String[] arrayOfString1 = localHeader.getValue().split(";\\s*");
          String str1 = arrayOfString1[0];
          int j = 1;
          String str2 = null;
          String str3 = null;
          str4 = null;
          if (j < arrayOfString1.length)
          {
            String[] arrayOfString3;
            if (arrayOfString1[j].indexOf('=') > 0)
            {
              arrayOfString3 = arrayOfString1[j].split("=");
              if (!"expires".equalsIgnoreCase(arrayOfString3[0]))
                break label174;
              str4 = arrayOfString3[1];
            }
            while (true)
            {
              j++;
              break;
              if ("domain".equalsIgnoreCase(arrayOfString3[0]))
                str2 = arrayOfString3[1];
              else if ("path".equalsIgnoreCase(arrayOfString3[0]))
                str3 = arrayOfString3[1];
            }
          }
          if (str1.contains("="))
          {
            String[] arrayOfString2 = str1.split("=");
            str5 = arrayOfString2[0];
            localBasicClientCookie = new BasicClientCookie(str5, arrayOfString2[1]);
            localBasicClientCookie.setDomain(str2);
            localBasicClientCookie.setPath(str3);
          }
        }
        try
        {
          localBasicClientCookie.setExpiryDate(s.t(str4));
          b.getCookieStore().addCookie(localBasicClientCookie);
          i++;
        }
        catch (IllegalArgumentException localIllegalArgumentException)
        {
          while (true)
            new StringBuilder("ERROR - unable to set expire date for cookie with NAME '").append(str5).append("'").toString();
        }
      }
  }

  private static void a(HttpPost paramHttpPost, String paramString1, String paramString2)
  {
    new Object[] { paramString1, paramString2 };
    paramHttpPost.setHeader(paramString1, paramString2);
  }

  public static List<Cookie> b()
  {
    if (b != null)
      return b.getCookieStore().getCookies();
    return new ArrayList();
  }

  private void c()
  {
    if (b == null)
      return;
    try
    {
      CookieStore localCookieStore = b.getCookieStore();
      ArrayList localArrayList = new ArrayList();
      new Object[1][0] = Integer.valueOf(localCookieStore.getCookies().size());
      Iterator localIterator = localCookieStore.getCookies().iterator();
      while (localIterator.hasNext())
      {
        Cookie localCookie = (Cookie)localIterator.next();
        if ((localCookie.isPersistent()) && (!localCookie.isExpired(new Date())))
        {
          Object[] arrayOfObject3 = new Object[2];
          arrayOfObject3[0] = localCookie.getName();
          arrayOfObject3[1] = localCookie.getValue();
          localArrayList.add(new SerializableCookie(localCookie));
        }
        else
        {
          Object[] arrayOfObject2 = new Object[2];
          arrayOfObject2[0] = localCookie.getName();
          arrayOfObject2[1] = localCookie.getValue();
        }
      }
      ObjectOutputStream localObjectOutputStream = new ObjectOutputStream(this.d.openFileOutput("CookieStore", 0));
      localObjectOutputStream.writeObject(localArrayList);
      localObjectOutputStream.close();
      Object[] arrayOfObject1 = new Object[1];
      arrayOfObject1[0] = Integer.valueOf(localArrayList.size());
      String.format("Persisted %d cookies", arrayOfObject1);
      return;
    }
    catch (IOException localIOException)
    {
    }
  }

  private static void d()
  {
    Iterator localIterator = b().iterator();
    while (localIterator.hasNext())
    {
      Cookie localCookie = (Cookie)localIterator.next();
      Object[] arrayOfObject = new Object[4];
      arrayOfObject[0] = localCookie.getName();
      arrayOfObject[1] = localCookie.getDomain();
      arrayOfObject[2] = localCookie.getValue();
      arrayOfObject[3] = localCookie.getExpiryDate();
      String.format("HttpRequest Cookie: name=%s, domain=%s, value=%s, expires=%s", arrayOfObject);
    }
  }

  // ERROR //
  public final a a(String paramString1, String paramString2, boolean paramBoolean)
  {
    // Byte code:
    //   0: new 402	org/apache/http/client/methods/HttpPost
    //   3: dup
    //   4: aload_1
    //   5: invokespecial 461	org/apache/http/client/methods/HttpPost:<init>	(Ljava/lang/String;)V
    //   8: astore 4
    //   10: aload_0
    //   11: getfield 24	com/chase/sig/android/util/f:e	Lcom/chase/sig/android/ChaseApplication;
    //   14: invokevirtual 464	com/chase/sig/android/ChaseApplication:m	()Ljava/lang/String;
    //   17: astore 5
    //   19: iconst_3
    //   20: anewarray 4	java/lang/Object
    //   23: astore 6
    //   25: aload 6
    //   27: iconst_0
    //   28: getstatic 469	android/os/Build$VERSION:RELEASE	Ljava/lang/String;
    //   31: aastore
    //   32: aload 6
    //   34: iconst_1
    //   35: aload_0
    //   36: getfield 24	com/chase/sig/android/util/f:e	Lcom/chase/sig/android/ChaseApplication;
    //   39: invokevirtual 471	com/chase/sig/android/ChaseApplication:d	()Ljava/lang/String;
    //   42: aastore
    //   43: aload 6
    //   45: iconst_2
    //   46: aload 5
    //   48: aastore
    //   49: aload 4
    //   51: ldc_w 473
    //   54: ldc_w 475
    //   57: aload 6
    //   59: invokestatic 244	java/lang/String:format	(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    //   62: invokestatic 477	com/chase/sig/android/util/f:a	(Lorg/apache/http/client/methods/HttpPost;Ljava/lang/String;Ljava/lang/String;)V
    //   65: aload 4
    //   67: ldc_w 479
    //   70: aload_0
    //   71: getfield 24	com/chase/sig/android/util/f:e	Lcom/chase/sig/android/ChaseApplication;
    //   74: invokevirtual 482	com/chase/sig/android/ChaseApplication:h	()Ljava/lang/String;
    //   77: invokestatic 477	com/chase/sig/android/util/f:a	(Lorg/apache/http/client/methods/HttpPost;Ljava/lang/String;Ljava/lang/String;)V
    //   80: aload 4
    //   82: ldc_w 484
    //   85: ldc_w 486
    //   88: invokestatic 477	com/chase/sig/android/util/f:a	(Lorg/apache/http/client/methods/HttpPost;Ljava/lang/String;Ljava/lang/String;)V
    //   91: aload_0
    //   92: getfield 488	com/chase/sig/android/util/f:a	Ljava/lang/String;
    //   95: invokestatic 491	com/chase/sig/android/util/s:l	(Ljava/lang/String;)Z
    //   98: ifeq +162 -> 260
    //   101: ldc_w 493
    //   104: astore 7
    //   106: aload 4
    //   108: ldc_w 495
    //   111: aload 7
    //   113: invokestatic 477	com/chase/sig/android/util/f:a	(Lorg/apache/http/client/methods/HttpPost;Ljava/lang/String;Ljava/lang/String;)V
    //   116: aload 4
    //   118: ldc_w 497
    //   121: ldc_w 499
    //   124: invokestatic 477	com/chase/sig/android/util/f:a	(Lorg/apache/http/client/methods/HttpPost;Ljava/lang/String;Ljava/lang/String;)V
    //   127: aload 4
    //   129: ldc_w 501
    //   132: ldc_w 503
    //   135: invokestatic 477	com/chase/sig/android/util/f:a	(Lorg/apache/http/client/methods/HttpPost;Ljava/lang/String;Ljava/lang/String;)V
    //   138: aload 4
    //   140: ldc_w 505
    //   143: new 507	java/net/URL
    //   146: dup
    //   147: aload_1
    //   148: invokespecial 508	java/net/URL:<init>	(Ljava/lang/String;)V
    //   151: invokevirtual 511	java/net/URL:getHost	()Ljava/lang/String;
    //   154: invokevirtual 405	org/apache/http/client/methods/HttpPost:setHeader	(Ljava/lang/String;Ljava/lang/String;)V
    //   157: aload 4
    //   159: new 513	org/apache/http/entity/StringEntity
    //   162: dup
    //   163: aload_2
    //   164: ldc 227
    //   166: invokespecial 514	org/apache/http/entity/StringEntity:<init>	(Ljava/lang/String;Ljava/lang/String;)V
    //   169: invokevirtual 518	org/apache/http/client/methods/HttpPost:setEntity	(Lorg/apache/http/HttpEntity;)V
    //   172: iconst_1
    //   173: anewarray 4	java/lang/Object
    //   176: dup
    //   177: iconst_0
    //   178: aload_1
    //   179: aastore
    //   180: pop
    //   181: iconst_1
    //   182: anewarray 4	java/lang/Object
    //   185: dup
    //   186: iconst_0
    //   187: aload_2
    //   188: aastore
    //   189: pop
    //   190: invokestatic 520	com/chase/sig/android/util/f:d	()V
    //   193: aconst_null
    //   194: astore 12
    //   196: getstatic 56	com/chase/sig/android/util/f:b	Lorg/apache/http/impl/client/DefaultHttpClient;
    //   199: aload 4
    //   201: aload_0
    //   202: getfield 37	com/chase/sig/android/util/f:c	Lorg/apache/http/protocol/BasicHttpContext;
    //   205: invokevirtual 524	org/apache/http/impl/client/DefaultHttpClient:execute	(Lorg/apache/http/client/methods/HttpUriRequest;Lorg/apache/http/protocol/HttpContext;)Lorg/apache/http/HttpResponse;
    //   208: astore 15
    //   210: aload 15
    //   212: invokeinterface 528 1 0
    //   217: astore 16
    //   219: aconst_null
    //   220: astore 12
    //   222: aload 16
    //   224: ifnull +75 -> 299
    //   227: aload 16
    //   229: invokeinterface 533 1 0
    //   234: istore 17
    //   236: aconst_null
    //   237: astore 12
    //   239: iload 17
    //   241: sipush 403
    //   244: if_icmpne +55 -> 299
    //   247: new 535	com/chase/sig/android/service/JPService$CrossSiteRequestForgeryTokenFailureException
    //   250: dup
    //   251: invokespecial 536	com/chase/sig/android/service/JPService$CrossSiteRequestForgeryTokenFailureException:<init>	()V
    //   254: athrow
    //   255: astore 14
    //   257: aload 14
    //   259: athrow
    //   260: aload_0
    //   261: getfield 488	com/chase/sig/android/util/f:a	Ljava/lang/String;
    //   264: astore 7
    //   266: goto -160 -> 106
    //   269: astore 9
    //   271: new 460	com/chase/sig/android/util/ChaseException
    //   274: dup
    //   275: aload 9
    //   277: ldc_w 538
    //   280: invokespecial 541	com/chase/sig/android/util/ChaseException:<init>	(Ljava/lang/Exception;Ljava/lang/String;)V
    //   283: athrow
    //   284: astore 8
    //   286: new 460	com/chase/sig/android/util/ChaseException
    //   289: dup
    //   290: aload 8
    //   292: ldc_w 543
    //   295: invokespecial 541	com/chase/sig/android/util/ChaseException:<init>	(Ljava/lang/Exception;Ljava/lang/String;)V
    //   298: athrow
    //   299: aload 15
    //   301: invokeinterface 547 1 0
    //   306: astore 18
    //   308: aconst_null
    //   309: astore 12
    //   311: iload_3
    //   312: ifeq +62 -> 374
    //   315: new 549	com/chase/sig/android/util/f$a
    //   318: dup
    //   319: aload 18
    //   321: invokestatic 554	org/apache/http/util/EntityUtils:toString	(Lorg/apache/http/HttpEntity;)Ljava/lang/String;
    //   324: invokespecial 555	com/chase/sig/android/util/f$a:<init>	(Ljava/lang/String;)V
    //   327: astore 12
    //   329: aload 18
    //   331: invokeinterface 560 1 0
    //   336: aload 15
    //   338: invokestatic 562	com/chase/sig/android/util/f:a	(Lorg/apache/http/HttpResponse;)V
    //   341: invokestatic 565	com/chase/sig/android/ChaseApplication:n	()V
    //   344: aload 12
    //   346: getfield 568	com/chase/sig/android/util/f$a:b	[B
    //   349: ifnull +18 -> 367
    //   352: iconst_1
    //   353: anewarray 4	java/lang/Object
    //   356: iconst_0
    //   357: aload 12
    //   359: getfield 568	com/chase/sig/android/util/f$a:b	[B
    //   362: arraylength
    //   363: invokestatic 325	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   366: aastore
    //   367: aload_0
    //   368: invokespecial 570	com/chase/sig/android/util/f:c	()V
    //   371: aload 12
    //   373: areturn
    //   374: new 549	com/chase/sig/android/util/f$a
    //   377: dup
    //   378: aload 18
    //   380: invokestatic 574	org/apache/http/util/EntityUtils:toByteArray	(Lorg/apache/http/HttpEntity;)[B
    //   383: invokespecial 577	com/chase/sig/android/util/f$a:<init>	([B)V
    //   386: astore 19
    //   388: aload 19
    //   390: astore 12
    //   392: goto -63 -> 329
    //   395: astore 13
    //   397: aload 12
    //   399: areturn
    //
    // Exception table:
    //   from	to	target	type
    //   196	219	255	com/chase/sig/android/util/ChaseException
    //   227	236	255	com/chase/sig/android/util/ChaseException
    //   247	255	255	com/chase/sig/android/util/ChaseException
    //   299	308	255	com/chase/sig/android/util/ChaseException
    //   315	329	255	com/chase/sig/android/util/ChaseException
    //   329	367	255	com/chase/sig/android/util/ChaseException
    //   367	371	255	com/chase/sig/android/util/ChaseException
    //   374	388	255	com/chase/sig/android/util/ChaseException
    //   138	172	269	java/io/UnsupportedEncodingException
    //   138	172	284	java/net/MalformedURLException
    //   196	219	395	java/lang/Exception
    //   227	236	395	java/lang/Exception
    //   247	255	395	java/lang/Exception
    //   299	308	395	java/lang/Exception
    //   315	329	395	java/lang/Exception
    //   329	367	395	java/lang/Exception
    //   367	371	395	java/lang/Exception
    //   374	388	395	java/lang/Exception
  }

  public final String a(String paramString1, String paramString2)
  {
    return a(paramString1, paramString2, true).a;
  }

  public final List<Cookie> a(CookieOrigin paramCookieOrigin)
  {
    List localList = b();
    ArrayList localArrayList = new ArrayList();
    BrowserCompatSpec localBrowserCompatSpec = new BrowserCompatSpec();
    Iterator localIterator = localList.iterator();
    while (localIterator.hasNext())
    {
      Cookie localCookie = (Cookie)localIterator.next();
      if (localCookie.getName().equals("jisi.web.cacheId"))
        localCookie = a(localCookie);
      if (localBrowserCompatSpec.match(localCookie, paramCookieOrigin))
        localArrayList.add(localCookie);
    }
    return localArrayList;
  }

  public final HttpResponse a(String paramString)
  {
    HttpGet localHttpGet = new HttpGet(paramString);
    new StringBuilder("URL: ").append(paramString).toString();
    d();
    if (this.f != null)
    {
      Iterator localIterator = this.f.keySet().iterator();
      while (localIterator.hasNext())
      {
        String str = (String)localIterator.next();
        localHttpGet.setHeader(str, (String)this.f.get(str));
      }
    }
    try
    {
      HttpResponse localHttpResponse = b.execute(localHttpGet, this.c);
      return localHttpResponse;
    }
    catch (Exception localException)
    {
    }
    return null;
  }

  final Cookie a(Cookie paramCookie)
  {
    return new j(this, paramCookie);
  }

  public final void a()
  {
    if (b != null)
      b.getCookieStore().clear();
    if (this.e != null)
      a(this.e.getApplicationContext());
  }

  private static final class a
  {
    public String a;
    public byte[] b;

    public a(String paramString)
    {
      this.a = paramString;
    }

    public a(byte[] paramArrayOfByte)
    {
      this.b = paramArrayOfByte;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.util.f
 * JD-Core Version:    0.6.2
 */