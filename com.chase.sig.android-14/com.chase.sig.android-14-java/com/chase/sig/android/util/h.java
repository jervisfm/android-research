package com.chase.sig.android.util;

import org.apache.http.cookie.Cookie;
import org.apache.http.cookie.CookieOrigin;
import org.apache.http.impl.cookie.BrowserCompatSpec;

final class h extends BrowserCompatSpec
{
  h(g paramg)
  {
  }

  public final boolean match(Cookie paramCookie, CookieOrigin paramCookieOrigin)
  {
    if (paramCookie.getName().equals("jisi.web.cacheId"))
      paramCookie = this.a.a.a(paramCookie);
    return super.match(paramCookie, paramCookieOrigin);
  }

  public final void validate(Cookie paramCookie, CookieOrigin paramCookieOrigin)
  {
    if (!paramCookie.getName().startsWith("adtoken."))
      super.validate(paramCookie, paramCookieOrigin);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.util.h
 * JD-Core Version:    0.6.2
 */