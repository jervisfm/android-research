package com.chase.sig.android.util.imagebrowser;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.View;
import android.view.View.BaseSavedState;
import android.widget.GridView;
import android.widget.ListAdapter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class GridViewCompat extends GridView
{
  private static final String e = GridViewCompat.class.getSimpleName();
  private static boolean f = false;
  private static Method g;
  private static Method h;
  private static Method i;
  private static Method j;
  private static Method k;
  private static Method l;
  private static Method m;
  private static Method n;
  private static Method o;
  int a;
  SparseBooleanArray b;
  b<Integer> c;
  int d = 2;

  static
  {
    try
    {
      f = false;
      n = GridView.class.getMethod("getChoiceMode", null);
      g = GridView.class.getMethod("getCheckedItemIds", null);
      Class[] arrayOfClass1 = new Class[1];
      arrayOfClass1[0] = Integer.TYPE;
      h = GridView.class.getMethod("isItemChecked", arrayOfClass1);
      i = GridView.class.getMethod("getCheckedItemPosition", null);
      j = GridView.class.getMethod("getCheckedItemPositions", null);
      k = GridView.class.getMethod("clearChoices", null);
      Class[] arrayOfClass2 = new Class[2];
      arrayOfClass2[0] = Integer.TYPE;
      arrayOfClass2[1] = Boolean.TYPE;
      l = GridView.class.getMethod("setItemChecked", arrayOfClass2);
      Class[] arrayOfClass3 = new Class[1];
      arrayOfClass3[0] = Integer.TYPE;
      m = GridView.class.getMethod("setChoiceMode", arrayOfClass3);
      o = GridView.class.getMethod("getCheckedItemCount", null);
      return;
    }
    catch (NoSuchMethodException localNoSuchMethodException)
    {
      Log.d(e, "Running in compatibility mode as '" + localNoSuchMethodException.getMessage() + "' not found");
      f = true;
      g = null;
      h = null;
      i = null;
      j = null;
      k = null;
      l = null;
      m = null;
      n = null;
      o = null;
    }
  }

  public GridViewCompat(Context paramContext)
  {
    super(paramContext);
  }

  public GridViewCompat(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }

  public GridViewCompat(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
  }

  public final void a(int paramInt, boolean paramBoolean)
  {
    if (!f);
    try
    {
      Method localMethod = l;
      Object[] arrayOfObject = new Object[2];
      arrayOfObject[0] = Integer.valueOf(paramInt);
      arrayOfObject[1] = Boolean.valueOf(paramBoolean);
      localMethod.invoke(this, arrayOfObject);
      do
        return;
      while (this.d == 0);
      if (this.d == 2)
      {
        boolean bool = this.b.get(paramInt);
        this.b.put(paramInt, paramBoolean);
        if ((this.c != null) && (getAdapter().hasStableIds()))
        {
          if (paramBoolean)
            this.c.a(getAdapter().getItemId(paramInt), Integer.valueOf(paramInt));
        }
        else if (bool != paramBoolean)
        {
          if (!paramBoolean)
            break label166;
          this.a = (1 + this.a);
        }
      }
      while (true)
      {
        invalidateViews();
        return;
        this.c.a(getAdapter().getItemId(paramInt));
        break;
        label166: this.a = (-1 + this.a);
        continue;
        if ((this.c != null) && (getAdapter().hasStableIds()));
        for (int i1 = 1; ; i1 = 0)
        {
          if ((paramBoolean) || (a(paramInt)))
          {
            this.b.clear();
            if (i1 != 0)
              this.c.b();
          }
          if (!paramBoolean)
            break label281;
          this.b.put(paramInt, true);
          if (i1 != 0)
            this.c.a(getAdapter().getItemId(paramInt), Integer.valueOf(paramInt));
          this.a = 1;
          break;
        }
        label281: if ((this.b.size() == 0) || (!this.b.valueAt(0)))
          this.a = 0;
      }
    }
    catch (InvocationTargetException localInvocationTargetException)
    {
    }
    catch (IllegalAccessException localIllegalAccessException)
    {
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
    }
  }

  public final boolean a(int paramInt)
  {
    if (!f);
    try
    {
      Method localMethod = h;
      Object[] arrayOfObject = new Object[1];
      arrayOfObject[0] = Integer.valueOf(paramInt);
      boolean bool = ((Boolean)localMethod.invoke(this, arrayOfObject)).booleanValue();
      return bool;
    }
    catch (InvocationTargetException localInvocationTargetException)
    {
      if ((this.d != 0) && (this.b != null))
        return this.b.get(paramInt);
      return false;
    }
    catch (IllegalAccessException localIllegalAccessException)
    {
      break label46;
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      label46: break label46;
    }
  }

  public int getCheckedItemCountC()
  {
    if (!f);
    try
    {
      int i1 = ((Integer)o.invoke(this, null)).intValue();
      return i1;
    }
    catch (InvocationTargetException localInvocationTargetException)
    {
      return this.a;
    }
    catch (IllegalAccessException localIllegalAccessException)
    {
      break label26;
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      label26: break label26;
    }
  }

  public long[] getCheckedItemIdsC()
  {
    int i1 = 0;
    if (!f);
    try
    {
      arrayOfLong = (long[])g.invoke(this, null);
      return arrayOfLong;
    }
    catch (InvocationTargetException localInvocationTargetException)
    {
      if ((this.d == 0) || (this.c == null) || (getAdapter() == null))
        return new long[0];
      b localb = this.c;
      int i2 = localb.a();
      long[] arrayOfLong = new long[i2];
      while (i1 < i2)
      {
        arrayOfLong[i1] = localb.a(i1);
        i1++;
      }
    }
    catch (IllegalAccessException localIllegalAccessException)
    {
      break label29;
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      label29: break label29;
    }
  }

  public int getCheckedItemPositionC()
  {
    if (!f);
    try
    {
      int i1 = ((Integer)i.invoke(this, null)).intValue();
      return i1;
    }
    catch (InvocationTargetException localInvocationTargetException)
    {
      if ((this.d == 1) && (this.b != null) && (this.b.size() == 1))
        return this.b.keyAt(0);
      return -1;
    }
    catch (IllegalAccessException localIllegalAccessException)
    {
      break label26;
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      label26: break label26;
    }
  }

  public SparseBooleanArray getCheckedItemPositionsC()
  {
    if (!f);
    try
    {
      SparseBooleanArray localSparseBooleanArray = (SparseBooleanArray)j.invoke(this, null);
      return localSparseBooleanArray;
    }
    catch (InvocationTargetException localInvocationTargetException)
    {
      if (this.d != 0)
        return this.b;
      return null;
    }
    catch (IllegalAccessException localIllegalAccessException)
    {
      break label23;
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      label23: break label23;
    }
  }

  public int getChoiceModeC()
  {
    if ((!f) && (n != null));
    try
    {
      int i1 = ((Integer)n.invoke(this, null)).intValue();
      return i1;
    }
    catch (InvocationTargetException localInvocationTargetException)
    {
      return this.d;
    }
    catch (IllegalAccessException localIllegalAccessException)
    {
      break label32;
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      label32: break label32;
    }
  }

  public void onRestoreInstanceState(Parcelable paramParcelable)
  {
    if (!f)
    {
      super.onRestoreInstanceState(paramParcelable);
      return;
    }
    SavedState localSavedState = (SavedState)paramParcelable;
    super.onRestoreInstanceState(localSavedState.getSuperState());
    if (localSavedState.b != null)
      this.b = localSavedState.b;
    if (localSavedState.c != null)
      this.c = localSavedState.c;
    this.a = localSavedState.a;
    invalidateViews();
  }

  public Parcelable onSaveInstanceState()
  {
    int i1 = 0;
    if (!f)
      return super.onSaveInstanceState();
    SavedState localSavedState = new SavedState(super.onSaveInstanceState());
    if (this.b != null)
    {
      SparseBooleanArray localSparseBooleanArray1 = this.b;
      SparseBooleanArray localSparseBooleanArray2 = new SparseBooleanArray();
      int i3 = localSparseBooleanArray1.size();
      for (int i4 = 0; i4 < i3; i4++)
      {
        int i5 = localSparseBooleanArray1.keyAt(i4);
        localSparseBooleanArray2.put(i5, localSparseBooleanArray1.get(i5));
      }
      localSavedState.b = localSparseBooleanArray2;
    }
    if (this.c != null)
    {
      b localb = new b();
      int i2 = this.c.a();
      while (i1 < i2)
      {
        localb.a(this.c.a(i1), this.c.b(i1));
        i1++;
      }
      localSavedState.c = localb;
    }
    localSavedState.a = this.a;
    return localSavedState;
  }

  public boolean performItemClick(View paramView, int paramInt, long paramLong)
  {
    int i1 = 1;
    if (!f)
      return super.performItemClick(paramView, paramInt, paramLong);
    if (this.d != 0)
      if (this.d == 2)
      {
        boolean bool1 = this.b.get(paramInt, false);
        boolean bool2 = false;
        if (!bool1)
          bool2 = i1;
        this.b.put(paramInt, bool2);
        if ((this.c != null) && (getAdapter().hasStableIds()))
        {
          if (bool2)
            this.c.a(getAdapter().getItemId(paramInt), Integer.valueOf(paramInt));
        }
        else
        {
          if (!bool2)
            break label160;
          this.a = (1 + this.a);
          label125: invalidateViews();
        }
      }
    while (true)
    {
      return i1 | super.performItemClick(paramView, paramInt, paramLong);
      this.c.a(getAdapter().getItemId(paramInt));
      break;
      label160: this.a = (-1 + this.a);
      break label125;
      if (this.d != i1)
        break label125;
      if (!this.b.get(paramInt, false));
      int i4;
      for (int i3 = i1; ; i4 = 0)
      {
        if (i3 == 0)
          break label282;
        this.b.clear();
        this.b.put(paramInt, i1);
        if ((this.c != null) && (getAdapter().hasStableIds()))
        {
          this.c.b();
          this.c.a(getAdapter().getItemId(paramInt), Integer.valueOf(paramInt));
        }
        this.a = i1;
        break;
      }
      label282: if ((this.b.size() != 0) && (this.b.valueAt(0)))
        break label125;
      this.a = 0;
      break label125;
      int i2 = 0;
    }
  }

  public void setAdapter(ListAdapter paramListAdapter)
  {
    if (!f)
      super.setAdapter(paramListAdapter);
    do
    {
      return;
      super.setAdapter(paramListAdapter);
      if ((paramListAdapter != null) && (this.d != 0) && (getAdapter().hasStableIds()) && (this.c == null))
        this.c = new b();
      if (this.b != null)
        this.b.clear();
    }
    while (this.c == null);
    this.c.b();
  }

  public void setChoiceModeC(int paramInt)
  {
    if (!f);
    try
    {
      Method localMethod = m;
      Object[] arrayOfObject = new Object[1];
      arrayOfObject[0] = Integer.valueOf(paramInt);
      localMethod.invoke(this, arrayOfObject);
      do
      {
        do
        {
          return;
          this.d = paramInt;
        }
        while (this.d == 0);
        if (this.b == null)
          this.b = new SparseBooleanArray();
      }
      while ((this.c != null) || (getAdapter() == null) || (!getAdapter().hasStableIds()));
      this.c = new b();
      return;
    }
    catch (InvocationTargetException localInvocationTargetException)
    {
    }
    catch (IllegalAccessException localIllegalAccessException)
    {
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
    }
  }

  static class SavedState extends View.BaseSavedState
  {
    public static final Parcelable.Creator<SavedState> CREATOR = new b();
    int a;
    SparseBooleanArray b;
    GridViewCompat.b<Integer> c;

    private SavedState(Parcel paramParcel)
    {
      super();
      this.a = paramParcel.readInt();
      this.b = paramParcel.readSparseBooleanArray();
      int i = paramParcel.readInt();
      if (i > 0)
      {
        this.c = new GridViewCompat.b();
        for (int j = 0; j < i; j++)
        {
          long l = paramParcel.readLong();
          int k = paramParcel.readInt();
          this.c.a(l, Integer.valueOf(k));
        }
      }
    }

    SavedState(Parcelable paramParcelable)
    {
      super();
    }

    public String toString()
    {
      return "AbsListView.SavedState{" + Integer.toHexString(System.identityHashCode(this)) + " checkState=" + this.b + "}";
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
      int i = 0;
      super.writeToParcel(paramParcel, paramInt);
      paramParcel.writeInt(this.a);
      paramParcel.writeSparseBooleanArray(this.b);
      if (this.c != null);
      for (int j = this.c.a(); ; j = 0)
      {
        paramParcel.writeInt(j);
        while (i < j)
        {
          paramParcel.writeLong(this.c.a(i));
          paramParcel.writeInt(((Integer)this.c.b(i)).intValue());
          i++;
        }
      }
    }
  }

  private static final class a
  {
    private static Object[] a = new Object[0];
    private static Object[] b = new Object[73];

    public static int a(int paramInt)
    {
      int i = paramInt * 4;
      for (int j = 4; ; j++)
        if (j < 32)
        {
          if (i <= -12 + (1 << j))
            i = -12 + (1 << j);
        }
        else
          return i / 4;
    }
  }

  private static final class b<E>
  {
    private static final Object a = new Object();
    private boolean b = false;
    private long[] c;
    private Object[] d;
    private int e;

    public b()
    {
      this((byte)0);
    }

    private b(byte paramByte)
    {
      int i = GridViewCompat.a.a(10);
      this.c = new long[i];
      this.d = new Object[i];
      this.e = 0;
    }

    private static int a(long[] paramArrayOfLong, int paramInt, long paramLong)
    {
      int i = paramInt + 0;
      int j = -1;
      int k = i;
      while (k - j > 1)
      {
        int m = (k + j) / 2;
        if (paramArrayOfLong[m] < paramLong)
          j = m;
        else
          k = m;
      }
      if (k == paramInt + 0)
        k = 0xFFFFFFFF ^ paramInt + 0;
      while (paramArrayOfLong[k] == paramLong)
        return k;
      return k ^ 0xFFFFFFFF;
    }

    private void c()
    {
      int i = this.e;
      long[] arrayOfLong = this.c;
      Object[] arrayOfObject = this.d;
      int j = 0;
      int k = 0;
      while (j < i)
      {
        Object localObject = arrayOfObject[j];
        if (localObject != a)
        {
          if (j != k)
          {
            arrayOfLong[k] = arrayOfLong[j];
            arrayOfObject[k] = localObject;
          }
          k++;
        }
        j++;
      }
      this.b = false;
      this.e = k;
    }

    public final int a()
    {
      if (this.b)
        c();
      return this.e;
    }

    public final long a(int paramInt)
    {
      if (this.b)
        c();
      return this.c[paramInt];
    }

    public final void a(long paramLong)
    {
      int i = a(this.c, this.e, paramLong);
      if ((i >= 0) && (this.d[i] != a))
      {
        this.d[i] = a;
        this.b = true;
      }
    }

    public final void a(long paramLong, E paramE)
    {
      int i = a(this.c, this.e, paramLong);
      if (i >= 0)
      {
        this.d[i] = paramE;
        return;
      }
      int j = i ^ 0xFFFFFFFF;
      if ((j < this.e) && (this.d[j] == a))
      {
        this.c[j] = paramLong;
        this.d[j] = paramE;
        return;
      }
      if ((this.b) && (this.e >= this.c.length))
      {
        c();
        j = 0xFFFFFFFF ^ a(this.c, this.e, paramLong);
      }
      if (this.e >= this.c.length)
      {
        int k = GridViewCompat.a.a(1 + this.e);
        long[] arrayOfLong = new long[k];
        Object[] arrayOfObject = new Object[k];
        System.arraycopy(this.c, 0, arrayOfLong, 0, this.c.length);
        System.arraycopy(this.d, 0, arrayOfObject, 0, this.d.length);
        this.c = arrayOfLong;
        this.d = arrayOfObject;
      }
      if (this.e - j != 0)
      {
        System.arraycopy(this.c, j, this.c, j + 1, this.e - j);
        System.arraycopy(this.d, j, this.d, j + 1, this.e - j);
      }
      this.c[j] = paramLong;
      this.d[j] = paramE;
      this.e = (1 + this.e);
    }

    public final E b(int paramInt)
    {
      if (this.b)
        c();
      return this.d[paramInt];
    }

    public final void b()
    {
      int i = this.e;
      Object[] arrayOfObject = this.d;
      for (int j = 0; j < i; j++)
        arrayOfObject[j] = null;
      this.e = 0;
      this.b = false;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.util.imagebrowser.GridViewCompat
 * JD-Core Version:    0.6.2
 */