package com.chase.sig.android.util.imagebrowser;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.RejectedExecutionException;

public class ImageBrowserActivity extends Activity
{
  private static final String e = ImageBrowserActivity.class.getSimpleName();
  GridViewCompat a;
  Button b;
  b c;
  int d;

  private void a()
  {
    String str = getString(2131165911);
    Object[] arrayOfObject = new Object[2];
    arrayOfObject[0] = Integer.valueOf(this.a.getCheckedItemCountC());
    arrayOfObject[1] = Integer.valueOf(this.d);
    setTitle(String.format(str, arrayOfObject));
    if (this.a.getCheckedItemCountC() > 0)
    {
      this.b.setEnabled(true);
      return;
    }
    this.b.setEnabled(false);
  }

  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    if ((paramInt1 == 1) && (paramInt2 == -1))
    {
      if (paramIntent != null)
      {
        boolean bool = paramIntent.getBooleanExtra("extra_key_is_selected", false);
        int i = paramIntent.getIntExtra("extra_key_position", -1);
        this.a.a(i, bool);
      }
      return;
    }
    super.onActivityResult(paramInt1, paramInt2, paramIntent);
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    if (paramBundle != null)
    {
      this.d = paramBundle.getInt("extra_key_max_sel_allowed", 0);
      setContentView(2130903110);
      this.a = ((GridViewCompat)findViewById(2131296488));
      if (getLastNonConfigurationInstance() == null)
        break label192;
    }
    label192: for (this.c = ((b)getLastNonConfigurationInstance()); ; this.c = new b(getApplicationContext()))
    {
      this.a.setChoiceModeC(2);
      this.a.setAdapter(this.c);
      this.b = ((Button)findViewById(2131296487));
      this.a.setOnScrollListener(new c(this));
      this.a.setOnItemClickListener(new d(this));
      this.a.setOnItemLongClickListener(new e(this));
      this.b.setOnClickListener(new f(this));
      new a().execute(new Void[0]);
      return;
      Bundle localBundle = getIntent().getExtras();
      if (localBundle == null)
        break;
      this.d = localBundle.getInt("extra_key_max_sel_allowed", 0);
      break;
    }
  }

  protected void onResume()
  {
    a();
    super.onResume();
  }

  public Object onRetainNonConfigurationInstance()
  {
    return this.c;
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    paramBundle.putInt("extra_key_max_sel_allowed", this.d);
  }

  final class a extends AsyncTask<Void, Void, Cursor>
  {
    a()
    {
    }
  }

  static class b extends CursorAdapter
  {
    private static final String d = b.class.getSimpleName();
    GridViewCompat a;
    ConcurrentHashMap<View, AsyncTask<Long, Void, Bitmap>> b = new ConcurrentHashMap();
    ImageBrowserActivity.c<Long, Bitmap> c = new ImageBrowserActivity.c();
    private boolean e = true;

    public b(Context paramContext)
    {
      super(null, false);
    }

    public final void a(boolean paramBoolean)
    {
      this.e = paramBoolean;
    }

    public void bindView(View paramView, Context paramContext, Cursor paramCursor)
    {
      int i = 0;
      int j = paramCursor.getColumnIndex("_id");
      a locala = (a)paramView.getTag();
      long l = paramCursor.getLong(j);
      Bitmap localBitmap = (Bitmap)this.c.get(Long.valueOf(l));
      if (localBitmap != null)
        locala.a.setImageBitmap(localBitmap);
      while (true)
      {
        if (this.a.getChoiceModeC() == 2)
        {
          SparseBooleanArray localSparseBooleanArray = this.a.getCheckedItemPositionsC();
          locala.b.setChecked(localSparseBooleanArray.get(paramCursor.getPosition()));
        }
        return;
        locala.a.setImageBitmap(null);
        int k = 0;
        if (i != 0)
          continue;
        try
        {
          AsyncTask localAsyncTask1 = (AsyncTask)this.b.get(locala.a);
          if (localAsyncTask1 != null)
            localAsyncTask1.cancel(true);
          b localb = new b(paramContext, locala.a, this.b, this.c);
          Long[] arrayOfLong = new Long[1];
          arrayOfLong[0] = Long.valueOf(l);
          AsyncTask localAsyncTask2 = localb.execute(arrayOfLong);
          this.b.put(locala.a, localAsyncTask2);
          m = 1;
          int n = k + 1;
          if (n > 100)
            continue;
          k = n;
          i = m;
        }
        catch (RejectedExecutionException localRejectedExecutionException)
        {
          while (true)
            int m = i;
        }
      }
    }

    public View newView(Context paramContext, Cursor paramCursor, ViewGroup paramViewGroup)
    {
      View localView = LayoutInflater.from(paramContext).inflate(2130903111, paramViewGroup, false);
      localView.setTag(new a(localView));
      this.a = ((GridViewCompat)paramViewGroup);
      return localView;
    }

    private static final class a
    {
      public ImageView a;
      public CheckBox b;

      public a(View paramView)
      {
        this.a = ((ImageView)paramView.findViewById(2131296489));
        this.b = ((CheckBox)paramView.findViewById(2131296415));
      }
    }

    static final class b extends AsyncTask<Long, Void, Bitmap>
    {
      Context a;
      ImageView b;
      ConcurrentHashMap<View, AsyncTask<Long, Void, Bitmap>> c;
      ImageBrowserActivity.c<Long, Bitmap> d;
      Long e;

      public b(Context paramContext, ImageView paramImageView, ConcurrentHashMap<View, AsyncTask<Long, Void, Bitmap>> paramConcurrentHashMap, ImageBrowserActivity.c<Long, Bitmap> paramc)
      {
        this.a = paramContext;
        this.b = paramImageView;
        this.c = paramConcurrentHashMap;
        this.d = paramc;
      }
    }
  }

  static final class c<K, V> extends LinkedHashMap<K, V>
  {
    private final int a = 100;

    public c()
    {
      super(1.0F, true);
    }

    protected final boolean removeEldestEntry(Map.Entry<K, V> paramEntry)
    {
      return super.size() > this.a;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.util.imagebrowser.ImageBrowserActivity
 * JD-Core Version:    0.6.2
 */