package com.chase.sig.android.util.imagebrowser;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import java.lang.ref.WeakReference;

public class ImageViewerActivity extends Activity
{
  private static final String f = ImageViewerActivity.class.getSimpleName();
  ImageView a;
  Button b;
  boolean c;
  int d;
  boolean e;

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(2130903112);
    if (paramBundle != null)
    {
      this.c = paramBundle.getBoolean("extra_key_is_selected", false);
      this.d = paramBundle.getInt("extra_key_position", -1);
      this.e = paramBundle.getBoolean("extra_key_can_select", false);
    }
    while (true)
    {
      Uri localUri = getIntent().getData();
      this.a = ((ImageView)findViewById(2131296489));
      new a(this.a).execute(new Uri[] { localUri });
      this.b = ((Button)findViewById(2131296490));
      this.b.setOnClickListener(new g(this));
      return;
      Bundle localBundle = getIntent().getExtras();
      if (localBundle != null)
      {
        this.c = localBundle.getBoolean("extra_key_is_selected", false);
        this.d = localBundle.getInt("extra_key_position", -1);
        this.e = localBundle.getBoolean("extra_key_can_select", false);
      }
    }
  }

  protected void onDestroy()
  {
    BitmapDrawable localBitmapDrawable = (BitmapDrawable)this.a.getDrawable();
    if (localBitmapDrawable != null)
    {
      localBitmapDrawable.getBitmap().recycle();
      System.gc();
    }
    super.onDestroy();
  }

  protected void onResume()
  {
    super.onResume();
    Bundle localBundle = getIntent().getExtras();
    String str1 = null;
    if (localBundle != null)
      str1 = localBundle.getString("extra_key_title");
    if (str1 == null)
      str1 = getString(2131165914);
    setTitle(str1);
    this.b.setEnabled(this.e);
    Button localButton = this.b;
    if (this.c);
    for (String str2 = getString(2131165912); ; str2 = getString(2131165913))
    {
      localButton.setText(str2);
      return;
    }
  }

  protected void onSaveInstanceState(Bundle paramBundle)
  {
    super.onSaveInstanceState(paramBundle);
    paramBundle.putBoolean("extra_key_is_selected", this.c);
    paramBundle.putInt("extra_key_position", this.d);
    paramBundle.putBoolean("extra_key_can_select", this.e);
  }

  final class a extends AsyncTask<Uri, Void, Bitmap>
  {
    private final WeakReference<ImageView> b;

    public a(ImageView arg2)
    {
      Object localObject;
      this.b = new WeakReference(localObject);
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.util.imagebrowser.ImageViewerActivity
 * JD-Core Version:    0.6.2
 */