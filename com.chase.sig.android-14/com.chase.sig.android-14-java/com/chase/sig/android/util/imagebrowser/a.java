package com.chase.sig.android.util.imagebrowser;

import android.content.ContentResolver;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.net.Uri;
import java.io.FileNotFoundException;
import java.io.InputStream;

public final class a
{
  private static int a(BitmapFactory.Options paramOptions, int paramInt1, int paramInt2)
  {
    int i = paramOptions.outHeight;
    int j = paramOptions.outWidth;
    int k = 1;
    if ((i > paramInt2) || (j > paramInt1))
    {
      if (j > i)
        k = Math.round(i / paramInt2);
    }
    else
      return k;
    return Math.round(j / paramInt1);
  }

  public static Bitmap a(Context paramContext, Uri paramUri, int paramInt1, int paramInt2)
  {
    try
    {
      InputStream localInputStream1 = paramContext.getContentResolver().openInputStream(paramUri);
      BitmapFactory.Options localOptions = new BitmapFactory.Options();
      localOptions.inJustDecodeBounds = true;
      BitmapFactory.decodeStream(localInputStream1, null, localOptions);
      localOptions.inSampleSize = a(localOptions, paramInt1, paramInt2);
      InputStream localInputStream2 = paramContext.getContentResolver().openInputStream(paramUri);
      localOptions.inJustDecodeBounds = false;
      Bitmap localBitmap = BitmapFactory.decodeStream(localInputStream2, null, localOptions);
      return localBitmap;
    }
    catch (FileNotFoundException localFileNotFoundException)
    {
    }
    return null;
  }

  public static Bitmap a(Resources paramResources, int paramInt1, int paramInt2, int paramInt3)
  {
    BitmapFactory.Options localOptions = new BitmapFactory.Options();
    localOptions.inJustDecodeBounds = true;
    BitmapFactory.decodeResource(paramResources, paramInt1, localOptions);
    localOptions.inSampleSize = a(localOptions, paramInt2, paramInt3);
    localOptions.inJustDecodeBounds = false;
    return BitmapFactory.decodeResource(paramResources, paramInt1, localOptions);
  }

  public static Bitmap a(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    BitmapFactory.Options localOptions = new BitmapFactory.Options();
    localOptions.inJustDecodeBounds = true;
    BitmapFactory.decodeByteArray(paramArrayOfByte, 0, paramArrayOfByte.length, localOptions);
    localOptions.inSampleSize = a(localOptions, paramInt1, paramInt2);
    localOptions.inJustDecodeBounds = false;
    return BitmapFactory.decodeByteArray(paramArrayOfByte, 0, paramArrayOfByte.length, localOptions);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.util.imagebrowser.a
 * JD-Core Version:    0.6.2
 */