package com.chase.sig.android.util.imagebrowser;

import android.content.ContentUris;
import android.content.Intent;
import android.database.Cursor;
import android.provider.MediaStore.Images.Media;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;

final class e
  implements AdapterView.OnItemLongClickListener
{
  e(ImageBrowserActivity paramImageBrowserActivity)
  {
  }

  public final boolean onItemLongClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    Cursor localCursor = ((ImageBrowserActivity.b)this.a.a.getAdapter()).getCursor();
    int i = localCursor.getColumnIndex("title");
    localCursor.moveToPosition(paramInt);
    String str = localCursor.getString(i);
    Intent localIntent = new Intent(this.a, ImageViewerActivity.class);
    localIntent.setData(ContentUris.withAppendedId(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, paramLong));
    if (this.a.a.getCheckedItemCountC() < this.a.d)
      localIntent.putExtra("extra_key_can_select", true);
    while (true)
    {
      localIntent.putExtra("extra_key_title", str);
      localIntent.putExtra("extra_key_position", paramInt);
      localIntent.putExtra("extra_key_is_selected", this.a.a.a(paramInt));
      this.a.startActivityForResult(localIntent, 1);
      return false;
      if (this.a.a.a(paramInt))
        localIntent.putExtra("extra_key_can_select", true);
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.util.imagebrowser.e
 * JD-Core Version:    0.6.2
 */