package com.chase.sig.android.util.imagebrowser;

import android.content.ContentUris;
import android.content.Intent;
import android.database.Cursor;
import android.provider.MediaStore.Images.Media;
import android.util.SparseBooleanArray;
import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import java.util.ArrayList;

final class f
  implements View.OnClickListener
{
  f(ImageBrowserActivity paramImageBrowserActivity)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    SparseBooleanArray localSparseBooleanArray = this.a.a.getCheckedItemPositionsC();
    Cursor localCursor = ((ImageBrowserActivity.b)this.a.a.getAdapter()).getCursor();
    int i = localCursor.getColumnIndex("_id");
    int j = localSparseBooleanArray.size();
    ArrayList localArrayList = new ArrayList();
    for (int k = 0; k < j; k++)
      if (localSparseBooleanArray.get(localSparseBooleanArray.keyAt(k)))
      {
        localCursor.moveToPosition(localSparseBooleanArray.keyAt(k));
        long l = localCursor.getLong(i);
        localArrayList.add(ContentUris.withAppendedId(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, l));
      }
    Intent localIntent = new Intent();
    localIntent.setType("android.intent.extra.TEXT");
    localIntent.putParcelableArrayListExtra("image_uris", localArrayList);
    this.a.setResult(-1, localIntent);
    this.a.finish();
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.util.imagebrowser.f
 * JD-Core Version:    0.6.2
 */