package com.chase.sig.android.util.imagebrowser;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;

final class g
  implements View.OnClickListener
{
  g(ImageViewerActivity paramImageViewerActivity)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    Intent localIntent = new Intent();
    if (!this.a.c);
    for (boolean bool = true; ; bool = false)
    {
      localIntent.putExtra("extra_key_is_selected", bool);
      localIntent.putExtra("extra_key_position", this.a.d);
      this.a.setResult(-1, localIntent);
      this.a.finish();
      return;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.util.imagebrowser.g
 * JD-Core Version:    0.6.2
 */