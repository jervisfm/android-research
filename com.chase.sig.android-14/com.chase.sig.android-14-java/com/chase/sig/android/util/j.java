package com.chase.sig.android.util;

import java.util.Date;
import org.apache.http.cookie.Cookie;

final class j
  implements Cookie
{
  j(f paramf, Cookie paramCookie)
  {
  }

  public final String getComment()
  {
    return this.a.getComment();
  }

  public final String getCommentURL()
  {
    return this.a.getCommentURL();
  }

  public final String getDomain()
  {
    return ".chase.com";
  }

  public final Date getExpiryDate()
  {
    return this.a.getExpiryDate();
  }

  public final String getName()
  {
    return this.a.getName();
  }

  public final String getPath()
  {
    return this.a.getPath();
  }

  public final int[] getPorts()
  {
    return this.a.getPorts();
  }

  public final String getValue()
  {
    return this.a.getValue();
  }

  public final int getVersion()
  {
    return this.a.getVersion();
  }

  public final boolean isExpired(Date paramDate)
  {
    return this.a.isExpired(paramDate);
  }

  public final boolean isPersistent()
  {
    return this.a.isPersistent();
  }

  public final boolean isSecure()
  {
    return this.a.isSecure();
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.util.j
 * JD-Core Version:    0.6.2
 */