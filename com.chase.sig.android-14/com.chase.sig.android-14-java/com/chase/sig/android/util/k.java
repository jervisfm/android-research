package com.chase.sig.android.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.apache.http.client.CookieStore;
import org.apache.http.cookie.Cookie;

final class k
  implements CookieStore
{
  private Hashtable<String, Cookie> a = new Hashtable();

  public final void addCookie(Cookie paramCookie)
  {
    String str = paramCookie.getName();
    if (s.l(str));
    do
    {
      return;
      if (this.a.containsKey(str))
        this.a.remove(str);
    }
    while (paramCookie.isExpired(new Date()));
    this.a.put(str, paramCookie);
  }

  public final void clear()
  {
    this.a.clear();
  }

  public final boolean clearExpired(Date paramDate)
  {
    Date localDate = new Date();
    Iterator localIterator = this.a.keySet().iterator();
    boolean bool1 = false;
    if (localIterator.hasNext())
    {
      String str = (String)localIterator.next();
      if (!((Cookie)this.a.get(str)).isExpired(localDate))
        break label88;
      this.a.remove(str);
    }
    label88: for (boolean bool2 = true; ; bool2 = bool1)
    {
      bool1 = bool2;
      break;
      return bool1;
    }
  }

  public final List<Cookie> getCookies()
  {
    return new ArrayList(this.a.values());
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.util.k
 * JD-Core Version:    0.6.2
 */