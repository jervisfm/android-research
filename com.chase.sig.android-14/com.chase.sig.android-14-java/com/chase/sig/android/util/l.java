package com.chase.sig.android.util;

import android.text.format.Time;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public final class l
{
  private static Calendar[] a = arrayOfGregorianCalendar;
  private static int b = 86400000;

  static
  {
    GregorianCalendar[] arrayOfGregorianCalendar = new GregorianCalendar[7];
    arrayOfGregorianCalendar[0] = new GregorianCalendar(2012, 4, 28);
    arrayOfGregorianCalendar[1] = new GregorianCalendar(2012, 6, 4);
    arrayOfGregorianCalendar[2] = new GregorianCalendar(2012, 8, 3);
    arrayOfGregorianCalendar[3] = new GregorianCalendar(2012, 9, 8);
    arrayOfGregorianCalendar[4] = new GregorianCalendar(2012, 10, 12);
    arrayOfGregorianCalendar[5] = new GregorianCalendar(2012, 10, 22);
    arrayOfGregorianCalendar[6] = new GregorianCalendar(2012, 11, 25);
  }

  public static Calendar a(int paramInt)
  {
    Calendar localCalendar1 = Calendar.getInstance(TimeZone.getTimeZone("America/New_York"));
    if (localCalendar1.get(11) >= paramInt)
      localCalendar1.add(5, 1);
    if ((7 != localCalendar1.get(7)) && (1 != localCalendar1.get(7)) && (!a(localCalendar1)));
    for (int i = 1; ; i = 0)
    {
      if (i != 0)
        break label73;
      localCalendar1.add(5, 1);
      break;
    }
    label73: Calendar localCalendar2 = Calendar.getInstance();
    localCalendar2.set(2, localCalendar1.get(2));
    localCalendar2.set(5, localCalendar1.get(5));
    localCalendar2.set(1, localCalendar1.get(1));
    return localCalendar2;
  }

  public static TimeZone a()
  {
    return TimeZone.getTimeZone("America/New_York");
  }

  public static final void a(Calendar[] paramArrayOfCalendar)
  {
    a = paramArrayOfCalendar;
  }

  private static boolean a(Time paramTime)
  {
    Time localTime = new Time();
    for (int i = 0; ; i++)
    {
      int j = a.length;
      boolean bool = false;
      if (i < j)
      {
        localTime.set(a[i].getTime().getTime());
        if ((paramTime.year != localTime.year) || (paramTime.month != localTime.month) || (paramTime.monthDay != localTime.monthDay))
          break label85;
      }
      label85: for (int k = 1; k != 0; k = 0)
      {
        bool = true;
        return bool;
      }
    }
  }

  public static boolean a(Calendar paramCalendar)
  {
    Time localTime = new Time("America/New_York");
    localTime.set(paramCalendar.getTimeInMillis());
    return a(localTime);
  }

  public static boolean a(Calendar paramCalendar1, Calendar paramCalendar2)
  {
    return (paramCalendar1.get(5) == paramCalendar2.get(5)) && (paramCalendar1.get(2) == paramCalendar2.get(2)) && (paramCalendar1.get(1) == paramCalendar2.get(1));
  }

  public static boolean a(Date paramDate, Calendar paramCalendar)
  {
    Calendar localCalendar = Calendar.getInstance();
    localCalendar.setTime(paramDate);
    return a(localCalendar, paramCalendar);
  }

  public static long b()
  {
    return 0L;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.util.l
 * JD-Core Version:    0.6.2
 */