package com.chase.sig.android.util;

import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.ChaseApplication;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.apache.http.cookie.Cookie;
import org.apache.http.cookie.CookieOrigin;
import org.json.JSONException;
import org.json.JSONObject;

public final class m
{
  public static JSONObject a(ChaseApplication paramChaseApplication, String paramString1, String paramString2)
  {
    String str;
    try
    {
      str = new f(paramChaseApplication).a(paramString1, paramString2);
      new Object[] { str };
      if (str == null)
        throw new ChaseException(ChaseException.b, String.format("Did not recieve JSON response from %s", new Object[] { paramString1 }));
    }
    catch (JSONException localJSONException)
    {
      throw new ChaseException(ChaseException.b, localJSONException, String.format("Could not connect to: %s", new Object[] { paramString1 }));
    }
    JSONObject localJSONObject = new JSONObject(str);
    return localJSONObject;
  }

  public static JSONObject a(ChaseApplication paramChaseApplication, String paramString, Hashtable<String, String> paramHashtable)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramString, paramHashtable);
    return a(paramChaseApplication, paramString, f.a(paramHashtable));
  }

  public static JSONObject a(ChaseApplication paramChaseApplication, String paramString1, Hashtable<String, String> paramHashtable, String[] paramArrayOfString1, byte[][] paramArrayOfByte, String[] paramArrayOfString2, String paramString2)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.b(paramString1, paramHashtable);
    URLConnection localURLConnection;
    p localp;
    try
    {
      new Object[] { paramString1 };
      URL localURL = new URL(paramString1);
      str1 = "--------------------" + Long.toString(System.currentTimeMillis(), 16);
      localURLConnection = localURL.openConnection();
      if ((localURLConnection instanceof HttpURLConnection))
        ((HttpURLConnection)localURLConnection).setRequestMethod("POST");
      localURLConnection.setDoInput(true);
      localURLConnection.setDoOutput(true);
      localURLConnection.setUseCaches(false);
      localURLConnection.setDefaultUseCaches(false);
      localURLConnection.setRequestProperty("Accept", "*/*");
      localURLConnection.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + str1);
      localURLConnection.setRequestProperty("Connection", "Keep-Alive");
      localURLConnection.setRequestProperty("Cache-Control", "no-cache");
      localURLConnection.setRequestProperty("channel-id", paramString2);
      f localf = new f(paramChaseApplication);
      boolean bool = localURL.getProtocol().equalsIgnoreCase("https");
      i = localURL.getPort();
      if (i < 0)
      {
        if (bool)
          i = 443;
      }
      else
      {
        List localList = localf.a(new CookieOrigin(localURL.getHost(), i, localURL.getPath(), bool));
        localStringBuffer1 = new StringBuffer();
        Iterator localIterator1 = localList.iterator();
        while (localIterator1.hasNext())
        {
          Cookie localCookie = (Cookie)localIterator1.next();
          if (localStringBuffer1.length() > 0)
            localStringBuffer1.append("; ");
          localStringBuffer1.append(localCookie.getName() + "=" + localCookie.getValue());
        }
      }
    }
    catch (MalformedURLException localMalformedURLException)
    {
      String str1;
      StringBuffer localStringBuffer1;
      while (true)
      {
        throw new ChaseException(localMalformedURLException, "Service URL was bad.");
        int i = 80;
      }
      new Object[1][0] = localStringBuffer1.toString();
      localURLConnection.setRequestProperty("Cookie", localStringBuffer1.toString());
      localp = new p(localURLConnection.getOutputStream(), str1);
      Iterator localIterator2 = paramHashtable.keySet().iterator();
      if (localIterator2.hasNext())
      {
        str6 = (String)localIterator2.next();
        Object[] arrayOfObject2 = new Object[2];
        arrayOfObject2[0] = str6;
        arrayOfObject2[1] = paramHashtable.get(str6);
        str7 = (String)paramHashtable.get(str6);
        if (str6 == null)
          throw new IllegalArgumentException("Name cannot be null or empty.");
      }
    }
    catch (IOException localIOException)
    {
      while (true)
      {
        String str6;
        String str7;
        throw new ChaseException(localIOException, "Unable to post multi part form.");
        if (str7 == null)
          str7 = "";
        localp.a.writeBytes("--");
        localp.a.writeBytes(localp.b);
        localp.a.writeBytes("\r\n");
        localp.a.writeBytes("Content-Disposition: form-data; name=\"" + str6 + "\"");
        localp.a.writeBytes("\r\n");
        localp.a.writeBytes("\r\n");
        localp.a.writeBytes(str7);
        localp.a.writeBytes("\r\n");
        localp.a.flush();
      }
    }
    catch (JSONException localJSONException)
    {
      throw new ChaseException(localJSONException, "Could not parse JSON response.");
    }
    for (int j = 0; j < paramArrayOfString1.length; j++)
    {
      Object[] arrayOfObject1 = new Object[3];
      arrayOfObject1[0] = paramArrayOfString1[j];
      arrayOfObject1[1] = paramArrayOfString2[j];
      arrayOfObject1[2] = Integer.valueOf(paramArrayOfByte[j].length);
      String str3 = paramArrayOfString1[j];
      String str4 = paramArrayOfString2[j];
      String str5 = paramArrayOfString1[j];
      byte[] arrayOfByte = paramArrayOfByte[j];
      if (arrayOfByte == null)
        throw new IllegalArgumentException("Data cannot be null.");
      if ((str5 == null) || (str5.length() == 0))
        throw new IllegalArgumentException("File name cannot be null or empty.");
      localp.a.writeBytes("--");
      localp.a.writeBytes(localp.b);
      localp.a.writeBytes("\r\n");
      localp.a.writeBytes("Content-Disposition: form-data; name=\"" + str3 + "\"; filename=\"" + str5 + "\"");
      localp.a.writeBytes("\r\n");
      if (str4 != null)
      {
        localp.a.writeBytes("Content-Type: " + str4);
        localp.a.writeBytes("\r\n");
      }
      localp.a.writeBytes("\r\n");
      localp.a.write(arrayOfByte, 0, arrayOfByte.length);
      localp.a.writeBytes("\r\n");
      localp.a.flush();
    }
    localp.a.writeBytes("--");
    localp.a.writeBytes(localp.b);
    localp.a.writeBytes("--");
    localp.a.writeBytes("\r\n");
    localp.a.flush();
    localp.a.close();
    BufferedReader localBufferedReader = new BufferedReader(new InputStreamReader(localURLConnection.getInputStream()));
    StringBuffer localStringBuffer2 = new StringBuffer();
    while (true)
    {
      String str2 = localBufferedReader.readLine();
      if (str2 == null)
        break;
      localStringBuffer2.append(str2);
    }
    localBufferedReader.close();
    new Object[1][0] = localStringBuffer2.toString();
    ChaseApplication.n();
    JSONObject localJSONObject = new JSONObject(localStringBuffer2.toString());
    return localJSONObject;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.util.m
 * JD-Core Version:    0.6.2
 */