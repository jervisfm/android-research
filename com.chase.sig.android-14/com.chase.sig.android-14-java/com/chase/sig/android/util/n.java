package com.chase.sig.android.util;

import org.json.JSONObject;

public final class n
{
  public static boolean a(JSONObject paramJSONObject, String paramString)
  {
    return (paramJSONObject.has(paramString)) && (!paramJSONObject.isNull(paramString));
  }

  public static String b(JSONObject paramJSONObject, String paramString)
  {
    if (paramJSONObject.isNull(paramString))
      return "";
    return paramJSONObject.optString(paramString);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.util.n
 * JD-Core Version:    0.6.2
 */