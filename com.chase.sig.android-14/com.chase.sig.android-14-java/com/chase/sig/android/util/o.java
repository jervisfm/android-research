package com.chase.sig.android.util;

import com.chase.sig.android.domain.LabeledValue;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public final class o
{
  public static Map<String, String> a(List<LabeledValue> paramList)
  {
    HashMap localHashMap = new HashMap();
    Iterator localIterator = paramList.iterator();
    while (localIterator.hasNext())
    {
      LabeledValue localLabeledValue = (LabeledValue)localIterator.next();
      localHashMap.put(localLabeledValue.b(), localLabeledValue.a());
    }
    return localHashMap;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.util.o
 * JD-Core Version:    0.6.2
 */