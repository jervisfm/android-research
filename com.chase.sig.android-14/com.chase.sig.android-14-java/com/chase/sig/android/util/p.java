package com.chase.sig.android.util;

import java.io.DataOutputStream;
import java.io.OutputStream;

public final class p
{
  DataOutputStream a = null;
  String b = null;

  public p(OutputStream paramOutputStream, String paramString)
  {
    if (paramOutputStream == null)
      throw new IllegalArgumentException("Output stream is required.");
    if ((paramString == null) || (paramString.length() == 0))
      throw new IllegalArgumentException("Boundary stream is required.");
    this.a = new DataOutputStream(paramOutputStream);
    this.b = paramString;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.util.p
 * JD-Core Version:    0.6.2
 */