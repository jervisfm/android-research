package com.chase.sig.android.util;

import java.util.List;

public final class r<T>
{
  public static int a(List<T> paramList, q<T> paramq)
  {
    for (int i = 0; i < paramList.size(); i++)
      if (paramq.a(paramList.get(i)))
        return i;
    return -1;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.util.r
 * JD-Core Version:    0.6.2
 */