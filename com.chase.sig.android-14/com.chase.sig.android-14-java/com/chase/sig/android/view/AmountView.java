package com.chase.sig.android.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;
import com.chase.sig.android.util.Dollar;
import java.math.BigDecimal;

public class AmountView extends EditText
{
  public AmountView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }

  public AmountView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
  }

  public final boolean a()
  {
    Dollar localDollar = new Dollar(getText().toString());
    int i;
    if (localDollar.b() == null)
    {
      i = 1;
      if (i == 0)
        break label35;
    }
    label35: 
    while ((localDollar.b().scale() > 2) || (localDollar.b().doubleValue() <= 0.0D))
    {
      return false;
      i = 0;
      break;
    }
    return true;
  }

  public Dollar getDollarAmount()
  {
    return new Dollar(getText().toString());
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.view.AmountView
 * JD-Core Version:    0.6.2
 */