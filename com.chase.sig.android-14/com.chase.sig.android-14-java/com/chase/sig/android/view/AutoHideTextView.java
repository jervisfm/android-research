package com.chase.sig.android.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;
import android.widget.TextView.BufferType;

public class AutoHideTextView extends TextView
{
  public AutoHideTextView(Context paramContext)
  {
    super(paramContext);
  }

  public AutoHideTextView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }

  public void setText(CharSequence paramCharSequence, TextView.BufferType paramBufferType)
  {
    super.setText(paramCharSequence, paramBufferType);
    if ((paramCharSequence == null) || (paramCharSequence.toString().equals("")))
    {
      setVisibility(8);
      return;
    }
    setVisibility(0);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.view.AutoHideTextView
 * JD-Core Version:    0.6.2
 */