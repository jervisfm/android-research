package com.chase.sig.android.view;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Paint;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

public class HomeImageButton extends RelativeLayout
{
  private ImageView a;
  private TextView b;
  private ImageView c;
  private View d;

  public HomeImageButton(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    setBackgroundResource(2130837677);
    this.a = new ImageView(paramContext);
    this.a.setImageResource(paramAttributeSet.getAttributeResourceValue("http://chase.com/android", "image_src", -1));
    this.a.setPadding(5, 5, 5, 5);
    this.a.setScaleType(ImageView.ScaleType.FIT_CENTER);
    this.a.setAdjustViewBounds(false);
    this.a.setId(1);
    RelativeLayout.LayoutParams localLayoutParams1 = new RelativeLayout.LayoutParams(-2, -2);
    localLayoutParams1.addRule(15);
    localLayoutParams1.addRule(9);
    addView(this.a, localLayoutParams1);
    this.b = new TextView(paramContext);
    int i = paramAttributeSet.getAttributeResourceValue("http://chase.com/android", "label", 2131165372);
    String str = getResources().getString(i);
    if (str.contains("Chase"))
      str = str.replace("Find Chase", "Find\nChase");
    this.b.setText(str);
    this.b.setTextAppearance(paramContext, 2131230726);
    this.b.setGravity(3);
    this.b.setTextSize(20.0F);
    RelativeLayout.LayoutParams localLayoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
    localLayoutParams2.addRule(1, this.a.getId());
    localLayoutParams2.addRule(15);
    addView(this.b, localLayoutParams2);
    this.c = new ImageView(paramContext);
    this.c.setImageResource(paramAttributeSet.getAttributeResourceValue("http://chase.com/android", "chevron_image_src", -1));
    this.c.setPadding(5, 5, 5, 5);
    this.c.setScaleType(ImageView.ScaleType.FIT_CENTER);
    this.c.setAdjustViewBounds(false);
    RelativeLayout.LayoutParams localLayoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
    localLayoutParams3.addRule(1, this.b.getId());
    localLayoutParams3.addRule(15);
    localLayoutParams3.addRule(11);
    addView(this.c, localLayoutParams3);
    ShapeDrawable localShapeDrawable = new ShapeDrawable(new RectShape());
    localShapeDrawable.getPaint().setColor(getResources().getColor(2131099686));
    this.d = new i(this, getContext(), localShapeDrawable);
    RelativeLayout.LayoutParams localLayoutParams4 = new RelativeLayout.LayoutParams(-1, -2);
    localLayoutParams4.addRule(12);
    addView(this.d, localLayoutParams4);
    setOnFocusChangeListener(new j(this));
  }

  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
    int i = paramInt3 - paramInt1;
    int j = paramInt4 - paramInt2;
    int k = j / 2;
    this.a.layout(paramInt1, 0, paramInt1 + this.a.getMeasuredWidth(), j);
    int m = paramInt1 + this.a.getMeasuredWidth();
    this.b.layout(m, k - this.b.getHeight() / 2, m + this.b.getWidth(), j);
    this.c.layout(paramInt3 - this.c.getMeasuredWidth(), 0, paramInt3, j);
    this.d.layout(paramInt1, j - 2, i, j);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.view.HomeImageButton
 * JD-Core Version:    0.6.2
 */