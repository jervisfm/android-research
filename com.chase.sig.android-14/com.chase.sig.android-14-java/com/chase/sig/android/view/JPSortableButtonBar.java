package com.chase.sig.android.view;

import android.content.Context;
import android.content.res.Resources;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.BaseSavedState;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;

public class JPSortableButtonBar extends LinearLayout
{
  private View.OnClickListener a;
  private a b;
  private Button c;
  private boolean d = false;

  public JPSortableButtonBar(Context paramContext)
  {
    super(paramContext);
    a();
  }

  public JPSortableButtonBar(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    a();
  }

  private void a()
  {
    this.a = new o(this);
  }

  private void a(Button paramButton, boolean paramBoolean)
  {
    if (paramBoolean)
    {
      if (this.d);
      for (int i = 2130837560; ; i = 2130837561)
      {
        paramButton.setBackgroundResource(i);
        paramButton.setTextColor(getResources().getColor(2130837753));
        return;
      }
    }
    paramButton.setBackgroundResource(2130837559);
    paramButton.setTextColor(getResources().getColor(2130837750));
  }

  public final void a(int paramInt1, int paramInt2)
  {
    Button localButton = (Button)LayoutInflater.from(getContext()).inflate(2130903229, this, false);
    localButton.setText(getResources().getString(paramInt2));
    localButton.setOnClickListener(this.a);
    a(localButton, false);
    localButton.setTag(Integer.valueOf(paramInt1));
    addView(localButton);
  }

  public final void a(int paramInt, Boolean paramBoolean)
  {
    if (paramBoolean != null)
      this.d = paramBoolean.booleanValue();
    int i = getChildCount();
    int j = 0;
    if (j < i)
    {
      View localView = getChildAt(j);
      Object localObject = localView.getTag();
      Button localButton;
      if ((localObject != null) && (localObject.equals(Integer.valueOf(paramInt))))
      {
        if (this.c != null)
          a(this.c, false);
        this.c = ((Button)localView);
        a(this.c, true);
        this.c = ((Button)localView);
        localButton = this.c;
        if (!this.d)
          break label131;
      }
      label131: for (int k = 2130837560; ; k = 2130837561)
      {
        localButton.setBackgroundResource(k);
        j++;
        break;
      }
    }
  }

  protected void onRestoreInstanceState(Parcelable paramParcelable)
  {
    if ((paramParcelable != null) && ((paramParcelable instanceof JPSortableButtonBarState)))
    {
      JPSortableButtonBarState localJPSortableButtonBarState = (JPSortableButtonBarState)paramParcelable;
      boolean bool = localJPSortableButtonBarState.b();
      a(localJPSortableButtonBarState.a(), Boolean.valueOf(bool));
      super.onRestoreInstanceState(localJPSortableButtonBarState.getSuperState());
      return;
    }
    super.onRestoreInstanceState(paramParcelable);
  }

  protected Parcelable onSaveInstanceState()
  {
    Parcelable localParcelable = super.onSaveInstanceState();
    if (this.c != null)
      return new JPSortableButtonBarState(localParcelable, ((Integer)this.c.getTag()).intValue(), this.d);
    return localParcelable;
  }

  public void setSortableButtonBarListener(a parama)
  {
    this.b = parama;
  }

  public static class JPSortableButtonBarState extends View.BaseSavedState
  {
    public static final Parcelable.Creator<JPSortableButtonBarState> CREATOR = new p();
    private boolean a;
    private int b;

    public JPSortableButtonBarState(Parcel paramParcel)
    {
      super();
      this.b = paramParcel.readInt();
      if (paramParcel.readInt() == 0);
      for (boolean bool = false; ; bool = true)
      {
        this.a = bool;
        return;
      }
    }

    public JPSortableButtonBarState(Parcelable paramParcelable, int paramInt, boolean paramBoolean)
    {
      super();
      this.b = paramInt;
      this.a = paramBoolean;
    }

    public final int a()
    {
      return this.b;
    }

    public final boolean b()
    {
      return this.a;
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
      paramParcel.writeInt(this.b);
      if (this.a);
      for (int i = 1; ; i = 0)
      {
        paramParcel.writeInt(i);
        super.writeToParcel(paramParcel, paramInt);
        return;
      }
    }
  }

  public static abstract interface a
  {
    public abstract void a(int paramInt, boolean paramBoolean);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.view.JPSortableButtonBar
 * JD-Core Version:    0.6.2
 */