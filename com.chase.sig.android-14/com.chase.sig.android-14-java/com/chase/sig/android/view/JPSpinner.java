package com.chase.sig.android.view;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.BaseSavedState;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.chase.sig.android.e.a;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class JPSpinner extends LinearLayout
{
  private int a = -1;
  private int b = 0;
  private ListAdapter c;
  private DialogInterface.OnClickListener d;
  private List<x> e;
  private String f;
  private Dialog g;

  public JPSpinner(Context paramContext)
  {
    super(paramContext);
    a(paramContext);
  }

  public JPSpinner(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    TypedArray localTypedArray = paramContext.obtainStyledAttributes(paramAttributeSet, e.a.JPSpinner);
    this.f = localTypedArray.getString(0);
    this.b = localTypedArray.getInt(1, 0);
    paramAttributeSet.getAttributeValue("jp", "hint");
    a(paramContext);
  }

  private TextView a(String paramString)
  {
    TextView localTextView = new TextView(getContext());
    localTextView.setText(paramString);
    localTextView.setTextColor(getResources().getColor(2131099653));
    localTextView.setTextSize(2, 15.0F);
    return localTextView;
  }

  private void a(Context paramContext)
  {
    setFocusable(true);
    setClickable(true);
    a();
    setOnClickListener(new q(this, paramContext));
    this.d = new r(this);
  }

  private Dialog b(Context paramContext)
  {
    k.a locala = new k.a(paramContext);
    locala.b = getHint();
    locala.i = true;
    ListAdapter localListAdapter = this.c;
    DialogInterface.OnClickListener localOnClickListener = this.d;
    locala.h = new ListView(locala.a);
    locala.h.setSelector(17301602);
    locala.h.setBackgroundResource(17170443);
    locala.h.setCacheColorHint(locala.a.getResources().getColor(17170443));
    locala.l = localOnClickListener;
    locala.h.setAdapter(localListAdapter);
    return locala.d(getSelectedItemPosition());
  }

  public final void a()
  {
    this.a = -1;
    removeAllViews();
    addView(a(this.f));
  }

  public final void a(x paramx)
  {
    if (this.e == null)
      this.e = new ArrayList();
    this.e.add(paramx);
  }

  public final boolean b()
  {
    return this.a != -1;
  }

  public final void c()
  {
    if ((this.g != null) && (this.g.isShowing()))
      this.g.dismiss();
  }

  public Dialog getDialog()
  {
    return this.g;
  }

  public String getHint()
  {
    return this.f;
  }

  public int getPosition()
  {
    return this.a;
  }

  public Object getSelectedItem()
  {
    if (b())
      return this.c.getItem(this.a);
    return null;
  }

  public int getSelectedItemPosition()
  {
    return this.a;
  }

  protected void onRestoreInstanceState(Parcelable paramParcelable)
  {
    if (!(paramParcelable instanceof JPSpinnerState))
      super.onRestoreInstanceState(paramParcelable);
    JPSpinnerState localJPSpinnerState;
    do
    {
      return;
      localJPSpinnerState = (JPSpinnerState)paramParcelable;
      super.onRestoreInstanceState(localJPSpinnerState.getSuperState());
      setSelection(localJPSpinnerState.a());
    }
    while (!localJPSpinnerState.b());
    this.g = b(getContext());
    this.g.show();
  }

  protected Parcelable onSaveInstanceState()
  {
    JPSpinnerState localJPSpinnerState = new JPSpinnerState(super.onSaveInstanceState());
    localJPSpinnerState.a(this.a);
    if ((this.g != null) && (this.g.isShowing()));
    for (boolean bool = true; ; bool = false)
    {
      localJPSpinnerState.a(bool);
      return localJPSpinnerState;
    }
  }

  public void setAdapter(ListAdapter paramListAdapter)
  {
    this.c = paramListAdapter;
  }

  public void setChoiceMode(int paramInt)
  {
    this.b = paramInt;
  }

  public void setDialog(Dialog paramDialog)
  {
    this.g = paramDialog;
  }

  public void setHint(String paramString)
  {
    this.f = paramString;
  }

  public void setSelection(int paramInt)
  {
    if (paramInt == -1)
    {
      a();
      return;
    }
    removeAllViews();
    if (this.b == 1);
    for (Object localObject = a(this.c.getItem(paramInt).toString()); ; localObject = this.c.getView(paramInt, null, null))
    {
      addView((View)localObject);
      this.a = paramInt;
      if (this.e == null)
        break;
      Iterator localIterator = this.e.iterator();
      while (localIterator.hasNext())
        ((x)localIterator.next()).a(paramInt);
      break;
    }
  }

  static class JPSpinnerState extends View.BaseSavedState
  {
    public static final Parcelable.Creator<JPSpinnerState> CREATOR = new s();
    private int a;
    private boolean b;
    private boolean c;

    private JPSpinnerState(Parcel paramParcel)
    {
      super();
      this.a = paramParcel.readInt();
      boolean[] arrayOfBoolean = new boolean[1];
      paramParcel.readBooleanArray(arrayOfBoolean);
      this.b = arrayOfBoolean[0];
    }

    JPSpinnerState(Parcelable paramParcelable)
    {
      super();
    }

    public final int a()
    {
      return this.a;
    }

    public final void a(int paramInt)
    {
      this.a = paramInt;
    }

    public final void a(boolean paramBoolean)
    {
      this.b = paramBoolean;
    }

    public final boolean b()
    {
      return this.b;
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
      super.writeToParcel(paramParcel, paramInt);
      paramParcel.writeInt(this.a);
      boolean[] arrayOfBoolean1 = new boolean[1];
      arrayOfBoolean1[0] = this.b;
      paramParcel.writeBooleanArray(arrayOfBoolean1);
      boolean[] arrayOfBoolean2 = new boolean[1];
      arrayOfBoolean2[0] = this.c;
      paramParcel.writeBooleanArray(arrayOfBoolean2);
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.view.JPSpinner
 * JD-Core Version:    0.6.2
 */