package com.chase.sig.android.view;

import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class JPTabWidget extends LinearLayout
{
  private View.OnClickListener a;
  private b b;
  private Button c;

  public JPTabWidget(Context paramContext)
  {
    super(paramContext);
    c();
  }

  public JPTabWidget(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    c();
  }

  private void a(Button paramButton, boolean paramBoolean)
  {
    if (paramBoolean)
    {
      paramButton.setBackgroundResource(2130837752);
      paramButton.setTextColor(getResources().getColor(2130837753));
      return;
    }
    paramButton.setBackgroundResource(2130837749);
    paramButton.setTextColor(getResources().getColor(2130837750));
  }

  private void c()
  {
    setBackgroundResource(2130837746);
    this.a = new t(this);
  }

  public final void a()
  {
    removeAllViewsInLayout();
  }

  public final void a(int paramInt1, int paramInt2)
  {
    Button localButton = (Button)LayoutInflater.from(getContext()).inflate(2130903229, this, false);
    localButton.setText(getResources().getString(paramInt2));
    localButton.setOnClickListener(this.a);
    a(localButton, false);
    localButton.setTag(Integer.valueOf(paramInt1));
    addView(localButton);
  }

  public final void a(int paramInt, boolean paramBoolean)
  {
    int i = getChildCount();
    for (int j = 0; ; j++)
    {
      if (j < i)
      {
        View localView = getChildAt(j);
        Object localObject = localView.getTag();
        if ((localObject == null) || (!localObject.equals(Integer.valueOf(paramInt))))
          continue;
        if (b())
          a(this.c, false);
        this.c = ((Button)localView);
        a(this.c, true);
        if ((paramBoolean) && (this.b != null))
          this.b.a(((Integer)this.c.getTag()).intValue());
      }
      return;
    }
  }

  public final void a(String paramString)
  {
    TextView localTextView = (TextView)LayoutInflater.from(getContext()).inflate(2130903228, this, false);
    localTextView.setText(paramString);
    localTextView.setTag(null);
    addView(localTextView);
  }

  public final boolean b()
  {
    return this.c != null;
  }

  public int getSelectedTabId()
  {
    if (b())
      return ((Integer)this.c.getTag()).intValue();
    return -1;
  }

  public void setTabListener(b paramb)
  {
    this.b = paramb;
  }

  public static abstract class a extends GestureDetector.SimpleOnGestureListener
  {
    public abstract void a();

    public abstract void b();

    public boolean onDown(MotionEvent paramMotionEvent)
    {
      return true;
    }

    public boolean onFling(MotionEvent paramMotionEvent1, MotionEvent paramMotionEvent2, float paramFloat1, float paramFloat2)
    {
      if (Math.abs(paramMotionEvent1.getY() - paramMotionEvent2.getY()) > 250.0F);
      do
      {
        return false;
        if ((paramMotionEvent1.getX() - paramMotionEvent2.getX() > 120.0F) && (Math.abs(paramFloat1) > 200.0F))
        {
          b();
          return true;
        }
      }
      while ((paramMotionEvent2.getX() - paramMotionEvent1.getX() <= 120.0F) || (Math.abs(paramFloat1) <= 200.0F));
      a();
      return true;
    }
  }

  public static abstract interface b
  {
    public abstract void a(int paramInt);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.view.JPTabWidget
 * JD-Core Version:    0.6.2
 */