package com.chase.sig.android.view;

import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.BaseSavedState;
import android.view.animation.Animation;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

public final class MenuView extends RelativeLayout
{
  private int[] a;
  private Animation b;
  private Animation c;
  private View d;
  private ImageButton e;
  private boolean f;
  private int g;

  private void e()
  {
    int i;
    if (this.f)
    {
      i = 0;
      setVisibility(i);
      if (!this.f)
        break label92;
    }
    label92: for (int j = 1; ; j = 0)
    {
      setMenuButtonLevel(j);
      if (!this.f)
        break label97;
      int[] arrayOfInt2 = this.a;
      int i2 = arrayOfInt2.length;
      for (int i3 = 0; i3 < i2; i3++)
      {
        View localView = findViewById(arrayOfInt2[i3]);
        an.a((View)localView.getParent(), localView);
      }
      i = 8;
      break;
    }
    label97: int k = this.g;
    int[] arrayOfInt1 = this.a;
    int m = arrayOfInt1.length;
    int n = 0;
    if (n < m)
    {
      int i1 = arrayOfInt1[n];
      CompoundButton localCompoundButton = (CompoundButton)findViewById(i1);
      if (i1 == k);
      for (boolean bool = true; ; bool = false)
      {
        localCompoundButton.setChecked(bool);
        n++;
        break;
      }
    }
    requestLayout();
  }

  private View getContainer()
  {
    if (this.d != null)
      return this.d;
    View localView = getRootView().findViewById(2131296528);
    this.d = localView;
    return localView;
  }

  private ImageButton getMenuButton()
  {
    if (this.e != null)
      return this.e;
    ImageButton localImageButton = (ImageButton)getRootView().findViewById(2131296526);
    this.e = localImageButton;
    return localImageButton;
  }

  private void setMenuButtonLevel(int paramInt)
  {
    getMenuButton().getDrawable().setLevel(paramInt);
    getMenuButton().getBackground().setLevel(paramInt);
  }

  public final void a()
  {
    this.f = false;
    getContainer().startAnimation(this.c);
  }

  public final void b()
  {
    if (this.f)
    {
      a();
      return;
    }
    this.f = true;
    e();
    getContainer().startAnimation(this.b);
  }

  public final void c()
  {
    findViewById(2131296532).setVisibility(8);
  }

  public final boolean d()
  {
    return this.f;
  }

  public final boolean dispatchTouchEvent(MotionEvent paramMotionEvent)
  {
    return (this.f) && (super.dispatchTouchEvent(paramMotionEvent));
  }

  public final int getSelectedOptionId()
  {
    return this.g;
  }

  protected final void onRestoreInstanceState(Parcelable paramParcelable)
  {
    MenuState localMenuState = (MenuState)paramParcelable;
    super.onRestoreInstanceState(localMenuState.getSuperState());
    this.f = MenuState.a(localMenuState);
    this.g = MenuState.b(localMenuState);
    e();
  }

  protected final Parcelable onSaveInstanceState()
  {
    MenuState localMenuState = new MenuState(super.onSaveInstanceState());
    MenuState.a(localMenuState, this.f);
    MenuState.a(localMenuState, this.g);
    return localMenuState;
  }

  public final boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    a();
    return true;
  }

  public final void setSelectedOptionId(int paramInt)
  {
    this.g = paramInt;
  }

  private static class MenuState extends View.BaseSavedState
  {
    public static final Parcelable.Creator<MenuState> CREATOR = new v();
    private boolean a = false;
    private int b = 0;

    public MenuState(Parcel paramParcel)
    {
      super();
      this.a = Boolean.valueOf(paramParcel.readString()).booleanValue();
      this.b = paramParcel.readInt();
    }

    public MenuState(Parcelable paramParcelable)
    {
      super();
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
      super.writeToParcel(paramParcel, paramInt);
      paramParcel.writeString(String.valueOf(this.a));
      paramParcel.writeInt(this.b);
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.view.MenuView
 * JD-Core Version:    0.6.2
 */