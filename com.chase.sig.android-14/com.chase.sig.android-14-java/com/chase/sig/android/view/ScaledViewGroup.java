package com.chase.sig.android.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.MeasureSpec;
import android.widget.LinearLayout;

public class ScaledViewGroup extends LinearLayout
{
  static
  {
    if (!ScaledViewGroup.class.desiredAssertionStatus());
    for (boolean bool = true; ; bool = false)
    {
      a = bool;
      return;
    }
  }

  public ScaledViewGroup(Context paramContext)
  {
    super(paramContext);
  }

  public ScaledViewGroup(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }

  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    int i = getMeasuredHeight();
    int j = getMeasuredWidth();
    int k = getChildCount();
    int m = i / k;
    for (int n = 0; n < k; n++)
      getChildAt(n).layout(0, n * m, j, m + n * m);
  }

  protected void onMeasure(int paramInt1, int paramInt2)
  {
    if ((!a) && (View.MeasureSpec.getMode(paramInt2) == 0))
      throw new AssertionError();
    if ((!a) && (View.MeasureSpec.getMode(paramInt1) == 0))
      throw new AssertionError();
    int i = View.MeasureSpec.getSize(paramInt1) - getPaddingLeft() - getPaddingRight();
    int j = View.MeasureSpec.getSize(paramInt2) - getPaddingTop() - getPaddingBottom();
    int k = getChildCount();
    int m = j / k;
    for (int n = 0; n < k; n++)
    {
      View localView = getChildAt(n);
      if (localView.getVisibility() != 8)
        localView.measure(View.MeasureSpec.makeMeasureSpec(i, 1073741824), View.MeasureSpec.makeMeasureSpec(m, 1073741824));
    }
    setMeasuredDimension(i, j);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.view.ScaledViewGroup
 * JD-Core Version:    0.6.2
 */