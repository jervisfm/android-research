package com.chase.sig.android.view;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.util.AttributeSet;
import android.view.View;
import com.chase.sig.android.e.a;

public class SeparatorView extends View
{
  private int a;

  public SeparatorView(Context paramContext)
  {
    super(paramContext);
  }

  public SeparatorView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    this.a = paramContext.obtainStyledAttributes(paramAttributeSet, e.a.SeparatorView).getResourceId(0, 2131099663);
  }

  protected void onDraw(Canvas paramCanvas)
  {
    ShapeDrawable localShapeDrawable = new ShapeDrawable(new RectShape());
    localShapeDrawable.getPaint().setColor(getResources().getColor(this.a));
    localShapeDrawable.setBounds(0, 0, paramCanvas.getWidth(), paramCanvas.getHeight());
    localShapeDrawable.draw(paramCanvas);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.view.SeparatorView
 * JD-Core Version:    0.6.2
 */