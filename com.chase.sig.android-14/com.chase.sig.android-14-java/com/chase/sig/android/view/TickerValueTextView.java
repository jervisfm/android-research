package com.chase.sig.android.view;

import android.content.Context;
import android.content.res.Resources;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.AttributeSet;
import android.widget.TextView;
import com.chase.sig.android.util.Dollar;
import com.chase.sig.android.util.s;

public class TickerValueTextView extends TextView
{
  private static final Object c = "%";
  private boolean a = false;
  private boolean b = false;
  private Spannable d;

  public TickerValueTextView(Context paramContext)
  {
    super(paramContext);
  }

  public TickerValueTextView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }

  public TickerValueTextView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
  }

  public final void a(Dollar paramDollar1, Dollar paramDollar2, String paramString)
  {
    this.d = new SpannableString(paramDollar1.f());
    this.d.setSpan(new ForegroundColorSpan(getResources().getColor(2131099691)), 0, paramDollar1.f().length(), 33);
    a(paramDollar2, s.C(paramString));
  }

  public final void a(Dollar paramDollar, String paramString)
  {
    if (paramDollar == null)
      return;
    int i = 2131099664;
    StringBuffer localStringBuffer = new StringBuffer();
    if (this.d != null)
    {
      setText(this.d);
      localStringBuffer.append("&nbsp;&nbsp;&nbsp;");
      localStringBuffer.append(paramDollar.f());
      if (s.m(paramString))
      {
        localStringBuffer.append("&nbsp;&nbsp;&nbsp;");
        localStringBuffer.append(paramString);
        localStringBuffer.append(c);
      }
      if (!paramDollar.c())
        break label154;
      i = 2131099679;
      localStringBuffer.append(String.format("<img src=\"%s\" />", new Object[] { "positive" }));
    }
    label262: 
    while (true)
    {
      append(Html.fromHtml(localStringBuffer.toString(), new al(this), null));
      setTextColor(getResources().getColor(i));
      return;
      setText("");
      break;
      label154: if (paramDollar.d())
      {
        i = 2131099678;
        localStringBuffer.append(String.format("<img src=\"%s\" />", new Object[] { "negative" }));
      }
      else if (s.l(localStringBuffer.toString()))
      {
        if (this.a)
          localStringBuffer.append(getResources().getString(2131165319));
        while (true)
        {
          if (!this.b)
            break label262;
          localStringBuffer.append(String.format("<img src=\"%s\" />", new Object[] { "empty" }));
          break;
          localStringBuffer = new StringBuffer("--");
        }
      }
      else if (this.b)
      {
        localStringBuffer.append(String.format("<img src=\"%s\" />", new Object[] { "empty" }));
      }
    }
  }

  public final void a(String paramString1, String paramString2)
  {
    StringBuffer localStringBuffer = new StringBuffer();
    int i = 2131099664;
    if (s.m(paramString1))
    {
      localStringBuffer.append(paramString1);
      localStringBuffer.append(c);
    }
    if ("up".equalsIgnoreCase(paramString2))
    {
      i = 2131099679;
      localStringBuffer.append(String.format("<img src=\"%s\" />", new Object[] { "positive" }));
    }
    while (true)
    {
      setText(Html.fromHtml(localStringBuffer.toString(), new am(this), null));
      setTextColor(getResources().getColor(i));
      return;
      if ("down".equalsIgnoreCase(paramString2))
      {
        i = 2131099678;
        localStringBuffer.append(String.format("<img src=\"%s\" />", new Object[] { "negative" }));
      }
      else if (this.b)
      {
        localStringBuffer.append(String.format("<img src=\"%s\" />", new Object[] { "empty" }));
      }
    }
  }

  public final void a(String paramString, boolean paramBoolean)
  {
    StringBuffer localStringBuffer = new StringBuffer(paramString);
    if (paramBoolean)
      localStringBuffer.append(c);
    localStringBuffer.append(String.format("<img src=\"%s\" />", new Object[] { "empty" }));
    append(Html.fromHtml(localStringBuffer.toString(), new ak(this), null));
  }

  public void setColor(int paramInt)
  {
    setTextColor(paramInt);
  }

  public void setDisplayPlaceholder(boolean paramBoolean)
  {
    this.b = paramBoolean;
  }

  public void setShowNotApplicable(boolean paramBoolean)
  {
    this.a = paramBoolean;
  }

  public void setTickerValue(Dollar paramDollar)
  {
    a(paramDollar, null);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.view.TickerValueTextView
 * JD-Core Version:    0.6.2
 */