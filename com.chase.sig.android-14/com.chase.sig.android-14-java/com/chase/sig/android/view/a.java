package com.chase.sig.android.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

public final class a extends LinearLayout
{
  private final LayoutInflater a;
  private final String b;
  private final String c;
  private final String d;
  private View e;

  public a(Context paramContext, String paramString1, String paramString2, String paramString3)
  {
    this(paramContext, paramString1, paramString2, paramString3, (byte)0);
  }

  private a(Context paramContext, String paramString1, String paramString2, String paramString3, byte paramByte)
  {
    super(paramContext, null);
    this.b = paramString1;
    this.c = paramString2;
    this.d = paramString3;
    this.a = ((LayoutInflater)paramContext.getSystemService("layout_inflater"));
    setOrientation(0);
    setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
    View localView = this.a.inflate(2130903242, this, true);
    a(localView, 2131296984, this.b);
    a(localView, 2131296985, this.c);
    a(localView, 2131296503, "(" + this.d + ")");
    this.e = localView;
  }

  private static void a(View paramView, int paramInt, String paramString)
  {
    ((TextView)paramView.findViewById(paramInt)).setText(paramString);
  }

  public final View getView()
  {
    return this.e;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.view.a
 * JD-Core Version:    0.6.2
 */