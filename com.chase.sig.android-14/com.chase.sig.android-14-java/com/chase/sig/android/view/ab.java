package com.chase.sig.android.view;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.activity.AtmRecipientInfoActivity;
import com.chase.sig.android.domain.QuickPayAcceptMoneyTransaction;

final class ab
  implements View.OnClickListener
{
  ab(aa paramaa, QuickPayAcceptMoneyTransaction paramQuickPayAcceptMoneyTransaction)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    Intent localIntent = new Intent(this.b.getContext(), AtmRecipientInfoActivity.class);
    localIntent.setFlags(268435456);
    localIntent.putExtra("quick_pay_transaction", this.a);
    this.b.getContext().startActivity(localIntent);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.view.ab
 * JD-Core Version:    0.6.2
 */