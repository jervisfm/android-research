package com.chase.sig.android.view;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.activity.AtmSenderInfoActivity;
import com.chase.sig.android.domain.h;
import java.io.Serializable;

final class ac
  implements View.OnClickListener
{
  ac(aa paramaa, h paramh)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    Intent localIntent = new Intent(this.b.getContext(), AtmSenderInfoActivity.class);
    localIntent.setFlags(268435456);
    localIntent.putExtra("quick_pay_transaction", (Serializable)this.a);
    this.b.getContext().startActivity(localIntent);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.view.ac
 * JD-Core Version:    0.6.2
 */