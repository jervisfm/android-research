package com.chase.sig.android.view;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.activity.QuickPayATMGeneralInfoActivity;

final class ad
  implements View.OnClickListener
{
  ad(aa paramaa)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    Intent localIntent = new Intent(this.a.getContext(), QuickPayATMGeneralInfoActivity.class);
    localIntent.setFlags(268435456);
    this.a.getContext().startActivity(localIntent);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.view.ad
 * JD-Core Version:    0.6.2
 */