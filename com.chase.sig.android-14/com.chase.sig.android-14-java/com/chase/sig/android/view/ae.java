package com.chase.sig.android.view;

import android.content.Context;
import android.text.Spannable;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.chase.sig.android.domain.TransactionForReceipt;
import com.chase.sig.android.util.s;
import java.util.List;

public final class ae extends ArrayAdapter<TransactionForReceipt>
{
  private static LayoutInflater b;
  private List<TransactionForReceipt> a;

  public ae(Context paramContext, List<TransactionForReceipt> paramList)
  {
    super(paramContext, 2130903207, paramList);
    this.a = paramList;
    b = (LayoutInflater)paramContext.getSystemService("layout_inflater");
  }

  public final int getItemViewType(int paramInt)
  {
    return super.getItemViewType(paramInt);
  }

  public final View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    TransactionForReceipt localTransactionForReceipt = (TransactionForReceipt)this.a.get(paramInt);
    a locala2;
    if (paramView == null)
    {
      paramView = b.inflate(2130903207, null);
      locala2 = new a();
      locala2.a = ((TextView)paramView.findViewById(2131296930));
      paramView.setTag(locala2);
    }
    for (a locala1 = locala2; ; locala1 = (a)paramView.getTag())
    {
      String str = localTransactionForReceipt.b();
      TextView localTextView = locala1.a;
      Object[] arrayOfObject = new Object[3];
      arrayOfObject[0] = str;
      arrayOfObject[1] = s.j(localTransactionForReceipt.c());
      arrayOfObject[2] = localTransactionForReceipt.d();
      localTextView.setText(String.format("%s\n%s\n%s", arrayOfObject));
      ((Spannable)locala1.a.getText()).setSpan(new StyleSpan(1), 0, str.length(), 18);
      return paramView;
    }
  }

  public final int getViewTypeCount()
  {
    return 1;
  }

  static final class a
  {
    TextView a;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.view.ae
 * JD-Core Version:    0.6.2
 */