package com.chase.sig.android.view;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filter.FilterResults;
import android.widget.Filterable;
import android.widget.ListAdapter;
import android.widget.ListView;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class af extends BaseAdapter
  implements Filterable
{
  protected final ListAdapter a;
  private a b;
  private List<? extends Map<String, ?>> c;
  private ArrayList<Map<String, ?>> d;
  private int[] e;
  private String[] f;

  public af(ListAdapter paramListAdapter)
  {
    this.a = paramListAdapter;
  }

  public int getCount()
  {
    return this.a.getCount();
  }

  public Filter getFilter()
  {
    if (this.b == null)
      this.b = new a((byte)0);
    return this.b;
  }

  public Object getItem(int paramInt)
  {
    return this.a.getItem(paramInt);
  }

  public long getItemId(int paramInt)
  {
    return this.a.getItemId(paramInt);
  }

  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    View localView = this.a.getView(paramInt, paramView, paramViewGroup);
    ListView localListView = (ListView)paramViewGroup;
    localListView.setFooterDividersEnabled(false);
    int i = -1 + getCount();
    int j = 0;
    if (paramInt == i)
      j = 1;
    if (getCount() == 1)
    {
      localView.setBackgroundResource(2130837715);
      return localView;
    }
    if (paramInt == 0)
    {
      localView.setBackgroundResource(2130837714);
      return localView;
    }
    if ((j != 0) && (localListView.getFooterViewsCount() == 0))
    {
      localView.setBackgroundResource(2130837713);
      return localView;
    }
    localListView.getFooterViewsCount();
    localView.setBackgroundResource(2130837712);
    return localView;
  }

  private final class a extends Filter
  {
    private a()
    {
    }

    protected final Filter.FilterResults performFiltering(CharSequence paramCharSequence)
    {
      Filter.FilterResults localFilterResults = new Filter.FilterResults();
      if (af.a(af.this) == null)
        af.a(af.this, new ArrayList(af.b(af.this)));
      if ((paramCharSequence == null) || (paramCharSequence.length() == 0))
      {
        ArrayList localArrayList1 = af.a(af.this);
        localFilterResults.values = localArrayList1;
        localFilterResults.count = localArrayList1.size();
        return localFilterResults;
      }
      String str = paramCharSequence.toString().toLowerCase();
      ArrayList localArrayList2 = af.a(af.this);
      int i = localArrayList2.size();
      ArrayList localArrayList3 = new ArrayList(i);
      for (int j = 0; j < i; j++)
      {
        Map localMap = (Map)localArrayList2.get(j);
        if (localMap != null)
        {
          int k = af.c(af.this).length;
          int m = 0;
          if (m < k)
          {
            String[] arrayOfString = ((String)localMap.get(af.d(af.this)[m])).split(" ");
            int n = arrayOfString.length;
            for (int i1 = 0; ; i1++)
              if (i1 < n)
              {
                if (arrayOfString[i1].toLowerCase().startsWith(str))
                  localArrayList3.add(localMap);
              }
              else
              {
                m++;
                break;
              }
          }
        }
      }
      localFilterResults.values = localArrayList3;
      localFilterResults.count = localArrayList3.size();
      return localFilterResults;
    }

    protected final void publishResults(CharSequence paramCharSequence, Filter.FilterResults paramFilterResults)
    {
      af.a(af.this, (List)paramFilterResults.values);
      if (paramFilterResults.count > 0)
      {
        af.this.notifyDataSetChanged();
        return;
      }
      af.this.notifyDataSetInvalidated();
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.view.af
 * JD-Core Version:    0.6.2
 */