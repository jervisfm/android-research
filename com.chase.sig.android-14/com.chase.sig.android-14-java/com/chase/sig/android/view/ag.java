package com.chase.sig.android.view;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.domain.OneTimePasswordContact;
import java.util.List;

public final class ag extends Dialog
{
  private a a;

  public ag(Context paramContext, List<OneTimePasswordContact> paramList, a parama)
  {
    super(paramContext);
    this.a = parama;
    if ((paramList != null) && (paramList.size() > 0))
    {
      requestWindowFeature(3);
      setContentView(2130903070);
      setFeatureDrawableResource(3, 2130837680);
      setTitle(2131165386);
      ListView localListView = (ListView)findViewById(2131296381);
      localListView.setAdapter(new ap(paramContext, paramList));
      localListView.setOnItemClickListener(new b((byte)0));
    }
  }

  public static abstract interface a
  {
    public abstract void a(OneTimePasswordContact paramOneTimePasswordContact);
  }

  private final class b
    implements AdapterView.OnItemClickListener
  {
    private b()
    {
    }

    public final void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
    {
      try
      {
        OneTimePasswordContact localOneTimePasswordContact = (OneTimePasswordContact)paramView.getTag();
        ag.this.dismiss();
        ag.a(ag.this).a(localOneTimePasswordContact);
        BehaviorAnalyticsAspect.a();
        BehaviorAnalyticsAspect.a(paramAdapterView, paramView, paramInt);
        return;
      }
      catch (Throwable localThrowable)
      {
        BehaviorAnalyticsAspect.a();
        BehaviorAnalyticsAspect.a(paramAdapterView, paramView, paramInt);
        throw localThrowable;
      }
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.view.ag
 * JD-Core Version:    0.6.2
 */