package com.chase.sig.android.view;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.text.Html;
import android.text.TextUtils.TruncateAt;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.chase.sig.android.util.s;
import java.util.ArrayList;

public final class ah extends ArrayAdapter<ai>
{
  private ArrayList<ai> a;
  private LayoutInflater b;

  public ah(Context paramContext, ArrayList<ai> paramArrayList)
  {
    super(paramContext, 2130903226, paramArrayList);
    this.a = paramArrayList;
    this.b = ((LayoutInflater)paramContext.getSystemService("layout_inflater"));
  }

  private int a(int paramInt)
  {
    if (paramInt != -1)
      return getContext().getResources().getColor(paramInt);
    return getContext().getResources().getColor(2131099649);
  }

  public final int getItemViewType(int paramInt)
  {
    ai localai = (ai)this.a.get(paramInt);
    switch (localai.a)
    {
    case 2130903212:
    case 2130903213:
    case 2130903214:
    case 2130903215:
    default:
      new Object[1][0] = Integer.valueOf(localai.a);
      return -1;
    case 2130903226:
      return 0;
    case 2130903220:
      return 1;
    case 2130903221:
      return 2;
    case 2130903217:
      return 3;
    case 2130903225:
      return 4;
    case 2130903222:
      return 5;
    case 2130903218:
      return 6;
    case 2130903216:
      return 7;
    case 2130903227:
      return 8;
    case 2130903224:
      return 9;
    case 2130903223:
      return 10;
    case 2130903219:
      return 11;
    case 2130903211:
    }
    return 12;
  }

  public final View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    ai localai = (ai)this.a.get(paramInt);
    boolean bool = localai.a();
    aj localaj;
    Object localObject;
    if (paramView != null)
    {
      localaj = (aj)paramView;
      localObject = null;
      if (!bool)
      {
        localObject = (aj.a)localaj.getTag();
        if (localai.a == 2130903216)
        {
          ((aj.a)localObject).a.setEllipsize(TextUtils.TruncateAt.END);
          ((aj.a)localObject).a.setSingleLine(true);
        }
        ((aj.a)localObject).a.setTypeface(Typeface.DEFAULT_BOLD);
        ((aj.a)localObject).a.setText(localai.b);
        ((aj.a)localObject).b.setText(localai.c);
        ((aj.a)localObject).b.setTextColor(a(localai.k));
        if (((aj.a)localObject).d != null)
        {
          ((aj.a)localObject).d.setText(localai.g);
          ((aj.a)localObject).d.setVisibility(0);
        }
      }
      if (!bool)
      {
        if (!localai.f)
          break label513;
        ((aj.a)localObject).c.setVisibility(0);
      }
    }
    while (true)
    {
      if (localai.i)
      {
        TextView localTextView = (TextView)localaj.findViewById(2131296938);
        String str = s.r(s.q(s.p(localTextView.getText().toString())));
        localTextView.setTypeface(Typeface.DEFAULT, 0);
        localTextView.setText(Html.fromHtml(str));
      }
      return localaj;
      localaj = new aj(getContext(), localai, this.b);
      if (localai.a == 2130903225)
      {
        localaj.setFocusable(false);
        localaj.setClickable(false);
      }
      View localView1 = localaj.getChildAt(0);
      localObject = null;
      if (bool)
        break;
      aj.a locala = new aj.a();
      locala.a = ((TextView)localView1.findViewById(2131296938));
      if (localai.a == 2130903216)
      {
        locala.a.setSingleLine(true);
        locala.a.setEllipsize(TextUtils.TruncateAt.END);
      }
      locala.b = ((TextView)localView1.findViewById(2131296940));
      locala.c = ((ImageView)localView1.findViewById(2131296265));
      View localView2 = localView1.findViewById(2131296939);
      if (localView2 != null)
      {
        locala.d = ((TextView)localView2);
        if (!s.m(localai.g))
          break label500;
        locala.d.setText(localai.g);
        locala.d.setVisibility(0);
      }
      while (true)
      {
        localaj.setTag(locala);
        locala.a.setText(localai.b);
        locala.b.setText(localai.c);
        locala.b.setTextColor(a(localai.k));
        localObject = locala;
        break;
        label500: locala.d.setVisibility(8);
      }
      label513: ((aj.a)localObject).c.setVisibility(4);
    }
  }

  public final int getViewTypeCount()
  {
    return 13;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.view.ah
 * JD-Core Version:    0.6.2
 */