package com.chase.sig.android.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public final class aj extends RelativeLayout
{
  public ai a;

  public aj(Context paramContext, ai paramai, LayoutInflater paramLayoutInflater)
  {
    super(paramContext);
    if (paramai != null)
    {
      this.a = paramai;
      paramLayoutInflater.inflate(paramai.a, this);
    }
  }

  public final ai getDetails()
  {
    return this.a;
  }

  public static final class a
  {
    TextView a;
    TextView b;
    ImageView c;
    TextView d;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.view.aj
 * JD-Core Version:    0.6.2
 */