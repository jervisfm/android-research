package com.chase.sig.android.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.chase.sig.android.domain.OneTimePasswordContact;
import java.util.List;

public final class ap extends ArrayAdapter<OneTimePasswordContact>
{
  static LayoutInflater b;
  List<OneTimePasswordContact> a;

  public ap(Context paramContext, List<OneTimePasswordContact> paramList)
  {
    super(paramContext, 2130903088, paramList);
    this.a = paramList;
    b = (LayoutInflater)paramContext.getSystemService("layout_inflater");
  }

  public final View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    OneTimePasswordContact localOneTimePasswordContact = (OneTimePasswordContact)this.a.get(paramInt);
    if (paramView == null)
    {
      View localView = b.inflate(2130903088, null);
      localView.setTag(localOneTimePasswordContact);
      ((TextView)localView.findViewById(2131296433)).setText(localOneTimePasswordContact.d() + " : ");
      ((TextView)localView.findViewById(2131296434)).setText(localOneTimePasswordContact.b());
      return localView;
    }
    paramView.setTag(localOneTimePasswordContact);
    ((TextView)paramView.findViewById(2131296433)).setText(localOneTimePasswordContact.d() + " : ");
    ((TextView)paramView.findViewById(2131296434)).setText(localOneTimePasswordContact.b());
    return paramView;
  }

  public final int getViewTypeCount()
  {
    return 1;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.view.ap
 * JD-Core Version:    0.6.2
 */