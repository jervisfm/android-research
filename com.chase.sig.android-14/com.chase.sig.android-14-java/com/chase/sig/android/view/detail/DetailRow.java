package com.chase.sig.android.view.detail;

import android.content.Context;
import android.content.res.Resources;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;
import android.widget.TextView;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;

public class DetailRow extends a<DetailRow, String>
  implements Parcelable
{
  public DetailRow(String paramString1, String paramString2)
  {
    super(paramString1, paramString2);
  }

  protected final void a(Context paramContext)
  {
    BehaviorAnalyticsAspect.a().a(this);
    p().setTextColor(paramContext.getResources().getColor(h()));
  }

  public int describeContents()
  {
    return 0;
  }

  public final DetailRow e(int paramInt)
  {
    this.f = paramInt;
    return this;
  }

  public final TextView p()
  {
    return (TextView)this.t.findViewById(this.s);
  }

  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.view.detail.DetailRow
 * JD-Core Version:    0.6.2
 */