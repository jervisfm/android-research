package com.chase.sig.android.view.detail;

import android.content.Context;
import android.content.res.Resources;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;
import android.widget.TextView;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;

public class DetailRowWithSubItem extends a<DetailRow, String>
  implements Parcelable
{
  String a = "";

  public DetailRowWithSubItem(String paramString1, String paramString2, String paramString3)
  {
    super(paramString1, paramString3);
    this.a = paramString2;
  }

  private TextView p()
  {
    return (TextView)this.t.findViewById(this.s);
  }

  protected final void a(Context paramContext)
  {
    BehaviorAnalyticsAspect.a().a(this);
    p().setTextColor(paramContext.getResources().getColor(h()));
    ((TextView)this.t.findViewById(2131296417)).setText(this.a);
  }

  public int describeContents()
  {
    return 0;
  }

  public final int j()
  {
    return 2130903084;
  }

  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.view.detail.DetailRowWithSubItem
 * JD-Core Version:    0.6.2
 */