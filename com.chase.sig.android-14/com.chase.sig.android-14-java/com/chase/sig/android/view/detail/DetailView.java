package com.chase.sig.android.view.detail;

import android.content.Context;
import android.content.res.Resources;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

public class DetailView extends LinearLayout
{
  private LayoutInflater a;
  private a[] b;
  private Random c = new Random();
  private boolean d = true;
  private ArrayList<Integer> e = new ArrayList();

  public DetailView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    setOrientation(1);
    this.a = ((LayoutInflater)paramContext.getSystemService("layout_inflater"));
  }

  private void a(int paramInt)
  {
    a locala = this.b[paramInt];
    View localView1 = locala.k();
    boolean bool1 = b(paramInt);
    int i = 0;
    int j;
    label54: int k;
    label75: View localView2;
    label103: View localView3;
    label132: View localView4;
    if (i < this.b.length)
      if (!this.b[i].j)
        if (paramInt == i)
        {
          j = 1;
          if (!this.d)
            break label317;
          if ((j == 0) || (!bool1))
            break label286;
          k = 2130837715;
          localView1.setBackgroundResource(k);
          localView2 = localView1.findViewById(2131296277);
          if (!locala.n)
            break label326;
          localView2.setVisibility(8);
          localView3 = localView1.findViewById(2131296275);
          if ((!b()) || (locala.l))
            break label335;
          localView3.setVisibility(8);
          if (b(paramInt))
            localView3.setVisibility(8);
          localView4 = localView1.findViewById(2131296265);
          if (localView4 != null)
            if (!locala.m)
              break label344;
        }
    label286: label317: label326: label335: label344: for (int n = 0; ; n = 4)
    {
      localView4.setVisibility(n);
      CheckBox localCheckBox = (CheckBox)localView1.findViewById(2131296415);
      if (localCheckBox != null)
      {
        localCheckBox.setVisibility(0);
        View localView5 = localView1.findViewById(2131296413);
        RelativeLayout.LayoutParams localLayoutParams = (RelativeLayout.LayoutParams)localView3.getLayoutParams();
        localLayoutParams.addRule(3, localView5.getId());
        localView3.setLayoutParams(localLayoutParams);
      }
      localView1.setOnClickListener(locala.h);
      boolean bool2 = locala.j;
      int m = 0;
      if (bool2)
        m = 8;
      localView1.setVisibility(m);
      return;
      j = 0;
      break label54;
      i++;
      break;
      j = 0;
      break label54;
      if (j != 0)
      {
        k = 2130837714;
        break label75;
      }
      if (bool1)
      {
        k = 2130837713;
        break label75;
      }
      k = 2130837712;
      break label75;
      k = getBackgroundResourceForStraightCorners();
      break label75;
      localView2.setVisibility(0);
      break label103;
      localView3.setVisibility(0);
      break label132;
    }
  }

  private boolean b(int paramInt)
  {
    for (int i = -1 + this.b.length; ; i--)
    {
      boolean bool = false;
      if (i >= 0)
      {
        if (this.b[i].j)
          continue;
        bool = false;
        if (paramInt == i)
          bool = true;
      }
      return bool;
    }
  }

  private void c(int paramInt)
  {
    d(paramInt).setError("");
  }

  private TextView d(int paramInt)
  {
    return (TextView)getChildAt(paramInt).findViewById(2131296277);
  }

  private int g(String paramString)
  {
    for (int i = 0; i < this.b.length; i++)
      if (this.b[i].b == paramString)
        return i;
    return -1;
  }

  private int getBackgroundResourceForStraightCorners()
  {
    return 2130837712;
  }

  public final void a()
  {
    a[] arrayOfa = this.b;
    int i = arrayOfa.length;
    for (int j = 0; j < i; j++)
      arrayOfa[j].a();
  }

  public final void a(String paramString)
  {
    c(g(paramString));
  }

  public final void b(String paramString)
  {
    d(g(paramString)).setError(null);
  }

  public boolean b()
  {
    return false;
  }

  public final Object c(String paramString)
  {
    return this.b[g(paramString)].f();
  }

  public final void c()
  {
    for (int i = 0; i < this.b.length; i++)
      a(i);
  }

  public final String d(String paramString)
  {
    return this.b[g(paramString)].g();
  }

  public final void d()
  {
    this.d = false;
  }

  public final a e(String paramString)
  {
    return this.b[g(paramString)];
  }

  public final a f(String paramString)
  {
    for (a locala : this.b)
      if (paramString.equals(locala.r))
        return locala;
    return null;
  }

  public a[] getRows()
  {
    return this.b;
  }

  protected void onRestoreInstanceState(Parcelable paramParcelable)
  {
    int i = 0;
    if ((paramParcelable instanceof DetailViewState))
    {
      DetailViewState localDetailViewState = (DetailViewState)paramParcelable;
      if (this.b != null)
      {
        ArrayList localArrayList1 = localDetailViewState.a();
        int j = Math.min(this.b.length, Math.min(this.e.size(), localArrayList1.size()));
        for (int k = 0; k < j; k++)
        {
          findViewById(((Integer)this.e.get(k)).intValue()).setId(((Integer)localArrayList1.get(k)).intValue());
          this.b[k].a(((Integer)localArrayList1.get(k)).intValue());
        }
        this.e = localArrayList1;
        Iterator localIterator = localDetailViewState.b().iterator();
        while (localIterator.hasNext())
          c(((Integer)localIterator.next()).intValue());
        ArrayList localArrayList2 = localDetailViewState.c();
        while (i < localArrayList2.size())
        {
          this.b[i].a((Parcelable)localArrayList2.get(i));
          i++;
        }
      }
      super.onRestoreInstanceState(localDetailViewState.getSuperState());
      return;
    }
    super.onRestoreInstanceState(paramParcelable);
  }

  protected Parcelable onSaveInstanceState()
  {
    ArrayList localArrayList1 = new ArrayList();
    ArrayList localArrayList2 = new ArrayList();
    if (this.b != null)
    {
      int i = 0;
      if (i < this.b.length)
      {
        if (d(i).getError() != null);
        for (int j = 1; ; j = 0)
        {
          if (j != 0)
            localArrayList1.add(Integer.valueOf(i));
          localArrayList2.add(this.b[i].o());
          i++;
          break;
        }
      }
    }
    return new DetailViewState(super.onSaveInstanceState(), this.e, localArrayList1, localArrayList2);
  }

  public void setRows(a[] paramArrayOfa)
  {
    removeAllViews();
    ArrayList localArrayList = new ArrayList();
    int i = paramArrayOfa.length;
    for (int j = 0; j < i; j++)
    {
      a locala2 = paramArrayOfa[j];
      if (locala2 != null)
        localArrayList.add(locala2);
    }
    this.b = ((a[])localArrayList.toArray(new a[0]));
    int k = this.b.length;
    int m = 0;
    if (m < k)
    {
      a locala1 = this.b[m];
      locala1.l();
      View localView1 = this.a.inflate(locala1.j(), null);
      boolean bool;
      label120: View localView2;
      if (locala1.h != null)
      {
        bool = true;
        localView1.setClickable(bool);
        localView1.setFocusable(bool);
        localView2 = localView1.findViewById(2131296265);
        if (localView2 != null)
          if (!bool)
            break label314;
      }
      label314: for (int i1 = 0; ; i1 = 4)
      {
        localView2.setVisibility(i1);
        TextView localTextView = (TextView)localView1.findViewById(2131296277);
        localTextView.setText(locala1.c);
        localTextView.setTextColor(getContext().getResources().getColor(locala1.e));
        View localView3 = localView1.findViewById(2131296276);
        if (!locala1.q)
          localView1.setEnabled(false);
        int n;
        do
          n = this.c.nextInt();
        while ((n < 0) || (findViewById(n) != null));
        this.e.add(Integer.valueOf(n));
        localView3.setId(n);
        locala1.a(n);
        addView(localView1);
        locala1.a(localView1, getContext());
        a(m);
        m++;
        break;
        bool = false;
        break label120;
      }
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.view.detail.DetailView
 * JD-Core Version:    0.6.2
 */