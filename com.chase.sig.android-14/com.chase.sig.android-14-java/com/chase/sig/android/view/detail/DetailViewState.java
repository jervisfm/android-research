package com.chase.sig.android.view.detail;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.view.View.BaseSavedState;
import java.util.ArrayList;

public class DetailViewState extends View.BaseSavedState
{
  public static final Parcelable.Creator<DetailViewState> CREATOR = new h();
  private ArrayList<Integer> a;
  private ArrayList<Integer> b;
  private ArrayList<Parcelable> c;

  public DetailViewState(Parcel paramParcel)
  {
    super(paramParcel);
    this.a = ((ArrayList)paramParcel.readSerializable());
    this.b = ((ArrayList)paramParcel.readSerializable());
    this.c = ((ArrayList)paramParcel.readSerializable());
  }

  public DetailViewState(Parcelable paramParcelable, ArrayList<Integer> paramArrayList1, ArrayList<Integer> paramArrayList2, ArrayList<Parcelable> paramArrayList)
  {
    super(paramParcelable);
    this.a = paramArrayList1;
    this.b = paramArrayList2;
    this.c = paramArrayList;
  }

  public final ArrayList<Integer> a()
  {
    return this.a;
  }

  public final ArrayList<Integer> b()
  {
    return this.b;
  }

  public final ArrayList<Parcelable> c()
  {
    return this.c;
  }

  public int describeContents()
  {
    return getSuperState().describeContents();
  }

  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    super.writeToParcel(paramParcel, paramInt);
    paramParcel.writeSerializable(this.a);
    paramParcel.writeSerializable(this.b);
    if (this.c != null)
      paramParcel.writeTypedList(this.c);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.view.detail.DetailViewState
 * JD-Core Version:    0.6.2
 */