package com.chase.sig.android.view.detail;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler.Callback;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;
import android.widget.TextView;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;

public class TogglableDetailRow extends a<DetailRow, String>
  implements Parcelable
{
  private boolean a = false;
  private String u;
  private String v;
  private String w;
  private String x;
  private Bundle y;

  public TogglableDetailRow(String paramString1, String paramString2, Handler.Callback paramCallback, Bundle paramBundle)
  {
    super(paramString1, paramString2);
    this.m = false;
    this.y = paramBundle;
    this.u = paramString1;
    this.w = paramString2;
    this.c = paramString1;
    this.h = new v(this, paramCallback);
  }

  private TextView r()
  {
    return (TextView)this.t.findViewById(this.s);
  }

  protected final void a(Context paramContext)
  {
    BehaviorAnalyticsAspect.a().a(this);
    ((TextView)this.t.findViewById(2131296277)).setTextColor(paramContext.getResources().getColor(2131099675));
  }

  public final void a(Parcelable paramParcelable)
  {
    super.a(paramParcelable);
    if ((paramParcelable instanceof Bundle))
    {
      boolean bool1 = ((Bundle)paramParcelable).getBoolean("toggle_state", false);
      boolean bool2 = false;
      if (!bool1)
        bool2 = true;
      this.a = bool2;
      p();
    }
  }

  public final void a(String paramString1, String paramString2)
  {
    this.v = paramString1;
    this.x = paramString2;
  }

  public int describeContents()
  {
    return 0;
  }

  public final Parcelable o()
  {
    Bundle localBundle = new Bundle();
    localBundle.putBoolean("toggle_state", this.a);
    return localBundle;
  }

  public final void p()
  {
    boolean bool;
    String str1;
    if (!this.a)
    {
      bool = true;
      this.a = bool;
      if (!this.a)
        break label78;
      str1 = this.v;
      label26: this.c = str1;
      ((TextView)this.t.findViewById(2131296277)).setText(str1);
      if (!this.a)
        break label86;
    }
    label78: label86: for (String str2 = this.x; ; str2 = this.w)
    {
      b(str2);
      r().setText(str2);
      return;
      bool = false;
      break;
      str1 = this.u;
      break label26;
    }
  }

  public final boolean q()
  {
    return (this.v != null) || (this.x != null);
  }

  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.view.detail.TogglableDetailRow
 * JD-Core Version:    0.6.2
 */