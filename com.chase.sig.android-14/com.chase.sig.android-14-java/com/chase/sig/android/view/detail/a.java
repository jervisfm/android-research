package com.chase.sig.android.view.detail;

import android.content.Context;
import android.content.res.Resources;
import android.os.Parcelable;
import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.util.s;
import com.chase.sig.android.view.x;

public abstract class a<T extends a, ValueType>
{
  public String b = "UNDEFINED";
  public String c;
  protected ValueType d;
  public int e;
  protected int f;
  protected String g;
  public View.OnClickListener h;
  public int i = 2130903075;
  public boolean j;
  protected ValueType k;
  public boolean l;
  public boolean m;
  public boolean n;
  public boolean o;
  public boolean p;
  public boolean q = true;
  public String r;
  protected int s = 2131296276;
  protected View t;

  public a(int paramInt, ValueType paramValueType)
  {
    this(d(paramInt), paramValueType);
  }

  public a(String paramString, ValueType paramValueType)
  {
    this.c = paramString;
    this.d = paramValueType;
    this.k = paramValueType;
    this.e = 2131099650;
    this.f = 2131099650;
  }

  protected static String d(int paramInt)
  {
    return ChaseApplication.a().getResources().getString(paramInt);
  }

  public final T a(String paramString)
  {
    this.g = paramString;
    return this;
  }

  public final void a()
  {
    this.d = this.k;
    a(this.k);
  }

  public final void a(int paramInt)
  {
    this.s = paramInt;
  }

  protected abstract void a(Context paramContext);

  public void a(Parcelable paramParcelable)
  {
  }

  protected final void a(View paramView, Context paramContext)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(this, paramView);
    this.t = paramView;
    a(paramContext);
    a(this.k);
  }

  public void a(g.b paramb)
  {
  }

  public void a(x paramx)
  {
  }

  abstract void a(ValueType paramValueType);

  public final T b()
  {
    if (!this.j);
    for (boolean bool = true; ; bool = false)
    {
      this.j = bool;
      return this;
    }
  }

  public final T b(int paramInt)
  {
    this.f = paramInt;
    return this;
  }

  protected void b(ValueType paramValueType)
  {
    this.d = paramValueType;
  }

  public final a c()
  {
    this.l = true;
    return this;
  }

  public final T c(int paramInt)
  {
    this.g = d(paramInt);
    return this;
  }

  public final a<T, ValueType> d()
  {
    this.m = true;
    return this;
  }

  public final int e()
  {
    return this.s;
  }

  public Object f()
  {
    return this.d;
  }

  public final String g()
  {
    if (f() == null)
      return null;
    return f().toString();
  }

  public final int h()
  {
    return this.f;
  }

  public final String i()
  {
    return this.g;
  }

  public int j()
  {
    return this.i;
  }

  public View k()
  {
    return this.t;
  }

  public final void l()
  {
    this.s = 2131296276;
    this.t = null;
  }

  public final boolean m()
  {
    return (this.o) || (this.p);
  }

  public boolean n()
  {
    return (!this.o) || (!s.l(g()));
  }

  public Parcelable o()
  {
    return null;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.view.detail.a
 * JD-Core Version:    0.6.2
 */