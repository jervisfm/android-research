package com.chase.sig.android.view.detail;

import android.content.Context;
import android.content.res.Resources;
import android.view.View;
import android.widget.TextView;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.util.s;

public final class b extends a<DetailRow, String>
{
  String a = "";

  public b(String paramString1, String paramString2, String paramString3)
  {
    super(paramString1, paramString2);
    this.a = paramString3;
  }

  private TextView p()
  {
    return (TextView)this.t.findViewById(this.s);
  }

  protected final void a(Context paramContext)
  {
    BehaviorAnalyticsAspect.a().a(this);
    p().setTextColor(paramContext.getResources().getColor(h()));
    if (s.m(this.a))
    {
      ((TextView)this.t.findViewById(2131296295)).setText(this.a);
      return;
    }
    ((TextView)this.t.findViewById(2131296295)).setVisibility(8);
  }

  public final int j()
  {
    return 2130903051;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.view.detail.b
 * JD-Core Version:    0.6.2
 */