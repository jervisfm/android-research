package com.chase.sig.android.view.detail;

import android.content.Context;
import android.view.View;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.util.Dollar;
import com.chase.sig.android.view.AmountView;

public final class c extends a<c, Dollar>
{
  public c(String paramString)
  {
    this(paramString, new Dollar(""));
  }

  public c(String paramString, Dollar paramDollar)
  {
    super(paramString, paramDollar);
  }

  private void a(Dollar paramDollar)
  {
    p().setText(paramDollar.g());
  }

  protected final void a(Context paramContext)
  {
    BehaviorAnalyticsAspect.a().a(this);
    p().setHint(this.g);
  }

  public final void a(g.b paramb)
  {
    if (m())
      p().addTextChangedListener(paramb);
  }

  public final void b(String paramString)
  {
    a(new Dollar(paramString));
  }

  public final Object f()
  {
    return p().getDollarAmount();
  }

  public final int j()
  {
    return 2130903052;
  }

  public final boolean n()
  {
    return p().a();
  }

  public final AmountView p()
  {
    return (AmountView)k().findViewById(this.s);
  }

  public final Dollar q()
  {
    return p().getDollarAmount();
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.view.detail.c
 * JD-Core Version:    0.6.2
 */