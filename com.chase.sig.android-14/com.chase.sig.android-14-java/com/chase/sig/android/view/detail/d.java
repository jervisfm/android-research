package com.chase.sig.android.view.detail;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;

public final class d extends a<d, String>
{
  public View.OnClickListener a;
  private int u = -1;

  public d(String paramString1, String paramString2)
  {
    super(paramString1, paramString2);
  }

  private Button r()
  {
    return (Button)this.t.findViewById(this.s);
  }

  protected final void a(Context paramContext)
  {
    BehaviorAnalyticsAspect.a().a(this);
    r().setHint(this.g);
    if (this.a != null)
      r().setOnClickListener(this.a);
    if (this.u != -1)
      r().setBackgroundResource(this.u);
  }

  public final void b(String paramString)
  {
    r().setText(paramString);
  }

  public final int j()
  {
    return 2130903076;
  }

  public final String p()
  {
    if (this.t == null)
      return (String)this.k;
    return r().getText().toString();
  }

  public final d q()
  {
    this.u = 2130837603;
    return this;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.view.detail.d
 * JD-Core Version:    0.6.2
 */