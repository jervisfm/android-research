package com.chase.sig.android.view.detail;

import android.content.Context;
import android.view.View;
import android.widget.TextView;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;

public final class e extends a<DetailRow, String>
{
  String a = "";

  public e(String paramString1, String paramString2, String paramString3)
  {
    super(paramString1, paramString2);
    this.a = paramString3;
  }

  protected final void a(Context paramContext)
  {
    BehaviorAnalyticsAspect.a().a(this);
    ((TextView)this.t.findViewById(2131296417)).setText(this.a);
  }

  public final int j()
  {
    return 2130903083;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.view.detail.e
 * JD-Core Version:    0.6.2
 */