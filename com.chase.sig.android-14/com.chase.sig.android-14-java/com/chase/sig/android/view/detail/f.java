package com.chase.sig.android.view.detail;

import android.app.Activity;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import com.chase.sig.android.view.AmountView;
import com.chase.sig.android.view.JPSpinner;
import com.chase.sig.android.view.x;

public final class f
{
  private DetailView a;
  private int b;
  private Activity c;

  public f(DetailView paramDetailView, Activity paramActivity, int paramInt)
  {
    this.a = paramDetailView;
    this.b = paramInt;
    this.c = paramActivity;
    a();
  }

  private void a()
  {
    a[] arrayOfa = this.a.getRows();
    a locala = new a();
    int i = arrayOfa.length;
    for (int j = 0; j < i; j++)
    {
      a locala1 = arrayOfa[j];
      if (q.class.isInstance(locala1))
        ((q)locala1).q().a(new b());
      if ((locala1 instanceof l))
        ((l)locala1).p().addTextChangedListener(locala);
      if ((locala1 instanceof c))
        ((c)locala1).p().addTextChangedListener(locala);
    }
  }

  protected final class a
    implements TextWatcher
  {
    protected a()
    {
    }

    public final void afterTextChanged(Editable paramEditable)
    {
      f.a(f.this);
    }

    public final void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
    {
    }

    public final void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
    {
    }
  }

  protected final class b
    implements x
  {
    protected b()
    {
    }

    public final void a(int paramInt)
    {
      f.a(f.this);
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.view.detail.f
 * JD-Core Version:    0.6.2
 */