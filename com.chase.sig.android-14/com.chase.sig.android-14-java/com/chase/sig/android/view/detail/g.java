package com.chase.sig.android.view.detail;

import android.text.Editable;
import android.text.TextWatcher;
import com.chase.sig.android.activity.eb;
import com.chase.sig.android.util.s;
import com.chase.sig.android.view.x;

public final class g
{
  private DetailView a;
  private eb b;
  private int c;
  private a d;
  private b e;

  public g(DetailView paramDetailView, eb parameb, int paramInt)
  {
    this.a = paramDetailView;
    this.b = parameb;
    this.c = paramInt;
    a();
  }

  private void a()
  {
    for (a locala : this.a.getRows())
    {
      if (q.class.isInstance(locala))
      {
        this.d = new a();
        locala.a(this.d);
      }
      this.e = new b();
      locala.a(this.e);
    }
    this.b.b(b(), this.c);
  }

  private boolean b()
  {
    for (a locala : this.a.getRows())
    {
      if ((locala.o) && (s.l(locala.g())));
      for (int k = 1; k != 0; k = 0)
        return false;
    }
    return true;
  }

  protected final class a
    implements x
  {
    protected a()
    {
    }

    public final void a(int paramInt)
    {
      g.a(g.this);
    }
  }

  public final class b
    implements TextWatcher
  {
    public b()
    {
    }

    public final void afterTextChanged(Editable paramEditable)
    {
      g.a(g.this);
    }

    public final void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
    {
    }

    public final void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
    {
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.view.detail.g
 * JD-Core Version:    0.6.2
 */