package com.chase.sig.android.view.detail;

import com.chase.sig.android.service.IServiceError;
import com.chase.sig.android.service.ServiceErrorAttribute;
import java.util.Iterator;
import java.util.List;

public final class i
{
  private DetailView a;

  public i(DetailView paramDetailView)
  {
    this.a = paramDetailView;
  }

  public final void a(List<IServiceError> paramList)
  {
    if (paramList == null)
      return;
    Iterator localIterator1 = paramList.iterator();
    while (localIterator1.hasNext())
    {
      Iterator localIterator2 = ((IServiceError)localIterator1.next()).e().iterator();
      while (localIterator2.hasNext())
      {
        ServiceErrorAttribute localServiceErrorAttribute = (ServiceErrorAttribute)localIterator2.next();
        if (localServiceErrorAttribute.b())
        {
          a locala = this.a.f(localServiceErrorAttribute.a());
          if (locala != null)
            this.a.a(locala.b);
        }
      }
    }
  }

  public final boolean a()
  {
    boolean bool = true;
    a[] arrayOfa = this.a.getRows();
    int i = arrayOfa.length;
    int j = 0;
    if (j < i)
    {
      a locala = arrayOfa[j];
      if (!locala.n())
      {
        this.a.a(locala.b);
        bool = false;
      }
      while (true)
      {
        j++;
        break;
        this.a.b(locala.b);
      }
    }
    return bool;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.view.detail.i
 * JD-Core Version:    0.6.2
 */