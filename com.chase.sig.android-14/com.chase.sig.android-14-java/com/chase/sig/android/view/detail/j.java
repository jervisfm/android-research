package com.chase.sig.android.view.detail;

import android.content.Context;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.view.f;

public class j extends a<j, f>
{
  public j(String paramString, f paramf)
  {
    super(paramString, paramf);
  }

  protected void a(Context paramContext)
  {
    BehaviorAnalyticsAspect.a().a(this);
  }

  public final Object f()
  {
    return p().getText();
  }

  public final int j()
  {
    return 2130903077;
  }

  public final View k()
  {
    return this.t;
  }

  public final EditText p()
  {
    return (EditText)this.t.findViewById(this.s);
  }

  public final CheckBox q()
  {
    return (CheckBox)this.t.findViewById(2131296415);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.view.detail.j
 * JD-Core Version:    0.6.2
 */