package com.chase.sig.android.view.detail;

import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;

final class k
  implements CompoundButton.OnCheckedChangeListener
{
  k(j paramj)
  {
  }

  public final void onCheckedChanged(CompoundButton paramCompoundButton, boolean paramBoolean)
  {
    boolean bool1 = true;
    EditText localEditText1 = this.a.p();
    boolean bool2;
    boolean bool3;
    label41: EditText localEditText3;
    if (!paramBoolean)
    {
      bool2 = bool1;
      localEditText1.setEnabled(bool2);
      EditText localEditText2 = this.a.p();
      if (paramBoolean)
        break label74;
      bool3 = bool1;
      localEditText2.setFocusable(bool3);
      localEditText3 = this.a.p();
      if (paramBoolean)
        break label80;
    }
    while (true)
    {
      localEditText3.setFocusableInTouchMode(bool1);
      return;
      bool2 = false;
      break;
      label74: bool3 = false;
      break label41;
      label80: bool1 = false;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.view.detail.k
 * JD-Core Version:    0.6.2
 */