package com.chase.sig.android.view.detail;

import android.content.Context;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputFilter.LengthFilter;
import android.text.Selection;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.util.s;
import java.util.ArrayList;
import java.util.List;

public final class l extends a<l, String>
{
  public int a = 0;
  public int u = 0;
  public String v = "";

  public l(int paramInt, String paramString)
  {
    super(paramInt, paramString);
  }

  public l(String paramString)
  {
    this(paramString, "");
  }

  public l(String paramString1, String paramString2)
  {
    super(paramString1, paramString2);
  }

  protected final void a(Context paramContext)
  {
    BehaviorAnalyticsAspect.a().a(this);
    p().setHint(this.g);
    ArrayList localArrayList = new ArrayList();
    if (this.a > 0)
      localArrayList.add(new InputFilter.LengthFilter(this.a));
    Object localObject;
    switch (this.u)
    {
    default:
      if (s.m(this.v))
        localArrayList.add(new m(this));
      p().setFilters((InputFilter[])localArrayList.toArray(new InputFilter[0]));
      int i = this.u;
      localObject = null;
      switch (i)
      {
      default:
      case 5:
      case 4:
      }
      break;
    case 4:
    case 5:
    case 1:
    case 2:
    case 3:
    }
    while (true)
    {
      if (localObject != null)
      {
        p().addTextChangedListener((TextWatcher)localObject);
        p().setInputType(3);
      }
      return;
      this.v = "1234567890-";
      this.a = 10;
      break;
      this.v = "1234567890-";
      this.a = 12;
      p().setOnKeyListener(new n(this));
      break;
      this.v = "1234567890-,.";
      break;
      this.v = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890 .,()-'";
      break;
      this.v = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890 -";
      break;
      localObject = new PhoneNumberFormattingTextWatcher();
      continue;
      localObject = new a((byte)0);
    }
  }

  public final void a(g.b paramb)
  {
    if (m())
      p().addTextChangedListener(paramb);
  }

  public final void b(String paramString)
  {
    p().setText(paramString);
  }

  public final Object f()
  {
    return p().getText();
  }

  public final int j()
  {
    return 2130903095;
  }

  public final boolean n()
  {
    boolean bool = true;
    if (!super.n())
      bool = false;
    String str;
    do
    {
      return bool;
      str = g();
    }
    while ((!this.o) && (s.l(str)));
    switch (this.u)
    {
    default:
      return bool;
    case 4:
      return s.u(str);
    case 5:
    }
    return s.v(str);
  }

  public final EditText p()
  {
    return (EditText)this.t.findViewById(this.s);
  }

  public final l q()
  {
    this.u = 2;
    return this;
  }

  private final class a
    implements TextWatcher
  {
    private boolean b;
    private boolean c;
    private int d;
    private boolean e;

    private a()
    {
    }

    public final void afterTextChanged(Editable paramEditable)
    {
      if (!this.b)
      {
        this.b = true;
        if ((this.c) && (this.d > 0))
        {
          if (!this.e)
            break label108;
          if (-1 + this.d < paramEditable.length())
            paramEditable.delete(-1 + this.d, this.d);
        }
        int i = 0;
        while (true)
        {
          if (i >= paramEditable.length())
            break label147;
          if (paramEditable.charAt(i) == '-')
          {
            paramEditable.delete(i, i + 1);
            continue;
            label108: if (this.d >= paramEditable.length())
              break;
            paramEditable.delete(this.d, 1 + this.d);
            break;
          }
          i++;
        }
        label147: if (paramEditable.length() > 5)
          paramEditable.insert(5, "-");
        this.b = false;
      }
    }

    public final void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
    {
      if (!this.b)
      {
        int i = Selection.getSelectionStart(paramCharSequence);
        int j = Selection.getSelectionEnd(paramCharSequence);
        if ((paramCharSequence.length() <= 1) || (paramInt2 != 1) || (paramInt3 != 0) || (paramCharSequence.charAt(paramInt1) != '-') || (i != j))
          break label88;
        this.c = true;
        this.d = paramInt1;
        if (i == paramInt1 + 1)
          this.e = true;
      }
      else
      {
        return;
      }
      this.e = false;
      return;
      label88: this.c = false;
    }

    public final void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
    {
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.view.detail.l
 * JD-Core Version:    0.6.2
 */