package com.chase.sig.android.view.detail;

import android.text.Editable;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.widget.EditText;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;

final class n
  implements View.OnKeyListener
{
  n(l paraml)
  {
  }

  public final boolean onKey(View paramView, int paramInt, KeyEvent paramKeyEvent)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView, paramInt, paramKeyEvent);
    EditText localEditText = (EditText)paramView;
    int i;
    if (localEditText.getSelectionStart() == 0)
    {
      i = 1;
      if (localEditText.getText().length() > 11)
        if ((paramInt < 7) || (paramInt > 16))
          break label81;
    }
    label81: for (int j = 1; ; j = 0)
    {
      if (j == 0)
        if ((i == 0) || (paramInt != 8))
          break label87;
      return true;
      i = 0;
      break;
    }
    label87: return false;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.view.detail.n
 * JD-Core Version:    0.6.2
 */