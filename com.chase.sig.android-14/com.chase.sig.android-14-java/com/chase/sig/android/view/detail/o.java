package com.chase.sig.android.view.detail;

import android.content.Context;
import android.view.View;
import android.widget.ArrayAdapter;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.domain.LabeledValue;
import com.chase.sig.android.domain.MMBFrequency;
import com.chase.sig.android.view.JPSpinner;
import java.util.ArrayList;
import java.util.Iterator;

public final class o extends q
{
  private ArrayList<LabeledValue> a;

  public o(String paramString1, String paramString2)
  {
    super(paramString1, MMBFrequency.a(MMBFrequency.a(paramString2)));
  }

  public o(String paramString1, String paramString2, ArrayList<LabeledValue> paramArrayList)
  {
    super(paramString1, a(paramString2, paramArrayList));
    this.a = paramArrayList;
  }

  private static int a(String paramString, ArrayList<LabeledValue> paramArrayList)
  {
    for (int i = 0; i < paramArrayList.size(); i++)
      if (paramString.equalsIgnoreCase(((LabeledValue)paramArrayList.get(i)).b()))
        return i;
    return -1;
  }

  private String[] r()
  {
    String[] arrayOfString = new String[this.a.size()];
    Iterator localIterator = this.a.iterator();
    int j;
    for (int i = 0; localIterator.hasNext(); i = j)
    {
      j = i + 1;
      arrayOfString[i] = ((LabeledValue)localIterator.next()).a();
    }
    return arrayOfString;
  }

  protected final void a(Context paramContext)
  {
    BehaviorAnalyticsAspect.a().a(this);
    super.a(paramContext);
    JPSpinner localJPSpinner = q();
    if (this.a != null);
    for (ArrayAdapter localArrayAdapter = new ArrayAdapter(this.t.getContext(), 2130903213, r()); ; localArrayAdapter = new ArrayAdapter(this.t.getContext(), 2130903213, MMBFrequency.a()))
    {
      localJPSpinner.setAdapter(localArrayAdapter);
      q().setSelection(((Integer)this.d).intValue());
      return;
    }
  }

  public final String p()
  {
    return ((LabeledValue)this.a.get(q().getSelectedItemPosition())).b();
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.view.detail.o
 * JD-Core Version:    0.6.2
 */