package com.chase.sig.android.view.detail;

import android.content.Context;
import android.content.res.Resources;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;

public final class p extends a<DetailRow, String>
{
  private int a = -1;

  public p(String paramString)
  {
    super(paramString, "");
  }

  private ImageView p()
  {
    return (ImageView)this.t.findViewById(2131296416);
  }

  private TextView q()
  {
    return (TextView)this.t.findViewById(this.s);
  }

  protected final void a(Context paramContext)
  {
    BehaviorAnalyticsAspect.a().a(this);
    q().setTextColor(paramContext.getResources().getColor(h()));
  }

  public final int j()
  {
    return 2130903078;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.view.detail.p
 * JD-Core Version:    0.6.2
 */