package com.chase.sig.android.view.detail;

import android.content.Context;
import android.view.View;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.view.JPSpinner;
import com.chase.sig.android.view.x;

public class q extends a<q, Integer>
{
  private x a;

  public q(int paramInt)
  {
    super(d(2131165332), Integer.valueOf(paramInt));
  }

  public q(String paramString)
  {
    super(paramString, Integer.valueOf(-1));
  }

  public q(String paramString, int paramInt)
  {
    super(paramString, Integer.valueOf(paramInt));
  }

  public void a(Context paramContext)
  {
    BehaviorAnalyticsAspect.a().a(this);
    JPSpinner localJPSpinner = q();
    if (this.g == null);
    for (String str = this.c; ; str = this.g)
    {
      localJPSpinner.setHint(str);
      if (this.a != null)
        q().a(this.a);
      return;
    }
  }

  public final void a(x paramx)
  {
    if (m())
      q().a(paramx);
  }

  public final q b(x paramx)
  {
    this.a = paramx;
    return this;
  }

  public final Object f()
  {
    if (q().getSelectedItemPosition() == -1)
      return null;
    return super.f();
  }

  public final int j()
  {
    return 2130903079;
  }

  public final JPSpinner q()
  {
    return (JPSpinner)this.t.findViewById(this.s);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.view.detail.q
 * JD-Core Version:    0.6.2
 */