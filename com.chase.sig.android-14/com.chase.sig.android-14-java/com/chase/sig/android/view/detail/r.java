package com.chase.sig.android.view.detail;

import android.content.Context;
import android.text.InputFilter;
import android.text.InputFilter.LengthFilter;
import android.widget.EditText;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.util.s;
import com.chase.sig.android.view.f;

public final class r extends j
{
  public r(String paramString, f paramf)
  {
    super(paramString, paramf);
  }

  protected final void a(Context paramContext)
  {
    BehaviorAnalyticsAspect.a().a(this);
    super.a(paramContext);
    p().setInputType(2);
    InputFilter[] arrayOfInputFilter = new InputFilter[1];
    arrayOfInputFilter[0] = new InputFilter.LengthFilter(3);
    p().setFilters(arrayOfInputFilter);
  }

  public final boolean n()
  {
    if (s.l(r()));
    while ((r().startsWith("0")) || (Integer.parseInt(r()) < 0))
      return false;
    return true;
  }

  public final String r()
  {
    return p().getText().toString();
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.view.detail.r
 * JD-Core Version:    0.6.2
 */