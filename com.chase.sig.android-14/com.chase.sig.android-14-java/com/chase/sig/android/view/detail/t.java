package com.chase.sig.android.view.detail;

import android.content.Context;
import android.text.TextUtils.TruncateAt;
import android.view.View;
import android.widget.TextView;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;

public final class t extends a<t, String>
{
  private String a;
  private boolean u;

  public t(String paramString1, String paramString2)
  {
    super(paramString1, "");
    this.a = paramString2;
  }

  public t(String paramString1, String paramString2, byte paramByte)
  {
    this(paramString1, paramString2);
    this.u = true;
  }

  protected final void a(Context paramContext)
  {
    BehaviorAnalyticsAspect.a().a(this);
    ((TextView)this.t.findViewById(2131296417)).setText(this.a);
    TextView localTextView = (TextView)this.t.findViewById(2131296277);
    if (!this.u)
    {
      localTextView.setSingleLine(true);
      localTextView.setEllipsize(TextUtils.TruncateAt.END);
    }
  }

  public final int j()
  {
    return 2130903081;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.view.detail.t
 * JD-Core Version:    0.6.2
 */