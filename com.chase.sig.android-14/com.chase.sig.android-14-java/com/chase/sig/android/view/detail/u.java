package com.chase.sig.android.view.detail;

import android.content.Context;
import android.view.View;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;
import com.chase.sig.android.view.TickerValueTextView;

public final class u extends a<u, String>
{
  private boolean a = false;

  public u(String paramString1, String paramString2)
  {
    super(paramString1, paramString2);
  }

  private TickerValueTextView p()
  {
    return (TickerValueTextView)this.t.findViewById(this.s);
  }

  protected final void a(Context paramContext)
  {
    BehaviorAnalyticsAspect.a().a(this);
  }

  public final int j()
  {
    return 2130903082;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.view.detail.u
 * JD-Core Version:    0.6.2
 */