package com.chase.sig.android.view.detail;

import android.os.Bundle;
import android.os.Handler.Callback;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;

final class v
  implements View.OnClickListener
{
  v(TogglableDetailRow paramTogglableDetailRow, Handler.Callback paramCallback)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    Message localMessage = new Message();
    TogglableDetailRow.a(this.b).putString("ROW_ID", this.b.b);
    localMessage.setData(TogglableDetailRow.a(this.b));
    this.a.handleMessage(localMessage);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.view.detail.v
 * JD-Core Version:    0.6.2
 */