package com.chase.sig.android.view;

import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

final class e
  implements DialogInterface.OnKeyListener
{
  e(b paramb)
  {
  }

  public final boolean onKey(DialogInterface paramDialogInterface, int paramInt, KeyEvent paramKeyEvent)
  {
    View localView = this.a.getCurrentFocus();
    if ((paramInt == 23) && (paramKeyEvent.getAction() == 0) && (localView.getTag() != null))
      this.a.a((TextView)localView);
    return false;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.view.e
 * JD-Core Version:    0.6.2
 */