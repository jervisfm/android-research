package com.chase.sig.android.view;

import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.TextView;
import com.chase.sig.android.util.s;
import java.util.Map;

public final class g extends af
{
  public g(ListAdapter paramListAdapter)
  {
    super(paramListAdapter);
  }

  public final View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    View localView = super.getView(paramInt, paramView, paramViewGroup);
    Map localMap = (Map)this.a.getItem(paramInt);
    int i;
    TextView localTextView1;
    TextView localTextView2;
    TextView localTextView3;
    if (localMap != null)
    {
      if ((!localMap.containsKey("status")) || (!localMap.containsKey("email")) || (!localMap.containsKey("name")) || (localMap.size() != 3))
        break label212;
      i = 1;
      if (i != 0)
      {
        String str1 = (String)localMap.get("status");
        String str2 = (String)localMap.get("email");
        localTextView1 = (TextView)localView.findViewById(2131296809);
        localTextView2 = (TextView)localView.findViewById(2131296770);
        localTextView3 = (TextView)localView.findViewById(2131296772);
        if ("ACTIVE".equals(str1))
          break label228;
        localTextView1.setText(2131165679);
        localTextView2.setTextColor(Color.parseColor("#696969"));
        if (!s.m(str2))
          break label218;
        localTextView3.setTextColor(Color.parseColor("#696969"));
      }
    }
    while (true)
    {
      localTextView1.setVisibility(0);
      localTextView1.setTextColor(Color.parseColor("#696969"));
      return localView;
      label212: i = 0;
      break;
      label218: localTextView3.setVisibility(8);
    }
    label228: localTextView1.setVisibility(8);
    localTextView2.setTextColor(Color.parseColor("#000000"));
    localTextView3.setVisibility(0);
    localTextView3.setTextColor(Color.parseColor("#000000"));
    return localView;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.view.g
 * JD-Core Version:    0.6.2
 */