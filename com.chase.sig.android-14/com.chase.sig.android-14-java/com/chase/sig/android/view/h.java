package com.chase.sig.android.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.chase.sig.android.domain.Transaction;
import com.chase.sig.android.util.Dollar;
import com.chase.sig.android.util.s;
import java.util.List;

public final class h<T extends Transaction> extends ArrayAdapter<T>
{
  private List<T> a;
  private LayoutInflater b;

  public h(Context paramContext, List<T> paramList)
  {
    super(paramContext, 2130903235, paramList);
    this.a = paramList;
    this.b = ((LayoutInflater)paramContext.getSystemService("layout_inflater"));
  }

  public final int getItemViewType(int paramInt)
  {
    return super.getItemViewType(paramInt);
  }

  public final View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    Transaction localTransaction = (Transaction)this.a.get(paramInt);
    if (paramView == null)
      paramView = this.b.inflate(2130903235, null);
    ((TextView)paramView.findViewById(2131296577)).setText(localTransaction.B());
    ((TextView)paramView.findViewById(2131296964)).setText(s.h(localTransaction.q()));
    ((TextView)paramView.findViewById(2131296966)).setText(localTransaction.e().h());
    ((TextView)paramView.findViewById(2131296968)).setText(localTransaction.t());
    return paramView;
  }

  public final int getViewTypeCount()
  {
    return 1;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.view.h
 * JD-Core Version:    0.6.2
 */