package com.chase.sig.android.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.ShapeDrawable;
import android.view.View;

final class i extends View
{
  i(HomeImageButton paramHomeImageButton, Context paramContext, ShapeDrawable paramShapeDrawable)
  {
    super(paramContext);
  }

  protected final void onDraw(Canvas paramCanvas)
  {
    this.a.setBounds(0, 0, paramCanvas.getWidth(), paramCanvas.getHeight());
    this.a.draw(paramCanvas);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.view.i
 * JD-Core Version:    0.6.2
 */