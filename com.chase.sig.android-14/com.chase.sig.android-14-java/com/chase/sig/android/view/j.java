package com.chase.sig.android.view;

import android.view.View;
import android.view.View.OnFocusChangeListener;

final class j
  implements View.OnFocusChangeListener
{
  j(HomeImageButton paramHomeImageButton)
  {
  }

  public final void onFocusChange(View paramView, boolean paramBoolean)
  {
    if (paramBoolean)
    {
      HomeImageButton.a(this.a).setVisibility(4);
      return;
    }
    HomeImageButton.a(this.a).setVisibility(0);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.view.j
 * JD-Core Version:    0.6.2
 */