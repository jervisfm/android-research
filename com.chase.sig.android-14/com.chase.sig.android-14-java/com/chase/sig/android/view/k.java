package com.chase.sig.android.view;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.graphics.drawable.Drawable;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;

public final class k extends Dialog
  implements DialogInterface
{
  public k(Context paramContext)
  {
    super(paramContext, 2131230842);
  }

  public static final class a
  {
    final Context a;
    public CharSequence b;
    public CharSequence c;
    public Drawable d;
    public int e = 1;
    public DialogInterface.OnClickListener f;
    public DialogInterface.OnCancelListener g;
    ListView h;
    public boolean i = true;
    public String j;
    public View k;
    DialogInterface.OnClickListener l;
    private String m;
    private String n;
    private View o;
    private int p = 0;
    private final int q = 1;
    private final int r = 2;
    private final int s = 2;
    private DialogInterface.OnClickListener t;
    private DialogInterface.OnClickListener u;

    public a(Context paramContext)
    {
      this.a = paramContext;
    }

    private void a(int paramInt1, String paramString, DialogInterface.OnClickListener paramOnClickListener, Dialog paramDialog, View paramView, int paramInt2, boolean paramBoolean)
    {
      ((Button)paramView.findViewById(paramInt1)).setText(paramString);
      this.p = (1 + this.p);
      if (paramBoolean)
      {
        ((Button)paramView.findViewById(paramInt1)).setOnClickListener(new m(this, paramOnClickListener, paramDialog, paramInt2));
        return;
      }
      ((Button)paramView.findViewById(paramInt1)).setOnClickListener(new n(this, paramOnClickListener, paramDialog, paramInt2));
    }

    public final a a(int paramInt)
    {
      return b(this.a.getText(paramInt).toString());
    }

    public final a a(int paramInt, DialogInterface.OnClickListener paramOnClickListener)
    {
      this.m = ((String)this.a.getText(paramInt));
      this.t = paramOnClickListener;
      return this;
    }

    public final a a(String paramString)
    {
      try
      {
        this.c = paramString;
        BehaviorAnalyticsAspect.a();
        BehaviorAnalyticsAspect.a(this);
        return this;
      }
      catch (Throwable localThrowable)
      {
        BehaviorAnalyticsAspect.a();
        BehaviorAnalyticsAspect.a(this);
        throw localThrowable;
      }
    }

    public final a a(String paramString, DialogInterface.OnClickListener paramOnClickListener)
    {
      this.m = paramString;
      this.t = paramOnClickListener;
      return this;
    }

    public final a b(int paramInt)
    {
      try
      {
        this.c = this.a.getText(paramInt);
        BehaviorAnalyticsAspect.a();
        BehaviorAnalyticsAspect.a(this);
        return this;
      }
      catch (Throwable localThrowable)
      {
        BehaviorAnalyticsAspect.a();
        BehaviorAnalyticsAspect.a(this);
        throw localThrowable;
      }
    }

    public final a b(int paramInt, DialogInterface.OnClickListener paramOnClickListener)
    {
      this.n = ((String)this.a.getText(paramInt));
      this.u = paramOnClickListener;
      return this;
    }

    public final a b(String paramString)
    {
      this.c = Html.fromHtml(paramString);
      return this;
    }

    public final a b(String paramString, DialogInterface.OnClickListener paramOnClickListener)
    {
      this.n = paramString;
      this.u = paramOnClickListener;
      return this;
    }

    public final a c(int paramInt)
    {
      this.b = ((String)this.a.getText(paramInt));
      return this;
    }

    public final a c(int paramInt, DialogInterface.OnClickListener paramOnClickListener)
    {
      this.j = ((String)this.a.getText(paramInt));
      this.f = paramOnClickListener;
      return this;
    }

    public final k d(int paramInt)
    {
      LayoutInflater localLayoutInflater = (LayoutInflater)this.a.getSystemService("layout_inflater");
      k localk = new k(this.a);
      View localView1 = localLayoutInflater.inflate(2130903118, null);
      ViewGroup localViewGroup = (ViewGroup)localView1.findViewById(2131296518);
      View localView2;
      if (this.e == 1)
      {
        localView2 = localLayoutInflater.inflate(2130903119, null);
        localViewGroup.addView(localView2);
        if (this.m == null)
          break label412;
        a(2131296520, this.m, this.t, localk, localView1, -1, true);
        label96: if (this.n == null)
          break label427;
        a(2131296522, this.n, this.u, localk, localView1, -2, true);
        label123: if (this.j == null)
          break label442;
        a(2131296521, this.j, this.f, localk, localView1, -3, false);
        label150: if (this.c == null)
          break label457;
        ((TextView)localView1.findViewById(2131296515)).setText(this.c);
        label174: if (this.b != null)
        {
          localView1.findViewById(2131296509).setVisibility(0);
          ((TextView)localView1.findViewById(2131296313)).setText(this.b);
        }
        if (this.d == null)
          break label508;
        localView1.findViewById(2131296511).setBackgroundDrawable(this.d);
        label230: localk.setCancelable(this.i);
        if (this.i)
          localk.setOnCancelListener(this.g);
        if (this.l != null)
        {
          this.h.setOnItemClickListener(new l(this, localk));
          this.h.setSelection(paramInt);
          this.h.setChoiceMode(1);
          this.h.setItemChecked(paramInt, true);
          this.k = this.h;
        }
        if (this.k != null)
        {
          ((FrameLayout)localView1.findViewById(2131296517)).addView(this.k);
          localView1.findViewById(2131296517).setVisibility(0);
          localView1.findViewById(2131296516).setVisibility(0);
          localView1.findViewById(2131296513).setVisibility(8);
        }
        switch (this.p)
        {
        default:
        case 0:
        case 1:
        }
      }
      while (true)
      {
        localk.setContentView(localView1);
        return localk;
        localView2 = localLayoutInflater.inflate(2130903120, null);
        break;
        label412: localView1.findViewById(2131296520).setVisibility(8);
        break label96;
        label427: localView1.findViewById(2131296522).setVisibility(8);
        break label123;
        label442: localView1.findViewById(2131296521).setVisibility(8);
        break label150;
        label457: if (this.o == null)
          break label174;
        ((LinearLayout)localView1.findViewById(2131296515)).removeAllViews();
        ((LinearLayout)localView1.findViewById(2131296515)).addView(this.o, new ViewGroup.LayoutParams(-2, -2));
        break label174;
        label508: localView1.findViewById(2131296511).setBackgroundDrawable(null);
        break label230;
        localView1.findViewById(2131296518).setVisibility(8);
        continue;
        localView1.findViewById(2131296519).setVisibility(0);
        localView1.findViewById(2131296523).setVisibility(0);
      }
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.view.k
 * JD-Core Version:    0.6.2
 */