package com.chase.sig.android.view;

import android.app.Dialog;
import android.content.DialogInterface.OnClickListener;
import android.view.View;
import android.view.View.OnClickListener;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;

final class m
  implements View.OnClickListener
{
  m(k.a parama, DialogInterface.OnClickListener paramOnClickListener, Dialog paramDialog, int paramInt)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    if (this.a != null)
      this.a.onClick(this.b, this.c);
    this.b.dismiss();
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.view.m
 * JD-Core Version:    0.6.2
 */