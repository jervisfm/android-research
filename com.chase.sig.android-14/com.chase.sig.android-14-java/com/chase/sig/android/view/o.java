package com.chase.sig.android.view;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import com.chase.sig.analytics.BehaviorAnalyticsAspect;

final class o
  implements View.OnClickListener
{
  o(JPSortableButtonBar paramJPSortableButtonBar)
  {
  }

  public final void onClick(View paramView)
  {
    BehaviorAnalyticsAspect.a();
    BehaviorAnalyticsAspect.a(paramView);
    boolean bool;
    if (JPSortableButtonBar.a(this.a) == paramView)
    {
      JPSortableButtonBar localJPSortableButtonBar = this.a;
      if (!JPSortableButtonBar.b(this.a))
      {
        bool = true;
        JPSortableButtonBar.a(localJPSortableButtonBar, bool);
      }
    }
    while (true)
    {
      if (JPSortableButtonBar.a(this.a) != null)
        JPSortableButtonBar.a(this.a, JPSortableButtonBar.a(this.a), false);
      JPSortableButtonBar.a(this.a, (Button)paramView);
      JPSortableButtonBar.a(this.a, JPSortableButtonBar.a(this.a), true);
      if (JPSortableButtonBar.c(this.a) != null)
        JPSortableButtonBar.c(this.a).a(((Integer)JPSortableButtonBar.a(this.a).getTag()).intValue(), JPSortableButtonBar.b(this.a));
      return;
      bool = false;
      break;
      JPSortableButtonBar.a(this.a, false);
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.view.o
 * JD-Core Version:    0.6.2
 */