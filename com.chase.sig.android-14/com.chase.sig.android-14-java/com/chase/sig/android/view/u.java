package com.chase.sig.android.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.chase.sig.android.domain.BranchLocation;
import java.util.List;

public final class u extends ArrayAdapter<BranchLocation>
{
  private static LayoutInflater b;
  private List<BranchLocation> a;

  public u(Context paramContext, List<BranchLocation> paramList)
  {
    super(paramContext, 2130903129, paramList);
    this.a = paramList;
    b = (LayoutInflater)paramContext.getSystemService("layout_inflater");
  }

  public final int getItemViewType(int paramInt)
  {
    return super.getItemViewType(paramInt);
  }

  public final View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    BranchLocation localBranchLocation = (BranchLocation)this.a.get(paramInt);
    a locala1;
    String str;
    label127: ImageView localImageView;
    if (paramView == null)
    {
      paramView = b.inflate(2130903129, null);
      a locala2 = new a();
      paramView.setTag(locala2);
      locala1 = locala2;
      locala1.b = ((TextView)paramView.findViewById(2131296550));
      locala1.a = ((TextView)paramView.findViewById(2131296577));
      locala1.c = ((ImageView)paramView.findViewById(2131296576));
      boolean bool = "branch".equalsIgnoreCase(localBranchLocation.d());
      TextView localTextView = locala1.a;
      StringBuilder localStringBuilder = new StringBuilder();
      if (!bool)
        break label249;
      str = "Branch";
      localTextView.setText(str + "\n" + localBranchLocation.j() + " miles");
      locala1.b.setText(localBranchLocation.e() + "\n" + localBranchLocation.a() + ", " + localBranchLocation.c());
      localImageView = locala1.c;
      if (!bool)
        break label256;
    }
    label256: for (int i = 2130837694; ; i = 2130837693)
    {
      localImageView.setImageResource(i);
      return paramView;
      locala1 = (a)paramView.getTag();
      break;
      label249: str = "ATM";
      break label127;
    }
  }

  public final int getViewTypeCount()
  {
    return 1;
  }

  static final class a
  {
    TextView a;
    TextView b;
    ImageView c;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.view.u
 * JD-Core Version:    0.6.2
 */