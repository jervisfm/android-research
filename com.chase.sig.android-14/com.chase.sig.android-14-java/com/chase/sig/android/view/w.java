package com.chase.sig.android.view;

import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.BaseAdapter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public abstract class w extends BaseAdapter
{
  private static int b = 0;
  private List<a> a = new ArrayList();

  protected abstract View a(String paramString);

  public final void a(String paramString, Adapter paramAdapter)
  {
    this.a.add(new a(paramString, paramAdapter));
  }

  public int getCount()
  {
    Iterator localIterator = this.a.iterator();
    int i = 0;
    while (localIterator.hasNext())
      i += 1 + ((a)localIterator.next()).b.getCount();
    return i;
  }

  public Object getItem(int paramInt)
  {
    Iterator localIterator = this.a.iterator();
    while (localIterator.hasNext())
    {
      a locala = (a)localIterator.next();
      if (paramInt == 0)
        return locala;
      int i = 1 + locala.b.getCount();
      if (paramInt < i)
        return locala.b.getItem(paramInt - 1);
      paramInt -= i;
    }
    return null;
  }

  public long getItemId(int paramInt)
  {
    return paramInt;
  }

  public int getItemViewType(int paramInt)
  {
    int i = 1 + b;
    Iterator localIterator = this.a.iterator();
    int j = i;
    while (localIterator.hasNext())
    {
      a locala = (a)localIterator.next();
      if (paramInt == 0)
        return b;
      int k = 1 + locala.b.getCount();
      if (paramInt < k)
        return j + locala.b.getItemViewType(paramInt - 1);
      paramInt -= k;
      j += locala.b.getViewTypeCount();
    }
    return -1;
  }

  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    Iterator localIterator = this.a.iterator();
    while (localIterator.hasNext())
    {
      a locala = (a)localIterator.next();
      if (paramInt == 0)
        return a(locala.a);
      int i = 1 + locala.b.getCount();
      if (paramInt < i)
        return locala.b.getView(paramInt - 1, paramView, paramViewGroup);
      paramInt -= i;
    }
    return null;
  }

  public int getViewTypeCount()
  {
    Iterator localIterator = this.a.iterator();
    int i = 1;
    while (localIterator.hasNext())
      i += ((a)localIterator.next()).b.getViewTypeCount();
    return i;
  }

  public boolean isEnabled(int paramInt)
  {
    return getItemViewType(paramInt) != b;
  }

  final class a
  {
    String a;
    Adapter b;

    a(String paramAdapter, Adapter arg3)
    {
      this.a = paramAdapter;
      Object localObject;
      this.b = localObject;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.view.w
 * JD-Core Version:    0.6.2
 */