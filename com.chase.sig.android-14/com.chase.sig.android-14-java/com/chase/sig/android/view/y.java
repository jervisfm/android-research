package com.chase.sig.android.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.TextView;

public final class y extends LinearLayout
{
  View a;
  private LayoutInflater b;

  public y(Context paramContext, String paramString1, String paramString2, String paramString3)
  {
    super(paramContext, null);
    this.b = ((LayoutInflater)paramContext.getSystemService("layout_inflater"));
    if (paramString3 != null)
    {
      setOrientation(1);
      setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
    }
    for (this.a = this.b.inflate(2130903174, this, true); ; this.a = this.b.inflate(2130903159, this, true))
    {
      LinearLayout localLinearLayout = (LinearLayout)this.a.findViewById(2131296725);
      View localView = localLinearLayout.findViewById(2131296728);
      if (localView != null)
        localView.setVisibility(8);
      localLinearLayout.setBackgroundDrawable(getBackground());
      RelativeLayout localRelativeLayout = (RelativeLayout)localLinearLayout.getChildAt(0);
      ((TextView)localRelativeLayout.getChildAt(0)).setText(paramString1);
      ((TextView)localRelativeLayout.getChildAt(1)).setText(paramString2);
      if (paramString3 != null)
        ((TextView)localRelativeLayout.getChildAt(2)).setText(paramString3);
      return;
      setOrientation(1);
      setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
    }
  }

  public final void a(String paramString)
  {
    z localz = new z(getContext(), paramString);
    ((ViewGroup)this.a).addView(localz, 1);
    LinearLayout localLinearLayout = (LinearLayout)this.a.findViewById(2131296725);
    localLinearLayout.setPadding(localLinearLayout.getPaddingLeft(), localLinearLayout.getPaddingTop(), localLinearLayout.getPaddingRight(), 5);
  }

  public final View getView()
  {
    return this.a;
  }

  public final void setValueColor(int paramInt)
  {
    ((TextView)((LinearLayout)this.a.findViewById(2131296725)).findViewById(2131296727)).setTextColor(paramInt);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.view.y
 * JD-Core Version:    0.6.2
 */