package com.chase.sig.android.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

public final class z extends LinearLayout
{
  private View a;
  private LayoutInflater b;

  public z(Context paramContext, String paramString)
  {
    super(paramContext, null);
    this.b = ((LayoutInflater)paramContext.getSystemService("layout_inflater"));
    setOrientation(1);
    setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
    this.a = this.b.inflate(2130903160, this, true);
    ((TextView)((LinearLayout)this.a.findViewById(2131296729)).getChildAt(0)).setText(paramString);
  }

  public final View getView()
  {
    return this.a;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.chase.sig.android.view.z
 * JD-Core Version:    0.6.2
 */