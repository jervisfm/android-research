package com.google.common.a;

import java.io.IOException;
import java.util.Iterator;
import javax.annotation.CheckReturnValue;

public class a
{
  final String a;

  private a(a parama)
  {
    this.a = parama.a;
  }

  private a(String paramString)
  {
    this.a = ((String)c.a(paramString));
  }

  public static a a(String paramString)
  {
    return new a(paramString);
  }

  static CharSequence a(Object paramObject)
  {
    c.a(paramObject);
    if ((paramObject instanceof CharSequence))
      return (CharSequence)paramObject;
    return paramObject.toString();
  }

  private StringBuilder a(StringBuilder paramStringBuilder, Iterable<?> paramIterable)
  {
    try
    {
      a(paramStringBuilder, paramIterable);
      return paramStringBuilder;
    }
    catch (IOException localIOException)
    {
      throw new AssertionError(localIOException);
    }
  }

  @CheckReturnValue
  public final a a()
  {
    return new b(this, this);
  }

  public <A extends Appendable> A a(A paramA, Iterable<?> paramIterable)
  {
    c.a(paramA);
    Iterator localIterator = paramIterable.iterator();
    if (localIterator.hasNext())
    {
      paramA.append(a(localIterator.next()));
      while (localIterator.hasNext())
      {
        paramA.append(this.a);
        paramA.append(a(localIterator.next()));
      }
    }
    return paramA;
  }

  public final String a(Iterable<?> paramIterable)
  {
    return a(new StringBuilder(), paramIterable).toString();
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.common.a.a
 * JD-Core Version:    0.6.2
 */