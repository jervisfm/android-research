package com.google.common.a;

import javax.annotation.Nullable;

public final class e
{
  public static String a(@Nullable String paramString)
  {
    if (paramString == null)
      paramString = "";
    return paramString;
  }

  public static boolean b(@Nullable String paramString)
  {
    return (paramString == null) || (paramString.length() == 0);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.common.a.e
 * JD-Core Version:    0.6.2
 */