package com.google.common.b;

import com.google.common.a.c;
import java.util.Iterator;

public final class d
{
  static final h<Object> a = new e();
  private static final Iterator<Object> b = new f();

  public static <T> h<T> a(Iterator<T> paramIterator, com.google.common.a.d<? super T> paramd)
  {
    c.a(paramIterator);
    c.a(paramd);
    return new g(paramIterator, paramd);
  }

  public static String a(Iterator<?> paramIterator)
  {
    if (!paramIterator.hasNext())
      return "[]";
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append('[').append(paramIterator.next());
    while (paramIterator.hasNext())
      localStringBuilder.append(", ").append(paramIterator.next());
    return ']';
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.common.b.d
 * JD-Core Version:    0.6.2
 */