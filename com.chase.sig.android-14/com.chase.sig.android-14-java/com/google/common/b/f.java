package com.google.common.b;

import java.util.Iterator;
import java.util.NoSuchElementException;

final class f
  implements Iterator<Object>
{
  public final boolean hasNext()
  {
    return false;
  }

  public final Object next()
  {
    throw new NoSuchElementException();
  }

  public final void remove()
  {
    throw new IllegalStateException();
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.common.b.f
 * JD-Core Version:    0.6.2
 */