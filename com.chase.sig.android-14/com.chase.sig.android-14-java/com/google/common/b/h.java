package com.google.common.b;

import java.util.Iterator;

public abstract class h<E>
  implements Iterator<E>
{
  public final void remove()
  {
    throw new UnsupportedOperationException();
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.common.b.h
 * JD-Core Version:    0.6.2
 */