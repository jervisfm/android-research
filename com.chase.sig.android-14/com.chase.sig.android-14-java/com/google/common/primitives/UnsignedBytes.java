package com.google.common.primitives;

import java.nio.ByteOrder;
import java.security.AccessController;
import java.util.Comparator;
import sun.misc.Unsafe;

public final class UnsignedBytes
{
  public static int a(byte paramByte1, byte paramByte2)
  {
    return (paramByte1 & 0xFF) - (paramByte2 & 0xFF);
  }

  static class LexicographicalComparatorHolder
  {
    static final String a = LexicographicalComparatorHolder.class.getName() + "$UnsafeComparator";
    static final Comparator<byte[]> b = a();

    private static Comparator<byte[]> a()
    {
      try
      {
        Comparator localComparator = (Comparator)Class.forName(a).getEnumConstants()[0];
        return localComparator;
      }
      catch (Throwable localThrowable)
      {
      }
      return a.a;
    }

    static enum UnsafeComparator
      implements Comparator<byte[]>
    {
      static final boolean b;
      static final int c;
      static final Unsafe theUnsafe;

      static
      {
        UnsafeComparator[] arrayOfUnsafeComparator = new UnsafeComparator[1];
        arrayOfUnsafeComparator[0] = a;
        d = arrayOfUnsafeComparator;
        b = ByteOrder.nativeOrder().equals(ByteOrder.LITTLE_ENDIAN);
        Unsafe localUnsafe = (Unsafe)AccessController.doPrivileged(new a());
        theUnsafe = localUnsafe;
        c = localUnsafe.arrayBaseOffset([B.class);
        if (theUnsafe.arrayIndexScale([B.class) != 1)
          throw new AssertionError();
      }
    }

    static enum a
      implements Comparator<byte[]>
    {
      static
      {
        a[] arrayOfa = new a[1];
        arrayOfa[0] = a;
      }
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.common.primitives.UnsignedBytes
 * JD-Core Version:    0.6.2
 */