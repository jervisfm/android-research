package com.google.common.primitives;

import java.lang.reflect.Field;
import java.security.PrivilegedAction;
import sun.misc.Unsafe;

final class a
  implements PrivilegedAction<Object>
{
  public final Object run()
  {
    try
    {
      Field localField = Unsafe.class.getDeclaredField("theUnsafe");
      localField.setAccessible(true);
      Object localObject = localField.get(null);
      return localObject;
    }
    catch (NoSuchFieldException localNoSuchFieldException)
    {
      throw new Error();
    }
    catch (IllegalAccessException localIllegalAccessException)
    {
    }
    throw new Error();
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.common.primitives.a
 * JD-Core Version:    0.6.2
 */