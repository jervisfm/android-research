package com.google.common.primitives;

import java.math.BigInteger;

public final class b
{
  private static final long[] a = new long[37];
  private static final int[] b = new int[37];
  private static final int[] c = new int[37];

  static
  {
    BigInteger localBigInteger = new BigInteger("10000000000000000", 16);
    int i = 2;
    if (i <= 36)
    {
      long[] arrayOfLong = a;
      long l1 = i;
      long l3;
      if (l1 < 0L)
        if (a(-1L, l1) < 0)
          l3 = 0L;
      long l4;
      long l6;
      while (true)
      {
        arrayOfLong[i] = l3;
        int[] arrayOfInt = b;
        l4 = i;
        if (l4 >= 0L)
          break label215;
        if (a(-1L, l4) >= 0)
          break label200;
        l6 = -1L;
        arrayOfInt[i] = ((int)l6);
        c[i] = (-1 + localBigInteger.toString(i).length());
        i++;
        break;
        l3 = 1L;
        continue;
        if (-1L < 0L)
          break label156;
        l3 = -1L / l1;
      }
      label156: long l2 = 9223372036854775807L / l1 << 1;
      if (a(-1L - l2 * l1, l1) >= 0);
      for (int j = 1; ; j = 0)
      {
        l3 = l2 + j;
        break;
      }
      label200: long l5 = -1L;
      while (true)
      {
        l6 = l5 - l4;
        break;
        label215: if (-1L >= 0L)
        {
          l6 = -1L % l4;
          break;
        }
        l5 = -1L - l4 * (9223372036854775807L / l4 << 1);
        if (a(l5, l4) < 0)
          l4 = 0L;
      }
    }
  }

  public static int a(long paramLong1, long paramLong2)
  {
    long l1 = paramLong1 ^ 0x0;
    long l2 = 0x0 ^ paramLong2;
    if (l1 < l2)
      return -1;
    if (l1 > l2)
      return 1;
    return 0;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.common.primitives.b
 * JD-Core Version:    0.6.2
 */