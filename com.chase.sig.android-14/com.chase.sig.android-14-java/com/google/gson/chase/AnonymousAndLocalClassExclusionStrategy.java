package com.google.gson.chase;

final class AnonymousAndLocalClassExclusionStrategy
  implements ExclusionStrategy
{
  private static boolean b(Class<?> paramClass)
  {
    return (!Enum.class.isAssignableFrom(paramClass)) && ((paramClass.isAnonymousClass()) || (paramClass.isLocalClass()));
  }

  public final boolean a(FieldAttributes paramFieldAttributes)
  {
    return b(paramFieldAttributes.c());
  }

  public final boolean a(Class<?> paramClass)
  {
    return b(paramClass);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.AnonymousAndLocalClassExclusionStrategy
 * JD-Core Version:    0.6.2
 */