package com.google.gson.chase;

import com.google.gson.chase.internal..Gson.Preconditions;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.Collection;

final class CamelCaseSeparatorNamingPolicy extends RecursiveFieldNamingPolicy
{
  private final String a;

  public CamelCaseSeparatorNamingPolicy(String paramString)
  {
    .Gson.Preconditions.a(paramString);
    if (!"".equals(paramString));
    for (boolean bool = true; ; bool = false)
    {
      .Gson.Preconditions.a(bool);
      this.a = paramString;
      return;
    }
  }

  protected final String a(String paramString, Type paramType, Collection<Annotation> paramCollection)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    for (int i = 0; i < paramString.length(); i++)
    {
      char c = paramString.charAt(i);
      if ((Character.isUpperCase(c)) && (localStringBuilder.length() != 0))
        localStringBuilder.append(this.a);
      localStringBuilder.append(c);
    }
    return localStringBuilder.toString();
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.CamelCaseSeparatorNamingPolicy
 * JD-Core Version:    0.6.2
 */