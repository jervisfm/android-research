package com.google.gson.chase;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.Collection;

abstract class CompositionFieldNamingPolicy extends RecursiveFieldNamingPolicy
{
  private final RecursiveFieldNamingPolicy[] a;

  public CompositionFieldNamingPolicy(RecursiveFieldNamingPolicy[] paramArrayOfRecursiveFieldNamingPolicy)
  {
    if (paramArrayOfRecursiveFieldNamingPolicy == null)
      throw new NullPointerException("naming policies can not be null.");
    this.a = paramArrayOfRecursiveFieldNamingPolicy;
  }

  protected final String a(String paramString, Type paramType, Collection<Annotation> paramCollection)
  {
    RecursiveFieldNamingPolicy[] arrayOfRecursiveFieldNamingPolicy = this.a;
    int i = arrayOfRecursiveFieldNamingPolicy.length;
    for (int j = 0; j < i; j++)
      paramString = arrayOfRecursiveFieldNamingPolicy[j].a(paramString, paramType, paramCollection);
    return paramString;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.CompositionFieldNamingPolicy
 * JD-Core Version:    0.6.2
 */