package com.google.gson.chase;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

final class DefaultTypeAdapters
{
  static final class DefaultDateTypeAdapter
    implements JsonDeserializer<Date>, JsonSerializer<Date>
  {
    private final DateFormat a;
    private final DateFormat b;
    private final DateFormat c;

    DefaultDateTypeAdapter()
    {
      this(DateFormat.getDateTimeInstance(2, 2, Locale.US), DateFormat.getDateTimeInstance(2, 2));
    }

    public DefaultDateTypeAdapter(int paramInt1, int paramInt2)
    {
      this(DateFormat.getDateTimeInstance(paramInt1, paramInt2, Locale.US), DateFormat.getDateTimeInstance(paramInt1, paramInt2));
    }

    DefaultDateTypeAdapter(String paramString)
    {
      this(new SimpleDateFormat(paramString, Locale.US), new SimpleDateFormat(paramString));
    }

    private DefaultDateTypeAdapter(DateFormat paramDateFormat1, DateFormat paramDateFormat2)
    {
      this.a = paramDateFormat1;
      this.b = paramDateFormat2;
      this.c = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
      this.c.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    private JsonElement a(Date paramDate)
    {
      synchronized (this.b)
      {
        JsonPrimitive localJsonPrimitive = new JsonPrimitive(this.a.format(paramDate));
        return localJsonPrimitive;
      }
    }

    private Date a(JsonElement paramJsonElement)
    {
      synchronized (this.b)
      {
        try
        {
          Date localDate3 = this.b.parse(paramJsonElement.b());
          return localDate3;
        }
        catch (ParseException localParseException1)
        {
        }
      }
      try
      {
        Date localDate2 = this.a.parse(paramJsonElement.b());
        return localDate2;
        localObject = finally;
        throw localObject;
      }
      catch (ParseException localParseException2)
      {
        try
        {
          Date localDate1 = this.c.parse(paramJsonElement.b());
          return localDate1;
        }
        catch (ParseException localParseException3)
        {
          throw new JsonSyntaxException(paramJsonElement.b(), localParseException3);
        }
      }
    }

    public final String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append(DefaultDateTypeAdapter.class.getSimpleName());
      localStringBuilder.append('(').append(this.b.getClass().getSimpleName()).append(')');
      return localStringBuilder.toString();
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.DefaultTypeAdapters
 * JD-Core Version:    0.6.2
 */