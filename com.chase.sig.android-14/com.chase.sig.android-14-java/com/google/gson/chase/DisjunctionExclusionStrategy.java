package com.google.gson.chase;

import com.google.gson.chase.internal..Gson.Preconditions;
import java.util.Collection;
import java.util.Iterator;

final class DisjunctionExclusionStrategy
  implements ExclusionStrategy
{
  private final Collection<ExclusionStrategy> a;

  DisjunctionExclusionStrategy(Collection<ExclusionStrategy> paramCollection)
  {
    this.a = ((Collection).Gson.Preconditions.a(paramCollection));
  }

  public final boolean a(FieldAttributes paramFieldAttributes)
  {
    Iterator localIterator = this.a.iterator();
    while (localIterator.hasNext())
      if (((ExclusionStrategy)localIterator.next()).a(paramFieldAttributes))
        return true;
    return false;
  }

  public final boolean a(Class<?> paramClass)
  {
    Iterator localIterator = this.a.iterator();
    while (localIterator.hasNext())
      if (((ExclusionStrategy)localIterator.next()).a(paramClass))
        return true;
    return false;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.DisjunctionExclusionStrategy
 * JD-Core Version:    0.6.2
 */