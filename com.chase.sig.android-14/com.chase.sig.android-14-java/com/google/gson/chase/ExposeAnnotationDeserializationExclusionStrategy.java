package com.google.gson.chase;

import com.google.gson.chase.annotations.Expose;

final class ExposeAnnotationDeserializationExclusionStrategy
  implements ExclusionStrategy
{
  public final boolean a(FieldAttributes paramFieldAttributes)
  {
    Expose localExpose = (Expose)paramFieldAttributes.a(Expose.class);
    if (localExpose == null)
      return true;
    return !localExpose.b();
  }

  public final boolean a(Class<?> paramClass)
  {
    return false;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.ExposeAnnotationDeserializationExclusionStrategy
 * JD-Core Version:    0.6.2
 */