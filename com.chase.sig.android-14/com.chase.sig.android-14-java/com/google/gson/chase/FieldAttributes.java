package com.google.gson.chase;

import com.google.gson.chase.internal..Gson.Preconditions;
import com.google.gson.chase.internal.Pair;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;

public final class FieldAttributes
{
  private static final Cache<Pair<Class<?>, String>, Collection<Annotation>> a = new LruCache(g());
  private final Class<?> b;
  private final Field c;
  private final Class<?> d;
  private final boolean e;
  private final int f;
  private final String g;
  private Type h;
  private Collection<Annotation> i;

  FieldAttributes(Class<?> paramClass, Field paramField)
  {
    this.b = ((Class).Gson.Preconditions.a(paramClass));
    this.g = paramField.getName();
    this.d = paramField.getType();
    this.e = paramField.isSynthetic();
    this.f = paramField.getModifiers();
    this.c = paramField;
  }

  private static int g()
  {
    try
    {
      int j = Integer.parseInt(System.getProperty("com.google.gson.chase.annotation_cache_size_hint", String.valueOf(2000)));
      return j;
    }
    catch (NumberFormatException localNumberFormatException)
    {
    }
    return 2000;
  }

  public final String a()
  {
    return this.g;
  }

  public final <T extends Annotation> T a(Class<T> paramClass)
  {
    Iterator localIterator = d().iterator();
    while (localIterator.hasNext())
    {
      Annotation localAnnotation = (Annotation)localIterator.next();
      if (localAnnotation.annotationType() == paramClass)
        return localAnnotation;
    }
    return null;
  }

  public final boolean a(int paramInt)
  {
    return (paramInt & this.f) != 0;
  }

  public final Type b()
  {
    if (this.h == null)
      this.h = this.c.getGenericType();
    return this.h;
  }

  public final Class<?> c()
  {
    return this.d;
  }

  public final Collection<Annotation> d()
  {
    if (this.i == null)
    {
      Pair localPair = new Pair(this.b, this.g);
      Collection localCollection = (Collection)a.a(localPair);
      if (localCollection == null)
      {
        localCollection = Collections.unmodifiableCollection(Arrays.asList(this.c.getAnnotations()));
        a.a(localPair, localCollection);
      }
      this.i = localCollection;
    }
    return this.i;
  }

  final boolean e()
  {
    return this.e;
  }

  @Deprecated
  final Field f()
  {
    return this.c;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.FieldAttributes
 * JD-Core Version:    0.6.2
 */