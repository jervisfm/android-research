package com.google.gson.chase;

public enum FieldNamingPolicy
{
  private final FieldNamingStrategy2 e;

  static
  {
    FieldNamingPolicy[] arrayOfFieldNamingPolicy = new FieldNamingPolicy[4];
    arrayOfFieldNamingPolicy[0] = a;
    arrayOfFieldNamingPolicy[1] = b;
    arrayOfFieldNamingPolicy[2] = c;
    arrayOfFieldNamingPolicy[3] = d;
  }

  private FieldNamingPolicy(FieldNamingStrategy2 paramFieldNamingStrategy2)
  {
    this.e = paramFieldNamingStrategy2;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.FieldNamingPolicy
 * JD-Core Version:    0.6.2
 */