package com.google.gson.chase;

import com.google.gson.chase.internal.ConstructorConstructor;
import com.google.gson.chase.internal.bind.ReflectiveTypeAdapterFactory;
import java.lang.reflect.Field;

class Gson$1 extends ReflectiveTypeAdapterFactory
{
  Gson$1(Gson paramGson, ConstructorConstructor paramConstructorConstructor, FieldNamingStrategy2 paramFieldNamingStrategy2)
  {
    super(paramConstructorConstructor);
  }

  public final String a(Class<?> paramClass, Field paramField)
  {
    return this.a.a(new FieldAttributes(paramClass, paramField));
  }

  public final boolean b(Class<?> paramClass, Field paramField)
  {
    ExclusionStrategy localExclusionStrategy = Gson.a(this.b);
    return (!localExclusionStrategy.a(paramField.getType())) && (!localExclusionStrategy.a(new FieldAttributes(paramClass, paramField)));
  }

  public final boolean c(Class<?> paramClass, Field paramField)
  {
    ExclusionStrategy localExclusionStrategy = Gson.b(this.b);
    return (!localExclusionStrategy.a(paramField.getType())) && (!localExclusionStrategy.a(new FieldAttributes(paramClass, paramField)));
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.Gson.1
 * JD-Core Version:    0.6.2
 */