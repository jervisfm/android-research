package com.google.gson.chase;

import com.google.gson.chase.internal.ConstructorConstructor;
import com.google.gson.chase.internal.ParameterizedTypeHandlerMap;
import com.google.gson.chase.internal.Primitives;
import com.google.gson.chase.internal.Streams;
import com.google.gson.chase.internal.bind.ArrayTypeAdapter;
import com.google.gson.chase.internal.bind.BigDecimalTypeAdapter;
import com.google.gson.chase.internal.bind.BigIntegerTypeAdapter;
import com.google.gson.chase.internal.bind.CollectionTypeAdapterFactory;
import com.google.gson.chase.internal.bind.DateTypeAdapter;
import com.google.gson.chase.internal.bind.ExcludedTypeAdapterFactory;
import com.google.gson.chase.internal.bind.MapTypeAdapterFactory;
import com.google.gson.chase.internal.bind.MiniGson;
import com.google.gson.chase.internal.bind.MiniGson.Builder;
import com.google.gson.chase.internal.bind.ObjectTypeAdapter;
import com.google.gson.chase.internal.bind.SqlDateTypeAdapter;
import com.google.gson.chase.internal.bind.TimeTypeAdapter;
import com.google.gson.chase.internal.bind.TypeAdapter;
import com.google.gson.chase.internal.bind.TypeAdapter.Factory;
import com.google.gson.chase.internal.bind.TypeAdapters;
import com.google.gson.chase.reflect.TypeToken;
import com.google.gson.chase.stream.JsonReader;
import com.google.gson.chase.stream.JsonToken;
import com.google.gson.chase.stream.JsonWriter;
import com.google.gson.chase.stream.MalformedJsonException;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public final class Gson
{
  static final ParameterizedTypeHandlerMap a = new ParameterizedTypeHandlerMap().a();
  static final AnonymousAndLocalClassExclusionStrategy b = new AnonymousAndLocalClassExclusionStrategy();
  static final SyntheticFieldExclusionStrategy c = new SyntheticFieldExclusionStrategy();
  static final ModifierBasedExclusionStrategy d = new ModifierBasedExclusionStrategy(new int[] { 128, 8 });
  static final FieldNamingStrategy2 e = new SerializedNameAnnotationInterceptingNamingPolicy(new JavaFieldNamingPolicy());
  private static final ExclusionStrategy f = new DisjunctionExclusionStrategy(localLinkedList);
  private final ExclusionStrategy g;
  private final ExclusionStrategy h;
  private final ConstructorConstructor i;
  private final ParameterizedTypeHandlerMap<JsonSerializer<?>> j;
  private final ParameterizedTypeHandlerMap<JsonDeserializer<?>> k;
  private final boolean l;
  private final boolean m;
  private final boolean n;
  private final boolean o;
  private final MiniGson p;

  static
  {
    LinkedList localLinkedList = new LinkedList();
    localLinkedList.add(b);
    localLinkedList.add(c);
    localLinkedList.add(d);
  }

  public Gson()
  {
    this(f, f, e, a, false, a, a, false, false, true, false, false, LongSerializationPolicy.a, Collections.emptyList());
  }

  Gson(ExclusionStrategy paramExclusionStrategy1, ExclusionStrategy paramExclusionStrategy2, FieldNamingStrategy2 paramFieldNamingStrategy2, ParameterizedTypeHandlerMap<InstanceCreator<?>> paramParameterizedTypeHandlerMap, boolean paramBoolean1, ParameterizedTypeHandlerMap<JsonSerializer<?>> paramParameterizedTypeHandlerMap1, ParameterizedTypeHandlerMap<JsonDeserializer<?>> paramParameterizedTypeHandlerMap2, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, boolean paramBoolean5, boolean paramBoolean6, LongSerializationPolicy paramLongSerializationPolicy, List<TypeAdapter.Factory> paramList)
  {
    this.g = paramExclusionStrategy1;
    this.h = paramExclusionStrategy2;
    this.i = new ConstructorConstructor(paramParameterizedTypeHandlerMap);
    this.l = paramBoolean1;
    this.j = paramParameterizedTypeHandlerMap1;
    this.k = paramParameterizedTypeHandlerMap2;
    this.n = paramBoolean3;
    this.m = paramBoolean4;
    this.o = paramBoolean5;
    Gson.1 local1 = new Gson.1(this, this.i, paramFieldNamingStrategy2);
    MiniGson.Builder localBuilder1 = new MiniGson.Builder().a().a(TypeAdapters.w).a(TypeAdapters.k).a(TypeAdapters.e).a(TypeAdapters.g).a(TypeAdapters.i);
    Class localClass1 = Long.TYPE;
    Object localObject1;
    Object localObject2;
    label169: MiniGson.Builder localBuilder3;
    Class localClass3;
    if (paramLongSerializationPolicy == LongSerializationPolicy.a)
    {
      localObject1 = TypeAdapters.l;
      MiniGson.Builder localBuilder2 = localBuilder1.a(TypeAdapters.a(localClass1, Long.class, (TypeAdapter)localObject1));
      Class localClass2 = Double.TYPE;
      if (!paramBoolean6)
        break label339;
      localObject2 = TypeAdapters.p;
      localBuilder3 = localBuilder2.a(TypeAdapters.a(localClass2, Double.class, (TypeAdapter)localObject2));
      localClass3 = Float.TYPE;
      if (!paramBoolean6)
        break label352;
    }
    MiniGson.Builder localBuilder4;
    label339: label352: for (Object localObject3 = TypeAdapters.n; ; localObject3 = new Gson.3(this))
    {
      localBuilder4 = localBuilder3.a(TypeAdapters.a(localClass3, Float.class, (TypeAdapter)localObject3)).a(new ExcludedTypeAdapterFactory(paramExclusionStrategy2, paramExclusionStrategy1)).a(TypeAdapters.s).a(TypeAdapters.u).a(TypeAdapters.y).a(TypeAdapters.A).a(BigDecimal.class, new BigDecimalTypeAdapter()).a(BigInteger.class, new BigIntegerTypeAdapter()).a(TypeAdapters.P).a(ObjectTypeAdapter.a);
      Iterator localIterator = paramList.iterator();
      while (localIterator.hasNext())
        localBuilder4.a((TypeAdapter.Factory)localIterator.next());
      localObject1 = new Gson.4(this);
      break;
      localObject2 = new Gson.2(this);
      break label169;
    }
    localBuilder4.a(new GsonToMiniGsonTypeAdapterFactory(this, paramParameterizedTypeHandlerMap1, paramParameterizedTypeHandlerMap2)).a(new CollectionTypeAdapterFactory(this.i)).a(TypeAdapters.C).a(TypeAdapters.E).a(TypeAdapters.I).a(TypeAdapters.N).a(TypeAdapters.G).a(TypeAdapters.b).a(DateTypeAdapter.a).a(TypeAdapters.L).a(TimeTypeAdapter.a).a(SqlDateTypeAdapter.a).a(TypeAdapters.J).a(new MapTypeAdapterFactory(this.i, paramBoolean2)).a(ArrayTypeAdapter.a).a(TypeAdapters.Q).a(local1);
    this.p = localBuilder4.b();
  }

  private JsonWriter a(Writer paramWriter)
  {
    if (this.n)
      paramWriter.write(")]}'\n");
    JsonWriter localJsonWriter = new JsonWriter(paramWriter);
    if (this.o)
      localJsonWriter.c("  ");
    localJsonWriter.d(this.l);
    return localJsonWriter;
  }

  // ERROR //
  private <T> T a(JsonReader paramJsonReader, Type paramType)
  {
    // Byte code:
    //   0: iconst_1
    //   1: istore_3
    //   2: aload_1
    //   3: invokevirtual 329	com/google/gson/chase/stream/JsonReader:o	()Z
    //   6: istore 4
    //   8: aload_1
    //   9: iload_3
    //   10: invokevirtual 331	com/google/gson/chase/stream/JsonReader:a	(Z)V
    //   13: aload_1
    //   14: invokevirtual 334	com/google/gson/chase/stream/JsonReader:f	()Lcom/google/gson/chase/stream/JsonToken;
    //   17: pop
    //   18: iconst_0
    //   19: istore_3
    //   20: aload_0
    //   21: getfield 296	com/google/gson/chase/Gson:p	Lcom/google/gson/chase/internal/bind/MiniGson;
    //   24: aload_2
    //   25: invokestatic 339	com/google/gson/chase/reflect/TypeToken:a	(Ljava/lang/reflect/Type;)Lcom/google/gson/chase/reflect/TypeToken;
    //   28: invokevirtual 344	com/google/gson/chase/internal/bind/MiniGson:a	(Lcom/google/gson/chase/reflect/TypeToken;)Lcom/google/gson/chase/internal/bind/TypeAdapter;
    //   31: aload_1
    //   32: invokevirtual 349	com/google/gson/chase/internal/bind/TypeAdapter:a	(Lcom/google/gson/chase/stream/JsonReader;)Ljava/lang/Object;
    //   35: astore 10
    //   37: aload_1
    //   38: iload 4
    //   40: invokevirtual 331	com/google/gson/chase/stream/JsonReader:a	(Z)V
    //   43: aload 10
    //   45: areturn
    //   46: astore 8
    //   48: iload_3
    //   49: ifeq +11 -> 60
    //   52: aload_1
    //   53: iload 4
    //   55: invokevirtual 331	com/google/gson/chase/stream/JsonReader:a	(Z)V
    //   58: aconst_null
    //   59: areturn
    //   60: new 351	com/google/gson/chase/JsonSyntaxException
    //   63: dup
    //   64: aload 8
    //   66: invokespecial 354	com/google/gson/chase/JsonSyntaxException:<init>	(Ljava/lang/Throwable;)V
    //   69: athrow
    //   70: astore 6
    //   72: aload_1
    //   73: iload 4
    //   75: invokevirtual 331	com/google/gson/chase/stream/JsonReader:a	(Z)V
    //   78: aload 6
    //   80: athrow
    //   81: astore 7
    //   83: new 351	com/google/gson/chase/JsonSyntaxException
    //   86: dup
    //   87: aload 7
    //   89: invokespecial 354	com/google/gson/chase/JsonSyntaxException:<init>	(Ljava/lang/Throwable;)V
    //   92: athrow
    //   93: astore 5
    //   95: new 351	com/google/gson/chase/JsonSyntaxException
    //   98: dup
    //   99: aload 5
    //   101: invokespecial 354	com/google/gson/chase/JsonSyntaxException:<init>	(Ljava/lang/Throwable;)V
    //   104: athrow
    //
    // Exception table:
    //   from	to	target	type
    //   13	18	46	java/io/EOFException
    //   20	37	46	java/io/EOFException
    //   13	18	70	finally
    //   20	37	70	finally
    //   60	70	70	finally
    //   83	93	70	finally
    //   95	105	70	finally
    //   13	18	81	java/lang/IllegalStateException
    //   20	37	81	java/lang/IllegalStateException
    //   13	18	93	java/io/IOException
    //   20	37	93	java/io/IOException
  }

  public final <T> T a(String paramString, Class<T> paramClass)
  {
    Object localObject = a(paramString, paramClass);
    return Primitives.a(paramClass).cast(localObject);
  }

  public final <T> T a(String paramString, Type paramType)
  {
    Object localObject;
    if (paramString == null)
      localObject = null;
    while (true)
    {
      return localObject;
      JsonReader localJsonReader = new JsonReader(new StringReader(paramString));
      localObject = a(localJsonReader, paramType);
      if (localObject == null)
        continue;
      try
      {
        if (localJsonReader.f() == JsonToken.j)
          continue;
        throw new JsonIOException("JSON document was not fully consumed.");
      }
      catch (MalformedJsonException localMalformedJsonException)
      {
        throw new JsonSyntaxException(localMalformedJsonException);
      }
      catch (IOException localIOException)
      {
        throw new JsonIOException(localIOException);
      }
    }
  }

  public final String a(Object paramObject)
  {
    if (paramObject == null)
    {
      JsonNull localJsonNull = JsonNull.a;
      StringWriter localStringWriter2 = new StringWriter();
      try
      {
        JsonWriter localJsonWriter2 = a(Streams.a(localStringWriter2));
        boolean bool4 = localJsonWriter2.g();
        localJsonWriter2.b(true);
        boolean bool5 = localJsonWriter2.h();
        localJsonWriter2.c(this.m);
        boolean bool6 = localJsonWriter2.i();
        localJsonWriter2.d(this.l);
        try
        {
          Streams.a(localJsonNull, localJsonWriter2);
          return localStringWriter2.toString();
        }
        catch (IOException localIOException4)
        {
          throw new JsonIOException(localIOException4);
        }
        finally
        {
          localJsonWriter2.b(bool4);
          localJsonWriter2.c(bool5);
          localJsonWriter2.d(bool6);
        }
      }
      catch (IOException localIOException3)
      {
        throw new RuntimeException(localIOException3);
      }
    }
    Class localClass = paramObject.getClass();
    StringWriter localStringWriter1 = new StringWriter();
    try
    {
      JsonWriter localJsonWriter1 = a(Streams.a(localStringWriter1));
      TypeAdapter localTypeAdapter = this.p.a(TypeToken.a(localClass));
      boolean bool1 = localJsonWriter1.g();
      localJsonWriter1.b(true);
      boolean bool2 = localJsonWriter1.h();
      localJsonWriter1.c(this.m);
      boolean bool3 = localJsonWriter1.i();
      localJsonWriter1.d(this.l);
      try
      {
        localTypeAdapter.a(localJsonWriter1, paramObject);
        return localStringWriter1.toString();
      }
      catch (IOException localIOException2)
      {
        throw new JsonIOException(localIOException2);
      }
      finally
      {
        localJsonWriter1.b(bool1);
        localJsonWriter1.c(bool2);
        localJsonWriter1.d(bool3);
      }
    }
    catch (IOException localIOException1)
    {
      throw new JsonIOException(localIOException1);
    }
  }

  public final String toString()
  {
    return "{serializeNulls:" + this.l + ",serializers:" + this.j + ",deserializers:" + this.k + ",instanceCreators:" + this.i + "}";
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.Gson
 * JD-Core Version:    0.6.2
 */