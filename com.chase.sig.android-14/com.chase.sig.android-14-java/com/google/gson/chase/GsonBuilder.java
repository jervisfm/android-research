package com.google.gson.chase;

import com.google.gson.chase.internal..Gson.Preconditions;
import com.google.gson.chase.internal.ParameterizedTypeHandlerMap;
import com.google.gson.chase.internal.Primitives;
import com.google.gson.chase.internal.bind.TypeAdapter.Factory;
import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public final class GsonBuilder
{
  private static final InnerClassExclusionStrategy a = new InnerClassExclusionStrategy();
  private static final ExposeAnnotationDeserializationExclusionStrategy b = new ExposeAnnotationDeserializationExclusionStrategy();
  private static final ExposeAnnotationSerializationExclusionStrategy c = new ExposeAnnotationSerializationExclusionStrategy();
  private final Set<ExclusionStrategy> d = new HashSet();
  private final Set<ExclusionStrategy> e = new HashSet();
  private double f;
  private ModifierBasedExclusionStrategy g;
  private boolean h;
  private boolean i;
  private LongSerializationPolicy j;
  private FieldNamingStrategy2 k;
  private final ParameterizedTypeHandlerMap<InstanceCreator<?>> l;
  private final ParameterizedTypeHandlerMap<JsonSerializer<?>> m;
  private final ParameterizedTypeHandlerMap<JsonDeserializer<?>> n;
  private final List<TypeAdapter.Factory> o = new ArrayList();
  private boolean p;
  private String q;
  private int r;
  private int s;
  private boolean t = false;
  private boolean u;
  private boolean v;
  private boolean w;
  private boolean x;

  public GsonBuilder()
  {
    this.e.add(Gson.b);
    this.e.add(Gson.c);
    this.d.add(Gson.b);
    this.d.add(Gson.c);
    this.f = -1.0D;
    this.h = true;
    this.w = false;
    this.v = true;
    this.g = Gson.d;
    this.i = false;
    this.j = LongSerializationPolicy.a;
    this.k = Gson.e;
    this.l = new ParameterizedTypeHandlerMap();
    this.m = new ParameterizedTypeHandlerMap();
    this.n = new ParameterizedTypeHandlerMap();
    this.p = false;
    this.r = 2;
    this.s = 2;
    this.u = false;
    this.x = false;
  }

  private static <T> void a(Class<?> paramClass, ParameterizedTypeHandlerMap<T> paramParameterizedTypeHandlerMap, T paramT)
  {
    if (!paramParameterizedTypeHandlerMap.a(paramClass))
      paramParameterizedTypeHandlerMap.a(paramClass, paramT);
  }

  public final GsonBuilder a()
  {
    this.t = true;
    return this;
  }

  public final GsonBuilder a(Type paramType, Object paramObject)
  {
    if (((paramObject instanceof JsonSerializer)) || ((paramObject instanceof JsonDeserializer)) || ((paramObject instanceof InstanceCreator)) || ((paramObject instanceof TypeAdapter.Factory)));
    for (boolean bool = true; ; bool = false)
    {
      .Gson.Preconditions.a(bool);
      if ((!Primitives.a(paramType)) && (!Primitives.b(paramType)))
        break;
      throw new IllegalArgumentException("Cannot register type adapters for " + paramType);
    }
    if ((paramObject instanceof InstanceCreator))
    {
      InstanceCreator localInstanceCreator = (InstanceCreator)paramObject;
      this.l.a(paramType, localInstanceCreator);
    }
    if ((paramObject instanceof JsonSerializer))
    {
      JsonSerializer localJsonSerializer = (JsonSerializer)paramObject;
      this.m.a(paramType, localJsonSerializer);
    }
    if ((paramObject instanceof JsonDeserializer))
    {
      JsonDeserializer localJsonDeserializer = (JsonDeserializer)paramObject;
      this.n.a(paramType, new JsonDeserializerExceptionWrapper(localJsonDeserializer));
    }
    if ((paramObject instanceof TypeAdapter.Factory))
      this.o.add((TypeAdapter.Factory)paramObject);
    return this;
  }

  public final Gson b()
  {
    LinkedList localLinkedList1 = new LinkedList(this.e);
    LinkedList localLinkedList2 = new LinkedList(this.d);
    localLinkedList1.add(this.g);
    localLinkedList2.add(this.g);
    if (!this.h)
    {
      localLinkedList1.add(a);
      localLinkedList2.add(a);
    }
    if (this.f != -1.0D)
    {
      VersionExclusionStrategy localVersionExclusionStrategy = new VersionExclusionStrategy(this.f);
      localLinkedList1.add(localVersionExclusionStrategy);
      localLinkedList2.add(localVersionExclusionStrategy);
    }
    if (this.i)
    {
      localLinkedList1.add(b);
      localLinkedList2.add(c);
    }
    String str = this.q;
    int i1 = this.r;
    int i2 = this.s;
    ParameterizedTypeHandlerMap localParameterizedTypeHandlerMap1 = this.m;
    ParameterizedTypeHandlerMap localParameterizedTypeHandlerMap2 = this.n;
    DefaultTypeAdapters.DefaultDateTypeAdapter localDefaultDateTypeAdapter;
    if ((str != null) && (!"".equals(str.trim())))
      localDefaultDateTypeAdapter = new DefaultTypeAdapters.DefaultDateTypeAdapter(str);
    while (true)
    {
      if (localDefaultDateTypeAdapter != null)
      {
        a(java.util.Date.class, localParameterizedTypeHandlerMap1, localDefaultDateTypeAdapter);
        a(java.util.Date.class, localParameterizedTypeHandlerMap2, localDefaultDateTypeAdapter);
        a(Timestamp.class, localParameterizedTypeHandlerMap1, localDefaultDateTypeAdapter);
        a(Timestamp.class, localParameterizedTypeHandlerMap2, localDefaultDateTypeAdapter);
        a(java.sql.Date.class, localParameterizedTypeHandlerMap1, localDefaultDateTypeAdapter);
        a(java.sql.Date.class, localParameterizedTypeHandlerMap2, localDefaultDateTypeAdapter);
      }
      return new Gson(new DisjunctionExclusionStrategy(localLinkedList1), new DisjunctionExclusionStrategy(localLinkedList2), this.k, this.l.b().a(), this.p, this.m.b().a(), this.n.b().a(), this.t, this.x, this.v, this.w, this.u, this.j, this.o);
      localDefaultDateTypeAdapter = null;
      if (i1 != 2)
      {
        localDefaultDateTypeAdapter = null;
        if (i2 != 2)
          localDefaultDateTypeAdapter = new DefaultTypeAdapters.DefaultDateTypeAdapter(i1, i2);
      }
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.GsonBuilder
 * JD-Core Version:    0.6.2
 */