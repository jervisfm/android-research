package com.google.gson.chase;

import com.google.gson.chase.internal.Streams;
import com.google.gson.chase.internal.bind.MiniGson;
import com.google.gson.chase.internal.bind.TypeAdapter;
import com.google.gson.chase.reflect.TypeToken;
import com.google.gson.chase.stream.JsonReader;
import com.google.gson.chase.stream.JsonWriter;
import java.lang.reflect.Type;

class GsonToMiniGsonTypeAdapterFactory$3 extends TypeAdapter<T>
{
  private TypeAdapter<T> g;

  GsonToMiniGsonTypeAdapterFactory$3(GsonToMiniGsonTypeAdapterFactory paramGsonToMiniGsonTypeAdapterFactory, JsonDeserializer paramJsonDeserializer, Type paramType, JsonSerializer paramJsonSerializer, MiniGson paramMiniGson, TypeToken paramTypeToken)
  {
  }

  private TypeAdapter<T> a()
  {
    TypeAdapter localTypeAdapter1 = this.g;
    if (localTypeAdapter1 != null)
      return localTypeAdapter1;
    TypeAdapter localTypeAdapter2 = this.d.a(this.f, this.e);
    this.g = localTypeAdapter2;
    return localTypeAdapter2;
  }

  public final T a(JsonReader paramJsonReader)
  {
    if (this.a == null)
      return a().a(paramJsonReader);
    JsonElement localJsonElement = Streams.a(paramJsonReader);
    if (localJsonElement.j())
      return null;
    return this.a.a(localJsonElement, this.b, GsonToMiniGsonTypeAdapterFactory.a(this.f));
  }

  public final void a(JsonWriter paramJsonWriter, T paramT)
  {
    if (this.c == null)
    {
      a().a(paramJsonWriter, paramT);
      return;
    }
    if (paramT == null)
    {
      paramJsonWriter.f();
      return;
    }
    JsonSerializer localJsonSerializer = this.c;
    GsonToMiniGsonTypeAdapterFactory.b(this.f);
    Streams.a(localJsonSerializer.a(paramT), paramJsonWriter);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.GsonToMiniGsonTypeAdapterFactory.3
 * JD-Core Version:    0.6.2
 */