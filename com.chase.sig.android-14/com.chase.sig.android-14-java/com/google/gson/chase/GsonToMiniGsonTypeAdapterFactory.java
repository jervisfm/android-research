package com.google.gson.chase;

import com.google.gson.chase.internal.ParameterizedTypeHandlerMap;
import com.google.gson.chase.internal.bind.MiniGson;
import com.google.gson.chase.internal.bind.TypeAdapter;
import com.google.gson.chase.internal.bind.TypeAdapter.Factory;
import com.google.gson.chase.reflect.TypeToken;
import java.lang.reflect.Type;

final class GsonToMiniGsonTypeAdapterFactory
  implements TypeAdapter.Factory
{
  private final ParameterizedTypeHandlerMap<JsonSerializer<?>> a;
  private final ParameterizedTypeHandlerMap<JsonDeserializer<?>> b;
  private final JsonDeserializationContext c;
  private final JsonSerializationContext d;

  public GsonToMiniGsonTypeAdapterFactory(Gson paramGson, ParameterizedTypeHandlerMap<JsonSerializer<?>> paramParameterizedTypeHandlerMap, ParameterizedTypeHandlerMap<JsonDeserializer<?>> paramParameterizedTypeHandlerMap1)
  {
    this.a = paramParameterizedTypeHandlerMap;
    this.b = paramParameterizedTypeHandlerMap1;
    this.c = new GsonToMiniGsonTypeAdapterFactory.1(this, paramGson);
    this.d = new GsonToMiniGsonTypeAdapterFactory.2(this, paramGson);
  }

  public final <T> TypeAdapter<T> a(MiniGson paramMiniGson, TypeToken<T> paramTypeToken)
  {
    Type localType = paramTypeToken.b();
    JsonSerializer localJsonSerializer = (JsonSerializer)this.a.a(localType, false);
    JsonDeserializer localJsonDeserializer = (JsonDeserializer)this.b.a(localType, false);
    if ((localJsonSerializer == null) && (localJsonDeserializer == null))
      return null;
    return new GsonToMiniGsonTypeAdapterFactory.3(this, localJsonDeserializer, localType, localJsonSerializer, paramMiniGson, paramTypeToken);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.GsonToMiniGsonTypeAdapterFactory
 * JD-Core Version:    0.6.2
 */