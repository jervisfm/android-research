package com.google.gson.chase;

final class InnerClassExclusionStrategy
  implements ExclusionStrategy
{
  private static boolean b(Class<?> paramClass)
  {
    if (paramClass.isMemberClass())
    {
      if ((0x8 & paramClass.getModifiers()) != 0);
      for (int i = 1; i == 0; i = 0)
        return true;
    }
    return false;
  }

  public final boolean a(FieldAttributes paramFieldAttributes)
  {
    return b(paramFieldAttributes.c());
  }

  public final boolean a(Class<?> paramClass)
  {
    return b(paramClass);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.InnerClassExclusionStrategy
 * JD-Core Version:    0.6.2
 */