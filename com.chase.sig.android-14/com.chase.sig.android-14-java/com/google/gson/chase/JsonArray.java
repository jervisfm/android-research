package com.google.gson.chase;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class JsonArray extends JsonElement
  implements Iterable<JsonElement>
{
  private final List<JsonElement> a = new ArrayList();

  public final Number a()
  {
    if (this.a.size() == 1)
      return ((JsonElement)this.a.get(0)).a();
    throw new IllegalStateException();
  }

  public final void a(JsonElement paramJsonElement)
  {
    if (paramJsonElement == null)
      paramJsonElement = JsonNull.a;
    this.a.add(paramJsonElement);
  }

  public final String b()
  {
    if (this.a.size() == 1)
      return ((JsonElement)this.a.get(0)).b();
    throw new IllegalStateException();
  }

  public final double c()
  {
    if (this.a.size() == 1)
      return ((JsonElement)this.a.get(0)).c();
    throw new IllegalStateException();
  }

  public final long d()
  {
    if (this.a.size() == 1)
      return ((JsonElement)this.a.get(0)).d();
    throw new IllegalStateException();
  }

  public final int e()
  {
    if (this.a.size() == 1)
      return ((JsonElement)this.a.get(0)).e();
    throw new IllegalStateException();
  }

  public final boolean equals(Object paramObject)
  {
    return (paramObject == this) || (((paramObject instanceof JsonArray)) && (((JsonArray)paramObject).a.equals(this.a)));
  }

  public final boolean f()
  {
    if (this.a.size() == 1)
      return ((JsonElement)this.a.get(0)).f();
    throw new IllegalStateException();
  }

  public final int hashCode()
  {
    return this.a.hashCode();
  }

  public final Iterator<JsonElement> iterator()
  {
    return this.a.iterator();
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.JsonArray
 * JD-Core Version:    0.6.2
 */