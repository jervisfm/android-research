package com.google.gson.chase;

import java.lang.reflect.Type;

public abstract interface JsonDeserializer<T>
{
  public abstract T a(JsonElement paramJsonElement, Type paramType, JsonDeserializationContext paramJsonDeserializationContext);
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.JsonDeserializer
 * JD-Core Version:    0.6.2
 */