package com.google.gson.chase;

import com.google.gson.chase.internal..Gson.Preconditions;
import java.lang.reflect.Type;

final class JsonDeserializerExceptionWrapper<T>
  implements JsonDeserializer<T>
{
  private final JsonDeserializer<T> a;

  JsonDeserializerExceptionWrapper(JsonDeserializer<T> paramJsonDeserializer)
  {
    this.a = ((JsonDeserializer).Gson.Preconditions.a(paramJsonDeserializer));
  }

  public final T a(JsonElement paramJsonElement, Type paramType, JsonDeserializationContext paramJsonDeserializationContext)
  {
    try
    {
      Object localObject = this.a.a(paramJsonElement, paramType, paramJsonDeserializationContext);
      return localObject;
    }
    catch (JsonParseException localJsonParseException)
    {
      throw localJsonParseException;
    }
    catch (Exception localException)
    {
      throw new JsonParseException("The JsonDeserializer " + this.a + " failed to deserialize json object " + paramJsonElement + " given the type " + paramType, localException);
    }
  }

  public final String toString()
  {
    return this.a.toString();
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.JsonDeserializerExceptionWrapper
 * JD-Core Version:    0.6.2
 */