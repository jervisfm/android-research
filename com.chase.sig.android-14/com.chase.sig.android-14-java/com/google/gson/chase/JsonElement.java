package com.google.gson.chase;

import com.google.gson.chase.internal.Streams;
import com.google.gson.chase.stream.JsonWriter;
import java.io.IOException;
import java.io.StringWriter;

public abstract class JsonElement
{
  public Number a()
  {
    throw new UnsupportedOperationException(getClass().getSimpleName());
  }

  public String b()
  {
    throw new UnsupportedOperationException(getClass().getSimpleName());
  }

  public double c()
  {
    throw new UnsupportedOperationException(getClass().getSimpleName());
  }

  public long d()
  {
    throw new UnsupportedOperationException(getClass().getSimpleName());
  }

  public int e()
  {
    throw new UnsupportedOperationException(getClass().getSimpleName());
  }

  public boolean f()
  {
    throw new UnsupportedOperationException(getClass().getSimpleName());
  }

  public final boolean g()
  {
    return this instanceof JsonArray;
  }

  public final boolean h()
  {
    return this instanceof JsonObject;
  }

  public final boolean i()
  {
    return this instanceof JsonPrimitive;
  }

  public final boolean j()
  {
    return this instanceof JsonNull;
  }

  public final JsonObject k()
  {
    if ((this instanceof JsonObject))
      return (JsonObject)this;
    throw new IllegalStateException("Not a JSON Object: " + this);
  }

  public final JsonArray l()
  {
    if ((this instanceof JsonArray))
      return (JsonArray)this;
    throw new IllegalStateException("This is not a JSON Array.");
  }

  public final JsonPrimitive m()
  {
    if ((this instanceof JsonPrimitive))
      return (JsonPrimitive)this;
    throw new IllegalStateException("This is not a JSON Primitive.");
  }

  public String toString()
  {
    try
    {
      StringWriter localStringWriter = new StringWriter();
      JsonWriter localJsonWriter = new JsonWriter(localStringWriter);
      localJsonWriter.b(true);
      Streams.a(this, localJsonWriter);
      String str = localStringWriter.toString();
      return str;
    }
    catch (IOException localIOException)
    {
      throw new AssertionError(localIOException);
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.JsonElement
 * JD-Core Version:    0.6.2
 */