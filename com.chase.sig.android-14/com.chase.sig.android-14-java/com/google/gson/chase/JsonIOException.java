package com.google.gson.chase;

public final class JsonIOException extends JsonParseException
{
  public JsonIOException(String paramString)
  {
    super(paramString);
  }

  public JsonIOException(Throwable paramThrowable)
  {
    super(paramThrowable);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.JsonIOException
 * JD-Core Version:    0.6.2
 */