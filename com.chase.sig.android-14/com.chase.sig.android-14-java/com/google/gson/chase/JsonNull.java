package com.google.gson.chase;

public final class JsonNull extends JsonElement
{
  public static final JsonNull a = new JsonNull();

  public final boolean equals(Object paramObject)
  {
    return (this == paramObject) || ((paramObject instanceof JsonNull));
  }

  public final int hashCode()
  {
    return JsonNull.class.hashCode();
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.JsonNull
 * JD-Core Version:    0.6.2
 */