package com.google.gson.chase;

import com.google.gson.chase.internal..Gson.Preconditions;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public final class JsonObject extends JsonElement
{
  private final Map<String, JsonElement> a = new LinkedHashMap();

  public final JsonElement a(String paramString)
  {
    if (this.a.containsKey(paramString))
    {
      Object localObject = (JsonElement)this.a.get(paramString);
      if (localObject == null)
        localObject = JsonNull.a;
      return localObject;
    }
    return null;
  }

  public final void a(String paramString, JsonElement paramJsonElement)
  {
    if (paramJsonElement == null)
      paramJsonElement = JsonNull.a;
    this.a.put(.Gson.Preconditions.a(paramString), paramJsonElement);
  }

  public final boolean equals(Object paramObject)
  {
    return (paramObject == this) || (((paramObject instanceof JsonObject)) && (((JsonObject)paramObject).a.equals(this.a)));
  }

  public final int hashCode()
  {
    return this.a.hashCode();
  }

  public final Set<Map.Entry<String, JsonElement>> n()
  {
    return this.a.entrySet();
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.JsonObject
 * JD-Core Version:    0.6.2
 */