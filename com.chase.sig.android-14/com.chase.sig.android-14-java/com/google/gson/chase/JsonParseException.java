package com.google.gson.chase;

public class JsonParseException extends RuntimeException
{
  public JsonParseException(String paramString)
  {
    super(paramString);
  }

  public JsonParseException(String paramString, Throwable paramThrowable)
  {
    super(paramString, paramThrowable);
  }

  public JsonParseException(Throwable paramThrowable)
  {
    super(paramThrowable);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.JsonParseException
 * JD-Core Version:    0.6.2
 */