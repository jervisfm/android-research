package com.google.gson.chase;

import com.google.gson.chase.internal.Streams;
import com.google.gson.chase.stream.JsonReader;
import com.google.gson.chase.stream.JsonToken;
import com.google.gson.chase.stream.MalformedJsonException;
import java.io.EOFException;
import java.io.IOException;
import java.util.Iterator;
import java.util.NoSuchElementException;

public final class JsonStreamParser
  implements Iterator<JsonElement>
{
  private final JsonReader a;
  private final Object b;

  private JsonElement a()
  {
    if (!hasNext())
      throw new NoSuchElementException();
    try
    {
      JsonElement localJsonElement = Streams.a(this.a);
      return localJsonElement;
    }
    catch (StackOverflowError localStackOverflowError)
    {
      throw new JsonParseException("Failed parsing JSON source to Json", localStackOverflowError);
    }
    catch (OutOfMemoryError localOutOfMemoryError)
    {
      throw new JsonParseException("Failed parsing JSON source to Json", localOutOfMemoryError);
    }
    catch (JsonParseException localJsonParseException)
    {
      NoSuchElementException localNoSuchElementException;
      if ((localJsonParseException.getCause() instanceof EOFException))
        localNoSuchElementException = new NoSuchElementException();
      throw localNoSuchElementException;
    }
  }

  public final boolean hasNext()
  {
    while (true)
    {
      try
      {
        synchronized (this.b)
        {
          try
          {
            JsonToken localJsonToken1 = this.a.f();
            JsonToken localJsonToken2 = JsonToken.j;
            if (localJsonToken1 != localJsonToken2)
            {
              bool = true;
              return bool;
            }
          }
          catch (MalformedJsonException localMalformedJsonException)
          {
            throw new JsonSyntaxException(localMalformedJsonException);
          }
        }
      }
      catch (IOException localIOException)
      {
        throw new JsonIOException(localIOException);
      }
      boolean bool = false;
    }
  }

  public final void remove()
  {
    throw new UnsupportedOperationException();
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.JsonStreamParser
 * JD-Core Version:    0.6.2
 */