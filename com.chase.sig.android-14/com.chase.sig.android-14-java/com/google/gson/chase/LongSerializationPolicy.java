package com.google.gson.chase;

public enum LongSerializationPolicy
{
  private final Strategy c;

  static
  {
    LongSerializationPolicy[] arrayOfLongSerializationPolicy = new LongSerializationPolicy[2];
    arrayOfLongSerializationPolicy[0] = a;
    arrayOfLongSerializationPolicy[1] = b;
  }

  private LongSerializationPolicy(Strategy paramStrategy)
  {
    this.c = paramStrategy;
  }

  private static class DefaultStrategy
    implements LongSerializationPolicy.Strategy
  {
  }

  private static abstract interface Strategy
  {
  }

  private static class StringStrategy
    implements LongSerializationPolicy.Strategy
  {
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.LongSerializationPolicy
 * JD-Core Version:    0.6.2
 */