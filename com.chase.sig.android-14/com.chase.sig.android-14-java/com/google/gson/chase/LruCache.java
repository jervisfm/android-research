package com.google.gson.chase;

import java.util.LinkedHashMap;
import java.util.Map.Entry;

final class LruCache<K, V> extends LinkedHashMap<K, V>
  implements Cache<K, V>
{
  private final int a;

  public LruCache(int paramInt)
  {
    super(paramInt, 0.7F, true);
    this.a = paramInt;
  }

  public final V a(K paramK)
  {
    try
    {
      Object localObject2 = get(paramK);
      return localObject2;
    }
    finally
    {
      localObject1 = finally;
      throw localObject1;
    }
  }

  public final void a(K paramK, V paramV)
  {
    try
    {
      put(paramK, paramV);
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  protected final boolean removeEldestEntry(Map.Entry<K, V> paramEntry)
  {
    return size() > this.a;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.LruCache
 * JD-Core Version:    0.6.2
 */