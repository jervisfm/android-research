package com.google.gson.chase;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

final class ModifierBasedExclusionStrategy
  implements ExclusionStrategy
{
  private final Collection<Integer> a = new HashSet();

  public ModifierBasedExclusionStrategy(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt != null)
    {
      int i = paramArrayOfInt.length;
      for (int j = 0; j < i; j++)
      {
        int k = paramArrayOfInt[j];
        this.a.add(Integer.valueOf(k));
      }
    }
  }

  public final boolean a(FieldAttributes paramFieldAttributes)
  {
    Iterator localIterator = this.a.iterator();
    while (localIterator.hasNext())
      if (paramFieldAttributes.a(((Integer)localIterator.next()).intValue()))
        return true;
    return false;
  }

  public final boolean a(Class<?> paramClass)
  {
    return false;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.ModifierBasedExclusionStrategy
 * JD-Core Version:    0.6.2
 */