package com.google.gson.chase;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.Collection;

abstract class RecursiveFieldNamingPolicy
  implements FieldNamingStrategy2
{
  public final String a(FieldAttributes paramFieldAttributes)
  {
    return a(paramFieldAttributes.a(), paramFieldAttributes.b(), paramFieldAttributes.d());
  }

  protected abstract String a(String paramString, Type paramType, Collection<Annotation> paramCollection);
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.RecursiveFieldNamingPolicy
 * JD-Core Version:    0.6.2
 */