package com.google.gson.chase;

import com.google.gson.chase.annotations.SerializedName;

final class SerializedNameAnnotationInterceptingNamingPolicy
  implements FieldNamingStrategy2
{
  private final FieldNamingStrategy2 a;

  SerializedNameAnnotationInterceptingNamingPolicy(FieldNamingStrategy2 paramFieldNamingStrategy2)
  {
    this.a = paramFieldNamingStrategy2;
  }

  public final String a(FieldAttributes paramFieldAttributes)
  {
    SerializedName localSerializedName = (SerializedName)paramFieldAttributes.a(SerializedName.class);
    if (localSerializedName == null)
      return this.a.a(paramFieldAttributes);
    return localSerializedName.a();
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.SerializedNameAnnotationInterceptingNamingPolicy
 * JD-Core Version:    0.6.2
 */