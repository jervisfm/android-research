package com.google.gson.chase;

final class SyntheticFieldExclusionStrategy
  implements ExclusionStrategy
{
  private final boolean a = true;

  public final boolean a(FieldAttributes paramFieldAttributes)
  {
    return (this.a) && (paramFieldAttributes.e());
  }

  public final boolean a(Class<?> paramClass)
  {
    return false;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.SyntheticFieldExclusionStrategy
 * JD-Core Version:    0.6.2
 */