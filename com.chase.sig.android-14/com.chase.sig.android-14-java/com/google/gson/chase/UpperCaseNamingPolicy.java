package com.google.gson.chase;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.Collection;

final class UpperCaseNamingPolicy extends RecursiveFieldNamingPolicy
{
  protected final String a(String paramString, Type paramType, Collection<Annotation> paramCollection)
  {
    return paramString.toUpperCase();
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.UpperCaseNamingPolicy
 * JD-Core Version:    0.6.2
 */