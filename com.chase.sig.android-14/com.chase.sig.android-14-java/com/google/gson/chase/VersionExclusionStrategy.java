package com.google.gson.chase;

import com.google.gson.chase.annotations.Since;
import com.google.gson.chase.annotations.Until;
import com.google.gson.chase.internal..Gson.Preconditions;

final class VersionExclusionStrategy
  implements ExclusionStrategy
{
  private final double a;

  VersionExclusionStrategy(double paramDouble)
  {
    if (paramDouble >= 0.0D);
    for (boolean bool = true; ; bool = false)
    {
      .Gson.Preconditions.a(bool);
      this.a = paramDouble;
      return;
    }
  }

  private boolean a(Since paramSince, Until paramUntil)
  {
    int i;
    if ((paramSince != null) && (paramSince.a() > this.a))
    {
      i = 0;
      if (i == 0)
        break label63;
      if ((paramUntil == null) || (paramUntil.a() > this.a))
        break label57;
    }
    label57: for (int j = 0; ; j = 1)
    {
      if (j == 0)
        break label63;
      return true;
      i = 1;
      break;
    }
    label63: return false;
  }

  public final boolean a(FieldAttributes paramFieldAttributes)
  {
    return !a((Since)paramFieldAttributes.a(Since.class), (Until)paramFieldAttributes.a(Until.class));
  }

  public final boolean a(Class<?> paramClass)
  {
    return !a((Since)paramClass.getAnnotation(Since.class), (Until)paramClass.getAnnotation(Until.class));
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.VersionExclusionStrategy
 * JD-Core Version:    0.6.2
 */