package com.google.gson.chase.internal;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.GenericDeclaration;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Properties;

public final class $Gson$Types
{
  static final Type[] a = new Type[0];

  public static Type a(Type paramType)
  {
    if ((paramType instanceof Class))
    {
      paramType = (Class)paramType;
      if (paramType.isArray())
        paramType = new GenericArrayTypeImpl(a(paramType.getComponentType()));
    }
    do
    {
      return paramType;
      if ((paramType instanceof ParameterizedType))
      {
        ParameterizedType localParameterizedType = (ParameterizedType)paramType;
        return new ParameterizedTypeImpl(localParameterizedType.getOwnerType(), localParameterizedType.getRawType(), localParameterizedType.getActualTypeArguments());
      }
      if ((paramType instanceof GenericArrayType))
        return new GenericArrayTypeImpl(((GenericArrayType)paramType).getGenericComponentType());
    }
    while (!(paramType instanceof WildcardType));
    WildcardType localWildcardType = (WildcardType)paramType;
    return new WildcardTypeImpl(localWildcardType.getUpperBounds(), localWildcardType.getLowerBounds());
  }

  public static Type a(Type paramType, Class<?> paramClass)
  {
    Type localType = b(paramType, paramClass, Collection.class);
    if ((localType instanceof WildcardType))
      localType = ((WildcardType)localType).getUpperBounds()[0];
    if ((localType instanceof ParameterizedType))
      return ((ParameterizedType)localType).getActualTypeArguments()[0];
    return Object.class;
  }

  private static Type a(Type paramType, Class<?> paramClass1, Class<?> paramClass2)
  {
    Object localObject1 = paramClass1;
    Object localObject2 = paramType;
    if (paramClass2 == localObject1)
      paramClass2 = (Class<?>)localObject2;
    label159: 
    while (true)
    {
      return paramClass2;
      if (paramClass2.isInterface())
      {
        Class[] arrayOfClass = ((Class)localObject1).getInterfaces();
        int i = 0;
        int j = arrayOfClass.length;
        while (true)
        {
          if (i >= j)
            break label100;
          if (arrayOfClass[i] == paramClass2)
            return localObject1.getGenericInterfaces()[i];
          if (paramClass2.isAssignableFrom(arrayOfClass[i]))
          {
            Type localType2 = localObject1.getGenericInterfaces()[i];
            localObject1 = arrayOfClass[i];
            localObject2 = localType2;
            break;
          }
          i++;
        }
      }
      label100: if (!((Class)localObject1).isInterface())
        while (true)
        {
          if (localObject1 == Object.class)
            break label159;
          Class localClass = ((Class)localObject1).getSuperclass();
          if (localClass == paramClass2)
            return ((Class)localObject1).getGenericSuperclass();
          if (paramClass2.isAssignableFrom(localClass))
          {
            Type localType1 = ((Class)localObject1).getGenericSuperclass();
            localObject1 = localClass;
            localObject2 = localType1;
            break;
          }
          localObject1 = localClass;
        }
    }
  }

  public static Type a(Type paramType1, Class<?> paramClass, Type paramType2)
  {
    int i = 0;
    Object localObject = paramType2;
    TypeVariable localTypeVariable;
    Class localClass2;
    label44: int m;
    if ((localObject instanceof TypeVariable))
    {
      localTypeVariable = (TypeVariable)localObject;
      GenericDeclaration localGenericDeclaration = localTypeVariable.getGenericDeclaration();
      if ((localGenericDeclaration instanceof Class))
      {
        localClass2 = (Class)localGenericDeclaration;
        if (localClass2 == null)
          break label142;
        Type localType9 = a(paramType1, paramClass, localClass2);
        if (!(localType9 instanceof ParameterizedType))
          break label142;
        TypeVariable[] arrayOfTypeVariable = localClass2.getTypeParameters();
        m = 0;
        label76: if (m >= arrayOfTypeVariable.length)
          break label134;
        if (!localTypeVariable.equals(arrayOfTypeVariable[m]))
          break label128;
        localObject = ((ParameterizedType)localType9).getActualTypeArguments()[m];
        label112: if (localObject != localTypeVariable)
          break label147;
      }
    }
    label128: label134: label142: label147: label204: label250: Type[] arrayOfType2;
    label393: label479: Type localType1;
    do
    {
      do
      {
        Type[] arrayOfType1;
        Type localType2;
        do
        {
          do
          {
            Type localType4;
            int j;
            Type[] arrayOfType3;
            do
            {
              Type localType6;
              Type localType7;
              do
              {
                Class localClass1;
                Type localType8;
                do
                {
                  return localObject;
                  localClass2 = null;
                  break label44;
                  m++;
                  break label76;
                  throw new NoSuchElementException();
                  localObject = localTypeVariable;
                  break label112;
                  break;
                  if ((!(localObject instanceof Class)) || (!((Class)localObject).isArray()))
                    break label204;
                  localObject = (Class)localObject;
                  localClass1 = ((Class)localObject).getComponentType();
                  localType8 = a(paramType1, paramClass, localClass1);
                }
                while (localClass1 == localType8);
                return f(localType8);
                if (!(localObject instanceof GenericArrayType))
                  break label250;
                localObject = (GenericArrayType)localObject;
                localType6 = ((GenericArrayType)localObject).getGenericComponentType();
                localType7 = a(paramType1, paramClass, localType6);
              }
              while (localType6 == localType7);
              return f(localType7);
              if (!(localObject instanceof ParameterizedType))
                break label393;
              localObject = (ParameterizedType)localObject;
              Type localType3 = ((ParameterizedType)localObject).getOwnerType();
              localType4 = a(paramType1, paramClass, localType3);
              if (localType4 != localType3);
              for (j = 1; ; j = 0)
              {
                arrayOfType3 = ((ParameterizedType)localObject).getActualTypeArguments();
                int k = arrayOfType3.length;
                while (i < k)
                {
                  Type localType5 = a(paramType1, paramClass, arrayOfType3[i]);
                  if (localType5 != arrayOfType3[i])
                  {
                    if (j == 0)
                    {
                      arrayOfType3 = (Type[])arrayOfType3.clone();
                      j = 1;
                    }
                    arrayOfType3[i] = localType5;
                  }
                  i++;
                }
              }
            }
            while (j == 0);
            return new ParameterizedTypeImpl(localType4, ((ParameterizedType)localObject).getRawType(), arrayOfType3);
          }
          while (!(localObject instanceof WildcardType));
          localObject = (WildcardType)localObject;
          arrayOfType1 = ((WildcardType)localObject).getLowerBounds();
          arrayOfType2 = ((WildcardType)localObject).getUpperBounds();
          if (arrayOfType1.length != 1)
            break label479;
          localType2 = a(paramType1, paramClass, arrayOfType1[0]);
        }
        while (localType2 == arrayOfType1[0]);
        return new WildcardTypeImpl(new Type[] { Object.class }, new Type[] { localType2 });
      }
      while (arrayOfType2.length != 1);
      localType1 = a(paramType1, paramClass, arrayOfType2[0]);
    }
    while (localType1 == arrayOfType2[0]);
    return new WildcardTypeImpl(new Type[] { localType1 }, a);
  }

  public static boolean a(Type paramType1, Type paramType2)
  {
    Type localType1 = paramType2;
    Type localType2 = paramType1;
    while (true)
    {
      if (localType2 == localType1)
        return true;
      if ((localType2 instanceof Class))
        return localType2.equals(localType1);
      if ((localType2 instanceof ParameterizedType))
      {
        if (!(localType1 instanceof ParameterizedType))
          return false;
        ParameterizedType localParameterizedType1 = (ParameterizedType)localType2;
        ParameterizedType localParameterizedType2 = (ParameterizedType)localType1;
        Type localType3 = localParameterizedType1.getOwnerType();
        Type localType4 = localParameterizedType2.getOwnerType();
        if ((localType3 == localType4) || ((localType3 != null) && (localType3.equals(localType4))));
        for (int i = 1; (i != 0) && (localParameterizedType1.getRawType().equals(localParameterizedType2.getRawType())) && (Arrays.equals(localParameterizedType1.getActualTypeArguments(), localParameterizedType2.getActualTypeArguments())); i = 0)
          return true;
        return false;
      }
      if (!(localType2 instanceof GenericArrayType))
        break;
      if (!(localType1 instanceof GenericArrayType))
        return false;
      GenericArrayType localGenericArrayType1 = (GenericArrayType)localType2;
      GenericArrayType localGenericArrayType2 = (GenericArrayType)localType1;
      localType2 = localGenericArrayType1.getGenericComponentType();
      localType1 = localGenericArrayType2.getGenericComponentType();
    }
    if ((localType2 instanceof WildcardType))
    {
      if (!(localType1 instanceof WildcardType))
        return false;
      WildcardType localWildcardType1 = (WildcardType)localType2;
      WildcardType localWildcardType2 = (WildcardType)localType1;
      return (Arrays.equals(localWildcardType1.getUpperBounds(), localWildcardType2.getUpperBounds())) && (Arrays.equals(localWildcardType1.getLowerBounds(), localWildcardType2.getLowerBounds()));
    }
    if ((localType2 instanceof TypeVariable))
    {
      if (!(localType1 instanceof TypeVariable))
        return false;
      TypeVariable localTypeVariable1 = (TypeVariable)localType2;
      TypeVariable localTypeVariable2 = (TypeVariable)localType1;
      return (localTypeVariable1.getGenericDeclaration() == localTypeVariable2.getGenericDeclaration()) && (localTypeVariable1.getName().equals(localTypeVariable2.getName()));
    }
    return false;
  }

  public static Class<?> b(Type paramType)
  {
    for (Type localType1 = paramType; ; localType1 = ((WildcardType)localType1).getUpperBounds()[0])
    {
      if ((localType1 instanceof Class))
        return (Class)localType1;
      if ((localType1 instanceof ParameterizedType))
      {
        Type localType2 = ((ParameterizedType)localType1).getRawType();
        .Gson.Preconditions.a(localType2 instanceof Class);
        return (Class)localType2;
      }
      if ((localType1 instanceof GenericArrayType))
        return Array.newInstance(b(((GenericArrayType)localType1).getGenericComponentType()), 0).getClass();
      if ((localType1 instanceof TypeVariable))
        return Object.class;
      if (!(localType1 instanceof WildcardType))
        break;
    }
    if (localType1 == null);
    for (String str = "null"; ; str = localType1.getClass().getName())
      throw new IllegalArgumentException("Expected a Class, ParameterizedType, or GenericArrayType, but <" + localType1 + "> is of type " + str);
  }

  private static Type b(Type paramType, Class<?> paramClass1, Class<?> paramClass2)
  {
    .Gson.Preconditions.a(paramClass2.isAssignableFrom(paramClass1));
    return a(paramType, paramClass1, a(paramType, paramClass1, paramClass2));
  }

  public static Type[] b(Type paramType, Class<?> paramClass)
  {
    if (paramType == Properties.class)
      return new Type[] { String.class, String.class };
    Type localType = b(paramType, paramClass, Map.class);
    if ((localType instanceof ParameterizedType))
      return ((ParameterizedType)localType).getActualTypeArguments();
    return new Type[] { Object.class, Object.class };
  }

  public static String c(Type paramType)
  {
    if ((paramType instanceof Class))
      return ((Class)paramType).getName();
    return paramType.toString();
  }

  public static Type d(Type paramType)
  {
    if ((paramType instanceof GenericArrayType))
      return ((GenericArrayType)paramType).getGenericComponentType();
    return ((Class)paramType).getComponentType();
  }

  private static GenericArrayType f(Type paramType)
  {
    return new GenericArrayTypeImpl(paramType);
  }

  private static final class GenericArrayTypeImpl
    implements Serializable, GenericArrayType
  {
    private final Type a;

    public GenericArrayTypeImpl(Type paramType)
    {
      this.a = .Gson.Types.a(paramType);
    }

    public final boolean equals(Object paramObject)
    {
      return ((paramObject instanceof GenericArrayType)) && (.Gson.Types.a(this, (GenericArrayType)paramObject));
    }

    public final Type getGenericComponentType()
    {
      return this.a;
    }

    public final int hashCode()
    {
      return this.a.hashCode();
    }

    public final String toString()
    {
      return .Gson.Types.c(this.a) + "[]";
    }
  }

  private static final class ParameterizedTypeImpl
    implements Serializable, ParameterizedType
  {
    private final Type a;
    private final Type b;
    private final Type[] c;

    public ParameterizedTypeImpl(Type paramType1, Type paramType2, Type[] paramArrayOfType)
    {
      boolean bool2;
      if ((paramType2 instanceof Class))
      {
        Class localClass = (Class)paramType2;
        if ((paramType1 != null) || (localClass.getEnclosingClass() == null))
        {
          bool2 = bool1;
          .Gson.Preconditions.a(bool2);
          if ((paramType1 != null) && (localClass.getEnclosingClass() == null))
            break label153;
          label56: .Gson.Preconditions.a(bool1);
        }
      }
      else
      {
        if (paramType1 != null)
          break label159;
      }
      label153: label159: for (Type localType = null; ; localType = .Gson.Types.a(paramType1))
      {
        this.a = localType;
        this.b = .Gson.Types.a(paramType2);
        this.c = ((Type[])paramArrayOfType.clone());
        while (i < this.c.length)
        {
          .Gson.Preconditions.a(this.c[i]);
          .Gson.Types.e(this.c[i]);
          this.c[i] = .Gson.Types.a(this.c[i]);
          i++;
        }
        bool2 = false;
        break;
        bool1 = false;
        break label56;
      }
    }

    public final boolean equals(Object paramObject)
    {
      return ((paramObject instanceof ParameterizedType)) && (.Gson.Types.a(this, (ParameterizedType)paramObject));
    }

    public final Type[] getActualTypeArguments()
    {
      return (Type[])this.c.clone();
    }

    public final Type getOwnerType()
    {
      return this.a;
    }

    public final Type getRawType()
    {
      return this.b;
    }

    public final int hashCode()
    {
      return Arrays.hashCode(this.c) ^ this.b.hashCode() ^ .Gson.Types.a(this.a);
    }

    public final String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder(30 * (1 + this.c.length));
      localStringBuilder.append(.Gson.Types.c(this.b));
      if (this.c.length == 0)
        return localStringBuilder.toString();
      localStringBuilder.append("<").append(.Gson.Types.c(this.c[0]));
      for (int i = 1; i < this.c.length; i++)
        localStringBuilder.append(", ").append(.Gson.Types.c(this.c[i]));
      return ">";
    }
  }

  private static final class WildcardTypeImpl
    implements Serializable, WildcardType
  {
    private final Type a;
    private final Type b;

    public WildcardTypeImpl(Type[] paramArrayOfType1, Type[] paramArrayOfType2)
    {
      if (paramArrayOfType2.length <= i)
      {
        int k = i;
        .Gson.Preconditions.a(k);
        if (paramArrayOfType1.length != i)
          break label88;
        int n = i;
        label29: .Gson.Preconditions.a(n);
        if (paramArrayOfType2.length != i)
          break label99;
        .Gson.Preconditions.a(paramArrayOfType2[0]);
        .Gson.Types.e(paramArrayOfType2[0]);
        if (paramArrayOfType1[0] != Object.class)
          break label94;
      }
      while (true)
      {
        .Gson.Preconditions.a(i);
        this.b = .Gson.Types.a(paramArrayOfType2[0]);
        this.a = Object.class;
        return;
        int m = 0;
        break;
        label88: int i1 = 0;
        break label29;
        label94: int j = 0;
      }
      label99: .Gson.Preconditions.a(paramArrayOfType1[0]);
      .Gson.Types.e(paramArrayOfType1[0]);
      this.b = null;
      this.a = .Gson.Types.a(paramArrayOfType1[0]);
    }

    public final boolean equals(Object paramObject)
    {
      return ((paramObject instanceof WildcardType)) && (.Gson.Types.a(this, (WildcardType)paramObject));
    }

    public final Type[] getLowerBounds()
    {
      if (this.b != null)
      {
        Type[] arrayOfType = new Type[1];
        arrayOfType[0] = this.b;
        return arrayOfType;
      }
      return .Gson.Types.a;
    }

    public final Type[] getUpperBounds()
    {
      Type[] arrayOfType = new Type[1];
      arrayOfType[0] = this.a;
      return arrayOfType;
    }

    public final int hashCode()
    {
      if (this.b != null);
      for (int i = 31 + this.b.hashCode(); ; i = 1)
        return i ^ 31 + this.a.hashCode();
    }

    public final String toString()
    {
      if (this.b != null)
        return "? super " + .Gson.Types.c(this.b);
      if (this.a == Object.class)
        return "?";
      return "? extends " + .Gson.Types.c(this.a);
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.internal..Gson.Types
 * JD-Core Version:    0.6.2
 */