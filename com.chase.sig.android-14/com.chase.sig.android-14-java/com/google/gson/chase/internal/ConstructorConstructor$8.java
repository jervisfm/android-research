package com.google.gson.chase.internal;

import java.lang.reflect.Type;

class ConstructorConstructor$8
  implements ObjectConstructor<T>
{
  private final UnsafeAllocator d = UnsafeAllocator.a();

  ConstructorConstructor$8(ConstructorConstructor paramConstructorConstructor, Class paramClass, Type paramType)
  {
  }

  public final T a()
  {
    try
    {
      Object localObject = this.d.a(this.a);
      return localObject;
    }
    catch (Exception localException)
    {
      throw new RuntimeException("Unable to invoke no-args constructor for " + this.b + ". Register an InstanceCreator with Gson for this type may fix this problem.", localException);
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.internal.ConstructorConstructor.8
 * JD-Core Version:    0.6.2
 */