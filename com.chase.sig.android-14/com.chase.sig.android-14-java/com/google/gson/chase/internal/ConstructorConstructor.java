package com.google.gson.chase.internal;

import com.google.gson.chase.InstanceCreator;
import com.google.gson.chase.reflect.TypeToken;
import java.lang.reflect.Constructor;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.SortedSet;

public final class ConstructorConstructor
{
  private final ParameterizedTypeHandlerMap<InstanceCreator<?>> a;

  public ConstructorConstructor()
  {
    this(new ParameterizedTypeHandlerMap());
  }

  public ConstructorConstructor(ParameterizedTypeHandlerMap<InstanceCreator<?>> paramParameterizedTypeHandlerMap)
  {
    this.a = paramParameterizedTypeHandlerMap;
  }

  private <T> ObjectConstructor<T> a(Class<? super T> paramClass)
  {
    try
    {
      Constructor localConstructor = paramClass.getDeclaredConstructor(new Class[0]);
      if (!localConstructor.isAccessible())
        localConstructor.setAccessible(true);
      ConstructorConstructor.2 local2 = new ConstructorConstructor.2(this, localConstructor);
      return local2;
    }
    catch (NoSuchMethodException localNoSuchMethodException)
    {
    }
    return null;
  }

  public final <T> ObjectConstructor<T> a(TypeToken<T> paramTypeToken)
  {
    Type localType = paramTypeToken.b();
    Class localClass = paramTypeToken.a();
    InstanceCreator localInstanceCreator = (InstanceCreator)this.a.a(localType, false);
    Object localObject;
    if (localInstanceCreator != null)
      localObject = new ConstructorConstructor.1(this, localInstanceCreator, localType);
    while (true)
    {
      return localObject;
      localObject = a(localClass);
      if (localObject == null)
      {
        if (Collection.class.isAssignableFrom(localClass))
          if (SortedSet.class.isAssignableFrom(localClass))
            localObject = new ConstructorConstructor.3(this);
        while (localObject == null)
        {
          return new ConstructorConstructor.8(this, localClass, localType);
          if (Set.class.isAssignableFrom(localClass))
          {
            localObject = new ConstructorConstructor.4(this);
          }
          else if (Queue.class.isAssignableFrom(localClass))
          {
            localObject = new ConstructorConstructor.5(this);
          }
          else
          {
            localObject = new ConstructorConstructor.6(this);
            continue;
            if (Map.class.isAssignableFrom(localClass))
              localObject = new ConstructorConstructor.7(this);
            else
              localObject = null;
          }
        }
      }
    }
  }

  public final String toString()
  {
    return this.a.toString();
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.internal.ConstructorConstructor
 * JD-Core Version:    0.6.2
 */