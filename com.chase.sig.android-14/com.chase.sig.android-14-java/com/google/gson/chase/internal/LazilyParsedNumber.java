package com.google.gson.chase.internal;

import java.math.BigInteger;

public final class LazilyParsedNumber extends Number
{
  private final String a;

  public LazilyParsedNumber(String paramString)
  {
    this.a = paramString;
  }

  public final double doubleValue()
  {
    return Double.parseDouble(this.a);
  }

  public final float floatValue()
  {
    return Float.parseFloat(this.a);
  }

  public final int intValue()
  {
    try
    {
      int i = Integer.parseInt(this.a);
      return i;
    }
    catch (NumberFormatException localNumberFormatException1)
    {
      try
      {
        long l = Long.parseLong(this.a);
        return (int)l;
      }
      catch (NumberFormatException localNumberFormatException2)
      {
      }
    }
    return new BigInteger(this.a).intValue();
  }

  public final long longValue()
  {
    try
    {
      long l = Long.parseLong(this.a);
      return l;
    }
    catch (NumberFormatException localNumberFormatException)
    {
    }
    return new BigInteger(this.a).longValue();
  }

  public final String toString()
  {
    return this.a;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.internal.LazilyParsedNumber
 * JD-Core Version:    0.6.2
 */