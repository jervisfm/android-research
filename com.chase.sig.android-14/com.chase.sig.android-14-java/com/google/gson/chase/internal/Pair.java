package com.google.gson.chase.internal;

public final class Pair<FIRST, SECOND>
{
  public final FIRST a;
  public final SECOND b;

  public Pair(FIRST paramFIRST, SECOND paramSECOND)
  {
    this.a = paramFIRST;
    this.b = paramSECOND;
  }

  private static boolean a(Object paramObject1, Object paramObject2)
  {
    return (paramObject1 == paramObject2) || ((paramObject1 != null) && (paramObject1.equals(paramObject2)));
  }

  public final boolean equals(Object paramObject)
  {
    if (!(paramObject instanceof Pair));
    Pair localPair;
    do
    {
      return false;
      localPair = (Pair)paramObject;
    }
    while ((!a(this.a, localPair.a)) || (!a(this.b, localPair.b)));
    return true;
  }

  public final int hashCode()
  {
    if (this.a != null);
    for (int i = this.a.hashCode(); ; i = 0)
    {
      int j = i * 17;
      Object localObject = this.b;
      int k = 0;
      if (localObject != null)
        k = this.b.hashCode();
      return j + k * 17;
    }
  }

  public final String toString()
  {
    Object[] arrayOfObject = new Object[2];
    arrayOfObject[0] = this.a;
    arrayOfObject[1] = this.b;
    return String.format("{%s,%s}", arrayOfObject);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.internal.Pair
 * JD-Core Version:    0.6.2
 */