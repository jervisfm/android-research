package com.google.gson.chase.internal;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class ParameterizedTypeHandlerMap<T>
{
  private static final Logger a = Logger.getLogger(ParameterizedTypeHandlerMap.class.getName());
  private final Map<Type, T> b = new HashMap();
  private final Map<Type, T> c = new HashMap();
  private final List<Pair<Class<?>, T>> d = new ArrayList();
  private final List<Pair<Class<?>, T>> e = new ArrayList();
  private boolean f = true;

  private static void a(StringBuilder paramStringBuilder, List<Pair<Class<?>, T>> paramList)
  {
    Iterator localIterator = paramList.iterator();
    int i = 1;
    if (localIterator.hasNext())
    {
      Pair localPair = (Pair)localIterator.next();
      if (i != 0);
      for (int j = 0; ; j = i)
      {
        paramStringBuilder.append(.Gson.Types.b((Type)localPair.a).getSimpleName()).append(':');
        paramStringBuilder.append(localPair.b);
        i = j;
        break;
        paramStringBuilder.append(',');
      }
    }
  }

  private static void a(StringBuilder paramStringBuilder, Map<Type, T> paramMap)
  {
    Iterator localIterator = paramMap.entrySet().iterator();
    int i = 1;
    if (localIterator.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      if (i != 0);
      for (int j = 0; ; j = i)
      {
        paramStringBuilder.append(.Gson.Types.b((Type)localEntry.getKey()).getSimpleName()).append(':');
        paramStringBuilder.append(localEntry.getValue());
        i = j;
        break;
        paramStringBuilder.append(',');
      }
    }
  }

  public final ParameterizedTypeHandlerMap<T> a()
  {
    try
    {
      this.f = false;
      return this;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public final T a(Type paramType, boolean paramBoolean)
  {
    if (!paramBoolean);
    try
    {
      Object localObject3 = this.c.get(paramType);
      Object localObject2 = localObject3;
      if (localObject2 != null);
      while (true)
      {
        return localObject2;
        localObject2 = this.b.get(paramType);
        if (localObject2 == null)
        {
          Class localClass = .Gson.Types.b(paramType);
          if (localClass != paramType)
          {
            localObject2 = a(localClass, paramBoolean);
            if (localObject2 != null);
          }
          else
          {
            if (!paramBoolean)
            {
              Iterator localIterator1 = this.e.iterator();
              while (true)
                if (localIterator1.hasNext())
                {
                  Pair localPair2 = (Pair)localIterator1.next();
                  if (((Class)localPair2.a).isAssignableFrom(localClass))
                  {
                    localObject2 = localPair2.b;
                    break;
                  }
                }
            }
            Iterator localIterator2 = this.d.iterator();
            while (true)
              if (localIterator2.hasNext())
              {
                Pair localPair1 = (Pair)localIterator2.next();
                if (((Class)localPair1.a).isAssignableFrom(localClass))
                {
                  localObject2 = localPair1.b;
                  break;
                }
              }
            localObject2 = null;
          }
        }
      }
    }
    finally
    {
    }
  }

  public final void a(Type paramType, T paramT)
  {
    try
    {
      if (!this.f)
        throw new IllegalStateException("Attempted to modify an unmodifiable map.");
    }
    finally
    {
    }
    if (a(paramType))
      a.log(Level.WARNING, "Overriding the existing type handler for {0}", paramType);
    this.c.put(paramType, paramT);
  }

  public final boolean a(Type paramType)
  {
    try
    {
      if (!this.c.containsKey(paramType))
      {
        boolean bool2 = this.b.containsKey(paramType);
        if (!bool2);
      }
      else
      {
        bool1 = true;
        return bool1;
      }
      boolean bool1 = false;
    }
    finally
    {
    }
  }

  public final ParameterizedTypeHandlerMap<T> b()
  {
    try
    {
      ParameterizedTypeHandlerMap localParameterizedTypeHandlerMap = new ParameterizedTypeHandlerMap();
      localParameterizedTypeHandlerMap.b.putAll(this.b);
      localParameterizedTypeHandlerMap.c.putAll(this.c);
      localParameterizedTypeHandlerMap.d.addAll(this.d);
      localParameterizedTypeHandlerMap.e.addAll(this.e);
      return localParameterizedTypeHandlerMap;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public final String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder("{userTypeHierarchyList:{");
    a(localStringBuilder, this.e);
    localStringBuilder.append("},systemTypeHierarchyList:{");
    a(localStringBuilder, this.d);
    localStringBuilder.append("},userMap:{");
    a(localStringBuilder, this.c);
    localStringBuilder.append("},systemMap:{");
    a(localStringBuilder, this.b);
    localStringBuilder.append("}");
    return localStringBuilder.toString();
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.internal.ParameterizedTypeHandlerMap
 * JD-Core Version:    0.6.2
 */