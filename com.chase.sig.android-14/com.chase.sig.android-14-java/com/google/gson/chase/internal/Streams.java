package com.google.gson.chase.internal;

import com.google.gson.chase.JsonElement;
import com.google.gson.chase.JsonIOException;
import com.google.gson.chase.JsonNull;
import com.google.gson.chase.JsonSyntaxException;
import com.google.gson.chase.internal.bind.TypeAdapter;
import com.google.gson.chase.internal.bind.TypeAdapters;
import com.google.gson.chase.stream.JsonReader;
import com.google.gson.chase.stream.JsonWriter;
import com.google.gson.chase.stream.MalformedJsonException;
import java.io.EOFException;
import java.io.IOException;
import java.io.Writer;

public final class Streams
{
  public static JsonElement a(JsonReader paramJsonReader)
  {
    int i = 1;
    try
    {
      paramJsonReader.f();
      i = 0;
      JsonElement localJsonElement = (JsonElement)TypeAdapters.O.a(paramJsonReader);
      return localJsonElement;
    }
    catch (EOFException localEOFException)
    {
      if (i != 0)
        return JsonNull.a;
      throw new JsonIOException(localEOFException);
    }
    catch (MalformedJsonException localMalformedJsonException)
    {
      throw new JsonSyntaxException(localMalformedJsonException);
    }
    catch (IOException localIOException)
    {
      throw new JsonIOException(localIOException);
    }
    catch (NumberFormatException localNumberFormatException)
    {
      throw new JsonSyntaxException(localNumberFormatException);
    }
  }

  public static Writer a(Appendable paramAppendable)
  {
    if ((paramAppendable instanceof Writer))
      return (Writer)paramAppendable;
    return new AppendableWriter(paramAppendable, (byte)0);
  }

  public static void a(JsonElement paramJsonElement, JsonWriter paramJsonWriter)
  {
    TypeAdapters.O.a(paramJsonWriter, paramJsonElement);
  }

  private static class AppendableWriter extends Writer
  {
    private final Appendable a;
    private final CurrentWrite b = new CurrentWrite();

    private AppendableWriter(Appendable paramAppendable)
    {
      this.a = paramAppendable;
    }

    public void close()
    {
    }

    public void flush()
    {
    }

    public void write(int paramInt)
    {
      this.a.append((char)paramInt);
    }

    public void write(char[] paramArrayOfChar, int paramInt1, int paramInt2)
    {
      this.b.a = paramArrayOfChar;
      this.a.append(this.b, paramInt1, paramInt1 + paramInt2);
    }

    static class CurrentWrite
      implements CharSequence
    {
      char[] a;

      public char charAt(int paramInt)
      {
        return this.a[paramInt];
      }

      public int length()
      {
        return this.a.length;
      }

      public CharSequence subSequence(int paramInt1, int paramInt2)
      {
        return new String(this.a, paramInt1, paramInt2 - paramInt1);
      }
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.internal.Streams
 * JD-Core Version:    0.6.2
 */