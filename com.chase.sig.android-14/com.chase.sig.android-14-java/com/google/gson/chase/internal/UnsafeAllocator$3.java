package com.google.gson.chase.internal;

import java.lang.reflect.Method;

class UnsafeAllocator$3 extends UnsafeAllocator
{
  UnsafeAllocator$3(Method paramMethod, int paramInt)
  {
  }

  public final <T> T a(Class<T> paramClass)
  {
    Method localMethod = this.a;
    Object[] arrayOfObject = new Object[2];
    arrayOfObject[0] = paramClass;
    arrayOfObject[1] = Integer.valueOf(this.b);
    return localMethod.invoke(null, arrayOfObject);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.internal.UnsafeAllocator.3
 * JD-Core Version:    0.6.2
 */