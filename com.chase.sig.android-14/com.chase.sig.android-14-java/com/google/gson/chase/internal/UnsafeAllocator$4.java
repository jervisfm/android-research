package com.google.gson.chase.internal;

class UnsafeAllocator$4 extends UnsafeAllocator
{
  public final <T> T a(Class<T> paramClass)
  {
    throw new UnsupportedOperationException("Cannot allocate " + paramClass);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.internal.UnsafeAllocator.4
 * JD-Core Version:    0.6.2
 */