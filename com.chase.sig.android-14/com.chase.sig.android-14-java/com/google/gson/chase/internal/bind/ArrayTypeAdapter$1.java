package com.google.gson.chase.internal.bind;

import com.google.gson.chase.internal..Gson.Types;
import com.google.gson.chase.reflect.TypeToken;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.Type;

class ArrayTypeAdapter$1
  implements TypeAdapter.Factory
{
  public final <T> TypeAdapter<T> a(MiniGson paramMiniGson, TypeToken<T> paramTypeToken)
  {
    Type localType1 = paramTypeToken.b();
    if ((!(localType1 instanceof GenericArrayType)) && ((!(localType1 instanceof Class)) || (!((Class)localType1).isArray())))
      return null;
    Type localType2 = .Gson.Types.d(localType1);
    return new ArrayTypeAdapter(paramMiniGson, paramMiniGson.a(TypeToken.a(localType2)), .Gson.Types.b(localType2));
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.internal.bind.ArrayTypeAdapter.1
 * JD-Core Version:    0.6.2
 */