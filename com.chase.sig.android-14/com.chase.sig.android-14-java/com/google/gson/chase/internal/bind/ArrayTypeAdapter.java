package com.google.gson.chase.internal.bind;

import com.google.gson.chase.stream.JsonReader;
import com.google.gson.chase.stream.JsonToken;
import com.google.gson.chase.stream.JsonWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public final class ArrayTypeAdapter<E> extends TypeAdapter<Object>
{
  public static final TypeAdapter.Factory a = new ArrayTypeAdapter.1();
  private final Class<E> b;
  private final TypeAdapter<E> c;

  public ArrayTypeAdapter(MiniGson paramMiniGson, TypeAdapter<E> paramTypeAdapter, Class<E> paramClass)
  {
    this.c = new TypeAdapterRuntimeTypeWrapper(paramMiniGson, paramTypeAdapter, paramClass);
    this.b = paramClass;
  }

  public final Object a(JsonReader paramJsonReader)
  {
    if (paramJsonReader.f() == JsonToken.i)
    {
      paramJsonReader.j();
      return null;
    }
    ArrayList localArrayList = new ArrayList();
    paramJsonReader.a();
    while (paramJsonReader.e())
      localArrayList.add(this.c.a(paramJsonReader));
    paramJsonReader.b();
    Object localObject = Array.newInstance(this.b, localArrayList.size());
    for (int i = 0; i < localArrayList.size(); i++)
      Array.set(localObject, i, localArrayList.get(i));
    return localObject;
  }

  public final void a(JsonWriter paramJsonWriter, Object paramObject)
  {
    if (paramObject == null)
    {
      paramJsonWriter.f();
      return;
    }
    paramJsonWriter.b();
    int i = 0;
    int j = Array.getLength(paramObject);
    while (i < j)
    {
      Object localObject = Array.get(paramObject, i);
      this.c.a(paramJsonWriter, localObject);
      i++;
    }
    paramJsonWriter.c();
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.internal.bind.ArrayTypeAdapter
 * JD-Core Version:    0.6.2
 */