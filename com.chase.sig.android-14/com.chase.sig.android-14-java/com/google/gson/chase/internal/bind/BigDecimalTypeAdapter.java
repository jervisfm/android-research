package com.google.gson.chase.internal.bind;

import com.google.gson.chase.JsonSyntaxException;
import com.google.gson.chase.stream.JsonReader;
import com.google.gson.chase.stream.JsonToken;
import java.math.BigDecimal;

public final class BigDecimalTypeAdapter extends TypeAdapter<BigDecimal>
{
  private static BigDecimal b(JsonReader paramJsonReader)
  {
    if (paramJsonReader.f() == JsonToken.i)
    {
      paramJsonReader.j();
      return null;
    }
    try
    {
      BigDecimal localBigDecimal = new BigDecimal(paramJsonReader.h());
      return localBigDecimal;
    }
    catch (NumberFormatException localNumberFormatException)
    {
      throw new JsonSyntaxException(localNumberFormatException);
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.internal.bind.BigDecimalTypeAdapter
 * JD-Core Version:    0.6.2
 */