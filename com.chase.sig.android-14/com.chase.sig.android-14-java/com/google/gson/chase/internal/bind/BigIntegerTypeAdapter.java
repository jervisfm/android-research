package com.google.gson.chase.internal.bind;

import com.google.gson.chase.JsonSyntaxException;
import com.google.gson.chase.stream.JsonReader;
import com.google.gson.chase.stream.JsonToken;
import java.math.BigInteger;

public final class BigIntegerTypeAdapter extends TypeAdapter<BigInteger>
{
  private static BigInteger b(JsonReader paramJsonReader)
  {
    if (paramJsonReader.f() == JsonToken.i)
    {
      paramJsonReader.j();
      return null;
    }
    try
    {
      BigInteger localBigInteger = new BigInteger(paramJsonReader.h());
      return localBigInteger;
    }
    catch (NumberFormatException localNumberFormatException)
    {
      throw new JsonSyntaxException(localNumberFormatException);
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.internal.bind.BigIntegerTypeAdapter
 * JD-Core Version:    0.6.2
 */