package com.google.gson.chase.internal.bind;

import com.google.gson.chase.internal..Gson.Types;
import com.google.gson.chase.internal.ConstructorConstructor;
import com.google.gson.chase.internal.ObjectConstructor;
import com.google.gson.chase.reflect.TypeToken;
import java.lang.reflect.Type;
import java.util.Collection;

public final class CollectionTypeAdapterFactory
  implements TypeAdapter.Factory
{
  private final ConstructorConstructor a;

  public CollectionTypeAdapterFactory(ConstructorConstructor paramConstructorConstructor)
  {
    this.a = paramConstructorConstructor;
  }

  public final <T> TypeAdapter<T> a(MiniGson paramMiniGson, TypeToken<T> paramTypeToken)
  {
    Type localType1 = paramTypeToken.b();
    Class localClass = paramTypeToken.a();
    if (!Collection.class.isAssignableFrom(localClass))
      return null;
    Type localType2 = .Gson.Types.a(localType1, localClass);
    return new Adapter(paramMiniGson, localType2, paramMiniGson.a(TypeToken.a(localType2)), this.a.a(paramTypeToken));
  }

  private final class Adapter<E> extends TypeAdapter<Collection<E>>
  {
    private final TypeAdapter<E> b;
    private final ObjectConstructor<? extends Collection<E>> c;

    public Adapter(Type paramTypeAdapter, TypeAdapter<E> paramObjectConstructor, ObjectConstructor<? extends Collection<E>> arg4)
    {
      TypeAdapter localTypeAdapter;
      this.b = new TypeAdapterRuntimeTypeWrapper(paramTypeAdapter, localTypeAdapter, paramObjectConstructor);
      Object localObject;
      this.c = localObject;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.internal.bind.CollectionTypeAdapterFactory
 * JD-Core Version:    0.6.2
 */