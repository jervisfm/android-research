package com.google.gson.chase.internal.bind;

import com.google.gson.chase.reflect.TypeToken;
import com.google.gson.chase.stream.JsonReader;
import com.google.gson.chase.stream.JsonWriter;

class ExcludedTypeAdapterFactory$1 extends TypeAdapter<T>
{
  private TypeAdapter<T> f;

  ExcludedTypeAdapterFactory$1(ExcludedTypeAdapterFactory paramExcludedTypeAdapterFactory, boolean paramBoolean1, boolean paramBoolean2, MiniGson paramMiniGson, TypeToken paramTypeToken)
  {
  }

  private TypeAdapter<T> a()
  {
    TypeAdapter localTypeAdapter1 = this.f;
    if (localTypeAdapter1 != null)
      return localTypeAdapter1;
    TypeAdapter localTypeAdapter2 = this.c.a(this.e, this.d);
    this.f = localTypeAdapter2;
    return localTypeAdapter2;
  }

  public final T a(JsonReader paramJsonReader)
  {
    if (this.a)
    {
      paramJsonReader.n();
      return null;
    }
    return a().a(paramJsonReader);
  }

  public final void a(JsonWriter paramJsonWriter, T paramT)
  {
    if (this.b)
    {
      paramJsonWriter.f();
      return;
    }
    a().a(paramJsonWriter, paramT);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.internal.bind.ExcludedTypeAdapterFactory.1
 * JD-Core Version:    0.6.2
 */