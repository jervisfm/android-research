package com.google.gson.chase.internal.bind;

import com.google.gson.chase.ExclusionStrategy;
import com.google.gson.chase.reflect.TypeToken;

public final class ExcludedTypeAdapterFactory
  implements TypeAdapter.Factory
{
  private final ExclusionStrategy a;
  private final ExclusionStrategy b;

  public ExcludedTypeAdapterFactory(ExclusionStrategy paramExclusionStrategy1, ExclusionStrategy paramExclusionStrategy2)
  {
    this.a = paramExclusionStrategy1;
    this.b = paramExclusionStrategy2;
  }

  public final <T> TypeAdapter<T> a(MiniGson paramMiniGson, TypeToken<T> paramTypeToken)
  {
    Class localClass = paramTypeToken.a();
    boolean bool1 = this.a.a(localClass);
    boolean bool2 = this.b.a(localClass);
    if ((!bool1) && (!bool2))
      return null;
    return new ExcludedTypeAdapterFactory.1(this, bool2, bool1, paramMiniGson, paramTypeToken);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.internal.bind.ExcludedTypeAdapterFactory
 * JD-Core Version:    0.6.2
 */