package com.google.gson.chase.internal.bind;

import com.google.gson.chase.JsonArray;
import com.google.gson.chase.JsonElement;
import com.google.gson.chase.JsonNull;
import com.google.gson.chase.JsonObject;
import com.google.gson.chase.JsonPrimitive;
import com.google.gson.chase.stream.JsonReader;
import com.google.gson.chase.stream.JsonToken;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

public final class JsonElementReader extends JsonReader
{
  private static final Reader b = new JsonElementReader.1();
  private static final Object c = new Object();
  private final List<Object> d = new ArrayList();

  public JsonElementReader(JsonElement paramJsonElement)
  {
    super(b);
    this.d.add(paramJsonElement);
  }

  private void a(JsonToken paramJsonToken)
  {
    if (f() != paramJsonToken)
      throw new IllegalStateException("Expected " + paramJsonToken + " but was " + f());
  }

  private Object p()
  {
    return this.d.get(-1 + this.d.size());
  }

  private Object q()
  {
    return this.d.remove(-1 + this.d.size());
  }

  public final void a()
  {
    a(JsonToken.a);
    JsonArray localJsonArray = (JsonArray)p();
    this.d.add(localJsonArray.iterator());
  }

  public final void b()
  {
    a(JsonToken.b);
    q();
    q();
  }

  public final void c()
  {
    a(JsonToken.c);
    JsonObject localJsonObject = (JsonObject)p();
    this.d.add(localJsonObject.n().iterator());
  }

  public final void close()
  {
    this.d.clear();
    this.d.add(c);
  }

  public final void d()
  {
    a(JsonToken.d);
    q();
    q();
  }

  public final boolean e()
  {
    JsonToken localJsonToken = f();
    return (localJsonToken != JsonToken.d) && (localJsonToken != JsonToken.b);
  }

  public final JsonToken f()
  {
    Object localObject;
    boolean bool;
    while (true)
    {
      if (this.d.isEmpty())
        return JsonToken.j;
      localObject = p();
      if (!(localObject instanceof Iterator))
        break label109;
      bool = this.d.get(-2 + this.d.size()) instanceof JsonObject;
      Iterator localIterator = (Iterator)localObject;
      if (!localIterator.hasNext())
        break;
      if (bool)
        return JsonToken.e;
      this.d.add(localIterator.next());
    }
    if (bool)
      return JsonToken.d;
    return JsonToken.b;
    label109: if ((localObject instanceof JsonObject))
      return JsonToken.c;
    if ((localObject instanceof JsonArray))
      return JsonToken.a;
    if ((localObject instanceof JsonPrimitive))
    {
      JsonPrimitive localJsonPrimitive = (JsonPrimitive)localObject;
      if (localJsonPrimitive.p())
        return JsonToken.f;
      if (localJsonPrimitive.n())
        return JsonToken.h;
      if (localJsonPrimitive.o())
        return JsonToken.g;
      throw new AssertionError();
    }
    if ((localObject instanceof JsonNull))
      return JsonToken.i;
    if (localObject == c)
      throw new IllegalStateException("JsonReader is closed");
    throw new AssertionError();
  }

  public final String g()
  {
    a(JsonToken.e);
    Map.Entry localEntry = (Map.Entry)((Iterator)p()).next();
    this.d.add(localEntry.getValue());
    return (String)localEntry.getKey();
  }

  public final String h()
  {
    JsonToken localJsonToken = f();
    if ((localJsonToken != JsonToken.f) && (localJsonToken != JsonToken.g))
      throw new IllegalStateException("Expected " + JsonToken.f + " but was " + localJsonToken);
    return ((JsonPrimitive)q()).b();
  }

  public final boolean i()
  {
    a(JsonToken.h);
    return ((JsonPrimitive)q()).f();
  }

  public final void j()
  {
    a(JsonToken.i);
    q();
  }

  public final double k()
  {
    JsonToken localJsonToken = f();
    if ((localJsonToken != JsonToken.g) && (localJsonToken != JsonToken.f))
      throw new IllegalStateException("Expected " + JsonToken.g + " but was " + localJsonToken);
    double d1 = ((JsonPrimitive)p()).c();
    if ((!this.a) && ((Double.isNaN(d1)) || (Double.isInfinite(d1))))
      throw new NumberFormatException("JSON forbids NaN and infinities: " + d1);
    q();
    return d1;
  }

  public final long l()
  {
    JsonToken localJsonToken = f();
    if ((localJsonToken != JsonToken.g) && (localJsonToken != JsonToken.f))
      throw new IllegalStateException("Expected " + JsonToken.g + " but was " + localJsonToken);
    long l = ((JsonPrimitive)p()).d();
    q();
    return l;
  }

  public final int m()
  {
    JsonToken localJsonToken = f();
    if ((localJsonToken != JsonToken.g) && (localJsonToken != JsonToken.f))
      throw new IllegalStateException("Expected " + JsonToken.g + " but was " + localJsonToken);
    int i = ((JsonPrimitive)p()).e();
    q();
    return i;
  }

  public final void n()
  {
    if (f() == JsonToken.e)
    {
      g();
      return;
    }
    q();
  }

  public final String toString()
  {
    return getClass().getSimpleName();
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.internal.bind.JsonElementReader
 * JD-Core Version:    0.6.2
 */