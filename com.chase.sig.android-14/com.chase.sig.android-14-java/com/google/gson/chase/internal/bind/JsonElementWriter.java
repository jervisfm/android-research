package com.google.gson.chase.internal.bind;

import com.google.gson.chase.JsonArray;
import com.google.gson.chase.JsonElement;
import com.google.gson.chase.JsonNull;
import com.google.gson.chase.JsonObject;
import com.google.gson.chase.JsonPrimitive;
import com.google.gson.chase.stream.JsonWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

public final class JsonElementWriter extends JsonWriter
{
  private static final Writer c = new JsonElementWriter.1();
  private static final JsonPrimitive d = new JsonPrimitive("closed");
  private final List<JsonElement> e = new ArrayList();
  private String f;
  private JsonElement g = JsonNull.a;

  public JsonElementWriter()
  {
    super(c);
  }

  private void a(JsonElement paramJsonElement)
  {
    if (this.f != null)
    {
      if ((!paramJsonElement.j()) || (this.b))
        ((JsonObject)j()).a(this.f, paramJsonElement);
      this.f = null;
      return;
    }
    if (this.e.isEmpty())
    {
      this.g = paramJsonElement;
      return;
    }
    JsonElement localJsonElement = j();
    if ((localJsonElement instanceof JsonArray))
    {
      ((JsonArray)localJsonElement).a(paramJsonElement);
      return;
    }
    throw new IllegalStateException();
  }

  private JsonElement j()
  {
    return (JsonElement)this.e.get(-1 + this.e.size());
  }

  public final JsonElement a()
  {
    if (!this.e.isEmpty())
      throw new IllegalStateException("Expected one JSON element but was " + this.e);
    return this.g;
  }

  public final JsonWriter a(long paramLong)
  {
    a(new JsonPrimitive(Long.valueOf(paramLong)));
    return this;
  }

  public final JsonWriter a(Number paramNumber)
  {
    if (paramNumber == null)
      return f();
    if (!this.a)
    {
      double d1 = paramNumber.doubleValue();
      if ((Double.isNaN(d1)) || (Double.isInfinite(d1)))
        throw new IllegalArgumentException("JSON forbids NaN and infinities: " + paramNumber);
    }
    a(new JsonPrimitive(paramNumber));
    return this;
  }

  public final JsonWriter a(String paramString)
  {
    if ((this.e.isEmpty()) || (this.f != null))
      throw new IllegalStateException();
    if ((j() instanceof JsonObject))
    {
      this.f = paramString;
      return this;
    }
    throw new IllegalStateException();
  }

  public final JsonWriter a(boolean paramBoolean)
  {
    a(new JsonPrimitive(Boolean.valueOf(paramBoolean)));
    return this;
  }

  public final JsonWriter b()
  {
    JsonArray localJsonArray = new JsonArray();
    a(localJsonArray);
    this.e.add(localJsonArray);
    return this;
  }

  public final JsonWriter b(String paramString)
  {
    if (paramString == null)
      return f();
    a(new JsonPrimitive(paramString));
    return this;
  }

  public final JsonWriter c()
  {
    if ((this.e.isEmpty()) || (this.f != null))
      throw new IllegalStateException();
    if ((j() instanceof JsonArray))
    {
      this.e.remove(-1 + this.e.size());
      return this;
    }
    throw new IllegalStateException();
  }

  public final void close()
  {
    if (!this.e.isEmpty())
      throw new IOException("Incomplete document");
    this.e.add(d);
  }

  public final JsonWriter d()
  {
    JsonObject localJsonObject = new JsonObject();
    a(localJsonObject);
    this.e.add(localJsonObject);
    return this;
  }

  public final JsonWriter e()
  {
    if ((this.e.isEmpty()) || (this.f != null))
      throw new IllegalStateException();
    if ((j() instanceof JsonObject))
    {
      this.e.remove(-1 + this.e.size());
      return this;
    }
    throw new IllegalStateException();
  }

  public final JsonWriter f()
  {
    a(JsonNull.a);
    return this;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.internal.bind.JsonElementWriter
 * JD-Core Version:    0.6.2
 */