package com.google.gson.chase.internal.bind;

import com.google.gson.chase.internal..Gson.Types;
import com.google.gson.chase.internal.ConstructorConstructor;
import com.google.gson.chase.internal.ObjectConstructor;
import com.google.gson.chase.reflect.TypeToken;
import java.lang.reflect.Type;
import java.util.Map;

public final class MapTypeAdapterFactory
  implements TypeAdapter.Factory
{
  private final ConstructorConstructor a;
  private final boolean b;

  public MapTypeAdapterFactory(ConstructorConstructor paramConstructorConstructor, boolean paramBoolean)
  {
    this.a = paramConstructorConstructor;
    this.b = paramBoolean;
  }

  public final <T> TypeAdapter<T> a(MiniGson paramMiniGson, TypeToken<T> paramTypeToken)
  {
    Type localType1 = paramTypeToken.b();
    if (!Map.class.isAssignableFrom(paramTypeToken.a()))
      return null;
    Type[] arrayOfType = .Gson.Types.b(localType1, .Gson.Types.b(localType1));
    Type localType2 = arrayOfType[0];
    if ((localType2 == Boolean.TYPE) || (localType2 == Boolean.class));
    for (TypeAdapter localTypeAdapter1 = TypeAdapters.d; ; localTypeAdapter1 = paramMiniGson.a(TypeToken.a(localType2)))
    {
      TypeAdapter localTypeAdapter2 = paramMiniGson.a(TypeToken.a(arrayOfType[1]));
      ObjectConstructor localObjectConstructor = this.a.a(paramTypeToken);
      return new Adapter(paramMiniGson, arrayOfType[0], localTypeAdapter1, arrayOfType[1], localTypeAdapter2, localObjectConstructor);
    }
  }

  private final class Adapter<K, V> extends TypeAdapter<Map<K, V>>
  {
    private final TypeAdapter<K> b;
    private final TypeAdapter<V> c;
    private final ObjectConstructor<? extends Map<K, V>> d;

    public Adapter(Type paramTypeAdapter, TypeAdapter<K> paramType1, Type paramTypeAdapter1, TypeAdapter<V> paramObjectConstructor, ObjectConstructor<? extends Map<K, V>> arg6)
    {
      this.b = new TypeAdapterRuntimeTypeWrapper(paramTypeAdapter, paramTypeAdapter1, paramType1);
      TypeAdapter localTypeAdapter;
      this.c = new TypeAdapterRuntimeTypeWrapper(paramTypeAdapter, localTypeAdapter, paramObjectConstructor);
      Object localObject;
      this.d = localObject;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.internal.bind.MapTypeAdapterFactory
 * JD-Core Version:    0.6.2
 */