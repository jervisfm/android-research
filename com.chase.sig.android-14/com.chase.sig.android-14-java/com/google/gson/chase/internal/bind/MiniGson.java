package com.google.gson.chase.internal.bind;

import com.google.gson.chase.internal.ConstructorConstructor;
import com.google.gson.chase.reflect.TypeToken;
import com.google.gson.chase.stream.JsonReader;
import com.google.gson.chase.stream.JsonWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public final class MiniGson
{
  private final ThreadLocal<Map<TypeToken<?>, FutureTypeAdapter<?>>> a = new MiniGson.1(this);
  private final List<TypeAdapter.Factory> b;

  private MiniGson(Builder paramBuilder)
  {
    ConstructorConstructor localConstructorConstructor = new ConstructorConstructor();
    ArrayList localArrayList = new ArrayList();
    if (paramBuilder.a)
    {
      localArrayList.add(TypeAdapters.e);
      localArrayList.add(TypeAdapters.k);
      localArrayList.add(TypeAdapters.q);
      localArrayList.add(TypeAdapters.o);
      localArrayList.add(TypeAdapters.m);
      localArrayList.add(TypeAdapters.w);
    }
    localArrayList.addAll(Builder.a(paramBuilder));
    if (paramBuilder.a)
    {
      localArrayList.add(new CollectionTypeAdapterFactory(localConstructorConstructor));
      localArrayList.add(new StringToValueMapTypeAdapterFactory(localConstructorConstructor));
      localArrayList.add(ArrayTypeAdapter.a);
      localArrayList.add(ObjectTypeAdapter.a);
      localArrayList.add(new ReflectiveTypeAdapterFactory(localConstructorConstructor));
    }
    this.b = Collections.unmodifiableList(localArrayList);
  }

  public final <T> TypeAdapter<T> a(TypeAdapter.Factory paramFactory, TypeToken<T> paramTypeToken)
  {
    Iterator localIterator = this.b.iterator();
    int i = 0;
    while (localIterator.hasNext())
    {
      TypeAdapter.Factory localFactory = (TypeAdapter.Factory)localIterator.next();
      if (i == 0)
      {
        if (localFactory == paramFactory)
          i = 1;
      }
      else
      {
        TypeAdapter localTypeAdapter = localFactory.a(this, paramTypeToken);
        if (localTypeAdapter != null)
          return localTypeAdapter;
      }
    }
    throw new IllegalArgumentException("This MiniGSON cannot serialize " + paramTypeToken);
  }

  public final <T> TypeAdapter<T> a(TypeToken<T> paramTypeToken)
  {
    Map localMap = (Map)this.a.get();
    FutureTypeAdapter localFutureTypeAdapter1 = (FutureTypeAdapter)localMap.get(paramTypeToken);
    if (localFutureTypeAdapter1 != null)
      return localFutureTypeAdapter1;
    FutureTypeAdapter localFutureTypeAdapter2 = new FutureTypeAdapter();
    localMap.put(paramTypeToken, localFutureTypeAdapter2);
    try
    {
      Iterator localIterator = this.b.iterator();
      while (localIterator.hasNext())
      {
        TypeAdapter localTypeAdapter = ((TypeAdapter.Factory)localIterator.next()).a(this, paramTypeToken);
        if (localTypeAdapter != null)
        {
          localFutureTypeAdapter2.a(localTypeAdapter);
          return localTypeAdapter;
        }
      }
      throw new IllegalArgumentException("This MiniGSON cannot handle " + paramTypeToken);
    }
    finally
    {
      localMap.remove(paramTypeToken);
    }
  }

  public final <T> TypeAdapter<T> a(Class<T> paramClass)
  {
    return a(TypeToken.a(paramClass));
  }

  public static final class Builder
  {
    boolean a = true;
    private final List<TypeAdapter.Factory> b = new ArrayList();

    public final Builder a()
    {
      this.a = false;
      return this;
    }

    public final Builder a(TypeAdapter.Factory paramFactory)
    {
      this.b.add(paramFactory);
      return this;
    }

    public final <T> Builder a(Class<T> paramClass, TypeAdapter<T> paramTypeAdapter)
    {
      this.b.add(TypeAdapters.a(paramClass, paramTypeAdapter));
      return this;
    }

    public final MiniGson b()
    {
      return new MiniGson(this, (byte)0);
    }
  }

  static class FutureTypeAdapter<T> extends TypeAdapter<T>
  {
    private TypeAdapter<T> a;

    public final T a(JsonReader paramJsonReader)
    {
      if (this.a == null)
        throw new IllegalStateException();
      return this.a.a(paramJsonReader);
    }

    public final void a(TypeAdapter<T> paramTypeAdapter)
    {
      if (this.a != null)
        throw new AssertionError();
      this.a = paramTypeAdapter;
    }

    public final void a(JsonWriter paramJsonWriter, T paramT)
    {
      if (this.a == null)
        throw new IllegalStateException();
      this.a.a(paramJsonWriter, paramT);
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.internal.bind.MiniGson
 * JD-Core Version:    0.6.2
 */