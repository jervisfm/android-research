package com.google.gson.chase.internal.bind;

import com.google.gson.chase.stream.JsonReader;
import com.google.gson.chase.stream.JsonToken;
import com.google.gson.chase.stream.JsonWriter;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public final class ObjectTypeAdapter extends TypeAdapter<Object>
{
  public static final TypeAdapter.Factory a = new ObjectTypeAdapter.1();
  private final MiniGson b;

  private ObjectTypeAdapter(MiniGson paramMiniGson)
  {
    this.b = paramMiniGson;
  }

  public final Object a(JsonReader paramJsonReader)
  {
    JsonToken localJsonToken = paramJsonReader.f();
    switch (2.a[localJsonToken.ordinal()])
    {
    default:
      throw new IllegalStateException();
    case 1:
      ArrayList localArrayList = new ArrayList();
      paramJsonReader.a();
      while (paramJsonReader.e())
        localArrayList.add(a(paramJsonReader));
      paramJsonReader.b();
      return localArrayList;
    case 2:
      LinkedHashMap localLinkedHashMap = new LinkedHashMap();
      paramJsonReader.c();
      while (paramJsonReader.e())
        localLinkedHashMap.put(paramJsonReader.g(), a(paramJsonReader));
      paramJsonReader.d();
      return localLinkedHashMap;
    case 3:
      return paramJsonReader.h();
    case 4:
      return Double.valueOf(paramJsonReader.k());
    case 5:
      return Boolean.valueOf(paramJsonReader.i());
    case 6:
    }
    paramJsonReader.j();
    return null;
  }

  public final void a(JsonWriter paramJsonWriter, Object paramObject)
  {
    if (paramObject == null)
    {
      paramJsonWriter.f();
      return;
    }
    TypeAdapter localTypeAdapter = this.b.a(paramObject.getClass());
    if ((localTypeAdapter instanceof ObjectTypeAdapter))
    {
      paramJsonWriter.d();
      paramJsonWriter.e();
      return;
    }
    localTypeAdapter.a(paramJsonWriter, paramObject);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.internal.bind.ObjectTypeAdapter
 * JD-Core Version:    0.6.2
 */