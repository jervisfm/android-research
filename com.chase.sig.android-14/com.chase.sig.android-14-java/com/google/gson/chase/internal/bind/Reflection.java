package com.google.gson.chase.internal.bind;

import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;

final class Reflection
{
  public static Type a(Type paramType, Object paramObject)
  {
    if ((paramObject != null) && ((paramType == Object.class) || ((paramType instanceof TypeVariable)) || ((paramType instanceof Class))))
      paramType = paramObject.getClass();
    return paramType;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.internal.bind.Reflection
 * JD-Core Version:    0.6.2
 */