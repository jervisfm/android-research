package com.google.gson.chase.internal.bind;

import com.google.gson.chase.reflect.TypeToken;
import com.google.gson.chase.stream.JsonReader;
import com.google.gson.chase.stream.JsonWriter;
import java.lang.reflect.Field;

class ReflectiveTypeAdapterFactory$1 extends ReflectiveTypeAdapterFactory.BoundField
{
  final TypeAdapter<?> a = this.b.a(this.c);

  ReflectiveTypeAdapterFactory$1(ReflectiveTypeAdapterFactory paramReflectiveTypeAdapterFactory, String paramString, boolean paramBoolean1, boolean paramBoolean2, MiniGson paramMiniGson, TypeToken paramTypeToken, Field paramField, boolean paramBoolean3)
  {
    super(paramString, paramBoolean1, paramBoolean2);
  }

  final void a(JsonReader paramJsonReader, Object paramObject)
  {
    Object localObject = this.a.a(paramJsonReader);
    if ((localObject != null) || (!this.e))
      this.d.set(paramObject, localObject);
  }

  final void a(JsonWriter paramJsonWriter, Object paramObject)
  {
    Object localObject = this.d.get(paramObject);
    new TypeAdapterRuntimeTypeWrapper(this.b, this.a, this.c.b()).a(paramJsonWriter, localObject);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.internal.bind.ReflectiveTypeAdapterFactory.1
 * JD-Core Version:    0.6.2
 */