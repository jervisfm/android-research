package com.google.gson.chase.internal.bind;

import com.google.gson.chase.JsonSyntaxException;
import com.google.gson.chase.internal..Gson.Types;
import com.google.gson.chase.internal.ConstructorConstructor;
import com.google.gson.chase.internal.ObjectConstructor;
import com.google.gson.chase.internal.Primitives;
import com.google.gson.chase.reflect.TypeToken;
import com.google.gson.chase.stream.JsonReader;
import com.google.gson.chase.stream.JsonToken;
import com.google.gson.chase.stream.JsonWriter;
import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

public class ReflectiveTypeAdapterFactory
  implements TypeAdapter.Factory
{
  private final ConstructorConstructor a;

  public ReflectiveTypeAdapterFactory(ConstructorConstructor paramConstructorConstructor)
  {
    this.a = paramConstructorConstructor;
  }

  private Map<String, BoundField> a(MiniGson paramMiniGson, TypeToken<?> paramTypeToken, Class<?> paramClass)
  {
    LinkedHashMap localLinkedHashMap = new LinkedHashMap();
    if (paramClass.isInterface())
      return localLinkedHashMap;
    Type localType1 = paramTypeToken.b();
    while (paramClass != Object.class)
    {
      Field[] arrayOfField = paramClass.getDeclaredFields();
      AccessibleObject.setAccessible(arrayOfField, true);
      int i = arrayOfField.length;
      for (int j = 0; j < i; j++)
      {
        Field localField = arrayOfField[j];
        boolean bool1 = b(paramClass, localField);
        boolean bool2 = c(paramClass, localField);
        if ((bool1) || (bool2))
        {
          Type localType4 = paramTypeToken.b();
          Type localType5 = localField.getGenericType();
          Type localType6 = .Gson.Types.a(localType4, paramClass, localType5);
          String str = a(paramClass, localField);
          TypeToken localTypeToken = TypeToken.a(localType6);
          ReflectiveTypeAdapterFactory.1 local1 = new ReflectiveTypeAdapterFactory.1(this, str, bool1, bool2, paramMiniGson, localTypeToken, localField, Primitives.a(localTypeToken.a()));
          BoundField localBoundField = (BoundField)localLinkedHashMap.put(local1.g, local1);
          if (localBoundField != null)
            throw new IllegalArgumentException(localType1 + " declares multiple JSON fields named " + localBoundField.g);
        }
      }
      Type localType2 = paramTypeToken.b();
      Type localType3 = paramClass.getGenericSuperclass();
      paramTypeToken = TypeToken.a(.Gson.Types.a(localType2, paramClass, localType3));
      paramClass = paramTypeToken.a();
    }
    return localLinkedHashMap;
  }

  public final <T> TypeAdapter<T> a(MiniGson paramMiniGson, TypeToken<T> paramTypeToken)
  {
    Class localClass = paramTypeToken.a();
    if (!Object.class.isAssignableFrom(localClass))
      return null;
    return new Adapter(this.a.a(paramTypeToken), a(paramMiniGson, paramTypeToken, localClass), (byte)0);
  }

  protected String a(Class<?> paramClass, Field paramField)
  {
    return paramField.getName();
  }

  protected boolean b(Class<?> paramClass, Field paramField)
  {
    return !paramField.isSynthetic();
  }

  protected boolean c(Class<?> paramClass, Field paramField)
  {
    return !paramField.isSynthetic();
  }

  public final class Adapter<T> extends TypeAdapter<T>
  {
    private final ObjectConstructor<T> b;
    private final Map<String, ReflectiveTypeAdapterFactory.BoundField> c;

    private Adapter(Map<String, ReflectiveTypeAdapterFactory.BoundField> arg2)
    {
      Object localObject1;
      this.b = localObject1;
      Object localObject2;
      this.c = localObject2;
    }

    public final T a(JsonReader paramJsonReader)
    {
      if (paramJsonReader.f() == JsonToken.i)
      {
        paramJsonReader.j();
        return null;
      }
      Object localObject = this.b.a();
      try
      {
        paramJsonReader.c();
        while (true)
        {
          if (!paramJsonReader.e())
            break label111;
          String str = paramJsonReader.g();
          localBoundField = (ReflectiveTypeAdapterFactory.BoundField)this.c.get(str);
          if ((localBoundField != null) && (localBoundField.i))
            break;
          paramJsonReader.n();
        }
      }
      catch (IllegalStateException localIllegalStateException)
      {
        while (true)
        {
          ReflectiveTypeAdapterFactory.BoundField localBoundField;
          throw new JsonSyntaxException(localIllegalStateException);
          localBoundField.a(paramJsonReader, localObject);
        }
      }
      catch (IllegalAccessException localIllegalAccessException)
      {
        throw new AssertionError(localIllegalAccessException);
      }
      label111: paramJsonReader.d();
      return localObject;
    }

    public final void a(JsonWriter paramJsonWriter, T paramT)
    {
      if (paramT == null)
      {
        paramJsonWriter.f();
        return;
      }
      paramJsonWriter.d();
      try
      {
        Iterator localIterator = this.c.values().iterator();
        while (localIterator.hasNext())
        {
          ReflectiveTypeAdapterFactory.BoundField localBoundField = (ReflectiveTypeAdapterFactory.BoundField)localIterator.next();
          if (localBoundField.h)
          {
            paramJsonWriter.a(localBoundField.g);
            localBoundField.a(paramJsonWriter, paramT);
          }
        }
      }
      catch (IllegalAccessException localIllegalAccessException)
      {
        throw new AssertionError();
      }
      paramJsonWriter.e();
    }
  }

  static abstract class BoundField
  {
    final String g;
    final boolean h;
    final boolean i;

    protected BoundField(String paramString, boolean paramBoolean1, boolean paramBoolean2)
    {
      this.g = paramString;
      this.h = paramBoolean1;
      this.i = paramBoolean2;
    }

    abstract void a(JsonReader paramJsonReader, Object paramObject);

    abstract void a(JsonWriter paramJsonWriter, Object paramObject);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.internal.bind.ReflectiveTypeAdapterFactory
 * JD-Core Version:    0.6.2
 */