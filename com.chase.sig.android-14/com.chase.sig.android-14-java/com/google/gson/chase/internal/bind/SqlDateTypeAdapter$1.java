package com.google.gson.chase.internal.bind;

import com.google.gson.chase.reflect.TypeToken;
import java.sql.Date;

class SqlDateTypeAdapter$1
  implements TypeAdapter.Factory
{
  public final <T> TypeAdapter<T> a(MiniGson paramMiniGson, TypeToken<T> paramTypeToken)
  {
    if (paramTypeToken.a() == Date.class)
      return new SqlDateTypeAdapter();
    return null;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.internal.bind.SqlDateTypeAdapter.1
 * JD-Core Version:    0.6.2
 */