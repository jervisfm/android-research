package com.google.gson.chase.internal.bind;

import com.google.gson.chase.JsonSyntaxException;
import com.google.gson.chase.stream.JsonReader;
import com.google.gson.chase.stream.JsonToken;
import com.google.gson.chase.stream.JsonWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public final class SqlDateTypeAdapter extends TypeAdapter<java.sql.Date>
{
  public static final TypeAdapter.Factory a = new SqlDateTypeAdapter.1();
  private final DateFormat b = new SimpleDateFormat("MMM d, yyyy");

  private void a(JsonWriter paramJsonWriter, java.sql.Date paramDate)
  {
    Object localObject2;
    if (paramDate == null)
      localObject2 = null;
    try
    {
      while (true)
      {
        paramJsonWriter.b((String)localObject2);
        return;
        String str = this.b.format(paramDate);
        localObject2 = str;
      }
    }
    finally
    {
    }
  }

  private java.sql.Date b(JsonReader paramJsonReader)
  {
    try
    {
      java.sql.Date localDate;
      if (paramJsonReader.f() == JsonToken.i)
      {
        paramJsonReader.j();
        localDate = null;
      }
      while (true)
      {
        return localDate;
        try
        {
          localDate = new java.sql.Date(this.b.parse(paramJsonReader.h()).getTime());
        }
        catch (ParseException localParseException)
        {
          throw new JsonSyntaxException(localParseException);
        }
      }
    }
    finally
    {
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.internal.bind.SqlDateTypeAdapter
 * JD-Core Version:    0.6.2
 */