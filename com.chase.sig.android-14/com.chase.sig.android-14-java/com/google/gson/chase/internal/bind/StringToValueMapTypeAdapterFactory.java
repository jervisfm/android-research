package com.google.gson.chase.internal.bind;

import com.google.gson.chase.internal..Gson.Types;
import com.google.gson.chase.internal.ConstructorConstructor;
import com.google.gson.chase.internal.ObjectConstructor;
import com.google.gson.chase.reflect.TypeToken;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Map;

public final class StringToValueMapTypeAdapterFactory
  implements TypeAdapter.Factory
{
  private final ConstructorConstructor a;

  public StringToValueMapTypeAdapterFactory(ConstructorConstructor paramConstructorConstructor)
  {
    this.a = paramConstructorConstructor;
  }

  public final <T> TypeAdapter<T> a(MiniGson paramMiniGson, TypeToken<T> paramTypeToken)
  {
    Type localType = paramTypeToken.b();
    if (!(localType instanceof ParameterizedType));
    Type[] arrayOfType;
    do
    {
      Class localClass;
      do
      {
        return null;
        localClass = paramTypeToken.a();
      }
      while (!Map.class.isAssignableFrom(localClass));
      arrayOfType = .Gson.Types.b(localType, localClass);
    }
    while (arrayOfType[0] != String.class);
    return new Adapter(paramMiniGson.a(TypeToken.a(arrayOfType[1])), this.a.a(paramTypeToken));
  }

  private final class Adapter<V> extends TypeAdapter<Map<String, V>>
  {
    private final TypeAdapter<V> b;
    private final ObjectConstructor<? extends Map<String, V>> c;

    public Adapter(ObjectConstructor<? extends Map<String, V>> arg2)
    {
      Object localObject1;
      this.b = localObject1;
      Object localObject2;
      this.c = localObject2;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.internal.bind.StringToValueMapTypeAdapterFactory
 * JD-Core Version:    0.6.2
 */