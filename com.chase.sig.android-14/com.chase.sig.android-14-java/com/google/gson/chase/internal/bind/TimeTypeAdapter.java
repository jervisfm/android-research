package com.google.gson.chase.internal.bind;

import com.google.gson.chase.JsonSyntaxException;
import com.google.gson.chase.stream.JsonReader;
import com.google.gson.chase.stream.JsonToken;
import com.google.gson.chase.stream.JsonWriter;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class TimeTypeAdapter extends TypeAdapter<Time>
{
  public static final TypeAdapter.Factory a = new TimeTypeAdapter.1();
  private final DateFormat b = new SimpleDateFormat("hh:mm:ss a");

  private void a(JsonWriter paramJsonWriter, Time paramTime)
  {
    Object localObject2;
    if (paramTime == null)
      localObject2 = null;
    try
    {
      while (true)
      {
        paramJsonWriter.b((String)localObject2);
        return;
        String str = this.b.format(paramTime);
        localObject2 = str;
      }
    }
    finally
    {
    }
  }

  private Time b(JsonReader paramJsonReader)
  {
    try
    {
      Time localTime;
      if (paramJsonReader.f() == JsonToken.i)
      {
        paramJsonReader.j();
        localTime = null;
      }
      while (true)
      {
        return localTime;
        try
        {
          localTime = new Time(this.b.parse(paramJsonReader.h()).getTime());
        }
        catch (ParseException localParseException)
        {
          throw new JsonSyntaxException(localParseException);
        }
      }
    }
    finally
    {
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.internal.bind.TimeTypeAdapter
 * JD-Core Version:    0.6.2
 */