package com.google.gson.chase.internal.bind;

import com.google.gson.chase.JsonElement;
import com.google.gson.chase.JsonIOException;
import com.google.gson.chase.reflect.TypeToken;
import com.google.gson.chase.stream.JsonReader;
import com.google.gson.chase.stream.JsonWriter;
import java.io.IOException;

public abstract class TypeAdapter<T>
{
  public final JsonElement a(T paramT)
  {
    try
    {
      JsonElementWriter localJsonElementWriter = new JsonElementWriter();
      localJsonElementWriter.b(true);
      a(localJsonElementWriter, paramT);
      JsonElement localJsonElement = localJsonElementWriter.a();
      return localJsonElement;
    }
    catch (IOException localIOException)
    {
      throw new JsonIOException(localIOException);
    }
  }

  public final T a(JsonElement paramJsonElement)
  {
    try
    {
      JsonElementReader localJsonElementReader = new JsonElementReader(paramJsonElement);
      localJsonElementReader.a(true);
      Object localObject = a(localJsonElementReader);
      return localObject;
    }
    catch (IOException localIOException)
    {
      throw new JsonIOException(localIOException);
    }
  }

  public abstract T a(JsonReader paramJsonReader);

  public abstract void a(JsonWriter paramJsonWriter, T paramT);

  public static abstract interface Factory
  {
    public abstract <T> TypeAdapter<T> a(MiniGson paramMiniGson, TypeToken<T> paramTypeToken);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.internal.bind.TypeAdapter
 * JD-Core Version:    0.6.2
 */