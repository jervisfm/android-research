package com.google.gson.chase.internal.bind;

import com.google.gson.chase.reflect.TypeToken;
import com.google.gson.chase.stream.JsonReader;
import com.google.gson.chase.stream.JsonWriter;
import java.lang.reflect.Type;

final class TypeAdapterRuntimeTypeWrapper<T> extends TypeAdapter<T>
{
  private final MiniGson a;
  private final TypeAdapter<T> b;
  private final Type c;

  TypeAdapterRuntimeTypeWrapper(MiniGson paramMiniGson, TypeAdapter<T> paramTypeAdapter, Type paramType)
  {
    this.a = paramMiniGson;
    this.b = paramTypeAdapter;
    this.c = paramType;
  }

  public final T a(JsonReader paramJsonReader)
  {
    return this.b.a(paramJsonReader);
  }

  public final void a(JsonWriter paramJsonWriter, T paramT)
  {
    TypeAdapter localTypeAdapter = this.b;
    Type localType = Reflection.a(this.c, paramT);
    if (localType != this.c)
    {
      localTypeAdapter = this.a.a(TypeToken.a(localType));
      if (((localTypeAdapter instanceof ReflectiveTypeAdapterFactory.Adapter)) && (!(this.b instanceof ReflectiveTypeAdapterFactory.Adapter)))
        localTypeAdapter = this.b;
    }
    localTypeAdapter.a(paramJsonWriter, paramT);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.internal.bind.TypeAdapterRuntimeTypeWrapper
 * JD-Core Version:    0.6.2
 */