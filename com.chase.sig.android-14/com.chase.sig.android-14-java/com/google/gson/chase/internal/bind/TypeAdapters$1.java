package com.google.gson.chase.internal.bind;

import com.google.gson.chase.JsonSyntaxException;
import com.google.gson.chase.stream.JsonReader;
import com.google.gson.chase.stream.JsonToken;
import java.util.BitSet;

class TypeAdapters$1 extends TypeAdapter<BitSet>
{
  private static BitSet b(JsonReader paramJsonReader)
  {
    if (paramJsonReader.f() == JsonToken.i)
    {
      paramJsonReader.j();
      return null;
    }
    BitSet localBitSet = new BitSet();
    paramJsonReader.a();
    JsonToken localJsonToken = paramJsonReader.f();
    int i = 0;
    if (localJsonToken != JsonToken.b)
    {
      boolean bool;
      switch (TypeAdapters.29.a[localJsonToken.ordinal()])
      {
      default:
        throw new JsonSyntaxException("Invalid bitset value type: " + localJsonToken);
      case 1:
        if (paramJsonReader.m() != 0)
          bool = true;
        break;
      case 2:
      case 3:
      }
      while (true)
      {
        if (bool)
          localBitSet.set(i);
        i++;
        localJsonToken = paramJsonReader.f();
        break;
        bool = false;
        continue;
        bool = paramJsonReader.i();
        continue;
        String str = paramJsonReader.h();
        try
        {
          int j = Integer.parseInt(str);
          if (j != 0)
            bool = true;
          else
            bool = false;
        }
        catch (NumberFormatException localNumberFormatException)
        {
          throw new JsonSyntaxException("Error: Expecting: bitset number value (1, 0), Found: " + str);
        }
      }
    }
    paramJsonReader.b();
    return localBitSet;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.internal.bind.TypeAdapters.1
 * JD-Core Version:    0.6.2
 */