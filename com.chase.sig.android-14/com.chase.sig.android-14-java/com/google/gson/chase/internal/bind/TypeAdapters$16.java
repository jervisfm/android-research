package com.google.gson.chase.internal.bind;

import com.google.gson.chase.JsonIOException;
import com.google.gson.chase.stream.JsonReader;
import com.google.gson.chase.stream.JsonToken;
import java.net.URI;
import java.net.URISyntaxException;

class TypeAdapters$16 extends TypeAdapter<URI>
{
  private static URI b(JsonReader paramJsonReader)
  {
    if (paramJsonReader.f() == JsonToken.i)
      paramJsonReader.j();
    while (true)
    {
      return null;
      try
      {
        String str = paramJsonReader.h();
        if ("null".equals(str))
          continue;
        URI localURI = new URI(str);
        return localURI;
      }
      catch (URISyntaxException localURISyntaxException)
      {
        throw new JsonIOException(localURISyntaxException);
      }
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.internal.bind.TypeAdapters.16
 * JD-Core Version:    0.6.2
 */