package com.google.gson.chase.internal.bind;

import com.google.gson.chase.reflect.TypeToken;
import java.sql.Timestamp;
import java.util.Date;

class TypeAdapters$19
  implements TypeAdapter.Factory
{
  public final <T> TypeAdapter<T> a(MiniGson paramMiniGson, TypeToken<T> paramTypeToken)
  {
    if (paramTypeToken.a() != Timestamp.class)
      return null;
    return new TypeAdapters.19.1(this, paramMiniGson.a(Date.class));
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.internal.bind.TypeAdapters.19
 * JD-Core Version:    0.6.2
 */