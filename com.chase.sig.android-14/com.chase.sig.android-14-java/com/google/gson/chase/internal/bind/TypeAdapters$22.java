package com.google.gson.chase.internal.bind;

import com.google.gson.chase.JsonArray;
import com.google.gson.chase.JsonElement;
import com.google.gson.chase.JsonNull;
import com.google.gson.chase.JsonObject;
import com.google.gson.chase.JsonPrimitive;
import com.google.gson.chase.internal.LazilyParsedNumber;
import com.google.gson.chase.stream.JsonReader;
import com.google.gson.chase.stream.JsonWriter;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

class TypeAdapters$22 extends TypeAdapter<JsonElement>
{
  private void a(JsonWriter paramJsonWriter, JsonElement paramJsonElement)
  {
    if ((paramJsonElement == null) || (paramJsonElement.j()))
    {
      paramJsonWriter.f();
      return;
    }
    if (paramJsonElement.i())
    {
      JsonPrimitive localJsonPrimitive = paramJsonElement.m();
      if (localJsonPrimitive.o())
      {
        paramJsonWriter.a(localJsonPrimitive.a());
        return;
      }
      if (localJsonPrimitive.n())
      {
        paramJsonWriter.a(localJsonPrimitive.f());
        return;
      }
      paramJsonWriter.b(localJsonPrimitive.b());
      return;
    }
    if (paramJsonElement.g())
    {
      paramJsonWriter.b();
      Iterator localIterator2 = paramJsonElement.l().iterator();
      while (localIterator2.hasNext())
        a(paramJsonWriter, (JsonElement)localIterator2.next());
      paramJsonWriter.c();
      return;
    }
    if (paramJsonElement.h())
    {
      paramJsonWriter.d();
      Iterator localIterator1 = paramJsonElement.k().n().iterator();
      while (localIterator1.hasNext())
      {
        Map.Entry localEntry = (Map.Entry)localIterator1.next();
        paramJsonWriter.a((String)localEntry.getKey());
        a(paramJsonWriter, (JsonElement)localEntry.getValue());
      }
      paramJsonWriter.e();
      return;
    }
    throw new IllegalArgumentException("Couldn't write " + paramJsonElement.getClass());
  }

  private JsonElement b(JsonReader paramJsonReader)
  {
    switch (TypeAdapters.29.a[paramJsonReader.f().ordinal()])
    {
    default:
      throw new IllegalArgumentException();
    case 3:
      return new JsonPrimitive(paramJsonReader.h());
    case 1:
      return new JsonPrimitive(new LazilyParsedNumber(paramJsonReader.h()));
    case 2:
      return new JsonPrimitive(Boolean.valueOf(paramJsonReader.i()));
    case 4:
      paramJsonReader.j();
      return JsonNull.a;
    case 5:
      JsonArray localJsonArray = new JsonArray();
      paramJsonReader.a();
      while (paramJsonReader.e())
        localJsonArray.a(b(paramJsonReader));
      paramJsonReader.b();
      return localJsonArray;
    case 6:
    }
    JsonObject localJsonObject = new JsonObject();
    paramJsonReader.c();
    while (paramJsonReader.e())
      localJsonObject.a(paramJsonReader.g(), b(paramJsonReader));
    paramJsonReader.d();
    return localJsonObject;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.internal.bind.TypeAdapters.22
 * JD-Core Version:    0.6.2
 */