package com.google.gson.chase.internal.bind;

import com.google.gson.chase.reflect.TypeToken;

class TypeAdapters$23
  implements TypeAdapter.Factory
{
  TypeAdapters$23(Class paramClass)
  {
  }

  public final <T> TypeAdapter<T> a(MiniGson paramMiniGson, TypeToken<T> paramTypeToken)
  {
    Class localClass = paramTypeToken.a();
    if (this.a.isAssignableFrom(localClass))
      return new TypeAdapters.EnumTypeAdapter(localClass);
    return null;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.internal.bind.TypeAdapters.23
 * JD-Core Version:    0.6.2
 */