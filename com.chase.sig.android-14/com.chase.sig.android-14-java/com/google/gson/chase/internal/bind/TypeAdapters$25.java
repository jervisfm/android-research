package com.google.gson.chase.internal.bind;

import com.google.gson.chase.reflect.TypeToken;

class TypeAdapters$25
  implements TypeAdapter.Factory
{
  TypeAdapters$25(Class paramClass, TypeAdapter paramTypeAdapter)
  {
  }

  public final <T> TypeAdapter<T> a(MiniGson paramMiniGson, TypeToken<T> paramTypeToken)
  {
    if (paramTypeToken.a() == this.a)
      return this.b;
    return null;
  }

  public String toString()
  {
    return "Factory[type=" + this.a.getName() + ",adapter=" + this.b + "]";
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.internal.bind.TypeAdapters.25
 * JD-Core Version:    0.6.2
 */