package com.google.gson.chase.internal.bind;

import com.google.gson.chase.reflect.TypeToken;

class TypeAdapters$27
  implements TypeAdapter.Factory
{
  TypeAdapters$27(Class paramClass1, Class paramClass2, TypeAdapter paramTypeAdapter)
  {
  }

  public final <T> TypeAdapter<T> a(MiniGson paramMiniGson, TypeToken<T> paramTypeToken)
  {
    Class localClass = paramTypeToken.a();
    if ((localClass == this.a) || (localClass == this.b))
      return this.c;
    return null;
  }

  public String toString()
  {
    return "Factory[type=" + this.a.getName() + "+" + this.b.getName() + ",adapter=" + this.c + "]";
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.internal.bind.TypeAdapters.27
 * JD-Core Version:    0.6.2
 */