package com.google.gson.chase.internal.bind;

import com.google.gson.chase.reflect.TypeToken;

class TypeAdapters$28
  implements TypeAdapter.Factory
{
  TypeAdapters$28(Class paramClass, TypeAdapter paramTypeAdapter)
  {
  }

  public final <T> TypeAdapter<T> a(MiniGson paramMiniGson, TypeToken<T> paramTypeToken)
  {
    if (this.a.isAssignableFrom(paramTypeToken.a()))
      return this.b;
    return null;
  }

  public String toString()
  {
    return "Factory[typeHierarchy=" + this.a.getName() + ",adapter=" + this.b + "]";
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.internal.bind.TypeAdapters.28
 * JD-Core Version:    0.6.2
 */