package com.google.gson.chase.internal.bind;

import com.google.gson.chase.JsonSyntaxException;
import com.google.gson.chase.stream.JsonReader;
import com.google.gson.chase.stream.JsonToken;

class TypeAdapters$5 extends TypeAdapter<Number>
{
  private static Number b(JsonReader paramJsonReader)
  {
    if (paramJsonReader.f() == JsonToken.i)
    {
      paramJsonReader.j();
      return null;
    }
    try
    {
      Short localShort = Short.valueOf((short)paramJsonReader.m());
      return localShort;
    }
    catch (NumberFormatException localNumberFormatException)
    {
      throw new JsonSyntaxException(localNumberFormatException);
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.internal.bind.TypeAdapters.5
 * JD-Core Version:    0.6.2
 */