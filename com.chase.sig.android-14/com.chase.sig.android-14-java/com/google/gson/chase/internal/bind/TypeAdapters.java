package com.google.gson.chase.internal.bind;

import com.google.gson.chase.JsonElement;
import java.net.InetAddress;
import java.net.URI;
import java.net.URL;
import java.util.BitSet;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.UUID;

public final class TypeAdapters
{
  public static final TypeAdapter.Factory A = a(StringBuffer.class, z);
  public static final TypeAdapter<URL> B = new TypeAdapters.15();
  public static final TypeAdapter.Factory C = a(URL.class, B);
  public static final TypeAdapter<URI> D = new TypeAdapters.16();
  public static final TypeAdapter.Factory E = a(URI.class, D);
  public static final TypeAdapter<InetAddress> F = new TypeAdapters.17();
  public static final TypeAdapter.Factory G = new TypeAdapters.28(InetAddress.class, F);
  public static final TypeAdapter<UUID> H = new TypeAdapters.18();
  public static final TypeAdapter.Factory I = a(UUID.class, H);
  public static final TypeAdapter.Factory J = new TypeAdapters.19();
  public static final TypeAdapter<Calendar> K = new TypeAdapters.20();
  public static final TypeAdapter.Factory L = new TypeAdapters.27(Calendar.class, GregorianCalendar.class, K);
  public static final TypeAdapter<Locale> M = new TypeAdapters.21();
  public static final TypeAdapter.Factory N = a(Locale.class, M);
  public static final TypeAdapter<JsonElement> O = new TypeAdapters.22();
  public static final TypeAdapter.Factory P = a(JsonElement.class, O);
  public static final TypeAdapter.Factory Q = new TypeAdapters.23(Enum.class);
  public static final TypeAdapter<BitSet> a = new TypeAdapters.1();
  public static final TypeAdapter.Factory b = a(BitSet.class, a);
  public static final TypeAdapter<Boolean> c = new TypeAdapters.2();
  public static final TypeAdapter<Boolean> d = new TypeAdapters.3();
  public static final TypeAdapter.Factory e = a(Boolean.TYPE, Boolean.class, c);
  public static final TypeAdapter<Number> f = new TypeAdapters.4();
  public static final TypeAdapter.Factory g = a(Byte.TYPE, Byte.class, f);
  public static final TypeAdapter<Number> h = new TypeAdapters.5();
  public static final TypeAdapter.Factory i = a(Short.TYPE, Short.class, h);
  public static final TypeAdapter<Number> j = new TypeAdapters.6();
  public static final TypeAdapter.Factory k = a(Integer.TYPE, Integer.class, j);
  public static final TypeAdapter<Number> l = new TypeAdapters.7();
  public static final TypeAdapter.Factory m = a(Long.TYPE, Long.class, l);
  public static final TypeAdapter<Number> n = new TypeAdapters.8();
  public static final TypeAdapter.Factory o = a(Float.TYPE, Float.class, n);
  public static final TypeAdapter<Number> p = new TypeAdapters.9();
  public static final TypeAdapter.Factory q = a(Double.TYPE, Double.class, p);
  public static final TypeAdapter<Number> r = new TypeAdapters.10();
  public static final TypeAdapter.Factory s = a(Number.class, r);
  public static final TypeAdapter<Character> t = new TypeAdapters.11();
  public static final TypeAdapter.Factory u = a(Character.TYPE, Character.class, t);
  public static final TypeAdapter<String> v = new TypeAdapters.12();
  public static final TypeAdapter.Factory w = a(String.class, v);
  public static final TypeAdapter<StringBuilder> x = new TypeAdapters.13();
  public static final TypeAdapter.Factory y = a(StringBuilder.class, x);
  public static final TypeAdapter<StringBuffer> z = new TypeAdapters.14();

  public static <TT> TypeAdapter.Factory a(Class<TT> paramClass, TypeAdapter<TT> paramTypeAdapter)
  {
    return new TypeAdapters.25(paramClass, paramTypeAdapter);
  }

  public static <TT> TypeAdapter.Factory a(Class<TT> paramClass1, Class<TT> paramClass2, TypeAdapter<? super TT> paramTypeAdapter)
  {
    return new TypeAdapters.26(paramClass1, paramClass2, paramTypeAdapter);
  }

  private static final class EnumTypeAdapter<T extends Enum<T>> extends TypeAdapter<T>
  {
    private final Class<T> a;

    public EnumTypeAdapter(Class<T> paramClass)
    {
      this.a = paramClass;
    }
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.internal.bind.TypeAdapters
 * JD-Core Version:    0.6.2
 */