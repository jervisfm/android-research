package com.google.gson.chase.reflect;

import com.google.gson.chase.internal..Gson.Preconditions;
import com.google.gson.chase.internal..Gson.Types;
import java.lang.reflect.Type;

public class TypeToken<T>
{
  final Class<? super T> b;
  final Type c;
  final int d;

  public TypeToken()
  {
    Type localType = getClass().getGenericSuperclass();
    if ((localType instanceof Class))
      throw new RuntimeException("Missing type parameter.");
    this.c = .Gson.Types.a(((java.lang.reflect.ParameterizedType)localType).getActualTypeArguments()[0]);
    this.b = .Gson.Types.b(this.c);
    this.d = this.c.hashCode();
  }

  private TypeToken(Type paramType)
  {
    this.c = .Gson.Types.a((Type).Gson.Preconditions.a(paramType));
    this.b = .Gson.Types.b(this.c);
    this.d = this.c.hashCode();
  }

  public static <T> TypeToken<T> a(Class<T> paramClass)
  {
    return new TypeToken(paramClass);
  }

  public static TypeToken<?> a(Type paramType)
  {
    return new TypeToken(paramType);
  }

  public final Class<? super T> a()
  {
    return this.b;
  }

  public final Type b()
  {
    return this.c;
  }

  public final boolean equals(Object paramObject)
  {
    return ((paramObject instanceof TypeToken)) && (.Gson.Types.a(this.c, ((TypeToken)paramObject).c));
  }

  public final int hashCode()
  {
    return this.d;
  }

  public final String toString()
  {
    return .Gson.Types.c(this.c);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.reflect.TypeToken
 * JD-Core Version:    0.6.2
 */