package com.google.gson.chase.stream;

import java.io.Closeable;
import java.io.EOFException;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

public class JsonReader
  implements Closeable
{
  private static final char[] b = ")]}'\n".toCharArray();
  public boolean a = false;
  private final StringPool c = new StringPool();
  private final Reader d;
  private final char[] e = new char[1024];
  private int f = 0;
  private int g = 0;
  private int h = 1;
  private int i = 1;
  private final List<JsonScope> j = new ArrayList();
  private boolean k;
  private JsonToken l;
  private String m;
  private String n;
  private boolean o;

  public JsonReader(Reader paramReader)
  {
    a(JsonScope.f);
    this.o = false;
    if (paramReader == null)
      throw new NullPointerException("in == null");
    this.d = paramReader;
  }

  private IOException a(String paramString)
  {
    throw new MalformedJsonException(paramString + " at line " + t() + " column " + u());
  }

  private String a(char paramChar)
  {
    Object localObject1 = null;
    int i1 = this.f;
    label7: char c2;
    label260: int i7;
    Object localObject2;
    if (this.f < this.g)
    {
      char[] arrayOfChar1 = this.e;
      int i2 = this.f;
      this.f = (i2 + 1);
      char c1 = arrayOfChar1[i2];
      if (c1 == paramChar)
      {
        if (this.o)
          return "skipped!";
        if (localObject1 == null)
          return this.c.a(this.e, i1, -1 + (this.f - i1));
        ((StringBuilder)localObject1).append(this.e, i1, -1 + (this.f - i1));
        return ((StringBuilder)localObject1).toString();
      }
      if (c1 != '\\')
        break label440;
      if (localObject1 == null)
        localObject1 = new StringBuilder();
      ((StringBuilder)localObject1).append(this.e, i1, -1 + (this.f - i1));
      if ((this.f == this.g) && (!a(1)))
        throw a("Unterminated escape sequence");
      char[] arrayOfChar2 = this.e;
      int i6 = this.f;
      this.f = (i6 + 1);
      c2 = arrayOfChar2[i6];
      switch (c2)
      {
      default:
        ((StringBuilder)localObject1).append(c2);
        i7 = this.f;
        localObject2 = localObject1;
      case 'u':
      case 't':
      case 'b':
      case 'n':
      case 'r':
      case 'f':
      }
    }
    label440: int i3;
    for (int i4 = i7; ; i4 = i3)
    {
      int i5 = i4;
      localObject1 = localObject2;
      i1 = i5;
      break label7;
      if ((4 + this.f > this.g) && (!a(4)))
        throw a("Unterminated escape sequence");
      String str = this.c.a(this.e, this.f, 4);
      this.f = (4 + this.f);
      c2 = (char)Integer.parseInt(str, 16);
      break label260;
      c2 = '\t';
      break label260;
      c2 = '\b';
      break label260;
      c2 = '\n';
      break label260;
      c2 = '\r';
      break label260;
      c2 = '\f';
      break label260;
      if (localObject1 == null)
        localObject1 = new StringBuilder();
      ((StringBuilder)localObject1).append(this.e, i1, this.f - i1);
      if (a(1))
        break;
      throw a("Unterminated string");
      i3 = i1;
      localObject2 = localObject1;
    }
  }

  private void a(JsonScope paramJsonScope)
  {
    this.j.add(paramJsonScope);
  }

  private void a(JsonToken paramJsonToken)
  {
    p();
    if (this.l != paramJsonToken)
      throw new IllegalStateException("Expected " + paramJsonToken + " but was " + f());
    q();
  }

  private boolean a(int paramInt)
  {
    int i1 = 0;
    if (i1 < this.f)
    {
      if (this.e[i1] == '\n')
        this.h = (1 + this.h);
      for (this.i = 1; ; this.i = (1 + this.i))
      {
        i1++;
        break;
      }
    }
    if (this.g != this.f)
    {
      this.g -= this.f;
      System.arraycopy(this.e, this.f, this.e, 0, this.g);
    }
    while (true)
    {
      this.f = 0;
      do
      {
        int i2 = this.d.read(this.e, this.g, this.e.length - this.g);
        bool = false;
        if (i2 == -1)
          break;
        this.g = (i2 + this.g);
        if ((this.h == 1) && (this.i == 1) && (this.g > 0) && (this.e[0] == 65279))
        {
          this.f = (1 + this.f);
          this.i = (-1 + this.i);
        }
      }
      while (this.g < paramInt);
      boolean bool = true;
      return bool;
      this.g = 0;
    }
  }

  private JsonToken b(boolean paramBoolean)
  {
    if (paramBoolean)
      b(JsonScope.b);
    while (true)
      switch (v())
      {
      default:
        this.f = (-1 + this.f);
        return s();
        switch (v())
        {
        case 44:
        default:
          throw a("Unterminated array");
        case 93:
          r();
          this.k = true;
          JsonToken localJsonToken3 = JsonToken.b;
          this.l = localJsonToken3;
          return localJsonToken3;
        case 59:
        }
        w();
      case 93:
      case 44:
      case 59:
      }
    if (paramBoolean)
    {
      r();
      this.k = true;
      JsonToken localJsonToken2 = JsonToken.b;
      this.l = localJsonToken2;
      return localJsonToken2;
    }
    w();
    this.f = (-1 + this.f);
    this.k = true;
    this.n = "null";
    JsonToken localJsonToken1 = JsonToken.i;
    this.l = localJsonToken1;
    return localJsonToken1;
  }

  private void b(JsonScope paramJsonScope)
  {
    this.j.set(-1 + this.j.size(), paramJsonScope);
  }

  private JsonToken c(boolean paramBoolean)
  {
    int i1;
    if (paramBoolean)
    {
      switch (v())
      {
      default:
        this.f = (-1 + this.f);
        i1 = v();
        switch (i1)
        {
        default:
          w();
          this.f = (-1 + this.f);
          this.m = y();
          if (this.m.length() != 0)
            break label215;
          throw a("Expected name");
        case 39:
        case 34:
        }
      case 125:
        r();
        this.k = true;
        JsonToken localJsonToken3 = JsonToken.d;
        this.l = localJsonToken3;
        return localJsonToken3;
      }
    }
    else
    {
      switch (v())
      {
      case 44:
      case 59:
      default:
        throw a("Unterminated object");
      case 125:
      }
      r();
      this.k = true;
      JsonToken localJsonToken1 = JsonToken.d;
      this.l = localJsonToken1;
      return localJsonToken1;
      w();
      this.m = a((char)i1);
    }
    label215: b(JsonScope.d);
    this.k = true;
    JsonToken localJsonToken2 = JsonToken.e;
    this.l = localJsonToken2;
    return localJsonToken2;
  }

  private JsonToken p()
  {
    JsonToken localJsonToken2;
    if (this.k)
      localJsonToken2 = this.l;
    while (true)
    {
      return localJsonToken2;
      switch (1.a[((JsonScope)this.j.get(-1 + this.j.size())).ordinal()])
      {
      default:
        throw new AssertionError();
      case 1:
        if (this.a)
        {
          v();
          this.f = (-1 + this.f);
          if ((this.f + b.length <= this.g) || (a(b.length)))
          {
            for (int i1 = 0; i1 < b.length; i1++)
              if (this.e[(i1 + this.f)] != b[i1])
                break label201;
            this.f += b.length;
          }
        }
        b(JsonScope.g);
        localJsonToken2 = s();
        if ((!this.a) && (localJsonToken2 != JsonToken.a) && (localJsonToken2 != JsonToken.c))
        {
          a("Expected JSON document to start with '[' or '{'");
          return localJsonToken2;
        }
        break;
      case 2:
        return b(true);
      case 3:
        return b(false);
      case 4:
        return c(true);
      case 5:
        switch (v())
        {
        case 59:
        case 60:
        default:
          throw a("Expected ':'");
        case 61:
          w();
          if (((this.f < this.g) || (a(1))) && (this.e[this.f] == '>'))
            this.f = (1 + this.f);
          break;
        case 58:
        }
        b(JsonScope.e);
        return s();
      case 6:
        return c(false);
      case 7:
        try
        {
          label201: localJsonToken2 = s();
          if (!this.a)
            throw a("Expected EOF");
        }
        catch (EOFException localEOFException)
        {
          this.k = true;
          JsonToken localJsonToken1 = JsonToken.j;
          this.l = localJsonToken1;
          return localJsonToken1;
        }
      case 8:
      }
    }
    throw new IllegalStateException("JsonReader is closed");
  }

  private JsonToken q()
  {
    p();
    JsonToken localJsonToken = this.l;
    this.k = false;
    this.l = null;
    this.n = null;
    this.m = null;
    return localJsonToken;
  }

  private JsonScope r()
  {
    return (JsonScope)this.j.remove(-1 + this.j.size());
  }

  private JsonToken s()
  {
    int i1 = v();
    String str;
    switch (i1)
    {
    default:
      this.f = (-1 + this.f);
      str = y();
      if (str.length() == 0)
        throw a("Expected literal value");
      break;
    case 123:
      a(JsonScope.c);
      this.k = true;
      JsonToken localJsonToken3 = JsonToken.c;
      this.l = localJsonToken3;
      return localJsonToken3;
    case 91:
      a(JsonScope.a);
      this.k = true;
      JsonToken localJsonToken2 = JsonToken.a;
      this.l = localJsonToken2;
      return localJsonToken2;
    case 39:
      w();
    case 34:
      this.n = a((char)i1);
      this.k = true;
      JsonToken localJsonToken1 = JsonToken.f;
      this.l = localJsonToken1;
      return localJsonToken1;
    }
    this.n = str;
    this.k = true;
    this.l = null;
    return null;
  }

  private int t()
  {
    int i1 = this.h;
    for (int i2 = 0; i2 < this.f; i2++)
      if (this.e[i2] == '\n')
        i1++;
    return i1;
  }

  private int u()
  {
    int i1 = this.i;
    int i2 = 0;
    if (i2 < this.f)
    {
      if (this.e[i2] == '\n');
      for (i1 = 1; ; i1++)
      {
        i2++;
        break;
      }
    }
    return i1;
  }

  private int v()
  {
    while ((this.f < this.g) || (a(1)))
    {
      char[] arrayOfChar = this.e;
      int i1 = this.f;
      this.f = (i1 + 1);
      int i2 = arrayOfChar[i1];
      switch (i2)
      {
      case 9:
      case 10:
      case 13:
      case 32:
      default:
      case 47:
        do
          return i2;
        while ((this.f == this.g) && (!a(1)));
        w();
        switch (this.e[this.f])
        {
        default:
          return i2;
        case '*':
          this.f = (1 + this.f);
          if ((this.f + "*/".length() <= this.g) || (a("*/".length())))
            for (int i3 = 0; i3 < "*/".length(); i3++)
              if (this.e[(i3 + this.f)] != "*/".charAt(i3))
                break label262;
          for (int i4 = 1; ; i4 = 0)
          {
            if (i4 != 0)
              break label281;
            throw a("Unterminated comment");
            label262: this.f = (1 + this.f);
            break;
          }
          label281: this.f = (2 + this.f);
          break;
        case '/':
        }
        this.f = (1 + this.f);
        x();
        break;
      case 35:
      }
      w();
      x();
    }
    throw new EOFException("End of input");
  }

  private void w()
  {
    if (!this.a)
      throw a("Use JsonReader.setLenient(true) to accept malformed JSON");
  }

  private void x()
  {
    int i2;
    do
    {
      if ((this.f >= this.g) && (!a(1)))
        break;
      char[] arrayOfChar = this.e;
      int i1 = this.f;
      this.f = (i1 + 1);
      i2 = arrayOfChar[i1];
    }
    while ((i2 != 13) && (i2 != 10));
  }

  private String y()
  {
    StringBuilder localStringBuilder = null;
    do
    {
      int i1 = this.f;
      while (this.f < this.g)
      {
        char[] arrayOfChar = this.e;
        int i2 = this.f;
        this.f = (i2 + 1);
        switch (arrayOfChar[i2])
        {
        default:
          break;
        case '\t':
        case '\n':
        case '\f':
        case '\r':
        case ' ':
        case ',':
        case ':':
        case '[':
        case ']':
        case '{':
        case '}':
        case '#':
        case '/':
        case ';':
        case '=':
        case '\\':
          while (true)
          {
            this.f = (-1 + this.f);
            if (!this.o)
              break;
            return "skipped!";
            w();
          }
          if (localStringBuilder == null)
            return this.c.a(this.e, i1, this.f - i1);
          localStringBuilder.append(this.e, i1, this.f - i1);
          return localStringBuilder.toString();
        }
      }
      if (localStringBuilder == null)
        localStringBuilder = new StringBuilder();
      localStringBuilder.append(this.e, i1, this.f - i1);
    }
    while (a(1));
    return localStringBuilder.toString();
  }

  public void a()
  {
    a(JsonToken.a);
  }

  public final void a(boolean paramBoolean)
  {
    this.a = paramBoolean;
  }

  public void b()
  {
    a(JsonToken.b);
  }

  public void c()
  {
    a(JsonToken.c);
  }

  public void close()
  {
    this.k = false;
    this.n = null;
    this.l = null;
    this.j.clear();
    this.j.add(JsonScope.h);
    this.d.close();
  }

  public void d()
  {
    a(JsonToken.d);
  }

  public boolean e()
  {
    p();
    return (this.l != JsonToken.d) && (this.l != JsonToken.b);
  }

  public JsonToken f()
  {
    p();
    if (this.l == null)
    {
      if (!this.n.equalsIgnoreCase("null"))
        break label36;
      this.l = JsonToken.i;
    }
    while (true)
    {
      return this.l;
      label36: if ((this.n.equalsIgnoreCase("true")) || (this.n.equalsIgnoreCase("false")))
        this.l = JsonToken.h;
      else
        try
        {
          Double.parseDouble(this.n);
          this.l = JsonToken.g;
        }
        catch (NumberFormatException localNumberFormatException)
        {
          w();
          this.l = JsonToken.f;
        }
    }
  }

  public String g()
  {
    p();
    if (this.l != JsonToken.e)
      throw new IllegalStateException("Expected a name but was " + f());
    String str = this.m;
    q();
    return str;
  }

  public String h()
  {
    f();
    if ((this.n == null) || ((this.l != JsonToken.f) && (this.l != JsonToken.g)))
      throw new IllegalStateException("Expected a string but was " + f());
    String str = this.n;
    q();
    return str;
  }

  public boolean i()
  {
    p();
    if ((this.n == null) || (this.l == JsonToken.f))
      throw new IllegalStateException("Expected a boolean but was " + f());
    if (this.n.equalsIgnoreCase("true"));
    for (boolean bool = true; ; bool = false)
    {
      q();
      return bool;
      if (!this.n.equalsIgnoreCase("false"))
        break;
    }
    throw new IllegalStateException("Not a boolean: " + this.n);
  }

  public void j()
  {
    p();
    if ((this.n == null) || (this.l == JsonToken.f))
      throw new IllegalStateException("Expected null but was " + f());
    if (!this.n.equalsIgnoreCase("null"))
      throw new IllegalStateException("Not a null: " + this.n);
    q();
  }

  public double k()
  {
    p();
    if (this.n == null)
      throw new IllegalStateException("Expected a double but was " + f());
    double d1 = Double.parseDouble(this.n);
    if ((d1 >= 1.0D) && (this.n.startsWith("0")))
      throw new NumberFormatException("JSON forbids octal prefixes: " + this.n);
    if ((!this.a) && ((Double.isNaN(d1)) || (Double.isInfinite(d1))))
      throw new NumberFormatException("JSON forbids NaN and infinities: " + this.n);
    q();
    return d1;
  }

  public long l()
  {
    p();
    if (this.n == null)
      throw new IllegalStateException("Expected a long but was " + f());
    long l1;
    try
    {
      long l2 = Long.parseLong(this.n);
      l1 = l2;
      if ((l1 >= 1L) && (this.n.startsWith("0")))
        throw new NumberFormatException("JSON forbids octal prefixes: " + this.n);
    }
    catch (NumberFormatException localNumberFormatException)
    {
      double d1;
      do
      {
        d1 = Double.parseDouble(this.n);
        l1 = ()d1;
      }
      while (l1 == d1);
      throw new NumberFormatException(this.n);
    }
    q();
    return l1;
  }

  public int m()
  {
    p();
    if (this.n == null)
      throw new IllegalStateException("Expected an int but was " + f());
    int i1;
    try
    {
      int i2 = Integer.parseInt(this.n);
      i1 = i2;
      if ((i1 >= 1L) && (this.n.startsWith("0")))
        throw new NumberFormatException("JSON forbids octal prefixes: " + this.n);
    }
    catch (NumberFormatException localNumberFormatException)
    {
      double d1;
      do
      {
        d1 = Double.parseDouble(this.n);
        i1 = (int)d1;
      }
      while (i1 == d1);
      throw new NumberFormatException(this.n);
    }
    q();
    return i1;
  }

  public void n()
  {
    this.o = true;
    int i1 = 0;
    try
    {
      JsonToken localJsonToken1 = q();
      if (localJsonToken1 != JsonToken.a)
      {
        JsonToken localJsonToken2 = JsonToken.c;
        if (localJsonToken1 != localJsonToken2);
      }
      else
      {
        i1++;
      }
      while (i1 == 0)
      {
        return;
        if (localJsonToken1 != JsonToken.b)
        {
          JsonToken localJsonToken3 = JsonToken.d;
          if (localJsonToken1 != localJsonToken3);
        }
        else
        {
          i1--;
        }
      }
    }
    finally
    {
      this.o = false;
    }
  }

  public final boolean o()
  {
    return this.a;
  }

  public String toString()
  {
    StringBuilder localStringBuilder1 = new StringBuilder().append(getClass().getSimpleName()).append(" near ");
    StringBuilder localStringBuilder2 = new StringBuilder();
    int i1 = Math.min(this.f, 20);
    localStringBuilder2.append(this.e, this.f - i1, i1);
    int i2 = Math.min(this.g - this.f, 20);
    localStringBuilder2.append(this.e, this.f, i2);
    return localStringBuilder2;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.stream.JsonReader
 * JD-Core Version:    0.6.2
 */