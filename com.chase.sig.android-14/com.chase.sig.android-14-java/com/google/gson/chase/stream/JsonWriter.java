package com.google.gson.chase.stream;

import java.io.Closeable;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

public class JsonWriter
  implements Closeable
{
  public boolean a;
  public boolean b;
  private final Writer c;
  private final List<JsonScope> d = new ArrayList();
  private String e;
  private String f;
  private boolean g;
  private String h;

  public JsonWriter(Writer paramWriter)
  {
    this.d.add(JsonScope.f);
    this.f = ":";
    this.b = true;
    if (paramWriter == null)
      throw new NullPointerException("out == null");
    this.c = paramWriter;
  }

  private JsonScope a()
  {
    return (JsonScope)this.d.get(-1 + this.d.size());
  }

  private JsonWriter a(JsonScope paramJsonScope1, JsonScope paramJsonScope2, String paramString)
  {
    JsonScope localJsonScope = a();
    if ((localJsonScope != paramJsonScope2) && (localJsonScope != paramJsonScope1))
      throw new IllegalStateException("Nesting problem: " + this.d);
    if (this.h != null)
      throw new IllegalStateException("Dangling name: " + this.h);
    this.d.remove(-1 + this.d.size());
    if (localJsonScope == paramJsonScope2)
      k();
    this.c.write(paramString);
    return this;
  }

  private JsonWriter a(JsonScope paramJsonScope, String paramString)
  {
    e(true);
    this.d.add(paramJsonScope);
    this.c.write(paramString);
    return this;
  }

  private void a(JsonScope paramJsonScope)
  {
    this.d.set(-1 + this.d.size(), paramJsonScope);
  }

  private void d(String paramString)
  {
    this.c.write("\"");
    int i = paramString.length();
    int j = 0;
    if (j < i)
    {
      int k = paramString.charAt(j);
      switch (k)
      {
      default:
        if (k <= 31)
        {
          Writer localWriter3 = this.c;
          Object[] arrayOfObject3 = new Object[1];
          arrayOfObject3[0] = Integer.valueOf(k);
          localWriter3.write(String.format("\\u%04x", arrayOfObject3));
        }
        break;
      case 34:
      case 92:
      case 9:
      case 8:
      case 10:
      case 13:
      case 12:
      case 38:
      case 39:
      case 60:
      case 61:
      case 62:
      case 8232:
      case 8233:
      }
      while (true)
      {
        j++;
        break;
        this.c.write(92);
        this.c.write(k);
        continue;
        this.c.write("\\t");
        continue;
        this.c.write("\\b");
        continue;
        this.c.write("\\n");
        continue;
        this.c.write("\\r");
        continue;
        this.c.write("\\f");
        continue;
        if (this.g)
        {
          Writer localWriter2 = this.c;
          Object[] arrayOfObject2 = new Object[1];
          arrayOfObject2[0] = Integer.valueOf(k);
          localWriter2.write(String.format("\\u%04x", arrayOfObject2));
        }
        else
        {
          this.c.write(k);
          continue;
          Writer localWriter1 = this.c;
          Object[] arrayOfObject1 = new Object[1];
          arrayOfObject1[0] = Integer.valueOf(k);
          localWriter1.write(String.format("\\u%04x", arrayOfObject1));
        }
      }
    }
    this.c.write("\"");
  }

  private void e(boolean paramBoolean)
  {
    switch (1.a[a().ordinal()])
    {
    default:
      throw new IllegalStateException("Nesting problem: " + this.d);
    case 1:
      if ((!this.a) && (!paramBoolean))
        throw new IllegalStateException("JSON must start with an array or an object.");
      a(JsonScope.g);
      return;
    case 2:
      a(JsonScope.b);
      k();
      return;
    case 3:
      this.c.append(',');
      k();
      return;
    case 4:
      this.c.append(this.f);
      a(JsonScope.e);
      return;
    case 5:
    }
    throw new IllegalStateException("JSON must have only one top-level value.");
  }

  private void j()
  {
    JsonScope localJsonScope;
    if (this.h != null)
    {
      localJsonScope = a();
      if (localJsonScope != JsonScope.e)
        break label53;
      this.c.write(44);
    }
    label53: 
    while (localJsonScope == JsonScope.c)
    {
      k();
      a(JsonScope.d);
      d(this.h);
      this.h = null;
      return;
    }
    throw new IllegalStateException("Nesting problem: " + this.d);
  }

  private void k()
  {
    if (this.e == null);
    while (true)
    {
      return;
      this.c.write("\n");
      for (int i = 1; i < this.d.size(); i++)
        this.c.write(this.e);
    }
  }

  public JsonWriter a(long paramLong)
  {
    j();
    e(false);
    this.c.write(Long.toString(paramLong));
    return this;
  }

  public JsonWriter a(Number paramNumber)
  {
    if (paramNumber == null)
      return f();
    j();
    String str = paramNumber.toString();
    if ((!this.a) && ((str.equals("-Infinity")) || (str.equals("Infinity")) || (str.equals("NaN"))))
      throw new IllegalArgumentException("Numeric values must be finite, but was " + paramNumber);
    e(false);
    this.c.append(str);
    return this;
  }

  public JsonWriter a(String paramString)
  {
    if (paramString == null)
      throw new NullPointerException("name == null");
    if (this.h != null)
      throw new IllegalStateException();
    this.h = paramString;
    return this;
  }

  public JsonWriter a(boolean paramBoolean)
  {
    j();
    e(false);
    Writer localWriter = this.c;
    if (paramBoolean);
    for (String str = "true"; ; str = "false")
    {
      localWriter.write(str);
      return this;
    }
  }

  public JsonWriter b()
  {
    j();
    return a(JsonScope.a, "[");
  }

  public JsonWriter b(String paramString)
  {
    if (paramString == null)
      return f();
    j();
    e(false);
    d(paramString);
    return this;
  }

  public final void b(boolean paramBoolean)
  {
    this.a = paramBoolean;
  }

  public JsonWriter c()
  {
    return a(JsonScope.a, JsonScope.b, "]");
  }

  public final void c(String paramString)
  {
    if (paramString.length() == 0)
    {
      this.e = null;
      this.f = ":";
      return;
    }
    this.e = paramString;
    this.f = ": ";
  }

  public final void c(boolean paramBoolean)
  {
    this.g = paramBoolean;
  }

  public void close()
  {
    this.c.close();
    if (a() != JsonScope.g)
      throw new IOException("Incomplete document");
  }

  public JsonWriter d()
  {
    j();
    return a(JsonScope.c, "{");
  }

  public final void d(boolean paramBoolean)
  {
    this.b = paramBoolean;
  }

  public JsonWriter e()
  {
    return a(JsonScope.c, JsonScope.e, "}");
  }

  public JsonWriter f()
  {
    if (this.h != null)
    {
      if (this.b)
        j();
    }
    else
    {
      e(false);
      this.c.write("null");
      return this;
    }
    this.h = null;
    return this;
  }

  public final boolean g()
  {
    return this.a;
  }

  public final boolean h()
  {
    return this.g;
  }

  public final boolean i()
  {
    return this.b;
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.stream.JsonWriter
 * JD-Core Version:    0.6.2
 */