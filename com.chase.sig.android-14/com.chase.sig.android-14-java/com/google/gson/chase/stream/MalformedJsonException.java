package com.google.gson.chase.stream;

import java.io.IOException;

public final class MalformedJsonException extends IOException
{
  public MalformedJsonException(String paramString)
  {
    super(paramString);
  }
}

/* Location:           D:\code\Research\Android\apks\com.chase.sig.android-14\com.chase.sig.android-14_dex2jar.jar
 * Qualified Name:     com.google.gson.chase.stream.MalformedJsonException
 * JD-Core Version:    0.6.2
 */