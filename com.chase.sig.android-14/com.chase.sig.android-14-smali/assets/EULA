This End User License Addendum ("License Agreement") relates only to your use of the Chase Mobile and J.P. Morgan Mobile application (each an, "Application"). This Agreement is a legal agreement between you and JPMorgan Chase Bank, N.A. ("Licensor") and supplements the Online Service Agreement ("Initial Agreement") you previously agreed to when you enrolled to use Chase Online, Chase Online for Business Banking, or J.P. Morgan Online, and amends and becomes a part of the Initial Agreement. As used herein, the terms "us," "we," or "our" means JPMorgan Chase Bank, N.A.   By installing, copying or otherwise using the Application, you agree to be bound by the terms of this License Agreement. In the event of any conflict between the terms of the Initial Agreement and this License Agreement, the terms of this License Agreement controls. 

1.  Grant of License

Licensor hereby grants you a non-exclusive, non-transferable, worldwide, limited personal license to install and use the Application on your mobile device for your personal use. Such installation must occur in the United States. You may not, and will not allow or cause any third party to: (a) decompile, reverse engineer, disassemble, attempt to derive the source code of, or modify the Application, or use the Application to develop functionally similar Applications; (b) copy the Application, except as expressly permitted by this Agreement; (c) sublicense, distribute, export or resell the Application or otherwise transfer any rights; (d) remove any proprietary or intellectual property rights notices or labels on the Application; or (e) otherwise exercise any other right to the Application not expressly granted in this License Agreement.  

2.  Ownership of Application

Licensor owns all right, title and interest in and to the Application.  No license or other right in or to the Application is granted to you except for the rights specifically set forth in this License Agreement.  

3.  Consent to Use of Data

You agree that Licensor may collect and use technical data and related information, including but not limited to technical information about your device, system and application software and peripherals, that is gathered periodically to facilitate the provision of software updates, product support, fraud prevention and other services to you (if any) related to the Application.  Licensor may also use this information to improve its products or to provide services or technologies to you.

4.  Termination

This License Agreement is valid until terminated by you or Licensor.  Licensor may terminate the License at any time or for any reason. Your rights under this License will terminate immediately if you breach any term of this License.  Upon termination of this License, you shall immediately discontinue use of the Application and delete all copies of the Application.

5.  Disclaimer of Warranty

YOU UNDERSTAND AND AGREE THAT THE APPLICATION IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTY OF ANY KIND, AND LICENSOR HEREBY DISCLAIMS ALL WARRANTIES AND CONDITIONS WITH RESPECT TO THE APPLICATION, EITHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT. LICENSOR DOES NOT WARRANT AGAINST INTERFERENCE WITH YOUR ENJOYMENT OF THE APPLICATION, THAT THE FUNCTIONS CONTAINED IN, OR SERVICES PERFORMED OR PROVIDED BY, THE APPLICATION WILL MEET YOUR REQUIRMENTS, THAT THE OPERATION OF THE APPLICATION WILL BE UNINTERRUPTED OR ERROR-FREE, OR THAT DEFECTS IN THE APPLICATION WILL BE CORRECTED.  

6.  Limitation of Liability

TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, LICENSOR AND ITS ASSOCIATED SERVICE PROVIDERS SHALL NOT BE RESPONSIBLE FOR ANY LOSS, DAMAGE OR INJURY OR FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES, INCLUDING, BUT NOT LIMITED TO, LOST PROFITS, ARISING FROM OR RELATED TO THE APPLICATION AND/OR THE INSTALLATION OR MAINTENENACE THEREOF, FAILURE OF ELECTRONIC OR MECHANICAL EQUIPMENT, ERRORS, CONFIGURATION OR INCOMPATIBILITY PROBLEMS, PROBLEMS OR DELAYS WITH INTERMEDIATE COMPUTER OR COMMUNICATIONS NETWORKS, OR ANY OTHER PROBLEMS YOU EXPERIENCE DUE TO CAUSES BEYOND OUR CONTROL. 

7.  Additional Terms 

Maps used to provide ATM/branch locator services are provided by Google, Inc. ("Google") and are subject to Google's Terms of Use, which may be accessed at http://www.google.com/intl/en_us/help/terms_maps.html. You agree that we may share your geographical location (longitude/latitude) with Google when you use the service. You understand that Google's privacy policy and security practices may differ from our standards. We assume no responsibility for, nor do we control, endorse or guarantee any aspect of your use of the Google Maps. 

8.  Miscellaneous

This License Agreement and its enforcement shall be governed by the laws of the State of New York, without regard to any choice of law provision. Your use of the Application may also be subject to other local, state, national or international laws. If either party waives any provision of this License Agreement, that waiver is not deemed to be a continuing waiver of the same or any other provision. If any provision of this License Agreement is found to be unenforceable for any reason, that provision will be deemed to be restated to reflect as nearly as possible the original intentions of the parties in accordance with applicable law. The remaining provisions of this License Agreement will not be affected thereby, and each of those provisions will be valid and enforceable to the full extent permitted by law. Any rights not expressly granted in this License Agreement are reserved by Licensor. Each associated service provider of Licensor is an intended third party beneficiary of this License Agreement and is entitled to rely upon all rights, representations, warranties and covenants made in this Agreement.
