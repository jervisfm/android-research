.class public Lcom/chase/sig/analytics/AspectAnalyticsUtil;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final ACCT_ID:Ljava/lang/String; = "accountId"

.field private static final CARRIER_NETWORK:Ljava/lang/String; = "CARRIER_NETWORK"

.field private static final EPAY_ACCT_ID:Ljava/lang/String; = "epayAccountId"

.field private static final QP_SELECTED_ACCT_ID:Ljava/lang/String; = "selectedAccountId"

.field private static final TRACKING_DATA:Ljava/lang/String; = "trackingData"

.field private static final TRANSFER_ACTIVITY_PREFIX:Ljava/lang/String; = "Transfer"

.field protected static final UNRECOGNIZABLE:Ljava/lang/String; = "Unrecognized widget clicked!"

.field protected static final UNRECOGNIZABLE_SCREEN:Ljava/lang/String; = "Unrecognized Screen"

.field private static final WIFI:Ljava/lang/String; = "WIFI"

.field private static currentEditText:Landroid/widget/EditText;

.field public static iServiceErrorCode:Ljava/lang/String;

.field private static lastVisitedScreen:Lcom/chase/sig/analytics/ScreenAnalyticsData;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 35
    sput-object v0, Lcom/chase/sig/analytics/AspectAnalyticsUtil;->lastVisitedScreen:Lcom/chase/sig/analytics/ScreenAnalyticsData;

    .line 36
    sput-object v0, Lcom/chase/sig/analytics/AspectAnalyticsUtil;->currentEditText:Landroid/widget/EditText;

    .line 48
    const-string v0, ""

    sput-object v0, Lcom/chase/sig/analytics/AspectAnalyticsUtil;->iServiceErrorCode:Ljava/lang/String;

    .line 33
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/util/ArrayList;)Landroid/widget/TextView;
    .locals 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;)",
            "Landroid/widget/TextView;"
        }
    .end annotation

    .prologue
    .line 81
    const/4 v1, 0x0

    .line 83
    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    move-object v0, v1

    .line 90
    :goto_0
    return-object v0

    .line 83
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 84
    instance-of v3, v0, Landroid/widget/TextView;

    if-eqz v3, :cond_0

    .line 85
    check-cast v0, Landroid/widget/TextView;

    goto :goto_0
.end method

.method public static a()Lcom/chase/sig/analytics/ScreenAnalyticsData;
    .locals 1

    .prologue
    .line 128
    sget-object v0, Lcom/chase/sig/analytics/AspectAnalyticsUtil;->lastVisitedScreen:Lcom/chase/sig/analytics/ScreenAnalyticsData;

    return-object v0
.end method

.method public static a(Landroid/view/View;)Ljava/lang/String;
    .locals 2
    .parameter

    .prologue
    .line 194
    invoke-virtual {p0}, Landroid/view/View;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 196
    if-nez v0, :cond_1

    .line 197
    instance-of v1, p0, Landroid/widget/EditText;

    if-eqz v1, :cond_2

    .line 199
    check-cast p0, Landroid/widget/EditText;

    invoke-virtual {p0}, Landroid/widget/EditText;->getHint()Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 221
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 222
    const-string v0, "Unrecognized widget clicked!"

    .line 226
    :cond_1
    return-object v0

    .line 200
    :cond_2
    instance-of v1, p0, Landroid/widget/CheckBox;

    if-eqz v1, :cond_3

    .line 202
    check-cast p0, Landroid/widget/CheckBox;

    invoke-virtual {p0}, Landroid/widget/CheckBox;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    .line 203
    :cond_3
    instance-of v1, p0, Lcom/chase/sig/android/view/JPSpinner;

    if-eqz v1, :cond_4

    .line 205
    check-cast p0, Lcom/chase/sig/android/view/JPSpinner;

    invoke-virtual {p0}, Lcom/chase/sig/android/view/JPSpinner;->getHint()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 206
    :cond_4
    instance-of v1, p0, Landroid/widget/Button;

    if-eqz v1, :cond_5

    .line 208
    check-cast p0, Landroid/widget/TextView;

    invoke-static {p0}, Lcom/chase/sig/analytics/AspectAnalyticsUtil;->a(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 209
    :cond_5
    instance-of v1, p0, Landroid/widget/ImageView;

    if-eqz v1, :cond_6

    .line 211
    invoke-static {p0}, Lcom/chase/sig/analytics/AspectAnalyticsUtil;->d(Landroid/view/View;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 212
    :cond_6
    invoke-static {p0}, Lcom/chase/sig/analytics/AspectAnalyticsUtil;->c(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 214
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {p0, v0}, Lcom/chase/sig/analytics/AspectAnalyticsUtil;->a(Landroid/view/View;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    .line 215
    invoke-static {v0}, Lcom/chase/sig/analytics/AspectAnalyticsUtil;->a(Ljava/util/ArrayList;)Landroid/widget/TextView;

    move-result-object v0

    .line 216
    invoke-static {v0}, Lcom/chase/sig/analytics/AspectAnalyticsUtil;->a(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/widget/AdapterView;I)Ljava/lang/String;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;I)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 231
    invoke-virtual {p0, p1}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/ai;

    .line 233
    iget-object v0, v0, Lcom/chase/sig/android/view/ai;->b:Ljava/lang/String;

    .line 238
    if-nez v0, :cond_0

    .line 239
    const-string v0, "Unrecognized widget clicked!"

    .line 242
    :cond_0
    return-object v0
.end method

.method private static a(Landroid/widget/TextView;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 174
    invoke-virtual {p0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    instance-of v0, v0, Landroid/text/SpannedString;

    if-eqz v0, :cond_0

    .line 177
    invoke-virtual {p0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Landroid/text/SpannedString;

    .line 178
    invoke-virtual {v0}, Landroid/text/SpannedString;->toString()Ljava/lang/String;

    move-result-object v0

    .line 183
    :goto_0
    return-object v0

    .line 180
    :cond_0
    invoke-virtual {p0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public static a(Landroid/view/View;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 4
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 52
    invoke-static {p0}, Lcom/chase/sig/analytics/AspectAnalyticsUtil;->c(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, p0

    .line 54
    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    .line 55
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-lt v1, v2, :cond_1

    .line 65
    :cond_0
    return-object p1

    :cond_1
    move-object v0, p0

    .line 56
    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 58
    invoke-static {v0}, Lcom/chase/sig/analytics/AspectAnalyticsUtil;->c(Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 59
    invoke-static {v0, p1}, Lcom/chase/sig/analytics/AspectAnalyticsUtil;->a(Landroid/view/View;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 55
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 61
    :cond_2
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public static a(Lcom/chase/sig/android/activity/eb;)V
    .locals 4
    .parameter

    .prologue
    .line 96
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 98
    invoke-static {v0}, Lcom/chase/sig/analytics/AspectAnalyticsUtil;->d(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 99
    new-instance v1, Lcom/chase/sig/analytics/ScreenAnalyticsData;

    invoke-direct {v1, v0}, Lcom/chase/sig/analytics/ScreenAnalyticsData;-><init>(Ljava/lang/String;)V

    .line 100
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v2

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v3

    if-ne v2, v3, :cond_1

    const/4 v0, 0x3

    :goto_0
    invoke-virtual {v1, v0}, Lcom/chase/sig/analytics/ScreenAnalyticsData;->a(I)V

    .line 101
    sput-object v1, Lcom/chase/sig/analytics/AspectAnalyticsUtil;->lastVisitedScreen:Lcom/chase/sig/analytics/ScreenAnalyticsData;

    .line 103
    :cond_0
    return-void

    .line 100
    :cond_1
    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v2

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v0

    if-ge v2, v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)V
    .locals 2
    .parameter

    .prologue
    .line 165
    if-eqz p0, :cond_0

    const-string v0, "Unrecognized widget clicked!"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 167
    new-instance v0, Lcom/chase/sig/analytics/FieldAnalyticsData;

    invoke-direct {v0, p0}, Lcom/chase/sig/analytics/FieldAnalyticsData;-><init>(Ljava/lang/String;)V

    .line 168
    sget-object v1, Lcom/chase/sig/analytics/AspectAnalyticsUtil;->lastVisitedScreen:Lcom/chase/sig/analytics/ScreenAnalyticsData;

    .line 169
    invoke-virtual {v1, v0}, Lcom/chase/sig/analytics/ScreenAnalyticsData;->a(Lcom/chase/sig/analytics/FieldAnalyticsData;)V

    .line 171
    :cond_0
    return-void
.end method

.method public static a(Ljava/util/Hashtable;)V
    .locals 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 337
    invoke-static {}, Lcom/chase/sig/analytics/TrackingData;->a()Lcom/chase/sig/analytics/TrackingData;

    move-result-object v2

    .line 338
    const/4 v0, 0x0

    const-string v1, "epayAccountId"

    invoke-virtual {p0, v1}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v0, "epayAccountId"

    invoke-virtual {p0, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    :goto_0
    if-eqz v1, :cond_0

    invoke-static {}, Lcom/chase/sig/analytics/TrackingData;->a()Lcom/chase/sig/analytics/TrackingData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/analytics/TrackingData;->b()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/analytics/ScreenAnalyticsData;

    invoke-virtual {v0}, Lcom/chase/sig/analytics/ScreenAnalyticsData;->a()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Transfer"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v0, v1}, Lcom/chase/sig/analytics/ScreenAnalyticsData;->a(Ljava/lang/String;)V

    .line 342
    :cond_0
    monitor-enter v2

    .line 343
    :try_start_0
    invoke-virtual {v2}, Lcom/chase/sig/analytics/TrackingData;->c()Ljava/lang/String;

    move-result-object v0

    .line 342
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 346
    const-string v1, "trackingData"

    invoke-virtual {p0, v1, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 349
    invoke-static {}, Lcom/chase/sig/analytics/TrackingData;->a()Lcom/chase/sig/analytics/TrackingData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/analytics/TrackingData;->d()V

    .line 350
    sget-object v0, Lcom/chase/sig/analytics/AspectAnalyticsUtil;->lastVisitedScreen:Lcom/chase/sig/analytics/ScreenAnalyticsData;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/chase/sig/analytics/ScreenAnalyticsData;->b()V

    .line 351
    :cond_1
    return-void

    .line 338
    :cond_2
    const-string v1, "selectedAccountId"

    invoke-virtual {p0, v1}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v0, "selectedAccountId"

    invoke-virtual {p0, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    goto :goto_0

    :cond_3
    const-string v1, "accountId"

    invoke-virtual {p0, v1}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v0, "accountId"

    invoke-virtual {p0, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    goto :goto_0

    .line 342
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_4
    move-object v1, v0

    goto :goto_0
.end method

.method public static a(Landroid/widget/EditText;)Z
    .locals 1
    .parameter

    .prologue
    .line 395
    sget-object v0, Lcom/chase/sig/analytics/AspectAnalyticsUtil;->currentEditText:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/chase/sig/analytics/AspectAnalyticsUtil;->currentEditText:Landroid/widget/EditText;

    if-eq v0, p0, :cond_1

    .line 396
    :cond_0
    sput-object p0, Lcom/chase/sig/analytics/AspectAnalyticsUtil;->currentEditText:Landroid/widget/EditText;

    .line 397
    const/4 v0, 0x1

    .line 399
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/widget/EditText;Landroid/view/KeyEvent;I)Z
    .locals 3
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 379
    .line 381
    invoke-static {p0}, Lcom/chase/sig/analytics/AspectAnalyticsUtil;->a(Landroid/widget/EditText;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 383
    sparse-switch p2, :sswitch_data_0

    move v2, v0

    :goto_0
    if-eqz v2, :cond_0

    .line 384
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 390
    :goto_1
    return v0

    :sswitch_0
    move v2, v1

    .line 383
    goto :goto_0

    :cond_0
    move v0, v1

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_0
        0x14 -> :sswitch_0
        0x15 -> :sswitch_0
        0x16 -> :sswitch_0
        0x17 -> :sswitch_0
        0x42 -> :sswitch_0
    .end sparse-switch
.end method

.method public static b(Landroid/view/View;)Ljava/lang/String;
    .locals 4
    .parameter

    .prologue
    .line 276
    const/4 v0, 0x0

    .line 278
    instance-of v1, p0, Lcom/chase/sig/android/view/aj;

    if-eqz v1, :cond_0

    .line 281
    check-cast p0, Lcom/chase/sig/android/view/aj;

    invoke-virtual {p0}, Lcom/chase/sig/android/view/aj;->getDetails()Lcom/chase/sig/android/view/ai;

    move-result-object v2

    .line 283
    iget-object v1, v2, Lcom/chase/sig/android/view/ai;->b:Ljava/lang/String;

    .line 285
    iget-boolean v3, v2, Lcom/chase/sig/android/view/ai;->f:Z

    if-eqz v3, :cond_0

    const-string v3, "Business Accounts"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "Personal Accounts"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x0

    :goto_0
    if-nez v1, :cond_0

    .line 287
    iget-object v0, v2, Lcom/chase/sig/android/view/ai;->d:Ljava/lang/String;

    .line 291
    :cond_0
    return-object v0

    .line 285
    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static b()V
    .locals 2

    .prologue
    .line 263
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 264
    const-string v1, "wifi"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 263
    check-cast v0, Landroid/net/wifi/WifiManager;

    .line 262
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 267
    invoke-static {}, Lcom/chase/sig/analytics/TrackingData;->a()Lcom/chase/sig/analytics/TrackingData;

    move-result-object v0

    const-string v1, "WIFI"

    invoke-virtual {v0, v1}, Lcom/chase/sig/analytics/TrackingData;->a(Ljava/lang/String;)V

    .line 271
    :goto_0
    return-void

    .line 269
    :cond_0
    invoke-static {}, Lcom/chase/sig/analytics/TrackingData;->a()Lcom/chase/sig/analytics/TrackingData;

    move-result-object v0

    const-string v1, "CARRIER_NETWORK"

    invoke-virtual {v0, v1}, Lcom/chase/sig/analytics/TrackingData;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static b(Lcom/chase/sig/android/activity/eb;)V
    .locals 2
    .parameter

    .prologue
    .line 107
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 109
    invoke-static {v0}, Lcom/chase/sig/analytics/AspectAnalyticsUtil;->d(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 111
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->o()Landroid/widget/TextView;

    move-result-object v0

    .line 112
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 113
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 114
    sget-object v1, Lcom/chase/sig/analytics/AspectAnalyticsUtil;->lastVisitedScreen:Lcom/chase/sig/analytics/ScreenAnalyticsData;

    if-nez v0, :cond_0

    const-string v0, ""

    :cond_0
    invoke-virtual {v1, v0}, Lcom/chase/sig/analytics/ScreenAnalyticsData;->b(Ljava/lang/String;)V

    .line 117
    :cond_1
    invoke-static {}, Lcom/chase/sig/analytics/TrackingData;->a()Lcom/chase/sig/analytics/TrackingData;

    move-result-object v0

    sget-object v1, Lcom/chase/sig/analytics/AspectAnalyticsUtil;->lastVisitedScreen:Lcom/chase/sig/analytics/ScreenAnalyticsData;

    invoke-virtual {v0, v1}, Lcom/chase/sig/analytics/TrackingData;->a(Lcom/chase/sig/analytics/ScreenAnalyticsData;)V

    .line 119
    :cond_2
    return-void
.end method

.method public static b(Ljava/lang/String;)V
    .locals 2
    .parameter

    .prologue
    .line 248
    new-instance v0, Lcom/chase/sig/analytics/ErrorAnalyticsData;

    sget-object v1, Lcom/chase/sig/analytics/AspectAnalyticsUtil;->iServiceErrorCode:Ljava/lang/String;

    invoke-direct {v0, v1, p0}, Lcom/chase/sig/analytics/ErrorAnalyticsData;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    sget-object v1, Lcom/chase/sig/analytics/AspectAnalyticsUtil;->lastVisitedScreen:Lcom/chase/sig/analytics/ScreenAnalyticsData;

    .line 251
    invoke-virtual {v1, v0}, Lcom/chase/sig/analytics/ScreenAnalyticsData;->a(Lcom/chase/sig/analytics/ErrorAnalyticsData;)V

    .line 252
    const-string v0, ""

    sput-object v0, Lcom/chase/sig/analytics/AspectAnalyticsUtil;->iServiceErrorCode:Ljava/lang/String;

    .line 254
    return-void
.end method

.method public static c(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 257
    sput-object p0, Lcom/chase/sig/analytics/AspectAnalyticsUtil;->iServiceErrorCode:Ljava/lang/String;

    .line 258
    return-void
.end method

.method private static c(Landroid/view/View;)Z
    .locals 1
    .parameter

    .prologue
    .line 69
    instance-of v0, p0, Landroid/widget/RelativeLayout;

    if-nez v0, :cond_0

    instance-of v0, p0, Landroid/widget/LinearLayout;

    if-nez v0, :cond_0

    .line 70
    instance-of v0, p0, Landroid/widget/ScrollView;

    if-nez v0, :cond_0

    instance-of v0, p0, Landroid/widget/ListView;

    if-nez v0, :cond_0

    .line 71
    instance-of v0, p0, Landroid/widget/FrameLayout;

    if-eqz v0, :cond_1

    .line 73
    :cond_0
    const/4 v0, 0x1

    .line 76
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static d(Landroid/view/View;)Ljava/lang/String;
    .locals 3
    .parameter

    .prologue
    .line 406
    const-string v0, "_ImageViewArea"

    .line 411
    :try_start_0
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p0}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getResourceName(I)Ljava/lang/String;

    move-result-object v1

    .line 413
    invoke-static {v1}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 415
    const/16 v2, 0x2f

    invoke-virtual {v1, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 416
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 417
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 418
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 423
    :cond_0
    :goto_0
    return-object v0

    :catchall_0
    move-exception v1

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private static d(Ljava/lang/String;)Z
    .locals 3
    .parameter

    .prologue
    .line 133
    const/4 v0, 0x0

    .line 134
    invoke-static {}, Lcom/chase/sig/analytics/TrackingData;->a()Lcom/chase/sig/analytics/TrackingData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/analytics/TrackingData;->b()Ljava/util/ArrayList;

    move-result-object v1

    .line 136
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 138
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 139
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/analytics/ScreenAnalyticsData;

    .line 141
    invoke-virtual {v0}, Lcom/chase/sig/analytics/ScreenAnalyticsData;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    .line 144
    :cond_0
    return v0
.end method
