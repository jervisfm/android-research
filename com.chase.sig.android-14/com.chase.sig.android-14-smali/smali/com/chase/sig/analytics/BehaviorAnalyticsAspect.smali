.class public Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final GWO_PATH:Ljava/lang/String; = "/gws/"

.field private static synthetic ajc$initFailureCause:Ljava/lang/Throwable;

.field public static final synthetic ajc$perSingletonInstance:Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    :try_start_0
    new-instance v0, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-direct {v0}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;-><init>()V

    sput-object v0, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->ajc$perSingletonInstance:Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    sput-object v0, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->ajc$initFailureCause:Ljava/lang/Throwable;

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;
    .locals 3

    .prologue
    .line 1
    sget-object v0, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->ajc$perSingletonInstance:Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    if-nez v0, :cond_0

    new-instance v0, La/a/a/a;

    const-string v1, "com_chase_sig_analytics_BehaviorAnalyticsAspect"

    sget-object v2, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->ajc$initFailureCause:Ljava/lang/Throwable;

    invoke-direct {v0, v1, v2}, La/a/a/a;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    :cond_0
    sget-object v0, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->ajc$perSingletonInstance:Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    return-object v0
.end method

.method public static a(Landroid/view/View;)V
    .locals 1
    .parameter

    .prologue
    .line 218
    invoke-static {p0}, Lcom/chase/sig/analytics/AspectAnalyticsUtil;->a(Landroid/view/View;)Ljava/lang/String;

    move-result-object v0

    .line 222
    invoke-static {v0}, Lcom/chase/sig/analytics/AspectAnalyticsUtil;->a(Ljava/lang/String;)V

    .line 223
    return-void
.end method

.method public static a(Landroid/view/View;ILandroid/view/KeyEvent;)V
    .locals 2
    .parameter
    .parameter
    .parameter

    .prologue
    .line 165
    const/4 v1, 0x0

    .line 167
    instance-of v0, p0, Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    const/16 v0, 0x17

    if-ne p1, v0, :cond_0

    .line 168
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    .line 170
    invoke-static {p0}, Lcom/chase/sig/analytics/AspectAnalyticsUtil;->a(Landroid/view/View;)Ljava/lang/String;

    move-result-object v0

    .line 180
    :goto_0
    invoke-static {v0}, Lcom/chase/sig/analytics/AspectAnalyticsUtil;->a(Ljava/lang/String;)V

    .line 182
    return-void

    .line 172
    :cond_0
    instance-of v0, p0, Landroid/widget/EditText;

    if-eqz v0, :cond_1

    move-object v0, p0

    .line 174
    check-cast v0, Landroid/widget/EditText;

    invoke-static {v0, p2, p1}, Lcom/chase/sig/analytics/AspectAnalyticsUtil;->a(Landroid/widget/EditText;Landroid/view/KeyEvent;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 176
    invoke-static {p0}, Lcom/chase/sig/analytics/AspectAnalyticsUtil;->a(Landroid/view/View;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public static a(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 190
    const/4 v1, 0x0

    .line 192
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    .line 194
    instance-of v0, p0, Landroid/widget/EditText;

    if-eqz v0, :cond_0

    move-object v0, p0

    .line 196
    check-cast v0, Landroid/widget/EditText;

    invoke-static {v0}, Lcom/chase/sig/analytics/AspectAnalyticsUtil;->a(Landroid/widget/EditText;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 198
    invoke-static {p0}, Lcom/chase/sig/analytics/AspectAnalyticsUtil;->a(Landroid/view/View;)Ljava/lang/String;

    move-result-object v0

    .line 206
    :goto_0
    invoke-static {v0}, Lcom/chase/sig/analytics/AspectAnalyticsUtil;->a(Ljava/lang/String;)V

    .line 208
    return-void

    .line 200
    :cond_0
    instance-of v0, p0, Landroid/widget/CheckBox;

    if-eqz v0, :cond_1

    .line 202
    invoke-static {p0}, Lcom/chase/sig/analytics/AspectAnalyticsUtil;->a(Landroid/view/View;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public static a(Landroid/widget/AdapterView;Landroid/view/View;I)V
    .locals 3
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 232
    instance-of v0, p1, Lcom/chase/sig/android/view/aj;

    if-eqz v0, :cond_2

    .line 235
    invoke-static {p0, p2}, Lcom/chase/sig/analytics/AspectAnalyticsUtil;->a(Landroid/widget/AdapterView;I)Ljava/lang/String;

    move-result-object v0

    .line 240
    :goto_0
    const-string v1, "Unrecognized widget clicked!"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 242
    invoke-static {}, Lcom/chase/sig/analytics/AspectAnalyticsUtil;->a()Lcom/chase/sig/analytics/ScreenAnalyticsData;

    move-result-object v1

    .line 244
    invoke-static {p1}, Lcom/chase/sig/analytics/AspectAnalyticsUtil;->b(Landroid/view/View;)Ljava/lang/String;

    move-result-object v2

    .line 246
    if-eqz v2, :cond_0

    .line 247
    invoke-virtual {v1, v2}, Lcom/chase/sig/analytics/ScreenAnalyticsData;->a(Ljava/lang/String;)V

    .line 250
    :cond_0
    new-instance v2, Lcom/chase/sig/analytics/FieldAnalyticsData;

    invoke-direct {v2, v0}, Lcom/chase/sig/analytics/FieldAnalyticsData;-><init>(Ljava/lang/String;)V

    .line 251
    invoke-virtual {v1, v2}, Lcom/chase/sig/analytics/ScreenAnalyticsData;->a(Lcom/chase/sig/analytics/FieldAnalyticsData;)V

    .line 253
    :cond_1
    return-void

    .line 237
    :cond_2
    invoke-static {p1}, Lcom/chase/sig/analytics/AspectAnalyticsUtil;->a(Landroid/view/View;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/chase/sig/android/activity/eb;)V
    .locals 0
    .parameter

    .prologue
    .line 49
    invoke-static {p0}, Lcom/chase/sig/analytics/AspectAnalyticsUtil;->a(Lcom/chase/sig/android/activity/eb;)V

    .line 50
    return-void
.end method

.method public static a(Lcom/chase/sig/android/service/IServiceError;)V
    .locals 1
    .parameter

    .prologue
    .line 275
    invoke-interface {p0}, Lcom/chase/sig/android/service/IServiceError;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/analytics/AspectAnalyticsUtil;->c(Ljava/lang/String;)V

    .line 276
    return-void
.end method

.method public static a(Lcom/chase/sig/android/view/detail/a;Landroid/view/View;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 77
    const v0, 0x7f090015

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 78
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 80
    invoke-static {v0}, Lcom/chase/sig/android/util/s;->l(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 81
    invoke-virtual {p0}, Lcom/chase/sig/android/view/detail/a;->i()Ljava/lang/String;

    move-result-object v0

    .line 84
    :cond_0
    invoke-virtual {p0}, Lcom/chase/sig/android/view/detail/a;->e()I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 85
    if-eqz v1, :cond_1

    .line 86
    invoke-virtual {v1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 88
    :cond_1
    return-void
.end method

.method public static a(Lcom/chase/sig/android/view/k$a;)V
    .locals 1
    .parameter

    .prologue
    .line 264
    iget-object v0, p0, Lcom/chase/sig/android/view/k$a;->c:Ljava/lang/CharSequence;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/analytics/AspectAnalyticsUtil;->b(Ljava/lang/String;)V

    .line 265
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/util/Hashtable;)V
    .locals 0
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 312
    invoke-static {p1, p0}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Ljava/util/Hashtable;Ljava/lang/String;)V

    .line 313
    return-void
.end method

.method private static a(Ljava/util/Hashtable;Ljava/lang/String;)V
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 343
    const-string v0, "/gws/"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 345
    invoke-static {}, Lcom/chase/sig/analytics/AspectAnalyticsUtil;->b()V

    .line 346
    invoke-static {p0}, Lcom/chase/sig/analytics/AspectAnalyticsUtil;->a(Ljava/util/Hashtable;)V

    .line 348
    :cond_0
    return-void
.end method

.method public static b(Lcom/chase/sig/android/activity/eb;)V
    .locals 0
    .parameter

    .prologue
    .line 60
    invoke-static {p0}, Lcom/chase/sig/analytics/AspectAnalyticsUtil;->b(Lcom/chase/sig/android/activity/eb;)V

    .line 61
    return-void
.end method

.method public static b(Lcom/chase/sig/android/service/IServiceError;)V
    .locals 1
    .parameter

    .prologue
    .line 286
    invoke-interface {p0}, Lcom/chase/sig/android/service/IServiceError;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/analytics/AspectAnalyticsUtil;->c(Ljava/lang/String;)V

    .line 287
    return-void
.end method

.method public static b(Ljava/lang/String;Ljava/util/Hashtable;)V
    .locals 0
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 326
    invoke-static {p1, p0}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Ljava/util/Hashtable;Ljava/lang/String;)V

    .line 327
    return-void
.end method

.method public static c(Ljava/lang/String;Ljava/util/Hashtable;)V
    .locals 0
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 338
    invoke-static {p1, p0}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Ljava/util/Hashtable;Ljava/lang/String;)V

    .line 339
    return-void
.end method


# virtual methods
.method public final a(Lcom/chase/sig/android/view/detail/a;)V
    .locals 2
    .parameter

    .prologue
    .line 101
    invoke-virtual {p1}, Lcom/chase/sig/android/view/detail/a;->k()Landroid/view/View;

    move-result-object v0

    .line 103
    invoke-virtual {p1}, Lcom/chase/sig/android/view/detail/a;->e()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 104
    if-eqz v0, :cond_0

    .line 106
    new-instance v1, Lcom/chase/sig/analytics/a;

    invoke-direct {v1, p0}, Lcom/chase/sig/analytics/a;-><init>(Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 114
    new-instance v1, Lcom/chase/sig/analytics/b;

    invoke-direct {v1, p0}, Lcom/chase/sig/analytics/b;-><init>(Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 124
    :cond_0
    return-void
.end method

.method public final c(Lcom/chase/sig/android/activity/eb;)V
    .locals 3
    .parameter

    .prologue
    .line 133
    invoke-virtual {p1}, Lcom/chase/sig/android/activity/eb;->n()Landroid/view/ViewGroup;

    move-result-object v0

    .line 136
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 135
    invoke-static {v0, v1}, Lcom/chase/sig/analytics/AspectAnalyticsUtil;->a(Landroid/view/View;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    .line 138
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 157
    return-void

    .line 138
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 139
    instance-of v2, v0, Landroid/widget/EditText;

    if-nez v2, :cond_2

    instance-of v2, v0, Landroid/widget/CheckBox;

    if-eqz v2, :cond_0

    .line 140
    :cond_2
    new-instance v2, Lcom/chase/sig/analytics/c;

    invoke-direct {v2, p0}, Lcom/chase/sig/analytics/c;-><init>(Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 148
    new-instance v2, Lcom/chase/sig/analytics/d;

    invoke-direct {v2, p0}, Lcom/chase/sig/analytics/d;-><init>(Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    goto :goto_0
.end method
