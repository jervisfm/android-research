.class public Lcom/chase/sig/analytics/ScreenAnalyticsData;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final LANDSCAPE:Ljava/lang/String; = "landscape"

.field private static final PORTRAIT:Ljava/lang/String; = "portrait"


# instance fields
.field private accountId:Ljava/lang/String;

.field private attributes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/chase/sig/analytics/AttributeAnalyticsData;",
            ">;"
        }
    .end annotation
.end field

.field private errors:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/chase/sig/analytics/ErrorAnalyticsData;",
            ">;"
        }
    .end annotation
.end field

.field private fields:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/chase/sig/analytics/FieldAnalyticsData;",
            ">;"
        }
    .end annotation
.end field

.field private name:Ljava/lang/String;

.field private timestamp:J

.field private title:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .parameter

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/chase/sig/analytics/ScreenAnalyticsData;->timestamp:J

    .line 24
    iput-object p1, p0, Lcom/chase/sig/analytics/ScreenAnalyticsData;->name:Ljava/lang/String;

    .line 26
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/chase/sig/analytics/ScreenAnalyticsData;->attributes:Ljava/util/ArrayList;

    .line 27
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/chase/sig/analytics/ScreenAnalyticsData;->fields:Ljava/util/ArrayList;

    .line 28
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/chase/sig/analytics/ScreenAnalyticsData;->errors:Ljava/util/ArrayList;

    .line 30
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/chase/sig/analytics/ScreenAnalyticsData;->name:Ljava/lang/String;

    return-object v0
.end method

.method public final a(I)V
    .locals 3
    .parameter

    .prologue
    .line 68
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 69
    const-string v0, "landscape"

    .line 74
    :goto_0
    new-instance v1, Lcom/chase/sig/analytics/AttributeAnalyticsData;

    const-string v2, "interfaceOrientation"

    invoke-direct {v1, v2, v0}, Lcom/chase/sig/analytics/AttributeAnalyticsData;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/chase/sig/analytics/ScreenAnalyticsData;->attributes:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 75
    return-void

    .line 71
    :cond_0
    const-string v0, "portrait"

    goto :goto_0
.end method

.method public final a(Lcom/chase/sig/analytics/ErrorAnalyticsData;)V
    .locals 1
    .parameter

    .prologue
    .line 80
    iget-object v0, p0, Lcom/chase/sig/analytics/ScreenAnalyticsData;->errors:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 81
    return-void
.end method

.method public final a(Lcom/chase/sig/analytics/FieldAnalyticsData;)V
    .locals 1
    .parameter

    .prologue
    .line 55
    iget-object v0, p0, Lcom/chase/sig/analytics/ScreenAnalyticsData;->fields:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 56
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 104
    iput-object p1, p0, Lcom/chase/sig/analytics/ScreenAnalyticsData;->accountId:Ljava/lang/String;

    .line 105
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/chase/sig/analytics/ScreenAnalyticsData;->attributes:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 121
    iget-object v0, p0, Lcom/chase/sig/analytics/ScreenAnalyticsData;->errors:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 122
    iget-object v0, p0, Lcom/chase/sig/analytics/ScreenAnalyticsData;->fields:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 124
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1
    .parameter

    .prologue
    .line 113
    iget-object v0, p0, Lcom/chase/sig/analytics/ScreenAnalyticsData;->title:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 114
    iput-object p1, p0, Lcom/chase/sig/analytics/ScreenAnalyticsData;->title:Ljava/lang/String;

    .line 116
    :cond_0
    return-void
.end method
