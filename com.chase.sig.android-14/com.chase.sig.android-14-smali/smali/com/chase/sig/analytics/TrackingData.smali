.class public Lcom/chase/sig/analytics/TrackingData;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static trackingData:Lcom/chase/sig/analytics/TrackingData;


# instance fields
.field private attributes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/chase/sig/analytics/AttributeAnalyticsData;",
            ">;"
        }
    .end annotation
.end field

.field private bandwidth:Ljava/lang/String;

.field private channelId:Ljava/lang/String;

.field private screens:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/chase/sig/analytics/ScreenAnalyticsData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    new-instance v0, Lcom/chase/sig/analytics/TrackingData;

    invoke-direct {v0}, Lcom/chase/sig/analytics/TrackingData;-><init>()V

    sput-object v0, Lcom/chase/sig/analytics/TrackingData;->trackingData:Lcom/chase/sig/analytics/TrackingData;

    .line 12
    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    const-string v0, "WIFI"

    iput-object v0, p0, Lcom/chase/sig/analytics/TrackingData;->bandwidth:Ljava/lang/String;

    .line 24
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->h()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/analytics/TrackingData;->channelId:Ljava/lang/String;

    .line 25
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/chase/sig/analytics/TrackingData;->attributes:Ljava/util/ArrayList;

    .line 26
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/chase/sig/analytics/TrackingData;->screens:Ljava/util/ArrayList;

    .line 27
    return-void
.end method

.method public static a()Lcom/chase/sig/analytics/TrackingData;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/chase/sig/analytics/TrackingData;->trackingData:Lcom/chase/sig/analytics/TrackingData;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/chase/sig/analytics/ScreenAnalyticsData;)V
    .locals 1
    .parameter

    .prologue
    .line 58
    iget-object v0, p0, Lcom/chase/sig/analytics/TrackingData;->screens:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 59
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 42
    iput-object p1, p0, Lcom/chase/sig/analytics/TrackingData;->bandwidth:Ljava/lang/String;

    .line 43
    return-void
.end method

.method public final b()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/chase/sig/analytics/ScreenAnalyticsData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 66
    iget-object v0, p0, Lcom/chase/sig/analytics/TrackingData;->screens:Ljava/util/ArrayList;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    new-instance v0, Lcom/google/gson/chase/GsonBuilder;

    invoke-direct {v0}, Lcom/google/gson/chase/GsonBuilder;-><init>()V

    .line 88
    invoke-virtual {v0}, Lcom/google/gson/chase/GsonBuilder;->a()Lcom/google/gson/chase/GsonBuilder;

    move-result-object v0

    .line 89
    invoke-virtual {v0}, Lcom/google/gson/chase/GsonBuilder;->b()Lcom/google/gson/chase/Gson;

    move-result-object v0

    .line 87
    invoke-virtual {v0, p0}, Lcom/google/gson/chase/Gson;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/chase/sig/analytics/TrackingData;->screens:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 97
    iget-object v0, p0, Lcom/chase/sig/analytics/TrackingData;->attributes:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 98
    return-void
.end method
