.class public Lcom/chase/sig/android/ChaseApplication;
.super Landroid/app/Application;
.source "SourceFile"


# static fields
.field public static a:Z

.field private static b:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static c:Ljava/util/Properties;

.field private static d:Lcom/chase/sig/android/ChaseApplication;

.field private static e:Lcom/chase/sig/android/f;

.field private static f:Lcom/chase/sig/android/a;


# instance fields
.field private g:Lcom/chase/sig/android/domain/o;

.field private h:Lcom/chase/sig/android/domain/MobileAdResponse;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    sput-object v0, Lcom/chase/sig/android/ChaseApplication;->b:Ljava/util/Hashtable;

    .line 47
    const/4 v0, 0x0

    sput-boolean v0, Lcom/chase/sig/android/ChaseApplication;->a:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    .line 54
    sput-object p0, Lcom/chase/sig/android/ChaseApplication;->d:Lcom/chase/sig/android/ChaseApplication;

    .line 55
    return-void
.end method

.method public static a()Lcom/chase/sig/android/ChaseApplication;
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/chase/sig/android/ChaseApplication;->d:Lcom/chase/sig/android/ChaseApplication;

    return-object v0
.end method

.method public static a(Lcom/chase/sig/android/a;)V
    .locals 0
    .parameter

    .prologue
    .line 268
    sput-object p0, Lcom/chase/sig/android/ChaseApplication;->f:Lcom/chase/sig/android/a;

    .line 269
    return-void
.end method

.method public static a(Lcom/chase/sig/android/f;)V
    .locals 0
    .parameter

    .prologue
    .line 260
    sput-object p0, Lcom/chase/sig/android/ChaseApplication;->e:Lcom/chase/sig/android/f;

    .line 261
    return-void
.end method

.method public static n()V
    .locals 1

    .prologue
    .line 247
    sget-object v0, Lcom/chase/sig/android/ChaseApplication;->e:Lcom/chase/sig/android/f;

    if-eqz v0, :cond_0

    .line 248
    sget-object v0, Lcom/chase/sig/android/ChaseApplication;->e:Lcom/chase/sig/android/f;

    invoke-static {}, Lcom/chase/sig/android/f;->a()V

    .line 250
    :cond_0
    return-void
.end method

.method public static o()Lcom/chase/sig/android/f;
    .locals 1

    .prologue
    .line 256
    sget-object v0, Lcom/chase/sig/android/ChaseApplication;->e:Lcom/chase/sig/android/f;

    return-object v0
.end method

.method public static p()Lcom/chase/sig/android/a;
    .locals 1

    .prologue
    .line 264
    sget-object v0, Lcom/chase/sig/android/ChaseApplication;->f:Lcom/chase/sig/android/a;

    return-object v0
.end method

.method public static u()V
    .locals 1

    .prologue
    .line 322
    sget-object v0, Lcom/chase/sig/android/ChaseApplication;->e:Lcom/chase/sig/android/f;

    if-eqz v0, :cond_0

    .line 323
    sget-object v0, Lcom/chase/sig/android/ChaseApplication;->e:Lcom/chase/sig/android/f;

    invoke-virtual {v0}, Lcom/chase/sig/android/f;->interrupt()V

    .line 324
    const/4 v0, 0x0

    sput-object v0, Lcom/chase/sig/android/ChaseApplication;->e:Lcom/chase/sig/android/f;

    .line 326
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .parameter

    .prologue
    .line 115
    sget-object v0, Lcom/chase/sig/android/ChaseApplication;->c:Ljava/util/Properties;

    if-nez v0, :cond_0

    .line 116
    :try_start_0
    invoke-virtual {p0}, Lcom/chase/sig/android/ChaseApplication;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    const-string v1, "application_config.properties"

    invoke-virtual {v0, v1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    new-instance v1, Ljava/util/Properties;

    invoke-direct {v1}, Ljava/util/Properties;-><init>()V

    sput-object v1, Lcom/chase/sig/android/ChaseApplication;->c:Ljava/util/Properties;

    invoke-virtual {v1, v0}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 119
    :cond_0
    :goto_0
    sget-object v0, Lcom/chase/sig/android/ChaseApplication;->c:Ljava/util/Properties;

    invoke-virtual {v0, p1}, Ljava/util/Properties;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(Lcom/chase/sig/android/domain/MobileAdResponse;)V
    .locals 0
    .parameter

    .prologue
    .line 329
    iput-object p1, p0, Lcom/chase/sig/android/ChaseApplication;->h:Lcom/chase/sig/android/domain/MobileAdResponse;

    .line 330
    return-void
.end method

.method public final a(Lcom/chase/sig/android/domain/o;)V
    .locals 0
    .parameter

    .prologue
    .line 71
    iput-object p1, p0, Lcom/chase/sig/android/ChaseApplication;->g:Lcom/chase/sig/android/domain/o;

    .line 72
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 157
    const-string v0, "application.preferences"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/ChaseApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 158
    const-string v1, "CURRENT_ENVIRONMENT"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 160
    if-eqz p2, :cond_0

    .line 161
    const-string v1, "CURRENT_ENVIRONMENT_HOSTNAME"

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 164
    :cond_0
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 165
    return-void
.end method

.method public final a(Z)V
    .locals 2
    .parameter

    .prologue
    .line 288
    const-string v0, "application.preferences"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/ChaseApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 289
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 290
    const-string v1, "can_auto_focus"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 291
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 292
    return-void
.end method

.method public final b()Lcom/chase/sig/android/domain/o;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/chase/sig/android/ChaseApplication;->g:Lcom/chase/sig/android/domain/o;

    if-nez v0, :cond_0

    .line 64
    new-instance v0, Lcom/chase/sig/android/domain/o;

    invoke-direct {v0}, Lcom/chase/sig/android/domain/o;-><init>()V

    .line 67
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/ChaseApplication;->g:Lcom/chase/sig/android/domain/o;

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1
    .parameter

    .prologue
    .line 168
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/chase/sig/android/ChaseApplication;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    return-void
.end method

.method public final b(Z)V
    .locals 2
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 337
    sput-boolean p1, Lcom/chase/sig/android/ChaseApplication;->a:Z

    .line 338
    iput-object v1, p0, Lcom/chase/sig/android/ChaseApplication;->h:Lcom/chase/sig/android/domain/MobileAdResponse;

    .line 339
    sget-object v0, Lcom/chase/sig/android/activity/AccountsActivity;->a:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 341
    sget-object v0, Lcom/chase/sig/android/activity/AccountsActivity;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 342
    sput-object v1, Lcom/chase/sig/android/activity/AccountsActivity;->a:Landroid/graphics/Bitmap;

    .line 344
    :cond_0
    return-void
.end method

.method public final c(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .parameter

    .prologue
    .line 236
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "environment."

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/chase/sig/android/ChaseApplication;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/ChaseApplication;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 238
    invoke-virtual {p0}, Lcom/chase/sig/android/ChaseApplication;->g()Ljava/lang/String;

    move-result-object v1

    .line 239
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 240
    const-string v2, "_HOST_AND_PORT_"

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 243
    :cond_0
    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/chase/sig/android/ChaseApplication;->g:Lcom/chase/sig/android/domain/o;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/ChaseApplication;->g:Lcom/chase/sig/android/domain/o;

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->a:Lcom/chase/sig/android/domain/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/ChaseApplication;->g:Lcom/chase/sig/android/domain/o;

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->a:Lcom/chase/sig/android/domain/a;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 136
    const-string v0, "application.preferences"

    invoke-virtual {p0, v0, v3}, Lcom/chase/sig/android/ChaseApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "deviceId"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 137
    if-eqz v0, :cond_0

    .line 147
    :goto_0
    return-object v0

    .line 141
    :cond_0
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    .line 142
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v0, v1, v3

    .line 144
    const-string v1, "application.preferences"

    invoke-virtual {p0, v1, v3}, Lcom/chase/sig/android/ChaseApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 145
    const-string v2, "deviceId"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 146
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method

.method public final e()Landroid/content/SharedPreferences;
    .locals 2

    .prologue
    .line 152
    const-string v0, "application.preferences"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/ChaseApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 3

    .prologue
    .line 173
    const-string v0, "application.preferences"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/ChaseApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 174
    const-string v1, "CURRENT_ENVIRONMENT"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 175
    const-string v1, "CURRENT_ENVIRONMENT"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 178
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "environment.default"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/ChaseApplication;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final g()Ljava/lang/String;
    .locals 3

    .prologue
    .line 182
    const-string v0, "application.preferences"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/ChaseApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "CURRENT_ENVIRONMENT_HOSTNAME"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final h()Ljava/lang/String;
    .locals 2

    .prologue
    .line 186
    invoke-virtual {p0}, Lcom/chase/sig/android/ChaseApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f07

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final i()Z
    .locals 2

    .prologue
    .line 190
    const-string v0, "PBD"

    invoke-virtual {p0}, Lcom/chase/sig/android/ChaseApplication;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final j()Z
    .locals 2

    .prologue
    .line 194
    const-string v0, "debug"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/ChaseApplication;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 195
    invoke-static {v0}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "true"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()Ljava/lang/String;
    .locals 6

    .prologue
    .line 200
    invoke-virtual {p0}, Lcom/chase/sig/android/ChaseApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070001

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 201
    invoke-virtual {p0}, Lcom/chase/sig/android/ChaseApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070002

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 202
    invoke-virtual {p0}, Lcom/chase/sig/android/ChaseApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070003

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 204
    const-string v3, "%s.%s.%s"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    const/4 v0, 0x1

    aput-object v1, v4, v0

    const/4 v0, 0x2

    aput-object v2, v4, v0

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 206
    const-string v1, "%"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v0, "0"

    :cond_0
    return-object v0
.end method

.method public final l()Ljava/util/Date;
    .locals 5

    .prologue
    .line 210
    const-string v0, "application.preferences"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/ChaseApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 211
    const-string v0, "app_install_date"

    const-wide/16 v3, -0x1

    invoke-interface {v2, v0, v3, v4}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 213
    const-wide/16 v3, 0x1

    cmp-long v3, v0, v3

    if-gez v3, :cond_0

    .line 214
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 215
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    .line 217
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 218
    const-string v3, "app_install_date"

    invoke-interface {v2, v3, v0, v1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 220
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 223
    :cond_0
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, v0, v1}, Ljava/util/Date;-><init>(J)V

    return-object v2
.end method

.method public final m()Ljava/lang/String;
    .locals 2

    .prologue
    .line 231
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyy-MM-dd HH:mm:ss.S"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 232
    invoke-virtual {p0}, Lcom/chase/sig/android/ChaseApplication;->l()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 0

    .prologue
    .line 96
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 98
    invoke-virtual {p0}, Lcom/chase/sig/android/ChaseApplication;->l()Ljava/util/Date;

    .line 99
    return-void
.end method

.method public onLowMemory()V
    .locals 0

    .prologue
    .line 103
    invoke-virtual {p0}, Lcom/chase/sig/android/ChaseApplication;->j()Z

    .line 104
    return-void
.end method

.method public final q()V
    .locals 1

    .prologue
    .line 277
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/chase/sig/android/ChaseApplication;->g:Lcom/chase/sig/android/domain/o;

    .line 279
    new-instance v0, Lcom/chase/sig/android/util/f;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/util/f;-><init>(Lcom/chase/sig/android/ChaseApplication;)V

    .line 280
    invoke-virtual {v0}, Lcom/chase/sig/android/util/f;->a()V

    .line 282
    invoke-static {}, Lcom/chase/sig/android/c;->a()V

    .line 284
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/ChaseApplication;->b(Z)V

    .line 285
    return-void
.end method

.method public final r()Z
    .locals 3

    .prologue
    .line 298
    const-string v0, "application.preferences"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/ChaseApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "show_jpm_upgrade_prompt"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final s()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 307
    const-string v0, "application.preferences"

    invoke-virtual {p0, v0, v2}, Lcom/chase/sig/android/ChaseApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 308
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 309
    const-string v1, "show_jpm_upgrade_prompt"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 310
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 311
    return-void
.end method

.method public final t()Z
    .locals 3

    .prologue
    .line 318
    const-string v0, "application.preferences"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/ChaseApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "can_auto_focus"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final v()Lcom/chase/sig/android/domain/MobileAdResponse;
    .locals 1

    .prologue
    .line 333
    iget-object v0, p0, Lcom/chase/sig/android/ChaseApplication;->h:Lcom/chase/sig/android/domain/MobileAdResponse;

    return-object v0
.end method
