.class public final Lcom/chase/sig/android/a;
.super Ljava/lang/Thread;
.source "SourceFile"


# instance fields
.field private a:Lcom/chase/sig/android/activity/eb;


# direct methods
.method public constructor <init>(Lcom/chase/sig/android/activity/eb;)V
    .locals 0
    .parameter

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/chase/sig/android/a;->a:Lcom/chase/sig/android/activity/eb;

    .line 19
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 28
    const-wide/32 v0, 0xdbba0

    :try_start_0
    invoke-static {v0, v1}, Lcom/chase/sig/android/a;->sleep(J)V

    .line 30
    new-instance v1, Ljava/util/Hashtable;

    invoke-direct {v1}, Ljava/util/Hashtable;-><init>()V

    .line 31
    const-string v0, "trackingData"

    invoke-static {}, Lcom/chase/sig/analytics/TrackingData;->a()Lcom/chase/sig/analytics/TrackingData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/chase/sig/analytics/TrackingData;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/chase/sig/analytics/TrackingData;->a()Lcom/chase/sig/analytics/TrackingData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/chase/sig/analytics/TrackingData;->d()V

    invoke-virtual {v1, v0, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    const-string v2, "userId"

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->a:Lcom/chase/sig/android/domain/a;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/chase/sig/android/domain/a;->c:Ljava/lang/String;

    :goto_0
    invoke-virtual {v1, v2, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    new-instance v2, Lcom/chase/sig/android/service/u;

    iget-object v3, p0, Lcom/chase/sig/android/a;->a:Lcom/chase/sig/android/activity/eb;

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->a:Lcom/chase/sig/android/domain/a;

    if-eqz v0, :cond_1

    const v0, 0x7f070011

    :goto_1
    invoke-virtual {v3, v0}, Lcom/chase/sig/android/activity/eb;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0, v1}, Lcom/chase/sig/android/service/u;-><init>(Ljava/lang/String;Ljava/util/Hashtable;)V

    .line 37
    invoke-virtual {v2}, Lcom/chase/sig/android/service/u;->a()V

    .line 44
    :goto_2
    return-void

    .line 32
    :cond_0
    const-string v0, ""
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 34
    :cond_1
    const v0, 0x7f070012

    goto :goto_1

    .line 44
    :catch_0
    move-exception v0

    goto :goto_2
.end method
