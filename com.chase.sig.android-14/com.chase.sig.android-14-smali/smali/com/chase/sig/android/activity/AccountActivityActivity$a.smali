.class final Lcom/chase/sig/android/activity/AccountActivityActivity$a;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/AccountActivityActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/chase/sig/android/domain/AccountActivity;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/AccountActivityActivity;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/AccountActivity;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/chase/sig/android/activity/AccountActivityActivity;Landroid/app/Activity;Ljava/util/List;)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 251
    iput-object p1, p0, Lcom/chase/sig/android/activity/AccountActivityActivity$a;->a:Lcom/chase/sig/android/activity/AccountActivityActivity;

    .line 252
    const v0, 0x7f030005

    invoke-direct {p0, p2, v0, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 253
    iput-object p3, p0, Lcom/chase/sig/android/activity/AccountActivityActivity$a;->b:Ljava/util/List;

    .line 254
    return-void
.end method

.method private a(Landroid/widget/RelativeLayout;Lcom/chase/sig/android/domain/ActivityValue;)Landroid/widget/RelativeLayout;
    .locals 4
    .parameter
    .parameter

    .prologue
    .line 322
    const v0, 0x7f090016

    invoke-virtual {p1, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 323
    const v1, 0x7f090017

    invoke-virtual {p1, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 325
    invoke-virtual {p2}, Lcom/chase/sig/android/domain/ActivityValue;->d()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/chase/sig/android/activity/AccountActivityActivity$a;->a:Lcom/chase/sig/android/activity/AccountActivityActivity;

    invoke-virtual {v2}, Lcom/chase/sig/android/activity/AccountActivityActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f060015

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 330
    :goto_0
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 332
    iget-object v2, p2, Lcom/chase/sig/android/domain/ActivityValue;->label:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 333
    invoke-virtual {p2}, Lcom/chase/sig/android/domain/ActivityValue;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "N/A"

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 335
    return-object p1

    .line 325
    :cond_0
    iget-object v2, p0, Lcom/chase/sig/android/activity/AccountActivityActivity$a;->a:Lcom/chase/sig/android/activity/AccountActivityActivity;

    invoke-virtual {v2}, Lcom/chase/sig/android/activity/AccountActivityActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f060005

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    goto :goto_0

    .line 333
    :cond_1
    iget-object v0, p2, Lcom/chase/sig/android/domain/ActivityValue;->value:Ljava/lang/String;

    goto :goto_1
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/AccountActivityActivity$a;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 244
    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountActivityActivity$a;->b:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/util/Collection;)V
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/chase/sig/android/domain/AccountActivity;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 257
    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountActivityActivity$a;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 258
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/AccountActivityActivity$a;->notifyDataSetChanged()V

    .line 259
    return-void
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11
    .parameter
    .parameter
    .parameter

    .prologue
    const v3, 0x7f090008

    const v10, 0x7f030005

    const/4 v9, 0x0

    const/4 v5, 0x0

    .line 264
    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountActivityActivity$a;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/AccountActivity;

    .line 265
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/AccountActivity;->b()Ljava/util/ArrayList;

    move-result-object v7

    .line 266
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v8

    .line 268
    iget-object v1, p0, Lcom/chase/sig/android/activity/AccountActivityActivity$a;->a:Lcom/chase/sig/android/activity/AccountActivityActivity;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/activity/AccountActivityActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 272
    if-nez p2, :cond_1

    .line 274
    const v2, 0x7f030001

    invoke-virtual {v1, v2, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 276
    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    .line 277
    const v4, 0x7f090009

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    iget-object v4, p0, Lcom/chase/sig/android/activity/AccountActivityActivity$a;->a:Lcom/chase/sig/android/activity/AccountActivityActivity;

    invoke-static {v4}, Lcom/chase/sig/android/activity/AccountActivityActivity;->e(Lcom/chase/sig/android/activity/AccountActivityActivity;)Lcom/chase/sig/android/domain/IAccount;

    move-result-object v4

    invoke-interface {v4}, Lcom/chase/sig/android/domain/IAccount;->m()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/chase/sig/android/activity/AccountActivityActivity$a;->a:Lcom/chase/sig/android/activity/AccountActivityActivity;

    invoke-virtual {v4}, Lcom/chase/sig/android/activity/AccountActivityActivity;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v4

    iget-object v4, v4, Lcom/chase/sig/android/domain/o;->b:Lcom/chase/sig/android/domain/g;

    invoke-interface {v4}, Lcom/chase/sig/android/domain/g;->C()Z

    move-result v4

    if-eqz v4, :cond_0

    move v4, v5

    :goto_0
    invoke-virtual {v6, v4}, Landroid/view/View;->setVisibility(I)V

    move v6, v5

    .line 281
    :goto_1
    if-ge v6, v8, :cond_4

    .line 284
    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/chase/sig/android/domain/ActivityValue;

    .line 285
    invoke-virtual {v1, v10, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/RelativeLayout;

    .line 286
    invoke-direct {p0, v5, v4}, Lcom/chase/sig/android/activity/AccountActivityActivity$a;->a(Landroid/widget/RelativeLayout;Lcom/chase/sig/android/domain/ActivityValue;)Landroid/widget/RelativeLayout;

    move-result-object v4

    .line 288
    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 281
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_1

    .line 277
    :cond_0
    const/16 v4, 0x8

    goto :goto_0

    .line 293
    :cond_1
    check-cast p2, Landroid/widget/LinearLayout;

    .line 294
    invoke-virtual {p2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    .line 296
    :goto_2
    if-ge v5, v8, :cond_3

    .line 298
    invoke-virtual {v2, v5}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    .line 299
    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/chase/sig/android/domain/ActivityValue;

    .line 301
    if-nez v3, :cond_2

    .line 302
    invoke-virtual {v1, v10, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    .line 303
    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 306
    :cond_2
    invoke-direct {p0, v3, v4}, Lcom/chase/sig/android/activity/AccountActivityActivity$a;->a(Landroid/widget/RelativeLayout;Lcom/chase/sig/android/domain/ActivityValue;)Landroid/widget/RelativeLayout;

    .line 296
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    :cond_3
    move-object v2, p2

    .line 310
    :cond_4
    const v1, 0x7f090007

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 311
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/AccountActivity;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 313
    return-object v2
.end method
