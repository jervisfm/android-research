.class final Lcom/chase/sig/android/activity/AccountActivityActivity$b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/AccountActivityActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/AccountActivityActivity;


# direct methods
.method private constructor <init>(Lcom/chase/sig/android/activity/AccountActivityActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 354
    iput-object p1, p0, Lcom/chase/sig/android/activity/AccountActivityActivity$b;->a:Lcom/chase/sig/android/activity/AccountActivityActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/chase/sig/android/activity/AccountActivityActivity;B)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 354
    invoke-direct {p0, p1}, Lcom/chase/sig/android/activity/AccountActivityActivity$b;-><init>(Lcom/chase/sig/android/activity/AccountActivityActivity;)V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 359
    :try_start_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountActivityActivity$b;->a:Lcom/chase/sig/android/activity/AccountActivityActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/AccountActivityActivity;->s()Lcom/chase/sig/android/service/SplashResponse;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountActivityActivity$b;->a:Lcom/chase/sig/android/activity/AccountActivityActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/AccountActivityActivity;->s()Lcom/chase/sig/android/service/SplashResponse;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/service/SplashResponse;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 361
    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountActivityActivity$b;->a:Lcom/chase/sig/android/activity/AccountActivityActivity;

    iget-object v1, p0, Lcom/chase/sig/android/activity/AccountActivityActivity$b;->a:Lcom/chase/sig/android/activity/AccountActivityActivity;

    invoke-virtual {v1}, Lcom/chase/sig/android/activity/AccountActivityActivity;->s()Lcom/chase/sig/android/service/SplashResponse;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/service/SplashResponse;->s()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/AccountActivityActivity;->e(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 367
    :goto_0
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1, p2, p3}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/widget/AdapterView;Landroid/view/View;I)V

    return-void

    .line 364
    :cond_0
    :try_start_1
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/AccountActivity;

    .line 365
    iget-object v1, p0, Lcom/chase/sig/android/activity/AccountActivityActivity$b;->a:Lcom/chase/sig/android/activity/AccountActivityActivity;

    invoke-static {v1, v0}, Lcom/chase/sig/android/activity/AccountActivityActivity;->a(Lcom/chase/sig/android/activity/AccountActivityActivity;Lcom/chase/sig/android/domain/AccountActivity;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 367
    :catch_0
    move-exception v0

    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1, p2, p3}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/widget/AdapterView;Landroid/view/View;I)V

    throw v0
.end method
