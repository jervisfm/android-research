.class public Lcom/chase/sig/android/activity/AccountActivityActivity$c;
.super Lcom/chase/sig/android/b;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/AccountActivityActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/b",
        "<",
        "Lcom/chase/sig/android/activity/AccountActivityActivity;",
        "Ljava/lang/Object;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/service/AccountActivityResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 136
    invoke-direct {p0}, Lcom/chase/sig/android/b;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .parameter

    .prologue
    .line 136
    const/4 v0, 0x0

    aget-object v0, p1, v0

    check-cast v0, Ljava/lang/String;

    const/4 v1, 0x1

    aget-object v1, p1, v1

    check-cast v1, Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v2, Lcom/chase/sig/android/activity/AccountActivityActivity;

    invoke-virtual {v2}, Lcom/chase/sig/android/activity/AccountActivityActivity;->A()Lcom/chase/sig/android/service/n;

    move-result-object v2

    iget-object v3, v2, Lcom/chase/sig/android/service/n;->c:Lcom/chase/sig/android/service/a;

    if-nez v3, :cond_0

    new-instance v3, Lcom/chase/sig/android/service/a;

    invoke-direct {v3}, Lcom/chase/sig/android/service/a;-><init>()V

    iput-object v3, v2, Lcom/chase/sig/android/service/n;->c:Lcom/chase/sig/android/service/a;

    :cond_0
    iget-object v2, v2, Lcom/chase/sig/android/service/n;->c:Lcom/chase/sig/android/service/a;

    invoke-static {}, Lcom/chase/sig/android/activity/AccountActivityActivity;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/chase/sig/android/activity/AccountActivityActivity;->b()Z

    move-result v3

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v0, v2, v3, v1}, Lcom/chase/sig/android/service/a;->a(Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/chase/sig/android/service/AccountActivityResponse;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 2
    .parameter

    .prologue
    .line 136
    check-cast p1, Lcom/chase/sig/android/service/AccountActivityResponse;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/chase/sig/android/service/AccountActivityResponse;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/AccountActivityActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/AccountActivityResponse;->i()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/AccountActivityActivity;->b(Ljava/util/List;)V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/chase/sig/android/service/AccountActivityResponse;->a()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/chase/sig/android/service/AccountActivityResponse;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/AccountActivityActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/AccountActivityActivity;->a(Lcom/chase/sig/android/activity/AccountActivityActivity;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Lcom/chase/sig/android/service/AccountActivityResponse;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/activity/AccountActivityActivity;->a(Ljava/lang/String;)Ljava/lang/String;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/AccountActivityResponse;->b()Z

    move-result v0

    invoke-static {v0}, Lcom/chase/sig/android/activity/AccountActivityActivity;->a(Z)Z

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/AccountActivityActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/AccountActivityActivity;->b(Lcom/chase/sig/android/activity/AccountActivityActivity;)Lcom/chase/sig/android/activity/AccountActivityActivity$a;

    move-result-object v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/AccountActivityActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/AccountActivityResponse;->a()Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/chase/sig/android/activity/AccountActivityActivity;->a(Lcom/chase/sig/android/activity/AccountActivityActivity;Ljava/util/List;)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/AccountActivityActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/AccountActivityActivity;->b(Lcom/chase/sig/android/activity/AccountActivityActivity;)Lcom/chase/sig/android/activity/AccountActivityActivity$a;

    move-result-object v0

    invoke-virtual {p1}, Lcom/chase/sig/android/service/AccountActivityResponse;->a()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/AccountActivityActivity$a;->a(Ljava/util/Collection;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/AccountActivityActivity;

    invoke-static {}, Lcom/chase/sig/android/activity/AccountActivityActivity;->b()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/chase/sig/android/activity/AccountActivityActivity;->a(Lcom/chase/sig/android/activity/AccountActivityActivity;Z)V

    goto :goto_0
.end method

.method protected onCancelled()V
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/AccountActivityActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/AccountActivityActivity;->finish()V

    .line 174
    return-void
.end method
