.class public Lcom/chase/sig/android/activity/AccountActivityActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$d;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/AccountActivityActivity$b;,
        Lcom/chase/sig/android/activity/AccountActivityActivity$a;,
        Lcom/chase/sig/android/activity/AccountActivityActivity$c;
    }
.end annotation


# static fields
.field private static d:Ljava/lang/String;

.field private static k:Z


# instance fields
.field private a:Ljava/lang/String;

.field private b:Landroid/widget/ListView;

.field private c:Landroid/view/View;

.field private l:Lcom/chase/sig/android/domain/IAccount;

.field private m:Ljava/lang/String;

.field private n:Lcom/chase/sig/android/activity/AccountActivityActivity$a;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    .line 354
    return-void
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/chase/sig/android/activity/AccountActivityActivity;->d:Ljava/lang/String;

    return-object v0
.end method

.method private a(Lcom/chase/sig/android/domain/IAccount;)Ljava/lang/String;
    .locals 5
    .parameter

    .prologue
    .line 126
    invoke-interface {p1}, Lcom/chase/sig/android/domain/IAccount;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 127
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/ChaseApplication;

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->b:Lcom/chase/sig/android/domain/g;

    invoke-interface {p1}, Lcom/chase/sig/android/domain/IAccount;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/chase/sig/android/domain/g;->e(Ljava/lang/String;)Lcom/chase/sig/android/domain/IAccount;

    move-result-object v0

    .line 129
    const-string v1, "%s (%s)"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-interface {p1}, Lcom/chase/sig/android/domain/IAccount;->a()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->c()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 133
    :goto_0
    return-object v0

    .line 131
    :cond_0
    invoke-interface {p1}, Lcom/chase/sig/android/domain/IAccount;->w()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .parameter

    .prologue
    .line 34
    sput-object p0, Lcom/chase/sig/android/activity/AccountActivityActivity;->d:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/AccountActivityActivity;)V
    .locals 4
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 34
    const v0, 0x7f090003

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/AccountActivityActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/ChaseApplication;

    invoke-virtual {v1}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v1

    iget-object v1, v1, Lcom/chase/sig/android/domain/o;->b:Lcom/chase/sig/android/domain/g;

    iget-object v2, p0, Lcom/chase/sig/android/activity/AccountActivityActivity;->a:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/chase/sig/android/domain/g;->a(Ljava/lang/String;)Lcom/chase/sig/android/domain/IAccount;

    move-result-object v1

    const v2, 0x7f090004

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-direct {p0, v1}, Lcom/chase/sig/android/activity/AccountActivityActivity;->a(Lcom/chase/sig/android/domain/IAccount;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    const v0, 0x7f090005

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/AccountActivityActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/AccountActivityActivity;Lcom/chase/sig/android/domain/AccountActivity;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 34
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/chase/sig/android/activity/TransactionDetailActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "transaction_details"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v1, "selectedAccountId"

    iget-object v2, p0, Lcom/chase/sig/android/activity/AccountActivityActivity;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "account_nickname_mask"

    iget-object v2, p0, Lcom/chase/sig/android/activity/AccountActivityActivity;->m:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/AccountActivityActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/AccountActivityActivity;Ljava/util/List;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/chase/sig/android/activity/AccountActivityActivity;->a(Ljava/util/List;)V

    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/AccountActivityActivity;Z)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 34
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountActivityActivity;->b:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/chase/sig/android/activity/AccountActivityActivity;->c:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->removeFooterView(Landroid/view/View;)Z

    :cond_0
    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 8
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/AccountActivity;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    const v6, 0x7f0900b3

    const/16 v5, 0xa

    const/4 v3, 0x5

    const/4 v4, 0x0

    .line 179
    new-instance v0, Lcom/chase/sig/android/activity/AccountActivityActivity$a;

    invoke-direct {v0, p0, p0, p1}, Lcom/chase/sig/android/activity/AccountActivityActivity$a;-><init>(Lcom/chase/sig/android/activity/AccountActivityActivity;Landroid/app/Activity;Ljava/util/List;)V

    iput-object v0, p0, Lcom/chase/sig/android/activity/AccountActivityActivity;->n:Lcom/chase/sig/android/activity/AccountActivityActivity$a;

    .line 182
    new-instance v0, Landroid/widget/ListView;

    invoke-direct {v0, p0}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/chase/sig/android/activity/AccountActivityActivity;->b:Landroid/widget/ListView;

    .line 184
    sget-boolean v0, Lcom/chase/sig/android/activity/AccountActivityActivity;->k:Z

    if-eqz v0, :cond_0

    .line 185
    const-string v0, "layout_inflater"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/AccountActivityActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v1, 0x7f030061

    invoke-virtual {v0, v1, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/AccountActivityActivity;->c:Landroid/view/View;

    .line 189
    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountActivityActivity;->c:Landroid/view/View;

    new-instance v1, Lcom/chase/sig/android/activity/o;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/o;-><init>(Lcom/chase/sig/android/activity/AccountActivityActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 200
    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountActivityActivity;->b:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/chase/sig/android/activity/AccountActivityActivity;->c:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;)V

    .line 203
    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountActivityActivity;->b:Landroid/widget/ListView;

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/ChaseApplication;->i()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/AccountActivityActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030031

    invoke-virtual {v1, v2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const v2, 0x7f060008

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundResource(I)V

    invoke-virtual {v1, v3, v5, v3, v5}, Landroid/view/View;->setPadding(IIII)V

    invoke-virtual {v1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;)V

    invoke-virtual {v1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const-class v1, Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/AccountActivityActivity;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/a/f;

    move-result-object v1

    const-string v3, "title"

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Lcom/chase/sig/android/activity/a/f;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/activity/a/f;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 205
    :cond_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountActivityActivity;->b:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/chase/sig/android/activity/AccountActivityActivity;->n:Lcom/chase/sig/android/activity/AccountActivityActivity$a;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 206
    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountActivityActivity;->b:Landroid/widget/ListView;

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setFooterDividersEnabled(Z)V

    .line 207
    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountActivityActivity;->b:Landroid/widget/ListView;

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setDividerHeight(I)V

    .line 208
    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountActivityActivity;->b:Landroid/widget/ListView;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/AccountActivityActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060020

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setCacheColorHint(I)V

    .line 210
    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountActivityActivity;->b:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->requestFocus()Z

    .line 212
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/ChaseApplication;

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->b:Lcom/chase/sig/android/domain/g;

    invoke-interface {v0}, Lcom/chase/sig/android/domain/g;->C()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 213
    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountActivityActivity;->l:Lcom/chase/sig/android/domain/IAccount;

    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->m()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 214
    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountActivityActivity;->b:Landroid/widget/ListView;

    new-instance v1, Lcom/chase/sig/android/activity/AccountActivityActivity$b;

    invoke-direct {v1, p0, v4}, Lcom/chase/sig/android/activity/AccountActivityActivity$b;-><init>(Lcom/chase/sig/android/activity/AccountActivityActivity;B)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 220
    :cond_2
    :goto_0
    const v0, 0x7f090003

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/AccountActivityActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 222
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/ChaseApplication;

    invoke-virtual {v1}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v1

    iget-object v1, v1, Lcom/chase/sig/android/domain/o;->b:Lcom/chase/sig/android/domain/g;

    iget-object v2, p0, Lcom/chase/sig/android/activity/AccountActivityActivity;->a:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/chase/sig/android/domain/g;->a(Ljava/lang/String;)Lcom/chase/sig/android/domain/IAccount;

    move-result-object v2

    .line 223
    const v1, 0x7f090004

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 225
    invoke-direct {p0, v2}, Lcom/chase/sig/android/activity/AccountActivityActivity;->a(Lcom/chase/sig/android/domain/IAccount;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/chase/sig/android/activity/AccountActivityActivity;->m:Ljava/lang/String;

    .line 226
    iget-object v2, p0, Lcom/chase/sig/android/activity/AccountActivityActivity;->m:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 227
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 229
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 231
    iget-object v2, p0, Lcom/chase/sig/android/activity/AccountActivityActivity;->b:Landroid/widget/ListView;

    invoke-virtual {v0, v2, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 232
    return-void

    .line 216
    :cond_3
    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountActivityActivity;->b:Landroid/widget/ListView;

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setClickable(Z)V

    goto :goto_0
.end method

.method static synthetic a(Z)Z
    .locals 0
    .parameter

    .prologue
    .line 34
    sput-boolean p0, Lcom/chase/sig/android/activity/AccountActivityActivity;->k:Z

    return p0
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/AccountActivityActivity;)Lcom/chase/sig/android/activity/AccountActivityActivity$a;
    .locals 1
    .parameter

    .prologue
    .line 34
    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountActivityActivity;->n:Lcom/chase/sig/android/activity/AccountActivityActivity$a;

    return-object v0
.end method

.method static synthetic b()Z
    .locals 1

    .prologue
    .line 34
    sget-boolean v0, Lcom/chase/sig/android/activity/AccountActivityActivity;->k:Z

    return v0
.end method

.method static synthetic c(Lcom/chase/sig/android/activity/AccountActivityActivity;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 34
    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountActivityActivity;->a:Ljava/lang/String;

    return-object v0
.end method

.method private d()Z
    .locals 1

    .prologue
    .line 351
    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountActivityActivity;->l:Lcom/chase/sig/android/domain/IAccount;

    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->D()Z

    move-result v0

    return v0
.end method

.method static synthetic d(Lcom/chase/sig/android/activity/AccountActivityActivity;)Z
    .locals 1
    .parameter

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/chase/sig/android/activity/AccountActivityActivity;->d()Z

    move-result v0

    return v0
.end method

.method static synthetic e(Lcom/chase/sig/android/activity/AccountActivityActivity;)Lcom/chase/sig/android/domain/IAccount;
    .locals 1
    .parameter

    .prologue
    .line 34
    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountActivityActivity;->l:Lcom/chase/sig/android/domain/IAccount;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 4
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 56
    const v0, 0x7f0700fe

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/AccountActivityActivity;->setTitle(I)V

    .line 57
    const/high16 v0, 0x7f03

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/AccountActivityActivity;->b(I)V

    .line 59
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/AccountActivityActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 60
    const-string v1, "selectedAccountId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/AccountActivityActivity;->a:Ljava/lang/String;

    .line 61
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/ChaseApplication;

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->b:Lcom/chase/sig/android/domain/g;

    iget-object v1, p0, Lcom/chase/sig/android/activity/AccountActivityActivity;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/chase/sig/android/domain/g;->a(Ljava/lang/String;)Lcom/chase/sig/android/domain/IAccount;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/AccountActivityActivity;->l:Lcom/chase/sig/android/domain/IAccount;

    .line 62
    sput-boolean v3, Lcom/chase/sig/android/activity/AccountActivityActivity;->k:Z

    .line 64
    if-eqz p1, :cond_1

    const-string v0, "activity"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 66
    const-string v0, "activity"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 68
    const-string v1, "activity_last_page"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/chase/sig/android/activity/AccountActivityActivity;->d:Ljava/lang/String;

    .line 69
    const-string v1, "activity_has_more"

    invoke-virtual {p1, v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    sput-boolean v1, Lcom/chase/sig/android/activity/AccountActivityActivity;->k:Z

    .line 71
    invoke-direct {p0, v0}, Lcom/chase/sig/android/activity/AccountActivityActivity;->a(Ljava/util/List;)V

    .line 73
    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountActivityActivity;->b:Landroid/widget/ListView;

    const-string v1, "activity_last_position"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 84
    :cond_0
    :goto_0
    return-void

    .line 78
    :cond_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountActivityActivity;->e:Lcom/chase/sig/android/activity/mc;

    const-class v1, Lcom/chase/sig/android/activity/AccountActivityActivity$c;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/mc;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/ae;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/activity/AccountActivityActivity$c;

    .line 80
    invoke-virtual {v0}, Lcom/chase/sig/android/activity/AccountActivityActivity$c;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v1

    sget-object v2, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-eq v1, v2, :cond_0

    .line 81
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/chase/sig/android/activity/AccountActivityActivity;->a:Ljava/lang/String;

    aput-object v2, v1, v3

    const/4 v2, 0x1

    invoke-direct {p0}, Lcom/chase/sig/android/activity/AccountActivityActivity;->d()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/AccountActivityActivity$c;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 101
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ai;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 103
    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountActivityActivity;->n:Lcom/chase/sig/android/activity/AccountActivityActivity$a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountActivityActivity;->n:Lcom/chase/sig/android/activity/AccountActivityActivity$a;

    invoke-static {v0}, Lcom/chase/sig/android/activity/AccountActivityActivity$a;->a(Lcom/chase/sig/android/activity/AccountActivityActivity$a;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 104
    const-string v1, "activity"

    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountActivityActivity;->n:Lcom/chase/sig/android/activity/AccountActivityActivity$a;

    invoke-static {v0}, Lcom/chase/sig/android/activity/AccountActivityActivity$a;->a(Lcom/chase/sig/android/activity/AccountActivityActivity$a;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/io/Serializable;

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 105
    const-string v0, "activity_last_page"

    sget-object v1, Lcom/chase/sig/android/activity/AccountActivityActivity;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    const-string v0, "activity_has_more"

    sget-boolean v1, Lcom/chase/sig/android/activity/AccountActivityActivity;->k:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 107
    const-string v0, "activity_last_position"

    iget-object v1, p0, Lcom/chase/sig/android/activity/AccountActivityActivity;->b:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 108
    const-string v0, "activity_last_position"

    iget-object v1, p0, Lcom/chase/sig/android/activity/AccountActivityActivity;->b:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 110
    :cond_0
    return-void
.end method
