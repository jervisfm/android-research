.class public Lcom/chase/sig/android/activity/AccountDetailActivity$a;
.super Lcom/chase/sig/android/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/AccountDetailActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/d",
        "<",
        "Lcom/chase/sig/android/activity/AccountDetailActivity;",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/service/AccountDetailResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 584
    invoke-direct {p0}, Lcom/chase/sig/android/d;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .parameter

    .prologue
    .line 584
    check-cast p1, [Ljava/lang/String;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/AccountDetailActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/AccountDetailActivity;->A()Lcom/chase/sig/android/service/n;

    move-result-object v0

    iget-object v1, v0, Lcom/chase/sig/android/service/n;->d:Lcom/chase/sig/android/service/b;

    if-nez v1, :cond_0

    new-instance v1, Lcom/chase/sig/android/service/b;

    invoke-direct {v1}, Lcom/chase/sig/android/service/b;-><init>()V

    iput-object v1, v0, Lcom/chase/sig/android/service/n;->d:Lcom/chase/sig/android/service/b;

    :cond_0
    iget-object v0, v0, Lcom/chase/sig/android/service/n;->d:Lcom/chase/sig/android/service/b;

    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-static {v0}, Lcom/chase/sig/android/service/b;->a(Ljava/lang/String;)Lcom/chase/sig/android/service/AccountDetailResponse;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 2
    .parameter

    .prologue
    .line 584
    check-cast p1, Lcom/chase/sig/android/service/AccountDetailResponse;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/AccountDetailResponse;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/AccountDetailActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/AccountDetailResponse;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/AccountDetailActivity;->b(Ljava/util/List;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/AccountDetailActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/AccountDetailResponse;->a()Lcom/chase/sig/android/domain/AccountDetail;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/AccountDetailActivity;->a(Lcom/chase/sig/android/domain/AccountDetail;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/AccountDetailActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/AccountDetailActivity;->a(Lcom/chase/sig/android/activity/AccountDetailActivity;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/AccountDetailActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/AccountDetailActivity;->b(Lcom/chase/sig/android/activity/AccountDetailActivity;)Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/detail/DetailView;->setVisibility(I)V

    goto :goto_0
.end method
