.class public Lcom/chase/sig/android/activity/AccountDetailActivity$b;
.super Lcom/chase/sig/android/b;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/AccountDetailActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/b",
        "<",
        "Lcom/chase/sig/android/activity/AccountDetailActivity;",
        "Landroid/os/Bundle;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/service/SmartService$SmartResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Landroid/os/Bundle;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 544
    invoke-direct {p0}, Lcom/chase/sig/android/b;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .parameter

    .prologue
    .line 544
    check-cast p1, [Landroid/os/Bundle;

    const/4 v0, 0x0

    aget-object v0, p1, v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/AccountDetailActivity$b;->a:Landroid/os/Bundle;

    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountDetailActivity$b;->a:Landroid/os/Bundle;

    const-string v1, "action"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/ui/Action;

    iget-object v1, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v1, Lcom/chase/sig/android/activity/AccountDetailActivity;

    invoke-virtual {v1}, Lcom/chase/sig/android/activity/AccountDetailActivity;->A()Lcom/chase/sig/android/service/n;

    move-result-object v1

    iget-object v2, v1, Lcom/chase/sig/android/service/n;->r:Lcom/chase/sig/android/service/SmartService;

    if-nez v2, :cond_0

    new-instance v2, Lcom/chase/sig/android/service/SmartService;

    invoke-direct {v2}, Lcom/chase/sig/android/service/SmartService;-><init>()V

    iput-object v2, v1, Lcom/chase/sig/android/service/n;->r:Lcom/chase/sig/android/service/SmartService;

    :cond_0
    iget-object v1, v1, Lcom/chase/sig/android/service/n;->r:Lcom/chase/sig/android/service/SmartService;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/ui/Action;->b()Lcom/chase/sig/android/domain/ui/Action$Url;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/service/SmartService;->a(Lcom/chase/sig/android/domain/ui/Action$Url;)Lcom/chase/sig/android/service/SmartService$SmartResponse;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 8
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 544
    check-cast p1, Lcom/chase/sig/android/service/SmartService$SmartResponse;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/SmartService$SmartResponse;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/AccountDetailActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/SmartService$SmartResponse;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/AccountDetailActivity;->c(Ljava/util/List;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/chase/sig/android/service/SmartService$SmartResponse;->a()Lcom/chase/sig/android/service/SmartService$SmartContent;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/service/SmartService$SmartContent;->a()[Lcom/chase/sig/android/service/SmartService$SmartSection;

    move-result-object v0

    aget-object v2, v0, v1

    invoke-virtual {v2}, Lcom/chase/sig/android/service/SmartService$SmartSection;->a()[Lcom/chase/sig/android/domain/Item;

    move-result-object v3

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/chase/sig/android/service/SmartService$SmartSection;->b()[Lcom/chase/sig/android/domain/ui/Action;

    move-result-object v0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/ui/Action;->a()Ljava/lang/String;

    move-result-object v4

    const-string v0, ""

    move-object v2, v0

    move v0, v1

    :goto_1
    array-length v5, v3

    if-ge v0, v5, :cond_1

    const-string v5, "\n%s"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    aget-object v7, v3, v0

    invoke-interface {v7}, Lcom/chase/sig/android/domain/f;->a()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v1

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/AccountDetailActivity;

    iget-object v1, p0, Lcom/chase/sig/android/activity/AccountDetailActivity$b;->a:Landroid/os/Bundle;

    const-string v3, "rowId"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v4, v2}, Lcom/chase/sig/android/activity/AccountDetailActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
