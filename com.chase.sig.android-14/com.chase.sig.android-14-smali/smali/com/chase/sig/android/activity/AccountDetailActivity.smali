.class public Lcom/chase/sig/android/activity/AccountDetailActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$d;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/AccountDetailActivity$a;,
        Lcom/chase/sig/android/activity/AccountDetailActivity$b;
    }
.end annotation


# instance fields
.field private a:Lcom/chase/sig/android/view/detail/DetailView;

.field private b:Lcom/chase/sig/android/domain/AccountDetail;

.field private c:Lcom/chase/sig/android/domain/IAccount;

.field private d:Lcom/chase/sig/android/domain/g;

.field private k:Landroid/os/Handler$Callback;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    .line 90
    new-instance v0, Lcom/chase/sig/android/activity/p;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/activity/p;-><init>(Lcom/chase/sig/android/activity/AccountDetailActivity;)V

    iput-object v0, p0, Lcom/chase/sig/android/activity/AccountDetailActivity;->k:Landroid/os/Handler$Callback;

    .line 584
    return-void
.end method

.method private a(Lcom/chase/sig/android/domain/IAccount;)Lcom/chase/sig/android/domain/IAccount;
    .locals 2
    .parameter

    .prologue
    .line 160
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/ChaseApplication;

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->b:Lcom/chase/sig/android/domain/g;

    invoke-interface {p1}, Lcom/chase/sig/android/domain/IAccount;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/chase/sig/android/domain/g;->e(Ljava/lang/String;)Lcom/chase/sig/android/domain/IAccount;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .parameter

    .prologue
    .line 504
    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountDetailActivity;->c:Lcom/chase/sig/android/domain/IAccount;

    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->e()Ljava/util/Hashtable;

    move-result-object v0

    .line 505
    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 506
    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 508
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/AccountDetailActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/chase/sig/android/activity/AccountDetailActivity;->d()V

    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/AccountDetailActivity;Landroid/os/Bundle;)V
    .locals 4
    .parameter
    .parameter

    .prologue
    .line 38
    const-string v0, "action"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/ui/Action;

    const-string v1, "ROW_ID"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/chase/sig/android/activity/AccountDetailActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    invoke-virtual {v2, v1}, Lcom/chase/sig/android/view/detail/DetailView;->e(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v2

    if-eqz v2, :cond_0

    instance-of v1, v2, Lcom/chase/sig/android/view/detail/TogglableDetailRow;

    if-eqz v1, :cond_0

    move-object v1, v2

    check-cast v1, Lcom/chase/sig/android/view/detail/TogglableDetailRow;

    invoke-virtual {v1}, Lcom/chase/sig/android/view/detail/TogglableDetailRow;->q()Z

    move-result v3

    if-nez v3, :cond_1

    const-string v1, "rowId"

    iget-object v2, v2, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "action"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    const-class v0, Lcom/chase/sig/android/activity/AccountDetailActivity$b;

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/os/Bundle;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/AccountDetailActivity;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v1}, Lcom/chase/sig/android/view/detail/TogglableDetailRow;->p()V

    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountDetailActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/DetailView;->c()V

    goto :goto_0
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/AccountDetailActivity;)Lcom/chase/sig/android/view/detail/DetailView;
    .locals 1
    .parameter

    .prologue
    .line 38
    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountDetailActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    return-object v0
.end method

.method private b(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .parameter

    .prologue
    .line 513
    invoke-direct {p0, p1}, Lcom/chase/sig/android/activity/AccountDetailActivity;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 515
    invoke-static {v0}, Lcom/chase/sig/android/util/s;->l(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v0, "--"

    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lcom/chase/sig/android/util/s;->h(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private c(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .parameter

    .prologue
    .line 521
    invoke-direct {p0, p1}, Lcom/chase/sig/android/activity/AccountDetailActivity;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 523
    invoke-static {v0}, Lcom/chase/sig/android/util/s;->l(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 524
    const-string v0, "--"

    .line 529
    :goto_0
    return-object v0

    .line 527
    :cond_0
    new-instance v1, Lcom/chase/sig/android/util/Dollar;

    invoke-direct {v1, v0}, Lcom/chase/sig/android/util/Dollar;-><init>(Ljava/lang/String;)V

    .line 529
    invoke-virtual {v1}, Lcom/chase/sig/android/util/Dollar;->h()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private d()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const v7, 0x7f0700d9

    const v6, 0x7f0700d8

    const v5, 0x7f0700d7

    const/4 v4, 0x0

    .line 188
    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountDetailActivity;->c:Lcom/chase/sig/android/domain/IAccount;

    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->s()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 211
    :pswitch_0
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/AccountDetailActivity;->finish()V

    .line 213
    :goto_0
    return-void

    .line 190
    :pswitch_1
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    new-instance v0, Lcom/chase/sig/android/view/detail/DetailRow;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/AccountDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0700d1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "current"

    invoke-direct {p0, v2}, Lcom/chase/sig/android/activity/AccountDetailActivity;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/chase/sig/android/view/detail/DetailRow;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/AccountDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0700d2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "available"

    invoke-direct {p0, v2}, Lcom/chase/sig/android/activity/AccountDetailActivity;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountDetailActivity;->c:Lcom/chase/sig/android/domain/IAccount;

    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->I()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountDetailActivity;->c:Lcom/chase/sig/android/domain/IAccount;

    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->e()Ljava/util/Hashtable;

    move-result-object v0

    const-string v1, "txnCounter"

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/chase/sig/android/activity/AccountDetailActivity;->c:Lcom/chase/sig/android/domain/IAccount;

    invoke-interface {v1}, Lcom/chase/sig/android/domain/IAccount;->e()Ljava/util/Hashtable;

    move-result-object v1

    const-string v2, "txnCounterMsg"

    invoke-virtual {v1, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v2, p0, Lcom/chase/sig/android/activity/AccountDetailActivity;->c:Lcom/chase/sig/android/domain/IAccount;

    const-string v3, "Withdrawals this period"

    invoke-interface {v2, v3}, Lcom/chase/sig/android/domain/IAccount;->c(Ljava/lang/String;)Lcom/chase/sig/android/domain/f;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v2}, Lcom/chase/sig/android/domain/f;->c()Lcom/chase/sig/android/domain/Account$Value;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Account$Value;->b()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2}, Lcom/chase/sig/android/domain/f;->b()Ljava/lang/String;

    move-result-object v1

    :cond_0
    new-instance v2, Lcom/chase/sig/android/view/detail/DetailRowWithSubItem;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/AccountDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v6, 0x7f0700fa

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v1, v0}, Lcom/chase/sig/android/view/detail/DetailRowWithSubItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/AccountDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "accountDetails"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    if-eqz v0, :cond_3

    check-cast v0, Ljava/util/List;

    move v3, v4

    :goto_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-ge v3, v1, :cond_3

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/domain/f;

    invoke-interface {v1}, Lcom/chase/sig/android/domain/f;->d()Ljava/lang/String;

    move-result-object v2

    const-string v6, "SHOW_HIDE_LIST"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Lcom/chase/sig/android/domain/f;->e()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/chase/sig/android/domain/ui/Action;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/ui/Action;->a()Ljava/lang/String;

    move-result-object v2

    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    const-string v7, "action"

    invoke-interface {v1}, Lcom/chase/sig/android/domain/f;->e()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/Serializable;

    invoke-virtual {v6, v7, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    new-instance v1, Lcom/chase/sig/android/view/detail/TogglableDetailRow;

    const-string v7, ""

    iget-object v8, p0, Lcom/chase/sig/android/activity/AccountDetailActivity;->k:Landroid/os/Handler$Callback;

    invoke-direct {v1, v2, v7, v8, v6}, Lcom/chase/sig/android/view/detail/TogglableDetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/os/Handler$Callback;Landroid/os/Bundle;)V

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v6, "extraDetailRow"

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1

    :cond_3
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lcom/chase/sig/android/view/detail/a;

    iget-object v1, p0, Lcom/chase/sig/android/activity/AccountDetailActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/chase/sig/android/view/detail/a;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/view/detail/DetailView;->setRows([Lcom/chase/sig/android/view/detail/a;)V

    goto/16 :goto_0

    .line 193
    :pswitch_2
    invoke-direct {p0}, Lcom/chase/sig/android/activity/AccountDetailActivity;->e()V

    goto/16 :goto_0

    .line 197
    :pswitch_3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lcom/chase/sig/android/activity/AccountDetailActivity;->c:Lcom/chase/sig/android/domain/IAccount;

    invoke-interface {v1}, Lcom/chase/sig/android/domain/IAccount;->E()Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/chase/sig/android/activity/AccountDetailActivity;->c:Lcom/chase/sig/android/domain/IAccount;

    invoke-interface {v1}, Lcom/chase/sig/android/domain/IAccount;->h()Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "spendingLimitFlag"

    invoke-direct {p0, v1}, Lcom/chase/sig/android/activity/AccountDetailActivity;->h(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_a

    :cond_4
    new-instance v1, Lcom/chase/sig/android/view/detail/DetailRow;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/AccountDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0700da

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "current"

    invoke-direct {p0, v3}, Lcom/chase/sig/android/activity/AccountDetailActivity;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/chase/sig/android/view/detail/DetailRow;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/AccountDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0700db

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "nextPaymentDate"

    invoke-direct {p0, v3}, Lcom/chase/sig/android/activity/AccountDetailActivity;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/chase/sig/android/view/detail/DetailRow;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/AccountDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0700e7

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "nextPaymentAmount"

    invoke-direct {p0, v3}, Lcom/chase/sig/android/activity/AccountDetailActivity;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/chase/sig/android/view/detail/DetailRow;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/AccountDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0700dd

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "lastStmtBalance"

    invoke-direct {p0, v3}, Lcom/chase/sig/android/activity/AccountDetailActivity;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_2
    iget-object v1, p0, Lcom/chase/sig/android/activity/AccountDetailActivity;->c:Lcom/chase/sig/android/domain/IAccount;

    invoke-interface {v1}, Lcom/chase/sig/android/domain/IAccount;->l()Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, "goalPayAmountDue"

    invoke-direct {p0, v1}, Lcom/chase/sig/android/activity/AccountDetailActivity;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/chase/sig/android/view/detail/DetailRowWithSubItem;

    const v3, 0x7f0700ef

    invoke-virtual {p0, v3}, Lcom/chase/sig/android/activity/AccountDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0700f0

    invoke-virtual {p0, v4}, Lcom/chase/sig/android/activity/AccountDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4, v1}, Lcom/chase/sig/android/view/detail/DetailRowWithSubItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_5
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Lcom/chase/sig/android/view/detail/a;

    iget-object v2, p0, Lcom/chase/sig/android/activity/AccountDetailActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/chase/sig/android/view/detail/a;

    invoke-virtual {v2, v0}, Lcom/chase/sig/android/view/detail/DetailView;->setRows([Lcom/chase/sig/android/view/detail/a;)V

    goto/16 :goto_0

    :cond_6
    const-string v1, "spendingLimitFlag"

    invoke-direct {p0, v1}, Lcom/chase/sig/android/activity/AccountDetailActivity;->h(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_a

    new-instance v1, Lcom/chase/sig/android/view/detail/DetailRow;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/AccountDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0700da

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "current"

    invoke-direct {p0, v3}, Lcom/chase/sig/android/activity/AccountDetailActivity;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/chase/sig/android/activity/AccountDetailActivity;->c:Lcom/chase/sig/android/domain/IAccount;

    invoke-interface {v1}, Lcom/chase/sig/android/domain/IAccount;->i()Z

    move-result v1

    if-nez v1, :cond_7

    iget-object v1, p0, Lcom/chase/sig/android/activity/AccountDetailActivity;->c:Lcom/chase/sig/android/domain/IAccount;

    invoke-interface {v1}, Lcom/chase/sig/android/domain/IAccount;->h()Z

    move-result v1

    if-eqz v1, :cond_9

    :cond_7
    new-instance v1, Lcom/chase/sig/android/view/detail/DetailRow;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/AccountDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0700d5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "spendingLimit"

    invoke-direct {p0, v3}, Lcom/chase/sig/android/activity/AccountDetailActivity;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/chase/sig/android/activity/AccountDetailActivity;->c:Lcom/chase/sig/android/domain/IAccount;

    invoke-interface {v1}, Lcom/chase/sig/android/domain/IAccount;->i()Z

    move-result v1

    if-eqz v1, :cond_8

    new-instance v1, Lcom/chase/sig/android/view/detail/DetailRow;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/AccountDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "cardCashAdvanceBalance"

    invoke-direct {p0, v3}, Lcom/chase/sig/android/activity/AccountDetailActivity;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/chase/sig/android/view/detail/DetailRow;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/AccountDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "cardCashAdvanceLimit"

    invoke-direct {p0, v3}, Lcom/chase/sig/android/activity/AccountDetailActivity;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/chase/sig/android/view/detail/DetailRow;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/AccountDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "cardCashAdvanceAvailable"

    invoke-direct {p0, v3}, Lcom/chase/sig/android/activity/AccountDetailActivity;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    :cond_8
    new-instance v1, Lcom/chase/sig/android/view/detail/DetailRow;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/AccountDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "cashAdvanceBalance"

    invoke-direct {p0, v3}, Lcom/chase/sig/android/activity/AccountDetailActivity;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/chase/sig/android/view/detail/DetailRow;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/AccountDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "cashAdvanceLimit"

    invoke-direct {p0, v3}, Lcom/chase/sig/android/activity/AccountDetailActivity;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/chase/sig/android/view/detail/DetailRow;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/AccountDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "cashAdvanceAvailable"

    invoke-direct {p0, v3}, Lcom/chase/sig/android/activity/AccountDetailActivity;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    :cond_9
    new-instance v1, Lcom/chase/sig/android/view/detail/DetailRow;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/AccountDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0700e4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "nextPaymentDate"

    invoke-direct {p0, v3}, Lcom/chase/sig/android/activity/AccountDetailActivity;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/chase/sig/android/view/detail/DetailRow;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/AccountDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0700dc

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "nextPaymentAmount"

    invoke-direct {p0, v3}, Lcom/chase/sig/android/activity/AccountDetailActivity;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/chase/sig/android/view/detail/DetailRow;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/AccountDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0700dd

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "lastStmtBalance"

    invoke-direct {p0, v3}, Lcom/chase/sig/android/activity/AccountDetailActivity;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/chase/sig/android/view/detail/DetailRow;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/AccountDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0700de

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "available"

    invoke-direct {p0, v3}, Lcom/chase/sig/android/activity/AccountDetailActivity;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/chase/sig/android/view/detail/DetailRow;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/AccountDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0700df

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "creditLimit"

    invoke-direct {p0, v3}, Lcom/chase/sig/android/activity/AccountDetailActivity;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    :cond_a
    new-instance v1, Lcom/chase/sig/android/view/detail/DetailRow;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/AccountDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0700d3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "spendingBalance"

    invoke-direct {p0, v3}, Lcom/chase/sig/android/activity/AccountDetailActivity;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/chase/sig/android/view/detail/DetailRow;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/AccountDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0700d4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "spendingLimit"

    invoke-direct {p0, v3}, Lcom/chase/sig/android/activity/AccountDetailActivity;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/chase/sig/android/view/detail/DetailRow;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/AccountDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0700d6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "spendingLimitAvailable"

    invoke-direct {p0, v3}, Lcom/chase/sig/android/activity/AccountDetailActivity;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/chase/sig/android/view/detail/DetailRow;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/AccountDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "cardCashAdvanceBalance"

    invoke-direct {p0, v3}, Lcom/chase/sig/android/activity/AccountDetailActivity;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/chase/sig/android/view/detail/DetailRow;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/AccountDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "cardCashAdvanceLimit"

    invoke-direct {p0, v3}, Lcom/chase/sig/android/activity/AccountDetailActivity;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/chase/sig/android/view/detail/DetailRow;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/AccountDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "cardCashAdvanceAvailable"

    invoke-direct {p0, v3}, Lcom/chase/sig/android/activity/AccountDetailActivity;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 200
    :pswitch_4
    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountDetailActivity;->c:Lcom/chase/sig/android/domain/IAccount;

    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->d()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ALA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v0, 0x5

    new-array v2, v0, [Lcom/chase/sig/android/view/detail/DetailRow;

    new-instance v0, Lcom/chase/sig/android/view/detail/DetailRow;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/AccountDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f0700eb

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v5, "current"

    invoke-direct {p0, v5}, Lcom/chase/sig/android/activity/AccountDetailActivity;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v3, v5}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-boolean v1, v0, Lcom/chase/sig/android/view/detail/a;->j:Z

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailRow;

    aput-object v0, v2, v4

    new-instance v0, Lcom/chase/sig/android/view/detail/DetailRow;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/AccountDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0700db

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v3, "nextPaymentAmount"

    invoke-direct {p0, v3}, Lcom/chase/sig/android/activity/AccountDetailActivity;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v3}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v0, v2, v8

    const/4 v0, 0x2

    new-instance v1, Lcom/chase/sig/android/view/detail/DetailRow;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/AccountDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0700ec

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "nextPaymentDate"

    invoke-direct {p0, v4}, Lcom/chase/sig/android/activity/AccountDetailActivity;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v3, v4}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v1, v2, v0

    const/4 v0, 0x3

    new-instance v1, Lcom/chase/sig/android/view/detail/DetailRow;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/AccountDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0700e0

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "lastPaymentDate"

    invoke-direct {p0, v4}, Lcom/chase/sig/android/activity/AccountDetailActivity;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v3, v4}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v1, v2, v0

    const/4 v0, 0x4

    new-instance v1, Lcom/chase/sig/android/view/detail/DetailRow;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/AccountDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0700e2

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "lastPaymentAmount"

    invoke-direct {p0, v4}, Lcom/chase/sig/android/activity/AccountDetailActivity;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v3, v4}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v1, v2, v0

    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountDetailActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/view/detail/DetailView;->setRows([Lcom/chase/sig/android/view/detail/a;)V

    goto/16 :goto_0

    .line 203
    :pswitch_5
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountDetailActivity;->c:Lcom/chase/sig/android/domain/IAccount;

    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->n()Z

    move-result v0

    new-instance v2, Lcom/chase/sig/android/view/detail/DetailRow;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/AccountDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0700e0

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "lastPaymentDate"

    invoke-direct {p0, v4}, Lcom/chase/sig/android/activity/AccountDetailActivity;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/chase/sig/android/view/detail/DetailRow;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/AccountDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0700e1

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "current"

    invoke-direct {p0, v4}, Lcom/chase/sig/android/activity/AccountDetailActivity;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/chase/sig/android/view/detail/DetailRow;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/AccountDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0700e2

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "lastPaymentAmount"

    invoke-direct {p0, v4}, Lcom/chase/sig/android/activity/AccountDetailActivity;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-eqz v0, :cond_b

    new-instance v0, Lcom/chase/sig/android/view/detail/DetailRow;

    const v2, 0x7f0700e5

    invoke-virtual {p0, v2}, Lcom/chase/sig/android/activity/AccountDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "nextPaymentAmount"

    invoke-direct {p0, v3}, Lcom/chase/sig/android/activity/AccountDetailActivity;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v2, ""

    const v3, 0x7f0700f1

    invoke-virtual {p0, v3}, Lcom/chase/sig/android/activity/AccountDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const v2, 0x7f030004

    iput v2, v0, Lcom/chase/sig/android/view/detail/a;->i:I

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/AccountDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/high16 v3, 0x7f06

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/view/detail/DetailRow;->e(I)Lcom/chase/sig/android/view/detail/DetailRow;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_3
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lcom/chase/sig/android/view/detail/a;

    iget-object v2, p0, Lcom/chase/sig/android/activity/AccountDetailActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/chase/sig/android/view/detail/a;

    invoke-virtual {v2, v0}, Lcom/chase/sig/android/view/detail/DetailView;->setRows([Lcom/chase/sig/android/view/detail/a;)V

    goto/16 :goto_0

    :cond_b
    const-string v0, "HMG"

    iget-object v2, p0, Lcom/chase/sig/android/activity/AccountDetailActivity;->c:Lcom/chase/sig/android/domain/IAccount;

    invoke-interface {v2}, Lcom/chase/sig/android/domain/IAccount;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    const-string v0, "nextPaymentDate"

    :goto_4
    new-instance v2, Lcom/chase/sig/android/view/detail/DetailRow;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/AccountDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0700de

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "available"

    invoke-direct {p0, v4}, Lcom/chase/sig/android/activity/AccountDetailActivity;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "HMG"

    iget-object v4, p0, Lcom/chase/sig/android/activity/AccountDetailActivity;->c:Lcom/chase/sig/android/domain/IAccount;

    invoke-interface {v4}, Lcom/chase/sig/android/domain/IAccount;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    iput-boolean v3, v2, Lcom/chase/sig/android/view/detail/a;->j:Z

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/chase/sig/android/view/detail/DetailRow;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/AccountDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0700e3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v0}, Lcom/chase/sig/android/activity/AccountDetailActivity;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/chase/sig/android/view/detail/DetailRow;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/AccountDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0700e6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "nextPaymentAmount"

    invoke-direct {p0, v3}, Lcom/chase/sig/android/activity/AccountDetailActivity;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_c
    const-string v0, "oldestDueDate"

    goto :goto_4

    .line 206
    :pswitch_6
    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountDetailActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    const/4 v1, 0x5

    new-array v1, v1, [Lcom/chase/sig/android/view/detail/a;

    new-instance v2, Lcom/chase/sig/android/view/detail/DetailRow;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/AccountDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f0700d1

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v5, "current"

    invoke-direct {p0, v5}, Lcom/chase/sig/android/activity/AccountDetailActivity;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v3, v5}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v1, v4

    new-instance v2, Lcom/chase/sig/android/view/detail/DetailRow;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/AccountDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0700e8

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "available"

    invoke-direct {p0, v4}, Lcom/chase/sig/android/activity/AccountDetailActivity;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v1, v8

    const/4 v2, 0x2

    new-instance v3, Lcom/chase/sig/android/view/detail/DetailRow;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/AccountDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0700e9

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "creditLine"

    invoke-direct {p0, v5}, Lcom/chase/sig/android/activity/AccountDetailActivity;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    const/4 v2, 0x3

    new-instance v3, Lcom/chase/sig/android/view/detail/DetailRow;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/AccountDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0700ea

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "lastPaymentAmount"

    invoke-direct {p0, v5}, Lcom/chase/sig/android/activity/AccountDetailActivity;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    const/4 v2, 0x4

    new-instance v3, Lcom/chase/sig/android/view/detail/DetailRow;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/AccountDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0700e3

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "nextPaymentDate"

    invoke-direct {p0, v5}, Lcom/chase/sig/android/activity/AccountDetailActivity;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/detail/DetailView;->setRows([Lcom/chase/sig/android/view/detail/a;)V

    goto/16 :goto_0

    .line 188
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_3
        :pswitch_3
        :pswitch_5
        :pswitch_4
        :pswitch_6
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private e()V
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 353
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 356
    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountDetailActivity;->c:Lcom/chase/sig/android/domain/IAccount;

    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->M()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 357
    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountDetailActivity;->c:Lcom/chase/sig/android/domain/IAccount;

    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->K()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/f;

    .line 358
    invoke-interface {v0}, Lcom/chase/sig/android/domain/f;->d()Ljava/lang/String;

    move-result-object v1

    const-string v3, "SHOW_HIDE_LIST"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 359
    new-instance v3, Lcom/chase/sig/android/view/detail/DetailRow;

    invoke-interface {v0}, Lcom/chase/sig/android/domain/f;->a()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v0}, Lcom/chase/sig/android/domain/f;->c()Lcom/chase/sig/android/domain/Account$Value;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/Account$Value;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0}, Lcom/chase/sig/android/domain/f;->c()Lcom/chase/sig/android/domain/Account$Value;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Account$Value;->a()Ljava/lang/String;

    move-result-object v0

    const-string v7, "CURRENCY"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    new-instance v0, Lcom/chase/sig/android/util/Dollar;

    invoke-direct {v0, v1}, Lcom/chase/sig/android/util/Dollar;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/chase/sig/android/util/Dollar;->h()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-direct {v3, v6, v0}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    const-string v7, "DATE"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->l(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->B(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const-string v0, "--"

    goto :goto_1

    :cond_3
    invoke-static {v1}, Lcom/chase/sig/android/util/s;->h(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_4
    move-object v0, v1

    goto :goto_1

    .line 365
    :cond_5
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/AccountDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 366
    const-string v1, "accountDetails"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    .line 367
    if-eqz v0, :cond_7

    .line 370
    check-cast v0, Ljava/util/List;

    move v3, v4

    .line 371
    :goto_2
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-ge v3, v1, :cond_7

    .line 372
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/domain/f;

    .line 373
    invoke-interface {v1}, Lcom/chase/sig/android/domain/f;->d()Ljava/lang/String;

    move-result-object v2

    const-string v6, "SHOW_HIDE_LIST"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 374
    invoke-interface {v1}, Lcom/chase/sig/android/domain/f;->e()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/chase/sig/android/domain/ui/Action;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/ui/Action;->a()Ljava/lang/String;

    move-result-object v2

    .line 376
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 377
    const-string v7, "action"

    invoke-interface {v1}, Lcom/chase/sig/android/domain/f;->e()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/Serializable;

    invoke-virtual {v6, v7, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 379
    new-instance v1, Lcom/chase/sig/android/view/detail/TogglableDetailRow;

    const-string v7, ""

    iget-object v8, p0, Lcom/chase/sig/android/activity/AccountDetailActivity;->k:Landroid/os/Handler$Callback;

    invoke-direct {v1, v2, v7, v8, v6}, Lcom/chase/sig/android/view/detail/TogglableDetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/os/Handler$Callback;Landroid/os/Bundle;)V

    .line 382
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v6, "extraDetailRow"

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    .line 383
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 371
    :cond_6
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_2

    .line 389
    :cond_7
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lcom/chase/sig/android/view/detail/a;

    .line 390
    iget-object v1, p0, Lcom/chase/sig/android/activity/AccountDetailActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/chase/sig/android/view/detail/a;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/view/detail/DetailView;->setRows([Lcom/chase/sig/android/view/detail/a;)V

    .line 391
    return-void
.end method

.method private h(Ljava/lang/String;)Z
    .locals 2
    .parameter

    .prologue
    .line 534
    invoke-direct {p0, p1}, Lcom/chase/sig/android/activity/AccountDetailActivity;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 536
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Y"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 10
    .parameter

    .prologue
    const v9, 0x7f0900b3

    const/16 v8, 0xa

    const/4 v7, 0x5

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 58
    const v0, 0x7f030002

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/AccountDetailActivity;->b(I)V

    .line 59
    const v0, 0x7f0700d0

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/AccountDetailActivity;->setTitle(I)V

    .line 61
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/AccountDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 62
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/ChaseApplication;

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->b:Lcom/chase/sig/android/domain/g;

    iput-object v0, p0, Lcom/chase/sig/android/activity/AccountDetailActivity;->d:Lcom/chase/sig/android/domain/g;

    .line 64
    const-string v0, "selectedAccountId"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 65
    iget-object v1, p0, Lcom/chase/sig/android/activity/AccountDetailActivity;->d:Lcom/chase/sig/android/domain/g;

    invoke-interface {v1, v0}, Lcom/chase/sig/android/domain/g;->a(Ljava/lang/String;)Lcom/chase/sig/android/domain/IAccount;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/AccountDetailActivity;->c:Lcom/chase/sig/android/domain/IAccount;

    .line 67
    const v0, 0x7f09000d

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/AccountDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 69
    iget-object v1, p0, Lcom/chase/sig/android/activity/AccountDetailActivity;->c:Lcom/chase/sig/android/domain/IAccount;

    invoke-interface {v1}, Lcom/chase/sig/android/domain/IAccount;->L()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 70
    const v1, 0x7f09000e

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/AccountDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 71
    iget-object v2, p0, Lcom/chase/sig/android/activity/AccountDetailActivity;->c:Lcom/chase/sig/android/domain/IAccount;

    invoke-interface {v2}, Lcom/chase/sig/android/domain/IAccount;->L()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 72
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 75
    :cond_0
    iget-object v1, p0, Lcom/chase/sig/android/activity/AccountDetailActivity;->c:Lcom/chase/sig/android/domain/IAccount;

    invoke-interface {v1}, Lcom/chase/sig/android/domain/IAccount;->g()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/chase/sig/android/activity/AccountDetailActivity;->c:Lcom/chase/sig/android/domain/IAccount;

    invoke-direct {p0, v1}, Lcom/chase/sig/android/activity/AccountDetailActivity;->a(Lcom/chase/sig/android/domain/IAccount;)Lcom/chase/sig/android/domain/IAccount;

    move-result-object v1

    const-string v2, "%s (%s)"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/chase/sig/android/activity/AccountDetailActivity;->c:Lcom/chase/sig/android/domain/IAccount;

    invoke-interface {v4}, Lcom/chase/sig/android/domain/IAccount;->a()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-interface {v1}, Lcom/chase/sig/android/domain/IAccount;->c()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 77
    const v0, 0x7f09000f

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/AccountDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/AccountDetailActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    .line 78
    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountDetailActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/DetailView;->d()V

    .line 82
    if-nez p1, :cond_5

    .line 83
    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountDetailActivity;->c:Lcom/chase/sig/android/domain/IAccount;

    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->h()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountDetailActivity;->e:Lcom/chase/sig/android/activity/mc;

    const-class v1, Lcom/chase/sig/android/activity/AccountDetailActivity$a;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/mc;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/ae;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/activity/AccountDetailActivity$a;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/AccountDetailActivity$a;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v1

    sget-object v2, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-eq v1, v2, :cond_1

    new-array v2, v6, [Ljava/lang/String;

    iget-object v1, p0, Lcom/chase/sig/android/activity/AccountDetailActivity;->c:Lcom/chase/sig/android/domain/IAccount;

    invoke-interface {v1}, Lcom/chase/sig/android/domain/IAccount;->g()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/chase/sig/android/activity/AccountDetailActivity;->c:Lcom/chase/sig/android/domain/IAccount;

    invoke-direct {p0, v1}, Lcom/chase/sig/android/activity/AccountDetailActivity;->a(Lcom/chase/sig/android/domain/IAccount;)Lcom/chase/sig/android/domain/IAccount;

    move-result-object v1

    invoke-interface {v1}, Lcom/chase/sig/android/domain/IAccount;->b()Ljava/lang/String;

    move-result-object v1

    :goto_1
    aput-object v1, v2, v5

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/activity/AccountDetailActivity$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 88
    :cond_1
    :goto_2
    return-void

    .line 75
    :cond_2
    iget-object v1, p0, Lcom/chase/sig/android/activity/AccountDetailActivity;->c:Lcom/chase/sig/android/domain/IAccount;

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/chase/sig/android/ChaseApplication;->i()Z

    move-result v2

    invoke-interface {v1, v2}, Lcom/chase/sig/android/domain/IAccount;->a(Z)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 83
    :cond_3
    iget-object v1, p0, Lcom/chase/sig/android/activity/AccountDetailActivity;->c:Lcom/chase/sig/android/domain/IAccount;

    invoke-interface {v1}, Lcom/chase/sig/android/domain/IAccount;->b()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_4
    new-instance v0, Lcom/chase/sig/android/domain/AccountDetail;

    invoke-direct {v0}, Lcom/chase/sig/android/domain/AccountDetail;-><init>()V

    iput-object v0, p0, Lcom/chase/sig/android/activity/AccountDetailActivity;->b:Lcom/chase/sig/android/domain/AccountDetail;

    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountDetailActivity;->b:Lcom/chase/sig/android/domain/AccountDetail;

    iget-object v1, p0, Lcom/chase/sig/android/activity/AccountDetailActivity;->c:Lcom/chase/sig/android/domain/IAccount;

    invoke-interface {v1}, Lcom/chase/sig/android/domain/IAccount;->e()Ljava/util/Hashtable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/AccountDetail;->a(Ljava/util/Hashtable;)V

    invoke-direct {p0}, Lcom/chase/sig/android/activity/AccountDetailActivity;->d()V

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/AccountDetailActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030031

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f060008

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    invoke-virtual {v0, v7, v8, v7, v8}, Landroid/view/View;->setPadding(IIII)V

    invoke-virtual {v0, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/chase/sig/android/activity/AccountDetailActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    invoke-virtual {v2, v0}, Lcom/chase/sig/android/view/detail/DetailView;->addView(Landroid/view/View;)V

    invoke-virtual {v0, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const-class v2, Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;

    invoke-virtual {p0, v2}, Lcom/chase/sig/android/activity/AccountDetailActivity;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/a/f;

    move-result-object v2

    const-string v3, "title"

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Lcom/chase/sig/android/activity/a/f;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/activity/a/f;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2

    .line 84
    :cond_5
    const-string v0, "detailRows"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 85
    iget-object v1, p0, Lcom/chase/sig/android/activity/AccountDetailActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    const-string v0, "detailRows"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, [Lcom/chase/sig/android/view/detail/a;

    check-cast v0, [Lcom/chase/sig/android/view/detail/a;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/view/detail/DetailView;->setRows([Lcom/chase/sig/android/view/detail/a;)V

    goto/16 :goto_2
.end method

.method public final a(Lcom/chase/sig/android/domain/AccountDetail;)V
    .locals 0
    .parameter

    .prologue
    .line 607
    iput-object p1, p0, Lcom/chase/sig/android/activity/AccountDetailActivity;->b:Lcom/chase/sig/android/domain/AccountDetail;

    .line 608
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .parameter
    .parameter
    .parameter

    .prologue
    .line 611
    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountDetailActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    invoke-virtual {v0, p1}, Lcom/chase/sig/android/view/detail/DetailView;->e(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    .line 612
    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/chase/sig/android/view/detail/TogglableDetailRow;

    if-eqz v1, :cond_0

    .line 613
    check-cast v0, Lcom/chase/sig/android/view/detail/TogglableDetailRow;

    .line 614
    invoke-virtual {v0, p2, p3}, Lcom/chase/sig/android/view/detail/TogglableDetailRow;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/TogglableDetailRow;->p()V

    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountDetailActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/DetailView;->c()V

    .line 616
    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 177
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ai;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 179
    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountDetailActivity;->b:Lcom/chase/sig/android/domain/AccountDetail;

    if-eqz v0, :cond_0

    .line 180
    const-string v0, "accountDetails"

    iget-object v1, p0, Lcom/chase/sig/android/activity/AccountDetailActivity;->b:Lcom/chase/sig/android/domain/AccountDetail;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 183
    :cond_0
    const-string v0, "detailRows"

    iget-object v1, p0, Lcom/chase/sig/android/activity/AccountDetailActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    invoke-virtual {v1}, Lcom/chase/sig/android/view/detail/DetailView;->getRows()[Lcom/chase/sig/android/view/detail/a;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 184
    return-void
.end method
