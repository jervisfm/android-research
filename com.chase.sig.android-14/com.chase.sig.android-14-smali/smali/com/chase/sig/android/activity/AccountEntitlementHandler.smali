.class public abstract enum Lcom/chase/sig/android/activity/AccountEntitlementHandler;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/chase/sig/android/activity/AccountEntitlementHandler;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/chase/sig/android/activity/AccountEntitlementHandler;

.field public static final enum b:Lcom/chase/sig/android/activity/AccountEntitlementHandler;

.field public static final enum c:Lcom/chase/sig/android/activity/AccountEntitlementHandler;

.field public static final enum d:Lcom/chase/sig/android/activity/AccountEntitlementHandler;

.field private static final synthetic e:[Lcom/chase/sig/android/activity/AccountEntitlementHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 9
    new-instance v0, Lcom/chase/sig/android/activity/q;

    const-string v1, "LOAN_ACCT_TRANSFER"

    invoke-direct {v0, v1}, Lcom/chase/sig/android/activity/q;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/activity/AccountEntitlementHandler;->a:Lcom/chase/sig/android/activity/AccountEntitlementHandler;

    .line 31
    new-instance v0, Lcom/chase/sig/android/activity/r;

    const-string v1, "EPAY"

    invoke-direct {v0, v1}, Lcom/chase/sig/android/activity/r;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/activity/AccountEntitlementHandler;->b:Lcom/chase/sig/android/activity/AccountEntitlementHandler;

    .line 47
    new-instance v0, Lcom/chase/sig/android/activity/s;

    const-string v1, "BILL_PAY"

    invoke-direct {v0, v1}, Lcom/chase/sig/android/activity/s;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/activity/AccountEntitlementHandler;->c:Lcom/chase/sig/android/activity/AccountEntitlementHandler;

    .line 63
    new-instance v0, Lcom/chase/sig/android/activity/t;

    const-string v1, "DEBIT_TRANSFER"

    invoke-direct {v0, v1}, Lcom/chase/sig/android/activity/t;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/activity/AccountEntitlementHandler;->d:Lcom/chase/sig/android/activity/AccountEntitlementHandler;

    .line 8
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/chase/sig/android/activity/AccountEntitlementHandler;

    const/4 v1, 0x0

    sget-object v2, Lcom/chase/sig/android/activity/AccountEntitlementHandler;->a:Lcom/chase/sig/android/activity/AccountEntitlementHandler;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/chase/sig/android/activity/AccountEntitlementHandler;->b:Lcom/chase/sig/android/activity/AccountEntitlementHandler;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/chase/sig/android/activity/AccountEntitlementHandler;->c:Lcom/chase/sig/android/activity/AccountEntitlementHandler;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/chase/sig/android/activity/AccountEntitlementHandler;->d:Lcom/chase/sig/android/activity/AccountEntitlementHandler;

    aput-object v2, v0, v1

    sput-object v0, Lcom/chase/sig/android/activity/AccountEntitlementHandler;->e:[Lcom/chase/sig/android/activity/AccountEntitlementHandler;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 8
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IB)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 8
    invoke-direct {p0, p1, p2}, Lcom/chase/sig/android/activity/AccountEntitlementHandler;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/chase/sig/android/activity/AccountEntitlementHandler;
    .locals 1
    .parameter

    .prologue
    .line 8
    const-class v0, Lcom/chase/sig/android/activity/AccountEntitlementHandler;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/activity/AccountEntitlementHandler;

    return-object v0
.end method

.method public static values()[Lcom/chase/sig/android/activity/AccountEntitlementHandler;
    .locals 1

    .prologue
    .line 8
    sget-object v0, Lcom/chase/sig/android/activity/AccountEntitlementHandler;->e:[Lcom/chase/sig/android/activity/AccountEntitlementHandler;

    invoke-virtual {v0}, [Lcom/chase/sig/android/activity/AccountEntitlementHandler;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/chase/sig/android/activity/AccountEntitlementHandler;

    return-object v0
.end method


# virtual methods
.method abstract a()I
.end method

.method abstract a(Lcom/chase/sig/android/domain/IAccount;)Z
.end method

.method abstract b()Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end method
