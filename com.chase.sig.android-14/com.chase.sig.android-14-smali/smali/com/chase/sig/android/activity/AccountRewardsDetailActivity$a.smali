.class public Lcom/chase/sig/android/activity/AccountRewardsDetailActivity$a;
.super Lcom/chase/sig/android/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/d",
        "<",
        "Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/service/AccountRewardsDetailResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 268
    invoke-direct {p0}, Lcom/chase/sig/android/d;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .parameter

    .prologue
    .line 268
    check-cast p1, [Ljava/lang/String;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;->A()Lcom/chase/sig/android/service/n;

    move-result-object v0

    iget-object v1, v0, Lcom/chase/sig/android/service/n;->f:Lcom/chase/sig/android/service/c;

    if-nez v1, :cond_0

    new-instance v1, Lcom/chase/sig/android/service/c;

    invoke-direct {v1}, Lcom/chase/sig/android/service/c;-><init>()V

    iput-object v1, v0, Lcom/chase/sig/android/service/n;->f:Lcom/chase/sig/android/service/c;

    :cond_0
    iget-object v0, v0, Lcom/chase/sig/android/service/n;->f:Lcom/chase/sig/android/service/c;

    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-static {v0}, Lcom/chase/sig/android/service/c;->a(Ljava/lang/String;)Lcom/chase/sig/android/service/AccountRewardsDetailResponse;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 3
    .parameter

    .prologue
    .line 268
    check-cast p1, Lcom/chase/sig/android/service/AccountRewardsDetailResponse;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/AccountRewardsDetailResponse;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/AccountRewardsDetailResponse;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;->b(Ljava/util/List;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/chase/sig/android/service/AccountRewardsDetailResponse;->a()Lcom/chase/sig/android/domain/AccountRewardsDetail;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-virtual {p1}, Lcom/chase/sig/android/service/AccountRewardsDetailResponse;->b()Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;->v()V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lcom/chase/sig/android/service/AccountRewardsDetailResponse;->b()Ljava/util/List;

    move-result-object v1

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/AccountRewardsDetailResponse;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;->a(Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;->a(Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;)Lcom/chase/sig/android/activity/AccountRewardsDetailActivity$b;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;

    invoke-static {v0, v1}, Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;->a(Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;Ljava/util/List;)Ljava/util/List;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;

    invoke-static {v0, v1}, Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;->b(Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;Ljava/util/List;)V

    goto :goto_0
.end method
