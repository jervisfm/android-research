.class final Lcom/chase/sig/android/activity/AccountRewardsDetailActivity$b;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/chase/sig/android/view/ai;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/view/ai;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;Landroid/app/Activity;Ljava/util/List;Ljava/lang/String;)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 207
    iput-object p1, p0, Lcom/chase/sig/android/activity/AccountRewardsDetailActivity$b;->a:Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;

    .line 208
    const v0, 0x7f0300ab

    invoke-direct {p0, p2, v0, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 210
    iput-object p3, p0, Lcom/chase/sig/android/activity/AccountRewardsDetailActivity$b;->b:Ljava/util/List;

    .line 212
    iput-object p4, p0, Lcom/chase/sig/android/activity/AccountRewardsDetailActivity$b;->c:Ljava/lang/String;

    .line 214
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/AccountRewardsDetailActivity$b;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 200
    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountRewardsDetailActivity$b;->b:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x0

    const v3, 0x7f0300b9

    .line 226
    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountRewardsDetailActivity$b;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 227
    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountRewardsDetailActivity$b;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/ai;

    .line 230
    iget-object v1, p0, Lcom/chase/sig/android/activity/AccountRewardsDetailActivity$b;->a:Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 232
    iget v2, v0, Lcom/chase/sig/android/view/ai;->a:I

    if-ne v2, v3, :cond_1

    .line 233
    invoke-virtual {v1, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    move-object v1, v0

    .line 263
    :cond_0
    :goto_0
    return-object v1

    .line 242
    :cond_1
    const v2, 0x7f0300ab

    invoke-virtual {v1, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    .line 243
    const v2, 0x7f0902ac

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 244
    iget-object v3, v0, Lcom/chase/sig/android/view/ai;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 246
    const v2, 0x7f0902a9

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 249
    const v3, 0x7f0902aa

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 250
    iget-object v4, v0, Lcom/chase/sig/android/view/ai;->b:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 252
    iget-object v3, v0, Lcom/chase/sig/android/view/ai;->g:Ljava/lang/String;

    .line 254
    invoke-static {v3}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 255
    const v0, 0x7f0902ab

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 256
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 258
    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
