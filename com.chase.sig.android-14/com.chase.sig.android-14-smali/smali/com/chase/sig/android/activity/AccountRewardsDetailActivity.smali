.class public Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$d;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/AccountRewardsDetailActivity$a;,
        Lcom/chase/sig/android/activity/AccountRewardsDetailActivity$b;
    }
.end annotation


# instance fields
.field private a:Landroid/widget/ListView;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/AccountRewardsDetail;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/chase/sig/android/activity/AccountRewardsDetailActivity$b;

.field private d:Ljava/lang/String;

.field private k:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    .line 268
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;)Lcom/chase/sig/android/activity/AccountRewardsDetailActivity$b;
    .locals 1
    .parameter

    .prologue
    .line 33
    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;->c:Lcom/chase/sig/android/activity/AccountRewardsDetailActivity$b;

    return-object v0
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 33
    iput-object p1, p0, Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;->d:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 33
    iput-object p1, p0, Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;->b:Ljava/util/List;

    return-object p1
.end method

.method private a(Ljava/util/List;)V
    .locals 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/AccountRewardsDetail;",
            ">;)V"
        }
    .end annotation

    .prologue
    const v2, 0x7f0902a5

    const/4 v3, 0x0

    .line 92
    iget-boolean v0, p0, Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;->k:Z

    if-eqz v0, :cond_1

    .line 93
    invoke-virtual {p0, v2}, Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 107
    :goto_0
    new-instance v0, Lcom/chase/sig/android/activity/AccountRewardsDetailActivity$b;

    invoke-direct {p0, p1}, Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;->d(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;->d:Ljava/lang/String;

    invoke-direct {v0, p0, p0, v1, v2}, Lcom/chase/sig/android/activity/AccountRewardsDetailActivity$b;-><init>(Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;Landroid/app/Activity;Ljava/util/List;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;->c:Lcom/chase/sig/android/activity/AccountRewardsDetailActivity$b;

    .line 110
    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;->d:Ljava/lang/String;

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 111
    const-string v0, "layout_inflater"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v1, 0x7f0300aa

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 113
    const v0, 0x7f0902a7

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 114
    iget-object v2, p0, Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;->d:Ljava/lang/String;

    invoke-static {v2}, Lcom/chase/sig/android/util/s;->q(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;->d:Ljava/lang/String;

    .line 115
    sget-object v2, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v0, v2, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 116
    iget-object v2, p0, Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;->d:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 117
    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;->a:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;)V

    .line 119
    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;->a:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;->c:Lcom/chase/sig/android/activity/AccountRewardsDetailActivity$b;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 120
    return-void

    .line 95
    :cond_1
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/AccountRewardsDetail;

    .line 96
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/AccountRewardsDetail;->a()Ljava/util/Hashtable;

    move-result-object v0

    .line 97
    const-string v1, "programName"

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 98
    invoke-static {v0}, Lcom/chase/sig/android/util/s;->p(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 99
    invoke-static {v0}, Lcom/chase/sig/android/util/s;->q(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 100
    invoke-static {v0}, Lcom/chase/sig/android/util/s;->r(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 101
    invoke-virtual {p0, v2}, Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 102
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 103
    sget-object v2, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v0, v2, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 104
    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;Ljava/util/List;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;->a(Ljava/util/List;)V

    return-void
.end method

.method private d(Ljava/util/List;)Ljava/util/ArrayList;
    .locals 9
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/AccountRewardsDetail;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/chase/sig/android/view/ai;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 124
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    .line 125
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    move v3, v2

    .line 127
    :goto_0
    if-ge v3, v4, :cond_a

    .line 128
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/AccountRewardsDetail;

    .line 130
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/AccountRewardsDetail;->a()Ljava/util/Hashtable;

    move-result-object v6

    .line 132
    invoke-virtual {v6}, Ljava/util/Hashtable;->size()I

    move-result v0

    if-lez v0, :cond_4

    const-string v0, "showBalance"

    invoke-virtual {v6, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "false"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "bodyNote"

    invoke-virtual {v6, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_0
    const/4 v0, 0x1

    .line 136
    :goto_1
    if-eqz v0, :cond_3

    .line 137
    new-instance v7, Lcom/chase/sig/android/view/ai;

    invoke-direct {v7}, Lcom/chase/sig/android/view/ai;-><init>()V

    iput-boolean v2, v7, Lcom/chase/sig/android/view/ai;->j:Z

    const-string v0, "displayFormat"

    invoke-virtual {v6, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "failed"

    invoke-virtual {v6, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "true"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "accountMask"

    invoke-virtual {v6, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v7, Lcom/chase/sig/android/view/ai;->b:Ljava/lang/String;

    const-string v0, "formattedBalance"

    invoke-virtual {v6, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v7, Lcom/chase/sig/android/view/ai;->c:Ljava/lang/String;

    :cond_1
    :goto_2
    const-string v0, "bodyNote"

    invoke-virtual {v6, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    iput-object v0, v7, Lcom/chase/sig/android/view/ai;->g:Ljava/lang/String;

    :cond_2
    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 139
    new-instance v0, Lcom/chase/sig/android/view/ai;

    invoke-direct {v0}, Lcom/chase/sig/android/view/ai;-><init>()V

    .line 140
    const v1, 0x7f0300b9

    iput v1, v0, Lcom/chase/sig/android/view/ai;->a:I

    .line 141
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 127
    :cond_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto/16 :goto_0

    :cond_4
    move v0, v2

    .line 132
    goto :goto_1

    .line 137
    :cond_5
    const-string v0, "showBalance"

    invoke-virtual {v6, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "false"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, ""

    iput-object v0, v7, Lcom/chase/sig/android/view/ai;->b:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, v7, Lcom/chase/sig/android/view/ai;->c:Ljava/lang/String;

    goto :goto_2

    :cond_6
    const-string v0, "programName"

    invoke-virtual {v6, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-boolean v1, p0, Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;->k:Z

    if-eqz v1, :cond_8

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v0, "accountMask"

    invoke-virtual {v6, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v0, "balance"

    invoke-virtual {v6, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_7

    const-string v0, "N/A"

    :goto_3
    invoke-static {v1}, Lcom/chase/sig/android/util/s;->p(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v7, Lcom/chase/sig/android/view/ai;->b:Ljava/lang/String;

    iput-object v0, v7, Lcom/chase/sig/android/view/ai;->c:Ljava/lang/String;

    goto/16 :goto_2

    :cond_7
    const-string v0, "formattedBalance"

    invoke-virtual {v6, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_3

    :cond_8
    const-string v1, "balanceType"

    invoke-virtual {v6, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v8, "<i>"

    invoke-virtual {v0, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    const/4 v8, -0x1

    if-eq v0, v8, :cond_9

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v8, "<i>"

    invoke-direct {v0, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "</i>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_9
    const-string v0, "formattedBalance"

    invoke-virtual {v6, v0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_3

    .line 146
    :cond_a
    return-object v5
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 4
    .parameter

    .prologue
    .line 46
    const v0, 0x7f0300a9

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;->b(I)V

    .line 47
    const v0, 0x7f0902a6

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;->a:Landroid/widget/ListView;

    .line 49
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 50
    const-string v1, "selectedAccountId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 51
    const-string v2, "isATMRewards"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;->k:Z

    .line 53
    if-eqz p1, :cond_2

    const-string v0, "rewards_details"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 54
    const-string v0, "rewards_details"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;->b:Ljava/util/List;

    .line 56
    const-string v0, "rewards_footnote"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;->d:Ljava/lang/String;

    .line 58
    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 59
    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;->b:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;->a(Ljava/util/List;)V

    .line 72
    :cond_0
    :goto_0
    return-void

    .line 61
    :cond_1
    const v0, 0x7f07027c

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;->f(I)V

    goto :goto_0

    .line 66
    :cond_2
    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;->e:Lcom/chase/sig/android/activity/mc;

    const-class v2, Lcom/chase/sig/android/activity/AccountRewardsDetailActivity$a;

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/activity/mc;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/ae;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/activity/AccountRewardsDetailActivity$a;

    .line 68
    invoke-virtual {v0}, Lcom/chase/sig/android/activity/AccountRewardsDetailActivity$a;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v2

    sget-object v3, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-eq v2, v3, :cond_0

    .line 69
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/activity/AccountRewardsDetailActivity$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 75
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ai;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 77
    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;->c:Lcom/chase/sig/android/activity/AccountRewardsDetailActivity$b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;->c:Lcom/chase/sig/android/activity/AccountRewardsDetailActivity$b;

    invoke-static {v0}, Lcom/chase/sig/android/activity/AccountRewardsDetailActivity$b;->a(Lcom/chase/sig/android/activity/AccountRewardsDetailActivity$b;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 79
    const-string v1, "rewards_details"

    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;->b:Ljava/util/List;

    check-cast v0, Ljava/io/Serializable;

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 82
    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;->d:Ljava/lang/String;

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83
    const-string v0, "rewards_footnote"

    iget-object v1, p0, Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 88
    :cond_0
    return-void
.end method
