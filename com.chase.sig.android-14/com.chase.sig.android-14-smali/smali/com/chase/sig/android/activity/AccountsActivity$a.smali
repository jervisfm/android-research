.class public Lcom/chase/sig/android/activity/AccountsActivity$a;
.super Lcom/chase/sig/android/activity/ae;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/AccountsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/activity/ae",
        "<",
        "Lcom/chase/sig/android/activity/AccountsActivity;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/domain/MobileAdResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1347
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ae;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 1347
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/AccountsActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/AccountsActivity;->A()Lcom/chase/sig/android/service/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/service/n;->o()Lcom/chase/sig/android/service/s;

    invoke-static {}, Lcom/chase/sig/android/service/s;->a()Lcom/chase/sig/android/domain/MobileAdResponse;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/MobileAdResponse;->e()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/MobileAdResponse;->a()Lcom/chase/sig/android/domain/MobileAdResponseContent;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/ChaseApplication;->a(Lcom/chase/sig/android/domain/MobileAdResponse;)V

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/MobileAdResponse;->a()Lcom/chase/sig/android/domain/MobileAdResponseContent;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/MobileAdResponseContent;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "https:"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "http:"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/MobileAdResponse;->a()Lcom/chase/sig/android/domain/MobileAdResponseContent;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/MobileAdResponseContent;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/service/s;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    :goto_0
    sget-object v0, Lcom/chase/sig/android/activity/AccountsActivity;->a:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/AccountsActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/AccountsActivity;->A()Lcom/chase/sig/android/service/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/service/n;->n()Lcom/chase/sig/android/service/j;

    const/4 v0, 0x0

    invoke-static {v1, v0, v3}, Lcom/chase/sig/android/service/j;->a(Ljava/lang/String;ZLjava/util/HashMap;)Lcom/chase/sig/android/domain/ImageDownloadResponse;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/ImageDownloadResponse;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lcom/chase/sig/android/activity/AccountsActivity;->a:Landroid/graphics/Bitmap;

    :cond_0
    move-object v0, v2

    :goto_1
    return-object v0

    :cond_1
    move-object v0, v3

    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method protected final bridge synthetic a(Ljava/lang/Object;)V
    .locals 1
    .parameter

    .prologue
    .line 1347
    check-cast p1, Lcom/chase/sig/android/domain/MobileAdResponse;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/AccountsActivity;

    invoke-static {v0, p1}, Lcom/chase/sig/android/activity/AccountsActivity;->a(Lcom/chase/sig/android/activity/AccountsActivity;Lcom/chase/sig/android/domain/MobileAdResponse;)V

    :cond_0
    return-void
.end method
