.class public Lcom/chase/sig/android/activity/AccountsActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$d;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/AccountsActivity$b;,
        Lcom/chase/sig/android/activity/AccountsActivity$a;
    }
.end annotation


# static fields
.field public static a:Landroid/graphics/Bitmap;


# instance fields
.field private b:Landroid/widget/ListView;

.field private c:Landroid/view/View;

.field private final d:Z

.field private final k:Z

.field private final l:Landroid/widget/AdapterView$OnItemClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    .line 81
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->i()Z

    move-result v0

    iput-boolean v0, p0, Lcom/chase/sig/android/activity/AccountsActivity;->d:Z

    .line 101
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/chase/sig/android/activity/AccountsActivity;->k:Z

    .line 257
    new-instance v0, Lcom/chase/sig/android/activity/v;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/activity/v;-><init>(Lcom/chase/sig/android/activity/AccountsActivity;)V

    iput-object v0, p0, Lcom/chase/sig/android/activity/AccountsActivity;->l:Landroid/widget/AdapterView$OnItemClickListener;

    .line 1390
    return-void
.end method

.method private static a(Ljava/util/List;Ljava/util/ArrayList;)I
    .locals 10
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/IAccount;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/chase/sig/android/view/ai;",
            ">;)I"
        }
    .end annotation

    .prologue
    const/4 v9, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1183
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 1187
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/IAccount;

    .line 1188
    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->d()Ljava/lang/String;

    move-result-object v6

    .line 1190
    new-array v0, v9, [Ljava/lang/String;

    const-string v7, "ATM"

    aput-object v7, v0, v2

    const-string v7, "RWA"

    aput-object v7, v0, v3

    invoke-static {v6, v0}, Lcom/chase/sig/android/activity/AccountsActivity;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v3

    :goto_1
    if-eqz v0, :cond_6

    .line 1192
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const-string v7, "CHK"

    aput-object v7, v0, v2

    const-string v7, "AMA"

    aput-object v7, v0, v3

    const-string v7, "SAV"

    aput-object v7, v0, v9

    const/4 v7, 0x3

    const-string v8, "MMA"

    aput-object v8, v0, v7

    invoke-static {v6, v0}, Lcom/chase/sig/android/activity/AccountsActivity;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1193
    add-int/lit8 v0, v1, 0x1

    :goto_2
    move v1, v0

    .line 1199
    goto :goto_0

    :cond_0
    move v0, v2

    .line 1190
    goto :goto_1

    .line 1202
    :cond_1
    if-nez v1, :cond_2

    .line 1222
    :goto_3
    return v2

    :cond_2
    move v3, v2

    move v4, v2

    .line 1208
    :goto_4
    if-ge v3, v5, :cond_3

    .line 1209
    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/ai;

    .line 1211
    invoke-virtual {v0}, Lcom/chase/sig/android/view/ai;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1212
    add-int/lit8 v0, v4, 0x1

    .line 1215
    :goto_5
    if-ne v0, v1, :cond_4

    move v2, v3

    .line 1222
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 1208
    :cond_4
    add-int/lit8 v3, v3, 0x1

    move v4, v0

    goto :goto_4

    :cond_5
    move v0, v4

    goto :goto_5

    :cond_6
    move v0, v1

    goto :goto_2
.end method

.method private static a(Lcom/chase/sig/android/domain/IAccount;Ljava/lang/String;)Lcom/chase/sig/android/view/ai;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 924
    new-instance v0, Lcom/chase/sig/android/view/ai;

    invoke-direct {v0}, Lcom/chase/sig/android/view/ai;-><init>()V

    .line 926
    const/4 v1, 0x0

    iput v1, v0, Lcom/chase/sig/android/view/ai;->e:I

    .line 927
    const v1, 0x7f0300b0

    iput v1, v0, Lcom/chase/sig/android/view/ai;->a:I

    .line 928
    iput-object p1, v0, Lcom/chase/sig/android/view/ai;->b:Ljava/lang/String;

    .line 929
    const-string v1, ""

    iput-object v1, v0, Lcom/chase/sig/android/view/ai;->c:Ljava/lang/String;

    .line 930
    invoke-interface {p0}, Lcom/chase/sig/android/domain/IAccount;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/chase/sig/android/view/ai;->d:Ljava/lang/String;

    .line 931
    return-object v0
.end method

.method private a(Lcom/chase/sig/android/domain/IAccount;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 943
    invoke-interface {p1}, Lcom/chase/sig/android/domain/IAccount;->s()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 972
    const-string v0, ""

    :goto_0
    return-object v0

    .line 945
    :pswitch_0
    const-string v0, "Principal balance"

    goto :goto_0

    .line 947
    :pswitch_1
    const-string v0, "Outstanding balance"

    goto :goto_0

    .line 950
    :pswitch_2
    const-string v0, "Available balance"

    goto :goto_0

    .line 952
    :pswitch_3
    const-string v0, "Spending this period"

    goto :goto_0

    .line 954
    :pswitch_4
    const-string v0, "Last payment date"

    goto :goto_0

    .line 956
    :pswitch_5
    iget-boolean v0, p0, Lcom/chase/sig/android/activity/AccountsActivity;->d:Z

    if-eqz v0, :cond_0

    .line 957
    const-string v0, "Value"

    goto :goto_0

    .line 959
    :cond_0
    const-string v0, "Market value"

    goto :goto_0

    .line 962
    :pswitch_6
    iget-boolean v0, p0, Lcom/chase/sig/android/activity/AccountsActivity;->d:Z

    if-eqz v0, :cond_1

    .line 963
    const-string v0, "Value"

    goto :goto_0

    .line 965
    :cond_1
    const-string v0, "Market value"

    goto :goto_0

    .line 968
    :pswitch_7
    const-string v0, "Outstanding balance"

    goto :goto_0

    .line 970
    :pswitch_8
    const-string v0, "Current balance"

    goto :goto_0

    .line 943
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_8
        :pswitch_2
        :pswitch_6
        :pswitch_7
        :pswitch_2
    .end packed-switch
.end method

.method private static a(Ljava/util/List;)Ljava/util/ArrayList;
    .locals 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/IAccount;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1169
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1171
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/IAccount;

    .line 1172
    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->d()Ljava/lang/String;

    move-result-object v3

    const-string v4, "RWA"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->r()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1173
    :cond_1
    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1177
    :cond_2
    return-object v1
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/AccountsActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/chase/sig/android/activity/AccountsActivity;->g()V

    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/AccountsActivity;Lcom/chase/sig/android/domain/MobileAdResponse;)V
    .locals 5
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x1

    .line 67
    sget-object v0, Lcom/chase/sig/android/activity/AccountsActivity;->a:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    const v0, 0x7f03000a

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/AccountsActivity;->c:Landroid/view/View;

    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountsActivity;->c:Landroid/view/View;

    const v1, 0x7f090024

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    sget-object v1, Lcom/chase/sig/android/activity/AccountsActivity;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/AccountsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x4248

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    invoke-static {v4, v2, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    const/high16 v3, 0x40a0

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-static {v4, v3, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    add-float/2addr v1, v2

    float-to-int v1, v1

    invoke-direct {p0, v1}, Lcom/chase/sig/android/activity/AccountsActivity;->k(I)V

    new-instance v1, Lcom/chase/sig/android/activity/ab;

    invoke-direct {v1, p0, p1}, Lcom/chase/sig/android/activity/ab;-><init>(Lcom/chase/sig/android/activity/AccountsActivity;Lcom/chase/sig/android/domain/MobileAdResponse;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/chase/sig/android/activity/AccountsActivity;->c:Landroid/view/View;

    const v2, 0x7f090025

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/chase/sig/android/activity/ac;

    invoke-direct {v2, p0}, Lcom/chase/sig/android/activity/ac;-><init>(Lcom/chase/sig/android/activity/AccountsActivity;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    const/16 v2, 0x1e

    if-le v1, v2, :cond_1

    const/16 v1, 0x14

    :goto_0
    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/AccountsActivity;->n()Landroid/view/ViewGroup;

    move-result-object v1

    iget-object v2, p0, Lcom/chase/sig/android/activity/AccountsActivity;->c:Landroid/view/View;

    invoke-virtual {v1, v2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :cond_0
    return-void

    :cond_1
    const/16 v1, 0xa

    goto :goto_0
.end method

.method private a(Lcom/chase/sig/android/domain/IAccount;Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 11
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/chase/sig/android/domain/IAccount;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/chase/sig/android/view/ai;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const v10, 0x7f0300ba

    const/4 v9, 0x6

    const/4 v8, 0x5

    const/4 v0, 0x0

    const/4 v7, 0x1

    .line 552
    invoke-interface {p1}, Lcom/chase/sig/android/domain/IAccount;->x()Lcom/chase/sig/android/domain/IAccount;

    move-result-object v1

    .line 555
    invoke-static {p1, p3}, Lcom/chase/sig/android/activity/AccountsActivity;->a(Lcom/chase/sig/android/domain/IAccount;Ljava/lang/String;)Lcom/chase/sig/android/view/ai;

    move-result-object v2

    .line 556
    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 559
    new-instance v2, Lcom/chase/sig/android/view/ai;

    invoke-direct {v2}, Lcom/chase/sig/android/view/ai;-><init>()V

    .line 560
    iput v9, v2, Lcom/chase/sig/android/view/ai;->e:I

    .line 561
    const v3, 0x7f0300b6

    iput v3, v2, Lcom/chase/sig/android/view/ai;->a:I

    .line 562
    const-string v3, "%s (%s)"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-interface {v1}, Lcom/chase/sig/android/domain/IAccount;->a()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    invoke-interface {p1}, Lcom/chase/sig/android/domain/IAccount;->c()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/chase/sig/android/view/ai;->b:Ljava/lang/String;

    .line 564
    const-string v3, ""

    iput-object v3, v2, Lcom/chase/sig/android/view/ai;->c:Ljava/lang/String;

    .line 565
    iput-boolean v7, v2, Lcom/chase/sig/android/view/ai;->f:Z

    .line 566
    invoke-interface {v1}, Lcom/chase/sig/android/domain/IAccount;->b()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/chase/sig/android/view/ai;->d:Ljava/lang/String;

    .line 567
    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 570
    new-instance v2, Lcom/chase/sig/android/view/ai;

    invoke-direct {v2}, Lcom/chase/sig/android/view/ai;-><init>()V

    .line 571
    iput v8, v2, Lcom/chase/sig/android/view/ai;->e:I

    .line 572
    invoke-direct {p0, v1}, Lcom/chase/sig/android/activity/AccountsActivity;->b(Lcom/chase/sig/android/domain/IAccount;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/chase/sig/android/view/ai;->g:Ljava/lang/String;

    .line 574
    invoke-direct {p0, v1}, Lcom/chase/sig/android/activity/AccountsActivity;->e(Lcom/chase/sig/android/domain/IAccount;)Ljava/lang/String;

    move-result-object v3

    .line 576
    iget-object v4, v2, Lcom/chase/sig/android/view/ai;->g:Ljava/lang/String;

    if-eqz v4, :cond_3

    .line 577
    const v4, 0x7f0300bb

    iput v4, v2, Lcom/chase/sig/android/view/ai;->a:I

    .line 582
    :goto_0
    invoke-direct {p0, v1}, Lcom/chase/sig/android/activity/AccountsActivity;->g(Lcom/chase/sig/android/domain/IAccount;)Ljava/lang/String;

    move-result-object v4

    .line 584
    invoke-direct {p0, v1}, Lcom/chase/sig/android/activity/AccountsActivity;->a(Lcom/chase/sig/android/domain/IAccount;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v2, Lcom/chase/sig/android/view/ai;->b:Ljava/lang/String;

    .line 585
    invoke-interface {v1}, Lcom/chase/sig/android/domain/IAccount;->b()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v2, Lcom/chase/sig/android/view/ai;->d:Ljava/lang/String;

    .line 586
    iput-object v4, v2, Lcom/chase/sig/android/view/ai;->c:Ljava/lang/String;

    .line 587
    invoke-static {v4}, Lcom/chase/sig/android/activity/AccountsActivity;->g(Ljava/lang/String;)I

    move-result v4

    iput v4, v2, Lcom/chase/sig/android/view/ai;->k:I

    .line 588
    invoke-interface {v1}, Lcom/chase/sig/android/domain/IAccount;->f()Z

    move-result v4

    iput-boolean v4, v2, Lcom/chase/sig/android/view/ai;->f:Z

    .line 589
    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 592
    invoke-static {v3}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 593
    new-instance v2, Lcom/chase/sig/android/view/ai;

    invoke-direct {v2}, Lcom/chase/sig/android/view/ai;-><init>()V

    .line 594
    invoke-interface {v1}, Lcom/chase/sig/android/domain/IAccount;->u()Z

    const v4, 0x7f0300b8

    iput v4, v2, Lcom/chase/sig/android/view/ai;->a:I

    .line 597
    const/4 v4, 0x4

    iput v4, v2, Lcom/chase/sig/android/view/ai;->e:I

    .line 598
    iput-object v3, v2, Lcom/chase/sig/android/view/ai;->b:Ljava/lang/String;

    .line 599
    const-string v3, ""

    iput-object v3, v2, Lcom/chase/sig/android/view/ai;->c:Ljava/lang/String;

    .line 600
    iput-boolean v7, v2, Lcom/chase/sig/android/view/ai;->f:Z

    .line 601
    invoke-interface {v1}, Lcom/chase/sig/android/domain/IAccount;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lcom/chase/sig/android/view/ai;->d:Ljava/lang/String;

    .line 602
    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 606
    :cond_0
    new-instance v1, Lcom/chase/sig/android/view/ai;

    invoke-direct {v1}, Lcom/chase/sig/android/view/ai;-><init>()V

    .line 607
    const v2, 0x7f0300b7

    iput v2, v1, Lcom/chase/sig/android/view/ai;->a:I

    .line 609
    iput v9, v1, Lcom/chase/sig/android/view/ai;->e:I

    .line 610
    const-string v2, "%s (%s)"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-interface {p1}, Lcom/chase/sig/android/domain/IAccount;->a()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-interface {p1}, Lcom/chase/sig/android/domain/IAccount;->c()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/chase/sig/android/view/ai;->b:Ljava/lang/String;

    .line 612
    const-string v2, ""

    iput-object v2, v1, Lcom/chase/sig/android/view/ai;->c:Ljava/lang/String;

    .line 613
    iput-boolean v7, v1, Lcom/chase/sig/android/view/ai;->f:Z

    .line 614
    invoke-interface {p1}, Lcom/chase/sig/android/domain/IAccount;->b()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/chase/sig/android/view/ai;->d:Ljava/lang/String;

    .line 615
    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 617
    invoke-interface {p1}, Lcom/chase/sig/android/domain/IAccount;->f()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 618
    new-instance v1, Lcom/chase/sig/android/view/ai;

    invoke-direct {v1}, Lcom/chase/sig/android/view/ai;-><init>()V

    .line 620
    invoke-interface {p1}, Lcom/chase/sig/android/domain/IAccount;->v()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-gt v2, v7, :cond_4

    .line 621
    const v2, 0x7f0300b1

    iput v2, v1, Lcom/chase/sig/android/view/ai;->a:I

    .line 626
    :goto_1
    iput v8, v1, Lcom/chase/sig/android/view/ai;->e:I

    .line 627
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/AccountsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0700cf

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/chase/sig/android/view/ai;->b:Ljava/lang/String;

    .line 628
    const-string v2, ""

    iput-object v2, v1, Lcom/chase/sig/android/view/ai;->c:Ljava/lang/String;

    .line 629
    iput-boolean v7, v1, Lcom/chase/sig/android/view/ai;->f:Z

    .line 630
    invoke-interface {p1}, Lcom/chase/sig/android/domain/IAccount;->b()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/chase/sig/android/view/ai;->d:Ljava/lang/String;

    .line 632
    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 637
    :cond_1
    new-instance v1, Lcom/chase/sig/android/util/r;

    invoke-direct {v1}, Lcom/chase/sig/android/util/r;-><init>()V

    .line 638
    invoke-interface {p1}, Lcom/chase/sig/android/domain/IAccount;->v()Ljava/util/List;

    move-result-object v1

    new-instance v2, Lcom/chase/sig/android/activity/y;

    invoke-direct {v2, p0}, Lcom/chase/sig/android/activity/y;-><init>(Lcom/chase/sig/android/activity/AccountsActivity;)V

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v2, v4}, Lcom/chase/sig/android/util/q;->a(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    invoke-interface {v3, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 579
    :cond_3
    iput v10, v2, Lcom/chase/sig/android/view/ai;->a:I

    goto/16 :goto_0

    .line 623
    :cond_4
    iput v10, v1, Lcom/chase/sig/android/view/ai;->a:I

    goto :goto_1

    .line 648
    :cond_5
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 649
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v0

    .line 651
    :goto_3
    if-ge v1, v3, :cond_9

    .line 652
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/IAccount;

    .line 655
    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->g()Z

    move-result v4

    if-nez v4, :cond_6

    .line 656
    new-instance v4, Lcom/chase/sig/android/view/ai;

    invoke-direct {v4}, Lcom/chase/sig/android/view/ai;-><init>()V

    .line 661
    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->g()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 662
    const v5, 0x7f0300b7

    iput v5, v4, Lcom/chase/sig/android/view/ai;->a:I

    .line 667
    :goto_4
    iput v9, v4, Lcom/chase/sig/android/view/ai;->e:I

    .line 668
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v5

    invoke-virtual {v5}, Lcom/chase/sig/android/ChaseApplication;->i()Z

    move-result v5

    invoke-interface {v0, v5}, Lcom/chase/sig/android/domain/IAccount;->a(Z)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/chase/sig/android/view/ai;->b:Ljava/lang/String;

    .line 669
    const-string v5, ""

    iput-object v5, v4, Lcom/chase/sig/android/view/ai;->c:Ljava/lang/String;

    .line 670
    iput-boolean v7, v4, Lcom/chase/sig/android/view/ai;->f:Z

    .line 671
    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->b()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/chase/sig/android/view/ai;->d:Ljava/lang/String;

    .line 672
    invoke-virtual {p2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 674
    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->f()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 675
    new-instance v4, Lcom/chase/sig/android/view/ai;

    invoke-direct {v4}, Lcom/chase/sig/android/view/ai;-><init>()V

    .line 677
    add-int/lit8 v5, v3, -0x1

    if-ne v1, v5, :cond_8

    .line 678
    const v5, 0x7f0300b1

    iput v5, v4, Lcom/chase/sig/android/view/ai;->a:I

    .line 683
    :goto_5
    iput v8, v4, Lcom/chase/sig/android/view/ai;->e:I

    .line 684
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/AccountsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0700cf

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/chase/sig/android/view/ai;->b:Ljava/lang/String;

    .line 685
    const-string v5, ""

    iput-object v5, v4, Lcom/chase/sig/android/view/ai;->c:Ljava/lang/String;

    .line 686
    iput-boolean v7, v4, Lcom/chase/sig/android/view/ai;->f:Z

    .line 687
    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/chase/sig/android/view/ai;->d:Ljava/lang/String;

    .line 689
    invoke-virtual {p2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 651
    :cond_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 664
    :cond_7
    const v5, 0x7f0300b6

    iput v5, v4, Lcom/chase/sig/android/view/ai;->a:I

    goto :goto_4

    .line 680
    :cond_8
    iput v10, v4, Lcom/chase/sig/android/view/ai;->a:I

    goto :goto_5

    .line 692
    :cond_9
    return-void
.end method

.method private a(Lcom/chase/sig/android/domain/e;)V
    .locals 7
    .parameter

    .prologue
    .line 332
    invoke-interface {p1}, Lcom/chase/sig/android/domain/e;->c()Ljava/lang/String;

    move-result-object v2

    .line 333
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 335
    invoke-interface {p1}, Lcom/chase/sig/android/domain/e;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 336
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 337
    const v0, 0x7f0700b3

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/AccountsActivity;->f(I)V

    .line 340
    :cond_0
    const/4 v0, 0x0

    .line 341
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v0

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/IAccount;

    .line 342
    if-eqz v1, :cond_1

    .line 343
    const/4 v6, 0x0

    invoke-direct {p0, v0, v3, v6}, Lcom/chase/sig/android/activity/AccountsActivity;->b(Lcom/chase/sig/android/domain/IAccount;Ljava/util/ArrayList;Ljava/lang/String;)V

    goto :goto_0

    .line 345
    :cond_1
    invoke-direct {p0, v0, v3, v2}, Lcom/chase/sig/android/activity/AccountsActivity;->b(Lcom/chase/sig/android/domain/IAccount;Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 346
    const/4 v0, 0x1

    move v1, v0

    goto :goto_0

    .line 350
    :cond_2
    invoke-static {v4}, Lcom/chase/sig/android/activity/AccountsActivity;->a(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v0

    .line 352
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_3

    .line 353
    invoke-static {v4, v3}, Lcom/chase/sig/android/activity/AccountsActivity;->a(Ljava/util/List;Ljava/util/ArrayList;)I

    move-result v1

    .line 354
    invoke-direct {p0, v0, v3, v1}, Lcom/chase/sig/android/activity/AccountsActivity;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;I)V

    .line 357
    :cond_3
    invoke-direct {p0, v3}, Lcom/chase/sig/android/activity/AccountsActivity;->a(Ljava/util/ArrayList;)V

    .line 358
    return-void
.end method

.method private a(Ljava/util/ArrayList;)V
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/chase/sig/android/view/ai;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 502
    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/ai;

    iget v0, v0, Lcom/chase/sig/android/view/ai;->a:I

    const v1, 0x7f0300b9

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 503
    :cond_0
    new-instance v0, Lcom/chase/sig/android/view/ah;

    invoke-direct {v0, p0, p1}, Lcom/chase/sig/android/view/ah;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    .line 505
    iget-object v1, p0, Lcom/chase/sig/android/activity/AccountsActivity;->b:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 506
    return-void
.end method

.method private a(Ljava/util/ArrayList;Ljava/util/ArrayList;I)V
    .locals 6
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/chase/sig/android/view/ai;",
            ">;I)V"
        }
    .end annotation

    .prologue
    const/16 v5, 0x9

    const/4 v4, 0x1

    .line 894
    invoke-virtual {p1}, Ljava/util/ArrayList;->toString()Ljava/lang/String;

    move-result-object v0

    .line 898
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v4, v1}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 899
    new-instance v1, Lcom/chase/sig/android/view/ai;

    invoke-direct {v1}, Lcom/chase/sig/android/view/ai;-><init>()V

    .line 900
    iput v5, v1, Lcom/chase/sig/android/view/ai;->e:I

    .line 901
    const v2, 0x7f0300b4

    iput v2, v1, Lcom/chase/sig/android/view/ai;->a:I

    .line 902
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/AccountsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0700ff

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/chase/sig/android/view/ai;->b:Ljava/lang/String;

    .line 903
    const-string v2, ""

    iput-object v2, v1, Lcom/chase/sig/android/view/ai;->c:Ljava/lang/String;

    .line 904
    iput-boolean v4, v1, Lcom/chase/sig/android/view/ai;->f:Z

    .line 905
    iput-object v0, v1, Lcom/chase/sig/android/view/ai;->d:Ljava/lang/String;

    .line 907
    invoke-virtual {p2, p3, v1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 909
    new-instance v1, Lcom/chase/sig/android/view/ai;

    invoke-direct {v1}, Lcom/chase/sig/android/view/ai;-><init>()V

    .line 910
    const v2, 0x7f0300b1

    iput v2, v1, Lcom/chase/sig/android/view/ai;->a:I

    .line 911
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/AccountsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070100

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/chase/sig/android/view/ai;->b:Ljava/lang/String;

    .line 912
    iput v5, v1, Lcom/chase/sig/android/view/ai;->e:I

    .line 913
    iput-boolean v4, v1, Lcom/chase/sig/android/view/ai;->f:Z

    .line 914
    iput-object v0, v1, Lcom/chase/sig/android/view/ai;->d:Ljava/lang/String;

    .line 916
    add-int/lit8 v0, p3, 0x1

    invoke-virtual {p2, v0, v1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 918
    add-int/lit8 v0, p3, 0x2

    invoke-static {}, Lcom/chase/sig/android/activity/AccountsActivity;->f()Lcom/chase/sig/android/view/ai;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 920
    return-void
.end method

.method private static varargs a(Ljava/lang/String;[Ljava/lang/String;)Z
    .locals 4
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 1226
    array-length v2, p1

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, p1, v1

    .line 1227
    invoke-virtual {p0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1228
    const/4 v0, 0x1

    .line 1232
    :cond_0
    return v0

    .line 1226
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/AccountsActivity;)Landroid/view/View;
    .locals 1
    .parameter

    .prologue
    .line 67
    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountsActivity;->c:Landroid/view/View;

    return-object v0
.end method

.method private b(Lcom/chase/sig/android/domain/IAccount;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 1005
    invoke-direct {p0, p1}, Lcom/chase/sig/android/activity/AccountsActivity;->c(Lcom/chase/sig/android/domain/IAccount;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1006
    const-string v0, "see positions"

    .line 1015
    :goto_0
    return-object v0

    .line 1008
    :cond_0
    invoke-direct {p0, p1}, Lcom/chase/sig/android/activity/AccountsActivity;->d(Lcom/chase/sig/android/domain/IAccount;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/chase/sig/android/activity/AccountsActivity;->d:Z

    if-nez v0, :cond_1

    .line 1009
    const-string v0, "see activity"

    goto :goto_0

    .line 1011
    :cond_1
    invoke-direct {p0, p1}, Lcom/chase/sig/android/activity/AccountsActivity;->d(Lcom/chase/sig/android/domain/IAccount;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/chase/sig/android/activity/AccountsActivity;->d:Z

    if-eqz v0, :cond_2

    .line 1012
    const-string v0, "see transactions"

    goto :goto_0

    .line 1015
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Lcom/chase/sig/android/domain/IAccount;Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 10
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/chase/sig/android/domain/IAccount;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/chase/sig/android/view/ai;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 727
    invoke-direct {p0, p1}, Lcom/chase/sig/android/activity/AccountsActivity;->d(Lcom/chase/sig/android/domain/IAccount;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/chase/sig/android/activity/AccountsActivity;->d:Z

    if-eqz v0, :cond_1

    invoke-interface {p1}, Lcom/chase/sig/android/domain/IAccount;->G()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    move v1, v0

    .line 730
    :goto_0
    new-instance v4, Lcom/chase/sig/android/view/ai;

    invoke-direct {v4}, Lcom/chase/sig/android/view/ai;-><init>()V

    .line 731
    new-instance v5, Lcom/chase/sig/android/view/ai;

    invoke-direct {v5}, Lcom/chase/sig/android/view/ai;-><init>()V

    .line 733
    invoke-interface {p1}, Lcom/chase/sig/android/domain/IAccount;->d()Ljava/lang/String;

    move-result-object v0

    .line 734
    const-string v2, "RWA"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "ATM"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 874
    :cond_0
    :goto_1
    return-void

    .line 727
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    .line 739
    :cond_2
    invoke-static {p3}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v3

    .line 741
    if-eqz v3, :cond_3

    .line 742
    invoke-static {p1, p3}, Lcom/chase/sig/android/activity/AccountsActivity;->a(Lcom/chase/sig/android/domain/IAccount;Ljava/lang/String;)Lcom/chase/sig/android/view/ai;

    move-result-object v0

    .line 743
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 746
    :cond_3
    const/4 v0, 0x0

    .line 747
    invoke-interface {p1}, Lcom/chase/sig/android/domain/IAccount;->i()Z

    move-result v2

    if-eqz v2, :cond_1e

    .line 755
    invoke-interface {p1}, Lcom/chase/sig/android/domain/IAccount;->x()Lcom/chase/sig/android/domain/IAccount;

    move-result-object v0

    if-eqz v0, :cond_1d

    .line 756
    invoke-interface {p1}, Lcom/chase/sig/android/domain/IAccount;->x()Lcom/chase/sig/android/domain/IAccount;

    move-result-object v0

    move-object v2, v0

    .line 760
    :goto_2
    new-instance v6, Lcom/chase/sig/android/view/ai;

    invoke-direct {v6}, Lcom/chase/sig/android/view/ai;-><init>()V

    .line 761
    invoke-interface {v2}, Lcom/chase/sig/android/domain/IAccount;->g()Z

    move-result v0

    if-eqz v0, :cond_c

    const/4 v0, 0x7

    :goto_3
    iput v0, v6, Lcom/chase/sig/android/view/ai;->e:I

    .line 762
    if-eqz v3, :cond_f

    const v0, 0x7f0300b6

    :goto_4
    iput v0, v6, Lcom/chase/sig/android/view/ai;->a:I

    .line 765
    if-nez p1, :cond_11

    invoke-interface {v2}, Lcom/chase/sig/android/domain/IAccount;->i()Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-interface {v2}, Lcom/chase/sig/android/domain/IAccount;->v()Ljava/util/List;

    move-result-object v0

    const/4 v3, 0x0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/IAccount;

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/chase/sig/android/ChaseApplication;->i()Z

    move-result v3

    invoke-interface {v0, v3}, Lcom/chase/sig/android/domain/IAccount;->a(Z)Ljava/lang/String;

    move-result-object v0

    :goto_5
    iput-object v0, v6, Lcom/chase/sig/android/view/ai;->b:Ljava/lang/String;

    .line 768
    const-string v0, ""

    iput-object v0, v6, Lcom/chase/sig/android/view/ai;->c:Ljava/lang/String;

    .line 769
    invoke-interface {v2}, Lcom/chase/sig/android/domain/IAccount;->C()Z

    move-result v0

    iput-boolean v0, v6, Lcom/chase/sig/android/view/ai;->f:Z

    .line 770
    if-nez p1, :cond_12

    invoke-interface {v2}, Lcom/chase/sig/android/domain/IAccount;->b()Ljava/lang/String;

    move-result-object v0

    :goto_6
    iput-object v0, v6, Lcom/chase/sig/android/view/ai;->d:Ljava/lang/String;

    .line 772
    invoke-interface {v2}, Lcom/chase/sig/android/domain/IAccount;->F()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v6, Lcom/chase/sig/android/view/ai;->l:Ljava/lang/String;

    .line 774
    invoke-virtual {p2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 776
    invoke-direct {p0, v2}, Lcom/chase/sig/android/activity/AccountsActivity;->e(Lcom/chase/sig/android/domain/IAccount;)Ljava/lang/String;

    move-result-object v6

    .line 778
    const/4 v0, 0x5

    iput v0, v4, Lcom/chase/sig/android/view/ai;->e:I

    .line 780
    invoke-direct {p0, v2}, Lcom/chase/sig/android/activity/AccountsActivity;->b(Lcom/chase/sig/android/domain/IAccount;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/chase/sig/android/view/ai;->g:Ljava/lang/String;

    .line 784
    const/4 v3, 0x0

    .line 785
    invoke-interface {v2}, Lcom/chase/sig/android/domain/IAccount;->e()Ljava/util/Hashtable;

    move-result-object v0

    const-string v7, "marginAcctMask"

    invoke-virtual {v0, v7}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 786
    invoke-interface {v2}, Lcom/chase/sig/android/domain/IAccount;->u()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    const-string v7, "null"

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 788
    const/4 v0, 0x1

    move v3, v0

    .line 791
    :cond_4
    invoke-interface {v2}, Lcom/chase/sig/android/domain/IAccount;->e()Ljava/util/Hashtable;

    move-result-object v0

    const-string v7, "rewardsProgramName"

    invoke-virtual {v0, v7}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v7

    .line 794
    invoke-static {v6}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v8

    .line 795
    iget-object v0, v4, Lcom/chase/sig/android/view/ai;->g:Ljava/lang/String;

    if-eqz v0, :cond_13

    if-nez v6, :cond_5

    if-nez v7, :cond_5

    if-eqz v1, :cond_13

    .line 797
    :cond_5
    const v0, 0x7f0300bb

    iput v0, v4, Lcom/chase/sig/android/view/ai;->a:I

    .line 808
    :goto_7
    invoke-direct {p0, v2}, Lcom/chase/sig/android/activity/AccountsActivity;->a(Lcom/chase/sig/android/domain/IAccount;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/chase/sig/android/view/ai;->b:Ljava/lang/String;

    .line 810
    invoke-direct {p0, v2}, Lcom/chase/sig/android/activity/AccountsActivity;->g(Lcom/chase/sig/android/domain/IAccount;)Ljava/lang/String;

    move-result-object v9

    .line 811
    invoke-interface {v2}, Lcom/chase/sig/android/domain/IAccount;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/chase/sig/android/view/ai;->d:Ljava/lang/String;

    .line 812
    iput-object v9, v4, Lcom/chase/sig/android/view/ai;->c:Ljava/lang/String;

    .line 813
    invoke-direct {p0, v2}, Lcom/chase/sig/android/activity/AccountsActivity;->c(Lcom/chase/sig/android/domain/IAccount;)Z

    move-result v0

    if-nez v0, :cond_6

    invoke-direct {p0, v2}, Lcom/chase/sig/android/activity/AccountsActivity;->d(Lcom/chase/sig/android/domain/IAccount;)Z

    move-result v0

    if-eqz v0, :cond_18

    :cond_6
    const/4 v0, 0x1

    .line 814
    :goto_8
    iput-boolean v0, v4, Lcom/chase/sig/android/view/ai;->f:Z

    .line 815
    iput-boolean v0, v4, Lcom/chase/sig/android/view/ai;->j:Z

    .line 816
    invoke-interface {v2}, Lcom/chase/sig/android/domain/IAccount;->H()Z

    move-result v0

    iput-boolean v0, v4, Lcom/chase/sig/android/view/ai;->h:Z

    .line 818
    invoke-static {v9}, Lcom/chase/sig/android/activity/AccountsActivity;->g(Ljava/lang/String;)I

    move-result v0

    iput v0, v4, Lcom/chase/sig/android/view/ai;->k:I

    .line 820
    invoke-virtual {p2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 822
    if-eqz v1, :cond_7

    .line 823
    if-eqz v8, :cond_19

    const v0, 0x7f0300ba

    :goto_9
    iput v0, v5, Lcom/chase/sig/android/view/ai;->a:I

    .line 825
    const-string v0, "See transactions"

    iput-object v0, v5, Lcom/chase/sig/android/view/ai;->b:Ljava/lang/String;

    .line 826
    invoke-interface {v2}, Lcom/chase/sig/android/domain/IAccount;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v5, Lcom/chase/sig/android/view/ai;->d:Ljava/lang/String;

    .line 827
    const-string v0, ""

    iput-object v0, v5, Lcom/chase/sig/android/view/ai;->c:Ljava/lang/String;

    .line 828
    const/4 v0, 0x1

    iput-boolean v0, v5, Lcom/chase/sig/android/view/ai;->f:Z

    .line 829
    const/4 v0, 0x1

    iput-boolean v0, v5, Lcom/chase/sig/android/view/ai;->j:Z

    .line 830
    const/16 v0, 0xc

    iput v0, v5, Lcom/chase/sig/android/view/ai;->e:I

    .line 832
    invoke-virtual {p2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 835
    :cond_7
    if-eqz v3, :cond_1a

    iget-boolean v0, p0, Lcom/chase/sig/android/activity/AccountsActivity;->d:Z

    if-nez v0, :cond_1a

    const/4 v0, 0x1

    .line 836
    :goto_a
    invoke-interface {v2}, Lcom/chase/sig/android/domain/IAccount;->A()Z

    move-result v1

    if-eqz v1, :cond_1b

    if-eqz v7, :cond_1b

    const/4 v1, 0x1

    .line 838
    :goto_b
    if-eqz v8, :cond_9

    .line 839
    new-instance v3, Lcom/chase/sig/android/view/ai;

    invoke-direct {v3}, Lcom/chase/sig/android/view/ai;-><init>()V

    .line 841
    if-nez v0, :cond_8

    if-eqz v1, :cond_1c

    .line 842
    :cond_8
    const v4, 0x7f0300b8

    iput v4, v3, Lcom/chase/sig/android/view/ai;->a:I

    .line 847
    :goto_c
    const/4 v4, 0x4

    iput v4, v3, Lcom/chase/sig/android/view/ai;->e:I

    .line 848
    iput-object v6, v3, Lcom/chase/sig/android/view/ai;->b:Ljava/lang/String;

    .line 849
    const-string v4, ""

    iput-object v4, v3, Lcom/chase/sig/android/view/ai;->c:Ljava/lang/String;

    .line 850
    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/chase/sig/android/view/ai;->f:Z

    .line 851
    invoke-interface {v2}, Lcom/chase/sig/android/domain/IAccount;->b()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/chase/sig/android/view/ai;->d:Ljava/lang/String;

    .line 853
    invoke-virtual {p2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 856
    :cond_9
    if-eqz v0, :cond_a

    .line 857
    new-instance v0, Lcom/chase/sig/android/view/ai;

    invoke-direct {v0}, Lcom/chase/sig/android/view/ai;-><init>()V

    const v3, 0x7f0300b6

    iput v3, v0, Lcom/chase/sig/android/view/ai;->a:I

    const-string v3, "MARGIN ACCOUNT (%s)"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-interface {v2}, Lcom/chase/sig/android/domain/IAccount;->e()Ljava/util/Hashtable;

    move-result-object v6

    const-string v7, "marginAcctMask"

    invoke-virtual {v6, v7}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/chase/sig/android/view/ai;->b:Ljava/lang/String;

    const-string v3, ""

    iput-object v3, v0, Lcom/chase/sig/android/view/ai;->c:Ljava/lang/String;

    const/4 v3, 0x0

    iput-boolean v3, v0, Lcom/chase/sig/android/view/ai;->f:Z

    const/4 v3, 0x0

    iput-object v3, v0, Lcom/chase/sig/android/view/ai;->d:Ljava/lang/String;

    const/4 v3, 0x6

    iput v3, v0, Lcom/chase/sig/android/view/ai;->e:I

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v3, Lcom/chase/sig/android/view/ai;

    invoke-direct {v3}, Lcom/chase/sig/android/view/ai;-><init>()V

    const v0, 0x7f0300b1

    iput v0, v3, Lcom/chase/sig/android/view/ai;->a:I

    const-string v0, "Market value"

    iput-object v0, v3, Lcom/chase/sig/android/view/ai;->b:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/chase/sig/android/activity/AccountsActivity;->b(Lcom/chase/sig/android/domain/IAccount;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/chase/sig/android/view/ai;->g:Ljava/lang/String;

    new-instance v4, Lcom/chase/sig/android/util/Dollar;

    invoke-interface {v2}, Lcom/chase/sig/android/domain/IAccount;->e()Ljava/util/Hashtable;

    move-result-object v0

    const-string v5, "marginAcctMktVal"

    invoke-virtual {v0, v5}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v4, v0}, Lcom/chase/sig/android/util/Dollar;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/chase/sig/android/util/Dollar;->h()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/chase/sig/android/view/ai;->c:Ljava/lang/String;

    invoke-interface {v2}, Lcom/chase/sig/android/domain/IAccount;->e()Ljava/util/Hashtable;

    move-result-object v0

    const-string v4, "marginAcctMktVal"

    invoke-virtual {v0, v4}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/chase/sig/android/activity/AccountsActivity;->g(Ljava/lang/String;)I

    move-result v0

    iput v0, v3, Lcom/chase/sig/android/view/ai;->k:I

    const/4 v0, 0x0

    iput-boolean v0, v3, Lcom/chase/sig/android/view/ai;->f:Z

    invoke-interface {v2}, Lcom/chase/sig/android/domain/IAccount;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/chase/sig/android/view/ai;->d:Ljava/lang/String;

    invoke-virtual {p2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 860
    :cond_a
    if-eqz v1, :cond_b

    .line 862
    new-instance v1, Lcom/chase/sig/android/view/ai;

    invoke-direct {v1}, Lcom/chase/sig/android/view/ai;-><init>()V

    .line 863
    const v0, 0x7f0300b2

    iput v0, v1, Lcom/chase/sig/android/view/ai;->a:I

    .line 864
    const/16 v0, 0x8

    iput v0, v1, Lcom/chase/sig/android/view/ai;->e:I

    .line 865
    const/4 v0, 0x1

    iput-boolean v0, v1, Lcom/chase/sig/android/view/ai;->i:Z

    .line 866
    invoke-interface {v2}, Lcom/chase/sig/android/domain/IAccount;->e()Ljava/util/Hashtable;

    move-result-object v0

    const-string v3, "rewardsProgramName"

    invoke-virtual {v0, v3}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v1, Lcom/chase/sig/android/view/ai;->b:Ljava/lang/String;

    .line 867
    const-string v0, ""

    iput-object v0, v1, Lcom/chase/sig/android/view/ai;->c:Ljava/lang/String;

    .line 868
    const/4 v0, 0x1

    iput-boolean v0, v1, Lcom/chase/sig/android/view/ai;->f:Z

    .line 869
    invoke-interface {v2}, Lcom/chase/sig/android/domain/IAccount;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/chase/sig/android/view/ai;->d:Ljava/lang/String;

    .line 870
    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 873
    :cond_b
    invoke-static {}, Lcom/chase/sig/android/activity/AccountsActivity;->f()Lcom/chase/sig/android/view/ai;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 761
    :cond_c
    invoke-interface {v2}, Lcom/chase/sig/android/domain/IAccount;->D()Z

    move-result v0

    if-eqz v0, :cond_d

    const/16 v0, 0xa

    goto/16 :goto_3

    :cond_d
    invoke-interface {v2}, Lcom/chase/sig/android/domain/IAccount;->G()Z

    move-result v0

    if-eqz v0, :cond_e

    const/16 v0, 0xb

    goto/16 :goto_3

    :cond_e
    const/4 v0, 0x6

    goto/16 :goto_3

    .line 762
    :cond_f
    const v0, 0x7f0300b4

    goto/16 :goto_4

    .line 765
    :cond_10
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->i()Z

    move-result v0

    invoke-interface {v2, v0}, Lcom/chase/sig/android/domain/IAccount;->a(Z)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_5

    :cond_11
    const-string v0, "%s (%s)"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-interface {v2}, Lcom/chase/sig/android/domain/IAccount;->a()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v3, v7

    const/4 v7, 0x1

    invoke-interface {p1}, Lcom/chase/sig/android/domain/IAccount;->c()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v3, v7

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_5

    .line 770
    :cond_12
    invoke-interface {p1}, Lcom/chase/sig/android/domain/IAccount;->b()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_6

    .line 798
    :cond_13
    iget-object v0, v4, Lcom/chase/sig/android/view/ai;->g:Ljava/lang/String;

    if-eqz v0, :cond_15

    if-nez v6, :cond_15

    iget-boolean v0, p0, Lcom/chase/sig/android/activity/AccountsActivity;->d:Z

    if-nez v0, :cond_14

    if-nez v3, :cond_15

    .line 801
    :cond_14
    const v0, 0x7f0300b3

    iput v0, v4, Lcom/chase/sig/android/view/ai;->a:I

    goto/16 :goto_7

    .line 802
    :cond_15
    if-nez v8, :cond_16

    if-eqz v3, :cond_17

    .line 803
    :cond_16
    const v0, 0x7f0300ba

    iput v0, v4, Lcom/chase/sig/android/view/ai;->a:I

    goto/16 :goto_7

    .line 805
    :cond_17
    const v0, 0x7f0300b1

    iput v0, v4, Lcom/chase/sig/android/view/ai;->a:I

    goto/16 :goto_7

    .line 813
    :cond_18
    const/4 v0, 0x0

    goto/16 :goto_8

    .line 823
    :cond_19
    const v0, 0x7f0300b1

    goto/16 :goto_9

    .line 835
    :cond_1a
    const/4 v0, 0x0

    goto/16 :goto_a

    .line 836
    :cond_1b
    const/4 v1, 0x0

    goto/16 :goto_b

    .line 844
    :cond_1c
    const v4, 0x7f0300b2

    iput v4, v3, Lcom/chase/sig/android/view/ai;->a:I

    goto/16 :goto_c

    :cond_1d
    move-object v2, p1

    goto/16 :goto_2

    :cond_1e
    move-object v2, p1

    move-object p1, v0

    goto/16 :goto_2
.end method

.method private c(Lcom/chase/sig/android/domain/IAccount;)Z
    .locals 1
    .parameter

    .prologue
    .line 1019
    iget-boolean v0, p0, Lcom/chase/sig/android/activity/AccountsActivity;->d:Z

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lcom/chase/sig/android/domain/IAccount;->H()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 416
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/ChaseApplication;

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->b:Lcom/chase/sig/android/domain/g;

    invoke-interface {v0}, Lcom/chase/sig/android/domain/g;->o()Ljava/util/List;

    move-result-object v2

    .line 417
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 419
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    .line 422
    if-ne v4, v7, :cond_0

    .line 423
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/e;

    invoke-direct {p0, v0}, Lcom/chase/sig/android/activity/AccountsActivity;->a(Lcom/chase/sig/android/domain/e;)V

    .line 461
    :goto_0
    return-void

    .line 427
    :cond_0
    :goto_1
    if-ge v1, v4, :cond_3

    .line 429
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/e;

    .line 430
    new-instance v5, Lcom/chase/sig/android/view/ai;

    invoke-direct {v5}, Lcom/chase/sig/android/view/ai;-><init>()V

    .line 432
    const/4 v6, 0x3

    iput v6, v5, Lcom/chase/sig/android/view/ai;->e:I

    .line 433
    invoke-interface {v0}, Lcom/chase/sig/android/domain/e;->c()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/chase/sig/android/view/ai;->b:Ljava/lang/String;

    .line 434
    const-string v6, ""

    iput-object v6, v5, Lcom/chase/sig/android/view/ai;->c:Ljava/lang/String;

    .line 435
    iput-boolean v7, v5, Lcom/chase/sig/android/view/ai;->f:Z

    .line 436
    invoke-interface {v0}, Lcom/chase/sig/android/domain/e;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v5, Lcom/chase/sig/android/view/ai;->d:Ljava/lang/String;

    .line 438
    if-nez v1, :cond_1

    .line 439
    const v0, 0x7f0300b4

    iput v0, v5, Lcom/chase/sig/android/view/ai;->a:I

    .line 446
    :goto_2
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 427
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 440
    :cond_1
    add-int/lit8 v0, v4, -0x1

    if-ne v1, v0, :cond_2

    .line 441
    const v0, 0x7f0300b5

    iput v0, v5, Lcom/chase/sig/android/view/ai;->a:I

    goto :goto_2

    .line 443
    :cond_2
    const v0, 0x7f0300b6

    iput v0, v5, Lcom/chase/sig/android/view/ai;->a:I

    goto :goto_2

    .line 449
    :cond_3
    invoke-direct {p0, v3}, Lcom/chase/sig/android/activity/AccountsActivity;->a(Ljava/util/ArrayList;)V

    .line 451
    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountsActivity;->b:Landroid/widget/ListView;

    new-instance v1, Lcom/chase/sig/android/activity/w;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/w;-><init>(Lcom/chase/sig/android/activity/AccountsActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    goto :goto_0
.end method

.method private d(Lcom/chase/sig/android/domain/IAccount;)Z
    .locals 1
    .parameter

    .prologue
    .line 1023
    iget-boolean v0, p0, Lcom/chase/sig/android/activity/AccountsActivity;->d:Z

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lcom/chase/sig/android/domain/IAccount;->f()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-interface {p1}, Lcom/chase/sig/android/domain/IAccount;->G()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-interface {p1}, Lcom/chase/sig/android/domain/IAccount;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e(Lcom/chase/sig/android/domain/IAccount;)Ljava/lang/String;
    .locals 2
    .parameter

    .prologue
    .line 1029
    invoke-static {p1}, Lcom/chase/sig/android/activity/AccountsActivity;->f(Lcom/chase/sig/android/domain/IAccount;)Lcom/chase/sig/android/activity/AccountEntitlementHandler;

    move-result-object v0

    .line 1031
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/AccountsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/AccountEntitlementHandler;->a()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()V
    .locals 7

    .prologue
    .line 522
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/ChaseApplication;

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->b:Lcom/chase/sig/android/domain/g;

    invoke-interface {v0}, Lcom/chase/sig/android/domain/g;->p()Ljava/util/List;

    move-result-object v3

    .line 523
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/IAccount;

    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->e()Ljava/util/Hashtable;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v2, 0x0

    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->h()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/ChaseApplication;

    invoke-virtual {v1}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v1

    iget-object v1, v1, Lcom/chase/sig/android/domain/o;->b:Lcom/chase/sig/android/domain/g;

    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->b()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v1, v6}, Lcom/chase/sig/android/domain/g;->c(Ljava/lang/String;)Lcom/chase/sig/android/domain/e;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-interface {v1}, Lcom/chase/sig/android/domain/e;->c()Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-direct {p0, v0, v4, v1}, Lcom/chase/sig/android/activity/AccountsActivity;->b(Lcom/chase/sig/android/domain/IAccount;Ljava/util/ArrayList;Ljava/lang/String;)V

    goto :goto_0

    .line 525
    :cond_1
    invoke-static {v3}, Lcom/chase/sig/android/activity/AccountsActivity;->a(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v0

    .line 528
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_2

    .line 529
    invoke-static {v3, v4}, Lcom/chase/sig/android/activity/AccountsActivity;->a(Ljava/util/List;Ljava/util/ArrayList;)I

    move-result v1

    .line 530
    invoke-direct {p0, v0, v4, v1}, Lcom/chase/sig/android/activity/AccountsActivity;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;I)V

    .line 533
    :cond_2
    new-instance v0, Lcom/chase/sig/android/view/ah;

    invoke-direct {v0, p0, v4}, Lcom/chase/sig/android/view/ah;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    .line 535
    iget-object v1, p0, Lcom/chase/sig/android/activity/AccountsActivity;->b:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 536
    return-void

    :cond_3
    move-object v1, v2

    goto :goto_1
.end method

.method private static f(Lcom/chase/sig/android/domain/IAccount;)Lcom/chase/sig/android/activity/AccountEntitlementHandler;
    .locals 3
    .parameter

    .prologue
    .line 1036
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/chase/sig/android/activity/AccountEntitlementHandler;

    const/4 v1, 0x0

    sget-object v2, Lcom/chase/sig/android/activity/AccountEntitlementHandler;->b:Lcom/chase/sig/android/activity/AccountEntitlementHandler;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/chase/sig/android/activity/AccountEntitlementHandler;->c:Lcom/chase/sig/android/activity/AccountEntitlementHandler;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/chase/sig/android/activity/AccountEntitlementHandler;->a:Lcom/chase/sig/android/activity/AccountEntitlementHandler;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/chase/sig/android/activity/AccountEntitlementHandler;->d:Lcom/chase/sig/android/activity/AccountEntitlementHandler;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/activity/AccountEntitlementHandler;

    .line 1039
    invoke-virtual {v0, p0}, Lcom/chase/sig/android/activity/AccountEntitlementHandler;->a(Lcom/chase/sig/android/domain/IAccount;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1043
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static f()Lcom/chase/sig/android/view/ai;
    .locals 2

    .prologue
    .line 1115
    new-instance v0, Lcom/chase/sig/android/view/ai;

    invoke-direct {v0}, Lcom/chase/sig/android/view/ai;-><init>()V

    .line 1116
    const v1, 0x7f0300b9

    iput v1, v0, Lcom/chase/sig/android/view/ai;->a:I

    .line 1117
    return-object v0
.end method

.method private g(Lcom/chase/sig/android/domain/IAccount;)Ljava/lang/String;
    .locals 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 1055
    invoke-interface {p1}, Lcom/chase/sig/android/domain/IAccount;->e()Ljava/util/Hashtable;

    move-result-object v0

    .line 1058
    if-eqz v0, :cond_3

    .line 1059
    invoke-interface {p1}, Lcom/chase/sig/android/domain/IAccount;->s()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    move-object v0, v1

    .line 1102
    :goto_0
    :try_start_0
    new-instance v1, Lcom/chase/sig/android/util/Dollar;

    invoke-direct {v1, v0}, Lcom/chase/sig/android/util/Dollar;-><init>(Ljava/lang/String;)V

    .line 1103
    invoke-virtual {v1}, Lcom/chase/sig/android/util/Dollar;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1104
    invoke-virtual {v1}, Lcom/chase/sig/android/util/Dollar;->h()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 1109
    :goto_1
    return-object v0

    .line 1062
    :pswitch_0
    const-string v1, "available"

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    .line 1065
    :pswitch_1
    iget-boolean v1, p0, Lcom/chase/sig/android/activity/AccountsActivity;->d:Z

    if-eqz v1, :cond_0

    const-string v1, "marketValue"

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    :cond_0
    const-string v1, "current"

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    .line 1072
    :pswitch_2
    iget-boolean v1, p0, Lcom/chase/sig/android/activity/AccountsActivity;->d:Z

    if-eqz v1, :cond_1

    invoke-interface {p1}, Lcom/chase/sig/android/domain/IAccount;->u()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "marketValue"

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    :cond_1
    const-string v1, "current"

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    .line 1077
    :pswitch_3
    const-string v1, "outstanding"

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    .line 1080
    :pswitch_4
    const-string v1, "spendingBalance"

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    .line 1083
    :pswitch_5
    const-string v2, "lastPaymentDate"

    invoke-virtual {v0, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1084
    if-eqz v0, :cond_3

    .line 1088
    :try_start_1
    invoke-static {v0}, Lcom/chase/sig/android/util/s;->h(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_1

    .line 1091
    :catch_0
    move-exception v2

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to parse date from this value: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-object v0, v1

    .line 1093
    goto :goto_0

    .line 1106
    :cond_2
    const-string v0, "N/A"

    goto :goto_1

    .line 1109
    :catch_1
    move-exception v0

    const-string v0, "N/A"

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto/16 :goto_0

    .line 1059
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method

.method private g()V
    .locals 2

    .prologue
    .line 1342
    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountsActivity;->b:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getPaddingTop()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/chase/sig/android/activity/AccountsActivity;->k(I)V

    .line 1343
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/AccountsActivity;->n()Landroid/view/ViewGroup;

    move-result-object v0

    const v1, 0x7f090023

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/AccountsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1344
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/ChaseApplication;->b(Z)V

    .line 1345
    return-void
.end method

.method private k(I)V
    .locals 4
    .parameter

    .prologue
    .line 1255
    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountsActivity;->b:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/chase/sig/android/activity/AccountsActivity;->b:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getPaddingLeft()I

    move-result v1

    iget-object v2, p0, Lcom/chase/sig/android/activity/AccountsActivity;->b:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getPaddingTop()I

    move-result v2

    iget-object v3, p0, Lcom/chase/sig/android/activity/AccountsActivity;->b:Landroid/widget/ListView;

    invoke-virtual {v3}, Landroid/widget/ListView;->getPaddingRight()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3, p1}, Landroid/widget/ListView;->setPadding(IIII)V

    .line 1257
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 6
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 107
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/ChaseApplication;

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v3, v0, Lcom/chase/sig/android/domain/o;->b:Lcom/chase/sig/android/domain/g;

    .line 108
    const v0, 0x7f030008

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/AccountsActivity;->b(I)V

    .line 110
    const-class v0, Lcom/chase/sig/android/activity/AccountsActivity$b;

    new-array v4, v1, [Ljava/lang/Void;

    invoke-virtual {p0, v0, v4}, Lcom/chase/sig/android/activity/AccountsActivity;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 112
    const v0, 0x7f09001e

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/AccountsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/AccountsActivity;->b:Landroid/widget/ListView;

    .line 114
    invoke-interface {v3}, Lcom/chase/sig/android/domain/g;->E()Lcom/chase/sig/android/domain/MaintenanceInfo;

    move-result-object v0

    .line 115
    if-eqz v0, :cond_0

    .line 116
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/MaintenanceInfo;->a()Lcom/chase/sig/android/domain/Announcement;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/MaintenanceInfo;->a()Lcom/chase/sig/android/domain/Announcement;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Announcement;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 119
    const v0, 0x7f09001d

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/AccountsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-interface {v3}, Lcom/chase/sig/android/domain/g;->E()Lcom/chase/sig/android/domain/MaintenanceInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/chase/sig/android/domain/MaintenanceInfo;->a()Lcom/chase/sig/android/domain/Announcement;

    move-result-object v4

    invoke-virtual {p0, v0, v4}, Lcom/chase/sig/android/activity/AccountsActivity;->a(Landroid/view/View;Lcom/chase/sig/android/domain/Announcement;)V

    .line 125
    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountsActivity;->b:Landroid/widget/ListView;

    iget-object v4, p0, Lcom/chase/sig/android/activity/AccountsActivity;->l:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 127
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/AccountsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    .line 129
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/ChaseApplication;

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->b:Lcom/chase/sig/android/domain/g;

    invoke-interface {v0}, Lcom/chase/sig/android/domain/g;->i()Ljava/util/List;

    move-result-object v0

    .line 131
    iget-boolean v5, p0, Lcom/chase/sig/android/activity/AccountsActivity;->d:Z

    if-eqz v5, :cond_2

    if-nez v4, :cond_1

    invoke-interface {v3}, Lcom/chase/sig/android/domain/g;->s()Z

    move-result v5

    if-nez v5, :cond_2

    .line 132
    :cond_1
    iget-object v5, p0, Lcom/chase/sig/android/activity/AccountsActivity;->b:Landroid/widget/ListView;

    invoke-virtual {p0, v5}, Lcom/chase/sig/android/activity/AccountsActivity;->a(Landroid/widget/ListView;)V

    .line 135
    :cond_2
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/IAccount;

    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->z()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    :goto_0
    if-eqz v0, :cond_6

    .line 136
    :cond_4
    const v0, 0x7f0700b3

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/AccountsActivity;->f(I)V

    .line 214
    :goto_1
    return-void

    :cond_5
    move v0, v2

    .line 135
    goto :goto_0

    .line 143
    :cond_6
    const-string v0, "showAccountOfficer"

    invoke-static {v4, v0}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 145
    const-string v0, "showAccountOfficer"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 146
    invoke-interface {v3, v0}, Lcom/chase/sig/android/domain/g;->a(Ljava/lang/String;)Lcom/chase/sig/android/domain/IAccount;

    move-result-object v1

    .line 148
    const-string v0, "customerId"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 149
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/ChaseApplication;

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->b:Lcom/chase/sig/android/domain/g;

    invoke-interface {v0, v3}, Lcom/chase/sig/android/domain/g;->d(Ljava/lang/String;)Lcom/chase/sig/android/domain/e;

    move-result-object v0

    .line 150
    invoke-interface {v0}, Lcom/chase/sig/android/domain/e;->c()Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    new-instance v4, Lcom/chase/sig/android/view/ah;

    invoke-direct {v4, p0, v3}, Lcom/chase/sig/android/view/ah;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    invoke-direct {p0, v1, v3, v0}, Lcom/chase/sig/android/activity/AccountsActivity;->a(Lcom/chase/sig/android/domain/IAccount;Ljava/util/ArrayList;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountsActivity;->b:Landroid/widget/ListView;

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 213
    :cond_7
    :goto_2
    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountsActivity;->b:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    goto :goto_1

    .line 152
    :cond_8
    const-string v0, "showParticularCustomer"

    invoke-static {v4, v0}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 154
    const-string v0, "showParticularCustomer"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 155
    invoke-interface {v3, v0}, Lcom/chase/sig/android/domain/g;->b(Ljava/lang/String;)Lcom/chase/sig/android/domain/e;

    move-result-object v0

    .line 156
    invoke-direct {p0, v0}, Lcom/chase/sig/android/activity/AccountsActivity;->a(Lcom/chase/sig/android/domain/e;)V

    goto :goto_2

    .line 159
    :cond_9
    const-string v0, "showCustomerGroup"

    invoke-static {v4, v0}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 161
    const-string v0, "showCustomerGroup"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 163
    if-nez v0, :cond_b

    .line 164
    invoke-interface {v3}, Lcom/chase/sig/android/domain/g;->o()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v2, :cond_a

    .line 165
    invoke-direct {p0}, Lcom/chase/sig/android/activity/AccountsActivity;->d()V

    goto :goto_2

    .line 167
    :cond_a
    invoke-interface {v3}, Lcom/chase/sig/android/domain/g;->o()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/e;

    .line 168
    invoke-direct {p0, v0}, Lcom/chase/sig/android/activity/AccountsActivity;->a(Lcom/chase/sig/android/domain/e;)V

    goto :goto_2

    .line 171
    :cond_b
    invoke-direct {p0}, Lcom/chase/sig/android/activity/AccountsActivity;->e()V

    goto :goto_2

    .line 175
    :cond_c
    const-string v0, "showBusinessAccounts"

    invoke-static {v4, v0}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 176
    const-string v0, "showBusinessAccounts"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 179
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/ChaseApplication;

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->b:Lcom/chase/sig/android/domain/g;

    invoke-interface {v0, v1}, Lcom/chase/sig/android/domain/g;->d(Ljava/lang/String;)Lcom/chase/sig/android/domain/e;

    move-result-object v0

    .line 180
    invoke-direct {p0, v0}, Lcom/chase/sig/android/activity/AccountsActivity;->a(Lcom/chase/sig/android/domain/e;)V

    goto :goto_2

    .line 209
    :cond_d
    invoke-interface {v3}, Lcom/chase/sig/android/domain/g;->s()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-interface {v3}, Lcom/chase/sig/android/domain/g;->J()Z

    move-result v0

    if-nez v0, :cond_e

    invoke-interface {v3}, Lcom/chase/sig/android/domain/g;->K()Z

    move-result v0

    if-nez v0, :cond_e

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v3, Lcom/chase/sig/android/view/ai;

    invoke-direct {v3}, Lcom/chase/sig/android/view/ai;-><init>()V

    iput v1, v3, Lcom/chase/sig/android/view/ai;->e:I

    const v1, 0x7f0300b4

    iput v1, v3, Lcom/chase/sig/android/view/ai;->a:I

    const v1, 0x7f0700fc

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/AccountsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v3, Lcom/chase/sig/android/view/ai;->b:Ljava/lang/String;

    const-string v1, ""

    iput-object v1, v3, Lcom/chase/sig/android/view/ai;->c:Ljava/lang/String;

    iput-boolean v2, v3, Lcom/chase/sig/android/view/ai;->f:Z

    const-string v1, "0"

    iput-object v1, v3, Lcom/chase/sig/android/view/ai;->d:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/chase/sig/android/view/ai;

    invoke-direct {v1}, Lcom/chase/sig/android/view/ai;-><init>()V

    iput v2, v1, Lcom/chase/sig/android/view/ai;->e:I

    const v3, 0x7f0300b5

    iput v3, v1, Lcom/chase/sig/android/view/ai;->a:I

    const v3, 0x7f0700fd

    invoke-virtual {p0, v3}, Lcom/chase/sig/android/activity/AccountsActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/chase/sig/android/view/ai;->b:Ljava/lang/String;

    const-string v3, ""

    iput-object v3, v1, Lcom/chase/sig/android/view/ai;->c:Ljava/lang/String;

    iput-boolean v2, v1, Lcom/chase/sig/android/view/ai;->f:Z

    const-string v3, "1"

    iput-object v3, v1, Lcom/chase/sig/android/view/ai;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-direct {p0, v0}, Lcom/chase/sig/android/activity/AccountsActivity;->a(Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountsActivity;->b:Landroid/widget/ListView;

    new-instance v1, Lcom/chase/sig/android/activity/x;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/x;-><init>(Lcom/chase/sig/android/activity/AccountsActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    goto/16 :goto_2

    :cond_e
    invoke-interface {v3}, Lcom/chase/sig/android/domain/g;->r()Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-interface {v3}, Lcom/chase/sig/android/domain/g;->K()Z

    move-result v0

    if-nez v0, :cond_f

    invoke-direct {p0}, Lcom/chase/sig/android/activity/AccountsActivity;->d()V

    goto/16 :goto_2

    :cond_f
    invoke-interface {v3}, Lcom/chase/sig/android/domain/g;->q()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v3}, Lcom/chase/sig/android/domain/g;->J()Z

    move-result v0

    if-nez v0, :cond_7

    invoke-direct {p0}, Lcom/chase/sig/android/activity/AccountsActivity;->e()V

    goto/16 :goto_2
.end method

.method protected final a(Lcom/chase/sig/android/view/ai;)V
    .locals 4
    .parameter

    .prologue
    .line 302
    new-instance v1, Landroid/content/Intent;

    const-class v0, Lcom/chase/sig/android/activity/AccountsActivity;

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 304
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/ChaseApplication;

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->b:Lcom/chase/sig/android/domain/g;

    iget-object v2, p1, Lcom/chase/sig/android/view/ai;->d:Ljava/lang/String;

    invoke-interface {v0, v2}, Lcom/chase/sig/android/domain/g;->c(Ljava/lang/String;)Lcom/chase/sig/android/domain/e;

    move-result-object v0

    .line 306
    const-string v2, "showAccountOfficer"

    iget-object v3, p1, Lcom/chase/sig/android/view/ai;->d:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 307
    const-string v2, "customerId"

    invoke-interface {v0}, Lcom/chase/sig/android/domain/e;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 309
    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/AccountsActivity;->startActivity(Landroid/content/Intent;)V

    .line 310
    return-void
.end method

.method protected final b(Lcom/chase/sig/android/view/ai;)V
    .locals 3
    .parameter

    .prologue
    .line 313
    iget-boolean v0, p1, Lcom/chase/sig/android/view/ai;->f:Z

    if-nez v0, :cond_0

    .line 320
    :goto_0
    return-void

    .line 317
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 318
    const-string v1, "selectedAccountId"

    iget-object v2, p1, Lcom/chase/sig/android/view/ai;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 319
    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/AccountsActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method protected final c(Lcom/chase/sig/android/view/ai;)V
    .locals 4
    .parameter

    .prologue
    .line 323
    new-instance v1, Landroid/content/Intent;

    const-class v0, Lcom/chase/sig/android/activity/AccountRewardsDetailActivity;

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 324
    const-string v0, "selectedAccountId"

    iget-object v2, p1, Lcom/chase/sig/android/view/ai;->d:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 325
    const-string v2, "isATMRewards"

    iget v0, p1, Lcom/chase/sig/android/view/ai;->e:I

    const/16 v3, 0x9

    if-ne v0, v3, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 327
    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/AccountsActivity;->startActivity(Landroid/content/Intent;)V

    .line 328
    return-void

    .line 325
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final d(Lcom/chase/sig/android/view/ai;)V
    .locals 7
    .parameter

    .prologue
    const/4 v6, 0x0

    .line 361
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/ChaseApplication;

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->b:Lcom/chase/sig/android/domain/g;

    iget-object v1, p1, Lcom/chase/sig/android/view/ai;->d:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/chase/sig/android/domain/g;->a(Ljava/lang/String;)Lcom/chase/sig/android/domain/IAccount;

    move-result-object v0

    .line 362
    invoke-static {v0}, Lcom/chase/sig/android/activity/AccountsActivity;->f(Lcom/chase/sig/android/domain/IAccount;)Lcom/chase/sig/android/activity/AccountEntitlementHandler;

    move-result-object v1

    .line 364
    if-nez v1, :cond_0

    .line 378
    :goto_0
    return-void

    .line 368
    :cond_0
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {v1}, Lcom/chase/sig/android/activity/AccountEntitlementHandler;->b()Ljava/lang/Class;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 369
    const-string v3, "selectedAccountId"

    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 370
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const-string v4, "PPA"

    aput-object v4, v3, v6

    const/4 v4, 0x1

    const-string v5, "PPX"

    aput-object v5, v3, v4

    invoke-interface {v0, v3}, Lcom/chase/sig/android/domain/IAccount;->a([Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 372
    const-string v0, "isDebitAccount"

    invoke-virtual {v2, v0, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 377
    :goto_1
    invoke-virtual {p0, v2}, Lcom/chase/sig/android/activity/AccountsActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 374
    :cond_1
    const-string v0, "isDebitAccount"

    sget-object v3, Lcom/chase/sig/android/activity/AccountEntitlementHandler;->d:Lcom/chase/sig/android/activity/AccountEntitlementHandler;

    invoke-virtual {v3, v1}, Lcom/chase/sig/android/activity/AccountEntitlementHandler;->equals(Ljava/lang/Object;)Z

    move-result v1

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_1
.end method

.method protected final e(Lcom/chase/sig/android/view/ai;)V
    .locals 3
    .parameter

    .prologue
    .line 381
    iget-object v0, p1, Lcom/chase/sig/android/view/ai;->d:Ljava/lang/String;

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 382
    new-instance v1, Landroid/content/Intent;

    const-class v0, Lcom/chase/sig/android/activity/AccountDetailActivity;

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 384
    const-string v0, "selectedAccountId"

    iget-object v2, p1, Lcom/chase/sig/android/view/ai;->d:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 386
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/ChaseApplication;

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->b:Lcom/chase/sig/android/domain/g;

    iget-object v2, p1, Lcom/chase/sig/android/view/ai;->d:Ljava/lang/String;

    invoke-interface {v0, v2}, Lcom/chase/sig/android/domain/g;->a(Ljava/lang/String;)Lcom/chase/sig/android/domain/IAccount;

    move-result-object v0

    .line 387
    const-string v2, "accountDetails"

    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->K()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/io/Serializable;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 390
    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/AccountsActivity;->startActivity(Landroid/content/Intent;)V

    .line 392
    :cond_0
    return-void
.end method

.method protected final f(Lcom/chase/sig/android/view/ai;)V
    .locals 3
    .parameter

    .prologue
    .line 395
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 397
    const-string v1, "selectedAccountId"

    iget-object v2, p1, Lcom/chase/sig/android/view/ai;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 398
    const-string v1, "outstandingValue"

    iget-object v2, p1, Lcom/chase/sig/android/view/ai;->l:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 399
    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/AccountsActivity;->startActivity(Landroid/content/Intent;)V

    .line 400
    return-void
.end method

.method protected final g(Lcom/chase/sig/android/view/ai;)V
    .locals 3
    .parameter

    .prologue
    .line 403
    iget-boolean v0, p1, Lcom/chase/sig/android/view/ai;->f:Z

    if-nez v0, :cond_0

    .line 412
    :goto_0
    return-void

    .line 407
    :cond_0
    new-instance v1, Landroid/content/Intent;

    iget-boolean v0, p1, Lcom/chase/sig/android/view/ai;->h:Z

    if-eqz v0, :cond_1

    const-class v0, Lcom/chase/sig/android/activity/PositionsActivity;

    :goto_1
    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 410
    const-string v0, "selectedAccountId"

    iget-object v2, p1, Lcom/chase/sig/android/view/ai;->d:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 411
    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/AccountsActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 407
    :cond_1
    const-class v0, Lcom/chase/sig/android/activity/AccountActivityActivity;

    goto :goto_1
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1241
    const/16 v0, 0x3f2

    if-ne p1, v0, :cond_0

    .line 1242
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 1245
    invoke-direct {p0}, Lcom/chase/sig/android/activity/AccountsActivity;->g()V

    .line 1251
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/chase/sig/android/activity/ai;->onActivityResult(IILandroid/content/Intent;)V

    .line 1252
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 4
    .parameter

    .prologue
    .line 1137
    new-instance v0, Lcom/chase/sig/android/view/k$a;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/view/k$a;-><init>(Landroid/content/Context;)V

    .line 1139
    packed-switch p1, :pswitch_data_0

    .line 1160
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ai;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    .line 1141
    :pswitch_0
    const v1, 0x7f070083

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/k$a;->b(I)Lcom/chase/sig/android/view/k$a;

    move-result-object v1

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/chase/sig/android/view/k$a;->i:Z

    const v2, 0x7f07006b

    new-instance v3, Lcom/chase/sig/android/activity/aa;

    invoke-direct {v3, p0}, Lcom/chase/sig/android/activity/aa;-><init>(Lcom/chase/sig/android/activity/AccountsActivity;)V

    invoke-virtual {v1, v2, v3}, Lcom/chase/sig/android/view/k$a;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    move-result-object v1

    const v2, 0x7f070069

    new-instance v3, Lcom/chase/sig/android/activity/z;

    invoke-direct {v3, p0}, Lcom/chase/sig/android/activity/z;-><init>(Lcom/chase/sig/android/activity/AccountsActivity;)V

    invoke-virtual {v1, v2, v3}, Lcom/chase/sig/android/view/k$a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    .line 1157
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/k$a;->d(I)Lcom/chase/sig/android/view/k;

    move-result-object v0

    goto :goto_0

    .line 1139
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 1122
    invoke-virtual {p0, p1}, Lcom/chase/sig/android/activity/AccountsActivity;->j(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1131
    :goto_0
    return v0

    .line 1126
    :cond_0
    const/4 v1, 0x4

    if-ne p1, v1, :cond_1

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/AccountsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1127
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/AccountsActivity;->showDialog(I)V

    goto :goto_0

    .line 1131
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/chase/sig/android/activity/ai;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 296
    invoke-super {p0}, Lcom/chase/sig/android/activity/ai;->onResume()V

    .line 297
    iget-boolean v0, p0, Lcom/chase/sig/android/activity/AccountsActivity;->d:Z

    if-nez v0, :cond_2

    sget-boolean v0, Lcom/chase/sig/android/ChaseApplication;->a:Z

    if-nez v0, :cond_2

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->v()Lcom/chase/sig/android/domain/MobileAdResponse;

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/chase/sig/android/activity/u;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/u;-><init>(Lcom/chase/sig/android/activity/AccountsActivity;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 298
    :cond_0
    :goto_0
    return-void

    .line 297
    :cond_1
    const-class v0, Lcom/chase/sig/android/activity/AccountsActivity$a;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/AccountsActivity;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountsActivity;->c:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/AccountsActivity;->c:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method
