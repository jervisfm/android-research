.class public Lcom/chase/sig/android/activity/ArticleActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$c;


# instance fields
.field private a:Lcom/chase/sig/android/domain/QuoteNewsArticle;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 3
    .parameter

    .prologue
    .line 19
    const v0, 0x7f0702c9

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ArticleActivity;->setTitle(I)V

    .line 20
    const v0, 0x7f0300ac

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ArticleActivity;->b(I)V

    .line 22
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/ArticleActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "article_content"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/QuoteNewsArticle;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ArticleActivity;->a:Lcom/chase/sig/android/domain/QuoteNewsArticle;

    .line 26
    iget-object v0, p0, Lcom/chase/sig/android/activity/ArticleActivity;->a:Lcom/chase/sig/android/domain/QuoteNewsArticle;

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 27
    const-string v0, "current_article"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/QuoteNewsArticle;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ArticleActivity;->a:Lcom/chase/sig/android/domain/QuoteNewsArticle;

    .line 30
    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/ArticleActivity;->a:Lcom/chase/sig/android/domain/QuoteNewsArticle;

    if-eqz v0, :cond_1

    .line 31
    const v0, 0x7f0902ae

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ArticleActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/chase/sig/android/activity/ArticleActivity;->a:Lcom/chase/sig/android/domain/QuoteNewsArticle;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/QuoteNewsArticle;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    sget-object v2, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 33
    const v0, 0x7f0902af

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ArticleActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/chase/sig/android/activity/ArticleActivity;->a:Lcom/chase/sig/android/domain/QuoteNewsArticle;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/QuoteNewsArticle;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 35
    const v0, 0x7f0902b0

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ArticleActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/chase/sig/android/activity/ArticleActivity;->a:Lcom/chase/sig/android/domain/QuoteNewsArticle;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/QuoteNewsArticle;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 36
    const v0, 0x7f0902b1

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ArticleActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 37
    iget-object v1, p0, Lcom/chase/sig/android/activity/ArticleActivity;->a:Lcom/chase/sig/android/domain/QuoteNewsArticle;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/QuoteNewsArticle;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    sget-object v2, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 39
    const/16 v1, 0xf

    invoke-static {v0, v1}, Landroid/text/util/Linkify;->addLinks(Landroid/widget/TextView;I)Z

    .line 42
    :cond_1
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 46
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ai;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 47
    const-string v0, "current_article"

    iget-object v1, p0, Lcom/chase/sig/android/activity/ArticleActivity;->a:Lcom/chase/sig/android/domain/QuoteNewsArticle;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 48
    return-void
.end method
