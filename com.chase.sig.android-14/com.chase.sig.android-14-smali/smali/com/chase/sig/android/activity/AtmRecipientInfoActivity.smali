.class public Lcom/chase/sig/android/activity/AtmRecipientInfoActivity;
.super Lcom/chase/sig/android/activity/eb;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/chase/sig/android/activity/eb;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 4
    .parameter

    .prologue
    .line 14
    const v0, 0x7f03007e

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/AtmRecipientInfoActivity;->b(I)V

    .line 15
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/AtmRecipientInfoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "quick_pay_transaction"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/QuickPayAcceptMoneyTransaction;

    .line 17
    const v1, 0x7f0901fe

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/AtmRecipientInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayAcceptMoneyTransaction;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 18
    const v1, 0x7f0901ff

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/AtmRecipientInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "You must get this from "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayAcceptMoneyTransaction;->j()Lcom/chase/sig/android/domain/QuickPayRecipient;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayRecipient;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 20
    return-void
.end method
