.class public Lcom/chase/sig/android/activity/AtmSenderInfoActivity;
.super Lcom/chase/sig/android/activity/eb;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/chase/sig/android/activity/eb;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 3
    .parameter

    .prologue
    .line 13
    const v0, 0x7f030080

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/AtmSenderInfoActivity;->b(I)V

    .line 14
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/AtmSenderInfoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "quick_pay_transaction"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/h;

    .line 15
    const v1, 0x7f090202

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/AtmSenderInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-interface {v0}, Lcom/chase/sig/android/domain/h;->j()Lcom/chase/sig/android/domain/QuickPayRecipient;

    move-result-object v2

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/QuickPayRecipient;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 16
    const v1, 0x7f090204

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/AtmSenderInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-interface {v0}, Lcom/chase/sig/android/domain/h;->j()Lcom/chase/sig/android/domain/QuickPayRecipient;

    move-result-object v2

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/QuickPayRecipient;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 17
    const v1, 0x7f090203

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/AtmSenderInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-interface {v0}, Lcom/chase/sig/android/domain/h;->j()Lcom/chase/sig/android/domain/QuickPayRecipient;

    move-result-object v2

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/QuickPayRecipient;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 18
    const v1, 0x7f0901ff

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/AtmSenderInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-interface {v0}, Lcom/chase/sig/android/domain/h;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 19
    return-void
.end method
