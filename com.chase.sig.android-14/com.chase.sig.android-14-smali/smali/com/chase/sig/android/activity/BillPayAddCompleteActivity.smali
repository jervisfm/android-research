.class public Lcom/chase/sig/android/activity/BillPayAddCompleteActivity;
.super Lcom/chase/sig/android/activity/m;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/BillPayAddCompleteActivity$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/activity/m",
        "<",
        "Lcom/chase/sig/android/domain/BillPayTransaction;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/chase/sig/android/activity/m;-><init>()V

    .line 47
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1
    .parameter

    .prologue
    .line 18
    const v0, 0x7f070074

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayAddCompleteActivity;->setTitle(I)V

    .line 19
    const v0, 0x7f03005e

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayAddCompleteActivity;->b(I)V

    .line 20
    invoke-static {}, Lcom/chase/sig/android/c;->a()V

    .line 21
    const-class v0, Lcom/chase/sig/android/activity/BillPayAddCompleteActivity$a;

    invoke-virtual {p0, p1, v0}, Lcom/chase/sig/android/activity/BillPayAddCompleteActivity;->a(Landroid/os/Bundle;Ljava/lang/Class;)V

    .line 22
    return-void
.end method

.method protected final a(Z)V
    .locals 7
    .parameter

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/ChaseApplication;

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v1, v0, Lcom/chase/sig/android/domain/o;->b:Lcom/chase/sig/android/domain/g;

    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayAddCompleteActivity;->a:Lcom/chase/sig/android/domain/Transaction;

    check-cast v0, Lcom/chase/sig/android/domain/BillPayTransaction;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayTransaction;->c()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/chase/sig/android/domain/g;->a(Ljava/lang/String;)Lcom/chase/sig/android/domain/IAccount;

    move-result-object v2

    .line 28
    const v0, 0x7f090153

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayAddCompleteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailView;

    .line 29
    const/16 v1, 0x8

    new-array v3, v1, [Lcom/chase/sig/android/view/detail/a;

    const/4 v4, 0x0

    new-instance v5, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v6, "Transaction Number"

    iget-object v1, p0, Lcom/chase/sig/android/activity/BillPayAddCompleteActivity;->a:Lcom/chase/sig/android/domain/Transaction;

    check-cast v1, Lcom/chase/sig/android/domain/BillPayTransaction;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/BillPayTransaction;->n()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v5, v6, v1}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v3, v4

    const/4 v4, 0x1

    new-instance v5, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v6, "Pay To"

    iget-object v1, p0, Lcom/chase/sig/android/activity/BillPayAddCompleteActivity;->a:Lcom/chase/sig/android/domain/Transaction;

    check-cast v1, Lcom/chase/sig/android/domain/BillPayTransaction;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/BillPayTransaction;->h()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v5, v6, v1}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v3, v4

    const/4 v1, 0x2

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Pay From"

    invoke-interface {v2}, Lcom/chase/sig/android/domain/IAccount;->B()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v5, v2}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v3, v1

    const/4 v2, 0x3

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Send On"

    iget-object v1, p0, Lcom/chase/sig/android/activity/BillPayAddCompleteActivity;->a:Lcom/chase/sig/android/domain/Transaction;

    check-cast v1, Lcom/chase/sig/android/domain/BillPayTransaction;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/BillPayTransaction;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->h(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v5, v1}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v3, v2

    const/4 v2, 0x4

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Deliver By"

    iget-object v1, p0, Lcom/chase/sig/android/activity/BillPayAddCompleteActivity;->a:Lcom/chase/sig/android/domain/Transaction;

    check-cast v1, Lcom/chase/sig/android/domain/BillPayTransaction;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/BillPayTransaction;->q()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->h(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v5, v1}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v3, v2

    const/4 v2, 0x5

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Amount"

    iget-object v1, p0, Lcom/chase/sig/android/activity/BillPayAddCompleteActivity;->a:Lcom/chase/sig/android/domain/Transaction;

    check-cast v1, Lcom/chase/sig/android/domain/BillPayTransaction;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/BillPayTransaction;->e()Lcom/chase/sig/android/util/Dollar;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/util/Dollar;->h()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v5, v1}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v3, v2

    const/4 v2, 0x6

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Memo"

    iget-object v1, p0, Lcom/chase/sig/android/activity/BillPayAddCompleteActivity;->a:Lcom/chase/sig/android/domain/Transaction;

    check-cast v1, Lcom/chase/sig/android/domain/BillPayTransaction;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/BillPayTransaction;->o()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v5, v1}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v3, v2

    const/4 v2, 0x7

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Status"

    iget-object v1, p0, Lcom/chase/sig/android/activity/BillPayAddCompleteActivity;->a:Lcom/chase/sig/android/domain/Transaction;

    check-cast v1, Lcom/chase/sig/android/domain/BillPayTransaction;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/BillPayTransaction;->t()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v5, v1}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v3, v2

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/view/detail/DetailView;->setRows([Lcom/chase/sig/android/view/detail/a;)V

    .line 38
    const v0, 0x7f070184

    const-class v1, Lcom/chase/sig/android/activity/BillPayHomeActivity;

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/BillPayAddCompleteActivity;->b(ILjava/lang/Class;)V

    .line 39
    const v0, 0x7f070183

    const-class v1, Lcom/chase/sig/android/activity/BillPayHistoryActivity;

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/BillPayAddCompleteActivity;->a(ILjava/lang/Class;)V

    .line 40
    return-void
.end method

.method protected final b()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/chase/sig/android/activity/eb;",
            ">;"
        }
    .end annotation

    .prologue
    .line 44
    const-class v0, Lcom/chase/sig/android/activity/BillPayHomeActivity;

    return-object v0
.end method
