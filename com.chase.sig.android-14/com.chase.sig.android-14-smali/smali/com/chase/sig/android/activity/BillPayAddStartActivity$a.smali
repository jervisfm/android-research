.class public Lcom/chase/sig/android/activity/BillPayAddStartActivity$a;
.super Lcom/chase/sig/android/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/BillPayAddStartActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/d",
        "<",
        "Lcom/chase/sig/android/activity/BillPayAddStartActivity;",
        "Lcom/chase/sig/android/domain/BillPayTransaction;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/service/movemoney/ServiceResponse;",
        ">;"
    }
.end annotation


# instance fields
.field a:Lcom/chase/sig/android/domain/BillPayTransaction;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 270
    invoke-direct {p0}, Lcom/chase/sig/android/d;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .parameter

    .prologue
    .line 270
    check-cast p1, [Lcom/chase/sig/android/domain/BillPayTransaction;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/BillPayAddStartActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/BillPayAddStartActivity;->A()Lcom/chase/sig/android/service/n;

    invoke-static {}, Lcom/chase/sig/android/service/n;->g()Lcom/chase/sig/android/service/billpay/b;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v1, p1, v1

    iput-object v1, p0, Lcom/chase/sig/android/activity/BillPayAddStartActivity$a;->a:Lcom/chase/sig/android/domain/BillPayTransaction;

    iget-object v1, p0, Lcom/chase/sig/android/activity/BillPayAddStartActivity$a;->a:Lcom/chase/sig/android/domain/BillPayTransaction;

    sget-object v2, Lcom/chase/sig/android/service/movemoney/RequestFlags;->a:Lcom/chase/sig/android/service/movemoney/RequestFlags;

    invoke-virtual {v0, v1, v2}, Lcom/chase/sig/android/service/billpay/b;->b(Lcom/chase/sig/android/domain/Transaction;Lcom/chase/sig/android/service/movemoney/RequestFlags;)Lcom/chase/sig/android/service/movemoney/ServiceResponse;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 6
    .parameter

    .prologue
    .line 270
    check-cast p1, Lcom/chase/sig/android/service/movemoney/ServiceResponse;

    const-string v0, "50532"

    invoke-virtual {p1, v0}, Lcom/chase/sig/android/service/movemoney/ServiceResponse;->c(Ljava/lang/String;)Z

    move-result v2

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/BillPayAddStartActivity;

    const v1, 0x7f07017f

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/BillPayAddStartActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    new-instance v3, Lcom/chase/sig/android/service/ServiceError;

    const-string v4, "50532"

    const/4 v5, 0x0

    invoke-direct {v3, v4, v0, v5}, Lcom/chase/sig/android/service/ServiceError;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    if-nez v2, :cond_0

    invoke-virtual {p1}, Lcom/chase/sig/android/service/movemoney/ServiceResponse;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/BillPayAddStartActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/movemoney/ServiceResponse;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/BillPayAddStartActivity;->c(Ljava/util/List;)V

    :goto_0
    return-void

    :cond_0
    new-instance v3, Landroid/content/Intent;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/BillPayAddStartActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/BillPayAddStartActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    const-class v4, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;

    invoke-direct {v3, v0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v0, 0x400

    invoke-virtual {v3, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-static {v3}, Lcom/chase/sig/android/c;->a(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayAddStartActivity$a;->a:Lcom/chase/sig/android/domain/BillPayTransaction;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/movemoney/ServiceResponse;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/domain/BillPayTransaction;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayAddStartActivity$a;->a:Lcom/chase/sig/android/domain/BillPayTransaction;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/movemoney/ServiceResponse;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/domain/BillPayTransaction;->m(Ljava/lang/String;)V

    new-instance v3, Landroid/content/Intent;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/BillPayAddStartActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/BillPayAddStartActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    const-class v4, Lcom/chase/sig/android/activity/BillPayAddVerifyActivity;

    invoke-direct {v3, v0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v0, "transaction_object"

    iget-object v4, p0, Lcom/chase/sig/android/activity/BillPayAddStartActivity$a;->a:Lcom/chase/sig/android/domain/BillPayTransaction;

    invoke-virtual {v3, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    if-eqz v2, :cond_1

    const-string v2, "queued_errors"

    move-object v0, v1

    check-cast v0, Ljava/io/Serializable;

    invoke-virtual {v3, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    :goto_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/BillPayAddStartActivity;

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/activity/BillPayAddStartActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_1
    const-string v1, "queued_errors"

    invoke-virtual {p1}, Lcom/chase/sig/android/service/movemoney/ServiceResponse;->g()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/io/Serializable;

    invoke-virtual {v3, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    goto :goto_1
.end method
