.class public Lcom/chase/sig/android/activity/BillPayAddStartActivity;
.super Lcom/chase/sig/android/activity/a;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/BillPayAddStartActivity$a;
    }
.end annotation


# instance fields
.field a:Lcom/chase/sig/android/view/b$a;

.field private b:Z

.field private c:Lcom/chase/sig/android/view/detail/DetailView;

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/chase/sig/android/activity/a;-><init>()V

    .line 54
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/chase/sig/android/activity/BillPayAddStartActivity;->b:Z

    .line 243
    new-instance v0, Lcom/chase/sig/android/activity/al;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/activity/al;-><init>(Lcom/chase/sig/android/activity/BillPayAddStartActivity;)V

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayAddStartActivity;->a:Lcom/chase/sig/android/view/b$a;

    .line 270
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/BillPayAddStartActivity;)Z
    .locals 1
    .parameter

    .prologue
    .line 52
    iget-boolean v0, p0, Lcom/chase/sig/android/activity/BillPayAddStartActivity;->b:Z

    return v0
.end method

.method private c(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 332
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayAddStartActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private d()Lcom/chase/sig/android/domain/Payee;
    .locals 1

    .prologue
    .line 336
    const-string v0, "payee"

    invoke-direct {p0, v0}, Lcom/chase/sig/android/activity/BillPayAddStartActivity;->c(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/Payee;

    return-object v0
.end method

.method private e()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/IAccount;",
            ">;"
        }
    .end annotation

    .prologue
    .line 340
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/ChaseApplication;

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->b:Lcom/chase/sig/android/domain/g;

    invoke-interface {v0}, Lcom/chase/sig/android/domain/g;->t()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 11
    .parameter

    .prologue
    const v10, 0x7f0902cb

    const/4 v6, 0x0

    const/4 v4, 0x0

    const/4 v9, 0x1

    .line 68
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/a;->a(Landroid/os/Bundle;)V

    .line 69
    const v0, 0x7f070178

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayAddStartActivity;->setTitle(I)V

    .line 71
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayAddStartActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    .line 72
    const-string v0, "payee"

    invoke-direct {p0, v0}, Lcom/chase/sig/android/activity/BillPayAddStartActivity;->c(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/Payee;

    .line 73
    const-string v1, "selectedAccountId"

    invoke-static {v3, v1, v6}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 76
    const-string v2, "scheduledAmount"

    invoke-static {v3, v2, v6}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 78
    const-string v5, "deliverBydate"

    invoke-static {v3, v5, v6}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    iput-object v3, p0, Lcom/chase/sig/android/activity/BillPayAddStartActivity;->d:Ljava/lang/String;

    .line 81
    const v3, 0x7f0902c9

    invoke-virtual {p0, v3}, Lcom/chase/sig/android/activity/BillPayAddStartActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/chase/sig/android/view/detail/DetailView;

    iput-object v3, p0, Lcom/chase/sig/android/activity/BillPayAddStartActivity;->c:Lcom/chase/sig/android/view/detail/DetailView;

    .line 82
    iget-object v5, p0, Lcom/chase/sig/android/activity/BillPayAddStartActivity;->c:Lcom/chase/sig/android/view/detail/DetailView;

    const/4 v3, 0x7

    new-array v6, v3, [Lcom/chase/sig/android/view/detail/a;

    new-instance v3, Lcom/chase/sig/android/view/detail/d;

    const-string v7, "Pay To"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Payee;->h()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v3, v7, v8}, Lcom/chase/sig/android/view/detail/d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-class v7, Lcom/chase/sig/android/activity/BillPayHomeActivity;

    invoke-virtual {p0, v7}, Lcom/chase/sig/android/activity/BillPayAddStartActivity;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/a/f;

    move-result-object v7

    iput-object v7, v3, Lcom/chase/sig/android/view/detail/d;->a:Landroid/view/View$OnClickListener;

    aput-object v3, v6, v4

    new-instance v3, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v7, "Date Last Paid"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Payee;->k()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v3, v7, v8}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v6, v9

    const/4 v3, 0x2

    new-instance v7, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v8, "Amount Last Paid"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Payee;->l()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v7, v8, v0}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7}, Lcom/chase/sig/android/view/detail/DetailRow;->c()Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    aput-object v0, v6, v3

    const/4 v3, 0x3

    new-instance v0, Lcom/chase/sig/android/view/detail/q;

    const-string v7, "Pay From"

    invoke-direct {v0, v7}, Lcom/chase/sig/android/view/detail/q;-><init>(Ljava/lang/String;)V

    const-string v7, "PAY_FROM"

    iput-object v7, v0, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v0, Lcom/chase/sig/android/view/detail/q;

    const-string v7, "Select Account"

    invoke-virtual {v0, v7}, Lcom/chase/sig/android/view/detail/q;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/q;

    iput-boolean v9, v0, Lcom/chase/sig/android/view/detail/a;->o:Z

    aput-object v0, v6, v3

    const/4 v7, 0x4

    new-instance v0, Lcom/chase/sig/android/view/detail/d;

    const-string v8, "Deliver By"

    if-eqz p1, :cond_0

    const-string v3, "deliverBy"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "deliverBy"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :goto_0
    invoke-direct {v0, v8, v3}, Lcom/chase/sig/android/view/detail/d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "DATE"

    iput-object v3, v0, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v0, Lcom/chase/sig/android/view/detail/d;

    new-instance v3, Lcom/chase/sig/android/activity/am;

    invoke-direct {v3, p0}, Lcom/chase/sig/android/activity/am;-><init>(Lcom/chase/sig/android/activity/BillPayAddStartActivity;)V

    iput-object v3, v0, Lcom/chase/sig/android/view/detail/d;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/d;->q()Lcom/chase/sig/android/view/detail/d;

    move-result-object v0

    aput-object v0, v6, v7

    const/4 v3, 0x5

    new-instance v0, Lcom/chase/sig/android/view/detail/c;

    const-string v7, "Amount $"

    invoke-direct {v0, v7}, Lcom/chase/sig/android/view/detail/c;-><init>(Ljava/lang/String;)V

    const-string v7, "AMOUNT"

    iput-object v7, v0, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v0, Lcom/chase/sig/android/view/detail/c;

    const-string v7, "Enter Amount"

    invoke-virtual {v0, v7}, Lcom/chase/sig/android/view/detail/c;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/c;

    iput-boolean v9, v0, Lcom/chase/sig/android/view/detail/a;->o:Z

    aput-object v0, v6, v3

    const/4 v3, 0x6

    new-instance v0, Lcom/chase/sig/android/view/detail/l;

    const-string v7, "Memo"

    invoke-direct {v0, v7}, Lcom/chase/sig/android/view/detail/l;-><init>(Ljava/lang/String;)V

    const-string v7, "MEMO"

    iput-object v7, v0, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v0, Lcom/chase/sig/android/view/detail/l;

    const/16 v7, 0x78

    iput v7, v0, Lcom/chase/sig/android/view/detail/l;->a:I

    const-string v7, "Optional"

    invoke-virtual {v0, v7}, Lcom/chase/sig/android/view/detail/l;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/l;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/l;->q()Lcom/chase/sig/android/view/detail/l;

    move-result-object v0

    aput-object v0, v6, v3

    invoke-virtual {v5, v6}, Lcom/chase/sig/android/view/detail/DetailView;->setRows([Lcom/chase/sig/android/view/detail/a;)V

    .line 95
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayAddStartActivity;->c:Lcom/chase/sig/android/view/detail/DetailView;

    invoke-virtual {p0, v0, v10}, Lcom/chase/sig/android/activity/BillPayAddStartActivity;->a(Lcom/chase/sig/android/view/detail/DetailView;I)V

    .line 97
    invoke-direct {p0}, Lcom/chase/sig/android/activity/BillPayAddStartActivity;->e()Ljava/util/List;

    move-result-object v5

    .line 98
    const-string v0, "PAY_FROM"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayAddStartActivity;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/q;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/q;->q()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v6

    .line 100
    invoke-static {p0, v5}, Lcom/chase/sig/android/util/a;->c(Landroid/content/Context;Ljava/util/List;)Landroid/widget/SimpleAdapter;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/chase/sig/android/view/JPSpinner;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 102
    invoke-static {v1}, Lcom/chase/sig/android/util/s;->l(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v9, :cond_2

    :goto_1
    invoke-virtual {v6, v4}, Lcom/chase/sig/android/view/JPSpinner;->setSelection(I)V

    .line 104
    const-string v0, "AMOUNT"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayAddStartActivity;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/c;

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/view/detail/c;->b(Ljava/lang/String;)V

    .line 106
    const v0, 0x7f0902ca

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayAddStartActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const v1, 0x7f07015b

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 108
    new-instance v0, Lcom/chase/sig/android/activity/aj;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/activity/aj;-><init>(Lcom/chase/sig/android/activity/BillPayAddStartActivity;)V

    invoke-virtual {p0, v10, v0}, Lcom/chase/sig/android/activity/BillPayAddStartActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 118
    return-void

    .line 82
    :cond_0
    iget-object v3, p0, Lcom/chase/sig/android/activity/BillPayAddStartActivity;->d:Ljava/lang/String;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/chase/sig/android/activity/BillPayAddStartActivity;->d:Ljava/lang/String;

    :goto_2
    invoke-static {v3}, Lcom/chase/sig/android/util/s;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/chase/sig/android/activity/BillPayAddStartActivity;->d()Lcom/chase/sig/android/domain/Payee;

    move-result-object v3

    invoke-virtual {v3}, Lcom/chase/sig/android/domain/Payee;->e()Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    :cond_2
    move v3, v4

    .line 102
    :goto_3
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_4

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/IAccount;

    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v4, v3

    goto :goto_1

    :cond_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    :cond_4
    const/4 v4, -0x1

    goto :goto_1
.end method

.method protected final b()V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 147
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayAddStartActivity;->y()V

    new-instance v0, Lcom/chase/sig/android/view/detail/i;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayAddStartActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/chase/sig/android/view/detail/i;-><init>(Lcom/chase/sig/android/view/detail/DetailView;)V

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/i;->a()Z

    move-result v2

    const-string v0, "DATE"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayAddStartActivity;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/d;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/d;->p()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    if-nez v0, :cond_5

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayAddStartActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v0

    const-string v2, "DATE"

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/view/detail/DetailView;->a(Ljava/lang/String;)V

    move v0, v1

    :goto_1
    if-eqz v0, :cond_0

    .line 149
    new-instance v2, Lcom/chase/sig/android/domain/BillPayTransaction;

    invoke-direct {v2}, Lcom/chase/sig/android/domain/BillPayTransaction;-><init>()V

    const-string v0, "AMOUNT"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayAddStartActivity;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/c;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/c;->q()Lcom/chase/sig/android/util/Dollar;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/chase/sig/android/domain/BillPayTransaction;->a(Lcom/chase/sig/android/util/Dollar;)V

    const-string v0, "DATE"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayAddStartActivity;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/d;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/d;->p()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->t(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/chase/sig/android/domain/BillPayTransaction;->a(Ljava/util/Date;)V

    const-string v0, "PAY_FROM"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayAddStartActivity;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/q;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/q;->q()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/view/JPSpinner;->getSelectedItemPosition()I

    move-result v0

    invoke-direct {p0}, Lcom/chase/sig/android/activity/BillPayAddStartActivity;->e()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/IAccount;

    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/chase/sig/android/domain/BillPayTransaction;->b(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayAddStartActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v0

    const-string v4, "MEMO"

    invoke-virtual {v0, v4}, Lcom/chase/sig/android/view/detail/DetailView;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayAddStartActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v0

    const-string v4, "MEMO"

    invoke-virtual {v0, v4}, Lcom/chase/sig/android/view/detail/DetailView;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-virtual {v2, v0}, Lcom/chase/sig/android/domain/BillPayTransaction;->l(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/chase/sig/android/activity/BillPayAddStartActivity;->d()Lcom/chase/sig/android/domain/Payee;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/chase/sig/android/domain/BillPayTransaction;->a(Lcom/chase/sig/android/domain/Payee;)V

    .line 150
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayAddStartActivity;->e:Lcom/chase/sig/android/activity/mc;

    const-class v4, Lcom/chase/sig/android/activity/BillPayAddStartActivity$a;

    invoke-virtual {v0, v4}, Lcom/chase/sig/android/activity/mc;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/ae;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/activity/BillPayAddStartActivity$a;

    .line 153
    invoke-virtual {v0}, Lcom/chase/sig/android/activity/BillPayAddStartActivity$a;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v4

    sget-object v5, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-eq v4, v5, :cond_0

    .line 154
    new-array v3, v3, [Lcom/chase/sig/android/domain/BillPayTransaction;

    aput-object v2, v3, v1

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/activity/BillPayAddStartActivity$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 157
    :cond_0
    return-void

    .line 147
    :cond_1
    const-string v4, "N/A"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    move v0, v1

    goto/16 :goto_0

    :cond_2
    invoke-static {v0}, Lcom/chase/sig/android/util/s;->t(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-direct {p0}, Lcom/chase/sig/android/activity/BillPayAddStartActivity;->d()Lcom/chase/sig/android/domain/Payee;

    move-result-object v4

    invoke-virtual {v4}, Lcom/chase/sig/android/domain/Payee;->f()Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    goto/16 :goto_0

    :cond_3
    move v0, v3

    goto/16 :goto_0

    .line 149
    :cond_4
    const-string v0, ""

    goto :goto_2

    :cond_5
    move v0, v2

    goto/16 :goto_1
.end method

.method protected final c()V
    .locals 4

    .prologue
    .line 214
    new-instance v0, Lcom/chase/sig/android/view/k$a;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/view/k$a;-><init>(Landroid/content/Context;)V

    .line 215
    const v1, 0x7f07017c

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/k$a;->b(I)Lcom/chase/sig/android/view/k$a;

    move-result-object v1

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/chase/sig/android/view/k$a;->i:Z

    const v2, 0x7f070068

    new-instance v3, Lcom/chase/sig/android/activity/ak;

    invoke-direct {v3, p0}, Lcom/chase/sig/android/activity/ak;-><init>(Lcom/chase/sig/android/activity/BillPayAddStartActivity;)V

    invoke-virtual {v1, v2, v3}, Lcom/chase/sig/android/view/k$a;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    .line 222
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/k$a;->d(I)Lcom/chase/sig/android/view/k;

    move-result-object v0

    .line 223
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 224
    return-void
.end method

.method public final k()V
    .locals 1

    .prologue
    .line 267
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayAddStartActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/DetailView;->a()V

    .line 268
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 3
    .parameter

    .prologue
    .line 229
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/a;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    .line 231
    packed-switch p1, :pswitch_data_0

    .line 240
    :goto_0
    return-object v0

    .line 234
    :pswitch_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 235
    invoke-direct {p0}, Lcom/chase/sig/android/activity/BillPayAddStartActivity;->d()Lcom/chase/sig/android/domain/Payee;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/Payee;->f()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 236
    iget-object v1, p0, Lcom/chase/sig/android/activity/BillPayAddStartActivity;->a:Lcom/chase/sig/android/view/b$a;

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/BillPayAddStartActivity;->a(Ljava/util/Calendar;Lcom/chase/sig/android/view/b$a;)Lcom/chase/sig/android/view/b;

    move-result-object v0

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayAddStartActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v1

    const-string v2, "DATE"

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/view/detail/DetailView;->e(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/view/detail/a;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->t(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/b;->a(Ljava/util/Date;)Lcom/chase/sig/android/view/b;

    move-result-object v0

    goto :goto_0

    .line 231
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 345
    invoke-virtual {p0, p1}, Lcom/chase/sig/android/activity/BillPayAddStartActivity;->j(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 353
    :goto_0
    return v0

    .line 349
    :cond_0
    const/4 v1, 0x4

    if-ne p1, v1, :cond_1

    .line 350
    invoke-static {p0, v0}, Lcom/chase/sig/android/activity/BillPayHomeActivity;->a(Landroid/content/Context;Z)V

    goto :goto_0

    .line 353
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/chase/sig/android/activity/a;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 325
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/a;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 327
    const-string v1, "deliverBy"

    const-string v0, "DATE"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayAddStartActivity;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/d;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/d;->p()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 329
    return-void
.end method
