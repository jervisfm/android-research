.class public Lcom/chase/sig/android/activity/BillPayAddVerifyActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$e;
.implements Lcom/chase/sig/android/c$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/BillPayAddVerifyActivity$a;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    .line 62
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 90
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 91
    const/high16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 92
    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayAddVerifyActivity;->startActivity(Landroid/content/Intent;)V

    .line 93
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 9
    .parameter

    .prologue
    const v8, 0x7f090157

    .line 29
    const v0, 0x7f070179

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayAddVerifyActivity;->setTitle(I)V

    .line 30
    const v0, 0x7f03005f

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayAddVerifyActivity;->b(I)V

    .line 32
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayAddVerifyActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "transaction_object"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/BillPayTransaction;

    .line 34
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/ChaseApplication;

    invoke-virtual {v1}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v1

    iget-object v1, v1, Lcom/chase/sig/android/domain/o;->b:Lcom/chase/sig/android/domain/g;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayTransaction;->c()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/chase/sig/android/domain/g;->a(Ljava/lang/String;)Lcom/chase/sig/android/domain/IAccount;

    move-result-object v2

    .line 36
    const v1, 0x7f090153

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/BillPayAddVerifyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/view/detail/DetailView;

    .line 37
    const/4 v3, 0x6

    new-array v3, v3, [Lcom/chase/sig/android/view/detail/a;

    const/4 v4, 0x0

    new-instance v5, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v6, "Pay To"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayTransaction;->a()Lcom/chase/sig/android/domain/Payee;

    move-result-object v7

    invoke-virtual {v7}, Lcom/chase/sig/android/domain/Payee;->h()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v3, v4

    const/4 v4, 0x1

    new-instance v5, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v6, "Pay From"

    invoke-interface {v2}, Lcom/chase/sig/android/domain/IAccount;->w()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v6, v2}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v3, v4

    const/4 v2, 0x2

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Send On"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayTransaction;->f()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/chase/sig/android/util/s;->h(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v3, v2

    const/4 v2, 0x3

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Deliver By"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayTransaction;->q()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/chase/sig/android/util/s;->h(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v3, v2

    const/4 v2, 0x4

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Amount"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayTransaction;->e()Lcom/chase/sig/android/util/Dollar;

    move-result-object v6

    invoke-virtual {v6}, Lcom/chase/sig/android/util/Dollar;->h()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v3, v2

    const/4 v2, 0x5

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Memo"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayTransaction;->o()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v3, v2

    invoke-virtual {v1, v3}, Lcom/chase/sig/android/view/detail/DetailView;->setRows([Lcom/chase/sig/android/view/detail/a;)V

    .line 46
    invoke-virtual {p0, v8}, Lcom/chase/sig/android/activity/BillPayAddVerifyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    const v2, 0x7f07017a

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(I)V

    .line 48
    new-instance v1, Lcom/chase/sig/android/activity/an;

    invoke-direct {v1, p0, v0}, Lcom/chase/sig/android/activity/an;-><init>(Lcom/chase/sig/android/activity/BillPayAddVerifyActivity;Lcom/chase/sig/android/domain/BillPayTransaction;)V

    invoke-virtual {p0, v8, v1}, Lcom/chase/sig/android/activity/BillPayAddVerifyActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 59
    const v0, 0x7f090156

    invoke-virtual {p0, p0}, Lcom/chase/sig/android/activity/BillPayAddVerifyActivity;->a(Lcom/chase/sig/android/c$a;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/BillPayAddVerifyActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 60
    return-void
.end method
