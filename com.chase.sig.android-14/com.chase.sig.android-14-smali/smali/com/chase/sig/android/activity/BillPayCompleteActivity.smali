.class public Lcom/chase/sig/android/activity/BillPayCompleteActivity;
.super Lcom/chase/sig/android/activity/m;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/BillPayCompleteActivity$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/activity/m",
        "<",
        "Lcom/chase/sig/android/domain/BillPayTransaction;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/chase/sig/android/activity/m;-><init>()V

    .line 74
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1
    .parameter

    .prologue
    .line 21
    const v0, 0x7f03005e

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayCompleteActivity;->b(I)V

    .line 22
    const v0, 0x7f070074

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayCompleteActivity;->setTitle(I)V

    .line 23
    invoke-static {}, Lcom/chase/sig/android/c;->a()V

    .line 24
    const-class v0, Lcom/chase/sig/android/activity/BillPayCompleteActivity$a;

    invoke-virtual {p0, p1, v0}, Lcom/chase/sig/android/activity/BillPayCompleteActivity;->a(Landroid/os/Bundle;Ljava/lang/Class;)V

    .line 25
    return-void
.end method

.method protected final a(Z)V
    .locals 8
    .parameter

    .prologue
    .line 84
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayCompleteActivity;->a:Lcom/chase/sig/android/domain/Transaction;

    check-cast v0, Lcom/chase/sig/android/domain/BillPayTransaction;

    .line 86
    const v1, 0x7f090153

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/BillPayCompleteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/view/detail/DetailView;

    .line 88
    const/16 v2, 0xa

    new-array v2, v2, [Lcom/chase/sig/android/view/detail/a;

    const/4 v3, 0x0

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Transaction Number"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayTransaction;->n()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    const/4 v3, 0x1

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Pay To"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayTransaction;->h()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayTransaction;->A()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    const/4 v3, 0x2

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Pay From"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayTransaction;->m()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    const/4 v3, 0x3

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Send On"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayTransaction;->f()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/chase/sig/android/util/s;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    const/4 v3, 0x4

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Deliver By"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayTransaction;->q()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/chase/sig/android/util/s;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    const/4 v3, 0x5

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Amount"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayTransaction;->e()Lcom/chase/sig/android/util/Dollar;

    move-result-object v6

    invoke-virtual {v6}, Lcom/chase/sig/android/util/Dollar;->h()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    const/4 v3, 0x6

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Memo"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayTransaction;->o()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    const/4 v3, 0x7

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Frequency"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayTransaction;->i()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/chase/sig/android/activity/BillPayCompleteActivity;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayCompleteActivity;->c()Z

    move-result v5

    iput-boolean v5, v4, Lcom/chase/sig/android/view/detail/a;->j:Z

    aput-object v4, v2, v3

    const/16 v3, 0x8

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Remaining Payments"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayTransaction;->w()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayCompleteActivity;->c()Z

    move-result v5

    iput-boolean v5, v4, Lcom/chase/sig/android/view/detail/a;->j:Z

    aput-object v4, v2, v3

    const/16 v3, 0x9

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Status"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayTransaction;->t()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v5, v0}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/view/detail/DetailView;->setRows([Lcom/chase/sig/android/view/detail/a;)V

    .line 99
    const v0, 0x7f070162

    const-class v1, Lcom/chase/sig/android/activity/BillPayHistoryActivity;

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/BillPayCompleteActivity;->a(ILjava/lang/Class;)V

    .line 100
    const v0, 0x7f070184

    const-class v1, Lcom/chase/sig/android/activity/BillPayHomeActivity;

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/BillPayCompleteActivity;->b(ILjava/lang/Class;)V

    .line 101
    invoke-virtual {p0, p1}, Lcom/chase/sig/android/activity/BillPayCompleteActivity;->b(Z)V

    .line 102
    return-void
.end method

.method protected final b()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/chase/sig/android/activity/eb;",
            ">;"
        }
    .end annotation

    .prologue
    .line 71
    const-class v0, Lcom/chase/sig/android/activity/BillPayHomeActivity;

    return-object v0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 10
    .parameter

    .prologue
    const v3, 0x7f070068

    const/4 v9, -0x1

    const/4 v8, 0x1

    .line 30
    new-instance v1, Lcom/chase/sig/android/view/k$a;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/view/k$a;-><init>(Landroid/content/Context;)V

    .line 32
    packed-switch p1, :pswitch_data_0

    .line 65
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/m;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    .line 35
    :pswitch_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayCompleteActivity;->a:Lcom/chase/sig/android/domain/Transaction;

    check-cast v0, Lcom/chase/sig/android/domain/BillPayTransaction;

    .line 36
    iput-boolean v8, v1, Lcom/chase/sig/android/view/k$a;->i:Z

    new-instance v2, Lcom/chase/sig/android/activity/ao;

    invoke-direct {v2, p0}, Lcom/chase/sig/android/activity/ao;-><init>(Lcom/chase/sig/android/activity/BillPayCompleteActivity;)V

    invoke-virtual {v1, v3, v2}, Lcom/chase/sig/android/view/k$a;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    move-result-object v2

    const v3, 0x7f07016b

    invoke-virtual {v2, v3}, Lcom/chase/sig/android/view/k$a;->c(I)Lcom/chase/sig/android/view/k$a;

    move-result-object v2

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayCompleteActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f07016d

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayTransaction;->i()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/chase/sig/android/activity/BillPayCompleteActivity;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayTransaction;->r()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayTransaction;->s()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    const/4 v6, 0x2

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayTransaction;->h()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayTransaction;->A()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x3

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayTransaction;->h()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayTransaction;->z()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->s(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v6

    invoke-static {v3, v4, v5}, Lcom/chase/sig/android/util/s;->a(Landroid/content/Context;I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/chase/sig/android/view/k$a;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/k$a;

    .line 54
    invoke-virtual {v1, v9}, Lcom/chase/sig/android/view/k$a;->d(I)Lcom/chase/sig/android/view/k;

    move-result-object v0

    goto/16 :goto_0

    .line 56
    :pswitch_1
    iput-boolean v8, v1, Lcom/chase/sig/android/view/k$a;->i:Z

    new-instance v0, Lcom/chase/sig/android/activity/ap;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/activity/ap;-><init>(Lcom/chase/sig/android/activity/BillPayCompleteActivity;)V

    invoke-virtual {v1, v3, v0}, Lcom/chase/sig/android/view/k$a;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    move-result-object v0

    const v2, 0x7f07016c

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/view/k$a;->b(I)Lcom/chase/sig/android/view/k$a;

    .line 63
    invoke-virtual {v1, v9}, Lcom/chase/sig/android/view/k$a;->d(I)Lcom/chase/sig/android/view/k;

    move-result-object v0

    goto/16 :goto_0

    .line 32
    :pswitch_data_0
    .packed-switch 0xc
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
