.class public Lcom/chase/sig/android/activity/BillPayDetailActivity;
.super Lcom/chase/sig/android/activity/c;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/BillPayDetailActivity$1;,
        Lcom/chase/sig/android/activity/BillPayDetailActivity$b;,
        Lcom/chase/sig/android/activity/BillPayDetailActivity$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/activity/c",
        "<",
        "Lcom/chase/sig/android/domain/BillPayTransaction;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/chase/sig/android/activity/c;-><init>()V

    .line 25
    return-void
.end method


# virtual methods
.method public final a(Lcom/chase/sig/android/activity/DetailResource;)I
    .locals 2
    .parameter

    .prologue
    .line 47
    sget-object v0, Lcom/chase/sig/android/activity/BillPayDetailActivity$1;->a:[I

    invoke-virtual {p1}, Lcom/chase/sig/android/activity/DetailResource;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 71
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 49
    :pswitch_0
    const v0, 0x7f070188

    goto :goto_0

    .line 51
    :pswitch_1
    const v0, 0x7f070187

    goto :goto_0

    .line 53
    :pswitch_2
    const v0, 0x7f07006d

    goto :goto_0

    .line 55
    :pswitch_3
    const v0, 0x7f07006a

    goto :goto_0

    .line 57
    :pswitch_4
    const v0, 0x7f07016a

    goto :goto_0

    .line 59
    :pswitch_5
    const v0, 0x7f070189

    goto :goto_0

    .line 61
    :pswitch_6
    const v0, 0x7f07018a

    goto :goto_0

    .line 63
    :pswitch_7
    const v0, 0x7f070085

    goto :goto_0

    .line 65
    :pswitch_8
    const v0, 0x7f07016f

    goto :goto_0

    .line 67
    :pswitch_9
    const v0, 0x7f07016e

    goto :goto_0

    .line 69
    :pswitch_a
    const v0, 0x7f07017e

    goto :goto_0

    .line 47
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1
    .parameter

    .prologue
    .line 16
    const v0, 0x7f0300c6

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayDetailActivity;->b(I)V

    .line 17
    const v0, 0x7f070186

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayDetailActivity;->setTitle(I)V

    .line 19
    const-class v0, Lcom/chase/sig/android/activity/BillPayDetailActivity$b;

    invoke-virtual {p0, p1, v0}, Lcom/chase/sig/android/activity/BillPayDetailActivity;->a(Landroid/os/Bundle;Ljava/lang/Class;)V

    .line 20
    return-void
.end method

.method protected final synthetic a(Lcom/chase/sig/android/view/detail/DetailView;Lcom/chase/sig/android/domain/Transaction;)V
    .locals 6
    .parameter
    .parameter

    .prologue
    .line 12
    check-cast p2, Lcom/chase/sig/android/domain/BillPayTransaction;

    const/16 v0, 0xa

    new-array v0, v0, [Lcom/chase/sig/android/view/detail/a;

    const/4 v1, 0x0

    new-instance v2, Lcom/chase/sig/android/view/detail/DetailColoredValueRow;

    const-string v3, "Transaction Number"

    invoke-virtual {p2}, Lcom/chase/sig/android/domain/BillPayTransaction;->n()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/chase/sig/android/view/detail/DetailColoredValueRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Lcom/chase/sig/android/view/detail/DetailColoredValueRow;

    const-string v3, "Pay to"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Lcom/chase/sig/android/domain/BillPayTransaction;->h()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p2}, Lcom/chase/sig/android/domain/BillPayTransaction;->A()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/chase/sig/android/view/detail/DetailColoredValueRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-instance v2, Lcom/chase/sig/android/view/detail/DetailColoredValueRow;

    const-string v3, "From"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Lcom/chase/sig/android/domain/BillPayTransaction;->r()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p2}, Lcom/chase/sig/android/domain/BillPayTransaction;->s()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/chase/sig/android/view/detail/DetailColoredValueRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x3

    new-instance v2, Lcom/chase/sig/android/view/detail/DetailColoredValueRow;

    const-string v3, "Amount"

    invoke-virtual {p2}, Lcom/chase/sig/android/domain/BillPayTransaction;->e()Lcom/chase/sig/android/util/Dollar;

    move-result-object v4

    invoke-virtual {v4}, Lcom/chase/sig/android/util/Dollar;->h()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/chase/sig/android/view/detail/DetailColoredValueRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-instance v2, Lcom/chase/sig/android/view/detail/DetailColoredValueRow;

    const-string v3, "Send On"

    invoke-virtual {p2}, Lcom/chase/sig/android/domain/BillPayTransaction;->f()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/chase/sig/android/util/s;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/chase/sig/android/view/detail/DetailColoredValueRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-instance v2, Lcom/chase/sig/android/view/detail/DetailColoredValueRow;

    const-string v3, "Deliver By"

    invoke-virtual {p2}, Lcom/chase/sig/android/domain/BillPayTransaction;->q()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/chase/sig/android/util/s;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/chase/sig/android/view/detail/DetailColoredValueRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-instance v2, Lcom/chase/sig/android/view/detail/DetailColoredValueRow;

    const-string v3, "Status"

    invoke-virtual {p2}, Lcom/chase/sig/android/domain/BillPayTransaction;->t()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/chase/sig/android/view/detail/DetailColoredValueRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const v2, 0x7f07007d

    invoke-virtual {p0, v2}, Lcom/chase/sig/android/activity/BillPayDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lcom/chase/sig/android/domain/BillPayTransaction;->i()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/chase/sig/android/activity/BillPayDetailActivity;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/chase/sig/android/activity/BillPayDetailActivity;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/view/detail/DetailColoredValueRow;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "Remaining Payments"

    invoke-virtual {p2}, Lcom/chase/sig/android/domain/BillPayTransaction;->w()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/chase/sig/android/activity/BillPayDetailActivity;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/view/detail/DetailColoredValueRow;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "Memo"

    invoke-virtual {p2}, Lcom/chase/sig/android/domain/BillPayTransaction;->o()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/chase/sig/android/activity/BillPayDetailActivity;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/view/detail/DetailColoredValueRow;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {p1, v0}, Lcom/chase/sig/android/view/detail/DetailView;->setRows([Lcom/chase/sig/android/view/detail/a;)V

    return-void
.end method

.method protected final b()Lcom/chase/sig/android/service/movemoney/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/chase/sig/android/service/movemoney/d",
            "<",
            "Lcom/chase/sig/android/domain/BillPayTransaction;",
            ">;"
        }
    .end annotation

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    invoke-static {}, Lcom/chase/sig/android/service/af;->a()Lcom/chase/sig/android/service/n;

    invoke-static {}, Lcom/chase/sig/android/service/n;->g()Lcom/chase/sig/android/service/billpay/b;

    move-result-object v0

    return-object v0
.end method

.method protected final c()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/chase/sig/android/activity/ae",
            "<*",
            "Ljava/lang/Boolean;",
            "**>;>;"
        }
    .end annotation

    .prologue
    .line 37
    const-class v0, Lcom/chase/sig/android/activity/BillPayDetailActivity$a;

    return-object v0
.end method

.method protected final d()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/chase/sig/android/activity/eb;",
            ">;"
        }
    .end annotation

    .prologue
    .line 77
    const-class v0, Lcom/chase/sig/android/activity/BillPayCompleteActivity;

    return-object v0
.end method

.method protected final e()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/chase/sig/android/activity/eb;",
            ">;"
        }
    .end annotation

    .prologue
    .line 82
    const-class v0, Lcom/chase/sig/android/activity/BillPayEditActivity;

    return-object v0
.end method
