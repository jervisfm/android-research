.class public Lcom/chase/sig/android/activity/BillPayEditActivity$b;
.super Lcom/chase/sig/android/activity/h$a;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/BillPayEditActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/activity/h$a",
        "<",
        "Lcom/chase/sig/android/domain/BillPayTransaction;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 150
    invoke-direct {p0}, Lcom/chase/sig/android/activity/h$a;-><init>()V

    return-void
.end method


# virtual methods
.method protected final a()Lcom/chase/sig/android/service/movemoney/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/chase/sig/android/service/movemoney/d",
            "<",
            "Lcom/chase/sig/android/domain/BillPayTransaction;",
            ">;"
        }
    .end annotation

    .prologue
    .line 155
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/h;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/h;->A()Lcom/chase/sig/android/service/n;

    invoke-static {}, Lcom/chase/sig/android/service/n;->g()Lcom/chase/sig/android/service/billpay/b;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Lcom/chase/sig/android/service/movemoney/ServiceResponse;)V
    .locals 6
    .parameter

    .prologue
    .line 175
    const-string v0, "50532"

    invoke-virtual {p1, v0}, Lcom/chase/sig/android/service/movemoney/ServiceResponse;->c(Ljava/lang/String;)Z

    move-result v2

    .line 176
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/h;

    const v1, 0x7f07017f

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/h;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 179
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 181
    new-instance v3, Lcom/chase/sig/android/service/ServiceError;

    const-string v4, "50532"

    const/4 v5, 0x0

    invoke-direct {v3, v4, v0, v5}, Lcom/chase/sig/android/service/ServiceError;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 182
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 184
    invoke-virtual {p0, p1}, Lcom/chase/sig/android/activity/BillPayEditActivity$b;->b(Lcom/chase/sig/android/service/movemoney/ServiceResponse;)V

    .line 186
    if-nez v2, :cond_0

    invoke-virtual {p1}, Lcom/chase/sig/android/service/movemoney/ServiceResponse;->f()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    if-eqz v2, :cond_2

    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayEditActivity$b;->a:Lcom/chase/sig/android/domain/Transaction;

    check-cast v0, Lcom/chase/sig/android/domain/BillPayTransaction;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayTransaction;->l()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/h;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/h;->c()Z

    move-result v0

    if-nez v0, :cond_2

    .line 189
    :cond_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/h;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/movemoney/ServiceResponse;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/h;->c(Ljava/util/List;)V

    .line 216
    :goto_0
    return-void

    .line 191
    :cond_2
    new-instance v3, Landroid/content/Intent;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/h;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/h;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-class v4, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;

    invoke-direct {v3, v0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 195
    const/high16 v0, 0x400

    invoke-virtual {v3, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 196
    invoke-static {v3}, Lcom/chase/sig/android/c;->a(Landroid/content/Intent;)V

    .line 198
    new-instance v3, Landroid/content/Intent;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/h;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/h;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    const-class v4, Lcom/chase/sig/android/activity/BillPayEditVerifyActivity;

    invoke-direct {v3, v0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 201
    const-string v4, "transaction_object"

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/h;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/h;->d()Lcom/chase/sig/android/domain/Transaction;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 204
    const-string v4, "request_flags"

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/h;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/h;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/chase/sig/android/service/movemoney/RequestFlags;->d:Lcom/chase/sig/android/service/movemoney/RequestFlags;

    :goto_1
    invoke-virtual {v3, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 207
    if-eqz v2, :cond_4

    .line 208
    const-string v2, "queued_errors"

    move-object v0, v1

    check-cast v0, Ljava/io/Serializable;

    invoke-virtual {v3, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 214
    :goto_2
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/h;

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/activity/h;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 204
    :cond_3
    sget-object v0, Lcom/chase/sig/android/service/movemoney/RequestFlags;->c:Lcom/chase/sig/android/service/movemoney/RequestFlags;

    goto :goto_1

    .line 210
    :cond_4
    const-string v1, "queued_errors"

    invoke-virtual {p1}, Lcom/chase/sig/android/service/movemoney/ServiceResponse;->g()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/io/Serializable;

    invoke-virtual {v3, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    goto :goto_2
.end method

.method protected final bridge synthetic a(Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 150
    check-cast p1, Lcom/chase/sig/android/service/movemoney/ServiceResponse;

    invoke-virtual {p0, p1}, Lcom/chase/sig/android/activity/BillPayEditActivity$b;->a(Lcom/chase/sig/android/service/movemoney/ServiceResponse;)V

    return-void
.end method

.method protected final b()I
    .locals 1

    .prologue
    .line 160
    const/4 v0, 0x0

    return v0
.end method

.method protected final b(Lcom/chase/sig/android/service/movemoney/ServiceResponse;)V
    .locals 3
    .parameter

    .prologue
    .line 165
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayEditActivity$b;->a:Lcom/chase/sig/android/domain/Transaction;

    check-cast v0, Lcom/chase/sig/android/domain/BillPayTransaction;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/movemoney/ServiceResponse;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/BillPayTransaction;->c(Ljava/lang/String;)V

    .line 166
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayEditActivity$b;->a:Lcom/chase/sig/android/domain/Transaction;

    check-cast v0, Lcom/chase/sig/android/domain/BillPayTransaction;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/movemoney/ServiceResponse;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/BillPayTransaction;->m(Ljava/lang/String;)V

    .line 167
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/h;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/h;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "transaction_object"

    iget-object v2, p0, Lcom/chase/sig/android/activity/BillPayEditActivity$b;->a:Lcom/chase/sig/android/domain/Transaction;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 170
    return-void
.end method
