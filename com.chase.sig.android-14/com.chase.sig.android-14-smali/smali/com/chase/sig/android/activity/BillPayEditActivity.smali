.class public Lcom/chase/sig/android/activity/BillPayEditActivity;
.super Lcom/chase/sig/android/activity/h;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/BillPayEditActivity$b;,
        Lcom/chase/sig/android/activity/BillPayEditActivity$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/activity/h",
        "<",
        "Lcom/chase/sig/android/domain/BillPayTransaction;",
        ">;"
    }
.end annotation


# instance fields
.field private b:Ljava/util/Date;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/chase/sig/android/activity/h;-><init>()V

    .line 42
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayEditActivity;->b:Ljava/util/Date;

    .line 150
    return-void
.end method

.method private D()Z
    .locals 2

    .prologue
    .line 224
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayEditActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v0

    const-string v1, "REMAINING"

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/detail/DetailView;->e(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/r;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/r;->q()Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/BillPayEditActivity;Ljava/util/Date;)Ljava/util/Date;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 40
    iput-object p1, p0, Lcom/chase/sig/android/activity/BillPayEditActivity;->b:Ljava/util/Date;

    return-object p1
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 9
    .parameter

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 46
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/h;->a(Landroid/os/Bundle;)V

    .line 47
    const v0, 0x7f07017d

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayEditActivity;->setTitle(I)V

    .line 49
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayEditActivity;->d()Lcom/chase/sig/android/domain/Transaction;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/BillPayTransaction;

    .line 51
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayEditActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v3

    const/4 v1, 0x7

    new-array v4, v1, [Lcom/chase/sig/android/view/detail/a;

    new-instance v1, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v2, "Pay To "

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayTransaction;->d()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v2, v5}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v1, v4, v7

    new-instance v1, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v2, "Pay From "

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayTransaction;->m()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v2, v5}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v1, v4, v8

    const/4 v1, 0x2

    new-instance v2, Lcom/chase/sig/android/view/detail/c;

    const-string v5, "Amount $ "

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayTransaction;->e()Lcom/chase/sig/android/util/Dollar;

    move-result-object v6

    invoke-direct {v2, v5, v6}, Lcom/chase/sig/android/view/detail/c;-><init>(Ljava/lang/String;Lcom/chase/sig/android/util/Dollar;)V

    const-string v5, "AMOUNT"

    iput-object v5, v2, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    aput-object v2, v4, v1

    const/4 v2, 0x3

    new-instance v1, Lcom/chase/sig/android/view/detail/d;

    const-string v5, "Deliver By "

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayTransaction;->q()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/chase/sig/android/util/s;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v1, v5, v6}, Lcom/chase/sig/android/view/detail/d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "DATE"

    iput-object v5, v1, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v1, Lcom/chase/sig/android/view/detail/d;

    invoke-virtual {p0, v7}, Lcom/chase/sig/android/activity/BillPayEditActivity;->i(I)Landroid/view/View$OnClickListener;

    move-result-object v5

    iput-object v5, v1, Lcom/chase/sig/android/view/detail/d;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v1}, Lcom/chase/sig/android/view/detail/d;->q()Lcom/chase/sig/android/view/detail/d;

    move-result-object v1

    aput-object v1, v4, v2

    const/4 v2, 0x4

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayEditActivity;->a(Lcom/chase/sig/android/domain/Transaction;)Lcom/chase/sig/android/view/detail/o;

    move-result-object v1

    const-string v5, "FREQUENCY"

    iput-object v5, v1, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v1, Lcom/chase/sig/android/view/detail/q;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayEditActivity;->b()Z

    move-result v5

    iput-boolean v5, v1, Lcom/chase/sig/android/view/detail/a;->j:Z

    aput-object v1, v4, v2

    const/4 v2, 0x5

    new-instance v1, Lcom/chase/sig/android/view/detail/r;

    const-string v5, "Remaining Payments"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayTransaction;->x()Lcom/chase/sig/android/view/f;

    move-result-object v6

    invoke-direct {v1, v5, v6}, Lcom/chase/sig/android/view/detail/r;-><init>(Ljava/lang/String;Lcom/chase/sig/android/view/f;)V

    const-string v5, "REMAINING"

    iput-object v5, v1, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v1, Lcom/chase/sig/android/view/detail/j;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayEditActivity;->b()Z

    move-result v5

    iput-boolean v5, v1, Lcom/chase/sig/android/view/detail/a;->j:Z

    aput-object v1, v4, v2

    const/4 v5, 0x6

    new-instance v1, Lcom/chase/sig/android/view/detail/l;

    const-string v6, "Memo "

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayTransaction;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayTransaction;->o()Ljava/lang/String;

    move-result-object v2

    :goto_0
    invoke-direct {v1, v6, v2}, Lcom/chase/sig/android/view/detail/l;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "MEMO"

    iput-object v2, v1, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v1, Lcom/chase/sig/android/view/detail/l;

    const/16 v2, 0x78

    iput v2, v1, Lcom/chase/sig/android/view/detail/l;->a:I

    const-string v2, "Optional"

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/view/detail/l;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v1

    aput-object v1, v4, v5

    invoke-virtual {v3, v4}, Lcom/chase/sig/android/view/detail/DetailView;->setRows([Lcom/chase/sig/android/view/detail/a;)V

    .line 64
    const-string v1, "earliest_payment_date"

    invoke-static {p1, v1}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 65
    const-string v0, "earliest_payment_date"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->t(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayEditActivity;->b:Ljava/util/Date;

    .line 70
    :goto_1
    return-void

    .line 51
    :cond_0
    const-string v2, ""

    goto :goto_0

    .line 68
    :cond_1
    const-class v1, Lcom/chase/sig/android/activity/BillPayEditActivity$a;

    new-array v2, v8, [Lcom/chase/sig/android/domain/BillPayTransaction;

    aput-object v0, v2, v7

    invoke-virtual {p0, v1, v2}, Lcom/chase/sig/android/activity/BillPayEditActivity;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public final synthetic f()Lcom/chase/sig/android/domain/Transaction;
    .locals 3

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayEditActivity;->d()Lcom/chase/sig/android/domain/Transaction;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/BillPayTransaction;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayEditActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v1

    const-string v2, "AMOUNT"

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/view/detail/DetailView;->c(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/util/Dollar;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/BillPayTransaction;->a(Lcom/chase/sig/android/util/Dollar;)V

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayEditActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v1

    const-string v2, "DATE"

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/view/detail/DetailView;->c(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->t(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->b(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/BillPayTransaction;->n(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayEditActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v1

    const-string v2, "MEMO"

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/view/detail/DetailView;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayEditActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v1

    const-string v2, "MEMO"

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/view/detail/DetailView;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/BillPayTransaction;->l(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayEditActivity;->b()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/chase/sig/android/activity/BillPayEditActivity;->D()Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "true"

    :goto_1
    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/BillPayTransaction;->s(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayEditActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v1

    const-string v2, "REMAINING"

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/view/detail/DetailView;->e(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/view/detail/r;

    invoke-virtual {v1}, Lcom/chase/sig/android/view/detail/r;->r()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/BillPayTransaction;->r(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayEditActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v1

    const-string v2, "FREQUENCY"

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/view/detail/DetailView;->e(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/view/detail/o;

    invoke-virtual {v1}, Lcom/chase/sig/android/view/detail/o;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/BillPayTransaction;->g(Ljava/lang/String;)V

    :cond_0
    return-object v0

    :cond_1
    const-string v1, ""

    goto :goto_0

    :cond_2
    const-string v1, "false"

    goto :goto_1
.end method

.method protected final g()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 94
    const/4 v1, 0x1

    .line 95
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayEditActivity;->y()V

    .line 97
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayEditActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v0

    const-string v3, "AMOUNT"

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/view/detail/DetailView;->e(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/c;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/c;->p()Lcom/chase/sig/android/view/AmountView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/view/AmountView;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 98
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayEditActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v0

    const-string v1, "AMOUNT"

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/detail/DetailView;->a(Ljava/lang/String;)V

    move v1, v2

    .line 102
    :cond_0
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayEditActivity;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 103
    invoke-direct {p0}, Lcom/chase/sig/android/activity/BillPayEditActivity;->D()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayEditActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v0

    const-string v3, "REMAINING"

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/view/detail/DetailView;->e(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/r;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/r;->n()Z

    move-result v0

    if-nez v0, :cond_1

    .line 104
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayEditActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v0

    const-string v1, "REMAINING"

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/detail/DetailView;->a(Ljava/lang/String;)V

    move v1, v2

    .line 108
    :cond_1
    return v1
.end method

.method protected final h()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/chase/sig/android/activity/BillPayEditActivity$b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 121
    const-class v0, Lcom/chase/sig/android/activity/BillPayEditActivity$b;

    return-object v0
.end method

.method protected final i()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/chase/sig/android/activity/eb;",
            ">;"
        }
    .end annotation

    .prologue
    .line 125
    const-class v0, Lcom/chase/sig/android/activity/BillPayEditVerifyActivity;

    return-object v0
.end method

.method protected final j()Ljava/util/Date;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayEditActivity;->b:Ljava/util/Date;

    return-object v0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 112
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/h;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 113
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayEditActivity;->b:Ljava/util/Date;

    if-eqz v0, :cond_0

    .line 114
    const-string v0, "earliest_payment_date"

    iget-object v1, p0, Lcom/chase/sig/android/activity/BillPayEditActivity;->b:Ljava/util/Date;

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->a(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    :cond_0
    return-void
.end method
