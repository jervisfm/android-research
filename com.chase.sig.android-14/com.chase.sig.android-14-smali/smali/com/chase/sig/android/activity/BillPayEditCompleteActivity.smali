.class public Lcom/chase/sig/android/activity/BillPayEditCompleteActivity;
.super Lcom/chase/sig/android/activity/m;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/BillPayEditCompleteActivity$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/activity/m",
        "<",
        "Lcom/chase/sig/android/domain/BillPayTransaction;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/chase/sig/android/activity/m;-><init>()V

    .line 52
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1
    .parameter

    .prologue
    .line 18
    const v0, 0x7f03005e

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayEditCompleteActivity;->b(I)V

    .line 19
    const v0, 0x7f070074

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayEditCompleteActivity;->setTitle(I)V

    .line 21
    invoke-static {}, Lcom/chase/sig/android/c;->a()V

    .line 22
    const-class v0, Lcom/chase/sig/android/activity/BillPayEditCompleteActivity$a;

    invoke-virtual {p0, p1, v0}, Lcom/chase/sig/android/activity/BillPayEditCompleteActivity;->a(Landroid/os/Bundle;Ljava/lang/Class;)V

    .line 23
    return-void
.end method

.method protected final a(Z)V
    .locals 8
    .parameter

    .prologue
    .line 27
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayEditCompleteActivity;->a:Lcom/chase/sig/android/domain/Transaction;

    check-cast v0, Lcom/chase/sig/android/domain/BillPayTransaction;

    .line 29
    const v1, 0x7f090153

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/BillPayEditCompleteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/view/detail/DetailView;

    .line 30
    const/16 v2, 0xa

    new-array v2, v2, [Lcom/chase/sig/android/view/detail/a;

    const/4 v3, 0x0

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Transaction Number"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayTransaction;->n()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    const/4 v3, 0x1

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Pay To"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayTransaction;->h()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayTransaction;->A()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    const/4 v3, 0x2

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Pay From"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayTransaction;->m()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    const/4 v3, 0x3

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Send On"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayTransaction;->f()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/chase/sig/android/util/s;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    const/4 v3, 0x4

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Deliver By"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayTransaction;->q()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/chase/sig/android/util/s;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    const/4 v3, 0x5

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Amount"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayTransaction;->e()Lcom/chase/sig/android/util/Dollar;

    move-result-object v6

    invoke-virtual {v6}, Lcom/chase/sig/android/util/Dollar;->h()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    const/4 v3, 0x6

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Memo"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayTransaction;->o()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    const/4 v3, 0x7

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Frequency"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayTransaction;->i()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/chase/sig/android/activity/BillPayEditCompleteActivity;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayEditCompleteActivity;->c()Z

    move-result v5

    iput-boolean v5, v4, Lcom/chase/sig/android/view/detail/a;->j:Z

    aput-object v4, v2, v3

    const/16 v3, 0x8

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Remaining Payments"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayTransaction;->w()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayEditCompleteActivity;->c()Z

    move-result v5

    iput-boolean v5, v4, Lcom/chase/sig/android/view/detail/a;->j:Z

    aput-object v4, v2, v3

    const/16 v3, 0x9

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Status"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayTransaction;->t()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v5, v0}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/view/detail/DetailView;->setRows([Lcom/chase/sig/android/view/detail/a;)V

    .line 41
    const v0, 0x7f070183

    const-class v1, Lcom/chase/sig/android/activity/BillPayHistoryActivity;

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/BillPayEditCompleteActivity;->a(ILjava/lang/Class;)V

    .line 43
    const v0, 0x7f070184

    const-class v1, Lcom/chase/sig/android/activity/BillPayHomeActivity;

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/BillPayEditCompleteActivity;->b(ILjava/lang/Class;)V

    .line 44
    invoke-virtual {p0, p1}, Lcom/chase/sig/android/activity/BillPayEditCompleteActivity;->b(Z)V

    .line 45
    return-void
.end method

.method protected final b()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/chase/sig/android/activity/eb;",
            ">;"
        }
    .end annotation

    .prologue
    .line 49
    const-class v0, Lcom/chase/sig/android/activity/BillPayHomeActivity;

    return-object v0
.end method
