.class public Lcom/chase/sig/android/activity/BillPayEditVerifyActivity;
.super Lcom/chase/sig/android/activity/k;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/BillPayEditVerifyActivity$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/activity/k",
        "<",
        "Lcom/chase/sig/android/domain/BillPayTransaction;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/chase/sig/android/activity/k;-><init>()V

    .line 60
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 19
    const v0, 0x7f0300c7

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayEditVerifyActivity;->b(I)V

    .line 20
    const v0, 0x7f070180

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayEditVerifyActivity;->setTitle(I)V

    .line 21
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayEditVerifyActivity;->h()V

    .line 22
    const v0, 0x7f0902d1

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayEditVerifyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const v1, 0x7f070182

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 24
    const v0, 0x7f0902d2

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayEditVerifyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const v1, 0x7f070181

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 26
    return-void
.end method

.method protected final a(Z)V
    .locals 6
    .parameter

    .prologue
    .line 31
    const v0, 0x7f0902d0

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayEditVerifyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailView;

    .line 32
    const/16 v1, 0x8

    new-array v2, v1, [Lcom/chase/sig/android/view/detail/a;

    const/4 v3, 0x0

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Pay To"

    iget-object v1, p0, Lcom/chase/sig/android/activity/BillPayEditVerifyActivity;->a:Lcom/chase/sig/android/domain/Transaction;

    check-cast v1, Lcom/chase/sig/android/domain/BillPayTransaction;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/BillPayTransaction;->d()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v5, v1}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    const/4 v3, 0x1

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Pay From"

    iget-object v1, p0, Lcom/chase/sig/android/activity/BillPayEditVerifyActivity;->a:Lcom/chase/sig/android/domain/Transaction;

    check-cast v1, Lcom/chase/sig/android/domain/BillPayTransaction;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/BillPayTransaction;->m()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v5, v1}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    const/4 v3, 0x2

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Send On"

    iget-object v1, p0, Lcom/chase/sig/android/activity/BillPayEditVerifyActivity;->a:Lcom/chase/sig/android/domain/Transaction;

    check-cast v1, Lcom/chase/sig/android/domain/BillPayTransaction;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/BillPayTransaction;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v5, v1}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    const/4 v3, 0x3

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Deliver By"

    iget-object v1, p0, Lcom/chase/sig/android/activity/BillPayEditVerifyActivity;->a:Lcom/chase/sig/android/domain/Transaction;

    check-cast v1, Lcom/chase/sig/android/domain/BillPayTransaction;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/BillPayTransaction;->q()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v5, v1}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    const/4 v3, 0x4

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Amount"

    iget-object v1, p0, Lcom/chase/sig/android/activity/BillPayEditVerifyActivity;->a:Lcom/chase/sig/android/domain/Transaction;

    check-cast v1, Lcom/chase/sig/android/domain/BillPayTransaction;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/BillPayTransaction;->e()Lcom/chase/sig/android/util/Dollar;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/util/Dollar;->h()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v5, v1}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    const/4 v3, 0x5

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Frequency"

    iget-object v1, p0, Lcom/chase/sig/android/activity/BillPayEditVerifyActivity;->a:Lcom/chase/sig/android/domain/Transaction;

    check-cast v1, Lcom/chase/sig/android/domain/BillPayTransaction;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/BillPayTransaction;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/BillPayEditVerifyActivity;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v5, v1}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayEditVerifyActivity;->b()Z

    move-result v1

    iput-boolean v1, v4, Lcom/chase/sig/android/view/detail/a;->j:Z

    aput-object v4, v2, v3

    const/4 v3, 0x6

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Remaining Payments"

    iget-object v1, p0, Lcom/chase/sig/android/activity/BillPayEditVerifyActivity;->a:Lcom/chase/sig/android/domain/Transaction;

    check-cast v1, Lcom/chase/sig/android/domain/BillPayTransaction;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/BillPayTransaction;->w()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v5, v1}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayEditVerifyActivity;->b()Z

    move-result v1

    iput-boolean v1, v4, Lcom/chase/sig/android/view/detail/a;->j:Z

    aput-object v4, v2, v3

    const/4 v3, 0x7

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Memo"

    iget-object v1, p0, Lcom/chase/sig/android/activity/BillPayEditVerifyActivity;->a:Lcom/chase/sig/android/domain/Transaction;

    check-cast v1, Lcom/chase/sig/android/domain/BillPayTransaction;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/BillPayTransaction;->o()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v5, v1}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/view/detail/DetailView;->setRows([Lcom/chase/sig/android/view/detail/a;)V

    .line 42
    const v0, 0x7f0902d2

    new-instance v1, Lcom/chase/sig/android/activity/aq;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/aq;-><init>(Lcom/chase/sig/android/activity/BillPayEditVerifyActivity;)V

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/BillPayEditVerifyActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 48
    const v0, 0x7f0902d1

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayEditVerifyActivity;->B()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/BillPayEditVerifyActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 49
    return-void
.end method

.method protected final c()Lcom/chase/sig/android/service/movemoney/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/chase/sig/android/service/movemoney/d",
            "<",
            "Lcom/chase/sig/android/domain/BillPayTransaction;",
            ">;"
        }
    .end annotation

    .prologue
    .line 53
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    invoke-static {}, Lcom/chase/sig/android/service/af;->a()Lcom/chase/sig/android/service/n;

    invoke-static {}, Lcom/chase/sig/android/service/n;->g()Lcom/chase/sig/android/service/billpay/b;

    move-result-object v0

    return-object v0
.end method

.method protected final d()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/chase/sig/android/activity/BillPayEditCompleteActivity;",
            ">;"
        }
    .end annotation

    .prologue
    .line 57
    const-class v0, Lcom/chase/sig/android/activity/BillPayEditCompleteActivity;

    return-object v0
.end method
