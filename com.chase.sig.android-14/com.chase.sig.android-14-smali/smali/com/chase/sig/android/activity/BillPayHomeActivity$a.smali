.class final Lcom/chase/sig/android/activity/BillPayHomeActivity$a;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/BillPayHomeActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/chase/sig/android/domain/Payee;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/BillPayHomeActivity;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/Payee;",
            ">;"
        }
    .end annotation
.end field

.field private final c:I


# direct methods
.method public constructor <init>(Lcom/chase/sig/android/activity/BillPayHomeActivity;Landroid/content/Context;Ljava/util/List;)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 306
    iput-object p1, p0, Lcom/chase/sig/android/activity/BillPayHomeActivity$a;->a:Lcom/chase/sig/android/activity/BillPayHomeActivity;

    .line 307
    const v0, 0x7f030018

    invoke-direct {p0, p2, v0, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 308
    iput-object p3, p0, Lcom/chase/sig/android/activity/BillPayHomeActivity$a;->b:Ljava/util/List;

    .line 309
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/chase/sig/android/activity/BillPayHomeActivity$a;->c:I

    .line 310
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/BillPayHomeActivity$a;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 301
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayHomeActivity$a;->b:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .parameter
    .parameter
    .parameter

    .prologue
    const v9, 0x7f060007

    const v8, 0x7f060005

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 314
    .line 315
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayHomeActivity$a;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/Payee;

    .line 317
    if-nez p2, :cond_0

    .line 318
    iget-object v1, p0, Lcom/chase/sig/android/activity/BillPayHomeActivity$a;->a:Lcom/chase/sig/android/activity/BillPayHomeActivity;

    invoke-virtual {v1}, Lcom/chase/sig/android/activity/BillPayHomeActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030018

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 319
    iget v1, p0, Lcom/chase/sig/android/activity/BillPayHomeActivity$a;->c:I

    if-ne p1, v1, :cond_0

    .line 320
    const v1, 0x7f090013

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 324
    :cond_0
    const v1, 0x7f09005d

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const v1, 0x7f090053

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Payee;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v2, 0x7f09005e

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Payee;->a()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    :pswitch_0
    const-string v4, ""

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    const-string v4, "Active"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Payee;->m()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v3, v7}, Landroid/view/View;->setFocusable(Z)V

    invoke-virtual {v3, v7}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    invoke-virtual {v3, v7}, Landroid/view/View;->setClickable(Z)V

    iget-object v4, p0, Lcom/chase/sig/android/activity/BillPayHomeActivity$a;->a:Lcom/chase/sig/android/activity/BillPayHomeActivity;

    invoke-virtual {v4}, Lcom/chase/sig/android/activity/BillPayHomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v1, p0, Lcom/chase/sig/android/activity/BillPayHomeActivity$a;->a:Lcom/chase/sig/android/activity/BillPayHomeActivity;

    invoke-virtual {v1}, Lcom/chase/sig/android/activity/BillPayHomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setTextColor(I)V

    new-instance v1, Lcom/chase/sig/android/activity/av;

    invoke-direct {v1, p0, v0}, Lcom/chase/sig/android/activity/av;-><init>(Lcom/chase/sig/android/activity/BillPayHomeActivity$a;Lcom/chase/sig/android/domain/Payee;)V

    invoke-virtual {v3, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 325
    :goto_1
    const v1, 0x7f09005a

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const-string v2, "Active"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Payee;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/chase/sig/android/activity/BillPayHomeActivity$a;->a:Lcom/chase/sig/android/activity/BillPayHomeActivity;

    invoke-static {v2}, Lcom/chase/sig/android/activity/BillPayHomeActivity;->b(Lcom/chase/sig/android/activity/BillPayHomeActivity;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/chase/sig/android/activity/BillPayHomeActivity$a;->a:Lcom/chase/sig/android/activity/BillPayHomeActivity;

    invoke-static {v2}, Lcom/chase/sig/android/activity/BillPayHomeActivity;->c(Lcom/chase/sig/android/activity/BillPayHomeActivity;)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_1
    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    const v2, 0x7f09005c

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    new-instance v3, Lcom/chase/sig/android/activity/aw;

    invoke-direct {v3, p0, v0}, Lcom/chase/sig/android/activity/aw;-><init>(Lcom/chase/sig/android/activity/BillPayHomeActivity$a;Lcom/chase/sig/android/domain/Payee;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-static {v1, v2}, Lcom/chase/sig/android/view/an;->a(Landroid/view/View;Landroid/view/View;)V

    .line 326
    :goto_2
    return-object p2

    .line 324
    :pswitch_1
    const v4, 0x7f070165

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :pswitch_2
    const v4, 0x7f070166

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :pswitch_3
    const v4, 0x7f070167

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_0

    :pswitch_4
    const v4, 0x7f070168

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_0

    :cond_2
    iget-object v4, p0, Lcom/chase/sig/android/activity/BillPayHomeActivity$a;->a:Lcom/chase/sig/android/activity/BillPayHomeActivity;

    invoke-virtual {v4}, Lcom/chase/sig/android/activity/BillPayHomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v1, p0, Lcom/chase/sig/android/activity/BillPayHomeActivity$a;->a:Lcom/chase/sig/android/activity/BillPayHomeActivity;

    invoke-virtual {v1}, Lcom/chase/sig/android/activity/BillPayHomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v1, p0, Lcom/chase/sig/android/activity/BillPayHomeActivity$a;->a:Lcom/chase/sig/android/activity/BillPayHomeActivity;

    invoke-virtual {v1}, Lcom/chase/sig/android/activity/BillPayHomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f07008b

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v3, v6}, Landroid/view/View;->setFocusable(Z)V

    invoke-virtual {v3, v6}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    invoke-virtual {v3, v6}, Landroid/view/View;->setClickable(Z)V

    goto/16 :goto_1

    .line 325
    :cond_3
    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 324
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method
