.class public Lcom/chase/sig/android/activity/BillPayHomeActivity$b;
.super Lcom/chase/sig/android/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/BillPayHomeActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/d",
        "<",
        "Lcom/chase/sig/android/activity/BillPayHomeActivity;",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        "Lcom/chase/sig/android/service/billpay/BillPayGetPayeesResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 280
    invoke-direct {p0}, Lcom/chase/sig/android/d;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 280
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/BillPayHomeActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/BillPayHomeActivity;->A()Lcom/chase/sig/android/service/n;

    invoke-static {}, Lcom/chase/sig/android/service/n;->g()Lcom/chase/sig/android/service/billpay/b;

    invoke-static {}, Lcom/chase/sig/android/service/billpay/b;->a()Lcom/chase/sig/android/service/billpay/BillPayGetPayeesResponse;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 2
    .parameter

    .prologue
    .line 280
    check-cast p1, Lcom/chase/sig/android/service/billpay/BillPayGetPayeesResponse;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/billpay/BillPayGetPayeesResponse;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/BillPayHomeActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/billpay/BillPayGetPayeesResponse;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/BillPayHomeActivity;->b(Ljava/util/List;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/BillPayHomeActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/billpay/BillPayGetPayeesResponse;->a()Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/chase/sig/android/activity/BillPayHomeActivity;->a(Lcom/chase/sig/android/activity/BillPayHomeActivity;Ljava/util/List;)V

    goto :goto_0
.end method
