.class public Lcom/chase/sig/android/activity/BillPayHomeActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$e;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/BillPayHomeActivity$a;,
        Lcom/chase/sig/android/activity/BillPayHomeActivity$b;
    }
.end annotation


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/Payee;",
            ">;"
        }
    .end annotation
.end field

.field private b:Landroid/widget/ListView;

.field private c:Ljava/lang/String;

.field private d:Z

.field private k:Z

.field private l:Z

.field private m:Lcom/chase/sig/android/domain/g;

.field private n:Landroid/view/View;

.field private o:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    .line 424
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/BillPayHomeActivity;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 31
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayHomeActivity;->c:Ljava/lang/String;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Z)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 418
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/chase/sig/android/activity/BillPayHomeActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 419
    const/high16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 420
    const-string v1, "refresh_payees_flag"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 421
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 422
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/BillPayHomeActivity;Landroid/content/DialogInterface;Landroid/os/Bundle;)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 31
    const-string v0, "FORWARDING_INTENT_KEY"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayHomeActivity;->startActivity(Landroid/content/Intent;)V

    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/BillPayHomeActivity;Ljava/lang/String;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 31
    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/BillPayHomeActivity;->getPreferences(I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/BillPayHomeActivity;Ljava/util/List;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/chase/sig/android/activity/BillPayHomeActivity;->a(Ljava/util/List;)V

    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/Payee;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 210
    iput-object p1, p0, Lcom/chase/sig/android/activity/BillPayHomeActivity;->a:Ljava/util/List;

    .line 212
    if-eqz p1, :cond_0

    if-eqz p1, :cond_2

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 213
    :cond_0
    iget-boolean v0, p0, Lcom/chase/sig/android/activity/BillPayHomeActivity;->d:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/chase/sig/android/activity/BillPayHomeActivity;->k:Z

    if-nez v0, :cond_1

    .line 214
    const v0, 0x7f070282

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayHomeActivity;->f(I)V

    .line 223
    :goto_0
    return-void

    .line 216
    :cond_1
    invoke-direct {p0}, Lcom/chase/sig/android/activity/BillPayHomeActivity;->d()V

    goto :goto_0

    .line 219
    :cond_2
    invoke-direct {p0}, Lcom/chase/sig/android/activity/BillPayHomeActivity;->d()V

    .line 220
    const v0, 0x7f090047

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayHomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayHomeActivity;->b:Landroid/widget/ListView;

    new-instance v1, Lcom/chase/sig/android/view/af;

    new-instance v2, Lcom/chase/sig/android/activity/BillPayHomeActivity$a;

    iget-object v3, p0, Lcom/chase/sig/android/activity/BillPayHomeActivity;->a:Ljava/util/List;

    invoke-direct {v2, p0, p0, v3}, Lcom/chase/sig/android/activity/BillPayHomeActivity$a;-><init>(Lcom/chase/sig/android/activity/BillPayHomeActivity;Landroid/content/Context;Ljava/util/List;)V

    invoke-direct {v1, v2}, Lcom/chase/sig/android/view/af;-><init>(Landroid/widget/ListAdapter;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayHomeActivity;->b:Landroid/widget/ListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/BillPayHomeActivity;)Z
    .locals 1
    .parameter

    .prologue
    .line 31
    iget-boolean v0, p0, Lcom/chase/sig/android/activity/BillPayHomeActivity;->d:Z

    return v0
.end method

.method static synthetic c(Lcom/chase/sig/android/activity/BillPayHomeActivity;)Z
    .locals 1
    .parameter

    .prologue
    .line 31
    iget-boolean v0, p0, Lcom/chase/sig/android/activity/BillPayHomeActivity;->k:Z

    return v0
.end method

.method private d()V
    .locals 11

    .prologue
    const v10, 0x7f070164

    const v9, 0x7f0200d3

    const/4 v8, 0x0

    const/16 v7, 0x8

    .line 234
    const v0, 0x7f090041

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayHomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 235
    const v0, 0x7f090042

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayHomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 236
    const v0, 0x7f090044

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayHomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 237
    const v0, 0x7f090043

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayHomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 238
    const v0, 0x7f090045

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayHomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 239
    const v0, 0x7f090046

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayHomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 241
    iget-boolean v6, p0, Lcom/chase/sig/android/activity/BillPayHomeActivity;->d:Z

    if-nez v6, :cond_0

    iget-boolean v6, p0, Lcom/chase/sig/android/activity/BillPayHomeActivity;->k:Z

    if-eqz v6, :cond_1

    .line 242
    :cond_0
    invoke-virtual {v1, v8}, Landroid/view/View;->setVisibility(I)V

    .line 243
    invoke-virtual {v2, v8}, Landroid/view/View;->setVisibility(I)V

    .line 246
    :cond_1
    iget-boolean v2, p0, Lcom/chase/sig/android/activity/BillPayHomeActivity;->k:Z

    if-nez v2, :cond_2

    .line 247
    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 248
    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 249
    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    .line 250
    iget-object v2, p0, Lcom/chase/sig/android/activity/BillPayHomeActivity;->n:Landroid/view/View;

    invoke-virtual {v2, v9}, Landroid/view/View;->setBackgroundResource(I)V

    .line 253
    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setText(I)V

    .line 256
    :cond_2
    iget-boolean v2, p0, Lcom/chase/sig/android/activity/BillPayHomeActivity;->d:Z

    if-nez v2, :cond_3

    .line 257
    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 258
    invoke-virtual {v5, v7}, Landroid/view/View;->setVisibility(I)V

    .line 259
    iget-object v2, p0, Lcom/chase/sig/android/activity/BillPayHomeActivity;->o:Landroid/view/View;

    invoke-virtual {v2, v9}, Landroid/view/View;->setBackgroundResource(I)V

    .line 263
    :cond_3
    iget-boolean v2, p0, Lcom/chase/sig/android/activity/BillPayHomeActivity;->d:Z

    if-eqz v2, :cond_4

    iget-boolean v2, p0, Lcom/chase/sig/android/activity/BillPayHomeActivity;->k:Z

    if-nez v2, :cond_4

    .line 264
    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    .line 265
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayHomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 268
    :cond_4
    iget-boolean v0, p0, Lcom/chase/sig/android/activity/BillPayHomeActivity;->l:Z

    if-eqz v0, :cond_5

    .line 269
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayHomeActivity;->o:Landroid/view/View;

    const v2, 0x7f0200d2

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 271
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayHomeActivity;->n:Landroid/view/View;

    const v2, 0x7f0200d1

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 273
    invoke-virtual {v3, v8}, Landroid/view/View;->setVisibility(I)V

    .line 274
    invoke-virtual {v1, v8}, Landroid/view/View;->setVisibility(I)V

    .line 278
    :goto_0
    return-void

    .line 276
    :cond_5
    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 4
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 58
    const v0, 0x7f070160

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayHomeActivity;->setTitle(I)V

    .line 59
    const v0, 0x7f030010

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayHomeActivity;->b(I)V

    .line 61
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->d:Lcom/chase/sig/android/service/SplashResponse;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->d:Lcom/chase/sig/android/service/SplashResponse;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/SplashResponse;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 62
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->d:Lcom/chase/sig/android/service/SplashResponse;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/SplashResponse;->n()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayHomeActivity;->e(Ljava/lang/String;)V

    .line 151
    :cond_0
    :goto_0
    return-void

    .line 65
    :cond_1
    const-string v0, ""

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayHomeActivity;->c:Ljava/lang/String;

    .line 66
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayHomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 68
    if-eqz v0, :cond_2

    .line 69
    const-string v3, "selectedAccountId"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayHomeActivity;->c:Ljava/lang/String;

    .line 73
    :cond_2
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/ChaseApplication;

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->b:Lcom/chase/sig/android/domain/g;

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayHomeActivity;->m:Lcom/chase/sig/android/domain/g;

    .line 74
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayHomeActivity;->m:Lcom/chase/sig/android/domain/g;

    invoke-interface {v0}, Lcom/chase/sig/android/domain/g;->k()Z

    move-result v0

    if-nez v0, :cond_3

    .line 76
    const v0, 0x7f070169

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayHomeActivity;->f(I)V

    goto :goto_0

    .line 80
    :cond_3
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayHomeActivity;->m:Lcom/chase/sig/android/domain/g;

    invoke-interface {v0}, Lcom/chase/sig/android/domain/g;->l()Z

    move-result v0

    iput-boolean v0, p0, Lcom/chase/sig/android/activity/BillPayHomeActivity;->d:Z

    .line 82
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayHomeActivity;->x()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayHomeActivity;->m:Lcom/chase/sig/android/domain/g;

    invoke-interface {v0}, Lcom/chase/sig/android/domain/g;->m()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->d:Lcom/chase/sig/android/service/SplashResponse;

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->d:Lcom/chase/sig/android/service/SplashResponse;

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->d:Lcom/chase/sig/android/service/SplashResponse;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/SplashResponse;->m()Z

    move-result v0

    if-nez v0, :cond_5

    :cond_4
    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/chase/sig/android/activity/BillPayHomeActivity;->k:Z

    .line 88
    iget-boolean v0, p0, Lcom/chase/sig/android/activity/BillPayHomeActivity;->d:Z

    if-eqz v0, :cond_6

    iget-boolean v0, p0, Lcom/chase/sig/android/activity/BillPayHomeActivity;->k:Z

    if-eqz v0, :cond_6

    :goto_2
    iput-boolean v1, p0, Lcom/chase/sig/android/activity/BillPayHomeActivity;->l:Z

    .line 90
    const v0, 0x7f090048

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayHomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayHomeActivity;->b:Landroid/widget/ListView;

    .line 92
    const v0, 0x7f090040

    const-class v1, Lcom/chase/sig/android/activity/BillPayHistoryActivity;

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/BillPayHomeActivity;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/a/f;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/BillPayHomeActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 95
    const v0, 0x7f090043

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayHomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayHomeActivity;->o:Landroid/view/View;

    .line 96
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayHomeActivity;->o:Landroid/view/View;

    new-instance v1, Lcom/chase/sig/android/activity/ar;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/ar;-><init>(Lcom/chase/sig/android/activity/BillPayHomeActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 127
    const v0, 0x7f090045

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayHomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayHomeActivity;->n:Landroid/view/View;

    .line 128
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayHomeActivity;->n:Landroid/view/View;

    new-instance v1, Lcom/chase/sig/android/activity/as;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/as;-><init>(Lcom/chase/sig/android/activity/BillPayHomeActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 141
    if-nez p1, :cond_7

    .line 142
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayHomeActivity;->e:Lcom/chase/sig/android/activity/mc;

    const-class v1, Lcom/chase/sig/android/activity/BillPayHomeActivity$b;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/mc;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/ae;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/activity/BillPayHomeActivity$b;

    .line 143
    invoke-virtual {v0}, Lcom/chase/sig/android/activity/BillPayHomeActivity$b;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v1

    sget-object v3, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-eq v1, v3, :cond_0

    .line 144
    new-array v1, v2, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/BillPayHomeActivity$b;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    :cond_5
    move v0, v2

    .line 82
    goto :goto_1

    :cond_6
    move v1, v2

    .line 88
    goto :goto_2

    .line 147
    :cond_7
    const-string v0, "payees"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/chase/sig/android/activity/BillPayHomeActivity;->a(Ljava/util/List;)V

    goto/16 :goto_0
.end method

.method protected onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 155
    packed-switch p1, :pswitch_data_0

    .line 171
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ai;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    .line 157
    :pswitch_0
    new-instance v0, Lcom/chase/sig/android/view/k$a;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/view/k$a;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0701a6

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/k$a;->a(I)Lcom/chase/sig/android/view/k$a;

    move-result-object v0

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/chase/sig/android/view/k$a;->i:Z

    const v1, 0x7f0701a3

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/k$a;->c(I)Lcom/chase/sig/android/view/k$a;

    move-result-object v0

    const-string v1, "Hide"

    new-instance v2, Lcom/chase/sig/android/activity/au;

    invoke-direct {v2, p0, p2}, Lcom/chase/sig/android/activity/au;-><init>(Lcom/chase/sig/android/activity/BillPayHomeActivity;Landroid/os/Bundle;)V

    invoke-virtual {v0, v1, v2}, Lcom/chase/sig/android/view/k$a;->a(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    move-result-object v0

    const-string v1, "Remind"

    new-instance v2, Lcom/chase/sig/android/activity/at;

    invoke-direct {v2, p0, p2}, Lcom/chase/sig/android/activity/at;-><init>(Lcom/chase/sig/android/activity/BillPayHomeActivity;Landroid/os/Bundle;)V

    invoke-virtual {v0, v1, v2}, Lcom/chase/sig/android/view/k$a;->b(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/k$a;->d(I)Lcom/chase/sig/android/view/k;

    move-result-object v0

    goto :goto_0

    .line 155
    :pswitch_data_0
    .packed-switch 0x2a
        :pswitch_0
    .end packed-switch
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 3
    .parameter

    .prologue
    .line 198
    invoke-virtual {p0, p1}, Lcom/chase/sig/android/activity/BillPayHomeActivity;->setIntent(Landroid/content/Intent;)V

    .line 199
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "refresh_payees_flag"

    invoke-static {v0, v1}, Lcom/chase/sig/android/util/e;->b(Landroid/os/Bundle;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 200
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayHomeActivity;->e:Lcom/chase/sig/android/activity/mc;

    const-class v1, Lcom/chase/sig/android/activity/BillPayHomeActivity$b;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/mc;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/ae;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/activity/BillPayHomeActivity$b;

    .line 202
    invoke-virtual {v0}, Lcom/chase/sig/android/activity/BillPayHomeActivity$b;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v1

    sget-object v2, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-eq v1, v2, :cond_0

    .line 203
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/BillPayHomeActivity$b;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 206
    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 191
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ai;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 193
    const-string v1, "payees"

    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayHomeActivity;->a:Ljava/util/List;

    check-cast v0, Ljava/io/Serializable;

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 194
    return-void
.end method
