.class public Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$e;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/BillPayPayeeAddActivity$a;
    }
.end annotation


# instance fields
.field private a:Lcom/chase/sig/android/view/detail/DetailView;

.field private b:Lcom/chase/sig/android/view/detail/i;

.field private c:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

.field private d:Landroid/widget/EditText;

.field private k:Landroid/widget/EditText;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    .line 320
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 235
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 236
    const-string v1, "payee_name"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 237
    const-string v1, "payee_zip_code"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 238
    const-string v1, "account_number"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 240
    return-object v0
.end method

.method private a(Landroid/widget/EditText;ZZI)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 310
    invoke-virtual {p1, p2}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 311
    invoke-virtual {p1, p2}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 312
    invoke-virtual {p1, p2}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    .line 313
    if-nez p2, :cond_0

    if-eqz p3, :cond_1

    .line 314
    :cond_0
    invoke-virtual {p0, p4}, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 318
    :goto_0
    return-void

    .line 316
    :cond_1
    const-string v0, ""

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;)V
    .locals 3
    .parameter

    .prologue
    .line 27
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->c:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->c()Lcom/chase/sig/android/domain/MerchantPayee;

    move-result-object v0

    iget-object v1, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    const-string v2, "PAYEE"

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/view/detail/DetailView;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/MerchantPayee;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->c:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    iget-object v1, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    const-string v2, "PAYEE_NICKNAME"

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/view/detail/DetailView;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->c:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    iget-object v1, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    const-string v2, "ACCOUNT_NUMBER"

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/view/detail/DetailView;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->c:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->c()Lcom/chase/sig/android/domain/MerchantPayee;

    move-result-object v0

    iget-object v1, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    const-string v2, "ADDRESS1"

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/view/detail/DetailView;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/MerchantPayee;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->c:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->c()Lcom/chase/sig/android/domain/MerchantPayee;

    move-result-object v0

    iget-object v1, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    const-string v2, "ADDRESS2"

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/view/detail/DetailView;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/MerchantPayee;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->c:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->c()Lcom/chase/sig/android/domain/MerchantPayee;

    move-result-object v0

    iget-object v1, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    const-string v2, "CITY"

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/view/detail/DetailView;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/MerchantPayee;->f(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->c:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->c()Lcom/chase/sig/android/domain/MerchantPayee;

    move-result-object v0

    iget-object v1, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    const-string v2, "PHONE_NUMBER"

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/view/detail/DetailView;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/MerchantPayee;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->c:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->c()Lcom/chase/sig/android/domain/MerchantPayee;

    move-result-object v0

    iget-object v1, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    const-string v2, "ZIP_CODE"

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/view/detail/DetailView;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/MerchantPayee;->g(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->c:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    iget-object v1, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    const-string v2, "BILLPAY_MESSAGE"

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/view/detail/DetailView;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->c:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->c:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->c()Lcom/chase/sig/android/domain/MerchantPayee;

    move-result-object v0

    iget-object v1, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    const-string v2, "STATE"

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/view/detail/DetailView;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/domain/State;->b(Ljava/lang/String;)Lcom/chase/sig/android/domain/State;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/MerchantPayee;->a(Lcom/chase/sig/android/domain/State;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    const-string v1, "STATE"

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/detail/DetailView;->e(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/q;

    iget-object v1, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->c:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->c()Lcom/chase/sig/android/domain/MerchantPayee;

    move-result-object v1

    invoke-static {}, Lcom/chase/sig/android/domain/State;->values()[Lcom/chase/sig/android/domain/State;

    move-result-object v2

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/q;->q()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/view/JPSpinner;->getSelectedItemPosition()I

    move-result v0

    aget-object v0, v2, v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/MerchantPayee;->a(Lcom/chase/sig/android/domain/State;)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;)Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;
    .locals 1
    .parameter

    .prologue
    .line 27
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->c:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    return-object v0
.end method

.method private b(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 170
    const-string v0, "errors"

    invoke-static {p1, v0}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 172
    const-string v0, "errors"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 174
    iget-object v1, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->b:Lcom/chase/sig/android/view/detail/i;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/view/detail/i;->a(Ljava/util/List;)V

    .line 176
    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->e()V

    return-void
.end method

.method private d()Z
    .locals 2

    .prologue
    .line 203
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    const-string v1, "ACCOUNT_NUMBER"

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/detail/DetailView;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->l(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    const-string v1, "BILLPAY_MESSAGE"

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/detail/DetailView;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->l(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 298
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->d:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v3

    .line 299
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->k:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v4

    .line 301
    if-nez v3, :cond_0

    if-nez v4, :cond_0

    move v0, v1

    .line 303
    :goto_0
    iget-object v5, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->k:Landroid/widget/EditText;

    if-nez v3, :cond_1

    move v3, v1

    :goto_1
    const v6, 0x7f070080

    invoke-direct {p0, v5, v3, v0, v6}, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->a(Landroid/widget/EditText;ZZI)V

    .line 304
    iget-object v3, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->d:Landroid/widget/EditText;

    if-nez v4, :cond_2

    :goto_2
    const v2, 0x7f070099

    invoke-direct {p0, v3, v1, v0, v2}, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->a(Landroid/widget/EditText;ZZI)V

    .line 305
    return-void

    :cond_0
    move v0, v2

    .line 301
    goto :goto_0

    :cond_1
    move v3, v2

    .line 303
    goto :goto_1

    :cond_2
    move v1, v2

    .line 304
    goto :goto_2
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 11
    .parameter

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v4, 0x0

    .line 44
    const v0, 0x7f07018b

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->setTitle(I)V

    .line 45
    const v0, 0x7f030011

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->b(I)V

    .line 47
    const v0, 0x7f090049

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    .line 48
    new-instance v0, Lcom/chase/sig/android/view/detail/i;

    iget-object v1, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    invoke-direct {v0, v1}, Lcom/chase/sig/android/view/detail/i;-><init>(Lcom/chase/sig/android/view/detail/DetailView;)V

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->b:Lcom/chase/sig/android/view/detail/i;

    .line 50
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 52
    const-string v0, "payee_info"

    invoke-static {v2, v0}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 54
    const-string v0, "payee_info"

    invoke-static {v2, v0, v4}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->c:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    .line 105
    :goto_0
    const v0, 0x7f090049

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    new-instance v0, Lcom/chase/sig/android/activity/bd;

    invoke-direct {v0}, Lcom/chase/sig/android/activity/bd;-><init>()V

    iget-object v1, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->c:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    const/16 v0, 0xa

    new-array v3, v0, [Lcom/chase/sig/android/view/detail/a;

    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->c:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->c()Lcom/chase/sig/android/domain/MerchantPayee;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/MerchantPayee;->b()Ljava/lang/String;

    move-result-object v4

    new-instance v0, Lcom/chase/sig/android/view/detail/DetailRow;

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v5

    invoke-virtual {v5}, Lcom/chase/sig/android/ChaseApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f070091

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v5, v4}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "PAYEE"

    iput-object v4, v0, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailRow;

    aput-object v0, v3, v7

    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->c:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/activity/bd;->h(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/l;

    move-result-object v0

    aput-object v0, v3, v8

    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->c:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/activity/bd;->g(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/l;

    move-result-object v0

    aput-object v0, v3, v9

    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->c:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->c()Lcom/chase/sig/android/domain/MerchantPayee;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/MerchantPayee;->d()Ljava/lang/String;

    move-result-object v4

    new-instance v0, Lcom/chase/sig/android/view/detail/DetailRow;

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v5

    invoke-virtual {v5}, Lcom/chase/sig/android/ChaseApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f070092

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v5, v4}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "ADDRESS1"

    iput-object v4, v0, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailRow;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/DetailRow;->c()Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailRow;

    aput-object v0, v3, v10

    const/4 v0, 0x4

    iget-object v4, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->c:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {v4}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->c()Lcom/chase/sig/android/domain/MerchantPayee;

    move-result-object v4

    invoke-virtual {v4}, Lcom/chase/sig/android/domain/MerchantPayee;->e()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/chase/sig/android/activity/bd;->e(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/l;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v4, 0x5

    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->c:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->c()Lcom/chase/sig/android/domain/MerchantPayee;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/MerchantPayee;->f()Ljava/lang/String;

    move-result-object v5

    new-instance v0, Lcom/chase/sig/android/view/detail/DetailRow;

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v6

    invoke-virtual {v6}, Lcom/chase/sig/android/ChaseApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f07008f

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v0, v6, v5}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "CITY"

    iput-object v5, v0, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailRow;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/DetailRow;->c()Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailRow;

    aput-object v0, v3, v4

    const/4 v4, 0x6

    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->c:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->c()Lcom/chase/sig/android/domain/MerchantPayee;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/MerchantPayee;->g()Lcom/chase/sig/android/domain/State;

    move-result-object v5

    new-instance v0, Lcom/chase/sig/android/view/detail/DetailRow;

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v6

    invoke-virtual {v6}, Lcom/chase/sig/android/ChaseApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f070094

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5}, Lcom/chase/sig/android/domain/State;->a()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v6, v5}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "STATE"

    iput-object v5, v0, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "state"

    iput-object v5, v0, Lcom/chase/sig/android/view/detail/a;->r:Ljava/lang/String;

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailRow;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/DetailRow;->c()Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailRow;

    aput-object v0, v3, v4

    const/4 v4, 0x7

    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->c:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->c()Lcom/chase/sig/android/domain/MerchantPayee;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/MerchantPayee;->h()Ljava/lang/String;

    move-result-object v5

    new-instance v0, Lcom/chase/sig/android/view/detail/DetailRow;

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v6

    invoke-virtual {v6}, Lcom/chase/sig/android/ChaseApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f07008c

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v0, v6, v5}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "ZIP_CODE"

    iput-object v5, v0, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "zipCode"

    iput-object v5, v0, Lcom/chase/sig/android/view/detail/a;->r:Ljava/lang/String;

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailRow;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/DetailRow;->c()Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailRow;

    aput-object v0, v3, v4

    const/16 v0, 0x8

    iget-object v4, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->c:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {v4}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->c()Lcom/chase/sig/android/domain/MerchantPayee;

    move-result-object v4

    invoke-virtual {v4}, Lcom/chase/sig/android/domain/MerchantPayee;->c()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/chase/sig/android/activity/bd;->b(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/l;

    move-result-object v4

    aput-object v4, v3, v0

    const/16 v0, 0x9

    iget-object v4, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->c:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {v4}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->g()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/chase/sig/android/activity/bd;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/l;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-virtual {v1, v3}, Lcom/chase/sig/android/view/detail/DetailView;->setRows([Lcom/chase/sig/android/view/detail/a;)V

    :goto_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    const v1, 0x7f09004b

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->a(Lcom/chase/sig/android/view/detail/DetailView;I)V

    .line 106
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    const-string v1, "BILLPAY_MESSAGE"

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/detail/DetailView;->e(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/l;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/l;->p()Landroid/widget/EditText;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->d:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    const-string v1, "ACCOUNT_NUMBER"

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/detail/DetailView;->e(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/l;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/l;->p()Landroid/widget/EditText;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->k:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->d:Landroid/widget/EditText;

    new-instance v1, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity$a;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity$a;-><init>(Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->k:Landroid/widget/EditText;

    new-instance v1, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity$a;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity$a;-><init>(Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    invoke-direct {p0}, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->e()V

    .line 107
    invoke-direct {p0, v2}, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->b(Landroid/os/Bundle;)V

    .line 109
    const v0, 0x7f09004a

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/chase/sig/android/activity/ax;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/ax;-><init>(Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 116
    const v0, 0x7f09004b

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/chase/sig/android/activity/ay;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/ay;-><init>(Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 141
    return-void

    .line 57
    :cond_0
    const-string v0, "image_capture_data"

    invoke-static {v2, v0}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 58
    const-string v0, "image_capture_data"

    invoke-static {v2, v0, v4}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/ImageCaptureData;

    .line 61
    new-instance v1, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-direct {v1}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;-><init>()V

    iput-object v1, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->c:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    .line 64
    new-instance v1, Lcom/chase/sig/android/domain/MerchantPayee;

    invoke-direct {v1}, Lcom/chase/sig/android/domain/MerchantPayee;-><init>()V

    .line 65
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/ImageCaptureData;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/chase/sig/android/domain/MerchantPayee;->b(Ljava/lang/String;)V

    .line 66
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/ImageCaptureData;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/chase/sig/android/domain/MerchantPayee;->d(Ljava/lang/String;)V

    .line 67
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/ImageCaptureData;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/chase/sig/android/domain/MerchantPayee;->e(Ljava/lang/String;)V

    .line 68
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/ImageCaptureData;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/chase/sig/android/domain/MerchantPayee;->f(Ljava/lang/String;)V

    .line 69
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/ImageCaptureData;->h()Lcom/chase/sig/android/domain/State;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/chase/sig/android/domain/MerchantPayee;->a(Lcom/chase/sig/android/domain/State;)V

    .line 70
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/ImageCaptureData;->i()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/chase/sig/android/domain/MerchantPayee;->g(Ljava/lang/String;)V

    .line 71
    iget-object v3, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->c:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {v3, v1}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->a(Lcom/chase/sig/android/domain/MerchantPayee;)V

    .line 73
    iget-object v1, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->c:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/ImageCaptureData;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->b(Ljava/lang/String;)V

    .line 74
    iget-object v3, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->c:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    const-string v1, "selectedAccountId"

    const-string v4, ""

    invoke-static {v2, v1, v4}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->a(Ljava/lang/String;)V

    .line 78
    iget-object v1, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->c:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/ImageCaptureData;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->f(Ljava/lang/String;)V

    .line 79
    iget-object v1, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->c:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/ImageCaptureData;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->g(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 83
    :cond_1
    new-instance v0, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-direct {v0}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;-><init>()V

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->c:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    .line 85
    iget-object v1, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->c:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    const-string v0, "merchant_payee_data"

    new-instance v3, Lcom/chase/sig/android/domain/MerchantPayee;

    invoke-direct {v3}, Lcom/chase/sig/android/domain/MerchantPayee;-><init>()V

    invoke-static {v2, v0, v3}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/MerchantPayee;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->a(Lcom/chase/sig/android/domain/MerchantPayee;)V

    .line 89
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->c:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->c()Lcom/chase/sig/android/domain/MerchantPayee;

    move-result-object v1

    const-string v0, "payee_name"

    invoke-static {v2, v0, v4}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/MerchantPayee;->b(Ljava/lang/String;)V

    .line 92
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->c:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->c()Lcom/chase/sig/android/domain/MerchantPayee;

    move-result-object v1

    const-string v0, "payee_zip_code"

    invoke-static {v2, v0, v4}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/MerchantPayee;->g(Ljava/lang/String;)V

    .line 95
    iget-object v1, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->c:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    const-string v0, "account_number"

    const-string v3, ""

    invoke-static {v2, v0, v3}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->b(Ljava/lang/String;)V

    .line 98
    iget-object v1, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->c:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    const-string v0, "selectedAccountId"

    const-string v3, ""

    invoke-static {v2, v0, v3}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->a(Ljava/lang/String;)V

    .line 101
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->c:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    const-string v1, "from_merchant_directory"

    invoke-static {v2, v1}, Lcom/chase/sig/android/util/e;->b(Landroid/os/Bundle;Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->a(Z)V

    goto/16 :goto_0

    .line 105
    :cond_2
    iget-object v1, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    const/16 v3, 0xa

    new-array v3, v3, [Lcom/chase/sig/android/view/detail/a;

    iget-object v4, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->c:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {v4}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->c()Lcom/chase/sig/android/domain/MerchantPayee;

    move-result-object v4

    invoke-virtual {v4}, Lcom/chase/sig/android/domain/MerchantPayee;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/chase/sig/android/activity/bd;->i(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/l;

    move-result-object v4

    aput-object v4, v3, v7

    iget-object v4, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->c:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {v4}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->f()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/chase/sig/android/activity/bd;->h(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/l;

    move-result-object v4

    aput-object v4, v3, v8

    iget-object v4, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->c:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {v4}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->d()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/chase/sig/android/activity/bd;->g(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/l;

    move-result-object v4

    aput-object v4, v3, v9

    iget-object v4, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->c:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {v4}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->c()Lcom/chase/sig/android/domain/MerchantPayee;

    move-result-object v4

    invoke-virtual {v4}, Lcom/chase/sig/android/domain/MerchantPayee;->d()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/chase/sig/android/activity/bd;->f(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/l;

    move-result-object v4

    aput-object v4, v3, v10

    const/4 v4, 0x4

    iget-object v5, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->c:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {v5}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->c()Lcom/chase/sig/android/domain/MerchantPayee;

    move-result-object v5

    invoke-virtual {v5}, Lcom/chase/sig/android/domain/MerchantPayee;->e()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/chase/sig/android/activity/bd;->e(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/l;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x5

    iget-object v5, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->c:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {v5}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->c()Lcom/chase/sig/android/domain/MerchantPayee;

    move-result-object v5

    invoke-virtual {v5}, Lcom/chase/sig/android/domain/MerchantPayee;->f()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/chase/sig/android/activity/bd;->d(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/l;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x6

    iget-object v5, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->c:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {v5}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->c()Lcom/chase/sig/android/domain/MerchantPayee;

    move-result-object v5

    invoke-virtual {v5}, Lcom/chase/sig/android/domain/MerchantPayee;->g()Lcom/chase/sig/android/domain/State;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/chase/sig/android/activity/bd;->a(Lcom/chase/sig/android/domain/State;)Lcom/chase/sig/android/view/detail/q;

    move-result-object v0

    aput-object v0, v3, v4

    const/4 v0, 0x7

    iget-object v4, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->c:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {v4}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->c()Lcom/chase/sig/android/domain/MerchantPayee;

    move-result-object v4

    invoke-virtual {v4}, Lcom/chase/sig/android/domain/MerchantPayee;->h()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/chase/sig/android/activity/bd;->c(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/l;

    move-result-object v4

    aput-object v4, v3, v0

    const/16 v0, 0x8

    iget-object v4, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->c:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {v4}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->c()Lcom/chase/sig/android/domain/MerchantPayee;

    move-result-object v4

    invoke-virtual {v4}, Lcom/chase/sig/android/domain/MerchantPayee;->c()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/chase/sig/android/activity/bd;->b(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/l;

    move-result-object v4

    aput-object v4, v3, v0

    const/16 v0, 0x9

    iget-object v4, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->c:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {v4}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->g()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/chase/sig/android/activity/bd;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/l;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-virtual {v1, v3}, Lcom/chase/sig/android/view/detail/DetailView;->setRows([Lcom/chase/sig/android/view/detail/a;)V

    goto/16 :goto_1
.end method

.method public final a(ZI)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 335
    invoke-direct {p0}, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->d()Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-super {p0, v0, p2}, Lcom/chase/sig/android/activity/ai;->a(ZI)V

    .line 337
    return-void

    .line 335
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final a()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 180
    .line 182
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->b:Lcom/chase/sig/android/view/detail/i;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/i;->a()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 186
    :goto_0
    invoke-direct {p0}, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->d()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v2, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    const-string v3, "ACCOUNT_NUMBER"

    invoke-virtual {v2, v3}, Lcom/chase/sig/android/view/detail/DetailView;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    const-string v3, "BILLPAY_MESSAGE"

    invoke-virtual {v2, v3}, Lcom/chase/sig/android/view/detail/DetailView;->a(Ljava/lang/String;)V

    move v2, v1

    :cond_0
    if-nez v2, :cond_1

    .line 190
    :goto_1
    return v1

    :cond_1
    move v1, v0

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method public final b(ZI)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 344
    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->d()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-super {p0, v0, p2}, Lcom/chase/sig/android/activity/ai;->b(ZI)V

    .line 346
    return-void

    .line 344
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 151
    const/16 v0, 0xa

    if-ne p1, v0, :cond_0

    .line 152
    invoke-static {}, Lcom/chase/sig/android/c;->a()V

    .line 154
    sparse-switch p2, :sswitch_data_0

    .line 163
    const/16 v0, 0x1e

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->setResult(I)V

    .line 164
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->finish()V

    .line 167
    :cond_0
    :goto_0
    :sswitch_0
    return-void

    .line 156
    :sswitch_1
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->b(Landroid/os/Bundle;)V

    goto :goto_0

    .line 154
    nop

    :sswitch_data_0
    .sparse-switch
        0x14 -> :sswitch_0
        0x19 -> :sswitch_1
    .end sparse-switch
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 145
    invoke-super {p0}, Lcom/chase/sig/android/activity/ai;->onResume()V

    .line 146
    invoke-static {}, Lcom/chase/sig/android/c;->a()V

    .line 147
    return-void
.end method
