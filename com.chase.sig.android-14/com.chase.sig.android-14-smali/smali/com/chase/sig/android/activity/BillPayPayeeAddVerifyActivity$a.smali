.class public Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity$a;
.super Lcom/chase/sig/android/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/d",
        "<",
        "Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/service/billpay/BillPayPayeeAddResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 258
    invoke-direct {p0}, Lcom/chase/sig/android/d;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .parameter

    .prologue
    .line 258
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->A()Lcom/chase/sig/android/service/n;

    invoke-static {}, Lcom/chase/sig/android/service/n;->g()Lcom/chase/sig/android/service/billpay/b;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->a(Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;)Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->c()Lcom/chase/sig/android/domain/MerchantPayee;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/MerchantPayee;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->d(Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;)Lcom/chase/sig/android/service/billpay/BillPayPayeeAddResponse;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/service/billpay/BillPayPayeeAddResponse;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/chase/sig/android/service/billpay/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/service/billpay/BillPayPayeeAddResponse;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 2
    .parameter

    .prologue
    .line 258
    check-cast p1, Lcom/chase/sig/android/service/billpay/BillPayPayeeAddResponse;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/billpay/BillPayPayeeAddResponse;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/billpay/BillPayPayeeAddResponse;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->c(Ljava/util/List;)V

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/chase/sig/android/c;->a()V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;

    invoke-static {v0, p1}, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->c(Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;Lcom/chase/sig/android/service/billpay/BillPayPayeeAddResponse;)V

    goto :goto_0
.end method
