.class public Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity$b;
.super Lcom/chase/sig/android/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/d",
        "<",
        "Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/service/billpay/BillPayGetPayeesResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 309
    invoke-direct {p0}, Lcom/chase/sig/android/d;-><init>()V

    .line 312
    const-string v0, ""

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity$b;->a:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 309
    check-cast p1, [Ljava/lang/String;

    const/4 v0, 0x0

    aget-object v0, p1, v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity$b;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->A()Lcom/chase/sig/android/service/n;

    invoke-static {}, Lcom/chase/sig/android/service/n;->g()Lcom/chase/sig/android/service/billpay/b;

    invoke-static {}, Lcom/chase/sig/android/service/billpay/b;->a()Lcom/chase/sig/android/service/billpay/BillPayGetPayeesResponse;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 3
    .parameter

    .prologue
    .line 309
    check-cast p1, Lcom/chase/sig/android/service/billpay/BillPayGetPayeesResponse;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/billpay/BillPayGetPayeesResponse;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/billpay/BillPayGetPayeesResponse;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->b(Ljava/util/List;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/chase/sig/android/service/billpay/BillPayGetPayeesResponse;->a()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;

    const v1, 0x7f070282

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->f(I)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v1, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;

    iget-object v2, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity$b;->a:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->a(Ljava/util/ArrayList;Ljava/lang/String;)V

    goto :goto_0
.end method
