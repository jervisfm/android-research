.class public Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity$c;
.super Lcom/chase/sig/android/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/d",
        "<",
        "Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/service/billpay/BillPayPayeeAddResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 209
    invoke-direct {p0}, Lcom/chase/sig/android/d;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 209
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->A()Lcom/chase/sig/android/service/n;

    invoke-static {}, Lcom/chase/sig/android/service/n;->g()Lcom/chase/sig/android/service/billpay/b;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->b(Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;)Lcom/chase/sig/android/service/billpay/a;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/service/billpay/b;->a(Lcom/chase/sig/android/service/billpay/a;)Lcom/chase/sig/android/service/billpay/BillPayPayeeAddResponse;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 3
    .parameter

    .prologue
    .line 209
    check-cast p1, Lcom/chase/sig/android/service/billpay/BillPayPayeeAddResponse;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/billpay/BillPayPayeeAddResponse;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/billpay/BillPayPayeeAddResponse;->g()Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->a(Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;Ljava/util/List;)Ljava/util/List;

    const-string v0, "50057"

    invoke-virtual {p1, v0}, Lcom/chase/sig/android/service/billpay/BillPayPayeeAddResponse;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;

    const/16 v1, 0xd

    const-string v2, "50057"

    invoke-virtual {p1, v2}, Lcom/chase/sig/android/service/billpay/BillPayPayeeAddResponse;->d(Ljava/lang/String;)Lcom/chase/sig/android/service/IServiceError;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->a(ILcom/chase/sig/android/service/IServiceError;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;

    const/16 v2, 0xc

    iget-object v1, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v1, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;

    invoke-static {v1}, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->c(Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->a(ILjava/util/List;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;

    invoke-static {v0, p1}, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->a(Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;Lcom/chase/sig/android/service/billpay/BillPayPayeeAddResponse;)Lcom/chase/sig/android/service/billpay/BillPayPayeeAddResponse;

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/chase/sig/android/c;->a(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;

    invoke-static {v0, p1}, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->b(Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;Lcom/chase/sig/android/service/billpay/BillPayPayeeAddResponse;)V

    goto :goto_0
.end method
