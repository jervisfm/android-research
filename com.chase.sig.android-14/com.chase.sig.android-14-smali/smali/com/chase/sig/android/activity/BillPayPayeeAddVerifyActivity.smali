.class public Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$e;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity$b;,
        Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity$a;,
        Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity$c;
    }
.end annotation


# instance fields
.field private a:Lcom/chase/sig/android/service/billpay/a;

.field private b:Lcom/chase/sig/android/view/detail/DetailView;

.field private c:Lcom/chase/sig/android/service/billpay/BillPayPayeeAddResponse;

.field private d:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

.field private k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/service/IServiceError;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    .line 309
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;)Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;
    .locals 1
    .parameter

    .prologue
    .line 38
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->d:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    return-object v0
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;Lcom/chase/sig/android/service/billpay/BillPayPayeeAddResponse;)Lcom/chase/sig/android/service/billpay/BillPayPayeeAddResponse;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 38
    iput-object p1, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->c:Lcom/chase/sig/android/service/billpay/BillPayPayeeAddResponse;

    return-object p1
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 38
    iput-object p1, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->k:Ljava/util/List;

    return-object p1
.end method

.method private a(Lcom/chase/sig/android/service/billpay/BillPayPayeeAddResponse;)V
    .locals 5
    .parameter

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 293
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->b:Lcom/chase/sig/android/view/detail/DetailView;

    const-string v1, "DELIVERY_METHOD"

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/detail/DetailView;->e(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailRow;

    .line 295
    invoke-virtual {p1}, Lcom/chase/sig/android/service/billpay/BillPayPayeeAddResponse;->b()Lcom/chase/sig/android/domain/Payee;

    move-result-object v1

    .line 296
    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/DetailRow;->p()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/Payee;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 297
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->b:Lcom/chase/sig/android/view/detail/DetailView;

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/view/detail/DetailView;->setVisibility(I)V

    .line 299
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->d:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 300
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->b:Lcom/chase/sig/android/view/detail/DetailView;

    const-string v2, "ACCOUNT_NUMBER"

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/view/detail/DetailView;->e(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailRow;

    .line 301
    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/DetailRow;->p()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/Payee;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 304
    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->d:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/Payee;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->c(Ljava/lang/String;)V

    .line 306
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->b:Lcom/chase/sig/android/view/detail/DetailView;

    const-string v1, "ACCOUNT_NUMBER"

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/detail/DetailView;->e(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailRow;

    iget-object v1, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->b:Lcom/chase/sig/android/view/detail/DetailView;

    const-string v2, "BILLPAY_MESSAGE"

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/view/detail/DetailView;->e(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/view/detail/DetailRow;

    iget-object v2, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->d:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->e()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/chase/sig/android/util/s;->l(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/DetailRow;->k()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v1}, Lcom/chase/sig/android/view/detail/DetailRow;->k()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 307
    :goto_0
    return-void

    .line 306
    :cond_1
    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/DetailRow;->k()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v1}, Lcom/chase/sig/android/view/detail/DetailRow;->k()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;)Lcom/chase/sig/android/service/billpay/a;
    .locals 1
    .parameter

    .prologue
    .line 38
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->a:Lcom/chase/sig/android/service/billpay/a;

    return-object v0
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;Lcom/chase/sig/android/service/billpay/BillPayPayeeAddResponse;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->a(Lcom/chase/sig/android/service/billpay/BillPayPayeeAddResponse;)V

    return-void
.end method

.method static synthetic c(Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 38
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->k:Ljava/util/List;

    return-object v0
.end method

.method static synthetic c(Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;Lcom/chase/sig/android/service/billpay/BillPayPayeeAddResponse;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 38
    const-string v0, "Active"

    invoke-virtual {p1}, Lcom/chase/sig/android/service/billpay/BillPayPayeeAddResponse;->b()Lcom/chase/sig/android/domain/Payee;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/Payee;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iput-object p1, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->c:Lcom/chase/sig/android/service/billpay/BillPayPayeeAddResponse;

    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->showDialog(I)V

    :goto_0
    return-void

    :cond_0
    const/16 v0, 0xe

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->showDialog(I)V

    goto :goto_0
.end method

.method private d()Landroid/content/DialogInterface$OnClickListener;
    .locals 1

    .prologue
    .line 432
    new-instance v0, Lcom/chase/sig/android/activity/bc;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/activity/bc;-><init>(Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;)V

    return-object v0
.end method

.method static synthetic d(Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;)Lcom/chase/sig/android/service/billpay/BillPayPayeeAddResponse;
    .locals 1
    .parameter

    .prologue
    .line 38
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->c:Lcom/chase/sig/android/service/billpay/BillPayPayeeAddResponse;

    return-object v0
.end method


# virtual methods
.method protected final a(I)V
    .locals 3
    .parameter

    .prologue
    .line 239
    packed-switch p1, :pswitch_data_0

    .line 253
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ai;->a(I)V

    .line 256
    :goto_0
    return-void

    .line 241
    :pswitch_0
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 242
    const-string v2, "errors"

    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->k:Ljava/util/List;

    check-cast v0, Ljava/io/Serializable;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 244
    const-string v0, "payee_info"

    iget-object v2, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->d:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 245
    const/16 v0, 0x19

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->setResult(ILandroid/content/Intent;)V

    .line 246
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->finish()V

    goto :goto_0

    .line 249
    :pswitch_1
    const/16 v0, 0x14

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->setResult(I)V

    .line 250
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->finish()V

    goto :goto_0

    .line 239
    nop

    :pswitch_data_0
    .packed-switch 0xc
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 8
    .parameter

    .prologue
    const/4 v7, 0x0

    const/4 v4, 0x0

    .line 77
    const v0, 0x7f07018c

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->setTitle(I)V

    .line 78
    const v0, 0x7f030012

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->b(I)V

    .line 80
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 82
    const-string v0, "payee_info"

    invoke-static {p1, v0}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 83
    const-string v0, "payee_info"

    invoke-static {p1, v0, v4}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->d:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    .line 109
    :goto_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->d:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->l(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->d:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    iget-object v1, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->d:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->c()Lcom/chase/sig/android/domain/MerchantPayee;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/MerchantPayee;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->d(Ljava/lang/String;)V

    .line 113
    :cond_0
    const-string v0, "add_payee_response"

    invoke-static {p1, v0, v4}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/service/billpay/BillPayPayeeAddResponse;

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->c:Lcom/chase/sig/android/service/billpay/BillPayPayeeAddResponse;

    .line 115
    const v0, 0x7f09000f

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->b:Lcom/chase/sig/android/view/detail/DetailView;

    .line 117
    const v0, 0x7f09004f

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 118
    new-instance v1, Lcom/chase/sig/android/activity/az;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/az;-><init>(Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 126
    iget-object v1, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->b:Lcom/chase/sig/android/view/detail/DetailView;

    const/16 v0, 0xa

    new-array v2, v0, [Lcom/chase/sig/android/view/detail/a;

    new-instance v0, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v3, "Payee name"

    iget-object v4, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->d:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {v4}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->c()Lcom/chase/sig/android/domain/MerchantPayee;

    move-result-object v4

    invoke-virtual {v4}, Lcom/chase/sig/android/domain/MerchantPayee;->b()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v3, v4}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "PAYEE"

    iput-object v3, v0, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    aput-object v0, v2, v7

    const/4 v0, 0x1

    new-instance v3, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v4, "Payee nickname"

    iget-object v5, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->d:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {v5}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->f()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "PAYEE_NICKNAME"

    iput-object v4, v3, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    aput-object v3, v2, v0

    const/4 v0, 0x2

    new-instance v3, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v4, "Account number"

    iget-object v5, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->d:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {v5}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->d()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "ACCOUNT_NUMBER"

    iput-object v4, v3, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    aput-object v3, v2, v0

    const/4 v0, 0x3

    new-instance v3, Lcom/chase/sig/android/view/detail/b;

    const-string v4, "Payee address"

    iget-object v5, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->d:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {v5}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->c()Lcom/chase/sig/android/domain/MerchantPayee;

    move-result-object v5

    invoke-virtual {v5}, Lcom/chase/sig/android/domain/MerchantPayee;->d()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->d:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {v6}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->c()Lcom/chase/sig/android/domain/MerchantPayee;

    move-result-object v6

    invoke-virtual {v6}, Lcom/chase/sig/android/domain/MerchantPayee;->e()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/chase/sig/android/util/s;->s(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v4, v5, v6}, Lcom/chase/sig/android/view/detail/b;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "ADDRESS"

    iput-object v4, v3, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    aput-object v3, v2, v0

    const/4 v0, 0x4

    new-instance v3, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v4, "City"

    iget-object v5, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->d:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {v5}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->c()Lcom/chase/sig/android/domain/MerchantPayee;

    move-result-object v5

    invoke-virtual {v5}, Lcom/chase/sig/android/domain/MerchantPayee;->f()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "CITY"

    iput-object v4, v3, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    aput-object v3, v2, v0

    const/4 v0, 0x5

    new-instance v3, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v4, "State"

    iget-object v5, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->d:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {v5}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->c()Lcom/chase/sig/android/domain/MerchantPayee;

    move-result-object v5

    invoke-virtual {v5}, Lcom/chase/sig/android/domain/MerchantPayee;->g()Lcom/chase/sig/android/domain/State;

    move-result-object v5

    invoke-virtual {v5}, Lcom/chase/sig/android/domain/State;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "STATE"

    iput-object v4, v3, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    aput-object v3, v2, v0

    const/4 v0, 0x6

    new-instance v3, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v4, "ZIP code"

    iget-object v5, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->d:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {v5}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->c()Lcom/chase/sig/android/domain/MerchantPayee;

    move-result-object v5

    invoke-virtual {v5}, Lcom/chase/sig/android/domain/MerchantPayee;->h()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/chase/sig/android/util/s;->x(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/chase/sig/android/util/s;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "ZIP_CODE"

    iput-object v4, v3, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    aput-object v3, v2, v0

    const/4 v3, 0x7

    new-instance v0, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v4, "Phone number"

    iget-object v5, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->d:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {v5}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->c()Lcom/chase/sig/android/domain/MerchantPayee;

    move-result-object v5

    invoke-virtual {v5}, Lcom/chase/sig/android/domain/MerchantPayee;->c()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v4, v5}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "PHONE_NUMBER"

    iput-object v4, v0, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailRow;

    iget-object v4, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->d:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {v4}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->c()Lcom/chase/sig/android/domain/MerchantPayee;

    move-result-object v4

    invoke-virtual {v4}, Lcom/chase/sig/android/domain/MerchantPayee;->c()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/chase/sig/android/util/s;->l(Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, v0, Lcom/chase/sig/android/view/detail/a;->j:Z

    aput-object v0, v2, v3

    const/16 v3, 0x8

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Message"

    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->d:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->l(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, ""

    :goto_1
    invoke-direct {v4, v5, v0}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "BILLPAY_MESSAGE"

    iput-object v0, v4, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    aput-object v4, v2, v3

    const/16 v0, 0x9

    new-instance v3, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v4, "Delivery method"

    const-string v5, "--"

    invoke-direct {v3, v4, v5}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "DELIVERY_METHOD"

    iput-object v4, v3, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    aput-object v3, v2, v0

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/view/detail/DetailView;->setRows([Lcom/chase/sig/android/view/detail/a;)V

    .line 152
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->c:Lcom/chase/sig/android/service/billpay/BillPayPayeeAddResponse;

    if-nez v0, :cond_5

    .line 153
    new-instance v0, Lcom/chase/sig/android/service/billpay/a;

    iget-object v1, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->d:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-direct {v0, v1}, Lcom/chase/sig/android/service/billpay/a;-><init>(Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;)V

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->a:Lcom/chase/sig/android/service/billpay/a;

    .line 154
    const-class v0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity$c;

    new-array v1, v7, [Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 159
    :goto_2
    const v0, 0x7f09004e

    new-instance v1, Lcom/chase/sig/android/activity/ba;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/ba;-><init>(Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;)V

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 170
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/chase/sig/android/activity/BillPayHomeActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 171
    const/high16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 172
    invoke-static {v0}, Lcom/chase/sig/android/c;->a(Landroid/content/Intent;)V

    .line 173
    return-void

    .line 84
    :cond_1
    const-string v0, "payee_info"

    invoke-static {v1, v0}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 85
    const-string v0, "payee_info"

    invoke-static {v1, v0, v4}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->d:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    goto/16 :goto_0

    .line 87
    :cond_2
    new-instance v0, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-direct {v0}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;-><init>()V

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->d:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    .line 88
    iget-object v2, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->d:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    const-string v0, "merchant_payee_data"

    invoke-static {v1, v0, v4}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/MerchantPayee;

    invoke-virtual {v2, v0}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->a(Lcom/chase/sig/android/domain/MerchantPayee;)V

    .line 91
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->d:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->c()Lcom/chase/sig/android/domain/MerchantPayee;

    move-result-object v0

    if-nez v0, :cond_3

    .line 93
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->finish()V

    .line 96
    :cond_3
    iget-object v2, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->d:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    const-string v0, "account_number"

    invoke-static {v1, v0, v4}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->s(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->b(Ljava/lang/String;)V

    .line 99
    iget-object v2, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->d:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    const-string v0, "merchant_bill_pay_message"

    const-string v3, ""

    invoke-static {v1, v0, v3}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->e(Ljava/lang/String;)V

    .line 101
    iget-object v2, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->d:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    const-string v0, "payee_nick_name"

    iget-object v3, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->d:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {v3}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->c()Lcom/chase/sig/android/domain/MerchantPayee;

    move-result-object v3

    invoke-virtual {v3}, Lcom/chase/sig/android/domain/MerchantPayee;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v0, v3}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->d(Ljava/lang/String;)V

    .line 103
    iget-object v2, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->d:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    const-string v0, "selectedAccountId"

    const-string v3, ""

    invoke-static {v1, v0, v3}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->a(Ljava/lang/String;)V

    .line 105
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->d:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    const-string v2, "from_merchant_directory"

    invoke-static {v1, v2}, Lcom/chase/sig/android/util/e;->b(Landroid/os/Bundle;Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->a(Z)V

    goto/16 :goto_0

    .line 126
    :cond_4
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->d:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->g()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 156
    :cond_5
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->c:Lcom/chase/sig/android/service/billpay/BillPayPayeeAddResponse;

    invoke-direct {p0, v0}, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->a(Lcom/chase/sig/android/service/billpay/BillPayPayeeAddResponse;)V

    goto/16 :goto_2
.end method

.method public final a(Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 4
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/chase/sig/android/domain/Payee;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 338
    const/4 v1, 0x0

    .line 340
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/Payee;

    .line 341
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Payee;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 347
    :goto_0
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/chase/sig/android/activity/BillPayAddStartActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 349
    const-string v2, "payee"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 350
    const-string v2, "selectedAccountId"

    iget-object v3, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->d:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {v3}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 352
    const-string v2, "scheduledAmount"

    iget-object v3, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->d:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {v3}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->h()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 353
    const-string v2, "deliverBydate"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Payee;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 355
    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->startActivity(Landroid/content/Intent;)V

    .line 356
    return-void

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 203
    const/16 v0, 0x32

    if-ne p1, v0, :cond_0

    const/16 v0, 0x1e

    if-ne p2, v0, :cond_0

    .line 205
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->finish()V

    .line 207
    :cond_0
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 8
    .parameter

    .prologue
    const v7, 0x7f070192

    const v3, 0x7f070191

    const/4 v6, 0x0

    const/4 v5, -0x1

    const/16 v4, 0x20

    .line 361
    new-instance v1, Lcom/chase/sig/android/view/k$a;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/view/k$a;-><init>(Landroid/content/Context;)V

    .line 364
    sparse-switch p1, :sswitch_data_0

    .line 427
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ai;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    .line 366
    :sswitch_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->d:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 367
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->d:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->d:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->e()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/chase/sig/android/util/s;->s(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ") "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 384
    :goto_1
    iput-boolean v6, v1, Lcom/chase/sig/android/view/k$a;->i:Z

    const v2, 0x7f070190

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/view/k$a;->c(I)Lcom/chase/sig/android/view/k$a;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/chase/sig/android/view/k$a;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/k$a;

    move-result-object v0

    const-string v2, "Done"

    invoke-direct {p0}, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->d()Landroid/content/DialogInterface$OnClickListener;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/chase/sig/android/view/k$a;->a(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    move-result-object v0

    const-string v2, "Pay Bill"

    new-instance v3, Lcom/chase/sig/android/activity/bb;

    invoke-direct {v3, p0}, Lcom/chase/sig/android/activity/bb;-><init>(Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;)V

    invoke-virtual {v0, v2, v3}, Lcom/chase/sig/android/view/k$a;->b(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    .line 397
    invoke-virtual {v1, v5}, Lcom/chase/sig/android/view/k$a;->d(I)Lcom/chase/sig/android/view/k;

    move-result-object v0

    goto :goto_0

    .line 376
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->d:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 400
    :sswitch_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->d:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 401
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070194

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->d:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->d:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->e()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/chase/sig/android/util/s;->s(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ") "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070195

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 418
    :goto_2
    iput-boolean v6, v1, Lcom/chase/sig/android/view/k$a;->i:Z

    const-string v2, "Ok"

    invoke-direct {p0}, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->d()Landroid/content/DialogInterface$OnClickListener;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/chase/sig/android/view/k$a;->a(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    move-result-object v2

    const v3, 0x7f070193

    invoke-virtual {v2, v3}, Lcom/chase/sig/android/view/k$a;->c(I)Lcom/chase/sig/android/view/k$a;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/chase/sig/android/view/k$a;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/k$a;

    .line 424
    invoke-virtual {v1, v5}, Lcom/chase/sig/android/view/k$a;->d(I)Lcom/chase/sig/android/view/k;

    move-result-object v0

    goto/16 :goto_0

    .line 410
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070194

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->d:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070195

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 364
    nop

    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_0
        0xe -> :sswitch_1
    .end sparse-switch
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 191
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ai;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 192
    const-string v0, "add_payee_response"

    iget-object v1, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->c:Lcom/chase/sig/android/service/billpay/BillPayPayeeAddResponse;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 193
    const-string v0, "payee_info"

    iget-object v1, p0, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;->d:Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 194
    return-void
.end method
