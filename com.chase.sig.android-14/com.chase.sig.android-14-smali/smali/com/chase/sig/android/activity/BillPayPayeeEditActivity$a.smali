.class public Lcom/chase/sig/android/activity/BillPayPayeeEditActivity$a;
.super Lcom/chase/sig/android/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/d",
        "<",
        "Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;",
        "Lcom/chase/sig/android/domain/Payee;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/service/billpay/DeletePayeeResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Lcom/chase/sig/android/domain/Payee;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 354
    invoke-direct {p0}, Lcom/chase/sig/android/d;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .parameter

    .prologue
    .line 354
    check-cast p1, [Lcom/chase/sig/android/domain/Payee;

    const/4 v0, 0x0

    aget-object v0, p1, v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity$a;->a:Lcom/chase/sig/android/domain/Payee;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->A()Lcom/chase/sig/android/service/n;

    invoke-static {}, Lcom/chase/sig/android/service/n;->g()Lcom/chase/sig/android/service/billpay/b;

    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity$a;->a:Lcom/chase/sig/android/domain/Payee;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Payee;->j()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity$a;->a:Lcom/chase/sig/android/domain/Payee;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/Payee;->n()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/chase/sig/android/service/billpay/b;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/service/billpay/DeletePayeeResponse;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 2
    .parameter

    .prologue
    .line 354
    check-cast p1, Lcom/chase/sig/android/service/billpay/DeletePayeeResponse;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/billpay/DeletePayeeResponse;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/billpay/DeletePayeeResponse;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->c(Ljava/util/List;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->showDialog(I)V

    goto :goto_0
.end method
