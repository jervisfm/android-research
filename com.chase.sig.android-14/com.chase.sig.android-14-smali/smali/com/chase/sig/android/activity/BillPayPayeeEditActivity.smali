.class public Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$e;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/BillPayPayeeEditActivity$a;,
        Lcom/chase/sig/android/activity/BillPayPayeeEditActivity$b;
    }
.end annotation


# instance fields
.field private a:Lcom/chase/sig/android/view/detail/DetailView;

.field private b:Lcom/chase/sig/android/view/detail/i;

.field private c:Lcom/chase/sig/android/domain/Payee;

.field private d:Landroid/widget/EditText;

.field private k:Landroid/widget/EditText;

.field private l:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    .line 354
    return-void
.end method

.method private static a(Landroid/widget/EditText;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 264
    invoke-virtual {p0, v0}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 265
    invoke-virtual {p0, v0}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 266
    const-string v0, ""

    invoke-virtual {p0, v0}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 267
    return-void
.end method

.method private a(Landroid/widget/EditText;ZZI)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 243
    invoke-virtual {p1, p2}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 244
    invoke-virtual {p1, p2}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 245
    invoke-virtual {p1, p2}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    .line 246
    if-nez p2, :cond_0

    if-eqz p3, :cond_1

    .line 247
    :cond_0
    invoke-virtual {p0, p4}, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 252
    :goto_0
    return-void

    .line 250
    :cond_1
    const-string v0, ""

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;)V
    .locals 4
    .parameter

    .prologue
    .line 32
    new-instance v1, Lcom/chase/sig/android/domain/PayeeAddress;

    invoke-direct {v1}, Lcom/chase/sig/android/domain/PayeeAddress;-><init>()V

    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->c:Lcom/chase/sig/android/domain/Payee;

    iget-object v2, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    const-string v3, "PAYEE"

    invoke-virtual {v2, v3}, Lcom/chase/sig/android/view/detail/DetailView;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/domain/Payee;->g(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->c:Lcom/chase/sig/android/domain/Payee;

    iget-object v2, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    const-string v3, "PAYEE_NICKNAME"

    invoke-virtual {v2, v3}, Lcom/chase/sig/android/view/detail/DetailView;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/domain/Payee;->f(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->c:Lcom/chase/sig/android/domain/Payee;

    iget-object v2, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    const-string v3, "ACCOUNT_NUMBER"

    invoke-virtual {v2, v3}, Lcom/chase/sig/android/view/detail/DetailView;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/domain/Payee;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    const-string v2, "ADDRESS1"

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/view/detail/DetailView;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/PayeeAddress;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    const-string v2, "ADDRESS2"

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/view/detail/DetailView;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/PayeeAddress;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    const-string v2, "CITY"

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/view/detail/DetailView;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/PayeeAddress;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->c:Lcom/chase/sig/android/domain/Payee;

    iget-object v2, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    const-string v3, "PHONE_NUMBER"

    invoke-virtual {v2, v3}, Lcom/chase/sig/android/view/detail/DetailView;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/domain/Payee;->j(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    const-string v2, "ZIP_CODE"

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/view/detail/DetailView;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/PayeeAddress;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->c:Lcom/chase/sig/android/domain/Payee;

    iget-object v2, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    const-string v3, "BILLPAY_MESSAGE"

    invoke-virtual {v2, v3}, Lcom/chase/sig/android/view/detail/DetailView;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/domain/Payee;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    const-string v2, "STATE"

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/view/detail/DetailView;->e(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/q;

    invoke-static {}, Lcom/chase/sig/android/domain/State;->values()[Lcom/chase/sig/android/domain/State;

    move-result-object v2

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/q;->q()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/view/JPSpinner;->getSelectedItemPosition()I

    move-result v0

    aget-object v0, v2, v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/PayeeAddress;->a(Lcom/chase/sig/android/domain/State;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->c:Lcom/chase/sig/android/domain/Payee;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/Payee;->a(Lcom/chase/sig/android/domain/PayeeAddress;)V

    return-void
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;)Lcom/chase/sig/android/domain/Payee;
    .locals 1
    .parameter

    .prologue
    .line 32
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->c:Lcom/chase/sig/android/domain/Payee;

    return-object v0
.end method

.method private b(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 299
    const-string v0, "errors"

    invoke-static {p1, v0}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 301
    const-string v0, "errors"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 303
    iget-object v1, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->b:Lcom/chase/sig/android/view/detail/i;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/view/detail/i;->a(Ljava/util/List;)V

    .line 305
    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;)V
    .locals 7
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 32
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->d:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v3

    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->k:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v4

    if-nez v3, :cond_0

    if-nez v4, :cond_0

    move v0, v1

    :goto_0
    iget-object v5, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->k:Landroid/widget/EditText;

    if-nez v3, :cond_1

    move v3, v1

    :goto_1
    const v6, 0x7f070080

    invoke-direct {p0, v5, v3, v0, v6}, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->a(Landroid/widget/EditText;ZZI)V

    iget-object v3, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->d:Landroid/widget/EditText;

    if-nez v4, :cond_2

    :goto_2
    const v2, 0x7f070099

    invoke-direct {p0, v3, v1, v0, v2}, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->a(Landroid/widget/EditText;ZZI)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v3, v2

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2
.end method

.method private d()V
    .locals 2

    .prologue
    .line 141
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    const v1, 0x7f090051

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->b(Lcom/chase/sig/android/view/detail/DetailView;I)V

    .line 142
    return-void
.end method

.method static synthetic d(Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;)V
    .locals 1
    .parameter

    .prologue
    .line 32
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/chase/sig/android/activity/BillPayHomeActivity;->a(Landroid/content/Context;Z)V

    return-void
.end method

.method private e()Z
    .locals 2

    .prologue
    .line 294
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    const-string v1, "ACCOUNT_NUMBER"

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/detail/DetailView;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->l(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    const-string v1, "BILLPAY_MESSAGE"

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/detail/DetailView;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->l(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 9
    .parameter

    .prologue
    const/4 v8, 0x0

    const v7, 0x7f090051

    .line 52
    const v0, 0x7f07018d

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->setTitle(I)V

    .line 53
    const v0, 0x7f030013

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->b(I)V

    .line 55
    invoke-virtual {p0, v7}, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->l:Landroid/widget/Button;

    .line 57
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 59
    if-eqz v1, :cond_2

    const-string v0, "payee"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 60
    const-string v0, "payee"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/Payee;

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->c:Lcom/chase/sig/android/domain/Payee;

    .line 65
    :goto_0
    new-instance v2, Lcom/chase/sig/android/activity/bd;

    invoke-direct {v2}, Lcom/chase/sig/android/activity/bd;-><init>()V

    .line 66
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->c:Lcom/chase/sig/android/domain/Payee;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Payee;->o()Lcom/chase/sig/android/domain/PayeeAddress;

    move-result-object v3

    .line 68
    const v0, 0x7f090049

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    .line 69
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    const/16 v4, 0xa

    new-array v4, v4, [Lcom/chase/sig/android/view/detail/a;

    iget-object v5, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->c:Lcom/chase/sig/android/domain/Payee;

    invoke-virtual {v5}, Lcom/chase/sig/android/domain/Payee;->i()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/chase/sig/android/activity/bd;->i(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/l;

    move-result-object v5

    aput-object v5, v4, v8

    const/4 v5, 0x1

    iget-object v6, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->c:Lcom/chase/sig/android/domain/Payee;

    invoke-virtual {v6}, Lcom/chase/sig/android/domain/Payee;->h()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/chase/sig/android/activity/bd;->h(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/l;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    iget-object v6, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->c:Lcom/chase/sig/android/domain/Payee;

    invoke-virtual {v6}, Lcom/chase/sig/android/domain/Payee;->c()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/chase/sig/android/activity/bd;->g(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/l;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x3

    invoke-virtual {v3}, Lcom/chase/sig/android/domain/PayeeAddress;->a()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/chase/sig/android/activity/bd;->f(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/l;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x4

    invoke-virtual {v3}, Lcom/chase/sig/android/domain/PayeeAddress;->b()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/chase/sig/android/activity/bd;->e(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/l;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x5

    invoke-virtual {v3}, Lcom/chase/sig/android/domain/PayeeAddress;->c()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/chase/sig/android/activity/bd;->d(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/l;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x6

    invoke-virtual {v3}, Lcom/chase/sig/android/domain/PayeeAddress;->e()Lcom/chase/sig/android/domain/State;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/chase/sig/android/activity/bd;->a(Lcom/chase/sig/android/domain/State;)Lcom/chase/sig/android/view/detail/q;

    move-result-object v2

    aput-object v2, v4, v5

    const/4 v2, 0x7

    invoke-virtual {v3}, Lcom/chase/sig/android/domain/PayeeAddress;->d()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/chase/sig/android/activity/bd;->c(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/l;

    move-result-object v3

    aput-object v3, v4, v2

    const/16 v2, 0x8

    iget-object v3, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->c:Lcom/chase/sig/android/domain/Payee;

    invoke-virtual {v3}, Lcom/chase/sig/android/domain/Payee;->p()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/chase/sig/android/activity/bd;->b(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/l;

    move-result-object v3

    aput-object v3, v4, v2

    const/16 v2, 0x9

    iget-object v3, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->c:Lcom/chase/sig/android/domain/Payee;

    invoke-virtual {v3}, Lcom/chase/sig/android/domain/Payee;->g()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/chase/sig/android/activity/bd;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/l;

    move-result-object v3

    aput-object v3, v4, v2

    invoke-virtual {v0, v4}, Lcom/chase/sig/android/view/detail/DetailView;->setRows([Lcom/chase/sig/android/view/detail/a;)V

    .line 81
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    const-string v2, "BILLPAY_MESSAGE"

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/view/detail/DetailView;->e(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/l;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/l;->p()Landroid/widget/EditText;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->d:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    const-string v2, "ACCOUNT_NUMBER"

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/view/detail/DetailView;->e(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/l;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/l;->p()Landroid/widget/EditText;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->k:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->d:Landroid/widget/EditText;

    new-instance v2, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity$b;

    invoke-direct {v2, p0}, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity$b;-><init>(Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;)V

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->k:Landroid/widget/EditText;

    new-instance v2, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity$b;

    invoke-direct {v2, p0}, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity$b;-><init>(Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;)V

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->d:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->k:Landroid/widget/EditText;

    invoke-static {v0}, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->a(Landroid/widget/EditText;)V

    .line 82
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    invoke-virtual {p0, v0, v7}, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->a(Lcom/chase/sig/android/view/detail/DetailView;I)V

    .line 83
    invoke-direct {p0, v1}, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->b(Landroid/os/Bundle;)V

    .line 85
    if-nez p1, :cond_1

    .line 86
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->l:Landroid/widget/Button;

    invoke-virtual {v0, v8}, Landroid/widget/Button;->setEnabled(Z)V

    .line 87
    invoke-direct {p0}, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->d()V

    .line 90
    :cond_1
    new-instance v0, Lcom/chase/sig/android/view/detail/i;

    iget-object v1, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    invoke-direct {v0, v1}, Lcom/chase/sig/android/view/detail/i;-><init>(Lcom/chase/sig/android/view/detail/DetailView;)V

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->b:Lcom/chase/sig/android/view/detail/i;

    .line 92
    const v0, 0x7f090050

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/chase/sig/android/activity/bf;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/bf;-><init>(Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 100
    const v0, 0x7f09004a

    new-instance v1, Lcom/chase/sig/android/activity/bg;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/bg;-><init>(Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;)V

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 108
    invoke-virtual {p0, v7}, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/chase/sig/android/activity/bh;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/bh;-><init>(Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 130
    return-void

    .line 62
    :cond_2
    new-instance v0, Lcom/chase/sig/android/domain/Payee;

    invoke-direct {v0}, Lcom/chase/sig/android/domain/Payee;-><init>()V

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->c:Lcom/chase/sig/android/domain/Payee;

    goto/16 :goto_0

    .line 81
    :cond_3
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->k:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->d:Landroid/widget/EditText;

    invoke-static {v0}, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->a(Landroid/widget/EditText;)V

    goto :goto_1
.end method

.method public final a(ZI)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 226
    invoke-direct {p0}, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->e()Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-super {p0, v0, p2}, Lcom/chase/sig/android/activity/ai;->a(ZI)V

    .line 228
    return-void

    .line 226
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final a()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 271
    .line 273
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->b:Lcom/chase/sig/android/view/detail/i;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/i;->a()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 277
    :goto_0
    invoke-direct {p0}, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->e()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v2, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    const-string v3, "ACCOUNT_NUMBER"

    invoke-virtual {v2, v3}, Lcom/chase/sig/android/view/detail/DetailView;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    const-string v3, "BILLPAY_MESSAGE"

    invoke-virtual {v2, v3}, Lcom/chase/sig/android/view/detail/DetailView;->a(Ljava/lang/String;)V

    move v2, v1

    :cond_0
    if-nez v2, :cond_1

    .line 281
    :goto_1
    return v1

    :cond_1
    move v1, v0

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method public final b(ZI)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 259
    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->e()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-super {p0, v0, p2}, Lcom/chase/sig/android/activity/ai;->b(ZI)V

    .line 261
    return-void

    .line 259
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 168
    const/16 v0, 0xc

    if-ne p1, v0, :cond_0

    .line 169
    sparse-switch p2, :sswitch_data_0

    .line 178
    const/16 v0, 0x1e

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->setResult(I)V

    .line 179
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->finish()V

    .line 182
    :cond_0
    :goto_0
    :sswitch_0
    return-void

    .line 171
    :sswitch_1
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->b(Landroid/os/Bundle;)V

    goto :goto_0

    .line 169
    :sswitch_data_0
    .sparse-switch
        0x14 -> :sswitch_0
        0x19 -> :sswitch_1
    .end sparse-switch
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 5
    .parameter

    .prologue
    const/4 v3, 0x0

    const/4 v4, -0x1

    .line 310
    new-instance v1, Lcom/chase/sig/android/view/k$a;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/view/k$a;-><init>(Landroid/content/Context;)V

    .line 312
    packed-switch p1, :pswitch_data_0

    .line 350
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ai;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    .line 315
    :pswitch_0
    const v0, 0x7f0701a8

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/view/k$a;->b(I)Lcom/chase/sig/android/view/k$a;

    move-result-object v0

    iput-boolean v3, v0, Lcom/chase/sig/android/view/k$a;->i:Z

    const-string v2, "Delete Payee"

    new-instance v3, Lcom/chase/sig/android/activity/bi;

    invoke-direct {v3, p0}, Lcom/chase/sig/android/activity/bi;-><init>(Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;)V

    invoke-virtual {v0, v2, v3}, Lcom/chase/sig/android/view/k$a;->a(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    move-result-object v0

    const-string v2, "Cancel"

    new-instance v3, Lcom/chase/sig/android/activity/a/b;

    invoke-direct {v3}, Lcom/chase/sig/android/activity/a/b;-><init>()V

    invoke-virtual {v0, v2, v3}, Lcom/chase/sig/android/view/k$a;->b(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    .line 324
    invoke-virtual {v1, v4}, Lcom/chase/sig/android/view/k$a;->d(I)Lcom/chase/sig/android/view/k;

    move-result-object v0

    goto :goto_0

    .line 329
    :pswitch_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->c:Lcom/chase/sig/android/domain/Payee;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Payee;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 330
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Payee "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->c:Lcom/chase/sig/android/domain/Payee;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/Payee;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->c:Lcom/chase/sig/android/domain/Payee;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/Payee;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/chase/sig/android/util/s;->s(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ") has been deleted."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 337
    :goto_1
    iput-boolean v3, v1, Lcom/chase/sig/android/view/k$a;->i:Z

    const-string v2, "Ok"

    new-instance v3, Lcom/chase/sig/android/activity/bj;

    invoke-direct {v3, p0}, Lcom/chase/sig/android/activity/bj;-><init>(Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;)V

    invoke-virtual {v1, v2, v3}, Lcom/chase/sig/android/view/k$a;->a(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    move-result-object v2

    const v3, 0x7f0701a9

    invoke-virtual {v2, v3}, Lcom/chase/sig/android/view/k$a;->c(I)Lcom/chase/sig/android/view/k$a;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/chase/sig/android/view/k$a;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/k$a;

    .line 347
    invoke-virtual {v1, v4}, Lcom/chase/sig/android/view/k$a;->d(I)Lcom/chase/sig/android/view/k;

    move-result-object v0

    goto :goto_0

    .line 334
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Payee "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->c:Lcom/chase/sig/android/domain/Payee;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/Payee;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " has been deleted."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 312
    nop

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 134
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ai;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 136
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->l:Landroid/widget/Button;

    const-string v1, "change_button_is_enabled"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 137
    invoke-direct {p0}, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->d()V

    .line 138
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 186
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ai;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 187
    const-string v0, "change_button_is_enabled"

    iget-object v1, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->l:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->isEnabled()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 188
    return-void
.end method
