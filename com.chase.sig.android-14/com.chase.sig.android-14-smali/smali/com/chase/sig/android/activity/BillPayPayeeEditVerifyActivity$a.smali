.class public Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity$a;
.super Lcom/chase/sig/android/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/d",
        "<",
        "Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/service/billpay/BillPayPayeeEditResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 224
    invoke-direct {p0}, Lcom/chase/sig/android/d;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .parameter

    .prologue
    .line 224
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;->A()Lcom/chase/sig/android/service/n;

    invoke-static {}, Lcom/chase/sig/android/service/n;->g()Lcom/chase/sig/android/service/billpay/b;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;->c(Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;)Lcom/chase/sig/android/service/billpay/BillPayPayeeEditResponse;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/service/billpay/BillPayPayeeEditResponse;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;->a(Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;)Lcom/chase/sig/android/domain/Payee;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Payee;->n()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/chase/sig/android/service/billpay/b;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/service/billpay/BillPayPayeeEditResponse;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 2
    .parameter

    .prologue
    .line 224
    check-cast p1, Lcom/chase/sig/android/service/billpay/BillPayPayeeEditResponse;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/billpay/BillPayPayeeEditResponse;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/billpay/BillPayPayeeEditResponse;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;->b(Ljava/util/List;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;

    invoke-virtual {v0, p1}, Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;->a(Lcom/chase/sig/android/service/billpay/BillPayPayeeEditResponse;)V

    goto :goto_0
.end method
