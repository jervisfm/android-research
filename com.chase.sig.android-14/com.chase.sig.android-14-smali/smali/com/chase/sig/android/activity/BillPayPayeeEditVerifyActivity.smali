.class public Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$e;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity$a;,
        Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity$b;
    }
.end annotation


# instance fields
.field private a:Lcom/chase/sig/android/view/detail/DetailView;

.field private b:Lcom/chase/sig/android/service/billpay/BillPayPayeeEditResponse;

.field private c:Lcom/chase/sig/android/domain/Payee;

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/service/IServiceError;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    .line 224
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;)Lcom/chase/sig/android/domain/Payee;
    .locals 1
    .parameter

    .prologue
    .line 27
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;->c:Lcom/chase/sig/android/domain/Payee;

    return-object v0
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;Lcom/chase/sig/android/service/billpay/BillPayPayeeEditResponse;)Lcom/chase/sig/android/service/billpay/BillPayPayeeEditResponse;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 27
    iput-object p1, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;->b:Lcom/chase/sig/android/service/billpay/BillPayPayeeEditResponse;

    return-object p1
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 27
    iput-object p1, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;->d:Ljava/util/List;

    return-object p1
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 27
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;->d:Ljava/util/List;

    return-object v0
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;Lcom/chase/sig/android/service/billpay/BillPayPayeeEditResponse;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;->b(Lcom/chase/sig/android/service/billpay/BillPayPayeeEditResponse;)V

    return-void
.end method

.method private b(Lcom/chase/sig/android/service/billpay/BillPayPayeeEditResponse;)V
    .locals 5
    .parameter

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 199
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    const-string v1, "DELIVERY_METHOD"

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/detail/DetailView;->e(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailRow;

    .line 201
    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/DetailRow;->p()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p1}, Lcom/chase/sig/android/service/billpay/BillPayPayeeEditResponse;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 202
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/view/detail/DetailView;->setVisibility(I)V

    .line 204
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    const-string v1, "ACCOUNT_NUMBER"

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/detail/DetailView;->e(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailRow;

    .line 205
    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/DetailRow;->p()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p1}, Lcom/chase/sig/android/service/billpay/BillPayPayeeEditResponse;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 207
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    const-string v1, "ACCOUNT_NUMBER"

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/detail/DetailView;->e(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailRow;

    iget-object v1, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    const-string v2, "BILLPAY_MESSAGE"

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/view/detail/DetailView;->e(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/view/detail/DetailRow;

    iget-object v2, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;->c:Lcom/chase/sig/android/domain/Payee;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/Payee;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/chase/sig/android/util/s;->l(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/DetailRow;->k()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v1}, Lcom/chase/sig/android/view/detail/DetailRow;->k()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 208
    :goto_0
    return-void

    .line 207
    :cond_0
    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/DetailRow;->k()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v1}, Lcom/chase/sig/android/view/detail/DetailRow;->k()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic c(Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;)Lcom/chase/sig/android/service/billpay/BillPayPayeeEditResponse;
    .locals 1
    .parameter

    .prologue
    .line 27
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;->b:Lcom/chase/sig/android/service/billpay/BillPayPayeeEditResponse;

    return-object v0
.end method


# virtual methods
.method protected final a(I)V
    .locals 3
    .parameter

    .prologue
    .line 179
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ai;->a(I)V

    .line 180
    packed-switch p1, :pswitch_data_0

    .line 191
    :goto_0
    return-void

    .line 182
    :pswitch_0
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 183
    const-string v2, "errors"

    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;->d:Ljava/util/List;

    check-cast v0, Ljava/io/Serializable;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 185
    const/16 v0, 0x19

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;->setResult(ILandroid/content/Intent;)V

    .line 186
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;->finish()V

    goto :goto_0

    .line 189
    :pswitch_1
    const/16 v0, 0x14

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;->setResult(I)V

    .line 190
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;->finish()V

    goto :goto_0

    .line 180
    :pswitch_data_0
    .packed-switch 0xc
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 8
    .parameter

    .prologue
    const/4 v7, 0x0

    const/4 v3, 0x0

    .line 58
    const v0, 0x7f07018c

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;->setTitle(I)V

    .line 59
    const v0, 0x7f030014

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;->b(I)V

    .line 61
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 64
    const/high16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 65
    invoke-static {v0}, Lcom/chase/sig/android/c;->a(Landroid/content/Intent;)V

    .line 67
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 69
    const-string v1, "payee"

    invoke-static {p1, v1}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 70
    const-string v0, "payee"

    invoke-static {p1, v0, v3}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/Payee;

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;->c:Lcom/chase/sig/android/domain/Payee;

    .line 78
    :goto_0
    const-string v0, "edit_payee_response"

    invoke-static {p1, v0, v3}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/service/billpay/BillPayPayeeEditResponse;

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;->b:Lcom/chase/sig/android/service/billpay/BillPayPayeeEditResponse;

    .line 81
    const v0, 0x7f09000f

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    .line 83
    const v0, 0x7f09004a

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/chase/sig/android/activity/bk;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/bk;-><init>(Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f090052

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lcom/chase/sig/android/activity/bm;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/bm;-><init>(Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 85
    iget-object v1, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    const/16 v0, 0xa

    new-array v2, v0, [Lcom/chase/sig/android/view/detail/a;

    new-instance v0, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v3, "Payee name"

    iget-object v4, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;->c:Lcom/chase/sig/android/domain/Payee;

    invoke-virtual {v4}, Lcom/chase/sig/android/domain/Payee;->i()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v3, v4}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v0, v2, v7

    const/4 v0, 0x1

    new-instance v3, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v4, "Payee nickname"

    iget-object v5, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;->c:Lcom/chase/sig/android/domain/Payee;

    invoke-virtual {v5}, Lcom/chase/sig/android/domain/Payee;->h()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "PAYEE_NICKNAME"

    iput-object v4, v3, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    aput-object v3, v2, v0

    const/4 v0, 0x2

    new-instance v3, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v4, "Account number"

    iget-object v5, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;->c:Lcom/chase/sig/android/domain/Payee;

    invoke-virtual {v5}, Lcom/chase/sig/android/domain/Payee;->d()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "ACCOUNT_NUMBER"

    iput-object v4, v3, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    aput-object v3, v2, v0

    const/4 v0, 0x3

    new-instance v3, Lcom/chase/sig/android/view/detail/b;

    const-string v4, "Payee address"

    iget-object v5, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;->c:Lcom/chase/sig/android/domain/Payee;

    invoke-virtual {v5}, Lcom/chase/sig/android/domain/Payee;->o()Lcom/chase/sig/android/domain/PayeeAddress;

    move-result-object v5

    invoke-virtual {v5}, Lcom/chase/sig/android/domain/PayeeAddress;->a()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;->c:Lcom/chase/sig/android/domain/Payee;

    invoke-virtual {v6}, Lcom/chase/sig/android/domain/Payee;->o()Lcom/chase/sig/android/domain/PayeeAddress;

    move-result-object v6

    invoke-virtual {v6}, Lcom/chase/sig/android/domain/PayeeAddress;->b()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/chase/sig/android/util/s;->s(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v4, v5, v6}, Lcom/chase/sig/android/view/detail/b;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "ADDRESS"

    iput-object v4, v3, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    aput-object v3, v2, v0

    const/4 v0, 0x4

    new-instance v3, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v4, "City"

    iget-object v5, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;->c:Lcom/chase/sig/android/domain/Payee;

    invoke-virtual {v5}, Lcom/chase/sig/android/domain/Payee;->o()Lcom/chase/sig/android/domain/PayeeAddress;

    move-result-object v5

    invoke-virtual {v5}, Lcom/chase/sig/android/domain/PayeeAddress;->c()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "CITY"

    iput-object v4, v3, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    aput-object v3, v2, v0

    const/4 v0, 0x5

    new-instance v3, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v4, "State"

    iget-object v5, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;->c:Lcom/chase/sig/android/domain/Payee;

    invoke-virtual {v5}, Lcom/chase/sig/android/domain/Payee;->o()Lcom/chase/sig/android/domain/PayeeAddress;

    move-result-object v5

    invoke-virtual {v5}, Lcom/chase/sig/android/domain/PayeeAddress;->e()Lcom/chase/sig/android/domain/State;

    move-result-object v5

    invoke-virtual {v5}, Lcom/chase/sig/android/domain/State;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "STATE"

    iput-object v4, v3, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    aput-object v3, v2, v0

    const/4 v0, 0x6

    new-instance v3, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v4, "ZIP code"

    iget-object v5, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;->c:Lcom/chase/sig/android/domain/Payee;

    invoke-virtual {v5}, Lcom/chase/sig/android/domain/Payee;->o()Lcom/chase/sig/android/domain/PayeeAddress;

    move-result-object v5

    invoke-virtual {v5}, Lcom/chase/sig/android/domain/PayeeAddress;->d()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "ZIP_CODE"

    iput-object v4, v3, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    aput-object v3, v2, v0

    const/4 v3, 0x7

    new-instance v0, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v4, "Phone number"

    iget-object v5, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;->c:Lcom/chase/sig/android/domain/Payee;

    invoke-virtual {v5}, Lcom/chase/sig/android/domain/Payee;->p()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v4, v5}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "PHONE_NUMBER"

    iput-object v4, v0, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailRow;

    iget-object v4, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;->c:Lcom/chase/sig/android/domain/Payee;

    invoke-virtual {v4}, Lcom/chase/sig/android/domain/Payee;->p()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/chase/sig/android/util/s;->l(Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, v0, Lcom/chase/sig/android/view/detail/a;->j:Z

    aput-object v0, v2, v3

    const/16 v3, 0x8

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Message"

    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;->c:Lcom/chase/sig/android/domain/Payee;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Payee;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->l(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, ""

    :goto_1
    invoke-direct {v4, v5, v0}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "BILLPAY_MESSAGE"

    iput-object v0, v4, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    aput-object v4, v2, v3

    const/16 v0, 0x9

    new-instance v3, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v4, "Delivery method"

    const-string v5, "--"

    invoke-direct {v3, v4, v5}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "DELIVERY_METHOD"

    iput-object v4, v3, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    aput-object v3, v2, v0

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/view/detail/DetailView;->setRows([Lcom/chase/sig/android/view/detail/a;)V

    .line 107
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;->b:Lcom/chase/sig/android/service/billpay/BillPayPayeeEditResponse;

    if-nez v0, :cond_3

    .line 108
    const-class v0, Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity$b;

    new-array v1, v7, [Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 112
    :goto_2
    return-void

    .line 71
    :cond_0
    const-string v1, "payee"

    invoke-static {v0, v1}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 72
    const-string v1, "payee"

    invoke-static {v0, v1, v3}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/Payee;

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;->c:Lcom/chase/sig/android/domain/Payee;

    goto/16 :goto_0

    .line 75
    :cond_1
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;->finish()V

    goto/16 :goto_0

    .line 85
    :cond_2
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;->c:Lcom/chase/sig/android/domain/Payee;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Payee;->g()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 110
    :cond_3
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;->b:Lcom/chase/sig/android/service/billpay/BillPayPayeeEditResponse;

    invoke-direct {p0, v0}, Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;->b(Lcom/chase/sig/android/service/billpay/BillPayPayeeEditResponse;)V

    goto :goto_2
.end method

.method public final a(Lcom/chase/sig/android/service/billpay/BillPayPayeeEditResponse;)V
    .locals 2
    .parameter

    .prologue
    .line 247
    const-string v0, "Active"

    invoke-virtual {p1}, Lcom/chase/sig/android/service/billpay/BillPayPayeeEditResponse;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 248
    invoke-static {}, Lcom/chase/sig/android/c;->a()V

    .line 249
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/chase/sig/android/activity/BillPayHomeActivity;->a(Landroid/content/Context;Z)V

    .line 251
    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 145
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ai;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 146
    const-string v0, "edit_payee_response"

    iget-object v1, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;->b:Lcom/chase/sig/android/service/billpay/BillPayPayeeEditResponse;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 147
    const-string v0, "payee"

    iget-object v1, p0, Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;->c:Lcom/chase/sig/android/domain/Payee;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 148
    return-void
.end method
