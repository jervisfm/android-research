.class public Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$e;


# instance fields
.field a:Landroid/hardware/Camera$ShutterCallback;

.field b:Landroid/hardware/Camera$PictureCallback;

.field c:Landroid/hardware/Camera$PictureCallback;

.field private d:Landroid/widget/ImageView;

.field private k:Landroid/widget/FrameLayout;

.field private l:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    .line 125
    new-instance v0, Lcom/chase/sig/android/activity/bq;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/activity/bq;-><init>(Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;)V

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;->a:Landroid/hardware/Camera$ShutterCallback;

    .line 132
    new-instance v0, Lcom/chase/sig/android/activity/br;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/activity/br;-><init>(Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;)V

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;->b:Landroid/hardware/Camera$PictureCallback;

    .line 139
    new-instance v0, Lcom/chase/sig/android/activity/bs;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/activity/bs;-><init>(Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;)V

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;->c:Landroid/hardware/Camera$PictureCallback;

    .line 30
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;)Landroid/widget/ImageView;
    .locals 1
    .parameter

    .prologue
    .line 30
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;->d:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic a([B)[B
    .locals 5
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 30
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    const/16 v1, 0x4000

    new-array v1, v1, [B

    iput-object v1, v0, Landroid/graphics/BitmapFactory$Options;->inTempStorage:[B

    sget-object v1, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object v1, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    iput-boolean v4, v0, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    array-length v1, p0

    invoke-static {p0, v3, v1, v0}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v3, 0x32

    invoke-virtual {v0, v2, v3, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    return-object v1
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;)Landroid/widget/FrameLayout;
    .locals 1
    .parameter

    .prologue
    .line 30
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;->k:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic c(Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 30
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;->l:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 4
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 45
    new-instance v2, Lcom/chase/sig/android/activity/CapturePreview;

    invoke-direct {v2, p0, v3}, Lcom/chase/sig/android/activity/CapturePreview;-><init>(Landroid/content/Context;B)V

    .line 47
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 48
    if-eqz v0, :cond_0

    .line 49
    const-string v1, "selectedAccountId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;->l:Ljava/lang/String;

    .line 52
    :cond_0
    const v0, 0x7f090032

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 53
    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 55
    const v0, 0x7f09003f

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;->k:Landroid/widget/FrameLayout;

    .line 56
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;->k:Landroid/widget/FrameLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 58
    const v0, 0x7f0701a6

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    .line 60
    const v0, 0x7f09003e

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 62
    const v0, 0x7f090036

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 63
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 65
    const v1, 0x7f09003c

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;->d:Landroid/widget/ImageView;

    .line 66
    iget-object v1, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;->d:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 68
    new-instance v1, Lcom/chase/sig/android/activity/bn;

    invoke-direct {v1, p0, v0}, Lcom/chase/sig/android/activity/bn;-><init>(Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;Landroid/widget/Button;)V

    .line 90
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 92
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;->d:Landroid/widget/ImageView;

    new-instance v1, Lcom/chase/sig/android/activity/bo;

    invoke-direct {v1, p0, v2}, Lcom/chase/sig/android/activity/bo;-><init>(Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;Lcom/chase/sig/android/activity/CapturePreview;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 115
    return-void
.end method

.method protected final a_()V
    .locals 1

    .prologue
    .line 39
    const v0, 0x7f03000f

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;->setContentView(I)V

    .line 40
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 179
    const/16 v0, 0x1b

    if-ne p1, v0, :cond_1

    .line 180
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 181
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;->d:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->performClick()Z

    .line 184
    :cond_0
    const/4 v0, 0x1

    .line 186
    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1, p2}, Lcom/chase/sig/android/activity/ai;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onResume()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 119
    invoke-super {p0}, Lcom/chase/sig/android/activity/ai;->onResume()V

    .line 121
    const v0, 0x7f090038

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 122
    const v0, 0x7f090037

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 123
    return-void
.end method
