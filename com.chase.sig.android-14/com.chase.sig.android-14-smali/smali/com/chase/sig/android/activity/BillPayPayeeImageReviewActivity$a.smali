.class public Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$a;
.super Lcom/chase/sig/android/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/d",
        "<",
        "Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;",
        "[B",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/service/billpay/BillPayPayeeSearchResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 320
    invoke-direct {p0}, Lcom/chase/sig/android/d;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 320
    check-cast p1, [[B

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;->A()Lcom/chase/sig/android/service/n;

    invoke-static {}, Lcom/chase/sig/android/service/n;->g()Lcom/chase/sig/android/service/billpay/b;

    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-static {v0}, Lcom/chase/sig/android/service/billpay/b;->a([B)Lcom/chase/sig/android/service/billpay/BillPayPayeeSearchResponse;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 3
    .parameter

    .prologue
    .line 320
    check-cast p1, Lcom/chase/sig/android/service/billpay/BillPayPayeeSearchResponse;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/billpay/BillPayPayeeSearchResponse;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/chase/sig/android/service/billpay/BillPayPayeeSearchResponse;->g()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/service/IServiceError;

    const-string v1, "retakeFlow"

    invoke-virtual {p1}, Lcom/chase/sig/android/service/billpay/BillPayPayeeSearchResponse;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v1, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;

    invoke-interface {v0}, Lcom/chase/sig/android/service/IServiceError;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;->a(Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;->showDialog(I)V

    :goto_0
    return-void

    :cond_0
    const-string v1, "manualFlow"

    invoke-virtual {p1}, Lcom/chase/sig/android/service/billpay/BillPayPayeeSearchResponse;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v1, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;

    const/4 v2, 0x2

    invoke-virtual {v1, v2, v0}, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;->a(ILcom/chase/sig/android/service/IServiceError;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/billpay/BillPayPayeeSearchResponse;->b()Lcom/chase/sig/android/domain/ImageCaptureData;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;->a(Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;Lcom/chase/sig/android/domain/ImageCaptureData;)Lcom/chase/sig/android/domain/ImageCaptureData;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/billpay/BillPayPayeeSearchResponse;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;->c(Ljava/util/List;)V

    goto :goto_0

    :cond_2
    new-instance v1, Landroid/content/Intent;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    const-class v2, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "image_search_results"

    invoke-virtual {p1}, Lcom/chase/sig/android/service/billpay/BillPayPayeeSearchResponse;->c()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/io/Serializable;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v0, "scheduled_amount_due"

    invoke-virtual {p1}, Lcom/chase/sig/android/service/billpay/BillPayPayeeSearchResponse;->b()Lcom/chase/sig/android/domain/ImageCaptureData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/ImageCaptureData;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "scheduled_due_date"

    invoke-virtual {p1}, Lcom/chase/sig/android/service/billpay/BillPayPayeeSearchResponse;->b()Lcom/chase/sig/android/domain/ImageCaptureData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/ImageCaptureData;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "selectedAccountId"

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;->c(Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "image_capture_data"

    invoke-virtual {p1}, Lcom/chase/sig/android/service/billpay/BillPayPayeeSearchResponse;->b()Lcom/chase/sig/android/domain/ImageCaptureData;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
