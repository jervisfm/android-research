.class final Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;
.super Landroid/view/View;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation


# instance fields
.field a:Z

.field private b:Landroid/graphics/Bitmap;

.field private final c:Landroid/graphics/Paint;

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 225
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 210
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->c:Landroid/graphics/Paint;

    .line 212
    iput v1, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->d:I

    .line 213
    iput v1, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->e:I

    .line 226
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 309
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 310
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->b:Landroid/graphics/Bitmap;

    .line 311
    return-void
.end method

.method public final a(FF)V
    .locals 2
    .parameter
    .parameter

    .prologue
    const/high16 v1, 0x4080

    .line 297
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 298
    iget v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->d:I

    int-to-float v0, v0

    add-float/2addr v0, p1

    float-to-int v0, v0

    iput v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->d:I

    .line 301
    :cond_0
    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    .line 302
    iget v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->e:I

    int-to-float v0, v0

    add-float/2addr v0, p2

    float-to-int v0, v0

    iput v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->e:I

    .line 305
    :cond_1
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->invalidate()V

    .line 306
    return-void
.end method

.method public final a(Landroid/graphics/Bitmap;)V
    .locals 1
    .parameter

    .prologue
    .line 314
    iput-object p1, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->b:Landroid/graphics/Bitmap;

    .line 315
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->h:I

    .line 316
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->i:I

    .line 317
    return-void
.end method

.method public final a(Z)V
    .locals 0
    .parameter

    .prologue
    .line 229
    iput-boolean p1, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->a:Z

    .line 230
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->invalidate()V

    .line 231
    return-void
.end method

.method protected final onDraw(Landroid/graphics/Canvas;)V
    .locals 8
    .parameter

    .prologue
    const/4 v7, 0x0

    const/4 v1, 0x0

    .line 242
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->b:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    if-gtz v0, :cond_1

    .line 272
    :cond_0
    :goto_0
    return-void

    .line 246
    :cond_1
    iget-boolean v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->a:Z

    if-eqz v0, :cond_2

    .line 248
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->getWidth()I

    move-result v2

    int-to-float v2, v2

    int-to-float v5, v3

    div-float/2addr v2, v5

    int-to-float v0, v0

    int-to-float v5, v4

    div-float/2addr v0, v5

    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    invoke-virtual {v5, v2, v0}, Landroid/graphics/Matrix;->postScale(FF)Z

    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->b:Landroid/graphics/Bitmap;

    const/4 v6, 0x1

    move v2, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v1, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v7, v7, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 252
    :cond_2
    iget v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->h:I

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->getWidth()I

    move-result v2

    sub-int/2addr v0, v2

    iput v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->f:I

    .line 253
    iget v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->i:I

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->getHeight()I

    move-result v2

    sub-int/2addr v0, v2

    iput v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->g:I

    .line 255
    iget v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->f:I

    if-gez v0, :cond_3

    move v0, v1

    :goto_1
    iput v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->f:I

    .line 256
    iget v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->g:I

    if-gez v0, :cond_4

    move v0, v1

    :goto_2
    iput v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->g:I

    .line 258
    iget v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->d:I

    iget v2, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->f:I

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->d:I

    .line 259
    iget v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->d:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->d:I

    .line 261
    iget v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->e:I

    iget v2, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->g:I

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->e:I

    .line 262
    iget v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->e:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->e:I

    .line 264
    iget v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->h:I

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->getWidth()I

    move-result v2

    if-ge v0, v2, :cond_5

    iget v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->h:I

    .line 265
    :goto_3
    iget v2, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->i:I

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->getHeight()I

    move-result v3

    if-ge v2, v3, :cond_6

    iget v2, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->i:I

    .line 266
    :goto_4
    new-instance v3, Landroid/graphics/Rect;

    iget v4, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->d:I

    iget v5, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->e:I

    iget v6, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->d:I

    add-int/2addr v0, v6

    iget v6, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->e:I

    add-int/2addr v2, v6

    invoke-direct {v3, v4, v5, v0, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 268
    new-instance v0, Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v2

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v4

    invoke-direct {v0, v1, v1, v2, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 270
    iget-object v1, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->b:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 255
    :cond_3
    iget v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->f:I

    goto :goto_1

    .line 256
    :cond_4
    iget v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->g:I

    goto :goto_2

    .line 264
    :cond_5
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->getWidth()I

    move-result v0

    goto :goto_3

    .line 265
    :cond_6
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->getHeight()I

    move-result v2

    goto :goto_4
.end method
