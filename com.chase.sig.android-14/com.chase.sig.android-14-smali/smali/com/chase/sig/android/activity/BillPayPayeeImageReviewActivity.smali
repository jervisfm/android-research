.class public Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$e;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$a;,
        Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;
    }
.end annotation


# instance fields
.field private a:Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;

.field private b:Z

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/TextView;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Lcom/chase/sig/android/domain/ImageCaptureData;

.field private n:[B

.field private final o:Landroid/view/GestureDetector$OnGestureListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    .line 102
    new-instance v0, Lcom/chase/sig/android/activity/bu;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/activity/bu;-><init>(Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;)V

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;->o:Landroid/view/GestureDetector$OnGestureListener;

    .line 320
    return-void
.end method

.method private static a([B)Landroid/graphics/Bitmap;
    .locals 5
    .parameter

    .prologue
    const/4 v0, 0x0

    const/4 v4, -0x1

    .line 180
    :try_start_0
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 181
    const/4 v2, 0x1

    iput-boolean v2, v1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 182
    const/4 v2, 0x0

    array-length v3, p0

    invoke-static {p0, v2, v3, v1}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 183
    iget-boolean v2, v1, Landroid/graphics/BitmapFactory$Options;->mCancel:Z

    if-nez v2, :cond_0

    iget v2, v1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    if-eq v2, v4, :cond_0

    iget v2, v1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    if-ne v2, v4, :cond_1

    .line 193
    :cond_0
    :goto_0
    return-object v0

    .line 186
    :cond_1
    const/4 v2, 0x0

    iput-boolean v2, v1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 188
    const/4 v2, 0x0

    iput-boolean v2, v1, Landroid/graphics/BitmapFactory$Options;->inDither:Z

    .line 189
    const/4 v2, 0x1

    iput-boolean v2, v1, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    .line 190
    sget-object v2, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object v2, v1, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 191
    const/4 v2, 0x0

    array-length v3, p0

    invoke-static {p0, v2, v3, v1}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 193
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;Lcom/chase/sig/android/domain/ImageCaptureData;)Lcom/chase/sig/android/domain/ImageCaptureData;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 35
    iput-object p1, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;->m:Lcom/chase/sig/android/domain/ImageCaptureData;

    return-object p1
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 35
    iput-object p1, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;->l:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;)V
    .locals 2
    .parameter

    .prologue
    .line 35
    iget-boolean v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;->b:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;->b:Z

    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;->a:Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;

    iget-boolean v1, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;->b:Z

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->a(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;)Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;
    .locals 1
    .parameter

    .prologue
    .line 35
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;->a:Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;

    return-object v0
.end method

.method static synthetic c(Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 35
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;->k:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;)[B
    .locals 1
    .parameter

    .prologue
    .line 35
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;->n:[B

    return-object v0
.end method


# virtual methods
.method protected final a(I)V
    .locals 3
    .parameter

    .prologue
    .line 365
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ai;->a(I)V

    .line 367
    const/4 v0, 0x2

    if-ne v0, p1, :cond_0

    .line 368
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 369
    const-string v1, "selectedAccountId"

    iget-object v2, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 370
    const-string v1, "image_capture_data"

    iget-object v2, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;->m:Lcom/chase/sig/android/domain/ImageCaptureData;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 371
    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;->startActivity(Landroid/content/Intent;)V

    .line 373
    :cond_0
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 5
    .parameter

    .prologue
    const/4 v4, -0x1

    .line 58
    const v0, 0x7f03000f

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;->setContentView(I)V

    .line 60
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 61
    if-eqz v0, :cond_0

    .line 62
    const-string v1, "selectedAccountId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;->k:Ljava/lang/String;

    .line 65
    :cond_0
    const v0, 0x7f090034

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;->c:Landroid/widget/TextView;

    .line 66
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;->c:Landroid/widget/TextView;

    const v1, 0x7f0701a5

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 68
    const v0, 0x7f09003a

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;->d:Landroid/widget/TextView;

    .line 69
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;->d:Landroid/widget/TextView;

    const v1, 0x7f0701a7

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 70
    const v0, 0x7f09003b

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 71
    const v0, 0x7f09003f

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 72
    const v0, 0x7f090031

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 75
    new-instance v1, Landroid/view/GestureDetector;

    iget-object v2, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;->o:Landroid/view/GestureDetector$OnGestureListener;

    invoke-direct {v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/view/GestureDetector$OnGestureListener;)V

    .line 77
    new-instance v2, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;

    invoke-direct {v2, p0}, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;->a:Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;

    .line 78
    iget-object v2, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;->a:Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->a(Z)V

    .line 80
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v2, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 82
    iget-object v3, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;->a:Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;

    invoke-virtual {v3, v2}, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 84
    iget-object v2, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;->a:Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;

    new-instance v3, Lcom/chase/sig/android/activity/bt;

    invoke-direct {v3, p0, v1}, Lcom/chase/sig/android/activity/bt;-><init>(Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;Landroid/view/GestureDetector;)V

    invoke-virtual {v2, v3}, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 92
    iget-object v1, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;->a:Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;I)V

    .line 94
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 4
    .parameter

    .prologue
    .line 377
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ai;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    .line 378
    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    .line 379
    new-instance v0, Lcom/chase/sig/android/view/k$a;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/view/k$a;-><init>(Landroid/content/Context;)V

    .line 380
    iget-object v1, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/k$a;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/k$a;

    move-result-object v1

    const v2, 0x7f070171

    new-instance v3, Lcom/chase/sig/android/activity/by;

    invoke-direct {v3, p0}, Lcom/chase/sig/android/activity/by;-><init>(Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;)V

    invoke-virtual {v1, v2, v3}, Lcom/chase/sig/android/view/k$a;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    move-result-object v1

    const v2, 0x7f070172

    new-instance v3, Lcom/chase/sig/android/activity/bx;

    invoke-direct {v3, p0}, Lcom/chase/sig/android/activity/bx;-><init>(Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;)V

    invoke-virtual {v1, v2, v3}, Lcom/chase/sig/android/view/k$a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    .line 403
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/k$a;->d(I)Lcom/chase/sig/android/view/k;

    move-result-object v0

    .line 405
    :cond_0
    return-object v0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 200
    invoke-super {p0}, Lcom/chase/sig/android/activity/ai;->onDestroy()V

    .line 201
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;->a:Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->a()V

    .line 202
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;->a:Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;

    .line 203
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 140
    invoke-super {p0}, Lcom/chase/sig/android/activity/ai;->onResume()V

    .line 142
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "image_data"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;->n:[B

    .line 143
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;->n:[B

    invoke-static {v0}, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;->a([B)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 145
    const v0, 0x7f09001f

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f090037

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    new-instance v2, Lcom/chase/sig/android/activity/bv;

    invoke-direct {v2, p0}, Lcom/chase/sig/android/activity/bv;-><init>(Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f090038

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    new-instance v2, Lcom/chase/sig/android/activity/bw;

    invoke-direct {v2, p0}, Lcom/chase/sig/android/activity/bw;-><init>(Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 146
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;->a:Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity$b;->a(Landroid/graphics/Bitmap;)V

    .line 147
    return-void
.end method
