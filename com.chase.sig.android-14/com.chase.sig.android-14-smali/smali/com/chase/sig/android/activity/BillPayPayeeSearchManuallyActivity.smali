.class public Lcom/chase/sig/android/activity/BillPayPayeeSearchManuallyActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$e;


# instance fields
.field private a:Lcom/chase/sig/android/view/detail/DetailView;

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    .line 20
    const-string v0, ""

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeSearchManuallyActivity;->b:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/BillPayPayeeSearchManuallyActivity;)V
    .locals 4
    .parameter

    .prologue
    .line 17
    new-instance v0, Lcom/chase/sig/android/view/detail/i;

    iget-object v1, p0, Lcom/chase/sig/android/activity/BillPayPayeeSearchManuallyActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    invoke-direct {v0, v1}, Lcom/chase/sig/android/view/detail/i;-><init>(Lcom/chase/sig/android/view/detail/DetailView;)V

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/i;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeSearchManuallyActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    const-string v1, "ACCOUNT_NUMBER"

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/detail/DetailView;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->l(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeSearchManuallyActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    const-string v1, "PAYEE"

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/detail/DetailView;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/chase/sig/android/activity/BillPayPayeeSearchManuallyActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    const-string v2, "ZIP_CODE"

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/view/detail/DetailView;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v2}, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "selectedAccountId"

    iget-object v2, p0, Lcom/chase/sig/android/activity/BillPayPayeeSearchManuallyActivity;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayPayeeSearchManuallyActivity;->startActivity(Landroid/content/Intent;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "payee_name"

    iget-object v2, p0, Lcom/chase/sig/android/activity/BillPayPayeeSearchManuallyActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    const-string v3, "PAYEE"

    invoke-virtual {v2, v3}, Lcom/chase/sig/android/view/detail/DetailView;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "zip_code"

    iget-object v2, p0, Lcom/chase/sig/android/activity/BillPayPayeeSearchManuallyActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    const-string v3, "ZIP_CODE"

    invoke-virtual {v2, v3}, Lcom/chase/sig/android/view/detail/DetailView;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "-"

    invoke-static {v2, v3}, Lcom/chase/sig/android/util/s;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "selectedAccountId"

    iget-object v2, p0, Lcom/chase/sig/android/activity/BillPayPayeeSearchManuallyActivity;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "payee_account_number"

    iget-object v2, p0, Lcom/chase/sig/android/activity/BillPayPayeeSearchManuallyActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    const-string v3, "ACCOUNT_NUMBER"

    invoke-virtual {v2, v3}, Lcom/chase/sig/android/view/detail/DetailView;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayPayeeSearchManuallyActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 6
    .parameter

    .prologue
    const v5, 0x7f09004b

    const/4 v4, 0x0

    .line 25
    const v0, 0x7f07018b

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayPayeeSearchManuallyActivity;->setTitle(I)V

    .line 26
    const v0, 0x7f030016

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayPayeeSearchManuallyActivity;->b(I)V

    .line 27
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayPayeeSearchManuallyActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "selectedAccountId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeSearchManuallyActivity;->b:Ljava/lang/String;

    .line 31
    const v0, 0x7f090055

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayPayeeSearchManuallyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeSearchManuallyActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    .line 33
    new-instance v0, Lcom/chase/sig/android/activity/bd;

    invoke-direct {v0}, Lcom/chase/sig/android/activity/bd;-><init>()V

    .line 34
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeSearchManuallyActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    const/4 v1, 0x3

    new-array v1, v1, [Lcom/chase/sig/android/view/detail/a;

    const/4 v2, 0x0

    invoke-static {v4}, Lcom/chase/sig/android/activity/bd;->i(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/l;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {v4}, Lcom/chase/sig/android/activity/bd;->c(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/l;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-static {v4}, Lcom/chase/sig/android/activity/bd;->g(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/l;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/detail/DetailView;->setRows([Lcom/chase/sig/android/view/detail/a;)V

    .line 40
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeSearchManuallyActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    invoke-virtual {p0, v0, v5}, Lcom/chase/sig/android/activity/BillPayPayeeSearchManuallyActivity;->a(Lcom/chase/sig/android/view/detail/DetailView;I)V

    .line 42
    const v0, 0x7f09004a

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayPayeeSearchManuallyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/chase/sig/android/activity/bz;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/bz;-><init>(Lcom/chase/sig/android/activity/BillPayPayeeSearchManuallyActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 50
    invoke-virtual {p0, v5}, Lcom/chase/sig/android/activity/BillPayPayeeSearchManuallyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/chase/sig/android/activity/ca;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/ca;-><init>(Lcom/chase/sig/android/activity/BillPayPayeeSearchManuallyActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 57
    return-void
.end method
