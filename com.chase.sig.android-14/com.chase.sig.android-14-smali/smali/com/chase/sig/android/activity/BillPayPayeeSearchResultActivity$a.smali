.class public Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity$a;
.super Lcom/chase/sig/android/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/d",
        "<",
        "Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/service/billpay/BillPayPayeeSearchResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 185
    invoke-direct {p0}, Lcom/chase/sig/android/d;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .parameter

    .prologue
    .line 185
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->A()Lcom/chase/sig/android/service/n;

    invoke-static {}, Lcom/chase/sig/android/service/n;->g()Lcom/chase/sig/android/service/billpay/b;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->g(Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->h(Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->c(Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/chase/sig/android/service/billpay/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/service/billpay/BillPayPayeeSearchResponse;

    move-result-object v0

    return-object v0
.end method

.method protected final bridge synthetic a(Ljava/lang/Object;)V
    .locals 1
    .parameter

    .prologue
    .line 185
    check-cast p1, Lcom/chase/sig/android/service/billpay/BillPayPayeeSearchResponse;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;

    invoke-static {v0, p1}, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->a(Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;Lcom/chase/sig/android/service/billpay/BillPayPayeeSearchResponse;)V

    return-void
.end method
