.class public Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$e;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity$a;
    }
.end annotation


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/MerchantPayee;",
            ">;"
        }
    .end annotation
.end field

.field private b:Landroid/widget/ListView;

.field private c:Landroid/widget/SimpleAdapter;

.field private d:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private p:Lcom/chase/sig/android/domain/ImageCaptureData;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    .line 185
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;)Landroid/widget/SimpleAdapter;
    .locals 1
    .parameter

    .prologue
    .line 30
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->c:Landroid/widget/SimpleAdapter;

    return-object v0
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;Lcom/chase/sig/android/service/billpay/BillPayPayeeSearchResponse;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 30
    invoke-virtual {p1}, Lcom/chase/sig/android/service/billpay/BillPayPayeeSearchResponse;->e()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/chase/sig/android/service/billpay/BillPayPayeeSearchResponse;->c()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->a:Ljava/util/List;

    invoke-direct {p0}, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->d()V

    :goto_0
    return-void

    :cond_0
    const-string v0, "manualFlow"

    invoke-virtual {p1}, Lcom/chase/sig/android/service/billpay/BillPayPayeeSearchResponse;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-virtual {p1}, Lcom/chase/sig/android/service/billpay/BillPayPayeeSearchResponse;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->a(ILjava/util/List;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/chase/sig/android/service/billpay/BillPayPayeeSearchResponse;->g()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->b(Ljava/util/List;)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 30
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->a:Ljava/util/List;

    return-object v0
.end method

.method static synthetic c(Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 30
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->l:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 30
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->m:Ljava/lang/String;

    return-object v0
.end method

.method private d()V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x0

    .line 232
    const v0, 0x7f090056

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f090058

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f090059

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 234
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 237
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/MerchantPayee;

    .line 238
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 239
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/MerchantPayee;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/MerchantPayee;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/MerchantPayee;->f()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/MerchantPayee;->g()Lcom/chase/sig/android/domain/State;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/MerchantPayee;->h()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 242
    const-string v5, "payeename"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/MerchantPayee;->b()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v3, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 243
    const-string v5, "optionId"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/MerchantPayee;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 244
    const-string v0, "address"

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 246
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 249
    :cond_0
    new-array v4, v8, [Ljava/lang/String;

    const-string v0, "payeename"

    aput-object v0, v4, v7

    const/4 v0, 0x1

    const-string v1, "address"

    aput-object v1, v4, v0

    .line 253
    new-array v5, v8, [I

    fill-array-data v5, :array_0

    .line 257
    new-instance v0, Landroid/widget/SimpleAdapter;

    const v3, 0x7f030015

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Landroid/widget/SimpleAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->c:Landroid/widget/SimpleAdapter;

    .line 259
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->b:Landroid/widget/ListView;

    new-instance v1, Lcom/chase/sig/android/view/af;

    iget-object v2, p0, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->c:Landroid/widget/SimpleAdapter;

    invoke-direct {v1, v2}, Lcom/chase/sig/android/view/af;-><init>(Landroid/widget/ListAdapter;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 260
    return-void

    .line 253
    :array_0
    .array-data 0x4
        0x53t 0x0t 0x9t 0x7ft
        0x54t 0x0t 0x9t 0x7ft
    .end array-data
.end method

.method static synthetic e(Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 30
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->o:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 30
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->n:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 30
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->k:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h(Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 30
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->d:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method protected final a(I)V
    .locals 3
    .parameter

    .prologue
    .line 218
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ai;->a(I)V

    .line 220
    if-nez p1, :cond_0

    .line 221
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->k:Ljava/lang/String;

    iget-object v1, p0, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->d:Ljava/lang/String;

    iget-object v2, p0, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->l:Ljava/lang/String;

    invoke-static {p0, v0, v1, v2}, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 223
    const-string v1, "selectedAccountId"

    iget-object v2, p0, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->m:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 225
    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->startActivity(Landroid/content/Intent;)V

    .line 226
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->finish()V

    .line 228
    :cond_0
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 5
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 72
    const v0, 0x7f07018b

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->setTitle(I)V

    .line 73
    const v0, 0x7f030017

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->b(I)V

    .line 75
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 77
    const-string v0, "image_capture_data"

    invoke-static {v1, v0}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 78
    const-string v0, "image_capture_data"

    invoke-static {v1, v0, v2}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/ImageCaptureData;

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->p:Lcom/chase/sig/android/domain/ImageCaptureData;

    .line 80
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->p:Lcom/chase/sig/android/domain/ImageCaptureData;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/ImageCaptureData;->i()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->d:Ljava/lang/String;

    .line 81
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->p:Lcom/chase/sig/android/domain/ImageCaptureData;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/ImageCaptureData;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->k:Ljava/lang/String;

    .line 82
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->p:Lcom/chase/sig/android/domain/ImageCaptureData;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/ImageCaptureData;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->l:Ljava/lang/String;

    .line 83
    const-string v0, "selectedAccountId"

    invoke-static {v1, v0, v2}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->m:Ljava/lang/String;

    .line 85
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->p:Lcom/chase/sig/android/domain/ImageCaptureData;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/ImageCaptureData;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->n:Ljava/lang/String;

    .line 86
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->p:Lcom/chase/sig/android/domain/ImageCaptureData;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/ImageCaptureData;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->o:Ljava/lang/String;

    .line 98
    :goto_0
    const v0, 0x7f090056

    new-instance v2, Lcom/chase/sig/android/activity/a/f;

    const-class v3, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;

    invoke-direct {v2, p0, v3}, Lcom/chase/sig/android/activity/a/f;-><init>(Lcom/chase/sig/android/activity/eb;Ljava/lang/Class;)V

    iget-object v3, p0, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->p:Lcom/chase/sig/android/domain/ImageCaptureData;

    if-eqz v3, :cond_0

    const-string v3, "image_capture_data"

    iget-object v4, p0, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->p:Lcom/chase/sig/android/domain/ImageCaptureData;

    invoke-virtual {v2, v3, v4}, Lcom/chase/sig/android/activity/a/f;->a(Ljava/lang/String;Ljava/io/Serializable;)Lcom/chase/sig/android/activity/a/f;

    :cond_0
    const-string v3, "selectedAccountId"

    iget-object v4, p0, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->m:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/chase/sig/android/activity/a/f;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/activity/a/f;

    const-string v3, "payee_name"

    iget-object v4, p0, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->k:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/chase/sig/android/activity/a/f;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/activity/a/f;

    const-string v3, "payee_zip_code"

    iget-object v4, p0, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->d:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/chase/sig/android/activity/a/f;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/activity/a/f;

    const-string v3, "account_number"

    iget-object v4, p0, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->l:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/chase/sig/android/activity/a/f;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/activity/a/f;

    invoke-virtual {p0, v0, v2}, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 100
    const v0, 0x7f090059

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->b:Landroid/widget/ListView;

    .line 102
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->b:Landroid/widget/ListView;

    new-instance v2, Lcom/chase/sig/android/activity/cb;

    invoke-direct {v2, p0}, Lcom/chase/sig/android/activity/cb;-><init>(Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 132
    if-nez p1, :cond_3

    const-string v0, "image_search_results"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 133
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->e:Lcom/chase/sig/android/activity/mc;

    const-class v1, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity$a;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/mc;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/ae;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity$a;

    .line 135
    invoke-virtual {v0}, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity$a;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v1

    sget-object v2, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-eq v1, v2, :cond_1

    .line 136
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 153
    :cond_1
    :goto_1
    return-void

    .line 89
    :cond_2
    const-string v0, "zip_code"

    invoke-static {v1, v0, v2}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->d:Ljava/lang/String;

    .line 90
    const-string v0, "payee_name"

    invoke-static {v1, v0, v2}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->k:Ljava/lang/String;

    .line 91
    const-string v0, "payee_account_number"

    invoke-static {v1, v0, v2}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->l:Ljava/lang/String;

    .line 92
    const-string v0, "selectedAccountId"

    invoke-static {v1, v0, v2}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->m:Ljava/lang/String;

    .line 94
    const-string v0, "scheduled_due_date"

    invoke-static {v1, v0, v2}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->n:Ljava/lang/String;

    .line 95
    const-string v0, "scheduled_amount_due"

    invoke-static {v1, v0, v2}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->o:Ljava/lang/String;

    goto/16 :goto_0

    .line 140
    :cond_3
    const-string v0, "image_search_results"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "image_search_results"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 143
    const-string v0, "image_search_results"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->a:Ljava/util/List;

    .line 149
    :goto_2
    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->a:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 150
    invoke-direct {p0}, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->d()V

    goto :goto_1

    .line 146
    :cond_4
    const-string v0, "payees"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->a:Ljava/util/List;

    goto :goto_2
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .parameter
    .parameter
    .parameter

    .prologue
    .line 170
    const/16 v0, 0x19

    if-ne p1, v0, :cond_0

    .line 171
    invoke-static {}, Lcom/chase/sig/android/c;->a()V

    .line 172
    packed-switch p2, :pswitch_data_0

    .line 177
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2, p3}, Lcom/chase/sig/android/activity/ai;->onActivityResult(IILandroid/content/Intent;)V

    .line 183
    return-void

    .line 174
    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 175
    const/4 v1, 0x2

    invoke-virtual {v0, p3, v1}, Landroid/content/Intent;->fillIn(Landroid/content/Intent;I)I

    .line 176
    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 172
    nop

    :pswitch_data_0
    .packed-switch 0x19
        :pswitch_0
    .end packed-switch
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 157
    invoke-super {p0}, Lcom/chase/sig/android/activity/ai;->onResume()V

    .line 158
    invoke-static {}, Lcom/chase/sig/android/c;->a()V

    .line 159
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 163
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ai;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 165
    const-string v1, "payees"

    iget-object v0, p0, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->a:Ljava/util/List;

    check-cast v0, Ljava/io/Serializable;

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 166
    return-void
.end method
