.class public final enum Lcom/chase/sig/android/activity/CapturePreview$Orientation;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/CapturePreview;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Orientation"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/chase/sig/android/activity/CapturePreview$Orientation;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/chase/sig/android/activity/CapturePreview$Orientation;

.field public static final enum b:Lcom/chase/sig/android/activity/CapturePreview$Orientation;

.field private static final synthetic c:[Lcom/chase/sig/android/activity/CapturePreview$Orientation;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 25
    new-instance v0, Lcom/chase/sig/android/activity/CapturePreview$Orientation;

    const-string v1, "LANDSCAPE"

    invoke-direct {v0, v1, v2}, Lcom/chase/sig/android/activity/CapturePreview$Orientation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/chase/sig/android/activity/CapturePreview$Orientation;->a:Lcom/chase/sig/android/activity/CapturePreview$Orientation;

    new-instance v0, Lcom/chase/sig/android/activity/CapturePreview$Orientation;

    const-string v1, "PORTRAIT"

    invoke-direct {v0, v1, v3}, Lcom/chase/sig/android/activity/CapturePreview$Orientation;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/chase/sig/android/activity/CapturePreview$Orientation;->b:Lcom/chase/sig/android/activity/CapturePreview$Orientation;

    .line 24
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/chase/sig/android/activity/CapturePreview$Orientation;

    sget-object v1, Lcom/chase/sig/android/activity/CapturePreview$Orientation;->a:Lcom/chase/sig/android/activity/CapturePreview$Orientation;

    aput-object v1, v0, v2

    sget-object v1, Lcom/chase/sig/android/activity/CapturePreview$Orientation;->b:Lcom/chase/sig/android/activity/CapturePreview$Orientation;

    aput-object v1, v0, v3

    sput-object v0, Lcom/chase/sig/android/activity/CapturePreview$Orientation;->c:[Lcom/chase/sig/android/activity/CapturePreview$Orientation;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/chase/sig/android/activity/CapturePreview$Orientation;
    .locals 1
    .parameter

    .prologue
    .line 24
    const-class v0, Lcom/chase/sig/android/activity/CapturePreview$Orientation;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/activity/CapturePreview$Orientation;

    return-object v0
.end method

.method public static values()[Lcom/chase/sig/android/activity/CapturePreview$Orientation;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/chase/sig/android/activity/CapturePreview$Orientation;->c:[Lcom/chase/sig/android/activity/CapturePreview$Orientation;

    invoke-virtual {v0}, [Lcom/chase/sig/android/activity/CapturePreview$Orientation;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/chase/sig/android/activity/CapturePreview$Orientation;

    return-object v0
.end method
