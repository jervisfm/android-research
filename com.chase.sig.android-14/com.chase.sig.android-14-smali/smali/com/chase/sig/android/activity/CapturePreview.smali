.class public final Lcom/chase/sig/android/activity/CapturePreview;
.super Landroid/view/SurfaceView;
.source "SourceFile"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/CapturePreview$Orientation;
    }
.end annotation


# static fields
.field public static final a:Lcom/chase/sig/android/activity/CapturePreview$Orientation;


# instance fields
.field public b:Landroid/hardware/Camera;

.field private c:Lcom/chase/sig/android/activity/CapturePreview$Orientation;

.field private d:I

.field private e:Landroid/view/SurfaceHolder;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/chase/sig/android/activity/CapturePreview$Orientation;->a:Lcom/chase/sig/android/activity/CapturePreview$Orientation;

    sput-object v0, Lcom/chase/sig/android/activity/CapturePreview;->a:Lcom/chase/sig/android/activity/CapturePreview$Orientation;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .parameter

    .prologue
    .line 35
    const/16 v0, 0x1e

    sget-object v1, Lcom/chase/sig/android/activity/CapturePreview;->a:Lcom/chase/sig/android/activity/CapturePreview$Orientation;

    invoke-direct {p0, p1, v0, v1}, Lcom/chase/sig/android/activity/CapturePreview;-><init>(Landroid/content/Context;ILcom/chase/sig/android/activity/CapturePreview$Orientation;)V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;B)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 39
    const/16 v0, 0x32

    sget-object v1, Lcom/chase/sig/android/activity/CapturePreview;->a:Lcom/chase/sig/android/activity/CapturePreview$Orientation;

    invoke-direct {p0, p1, v0, v1}, Lcom/chase/sig/android/activity/CapturePreview;-><init>(Landroid/content/Context;ILcom/chase/sig/android/activity/CapturePreview$Orientation;)V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILcom/chase/sig/android/activity/CapturePreview$Orientation;)V
    .locals 2
    .parameter
    .parameter
    .parameter

    .prologue
    .line 43
    invoke-direct {p0, p1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    .line 29
    sget-object v0, Lcom/chase/sig/android/activity/CapturePreview$Orientation;->a:Lcom/chase/sig/android/activity/CapturePreview$Orientation;

    iput-object v0, p0, Lcom/chase/sig/android/activity/CapturePreview;->c:Lcom/chase/sig/android/activity/CapturePreview$Orientation;

    .line 31
    const/16 v0, 0x1e

    iput v0, p0, Lcom/chase/sig/android/activity/CapturePreview;->d:I

    .line 45
    iput p2, p0, Lcom/chase/sig/android/activity/CapturePreview;->d:I

    .line 46
    iput-object p3, p0, Lcom/chase/sig/android/activity/CapturePreview;->c:Lcom/chase/sig/android/activity/CapturePreview$Orientation;

    .line 48
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/CapturePreview;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/CapturePreview;->e:Landroid/view/SurfaceHolder;

    .line 49
    iget-object v0, p0, Lcom/chase/sig/android/activity/CapturePreview;->e:Landroid/view/SurfaceHolder;

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 50
    iget-object v0, p0, Lcom/chase/sig/android/activity/CapturePreview;->e:Landroid/view/SurfaceHolder;

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setType(I)V

    .line 51
    return-void
.end method


# virtual methods
.method public final getJpegQuality()I
    .locals 1

    .prologue
    .line 300
    iget v0, p0, Lcom/chase/sig/android/activity/CapturePreview;->d:I

    return v0
.end method

.method public final setJpegQuality(I)V
    .locals 0
    .parameter

    .prologue
    .line 296
    iput p1, p0, Lcom/chase/sig/android/activity/CapturePreview;->d:I

    .line 297
    return-void
.end method

.method public final surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 17
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 56
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/chase/sig/android/activity/CapturePreview;->b:Landroid/hardware/Camera;

    if-nez v1, :cond_1

    .line 152
    :cond_0
    :goto_0
    return-void

    .line 61
    :cond_1
    if-eqz p1, :cond_2

    invoke-interface/range {p1 .. p1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 70
    :cond_2
    :try_start_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/chase/sig/android/activity/CapturePreview;->b:Landroid/hardware/Camera;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Landroid/hardware/Camera;->setPreviewDisplay(Landroid/view/SurfaceHolder;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    .line 72
    :goto_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/chase/sig/android/activity/CapturePreview;->b:Landroid/hardware/Camera;

    invoke-virtual {v1}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v6

    .line 81
    :try_start_1
    const-string v1, "auto"

    invoke-virtual {v6, v1}, Landroid/hardware/Camera$Parameters;->setFocusMode(Ljava/lang/String;)V

    .line 84
    invoke-virtual {v6}, Landroid/hardware/Camera$Parameters;->getSupportedFlashModes()Ljava/util/List;

    move-result-object v2

    .line 85
    if-eqz v2, :cond_4

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_4

    const/4 v1, 0x0

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v3, "auto"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    const-string v1, "auto"

    :cond_3
    if-eqz v1, :cond_4

    invoke-virtual {v6, v1}, Landroid/hardware/Camera$Parameters;->setFlashMode(Ljava/lang/String;)V

    .line 88
    :cond_4
    invoke-virtual {v6}, Landroid/hardware/Camera$Parameters;->getSupportedPictureSizes()Ljava/util/List;

    move-result-object v5

    .line 89
    if-nez v5, :cond_b

    const/4 v4, 0x0

    move-object v5, v4

    .line 90
    :goto_3
    iget v1, v5, Landroid/hardware/Camera$Size;->width:I

    iget v2, v5, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {v6, v1, v2}, Landroid/hardware/Camera$Parameters;->setPictureSize(II)V

    .line 93
    const/16 v1, 0x100

    invoke-virtual {v6, v1}, Landroid/hardware/Camera$Parameters;->setPictureFormat(I)V

    .line 96
    invoke-virtual {v6}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewSizes()Ljava/util/List;

    move-result-object v7

    .line 97
    move/from16 v0, p3

    int-to-double v1, v0

    move/from16 v0, p4

    int-to-double v3, v0

    div-double v8, v1, v3

    if-nez v7, :cond_f

    const/4 v4, 0x0

    .line 99
    :cond_5
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/chase/sig/android/activity/CapturePreview;->c:Lcom/chase/sig/android/activity/CapturePreview$Orientation;

    sget-object v2, Lcom/chase/sig/android/activity/CapturePreview$Orientation;->b:Lcom/chase/sig/android/activity/CapturePreview$Orientation;

    if-ne v1, v2, :cond_13

    .line 100
    const/16 v1, 0x5a

    invoke-virtual {v6, v1}, Landroid/hardware/Camera$Parameters;->setRotation(I)V

    .line 101
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x8

    if-lt v1, v2, :cond_6

    .line 102
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/chase/sig/android/activity/CapturePreview;->b:Landroid/hardware/Camera;

    const/16 v2, 0x5a

    invoke-virtual {v1, v2}, Landroid/hardware/Camera;->setDisplayOrientation(I)V

    .line 105
    :cond_6
    iget v1, v5, Landroid/hardware/Camera$Size;->height:I

    iget v2, v5, Landroid/hardware/Camera$Size;->width:I

    if-le v1, v2, :cond_12

    .line 106
    iget v1, v4, Landroid/hardware/Camera$Size;->height:I

    iget v2, v4, Landroid/hardware/Camera$Size;->width:I

    invoke-virtual {v6, v1, v2}, Landroid/hardware/Camera$Parameters;->setPreviewSize(II)V

    .line 115
    :goto_4
    move-object/from16 v0, p0

    iget v1, v0, Lcom/chase/sig/android/activity/CapturePreview;->d:I

    invoke-virtual {v6, v1}, Landroid/hardware/Camera$Parameters;->setJpegQuality(I)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 118
    :goto_5
    :try_start_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/chase/sig/android/activity/CapturePreview;->b:Landroid/hardware/Camera;

    invoke-virtual {v1, v6}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 133
    :try_start_3
    invoke-virtual {v6}, Landroid/hardware/Camera$Parameters;->getSupportedSceneModes()Ljava/util/List;

    move-result-object v1

    .line 134
    if-eqz v1, :cond_8

    .line 135
    const/4 v2, 0x0

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_7
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_17

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v4, "steadyphoto"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    const-string v1, "steadyphoto"

    .line 136
    :goto_6
    invoke-static {v1}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 137
    invoke-virtual {v6, v1}, Landroid/hardware/Camera$Parameters;->setSceneMode(Ljava/lang/String;)V

    .line 140
    :cond_8
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/chase/sig/android/activity/CapturePreview;->b:Landroid/hardware/Camera;

    invoke-virtual {v1, v6}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    .line 146
    :cond_9
    :try_start_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/chase/sig/android/activity/CapturePreview;->b:Landroid/hardware/Camera;

    invoke-virtual {v1}, Landroid/hardware/Camera;->startPreview()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    goto/16 :goto_0

    .line 148
    :catch_0
    move-exception v1

    invoke-virtual/range {p0 .. p0}, Lcom/chase/sig/android/activity/CapturePreview;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/activity/eb;

    const v2, 0x7f070283

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/activity/eb;->f(I)V

    goto/16 :goto_0

    .line 85
    :cond_a
    :try_start_5
    const-string v1, "off"

    goto/16 :goto_2

    .line 89
    :cond_b
    const/4 v4, 0x0

    const-wide v2, 0x7fefffffffffffffL

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_c
    :goto_7
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_d

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/Camera$Size;

    iget v8, v1, Landroid/hardware/Camera$Size;->width:I

    int-to-double v8, v8

    iget v10, v1, Landroid/hardware/Camera$Size;->height:I

    int-to-double v10, v10

    div-double/2addr v8, v10

    const-wide v10, 0x3ff5555555555555L

    sub-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->abs(D)D

    move-result-wide v8

    const-wide v10, 0x3fa999999999999aL

    cmpl-double v8, v8, v10

    if-gtz v8, :cond_c

    iget v8, v1, Landroid/hardware/Camera$Size;->height:I

    add-int/lit16 v8, v8, -0x4b0

    invoke-static {v8}, Ljava/lang/Math;->abs(I)I

    move-result v8

    int-to-double v8, v8

    cmpg-double v8, v8, v2

    if-gez v8, :cond_1b

    iget v2, v1, Landroid/hardware/Camera$Size;->height:I

    add-int/lit16 v2, v2, -0x4b0

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    int-to-double v2, v2

    move-wide v15, v2

    move-object v3, v1

    move-wide v1, v15

    :goto_8
    move-object v4, v3

    move-wide v15, v1

    move-wide v2, v15

    goto :goto_7

    :cond_d
    if-nez v4, :cond_e

    const-wide v2, 0x7fefffffffffffffL

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_9
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_e

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/Camera$Size;

    iget v7, v1, Landroid/hardware/Camera$Size;->height:I

    add-int/lit16 v7, v7, -0x4b0

    invoke-static {v7}, Ljava/lang/Math;->abs(I)I

    move-result v7

    int-to-double v7, v7

    cmpg-double v7, v7, v2

    if-gez v7, :cond_1a

    iget v2, v1, Landroid/hardware/Camera$Size;->height:I

    add-int/lit16 v2, v2, -0x4b0

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    int-to-double v2, v2

    move-wide v15, v2

    move-object v3, v1

    move-wide v1, v15

    :goto_a
    move-object v4, v3

    move-wide v15, v1

    move-wide v2, v15

    goto :goto_9

    :cond_e
    move-object v5, v4

    goto/16 :goto_3

    .line 97
    :cond_f
    const/4 v4, 0x0

    const-wide v2, 0x7fefffffffffffffL

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_10
    :goto_b
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_11

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/Camera$Size;

    iget v11, v1, Landroid/hardware/Camera$Size;->width:I

    int-to-double v11, v11

    iget v13, v1, Landroid/hardware/Camera$Size;->height:I

    int-to-double v13, v13

    div-double/2addr v11, v13

    sub-double/2addr v11, v8

    invoke-static {v11, v12}, Ljava/lang/Math;->abs(D)D

    move-result-wide v11

    const-wide v13, 0x3fb999999999999aL

    cmpl-double v11, v11, v13

    if-gtz v11, :cond_10

    iget v11, v1, Landroid/hardware/Camera$Size;->height:I

    sub-int v11, v11, p4

    invoke-static {v11}, Ljava/lang/Math;->abs(I)I

    move-result v11

    int-to-double v11, v11

    cmpg-double v11, v11, v2

    if-gez v11, :cond_19

    iget v2, v1, Landroid/hardware/Camera$Size;->height:I

    sub-int v2, v2, p4

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    int-to-double v2, v2

    move-wide v15, v2

    move-object v3, v1

    move-wide v1, v15

    :goto_c
    move-object v4, v3

    move-wide v15, v1

    move-wide v2, v15

    goto :goto_b

    :cond_11
    if-nez v4, :cond_5

    const-wide v2, 0x7fefffffffffffffL

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_d
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/Camera$Size;

    iget v8, v1, Landroid/hardware/Camera$Size;->height:I

    sub-int v8, v8, p4

    invoke-static {v8}, Ljava/lang/Math;->abs(I)I

    move-result v8

    int-to-double v8, v8

    cmpg-double v8, v8, v2

    if-gez v8, :cond_18

    iget v2, v1, Landroid/hardware/Camera$Size;->height:I

    sub-int v2, v2, p4

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    int-to-double v2, v2

    move-wide v15, v2

    move-object v3, v1

    move-wide v1, v15

    :goto_e
    move-object v4, v3

    move-wide v15, v1

    move-wide v2, v15

    goto :goto_d

    .line 108
    :cond_12
    iget v1, v4, Landroid/hardware/Camera$Size;->width:I

    iget v2, v4, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {v6, v1, v2}, Landroid/hardware/Camera$Parameters;->setPreviewSize(II)V

    goto/16 :goto_4

    .line 117
    :catch_1
    move-exception v1

    goto/16 :goto_5

    .line 111
    :cond_13
    iget v1, v4, Landroid/hardware/Camera$Size;->width:I

    iget v2, v4, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {v6, v1, v2}, Landroid/hardware/Camera$Parameters;->setPreviewSize(II)V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1

    goto/16 :goto_4

    .line 142
    :catch_2
    move-exception v1

    :try_start_6
    const-string v1, "We are ignoring it as this exception occurs even if the phone says it support it. This only happens for some phones like LG ALLY"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    array-length v3, v2

    if-gtz v3, :cond_16

    move-object v5, v1

    :goto_f
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v4

    div-int/lit16 v1, v4, 0xfa0

    rem-int/lit16 v2, v4, 0xfa0

    if-lez v2, :cond_14

    add-int/lit8 v1, v1, 0x1

    :cond_14
    const/4 v2, 0x0

    :goto_10
    if-ge v2, v1, :cond_9

    mul-int/lit16 v6, v2, 0xfa0

    add-int/lit16 v3, v6, 0xfa0

    if-le v3, v4, :cond_15

    move v3, v4

    :cond_15
    invoke-virtual {v5, v6, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    const-string v6, "Chase"

    const-string v7, "Unknown logging level specified for log message: %s"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v3, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const/4 v7, 0x0

    invoke-static {v6, v3, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    add-int/lit8 v2, v2, 0x1

    goto :goto_10

    :cond_16
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0

    move-result-object v1

    move-object v5, v1

    goto :goto_f

    :catch_3
    move-exception v1

    goto/16 :goto_1

    :cond_17
    move-object v1, v2

    goto/16 :goto_6

    :cond_18
    move-wide v15, v2

    move-wide v1, v15

    move-object v3, v4

    goto :goto_e

    :cond_19
    move-wide v15, v2

    move-wide v1, v15

    move-object v3, v4

    goto/16 :goto_c

    :cond_1a
    move-wide v15, v2

    move-wide v1, v15

    move-object v3, v4

    goto/16 :goto_a

    :cond_1b
    move-wide v15, v2

    move-wide v1, v15

    move-object v3, v4

    goto/16 :goto_8
.end method

.method public final surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 2
    .parameter

    .prologue
    .line 180
    if-nez p1, :cond_0

    .line 193
    :goto_0
    return-void

    .line 186
    :cond_0
    :try_start_0
    invoke-static {}, Landroid/hardware/Camera;->open()Landroid/hardware/Camera;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/CapturePreview;->b:Landroid/hardware/Camera;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 189
    :catch_0
    move-exception v0

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/CapturePreview;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/activity/QuickDepositCaptureActivity;

    const v1, 0x7f070284

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/QuickDepositCaptureActivity;->f(I)V

    goto :goto_0
.end method

.method public final surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 2
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 197
    iget-object v0, p0, Lcom/chase/sig/android/activity/CapturePreview;->b:Landroid/hardware/Camera;

    if-nez v0, :cond_0

    .line 205
    :goto_0
    return-void

    .line 201
    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/CapturePreview;->b:Landroid/hardware/Camera;

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V

    .line 202
    iget-object v0, p0, Lcom/chase/sig/android/activity/CapturePreview;->b:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->stopPreview()V

    .line 203
    iget-object v0, p0, Lcom/chase/sig/android/activity/CapturePreview;->b:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->release()V

    .line 204
    iput-object v1, p0, Lcom/chase/sig/android/activity/CapturePreview;->b:Landroid/hardware/Camera;

    goto :goto_0
.end method
