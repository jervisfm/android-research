.class public Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"


# instance fields
.field private a:Lcom/chase/sig/android/domain/PhoneBookContact;

.field private b:Landroid/widget/ListView;

.field private c:Landroid/widget/ListView;

.field private d:Lcom/chase/sig/android/domain/QuickPayRecipient;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;)Landroid/widget/ListView;
    .locals 1
    .parameter

    .prologue
    .line 27
    iget-object v0, p0, Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;->b:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;Lcom/chase/sig/android/domain/PhoneBookContact;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;->a(Lcom/chase/sig/android/domain/PhoneBookContact;)V

    return-void
.end method

.method private a(Lcom/chase/sig/android/domain/PhoneBookContact;)V
    .locals 3
    .parameter

    .prologue
    .line 114
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/PhoneBookContact;->a()Ljava/util/List;

    move-result-object v0

    .line 116
    new-instance v1, Landroid/widget/ArrayAdapter;

    const v2, 0x7f0300ae

    invoke-interface {v0}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v0

    invoke-direct {v1, p0, v2, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 119
    iget-object v0, p0, Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;->b:Landroid/widget/ListView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setVisibility(I)V

    .line 120
    iget-object v0, p0, Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;->b:Landroid/widget/ListView;

    new-instance v2, Lcom/chase/sig/android/view/af;

    invoke-direct {v2, v1}, Lcom/chase/sig/android/view/af;-><init>(Landroid/widget/ListAdapter;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 121
    iget-object v0, p0, Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;->b:Landroid/widget/ListView;

    new-instance v1, Lcom/chase/sig/android/activity/cf;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/cf;-><init>(Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 130
    return-void
.end method

.method public static a(Landroid/widget/ListAdapter;Landroid/util/SparseBooleanArray;)[J
    .locals 7
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 153
    if-eqz p1, :cond_3

    if-eqz p0, :cond_3

    .line 155
    invoke-virtual {p1}, Landroid/util/SparseBooleanArray;->size()I

    move-result v4

    move v2, v1

    move v0, v1

    .line 158
    :goto_0
    if-ge v2, v4, :cond_1

    .line 159
    invoke-virtual {p1, v2}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 160
    add-int/lit8 v0, v0, 0x1

    .line 158
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 163
    :cond_1
    new-array v2, v0, [J

    move v3, v1

    .line 164
    :goto_1
    if-ge v3, v4, :cond_2

    .line 167
    invoke-virtual {p1, v3}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 168
    add-int/lit8 v0, v1, 0x1

    invoke-virtual {p1, v3}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v5

    invoke-interface {p0, v5}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v5

    aput-wide v5, v2, v1

    .line 166
    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_1

    :cond_2
    move-object v0, v2

    .line 175
    :goto_3
    return-object v0

    :cond_3
    new-array v0, v1, [J

    goto :goto_3

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;)Landroid/widget/ListView;
    .locals 1
    .parameter

    .prologue
    .line 27
    iget-object v0, p0, Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;->c:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic c(Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;)Lcom/chase/sig/android/domain/PhoneBookContact;
    .locals 1
    .parameter

    .prologue
    .line 27
    iget-object v0, p0, Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;->a:Lcom/chase/sig/android/domain/PhoneBookContact;

    return-object v0
.end method

.method static synthetic d(Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;)Lcom/chase/sig/android/domain/QuickPayRecipient;
    .locals 1
    .parameter

    .prologue
    .line 27
    iget-object v0, p0, Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;->d:Lcom/chase/sig/android/domain/QuickPayRecipient;

    return-object v0
.end method

.method private d()V
    .locals 3

    .prologue
    .line 133
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 134
    new-instance v1, Lcom/chase/sig/android/domain/RecipientContact;

    invoke-direct {v1}, Lcom/chase/sig/android/domain/RecipientContact;-><init>()V

    .line 135
    const-string v2, "None"

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/domain/RecipientContact;->c(Ljava/lang/String;)V

    .line 136
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 138
    new-instance v1, Landroid/widget/ArrayAdapter;

    const v2, 0x7f0300ae

    invoke-interface {v0}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v0

    invoke-direct {v1, p0, v2, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 140
    iget-object v0, p0, Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;->c:Landroid/widget/ListView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setVisibility(I)V

    .line 141
    iget-object v0, p0, Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;->c:Landroid/widget/ListView;

    new-instance v2, Lcom/chase/sig/android/view/af;

    invoke-direct {v2, v1}, Lcom/chase/sig/android/view/af;-><init>(Landroid/widget/ListAdapter;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 142
    iget-object v0, p0, Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;->c:Landroid/widget/ListView;

    new-instance v1, Lcom/chase/sig/android/activity/cg;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/cg;-><init>(Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 150
    return-void
.end method

.method static synthetic e(Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;->d()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 37
    const v0, 0x7f03001b

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;->b(I)V

    .line 38
    const v0, 0x7f070063

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;->setTitle(I)V

    .line 39
    const v0, 0x7f09006b

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;->b:Landroid/widget/ListView;

    .line 40
    const v0, 0x7f09006c

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;->c:Landroid/widget/ListView;

    .line 42
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "contact"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/PhoneBookContact;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;->a:Lcom/chase/sig/android/domain/PhoneBookContact;

    .line 44
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "recipient"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/QuickPayRecipient;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;->d:Lcom/chase/sig/android/domain/QuickPayRecipient;

    .line 47
    iget-object v0, p0, Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;->a:Lcom/chase/sig/android/domain/PhoneBookContact;

    invoke-direct {p0, v0}, Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;->a(Lcom/chase/sig/android/domain/PhoneBookContact;)V

    invoke-direct {p0}, Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;->d()V

    const v0, 0x7f09006a

    new-instance v1, Lcom/chase/sig/android/activity/ce;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/ce;-><init>(Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;)V

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 49
    const v0, 0x7f090069

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 50
    const-class v1, Lcom/chase/sig/android/activity/QuickPayChooseRecipientActivity;

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/a/f;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/activity/a/f;->c()Lcom/chase/sig/android/activity/a/f;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/activity/a/f;->b()Lcom/chase/sig/android/activity/a/f;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/activity/a/f;->a()Lcom/chase/sig/android/activity/a/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 53
    return-void
.end method

.method protected final b()Lcom/chase/sig/android/domain/QuickPayRecipient;
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 96
    iget-object v0, p0, Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;->d:Lcom/chase/sig/android/domain/QuickPayRecipient;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayRecipient;->h()V

    .line 97
    iget-object v0, p0, Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;->b:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getCheckedItemPositions()Landroid/util/SparseBooleanArray;

    move-result-object v4

    .line 99
    iget-object v0, p0, Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;->b:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v5

    .line 100
    invoke-interface {v5}, Landroid/widget/ListAdapter;->getCount()I

    move-result v6

    .line 101
    const/4 v2, 0x1

    move v3, v1

    .line 102
    :goto_0
    if-ge v3, v6, :cond_0

    .line 103
    invoke-virtual {v4, v3}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 104
    invoke-interface {v5, v3}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/RecipientContact;

    .line 105
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/RecipientContact;->b()Ljava/lang/String;

    move-result-object v0

    .line 106
    iget-object v7, p0, Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;->d:Lcom/chase/sig/android/domain/QuickPayRecipient;

    new-instance v8, Lcom/chase/sig/android/domain/Email;

    const-string v9, "0"

    invoke-direct {v8, v0, v2, v9}, Lcom/chase/sig/android/domain/Email;-><init>(Ljava/lang/String;ZLjava/lang/String;)V

    invoke-virtual {v7, v8}, Lcom/chase/sig/android/domain/QuickPayRecipient;->a(Lcom/chase/sig/android/domain/Email;)V

    move v0, v1

    .line 102
    :goto_1
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v0

    goto :goto_0

    .line 110
    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;->d:Lcom/chase/sig/android/domain/QuickPayRecipient;

    return-object v0

    :cond_1
    move v0, v2

    goto :goto_1
.end method
