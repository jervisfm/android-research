.class final Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity$a;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/RecipientContact;",
            ">;"
        }
    .end annotation
.end field

.field private c:I


# direct methods
.method public constructor <init>(Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;Ljava/util/List;)V
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/RecipientContact;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 168
    iput-object p1, p0, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity$a;->a:Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 166
    const/4 v0, -0x1

    iput v0, p0, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity$a;->c:I

    .line 169
    iput-object p2, p0, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity$a;->b:Ljava/util/List;

    .line 170
    return-void
.end method


# virtual methods
.method public final a()Lcom/chase/sig/android/domain/RecipientContact;
    .locals 2

    .prologue
    .line 173
    iget-object v0, p0, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity$a;->b:Ljava/util/List;

    iget v1, p0, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity$a;->c:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/RecipientContact;

    return-object v0
.end method

.method public final a(I)V
    .locals 0
    .parameter

    .prologue
    .line 256
    iput p1, p0, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity$a;->c:I

    .line 257
    invoke-super {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 258
    return-void
.end method

.method public final areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 178
    const/4 v0, 0x1

    return v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 261
    iget v0, p0, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity$a;->c:I

    return v0
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity$a;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 193
    iget-object v0, p0, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity$a;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2
    .parameter

    .prologue
    .line 198
    iget-object v0, p0, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity$a;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/RecipientContact;

    .line 199
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/RecipientContact;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 1
    .parameter

    .prologue
    .line 204
    const/4 v0, 0x0

    return v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 211
    if-nez p2, :cond_0

    .line 212
    iget-object v0, p0, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity$a;->a:Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030065

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 215
    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity$a;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/RecipientContact;

    .line 220
    const v1, 0x7f090169

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckedTextView;

    .line 221
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/RecipientContact;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/CheckedTextView;->setText(Ljava/lang/CharSequence;)V

    .line 222
    iget v2, p0, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity$a;->c:I

    if-ne v2, p1, :cond_1

    const/4 v2, 0x1

    :goto_0
    invoke-virtual {v1, v2}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 224
    const v1, 0x7f0900f0

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 225
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/RecipientContact;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 227
    return-object p2

    .line 222
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 232
    const/4 v0, 0x1

    return v0
.end method

.method public final hasStableIds()Z
    .locals 1

    .prologue
    .line 237
    const/4 v0, 0x1

    return v0
.end method

.method public final isEmpty()Z
    .locals 1

    .prologue
    .line 242
    iget-object v0, p0, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity$a;->b:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity$a;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isEnabled(I)Z
    .locals 1
    .parameter

    .prologue
    .line 183
    const/4 v0, 0x1

    return v0
.end method

.method public final registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 0
    .parameter

    .prologue
    .line 247
    invoke-super {p0, p1}, Landroid/widget/BaseAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 248
    return-void
.end method

.method public final unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 0
    .parameter

    .prologue
    .line 252
    invoke-super {p0, p1}, Landroid/widget/BaseAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 253
    return-void
.end method
