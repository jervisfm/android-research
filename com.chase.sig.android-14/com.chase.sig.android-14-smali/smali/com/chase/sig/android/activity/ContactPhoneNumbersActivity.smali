.class public Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity$a;
    }
.end annotation


# instance fields
.field private a:Lcom/chase/sig/android/domain/PhoneBookContact;

.field private b:Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity$a;

.field private c:Landroid/widget/CheckedTextView;

.field private d:Landroid/widget/ListView;

.field private k:Lcom/chase/sig/android/domain/QuickPayRecipient;

.field private final l:Lcom/chase/sig/android/activity/cc;

.field private m:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    .line 36
    invoke-static {}, Lcom/chase/sig/android/activity/cc;->b()Lcom/chase/sig/android/activity/cc;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;->l:Lcom/chase/sig/android/activity/cc;

    .line 38
    const/4 v0, -0x1

    iput v0, p0, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;->m:I

    .line 163
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 26
    iput p1, p0, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;->m:I

    return p1
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;)Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity$a;
    .locals 1
    .parameter

    .prologue
    .line 26
    iget-object v0, p0, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;->b:Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity$a;

    return-object v0
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;Landroid/view/View;)V
    .locals 5
    .parameter
    .parameter

    .prologue
    const v4, 0x7f090169

    const/4 v3, 0x0

    .line 26
    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    iget-object v1, p0, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;->c:Landroid/widget/CheckedTextView;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;->c:Landroid/widget/CheckedTextView;

    invoke-virtual {v1, v3}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    :cond_0
    iput-object v0, p0, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;->c:Landroid/widget/CheckedTextView;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;->c:Landroid/widget/CheckedTextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    return-void

    :cond_1
    move v2, v3

    :goto_0
    iget-object v1, p0, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;->d:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getChildCount()I

    move-result v1

    if-ge v2, v1, :cond_0

    iget-object v1, p0, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;->d:Landroid/widget/ListView;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckedTextView;

    if-eqz v1, :cond_2

    invoke-virtual {v1, v3}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;)Lcom/chase/sig/android/domain/QuickPayRecipient;
    .locals 1
    .parameter

    .prologue
    .line 26
    iget-object v0, p0, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;->k:Lcom/chase/sig/android/domain/QuickPayRecipient;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 6
    .parameter

    .prologue
    const/4 v1, -0x1

    .line 43
    const v0, 0x7f03001c

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;->b(I)V

    .line 44
    const v0, 0x7f070064

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;->setTitle(I)V

    .line 46
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "contact"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/PhoneBookContact;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;->a:Lcom/chase/sig/android/domain/PhoneBookContact;

    .line 48
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "recipient"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/QuickPayRecipient;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;->k:Lcom/chase/sig/android/domain/QuickPayRecipient;

    .line 50
    if-nez p1, :cond_1

    move v0, v1

    :goto_0
    iput v0, p0, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;->m:I

    .line 52
    iget-object v0, p0, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;->a:Lcom/chase/sig/android/domain/PhoneBookContact;

    iget v2, p0, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;->m:I

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/PhoneBookContact;->b()Ljava/util/List;

    move-result-object v3

    new-instance v4, Lcom/chase/sig/android/domain/RecipientContact;

    invoke-direct {v4}, Lcom/chase/sig/android/domain/RecipientContact;-><init>()V

    const-string v5, "None"

    invoke-virtual {v4, v5}, Lcom/chase/sig/android/domain/RecipientContact;->b(Ljava/lang/String;)V

    const-string v5, ""

    invoke-virtual {v4, v5}, Lcom/chase/sig/android/domain/RecipientContact;->c(Ljava/lang/String;)V

    const-string v5, "None"

    invoke-virtual {v4, v5}, Lcom/chase/sig/android/domain/RecipientContact;->a(Ljava/lang/String;)V

    const-string v5, "-2"

    invoke-virtual {v4, v5}, Lcom/chase/sig/android/domain/RecipientContact;->d(Ljava/lang/String;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v3, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity$a;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/PhoneBookContact;->b()Ljava/util/List;

    move-result-object v0

    invoke-direct {v3, p0, v0}, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity$a;-><init>(Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;Ljava/util/List;)V

    iput-object v3, p0, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;->b:Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity$a;

    const v0, 0x7f09006f

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;->d:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;->d:Landroid/widget/ListView;

    new-instance v3, Lcom/chase/sig/android/view/af;

    iget-object v4, p0, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;->b:Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity$a;

    invoke-direct {v3, v4}, Lcom/chase/sig/android/view/af;-><init>(Landroid/widget/ListAdapter;)V

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;->d:Landroid/widget/ListView;

    new-instance v3, Lcom/chase/sig/android/activity/ch;

    invoke-direct {v3, p0}, Lcom/chase/sig/android/activity/ch;-><init>(Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;)V

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    if-ne v2, v1, :cond_3

    iget-object v0, p0, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;->l:Lcom/chase/sig/android/activity/cc;

    invoke-virtual {v0, p0}, Lcom/chase/sig/android/activity/cc;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;->b:Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity$a;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity$a;->getCount()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;->b:Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity$a;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity$a;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/RecipientContact;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/RecipientContact;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;->b:Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity$a;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity$a;->a(I)V

    :cond_0
    :goto_2
    const v0, 0x7f090070

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const-string v1, "Next"

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    new-instance v1, Lcom/chase/sig/android/activity/ci;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/ci;-><init>(Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 54
    const v0, 0x7f090069

    const-class v1, Lcom/chase/sig/android/activity/QuickPayChooseRecipientActivity;

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/a/f;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/activity/a/f;->b()Lcom/chase/sig/android/activity/a/f;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/activity/a/f;->a()Lcom/chase/sig/android/activity/a/f;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/activity/a/f;->c()Lcom/chase/sig/android/activity/a/f;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 56
    return-void

    .line 50
    :cond_1
    const-string v0, "selectedPhone"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    goto/16 :goto_0

    .line 52
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;->b:Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity$a;

    iget v1, p0, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;->m:I

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity$a;->a(I)V

    goto :goto_2
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 159
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ai;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 160
    const-string v0, "selectedPhone"

    iget v1, p0, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;->m:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 161
    return-void
.end method
