.class final Lcom/chase/sig/android/activity/ContactUsActivity$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/ContactUsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/ContactUsActivity;

.field private b:I

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/chase/sig/android/activity/ContactUsActivity;ILjava/lang/String;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 69
    iput-object p1, p0, Lcom/chase/sig/android/activity/ContactUsActivity$a;->a:Lcom/chase/sig/android/activity/ContactUsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    iput p2, p0, Lcom/chase/sig/android/activity/ContactUsActivity$a;->b:I

    .line 71
    iput-object p3, p0, Lcom/chase/sig/android/activity/ContactUsActivity$a;->c:Ljava/lang/String;

    .line 72
    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 6
    .parameter

    .prologue
    const/4 v5, 0x0

    .line 76
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ContactUsActivity$a;->a:Lcom/chase/sig/android/activity/ContactUsActivity;

    iget v1, p0, Lcom/chase/sig/android/activity/ContactUsActivity$a;->b:I

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/ContactUsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 77
    iget-object v1, p0, Lcom/chase/sig/android/activity/ContactUsActivity$a;->a:Lcom/chase/sig/android/activity/ContactUsActivity;

    const-string v2, "Please call %s at %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/chase/sig/android/activity/ContactUsActivity$a;->c:Ljava/lang/String;

    aput-object v4, v3, v5

    const/4 v4, 0x1

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2, v5}, Lcom/chase/sig/android/activity/ContactUsActivity;->b(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 79
    return-void
.end method
