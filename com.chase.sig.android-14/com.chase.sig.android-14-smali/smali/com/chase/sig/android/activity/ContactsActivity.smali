.class public Lcom/chase/sig/android/activity/ContactsActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"


# instance fields
.field private a:Landroid/widget/ListView;

.field private final b:Lcom/chase/sig/android/activity/cc;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/chase/sig/android/activity/ContactsActivity;->a:Landroid/widget/ListView;

    .line 29
    invoke-static {}, Lcom/chase/sig/android/activity/cc;->b()Lcom/chase/sig/android/activity/cc;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/ContactsActivity;->b:Lcom/chase/sig/android/activity/cc;

    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/ContactsActivity;)Lcom/chase/sig/android/activity/cc;
    .locals 1
    .parameter

    .prologue
    .line 26
    iget-object v0, p0, Lcom/chase/sig/android/activity/ContactsActivity;->b:Lcom/chase/sig/android/activity/cc;

    return-object v0
.end method

.method static synthetic a(Lcom/chase/sig/android/domain/PhoneBookContact;)Lcom/chase/sig/android/domain/QuickPayRecipient;
    .locals 6
    .parameter

    .prologue
    const/4 v5, 0x1

    .line 26
    invoke-static {p0}, Lcom/chase/sig/android/activity/ContactsActivity;->c(Lcom/chase/sig/android/domain/PhoneBookContact;)Lcom/chase/sig/android/domain/QuickPayRecipient;

    move-result-object v1

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/PhoneBookContact;->a()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-ne v2, v5, :cond_0

    invoke-interface {v0}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v0

    const/4 v2, 0x0

    aget-object v0, v0, v2

    check-cast v0, Lcom/chase/sig/android/domain/RecipientContact;

    new-instance v2, Lcom/chase/sig/android/domain/Email;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/RecipientContact;->b()Ljava/lang/String;

    move-result-object v0

    const-string v3, "0"

    invoke-direct {v2, v0, v5, v3}, Lcom/chase/sig/android/domain/Email;-><init>(Ljava/lang/String;ZLjava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/domain/QuickPayRecipient;->a(Lcom/chase/sig/android/domain/Email;)V

    :cond_0
    invoke-virtual {p0}, Lcom/chase/sig/android/domain/PhoneBookContact;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/PhoneBookContact;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/RecipientContact;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/RecipientContact;->a()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Mobile"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/PhoneBookContact;->b()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ne v3, v5, :cond_1

    :cond_2
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/RecipientContact;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/QuickPayRecipient;->e(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    return-object v1
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/ContactsActivity;Lcom/chase/sig/android/domain/QuickPayRecipient;Lcom/chase/sig/android/domain/PhoneBookContact;Ljava/lang/Class;)V
    .locals 3
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 26
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p0, p3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/ContactsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/content/Intent;)Landroid/content/Intent;

    const-string v1, "fromContactsActivity"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "contact"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v1, "recipient"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ContactsActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic b(Lcom/chase/sig/android/domain/PhoneBookContact;)Lcom/chase/sig/android/domain/QuickPayRecipient;
    .locals 1
    .parameter

    .prologue
    .line 26
    invoke-static {p0}, Lcom/chase/sig/android/activity/ContactsActivity;->c(Lcom/chase/sig/android/domain/PhoneBookContact;)Lcom/chase/sig/android/domain/QuickPayRecipient;

    move-result-object v0

    return-object v0
.end method

.method private static c(Lcom/chase/sig/android/domain/PhoneBookContact;)Lcom/chase/sig/android/domain/QuickPayRecipient;
    .locals 2
    .parameter

    .prologue
    .line 80
    new-instance v0, Lcom/chase/sig/android/domain/QuickPayRecipient;

    invoke-direct {v0}, Lcom/chase/sig/android/domain/QuickPayRecipient;-><init>()V

    .line 81
    invoke-virtual {p0}, Lcom/chase/sig/android/domain/PhoneBookContact;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/QuickPayRecipient;->a(Ljava/lang/String;)V

    .line 82
    return-object v0
.end method


# virtual methods
.method protected final a(I)V
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 153
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ai;->a(I)V

    .line 155
    if-ne p1, v2, :cond_0

    .line 156
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/chase/sig/android/activity/QuickPayChooseRecipientActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 157
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/ContactsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/content/Intent;)Landroid/content/Intent;

    .line 158
    const-string v1, "fromContactsActivity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 159
    const-string v1, "recipient"

    new-instance v2, Lcom/chase/sig/android/domain/QuickPayRecipient;

    invoke-direct {v2}, Lcom/chase/sig/android/domain/QuickPayRecipient;-><init>()V

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 160
    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ContactsActivity;->startActivity(Landroid/content/Intent;)V

    .line 162
    :cond_0
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 9
    .parameter

    .prologue
    const/4 v3, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 34
    const v0, 0x7f03001f

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ContactsActivity;->b(I)V

    .line 35
    const v0, 0x7f070295

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ContactsActivity;->setTitle(I)V

    .line 37
    const v0, 0x7f09007f

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ContactsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ContactsActivity;->a:Landroid/widget/ListView;

    .line 38
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v7

    const-string v0, "display_name"

    aput-object v0, v2, v8

    const-string v5, "display_name COLLATE LOCALIZED ASC"

    iget-object v0, p0, Lcom/chase/sig/android/activity/ContactsActivity;->b:Lcom/chase/sig/android/activity/cc;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/cc;->a()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    move-object v0, p0

    move-object v4, v3

    invoke-virtual/range {v0 .. v5}, Lcom/chase/sig/android/activity/ContactsActivity;->managedQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Lcom/chase/sig/android/service/ServiceError;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/ContactsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f070248

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v7}, Lcom/chase/sig/android/service/ServiceError;-><init>(Ljava/lang/String;Z)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0, v8, v0}, Lcom/chase/sig/android/activity/ContactsActivity;->a(ILjava/util/List;)V

    :cond_0
    new-array v4, v8, [Ljava/lang/String;

    const-string v0, "display_name"

    aput-object v0, v4, v7

    new-instance v6, Lcom/chase/sig/android/view/af;

    new-instance v0, Landroid/widget/SimpleCursorAdapter;

    const v2, 0x7f030064

    new-array v5, v8, [I

    const v1, 0x7f090168

    aput v1, v5, v7

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Landroid/widget/SimpleCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[I)V

    invoke-direct {v6, v0}, Lcom/chase/sig/android/view/af;-><init>(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ContactsActivity;->a:Landroid/widget/ListView;

    invoke-virtual {v0, v6}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 39
    iget-object v0, p0, Lcom/chase/sig/android/activity/ContactsActivity;->a:Landroid/widget/ListView;

    new-instance v1, Lcom/chase/sig/android/activity/ck;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/ck;-><init>(Lcom/chase/sig/android/activity/ContactsActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 40
    return-void
.end method
