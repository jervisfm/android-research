.class public Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity$a;
.super Lcom/chase/sig/android/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/d",
        "<",
        "Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/service/CreditFacilityAccountDetailResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0}, Lcom/chase/sig/android/d;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .parameter

    .prologue
    .line 87
    check-cast p1, [Ljava/lang/String;

    const/4 v0, 0x1

    aget-object v0, p1, v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity$a;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;->A()Lcom/chase/sig/android/service/n;

    move-result-object v0

    iget-object v1, v0, Lcom/chase/sig/android/service/n;->e:Lcom/chase/sig/android/service/e;

    if-nez v1, :cond_0

    new-instance v1, Lcom/chase/sig/android/service/e;

    invoke-direct {v1}, Lcom/chase/sig/android/service/e;-><init>()V

    iput-object v1, v0, Lcom/chase/sig/android/service/n;->e:Lcom/chase/sig/android/service/e;

    :cond_0
    iget-object v0, v0, Lcom/chase/sig/android/service/n;->e:Lcom/chase/sig/android/service/e;

    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-static {v0}, Lcom/chase/sig/android/service/e;->a(Ljava/lang/String;)Lcom/chase/sig/android/service/CreditFacilityAccountDetailResponse;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 2
    .parameter

    .prologue
    .line 87
    check-cast p1, Lcom/chase/sig/android/service/CreditFacilityAccountDetailResponse;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/CreditFacilityAccountDetailResponse;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/CreditFacilityAccountDetailResponse;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;->b(Ljava/util/List;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;

    iget-object v1, p0, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity$a;->a:Ljava/lang/String;

    invoke-static {v0, p1, v1}, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;->a(Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;Lcom/chase/sig/android/service/CreditFacilityAccountDetailResponse;Ljava/lang/String;)V

    goto :goto_0
.end method
