.class final Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity$b;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/chase/sig/android/domain/CreditFacilityAccountLoanDetail;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/CreditFacilityAccountLoanDetail;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;Landroid/content/Context;Ljava/util/List;)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/CreditFacilityAccountLoanDetail;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 145
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity$b;-><init>(Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;Landroid/content/Context;Ljava/util/List;B)V

    .line 146
    return-void
.end method

.method private constructor <init>(Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;Landroid/content/Context;Ljava/util/List;B)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "IBB)V"
        }
    .end annotation

    .prologue
    .line 148
    iput-object p1, p0, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity$b;->a:Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;

    .line 149
    const v0, 0x7f030057

    invoke-direct {p0, p2, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 150
    iput-object p3, p0, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity$b;->b:Ljava/util/List;

    .line 151
    return-void
.end method


# virtual methods
.method public final getCount()I
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity$b;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 141
    iget-object v0, p0, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity$b;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/CreditFacilityAccountLoanDetail;

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2
    .parameter

    .prologue
    .line 165
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 170
    if-nez p2, :cond_0

    .line 171
    iget-object v0, p0, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity$b;->a:Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030057

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 174
    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity$b;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/CreditFacilityAccountLoanDetail;

    .line 175
    const v1, 0x7f090117

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const-string v2, "%s   %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity$b;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0702a1

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/CreditFacilityAccountLoanDetail;->a()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 176
    const v1, 0x7f090119

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/CreditFacilityAccountLoanDetail;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 177
    const v1, 0x7f09011b

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/CreditFacilityAccountLoanDetail;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 178
    const v1, 0x7f09011d

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/CreditFacilityAccountLoanDetail;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 179
    const v1, 0x7f09011f

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/CreditFacilityAccountLoanDetail;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 180
    const v1, 0x7f090121

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/CreditFacilityAccountLoanDetail;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 182
    return-object p2
.end method
