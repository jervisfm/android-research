.class public Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$d;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity$b;,
        Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity$a;
    }
.end annotation


# instance fields
.field private a:Lcom/chase/sig/android/domain/CreditFacilityAccountDetail;

.field private b:Landroid/widget/ListView;

.field private c:Landroid/view/View;

.field private d:Landroid/widget/RelativeLayout;

.field private k:Landroid/widget/Button;

.field private l:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    .line 34
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;->l:Z

    .line 141
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;Lcom/chase/sig/android/service/CreditFacilityAccountDetailResponse;Ljava/lang/String;)V
    .locals 2
    .parameter
    .parameter
    .parameter

    .prologue
    .line 24
    invoke-virtual {p1}, Lcom/chase/sig/android/service/CreditFacilityAccountDetailResponse;->a()Lcom/chase/sig/android/domain/CreditFacilityAccountDetail;

    move-result-object v0

    new-instance v1, Lcom/chase/sig/android/util/Dollar;

    invoke-direct {v1, p2}, Lcom/chase/sig/android/util/Dollar;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/CreditFacilityAccountDetail;->c(Lcom/chase/sig/android/util/Dollar;)V

    invoke-direct {p0, v0}, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;->a(Lcom/chase/sig/android/domain/CreditFacilityAccountDetail;)V

    return-void
.end method

.method private a(Lcom/chase/sig/android/domain/CreditFacilityAccountDetail;)V
    .locals 3
    .parameter

    .prologue
    .line 109
    iput-object p1, p0, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;->a:Lcom/chase/sig/android/domain/CreditFacilityAccountDetail;

    .line 110
    const v0, 0x7f090081

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;->a:Lcom/chase/sig/android/domain/CreditFacilityAccountDetail;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/CreditFacilityAccountDetail;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 111
    const v0, 0x7f090086

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;->a:Lcom/chase/sig/android/domain/CreditFacilityAccountDetail;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/CreditFacilityAccountDetail;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 113
    const v0, 0x7f090088

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;->a:Lcom/chase/sig/android/domain/CreditFacilityAccountDetail;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/CreditFacilityAccountDetail;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 115
    const v0, 0x7f09008a

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;->a:Lcom/chase/sig/android/domain/CreditFacilityAccountDetail;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/CreditFacilityAccountDetail;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 117
    const v0, 0x7f09008c

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;->a:Lcom/chase/sig/android/domain/CreditFacilityAccountDetail;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/CreditFacilityAccountDetail;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 119
    iget-object v0, p0, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;->a:Lcom/chase/sig/android/domain/CreditFacilityAccountDetail;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/CreditFacilityAccountDetail;->f()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    const v0, 0x7f090090

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f090092

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 120
    :goto_0
    return-void

    .line 119
    :cond_0
    iget-object v1, p0, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;->b:Landroid/widget/ListView;

    new-instance v2, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity$b;

    invoke-direct {v2, p0, p0, v0}, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity$b;-><init>(Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;Landroid/content/Context;Ljava/util/List;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;)Z
    .locals 1
    .parameter

    .prologue
    .line 24
    iget-boolean v0, p0, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;->l:Z

    return v0
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 24
    iput-boolean p1, p0, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;->l:Z

    return p1
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;)Landroid/widget/RelativeLayout;
    .locals 1
    .parameter

    .prologue
    .line 24
    iget-object v0, p0, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;->d:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic c(Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;)Landroid/view/View;
    .locals 1
    .parameter

    .prologue
    .line 24
    iget-object v0, p0, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;->c:Landroid/view/View;

    return-object v0
.end method

.method static synthetic d(Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;)Landroid/widget/Button;
    .locals 1
    .parameter

    .prologue
    .line 24
    iget-object v0, p0, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;->k:Landroid/widget/Button;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 6
    .parameter

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 38
    const v0, 0x7f030020

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;->b(I)V

    .line 39
    const v0, 0x7f0700d0

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;->setTitle(I)V

    .line 41
    const v0, 0x7f090082

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;->c:Landroid/view/View;

    .line 42
    const v0, 0x7f090084

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;->d:Landroid/widget/RelativeLayout;

    .line 44
    const v0, 0x7f09008f

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;->k:Landroid/widget/Button;

    .line 46
    if-eqz p1, :cond_0

    .line 47
    const-string v0, "account_summary_state"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;->l:Z

    .line 51
    :cond_0
    iget-object v4, p0, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;->c:Landroid/view/View;

    iget-boolean v0, p0, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;->l:Z

    if-eqz v0, :cond_2

    move v0, v3

    :goto_1
    invoke-virtual {v4, v0}, Landroid/view/View;->setVisibility(I)V

    .line 52
    iget-object v0, p0, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;->d:Landroid/widget/RelativeLayout;

    iget-boolean v4, p0, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;->l:Z

    if-eqz v4, :cond_3

    :goto_2
    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 53
    iget-object v3, p0, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;->k:Landroid/widget/Button;

    iget-boolean v0, p0, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;->l:Z

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f0200ed

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_3
    invoke-virtual {v3, v0}, Landroid/widget/Button;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 57
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 58
    const-string v3, "selectedAccountId"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 59
    const-string v4, "outstandingValue"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 60
    const v0, 0x7f090092

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;->b:Landroid/widget/ListView;

    .line 63
    const-string v0, "credit_facility_account_detail"

    invoke-static {p1, v0}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 64
    const-string v0, "credit_facility_account_detail"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/CreditFacilityAccountDetail;

    iput-object v0, p0, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;->a:Lcom/chase/sig/android/domain/CreditFacilityAccountDetail;

    .line 65
    iget-object v0, p0, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;->a:Lcom/chase/sig/android/domain/CreditFacilityAccountDetail;

    invoke-direct {p0, v0}, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;->a(Lcom/chase/sig/android/domain/CreditFacilityAccountDetail;)V

    .line 70
    :goto_4
    const v0, 0x7f09008d

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/chase/sig/android/activity/cl;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/cl;-><init>(Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;->k:Landroid/widget/Button;

    new-instance v1, Lcom/chase/sig/android/activity/cm;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/cm;-><init>(Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 71
    return-void

    :cond_1
    move v0, v2

    .line 47
    goto :goto_0

    :cond_2
    move v0, v1

    .line 51
    goto :goto_1

    :cond_3
    move v3, v1

    .line 52
    goto :goto_2

    .line 53
    :cond_4
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f0200ee

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_3

    .line 67
    :cond_5
    const-class v0, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity$a;

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/String;

    aput-object v3, v5, v1

    aput-object v4, v5, v2

    invoke-virtual {p0, v0, v5}, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    goto :goto_4
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 75
    iget-object v0, p0, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;->a:Lcom/chase/sig/android/domain/CreditFacilityAccountDetail;

    if-eqz v0, :cond_0

    .line 76
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ai;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 77
    const-string v0, "credit_facility_account_detail"

    iget-object v1, p0, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;->a:Lcom/chase/sig/android/domain/CreditFacilityAccountDetail;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 80
    :cond_0
    const-string v0, "account_summary_state"

    iget-object v1, p0, Lcom/chase/sig/android/activity/CreditFacilityAccountDetailActivity;->d:Landroid/widget/RelativeLayout;

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 81
    return-void
.end method
