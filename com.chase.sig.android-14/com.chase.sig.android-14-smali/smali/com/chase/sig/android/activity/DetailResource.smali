.class public final enum Lcom/chase/sig/android/activity/DetailResource;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/chase/sig/android/activity/DetailResource;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/chase/sig/android/activity/DetailResource;

.field public static final enum b:Lcom/chase/sig/android/activity/DetailResource;

.field public static final enum c:Lcom/chase/sig/android/activity/DetailResource;

.field public static final enum d:Lcom/chase/sig/android/activity/DetailResource;

.field public static final enum e:Lcom/chase/sig/android/activity/DetailResource;

.field public static final enum f:Lcom/chase/sig/android/activity/DetailResource;

.field public static final enum g:Lcom/chase/sig/android/activity/DetailResource;

.field public static final enum h:Lcom/chase/sig/android/activity/DetailResource;

.field public static final enum i:Lcom/chase/sig/android/activity/DetailResource;

.field public static final enum j:Lcom/chase/sig/android/activity/DetailResource;

.field public static final enum k:Lcom/chase/sig/android/activity/DetailResource;

.field private static final synthetic l:[Lcom/chase/sig/android/activity/DetailResource;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 4
    new-instance v0, Lcom/chase/sig/android/activity/DetailResource;

    const-string v1, "CANCEL_BUTTON_TEXT"

    invoke-direct {v0, v1, v3}, Lcom/chase/sig/android/activity/DetailResource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/chase/sig/android/activity/DetailResource;->a:Lcom/chase/sig/android/activity/DetailResource;

    .line 5
    new-instance v0, Lcom/chase/sig/android/activity/DetailResource;

    const-string v1, "DO_NOT_CANCEL_BUTTON_TEXT"

    invoke-direct {v0, v1, v4}, Lcom/chase/sig/android/activity/DetailResource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/chase/sig/android/activity/DetailResource;->b:Lcom/chase/sig/android/activity/DetailResource;

    .line 6
    new-instance v0, Lcom/chase/sig/android/activity/DetailResource;

    const-string v1, "CANCEL_SINGLE_WARNING"

    invoke-direct {v0, v1, v5}, Lcom/chase/sig/android/activity/DetailResource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/chase/sig/android/activity/DetailResource;->c:Lcom/chase/sig/android/activity/DetailResource;

    .line 7
    new-instance v0, Lcom/chase/sig/android/activity/DetailResource;

    const-string v1, "CANCEL_REOCURRING_WARNING"

    invoke-direct {v0, v1, v6}, Lcom/chase/sig/android/activity/DetailResource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/chase/sig/android/activity/DetailResource;->d:Lcom/chase/sig/android/activity/DetailResource;

    .line 8
    new-instance v0, Lcom/chase/sig/android/activity/DetailResource;

    const-string v1, "SKIP_BUTTON_TEXT"

    invoke-direct {v0, v1, v7}, Lcom/chase/sig/android/activity/DetailResource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/chase/sig/android/activity/DetailResource;->e:Lcom/chase/sig/android/activity/DetailResource;

    .line 9
    new-instance v0, Lcom/chase/sig/android/activity/DetailResource;

    const-string v1, "CANCEL_ALL_BUTTON_TEXT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/chase/sig/android/activity/DetailResource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/chase/sig/android/activity/DetailResource;->f:Lcom/chase/sig/android/activity/DetailResource;

    .line 10
    new-instance v0, Lcom/chase/sig/android/activity/DetailResource;

    const-string v1, "MAKE_NO_CHANGES_BUTTON_TEXT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/chase/sig/android/activity/DetailResource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/chase/sig/android/activity/DetailResource;->g:Lcom/chase/sig/android/activity/DetailResource;

    .line 11
    new-instance v0, Lcom/chase/sig/android/activity/DetailResource;

    const-string v1, "EDIT_BUTTON_TEXT"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/chase/sig/android/activity/DetailResource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/chase/sig/android/activity/DetailResource;->h:Lcom/chase/sig/android/activity/DetailResource;

    .line 12
    new-instance v0, Lcom/chase/sig/android/activity/DetailResource;

    const-string v1, "EDIT_ONE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/chase/sig/android/activity/DetailResource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/chase/sig/android/activity/DetailResource;->i:Lcom/chase/sig/android/activity/DetailResource;

    .line 13
    new-instance v0, Lcom/chase/sig/android/activity/DetailResource;

    const-string v1, "EDIT_ALL"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/chase/sig/android/activity/DetailResource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/chase/sig/android/activity/DetailResource;->j:Lcom/chase/sig/android/activity/DetailResource;

    .line 14
    new-instance v0, Lcom/chase/sig/android/activity/DetailResource;

    const-string v1, "EDIT_REPEATING_MESSAGE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/chase/sig/android/activity/DetailResource;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/chase/sig/android/activity/DetailResource;->k:Lcom/chase/sig/android/activity/DetailResource;

    .line 3
    const/16 v0, 0xb

    new-array v0, v0, [Lcom/chase/sig/android/activity/DetailResource;

    sget-object v1, Lcom/chase/sig/android/activity/DetailResource;->a:Lcom/chase/sig/android/activity/DetailResource;

    aput-object v1, v0, v3

    sget-object v1, Lcom/chase/sig/android/activity/DetailResource;->b:Lcom/chase/sig/android/activity/DetailResource;

    aput-object v1, v0, v4

    sget-object v1, Lcom/chase/sig/android/activity/DetailResource;->c:Lcom/chase/sig/android/activity/DetailResource;

    aput-object v1, v0, v5

    sget-object v1, Lcom/chase/sig/android/activity/DetailResource;->d:Lcom/chase/sig/android/activity/DetailResource;

    aput-object v1, v0, v6

    sget-object v1, Lcom/chase/sig/android/activity/DetailResource;->e:Lcom/chase/sig/android/activity/DetailResource;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/chase/sig/android/activity/DetailResource;->f:Lcom/chase/sig/android/activity/DetailResource;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/chase/sig/android/activity/DetailResource;->g:Lcom/chase/sig/android/activity/DetailResource;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/chase/sig/android/activity/DetailResource;->h:Lcom/chase/sig/android/activity/DetailResource;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/chase/sig/android/activity/DetailResource;->i:Lcom/chase/sig/android/activity/DetailResource;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/chase/sig/android/activity/DetailResource;->j:Lcom/chase/sig/android/activity/DetailResource;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/chase/sig/android/activity/DetailResource;->k:Lcom/chase/sig/android/activity/DetailResource;

    aput-object v2, v0, v1

    sput-object v0, Lcom/chase/sig/android/activity/DetailResource;->l:[Lcom/chase/sig/android/activity/DetailResource;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/chase/sig/android/activity/DetailResource;
    .locals 1
    .parameter

    .prologue
    .line 3
    const-class v0, Lcom/chase/sig/android/activity/DetailResource;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/activity/DetailResource;

    return-object v0
.end method

.method public static values()[Lcom/chase/sig/android/activity/DetailResource;
    .locals 1

    .prologue
    .line 3
    sget-object v0, Lcom/chase/sig/android/activity/DetailResource;->l:[Lcom/chase/sig/android/activity/DetailResource;

    invoke-virtual {v0}, [Lcom/chase/sig/android/activity/DetailResource;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/chase/sig/android/activity/DetailResource;

    return-object v0
.end method
