.class final Lcom/chase/sig/android/activity/DeviceCodeLoginActivity$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;


# direct methods
.method private constructor <init>(Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 73
    iput-object p1, p0, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity$a;->a:Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;B)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 73
    invoke-direct {p0, p1}, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity$a;-><init>(Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;)V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 5
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 78
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity$a;->a:Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;

    const v2, 0x7f0900a8

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 80
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 82
    iget-object v0, p0, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity$a;->a:Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;

    const v3, 0x7f0900a9

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 83
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 84
    const/4 v0, 0x0

    .line 86
    iget-object v4, p0, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity$a;->a:Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;

    invoke-static {v4}, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;->a(Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 87
    iget-object v0, p0, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity$a;->a:Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;->b(Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 90
    :cond_0
    invoke-static {v2}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-static {v3}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity$a;->a:Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;

    invoke-static {v2}, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;->a(Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    const/4 v0, 0x1

    .line 95
    :goto_0
    if-eqz v0, :cond_4

    .line 97
    iget-object v0, p0, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity$a;->a:Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;

    iget-object v0, v0, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;->e:Lcom/chase/sig/android/activity/mc;

    const-class v2, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity$b;

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/activity/mc;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/ae;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity$b;

    .line 98
    invoke-virtual {v0}, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity$b;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v2

    sget-object v3, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-eq v2, v3, :cond_2

    .line 99
    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity$b;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 106
    :cond_2
    :goto_1
    return-void

    :cond_3
    move v0, v1

    .line 90
    goto :goto_0

    .line 102
    :cond_4
    iget-object v1, p0, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity$a;->a:Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;

    iget-object v0, p0, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity$a;->a:Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;->a(Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;)Z

    move-result v0

    if-eqz v0, :cond_5

    const v0, 0x7f0700c1

    :goto_2
    invoke-virtual {v1, v0}, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;->g(I)V

    goto :goto_1

    :cond_5
    const v0, 0x7f0700c0

    goto :goto_2
.end method
