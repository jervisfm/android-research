.class public Lcom/chase/sig/android/activity/DeviceCodeLoginActivity$b;
.super Lcom/chase/sig/android/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/d",
        "<",
        "Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/service/GenericResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 120
    invoke-direct {p0}, Lcom/chase/sig/android/d;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7
    .parameter

    .prologue
    .line 120
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;->A()Lcom/chase/sig/android/service/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/service/n;->b()Lcom/chase/sig/android/service/r;

    move-result-object v0

    iget-object v1, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v1, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;

    invoke-virtual {v1}, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;->z()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v4

    invoke-virtual {v4}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v1

    iget-object v1, v1, Lcom/chase/sig/android/domain/o;->a:Lcom/chase/sig/android/domain/a;

    iget-object v1, v1, Lcom/chase/sig/android/domain/a;->c:Ljava/lang/String;

    iget-object v2, p0, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity$b;->a:Ljava/lang/String;

    iget-object v3, p0, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity$b;->e:Ljava/lang/String;

    invoke-virtual {v4}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v4

    iget-object v4, v4, Lcom/chase/sig/android/domain/o;->a:Lcom/chase/sig/android/domain/a;

    iget-object v4, v4, Lcom/chase/sig/android/domain/a;->d:Ljava/lang/String;

    iget-object v5, p0, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity$b;->f:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/chase/sig/android/service/r;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/service/GenericResponse;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 6
    .parameter

    .prologue
    const/4 v5, 0x0

    .line 120
    check-cast p1, Lcom/chase/sig/android/service/GenericResponse;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;->u()V

    invoke-virtual {p1}, Lcom/chase/sig/android/service/GenericResponse;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "2034"

    invoke-virtual {p1, v0}, Lcom/chase/sig/android/service/GenericResponse;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "2034"

    invoke-virtual {p1, v0}, Lcom/chase/sig/android/service/GenericResponse;->d(Ljava/lang/String;)Lcom/chase/sig/android/service/IServiceError;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    const-class v3, Lcom/chase/sig/android/activity/ECDStandInActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "ecd_error"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v0}, Lcom/chase/sig/android/service/IServiceError;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v0}, Lcom/chase/sig/android/service/IServiceError;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;->startActivity(Landroid/content/Intent;)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;->z()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v1, v0, Lcom/chase/sig/android/domain/o;->a:Lcom/chase/sig/android/domain/a;

    const/4 v0, 0x3

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/a;->a()I

    move-result v0

    :cond_1
    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    new-instance v1, Landroid/content/Intent;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    const-class v2, Lcom/chase/sig/android/activity/AccountsActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;->startActivity(Landroid/content/Intent;)V

    :goto_1
    return-void

    :cond_2
    const-string v0, "3801"

    invoke-virtual {p1, v0}, Lcom/chase/sig/android/service/GenericResponse;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v1, Landroid/content/Intent;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    const-class v2, Lcom/chase/sig/android/activity/ManagedContentActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;->z()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    const-string v3, "mbb"

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/ChaseApplication;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f07005e

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "webUrl"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/GenericResponse;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v5, v1}, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;->a(ILjava/util/List;)V

    goto :goto_1

    :cond_4
    const/4 v1, 0x1

    if-ne v0, v1, :cond_5

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;

    const v1, 0x7f0700c2

    invoke-virtual {v0, v5, v1}, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;->a(II)V

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;

    iget-object v1, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v1, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;

    const v2, 0x7f07027c

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v5, v1}, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;->a(ILjava/lang/String;)V

    goto :goto_1
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 192
    invoke-super {p0}, Lcom/chase/sig/android/d;->onPreExecute()V

    .line 194
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;

    const v1, 0x7f0900a8

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 195
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity$b;->e:Ljava/lang/String;

    .line 197
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;

    const v1, 0x7f0900a9

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 198
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity$b;->a:Ljava/lang/String;

    .line 200
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;->a(Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 201
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;->b(Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity$b;->f:Ljava/lang/String;

    .line 203
    :cond_0
    return-void
.end method
