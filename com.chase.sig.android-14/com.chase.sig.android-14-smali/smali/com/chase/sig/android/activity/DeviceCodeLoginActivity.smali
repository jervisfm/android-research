.class public Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;
.super Lcom/chase/sig/android/activity/eb;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/DeviceCodeLoginActivity$b;,
        Lcom/chase/sig/android/activity/DeviceCodeLoginActivity$a;
    }
.end annotation


# instance fields
.field a:Lcom/chase/sig/android/view/ag$a;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/OneTimePasswordContact;",
            ">;"
        }
    .end annotation
.end field

.field private c:Landroid/widget/EditText;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/chase/sig/android/activity/eb;-><init>()V

    .line 226
    new-instance v0, Lcom/chase/sig/android/activity/cn;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/activity/cn;-><init>(Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;)V

    iput-object v0, p0, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;->a:Lcom/chase/sig/android/view/ag$a;

    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;)Z
    .locals 1
    .parameter

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;->d()Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;)Landroid/widget/EditText;
    .locals 1
    .parameter

    .prologue
    .line 26
    iget-object v0, p0, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;->c:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic c(Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 26
    iget-object v0, p0, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;->b:Ljava/util/List;

    return-object v0
.end method

.method private d()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 69
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/ChaseApplication;

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->a:Lcom/chase/sig/android/domain/a;

    .line 70
    if-eqz v0, :cond_1

    iget-object v3, v0, Lcom/chase/sig/android/domain/a;->e:[Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v3, v0, Lcom/chase/sig/android/domain/a;->e:[Ljava/lang/String;

    array-length v3, v3

    if-lez v3, :cond_0

    iget-object v0, v0, Lcom/chase/sig/android/domain/a;->e:[Ljava/lang/String;

    aget-object v0, v0, v2

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    return v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1
.end method


# virtual methods
.method protected final a(I)V
    .locals 2
    .parameter

    .prologue
    .line 111
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/eb;->a(I)V

    .line 113
    if-nez p1, :cond_0

    .line 114
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/chase/sig/android/activity/LoginActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 115
    const/high16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 116
    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;->startActivity(Landroid/content/Intent;)V

    .line 118
    :cond_0
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 4
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 39
    const v0, 0x7f03002e

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;->b(I)V

    .line 41
    const v0, 0x7f0900ab

    new-instance v1, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity$a;

    invoke-direct {v1, p0, v3}, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity$a;-><init>(Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;B)V

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 43
    const v0, 0x7f0900ac

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;->i(I)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 46
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 47
    if-eqz v1, :cond_0

    const-string v0, "extra_id_code"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 48
    const v0, 0x7f0900a8

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 49
    const-string v2, "extra_id_code"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 50
    const v0, 0x7f0900a9

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 53
    :cond_0
    if-eqz v1, :cond_1

    const-string v0, "otp_contacts"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 54
    const-string v0, "otp_contacts"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;->b:Ljava/util/List;

    .line 58
    :cond_1
    invoke-direct {p0}, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f0900aa

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;->c:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;->c:Landroid/widget/EditText;

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setVisibility(I)V

    .line 59
    :cond_2
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 3
    .parameter

    .prologue
    .line 214
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/eb;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    .line 216
    packed-switch p1, :pswitch_data_0

    .line 223
    :goto_0
    return-object v0

    .line 218
    :pswitch_0
    new-instance v0, Lcom/chase/sig/android/view/ag;

    iget-object v1, p0, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;->b:Ljava/util/List;

    iget-object v2, p0, Lcom/chase/sig/android/activity/DeviceCodeLoginActivity;->a:Lcom/chase/sig/android/view/ag$a;

    invoke-direct {v0, p0, v1, v2}, Lcom/chase/sig/android/view/ag;-><init>(Landroid/content/Context;Ljava/util/List;Lcom/chase/sig/android/view/ag$a;)V

    goto :goto_0

    .line 216
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .parameter

    .prologue
    .line 208
    const/4 v0, 0x0

    return v0
.end method
