.class public Lcom/chase/sig/android/activity/DeviceCodeNotFoundActivity;
.super Lcom/chase/sig/android/activity/eb;
.source "SourceFile"


# instance fields
.field a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/OneTimePasswordContact;",
            ">;"
        }
    .end annotation
.end field

.field b:Lcom/chase/sig/android/view/ag$a;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/chase/sig/android/activity/eb;-><init>()V

    .line 66
    new-instance v0, Lcom/chase/sig/android/activity/cp;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/activity/cp;-><init>(Lcom/chase/sig/android/activity/DeviceCodeNotFoundActivity;)V

    iput-object v0, p0, Lcom/chase/sig/android/activity/DeviceCodeNotFoundActivity;->b:Lcom/chase/sig/android/view/ag$a;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 28
    const v0, 0x7f03002f

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/DeviceCodeNotFoundActivity;->b(I)V

    .line 30
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/DeviceCodeNotFoundActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 31
    if-eqz v0, :cond_0

    const-string v1, "otp_contacts"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 32
    const-string v1, "otp_contacts"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/chase/sig/android/activity/DeviceCodeNotFoundActivity;->a:Ljava/util/List;

    .line 35
    :cond_0
    const v0, 0x7f0900ae

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/DeviceCodeNotFoundActivity;->i(I)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/DeviceCodeNotFoundActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 37
    const v0, 0x7f0900af

    new-instance v1, Lcom/chase/sig/android/activity/co;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/co;-><init>(Lcom/chase/sig/android/activity/DeviceCodeNotFoundActivity;)V

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/DeviceCodeNotFoundActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 47
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 3
    .parameter

    .prologue
    .line 57
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/eb;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    .line 59
    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    .line 60
    new-instance v0, Lcom/chase/sig/android/view/ag;

    iget-object v1, p0, Lcom/chase/sig/android/activity/DeviceCodeNotFoundActivity;->a:Ljava/util/List;

    iget-object v2, p0, Lcom/chase/sig/android/activity/DeviceCodeNotFoundActivity;->b:Lcom/chase/sig/android/view/ag$a;

    invoke-direct {v0, p0, v1, v2}, Lcom/chase/sig/android/view/ag;-><init>(Landroid/content/Context;Ljava/util/List;Lcom/chase/sig/android/view/ag$a;)V

    .line 63
    :cond_0
    return-object v0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .parameter

    .prologue
    .line 51
    const/4 v0, 0x0

    return v0
.end method
