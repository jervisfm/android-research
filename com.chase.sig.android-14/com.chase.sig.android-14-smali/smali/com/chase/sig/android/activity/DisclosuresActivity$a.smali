.class public Lcom/chase/sig/android/activity/DisclosuresActivity$a;
.super Lcom/chase/sig/android/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/DisclosuresActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/d",
        "<",
        "Lcom/chase/sig/android/activity/DisclosuresActivity;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/service/disclosures/DisclosuresResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 99
    invoke-direct {p0}, Lcom/chase/sig/android/d;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .parameter

    .prologue
    .line 99
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/DisclosuresActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/DisclosuresActivity;->A()Lcom/chase/sig/android/service/n;

    move-result-object v0

    iget-object v1, v0, Lcom/chase/sig/android/service/n;->i:Lcom/chase/sig/android/service/disclosures/a;

    if-nez v1, :cond_0

    new-instance v1, Lcom/chase/sig/android/service/disclosures/a;

    invoke-direct {v1}, Lcom/chase/sig/android/service/disclosures/a;-><init>()V

    iput-object v1, v0, Lcom/chase/sig/android/service/n;->i:Lcom/chase/sig/android/service/disclosures/a;

    :cond_0
    iget-object v0, v0, Lcom/chase/sig/android/service/n;->i:Lcom/chase/sig/android/service/disclosures/a;

    invoke-static {}, Lcom/chase/sig/android/service/disclosures/a;->a()Lcom/chase/sig/android/service/disclosures/DisclosuresResponse;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 2
    .parameter

    .prologue
    .line 99
    check-cast p1, Lcom/chase/sig/android/service/disclosures/DisclosuresResponse;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/chase/sig/android/service/disclosures/DisclosuresResponse;->a()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/chase/sig/android/service/disclosures/DisclosuresResponse;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/DisclosuresActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/DisclosuresActivity;->b(Lcom/chase/sig/android/activity/DisclosuresActivity;)Landroid/widget/TextView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/chase/sig/android/service/disclosures/DisclosuresResponse;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/DisclosuresActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/disclosures/DisclosuresResponse;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/DisclosuresActivity;->b(Ljava/util/List;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/DisclosuresActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/disclosures/DisclosuresResponse;->a()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/DisclosuresActivity;->a(Ljava/util/List;)V

    goto :goto_0
.end method
