.class public Lcom/chase/sig/android/activity/DisclosuresActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/DisclosuresActivity$a;
    }
.end annotation


# instance fields
.field private a:Landroid/widget/ListView;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/Disclosure;",
            ">;"
        }
    .end annotation
.end field

.field private c:Landroid/widget/TextView;

.field private d:Ljava/lang/String;

.field private k:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    .line 37
    const-string v0, "Loading"

    iput-object v0, p0, Lcom/chase/sig/android/activity/DisclosuresActivity;->d:Ljava/lang/String;

    .line 99
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/DisclosuresActivity;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 32
    iget-object v0, p0, Lcom/chase/sig/android/activity/DisclosuresActivity;->b:Ljava/util/List;

    return-object v0
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/DisclosuresActivity;)Landroid/widget/TextView;
    .locals 1
    .parameter

    .prologue
    .line 32
    iget-object v0, p0, Lcom/chase/sig/android/activity/DisclosuresActivity;->c:Landroid/widget/TextView;

    return-object v0
.end method

.method private d()V
    .locals 7

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 126
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 127
    iget-object v0, p0, Lcom/chase/sig/android/activity/DisclosuresActivity;->b:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/DisclosuresActivity;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 128
    iget-object v0, p0, Lcom/chase/sig/android/activity/DisclosuresActivity;->a:Landroid/widget/ListView;

    invoke-virtual {v0, v6}, Landroid/widget/ListView;->setVisibility(I)V

    .line 129
    iget-object v0, p0, Lcom/chase/sig/android/activity/DisclosuresActivity;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/Disclosure;

    .line 130
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 131
    const-string v4, "title"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Disclosure;->c()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 135
    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/DisclosuresActivity;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 138
    :cond_1
    new-array v4, v5, [Ljava/lang/String;

    const-string v0, "title"

    aput-object v0, v4, v6

    .line 141
    new-array v5, v5, [I

    const v0, 0x7f0900bc

    aput v0, v5, v6

    .line 145
    new-instance v0, Landroid/widget/SimpleAdapter;

    const v3, 0x7f030035

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Landroid/widget/SimpleAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V

    .line 147
    iget-object v1, p0, Lcom/chase/sig/android/activity/DisclosuresActivity;->a:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 148
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 4
    .parameter

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 44
    const v0, 0x7f030033

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/DisclosuresActivity;->b(I)V

    .line 46
    const v0, 0x7f0900b8

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/DisclosuresActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/DisclosuresActivity;->a:Landroid/widget/ListView;

    .line 47
    const v0, 0x7f0900b9

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/DisclosuresActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/DisclosuresActivity;->c:Landroid/widget/TextView;

    .line 48
    const-string v0, "footNote"

    invoke-static {p1, v0, v3}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/chase/sig/android/activity/DisclosuresActivity;->d:Ljava/lang/String;

    .line 50
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/DisclosuresActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "title"

    invoke-static {v0, v1, v3}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/chase/sig/android/activity/DisclosuresActivity;->k:Ljava/lang/String;

    .line 51
    iget-object v0, p0, Lcom/chase/sig/android/activity/DisclosuresActivity;->k:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 52
    iget-object v0, p0, Lcom/chase/sig/android/activity/DisclosuresActivity;->k:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/DisclosuresActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 56
    :goto_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/DisclosuresActivity;->e:Lcom/chase/sig/android/activity/mc;

    const-class v1, Lcom/chase/sig/android/activity/DisclosuresActivity$a;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/mc;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/ae;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/activity/DisclosuresActivity$a;

    .line 57
    invoke-virtual {v0}, Lcom/chase/sig/android/activity/DisclosuresActivity$a;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v1

    sget-object v3, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-eq v1, v3, :cond_2

    const/4 v1, 0x1

    .line 58
    :goto_1
    if-nez p1, :cond_3

    if-eqz v1, :cond_3

    .line 59
    new-array v1, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/DisclosuresActivity$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 65
    :cond_0
    :goto_2
    iget-object v0, p0, Lcom/chase/sig/android/activity/DisclosuresActivity;->a:Landroid/widget/ListView;

    new-instance v1, Lcom/chase/sig/android/activity/cq;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/cq;-><init>(Lcom/chase/sig/android/activity/DisclosuresActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 88
    return-void

    .line 54
    :cond_1
    const v0, 0x7f070289

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/DisclosuresActivity;->setTitle(I)V

    goto :goto_0

    :cond_2
    move v1, v2

    .line 57
    goto :goto_1

    .line 60
    :cond_3
    if-eqz v1, :cond_0

    .line 61
    const-string v0, "disclosures"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/chase/sig/android/activity/DisclosuresActivity;->b:Ljava/util/List;

    .line 62
    invoke-direct {p0}, Lcom/chase/sig/android/activity/DisclosuresActivity;->d()V

    goto :goto_2
.end method

.method public final a(Ljava/util/List;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/Disclosure;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 152
    iput-object p1, p0, Lcom/chase/sig/android/activity/DisclosuresActivity;->b:Ljava/util/List;

    .line 154
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/DisclosuresActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "dislosure id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/DisclosuresActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "dislosure id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/chase/sig/android/util/r;

    invoke-direct {v1}, Lcom/chase/sig/android/util/r;-><init>()V

    new-instance v1, Lcom/chase/sig/android/activity/cr;

    invoke-direct {v1, p0, v0}, Lcom/chase/sig/android/activity/cr;-><init>(Lcom/chase/sig/android/activity/DisclosuresActivity;Ljava/lang/String;)V

    invoke-static {p1, v1}, Lcom/chase/sig/android/util/r;->a(Ljava/util/List;Lcom/chase/sig/android/util/q;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/Disclosure;

    .line 156
    :goto_0
    if-nez v0, :cond_1

    .line 157
    invoke-direct {p0}, Lcom/chase/sig/android/activity/DisclosuresActivity;->d()V

    .line 165
    :goto_1
    return-void

    .line 154
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 159
    :cond_1
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/chase/sig/android/activity/DisclosuresDetailActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 161
    const/high16 v2, 0x4000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 162
    invoke-static {v1, v0}, Lcom/chase/sig/android/util/e;->a(Landroid/content/Intent;Ljava/io/Serializable;)V

    .line 163
    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/DisclosuresActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 93
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ai;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 95
    const-string v1, "disclosures"

    iget-object v0, p0, Lcom/chase/sig/android/activity/DisclosuresActivity;->b:Ljava/util/List;

    check-cast v0, Ljava/io/Serializable;

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 96
    const-string v0, "footNote"

    iget-object v1, p0, Lcom/chase/sig/android/activity/DisclosuresActivity;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    return-void
.end method
