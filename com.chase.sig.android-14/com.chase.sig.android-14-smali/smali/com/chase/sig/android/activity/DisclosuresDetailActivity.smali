.class public Lcom/chase/sig/android/activity/DisclosuresDetailActivity;
.super Lcom/chase/sig/android/activity/eb;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/chase/sig/android/activity/eb;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 5
    .parameter

    .prologue
    .line 18
    const v0, 0x7f070289

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/DisclosuresDetailActivity;->setTitle(I)V

    .line 19
    const v0, 0x7f030034

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/DisclosuresDetailActivity;->b(I)V

    .line 21
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/DisclosuresDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-class v1, Lcom/chase/sig/android/domain/Disclosure;

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/Disclosure;

    .line 23
    const v1, 0x7f0900ba

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/DisclosuresDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 24
    const v2, 0x7f0900bb

    invoke-virtual {p0, v2}, Lcom/chase/sig/android/activity/DisclosuresDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 26
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "*"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Disclosure;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 27
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Disclosure;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 28
    return-void
.end method
