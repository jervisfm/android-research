.class public Lcom/chase/sig/android/activity/EPayAuthorizeActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$e;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/EPayAuthorizeActivity$a;
    }
.end annotation


# instance fields
.field private a:Lcom/chase/sig/android/domain/EPayment;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    .line 62
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/EPayAuthorizeActivity;)Lcom/chase/sig/android/domain/EPayment;
    .locals 1
    .parameter

    .prologue
    .line 16
    iget-object v0, p0, Lcom/chase/sig/android/activity/EPayAuthorizeActivity;->a:Lcom/chase/sig/android/domain/EPayment;

    return-object v0
.end method


# virtual methods
.method protected final a(I)V
    .locals 2
    .parameter

    .prologue
    .line 51
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ai;->a(I)V

    .line 53
    if-nez p1, :cond_0

    .line 54
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/chase/sig/android/activity/EPayStartActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 56
    const/high16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 58
    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/EPayAuthorizeActivity;->startActivity(Landroid/content/Intent;)V

    .line 60
    :cond_0
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 24
    const v0, 0x7f03003c

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/EPayAuthorizeActivity;->b(I)V

    .line 25
    const v0, 0x7f0701ac

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/EPayAuthorizeActivity;->setTitle(I)V

    .line 26
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/EPayAuthorizeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 28
    sget-object v1, Lcom/chase/sig/android/activity/EPayVerifyActivity;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/EPayment;

    iput-object v0, p0, Lcom/chase/sig/android/activity/EPayAuthorizeActivity;->a:Lcom/chase/sig/android/domain/EPayment;

    .line 30
    const v0, 0x7f0900cc

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/EPayAuthorizeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 31
    iget-object v1, p0, Lcom/chase/sig/android/activity/EPayAuthorizeActivity;->a:Lcom/chase/sig/android/domain/EPayment;

    if-eqz v1, :cond_0

    .line 32
    iget-object v1, p0, Lcom/chase/sig/android/activity/EPayAuthorizeActivity;->a:Lcom/chase/sig/android/domain/EPayment;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/EPayment;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 35
    :cond_0
    const v0, 0x7f0900c9

    new-instance v1, Lcom/chase/sig/android/activity/cs;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/cs;-><init>(Lcom/chase/sig/android/activity/EPayAuthorizeActivity;)V

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/EPayAuthorizeActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 46
    const v0, 0x7f0900ca

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/EPayAuthorizeActivity;->B()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/EPayAuthorizeActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 47
    return-void
.end method
