.class public Lcom/chase/sig/android/activity/EPayConfirmationActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$e;


# instance fields
.field private a:Lcom/chase/sig/android/view/detail/DetailView;

.field private b:Lcom/chase/sig/android/service/epay/EPayTransaction;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 14
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    .line 16
    iput-object v0, p0, Lcom/chase/sig/android/activity/EPayConfirmationActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    .line 18
    iput-object v0, p0, Lcom/chase/sig/android/activity/EPayConfirmationActivity;->b:Lcom/chase/sig/android/service/epay/EPayTransaction;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 8
    .parameter

    .prologue
    const v4, 0x7f070074

    const/4 v1, 0x1

    const/4 v2, 0x0

    const v7, 0x7f060010

    const v6, 0x7f060001

    .line 22
    const v0, 0x7f030039

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/EPayConfirmationActivity;->b(I)V

    .line 23
    invoke-virtual {p0, v4}, Lcom/chase/sig/android/activity/EPayConfirmationActivity;->setTitle(I)V

    .line 25
    const v0, 0x7f0900c2

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/EPayConfirmationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/EPayConfirmationActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    .line 26
    if-eqz p1, :cond_0

    if-eqz p1, :cond_1

    move v0, v1

    :goto_0
    const-string v3, "bundleCancelConfirm"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    move v3, v1

    :goto_1
    and-int/2addr v0, v3

    if-eqz v0, :cond_3

    .line 29
    :cond_0
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/EPayConfirmationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "ePayCancelConfirm"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/service/epay/EPayTransaction;

    iput-object v0, p0, Lcom/chase/sig/android/activity/EPayConfirmationActivity;->b:Lcom/chase/sig/android/service/epay/EPayTransaction;

    .line 36
    :goto_2
    const/4 v0, 0x6

    new-array v3, v0, [Lcom/chase/sig/android/view/detail/DetailRow;

    new-instance v0, Lcom/chase/sig/android/view/detail/DetailRow;

    invoke-virtual {p0, v4}, Lcom/chase/sig/android/activity/EPayConfirmationActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/chase/sig/android/activity/EPayConfirmationActivity;->b:Lcom/chase/sig/android/service/epay/EPayTransaction;

    invoke-virtual {v5}, Lcom/chase/sig/android/service/epay/EPayTransaction;->n()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v4, v5}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput v7, v0, Lcom/chase/sig/android/view/detail/a;->e:I

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailRow;

    invoke-virtual {v0, v6}, Lcom/chase/sig/android/view/detail/DetailRow;->b(I)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailRow;

    aput-object v0, v3, v2

    new-instance v0, Lcom/chase/sig/android/view/detail/DetailRow;

    const v2, 0x7f070075

    invoke-virtual {p0, v2}, Lcom/chase/sig/android/activity/EPayConfirmationActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/chase/sig/android/activity/EPayConfirmationActivity;->b:Lcom/chase/sig/android/service/epay/EPayTransaction;

    invoke-virtual {v4}, Lcom/chase/sig/android/service/epay/EPayTransaction;->d()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v2, v4}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput v7, v0, Lcom/chase/sig/android/view/detail/a;->e:I

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailRow;

    invoke-virtual {v0, v6}, Lcom/chase/sig/android/view/detail/DetailRow;->b(I)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailRow;

    aput-object v0, v3, v1

    const/4 v1, 0x2

    new-instance v0, Lcom/chase/sig/android/view/detail/DetailRow;

    const v2, 0x7f070076

    invoke-virtual {p0, v2}, Lcom/chase/sig/android/activity/EPayConfirmationActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/chase/sig/android/activity/EPayConfirmationActivity;->b:Lcom/chase/sig/android/service/epay/EPayTransaction;

    invoke-virtual {v4}, Lcom/chase/sig/android/service/epay/EPayTransaction;->s()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v2, v4}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput v7, v0, Lcom/chase/sig/android/view/detail/a;->e:I

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailRow;

    invoke-virtual {v0, v6}, Lcom/chase/sig/android/view/detail/DetailRow;->b(I)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailRow;

    aput-object v0, v3, v1

    const/4 v1, 0x3

    new-instance v0, Lcom/chase/sig/android/view/detail/DetailRow;

    const v2, 0x7f070077

    invoke-virtual {p0, v2}, Lcom/chase/sig/android/activity/EPayConfirmationActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/chase/sig/android/activity/EPayConfirmationActivity;->b:Lcom/chase/sig/android/service/epay/EPayTransaction;

    invoke-virtual {v4}, Lcom/chase/sig/android/service/epay/EPayTransaction;->e()Lcom/chase/sig/android/util/Dollar;

    move-result-object v4

    invoke-virtual {v4}, Lcom/chase/sig/android/util/Dollar;->h()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v2, v4}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput v7, v0, Lcom/chase/sig/android/view/detail/a;->e:I

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailRow;

    invoke-virtual {v0, v6}, Lcom/chase/sig/android/view/detail/DetailRow;->b(I)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailRow;

    aput-object v0, v3, v1

    const/4 v1, 0x4

    new-instance v0, Lcom/chase/sig/android/view/detail/DetailRow;

    const v2, 0x7f070078

    invoke-virtual {p0, v2}, Lcom/chase/sig/android/activity/EPayConfirmationActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/chase/sig/android/activity/EPayConfirmationActivity;->b:Lcom/chase/sig/android/service/epay/EPayTransaction;

    invoke-virtual {v4}, Lcom/chase/sig/android/service/epay/EPayTransaction;->q()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/chase/sig/android/util/s;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v2, v4}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput v7, v0, Lcom/chase/sig/android/view/detail/a;->e:I

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailRow;

    invoke-virtual {v0, v6}, Lcom/chase/sig/android/view/detail/DetailRow;->b(I)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailRow;

    aput-object v0, v3, v1

    const/4 v1, 0x5

    new-instance v0, Lcom/chase/sig/android/view/detail/DetailRow;

    const v2, 0x7f070079

    invoke-virtual {p0, v2}, Lcom/chase/sig/android/activity/EPayConfirmationActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/chase/sig/android/activity/EPayConfirmationActivity;->b:Lcom/chase/sig/android/service/epay/EPayTransaction;

    invoke-virtual {v4}, Lcom/chase/sig/android/service/epay/EPayTransaction;->t()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v2, v4}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput v7, v0, Lcom/chase/sig/android/view/detail/a;->e:I

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailRow;

    invoke-virtual {v0, v6}, Lcom/chase/sig/android/view/detail/DetailRow;->b(I)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailRow;

    aput-object v0, v3, v1

    .line 51
    iget-object v0, p0, Lcom/chase/sig/android/activity/EPayConfirmationActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/view/detail/DetailView;->setRows([Lcom/chase/sig/android/view/detail/a;)V

    .line 53
    const v0, 0x7f0900c3

    const-class v1, Lcom/chase/sig/android/activity/EPayHistoryActivity;

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/EPayConfirmationActivity;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/a/f;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/EPayConfirmationActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 55
    const v0, 0x7f0900c4

    const-class v1, Lcom/chase/sig/android/activity/EPaySelectToAccount;

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/EPayConfirmationActivity;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/a/f;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/EPayConfirmationActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 57
    return-void

    :cond_1
    move v0, v2

    .line 26
    goto/16 :goto_0

    :cond_2
    move v3, v2

    goto/16 :goto_1

    .line 32
    :cond_3
    const-string v0, "bundleCancelConfirm"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/service/epay/EPayTransaction;

    iput-object v0, p0, Lcom/chase/sig/android/activity/EPayConfirmationActivity;->b:Lcom/chase/sig/android/service/epay/EPayTransaction;

    goto/16 :goto_2
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 66
    invoke-virtual {p0, p1}, Lcom/chase/sig/android/activity/EPayConfirmationActivity;->j(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 69
    :goto_0
    return v0

    .line 68
    :cond_0
    const-class v0, Lcom/chase/sig/android/activity/EPayHomeActivity;

    invoke-static {p1, p0, v0}, Lcom/chase/sig/android/a/a;->a(ILcom/chase/sig/android/activity/eb;Ljava/lang/Class;)V

    .line 69
    invoke-super {p0, p1, p2}, Lcom/chase/sig/android/activity/ai;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 61
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ai;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 62
    const-string v0, "bundleCancelConfirm"

    iget-object v1, p0, Lcom/chase/sig/android/activity/EPayConfirmationActivity;->b:Lcom/chase/sig/android/service/epay/EPayTransaction;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 63
    return-void
.end method
