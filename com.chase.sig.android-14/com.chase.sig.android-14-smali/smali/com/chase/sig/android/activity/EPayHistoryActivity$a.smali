.class public Lcom/chase/sig/android/activity/EPayHistoryActivity$a;
.super Lcom/chase/sig/android/activity/l$a;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/EPayHistoryActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/activity/l$a",
        "<",
        "Lcom/chase/sig/android/service/epay/EPayTransaction;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 84
    invoke-direct {p0}, Lcom/chase/sig/android/activity/l$a;-><init>()V

    return-void
.end method


# virtual methods
.method protected final varargs a([Ljava/lang/Integer;)Lcom/chase/sig/android/service/movemoney/ListServiceResponse;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Integer;",
            ")",
            "Lcom/chase/sig/android/service/movemoney/ListServiceResponse",
            "<",
            "Lcom/chase/sig/android/service/epay/EPayTransaction;",
            ">;"
        }
    .end annotation

    .prologue
    .line 94
    const/4 v0, 0x0

    aget-object v0, p1, v0

    .line 95
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/EPayHistoryActivity$a;->a()Lcom/chase/sig/android/service/movemoney/d;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/service/movemoney/d;->c(I)Lcom/chase/sig/android/service/movemoney/ListServiceResponse;

    move-result-object v0

    return-object v0
.end method

.method protected final a()Lcom/chase/sig/android/service/movemoney/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/chase/sig/android/service/movemoney/d",
            "<",
            "Lcom/chase/sig/android/service/epay/EPayTransaction;",
            ">;"
        }
    .end annotation

    .prologue
    .line 89
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/l;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/l;->A()Lcom/chase/sig/android/service/n;

    invoke-static {}, Lcom/chase/sig/android/service/n;->h()Lcom/chase/sig/android/service/epay/a;

    move-result-object v0

    return-object v0
.end method

.method protected final bridge synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 84
    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/chase/sig/android/activity/EPayHistoryActivity$a;->a([Ljava/lang/Integer;)Lcom/chase/sig/android/service/movemoney/ListServiceResponse;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Lcom/chase/sig/android/service/movemoney/ListServiceResponse;)V
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/chase/sig/android/service/movemoney/ListServiceResponse",
            "<",
            "Lcom/chase/sig/android/service/epay/EPayTransaction;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 99
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/l;

    iget-object v0, v0, Lcom/chase/sig/android/activity/l;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 100
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/l$a;->a(Lcom/chase/sig/android/service/movemoney/ListServiceResponse;)V

    .line 101
    return-void
.end method

.method protected final bridge synthetic a(Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 84
    check-cast p1, Lcom/chase/sig/android/service/movemoney/ListServiceResponse;

    invoke-virtual {p0, p1}, Lcom/chase/sig/android/activity/EPayHistoryActivity$a;->a(Lcom/chase/sig/android/service/movemoney/ListServiceResponse;)V

    return-void
.end method
