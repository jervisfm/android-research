.class public Lcom/chase/sig/android/activity/EPayHistoryActivity;
.super Lcom/chase/sig/android/activity/l;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/EPayHistoryActivity$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/activity/l",
        "<",
        "Lcom/chase/sig/android/service/epay/EPayTransaction;",
        ">;"
    }
.end annotation


# instance fields
.field private k:Lcom/chase/sig/android/view/JPSpinner;

.field private l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/IAccount;",
            ">;"
        }
    .end annotation
.end field

.field private m:Lcom/chase/sig/android/domain/g;

.field private n:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/chase/sig/android/activity/l;-><init>()V

    .line 84
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/EPayHistoryActivity;)I
    .locals 1
    .parameter

    .prologue
    .line 23
    iget v0, p0, Lcom/chase/sig/android/activity/EPayHistoryActivity;->n:I

    return v0
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/EPayHistoryActivity;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 23
    iput p1, p0, Lcom/chase/sig/android/activity/EPayHistoryActivity;->n:I

    return p1
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/EPayHistoryActivity;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, Lcom/chase/sig/android/activity/EPayHistoryActivity;->l:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method protected final a()I
    .locals 1

    .prologue
    .line 66
    const v0, 0x7f0701b2

    return v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 10
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v9, 0x0

    .line 33
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/l;->a(Landroid/os/Bundle;)V

    .line 35
    const v0, 0x7f0902d3

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/EPayHistoryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    .line 37
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/ChaseApplication;

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->b:Lcom/chase/sig/android/domain/g;

    iput-object v0, p0, Lcom/chase/sig/android/activity/EPayHistoryActivity;->m:Lcom/chase/sig/android/domain/g;

    .line 39
    const v0, 0x7f0902d4

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/EPayHistoryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/JPSpinner;

    iput-object v0, p0, Lcom/chase/sig/android/activity/EPayHistoryActivity;->k:Lcom/chase/sig/android/view/JPSpinner;

    .line 40
    iget-object v0, p0, Lcom/chase/sig/android/activity/EPayHistoryActivity;->m:Lcom/chase/sig/android/domain/g;

    invoke-interface {v0}, Lcom/chase/sig/android/domain/g;->u()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/EPayHistoryActivity;->l:Ljava/util/List;

    .line 42
    new-array v4, v1, [Ljava/lang/String;

    const-string v0, "account_name"

    aput-object v0, v4, v9

    new-array v5, v1, [I

    const v0, 0x7f09010c

    aput v0, v5, v9

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/chase/sig/android/activity/EPayHistoryActivity;->l:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/chase/sig/android/activity/EPayHistoryActivity;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/chase/sig/android/activity/EPayHistoryActivity;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/IAccount;

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->g()Z

    move-result v6

    if-nez v6, :cond_0

    const-string v6, "account_name"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->a()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->y()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v6, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_1
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object v6, p0, Lcom/chase/sig/android/activity/EPayHistoryActivity;->m:Lcom/chase/sig/android/domain/g;

    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->b()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Lcom/chase/sig/android/domain/g;->e(Ljava/lang/String;)Lcom/chase/sig/android/domain/IAccount;

    move-result-object v6

    const-string v7, "account_name"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {v6}, Lcom/chase/sig/android/domain/IAccount;->y()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v7, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_1
    new-instance v0, Landroid/widget/SimpleAdapter;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/EPayHistoryActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    const v3, 0x7f030051

    invoke-direct/range {v0 .. v5}, Landroid/widget/SimpleAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V

    iget-object v1, p0, Lcom/chase/sig/android/activity/EPayHistoryActivity;->k:Lcom/chase/sig/android/view/JPSpinner;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/view/JPSpinner;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 44
    iget-object v0, p0, Lcom/chase/sig/android/activity/EPayHistoryActivity;->k:Lcom/chase/sig/android/view/JPSpinner;

    new-instance v1, Lcom/chase/sig/android/activity/cu;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/cu;-><init>(Lcom/chase/sig/android/activity/EPayHistoryActivity;)V

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/JPSpinner;->a(Lcom/chase/sig/android/view/x;)V

    .line 54
    const-string v0, "payment_items"

    invoke-static {p1, v0}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "spinner_selection"

    invoke-static {p1, v0}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 55
    invoke-virtual {p0, p1}, Lcom/chase/sig/android/activity/EPayHistoryActivity;->c(Landroid/os/Bundle;)V

    .line 56
    const-string v0, "spinner_selection"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/chase/sig/android/activity/EPayHistoryActivity;->n:I

    .line 63
    :cond_2
    :goto_2
    return-void

    .line 58
    :cond_3
    const/4 v0, -0x1

    iput v0, p0, Lcom/chase/sig/android/activity/EPayHistoryActivity;->n:I

    .line 59
    iget-object v0, p0, Lcom/chase/sig/android/activity/EPayHistoryActivity;->l:Ljava/util/List;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/chase/sig/android/activity/EPayHistoryActivity;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 60
    iget-object v0, p0, Lcom/chase/sig/android/activity/EPayHistoryActivity;->k:Lcom/chase/sig/android/view/JPSpinner;

    invoke-virtual {v0, v9}, Lcom/chase/sig/android/view/JPSpinner;->setSelection(I)V

    goto :goto_2
.end method

.method protected final synthetic a(Lcom/chase/sig/android/domain/Transaction;)V
    .locals 3
    .parameter

    .prologue
    .line 23
    check-cast p1, Lcom/chase/sig/android/service/epay/EPayTransaction;

    iget-object v0, p0, Lcom/chase/sig/android/activity/EPayHistoryActivity;->l:Ljava/util/List;

    iget-object v1, p0, Lcom/chase/sig/android/activity/EPayHistoryActivity;->k:Lcom/chase/sig/android/view/JPSpinner;

    invoke-virtual {v1}, Lcom/chase/sig/android/view/JPSpinner;->getSelectedItemPosition()I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/IAccount;

    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->g()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->y()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/chase/sig/android/service/epay/EPayTransaction;->t(Ljava/lang/String;)V

    :goto_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/EPayHistoryActivity;->l:Ljava/util/List;

    iget-object v1, p0, Lcom/chase/sig/android/activity/EPayHistoryActivity;->k:Lcom/chase/sig/android/view/JPSpinner;

    invoke-virtual {v1}, Lcom/chase/sig/android/view/JPSpinner;->getSelectedItemPosition()I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/IAccount;

    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/chase/sig/android/service/epay/EPayTransaction;->u(Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/l;->a(Lcom/chase/sig/android/domain/Transaction;)V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/chase/sig/android/activity/EPayHistoryActivity;->m:Lcom/chase/sig/android/domain/g;

    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/chase/sig/android/domain/g;->e(Ljava/lang/String;)Lcom/chase/sig/android/domain/IAccount;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {v1}, Lcom/chase/sig/android/domain/IAccount;->y()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/chase/sig/android/service/epay/EPayTransaction;->t(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected final b()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/chase/sig/android/activity/l$a",
            "<",
            "Lcom/chase/sig/android/service/epay/EPayTransaction;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 77
    const-class v0, Lcom/chase/sig/android/activity/EPayHistoryActivity$a;

    return-object v0
.end method

.method protected final c()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/chase/sig/android/activity/eb;",
            ">;"
        }
    .end annotation

    .prologue
    .line 81
    const-class v0, Lcom/chase/sig/android/activity/EPayHistoryDetailActivity;

    return-object v0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 71
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/l;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 72
    const-string v0, "spinner_selection"

    iget-object v1, p0, Lcom/chase/sig/android/activity/EPayHistoryActivity;->k:Lcom/chase/sig/android/view/JPSpinner;

    invoke-virtual {v1}, Lcom/chase/sig/android/view/JPSpinner;->getSelectedItemPosition()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 73
    return-void
.end method
