.class public Lcom/chase/sig/android/activity/EPayHistoryDetailActivity$a;
.super Lcom/chase/sig/android/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/EPayHistoryDetailActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/d",
        "<",
        "Lcom/chase/sig/android/activity/EPayHistoryDetailActivity;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/service/movemoney/ServiceResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Lcom/chase/sig/android/service/epay/EPayTransaction;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 96
    invoke-direct {p0}, Lcom/chase/sig/android/d;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .parameter

    .prologue
    .line 96
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/EPayHistoryDetailActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/EPayHistoryDetailActivity;->a(Lcom/chase/sig/android/activity/EPayHistoryDetailActivity;)Lcom/chase/sig/android/service/epay/EPayTransaction;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/EPayHistoryDetailActivity$a;->a:Lcom/chase/sig/android/service/epay/EPayTransaction;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/EPayHistoryDetailActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/EPayHistoryDetailActivity;->A()Lcom/chase/sig/android/service/n;

    invoke-static {}, Lcom/chase/sig/android/service/n;->h()Lcom/chase/sig/android/service/epay/a;

    move-result-object v0

    iget-object v1, p0, Lcom/chase/sig/android/activity/EPayHistoryDetailActivity$a;->a:Lcom/chase/sig/android/service/epay/EPayTransaction;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/epay/a;->a(Lcom/chase/sig/android/domain/Transaction;)Lcom/chase/sig/android/service/movemoney/ServiceResponse;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 3
    .parameter

    .prologue
    .line 96
    check-cast p1, Lcom/chase/sig/android/service/movemoney/ServiceResponse;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/chase/sig/android/service/movemoney/ServiceResponse;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/EPayHistoryDetailActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/movemoney/ServiceResponse;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/EPayHistoryDetailActivity;->c(Ljava/util/List;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/EPayHistoryDetailActivity$a;->a:Lcom/chase/sig/android/service/epay/EPayTransaction;

    const-string v1, "Cancelled"

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/epay/EPayTransaction;->q(Ljava/lang/String;)V

    new-instance v1, Landroid/content/Intent;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    const-class v2, Lcom/chase/sig/android/activity/EPayConfirmationActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v0, 0x400

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v0, "ePayCancelConfirm"

    iget-object v2, p0, Lcom/chase/sig/android/activity/EPayHistoryDetailActivity$a;->a:Lcom/chase/sig/android/service/epay/EPayTransaction;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/EPayHistoryDetailActivity;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/EPayHistoryDetailActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
