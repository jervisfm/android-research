.class public Lcom/chase/sig/android/activity/EPayHistoryDetailActivity;
.super Lcom/chase/sig/android/activity/n;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/EPayHistoryDetailActivity$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/activity/n",
        "<",
        "Lcom/chase/sig/android/service/epay/EPayTransaction;",
        ">;"
    }
.end annotation


# instance fields
.field private d:Lcom/chase/sig/android/service/epay/EPayTransaction;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/chase/sig/android/activity/n;-><init>()V

    .line 27
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/chase/sig/android/activity/EPayHistoryDetailActivity;->d:Lcom/chase/sig/android/service/epay/EPayTransaction;

    .line 96
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/EPayHistoryDetailActivity;)Lcom/chase/sig/android/service/epay/EPayTransaction;
    .locals 1
    .parameter

    .prologue
    .line 24
    iget-object v0, p0, Lcom/chase/sig/android/activity/EPayHistoryDetailActivity;->d:Lcom/chase/sig/android/service/epay/EPayTransaction;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 8
    .parameter

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 31
    const v0, 0x7f0300c6

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/EPayHistoryDetailActivity;->b(I)V

    .line 32
    const v0, 0x7f0701b3

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/EPayHistoryDetailActivity;->setTitle(I)V

    .line 33
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/EPayHistoryDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "transaction_object"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/service/epay/EPayTransaction;

    iput-object v0, p0, Lcom/chase/sig/android/activity/EPayHistoryDetailActivity;->d:Lcom/chase/sig/android/service/epay/EPayTransaction;

    .line 36
    const v0, 0x7f090153

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/EPayHistoryDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailView;

    .line 38
    const/4 v1, 0x7

    new-array v4, v1, [Lcom/chase/sig/android/view/detail/a;

    new-instance v1, Lcom/chase/sig/android/view/detail/DetailRow;

    iget-object v5, p0, Lcom/chase/sig/android/activity/EPayHistoryDetailActivity;->d:Lcom/chase/sig/android/service/epay/EPayTransaction;

    invoke-virtual {v5}, Lcom/chase/sig/android/service/epay/EPayTransaction;->d()Ljava/lang/String;

    move-result-object v5

    const-string v6, ""

    invoke-direct {v1, v5, v6}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const v5, 0x7f060001

    iput v5, v1, Lcom/chase/sig/android/view/detail/a;->e:I

    check-cast v1, Lcom/chase/sig/android/view/detail/DetailRow;

    const v5, 0x7f060010

    invoke-virtual {v1, v5}, Lcom/chase/sig/android/view/detail/DetailRow;->b(I)Lcom/chase/sig/android/view/detail/a;

    move-result-object v1

    aput-object v1, v4, v2

    const/4 v1, 0x1

    new-instance v5, Lcom/chase/sig/android/view/detail/DetailColoredValueRow;

    const-string v6, "Payment Date"

    iget-object v7, p0, Lcom/chase/sig/android/activity/EPayHistoryDetailActivity;->d:Lcom/chase/sig/android/service/epay/EPayTransaction;

    invoke-virtual {v7}, Lcom/chase/sig/android/service/epay/EPayTransaction;->q()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/chase/sig/android/util/s;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lcom/chase/sig/android/view/detail/DetailColoredValueRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v4, v1

    const/4 v1, 0x2

    new-instance v5, Lcom/chase/sig/android/view/detail/DetailColoredValueRow;

    const-string v6, "Confirmation"

    iget-object v7, p0, Lcom/chase/sig/android/activity/EPayHistoryDetailActivity;->d:Lcom/chase/sig/android/service/epay/EPayTransaction;

    invoke-virtual {v7}, Lcom/chase/sig/android/service/epay/EPayTransaction;->n()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lcom/chase/sig/android/view/detail/DetailColoredValueRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v4, v1

    const/4 v1, 0x3

    new-instance v5, Lcom/chase/sig/android/view/detail/DetailColoredValueRow;

    const-string v6, "Description"

    iget-object v7, p0, Lcom/chase/sig/android/activity/EPayHistoryDetailActivity;->d:Lcom/chase/sig/android/service/epay/EPayTransaction;

    invoke-virtual {v7}, Lcom/chase/sig/android/service/epay/EPayTransaction;->a()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/chase/sig/android/util/s;->s(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lcom/chase/sig/android/view/detail/DetailColoredValueRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v4, v1

    const/4 v1, 0x4

    new-instance v5, Lcom/chase/sig/android/view/detail/DetailColoredValueRow;

    const-string v6, "Debit/Credit Amount"

    iget-object v7, p0, Lcom/chase/sig/android/activity/EPayHistoryDetailActivity;->d:Lcom/chase/sig/android/service/epay/EPayTransaction;

    invoke-virtual {v7}, Lcom/chase/sig/android/service/epay/EPayTransaction;->e()Lcom/chase/sig/android/util/Dollar;

    move-result-object v7

    invoke-virtual {v7}, Lcom/chase/sig/android/util/Dollar;->h()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lcom/chase/sig/android/view/detail/DetailColoredValueRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v4, v1

    const/4 v1, 0x5

    new-instance v5, Lcom/chase/sig/android/view/detail/DetailColoredValueRow;

    const-string v6, "Checking acct."

    iget-object v7, p0, Lcom/chase/sig/android/activity/EPayHistoryDetailActivity;->d:Lcom/chase/sig/android/service/epay/EPayTransaction;

    invoke-virtual {v7}, Lcom/chase/sig/android/service/epay/EPayTransaction;->s()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lcom/chase/sig/android/view/detail/DetailColoredValueRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v4, v1

    const/4 v1, 0x6

    new-instance v5, Lcom/chase/sig/android/view/detail/DetailColoredValueRow;

    const-string v6, "Status"

    iget-object v7, p0, Lcom/chase/sig/android/activity/EPayHistoryDetailActivity;->d:Lcom/chase/sig/android/service/epay/EPayTransaction;

    invoke-virtual {v7}, Lcom/chase/sig/android/service/epay/EPayTransaction;->t()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lcom/chase/sig/android/view/detail/DetailColoredValueRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v4, v1

    invoke-virtual {v0, v4}, Lcom/chase/sig/android/view/detail/DetailView;->setRows([Lcom/chase/sig/android/view/detail/a;)V

    .line 48
    const v0, 0x7f09027f

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/EPayHistoryDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 49
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 51
    const v0, 0x7f09027e

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/EPayHistoryDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 52
    iget-object v1, p0, Lcom/chase/sig/android/activity/EPayHistoryDetailActivity;->d:Lcom/chase/sig/android/service/epay/EPayTransaction;

    invoke-virtual {v1}, Lcom/chase/sig/android/service/epay/EPayTransaction;->u()Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v2

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 53
    const v1, 0x7f0701b4

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/EPayHistoryDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 55
    new-instance v1, Lcom/chase/sig/android/activity/cv;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/cv;-><init>(Lcom/chase/sig/android/activity/EPayHistoryDetailActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 62
    return-void

    :cond_0
    move v1, v3

    .line 52
    goto :goto_0
.end method

.method protected final a(Z)V
    .locals 0
    .parameter

    .prologue
    .line 66
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 4
    .parameter

    .prologue
    .line 71
    new-instance v0, Lcom/chase/sig/android/view/k$a;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/view/k$a;-><init>(Landroid/content/Context;)V

    .line 73
    packed-switch p1, :pswitch_data_0

    .line 83
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/n;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    .line 75
    :pswitch_0
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/chase/sig/android/view/k$a;->i:Z

    const v1, 0x7f0701b4

    new-instance v2, Lcom/chase/sig/android/activity/cw;

    invoke-direct {v2, p0}, Lcom/chase/sig/android/activity/cw;-><init>(Lcom/chase/sig/android/activity/EPayHistoryDetailActivity;)V

    invoke-virtual {v0, v1, v2}, Lcom/chase/sig/android/view/k$a;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    move-result-object v1

    const v2, 0x7f07006d

    new-instance v3, Lcom/chase/sig/android/activity/a/b;

    invoke-direct {v3}, Lcom/chase/sig/android/activity/a/b;-><init>()V

    invoke-virtual {v1, v2, v3}, Lcom/chase/sig/android/view/k$a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    move-result-object v1

    const v2, 0x7f07006a

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/view/k$a;->b(I)Lcom/chase/sig/android/view/k$a;

    .line 80
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/k$a;->d(I)Lcom/chase/sig/android/view/k;

    move-result-object v0

    goto :goto_0

    .line 73
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
