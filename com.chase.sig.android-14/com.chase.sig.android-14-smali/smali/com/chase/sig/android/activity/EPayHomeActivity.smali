.class public Lcom/chase/sig/android/activity/EPayHomeActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$e;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/EPayHomeActivity;)Z
    .locals 2
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 10
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/ChaseApplication;

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->b:Lcom/chase/sig/android/domain/g;

    invoke-interface {v0}, Lcom/chase/sig/android/domain/g;->u()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/EPayHomeActivity;)Ljava/lang/String;
    .locals 2
    .parameter

    .prologue
    .line 10
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/ChaseApplication;

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->b:Lcom/chase/sig/android/domain/g;

    invoke-interface {v0}, Lcom/chase/sig/android/domain/g;->u()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/IAccount;

    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 15
    const v0, 0x7f030038

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/EPayHomeActivity;->b(I)V

    .line 16
    const v0, 0x7f0701b1

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/EPayHomeActivity;->setTitle(I)V

    .line 18
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->d:Lcom/chase/sig/android/service/SplashResponse;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->d:Lcom/chase/sig/android/service/SplashResponse;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/SplashResponse;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 19
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->d:Lcom/chase/sig/android/service/SplashResponse;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/SplashResponse;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/EPayHomeActivity;->e(Ljava/lang/String;)V

    .line 49
    :goto_0
    return-void

    .line 21
    :cond_0
    const v0, 0x7f0900c1

    new-instance v1, Lcom/chase/sig/android/activity/cx;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/cx;-><init>(Lcom/chase/sig/android/activity/EPayHomeActivity;)V

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/EPayHomeActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 30
    const v0, 0x7f0900c0

    new-instance v1, Lcom/chase/sig/android/activity/cy;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/cy;-><init>(Lcom/chase/sig/android/activity/EPayHomeActivity;)V

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/EPayHomeActivity;->a(ILandroid/view/View$OnClickListener;)V

    goto :goto_0
.end method
