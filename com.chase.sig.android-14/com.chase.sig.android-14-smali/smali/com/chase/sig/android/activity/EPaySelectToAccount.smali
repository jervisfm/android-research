.class public Lcom/chase/sig/android/activity/EPaySelectToAccount;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$e;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/EPaySelectToAccount;Ljava/lang/String;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 18
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/chase/sig/android/activity/EPayStartActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "selectedAccountId"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "extra_came_from_select_pay_to"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/EPaySelectToAccount;->startActivity(Landroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 4
    .parameter

    .prologue
    .line 26
    const v0, 0x7f0701ab

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/EPaySelectToAccount;->setTitle(I)V

    .line 27
    const v0, 0x7f03003b

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/EPaySelectToAccount;->b(I)V

    .line 29
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/ChaseApplication;

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->b:Lcom/chase/sig/android/domain/g;

    .line 30
    invoke-interface {v0}, Lcom/chase/sig/android/domain/g;->u()Ljava/util/List;

    move-result-object v2

    .line 32
    const v0, 0x7f0900c8

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/EPaySelectToAccount;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 34
    new-instance v1, Lcom/chase/sig/android/view/af;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/EPaySelectToAccount;->getBaseContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v2}, Lcom/chase/sig/android/util/a;->d(Landroid/content/Context;Ljava/util/List;)Landroid/widget/SimpleAdapter;

    move-result-object v3

    invoke-direct {v1, v3}, Lcom/chase/sig/android/view/af;-><init>(Landroid/widget/ListAdapter;)V

    .line 38
    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 40
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/EPaySelectToAccount;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 41
    if-nez v1, :cond_0

    const/4 v1, 0x0

    .line 44
    :goto_0
    new-instance v3, Lcom/chase/sig/android/activity/cz;

    invoke-direct {v3, p0, v2, v1}, Lcom/chase/sig/android/activity/cz;-><init>(Lcom/chase/sig/android/activity/EPaySelectToAccount;Ljava/util/List;Z)V

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 61
    return-void

    .line 41
    :cond_0
    const-string v3, "extra_came_from_epay_start"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method
