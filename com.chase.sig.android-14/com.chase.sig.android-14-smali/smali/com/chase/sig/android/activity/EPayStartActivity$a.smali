.class public Lcom/chase/sig/android/activity/EPayStartActivity$a;
.super Lcom/chase/sig/android/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/EPayStartActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/d",
        "<",
        "Lcom/chase/sig/android/activity/EPayStartActivity;",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/service/epay/EPayAccountsAndPaymentOptionsResponse;",
        ">;"
    }
.end annotation


# instance fields
.field a:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 488
    invoke-direct {p0}, Lcom/chase/sig/android/d;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 488
    check-cast p1, [Ljava/lang/String;

    const/4 v0, 0x0

    aget-object v0, p1, v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/EPayStartActivity$a;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/EPayStartActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/EPayStartActivity;->A()Lcom/chase/sig/android/service/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/service/n;->a()Lcom/chase/sig/android/service/epay/b;

    iget-object v0, p0, Lcom/chase/sig/android/activity/EPayStartActivity$a;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/chase/sig/android/service/epay/b;->a(Ljava/lang/String;)Lcom/chase/sig/android/service/epay/EPayAccountsAndPaymentOptionsResponse;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 4
    .parameter

    .prologue
    .line 488
    check-cast p1, Lcom/chase/sig/android/service/epay/EPayAccountsAndPaymentOptionsResponse;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/epay/EPayAccountsAndPaymentOptionsResponse;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/EPayStartActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/epay/EPayAccountsAndPaymentOptionsResponse;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/EPayStartActivity;->b(Ljava/util/List;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/epay/EPayAccountsAndPaymentOptionsResponse;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/activity/EPayStartActivity;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/EPayStartActivity;

    const v1, 0x7f07027c

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/EPayStartActivity;->f(I)V

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/chase/sig/android/service/epay/EPayAccountsAndPaymentOptionsResponse;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/EPayStartActivity;

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v1, Lcom/chase/sig/android/activity/EPayStartActivity;

    const v3, 0x7f070280

    invoke-virtual {v1, v3}, Lcom/chase/sig/android/activity/EPayStartActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/chase/sig/android/activity/EPayStartActivity;->a(ILjava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/EPayStartActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/epay/EPayAccountsAndPaymentOptionsResponse;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/chase/sig/android/activity/EPayStartActivity;->a(Lcom/chase/sig/android/activity/EPayStartActivity;Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/EPayStartActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/epay/EPayAccountsAndPaymentOptionsResponse;->b()Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/chase/sig/android/activity/EPayStartActivity;->a(Lcom/chase/sig/android/activity/EPayStartActivity;Ljava/util/List;)Ljava/util/List;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/EPayStartActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/epay/EPayAccountsAndPaymentOptionsResponse;->c()Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/chase/sig/android/activity/EPayStartActivity;->b(Lcom/chase/sig/android/activity/EPayStartActivity;Ljava/util/List;)Ljava/util/List;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/EPayStartActivity;

    iget-object v1, p0, Lcom/chase/sig/android/activity/EPayStartActivity$a;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/chase/sig/android/activity/EPayStartActivity;->b(Lcom/chase/sig/android/activity/EPayStartActivity;Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/EPayStartActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/EPayStartActivity;->i(Lcom/chase/sig/android/activity/EPayStartActivity;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/EPayStartActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/EPayStartActivity;->j(Lcom/chase/sig/android/activity/EPayStartActivity;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/EPayStartActivity;

    iget-object v1, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v1, Lcom/chase/sig/android/activity/EPayStartActivity;

    invoke-static {v1}, Lcom/chase/sig/android/activity/EPayStartActivity;->k(Lcom/chase/sig/android/activity/EPayStartActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/chase/sig/android/activity/EPayStartActivity;->c(Lcom/chase/sig/android/activity/EPayStartActivity;Ljava/lang/String;)V

    goto :goto_0
.end method
