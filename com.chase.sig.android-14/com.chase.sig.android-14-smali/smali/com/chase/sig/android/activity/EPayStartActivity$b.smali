.class public Lcom/chase/sig/android/activity/EPayStartActivity$b;
.super Lcom/chase/sig/android/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/EPayStartActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/d",
        "<",
        "Lcom/chase/sig/android/activity/EPayStartActivity;",
        "Lcom/chase/sig/android/domain/EPayment;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/service/GenericResponse;",
        ">;"
    }
.end annotation


# instance fields
.field a:Lcom/chase/sig/android/domain/EPayment;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 550
    invoke-direct {p0}, Lcom/chase/sig/android/d;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 550
    check-cast p1, [Lcom/chase/sig/android/domain/EPayment;

    const/4 v0, 0x0

    aget-object v0, p1, v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/EPayStartActivity$b;->a:Lcom/chase/sig/android/domain/EPayment;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/EPayStartActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/EPayStartActivity;->A()Lcom/chase/sig/android/service/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/service/n;->a()Lcom/chase/sig/android/service/epay/b;

    iget-object v0, p0, Lcom/chase/sig/android/activity/EPayStartActivity$b;->a:Lcom/chase/sig/android/domain/EPayment;

    invoke-static {v0}, Lcom/chase/sig/android/service/epay/b;->a(Lcom/chase/sig/android/domain/EPayment;)Lcom/chase/sig/android/service/GenericResponse;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 3
    .parameter

    .prologue
    .line 550
    check-cast p1, Lcom/chase/sig/android/service/GenericResponse;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/GenericResponse;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "3832"

    invoke-virtual {p1, v0}, Lcom/chase/sig/android/service/GenericResponse;->d(Ljava/lang/String;)Lcom/chase/sig/android/service/IServiceError;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/EPayStartActivity;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/EPayStartActivity;->b(Lcom/chase/sig/android/service/IServiceError;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/EPayStartActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/GenericResponse;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/EPayStartActivity;->c(Ljava/util/List;)V

    goto :goto_0

    :cond_1
    new-instance v1, Landroid/content/Intent;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/EPayStartActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/EPayStartActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    const-class v2, Lcom/chase/sig/android/activity/EPayVerifyActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    sget-object v0, Lcom/chase/sig/android/activity/EPayVerifyActivity;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/chase/sig/android/activity/EPayStartActivity$b;->a:Lcom/chase/sig/android/domain/EPayment;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/EPayStartActivity;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/EPayStartActivity;->startActivity(Landroid/content/Intent;)V

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    const-class v2, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-static {v0}, Lcom/chase/sig/android/c;->a(Landroid/content/Intent;)V

    goto :goto_0
.end method
