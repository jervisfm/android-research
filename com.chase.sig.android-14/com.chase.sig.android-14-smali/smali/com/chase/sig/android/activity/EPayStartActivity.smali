.class public Lcom/chase/sig/android/activity/EPayStartActivity;
.super Lcom/chase/sig/android/activity/a;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/EPayStartActivity$b;,
        Lcom/chase/sig/android/activity/EPayStartActivity$a;
    }
.end annotation


# instance fields
.field private a:Lcom/chase/sig/android/domain/IAccount;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/EPayAccount;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/EPaymentOption;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private k:Ljava/lang/String;

.field private l:Landroid/widget/SimpleAdapter;

.field private m:Ljava/lang/String;

.field private n:Lcom/chase/sig/android/domain/g;

.field private o:Lcom/chase/sig/android/view/x;

.field private p:Landroid/view/View$OnClickListener;

.field private q:Lcom/chase/sig/android/view/b$a;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/chase/sig/android/activity/a;-><init>()V

    .line 74
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/chase/sig/android/activity/EPayStartActivity;->b:Ljava/util/List;

    .line 75
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/chase/sig/android/activity/EPayStartActivity;->c:Ljava/util/List;

    .line 76
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/chase/sig/android/activity/EPayStartActivity;->d:Ljava/util/List;

    .line 77
    const-string v0, ""

    iput-object v0, p0, Lcom/chase/sig/android/activity/EPayStartActivity;->k:Ljava/lang/String;

    .line 398
    new-instance v0, Lcom/chase/sig/android/activity/db;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/activity/db;-><init>(Lcom/chase/sig/android/activity/EPayStartActivity;)V

    iput-object v0, p0, Lcom/chase/sig/android/activity/EPayStartActivity;->o:Lcom/chase/sig/android/view/x;

    .line 429
    new-instance v0, Lcom/chase/sig/android/activity/dc;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/activity/dc;-><init>(Lcom/chase/sig/android/activity/EPayStartActivity;)V

    iput-object v0, p0, Lcom/chase/sig/android/activity/EPayStartActivity;->p:Landroid/view/View$OnClickListener;

    .line 622
    new-instance v0, Lcom/chase/sig/android/activity/dd;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/activity/dd;-><init>(Lcom/chase/sig/android/activity/EPayStartActivity;)V

    iput-object v0, p0, Lcom/chase/sig/android/activity/EPayStartActivity;->q:Lcom/chase/sig/android/view/b$a;

    return-void
.end method

.method private D()Lcom/chase/sig/android/view/JPSpinner;
    .locals 1

    .prologue
    .line 279
    const-string v0, "PAYMENT_OPTIONS"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/EPayStartActivity;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/q;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/q;->q()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v0

    return-object v0
.end method

.method private E()V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 284
    new-array v4, v2, [Ljava/lang/String;

    const-string v0, "account_name"

    aput-object v0, v4, v1

    .line 287
    new-array v5, v2, [I

    const v0, 0x7f09010c

    aput v0, v5, v1

    .line 291
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 292
    iget-object v0, p0, Lcom/chase/sig/android/activity/EPayStartActivity;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/EPayAccount;

    .line 293
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 294
    const-string v6, "account_name"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/EPayAccount;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v6, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 295
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 298
    :cond_0
    new-instance v0, Landroid/widget/SimpleAdapter;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/EPayStartActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    const v3, 0x7f030051

    invoke-direct/range {v0 .. v5}, Landroid/widget/SimpleAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V

    .line 301
    invoke-direct {p0}, Lcom/chase/sig/android/activity/EPayStartActivity;->j()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v1

    .line 302
    invoke-virtual {v1, v0}, Lcom/chase/sig/android/view/JPSpinner;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 303
    return-void
.end method

.method private F()V
    .locals 13

    .prologue
    const/4 v1, 0x3

    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 306
    iget-object v0, p0, Lcom/chase/sig/android/activity/EPayStartActivity;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 308
    new-array v4, v1, [Ljava/lang/String;

    const-string v0, "label"

    aput-object v0, v4, v10

    const-string v0, "value"

    aput-object v0, v4, v11

    const-string v0, "note"

    aput-object v0, v4, v12

    .line 311
    new-array v5, v1, [I

    fill-array-data v5, :array_0

    .line 315
    iget-object v0, p0, Lcom/chase/sig/android/activity/EPayStartActivity;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/EPaymentOption;

    .line 317
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/EPaymentOption;->b()Ljava/lang/String;

    move-result-object v2

    .line 318
    const-string v1, ""

    .line 319
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 323
    const v3, 0x7f070081

    invoke-virtual {p0, v3}, Lcom/chase/sig/android/activity/EPayStartActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 325
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/EPaymentOption;->d()Ljava/lang/String;

    move-result-object v8

    const-string v9, "-1"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 326
    const v1, 0x7f0701b8

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/EPayStartActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 327
    const-string v1, "$(xxx.xx)"

    .line 336
    :goto_1
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/EPaymentOption;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/chase/sig/android/util/s;->l(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 337
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/EPaymentOption;->b()Ljava/lang/String;

    .line 342
    :cond_0
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/EPaymentOption;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/chase/sig/android/util/s;->l(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 343
    const-string v3, ""

    .line 347
    :goto_2
    const-string v8, "label"

    invoke-interface {v7, v8, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 348
    const-string v2, "value"

    invoke-interface {v7, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 349
    const-string v1, "note"

    invoke-interface {v7, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 350
    const-string v1, "id"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/EPaymentOption;->d()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v7, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 352
    iget-object v0, p0, Lcom/chase/sig/android/activity/EPayStartActivity;->d:Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 329
    :cond_1
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/EPaymentOption;->a()Lcom/chase/sig/android/util/Dollar;

    move-result-object v8

    .line 330
    if-eqz v8, :cond_2

    invoke-virtual {v8}, Lcom/chase/sig/android/util/Dollar;->b()Ljava/math/BigDecimal;

    move-result-object v9

    if-eqz v9, :cond_2

    .line 331
    invoke-virtual {v8}, Lcom/chase/sig/android/util/Dollar;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 333
    :cond_2
    const-string v8, "%s%s"

    new-array v9, v12, [Ljava/lang/Object;

    aput-object v3, v9, v10

    aput-object v1, v9, v11

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 345
    :cond_3
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/EPaymentOption;->c()Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    .line 355
    :cond_4
    new-instance v0, Landroid/widget/SimpleAdapter;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/EPayStartActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/chase/sig/android/activity/EPayStartActivity;->d:Ljava/util/List;

    const v3, 0x7f03003a

    invoke-direct/range {v0 .. v5}, Landroid/widget/SimpleAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V

    iput-object v0, p0, Lcom/chase/sig/android/activity/EPayStartActivity;->l:Landroid/widget/SimpleAdapter;

    .line 357
    invoke-direct {p0}, Lcom/chase/sig/android/activity/EPayStartActivity;->D()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v0

    iget-object v1, p0, Lcom/chase/sig/android/activity/EPayStartActivity;->l:Landroid/widget/SimpleAdapter;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/JPSpinner;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 358
    return-void

    .line 311
    :array_0
    .array-data 0x4
        0xc5t 0x0t 0x9t 0x7ft
        0xc6t 0x0t 0x9t 0x7ft
        0xc7t 0x0t 0x9t 0x7ft
    .end array-data
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/EPayStartActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 51
    iput-object p1, p0, Lcom/chase/sig/android/activity/EPayStartActivity;->k:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/EPayStartActivity;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 51
    iput-object p1, p0, Lcom/chase/sig/android/activity/EPayStartActivity;->b:Ljava/util/List;

    return-object p1
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/EPayStartActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/chase/sig/android/activity/EPayStartActivity;->e()V

    return-void
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/EPayStartActivity;)Landroid/widget/SimpleAdapter;
    .locals 1
    .parameter

    .prologue
    .line 51
    iget-object v0, p0, Lcom/chase/sig/android/activity/EPayStartActivity;->l:Landroid/widget/SimpleAdapter;

    return-object v0
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/EPayStartActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 51
    iput-object p1, p0, Lcom/chase/sig/android/activity/EPayStartActivity;->m:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/EPayStartActivity;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 51
    iput-object p1, p0, Lcom/chase/sig/android/activity/EPayStartActivity;->c:Ljava/util/List;

    return-object p1
.end method

.method static synthetic c(Lcom/chase/sig/android/activity/EPayStartActivity;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/chase/sig/android/activity/EPayStartActivity;->i()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Lcom/chase/sig/android/activity/EPayStartActivity;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/chase/sig/android/activity/EPayStartActivity;->i(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic c(Ljava/lang/String;)Z
    .locals 1
    .parameter

    .prologue
    .line 51
    invoke-static {p0}, Lcom/chase/sig/android/activity/EPayStartActivity;->h(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic d(Lcom/chase/sig/android/activity/EPayStartActivity;)Lcom/chase/sig/android/domain/IAccount;
    .locals 1
    .parameter

    .prologue
    .line 51
    iget-object v0, p0, Lcom/chase/sig/android/activity/EPayStartActivity;->a:Lcom/chase/sig/android/domain/IAccount;

    return-object v0
.end method

.method private d()Z
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/chase/sig/android/activity/EPayStartActivity;->a:Lcom/chase/sig/android/domain/IAccount;

    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->l()Z

    move-result v0

    return v0
.end method

.method static synthetic e(Lcom/chase/sig/android/activity/EPayStartActivity;)Lcom/chase/sig/android/view/JPSpinner;
    .locals 1
    .parameter

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/chase/sig/android/activity/EPayStartActivity;->j()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v0

    return-object v0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 172
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/chase/sig/android/activity/EPaySelectToAccount;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 173
    const/high16 v1, 0x4000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 174
    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/EPayStartActivity;->startActivity(Landroid/content/Intent;)V

    .line 175
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/EPayStartActivity;->finish()V

    .line 176
    return-void
.end method

.method static synthetic f(Lcom/chase/sig/android/activity/EPayStartActivity;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 51
    iget-object v0, p0, Lcom/chase/sig/android/activity/EPayStartActivity;->b:Ljava/util/List;

    return-object v0
.end method

.method private f()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 227
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/ChaseApplication;

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->b:Lcom/chase/sig/android/domain/g;

    invoke-interface {v0}, Lcom/chase/sig/android/domain/g;->u()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic g(Lcom/chase/sig/android/activity/EPayStartActivity;)Lcom/chase/sig/android/view/JPSpinner;
    .locals 1
    .parameter

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/chase/sig/android/activity/EPayStartActivity;->D()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v0

    return-object v0
.end method

.method private g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 231
    iget-object v0, p0, Lcom/chase/sig/android/activity/EPayStartActivity;->k:Ljava/lang/String;

    invoke-static {v0}, Lcom/chase/sig/android/activity/EPayStartActivity;->h(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 232
    const-string v0, "N/A"

    .line 234
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/EPayStartActivity;->k:Ljava/lang/String;

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 259
    const-string v0, "DATE"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/EPayStartActivity;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/d;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/d;->p()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic h(Lcom/chase/sig/android/activity/EPayStartActivity;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 51
    iget-object v0, p0, Lcom/chase/sig/android/activity/EPayStartActivity;->c:Ljava/util/List;

    return-object v0
.end method

.method private static h(Ljava/lang/String;)Z
    .locals 1
    .parameter

    .prologue
    .line 263
    invoke-static {p0}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "N/A"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 267
    const-string v0, "DATE"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/EPayStartActivity;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/d;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/d;->p()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic i(Lcom/chase/sig/android/activity/EPayStartActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/chase/sig/android/activity/EPayStartActivity;->F()V

    return-void
.end method

.method private i(Ljava/lang/String;)V
    .locals 1
    .parameter

    .prologue
    .line 271
    const-string v0, "DATE"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/EPayStartActivity;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/d;

    invoke-virtual {v0, p1}, Lcom/chase/sig/android/view/detail/d;->b(Ljava/lang/String;)V

    .line 272
    return-void
.end method

.method private j()Lcom/chase/sig/android/view/JPSpinner;
    .locals 1

    .prologue
    .line 275
    const-string v0, "PAY_FROM"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/EPayStartActivity;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/q;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/q;->q()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v0

    return-object v0
.end method

.method static synthetic j(Lcom/chase/sig/android/activity/EPayStartActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/chase/sig/android/activity/EPayStartActivity;->E()V

    return-void
.end method

.method static synthetic k(Lcom/chase/sig/android/activity/EPayStartActivity;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/chase/sig/android/activity/EPayStartActivity;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected final a(I)V
    .locals 4
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 533
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/a;->a(I)V

    .line 535
    if-nez p1, :cond_1

    invoke-direct {p0}, Lcom/chase/sig/android/activity/EPayStartActivity;->f()Z

    move-result v2

    if-nez v2, :cond_1

    move v2, v0

    .line 537
    :goto_0
    if-nez p1, :cond_2

    invoke-direct {p0}, Lcom/chase/sig/android/activity/EPayStartActivity;->f()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 540
    :goto_1
    if-eqz v2, :cond_3

    .line 541
    invoke-direct {p0}, Lcom/chase/sig/android/activity/EPayStartActivity;->e()V

    .line 548
    :cond_0
    :goto_2
    return-void

    :cond_1
    move v2, v1

    .line 535
    goto :goto_0

    :cond_2
    move v0, v1

    .line 537
    goto :goto_1

    .line 544
    :cond_3
    if-eqz v0, :cond_0

    .line 545
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/EPayStartActivity;->finish()V

    goto :goto_2
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 9
    .parameter

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 85
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/a;->a(Landroid/os/Bundle;)V

    .line 86
    const v0, 0x7f0701ab

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/EPayStartActivity;->setTitle(I)V

    .line 88
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->d:Lcom/chase/sig/android/service/SplashResponse;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->d:Lcom/chase/sig/android/service/SplashResponse;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/SplashResponse;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->d:Lcom/chase/sig/android/service/SplashResponse;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/SplashResponse;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/EPayStartActivity;->e(Ljava/lang/String;)V

    .line 131
    :goto_0
    return-void

    .line 91
    :cond_0
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/EPayStartActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "selectedAccountId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 93
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/ChaseApplication;

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->b:Lcom/chase/sig/android/domain/g;

    iput-object v0, p0, Lcom/chase/sig/android/activity/EPayStartActivity;->n:Lcom/chase/sig/android/domain/g;

    .line 94
    iget-object v0, p0, Lcom/chase/sig/android/activity/EPayStartActivity;->n:Lcom/chase/sig/android/domain/g;

    invoke-interface {v0, v1}, Lcom/chase/sig/android/domain/g;->a(Ljava/lang/String;)Lcom/chase/sig/android/domain/IAccount;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/EPayStartActivity;->a:Lcom/chase/sig/android/domain/IAccount;

    .line 96
    const v0, 0x7f0902c9

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/EPayStartActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailView;

    const/16 v1, 0x8

    new-array v5, v1, [Lcom/chase/sig/android/view/detail/a;

    new-instance v2, Lcom/chase/sig/android/view/detail/d;

    const-string v6, "Pay To"

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/ChaseApplication;

    invoke-virtual {v1}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v1

    iget-object v1, v1, Lcom/chase/sig/android/domain/o;->b:Lcom/chase/sig/android/domain/g;

    iget-object v7, p0, Lcom/chase/sig/android/activity/EPayStartActivity;->a:Lcom/chase/sig/android/domain/IAccount;

    invoke-interface {v1, v7}, Lcom/chase/sig/android/domain/g;->a(Lcom/chase/sig/android/domain/IAccount;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v6, v1}, Lcom/chase/sig/android/view/detail/d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "PAY_TO"

    iput-object v1, v2, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    move-object v1, v2

    check-cast v1, Lcom/chase/sig/android/view/detail/d;

    new-instance v2, Lcom/chase/sig/android/activity/da;

    invoke-direct {v2, p0}, Lcom/chase/sig/android/activity/da;-><init>(Lcom/chase/sig/android/activity/EPayStartActivity;)V

    iput-object v2, v1, Lcom/chase/sig/android/view/detail/d;->a:Landroid/view/View$OnClickListener;

    aput-object v1, v5, v4

    new-instance v2, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v6, "Due Date"

    iget-object v1, p0, Lcom/chase/sig/android/activity/EPayStartActivity;->a:Lcom/chase/sig/android/domain/IAccount;

    invoke-interface {v1}, Lcom/chase/sig/android/domain/IAccount;->e()Ljava/util/Hashtable;

    move-result-object v1

    const-string v7, "nextPaymentDate"

    invoke-virtual {v1, v7}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->l(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    const-string v1, "N/A"

    :goto_1
    invoke-direct {v2, v6, v1}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v5, v3

    const/4 v6, 0x2

    new-instance v7, Lcom/chase/sig/android/view/detail/DetailRow;

    iget-object v1, p0, Lcom/chase/sig/android/activity/EPayStartActivity;->a:Lcom/chase/sig/android/domain/IAccount;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/chase/sig/android/activity/EPayStartActivity;->a:Lcom/chase/sig/android/domain/IAccount;

    invoke-interface {v1}, Lcom/chase/sig/android/domain/IAccount;->E()Z

    move-result v1

    if-eqz v1, :cond_2

    const v1, 0x7f0700e7

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/EPayStartActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    :goto_2
    iget-object v1, p0, Lcom/chase/sig/android/activity/EPayStartActivity;->a:Lcom/chase/sig/android/domain/IAccount;

    invoke-interface {v1}, Lcom/chase/sig/android/domain/IAccount;->e()Ljava/util/Hashtable;

    move-result-object v1

    const-string v8, "nextPaymentAmount"

    invoke-virtual {v1, v8}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->l(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_3

    const-string v1, "N/A"

    :goto_3
    invoke-direct {v7, v2, v1}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/chase/sig/android/activity/EPayStartActivity;->d()Z

    move-result v1

    if-nez v1, :cond_4

    move v1, v3

    :goto_4
    iput-boolean v1, v7, Lcom/chase/sig/android/view/detail/a;->l:Z

    aput-object v7, v5, v6

    const/4 v6, 0x3

    new-instance v2, Lcom/chase/sig/android/view/detail/DetailRow;

    const v1, 0x7f0700ed

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/EPayStartActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0}, Lcom/chase/sig/android/activity/EPayStartActivity;->d()Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, "N/A"

    :goto_5
    invoke-direct {v2, v7, v1}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/chase/sig/android/activity/EPayStartActivity;->d()Z

    move-result v1

    if-nez v1, :cond_7

    move v1, v3

    :goto_6
    iput-boolean v1, v2, Lcom/chase/sig/android/view/detail/a;->j:Z

    move-object v1, v2

    check-cast v1, Lcom/chase/sig/android/view/detail/DetailRow;

    invoke-virtual {v1}, Lcom/chase/sig/android/view/detail/DetailRow;->c()Lcom/chase/sig/android/view/detail/a;

    move-result-object v1

    aput-object v1, v5, v6

    const/4 v2, 0x4

    new-instance v1, Lcom/chase/sig/android/view/detail/q;

    const-string v6, "Pay From"

    invoke-direct {v1, v6}, Lcom/chase/sig/android/view/detail/q;-><init>(Ljava/lang/String;)V

    const-string v6, "PAY_FROM"

    iput-object v6, v1, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v1, Lcom/chase/sig/android/view/detail/q;

    const-string v6, "Select Pay From Account"

    invoke-virtual {v1, v6}, Lcom/chase/sig/android/view/detail/q;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v1

    aput-object v1, v5, v2

    const/4 v2, 0x5

    new-instance v1, Lcom/chase/sig/android/view/detail/d;

    const-string v6, "Payment Date"

    invoke-direct {p0}, Lcom/chase/sig/android/activity/EPayStartActivity;->g()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v1, v6, v7}, Lcom/chase/sig/android/view/detail/d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v6, "DATE"

    iput-object v6, v1, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v1, Lcom/chase/sig/android/view/detail/d;

    invoke-virtual {v1}, Lcom/chase/sig/android/view/detail/d;->q()Lcom/chase/sig/android/view/detail/d;

    move-result-object v1

    invoke-virtual {p0, v3}, Lcom/chase/sig/android/activity/EPayStartActivity;->i(I)Landroid/view/View$OnClickListener;

    move-result-object v6

    iput-object v6, v1, Lcom/chase/sig/android/view/detail/d;->a:Landroid/view/View$OnClickListener;

    aput-object v1, v5, v2

    const/4 v2, 0x6

    new-instance v1, Lcom/chase/sig/android/view/detail/c;

    const-string v6, "Other Amount $"

    invoke-direct {v1, v6}, Lcom/chase/sig/android/view/detail/c;-><init>(Ljava/lang/String;)V

    const-string v6, "Enter Other Amount"

    invoke-virtual {v1, v6}, Lcom/chase/sig/android/view/detail/c;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/view/detail/c;

    const-string v6, "AMOUNT"

    iput-object v6, v1, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v1, Lcom/chase/sig/android/view/detail/c;

    iput-boolean v3, v1, Lcom/chase/sig/android/view/detail/a;->j:Z

    aput-object v1, v5, v2

    const/4 v2, 0x7

    new-instance v1, Lcom/chase/sig/android/view/detail/q;

    const-string v6, "Amount $"

    invoke-direct {v1, v6}, Lcom/chase/sig/android/view/detail/q;-><init>(Ljava/lang/String;)V

    const-string v6, "PAYMENT_OPTIONS"

    iput-object v6, v1, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v1, Lcom/chase/sig/android/view/detail/q;

    const v6, 0x7f0701aa

    invoke-virtual {p0, v6}, Lcom/chase/sig/android/activity/EPayStartActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/chase/sig/android/view/detail/q;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/view/detail/q;

    iget-object v6, p0, Lcom/chase/sig/android/activity/EPayStartActivity;->o:Lcom/chase/sig/android/view/x;

    invoke-virtual {v1, v6}, Lcom/chase/sig/android/view/detail/q;->b(Lcom/chase/sig/android/view/x;)Lcom/chase/sig/android/view/detail/q;

    move-result-object v1

    iput-boolean v4, v1, Lcom/chase/sig/android/view/detail/a;->j:Z

    aput-object v1, v5, v2

    invoke-virtual {v0, v5}, Lcom/chase/sig/android/view/detail/DetailView;->setRows([Lcom/chase/sig/android/view/detail/a;)V

    .line 115
    const-string v0, "ePayFromAccounts"

    invoke-static {p1, v0}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 116
    const-string v0, "ePayFromAccounts"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/chase/sig/android/activity/EPayStartActivity;->b:Ljava/util/List;

    .line 117
    const-string v0, "options_loaded_for_account_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/EPayStartActivity;->m:Ljava/lang/String;

    .line 119
    const-string v0, "earliest_payment_date"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/EPayStartActivity;->k:Ljava/lang/String;

    .line 120
    const-string v0, "ePayAmountOptions"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/chase/sig/android/activity/EPayStartActivity;->c:Ljava/util/List;

    .line 122
    invoke-direct {p0}, Lcom/chase/sig/android/activity/EPayStartActivity;->E()V

    .line 123
    invoke-direct {p0}, Lcom/chase/sig/android/activity/EPayStartActivity;->F()V

    .line 128
    :goto_7
    const v0, 0x7f0902cb

    iget-object v1, p0, Lcom/chase/sig/android/activity/EPayStartActivity;->p:Landroid/view/View$OnClickListener;

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/EPayStartActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 129
    const v0, 0x7f0902ca

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/EPayStartActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const v1, 0x7f070296

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    goto/16 :goto_0

    .line 96
    :cond_1
    invoke-static {v1}, Lcom/chase/sig/android/util/s;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1

    :cond_2
    const-string v1, "Minimum Due"

    move-object v2, v1

    goto/16 :goto_2

    :cond_3
    new-instance v8, Lcom/chase/sig/android/util/Dollar;

    invoke-direct {v8, v1}, Lcom/chase/sig/android/util/Dollar;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Lcom/chase/sig/android/util/Dollar;->h()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_3

    :cond_4
    move v1, v4

    goto/16 :goto_4

    :cond_5
    iget-object v1, p0, Lcom/chase/sig/android/activity/EPayStartActivity;->a:Lcom/chase/sig/android/domain/IAccount;

    invoke-interface {v1}, Lcom/chase/sig/android/domain/IAccount;->e()Ljava/util/Hashtable;

    move-result-object v1

    const-string v8, "goalPayAmountDue"

    invoke-virtual {v1, v8}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    if-nez v1, :cond_6

    const-string v1, "N/A"

    goto/16 :goto_5

    :cond_6
    new-instance v8, Lcom/chase/sig/android/util/Dollar;

    invoke-direct {v8, v1}, Lcom/chase/sig/android/util/Dollar;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Lcom/chase/sig/android/util/Dollar;->h()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_5

    :cond_7
    move v1, v4

    goto/16 :goto_6

    .line 125
    :cond_8
    const-class v0, Lcom/chase/sig/android/activity/EPayStartActivity$a;

    new-array v1, v3, [Ljava/lang/String;

    iget-object v2, p0, Lcom/chase/sig/android/activity/EPayStartActivity;->a:Lcom/chase/sig/android/domain/IAccount;

    invoke-interface {v2}, Lcom/chase/sig/android/domain/IAccount;->b()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/EPayStartActivity;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    goto :goto_7
.end method

.method public final b()Z
    .locals 8

    .prologue
    const/4 v4, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 179
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/EPayStartActivity;->y()V

    .line 182
    invoke-direct {p0}, Lcom/chase/sig/android/activity/EPayStartActivity;->D()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/view/JPSpinner;->getSelectedItemPosition()I

    move-result v0

    if-ne v0, v4, :cond_b

    .line 184
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/EPayStartActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v0

    const-string v3, "PAYMENT_OPTIONS"

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/view/detail/DetailView;->a(Ljava/lang/String;)V

    move v0, v1

    .line 187
    :goto_0
    invoke-direct {p0}, Lcom/chase/sig/android/activity/EPayStartActivity;->j()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v3

    invoke-virtual {v3}, Lcom/chase/sig/android/view/JPSpinner;->getSelectedItemPosition()I

    move-result v3

    if-ne v3, v4, :cond_a

    .line 189
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/EPayStartActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v0

    const-string v3, "PAY_FROM"

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/view/detail/DetailView;->a(Ljava/lang/String;)V

    move v3, v1

    .line 192
    :goto_1
    invoke-direct {p0}, Lcom/chase/sig/android/activity/EPayStartActivity;->D()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/view/JPSpinner;->getSelectedItemPosition()I

    move-result v0

    if-ne v0, v4, :cond_2

    move v0, v1

    :goto_2
    if-nez v0, :cond_0

    .line 194
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/EPayStartActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v0

    const-string v3, "AMOUNT"

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/view/detail/DetailView;->a(Ljava/lang/String;)V

    move v3, v1

    .line 197
    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/EPayStartActivity;->k:Ljava/lang/String;

    invoke-static {v0}, Lcom/chase/sig/android/activity/EPayStartActivity;->h(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    move v2, v1

    :cond_1
    :goto_3
    if-nez v2, :cond_9

    .line 199
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/EPayStartActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v0

    const-string v2, "DATE"

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/view/detail/DetailView;->a(Ljava/lang/String;)V

    .line 201
    :goto_4
    return v1

    .line 192
    :cond_2
    iget-object v4, p0, Lcom/chase/sig/android/activity/EPayStartActivity;->c:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/EPaymentOption;

    const-string v4, "-1"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/EPaymentOption;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/EPaymentOption;->a()Lcom/chase/sig/android/util/Dollar;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/EPaymentOption;->a()Lcom/chase/sig/android/util/Dollar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/util/Dollar;->b()Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigDecimal;->longValue()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-gtz v0, :cond_4

    :cond_3
    move v0, v1

    goto :goto_2

    :cond_4
    move v0, v2

    goto :goto_2

    :cond_5
    const-string v0, "AMOUNT"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/EPayStartActivity;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/c;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/c;->p()Lcom/chase/sig/android/view/AmountView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/view/AmountView;->a()Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v1

    goto :goto_2

    :cond_6
    move v0, v2

    goto :goto_2

    .line 197
    :cond_7
    iget-object v0, p0, Lcom/chase/sig/android/activity/EPayStartActivity;->k:Ljava/lang/String;

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->t(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-direct {p0}, Lcom/chase/sig/android/activity/EPayStartActivity;->i()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/chase/sig/android/util/s;->l(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_8

    move v2, v1

    goto :goto_3

    :cond_8
    invoke-static {v4}, Lcom/chase/sig/android/util/s;->t(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v2, v1

    goto :goto_3

    :cond_9
    move v1, v3

    goto :goto_4

    :cond_a
    move v3, v0

    goto/16 :goto_1

    :cond_b
    move v0, v2

    goto/16 :goto_0
.end method

.method public final k()V
    .locals 2

    .prologue
    .line 469
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/EPayStartActivity;->y()V

    .line 470
    invoke-direct {p0}, Lcom/chase/sig/android/activity/EPayStartActivity;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 472
    const-string v0, "PAYMENT_OPTIONS"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/EPayStartActivity;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/q;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/chase/sig/android/view/detail/a;->n:Z

    .line 473
    invoke-direct {p0}, Lcom/chase/sig/android/activity/EPayStartActivity;->D()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/view/JPSpinner;->a()V

    .line 475
    const-string v0, "AMOUNT"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/EPayStartActivity;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/c;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/chase/sig/android/view/detail/a;->j:Z

    .line 476
    const-string v0, "AMOUNT"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/EPayStartActivity;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/c;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/detail/c;->b(Ljava/lang/String;)V

    .line 478
    invoke-direct {p0}, Lcom/chase/sig/android/activity/EPayStartActivity;->j()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/view/JPSpinner;->a()V

    .line 479
    invoke-direct {p0}, Lcom/chase/sig/android/activity/EPayStartActivity;->g()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/chase/sig/android/activity/EPayStartActivity;->i(Ljava/lang/String;)V

    .line 485
    :goto_0
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/EPayStartActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/DetailView;->c()V

    .line 486
    return-void

    .line 482
    :cond_0
    invoke-direct {p0}, Lcom/chase/sig/android/activity/EPayStartActivity;->e()V

    goto :goto_0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 8
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 596
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/a;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    .line 598
    packed-switch p1, :pswitch_data_0

    .line 619
    :goto_0
    return-object v0

    .line 602
    :pswitch_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v6

    .line 603
    const/4 v0, 0x5

    const/16 v1, 0x5d

    invoke-virtual {v6, v0, v1}, Ljava/util/Calendar;->add(II)V

    .line 605
    const/4 v7, 0x0

    .line 607
    invoke-direct {p0}, Lcom/chase/sig/android/activity/EPayStartActivity;->h()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/activity/EPayStartActivity;->h(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 608
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v7

    .line 609
    invoke-direct {p0}, Lcom/chase/sig/android/activity/EPayStartActivity;->h()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->t(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 612
    :cond_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v5

    .line 613
    invoke-direct {p0}, Lcom/chase/sig/android/activity/EPayStartActivity;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->t(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 614
    new-instance v0, Lcom/chase/sig/android/view/b;

    iget-object v2, p0, Lcom/chase/sig/android/activity/EPayStartActivity;->q:Lcom/chase/sig/android/view/b$a;

    move-object v1, p0

    move v4, v3

    invoke-direct/range {v0 .. v7}, Lcom/chase/sig/android/view/b;-><init>(Landroid/content/Context;Lcom/chase/sig/android/view/b$a;ZZLjava/util/Calendar;Ljava/util/Calendar;Ljava/util/Calendar;)V

    goto :goto_0

    .line 598
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 588
    invoke-super {p0}, Lcom/chase/sig/android/activity/a;->onResume()V

    .line 590
    invoke-static {}, Lcom/chase/sig/android/c;->a()V

    .line 591
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .parameter

    .prologue
    .line 371
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/a;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 373
    const-string v1, "ePayFromAccounts"

    iget-object v0, p0, Lcom/chase/sig/android/activity/EPayStartActivity;->b:Ljava/util/List;

    check-cast v0, Ljava/io/Serializable;

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 374
    const-string v0, "pay_to_account_id"

    iget-object v1, p0, Lcom/chase/sig/android/activity/EPayStartActivity;->a:Lcom/chase/sig/android/domain/IAccount;

    invoke-interface {v1}, Lcom/chase/sig/android/domain/IAccount;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 375
    const-string v0, "ePayToAccountSelected"

    invoke-direct {p0}, Lcom/chase/sig/android/activity/EPayStartActivity;->j()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/view/JPSpinner;->getSelectedItemPosition()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 376
    const-string v0, "selected_payment_date"

    invoke-direct {p0}, Lcom/chase/sig/android/activity/EPayStartActivity;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 377
    const-string v0, "ePayAmountSelected"

    invoke-direct {p0}, Lcom/chase/sig/android/activity/EPayStartActivity;->D()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/view/JPSpinner;->getSelectedItemPosition()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 379
    const-string v1, "ePayAmountOptions"

    iget-object v0, p0, Lcom/chase/sig/android/activity/EPayStartActivity;->c:Ljava/util/List;

    check-cast v0, Ljava/io/Serializable;

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 380
    const-string v0, "options_loaded_for_account_id"

    iget-object v1, p0, Lcom/chase/sig/android/activity/EPayStartActivity;->m:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 381
    const-string v0, "earliest_payment_date"

    iget-object v1, p0, Lcom/chase/sig/android/activity/EPayStartActivity;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 382
    const-string v0, "due_date"

    invoke-direct {p0}, Lcom/chase/sig/android/activity/EPayStartActivity;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 384
    invoke-direct {p0}, Lcom/chase/sig/android/activity/EPayStartActivity;->D()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/chase/sig/android/activity/EPayStartActivity;->D()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/view/JPSpinner;->getSelectedItemPosition()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/EPayStartActivity;->c:Ljava/util/List;

    invoke-direct {p0}, Lcom/chase/sig/android/activity/EPayStartActivity;->D()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/view/JPSpinner;->getSelectedItemPosition()I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/EPaymentOption;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/EPaymentOption;->d()Ljava/lang/String;

    move-result-object v0

    const-string v1, "-1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 390
    const-string v0, "AMOUNT"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/EPayStartActivity;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/c;

    .line 391
    const-string v1, "ePayOtherAmount"

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/c;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 393
    const-string v1, "visibility"

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/c;->p()Lcom/chase/sig/android/view/AmountView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/view/AmountView;->getVisibility()I

    move-result v0

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 396
    :cond_0
    return-void
.end method
