.class public Lcom/chase/sig/android/activity/EPayVerifyActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$e;


# static fields
.field static a:Ljava/lang/String;


# instance fields
.field private b:Lcom/chase/sig/android/domain/EPayment;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-string v0, "transaction_object"

    sput-object v0, Lcom/chase/sig/android/activity/EPayVerifyActivity;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/EPayVerifyActivity;)Lcom/chase/sig/android/domain/EPayment;
    .locals 1
    .parameter

    .prologue
    .line 15
    iget-object v0, p0, Lcom/chase/sig/android/activity/EPayVerifyActivity;->b:Lcom/chase/sig/android/domain/EPayment;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 11
    .parameter

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 22
    const v0, 0x7f03003d

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/EPayVerifyActivity;->b(I)V

    .line 23
    const v0, 0x7f0701ac

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/EPayVerifyActivity;->setTitle(I)V

    .line 25
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/EPayVerifyActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    sget-object v1, Lcom/chase/sig/android/activity/EPayVerifyActivity;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/EPayment;

    iput-object v0, p0, Lcom/chase/sig/android/activity/EPayVerifyActivity;->b:Lcom/chase/sig/android/domain/EPayment;

    .line 27
    iget-object v0, p0, Lcom/chase/sig/android/activity/EPayVerifyActivity;->b:Lcom/chase/sig/android/domain/EPayment;

    if-eqz v0, :cond_0

    .line 28
    const v0, 0x7f0900cd

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/EPayVerifyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailView;

    .line 29
    const/4 v1, 0x4

    new-array v3, v1, [Lcom/chase/sig/android/view/detail/a;

    new-instance v2, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v4, "To"

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/ChaseApplication;

    invoke-virtual {v1}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v1

    iget-object v1, v1, Lcom/chase/sig/android/domain/o;->b:Lcom/chase/sig/android/domain/g;

    iget-object v5, p0, Lcom/chase/sig/android/activity/EPayVerifyActivity;->b:Lcom/chase/sig/android/domain/EPayment;

    invoke-virtual {v5}, Lcom/chase/sig/android/domain/EPayment;->f()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1, v5}, Lcom/chase/sig/android/domain/g;->a(Ljava/lang/String;)Lcom/chase/sig/android/domain/IAccount;

    move-result-object v5

    invoke-interface {v5}, Lcom/chase/sig/android/domain/IAccount;->g()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/ChaseApplication;

    invoke-virtual {v1}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v1

    iget-object v1, v1, Lcom/chase/sig/android/domain/o;->b:Lcom/chase/sig/android/domain/g;

    invoke-interface {v5}, Lcom/chase/sig/android/domain/IAccount;->b()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v1, v6}, Lcom/chase/sig/android/domain/g;->e(Ljava/lang/String;)Lcom/chase/sig/android/domain/IAccount;

    move-result-object v1

    const-string v6, "%s (%s)"

    new-array v7, v10, [Ljava/lang/Object;

    invoke-interface {v5}, Lcom/chase/sig/android/domain/IAccount;->a()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v7, v8

    invoke-interface {v1}, Lcom/chase/sig/android/domain/IAccount;->c()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v7, v9

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-direct {v2, v4, v1}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v3, v8

    new-instance v1, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v2, "From"

    iget-object v4, p0, Lcom/chase/sig/android/activity/EPayVerifyActivity;->b:Lcom/chase/sig/android/domain/EPayment;

    invoke-virtual {v4}, Lcom/chase/sig/android/domain/EPayment;->i()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v4}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v1, v3, v9

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    iget-object v1, p0, Lcom/chase/sig/android/activity/EPayVerifyActivity;->b:Lcom/chase/sig/android/domain/EPayment;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/EPayment;->g()Ljava/lang/String;

    move-result-object v1

    const-string v2, "3"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const v1, 0x7f0700ee

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/EPayVerifyActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_1
    iget-object v2, p0, Lcom/chase/sig/android/activity/EPayVerifyActivity;->b:Lcom/chase/sig/android/domain/EPayment;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/EPayment;->d()Lcom/chase/sig/android/util/Dollar;

    move-result-object v2

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/chase/sig/android/activity/EPayVerifyActivity;->b:Lcom/chase/sig/android/domain/EPayment;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/EPayment;->d()Lcom/chase/sig/android/util/Dollar;

    move-result-object v2

    invoke-virtual {v2}, Lcom/chase/sig/android/util/Dollar;->h()Ljava/lang/String;

    move-result-object v2

    :goto_2
    invoke-direct {v4, v1, v2}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v3, v10

    const/4 v2, 0x3

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Payment Date"

    iget-object v1, p0, Lcom/chase/sig/android/activity/EPayVerifyActivity;->b:Lcom/chase/sig/android/domain/EPayment;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/EPayment;->e()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_7

    const-string v1, "N/A"

    :goto_3
    invoke-direct {v4, v5, v1}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v3, v2

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/view/detail/DetailView;->setRows([Lcom/chase/sig/android/view/detail/a;)V

    .line 37
    :cond_0
    const v0, 0x7f0900cf

    new-instance v1, Lcom/chase/sig/android/activity/de;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/de;-><init>(Lcom/chase/sig/android/activity/EPayVerifyActivity;)V

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/EPayVerifyActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 47
    const v0, 0x7f0900ce

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/EPayVerifyActivity;->B()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/EPayVerifyActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 48
    return-void

    .line 29
    :cond_1
    invoke-interface {v5}, Lcom/chase/sig/android/domain/IAccount;->w()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_2
    const-string v2, "2"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    const v1, 0x7f0701b9

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/EPayVerifyActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_3
    const-string v2, "-1"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    const v1, 0x7f0701b8

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/EPayVerifyActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_4
    const-string v2, "1"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const v1, 0x7f0701b7

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/EPayVerifyActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_5
    const-string v1, "Amount"

    goto :goto_1

    :cond_6
    const-string v2, ""

    goto :goto_2

    :cond_7
    iget-object v1, p0, Lcom/chase/sig/android/activity/EPayVerifyActivity;->b:Lcom/chase/sig/android/domain/EPayment;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/EPayment;->e()Ljava/lang/String;

    move-result-object v1

    goto :goto_3
.end method
