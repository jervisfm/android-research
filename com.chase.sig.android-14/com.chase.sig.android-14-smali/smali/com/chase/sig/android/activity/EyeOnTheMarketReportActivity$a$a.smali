.class final Lcom/chase/sig/android/activity/EyeOnTheMarketReportActivity$a$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/EyeOnTheMarketReportActivity$a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/EyeOnTheMarketReportActivity$a;


# direct methods
.method private constructor <init>(Lcom/chase/sig/android/activity/EyeOnTheMarketReportActivity$a;)V
    .locals 0
    .parameter

    .prologue
    .line 124
    iput-object p1, p0, Lcom/chase/sig/android/activity/EyeOnTheMarketReportActivity$a$a;->a:Lcom/chase/sig/android/activity/EyeOnTheMarketReportActivity$a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/chase/sig/android/activity/EyeOnTheMarketReportActivity$a;B)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 124
    invoke-direct {p0, p1}, Lcom/chase/sig/android/activity/EyeOnTheMarketReportActivity$a$a;-><init>(Lcom/chase/sig/android/activity/EyeOnTheMarketReportActivity$a;)V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 127
    :try_start_0
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/service/EyeOnTheMarketReport;

    .line 128
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/chase/sig/android/activity/EyeOnTheMarketReportActivity$a$a;->a:Lcom/chase/sig/android/activity/EyeOnTheMarketReportActivity$a;

    iget-object v2, v2, Lcom/chase/sig/android/activity/EyeOnTheMarketReportActivity$a;->a:Lcom/chase/sig/android/activity/EyeOnTheMarketReportActivity;

    const-class v3, Lcom/chase/sig/android/activity/EyeOnTheMarketReportArticleActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 129
    const-string v2, "transaction_object"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 130
    iget-object v0, p0, Lcom/chase/sig/android/activity/EyeOnTheMarketReportActivity$a$a;->a:Lcom/chase/sig/android/activity/EyeOnTheMarketReportActivity$a;

    iget-object v0, v0, Lcom/chase/sig/android/activity/EyeOnTheMarketReportActivity$a;->a:Lcom/chase/sig/android/activity/EyeOnTheMarketReportActivity;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/EyeOnTheMarketReportActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 131
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1, p2, p3}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/widget/AdapterView;Landroid/view/View;I)V

    return-void

    :catch_0
    move-exception v0

    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1, p2, p3}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/widget/AdapterView;Landroid/view/View;I)V

    throw v0
.end method
