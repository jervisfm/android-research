.class public Lcom/chase/sig/android/activity/EyeOnTheMarketReportActivity$b;
.super Lcom/chase/sig/android/b;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/EyeOnTheMarketReportActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/b",
        "<",
        "Lcom/chase/sig/android/activity/EyeOnTheMarketReportActivity;",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/service/EyeOnTheMarketReportResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/chase/sig/android/b;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .parameter

    .prologue
    .line 56
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/EyeOnTheMarketReportActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/EyeOnTheMarketReportActivity;->A()Lcom/chase/sig/android/service/n;

    move-result-object v0

    iget-object v1, v0, Lcom/chase/sig/android/service/n;->m:Lcom/chase/sig/android/service/h;

    if-nez v1, :cond_0

    new-instance v1, Lcom/chase/sig/android/service/h;

    invoke-direct {v1}, Lcom/chase/sig/android/service/h;-><init>()V

    iput-object v1, v0, Lcom/chase/sig/android/service/n;->m:Lcom/chase/sig/android/service/h;

    :cond_0
    iget-object v0, v0, Lcom/chase/sig/android/service/n;->m:Lcom/chase/sig/android/service/h;

    invoke-static {}, Lcom/chase/sig/android/service/h;->a()Lcom/chase/sig/android/service/EyeOnTheMarketReportResponse;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 2
    .parameter

    .prologue
    .line 56
    check-cast p1, Lcom/chase/sig/android/service/EyeOnTheMarketReportResponse;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/EyeOnTheMarketReportActivity;

    invoke-static {v0, p1}, Lcom/chase/sig/android/activity/EyeOnTheMarketReportActivity;->a(Lcom/chase/sig/android/activity/EyeOnTheMarketReportActivity;Lcom/chase/sig/android/service/EyeOnTheMarketReportResponse;)Lcom/chase/sig/android/service/EyeOnTheMarketReportResponse;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/EyeOnTheMarketReportResponse;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/EyeOnTheMarketReportActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/EyeOnTheMarketReportResponse;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/EyeOnTheMarketReportActivity;->b(Ljava/util/List;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/EyeOnTheMarketReportActivity;

    invoke-virtual {v0, p1}, Lcom/chase/sig/android/activity/EyeOnTheMarketReportActivity;->a(Lcom/chase/sig/android/service/EyeOnTheMarketReportResponse;)V

    goto :goto_0
.end method
