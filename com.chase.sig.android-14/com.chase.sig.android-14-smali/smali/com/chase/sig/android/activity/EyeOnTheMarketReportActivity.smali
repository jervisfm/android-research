.class public Lcom/chase/sig/android/activity/EyeOnTheMarketReportActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$c;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/EyeOnTheMarketReportActivity$a;,
        Lcom/chase/sig/android/activity/EyeOnTheMarketReportActivity$b;
    }
.end annotation


# instance fields
.field private a:Lcom/chase/sig/android/service/EyeOnTheMarketReportResponse;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    .line 88
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/EyeOnTheMarketReportActivity;Lcom/chase/sig/android/service/EyeOnTheMarketReportResponse;)Lcom/chase/sig/android/service/EyeOnTheMarketReportResponse;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 24
    iput-object p1, p0, Lcom/chase/sig/android/activity/EyeOnTheMarketReportActivity;->a:Lcom/chase/sig/android/service/EyeOnTheMarketReportResponse;

    return-object p1
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 32
    const v0, 0x7f03003e

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/EyeOnTheMarketReportActivity;->b(I)V

    .line 33
    const v0, 0x7f0702c3

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/EyeOnTheMarketReportActivity;->setTitle(I)V

    .line 34
    const v0, 0x7f0900d2

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/EyeOnTheMarketReportActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/EyeOnTheMarketReportActivity;->a(Landroid/widget/ListView;)V

    .line 36
    const-string v0, "report_response"

    invoke-static {p1, v0}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 37
    const-string v0, "report_response"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/service/EyeOnTheMarketReportResponse;

    iput-object v0, p0, Lcom/chase/sig/android/activity/EyeOnTheMarketReportActivity;->a:Lcom/chase/sig/android/service/EyeOnTheMarketReportResponse;

    .line 38
    iget-object v0, p0, Lcom/chase/sig/android/activity/EyeOnTheMarketReportActivity;->a:Lcom/chase/sig/android/service/EyeOnTheMarketReportResponse;

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/EyeOnTheMarketReportActivity;->a(Lcom/chase/sig/android/service/EyeOnTheMarketReportResponse;)V

    .line 42
    :goto_0
    return-void

    .line 40
    :cond_0
    const-class v0, Lcom/chase/sig/android/activity/EyeOnTheMarketReportActivity$b;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/EyeOnTheMarketReportActivity;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(Lcom/chase/sig/android/service/EyeOnTheMarketReportResponse;)V
    .locals 3
    .parameter

    .prologue
    .line 82
    const v0, 0x7f0900d2

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/EyeOnTheMarketReportActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 83
    new-instance v1, Lcom/chase/sig/android/activity/EyeOnTheMarketReportActivity$a;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/EyeOnTheMarketReportResponse;->a()Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, p0, p0, v2}, Lcom/chase/sig/android/activity/EyeOnTheMarketReportActivity$a;-><init>(Lcom/chase/sig/android/activity/EyeOnTheMarketReportActivity;Landroid/content/Context;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 85
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 86
    return-void
.end method

.method protected final c_()V
    .locals 0

    .prologue
    .line 77
    invoke-super {p0}, Lcom/chase/sig/android/activity/ai;->c_()V

    .line 78
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/EyeOnTheMarketReportActivity;->finish()V

    .line 79
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 46
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ai;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 47
    iget-object v0, p0, Lcom/chase/sig/android/activity/EyeOnTheMarketReportActivity;->a:Lcom/chase/sig/android/service/EyeOnTheMarketReportResponse;

    if-eqz v0, :cond_0

    .line 48
    const-string v0, "report_response"

    iget-object v1, p0, Lcom/chase/sig/android/activity/EyeOnTheMarketReportActivity;->a:Lcom/chase/sig/android/service/EyeOnTheMarketReportResponse;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 50
    :cond_0
    return-void
.end method
