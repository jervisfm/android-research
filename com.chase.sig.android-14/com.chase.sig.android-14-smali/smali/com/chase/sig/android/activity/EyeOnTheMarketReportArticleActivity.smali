.class public Lcom/chase/sig/android/activity/EyeOnTheMarketReportArticleActivity;
.super Lcom/chase/sig/android/activity/eb;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$c;


# instance fields
.field private final a:Ljava/lang/String;

.field private b:Landroid/webkit/WebView;

.field private c:Lcom/chase/sig/android/service/EyeOnTheMarketReport;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/chase/sig/android/activity/eb;-><init>()V

    .line 18
    const-string v0, "https://docs.google.com"

    iput-object v0, p0, Lcom/chase/sig/android/activity/EyeOnTheMarketReportArticleActivity;->a:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/EyeOnTheMarketReportArticleActivity;)Landroid/webkit/WebView;
    .locals 1
    .parameter

    .prologue
    .line 16
    iget-object v0, p0, Lcom/chase/sig/android/activity/EyeOnTheMarketReportArticleActivity;->b:Landroid/webkit/WebView;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 4
    .parameter

    .prologue
    .line 25
    const v0, 0x7f0702c3

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/EyeOnTheMarketReportArticleActivity;->setTitle(I)V

    .line 26
    const v0, 0x7f03003f

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/EyeOnTheMarketReportArticleActivity;->b(I)V

    .line 28
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/EyeOnTheMarketReportArticleActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "transaction_object"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/service/EyeOnTheMarketReport;

    iput-object v0, p0, Lcom/chase/sig/android/activity/EyeOnTheMarketReportArticleActivity;->c:Lcom/chase/sig/android/service/EyeOnTheMarketReport;

    .line 30
    iget-object v0, p0, Lcom/chase/sig/android/activity/EyeOnTheMarketReportArticleActivity;->c:Lcom/chase/sig/android/service/EyeOnTheMarketReport;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/EyeOnTheMarketReport;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/service/JPService;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 31
    const-string v1, "https://docs.google.com"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "viewer"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "embedded"

    const-string v3, "true"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "url"

    invoke-virtual {v1, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    .line 35
    const v0, 0x7f0900d3

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/EyeOnTheMarketReportArticleActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/EyeOnTheMarketReportArticleActivity;->b:Landroid/webkit/WebView;

    .line 36
    iget-object v0, p0, Lcom/chase/sig/android/activity/EyeOnTheMarketReportArticleActivity;->b:Landroid/webkit/WebView;

    new-instance v2, Lcom/chase/sig/android/activity/df;

    invoke-direct {v2, p0}, Lcom/chase/sig/android/activity/df;-><init>(Lcom/chase/sig/android/activity/EyeOnTheMarketReportArticleActivity;)V

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 75
    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 76
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Loading EOTM Article URL: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 78
    iget-object v1, p0, Lcom/chase/sig/android/activity/EyeOnTheMarketReportArticleActivity;->b:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 79
    iget-object v1, p0, Lcom/chase/sig/android/activity/EyeOnTheMarketReportArticleActivity;->b:Landroid/webkit/WebView;

    invoke-virtual {v1, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 80
    return-void
.end method

.method protected final c_()V
    .locals 1

    .prologue
    .line 84
    invoke-super {p0}, Lcom/chase/sig/android/activity/eb;->c_()V

    .line 85
    iget-object v0, p0, Lcom/chase/sig/android/activity/EyeOnTheMarketReportArticleActivity;->b:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->stopLoading()V

    .line 86
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/EyeOnTheMarketReportArticleActivity;->finish()V

    .line 87
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .parameter

    .prologue
    .line 92
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/eb;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 93
    return-void
.end method
