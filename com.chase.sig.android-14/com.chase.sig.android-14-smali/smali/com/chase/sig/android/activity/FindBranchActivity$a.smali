.class public Lcom/chase/sig/android/activity/FindBranchActivity$a;
.super Lcom/chase/sig/android/b;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/FindBranchActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/b",
        "<",
        "Lcom/chase/sig/android/activity/FindBranchActivity;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/service/BranchLocateResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 249
    invoke-direct {p0}, Lcom/chase/sig/android/b;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .parameter

    .prologue
    .line 249
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/FindBranchActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/FindBranchActivity;->A()Lcom/chase/sig/android/service/n;

    move-result-object v0

    iget-object v1, v0, Lcom/chase/sig/android/service/n;->h:Lcom/chase/sig/android/service/d;

    if-nez v1, :cond_0

    new-instance v1, Lcom/chase/sig/android/service/d;

    invoke-direct {v1}, Lcom/chase/sig/android/service/d;-><init>()V

    iput-object v1, v0, Lcom/chase/sig/android/service/n;->h:Lcom/chase/sig/android/service/d;

    :cond_0
    iget-object v0, v0, Lcom/chase/sig/android/service/n;->h:Lcom/chase/sig/android/service/d;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/FindBranchActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/FindBranchActivity;->c()Landroid/location/Location;

    move-result-object v1

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/FindBranchActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/FindBranchActivity;->b(Lcom/chase/sig/android/activity/FindBranchActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/chase/sig/android/service/d;->a(Landroid/location/Location;Ljava/lang/String;)Lcom/chase/sig/android/service/BranchLocateResponse;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 3
    .parameter

    .prologue
    .line 249
    check-cast p1, Lcom/chase/sig/android/service/BranchLocateResponse;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/BranchLocateResponse;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/FindBranchActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/BranchLocateResponse;->g()Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/service/IServiceError;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/FindBranchActivity;->a(Lcom/chase/sig/android/service/IServiceError;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/chase/sig/android/service/BranchLocateResponse;->a()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/FindBranchActivity;

    const v1, 0x7f070110

    invoke-static {v0, v1}, Lcom/chase/sig/android/activity/FindBranchActivity;->a(Lcom/chase/sig/android/activity/FindBranchActivity;I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/FindBranchActivity;

    invoke-static {v0, v1}, Lcom/chase/sig/android/activity/FindBranchActivity;->a(Lcom/chase/sig/android/activity/FindBranchActivity;Ljava/util/List;)V

    goto :goto_0
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 254
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/FindBranchActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/FindBranchActivity;->j(Lcom/chase/sig/android/activity/FindBranchActivity;)Landroid/widget/ListView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 255
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/FindBranchActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/FindBranchActivity;->f(Lcom/chase/sig/android/activity/FindBranchActivity;)Ljava/util/List;

    .line 256
    invoke-super {p0}, Lcom/chase/sig/android/b;->onPreExecute()V

    .line 257
    return-void
.end method
