.class public Lcom/chase/sig/android/activity/FindBranchActivity$b;
.super Lcom/chase/sig/android/activity/ae;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/FindBranchActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/activity/ae",
        "<",
        "Lcom/chase/sig/android/activity/FindBranchActivity;",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Landroid/location/Location;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 205
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ae;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .parameter

    .prologue
    .line 205
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/FindBranchActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/FindBranchActivity;->A()Lcom/chase/sig/android/service/n;

    move-result-object v0

    iget-object v1, v0, Lcom/chase/sig/android/service/n;->j:Lcom/chase/sig/android/service/f;

    if-nez v1, :cond_0

    new-instance v1, Lcom/chase/sig/android/service/f;

    invoke-direct {v1}, Lcom/chase/sig/android/service/f;-><init>()V

    iput-object v1, v0, Lcom/chase/sig/android/service/n;->j:Lcom/chase/sig/android/service/f;

    :cond_0
    iget-object v0, v0, Lcom/chase/sig/android/service/n;->j:Lcom/chase/sig/android/service/f;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/f;->a()Landroid/location/Location;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 3
    .parameter

    .prologue
    .line 205
    check-cast p1, Landroid/location/Location;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/FindBranchActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/FindBranchActivity;->h(Lcom/chase/sig/android/activity/FindBranchActivity;)V

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/FindBranchActivity$b;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/FindBranchActivity;

    invoke-static {v0, p1}, Lcom/chase/sig/android/activity/FindBranchActivity;->a(Lcom/chase/sig/android/activity/FindBranchActivity;Landroid/location/Location;)Landroid/location/Location;

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/FindBranchActivity;

    const-class v1, Lcom/chase/sig/android/activity/FindBranchActivity$a;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/chase/sig/android/activity/FindBranchActivity;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/FindBranchActivity;

    const v1, 0x7f07010f

    invoke-static {v0, v1}, Lcom/chase/sig/android/activity/FindBranchActivity;->a(Lcom/chase/sig/android/activity/FindBranchActivity;I)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/FindBranchActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/FindBranchActivity;->i(Lcom/chase/sig/android/activity/FindBranchActivity;)V

    goto :goto_0
.end method

.method protected onCancelled()V
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/FindBranchActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/FindBranchActivity;->h(Lcom/chase/sig/android/activity/FindBranchActivity;)V

    .line 227
    return-void
.end method

.method protected onPreExecute()V
    .locals 1

    .prologue
    .line 210
    invoke-super {p0}, Lcom/chase/sig/android/activity/ae;->onPreExecute()V

    .line 211
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/FindBranchActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/FindBranchActivity;->f(Lcom/chase/sig/android/activity/FindBranchActivity;)Ljava/util/List;

    .line 212
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/FindBranchActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/FindBranchActivity;->g(Lcom/chase/sig/android/activity/FindBranchActivity;)V

    .line 213
    return-void
.end method
