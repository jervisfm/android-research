.class public Lcom/chase/sig/android/activity/FindBranchActivity;
.super Lcom/chase/sig/android/activity/eb;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/FindBranchActivity$a;,
        Lcom/chase/sig/android/activity/FindBranchActivity$b;
    }
.end annotation


# instance fields
.field a:Landroid/widget/TextView;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/BranchLocation;",
            ">;"
        }
    .end annotation
.end field

.field private c:Landroid/widget/ListView;

.field private d:Landroid/widget/AutoCompleteTextView;

.field private k:Landroid/location/Location;

.field private l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private m:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private n:Landroid/widget/AdapterView$OnItemClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/chase/sig/android/activity/eb;-><init>()V

    .line 144
    new-instance v0, Lcom/chase/sig/android/activity/di;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/activity/di;-><init>(Lcom/chase/sig/android/activity/FindBranchActivity;)V

    iput-object v0, p0, Lcom/chase/sig/android/activity/FindBranchActivity;->n:Landroid/widget/AdapterView$OnItemClickListener;

    .line 249
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/FindBranchActivity;Landroid/location/Location;)Landroid/location/Location;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 37
    iput-object p1, p0, Lcom/chase/sig/android/activity/FindBranchActivity;->k:Landroid/location/Location;

    return-object p1
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/FindBranchActivity;)Landroid/widget/AutoCompleteTextView;
    .locals 1
    .parameter

    .prologue
    .line 37
    iget-object v0, p0, Lcom/chase/sig/android/activity/FindBranchActivity;->d:Landroid/widget/AutoCompleteTextView;

    return-object v0
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/FindBranchActivity;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/chase/sig/android/activity/FindBranchActivity;->k(I)V

    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/FindBranchActivity;Ljava/util/List;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/chase/sig/android/activity/FindBranchActivity;->a(Ljava/util/List;)V

    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/BranchLocation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 133
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 142
    :cond_0
    :goto_0
    return-void

    .line 137
    :cond_1
    iput-object p1, p0, Lcom/chase/sig/android/activity/FindBranchActivity;->b:Ljava/util/List;

    .line 138
    new-instance v0, Lcom/chase/sig/android/view/u;

    invoke-direct {v0, p0, p1}, Lcom/chase/sig/android/view/u;-><init>(Landroid/content/Context;Ljava/util/List;)V

    .line 140
    iget-object v1, p0, Lcom/chase/sig/android/activity/FindBranchActivity;->c:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 141
    iget-object v0, p0, Lcom/chase/sig/android/activity/FindBranchActivity;->c:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/FindBranchActivity;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 37
    iget-object v0, p0, Lcom/chase/sig/android/activity/FindBranchActivity;->d:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Lcom/chase/sig/android/activity/FindBranchActivity;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 37
    iget-object v0, p0, Lcom/chase/sig/android/activity/FindBranchActivity;->l:Ljava/util/List;

    return-object v0
.end method

.method static synthetic d(Lcom/chase/sig/android/activity/FindBranchActivity;)Landroid/widget/ArrayAdapter;
    .locals 1
    .parameter

    .prologue
    .line 37
    iget-object v0, p0, Lcom/chase/sig/android/activity/FindBranchActivity;->m:Landroid/widget/ArrayAdapter;

    return-object v0
.end method

.method private d()V
    .locals 3

    .prologue
    const v2, 0x7f0900db

    .line 171
    iget-object v0, p0, Lcom/chase/sig/android/activity/FindBranchActivity;->c:Landroid/widget/ListView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 173
    const v0, 0x7f040004

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 174
    invoke-virtual {p0, v2}, Lcom/chase/sig/android/activity/FindBranchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 175
    invoke-virtual {p0, v2}, Lcom/chase/sig/android/activity/FindBranchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 177
    const v0, 0x7f07010a

    invoke-direct {p0, v0}, Lcom/chase/sig/android/activity/FindBranchActivity;->k(I)V

    .line 178
    return-void
.end method

.method static synthetic e(Lcom/chase/sig/android/activity/FindBranchActivity;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 37
    iget-object v0, p0, Lcom/chase/sig/android/activity/FindBranchActivity;->b:Ljava/util/List;

    return-object v0
.end method

.method static synthetic f(Lcom/chase/sig/android/activity/FindBranchActivity;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 37
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/chase/sig/android/activity/FindBranchActivity;->b:Ljava/util/List;

    return-object v0
.end method

.method static synthetic g(Lcom/chase/sig/android/activity/FindBranchActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/chase/sig/android/activity/FindBranchActivity;->d()V

    return-void
.end method

.method static synthetic h(Lcom/chase/sig/android/activity/FindBranchActivity;)V
    .locals 3
    .parameter

    .prologue
    const v2, 0x7f0900db

    const/16 v1, 0x8

    .line 37
    invoke-virtual {p0, v2}, Lcom/chase/sig/android/activity/FindBranchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    invoke-virtual {p0, v2}, Lcom/chase/sig/android/activity/FindBranchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f0900d9

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/FindBranchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method static synthetic i(Lcom/chase/sig/android/activity/FindBranchActivity;)V
    .locals 2
    .parameter

    .prologue
    .line 37
    const v0, 0x7f0900d8

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/FindBranchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method static synthetic j(Lcom/chase/sig/android/activity/FindBranchActivity;)Landroid/widget/ListView;
    .locals 1
    .parameter

    .prologue
    .line 37
    iget-object v0, p0, Lcom/chase/sig/android/activity/FindBranchActivity;->c:Landroid/widget/ListView;

    return-object v0
.end method

.method private k(I)V
    .locals 2
    .parameter

    .prologue
    .line 192
    const v0, 0x7f0900d9

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/FindBranchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 193
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 194
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 195
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 4
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 54
    const v0, 0x7f030041

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/FindBranchActivity;->b(I)V

    .line 55
    const v0, 0x7f070107

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/FindBranchActivity;->setTitle(I)V

    .line 57
    const v0, 0x7f0900d9

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/FindBranchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/FindBranchActivity;->a:Landroid/widget/TextView;

    .line 59
    const-string v0, "message"

    invoke-static {p1, v0}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 60
    iget-object v1, p0, Lcom/chase/sig/android/activity/FindBranchActivity;->a:Landroid/widget/TextView;

    const-string v0, "message"

    const/4 v3, 0x0

    invoke-static {p1, v0, v3}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 63
    :cond_0
    const v0, 0x7f0900d7

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/FindBranchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AutoCompleteTextView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/FindBranchActivity;->d:Landroid/widget/AutoCompleteTextView;

    .line 64
    iget-object v0, p0, Lcom/chase/sig/android/activity/FindBranchActivity;->d:Landroid/widget/AutoCompleteTextView;

    new-instance v1, Lcom/chase/sig/android/activity/dh;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/dh;-><init>(Lcom/chase/sig/android/activity/FindBranchActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 66
    const-string v0, "prev_searches"

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {p1, v0, v1}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/chase/sig/android/activity/FindBranchActivity;->l:Ljava/util/List;

    .line 69
    new-instance v0, Landroid/widget/ArrayAdapter;

    const v1, 0x109000a

    iget-object v3, p0, Lcom/chase/sig/android/activity/FindBranchActivity;->l:Ljava/util/List;

    invoke-direct {v0, p0, v1, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v0, p0, Lcom/chase/sig/android/activity/FindBranchActivity;->m:Landroid/widget/ArrayAdapter;

    .line 72
    iget-object v0, p0, Lcom/chase/sig/android/activity/FindBranchActivity;->d:Landroid/widget/AutoCompleteTextView;

    iget-object v1, p0, Lcom/chase/sig/android/activity/FindBranchActivity;->m:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 74
    const v0, 0x7f0900d8

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/FindBranchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 75
    new-instance v1, Lcom/chase/sig/android/activity/dg;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/dg;-><init>(Lcom/chase/sig/android/activity/FindBranchActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 84
    const v0, 0x7f0900da

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/FindBranchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/FindBranchActivity;->c:Landroid/widget/ListView;

    .line 85
    iget-object v0, p0, Lcom/chase/sig/android/activity/FindBranchActivity;->c:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/chase/sig/android/activity/FindBranchActivity;->n:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 87
    if-eqz p1, :cond_3

    const-string v0, "atm_locations"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    move v1, v0

    .line 90
    :goto_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/FindBranchActivity;->e:Lcom/chase/sig/android/activity/mc;

    const-class v3, Lcom/chase/sig/android/activity/FindBranchActivity$b;

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/activity/mc;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/ae;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/activity/FindBranchActivity$b;

    .line 92
    invoke-virtual {v0}, Lcom/chase/sig/android/activity/FindBranchActivity$b;->d()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 93
    invoke-direct {p0}, Lcom/chase/sig/android/activity/FindBranchActivity;->d()V

    .line 96
    :cond_1
    if-eqz v1, :cond_4

    .line 97
    const-string v0, "atm_locations"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/chase/sig/android/activity/FindBranchActivity;->b:Ljava/util/List;

    .line 99
    iget-object v0, p0, Lcom/chase/sig/android/activity/FindBranchActivity;->b:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/chase/sig/android/activity/FindBranchActivity;->a(Ljava/util/List;)V

    .line 103
    :cond_2
    :goto_1
    return-void

    :cond_3
    move v1, v2

    .line 87
    goto :goto_0

    .line 100
    :cond_4
    invoke-virtual {v0}, Lcom/chase/sig/android/activity/FindBranchActivity$b;->d()Z

    move-result v0

    if-nez v0, :cond_2

    .line 101
    const-class v0, Lcom/chase/sig/android/activity/FindBranchActivity$b;

    new-array v1, v2, [Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/FindBranchActivity;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public final a(Lcom/chase/sig/android/service/IServiceError;)V
    .locals 2
    .parameter

    .prologue
    .line 291
    iget-object v0, p0, Lcom/chase/sig/android/activity/FindBranchActivity;->a:Landroid/widget/TextView;

    invoke-interface {p1}, Lcom/chase/sig/android/service/IServiceError;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 292
    iget-object v0, p0, Lcom/chase/sig/android/activity/FindBranchActivity;->a:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 293
    return-void
.end method

.method public final c()Landroid/location/Location;
    .locals 1

    .prologue
    .line 287
    iget-object v0, p0, Lcom/chase/sig/android/activity/FindBranchActivity;->k:Landroid/location/Location;

    return-object v0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 160
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/eb;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 161
    const-string v1, "atm_locations"

    iget-object v0, p0, Lcom/chase/sig/android/activity/FindBranchActivity;->b:Ljava/util/List;

    check-cast v0, Ljava/io/Serializable;

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 162
    const-string v1, "prev_searches"

    iget-object v0, p0, Lcom/chase/sig/android/activity/FindBranchActivity;->l:Ljava/util/List;

    check-cast v0, Ljava/io/Serializable;

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 164
    iget-object v0, p0, Lcom/chase/sig/android/activity/FindBranchActivity;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 165
    const-string v0, "message"

    iget-object v1, p0, Lcom/chase/sig/android/activity/FindBranchActivity;->a:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    :cond_0
    return-void
.end method
