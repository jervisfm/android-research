.class public Lcom/chase/sig/android/activity/HeadlinesActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"


# static fields
.field public static final a:Ljava/text/SimpleDateFormat;


# instance fields
.field private b:Lcom/chase/sig/android/view/detail/DetailView;

.field private c:Lcom/chase/sig/android/service/QuoteNewsResponse;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 19
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "h:mm a M/d/yy"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/activity/HeadlinesActivity;->a:Ljava/text/SimpleDateFormat;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 13
    .parameter

    .prologue
    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 28
    const v0, 0x7f0702c8

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/HeadlinesActivity;->setTitle(I)V

    .line 29
    const v0, 0x7f030044

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/HeadlinesActivity;->b(I)V

    .line 31
    const v0, 0x7f090115

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/HeadlinesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/HeadlinesActivity;->b:Lcom/chase/sig/android/view/detail/DetailView;

    .line 32
    iget-object v0, p0, Lcom/chase/sig/android/activity/HeadlinesActivity;->b:Lcom/chase/sig/android/view/detail/DetailView;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/DetailView;->d()V

    .line 34
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/HeadlinesActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "quote_news_response"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/service/QuoteNewsResponse;

    iput-object v0, p0, Lcom/chase/sig/android/activity/HeadlinesActivity;->c:Lcom/chase/sig/android/service/QuoteNewsResponse;

    .line 36
    iget-object v0, p0, Lcom/chase/sig/android/activity/HeadlinesActivity;->c:Lcom/chase/sig/android/service/QuoteNewsResponse;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    .line 37
    :cond_0
    const-string v0, "quote_news_response"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/service/QuoteNewsResponse;

    iput-object v0, p0, Lcom/chase/sig/android/activity/HeadlinesActivity;->c:Lcom/chase/sig/android/service/QuoteNewsResponse;

    .line 40
    :cond_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/HeadlinesActivity;->c:Lcom/chase/sig/android/service/QuoteNewsResponse;

    if-eqz v0, :cond_3

    .line 41
    iget-object v1, p0, Lcom/chase/sig/android/activity/HeadlinesActivity;->b:Lcom/chase/sig/android/view/detail/DetailView;

    iget-object v0, p0, Lcom/chase/sig/android/activity/HeadlinesActivity;->c:Lcom/chase/sig/android/service/QuoteNewsResponse;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/QuoteNewsResponse;->a()Ljava/util/ArrayList;

    move-result-object v0

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/QuoteNewsArticle;

    new-instance v4, Lcom/chase/sig/android/view/detail/t;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuoteNewsArticle;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    const-string v6, " "

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/String;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuoteNewsArticle;->a()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v11

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuoteNewsArticle;->b()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v12

    const/4 v8, 0x2

    sget-object v9, Lcom/chase/sig/android/activity/HeadlinesActivity;->a:Ljava/text/SimpleDateFormat;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuoteNewsArticle;->d()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/chase/sig/android/util/s;->b(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Lcom/chase/sig/android/util/s;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6, v11}, Lcom/chase/sig/android/view/detail/t;-><init>(Ljava/lang/String;Ljava/lang/String;B)V

    iput-boolean v12, v4, Lcom/chase/sig/android/view/detail/a;->m:Z

    new-instance v5, Lcom/chase/sig/android/activity/dj;

    invoke-direct {v5, p0, v0}, Lcom/chase/sig/android/activity/dj;-><init>(Lcom/chase/sig/android/activity/HeadlinesActivity;Lcom/chase/sig/android/domain/QuoteNewsArticle;)V

    iput-object v5, v4, Lcom/chase/sig/android/view/detail/a;->h:Landroid/view/View$OnClickListener;

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    new-array v0, v11, [Lcom/chase/sig/android/view/detail/t;

    invoke-interface {v2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/chase/sig/android/view/detail/a;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/view/detail/DetailView;->setRows([Lcom/chase/sig/android/view/detail/a;)V

    invoke-virtual {v1, v11}, Lcom/chase/sig/android/view/detail/DetailView;->setVisibility(I)V

    .line 43
    :cond_3
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 47
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ai;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 48
    const-string v0, "quote_news_response"

    iget-object v1, p0, Lcom/chase/sig/android/activity/HeadlinesActivity;->c:Lcom/chase/sig/android/service/QuoteNewsResponse;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 49
    return-void
.end method
