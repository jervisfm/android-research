.class public Lcom/chase/sig/android/activity/HomeActivity$a;
.super Lcom/chase/sig/android/b;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/HomeActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/b",
        "<",
        "Lcom/chase/sig/android/activity/HomeActivity;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/service/GenericResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 333
    invoke-direct {p0}, Lcom/chase/sig/android/b;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 333
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/HomeActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/HomeActivity;->A()Lcom/chase/sig/android/service/n;

    new-instance v0, Lcom/chase/sig/android/service/i;

    invoke-direct {v0}, Lcom/chase/sig/android/service/i;-><init>()V

    invoke-static {}, Lcom/chase/sig/android/service/i;->a()Lcom/chase/sig/android/service/GenericResponse;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 5
    .parameter

    .prologue
    .line 333
    check-cast p1, Lcom/chase/sig/android/service/GenericResponse;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/GenericResponse;->e()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/chase/sig/android/service/GenericResponse;->a()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/chase/sig/android/service/GenericResponse;->a()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "extra_upgrade_recommended"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "extra_upgrade_url"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "extra_upgrade_required"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    const-string v4, "extra_os_not_supported"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v2, Lcom/chase/sig/android/domain/SimpleDialogInfo;

    invoke-direct {v2, v1}, Lcom/chase/sig/android/domain/SimpleDialogInfo;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/SimpleDialogInfo;->g()V

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/SimpleDialogInfo;->c()V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/HomeActivity;

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/activity/HomeActivity;->a(Lcom/chase/sig/android/domain/SimpleDialogInfo;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {v2}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/HomeActivity;

    invoke-static {v0, v2}, Lcom/chase/sig/android/activity/HomeActivity;->a(Lcom/chase/sig/android/activity/HomeActivity;Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/HomeActivity;

    invoke-static {v0, v1}, Lcom/chase/sig/android/activity/HomeActivity;->b(Lcom/chase/sig/android/activity/HomeActivity;Ljava/lang/String;)Ljava/lang/String;

    if-eqz v3, :cond_2

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/HomeActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/HomeActivity;->showDialog(I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/HomeActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/HomeActivity;->showDialog(I)V

    goto :goto_0
.end method

.method protected onCancelled()V
    .locals 1

    .prologue
    .line 383
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/HomeActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/HomeActivity;->finish()V

    .line 384
    return-void
.end method
