.class public Lcom/chase/sig/android/activity/HomeActivity$b;
.super Lcom/chase/sig/android/activity/ae;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/HomeActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/activity/ae",
        "<",
        "Lcom/chase/sig/android/activity/HomeActivity;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 469
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ae;-><init>()V

    return-void
.end method

.method private static varargs a()Ljava/lang/Boolean;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 478
    :try_start_0
    invoke-static {}, Landroid/hardware/Camera;->open()Landroid/hardware/Camera;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 479
    :try_start_1
    invoke-virtual {v1}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v2

    .line 481
    invoke-virtual {v2}, Landroid/hardware/Camera$Parameters;->getFocusMode()Ljava/lang/String;

    move-result-object v2

    .line 482
    invoke-static {v2}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "fixed"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    .line 489
    if-eqz v1, :cond_0

    .line 490
    invoke-virtual {v1}, Landroid/hardware/Camera;->release()V

    .line 495
    :cond_0
    :goto_1
    return-object v0

    .line 482
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 486
    :catch_0
    move-exception v1

    move-object v1, v0

    .line 487
    :goto_2
    if-eqz v1, :cond_0

    .line 490
    invoke-virtual {v1}, Landroid/hardware/Camera;->release()V

    goto :goto_1

    .line 489
    :catchall_0
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    :goto_3
    if-eqz v1, :cond_2

    .line 490
    invoke-virtual {v1}, Landroid/hardware/Camera;->release()V

    .line 491
    :cond_2
    throw v0

    .line 489
    :catchall_1
    move-exception v0

    goto :goto_3

    .line 486
    :catch_1
    move-exception v2

    goto :goto_2
.end method


# virtual methods
.method protected final bridge synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 469
    invoke-static {}, Lcom/chase/sig/android/activity/HomeActivity$b;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 2
    .parameter

    .prologue
    .line 469
    check-cast p1, Ljava/lang/Boolean;

    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ae;->a(Ljava/lang/Object;)V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/HomeActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/HomeActivity;->z()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/ChaseApplication;->a(Z)V

    :cond_0
    return-void
.end method
