.class public Lcom/chase/sig/android/activity/HomeActivity;
.super Lcom/chase/sig/android/activity/eb;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/HomeActivity$b;,
        Lcom/chase/sig/android/activity/HomeActivity$a;
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/chase/sig/android/activity/eb;-><init>()V

    .line 469
    return-void
.end method

.method private static a(Lcom/chase/sig/android/ChaseApplication;)Landroid/content/pm/ApplicationInfo;
    .locals 3
    .parameter

    .prologue
    .line 279
    :try_start_0
    invoke-virtual {p0}, Lcom/chase/sig/android/ChaseApplication;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/chase/sig/android/ChaseApplication;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 282
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/HomeActivity;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 52
    iget-object v0, p0, Lcom/chase/sig/android/activity/HomeActivity;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/HomeActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 52
    iput-object p1, p0, Lcom/chase/sig/android/activity/HomeActivity;->b:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/HomeActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 52
    iput-object p1, p0, Lcom/chase/sig/android/activity/HomeActivity;->a:Ljava/lang/String;

    return-object p1
.end method

.method private d()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 559
    .line 562
    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v2, Ljava/io/InputStreamReader;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/HomeActivity;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v3

    const-string v4, "EULA"

    invoke-virtual {v3, v4}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v1, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4

    .line 564
    :try_start_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 566
    :goto_0
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 567
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0xa

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 573
    :catch_0
    move-exception v2

    :goto_1
    if-eqz v1, :cond_0

    .line 577
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 584
    :cond_0
    :goto_2
    return-object v0

    .line 570
    :cond_1
    :try_start_3
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    move-result-object v0

    .line 575
    :try_start_4
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_2

    :catch_1
    move-exception v1

    goto :goto_2

    :catchall_0
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    :goto_3
    if-eqz v1, :cond_2

    .line 577
    :try_start_5
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 579
    :cond_2
    :goto_4
    throw v0

    :catch_2
    move-exception v1

    goto :goto_2

    :catch_3
    move-exception v1

    goto :goto_4

    .line 575
    :catchall_1
    move-exception v0

    goto :goto_3

    .line 573
    :catch_4
    move-exception v1

    move-object v1, v0

    goto :goto_1
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 9
    .parameter

    .prologue
    const v8, 0x7f0900e4

    const v7, 0x7f0900e3

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 67
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/ChaseApplication;

    .line 68
    invoke-static {v0}, Lcom/chase/sig/android/activity/HomeActivity;->a(Lcom/chase/sig/android/ChaseApplication;)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    .line 71
    if-eqz v1, :cond_a

    .line 72
    iget v4, v1, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v4, v4, 0x2

    iput v4, v1, Landroid/content/pm/ApplicationInfo;->flags:I

    if-eqz v4, :cond_1

    move v1, v2

    .line 75
    :goto_0
    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->j()Z

    move-result v4

    if-nez v4, :cond_0

    if-eqz v1, :cond_0

    .line 76
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/HomeActivity;->finish()V

    .line 79
    :cond_0
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v1

    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    const-string v5, "allowed_devices"

    invoke-virtual {v1, v5}, Lcom/chase/sig/android/ChaseApplication;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v5, Ljava/util/StringTokenizer;

    const-string v6, ","

    invoke-direct {v5, v1, v6}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    invoke-virtual {v5}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v5}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    move v1, v3

    .line 72
    goto :goto_0

    .line 79
    :cond_2
    invoke-virtual {v4}, Ljava/util/HashSet;->size()I

    move-result v1

    if-eqz v1, :cond_5

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v5, "android_id"

    invoke-static {v1, v5}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    sget-object v5, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    const-string v6, "google_sdk"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    sget-object v5, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    const-string v6, "sdk"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    :cond_3
    move v1, v2

    :goto_2
    if-nez v1, :cond_6

    .line 80
    const-string v0, "Invalid Device."

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/HomeActivity;->e(Ljava/lang/String;)V

    .line 113
    :goto_3
    return-void

    .line 79
    :cond_4
    invoke-virtual {v4, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    new-array v4, v2, [Ljava/lang/Object;

    aput-object v1, v4, v3

    move v1, v3

    goto :goto_2

    :cond_5
    move v1, v2

    goto :goto_2

    .line 84
    :cond_6
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/ChaseApplication;

    invoke-virtual {v1}, Lcom/chase/sig/android/ChaseApplication;->e()Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v4, "can_auto_focus"

    invoke-interface {v1, v4}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_7

    const-class v1, Lcom/chase/sig/android/activity/HomeActivity$b;

    new-array v4, v3, [Ljava/lang/Void;

    invoke-virtual {p0, v1, v4}, Lcom/chase/sig/android/activity/HomeActivity;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 86
    :cond_7
    if-eqz p1, :cond_9

    .line 87
    const-string v1, "app_update_msg"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/chase/sig/android/activity/HomeActivity;->a:Ljava/lang/String;

    .line 88
    const-string v1, "app_update_url"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/chase/sig/android/activity/HomeActivity;->b:Ljava/lang/String;

    .line 93
    :goto_4
    const v1, 0x7f030045

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/HomeActivity;->b(I)V

    .line 95
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/HomeActivity;->n()Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    const v4, 0x7f02000f

    invoke-virtual {v1, v4}, Landroid/view/View;->setBackgroundResource(I)V

    .line 98
    const v1, 0x7f0900e1

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/HomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 101
    invoke-virtual {p0, v7, p0}, Lcom/chase/sig/android/activity/HomeActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 102
    const v1, 0x7f0900e5

    invoke-virtual {p0, v1, p0}, Lcom/chase/sig/android/activity/HomeActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 103
    invoke-virtual {p0, v8, p0}, Lcom/chase/sig/android/activity/HomeActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 105
    const v1, 0x7f09002e

    const/4 v3, 0x2

    invoke-virtual {p0, v3}, Lcom/chase/sig/android/activity/HomeActivity;->i(I)Landroid/view/View$OnClickListener;

    move-result-object v3

    invoke-virtual {p0, v1, v3}, Lcom/chase/sig/android/activity/HomeActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 107
    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->j()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-virtual {p0, v7}, Lcom/chase/sig/android/activity/HomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v3, Lcom/chase/sig/android/activity/dk;

    invoke-direct {v3, p0, v0}, Lcom/chase/sig/android/activity/dk;-><init>(Lcom/chase/sig/android/activity/HomeActivity;Lcom/chase/sig/android/ChaseApplication;)V

    invoke-virtual {v1, v3}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 109
    :cond_8
    const-class v0, Lcom/chase/sig/android/activity/ContactUsActivity;

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/HomeActivity;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/a/f;

    move-result-object v0

    invoke-virtual {p0, v8, v0}, Lcom/chase/sig/android/activity/HomeActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 110
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/HomeActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 111
    invoke-virtual {v0, v2}, Landroid/view/Window;->setFormat(I)V

    .line 112
    const/16 v1, 0x1000

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    goto/16 :goto_3

    .line 90
    :cond_9
    const-class v1, Lcom/chase/sig/android/activity/HomeActivity$a;

    new-array v4, v3, [Ljava/lang/Void;

    invoke-virtual {p0, v1, v4}, Lcom/chase/sig/android/activity/HomeActivity;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    goto :goto_4

    :cond_a
    move v1, v3

    goto/16 :goto_0
.end method

.method protected final b_()Z
    .locals 1

    .prologue
    .line 117
    const/4 v0, 0x0

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .parameter

    .prologue
    .line 312
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    const/4 v0, 0x0

    .line 314
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 328
    :goto_0
    if-eqz v0, :cond_0

    .line 329
    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/HomeActivity;->startActivity(Landroid/content/Intent;)V

    .line 331
    :cond_0
    return-void

    .line 316
    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/chase/sig/android/activity/LoginActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_0

    .line 320
    :pswitch_1
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/chase/sig/android/activity/FindBranchActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_0

    .line 324
    :pswitch_2
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/chase/sig/android/activity/ContactUsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_0

    .line 314
    :pswitch_data_0
    .packed-switch 0x7f0900e3
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 7
    .parameter

    .prologue
    const v6, 0x7f070082

    const v5, 0x7f07006f

    const/4 v3, 0x1

    const/4 v4, -0x1

    .line 392
    packed-switch p1, :pswitch_data_0

    .line 466
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/eb;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    .line 394
    :pswitch_0
    new-instance v0, Lcom/chase/sig/android/view/k$a;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/view/k$a;-><init>(Landroid/content/Context;)V

    .line 395
    iget-object v1, p0, Lcom/chase/sig/android/activity/HomeActivity;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/k$a;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/k$a;

    move-result-object v1

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/chase/sig/android/view/k$a;->i:Z

    invoke-virtual {v1, v6}, Lcom/chase/sig/android/view/k$a;->c(I)Lcom/chase/sig/android/view/k$a;

    move-result-object v1

    new-instance v2, Lcom/chase/sig/android/activity/dq;

    invoke-direct {v2, p0}, Lcom/chase/sig/android/activity/dq;-><init>(Lcom/chase/sig/android/activity/HomeActivity;)V

    invoke-virtual {v1, v5, v2}, Lcom/chase/sig/android/view/k$a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    .line 411
    invoke-virtual {v0, v4}, Lcom/chase/sig/android/view/k$a;->d(I)Lcom/chase/sig/android/view/k;

    move-result-object v0

    goto :goto_0

    .line 415
    :pswitch_1
    new-instance v0, Lcom/chase/sig/android/view/k$a;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/view/k$a;-><init>(Landroid/content/Context;)V

    .line 416
    iget-object v1, p0, Lcom/chase/sig/android/activity/HomeActivity;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/k$a;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/k$a;

    move-result-object v1

    invoke-virtual {v1, v6}, Lcom/chase/sig/android/view/k$a;->c(I)Lcom/chase/sig/android/view/k$a;

    move-result-object v1

    iput-boolean v3, v1, Lcom/chase/sig/android/view/k$a;->i:Z

    new-instance v2, Lcom/chase/sig/android/activity/ds;

    invoke-direct {v2, p0}, Lcom/chase/sig/android/activity/ds;-><init>(Lcom/chase/sig/android/activity/HomeActivity;)V

    invoke-virtual {v1, v5, v2}, Lcom/chase/sig/android/view/k$a;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    move-result-object v1

    iput-boolean v3, v1, Lcom/chase/sig/android/view/k$a;->i:Z

    const v2, 0x7f070070

    new-instance v3, Lcom/chase/sig/android/activity/dr;

    invoke-direct {v3, p0}, Lcom/chase/sig/android/activity/dr;-><init>(Lcom/chase/sig/android/activity/HomeActivity;)V

    invoke-virtual {v1, v2, v3}, Lcom/chase/sig/android/view/k$a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    .line 442
    invoke-virtual {v0, v4}, Lcom/chase/sig/android/view/k$a;->d(I)Lcom/chase/sig/android/view/k;

    move-result-object v0

    goto :goto_0

    .line 446
    :pswitch_2
    new-instance v2, Lcom/chase/sig/android/view/k$a;

    invoke-direct {v2, p0}, Lcom/chase/sig/android/view/k$a;-><init>(Landroid/content/Context;)V

    .line 448
    const-string v0, "layout_inflater"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/HomeActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 450
    const v1, 0x7f030049

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 452
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/HomeActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/ChaseApplication;

    .line 454
    const v1, 0x7f0900ee

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 455
    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 457
    const v0, 0x7f0900ed

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 458
    const v1, 0x7f070004

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/HomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 460
    iput-object v3, v2, Lcom/chase/sig/android/view/k$a;->k:Landroid/view/View;

    .line 462
    invoke-virtual {v2, v4}, Lcom/chase/sig/android/view/k$a;->d(I)Lcom/chase/sig/android/view/k;

    move-result-object v0

    goto/16 :goto_0

    .line 392
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .parameter

    .prologue
    .line 306
    const/4 v0, 0x0

    return v0
.end method

.method protected onResume()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 297
    invoke-super {p0}, Lcom/chase/sig/android/activity/eb;->onResume()V

    .line 298
    const-string v0, "eula"

    invoke-virtual {p0, v0, v2}, Lcom/chase/sig/android/activity/HomeActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "eula.accepted"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Lcom/chase/sig/android/view/k$a;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/view/k$a;-><init>(Landroid/content/Context;)V

    const v2, 0x7f070286

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/view/k$a;->c(I)Lcom/chase/sig/android/view/k$a;

    move-result-object v2

    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/chase/sig/android/view/k$a;->i:Z

    const v2, 0x7f070287

    new-instance v3, Lcom/chase/sig/android/activity/dt;

    invoke-direct {v3, p0, v0}, Lcom/chase/sig/android/activity/dt;-><init>(Lcom/chase/sig/android/activity/HomeActivity;Landroid/content/SharedPreferences;)V

    invoke-virtual {v1, v2, v3}, Lcom/chase/sig/android/view/k$a;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    const v0, 0x7f070288

    new-instance v2, Lcom/chase/sig/android/activity/du;

    invoke-direct {v2, p0}, Lcom/chase/sig/android/activity/du;-><init>(Lcom/chase/sig/android/activity/HomeActivity;)V

    invoke-virtual {v1, v0, v2}, Lcom/chase/sig/android/view/k$a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    new-instance v0, Lcom/chase/sig/android/activity/dv;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/activity/dv;-><init>(Lcom/chase/sig/android/activity/HomeActivity;)V

    iput-object v0, v1, Lcom/chase/sig/android/view/k$a;->g:Landroid/content/DialogInterface$OnCancelListener;

    invoke-direct {p0}, Lcom/chase/sig/android/activity/HomeActivity;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/view/k$a;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/k$a;

    const/4 v0, -0x1

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/view/k$a;->d(I)Lcom/chase/sig/android/view/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/view/k;->show()V

    .line 299
    :cond_0
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/ChaseApplication;

    .line 300
    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->q()V

    .line 301
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 289
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/eb;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 291
    const-string v0, "app_update_msg"

    iget-object v1, p0, Lcom/chase/sig/android/activity/HomeActivity;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    const-string v0, "app_update_url"

    iget-object v1, p0, Lcom/chase/sig/android/activity/HomeActivity;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 293
    return-void
.end method
