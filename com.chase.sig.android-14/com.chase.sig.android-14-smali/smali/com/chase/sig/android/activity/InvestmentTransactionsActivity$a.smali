.class public Lcom/chase/sig/android/activity/InvestmentTransactionsActivity$a;
.super Lcom/chase/sig/android/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/d",
        "<",
        "Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;",
        "Ljava/lang/Object;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/service/InvestmentTransactionsResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 109
    invoke-direct {p0}, Lcom/chase/sig/android/d;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .parameter

    .prologue
    .line 109
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->A()Lcom/chase/sig/android/service/n;

    move-result-object v0

    iget-object v1, v0, Lcom/chase/sig/android/service/n;->l:Lcom/chase/sig/android/service/k;

    if-nez v1, :cond_0

    new-instance v1, Lcom/chase/sig/android/service/k;

    invoke-direct {v1}, Lcom/chase/sig/android/service/k;-><init>()V

    iput-object v1, v0, Lcom/chase/sig/android/service/n;->l:Lcom/chase/sig/android/service/k;

    :cond_0
    iget-object v0, v0, Lcom/chase/sig/android/service/n;->l:Lcom/chase/sig/android/service/k;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->d(Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;)Lcom/chase/sig/android/domain/IAccount;

    move-result-object v0

    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->b()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->c(Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;)Lcom/chase/sig/android/domain/InvestmentTransaction$FilterType;

    move-result-object v2

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->b(Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0, v2}, Lcom/chase/sig/android/service/k;->a(Ljava/lang/String;Ljava/lang/String;Lcom/chase/sig/android/domain/InvestmentTransaction$FilterType;)Lcom/chase/sig/android/service/InvestmentTransactionsResponse;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 3
    .parameter

    .prologue
    .line 109
    check-cast p1, Lcom/chase/sig/android/service/InvestmentTransactionsResponse;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/InvestmentTransactionsResponse;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/InvestmentTransactionsResponse;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->b(Ljava/util/List;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;

    invoke-static {v0, p1}, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->a(Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;Lcom/chase/sig/android/service/InvestmentTransactionsResponse;)Lcom/chase/sig/android/service/InvestmentTransactionsResponse;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->e(Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;

    invoke-virtual {v0, p1}, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->a(Lcom/chase/sig/android/service/InvestmentTransactionsResponse;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/InvestmentTransactionsResponse;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->a(Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->c(Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;)Lcom/chase/sig/android/domain/InvestmentTransaction$FilterType;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;

    new-instance v1, Lcom/chase/sig/android/domain/InvestmentTransaction$FilterType;

    new-instance v2, Lcom/chase/sig/android/domain/InvestmentTransaction;

    invoke-direct {v2}, Lcom/chase/sig/android/domain/InvestmentTransaction;-><init>()V

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v1, v2}, Lcom/chase/sig/android/domain/InvestmentTransaction$FilterType;-><init>(Lcom/chase/sig/android/domain/InvestmentTransaction;)V

    invoke-static {v0, v1}, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->a(Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;Lcom/chase/sig/android/domain/InvestmentTransaction$FilterType;)Lcom/chase/sig/android/domain/InvestmentTransaction$FilterType;

    :cond_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->c(Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;)Lcom/chase/sig/android/domain/InvestmentTransaction$FilterType;

    move-result-object v0

    invoke-virtual {p1}, Lcom/chase/sig/android/service/InvestmentTransactionsResponse;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/InvestmentTransaction$FilterType;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->c(Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;)Lcom/chase/sig/android/domain/InvestmentTransaction$FilterType;

    move-result-object v0

    const-string v1, "TRID"

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/InvestmentTransaction$FilterType;->b(Ljava/lang/String;)V

    goto :goto_0
.end method
