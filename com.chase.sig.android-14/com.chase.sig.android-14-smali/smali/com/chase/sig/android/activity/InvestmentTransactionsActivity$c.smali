.class final Lcom/chase/sig/android/activity/InvestmentTransactionsActivity$c;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/chase/sig/android/domain/InvestmentTransaction;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/InvestmentTransaction;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/chase/sig/android/view/JPSortableButtonBar;

.field private final d:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;Landroid/content/Context;Ljava/util/List;Lcom/chase/sig/android/view/JPSortableButtonBar;)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Lcom/chase/sig/android/view/JPSortableButtonBar;",
            ")V"
        }
    .end annotation

    .prologue
    .line 208
    iput-object p1, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity$c;->a:Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;

    .line 210
    const v0, 0x7f03004c

    invoke-direct {p0, p2, v0, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 211
    iput-object p3, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity$c;->b:Ljava/util/List;

    .line 212
    iput-object p4, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity$c;->c:Lcom/chase/sig/android/view/JPSortableButtonBar;

    .line 213
    const-string v0, "layout_inflater"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity$c;->d:Landroid/view/LayoutInflater;

    .line 215
    return-void
.end method


# virtual methods
.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 220
    iget-object v0, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity$c;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/InvestmentTransaction;

    .line 222
    if-nez p2, :cond_0

    .line 223
    iget-object v1, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity$c;->d:Landroid/view/LayoutInflater;

    const v2, 0x7f03004c

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 226
    :cond_0
    const v1, 0x7f0900d5

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 227
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/InvestmentTransaction;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 229
    iget-object v1, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity$c;->c:Lcom/chase/sig/android/view/JPSortableButtonBar;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/view/JPSortableButtonBar;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v2

    .line 230
    const v1, 0x7f0900f3

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 231
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/InvestmentTransaction;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/chase/sig/android/util/s;->k(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 232
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setWidth(I)V

    .line 234
    const v1, 0x7f0900f0

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 235
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/InvestmentTransaction;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 237
    const v1, 0x7f0900f4

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 238
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/InvestmentTransaction;->a()Lcom/chase/sig/android/util/Dollar;

    move-result-object v2

    invoke-virtual {v2}, Lcom/chase/sig/android/util/Dollar;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 239
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/InvestmentTransaction;->a()Lcom/chase/sig/android/util/Dollar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/util/Dollar;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 240
    iget-object v0, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity$c;->a:Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f06001e

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 245
    :goto_0
    return-object p2

    .line 242
    :cond_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity$c;->a:Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f060002

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method
