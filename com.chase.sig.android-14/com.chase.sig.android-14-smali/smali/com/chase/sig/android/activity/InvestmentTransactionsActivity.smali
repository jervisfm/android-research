.class public Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$d;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/InvestmentTransactionsActivity$c;,
        Lcom/chase/sig/android/activity/InvestmentTransactionsActivity$b;,
        Lcom/chase/sig/android/activity/InvestmentTransactionsActivity$a;
    }
.end annotation


# instance fields
.field private a:Lcom/chase/sig/android/domain/g;

.field private b:Lcom/chase/sig/android/domain/IAccount;

.field private c:Landroid/widget/ListView;

.field private d:Landroid/widget/TextView;

.field private k:Landroid/widget/TextView;

.field private l:Landroid/widget/TextView;

.field private m:Lcom/chase/sig/android/view/JPSortableButtonBar;

.field private n:Lcom/chase/sig/android/service/InvestmentTransactionsResponse;

.field private o:Lcom/chase/sig/android/domain/InvestmentTransaction$FilterType;

.field private p:Ljava/lang/String;

.field private q:Z

.field private r:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    .line 200
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;Lcom/chase/sig/android/domain/InvestmentTransaction$FilterType;)Lcom/chase/sig/android/domain/InvestmentTransaction$FilterType;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 34
    iput-object p1, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->o:Lcom/chase/sig/android/domain/InvestmentTransaction$FilterType;

    return-object p1
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;)Lcom/chase/sig/android/service/InvestmentTransactionsResponse;
    .locals 1
    .parameter

    .prologue
    .line 34
    iget-object v0, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->n:Lcom/chase/sig/android/service/InvestmentTransactionsResponse;

    return-object v0
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;Lcom/chase/sig/android/service/InvestmentTransactionsResponse;)Lcom/chase/sig/android/service/InvestmentTransactionsResponse;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 34
    iput-object p1, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->n:Lcom/chase/sig/android/service/InvestmentTransactionsResponse;

    return-object p1
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 34
    iput-object p1, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->p:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;IZ)V
    .locals 2
    .parameter
    .parameter
    .parameter

    .prologue
    .line 34
    new-instance v0, Lcom/chase/sig/android/activity/dy;

    invoke-direct {v0, p0, p1}, Lcom/chase/sig/android/activity/dy;-><init>(Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;I)V

    if-eqz p2, :cond_0

    iget-object v1, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->n:Lcom/chase/sig/android/service/InvestmentTransactionsResponse;

    invoke-virtual {v1}, Lcom/chase/sig/android/service/InvestmentTransactionsResponse;->a()Ljava/util/List;

    move-result-object v1

    invoke-static {v1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    :goto_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->n:Lcom/chase/sig/android/service/InvestmentTransactionsResponse;

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->a(Lcom/chase/sig/android/service/InvestmentTransactionsResponse;)V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->n:Lcom/chase/sig/android/service/InvestmentTransactionsResponse;

    invoke-virtual {v1}, Lcom/chase/sig/android/service/InvestmentTransactionsResponse;->a()Ljava/util/List;

    move-result-object v1

    invoke-static {v0}, Ljava/util/Collections;->reverseOrder(Ljava/util/Comparator;)Ljava/util/Comparator;

    move-result-object v0

    invoke-static {v1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 34
    iget-object v0, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->p:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;)Lcom/chase/sig/android/domain/InvestmentTransaction$FilterType;
    .locals 1
    .parameter

    .prologue
    .line 34
    iget-object v0, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->o:Lcom/chase/sig/android/domain/InvestmentTransaction$FilterType;

    return-object v0
.end method

.method static synthetic d(Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;)Lcom/chase/sig/android/domain/IAccount;
    .locals 1
    .parameter

    .prologue
    .line 34
    iget-object v0, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->b:Lcom/chase/sig/android/domain/IAccount;

    return-object v0
.end method

.method private d()V
    .locals 3

    .prologue
    .line 273
    iget-object v0, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->m:Lcom/chase/sig/android/view/JPSortableButtonBar;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/chase/sig/android/view/JPSortableButtonBar;->a(ILjava/lang/Boolean;)V

    .line 274
    return-void
.end method

.method static synthetic e(Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->d()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 6
    .parameter

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 63
    const v0, 0x7f0702ad

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->setTitle(I)V

    .line 64
    const v0, 0x7f03004d

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->b(I)V

    .line 66
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/ChaseApplication;

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->b:Lcom/chase/sig/android/domain/g;

    iput-object v0, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->a:Lcom/chase/sig/android/domain/g;

    .line 68
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 69
    const-string v1, "selectedAccountId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 70
    iget-object v1, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->a:Lcom/chase/sig/android/domain/g;

    invoke-interface {v1, v0}, Lcom/chase/sig/android/domain/g;->a(Ljava/lang/String;)Lcom/chase/sig/android/domain/IAccount;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->b:Lcom/chase/sig/android/domain/IAccount;

    .line 72
    const v0, 0x7f09000d

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->d:Landroid/widget/TextView;

    .line 73
    iget-object v0, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->d:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->b:Lcom/chase/sig/android/domain/IAccount;

    invoke-interface {v1}, Lcom/chase/sig/android/domain/IAccount;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 75
    const v0, 0x7f0900f7

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->k:Landroid/widget/TextView;

    .line 76
    iget-object v0, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->k:Landroid/widget/TextView;

    const-string v1, "(%s)"

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->b:Lcom/chase/sig/android/domain/IAccount;

    invoke-interface {v3}, Lcom/chase/sig/android/domain/IAccount;->c()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 78
    const v0, 0x7f0900fb

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->l:Landroid/widget/TextView;

    .line 79
    const v0, 0x7f0900fa

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->c:Landroid/widget/ListView;

    .line 81
    const v0, 0x7f0900f9

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/JPSortableButtonBar;

    iput-object v0, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->m:Lcom/chase/sig/android/view/JPSortableButtonBar;

    iget-object v0, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->m:Lcom/chase/sig/android/view/JPSortableButtonBar;

    const v1, 0x7f070088

    invoke-virtual {v0, v5, v1}, Lcom/chase/sig/android/view/JPSortableButtonBar;->a(II)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->m:Lcom/chase/sig/android/view/JPSortableButtonBar;

    const/4 v1, 0x2

    const v2, 0x7f070089

    invoke-virtual {v0, v1, v2}, Lcom/chase/sig/android/view/JPSortableButtonBar;->a(II)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->m:Lcom/chase/sig/android/view/JPSortableButtonBar;

    const/4 v1, 0x3

    const v2, 0x7f07008a

    invoke-virtual {v0, v1, v2}, Lcom/chase/sig/android/view/JPSortableButtonBar;->a(II)V

    invoke-direct {p0}, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->d()V

    iget-object v0, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->m:Lcom/chase/sig/android/view/JPSortableButtonBar;

    new-instance v1, Lcom/chase/sig/android/activity/dx;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/dx;-><init>(Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;)V

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/JPSortableButtonBar;->setSortableButtonBarListener(Lcom/chase/sig/android/view/JPSortableButtonBar$a;)V

    .line 82
    if-nez p1, :cond_0

    .line 83
    const-class v0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity$a;

    new-array v1, v4, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 93
    :goto_0
    const v0, 0x7f0900f8

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->r:Landroid/widget/Button;

    .line 94
    iget-object v0, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->r:Landroid/widget/Button;

    new-instance v1, Lcom/chase/sig/android/activity/dw;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/dw;-><init>(Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 107
    return-void

    .line 85
    :cond_0
    const-string v0, "fromFilter"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->q:Z

    .line 86
    const-string v0, "transactionsResponse"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/service/InvestmentTransactionsResponse;

    iput-object v0, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->n:Lcom/chase/sig/android/service/InvestmentTransactionsResponse;

    .line 88
    const-string v0, "filterType"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/InvestmentTransaction$FilterType;

    iput-object v0, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->o:Lcom/chase/sig/android/domain/InvestmentTransaction$FilterType;

    .line 89
    const-string v0, "dateRange"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->p:Ljava/lang/String;

    .line 90
    iget-object v0, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->n:Lcom/chase/sig/android/service/InvestmentTransactionsResponse;

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->a(Lcom/chase/sig/android/service/InvestmentTransactionsResponse;)V

    goto :goto_0
.end method

.method public final a(Lcom/chase/sig/android/service/InvestmentTransactionsResponse;)V
    .locals 6
    .parameter

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 163
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/chase/sig/android/service/InvestmentTransactionsResponse;->a()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/chase/sig/android/service/InvestmentTransactionsResponse;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 166
    iget-object v0, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->c:Landroid/widget/ListView;

    new-instance v1, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity$c;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/InvestmentTransactionsResponse;->a()Ljava/util/List;

    move-result-object v2

    iget-object v3, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->m:Lcom/chase/sig/android/view/JPSortableButtonBar;

    invoke-direct {v1, p0, p0, v2, v3}, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity$c;-><init>(Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;Landroid/content/Context;Ljava/util/List;Lcom/chase/sig/android/view/JPSortableButtonBar;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 168
    iget-object v0, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->c:Landroid/widget/ListView;

    new-instance v1, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity$b;

    invoke-direct {v1, p0, v4}, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity$b;-><init>(Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;B)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 170
    iget-object v0, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->c:Landroid/widget/ListView;

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setVisibility(I)V

    .line 171
    iget-object v0, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->m:Lcom/chase/sig/android/view/JPSortableButtonBar;

    invoke-virtual {v0, v4}, Lcom/chase/sig/android/view/JPSortableButtonBar;->setVisibility(I)V

    .line 172
    iget-object v0, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->l:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 185
    :goto_0
    return-void

    .line 175
    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->c:Landroid/widget/ListView;

    invoke-virtual {v0, v5}, Landroid/widget/ListView;->setVisibility(I)V

    .line 176
    iget-object v0, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->m:Lcom/chase/sig/android/view/JPSortableButtonBar;

    invoke-virtual {v0, v5}, Lcom/chase/sig/android/view/JPSortableButtonBar;->setVisibility(I)V

    .line 178
    iget-object v0, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->l:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 179
    iget-boolean v0, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->q:Z

    if-eqz v0, :cond_1

    .line 180
    iget-object v0, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->l:Landroid/widget/TextView;

    const v1, 0x7f0702af

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 182
    :cond_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->l:Landroid/widget/TextView;

    const v1, 0x7f0702ae

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 5
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 251
    if-nez p1, :cond_0

    .line 252
    if-ne p2, v3, :cond_0

    .line 254
    iput-boolean v3, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->q:Z

    .line 255
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 256
    const-string v0, "filter_type"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/InvestmentTransaction$FilterType;

    iput-object v0, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->o:Lcom/chase/sig/android/domain/InvestmentTransaction$FilterType;

    .line 258
    const-string v0, "range"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->p:Ljava/lang/String;

    .line 260
    new-array v0, v3, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->o:Lcom/chase/sig/android/domain/InvestmentTransaction$FilterType;

    aput-object v1, v0, v4

    .line 261
    new-array v0, v3, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->p:Ljava/lang/String;

    aput-object v1, v0, v4

    .line 263
    const-class v0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity$a;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->p:Ljava/lang/String;

    aput-object v2, v1, v4

    iget-object v2, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->o:Lcom/chase/sig/android/domain/InvestmentTransaction$FilterType;

    aput-object v2, v1, v3

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 266
    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 153
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ai;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 154
    const-string v0, "transactionsResponse"

    iget-object v1, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->n:Lcom/chase/sig/android/service/InvestmentTransactionsResponse;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 155
    const-string v0, "fromFilter"

    iget-boolean v1, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->q:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 156
    const-string v0, "filterType"

    iget-object v1, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->o:Lcom/chase/sig/android/domain/InvestmentTransaction$FilterType;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 157
    const-string v0, "dateRange"

    iget-object v1, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->p:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    return-void
.end method
