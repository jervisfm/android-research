.class public Lcom/chase/sig/android/activity/InvestmentTransactionsFilterActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$d;


# static fields
.field private static final d:[Ljava/lang/String;

.field private static final k:[Ljava/lang/String;


# instance fields
.field private a:Lcom/chase/sig/android/view/JPSpinner;

.field private b:Lcom/chase/sig/android/view/JPSpinner;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/InvestmentTransaction$FilterType;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 34
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "Current Day"

    aput-object v1, v0, v2

    const-string v1, "Prior Day"

    aput-object v1, v0, v3

    const-string v1, "Last 7 Days"

    aput-object v1, v0, v4

    const-string v1, "Last 14 Days"

    aput-object v1, v0, v5

    const-string v1, "Last 30 Days"

    aput-object v1, v0, v6

    sput-object v0, Lcom/chase/sig/android/activity/InvestmentTransactionsFilterActivity;->d:[Ljava/lang/String;

    .line 36
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "CURRENT_DAY"

    aput-object v1, v0, v2

    const-string v1, "PRIOR_DAY"

    aput-object v1, v0, v3

    const-string v1, "LAST_7_DAYS"

    aput-object v1, v0, v4

    const-string v1, "LAST_14_DAYS"

    aput-object v1, v0, v5

    const-string v1, "LAST_30_DAYS"

    aput-object v1, v0, v6

    sput-object v0, Lcom/chase/sig/android/activity/InvestmentTransactionsFilterActivity;->k:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/InvestmentTransactionsFilterActivity;)V
    .locals 3
    .parameter

    .prologue
    .line 20
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    iget-object v0, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsFilterActivity;->c:Ljava/util/List;

    iget-object v2, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsFilterActivity;->a:Lcom/chase/sig/android/view/JPSpinner;

    invoke-virtual {v2}, Lcom/chase/sig/android/view/JPSpinner;->getPosition()I

    move-result v2

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/InvestmentTransaction$FilterType;

    const-string v2, "filter_type"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    sget-object v0, Lcom/chase/sig/android/activity/InvestmentTransactionsFilterActivity;->k:[Ljava/lang/String;

    iget-object v2, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsFilterActivity;->b:Lcom/chase/sig/android/view/JPSpinner;

    invoke-virtual {v2}, Lcom/chase/sig/android/view/JPSpinner;->getPosition()I

    move-result v2

    aget-object v0, v0, v2

    const-string v2, "range"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/4 v0, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/InvestmentTransactionsFilterActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/InvestmentTransactionsFilterActivity;->finish()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 7
    .parameter

    .prologue
    const v6, 0x7f0300af

    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 42
    const v0, 0x7f0702b1

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/InvestmentTransactionsFilterActivity;->setTitle(I)V

    .line 43
    const v0, 0x7f03004b

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/InvestmentTransactionsFilterActivity;->b(I)V

    .line 45
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/InvestmentTransactionsFilterActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 47
    const v0, 0x7f0900f0

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/InvestmentTransactionsFilterActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/JPSpinner;

    iput-object v0, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsFilterActivity;->a:Lcom/chase/sig/android/view/JPSpinner;

    .line 48
    iget-object v3, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsFilterActivity;->a:Lcom/chase/sig/android/view/JPSpinner;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/InvestmentTransactionsFilterActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v4, "filter_types"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsFilterActivity;->c:Ljava/util/List;

    new-instance v0, Landroid/widget/ArrayAdapter;

    iget-object v4, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsFilterActivity;->c:Ljava/util/List;

    invoke-direct {v0, p0, v6, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {v3, v0}, Lcom/chase/sig/android/view/JPSpinner;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 49
    iget-object v0, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsFilterActivity;->a:Lcom/chase/sig/android/view/JPSpinner;

    invoke-virtual {v0, v5}, Lcom/chase/sig/android/view/JPSpinner;->setChoiceMode(I)V

    .line 51
    const-string v0, "filter_type"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 52
    const-string v0, "filter_type"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/InvestmentTransaction$FilterType;

    new-instance v3, Lcom/chase/sig/android/util/r;

    invoke-direct {v3}, Lcom/chase/sig/android/util/r;-><init>()V

    iget-object v3, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsFilterActivity;->c:Ljava/util/List;

    new-instance v4, Lcom/chase/sig/android/activity/ea;

    invoke-direct {v4, p0, v0}, Lcom/chase/sig/android/activity/ea;-><init>(Lcom/chase/sig/android/activity/InvestmentTransactionsFilterActivity;Lcom/chase/sig/android/domain/InvestmentTransaction$FilterType;)V

    invoke-static {v3, v4}, Lcom/chase/sig/android/util/r;->a(Ljava/util/List;Lcom/chase/sig/android/util/q;)I

    move-result v0

    if-ltz v0, :cond_1

    :goto_0
    iget-object v3, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsFilterActivity;->a:Lcom/chase/sig/android/view/JPSpinner;

    invoke-virtual {v3, v0}, Lcom/chase/sig/android/view/JPSpinner;->setSelection(I)V

    .line 57
    :goto_1
    const v0, 0x7f0900f1

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/InvestmentTransactionsFilterActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/JPSpinner;

    iput-object v0, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsFilterActivity;->b:Lcom/chase/sig/android/view/JPSpinner;

    .line 58
    iget-object v0, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsFilterActivity;->b:Lcom/chase/sig/android/view/JPSpinner;

    new-instance v3, Landroid/widget/ArrayAdapter;

    sget-object v4, Lcom/chase/sig/android/activity/InvestmentTransactionsFilterActivity;->d:[Ljava/lang/String;

    invoke-direct {v3, p0, v6, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/view/JPSpinner;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 59
    iget-object v0, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsFilterActivity;->b:Lcom/chase/sig/android/view/JPSpinner;

    invoke-virtual {v0, v5}, Lcom/chase/sig/android/view/JPSpinner;->setChoiceMode(I)V

    .line 62
    const-string v0, "range"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 63
    invoke-static {v2}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    .line 64
    :goto_2
    sget-object v3, Lcom/chase/sig/android/activity/InvestmentTransactionsFilterActivity;->k:[Ljava/lang/String;

    array-length v3, v3

    if-ge v1, v3, :cond_4

    .line 65
    sget-object v3, Lcom/chase/sig/android/activity/InvestmentTransactionsFilterActivity;->k:[Ljava/lang/String;

    aget-object v3, v3, v1

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v0, v1

    .line 64
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_1
    move v0, v1

    .line 52
    goto :goto_0

    .line 54
    :cond_2
    iget-object v0, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsFilterActivity;->a:Lcom/chase/sig/android/view/JPSpinner;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/JPSpinner;->setSelection(I)V

    goto :goto_1

    :cond_3
    move v0, v1

    .line 71
    :cond_4
    iget-object v1, p0, Lcom/chase/sig/android/activity/InvestmentTransactionsFilterActivity;->b:Lcom/chase/sig/android/view/JPSpinner;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/view/JPSpinner;->setSelection(I)V

    .line 73
    const v0, 0x7f090070

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/InvestmentTransactionsFilterActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/chase/sig/android/activity/dz;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/dz;-><init>(Lcom/chase/sig/android/activity/InvestmentTransactionsFilterActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 80
    return-void
.end method
