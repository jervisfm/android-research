.class public Lcom/chase/sig/android/activity/LocationInfoActivity;
.super Lcom/chase/sig/android/activity/eb;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/chase/sig/android/activity/eb;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 7
    .parameter

    .prologue
    const/16 v6, 0x8

    .line 21
    const v0, 0x7f030058

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/LocationInfoActivity;->b(I)V

    .line 23
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/LocationInfoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "location"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/BranchLocation;

    .line 24
    const-string v1, "branch"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BranchLocation;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    .line 26
    if-eqz v2, :cond_0

    const v1, 0x7f070109

    :goto_0
    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/LocationInfoActivity;->setTitle(I)V

    .line 28
    const v1, 0x7f090122

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/LocationInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BranchLocation;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 29
    const v1, 0x7f090126

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/LocationInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BranchLocation;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BranchLocation;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BranchLocation;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BranchLocation;->h()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 31
    const v1, 0x7f09012c

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/LocationInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BranchLocation;->j()D

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " miles"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 33
    if-eqz v2, :cond_1

    .line 34
    const v1, 0x7f09012f

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/LocationInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BranchLocation;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 35
    const v1, 0x7f090132

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/LocationInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const-string v2, "\n"

    invoke-static {v2}, Lcom/google/common/a/a;->a(Ljava/lang/String;)Lcom/google/common/a/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/common/a/a;->a()Lcom/google/common/a/a;

    move-result-object v2

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BranchLocation;->k()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/common/a/a;->a(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 37
    const v1, 0x7f090135

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/LocationInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const-string v2, "\n"

    invoke-static {v2}, Lcom/google/common/a/a;->a(Ljava/lang/String;)Lcom/google/common/a/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/common/a/a;->a()Lcom/google/common/a/a;

    move-result-object v2

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BranchLocation;->l()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/common/a/a;->a(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 39
    const v1, 0x7f090129

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/LocationInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BranchLocation;->f()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/chase/sig/android/util/s;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 41
    const v1, 0x7f090139

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/LocationInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    .line 42
    const v1, 0x7f09013c

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/LocationInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    .line 43
    const v1, 0x7f090136

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/LocationInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    .line 56
    :goto_1
    const v1, 0x7f09013f

    new-instance v2, Lcom/chase/sig/android/activity/ev;

    invoke-direct {v2, p0, v0}, Lcom/chase/sig/android/activity/ev;-><init>(Lcom/chase/sig/android/activity/LocationInfoActivity;Lcom/chase/sig/android/domain/BranchLocation;)V

    invoke-virtual {p0, v1, v2}, Lcom/chase/sig/android/activity/LocationInfoActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 73
    return-void

    .line 26
    :cond_0
    const v1, 0x7f070108

    goto/16 :goto_0

    .line 45
    :cond_1
    const v1, 0x7f090138

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/LocationInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BranchLocation;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 46
    const v1, 0x7f09013b

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/LocationInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const-string v2, "\n"

    invoke-static {v2}, Lcom/google/common/a/a;->a(Ljava/lang/String;)Lcom/google/common/a/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/common/a/a;->a()Lcom/google/common/a/a;

    move-result-object v2

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BranchLocation;->n()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/common/a/a;->a(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 48
    const v1, 0x7f09013e

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/LocationInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const-string v2, "\n"

    invoke-static {v2}, Lcom/google/common/a/a;->a(Ljava/lang/String;)Lcom/google/common/a/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/common/a/a;->a()Lcom/google/common/a/a;

    move-result-object v2

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BranchLocation;->m()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/common/a/a;->a(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 50
    const v1, 0x7f090127

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/LocationInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    .line 51
    const v1, 0x7f090133

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/LocationInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    .line 52
    const v1, 0x7f090130

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/LocationInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    .line 53
    const v1, 0x7f09012d

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/LocationInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1
.end method
