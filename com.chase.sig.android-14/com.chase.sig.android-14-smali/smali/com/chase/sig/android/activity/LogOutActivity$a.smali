.class public Lcom/chase/sig/android/activity/LogOutActivity$a;
.super Lcom/chase/sig/android/activity/ae;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/LogOutActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/activity/ae",
        "<",
        "Lcom/chase/sig/android/activity/eb;",
        "Ljava/lang/Boolean;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ae;-><init>()V

    return-void
.end method

.method private varargs a([Ljava/lang/Boolean;)Ljava/lang/Void;
    .locals 4
    .parameter

    .prologue
    .line 44
    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/chase/sig/android/activity/LogOutActivity$a;->a:Z

    .line 46
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/eb;->A()Lcom/chase/sig/android/service/n;

    move-result-object v0

    iget-object v1, v0, Lcom/chase/sig/android/service/n;->a:Lcom/chase/sig/android/service/q;

    if-nez v1, :cond_0

    new-instance v1, Lcom/chase/sig/android/service/q;

    invoke-direct {v1}, Lcom/chase/sig/android/service/q;-><init>()V

    iput-object v1, v0, Lcom/chase/sig/android/service/n;->a:Lcom/chase/sig/android/service/q;

    :cond_0
    iget-object v0, v0, Lcom/chase/sig/android/service/n;->a:Lcom/chase/sig/android/service/q;

    const v0, 0x7f070009

    :try_start_0
    invoke-static {v0}, Lcom/chase/sig/android/service/q;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/chase/sig/android/service/q;->c()Ljava/util/Hashtable;

    move-result-object v1

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v2

    const-string v3, "userId"

    invoke-virtual {v2}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v2

    iget-object v2, v2, Lcom/chase/sig/android/domain/o;->a:Lcom/chase/sig/android/domain/a;

    iget-object v2, v2, Lcom/chase/sig/android/domain/a;->c:Ljava/lang/String;

    invoke-virtual {v1, v3, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v2

    invoke-static {v2, v0, v1}, Lcom/chase/sig/android/util/m;->a(Lcom/chase/sig/android/ChaseApplication;Ljava/lang/String;Ljava/util/Hashtable;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 48
    :goto_0
    const/4 v0, 0x0

    return-object v0

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method protected final bridge synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 36
    check-cast p1, [Ljava/lang/Boolean;

    invoke-direct {p0, p1}, Lcom/chase/sig/android/activity/LogOutActivity$a;->a([Ljava/lang/Boolean;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 3
    .parameter

    .prologue
    .line 36
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    const-class v2, Lcom/chase/sig/android/activity/LoginActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v1, "SESSION_TIMED_OUT"

    iget-boolean v2, p0, Lcom/chase/sig/android/activity/LogOutActivity$a;->a:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object v1, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/activity/eb;->startActivity(Landroid/content/Intent;)V

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->q()V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    const/4 v1, -0x3

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/eb;->removeDialog(I)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 53
    invoke-super {p0}, Lcom/chase/sig/android/activity/ae;->onPreExecute()V

    .line 55
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    const/4 v1, -0x3

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/eb;->showDialog(I)V

    .line 56
    return-void
.end method
