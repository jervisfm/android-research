.class public Lcom/chase/sig/android/activity/LogOutActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/LogOutActivity$a;
    }
.end annotation


# instance fields
.field private a:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    .line 36
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 17
    const v0, 0x7f03005b

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/LogOutActivity;->b(I)V

    .line 18
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/chase/sig/android/activity/LogOutActivity;->h:Z

    .line 19
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/LogOutActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 20
    const-string v1, "wasBecauseSessionTimedOut"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/chase/sig/android/activity/LogOutActivity;->a:Z

    .line 21
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 25
    invoke-super {p0}, Lcom/chase/sig/android/activity/ai;->onResume()V

    .line 27
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->u()V

    .line 28
    invoke-static {}, Lcom/chase/sig/android/c;->a()V

    .line 30
    iget-object v0, p0, Lcom/chase/sig/android/activity/LogOutActivity;->e:Lcom/chase/sig/android/activity/mc;

    const-class v1, Lcom/chase/sig/android/activity/LogOutActivity$a;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/mc;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/ae;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/activity/LogOutActivity$a;

    .line 31
    invoke-virtual {v0}, Lcom/chase/sig/android/activity/LogOutActivity$a;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v1

    sget-object v2, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-eq v1, v2, :cond_0

    .line 32
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Boolean;

    const/4 v2, 0x0

    iget-boolean v3, p0, Lcom/chase/sig/android/activity/LogOutActivity;->a:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/LogOutActivity$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 34
    :cond_0
    return-void
.end method
