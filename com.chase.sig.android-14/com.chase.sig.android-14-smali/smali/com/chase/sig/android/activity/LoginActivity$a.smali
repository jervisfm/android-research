.class public Lcom/chase/sig/android/activity/LoginActivity$a;
.super Lcom/chase/sig/android/b;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/LoginActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/b",
        "<",
        "Lcom/chase/sig/android/activity/LoginActivity;",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/service/GenericResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 465
    invoke-direct {p0}, Lcom/chase/sig/android/b;-><init>()V

    return-void
.end method

.method static a(Lcom/chase/sig/android/activity/LoginActivity;)V
    .locals 3
    .parameter

    .prologue
    .line 535
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/LoginActivity;->z()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    .line 538
    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->a:Lcom/chase/sig/android/domain/a;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/a;->a()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 556
    :pswitch_0
    invoke-static {p0}, Lcom/chase/sig/android/activity/LoginActivity;->e(Lcom/chase/sig/android/activity/LoginActivity;)V

    .line 558
    :cond_0
    :goto_0
    return-void

    .line 540
    :pswitch_1
    invoke-static {p0}, Lcom/chase/sig/android/activity/LoginActivity;->d(Lcom/chase/sig/android/activity/LoginActivity;)V

    goto :goto_0

    .line 543
    :pswitch_2
    iget-object v0, p0, Lcom/chase/sig/android/activity/LoginActivity;->e:Lcom/chase/sig/android/activity/mc;

    const-class v1, Lcom/chase/sig/android/activity/LoginActivity$b;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/mc;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/ae;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/activity/LoginActivity$b;

    .line 546
    invoke-virtual {v0}, Lcom/chase/sig/android/activity/LoginActivity$b;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v1

    sget-object v2, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-eq v1, v2, :cond_0

    .line 547
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/LoginActivity$b;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 551
    :pswitch_3
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/LoginActivity;->showDialog(I)V

    goto :goto_0

    .line 538
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8
    .parameter

    .prologue
    const/4 v7, 0x3

    const/4 v3, 0x0

    .line 465
    check-cast p1, [Ljava/lang/String;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/LoginActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/LoginActivity;->A()Lcom/chase/sig/android/service/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/service/n;->b()Lcom/chase/sig/android/service/r;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v1, p1, v1

    const/4 v2, 0x1

    aget-object v2, p1, v2

    array-length v4, p1

    if-lt v4, v7, :cond_1

    const/4 v4, 0x2

    aget-object v5, p1, v4

    :goto_0
    array-length v4, p1

    const/4 v6, 0x4

    if-ne v4, v6, :cond_0

    aget-object v6, p1, v7

    :goto_1
    move-object v4, v3

    invoke-virtual/range {v0 .. v6}, Lcom/chase/sig/android/service/r;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/service/GenericResponse;

    move-result-object v0

    return-object v0

    :cond_0
    move-object v6, v3

    goto :goto_1

    :cond_1
    move-object v5, v3

    goto :goto_0
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 5
    .parameter

    .prologue
    .line 465
    check-cast p1, Lcom/chase/sig/android/service/GenericResponse;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/LoginActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/LoginActivity;->c(Lcom/chase/sig/android/activity/LoginActivity;)V

    invoke-virtual {p1}, Lcom/chase/sig/android/service/GenericResponse;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "2034"

    invoke-virtual {p1, v0}, Lcom/chase/sig/android/service/GenericResponse;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "2034"

    invoke-virtual {p1, v0}, Lcom/chase/sig/android/service/GenericResponse;->d(Ljava/lang/String;)Lcom/chase/sig/android/service/IServiceError;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    const-class v3, Lcom/chase/sig/android/activity/ECDStandInActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "ecd_error"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v0}, Lcom/chase/sig/android/service/IServiceError;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v0}, Lcom/chase/sig/android/service/IServiceError;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/LoginActivity;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/LoginActivity;->startActivity(Landroid/content/Intent;)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "3801"

    invoke-virtual {p1, v0}, Lcom/chase/sig/android/service/GenericResponse;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v1, Landroid/content/Intent;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    const-class v2, Lcom/chase/sig/android/activity/ManagedContentActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/LoginActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/LoginActivity;->z()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    const-string v3, "mbb"

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/ChaseApplication;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/LoginActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/LoginActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f07005e

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "webUrl"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "subuser"

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/LoginActivity;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/LoginActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/LoginActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/GenericResponse;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/LoginActivity;->c(Ljava/util/List;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/LoginActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/LoginActivity;->z()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v1, v0, Lcom/chase/sig/android/domain/o;->b:Lcom/chase/sig/android/domain/g;

    if-eqz v1, :cond_3

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->b:Lcom/chase/sig/android/domain/g;

    invoke-interface {v0}, Lcom/chase/sig/android/domain/g;->F()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/LoginActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/LoginActivity;->z()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->r()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/LoginActivity;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/LoginActivity;->showDialog(I)V

    goto/16 :goto_0

    :cond_3
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/LoginActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/LoginActivity$a;->a(Lcom/chase/sig/android/activity/LoginActivity;)V

    goto/16 :goto_0
.end method

.method protected onCancelled()V
    .locals 1

    .prologue
    .line 562
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/LoginActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/LoginActivity;->c(Lcom/chase/sig/android/activity/LoginActivity;)V

    .line 563
    invoke-super {p0}, Lcom/chase/sig/android/b;->onCancelled()V

    .line 564
    return-void
.end method
