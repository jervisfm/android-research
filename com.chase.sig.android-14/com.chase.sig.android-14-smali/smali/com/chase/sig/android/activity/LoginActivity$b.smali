.class public Lcom/chase/sig/android/activity/LoginActivity$b;
.super Lcom/chase/sig/android/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/LoginActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/d",
        "<",
        "Lcom/chase/sig/android/activity/LoginActivity;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/service/OneTimePasswordContactsResponse;",
        ">;"
    }
.end annotation


# instance fields
.field a:Lcom/chase/sig/android/service/OneTimePasswordContactsResponse;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 599
    invoke-direct {p0}, Lcom/chase/sig/android/d;-><init>()V

    .line 602
    new-instance v0, Lcom/chase/sig/android/service/OneTimePasswordContactsResponse;

    invoke-direct {v0}, Lcom/chase/sig/android/service/OneTimePasswordContactsResponse;-><init>()V

    iput-object v0, p0, Lcom/chase/sig/android/activity/LoginActivity$b;->a:Lcom/chase/sig/android/service/OneTimePasswordContactsResponse;

    return-void
.end method

.method private varargs a()Lcom/chase/sig/android/service/OneTimePasswordContactsResponse;
    .locals 2

    .prologue
    .line 607
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/LoginActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/LoginActivity;->A()Lcom/chase/sig/android/service/n;

    move-result-object v0

    iget-object v1, v0, Lcom/chase/sig/android/service/n;->b:Lcom/chase/sig/android/service/t;

    if-nez v1, :cond_0

    new-instance v1, Lcom/chase/sig/android/service/t;

    invoke-direct {v1}, Lcom/chase/sig/android/service/t;-><init>()V

    iput-object v1, v0, Lcom/chase/sig/android/service/n;->b:Lcom/chase/sig/android/service/t;

    :cond_0
    iget-object v0, v0, Lcom/chase/sig/android/service/n;->b:Lcom/chase/sig/android/service/t;

    .line 610
    :try_start_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/LoginActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/LoginActivity;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/service/t;->a(Lcom/chase/sig/android/domain/o;)Lcom/chase/sig/android/service/OneTimePasswordContactsResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/LoginActivity$b;->a:Lcom/chase/sig/android/service/OneTimePasswordContactsResponse;
    :try_end_0
    .catch Lcom/chase/sig/android/util/ChaseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 615
    :goto_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/LoginActivity$b;->a:Lcom/chase/sig/android/service/OneTimePasswordContactsResponse;

    return-object v0

    .line 612
    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/chase/sig/android/activity/LoginActivity$b;->a:Lcom/chase/sig/android/service/OneTimePasswordContactsResponse;

    iget-object v1, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/OneTimePasswordContactsResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    goto :goto_0
.end method


# virtual methods
.method protected final bridge synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 599
    invoke-direct {p0}, Lcom/chase/sig/android/activity/LoginActivity$b;->a()Lcom/chase/sig/android/service/OneTimePasswordContactsResponse;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 6
    .parameter

    .prologue
    .line 599
    check-cast p1, Lcom/chase/sig/android/service/OneTimePasswordContactsResponse;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/OneTimePasswordContactsResponse;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/LoginActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/OneTimePasswordContactsResponse;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/LoginActivity;->c(Ljava/util/List;)V

    :goto_0
    return-void

    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/OneTimePasswordContactsResponse;->a()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {p1}, Lcom/chase/sig/android/service/OneTimePasswordContactsResponse;->a()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/OneTimePasswordContact;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/OneTimePasswordContact;->c()Ljava/lang/String;

    move-result-object v3

    const-string v4, "PHONE"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    new-instance v3, Lcom/chase/sig/android/domain/OneTimePasswordContact;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/OneTimePasswordContact;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/OneTimePasswordContact;->b()Ljava/lang/String;

    move-result-object v0

    const-string v5, "TEXT"

    invoke-direct {v3, v4, v0, v5}, Lcom/chase/sig/android/domain/OneTimePasswordContact;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/LoginActivity;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/LoginActivity;->a(Ljava/util/List;)V

    goto :goto_0
.end method
