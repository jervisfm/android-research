.class public Lcom/chase/sig/android/activity/LoginActivity$c;
.super Lcom/chase/sig/android/b;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/LoginActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/b",
        "<",
        "Lcom/chase/sig/android/activity/LoginActivity;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/service/SplashResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 442
    invoke-direct {p0}, Lcom/chase/sig/android/b;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .parameter

    .prologue
    .line 442
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/LoginActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/LoginActivity;->A()Lcom/chase/sig/android/service/n;

    move-result-object v0

    iget-object v1, v0, Lcom/chase/sig/android/service/n;->s:Lcom/chase/sig/android/service/ai;

    if-nez v1, :cond_0

    new-instance v1, Lcom/chase/sig/android/service/ai;

    invoke-direct {v1}, Lcom/chase/sig/android/service/ai;-><init>()V

    iput-object v1, v0, Lcom/chase/sig/android/service/n;->s:Lcom/chase/sig/android/service/ai;

    :cond_0
    iget-object v0, v0, Lcom/chase/sig/android/service/n;->s:Lcom/chase/sig/android/service/ai;

    invoke-static {}, Lcom/chase/sig/android/service/ai;->a()Lcom/chase/sig/android/service/SplashResponse;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 3
    .parameter

    .prologue
    .line 442
    check-cast p1, Lcom/chase/sig/android/service/SplashResponse;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/SplashResponse;->w()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/LoginActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/LoginActivity;->d()V

    :goto_0
    return-void

    :cond_0
    new-instance v1, Landroid/content/Intent;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/LoginActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/LoginActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    const-class v2, Lcom/chase/sig/android/activity/ManagedContentActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v0, "webUrl"

    invoke-virtual {p1}, Lcom/chase/sig/android/service/SplashResponse;->v()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/LoginActivity;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/LoginActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
