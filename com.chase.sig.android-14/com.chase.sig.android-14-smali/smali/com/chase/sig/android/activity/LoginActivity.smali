.class public Lcom/chase/sig/android/activity/LoginActivity;
.super Lcom/chase/sig/android/activity/eb;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/LoginActivity$b;,
        Lcom/chase/sig/android/activity/LoginActivity$a;,
        Lcom/chase/sig/android/activity/LoginActivity$c;
    }
.end annotation


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Landroid/widget/EditText;

.field private c:Landroid/widget/EditText;

.field private d:Landroid/widget/CheckBox;

.field private k:Landroid/widget/CheckBox;

.field private l:Landroid/widget/EditText;

.field private m:Landroid/widget/EditText;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/chase/sig/android/activity/eb;-><init>()V

    .line 599
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/LoginActivity;)Landroid/widget/EditText;
    .locals 1
    .parameter

    .prologue
    .line 51
    iget-object v0, p0, Lcom/chase/sig/android/activity/LoginActivity;->l:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/LoginActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/chase/sig/android/activity/LoginActivity;->e()V

    return-void
.end method

.method static synthetic c(Lcom/chase/sig/android/activity/LoginActivity;)V
    .locals 2
    .parameter

    .prologue
    .line 51
    iget-object v0, p0, Lcom/chase/sig/android/activity/LoginActivity;->c:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method static synthetic d(Lcom/chase/sig/android/activity/LoginActivity;)V
    .locals 2
    .parameter

    .prologue
    .line 51
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/chase/sig/android/activity/AccountsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/LoginActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private e()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 156
    new-instance v0, Lcom/chase/sig/android/activity/ey;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/activity/ey;-><init>(Lcom/chase/sig/android/activity/LoginActivity;)V

    .line 175
    invoke-direct {p0}, Lcom/chase/sig/android/activity/LoginActivity;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 176
    iget-object v1, p0, Lcom/chase/sig/android/activity/LoginActivity;->m:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 178
    iget-object v0, p0, Lcom/chase/sig/android/activity/LoginActivity;->c:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 179
    iget-object v0, p0, Lcom/chase/sig/android/activity/LoginActivity;->l:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 189
    :goto_0
    return-void

    .line 180
    :cond_0
    iget-object v1, p0, Lcom/chase/sig/android/activity/LoginActivity;->k:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 181
    iget-object v1, p0, Lcom/chase/sig/android/activity/LoginActivity;->l:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 183
    iget-object v0, p0, Lcom/chase/sig/android/activity/LoginActivity;->c:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    goto :goto_0

    .line 185
    :cond_1
    iget-object v1, p0, Lcom/chase/sig/android/activity/LoginActivity;->c:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 187
    iget-object v0, p0, Lcom/chase/sig/android/activity/LoginActivity;->l:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    goto :goto_0
.end method

.method static synthetic e(Lcom/chase/sig/android/activity/LoginActivity;)V
    .locals 2
    .parameter

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/chase/sig/android/activity/LoginActivity;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0700c8

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/LoginActivity;->h(I)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/LoginActivity;->c:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/LoginActivity;->l:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/LoginActivity;->m:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/LoginActivity;->k:Landroid/widget/CheckBox;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/chase/sig/android/activity/LoginActivity;->k:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f0700c7

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/LoginActivity;->h(I)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/LoginActivity;->c:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/LoginActivity;->l:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    const v0, 0x7f0700c2

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/LoginActivity;->h(I)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/LoginActivity;->c:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private f()Z
    .locals 3

    .prologue
    .line 211
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/LoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "sync_auth_tokens"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 6
    .parameter

    .prologue
    const v5, 0x7f090147

    const/4 v1, 0x0

    .line 78
    invoke-direct {p0}, Lcom/chase/sig/android/activity/LoginActivity;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 79
    const v0, 0x7f0300c2

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/LoginActivity;->b(I)V

    .line 80
    const v0, 0x7f0902c1

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/chase/sig/android/activity/LoginActivity;->m:Landroid/widget/EditText;

    .line 85
    :goto_0
    const v0, 0x7f090144

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/chase/sig/android/activity/LoginActivity;->b:Landroid/widget/EditText;

    .line 86
    const v0, 0x7f090145

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/chase/sig/android/activity/LoginActivity;->c:Landroid/widget/EditText;

    .line 87
    invoke-virtual {p0, v5}, Lcom/chase/sig/android/activity/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/chase/sig/android/activity/LoginActivity;->d:Landroid/widget/CheckBox;

    .line 89
    const v0, 0x7f090148

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/chase/sig/android/activity/LoginActivity;->k:Landroid/widget/CheckBox;

    .line 90
    const v0, 0x7f090146

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/chase/sig/android/activity/LoginActivity;->l:Landroid/widget/EditText;

    .line 92
    const v0, 0x7f090142

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/LoginActivity;->a:Landroid/widget/TextView;

    .line 93
    const-string v0, "chase"

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/LoginActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 94
    const-string v2, "user_id"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 96
    iget-object v3, p0, Lcom/chase/sig/android/activity/LoginActivity;->k:Landroid/widget/CheckBox;

    if-eqz v3, :cond_0

    .line 97
    iget-object v3, p0, Lcom/chase/sig/android/activity/LoginActivity;->k:Landroid/widget/CheckBox;

    const-string v4, "token_user"

    invoke-interface {v0, v4, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {v3, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 99
    iget-object v0, p0, Lcom/chase/sig/android/activity/LoginActivity;->k:Landroid/widget/CheckBox;

    new-instance v3, Lcom/chase/sig/android/activity/ew;

    invoke-direct {v3, p0}, Lcom/chase/sig/android/activity/ew;-><init>(Lcom/chase/sig/android/activity/LoginActivity;)V

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 113
    iget-object v3, p0, Lcom/chase/sig/android/activity/LoginActivity;->l:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/chase/sig/android/activity/LoginActivity;->k:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/widget/EditText;->setVisibility(I)V

    .line 117
    :cond_0
    if-eqz v2, :cond_1

    .line 118
    iget-object v0, p0, Lcom/chase/sig/android/activity/LoginActivity;->b:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 120
    invoke-virtual {p0, v5}, Lcom/chase/sig/android/activity/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 121
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 123
    iget-object v0, p0, Lcom/chase/sig/android/activity/LoginActivity;->a:Landroid/widget/TextView;

    const v1, 0x7f0700ba

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 126
    :cond_1
    const v0, 0x7f090149

    new-instance v1, Lcom/chase/sig/android/activity/ez;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/ez;-><init>(Lcom/chase/sig/android/activity/LoginActivity;)V

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/LoginActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 128
    const v0, 0x7f09014a

    new-instance v1, Lcom/chase/sig/android/activity/ex;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/ex;-><init>(Lcom/chase/sig/android/activity/LoginActivity;)V

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/LoginActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 152
    invoke-direct {p0}, Lcom/chase/sig/android/activity/LoginActivity;->e()V

    .line 153
    return-void

    .line 82
    :cond_2
    const v0, 0x7f03005a

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/LoginActivity;->b(I)V

    goto/16 :goto_0

    .line 113
    :cond_3
    const/16 v0, 0x8

    goto :goto_1
.end method

.method protected final a(Ljava/util/List;)V
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/OneTimePasswordContact;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 572
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/chase/sig/android/activity/DeviceCodeNotFoundActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 573
    const-string v1, "otp_contacts"

    check-cast p1, Ljava/io/Serializable;

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 574
    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/LoginActivity;->startActivity(Landroid/content/Intent;)V

    .line 575
    return-void
.end method

.method protected final b_()Z
    .locals 1

    .prologue
    .line 216
    const/4 v0, 0x0

    return v0
.end method

.method protected final d()V
    .locals 10

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 238
    iget-object v0, p0, Lcom/chase/sig/android/activity/LoginActivity;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 239
    iget-object v1, p0, Lcom/chase/sig/android/activity/LoginActivity;->c:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    .line 243
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 244
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 245
    iget-object v1, p0, Lcom/chase/sig/android/activity/LoginActivity;->b:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 248
    :cond_0
    invoke-static {v0}, Lcom/chase/sig/android/util/s;->l(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-static {v6}, Lcom/chase/sig/android/util/s;->l(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    :cond_1
    move v1, v3

    .line 251
    :goto_0
    invoke-direct {p0}, Lcom/chase/sig/android/activity/LoginActivity;->f()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 252
    iget-object v4, p0, Lcom/chase/sig/android/activity/LoginActivity;->l:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    .line 254
    iget-object v4, p0, Lcom/chase/sig/android/activity/LoginActivity;->m:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 256
    invoke-static {v5}, Lcom/chase/sig/android/util/s;->l(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_2

    invoke-static {v4}, Lcom/chase/sig/android/util/s;->l(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_6

    :cond_2
    move v1, v3

    .line 269
    :goto_1
    iget-object v7, p0, Lcom/chase/sig/android/activity/LoginActivity;->c:Landroid/widget/EditText;

    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-interface {v8}, Landroid/text/Editable;->length()I

    move-result v8

    new-array v8, v8, [C

    const/16 v9, 0x2a

    invoke-static {v8, v9}, Ljava/util/Arrays;->fill([CC)V

    invoke-static {v8}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 271
    if-eqz v1, :cond_b

    .line 273
    iget-object v0, p0, Lcom/chase/sig/android/activity/LoginActivity;->l:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_a

    const v0, 0x7f0700c3

    .line 278
    :goto_2
    invoke-direct {p0}, Lcom/chase/sig/android/activity/LoginActivity;->f()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 279
    const v0, 0x7f0700c5

    .line 282
    :cond_3
    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/LoginActivity;->g(I)V

    .line 296
    :cond_4
    :goto_3
    return-void

    :cond_5
    move v1, v2

    .line 248
    goto :goto_0

    .line 259
    :cond_6
    iget-object v7, p0, Lcom/chase/sig/android/activity/LoginActivity;->l:Landroid/widget/EditText;

    const-string v8, ""

    invoke-virtual {v7, v8}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 260
    iget-object v7, p0, Lcom/chase/sig/android/activity/LoginActivity;->m:Landroid/widget/EditText;

    const-string v8, ""

    invoke-virtual {v7, v8}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 263
    :cond_7
    iget-object v5, p0, Lcom/chase/sig/android/activity/LoginActivity;->k:Landroid/widget/CheckBox;

    if-eqz v5, :cond_e

    iget-object v5, p0, Lcom/chase/sig/android/activity/LoginActivity;->k:Landroid/widget/CheckBox;

    invoke-virtual {v5}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v5

    if-eqz v5, :cond_e

    .line 264
    iget-object v5, p0, Lcom/chase/sig/android/activity/LoginActivity;->l:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    .line 265
    if-nez v1, :cond_8

    invoke-static {v5}, Lcom/chase/sig/android/util/s;->l(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    :cond_8
    move v1, v3

    .line 266
    :goto_4
    iget-object v7, p0, Lcom/chase/sig/android/activity/LoginActivity;->l:Landroid/widget/EditText;

    const-string v8, ""

    invoke-virtual {v7, v8}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_9
    move v1, v2

    .line 265
    goto :goto_4

    .line 273
    :cond_a
    const v0, 0x7f0700c4

    goto :goto_2

    .line 285
    :cond_b
    iget-object v1, p0, Lcom/chase/sig/android/activity/LoginActivity;->d:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    const-string v7, "chase"

    invoke-virtual {p0, v7, v2}, Lcom/chase/sig/android/activity/LoginActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v7

    invoke-interface {v7}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    if-eqz v1, :cond_d

    const-string v1, "user_id"

    invoke-interface {v7, v1, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    :goto_5
    iget-object v1, p0, Lcom/chase/sig/android/activity/LoginActivity;->k:Landroid/widget/CheckBox;

    if-eqz v1, :cond_c

    const-string v1, "token_user"

    iget-object v8, p0, Lcom/chase/sig/android/activity/LoginActivity;->k:Landroid/widget/CheckBox;

    invoke-virtual {v8}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v8

    invoke-interface {v7, v1, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    :cond_c
    invoke-interface {v7}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 287
    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/String;

    aput-object v0, v1, v2

    aput-object v6, v1, v3

    const/4 v0, 0x2

    aput-object v5, v1, v0

    const/4 v0, 0x3

    aput-object v4, v1, v0

    .line 291
    iget-object v0, p0, Lcom/chase/sig/android/activity/LoginActivity;->e:Lcom/chase/sig/android/activity/mc;

    const-class v2, Lcom/chase/sig/android/activity/LoginActivity$a;

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/activity/mc;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/ae;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/activity/LoginActivity$a;

    .line 292
    invoke-virtual {v0}, Lcom/chase/sig/android/activity/LoginActivity$a;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v2

    sget-object v3, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-eq v2, v3, :cond_4

    .line 293
    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/LoginActivity$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_3

    .line 285
    :cond_d
    const-string v1, "user_id"

    invoke-interface {v7, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_5

    :cond_e
    move-object v5, v4

    goto/16 :goto_1
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 7
    .parameter

    .prologue
    const v3, 0x7f070068

    const/4 v6, -0x1

    const/4 v5, 0x0

    .line 316
    sparse-switch p1, :sswitch_data_0

    .line 439
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/eb;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    .line 318
    :sswitch_0
    const v0, 0x7f0700bb

    invoke-virtual {p0, p0, v0}, Lcom/chase/sig/android/activity/LoginActivity;->a(Landroid/content/Context;I)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    .line 322
    :sswitch_1
    new-instance v0, Lcom/chase/sig/android/view/k$a;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/view/k$a;-><init>(Landroid/content/Context;)V

    .line 323
    const v1, 0x7f0700af

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/k$a;->b(I)Lcom/chase/sig/android/view/k$a;

    move-result-object v1

    iput-boolean v5, v1, Lcom/chase/sig/android/view/k$a;->i:Z

    new-instance v2, Lcom/chase/sig/android/activity/fa;

    invoke-direct {v2, p0}, Lcom/chase/sig/android/activity/fa;-><init>(Lcom/chase/sig/android/activity/LoginActivity;)V

    invoke-virtual {v1, v3, v2}, Lcom/chase/sig/android/view/k$a;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    .line 330
    invoke-virtual {v0, v6}, Lcom/chase/sig/android/view/k$a;->d(I)Lcom/chase/sig/android/view/k;

    move-result-object v0

    goto :goto_0

    .line 333
    :sswitch_2
    new-instance v0, Lcom/chase/sig/android/view/k$a;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/view/k$a;-><init>(Landroid/content/Context;)V

    .line 334
    const v1, 0x7f0700c9

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/k$a;->b(I)Lcom/chase/sig/android/view/k$a;

    move-result-object v1

    iput-boolean v5, v1, Lcom/chase/sig/android/view/k$a;->i:Z

    new-instance v2, Lcom/chase/sig/android/activity/fb;

    invoke-direct {v2, p0}, Lcom/chase/sig/android/activity/fb;-><init>(Lcom/chase/sig/android/activity/LoginActivity;)V

    invoke-virtual {v1, v3, v2}, Lcom/chase/sig/android/view/k$a;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    .line 347
    invoke-virtual {v0, v6}, Lcom/chase/sig/android/view/k$a;->d(I)Lcom/chase/sig/android/view/k;

    move-result-object v0

    goto :goto_0

    .line 350
    :sswitch_3
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/LoginActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 351
    const/4 v1, 0x0

    .line 353
    :try_start_0
    const-string v2, "com.jpm.sig.android"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    move-object v1, v0

    .line 369
    :goto_1
    const v0, 0x7f0702dd

    .line 370
    if-eqz v1, :cond_0

    .line 371
    const v0, 0x7f0702de

    .line 374
    :cond_0
    new-instance v2, Lcom/chase/sig/android/view/k$a;

    invoke-direct {v2, p0}, Lcom/chase/sig/android/view/k$a;-><init>(Landroid/content/Context;)V

    .line 375
    const-string v3, ""

    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    .line 376
    const v3, 0x7f0702dc

    invoke-virtual {v2, v3}, Lcom/chase/sig/android/view/k$a;->b(I)Lcom/chase/sig/android/view/k$a;

    move-result-object v3

    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/chase/sig/android/view/k$a;->i:Z

    const v4, 0x7f0702db

    invoke-virtual {p0, v4}, Lcom/chase/sig/android/activity/LoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    iput-object v4, v3, Lcom/chase/sig/android/view/k$a;->b:Ljava/lang/CharSequence;

    iput v5, v3, Lcom/chase/sig/android/view/k$a;->e:I

    new-instance v4, Lcom/chase/sig/android/activity/fe;

    invoke-direct {v4, p0, v1}, Lcom/chase/sig/android/activity/fe;-><init>(Lcom/chase/sig/android/activity/LoginActivity;Landroid/content/pm/PackageInfo;)V

    invoke-virtual {v3, v0, v4}, Lcom/chase/sig/android/view/k$a;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    move-result-object v0

    const v1, 0x7f0702e0

    new-instance v3, Lcom/chase/sig/android/activity/fd;

    invoke-direct {v3, p0}, Lcom/chase/sig/android/activity/fd;-><init>(Lcom/chase/sig/android/activity/LoginActivity;)V

    invoke-virtual {v0, v1, v3}, Lcom/chase/sig/android/view/k$a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    move-result-object v0

    const v1, 0x7f0702df

    new-instance v3, Lcom/chase/sig/android/activity/fc;

    invoke-direct {v3, p0}, Lcom/chase/sig/android/activity/fc;-><init>(Lcom/chase/sig/android/activity/LoginActivity;)V

    invoke-virtual {v0, v1, v3}, Lcom/chase/sig/android/view/k$a;->c(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    .line 436
    invoke-virtual {v2, v6}, Lcom/chase/sig/android/view/k$a;->d(I)Lcom/chase/sig/android/view/k;

    move-result-object v0

    goto/16 :goto_0

    .line 354
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->getMessage()Ljava/lang/String;

    goto :goto_1

    .line 316
    :sswitch_data_0
    .sparse-switch
        -0xa -> :sswitch_0
        0x0 -> :sswitch_1
        0x1 -> :sswitch_2
        0x2 -> :sswitch_3
    .end sparse-switch
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 221
    invoke-super {p0}, Lcom/chase/sig/android/activity/eb;->onResume()V

    .line 223
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/ChaseApplication;

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->q()V

    .line 225
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/LoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "SESSION_TIMED_OUT"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/LoginActivity;->showDialog(I)V

    .line 226
    :cond_0
    return-void
.end method
