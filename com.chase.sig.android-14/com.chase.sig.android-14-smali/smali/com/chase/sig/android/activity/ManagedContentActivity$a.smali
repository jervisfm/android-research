.class final Lcom/chase/sig/android/activity/ManagedContentActivity$a;
.super Landroid/webkit/WebViewClient;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/ManagedContentActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/ManagedContentActivity;


# direct methods
.method private constructor <init>(Lcom/chase/sig/android/activity/ManagedContentActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 149
    iput-object p1, p0, Lcom/chase/sig/android/activity/ManagedContentActivity$a;->a:Lcom/chase/sig/android/activity/ManagedContentActivity;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/chase/sig/android/activity/ManagedContentActivity;B)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 149
    invoke-direct {p0, p1}, Lcom/chase/sig/android/activity/ManagedContentActivity$a;-><init>(Lcom/chase/sig/android/activity/ManagedContentActivity;)V

    return-void
.end method


# virtual methods
.method public final onLoadResource(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 4
    .parameter
    .parameter

    .prologue
    .line 168
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->onLoadResource(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 170
    iget-object v0, p0, Lcom/chase/sig/android/activity/ManagedContentActivity$a;->a:Lcom/chase/sig/android/activity/ManagedContentActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/ManagedContentActivity;->b(Lcom/chase/sig/android/activity/ManagedContentActivity;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "/Public/Home/Timeout"

    invoke-virtual {p2, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 171
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/CookieManager;->removeSessionCookie()V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ManagedContentActivity$a;->a:Lcom/chase/sig/android/activity/ManagedContentActivity;

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/chase/sig/android/activity/LoginActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v2, 0x400

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v2, "SESSION_TIMED_OUT"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/eb;->startActivity(Landroid/content/Intent;)V

    .line 175
    :cond_0
    :goto_0
    return-void

    .line 172
    :cond_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/ManagedContentActivity$a;->a:Lcom/chase/sig/android/activity/ManagedContentActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/ManagedContentActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 173
    iget-object v0, p0, Lcom/chase/sig/android/activity/ManagedContentActivity$a;->a:Lcom/chase/sig/android/activity/ManagedContentActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/ManagedContentActivity;->t()V

    goto :goto_0
.end method

.method public final onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 193
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 194
    iget-object v0, p0, Lcom/chase/sig/android/activity/ManagedContentActivity$a;->a:Lcom/chase/sig/android/activity/ManagedContentActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/ManagedContentActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 195
    iget-object v0, p0, Lcom/chase/sig/android/activity/ManagedContentActivity$a;->a:Lcom/chase/sig/android/activity/ManagedContentActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/ManagedContentActivity;->u()V

    .line 197
    :cond_0
    return-void
.end method

.method public final onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 3
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 157
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p4, v0, v1

    const/4 v1, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    aput-object p3, v0, v1

    .line 160
    iget-object v0, p0, Lcom/chase/sig/android/activity/ManagedContentActivity$a;->a:Lcom/chase/sig/android/activity/ManagedContentActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/ManagedContentActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 161
    iget-object v0, p0, Lcom/chase/sig/android/activity/ManagedContentActivity$a;->a:Lcom/chase/sig/android/activity/ManagedContentActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/ManagedContentActivity;->a(Lcom/chase/sig/android/activity/ManagedContentActivity;)Landroid/webkit/WebView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 162
    iget-object v0, p0, Lcom/chase/sig/android/activity/ManagedContentActivity$a;->a:Lcom/chase/sig/android/activity/ManagedContentActivity;

    const v1, 0x7f07027c

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/ManagedContentActivity;->f(I)V

    .line 164
    :cond_0
    return-void
.end method

.method public final shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 7
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 202
    const-string v2, "tel:"

    invoke-virtual {p2, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 203
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.DIAL"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 204
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 205
    iget-object v2, p0, Lcom/chase/sig/android/activity/ManagedContentActivity$a;->a:Lcom/chase/sig/android/activity/ManagedContentActivity;

    invoke-virtual {v2, v1}, Lcom/chase/sig/android/activity/ManagedContentActivity;->a(Landroid/content/Intent;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 206
    iget-object v2, p0, Lcom/chase/sig/android/activity/ManagedContentActivity$a;->a:Lcom/chase/sig/android/activity/ManagedContentActivity;

    invoke-virtual {v2, v1}, Lcom/chase/sig/android/activity/ManagedContentActivity;->startActivity(Landroid/content/Intent;)V

    .line 260
    :cond_0
    :goto_0
    return v0

    .line 213
    :cond_1
    const-string v2, "/Public/Home/LogOff"

    invoke-virtual {p2, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 214
    iget-object v2, p0, Lcom/chase/sig/android/activity/ManagedContentActivity$a;->a:Lcom/chase/sig/android/activity/ManagedContentActivity;

    invoke-virtual {v2, v1}, Lcom/chase/sig/android/activity/ManagedContentActivity;->d(Z)V

    goto :goto_0

    .line 218
    :cond_2
    const-string v2, "/Public/FindUs"

    invoke-virtual {p2, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 220
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/chase/sig/android/activity/ManagedContentActivity$a;->a:Lcom/chase/sig/android/activity/ManagedContentActivity;

    invoke-virtual {v2}, Lcom/chase/sig/android/activity/ManagedContentActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/chase/sig/android/activity/FindBranchActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 221
    iget-object v2, p0, Lcom/chase/sig/android/activity/ManagedContentActivity$a;->a:Lcom/chase/sig/android/activity/ManagedContentActivity;

    invoke-virtual {v2, v1}, Lcom/chase/sig/android/activity/ManagedContentActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 226
    :cond_3
    const-string v2, "/Public/Docs/Faqs"

    invoke-virtual {p2, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 228
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/chase/sig/android/activity/ManagedContentActivity$a;->a:Lcom/chase/sig/android/activity/ManagedContentActivity;

    invoke-virtual {v2}, Lcom/chase/sig/android/activity/ManagedContentActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/chase/sig/android/activity/ManagedContentActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 229
    const-string v2, "webUrl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/chase/sig/android/activity/ManagedContentActivity$a;->a:Lcom/chase/sig/android/activity/ManagedContentActivity;

    invoke-virtual {v4}, Lcom/chase/sig/android/activity/ManagedContentActivity;->z()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "environment."

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/chase/sig/android/activity/ManagedContentActivity$a;->a:Lcom/chase/sig/android/activity/ManagedContentActivity;

    invoke-virtual {v6}, Lcom/chase/sig/android/activity/ManagedContentActivity;->z()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v6

    invoke-virtual {v6}, Lcom/chase/sig/android/ChaseApplication;->f()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ".content"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/chase/sig/android/ChaseApplication;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/chase/sig/android/activity/ManagedContentActivity$a;->a:Lcom/chase/sig/android/activity/ManagedContentActivity;

    invoke-virtual {v4}, Lcom/chase/sig/android/activity/ManagedContentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f070065

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 234
    const-string v2, "viewTitle"

    const v3, 0x7f0701ba

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 235
    iget-object v2, p0, Lcom/chase/sig/android/activity/ManagedContentActivity$a;->a:Lcom/chase/sig/android/activity/ManagedContentActivity;

    invoke-virtual {v2, v1}, Lcom/chase/sig/android/activity/ManagedContentActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 239
    :cond_4
    const-string v2, "/Public/Home/LogOn"

    invoke-virtual {p2, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 241
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/chase/sig/android/activity/ManagedContentActivity$a;->a:Lcom/chase/sig/android/activity/ManagedContentActivity;

    invoke-virtual {v2}, Lcom/chase/sig/android/activity/ManagedContentActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/chase/sig/android/activity/HomeActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 242
    iget-object v2, p0, Lcom/chase/sig/android/activity/ManagedContentActivity$a;->a:Lcom/chase/sig/android/activity/ManagedContentActivity;

    invoke-virtual {v2, v1}, Lcom/chase/sig/android/activity/ManagedContentActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 246
    :cond_5
    const-string v2, "/Public/Docs/PrivacyPolicy?json=1"

    invoke-virtual {p2, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 248
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/chase/sig/android/activity/ManagedContentActivity$a;->a:Lcom/chase/sig/android/activity/ManagedContentActivity;

    invoke-virtual {v2}, Lcom/chase/sig/android/activity/ManagedContentActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/chase/sig/android/activity/ManagedContentActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 249
    const-string v2, "webUrl"

    iget-object v3, p0, Lcom/chase/sig/android/activity/ManagedContentActivity$a;->a:Lcom/chase/sig/android/activity/ManagedContentActivity;

    invoke-virtual {v3}, Lcom/chase/sig/android/activity/ManagedContentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f07005d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 251
    const-string v2, "viewTitle"

    const v3, 0x7f07029e

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 252
    iget-object v2, p0, Lcom/chase/sig/android/activity/ManagedContentActivity$a;->a:Lcom/chase/sig/android/activity/ManagedContentActivity;

    invoke-virtual {v2, v1}, Lcom/chase/sig/android/activity/ManagedContentActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 256
    :cond_6
    const-string v0, "/Public/Docs/ContactUs"

    invoke-virtual {p2, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    move v0, v1

    .line 257
    goto/16 :goto_0

    :cond_7
    move v0, v1

    .line 260
    goto/16 :goto_0
.end method
