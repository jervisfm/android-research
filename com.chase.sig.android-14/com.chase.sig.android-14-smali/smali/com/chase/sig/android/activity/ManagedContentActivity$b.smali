.class public Lcom/chase/sig/android/activity/ManagedContentActivity$b;
.super Lcom/chase/sig/android/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/ManagedContentActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/d",
        "<",
        "Lcom/chase/sig/android/activity/ManagedContentActivity;",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/service/content/ContentResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 264
    invoke-direct {p0}, Lcom/chase/sig/android/d;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 264
    check-cast p1, [Ljava/lang/String;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/ManagedContentActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/ManagedContentActivity;->A()Lcom/chase/sig/android/service/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/service/n;->p()Lcom/chase/sig/android/service/content/a;

    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-static {v0}, Lcom/chase/sig/android/service/content/a;->a(Ljava/lang/String;)Lcom/chase/sig/android/service/content/ContentResponse;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 6
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 264
    check-cast p1, Lcom/chase/sig/android/service/content/ContentResponse;

    invoke-super {p0, p1}, Lcom/chase/sig/android/d;->a(Ljava/lang/Object;)V

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/chase/sig/android/service/content/ContentResponse;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/ManagedContentActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/content/ContentResponse;->g()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/activity/ManagedContentActivity;->b(Ljava/util/List;)V

    :cond_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/ManagedContentActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/ManagedContentActivity;->a(Lcom/chase/sig/android/activity/ManagedContentActivity;)Landroid/webkit/WebView;

    move-result-object v0

    invoke-virtual {p1}, Lcom/chase/sig/android/service/content/ContentResponse;->a()Ljava/lang/String;

    move-result-object v2

    const-string v3, "text/html"

    const-string v4, "utf-8"

    move-object v5, v1

    invoke-virtual/range {v0 .. v5}, Landroid/webkit/WebView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
