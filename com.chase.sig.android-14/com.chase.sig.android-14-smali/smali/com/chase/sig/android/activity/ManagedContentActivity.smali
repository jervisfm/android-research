.class public Lcom/chase/sig/android/activity/ManagedContentActivity;
.super Lcom/chase/sig/android/activity/eb;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/ManagedContentActivity$b;,
        Lcom/chase/sig/android/activity/ManagedContentActivity$a;
    }
.end annotation


# instance fields
.field private a:Landroid/webkit/WebView;

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/chase/sig/android/activity/eb;-><init>()V

    .line 264
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/ManagedContentActivity;)Landroid/webkit/WebView;
    .locals 1
    .parameter

    .prologue
    .line 29
    iget-object v0, p0, Lcom/chase/sig/android/activity/ManagedContentActivity;->a:Landroid/webkit/WebView;

    return-object v0
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/ManagedContentActivity;)Z
    .locals 1
    .parameter

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ManagedContentActivity;->d()Z

    move-result v0

    return v0
.end method

.method private d()Z
    .locals 2

    .prologue
    .line 146
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/ManagedContentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/ManagedContentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "subuser"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 8
    .parameter

    .prologue
    const v7, 0x7f09014d

    const/4 v2, -0x1

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 43
    const v0, 0x7f03005c

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ManagedContentActivity;->b(I)V

    .line 44
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/ManagedContentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    .line 47
    const-string v0, "viewTitle"

    invoke-virtual {v5, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 48
    if-eq v0, v2, :cond_2

    .line 49
    const-string v0, "viewTitle"

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ManagedContentActivity;->setTitle(I)V

    .line 54
    :goto_0
    const-string v0, "content"

    invoke-static {v5, v0}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 55
    const-string v0, "content"

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/ManagedContentActivity;->b:Ljava/lang/String;

    move-object v2, v1

    .line 62
    :goto_1
    invoke-virtual {p0, v7}, Lcom/chase/sig/android/activity/ManagedContentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ManagedContentActivity;->a:Landroid/webkit/WebView;

    .line 63
    iget-object v0, p0, Lcom/chase/sig/android/activity/ManagedContentActivity;->a:Landroid/webkit/WebView;

    invoke-virtual {v0, p1}, Landroid/webkit/WebView;->restoreState(Landroid/os/Bundle;)Landroid/webkit/WebBackForwardList;

    .line 64
    iget-object v0, p0, Lcom/chase/sig/android/activity/ManagedContentActivity;->a:Landroid/webkit/WebView;

    new-instance v6, Lcom/chase/sig/android/activity/ManagedContentActivity$a;

    invoke-direct {v6, p0, v4}, Lcom/chase/sig/android/activity/ManagedContentActivity$a;-><init>(Lcom/chase/sig/android/activity/ManagedContentActivity;B)V

    invoke-virtual {v0, v6}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 65
    iget-object v0, p0, Lcom/chase/sig/android/activity/ManagedContentActivity;->a:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 67
    const-string v0, "userAgent"

    invoke-static {v5, v0}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 68
    iget-object v0, p0, Lcom/chase/sig/android/activity/ManagedContentActivity;->a:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v6

    const-string v0, "userAgent"

    invoke-static {v5, v0, v1}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v6, v0}, Landroid/webkit/WebSettings;->setUserAgentString(Ljava/lang/String;)V

    .line 71
    :cond_0
    invoke-virtual {p0, v7}, Lcom/chase/sig/android/activity/ManagedContentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ManagedContentActivity;->a:Landroid/webkit/WebView;

    .line 72
    iget-object v0, p0, Lcom/chase/sig/android/activity/ManagedContentActivity;->a:Landroid/webkit/WebView;

    invoke-virtual {v0, p1}, Landroid/webkit/WebView;->restoreState(Landroid/os/Bundle;)Landroid/webkit/WebBackForwardList;

    .line 73
    iget-object v0, p0, Lcom/chase/sig/android/activity/ManagedContentActivity;->a:Landroid/webkit/WebView;

    new-instance v6, Lcom/chase/sig/android/activity/ManagedContentActivity$a;

    invoke-direct {v6, p0, v4}, Lcom/chase/sig/android/activity/ManagedContentActivity$a;-><init>(Lcom/chase/sig/android/activity/ManagedContentActivity;B)V

    invoke-virtual {v0, v6}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 74
    iget-object v0, p0, Lcom/chase/sig/android/activity/ManagedContentActivity;->a:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 76
    const-string v0, "userAgent"

    invoke-static {v5, v0}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 77
    iget-object v0, p0, Lcom/chase/sig/android/activity/ManagedContentActivity;->a:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v6

    const-string v0, "userAgent"

    invoke-static {v5, v0, v1}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v6, v0}, Landroid/webkit/WebSettings;->setUserAgentString(Ljava/lang/String;)V

    .line 80
    :cond_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/ManagedContentActivity;->a:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->canGoBack()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v3

    :goto_2
    if-eqz v0, :cond_7

    .line 81
    invoke-static {v2}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 82
    invoke-static {p0}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v1

    new-instance v5, Lcom/chase/sig/android/util/f;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/ChaseApplication;

    invoke-direct {v5, v0}, Lcom/chase/sig/android/util/f;-><init>(Lcom/chase/sig/android/ChaseApplication;)V

    invoke-static {}, Lcom/chase/sig/android/util/f;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/cookie/Cookie;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v0}, Lorg/apache/http/cookie/Cookie;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {v0}, Lorg/apache/http/cookie/Cookie;->getValue()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "; domain="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {v0}, Lorg/apache/http/cookie/Cookie;->getDomain()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "; expires="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {v0}, Lorg/apache/http/cookie/Cookie;->getExpiryDate()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v2, v6, v4

    aput-object v0, v6, v3

    invoke-virtual {v1, v2, v0}, Landroid/webkit/CookieManager;->setCookie(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 51
    :cond_2
    const v0, 0x7f09002e

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ManagedContentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 56
    :cond_3
    const-string v0, "webUrl"

    invoke-static {v5, v0}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 57
    const-string v0, "webUrl"

    invoke-static {v5, v0, v1}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v2, v0

    goto/16 :goto_1

    .line 59
    :cond_4
    const-class v2, Lcom/chase/sig/android/activity/ManagedContentActivity$b;

    new-array v6, v3, [Ljava/lang/String;

    const-string v0, "documentId"

    invoke-static {v5, v0, v1}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v6, v4

    invoke-virtual {p0, v2, v6}, Lcom/chase/sig/android/activity/ManagedContentActivity;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    move-object v2, v1

    goto/16 :goto_1

    :cond_5
    move v0, v4

    .line 80
    goto/16 :goto_2

    .line 82
    :cond_6
    iget-object v0, p0, Lcom/chase/sig/android/activity/ManagedContentActivity;->a:Landroid/webkit/WebView;

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 87
    :cond_7
    :goto_4
    return-void

    .line 83
    :cond_8
    iget-object v0, p0, Lcom/chase/sig/android/activity/ManagedContentActivity;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 84
    iget-object v0, p0, Lcom/chase/sig/android/activity/ManagedContentActivity;->a:Landroid/webkit/WebView;

    iget-object v2, p0, Lcom/chase/sig/android/activity/ManagedContentActivity;->b:Ljava/lang/String;

    const-string v3, "text/html"

    const-string v4, "utf-8"

    move-object v5, v1

    invoke-virtual/range {v0 .. v5}, Landroid/webkit/WebView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 128
    invoke-virtual {p0, p1}, Lcom/chase/sig/android/activity/ManagedContentActivity;->j(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 142
    :goto_0
    return v0

    .line 132
    :cond_0
    const/4 v1, 0x4

    if-ne p1, v1, :cond_2

    .line 133
    iget-object v1, p0, Lcom/chase/sig/android/activity/ManagedContentActivity;->a:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->canGoBack()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 134
    iget-object v1, p0, Lcom/chase/sig/android/activity/ManagedContentActivity;->a:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->goBack()V

    goto :goto_0

    .line 137
    :cond_1
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ManagedContentActivity;->d()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 138
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/ManagedContentActivity;->d(Z)V

    goto :goto_0

    .line 142
    :cond_2
    invoke-super {p0, p1, p2}, Lcom/chase/sig/android/activity/eb;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .parameter

    .prologue
    .line 99
    iget-object v0, p0, Lcom/chase/sig/android/activity/ManagedContentActivity;->a:Landroid/webkit/WebView;

    invoke-virtual {v0, p1}, Landroid/webkit/WebView;->saveState(Landroid/os/Bundle;)Landroid/webkit/WebBackForwardList;

    .line 100
    return-void
.end method
