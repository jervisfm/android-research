.class public Lcom/chase/sig/android/activity/MarketsActivity$b;
.super Lcom/chase/sig/android/b;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/MarketsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/b",
        "<",
        "Lcom/chase/sig/android/activity/MarketsActivity;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/domain/n;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 268
    invoke-direct {p0}, Lcom/chase/sig/android/b;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .parameter

    .prologue
    .line 268
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/MarketsActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/MarketsActivity;->f()I

    move-result v0

    invoke-static {v0}, Lcom/chase/sig/android/activity/MarketsActivity;->k(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/MarketsActivity$b;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/MarketsActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/MarketsActivity;->A()Lcom/chase/sig/android/service/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/service/n;->m()Lcom/chase/sig/android/service/aa;

    iget-object v0, p0, Lcom/chase/sig/android/activity/MarketsActivity$b;->a:Ljava/lang/String;

    const-string v1, "1"

    invoke-static {v0, v1}, Lcom/chase/sig/android/service/aa;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/domain/QuoteResponse;

    move-result-object v1

    new-instance v0, Lcom/chase/sig/android/domain/ImageDownloadResponse;

    invoke-direct {v0}, Lcom/chase/sig/android/domain/ImageDownloadResponse;-><init>()V

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/QuoteResponse;->e()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/chase/sig/android/activity/MarketsActivity$b;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/domain/QuoteResponse;->a(Ljava/lang/String;)Lcom/chase/sig/android/domain/Quote;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/Quote;->q()Lcom/chase/sig/android/domain/Chart;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Chart;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->D(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/MarketsActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/MarketsActivity;->A()Lcom/chase/sig/android/service/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/service/n;->n()Lcom/chase/sig/android/service/j;

    const/4 v0, 0x1

    invoke-static {}, Lcom/chase/sig/android/service/JPService;->b()Ljava/util/HashMap;

    move-result-object v3

    invoke-static {v2, v0, v3}, Lcom/chase/sig/android/service/j;->a(Ljava/lang/String;ZLjava/util/HashMap;)Lcom/chase/sig/android/domain/ImageDownloadResponse;

    move-result-object v0

    :cond_0
    new-instance v2, Lcom/chase/sig/android/domain/n;

    invoke-direct {v2, v1, v0}, Lcom/chase/sig/android/domain/n;-><init>(Lcom/chase/sig/android/domain/QuoteResponse;Lcom/chase/sig/android/domain/ImageDownloadResponse;)V

    return-object v2
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 3
    .parameter

    .prologue
    .line 268
    check-cast p1, Lcom/chase/sig/android/domain/n;

    iget-object v1, p1, Lcom/chase/sig/android/domain/n;->a:Lcom/chase/sig/android/domain/QuoteResponse;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/QuoteResponse;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/MarketsActivity;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/QuoteResponse;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/MarketsActivity;->b(Ljava/util/List;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/MarketsActivity$b;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/QuoteResponse;->a(Ljava/lang/String;)Lcom/chase/sig/android/domain/Quote;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/MarketsActivity;

    invoke-static {v0, v1}, Lcom/chase/sig/android/activity/MarketsActivity;->b(Lcom/chase/sig/android/activity/MarketsActivity;Lcom/chase/sig/android/domain/Quote;)V

    if-nez v1, :cond_1

    sget-object v0, Lcom/chase/sig/android/domain/Chart;->a:Lcom/chase/sig/android/domain/Chart;

    move-object v1, v0

    :goto_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/MarketsActivity;

    iget-object v2, p1, Lcom/chase/sig/android/domain/n;->b:Lcom/chase/sig/android/domain/ImageDownloadResponse;

    invoke-virtual {v0, v2, v1}, Lcom/chase/sig/android/activity/MarketsActivity;->a(Lcom/chase/sig/android/domain/ImageDownloadResponse;Lcom/chase/sig/android/domain/Chart;)Z

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/MarketsActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/MarketsActivity;->a(Lcom/chase/sig/android/activity/MarketsActivity;)Lcom/chase/sig/android/view/JPTabWidget;

    move-result-object v1

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/MarketsActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/MarketsActivity;->f()I

    move-result v0

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lcom/chase/sig/android/view/JPTabWidget;->a(IZ)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/MarketsActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/MarketsActivity;->e()V

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Lcom/chase/sig/android/domain/Quote;->q()Lcom/chase/sig/android/domain/Chart;

    move-result-object v0

    move-object v1, v0

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/MarketsActivity;

    const v1, 0x7f07027c

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/MarketsActivity;->f(I)V

    goto :goto_0
.end method
