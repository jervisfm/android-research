.class public Lcom/chase/sig/android/activity/MarketsActivity;
.super Lcom/chase/sig/android/activity/b;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$c;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/MarketsActivity$b;,
        Lcom/chase/sig/android/activity/MarketsActivity$a;
    }
.end annotation


# instance fields
.field private b:Lcom/chase/sig/android/view/detail/DetailView;

.field private c:Lcom/chase/sig/android/service/QuoteNewsResponse;

.field private d:Lcom/chase/sig/android/activity/ag;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/chase/sig/android/activity/ag",
            "<",
            "Lcom/chase/sig/android/activity/MarketsActivity;",
            ">;"
        }
    .end annotation
.end field

.field private k:Lcom/chase/sig/android/view/JPTabWidget;

.field private l:Lcom/chase/sig/android/view/TickerValueTextView;

.field private m:Lcom/chase/sig/android/domain/QuoteResponse;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 52
    const v0, 0x7f020076

    const v1, 0x7f090151

    invoke-direct {p0, v0, v1}, Lcom/chase/sig/android/activity/b;-><init>(II)V

    .line 49
    new-instance v0, Lcom/chase/sig/android/domain/QuoteResponse;

    invoke-direct {v0}, Lcom/chase/sig/android/domain/QuoteResponse;-><init>()V

    iput-object v0, p0, Lcom/chase/sig/android/activity/MarketsActivity;->m:Lcom/chase/sig/android/domain/QuoteResponse;

    .line 53
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/MarketsActivity;I)Lcom/chase/sig/android/domain/Quote;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 25
    invoke-static {p1}, Lcom/chase/sig/android/activity/MarketsActivity;->l(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/chase/sig/android/activity/MarketsActivity;->m:Lcom/chase/sig/android/domain/QuoteResponse;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/QuoteResponse;->a(Ljava/lang/String;)Lcom/chase/sig/android/domain/Quote;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/MarketsActivity;)Lcom/chase/sig/android/view/JPTabWidget;
    .locals 1
    .parameter

    .prologue
    .line 25
    iget-object v0, p0, Lcom/chase/sig/android/activity/MarketsActivity;->k:Lcom/chase/sig/android/view/JPTabWidget;

    return-object v0
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/MarketsActivity;Lcom/chase/sig/android/domain/Quote;)V
    .locals 5
    .parameter
    .parameter

    .prologue
    const v4, 0x7f09014f

    .line 25
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/MarketsActivity;->l:Lcom/chase/sig/android/view/TickerValueTextView;

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/Quote;->i()Lcom/chase/sig/android/util/Dollar;

    move-result-object v1

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/Quote;->g()Lcom/chase/sig/android/util/Dollar;

    move-result-object v2

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/Quote;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/chase/sig/android/view/TickerValueTextView;->a(Lcom/chase/sig/android/util/Dollar;Lcom/chase/sig/android/util/Dollar;Ljava/lang/String;)V

    invoke-virtual {p0, v4}, Lcom/chase/sig/android/activity/MarketsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, v4}, Lcom/chase/sig/android/activity/MarketsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/MarketsActivity;Lcom/chase/sig/android/service/QuoteNewsResponse;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 25
    iput-object p1, p0, Lcom/chase/sig/android/activity/MarketsActivity;->c:Lcom/chase/sig/android/service/QuoteNewsResponse;

    iget-object v0, p0, Lcom/chase/sig/android/activity/MarketsActivity;->d:Lcom/chase/sig/android/activity/ag;

    iget-object v1, p0, Lcom/chase/sig/android/activity/MarketsActivity;->b:Lcom/chase/sig/android/view/detail/DetailView;

    invoke-virtual {v0, p0, v1, p1}, Lcom/chase/sig/android/activity/ag;->a(Lcom/chase/sig/android/activity/eb;Lcom/chase/sig/android/view/detail/DetailView;Lcom/chase/sig/android/service/QuoteNewsResponse;)V

    return-void
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/MarketsActivity;I)Ljava/lang/String;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 25
    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    const v0, 0x7f0702be

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/MarketsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    const v0, 0x7f0702bb

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/MarketsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    const v0, 0x7f0702bc

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/MarketsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    const v0, 0x7f0702bd

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/MarketsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_4
    const v0, 0x7f0702ba

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/MarketsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/MarketsActivity;)V
    .locals 2
    .parameter

    .prologue
    .line 25
    const v0, 0x7f0900db

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/MarketsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/MarketsActivity;Lcom/chase/sig/android/domain/Quote;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 25
    iget-object v0, p0, Lcom/chase/sig/android/activity/MarketsActivity;->m:Lcom/chase/sig/android/domain/QuoteResponse;

    invoke-virtual {v0, p1}, Lcom/chase/sig/android/domain/QuoteResponse;->a(Lcom/chase/sig/android/domain/Quote;)V

    return-void
.end method

.method static synthetic k(I)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 25
    invoke-static {p0}, Lcom/chase/sig/android/activity/MarketsActivity;->l(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static l(I)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 182
    packed-switch p0, :pswitch_data_0

    .line 195
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 184
    :pswitch_0
    const-string v0, "$XAX"

    goto :goto_0

    .line 186
    :pswitch_1
    const-string v0, "$INDU"

    goto :goto_0

    .line 188
    :pswitch_2
    const-string v0, "$COMPQ"

    goto :goto_0

    .line 190
    :pswitch_3
    const-string v0, "$NYA"

    goto :goto_0

    .line 192
    :pswitch_4
    const-string v0, "$SPX"

    goto :goto_0

    .line 182
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 6
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 57
    const v0, 0x7f07029f

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/MarketsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/eb;->f:Ljava/lang/String;

    .line 58
    const v0, 0x7f03005d

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/MarketsActivity;->b(I)V

    .line 60
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/MarketsActivity;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->b:Lcom/chase/sig/android/domain/g;

    invoke-interface {v0}, Lcom/chase/sig/android/domain/g;->z()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61
    const/high16 v0, 0x7f09

    const v1, 0x7f020041

    const v2, 0x7f020040

    new-instance v3, Lcom/chase/sig/android/activity/fj;

    invoke-direct {v3, p0}, Lcom/chase/sig/android/activity/fj;-><init>(Lcom/chase/sig/android/activity/MarketsActivity;)V

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/chase/sig/android/activity/MarketsActivity;->a(IIILandroid/view/View$OnClickListener;)V

    .line 65
    :cond_0
    const v0, 0x7f090001

    const v1, 0x7f020045

    const v2, 0x7f020044

    new-instance v3, Lcom/chase/sig/android/activity/ff;

    invoke-direct {v3, p0}, Lcom/chase/sig/android/activity/ff;-><init>(Lcom/chase/sig/android/activity/MarketsActivity;)V

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/chase/sig/android/activity/MarketsActivity;->a(IIILandroid/view/View$OnClickListener;)V

    .line 75
    const v0, 0x7f090002

    const v1, 0x7f020043

    const v2, 0x7f020042

    new-instance v3, Lcom/chase/sig/android/activity/fg;

    invoke-direct {v3, p0}, Lcom/chase/sig/android/activity/fg;-><init>(Lcom/chase/sig/android/activity/MarketsActivity;)V

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/chase/sig/android/activity/MarketsActivity;->a(IIILandroid/view/View$OnClickListener;)V

    .line 83
    const v0, 0x7f090150

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/MarketsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/TickerValueTextView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/MarketsActivity;->l:Lcom/chase/sig/android/view/TickerValueTextView;

    .line 85
    const v0, 0x7f09014e

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/MarketsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/JPTabWidget;

    iput-object v0, p0, Lcom/chase/sig/android/activity/MarketsActivity;->k:Lcom/chase/sig/android/view/JPTabWidget;

    .line 86
    const v0, 0x7f090115

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/MarketsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/MarketsActivity;->b:Lcom/chase/sig/android/view/detail/DetailView;

    .line 88
    iget-object v0, p0, Lcom/chase/sig/android/activity/MarketsActivity;->d:Lcom/chase/sig/android/activity/ag;

    if-nez v0, :cond_1

    .line 89
    new-instance v0, Lcom/chase/sig/android/activity/ag;

    invoke-direct {v0}, Lcom/chase/sig/android/activity/ag;-><init>()V

    iput-object v0, p0, Lcom/chase/sig/android/activity/MarketsActivity;->d:Lcom/chase/sig/android/activity/ag;

    .line 92
    :cond_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/MarketsActivity;->k:Lcom/chase/sig/android/view/JPTabWidget;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/JPTabWidget;->a()V

    iget-object v0, p0, Lcom/chase/sig/android/activity/MarketsActivity;->k:Lcom/chase/sig/android/view/JPTabWidget;

    const v1, 0x7f0702ba

    invoke-virtual {v0, v5, v1}, Lcom/chase/sig/android/view/JPTabWidget;->a(II)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/MarketsActivity;->k:Lcom/chase/sig/android/view/JPTabWidget;

    const v1, 0x7f0702bb

    invoke-virtual {v0, v4, v1}, Lcom/chase/sig/android/view/JPTabWidget;->a(II)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/MarketsActivity;->k:Lcom/chase/sig/android/view/JPTabWidget;

    const/4 v1, 0x2

    const v2, 0x7f0702bc

    invoke-virtual {v0, v1, v2}, Lcom/chase/sig/android/view/JPTabWidget;->a(II)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/MarketsActivity;->k:Lcom/chase/sig/android/view/JPTabWidget;

    const/4 v1, 0x3

    const v2, 0x7f0702bd

    invoke-virtual {v0, v1, v2}, Lcom/chase/sig/android/view/JPTabWidget;->a(II)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/MarketsActivity;->k:Lcom/chase/sig/android/view/JPTabWidget;

    const/4 v1, 0x4

    const v2, 0x7f0702be

    invoke-virtual {v0, v1, v2}, Lcom/chase/sig/android/view/JPTabWidget;->a(II)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/MarketsActivity;->k:Lcom/chase/sig/android/view/JPTabWidget;

    new-instance v1, Lcom/chase/sig/android/activity/fh;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/fh;-><init>(Lcom/chase/sig/android/activity/MarketsActivity;)V

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/JPTabWidget;->setTabListener(Lcom/chase/sig/android/view/JPTabWidget$b;)V

    .line 94
    if-eqz p1, :cond_2

    .line 95
    iget-object v0, p0, Lcom/chase/sig/android/activity/MarketsActivity;->d:Lcom/chase/sig/android/activity/ag;

    invoke-static {p1}, Lcom/chase/sig/android/activity/ag;->a(Landroid/os/Bundle;)Lcom/chase/sig/android/service/QuoteNewsResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/MarketsActivity;->c:Lcom/chase/sig/android/service/QuoteNewsResponse;

    .line 96
    const-string v0, "indexes_resposne"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/QuoteResponse;

    iput-object v0, p0, Lcom/chase/sig/android/activity/MarketsActivity;->m:Lcom/chase/sig/android/domain/QuoteResponse;

    .line 97
    invoke-virtual {p0, p1}, Lcom/chase/sig/android/activity/MarketsActivity;->b(Landroid/os/Bundle;)V

    .line 98
    iget-object v0, p0, Lcom/chase/sig/android/activity/MarketsActivity;->k:Lcom/chase/sig/android/view/JPTabWidget;

    const-string v1, "tab_selected"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1, v4}, Lcom/chase/sig/android/view/JPTabWidget;->a(IZ)V

    .line 99
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/MarketsActivity;->e()V

    .line 105
    :goto_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/MarketsActivity;->d:Lcom/chase/sig/android/activity/ag;

    iget-object v2, p0, Lcom/chase/sig/android/activity/MarketsActivity;->c:Lcom/chase/sig/android/service/QuoteNewsResponse;

    iget-object v3, p0, Lcom/chase/sig/android/activity/MarketsActivity;->b:Lcom/chase/sig/android/view/detail/DetailView;

    const-class v4, Lcom/chase/sig/android/activity/MarketsActivity$a;

    new-array v5, v5, [Ljava/lang/String;

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lcom/chase/sig/android/activity/ag;->a(Lcom/chase/sig/android/activity/eb;Lcom/chase/sig/android/service/QuoteNewsResponse;Lcom/chase/sig/android/view/detail/DetailView;Ljava/lang/Class;[Ljava/lang/String;)V

    .line 106
    return-void

    .line 101
    :cond_2
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/MarketsActivity;->d()V

    .line 102
    const v0, 0x7f040004

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    const v1, 0x7f0900db

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/MarketsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method protected final c_()V
    .locals 0

    .prologue
    .line 324
    invoke-super {p0}, Lcom/chase/sig/android/activity/b;->c_()V

    .line 325
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/MarketsActivity;->finish()V

    .line 326
    return-void
.end method

.method protected final d()V
    .locals 2

    .prologue
    .line 127
    const-class v0, Lcom/chase/sig/android/activity/MarketsActivity$b;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/MarketsActivity;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 128
    return-void
.end method

.method protected final e()V
    .locals 2

    .prologue
    .line 208
    const v0, 0x7f090151

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/MarketsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 209
    new-instance v1, Lcom/chase/sig/android/activity/fi;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/fi;-><init>(Lcom/chase/sig/android/activity/MarketsActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 222
    return-void
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lcom/chase/sig/android/activity/MarketsActivity;->k:Lcom/chase/sig/android/view/JPTabWidget;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/JPTabWidget;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 226
    iget-object v0, p0, Lcom/chase/sig/android/activity/MarketsActivity;->k:Lcom/chase/sig/android/view/JPTabWidget;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/JPTabWidget;->getSelectedTabId()I

    move-result v0

    .line 228
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    const/16 v0, 0x23

    .line 120
    if-ne p1, v0, :cond_0

    if-ne p2, v0, :cond_0

    .line 122
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/MarketsActivity;->d()V

    .line 124
    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 110
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/b;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 112
    iget-object v0, p0, Lcom/chase/sig/android/activity/MarketsActivity;->d:Lcom/chase/sig/android/activity/ag;

    iget-object v0, p0, Lcom/chase/sig/android/activity/MarketsActivity;->c:Lcom/chase/sig/android/service/QuoteNewsResponse;

    const-string v1, "quote_news_response"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 113
    const-string v0, "tab_selected"

    iget-object v1, p0, Lcom/chase/sig/android/activity/MarketsActivity;->k:Lcom/chase/sig/android/view/JPTabWidget;

    invoke-virtual {v1}, Lcom/chase/sig/android/view/JPTabWidget;->getSelectedTabId()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 114
    const-string v0, "indexes_resposne"

    iget-object v1, p0, Lcom/chase/sig/android/activity/MarketsActivity;->m:Lcom/chase/sig/android/domain/QuoteResponse;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 115
    invoke-virtual {p0, p1}, Lcom/chase/sig/android/activity/MarketsActivity;->c(Landroid/os/Bundle;)V

    .line 116
    return-void
.end method
