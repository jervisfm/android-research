.class public Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity$a;
.super Lcom/chase/sig/android/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/d",
        "<",
        "Lcom/chase/sig/android/activity/eb;",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/domain/MobileAdResponse;",
        ">;"
    }
.end annotation


# instance fields
.field a:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 201
    invoke-direct {p0}, Lcom/chase/sig/android/d;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 201
    check-cast p1, [Ljava/lang/String;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/eb;->A()Lcom/chase/sig/android/service/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/service/n;->o()Lcom/chase/sig/android/service/s;

    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-static {v0}, Lcom/chase/sig/android/service/s;->c(Ljava/lang/String;)Lcom/chase/sig/android/domain/MobileAdResponse;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 3
    .parameter

    .prologue
    .line 201
    check-cast p1, Lcom/chase/sig/android/domain/MobileAdResponse;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/MobileAdResponse;->e()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    sput-boolean v1, Lcom/chase/sig/android/ChaseApplication;->a:Z

    iget-boolean v1, p0, Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity$a;->a:Z

    if-eqz v1, :cond_0

    invoke-static {v0, p1}, Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity;->b(Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity;Lcom/chase/sig/android/domain/MobileAdResponse;)V

    :goto_0
    return-void

    :cond_0
    invoke-static {v0}, Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity;->b(Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/MobileAdResponse;->g()Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/service/IServiceError;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity;->b(Lcom/chase/sig/android/service/IServiceError;)V

    goto :goto_0
.end method
