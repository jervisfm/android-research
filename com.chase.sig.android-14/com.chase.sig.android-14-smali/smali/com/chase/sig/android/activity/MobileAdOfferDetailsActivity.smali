.class public Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$d;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity$a;,
        Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity$b;
    }
.end annotation


# instance fields
.field private a:Lcom/chase/sig/android/domain/MobileAdResponse;

.field private b:Z

.field private c:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity;->b:Z

    .line 53
    new-instance v0, Lcom/chase/sig/android/activity/fl;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/activity/fl;-><init>(Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity;)V

    iput-object v0, p0, Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity;->c:Landroid/view/View$OnClickListener;

    .line 201
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity;)Lcom/chase/sig/android/domain/MobileAdResponse;
    .locals 1
    .parameter

    .prologue
    .line 26
    iget-object v0, p0, Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity;->a:Lcom/chase/sig/android/domain/MobileAdResponse;

    return-object v0
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity;Lcom/chase/sig/android/domain/MobileAdResponse;)V
    .locals 5
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 26
    iput-object p1, p0, Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity;->a:Lcom/chase/sig/android/domain/MobileAdResponse;

    new-instance v0, Lcom/chase/sig/android/service/JPService;

    invoke-direct {v0}, Lcom/chase/sig/android/service/JPService;-><init>()V

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/MobileAdResponse;->a()Lcom/chase/sig/android/domain/MobileAdResponseContent;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/MobileAdResponseContent;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/service/JPService;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const v0, 0x7f090022

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    new-instance v2, Lcom/chase/sig/android/activity/fm;

    invoke-direct {v2, p0}, Lcom/chase/sig/android/activity/fm;-><init>(Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity;)V

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    const v0, 0x7f090020

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const v1, 0x7f090021

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/MobileAdResponse;->a()Lcom/chase/sig/android/domain/MobileAdResponseContent;

    move-result-object v2

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/MobileAdResponseContent;->b()Lcom/chase/sig/android/domain/ResponseActions;

    move-result-object v2

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/ResponseActions;->a()Lcom/chase/sig/android/domain/ResponseActions$Action;

    move-result-object v3

    invoke-virtual {v3}, Lcom/chase/sig/android/domain/ResponseActions$Action;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/ResponseActions;->b()Lcom/chase/sig/android/domain/ResponseActions$Action;

    move-result-object v2

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/ResponseActions$Action;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setVisibility(I)V

    return-void
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity;)V
    .locals 1
    .parameter

    .prologue
    .line 26
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity;->finish()V

    return-void
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity;Lcom/chase/sig/android/domain/MobileAdResponse;)V
    .locals 5
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x1

    .line 26
    iput-boolean v4, p0, Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity;->b:Z

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/MobileAdResponse;->a()Lcom/chase/sig/android/domain/MobileAdResponseContent;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/MobileAdResponseContent;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->l(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "tel:"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "We can\'t complete this call from your device. Please call %s to apply. We\'re available 24 hours a day."

    new-array v2, v4, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, v4}, Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity;->b(Ljava/lang/String;Ljava/lang/String;Z)V

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 4
    .parameter

    .prologue
    .line 35
    const v0, 0x7f030009

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity;->b(I)V

    .line 37
    const v0, 0x7f090021

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 38
    const v1, 0x7f090020

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 39
    iget-object v2, p0, Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 40
    iget-object v1, p0, Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 42
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ad_offer"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/MobileAdResponse;

    .line 45
    iget-object v1, p0, Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity;->e:Lcom/chase/sig/android/activity/mc;

    const-class v2, Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity$b;

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/activity/mc;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/ae;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity$b;

    .line 47
    invoke-virtual {v1}, Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity$b;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v2

    sget-object v3, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-eq v2, v3, :cond_0

    .line 48
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/MobileAdResponse;->a()Lcom/chase/sig/android/domain/MobileAdResponseContent;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/MobileAdResponseContent;->b()Lcom/chase/sig/android/domain/ResponseActions;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/ResponseActions;->b()Lcom/chase/sig/android/domain/ResponseActions$Action;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/ResponseActions$Action;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity$b;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 51
    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 165
    iget-boolean v0, p0, Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity;->b:Z

    if-eqz v0, :cond_0

    .line 166
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity;->setResult(I)V

    .line 170
    :goto_0
    invoke-super {p0}, Lcom/chase/sig/android/activity/ai;->onBackPressed()V

    .line 171
    return-void

    .line 168
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity;->setResult(I)V

    goto :goto_0
.end method
