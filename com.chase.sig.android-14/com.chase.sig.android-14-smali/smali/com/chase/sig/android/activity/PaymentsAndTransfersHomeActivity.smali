.class public Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$e;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity$a;
    }
.end annotation


# instance fields
.field a:Landroid/widget/LinearLayout;

.field b:Landroid/widget/LinearLayout;

.field c:Landroid/widget/LinearLayout;

.field d:Landroid/widget/LinearLayout;

.field k:Landroid/widget/LinearLayout;

.field l:Landroid/view/View;

.field m:Landroid/view/View;

.field n:Landroid/view/View;

.field o:Landroid/view/View;

.field private p:Lcom/chase/sig/android/domain/g;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    .line 255
    return-void
.end method

.method private static a(Landroid/widget/LinearLayout;I)V
    .locals 2
    .parameter
    .parameter

    .prologue
    const/16 v1, 0xa

    .line 139
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/widget/LinearLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 140
    invoke-virtual {p0, v1, v1, v1, v1}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 141
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;)Z
    .locals 1
    .parameter

    .prologue
    .line 19
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->d:Lcom/chase/sig/android/service/SplashResponse;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->d:Lcom/chase/sig/android/service/SplashResponse;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/SplashResponse;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;)Lcom/chase/sig/android/domain/g;
    .locals 1
    .parameter

    .prologue
    .line 19
    iget-object v0, p0, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->p:Lcom/chase/sig/android/domain/g;

    return-object v0
.end method

.method static synthetic c(Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->e()V

    return-void
.end method

.method private e()V
    .locals 3

    .prologue
    const v1, 0x7f0900db

    const/4 v2, 0x0

    .line 234
    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 235
    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 236
    const v0, 0x7f090162

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1, v2, v2, v2}, Landroid/view/View;->setPadding(IIII)V

    .line 237
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 9
    .parameter

    .prologue
    const v8, 0x7f090165

    const v7, 0x7f09015f

    const v6, 0x7f09015e

    const/4 v5, 0x0

    const/16 v4, 0x8

    .line 38
    const v0, 0x7f030063

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->b(I)V

    .line 39
    const v0, 0x7f070138

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->setTitle(I)V

    .line 41
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->b:Lcom/chase/sig/android/domain/g;

    iput-object v0, p0, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->p:Lcom/chase/sig/android/domain/g;

    .line 43
    invoke-virtual {p0, v6}, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->a:Landroid/widget/LinearLayout;

    .line 44
    invoke-virtual {p0, v7}, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->b:Landroid/widget/LinearLayout;

    .line 45
    invoke-virtual {p0, v8}, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->c:Landroid/widget/LinearLayout;

    .line 46
    const v0, 0x7f090167

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->d:Landroid/widget/LinearLayout;

    .line 47
    const v0, 0x7f090161

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->k:Landroid/widget/LinearLayout;

    .line 49
    const v0, 0x7f090044

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->l:Landroid/view/View;

    .line 50
    const v0, 0x7f090160

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->m:Landroid/view/View;

    .line 51
    const v0, 0x7f090164

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->n:Landroid/view/View;

    .line 52
    const v0, 0x7f090166

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->o:Landroid/view/View;

    .line 54
    const v0, 0x7f090162

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070251

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070252

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/chase/sig/android/util/s;->z(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 59
    iget-object v0, p0, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->p:Lcom/chase/sig/android/domain/g;

    invoke-interface {v0}, Lcom/chase/sig/android/domain/g;->B()Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->k:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v1, p0, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->n:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    invoke-interface {v0}, Lcom/chase/sig/android/domain/g;->n()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v1, p0, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->n:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    invoke-interface {v0}, Lcom/chase/sig/android/domain/g;->G()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v1, p0, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->l:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    invoke-interface {v0}, Lcom/chase/sig/android/domain/g;->H()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v1, p0, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->m:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    invoke-interface {v0}, Lcom/chase/sig/android/domain/g;->I()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->o:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 61
    :cond_3
    iget-object v0, p0, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-eq v0, v4, :cond_5

    iget-object v0, p0, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->a:Landroid/widget/LinearLayout;

    :goto_1
    iget-object v1, p0, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v1

    if-eq v1, v4, :cond_9

    iget-object v1, p0, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->d:Landroid/widget/LinearLayout;

    :goto_2
    if-ne v0, v1, :cond_d

    const v1, 0x7f0200d3

    invoke-static {v0, v1}, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->a(Landroid/widget/LinearLayout;I)V

    .line 63
    :goto_3
    const-class v0, Lcom/chase/sig/android/activity/TransferHomeActivity;

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/a/f;

    move-result-object v0

    invoke-virtual {p0, v6, v0}, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 65
    const-class v0, Lcom/chase/sig/android/activity/BillPayHomeActivity;

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/a/f;

    move-result-object v0

    invoke-virtual {p0, v7, v0}, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 67
    const-class v0, Lcom/chase/sig/android/activity/EPayHomeActivity;

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/a/f;

    move-result-object v0

    invoke-virtual {p0, v8, v0}, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 69
    const v0, 0x7f090167

    const-class v1, Lcom/chase/sig/android/activity/wire/WireHomeActivity;

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/a/f;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 70
    return-void

    .line 59
    :cond_4
    iget-object v1, p0, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->k:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v1, p0, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->n:Landroid/view/View;

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 61
    :cond_5
    iget-object v0, p0, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-eq v0, v4, :cond_6

    iget-object v0, p0, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->b:Landroid/widget/LinearLayout;

    goto :goto_1

    :cond_6
    iget-object v0, p0, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->k:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-eq v0, v4, :cond_7

    iget-object v0, p0, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->k:Landroid/widget/LinearLayout;

    goto :goto_1

    :cond_7
    iget-object v0, p0, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-eq v0, v4, :cond_8

    iget-object v0, p0, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->c:Landroid/widget/LinearLayout;

    goto :goto_1

    :cond_8
    iget-object v0, p0, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->d:Landroid/widget/LinearLayout;

    goto :goto_1

    :cond_9
    iget-object v1, p0, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v1

    if-eq v1, v4, :cond_a

    iget-object v1, p0, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->c:Landroid/widget/LinearLayout;

    goto :goto_2

    :cond_a
    iget-object v1, p0, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->k:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v1

    if-eq v1, v4, :cond_b

    iget-object v1, p0, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->k:Landroid/widget/LinearLayout;

    goto :goto_2

    :cond_b
    iget-object v1, p0, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v1

    if-eq v1, v4, :cond_c

    iget-object v1, p0, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->b:Landroid/widget/LinearLayout;

    goto :goto_2

    :cond_c
    iget-object v1, p0, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->a:Landroid/widget/LinearLayout;

    goto/16 :goto_2

    :cond_d
    const v2, 0x7f0200d2

    invoke-static {v0, v2}, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->a(Landroid/widget/LinearLayout;I)V

    const v0, 0x7f0200d1

    invoke-static {v1, v0}, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->a(Landroid/widget/LinearLayout;I)V

    goto/16 :goto_3
.end method

.method public final d()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 186
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->c:Lcom/chase/sig/android/service/quickpay/TodoListResponse;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->c:Lcom/chase/sig/android/service/quickpay/TodoListResponse;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->d()I

    move-result v0

    if-lez v0, :cond_0

    .line 187
    const v0, 0x7f090163

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v2

    iget-object v2, v2, Lcom/chase/sig/android/domain/o;->c:Lcom/chase/sig/android/service/quickpay/TodoListResponse;

    invoke-virtual {v2}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->d()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 191
    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->k:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 192
    iget-object v0, p0, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->k:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 194
    iget-object v0, p0, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->k:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/chase/sig/android/activity/fn;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/fn;-><init>(Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 231
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 74
    invoke-super {p0}, Lcom/chase/sig/android/activity/ai;->onResume()V

    .line 75
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->c:Lcom/chase/sig/android/service/quickpay/TodoListResponse;

    if-nez v0, :cond_0

    const v0, 0x7f040004

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    const v1, 0x7f0900db

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    const-class v0, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity$a;

    new-array v1, v3, [Ljava/lang/Integer;

    const/4 v2, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 76
    :goto_0
    return-void

    .line 75
    :cond_0
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->d()V

    invoke-direct {p0}, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->e()V

    goto :goto_0
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 251
    invoke-super {p0}, Lcom/chase/sig/android/activity/ai;->onStop()V

    .line 252
    iget-object v0, p0, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->e:Lcom/chase/sig/android/activity/mc;

    const-class v1, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity$a;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/mc;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/ae;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity$a;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity$a;->c()V

    .line 253
    return-void
.end method
