.class public Lcom/chase/sig/android/activity/PositionDetailActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$d;


# instance fields
.field a:Lcom/chase/sig/android/domain/Position;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 12
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 29
    const v0, 0x7f07029c

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/PositionDetailActivity;->setTitle(I)V

    .line 30
    const v0, 0x7f030066

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/PositionDetailActivity;->b(I)V

    .line 31
    const v0, 0x7f0900b3

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/PositionDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 32
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/PositionDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "transaction_object"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/domain/Position;

    iput-object v1, p0, Lcom/chase/sig/android/activity/PositionDetailActivity;->a:Lcom/chase/sig/android/domain/Position;

    .line 33
    iget-object v1, p0, Lcom/chase/sig/android/activity/PositionDetailActivity;->a:Lcom/chase/sig/android/domain/Position;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/Position;->h()Z

    move-result v6

    .line 34
    iget-object v1, p0, Lcom/chase/sig/android/activity/PositionDetailActivity;->a:Lcom/chase/sig/android/domain/Position;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/Position;->m()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->w(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 36
    new-instance v8, Lcom/chase/sig/android/activity/fo;

    invoke-direct {v8, p0}, Lcom/chase/sig/android/activity/fo;-><init>(Lcom/chase/sig/android/activity/PositionDetailActivity;)V

    .line 49
    const v1, 0x7f09000f

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/PositionDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/view/detail/DetailView;

    .line 50
    invoke-virtual {v1}, Lcom/chase/sig/android/view/detail/DetailView;->d()V

    .line 52
    iget-object v2, p0, Lcom/chase/sig/android/activity/PositionDetailActivity;->a:Lcom/chase/sig/android/domain/Position;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/Position;->t()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 53
    const v2, 0x7f0900ef

    invoke-virtual {p0, v2}, Lcom/chase/sig/android/activity/PositionDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/chase/sig/android/activity/PositionDetailActivity;->a:Lcom/chase/sig/android/domain/Position;

    invoke-virtual {v9}, Lcom/chase/sig/android/domain/Position;->b()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v3, p0, Lcom/chase/sig/android/activity/PositionDetailActivity;->a:Lcom/chase/sig/android/domain/Position;

    invoke-virtual {v3}, Lcom/chase/sig/android/domain/Position;->u()Z

    move-result v3

    if-eqz v3, :cond_3

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v10, " ("

    invoke-direct {v3, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v10, p0, Lcom/chase/sig/android/activity/PositionDetailActivity;->a:Lcom/chase/sig/android/domain/Position;

    invoke-virtual {v10}, Lcom/chase/sig/android/domain/Position;->i()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v10, ")"

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    :goto_0
    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 57
    :cond_0
    const/16 v2, 0x9

    new-array v9, v2, [Lcom/chase/sig/android/view/detail/a;

    new-instance v3, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v2, "Value"

    iget-object v10, p0, Lcom/chase/sig/android/activity/PositionDetailActivity;->a:Lcom/chase/sig/android/domain/Position;

    invoke-virtual {v10}, Lcom/chase/sig/android/domain/Position;->a()Lcom/chase/sig/android/util/Dollar;

    move-result-object v10

    invoke-virtual {v10}, Lcom/chase/sig/android/util/Dollar;->h()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/chase/sig/android/util/s;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v3, v2, v10}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/chase/sig/android/activity/PositionDetailActivity;->a:Lcom/chase/sig/android/domain/Position;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/Position;->w()Z

    move-result v2

    if-nez v2, :cond_4

    move v2, v4

    :goto_1
    iput-boolean v2, v3, Lcom/chase/sig/android/view/detail/a;->j:Z

    aput-object v3, v9, v5

    new-instance v3, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v10, "Quantity"

    iget-object v2, p0, Lcom/chase/sig/android/activity/PositionDetailActivity;->a:Lcom/chase/sig/android/domain/Position;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/Position;->f()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/chase/sig/android/activity/PositionDetailActivity;->a:Lcom/chase/sig/android/domain/Position;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/Position;->f()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v2

    const-string v11, "#,##0.00"

    invoke-static {v2, v11}, Lcom/chase/sig/android/util/s;->a(Ljava/lang/Double;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_2
    invoke-direct {v3, v10, v2}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/chase/sig/android/activity/PositionDetailActivity;->a:Lcom/chase/sig/android/domain/Position;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/Position;->s()Z

    move-result v2

    if-nez v2, :cond_6

    move v2, v4

    :goto_3
    iput-boolean v2, v3, Lcom/chase/sig/android/view/detail/a;->j:Z

    aput-object v3, v9, v4

    const/4 v10, 0x2

    new-instance v2, Lcom/chase/sig/android/view/detail/DetailRow;

    iget-object v3, p0, Lcom/chase/sig/android/activity/PositionDetailActivity;->a:Lcom/chase/sig/android/domain/Position;

    invoke-virtual {v3}, Lcom/chase/sig/android/domain/Position;->j()Ljava/lang/String;

    move-result-object v3

    iget-object v11, p0, Lcom/chase/sig/android/activity/PositionDetailActivity;->a:Lcom/chase/sig/android/domain/Position;

    invoke-virtual {v11}, Lcom/chase/sig/android/domain/Position;->i()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/chase/sig/android/util/s;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v2, v3, v11}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/chase/sig/android/activity/PositionDetailActivity;->a:Lcom/chase/sig/android/domain/Position;

    invoke-virtual {v3}, Lcom/chase/sig/android/domain/Position;->u()Z

    move-result v3

    if-nez v3, :cond_7

    move v3, v4

    :goto_4
    iput-boolean v3, v2, Lcom/chase/sig/android/view/detail/a;->j:Z

    check-cast v2, Lcom/chase/sig/android/view/detail/DetailRow;

    iput-boolean v6, v2, Lcom/chase/sig/android/view/detail/a;->m:Z

    if-eqz v6, :cond_1

    iput-object v8, v2, Lcom/chase/sig/android/view/detail/a;->h:Landroid/view/View$OnClickListener;

    :cond_1
    aput-object v2, v9, v10

    const/4 v3, 0x3

    new-instance v6, Lcom/chase/sig/android/view/detail/DetailRowWithSubItem;

    const-string v8, "Price"

    iget-object v2, p0, Lcom/chase/sig/android/activity/PositionDetailActivity;->a:Lcom/chase/sig/android/domain/Position;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/Position;->r()Z

    move-result v2

    if-eqz v2, :cond_8

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v10, "(as of "

    invoke-direct {v2, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, ")"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_5
    iget-object v7, p0, Lcom/chase/sig/android/activity/PositionDetailActivity;->a:Lcom/chase/sig/android/domain/Position;

    invoke-virtual {v7}, Lcom/chase/sig/android/domain/Position;->c()Lcom/chase/sig/android/util/Dollar;

    move-result-object v7

    invoke-virtual {v7}, Lcom/chase/sig/android/util/Dollar;->h()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/chase/sig/android/util/s;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v8, v2, v7}, Lcom/chase/sig/android/view/detail/DetailRowWithSubItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/chase/sig/android/activity/PositionDetailActivity;->a:Lcom/chase/sig/android/domain/Position;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/Position;->q()Z

    move-result v2

    if-nez v2, :cond_9

    move v2, v4

    :goto_6
    iput-boolean v2, v6, Lcom/chase/sig/android/view/detail/a;->j:Z

    aput-object v6, v9, v3

    const/4 v3, 0x4

    new-instance v6, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v2, "Cost"

    iget-object v7, p0, Lcom/chase/sig/android/activity/PositionDetailActivity;->a:Lcom/chase/sig/android/domain/Position;

    invoke-virtual {v7}, Lcom/chase/sig/android/domain/Position;->e()Lcom/chase/sig/android/util/Dollar;

    move-result-object v7

    invoke-virtual {v7}, Lcom/chase/sig/android/util/Dollar;->h()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/chase/sig/android/util/s;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v2, v7}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/chase/sig/android/activity/PositionDetailActivity;->a:Lcom/chase/sig/android/domain/Position;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/Position;->o()Z

    move-result v2

    if-nez v2, :cond_a

    move v2, v4

    :goto_7
    iput-boolean v2, v6, Lcom/chase/sig/android/view/detail/a;->j:Z

    aput-object v6, v9, v3

    const/4 v3, 0x5

    new-instance v6, Lcom/chase/sig/android/view/detail/u;

    const-string v2, "Gain/Loss"

    iget-object v7, p0, Lcom/chase/sig/android/activity/PositionDetailActivity;->a:Lcom/chase/sig/android/domain/Position;

    invoke-virtual {v7}, Lcom/chase/sig/android/domain/Position;->d()Lcom/chase/sig/android/util/Dollar;

    move-result-object v7

    invoke-virtual {v7}, Lcom/chase/sig/android/util/Dollar;->h()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v2, v7}, Lcom/chase/sig/android/view/detail/u;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/chase/sig/android/activity/PositionDetailActivity;->a:Lcom/chase/sig/android/domain/Position;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/Position;->v()Z

    move-result v2

    if-nez v2, :cond_b

    move v2, v4

    :goto_8
    iput-boolean v2, v6, Lcom/chase/sig/android/view/detail/a;->j:Z

    aput-object v6, v9, v3

    const/4 v3, 0x6

    new-instance v6, Lcom/chase/sig/android/view/detail/u;

    const v2, 0x7f0702b4

    invoke-virtual {p0, v2}, Lcom/chase/sig/android/activity/PositionDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v7, p0, Lcom/chase/sig/android/activity/PositionDetailActivity;->a:Lcom/chase/sig/android/domain/Position;

    invoke-virtual {v7}, Lcom/chase/sig/android/domain/Position;->x()Lcom/chase/sig/android/util/Dollar;

    move-result-object v7

    invoke-virtual {v7}, Lcom/chase/sig/android/util/Dollar;->h()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v2, v7}, Lcom/chase/sig/android/view/detail/u;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/chase/sig/android/activity/PositionDetailActivity;->a:Lcom/chase/sig/android/domain/Position;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/Position;->x()Lcom/chase/sig/android/util/Dollar;

    move-result-object v2

    if-nez v2, :cond_c

    move v2, v4

    :goto_9
    iput-boolean v2, v6, Lcom/chase/sig/android/view/detail/a;->j:Z

    aput-object v6, v9, v3

    const/4 v3, 0x7

    new-instance v6, Lcom/chase/sig/android/view/detail/s;

    iget-object v2, p0, Lcom/chase/sig/android/activity/PositionDetailActivity;->a:Lcom/chase/sig/android/domain/Position;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/Position;->g()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v6, v2}, Lcom/chase/sig/android/view/detail/s;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/chase/sig/android/activity/PositionDetailActivity;->a:Lcom/chase/sig/android/domain/Position;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/Position;->p()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/chase/sig/android/activity/PositionDetailActivity;->a:Lcom/chase/sig/android/domain/Position;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/Position;->g()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/chase/sig/android/util/s;->l(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_d

    :cond_2
    move v2, v4

    :goto_a
    iput-boolean v2, v6, Lcom/chase/sig/android/view/detail/a;->j:Z

    aput-object v6, v9, v3

    const/16 v2, 0x8

    new-instance v3, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v6, "Asset class"

    iget-object v7, p0, Lcom/chase/sig/android/activity/PositionDetailActivity;->a:Lcom/chase/sig/android/domain/Position;

    invoke-virtual {v7}, Lcom/chase/sig/android/domain/Position;->k()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v3, v6, v7}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v6, p0, Lcom/chase/sig/android/activity/PositionDetailActivity;->a:Lcom/chase/sig/android/domain/Position;

    invoke-virtual {v6}, Lcom/chase/sig/android/domain/Position;->n()Z

    move-result v6

    if-nez v6, :cond_e

    :goto_b
    iput-boolean v4, v3, Lcom/chase/sig/android/view/detail/a;->j:Z

    aput-object v3, v9, v2

    invoke-virtual {v1, v9}, Lcom/chase/sig/android/view/detail/DetailView;->setRows([Lcom/chase/sig/android/view/detail/a;)V

    .line 82
    const-class v1, Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/PositionDetailActivity;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/a/f;

    move-result-object v1

    const-string v2, "title"

    invoke-virtual {v0}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/chase/sig/android/activity/a/f;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/activity/a/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 85
    return-void

    .line 53
    :cond_3
    const-string v3, ""

    goto/16 :goto_0

    :cond_4
    move v2, v5

    .line 57
    goto/16 :goto_1

    :cond_5
    const-string v2, "--"

    goto/16 :goto_2

    :cond_6
    move v2, v5

    goto/16 :goto_3

    :cond_7
    move v3, v5

    goto/16 :goto_4

    :cond_8
    const-string v2, "(as of )"

    goto/16 :goto_5

    :cond_9
    move v2, v5

    goto/16 :goto_6

    :cond_a
    move v2, v5

    goto/16 :goto_7

    :cond_b
    move v2, v5

    goto/16 :goto_8

    :cond_c
    move v2, v5

    goto :goto_9

    :cond_d
    move v2, v5

    goto :goto_a

    :cond_e
    move v4, v5

    goto :goto_b
.end method
