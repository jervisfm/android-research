.class public final Lcom/chase/sig/android/activity/PositionsActivity$a;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/PositionsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/chase/sig/android/domain/Position;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/PositionsActivity;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/Position;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/chase/sig/android/activity/PositionsActivity;Landroid/content/Context;Ljava/util/Set;)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/Set",
            "<",
            "Lcom/chase/sig/android/domain/Position;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 196
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/chase/sig/android/activity/PositionsActivity$a;-><init>(Lcom/chase/sig/android/activity/PositionsActivity;Landroid/content/Context;Ljava/util/Set;B)V

    .line 197
    return-void
.end method

.method private constructor <init>(Lcom/chase/sig/android/activity/PositionsActivity;Landroid/content/Context;Ljava/util/Set;B)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "IBB)V"
        }
    .end annotation

    .prologue
    .line 200
    iput-object p1, p0, Lcom/chase/sig/android/activity/PositionsActivity$a;->a:Lcom/chase/sig/android/activity/PositionsActivity;

    .line 201
    const v0, 0x7f030067

    invoke-direct {p0, p2, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 202
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/chase/sig/android/activity/PositionsActivity$a;->b:Ljava/util/List;

    .line 203
    return-void
.end method


# virtual methods
.method public final getCount()I
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lcom/chase/sig/android/activity/PositionsActivity$a;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 190
    iget-object v0, p0, Lcom/chase/sig/android/activity/PositionsActivity$a;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/Position;

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2
    .parameter

    .prologue
    .line 217
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 222
    if-nez p2, :cond_0

    .line 223
    iget-object v0, p0, Lcom/chase/sig/android/activity/PositionsActivity$a;->a:Lcom/chase/sig/android/activity/PositionsActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/PositionsActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030067

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 226
    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/PositionsActivity$a;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/Position;

    .line 228
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Position;->t()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Position;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    .line 231
    :goto_0
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Position;->q()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Position;->c()Lcom/chase/sig/android/util/Dollar;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/util/Dollar;->h()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    move-object v3, v1

    .line 234
    :goto_1
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Position;->w()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Position;->a()Lcom/chase/sig/android/util/Dollar;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/util/Dollar;->h()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    move-object v4, v1

    .line 237
    :goto_2
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Position;->i()Ljava/lang/String;

    move-result-object v1

    .line 238
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Position;->u()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, " ("

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, ")"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v5, v1

    .line 242
    :goto_3
    const v1, 0x7f09016a

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 244
    const v1, 0x7f09016c

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 246
    const v1, 0x7f09016d

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/view/TickerValueTextView;

    .line 248
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Position;->v()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 249
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/view/TickerValueTextView;->setDisplayPlaceholder(Z)V

    .line 250
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Position;->d()Lcom/chase/sig/android/util/Dollar;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/view/TickerValueTextView;->setTickerValue(Lcom/chase/sig/android/util/Dollar;)V

    .line 255
    :goto_4
    const v0, 0x7f09016e

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 256
    return-object p2

    .line 228
    :cond_1
    const-string v1, "--"

    move-object v2, v1

    goto/16 :goto_0

    .line 231
    :cond_2
    const-string v1, "--"

    move-object v3, v1

    goto/16 :goto_1

    .line 234
    :cond_3
    const-string v1, "--"

    move-object v4, v1

    goto/16 :goto_2

    .line 238
    :cond_4
    const-string v1, "--"

    move-object v5, v1

    goto :goto_3

    .line 252
    :cond_5
    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/view/TickerValueTextView;->setVisibility(I)V

    goto :goto_4
.end method
