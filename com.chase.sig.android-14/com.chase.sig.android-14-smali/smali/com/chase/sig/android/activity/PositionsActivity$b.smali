.class final Lcom/chase/sig/android/activity/PositionsActivity$b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/PositionsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/PositionsActivity;


# direct methods
.method private constructor <init>(Lcom/chase/sig/android/activity/PositionsActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 180
    iput-object p1, p0, Lcom/chase/sig/android/activity/PositionsActivity$b;->a:Lcom/chase/sig/android/activity/PositionsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/chase/sig/android/activity/PositionsActivity;B)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 180
    invoke-direct {p0, p1}, Lcom/chase/sig/android/activity/PositionsActivity$b;-><init>(Lcom/chase/sig/android/activity/PositionsActivity;)V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 183
    :try_start_0
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/Position;

    .line 184
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/chase/sig/android/activity/PositionsActivity$b;->a:Lcom/chase/sig/android/activity/PositionsActivity;

    const-class v3, Lcom/chase/sig/android/activity/PositionDetailActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 185
    const-string v2, "transaction_object"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 186
    iget-object v0, p0, Lcom/chase/sig/android/activity/PositionsActivity$b;->a:Lcom/chase/sig/android/activity/PositionsActivity;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/PositionsActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 187
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1, p2, p3}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/widget/AdapterView;Landroid/view/View;I)V

    return-void

    :catch_0
    move-exception v0

    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1, p2, p3}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/widget/AdapterView;Landroid/view/View;I)V

    throw v0
.end method
