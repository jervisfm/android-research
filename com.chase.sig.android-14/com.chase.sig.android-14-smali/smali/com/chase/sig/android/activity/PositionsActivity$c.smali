.class public Lcom/chase/sig/android/activity/PositionsActivity$c;
.super Lcom/chase/sig/android/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/PositionsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/d",
        "<",
        "Lcom/chase/sig/android/activity/PositionsActivity;",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/service/PositionResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 103
    invoke-direct {p0}, Lcom/chase/sig/android/d;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .parameter

    .prologue
    .line 103
    check-cast p1, [Ljava/lang/String;

    const/4 v0, 0x0

    aget-object v1, p1, v0

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/PositionsActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/PositionsActivity;->A()Lcom/chase/sig/android/service/n;

    move-result-object v0

    iget-object v2, v0, Lcom/chase/sig/android/service/n;->k:Lcom/chase/sig/android/service/v;

    if-nez v2, :cond_0

    new-instance v2, Lcom/chase/sig/android/service/v;

    invoke-direct {v2}, Lcom/chase/sig/android/service/v;-><init>()V

    iput-object v2, v0, Lcom/chase/sig/android/service/n;->k:Lcom/chase/sig/android/service/v;

    :cond_0
    iget-object v0, v0, Lcom/chase/sig/android/service/n;->k:Lcom/chase/sig/android/service/v;

    invoke-static {v1}, Lcom/chase/sig/android/service/v;->a(Ljava/lang/String;)Lcom/chase/sig/android/service/PositionResponse;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 2
    .parameter

    .prologue
    .line 103
    check-cast p1, Lcom/chase/sig/android/service/PositionResponse;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/PositionsActivity;

    invoke-static {v0, p1}, Lcom/chase/sig/android/activity/PositionsActivity;->a(Lcom/chase/sig/android/activity/PositionsActivity;Lcom/chase/sig/android/service/PositionResponse;)Lcom/chase/sig/android/service/PositionResponse;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/PositionResponse;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/PositionsActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/PositionResponse;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/PositionsActivity;->b(Ljava/util/List;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/PositionsActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/chase/sig/android/activity/PositionsActivity;->a(Lcom/chase/sig/android/service/PositionResponse;Z)V

    goto :goto_0
.end method
