.class public Lcom/chase/sig/android/activity/PositionsActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$d;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/PositionsActivity$a;,
        Lcom/chase/sig/android/activity/PositionsActivity$b;,
        Lcom/chase/sig/android/activity/PositionsActivity$c;
    }
.end annotation


# instance fields
.field private a:Landroid/widget/ListView;

.field private b:Lcom/chase/sig/android/service/PositionResponse;

.field private c:Landroid/view/View;

.field private d:Landroid/view/View;

.field private k:Landroid/view/View;

.field private l:Z

.field private m:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    .line 49
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/chase/sig/android/activity/PositionsActivity;->l:Z

    .line 190
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/PositionsActivity;Lcom/chase/sig/android/service/PositionResponse;)Lcom/chase/sig/android/service/PositionResponse;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 39
    iput-object p1, p0, Lcom/chase/sig/android/activity/PositionsActivity;->b:Lcom/chase/sig/android/service/PositionResponse;

    return-object p1
.end method

.method private a(Ljava/util/Map;)V
    .locals 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/chase/sig/android/domain/PositionType;",
            "+",
            "Ljava/util/Set",
            "<",
            "Lcom/chase/sig/android/domain/Position;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 153
    new-instance v2, Lcom/chase/sig/android/activity/fp;

    invoke-direct {v2, p0}, Lcom/chase/sig/android/activity/fp;-><init>(Lcom/chase/sig/android/activity/PositionsActivity;)V

    .line 165
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/PositionType;

    .line 166
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Set;

    .line 167
    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 168
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/PositionType;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    new-instance v4, Lcom/chase/sig/android/activity/PositionsActivity$a;

    invoke-direct {v4, p0, p0, v0}, Lcom/chase/sig/android/activity/PositionsActivity$a;-><init>(Lcom/chase/sig/android/activity/PositionsActivity;Landroid/content/Context;Ljava/util/Set;)V

    invoke-virtual {v2, v1, v4}, Lcom/chase/sig/android/view/w;->a(Ljava/lang/String;Landroid/widget/Adapter;)V

    goto :goto_0

    .line 172
    :cond_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/PositionsActivity;->a:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 173
    iget-object v0, p0, Lcom/chase/sig/android/activity/PositionsActivity;->a:Landroid/widget/ListView;

    new-instance v1, Lcom/chase/sig/android/activity/PositionsActivity$b;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/chase/sig/android/activity/PositionsActivity$b;-><init>(Lcom/chase/sig/android/activity/PositionsActivity;B)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 174
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/PositionsActivity;)Z
    .locals 1
    .parameter

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/chase/sig/android/activity/PositionsActivity;->l:Z

    return v0
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/PositionsActivity;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 39
    iput-boolean p1, p0, Lcom/chase/sig/android/activity/PositionsActivity;->l:Z

    return p1
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/PositionsActivity;)Landroid/view/View;
    .locals 1
    .parameter

    .prologue
    .line 39
    iget-object v0, p0, Lcom/chase/sig/android/activity/PositionsActivity;->c:Landroid/view/View;

    return-object v0
.end method

.method static synthetic c(Lcom/chase/sig/android/activity/PositionsActivity;)Landroid/view/View;
    .locals 1
    .parameter

    .prologue
    .line 39
    iget-object v0, p0, Lcom/chase/sig/android/activity/PositionsActivity;->d:Landroid/view/View;

    return-object v0
.end method

.method static synthetic d(Lcom/chase/sig/android/activity/PositionsActivity;)Landroid/view/View;
    .locals 1
    .parameter

    .prologue
    .line 39
    iget-object v0, p0, Lcom/chase/sig/android/activity/PositionsActivity;->k:Landroid/view/View;

    return-object v0
.end method

.method static synthetic e(Lcom/chase/sig/android/activity/PositionsActivity;)Landroid/widget/Button;
    .locals 1
    .parameter

    .prologue
    .line 39
    iget-object v0, p0, Lcom/chase/sig/android/activity/PositionsActivity;->m:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic f(Lcom/chase/sig/android/activity/PositionsActivity;)Landroid/view/animation/Animation$AnimationListener;
    .locals 1
    .parameter

    .prologue
    .line 39
    new-instance v0, Lcom/chase/sig/android/activity/fr;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/activity/fr;-><init>(Lcom/chase/sig/android/activity/PositionsActivity;)V

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 6
    .parameter

    .prologue
    const/16 v3, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 55
    const v0, 0x7f070297

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/PositionsActivity;->setTitle(I)V

    .line 56
    const v0, 0x7f030068

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/PositionsActivity;->b(I)V

    .line 57
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/PositionsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    .line 58
    const v0, 0x7f09017c

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/PositionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/PositionsActivity;->a:Landroid/widget/ListView;

    .line 59
    const v0, 0x7f090170

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/PositionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/PositionsActivity;->c:Landroid/view/View;

    .line 60
    const v0, 0x7f090171

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/PositionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/PositionsActivity;->d:Landroid/view/View;

    .line 61
    const v0, 0x7f090172

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/PositionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/PositionsActivity;->k:Landroid/view/View;

    .line 62
    const v0, 0x7f09008f

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/PositionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/chase/sig/android/activity/PositionsActivity;->m:Landroid/widget/Button;

    .line 64
    if-eqz p1, :cond_0

    .line 65
    const-string v0, "account_summary_state"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/chase/sig/android/activity/PositionsActivity;->l:Z

    .line 69
    :cond_0
    iget-object v5, p0, Lcom/chase/sig/android/activity/PositionsActivity;->c:Landroid/view/View;

    iget-boolean v0, p0, Lcom/chase/sig/android/activity/PositionsActivity;->l:Z

    if-eqz v0, :cond_3

    move v0, v3

    :goto_1
    invoke-virtual {v5, v0}, Landroid/view/View;->setVisibility(I)V

    .line 70
    iget-object v5, p0, Lcom/chase/sig/android/activity/PositionsActivity;->k:Landroid/view/View;

    iget-boolean v0, p0, Lcom/chase/sig/android/activity/PositionsActivity;->l:Z

    if-eqz v0, :cond_4

    move v0, v3

    :goto_2
    invoke-virtual {v5, v0}, Landroid/view/View;->setVisibility(I)V

    .line 71
    iget-object v0, p0, Lcom/chase/sig/android/activity/PositionsActivity;->d:Landroid/view/View;

    iget-boolean v5, p0, Lcom/chase/sig/android/activity/PositionsActivity;->l:Z

    if-eqz v5, :cond_5

    :goto_3
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 73
    iget-object v3, p0, Lcom/chase/sig/android/activity/PositionsActivity;->m:Landroid/widget/Button;

    iget-boolean v0, p0, Lcom/chase/sig/android/activity/PositionsActivity;->l:Z

    if-eqz v0, :cond_6

    const v0, 0x7f0200ed

    :goto_4
    invoke-virtual {v3, v0}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 76
    iget-object v0, p0, Lcom/chase/sig/android/activity/PositionsActivity;->a:Landroid/widget/ListView;

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/PositionsActivity;->a(Landroid/widget/ListView;)V

    .line 78
    const-string v0, "response"

    invoke-static {p1, v0}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 79
    const-string v0, "response"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/service/PositionResponse;

    iput-object v0, p0, Lcom/chase/sig/android/activity/PositionsActivity;->b:Lcom/chase/sig/android/service/PositionResponse;

    .line 80
    iget-object v0, p0, Lcom/chase/sig/android/activity/PositionsActivity;->b:Lcom/chase/sig/android/service/PositionResponse;

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/PositionsActivity;->a(Lcom/chase/sig/android/service/PositionResponse;Z)V

    .line 86
    :cond_1
    :goto_5
    new-instance v0, Lcom/chase/sig/android/activity/fq;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/activity/fq;-><init>(Lcom/chase/sig/android/activity/PositionsActivity;)V

    const v1, 0x7f09017a

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/PositionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/chase/sig/android/activity/PositionsActivity;->m:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 87
    return-void

    :cond_2
    move v0, v2

    .line 65
    goto :goto_0

    :cond_3
    move v0, v2

    .line 69
    goto :goto_1

    :cond_4
    move v0, v2

    .line 70
    goto :goto_2

    :cond_5
    move v3, v2

    .line 71
    goto :goto_3

    .line 73
    :cond_6
    const v0, 0x7f0200ee

    goto :goto_4

    .line 81
    :cond_7
    const-string v0, "simple_dialogs"

    invoke-static {p1, v0}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 82
    const-string v0, "selectedAccountId"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 83
    const-class v3, Lcom/chase/sig/android/activity/PositionsActivity$c;

    new-array v1, v1, [Ljava/lang/String;

    aput-object v0, v1, v2

    invoke-virtual {p0, v3, v1}, Lcom/chase/sig/android/activity/PositionsActivity;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    goto :goto_5
.end method

.method public final a(Lcom/chase/sig/android/service/PositionResponse;Z)V
    .locals 4
    .parameter
    .parameter

    .prologue
    .line 127
    invoke-virtual {p1}, Lcom/chase/sig/android/service/PositionResponse;->c()Z

    move-result v0

    if-nez v0, :cond_1

    if-nez p2, :cond_1

    .line 128
    const v0, 0x7f07027c

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/PositionsActivity;->g(I)V

    .line 150
    :cond_0
    :goto_0
    return-void

    .line 132
    :cond_1
    invoke-virtual {p1}, Lcom/chase/sig/android/service/PositionResponse;->a()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/chase/sig/android/service/PositionResponse;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 133
    invoke-virtual {p1}, Lcom/chase/sig/android/service/PositionResponse;->a()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/service/PositionsAccount;

    .line 134
    const v1, 0x7f090081

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/PositionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/PositionsAccount;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 135
    const v1, 0x7f090174

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/PositionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    new-instance v2, Lcom/chase/sig/android/util/Dollar;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/PositionsAccount;->e()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/chase/sig/android/util/Dollar;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/chase/sig/android/util/Dollar;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 138
    const v1, 0x7f090176

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/PositionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/view/TickerValueTextView;

    .line 140
    new-instance v2, Lcom/chase/sig/android/util/Dollar;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/PositionsAccount;->d()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/chase/sig/android/util/Dollar;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/view/TickerValueTextView;->setTickerValue(Lcom/chase/sig/android/util/Dollar;)V

    .line 142
    const v1, 0x7f090179

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/PositionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/view/TickerValueTextView;

    .line 144
    invoke-virtual {v0}, Lcom/chase/sig/android/service/PositionsAccount;->g()Lcom/chase/sig/android/util/Dollar;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/view/TickerValueTextView;->setTickerValue(Lcom/chase/sig/android/util/Dollar;)V

    .line 146
    const v1, 0x7f090171

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/PositionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "As of "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/chase/sig/android/service/PositionsAccount;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->w(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 148
    invoke-virtual {p1}, Lcom/chase/sig/android/service/PositionResponse;->b()Ljava/util/Map;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/chase/sig/android/activity/PositionsActivity;->a(Ljava/util/Map;)V

    goto/16 :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 95
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ai;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 96
    iget-object v0, p0, Lcom/chase/sig/android/activity/PositionsActivity;->b:Lcom/chase/sig/android/service/PositionResponse;

    if-eqz v0, :cond_0

    .line 97
    const-string v0, "response"

    iget-object v1, p0, Lcom/chase/sig/android/activity/PositionsActivity;->b:Lcom/chase/sig/android/service/PositionResponse;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 100
    :cond_0
    const-string v0, "account_summary_state"

    iget-object v1, p0, Lcom/chase/sig/android/activity/PositionsActivity;->k:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 101
    return-void
.end method
