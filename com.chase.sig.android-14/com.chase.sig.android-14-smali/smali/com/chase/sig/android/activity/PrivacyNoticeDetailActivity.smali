.class public Lcom/chase/sig/android/activity/PrivacyNoticeDetailActivity;
.super Lcom/chase/sig/android/activity/eb;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/PrivacyNoticeDetailActivity$a;
    }
.end annotation


# static fields
.field private static b:I


# instance fields
.field private final a:[Ljava/lang/String;

.field private c:Landroid/webkit/WebView;

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/chase/sig/android/activity/eb;-><init>()V

    .line 23
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "JPMprivacyPolicy.txt"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "JPMSecuritiesprivacyPolicyNative.txt"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "privacyPolicyNative.txt"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "JPMCCprivacyPolicyNative.txt"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/chase/sig/android/activity/PrivacyNoticeDetailActivity;->a:[Ljava/lang/String;

    .line 87
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/PrivacyNoticeDetailActivity;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/chase/sig/android/activity/PrivacyNoticeDetailActivity;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 6
    .parameter

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 111
    iput-object p1, p0, Lcom/chase/sig/android/activity/PrivacyNoticeDetailActivity;->d:Ljava/lang/String;

    .line 112
    invoke-direct {p0}, Lcom/chase/sig/android/activity/PrivacyNoticeDetailActivity;->g()V

    .line 113
    const v0, 0x7f09015d

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/PrivacyNoticeDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/PrivacyNoticeDetailActivity;->c:Landroid/webkit/WebView;

    .line 114
    iget-object v0, p0, Lcom/chase/sig/android/activity/PrivacyNoticeDetailActivity;->c:Landroid/webkit/WebView;

    iget-object v1, p0, Lcom/chase/sig/android/activity/PrivacyNoticeDetailActivity;->d:Ljava/lang/String;

    const-string v2, "text/html"

    const-string v3, "utf-8"

    invoke-virtual {v0, v1, v2, v3}, Landroid/webkit/WebView;->loadData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    new-instance v0, Lcom/chase/sig/android/activity/fu;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/activity/fu;-><init>(Lcom/chase/sig/android/activity/PrivacyNoticeDetailActivity;)V

    .line 138
    iget-object v1, p0, Lcom/chase/sig/android/activity/PrivacyNoticeDetailActivity;->c:Landroid/webkit/WebView;

    invoke-virtual {v1, v0}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 140
    const v0, 0x7f09015b

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/PrivacyNoticeDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 141
    const v1, 0x7f09015a

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/PrivacyNoticeDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 143
    invoke-virtual {v1, v5}, Landroid/widget/Button;->setEnabled(Z)V

    .line 144
    invoke-virtual {v0, v5}, Landroid/widget/Button;->setEnabled(Z)V

    .line 146
    sget v2, Lcom/chase/sig/android/activity/PrivacyNoticeDetailActivity;->b:I

    iget-object v3, p0, Lcom/chase/sig/android/activity/PrivacyNoticeDetailActivity;->a:[Ljava/lang/String;

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    if-lt v2, v3, :cond_1

    .line 147
    invoke-virtual {v1, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 153
    :cond_0
    :goto_0
    return-void

    .line 149
    :cond_1
    sget v1, Lcom/chase/sig/android/activity/PrivacyNoticeDetailActivity;->b:I

    if-gtz v1, :cond_0

    .line 150
    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/PrivacyNoticeDetailActivity;)[Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 19
    iget-object v0, p0, Lcom/chase/sig/android/activity/PrivacyNoticeDetailActivity;->a:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/PrivacyNoticeDetailActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/chase/sig/android/activity/PrivacyNoticeDetailActivity;->h()V

    return-void
.end method

.method static synthetic d()I
    .locals 1

    .prologue
    .line 19
    sget v0, Lcom/chase/sig/android/activity/PrivacyNoticeDetailActivity;->b:I

    return v0
.end method

.method static synthetic e()I
    .locals 2

    .prologue
    .line 19
    sget v0, Lcom/chase/sig/android/activity/PrivacyNoticeDetailActivity;->b:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/chase/sig/android/activity/PrivacyNoticeDetailActivity;->b:I

    return v0
.end method

.method static synthetic f()I
    .locals 2

    .prologue
    .line 19
    sget v0, Lcom/chase/sig/android/activity/PrivacyNoticeDetailActivity;->b:I

    add-int/lit8 v1, v0, -0x1

    sput v1, Lcom/chase/sig/android/activity/PrivacyNoticeDetailActivity;->b:I

    return v0
.end method

.method private g()V
    .locals 5

    .prologue
    .line 75
    const v0, 0x7f09015c

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/PrivacyNoticeDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 76
    const-string v2, "*%s"

    const/4 v1, 0x1

    new-array v3, v1, [Ljava/lang/Object;

    const/4 v4, 0x0

    sget v1, Lcom/chase/sig/android/activity/PrivacyNoticeDetailActivity;->b:I

    packed-switch v1, :pswitch_data_0

    const/4 v1, 0x0

    :goto_0
    aput-object v1, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 77
    return-void

    .line 76
    :pswitch_0
    const v1, 0x7f07028d

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/PrivacyNoticeDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :pswitch_1
    const v1, 0x7f07028e

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/PrivacyNoticeDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :pswitch_2
    const v1, 0x7f07028f

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/PrivacyNoticeDetailActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private h()V
    .locals 5

    .prologue
    .line 81
    const-string v0, ""

    invoke-direct {p0, v0}, Lcom/chase/sig/android/activity/PrivacyNoticeDetailActivity;->a(Ljava/lang/String;)V

    .line 82
    invoke-direct {p0}, Lcom/chase/sig/android/activity/PrivacyNoticeDetailActivity;->g()V

    .line 84
    const-class v0, Lcom/chase/sig/android/activity/PrivacyNoticeDetailActivity$a;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/chase/sig/android/activity/PrivacyNoticeDetailActivity;->a:[Ljava/lang/String;

    sget v4, Lcom/chase/sig/android/activity/PrivacyNoticeDetailActivity;->b:I

    aget-object v3, v3, v4

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/PrivacyNoticeDetailActivity;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 85
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 38
    const v0, 0x7f07028b

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/PrivacyNoticeDetailActivity;->setTitle(I)V

    .line 39
    const v0, 0x7f030062

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/PrivacyNoticeDetailActivity;->b(I)V

    .line 41
    if-eqz p1, :cond_0

    const-string v0, "document_index"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 42
    const-string v0, "document_index"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/chase/sig/android/activity/PrivacyNoticeDetailActivity;->b:I

    .line 43
    const-string v0, "privacyNoticeData"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/PrivacyNoticeDetailActivity;->d:Ljava/lang/String;

    .line 44
    iget-object v0, p0, Lcom/chase/sig/android/activity/PrivacyNoticeDetailActivity;->d:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/chase/sig/android/activity/PrivacyNoticeDetailActivity;->a(Ljava/lang/String;)V

    .line 50
    :goto_0
    const v0, 0x7f09015a

    new-instance v1, Lcom/chase/sig/android/activity/fs;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/fs;-><init>(Lcom/chase/sig/android/activity/PrivacyNoticeDetailActivity;)V

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/PrivacyNoticeDetailActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 62
    const v0, 0x7f09015b

    new-instance v1, Lcom/chase/sig/android/activity/ft;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/ft;-><init>(Lcom/chase/sig/android/activity/PrivacyNoticeDetailActivity;)V

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/PrivacyNoticeDetailActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 72
    return-void

    .line 46
    :cond_0
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/PrivacyNoticeDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "document_index"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/chase/sig/android/activity/PrivacyNoticeDetailActivity;->b:I

    .line 47
    invoke-direct {p0}, Lcom/chase/sig/android/activity/PrivacyNoticeDetailActivity;->h()V

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 169
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/eb;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 170
    const-string v0, "privacyNoticeData"

    iget-object v1, p0, Lcom/chase/sig/android/activity/PrivacyNoticeDetailActivity;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    const-string v0, "document_index"

    sget v1, Lcom/chase/sig/android/activity/PrivacyNoticeDetailActivity;->b:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 172
    return-void
.end method
