.class public Lcom/chase/sig/android/activity/PrivacyNoticesActivity;
.super Lcom/chase/sig/android/activity/eb;
.source "SourceFile"


# instance fields
.field private a:Landroid/widget/ListView;

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/chase/sig/android/activity/eb;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 7
    .parameter

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 28
    const v0, 0x7f030033

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/PrivacyNoticesActivity;->b(I)V

    .line 30
    const v0, 0x7f0900b8

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/PrivacyNoticesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/PrivacyNoticesActivity;->a:Landroid/widget/ListView;

    .line 31
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 34
    iget-object v1, p0, Lcom/chase/sig/android/activity/PrivacyNoticesActivity;->a:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 36
    const v0, 0x7f0900b7

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/PrivacyNoticesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x106000c

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 38
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/PrivacyNoticesActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "title"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/chase/sig/android/activity/PrivacyNoticesActivity;->b:Ljava/lang/String;

    .line 40
    iget-object v0, p0, Lcom/chase/sig/android/activity/PrivacyNoticesActivity;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 41
    iget-object v0, p0, Lcom/chase/sig/android/activity/PrivacyNoticesActivity;->b:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/PrivacyNoticesActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 46
    :goto_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/chase/sig/android/activity/PrivacyNoticesActivity;->a:Landroid/widget/ListView;

    invoke-virtual {v0, v6}, Landroid/widget/ListView;->setVisibility(I)V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "title"

    const v3, 0x7f07028d

    invoke-virtual {p0, v3}, Lcom/chase/sig/android/activity/PrivacyNoticesActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "title"

    const v3, 0x7f07028e

    invoke-virtual {p0, v3}, Lcom/chase/sig/android/activity/PrivacyNoticesActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "title"

    const v3, 0x7f07028f

    invoke-virtual {p0, v3}, Lcom/chase/sig/android/activity/PrivacyNoticesActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-array v4, v5, [Ljava/lang/String;

    const-string v0, "title"

    aput-object v0, v4, v6

    new-array v5, v5, [I

    const v0, 0x7f0900bc

    aput v0, v5, v6

    new-instance v0, Landroid/widget/SimpleAdapter;

    const v3, 0x7f030035

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Landroid/widget/SimpleAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V

    iget-object v1, p0, Lcom/chase/sig/android/activity/PrivacyNoticesActivity;->a:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 48
    iget-object v0, p0, Lcom/chase/sig/android/activity/PrivacyNoticesActivity;->a:Landroid/widget/ListView;

    new-instance v1, Lcom/chase/sig/android/activity/fv;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/fv;-><init>(Lcom/chase/sig/android/activity/PrivacyNoticesActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 59
    return-void

    .line 43
    :cond_0
    const v0, 0x7f07028c

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/PrivacyNoticesActivity;->setTitle(I)V

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0
    .parameter

    .prologue
    .line 63
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/eb;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 64
    return-void
.end method
