.class public Lcom/chase/sig/android/activity/PrivateBankingDisclosureDetails;
.super Lcom/chase/sig/android/activity/eb;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/PrivateBankingDisclosureDetails$a;
    }
.end annotation


# instance fields
.field a:Landroid/widget/TextView;

.field private b:Ljava/lang/String;

.field private c:I

.field private d:Landroid/webkit/WebView;

.field private final k:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/chase/sig/android/activity/eb;-><init>()V

    .line 17
    const/4 v0, -0x1

    iput v0, p0, Lcom/chase/sig/android/activity/PrivateBankingDisclosureDetails;->c:I

    .line 23
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "jpm_gen_disc.html"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "jpm_invtacct_disc.html"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "jpm_bnkacct_disc.html"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "jpm_credacct_disc.html"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/chase/sig/android/activity/PrivateBankingDisclosureDetails;->k:[Ljava/lang/String;

    .line 48
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/PrivateBankingDisclosureDetails;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/chase/sig/android/activity/PrivateBankingDisclosureDetails;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 4
    .parameter

    .prologue
    .line 72
    iput-object p1, p0, Lcom/chase/sig/android/activity/PrivateBankingDisclosureDetails;->b:Ljava/lang/String;

    .line 73
    invoke-direct {p0}, Lcom/chase/sig/android/activity/PrivateBankingDisclosureDetails;->d()V

    .line 74
    const v0, 0x7f0900b6

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/PrivateBankingDisclosureDetails;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/PrivateBankingDisclosureDetails;->d:Landroid/webkit/WebView;

    .line 75
    iget-object v0, p0, Lcom/chase/sig/android/activity/PrivateBankingDisclosureDetails;->d:Landroid/webkit/WebView;

    iget-object v1, p0, Lcom/chase/sig/android/activity/PrivateBankingDisclosureDetails;->b:Ljava/lang/String;

    const-string v2, "text/html"

    const-string v3, "utf-8"

    invoke-virtual {v0, v1, v2, v3}, Landroid/webkit/WebView;->loadData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    new-instance v0, Lcom/chase/sig/android/activity/fw;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/activity/fw;-><init>(Lcom/chase/sig/android/activity/PrivateBankingDisclosureDetails;)V

    .line 90
    iget-object v1, p0, Lcom/chase/sig/android/activity/PrivateBankingDisclosureDetails;->d:Landroid/webkit/WebView;

    invoke-virtual {v1, v0}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 91
    invoke-direct {p0}, Lcom/chase/sig/android/activity/PrivateBankingDisclosureDetails;->d()V

    .line 92
    return-void
.end method

.method private d()V
    .locals 5

    .prologue
    .line 95
    const v0, 0x7f0900b5

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/PrivateBankingDisclosureDetails;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/PrivateBankingDisclosureDetails;->a:Landroid/widget/TextView;

    .line 96
    iget-object v1, p0, Lcom/chase/sig/android/activity/PrivateBankingDisclosureDetails;->a:Landroid/widget/TextView;

    const-string v2, "*%s"

    const/4 v0, 0x1

    new-array v3, v0, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget v0, p0, Lcom/chase/sig/android/activity/PrivateBankingDisclosureDetails;->c:I

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 97
    return-void

    .line 96
    :pswitch_0
    const v0, 0x7f070290

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/PrivateBankingDisclosureDetails;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    const v0, 0x7f070291

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/PrivateBankingDisclosureDetails;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    const v0, 0x7f070292

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/PrivateBankingDisclosureDetails;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    const v0, 0x7f070293

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/PrivateBankingDisclosureDetails;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 5
    .parameter

    .prologue
    .line 33
    const v0, 0x7f070289

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/PrivateBankingDisclosureDetails;->setTitle(I)V

    .line 34
    const v0, 0x7f030032

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/PrivateBankingDisclosureDetails;->b(I)V

    .line 36
    const v0, 0x7f0900b5

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/PrivateBankingDisclosureDetails;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/PrivateBankingDisclosureDetails;->a:Landroid/widget/TextView;

    .line 38
    if-eqz p1, :cond_0

    .line 39
    const-string v0, "disclosureData"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/PrivateBankingDisclosureDetails;->b:Ljava/lang/String;

    .line 40
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/PrivateBankingDisclosureDetails;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "disclosure_document_index"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/chase/sig/android/activity/PrivateBankingDisclosureDetails;->c:I

    .line 41
    iget-object v0, p0, Lcom/chase/sig/android/activity/PrivateBankingDisclosureDetails;->b:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/chase/sig/android/activity/PrivateBankingDisclosureDetails;->a(Ljava/lang/String;)V

    .line 46
    :goto_0
    return-void

    .line 43
    :cond_0
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/PrivateBankingDisclosureDetails;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "disclosure_document_index"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/chase/sig/android/activity/PrivateBankingDisclosureDetails;->c:I

    .line 44
    const-class v0, Lcom/chase/sig/android/activity/PrivateBankingDisclosureDetails$a;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/chase/sig/android/activity/PrivateBankingDisclosureDetails;->k:[Ljava/lang/String;

    iget v4, p0, Lcom/chase/sig/android/activity/PrivateBankingDisclosureDetails;->c:I

    aget-object v3, v3, v4

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/PrivateBankingDisclosureDetails;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 115
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/eb;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 116
    const-string v0, "disclosureData"

    iget-object v1, p0, Lcom/chase/sig/android/activity/PrivateBankingDisclosureDetails;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    const-string v0, "disclosure_document_index"

    iget v1, p0, Lcom/chase/sig/android/activity/PrivateBankingDisclosureDetails;->c:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 118
    return-void
.end method
