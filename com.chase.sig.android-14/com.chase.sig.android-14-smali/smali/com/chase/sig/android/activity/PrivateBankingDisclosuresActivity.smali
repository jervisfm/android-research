.class public Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity$a;
    }
.end annotation


# instance fields
.field private a:Landroid/widget/ListView;

.field private b:Ljava/lang/String;

.field private c:Landroid/webkit/WebView;

.field private d:Ljava/lang/String;

.field private k:Landroid/widget/SimpleAdapter;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    .line 33
    const-string v0, ""

    iput-object v0, p0, Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;->b:Ljava/lang/String;

    .line 225
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;)Landroid/widget/ListView;
    .locals 1
    .parameter

    .prologue
    .line 30
    iget-object v0, p0, Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;->a:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 30
    iget-object v0, p0, Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;->b:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 8
    .parameter

    .prologue
    const/4 v2, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 42
    const v0, 0x7f030033

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;->b(I)V

    .line 44
    const v0, 0x7f0900b8

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;->a:Landroid/widget/ListView;

    .line 45
    const-string v0, "footNote"

    invoke-static {p1, v0, v2}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;->b:Ljava/lang/String;

    .line 47
    iget-object v0, p0, Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 48
    const-string v0, ""

    iput-object v0, p0, Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;->b:Ljava/lang/String;

    .line 51
    :cond_0
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "title"

    invoke-static {v0, v1, v2}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;->d:Ljava/lang/String;

    .line 53
    iget-object v0, p0, Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 54
    iget-object v0, p0, Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;->d:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 59
    :goto_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;->a:Landroid/widget/ListView;

    invoke-virtual {v0, v7}, Landroid/widget/ListView;->setVisibility(I)V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "title"

    const v2, 0x7f070290

    invoke-virtual {p0, v2}, Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "title"

    const v2, 0x7f070291

    invoke-virtual {p0, v2}, Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "title"

    const v2, 0x7f070292

    invoke-virtual {p0, v2}, Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "title"

    const v2, 0x7f070293

    invoke-virtual {p0, v2}, Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "title"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "title"

    const v2, 0x7f07028c

    invoke-virtual {p0, v2}, Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "title"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Landroid/webkit/WebView;

    invoke-direct {v0, p0}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;->c:Landroid/webkit/WebView;

    iget-object v0, p0, Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;->c:Landroid/webkit/WebView;

    iget-object v1, p0, Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;->b:Ljava/lang/String;

    const-string v2, "text/html"

    const-string v4, "utf-8"

    invoke-virtual {v0, v1, v2, v4}, Landroid/webkit/WebView;->loadData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;->a:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;->c:Landroid/webkit/WebView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;)V

    new-instance v0, Lcom/chase/sig/android/activity/fy;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/activity/fy;-><init>(Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;)V

    iget-object v1, p0, Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;->c:Landroid/webkit/WebView;

    invoke-virtual {v1, v0}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    new-array v4, v6, [Ljava/lang/String;

    const-string v0, "title"

    aput-object v0, v4, v7

    new-array v5, v6, [I

    const v0, 0x7f0900bc

    aput v0, v5, v7

    new-instance v0, Lcom/chase/sig/android/activity/fz;

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/chase/sig/android/activity/fz;-><init>(Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;Landroid/content/Context;Ljava/util/List;[Ljava/lang/String;[I)V

    iput-object v0, p0, Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;->k:Landroid/widget/SimpleAdapter;

    iget-object v0, p0, Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;->a:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;->k:Landroid/widget/SimpleAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 61
    iget-object v0, p0, Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;->e:Lcom/chase/sig/android/activity/mc;

    const-class v1, Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity$a;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/mc;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/ae;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity$a;

    .line 62
    invoke-virtual {v0}, Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity$a;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v1

    sget-object v2, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-eq v1, v2, :cond_3

    move v1, v6

    .line 64
    :goto_1
    iget-object v2, p0, Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;->b:Ljava/lang/String;

    invoke-static {v2}, Lcom/chase/sig/android/util/s;->l(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    if-nez p1, :cond_4

    if-eqz v1, :cond_4

    .line 67
    new-array v1, v7, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 72
    :cond_1
    :goto_2
    iget-object v0, p0, Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;->a:Landroid/widget/ListView;

    new-instance v1, Lcom/chase/sig/android/activity/fx;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/fx;-><init>(Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 117
    return-void

    .line 56
    :cond_2
    const v0, 0x7f070289

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;->setTitle(I)V

    goto/16 :goto_0

    :cond_3
    move v1, v7

    .line 62
    goto :goto_1

    .line 68
    :cond_4
    if-eqz v1, :cond_1

    .line 69
    iget-object v0, p0, Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;->b:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;->a(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public final a(Ljava/lang/String;)V
    .locals 4
    .parameter

    .prologue
    .line 250
    iput-object p1, p0, Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;->b:Ljava/lang/String;

    .line 251
    iget-object v0, p0, Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;->c:Landroid/webkit/WebView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 252
    iget-object v0, p0, Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;->c:Landroid/webkit/WebView;

    iget-object v1, p0, Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;->b:Ljava/lang/String;

    const-string v2, "text/html"

    const-string v3, "utf-8"

    invoke-virtual {v0, v1, v2, v3}, Landroid/webkit/WebView;->loadData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 221
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ai;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 222
    const-string v0, "footNote"

    iget-object v1, p0, Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    return-void
.end method
