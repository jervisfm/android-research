.class public Lcom/chase/sig/android/activity/QuickDepositCaptureActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$a;


# instance fields
.field a:Landroid/hardware/Camera$ShutterCallback;

.field b:Landroid/hardware/Camera$PictureCallback;

.field c:Landroid/hardware/Camera$PictureCallback;

.field private d:Ljava/lang/String;

.field private k:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    .line 120
    new-instance v0, Lcom/chase/sig/android/activity/gd;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/activity/gd;-><init>(Lcom/chase/sig/android/activity/QuickDepositCaptureActivity;)V

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositCaptureActivity;->a:Landroid/hardware/Camera$ShutterCallback;

    .line 126
    new-instance v0, Lcom/chase/sig/android/activity/ge;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/activity/ge;-><init>(Lcom/chase/sig/android/activity/QuickDepositCaptureActivity;)V

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositCaptureActivity;->b:Landroid/hardware/Camera$PictureCallback;

    .line 133
    new-instance v0, Lcom/chase/sig/android/activity/gf;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/activity/gf;-><init>(Lcom/chase/sig/android/activity/QuickDepositCaptureActivity;)V

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositCaptureActivity;->c:Landroid/hardware/Camera$PictureCallback;

    .line 27
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/QuickDepositCaptureActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/chase/sig/android/activity/QuickDepositCaptureActivity;->d()V

    return-void
.end method

.method static synthetic a([B)[B
    .locals 4
    .parameter

    .prologue
    .line 27
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    const/16 v1, 0x4000

    new-array v1, v1, [B

    iput-object v1, v0, Landroid/graphics/BitmapFactory$Options;->inTempStorage:[B

    sget-object v1, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object v1, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    const/4 v1, 0x0

    array-length v2, p0

    invoke-static {p0, v1, v2, v0}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v3, 0x1e

    invoke-virtual {v0, v2, v3, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    return-object v1
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/QuickDepositCaptureActivity;)Landroid/widget/ImageView;
    .locals 1
    .parameter

    .prologue
    .line 27
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositCaptureActivity;->k:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic c(Lcom/chase/sig/android/activity/QuickDepositCaptureActivity;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 27
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositCaptureActivity;->d:Ljava/lang/String;

    return-object v0
.end method

.method private d()V
    .locals 3

    .prologue
    .line 110
    const v0, 0x7f090034

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositCaptureActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 111
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickDepositCaptureActivity;->d:Ljava/lang/String;

    const-string v2, "qd_check_front_image"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 113
    if-eqz v1, :cond_0

    .line 114
    const v1, 0x7f0701ce

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 118
    :goto_0
    return-void

    .line 116
    :cond_0
    const v1, 0x7f0701cf

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 4
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 40
    new-instance v1, Lcom/chase/sig/android/activity/CapturePreview;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/CapturePreview;-><init>(Landroid/content/Context;)V

    .line 41
    const v0, 0x7f090032

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositCaptureActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 42
    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 44
    const v0, 0x7f090036

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositCaptureActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 45
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 47
    new-instance v2, Lcom/chase/sig/android/activity/ga;

    invoke-direct {v2, p0, v0}, Lcom/chase/sig/android/activity/ga;-><init>(Lcom/chase/sig/android/activity/QuickDepositCaptureActivity;Landroid/widget/Button;)V

    .line 66
    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 68
    const v0, 0x7f09003c

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositCaptureActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositCaptureActivity;->k:Landroid/widget/ImageView;

    .line 69
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositCaptureActivity;->k:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 70
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositCaptureActivity;->k:Landroid/widget/ImageView;

    new-instance v2, Lcom/chase/sig/android/activity/gb;

    invoke-direct {v2, p0, v1}, Lcom/chase/sig/android/activity/gb;-><init>(Lcom/chase/sig/android/activity/QuickDepositCaptureActivity;Lcom/chase/sig/android/activity/CapturePreview;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 93
    return-void
.end method

.method protected final a_()V
    .locals 1

    .prologue
    .line 34
    const v0, 0x7f03006e

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositCaptureActivity;->setContentView(I)V

    .line 35
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 165
    const/16 v0, 0x1b

    if-ne p1, v0, :cond_1

    .line 166
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 167
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositCaptureActivity;->k:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->performClick()Z

    .line 169
    :cond_0
    const/4 v0, 0x1

    .line 171
    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1, p2}, Lcom/chase/sig/android/activity/ai;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onResume()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 99
    invoke-super {p0}, Lcom/chase/sig/android/activity/ai;->onResume()V

    .line 101
    const v0, 0x7f090038

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositCaptureActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 102
    const v0, 0x7f090037

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositCaptureActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 104
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickDepositCaptureActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "qd_image_side"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositCaptureActivity;->d:Ljava/lang/String;

    .line 106
    invoke-direct {p0}, Lcom/chase/sig/android/activity/QuickDepositCaptureActivity;->d()V

    .line 107
    return-void
.end method
