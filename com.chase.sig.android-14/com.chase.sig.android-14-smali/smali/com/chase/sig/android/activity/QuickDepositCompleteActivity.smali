.class public Lcom/chase/sig/android/activity/QuickDepositCompleteActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$a;


# instance fields
.field private a:Lcom/chase/sig/android/domain/QuickDeposit;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/QuickDepositCompleteActivity;)Lcom/chase/sig/android/domain/QuickDeposit;
    .locals 1
    .parameter

    .prologue
    .line 21
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositCompleteActivity;->a:Lcom/chase/sig/android/domain/QuickDeposit;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 6
    .parameter

    .prologue
    const/16 v2, 0x8

    .line 29
    const v0, 0x7f03006f

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositCompleteActivity;->b(I)V

    .line 30
    const v0, 0x7f070074

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositCompleteActivity;->setTitle(I)V

    .line 32
    invoke-static {}, Lcom/chase/sig/android/c;->a()V

    .line 34
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickDepositCompleteActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/domain/QuickDeposit;->a(Landroid/content/Intent;)Lcom/chase/sig/android/domain/QuickDeposit;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositCompleteActivity;->a:Lcom/chase/sig/android/domain/QuickDeposit;

    .line 36
    const v0, 0x7f090190

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositCompleteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 37
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickDepositCompleteActivity;->a:Lcom/chase/sig/android/domain/QuickDeposit;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/QuickDeposit;->g()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 39
    const v0, 0x7f090192

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositCompleteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 40
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickDepositCompleteActivity;->a:Lcom/chase/sig/android/domain/QuickDeposit;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/QuickDeposit;->e()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 42
    const v0, 0x7f09018d

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositCompleteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 43
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickDepositCompleteActivity;->a:Lcom/chase/sig/android/domain/QuickDeposit;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/QuickDeposit;->i()Lcom/chase/sig/android/util/Dollar;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/util/Dollar;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 45
    const v0, 0x7f09018c

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositCompleteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 47
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickDepositCompleteActivity;->a:Lcom/chase/sig/android/domain/QuickDeposit;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/QuickDeposit;->m()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 48
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickDepositCompleteActivity;->a:Lcom/chase/sig/android/domain/QuickDeposit;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/QuickDeposit;->n()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 55
    :goto_0
    const v0, 0x7f09018a

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositCompleteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 56
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickDepositCompleteActivity;->a:Lcom/chase/sig/android/domain/QuickDeposit;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/QuickDeposit;->o()Lcom/chase/sig/android/domain/QuickDepositAccount;

    move-result-object v1

    .line 58
    new-instance v2, Lcom/chase/sig/android/view/a;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "Deposit to"

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/QuickDepositAccount;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/QuickDepositAccount;->d()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v3, v4, v5, v1}, Lcom/chase/sig/android/view/a;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    invoke-virtual {v2}, Lcom/chase/sig/android/view/a;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 63
    const v0, 0x7f09018e

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositCompleteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 64
    invoke-static {}, Lcom/chase/sig/android/util/s;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 66
    const v0, 0x7f090194

    const-class v1, Lcom/chase/sig/android/activity/QuickDepositStartActivity;

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/QuickDepositCompleteActivity;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/a/f;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/activity/a/f;->b()Lcom/chase/sig/android/activity/a/f;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/QuickDepositCompleteActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 69
    const v0, 0x7f090193

    new-instance v1, Lcom/chase/sig/android/activity/gg;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/gg;-><init>(Lcom/chase/sig/android/activity/QuickDepositCompleteActivity;)V

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/QuickDepositCompleteActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 78
    return-void

    .line 50
    :cond_0
    const v1, 0x7f09018b

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/QuickDepositCompleteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 51
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 52
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 82
    invoke-virtual {p0, p1}, Lcom/chase/sig/android/activity/QuickDepositCompleteActivity;->j(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83
    const/4 v0, 0x1

    .line 87
    :goto_0
    return v0

    .line 86
    :cond_0
    const-class v0, Lcom/chase/sig/android/activity/AccountsActivity;

    invoke-static {p1, p0, v0}, Lcom/chase/sig/android/a/a;->a(ILcom/chase/sig/android/activity/eb;Ljava/lang/Class;)V

    .line 87
    invoke-super {p0, p1, p2}, Lcom/chase/sig/android/activity/ai;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method
