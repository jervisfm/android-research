.class public Lcom/chase/sig/android/activity/QuickDepositReceiptActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$a;


# instance fields
.field private a:Lcom/chase/sig/android/domain/QuickDeposit;

.field private b:Lcom/chase/sig/android/domain/IAccount;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 10
    .parameter

    .prologue
    const/16 v9, 0x8

    .line 29
    const v0, 0x7f030070

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositReceiptActivity;->b(I)V

    .line 30
    const v0, 0x7f0701d2

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositReceiptActivity;->setTitle(I)V

    .line 32
    const v0, 0x7f090196

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositReceiptActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 33
    const v1, 0x7f09019a

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/QuickDepositReceiptActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 34
    const v2, 0x7f090198

    invoke-virtual {p0, v2}, Lcom/chase/sig/android/activity/QuickDepositReceiptActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 35
    const v3, 0x7f090197

    invoke-virtual {p0, v3}, Lcom/chase/sig/android/activity/QuickDepositReceiptActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 38
    const v4, 0x7f09019d

    invoke-virtual {p0, v4}, Lcom/chase/sig/android/activity/QuickDepositReceiptActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 39
    const-string v5, "Available balance<sup><small>1</small></sup>"

    invoke-static {v5}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 41
    const v4, 0x7f09019e

    invoke-virtual {p0, v4}, Lcom/chase/sig/android/activity/QuickDepositReceiptActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 42
    const v5, 0x7f0901a0

    invoke-virtual {p0, v5}, Lcom/chase/sig/android/activity/QuickDepositReceiptActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 44
    const v6, 0x7f09019f

    invoke-virtual {p0, v6}, Lcom/chase/sig/android/activity/QuickDepositReceiptActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 45
    const-string v7, "Present balance<sup><small>2</small></sup>"

    invoke-static {v7}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 47
    const v6, 0x7f09019c

    invoke-virtual {p0, v6}, Lcom/chase/sig/android/activity/QuickDepositReceiptActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 49
    if-nez p1, :cond_4

    .line 50
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickDepositReceiptActivity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v7

    if-eqz v7, :cond_0

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickDepositReceiptActivity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v7

    const-string v8, "quick_deposit"

    invoke-virtual {v7, v8}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    if-eqz v7, :cond_0

    .line 53
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickDepositReceiptActivity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v7

    const-string v8, "quick_deposit"

    invoke-virtual {v7, v8}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/chase/sig/android/domain/QuickDeposit;

    iput-object v7, p0, Lcom/chase/sig/android/activity/QuickDepositReceiptActivity;->a:Lcom/chase/sig/android/domain/QuickDeposit;

    .line 59
    :cond_0
    :goto_0
    iget-object v7, p0, Lcom/chase/sig/android/activity/QuickDepositReceiptActivity;->a:Lcom/chase/sig/android/domain/QuickDeposit;

    if-eqz v7, :cond_3

    .line 60
    iget-object v7, p0, Lcom/chase/sig/android/activity/QuickDepositReceiptActivity;->a:Lcom/chase/sig/android/domain/QuickDeposit;

    invoke-virtual {v7}, Lcom/chase/sig/android/domain/QuickDeposit;->o()Lcom/chase/sig/android/domain/QuickDepositAccount;

    move-result-object v7

    .line 62
    if-eqz v7, :cond_3

    .line 63
    invoke-virtual {v7}, Lcom/chase/sig/android/domain/QuickDepositAccount;->d()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 64
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositReceiptActivity;->a:Lcom/chase/sig/android/domain/QuickDeposit;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickDeposit;->i()Lcom/chase/sig/android/util/Dollar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/util/Dollar;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 66
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositReceiptActivity;->a:Lcom/chase/sig/android/domain/QuickDeposit;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickDeposit;->m()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->l(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 67
    invoke-virtual {v2, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 68
    invoke-virtual {v3, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 71
    :cond_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositReceiptActivity;->a:Lcom/chase/sig/android/domain/QuickDeposit;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickDeposit;->o()Lcom/chase/sig/android/domain/QuickDepositAccount;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickDepositAccount;->a()Ljava/lang/String;

    move-result-object v0

    .line 72
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickDepositReceiptActivity;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v1

    iget-object v1, v1, Lcom/chase/sig/android/domain/o;->b:Lcom/chase/sig/android/domain/g;

    invoke-interface {v1, v0}, Lcom/chase/sig/android/domain/g;->a(Ljava/lang/String;)Lcom/chase/sig/android/domain/IAccount;

    move-result-object v0

    .line 74
    if-eqz v0, :cond_2

    .line 75
    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->k()Lcom/chase/sig/android/util/Dollar;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/util/Dollar;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 76
    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->j()Lcom/chase/sig/android/util/Dollar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/util/Dollar;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 79
    :cond_2
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositReceiptActivity;->a:Lcom/chase/sig/android/domain/QuickDeposit;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickDeposit;->n()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 80
    invoke-static {}, Lcom/chase/sig/android/util/s;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 84
    :cond_3
    const v0, 0x7f0901a2

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositReceiptActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 86
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "<sup><small>1</small></sup>"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickDepositReceiptActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0701e0

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 89
    const v0, 0x7f0901a3

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositReceiptActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 90
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "<sup><small>2</small></sup>"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickDepositReceiptActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0701e1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 93
    return-void

    .line 56
    :cond_4
    const-string v7, "deposit"

    invoke-virtual {p1, v7}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/chase/sig/android/domain/QuickDeposit;

    iput-object v7, p0, Lcom/chase/sig/android/activity/QuickDepositReceiptActivity;->a:Lcom/chase/sig/android/domain/QuickDeposit;

    goto/16 :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 97
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ai;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 98
    const-string v0, "deposit"

    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickDepositReceiptActivity;->a:Lcom/chase/sig/android/domain/QuickDeposit;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 99
    const-string v0, "account_deposited_into"

    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickDepositReceiptActivity;->b:Lcom/chase/sig/android/domain/IAccount;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 100
    return-void
.end method
