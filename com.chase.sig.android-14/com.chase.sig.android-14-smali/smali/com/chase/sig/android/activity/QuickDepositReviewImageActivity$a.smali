.class final Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;
.super Landroid/view/View;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field a:Z

.field private b:Landroid/graphics/Bitmap;

.field private c:Landroid/graphics/Paint;

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 272
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 257
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->c:Landroid/graphics/Paint;

    .line 259
    iput v1, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->d:I

    .line 260
    iput v1, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->e:I

    .line 273
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 356
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 357
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->b:Landroid/graphics/Bitmap;

    .line 358
    return-void
.end method

.method public final a(FF)V
    .locals 2
    .parameter
    .parameter

    .prologue
    const/high16 v1, 0x4080

    .line 344
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 345
    iget v0, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->d:I

    int-to-float v0, v0

    add-float/2addr v0, p1

    float-to-int v0, v0

    iput v0, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->d:I

    .line 348
    :cond_0
    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    .line 349
    iget v0, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->e:I

    int-to-float v0, v0

    add-float/2addr v0, p2

    float-to-int v0, v0

    iput v0, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->e:I

    .line 352
    :cond_1
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->invalidate()V

    .line 353
    return-void
.end method

.method public final a(Landroid/graphics/Bitmap;)V
    .locals 1
    .parameter

    .prologue
    .line 361
    iput-object p1, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->b:Landroid/graphics/Bitmap;

    .line 362
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->h:I

    .line 363
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->i:I

    .line 364
    return-void
.end method

.method public final a(Z)V
    .locals 0
    .parameter

    .prologue
    .line 276
    iput-boolean p1, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->a:Z

    .line 277
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->invalidate()V

    .line 278
    return-void
.end method

.method protected final onDraw(Landroid/graphics/Canvas;)V
    .locals 8
    .parameter

    .prologue
    const/4 v7, 0x0

    const/4 v1, 0x0

    .line 289
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->b:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    if-gtz v0, :cond_1

    .line 319
    :cond_0
    :goto_0
    return-void

    .line 293
    :cond_1
    iget-boolean v0, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->a:Z

    if-eqz v0, :cond_2

    .line 295
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->getWidth()I

    move-result v2

    int-to-float v2, v2

    int-to-float v5, v3

    div-float/2addr v2, v5

    int-to-float v0, v0

    int-to-float v5, v4

    div-float/2addr v0, v5

    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    invoke-virtual {v5, v2, v0}, Landroid/graphics/Matrix;->postScale(FF)Z

    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->b:Landroid/graphics/Bitmap;

    const/4 v6, 0x1

    move v2, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v7, v7, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 299
    :cond_2
    iget v0, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->h:I

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->getWidth()I

    move-result v2

    sub-int/2addr v0, v2

    iput v0, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->f:I

    .line 300
    iget v0, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->i:I

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->getHeight()I

    move-result v2

    sub-int/2addr v0, v2

    iput v0, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->g:I

    .line 302
    iget v0, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->f:I

    if-gez v0, :cond_3

    move v0, v1

    :goto_1
    iput v0, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->f:I

    .line 303
    iget v0, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->g:I

    if-gez v0, :cond_4

    move v0, v1

    :goto_2
    iput v0, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->g:I

    .line 305
    iget v0, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->d:I

    iget v2, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->f:I

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->d:I

    .line 306
    iget v0, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->d:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->d:I

    .line 308
    iget v0, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->e:I

    iget v2, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->g:I

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->e:I

    .line 309
    iget v0, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->e:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->e:I

    .line 311
    iget v0, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->h:I

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->getWidth()I

    move-result v2

    if-ge v0, v2, :cond_5

    iget v0, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->h:I

    .line 312
    :goto_3
    iget v2, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->i:I

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->getHeight()I

    move-result v3

    if-ge v2, v3, :cond_6

    iget v2, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->i:I

    .line 313
    :goto_4
    new-instance v3, Landroid/graphics/Rect;

    iget v4, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->d:I

    iget v5, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->e:I

    iget v6, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->d:I

    add-int/2addr v0, v6

    iget v6, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->e:I

    add-int/2addr v2, v6

    invoke-direct {v3, v4, v5, v0, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 315
    new-instance v0, Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v2

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v4

    invoke-direct {v0, v1, v1, v2, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 317
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->b:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 302
    :cond_3
    iget v0, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->f:I

    goto :goto_1

    .line 303
    :cond_4
    iget v0, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->g:I

    goto :goto_2

    .line 311
    :cond_5
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->getWidth()I

    move-result v0

    goto :goto_3

    .line 312
    :cond_6
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->getHeight()I

    move-result v2

    goto :goto_4
.end method
