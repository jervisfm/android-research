.class public Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;
.super Lcom/chase/sig/android/activity/eb;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;
    }
.end annotation


# instance fields
.field a:Landroid/view/GestureDetector$OnGestureListener;

.field private b:Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;

.field private c:Ljava/lang/String;

.field private d:Z

.field private k:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/chase/sig/android/activity/eb;-><init>()V

    .line 40
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;->k:Z

    .line 216
    new-instance v0, Lcom/chase/sig/android/activity/gk;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/activity/gk;-><init>(Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;)V

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;->a:Landroid/view/GestureDetector$OnGestureListener;

    .line 252
    return-void
.end method

.method private static a([B)Landroid/graphics/Bitmap;
    .locals 5
    .parameter

    .prologue
    const/4 v0, 0x0

    const/4 v4, -0x1

    .line 192
    :try_start_0
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 193
    const/4 v2, 0x1

    iput-boolean v2, v1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 194
    const/4 v2, 0x0

    array-length v3, p0

    invoke-static {p0, v2, v3, v1}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 195
    iget-boolean v2, v1, Landroid/graphics/BitmapFactory$Options;->mCancel:Z

    if-nez v2, :cond_0

    iget v2, v1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    if-eq v2, v4, :cond_0

    iget v2, v1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    if-ne v2, v4, :cond_1

    .line 205
    :cond_0
    :goto_0
    return-object v0

    .line 198
    :cond_1
    const/4 v2, 0x0

    iput-boolean v2, v1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 200
    const/4 v2, 0x0

    iput-boolean v2, v1, Landroid/graphics/BitmapFactory$Options;->inDither:Z

    .line 201
    const/4 v2, 0x1

    iput-boolean v2, v1, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    .line 202
    sget-object v2, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object v2, v1, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 203
    const/4 v2, 0x0

    array-length v3, p0

    invoke-static {p0, v2, v3, v1}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 205
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 31
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;)Lcom/chase/sig/android/domain/QuickDeposit;
    .locals 2
    .parameter

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "quick_deposit"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/QuickDeposit;

    return-object v0
.end method

.method static synthetic c(Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;)V
    .locals 2
    .parameter

    .prologue
    .line 31
    iget-boolean v0, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;->d:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;->d:Z

    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;->b:Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;

    iget-boolean v1, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;->d:Z

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->a(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic d(Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;)Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;
    .locals 1
    .parameter

    .prologue
    .line 31
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;->b:Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 5
    .parameter

    .prologue
    const/4 v4, -0x1

    .line 44
    const v0, 0x7f03006e

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;->setContentView(I)V

    .line 46
    const v0, 0x7f09003b

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 48
    const v0, 0x7f090031

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 51
    new-instance v1, Landroid/view/GestureDetector;

    iget-object v2, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;->a:Landroid/view/GestureDetector$OnGestureListener;

    invoke-direct {v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/view/GestureDetector$OnGestureListener;)V

    .line 53
    new-instance v2, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;

    invoke-direct {v2, p0}, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;->b:Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;

    .line 54
    iget-object v2, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;->b:Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->a(Z)V

    .line 56
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v2, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 58
    iget-object v3, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;->b:Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;

    invoke-virtual {v3, v2}, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 60
    iget-object v2, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;->b:Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;

    new-instance v3, Lcom/chase/sig/android/activity/gh;

    invoke-direct {v3, p0, v1}, Lcom/chase/sig/android/activity/gh;-><init>(Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;Landroid/view/GestureDetector;)V

    invoke-virtual {v2, v3}, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 68
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;->b:Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;I)V

    .line 69
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 73
    invoke-super {p0}, Lcom/chase/sig/android/activity/eb;->onDestroy()V

    .line 74
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;->b:Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->a()V

    .line 75
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;->b:Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;

    .line 76
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 370
    invoke-virtual {p0, p1}, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;->j(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 390
    :goto_0
    return v0

    .line 374
    :cond_0
    const/4 v1, 0x0

    .line 375
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 376
    if-eqz v2, :cond_1

    .line 377
    const-string v1, "qd_review_from_capture_image"

    invoke-virtual {v2, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 380
    :cond_1
    if-eqz v1, :cond_2

    const/4 v1, 0x4

    if-ne p1, v1, :cond_2

    .line 382
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/chase/sig/android/activity/QuickDepositCaptureActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 383
    const-string v2, "qd_image_side"

    iget-object v3, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;->c:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 384
    const/high16 v2, 0x4000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 385
    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 390
    :cond_2
    invoke-super {p0, p1, p2}, Lcom/chase/sig/android/activity/eb;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onResume()V
    .locals 9

    .prologue
    const v5, 0x7f09001f

    const/16 v8, 0x10

    const/4 v7, 0x1

    const/16 v4, 0x8

    const/4 v6, 0x0

    .line 169
    invoke-super {p0}, Lcom/chase/sig/android/activity/eb;->onResume()V

    .line 171
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 172
    const-string v1, "qd_review_only_mode"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;->k:Z

    .line 174
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/domain/QuickDeposit;->a(Landroid/content/Intent;)Lcom/chase/sig/android/domain/QuickDeposit;

    move-result-object v2

    .line 175
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "image_data"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    .line 176
    invoke-static {v0}, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;->a([B)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 178
    iget-boolean v0, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;->k:Z

    if-eqz v0, :cond_1

    .line 180
    const v0, 0x7f090034

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0701d1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    const v0, 0x7f090189

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0701ca

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    invoke-virtual {p0, v5}, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setOrientation(I)V

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    const v0, 0x7f090036

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f090038

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f090037

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/QuickDeposit;->o()Lcom/chase/sig/android/domain/QuickDepositAccount;

    move-result-object v4

    const v0, 0x7f090186

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v4}, Lcom/chase/sig/android/domain/QuickDepositAccount;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    if-le v5, v8, :cond_0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v6, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, "..."

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f090187

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    const-string v1, "(%s)"

    new-array v5, v7, [Ljava/lang/Object;

    invoke-virtual {v4}, Lcom/chase/sig/android/domain/QuickDepositAccount;->d()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v5, v6

    invoke-static {v1, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f090188

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/QuickDeposit;->i()Lcom/chase/sig/android/util/Dollar;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/util/Dollar;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 187
    :goto_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;->b:Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity$a;->a(Landroid/graphics/Bitmap;)V

    .line 188
    return-void

    .line 183
    :cond_1
    invoke-virtual {p0, v5}, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f090189

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0701c9

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    const v0, 0x7f090037

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setVisibility(I)V

    new-instance v1, Lcom/chase/sig/android/activity/gi;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/gi;-><init>(Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f090038

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setVisibility(I)V

    new-instance v1, Lcom/chase/sig/android/activity/gj;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/gj;-><init>(Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 185
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "qd_image_side"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;->c:Ljava/lang/String;

    goto :goto_0
.end method
