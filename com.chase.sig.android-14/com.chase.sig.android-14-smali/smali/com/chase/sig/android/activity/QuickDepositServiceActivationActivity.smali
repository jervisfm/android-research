.class public Lcom/chase/sig/android/activity/QuickDepositServiceActivationActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/QuickDepositServiceActivationActivity$b;,
        Lcom/chase/sig/android/activity/QuickDepositServiceActivationActivity$a;
    }
.end annotation


# static fields
.field private static b:Ljava/lang/String;


# instance fields
.field a:Lcom/chase/sig/android/service/LegalAgreementGetDetailsResponse;

.field private c:Z

.field private d:Z

.field private k:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-string v0, "first_time_user"

    sput-object v0, Lcom/chase/sig/android/activity/QuickDepositServiceActivationActivity;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 23
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    .line 27
    iput-boolean v0, p0, Lcom/chase/sig/android/activity/QuickDepositServiceActivationActivity;->c:Z

    .line 28
    iput-boolean v0, p0, Lcom/chase/sig/android/activity/QuickDepositServiceActivationActivity;->d:Z

    .line 29
    const-string v0, "agree_enabled"

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositServiceActivationActivity;->k:Ljava/lang/String;

    .line 125
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/QuickDepositServiceActivationActivity;)Z
    .locals 1
    .parameter

    .prologue
    .line 23
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/chase/sig/android/activity/QuickDepositServiceActivationActivity;->c:Z

    return v0
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/QuickDepositServiceActivationActivity;)Z
    .locals 1
    .parameter

    .prologue
    .line 23
    iget-boolean v0, p0, Lcom/chase/sig/android/activity/QuickDepositServiceActivationActivity;->c:Z

    return v0
.end method

.method static synthetic c(Lcom/chase/sig/android/activity/QuickDepositServiceActivationActivity;)V
    .locals 2
    .parameter

    .prologue
    .line 23
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/chase/sig/android/activity/AccountsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositServiceActivationActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/chase/sig/android/activity/QuickDepositServiceActivationActivity;->b:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 5
    .parameter

    .prologue
    const v4, 0x7f0901a5

    const/4 v3, 0x0

    .line 35
    const v0, 0x7f0701bc

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositServiceActivationActivity;->setTitle(I)V

    .line 36
    const v0, 0x7f030071

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositServiceActivationActivity;->b(I)V

    .line 38
    if-nez p1, :cond_1

    .line 39
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositServiceActivationActivity;->e:Lcom/chase/sig/android/activity/mc;

    const-class v1, Lcom/chase/sig/android/activity/QuickDepositServiceActivationActivity$a;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/mc;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/ae;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/activity/QuickDepositServiceActivationActivity$a;

    .line 41
    invoke-virtual {v0}, Lcom/chase/sig/android/activity/QuickDepositServiceActivationActivity$a;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v1

    sget-object v2, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-eq v1, v2, :cond_0

    .line 42
    new-array v1, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/QuickDepositServiceActivationActivity$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 50
    :cond_0
    :goto_0
    invoke-virtual {p0, v4}, Lcom/chase/sig/android/activity/QuickDepositServiceActivationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-boolean v1, p0, Lcom/chase/sig/android/activity/QuickDepositServiceActivationActivity;->d:Z

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 52
    const v0, 0x7f0901a7

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositServiceActivationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    .line 53
    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/webkit/WebSettings;->setSaveFormData(Z)V

    .line 54
    new-instance v1, Lcom/chase/sig/android/activity/gl;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/gl;-><init>(Lcom/chase/sig/android/activity/QuickDepositServiceActivationActivity;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 83
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/ChaseApplication;

    const-string v3, "documentum"

    invoke-virtual {v1, v3}, Lcom/chase/sig/android/ChaseApplication;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickDepositServiceActivationActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070067

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 85
    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 87
    const v0, 0x7f0901a6

    const-class v1, Lcom/chase/sig/android/activity/AccountsActivity;

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/QuickDepositServiceActivationActivity;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/a/f;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/QuickDepositServiceActivationActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 89
    new-instance v0, Lcom/chase/sig/android/activity/gm;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/activity/gm;-><init>(Lcom/chase/sig/android/activity/QuickDepositServiceActivationActivity;)V

    invoke-virtual {p0, v4, v0}, Lcom/chase/sig/android/activity/QuickDepositServiceActivationActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 99
    return-void

    .line 46
    :cond_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositServiceActivationActivity;->k:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 47
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositServiceActivationActivity;->k:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/chase/sig/android/activity/QuickDepositServiceActivationActivity;->d:Z

    goto :goto_0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 4
    .parameter

    .prologue
    .line 152
    if-nez p1, :cond_0

    .line 153
    new-instance v0, Lcom/chase/sig/android/view/k$a;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/view/k$a;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0701dc

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/k$a;->b(I)Lcom/chase/sig/android/view/k$a;

    move-result-object v1

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/chase/sig/android/view/k$a;->i:Z

    new-instance v2, Lcom/chase/sig/android/activity/gp;

    invoke-direct {v2, p0}, Lcom/chase/sig/android/activity/gp;-><init>(Lcom/chase/sig/android/activity/QuickDepositServiceActivationActivity;)V

    iput-object v2, v1, Lcom/chase/sig/android/view/k$a;->g:Landroid/content/DialogInterface$OnCancelListener;

    const v2, 0x7f0701dd

    new-instance v3, Lcom/chase/sig/android/activity/go;

    invoke-direct {v3, p0}, Lcom/chase/sig/android/activity/go;-><init>(Lcom/chase/sig/android/activity/QuickDepositServiceActivationActivity;)V

    invoke-virtual {v1, v2, v3}, Lcom/chase/sig/android/view/k$a;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    move-result-object v1

    const v2, 0x7f0701de

    new-instance v3, Lcom/chase/sig/android/activity/gn;

    invoke-direct {v3, p0}, Lcom/chase/sig/android/activity/gn;-><init>(Lcom/chase/sig/android/activity/QuickDepositServiceActivationActivity;)V

    invoke-virtual {v1, v2, v3}, Lcom/chase/sig/android/view/k$a;->c(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/k$a;->d(I)Lcom/chase/sig/android/view/k;

    move-result-object v0

    .line 156
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ai;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 103
    invoke-super {p0}, Lcom/chase/sig/android/activity/ai;->onResume()V

    .line 104
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 204
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ai;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 205
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositServiceActivationActivity;->k:Ljava/lang/String;

    const v1, 0x7f0901a5

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/QuickDepositServiceActivationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->isEnabled()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 206
    return-void
.end method
