.class public Lcom/chase/sig/android/activity/QuickDepositSignUpActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$a;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 4
    .parameter

    .prologue
    .line 15
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickDepositSignUpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 16
    const v0, 0x7f0701d5

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositSignUpActivity;->setTitle(I)V

    .line 20
    :goto_0
    const v0, 0x7f030072

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositSignUpActivity;->b(I)V

    .line 22
    const v0, 0x7f0901ab

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositSignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    .line 23
    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/webkit/WebSettings;->setSaveFormData(Z)V

    .line 24
    new-instance v1, Lcom/chase/sig/android/activity/gq;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/gq;-><init>(Lcom/chase/sig/android/activity/QuickDepositSignUpActivity;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 38
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/ChaseApplication;

    const-string v3, "documentum"

    invoke-virtual {v1, v3}, Lcom/chase/sig/android/ChaseApplication;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickDepositSignUpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070066

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 40
    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 42
    const v0, 0x7f0901a9

    const-class v1, Lcom/chase/sig/android/activity/QuickDepositServiceActivationActivity;

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/QuickDepositSignUpActivity;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/a/f;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/QuickDepositSignUpActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 44
    const v0, 0x7f0901aa

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickDepositSignUpActivity;->C()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/QuickDepositSignUpActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 45
    return-void

    .line 18
    :cond_0
    const v0, 0x7f0701bc

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositSignUpActivity;->setTitle(I)V

    goto :goto_0
.end method
