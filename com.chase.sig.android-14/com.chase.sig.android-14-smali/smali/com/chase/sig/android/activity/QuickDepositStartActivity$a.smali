.class public Lcom/chase/sig/android/activity/QuickDepositStartActivity$a;
.super Lcom/chase/sig/android/b;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/QuickDepositStartActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/b",
        "<",
        "Lcom/chase/sig/android/activity/QuickDepositStartActivity;",
        "Lcom/chase/sig/android/domain/QuickDeposit;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/service/quickdeposit/QuickDepositAccountsResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 704
    invoke-direct {p0}, Lcom/chase/sig/android/b;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 704
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->A()Lcom/chase/sig/android/service/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/service/n;->d()Lcom/chase/sig/android/service/quickdeposit/a;

    invoke-static {}, Lcom/chase/sig/android/service/quickdeposit/a;->a()Lcom/chase/sig/android/service/quickdeposit/QuickDepositAccountsResponse;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 2
    .parameter

    .prologue
    .line 704
    check-cast p1, Lcom/chase/sig/android/service/quickdeposit/QuickDepositAccountsResponse;

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;

    const v1, 0x7f0700b3

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->f(I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositAccountsResponse;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositAccountsResponse;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->b(Ljava/util/List;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositAccountsResponse;->a()Ljava/util/List;

    move-result-object v1

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;

    invoke-static {v0, v1}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->b(Lcom/chase/sig/android/activity/QuickDepositStartActivity;Ljava/util/List;)Ljava/util/List;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;

    invoke-static {v0, p1}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->a(Lcom/chase/sig/android/activity/QuickDepositStartActivity;Lcom/chase/sig/android/service/quickdeposit/QuickDepositAccountsResponse;)Lcom/chase/sig/android/service/quickdeposit/QuickDepositAccountsResponse;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;

    invoke-static {v0, v1}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->c(Lcom/chase/sig/android/activity/QuickDepositStartActivity;Ljava/util/List;)V

    goto :goto_0
.end method
