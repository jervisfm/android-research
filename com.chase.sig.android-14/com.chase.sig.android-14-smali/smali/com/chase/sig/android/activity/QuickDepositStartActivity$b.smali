.class public Lcom/chase/sig/android/activity/QuickDepositStartActivity$b;
.super Lcom/chase/sig/android/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/QuickDepositStartActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/d",
        "<",
        "Lcom/chase/sig/android/activity/QuickDepositStartActivity;",
        "Lcom/chase/sig/android/domain/QuickDeposit;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/service/quickdeposit/QuickDepositStartResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Lcom/chase/sig/android/domain/QuickDeposit;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 432
    invoke-direct {p0}, Lcom/chase/sig/android/d;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 432
    check-cast p1, [Lcom/chase/sig/android/domain/QuickDeposit;

    const/4 v0, 0x0

    aget-object v0, p1, v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity$b;->a:Lcom/chase/sig/android/domain/QuickDeposit;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->A()Lcom/chase/sig/android/service/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/service/n;->d()Lcom/chase/sig/android/service/quickdeposit/a;

    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity$b;->a:Lcom/chase/sig/android/domain/QuickDeposit;

    invoke-static {v0}, Lcom/chase/sig/android/service/quickdeposit/a;->a(Lcom/chase/sig/android/domain/QuickDeposit;)Lcom/chase/sig/android/service/quickdeposit/QuickDepositStartResponse;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 4
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 432
    check-cast p1, Lcom/chase/sig/android/service/quickdeposit/QuickDepositStartResponse;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositStartResponse;->e()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositStartResponse;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->c:Lcom/chase/sig/android/activity/mc;

    const-class v3, Lcom/chase/sig/android/activity/QuickDepositStartActivity$c;

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/activity/mc;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/ae;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/activity/QuickDepositStartActivity$c;

    new-array v1, v1, [Lcom/chase/sig/android/domain/QuickDeposit;

    iget-object v3, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity$b;->a:Lcom/chase/sig/android/domain/QuickDeposit;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/QuickDepositStartActivity$c;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositStartResponse;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;

    const v1, 0x7f0901b1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->e(I)Z

    :cond_1
    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositStartResponse;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;

    const v1, 0x7f0901ad

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->e(I)Z

    :cond_2
    return-void

    :cond_3
    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositStartResponse;->f()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositStartResponse;->h()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositStartResponse;->b()Z

    move-result v3

    if-nez v3, :cond_5

    :goto_1
    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->a(Z)V

    :cond_4
    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositStartResponse;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositStartResponse;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->c(Ljava/util/List;)V

    goto :goto_0

    :cond_5
    move v1, v2

    goto :goto_1
.end method
