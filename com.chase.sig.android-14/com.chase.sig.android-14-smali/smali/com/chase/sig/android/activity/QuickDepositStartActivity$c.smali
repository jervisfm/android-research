.class public Lcom/chase/sig/android/activity/QuickDepositStartActivity$c;
.super Lcom/chase/sig/android/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/QuickDepositStartActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/d",
        "<",
        "Lcom/chase/sig/android/activity/QuickDepositStartActivity;",
        "Lcom/chase/sig/android/domain/QuickDeposit;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Lcom/chase/sig/android/domain/QuickDeposit;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 474
    invoke-direct {p0}, Lcom/chase/sig/android/d;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 474
    check-cast p1, [Lcom/chase/sig/android/domain/QuickDeposit;

    const/4 v0, 0x0

    aget-object v0, p1, v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity$c;->a:Lcom/chase/sig/android/domain/QuickDeposit;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->A()Lcom/chase/sig/android/service/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/service/n;->d()Lcom/chase/sig/android/service/quickdeposit/a;

    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity$c;->a:Lcom/chase/sig/android/domain/QuickDeposit;

    invoke-static {v0}, Lcom/chase/sig/android/service/quickdeposit/a;->b(Lcom/chase/sig/android/domain/QuickDeposit;)Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 474
    check-cast p1, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;

    invoke-static {v0, p1}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->a(Lcom/chase/sig/android/activity/QuickDepositStartActivity;Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;)Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;

    move v0, v1

    :goto_0
    if-eqz v0, :cond_a

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->c(Ljava/util/List;)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;->h()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;->f()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_2
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->a(Z)V

    :cond_3
    :goto_2
    move v0, v2

    goto :goto_0

    :cond_4
    const-string v0, "20452"

    invoke-virtual {p1, v0}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;

    iget-object v1, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v1, Lcom/chase/sig/android/activity/QuickDepositStartActivity;

    invoke-virtual {v1}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->d()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->k(I)V

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;

    invoke-static {v0, p1}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->b(Lcom/chase/sig/android/activity/QuickDepositStartActivity;Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;)V

    goto :goto_2

    :cond_5
    const-string v0, "20100"

    invoke-virtual {p1, v0}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    const-string v0, "20057"

    invoke-virtual {p1, v0}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_6
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->a(Z)V

    goto :goto_2

    :cond_7
    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;->c()Z

    move-result v0

    if-nez v0, :cond_8

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;->b()Z

    move-result v0

    if-eqz v0, :cond_9

    :cond_8
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;

    const v1, 0x7f0901b1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->e(I)Z

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;

    const v1, 0x7f0901b4

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->e(I)Z

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;

    const v1, 0x7f0901b7

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->e(I)Z

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->k(Lcom/chase/sig/android/activity/QuickDepositStartActivity;)V

    goto :goto_2

    :cond_9
    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;

    invoke-static {v0, p1}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->b(Lcom/chase/sig/android/activity/QuickDepositStartActivity;Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;)V

    goto :goto_2

    :cond_a
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;

    invoke-static {v0, p1}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->a(Lcom/chase/sig/android/activity/QuickDepositStartActivity;Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;)Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;->g()Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->a(Lcom/chase/sig/android/activity/QuickDepositStartActivity;Ljava/util/List;)V

    goto/16 :goto_1
.end method
