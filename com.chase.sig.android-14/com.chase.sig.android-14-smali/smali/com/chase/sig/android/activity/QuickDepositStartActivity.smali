.class public Lcom/chase/sig/android/activity/QuickDepositStartActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/QuickDepositStartActivity$a;,
        Lcom/chase/sig/android/activity/QuickDepositStartActivity$c;,
        Lcom/chase/sig/android/activity/QuickDepositStartActivity$b;
    }
.end annotation


# instance fields
.field private A:Landroid/view/View$OnClickListener;

.field private B:Landroid/view/View$OnClickListener;

.field private C:Landroid/view/View$OnFocusChangeListener;

.field a:I

.field private b:Z

.field private c:Landroid/widget/SimpleAdapter;

.field private d:Landroid/widget/ListAdapter;

.field private k:Landroid/widget/TextView;

.field private l:Lcom/chase/sig/android/view/AmountView;

.field private m:Landroid/widget/TextView;

.field private n:Landroid/widget/ImageView;

.field private o:Landroid/widget/ImageView;

.field private p:Landroid/widget/Button;

.field private q:Landroid/widget/Button;

.field private r:Lcom/chase/sig/android/view/JPSpinner;

.field private s:Lcom/chase/sig/android/view/JPSpinner;

.field private t:I

.field private u:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/QuickDepositAccount;",
            ">;"
        }
    .end annotation
.end field

.field private v:Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;

.field private w:Lcom/chase/sig/android/service/quickdeposit/QuickDepositAccountsResponse;

.field private x:Lcom/chase/sig/android/domain/QuickDeposit;

.field private y:Landroid/view/View$OnClickListener;

.field private z:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 43
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    .line 73
    const/4 v0, 0x0

    iput v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->t:I

    .line 76
    iput-object v1, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->v:Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;

    .line 77
    iput-object v1, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->w:Lcom/chase/sig/android/service/quickdeposit/QuickDepositAccountsResponse;

    .line 79
    iput-object v1, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->x:Lcom/chase/sig/android/domain/QuickDeposit;

    .line 80
    const/4 v0, -0x1

    iput v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->a:I

    .line 232
    new-instance v0, Lcom/chase/sig/android/activity/gu;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/activity/gu;-><init>(Lcom/chase/sig/android/activity/QuickDepositStartActivity;)V

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->y:Landroid/view/View$OnClickListener;

    .line 249
    new-instance v0, Lcom/chase/sig/android/activity/gv;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/activity/gv;-><init>(Lcom/chase/sig/android/activity/QuickDepositStartActivity;)V

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->z:Landroid/view/View$OnClickListener;

    .line 369
    new-instance v0, Lcom/chase/sig/android/activity/gw;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/activity/gw;-><init>(Lcom/chase/sig/android/activity/QuickDepositStartActivity;)V

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->A:Landroid/view/View$OnClickListener;

    .line 396
    new-instance v0, Lcom/chase/sig/android/activity/gy;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/activity/gy;-><init>(Lcom/chase/sig/android/activity/QuickDepositStartActivity;)V

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->B:Landroid/view/View$OnClickListener;

    .line 606
    new-instance v0, Lcom/chase/sig/android/activity/gz;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/activity/gz;-><init>(Lcom/chase/sig/android/activity/QuickDepositStartActivity;)V

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->C:Landroid/view/View$OnFocusChangeListener;

    .line 704
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/QuickDepositStartActivity;Lcom/chase/sig/android/service/quickdeposit/QuickDepositAccountsResponse;)Lcom/chase/sig/android/service/quickdeposit/QuickDepositAccountsResponse;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 43
    iput-object p1, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->w:Lcom/chase/sig/android/service/quickdeposit/QuickDepositAccountsResponse;

    return-object p1
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/QuickDepositStartActivity;Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;)Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 43
    iput-object p1, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->v:Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;

    return-object p1
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/QuickDepositStartActivity;)Lcom/chase/sig/android/view/JPSpinner;
    .locals 1
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->s:Lcom/chase/sig/android/view/JPSpinner;

    return-object v0
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/QuickDepositStartActivity;Ljava/lang/String;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 43
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "qd_image_side"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "qd_check_front_image"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "image_data"

    iget-object v2, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->x:Lcom/chase/sig/android/domain/QuickDeposit;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/QuickDeposit;->f()[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    :goto_0
    const-string v1, "quick_deposit"

    iget-object v2, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->x:Lcom/chase/sig/android/domain/QuickDeposit;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v1, "qd_review_from_capture_image"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/high16 v1, 0x4000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->startActivity(Landroid/content/Intent;)V

    return-void

    :cond_0
    const-string v1, "image_data"

    iget-object v2, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->x:Lcom/chase/sig/android/domain/QuickDeposit;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/QuickDeposit;->d()[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    goto :goto_0
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/QuickDepositStartActivity;Ljava/util/List;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 43
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "response"

    iget-object v2, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->v:Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v1, "quick_deposit"

    iget-object v2, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->x:Lcom/chase/sig/android/domain/QuickDeposit;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v1, "queued_errors"

    check-cast p1, Ljava/io/Serializable;

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const/16 v1, 0x3e9

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private a(Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;)V
    .locals 3
    .parameter

    .prologue
    .line 222
    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;->m()Lcom/chase/sig/android/util/Dollar;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 223
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->m:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Scanned Amount: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;->m()Lcom/chase/sig/android/util/Dollar;

    move-result-object v2

    invoke-virtual {v2}, Lcom/chase/sig/android/util/Dollar;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 224
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->m:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 225
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->k:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->a(Landroid/widget/TextView;)Z

    .line 230
    :goto_0
    return-void

    .line 227
    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->k:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setError(Ljava/lang/CharSequence;)V

    .line 228
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->m:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private a(Ljava/util/List;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/QuickDepositAccount;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 763
    invoke-static {p0, p1}, Lcom/chase/sig/android/util/a;->b(Landroid/content/Context;Ljava/util/List;)Landroid/widget/SimpleAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->c:Landroid/widget/SimpleAdapter;

    .line 764
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->r:Lcom/chase/sig/android/view/JPSpinner;

    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->c:Landroid/widget/SimpleAdapter;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/JPSpinner;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 766
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->c:Landroid/widget/SimpleAdapter;

    invoke-virtual {v0}, Landroid/widget/SimpleAdapter;->getCount()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 768
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->u:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/QuickDepositAccount;

    .line 770
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickDepositAccount;->e()Lcom/chase/sig/android/domain/QuickDepositAccount$Authorization;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickDepositAccount$Authorization;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 771
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->r:Lcom/chase/sig/android/view/JPSpinner;

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/view/JPSpinner;->setSelection(I)V

    .line 772
    invoke-direct {p0}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->f()V

    .line 775
    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/QuickDepositStartActivity;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 43
    iput-object p1, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->u:Ljava/util/List;

    return-object p1
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/QuickDepositStartActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->f()V

    return-void
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/QuickDepositStartActivity;Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->a(Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;)V

    return-void
.end method

.method private b(Z)Z
    .locals 5
    .parameter

    .prologue
    const v4, 0x7f0901af

    const v3, 0x7f0901ad

    const/4 v1, 0x0

    .line 266
    const/4 v0, 0x1

    .line 268
    iget-object v2, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->r:Lcom/chase/sig/android/view/JPSpinner;

    invoke-virtual {v2}, Lcom/chase/sig/android/view/JPSpinner;->b()Z

    move-result v2

    if-nez v2, :cond_8

    .line 269
    if-eqz p1, :cond_0

    .line 270
    invoke-virtual {p0, v3}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->a(Landroid/widget/TextView;)Z

    :cond_0
    move v0, v1

    .line 278
    :goto_0
    iget-object v2, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->d:Landroid/widget/ListAdapter;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->d:Landroid/widget/ListAdapter;

    invoke-interface {v2}, Landroid/widget/ListAdapter;->getCount()I

    move-result v2

    if-eqz v2, :cond_2

    .line 279
    iget-object v2, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->s:Lcom/chase/sig/android/view/JPSpinner;

    invoke-virtual {v2}, Lcom/chase/sig/android/view/JPSpinner;->b()Z

    move-result v2

    if-nez v2, :cond_9

    .line 280
    if-eqz p1, :cond_1

    .line 281
    invoke-virtual {p0, v4}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->a(Landroid/widget/TextView;)Z

    :cond_1
    move v0, v1

    .line 290
    :cond_2
    :goto_1
    iget-object v2, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->l:Lcom/chase/sig/android/view/AmountView;

    invoke-virtual {v2}, Lcom/chase/sig/android/view/AmountView;->a()Z

    move-result v2

    if-nez v2, :cond_4

    .line 291
    if-eqz p1, :cond_3

    .line 292
    const v0, 0x7f0901b1

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->a(Landroid/widget/TextView;)Z

    :cond_3
    move v0, v1

    .line 298
    :cond_4
    iget-object v2, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->x:Lcom/chase/sig/android/domain/QuickDeposit;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/QuickDeposit;->j()Z

    move-result v2

    if-nez v2, :cond_6

    .line 299
    if-eqz p1, :cond_5

    .line 300
    const v0, 0x7f0901b4

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->a(Landroid/widget/TextView;)Z

    :cond_5
    move v0, v1

    .line 306
    :cond_6
    iget-object v2, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->x:Lcom/chase/sig/android/domain/QuickDeposit;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/QuickDeposit;->k()Z

    move-result v2

    if-nez v2, :cond_a

    .line 307
    if-eqz p1, :cond_7

    .line 308
    const v0, 0x7f0901b7

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->a(Landroid/widget/TextView;)Z

    .line 314
    :cond_7
    :goto_2
    return v1

    .line 275
    :cond_8
    invoke-virtual {p0, v3}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->d(I)V

    goto :goto_0

    .line 286
    :cond_9
    invoke-virtual {p0, v4}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->d(I)V

    goto :goto_1

    :cond_a
    move v1, v0

    goto :goto_2
.end method

.method static synthetic c(Lcom/chase/sig/android/activity/QuickDepositStartActivity;)Lcom/chase/sig/android/domain/QuickDeposit;
    .locals 1
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->x:Lcom/chase/sig/android/domain/QuickDeposit;

    return-object v0
.end method

.method static synthetic c(Lcom/chase/sig/android/activity/QuickDepositStartActivity;Ljava/util/List;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->a(Ljava/util/List;)V

    return-void
.end method

.method static synthetic d(Lcom/chase/sig/android/activity/QuickDepositStartActivity;)Landroid/widget/ImageView;
    .locals 1
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->n:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic e(Lcom/chase/sig/android/activity/QuickDepositStartActivity;)Landroid/widget/ImageView;
    .locals 1
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->o:Landroid/widget/ImageView;

    return-object v0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 618
    const v0, 0x7f0901bb

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 620
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->b(Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 621
    const v1, 0x7f02009d

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 625
    :goto_0
    return-void

    .line 623
    :cond_0
    const v1, 0x7f020012

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method private f()V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 736
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->u:Ljava/util/List;

    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->r:Lcom/chase/sig/android/view/JPSpinner;

    invoke-virtual {v1}, Lcom/chase/sig/android/view/JPSpinner;->getSelectedItemPosition()I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/QuickDepositAccount;

    .line 739
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->u:Ljava/util/List;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickDepositAccount;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v1, v0}, Lcom/chase/sig/android/util/a;->a(Landroid/content/Context;Ljava/util/List;Ljava/lang/String;)Landroid/widget/ArrayAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->d:Landroid/widget/ListAdapter;

    .line 742
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->s:Lcom/chase/sig/android/view/JPSpinner;

    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->d:Landroid/widget/ListAdapter;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/JPSpinner;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 744
    const v0, 0x7f0901af

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 745
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->d:Landroid/widget/ListAdapter;

    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    if-nez v1, :cond_0

    .line 746
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 747
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->s:Lcom/chase/sig/android/view/JPSpinner;

    invoke-virtual {v0, v4}, Lcom/chase/sig/android/view/JPSpinner;->setVisibility(I)V

    .line 760
    :goto_0
    return-void

    .line 750
    :cond_0
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->d:Landroid/widget/ListAdapter;

    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    if-ne v1, v3, :cond_1

    .line 751
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->s:Lcom/chase/sig/android/view/JPSpinner;

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/view/JPSpinner;->setSelection(I)V

    .line 754
    :cond_1
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 755
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->s:Lcom/chase/sig/android/view/JPSpinner;

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/view/JPSpinner;->setVisibility(I)V

    .line 756
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->s:Lcom/chase/sig/android/view/JPSpinner;

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/view/JPSpinner;->setClickable(Z)V

    .line 757
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->s:Lcom/chase/sig/android/view/JPSpinner;

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/view/JPSpinner;->setEnabled(Z)V

    goto :goto_0
.end method

.method static synthetic f(Lcom/chase/sig/android/activity/QuickDepositStartActivity;)Z
    .locals 1
    .parameter

    .prologue
    .line 43
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->b(Z)Z

    move-result v0

    return v0
.end method

.method static synthetic g(Lcom/chase/sig/android/activity/QuickDepositStartActivity;)Lcom/chase/sig/android/view/AmountView;
    .locals 1
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->l:Lcom/chase/sig/android/view/AmountView;

    return-object v0
.end method

.method static synthetic h(Lcom/chase/sig/android/activity/QuickDepositStartActivity;)Lcom/chase/sig/android/view/JPSpinner;
    .locals 1
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->r:Lcom/chase/sig/android/view/JPSpinner;

    return-object v0
.end method

.method static synthetic i(Lcom/chase/sig/android/activity/QuickDepositStartActivity;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->u:Ljava/util/List;

    return-object v0
.end method

.method static synthetic j(Lcom/chase/sig/android/activity/QuickDepositStartActivity;)Landroid/widget/ListAdapter;
    .locals 1
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->d:Landroid/widget/ListAdapter;

    return-object v0
.end method

.method static synthetic k(Lcom/chase/sig/android/activity/QuickDepositStartActivity;)V
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 43
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->n:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->o:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->p:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->q:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    new-instance v0, Lcom/chase/sig/android/domain/QuickDeposit;

    invoke-direct {v0}, Lcom/chase/sig/android/domain/QuickDeposit;-><init>()V

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->x:Lcom/chase/sig/android/domain/QuickDeposit;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "quick_deposit"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic l(Lcom/chase/sig/android/activity/QuickDepositStartActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->e()V

    return-void
.end method

.method static synthetic m(Lcom/chase/sig/android/activity/QuickDepositStartActivity;)Z
    .locals 1
    .parameter

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->b:Z

    return v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 7
    .parameter

    .prologue
    const v6, 0x7f0901b0

    const/16 v5, 0x8

    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 84
    const v0, 0x7f030073

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->b(I)V

    .line 85
    const v0, 0x7f0701bd

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->setTitle(I)V

    .line 87
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 89
    if-eqz v0, :cond_0

    .line 90
    const-string v1, "first_time_user"

    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->b:Z

    .line 93
    :cond_0
    const v0, 0x7f0901bb

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 94
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->B:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 96
    const v0, 0x7f0901ba

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 97
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->A:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 99
    const v0, 0x7f0901ae

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/JPSpinner;

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->r:Lcom/chase/sig/android/view/JPSpinner;

    .line 100
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->r:Lcom/chase/sig/android/view/JPSpinner;

    new-instance v1, Lcom/chase/sig/android/activity/gr;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/gr;-><init>(Lcom/chase/sig/android/activity/QuickDepositStartActivity;)V

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/JPSpinner;->a(Lcom/chase/sig/android/view/x;)V

    .line 112
    invoke-virtual {p0, v6}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/JPSpinner;

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->s:Lcom/chase/sig/android/view/JPSpinner;

    .line 114
    if-nez p1, :cond_1

    .line 115
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->e:Lcom/chase/sig/android/activity/mc;

    const-class v1, Lcom/chase/sig/android/activity/QuickDepositStartActivity$a;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/mc;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/ae;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/activity/QuickDepositStartActivity$a;

    .line 118
    invoke-virtual {v0}, Lcom/chase/sig/android/activity/QuickDepositStartActivity$a;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v1

    sget-object v2, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-eq v1, v2, :cond_1

    .line 119
    new-array v1, v4, [Lcom/chase/sig/android/domain/QuickDeposit;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/QuickDepositStartActivity$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 123
    :cond_1
    const v0, 0x7f0901af

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 124
    invoke-virtual {p0, v6}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 126
    const v0, 0x7f0901b6

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->p:Landroid/widget/Button;

    .line 127
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->p:Landroid/widget/Button;

    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->y:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 129
    const v0, 0x7f0901b9

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->q:Landroid/widget/Button;

    .line 130
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->q:Landroid/widget/Button;

    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->z:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 132
    const v0, 0x7f0901b5

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->n:Landroid/widget/ImageView;

    .line 133
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->n:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->z:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 135
    const v0, 0x7f0901b8

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->o:Landroid/widget/ImageView;

    .line 136
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->o:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->z:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 138
    const v0, 0x7f0901b2

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/AmountView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->l:Lcom/chase/sig/android/view/AmountView;

    .line 140
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->l:Lcom/chase/sig/android/view/AmountView;

    new-instance v1, Lcom/chase/sig/android/util/d;

    invoke-direct {v1}, Lcom/chase/sig/android/util/d;-><init>()V

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/AmountView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 142
    const v0, 0x7f0901b1

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->k:Landroid/widget/TextView;

    .line 143
    const v0, 0x7f0901b3

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->m:Landroid/widget/TextView;

    .line 145
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->l:Lcom/chase/sig/android/view/AmountView;

    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->C:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/AmountView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 147
    if-eqz p1, :cond_7

    .line 148
    const-string v0, "response"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 149
    const-string v0, "response"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->v:Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;

    .line 152
    :cond_2
    const-string v0, "submitCounter"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 153
    const-string v0, "submitCounter"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->t:I

    .line 156
    :cond_3
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->r:Lcom/chase/sig/android/view/JPSpinner;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/JPSpinner;->a()V

    .line 157
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->s:Lcom/chase/sig/android/view/JPSpinner;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/JPSpinner;->a()V

    .line 159
    const-string v0, "qdAccountResponse"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 160
    const-string v0, "qdAccountResponse"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/service/quickdeposit/QuickDepositAccountsResponse;

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->w:Lcom/chase/sig/android/service/quickdeposit/QuickDepositAccountsResponse;

    .line 165
    :cond_4
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->w:Lcom/chase/sig/android/service/quickdeposit/QuickDepositAccountsResponse;

    if-eqz v0, :cond_6

    .line 166
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->w:Lcom/chase/sig/android/service/quickdeposit/QuickDepositAccountsResponse;

    if-nez v0, :cond_8

    const v0, 0x7f0700b3

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->f(I)V

    .line 168
    :goto_0
    const-string v0, "qd_account_spinner_selection"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    if-eq v0, v3, :cond_5

    .line 170
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->r:Lcom/chase/sig/android/view/JPSpinner;

    const-string v1, "qd_account_spinner_selection"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/JPSpinner;->setSelection(I)V

    .line 174
    :cond_5
    const-string v0, "locations_spinner_selection"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    if-eq v0, v3, :cond_6

    .line 176
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->s:Lcom/chase/sig/android/view/JPSpinner;

    const-string v1, "locations_spinner_selection"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/JPSpinner;->setSelection(I)V

    .line 178
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->s:Lcom/chase/sig/android/view/JPSpinner;

    const-string v1, "locations_spinner_selection"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/JPSpinner;->setSelection(I)V

    .line 183
    :cond_6
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->v:Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;

    if-eqz v0, :cond_7

    .line 184
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->v:Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;

    invoke-direct {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->a(Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;)V

    .line 188
    :cond_7
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->n:Landroid/widget/ImageView;

    new-instance v1, Lcom/chase/sig/android/activity/gs;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/gs;-><init>(Lcom/chase/sig/android/activity/QuickDepositStartActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 198
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->o:Landroid/widget/ImageView;

    new-instance v1, Lcom/chase/sig/android/activity/gt;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/gt;-><init>(Lcom/chase/sig/android/activity/QuickDepositStartActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 207
    return-void

    .line 166
    :cond_8
    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositAccountsResponse;->e()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositAccountsResponse;->g()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->b(Ljava/util/List;)V

    goto :goto_0

    :cond_9
    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositAccountsResponse;->a()Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->u:Ljava/util/List;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositAccountsResponse;->a()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->a(Ljava/util/List;)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 4
    .parameter

    .prologue
    const/16 v1, 0x8

    const/4 v3, 0x4

    const/4 v2, 0x0

    .line 584
    if-eqz p1, :cond_0

    .line 585
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->r:Lcom/chase/sig/android/view/JPSpinner;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/JPSpinner;->a()V

    .line 586
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->s:Lcom/chase/sig/android/view/JPSpinner;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/JPSpinner;->a()V

    .line 587
    const v0, 0x7f0901af

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 588
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->s:Lcom/chase/sig/android/view/JPSpinner;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/JPSpinner;->setVisibility(I)V

    .line 589
    const/4 v0, -0x1

    iput v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->a:I

    .line 592
    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->l:Lcom/chase/sig/android/view/AmountView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/AmountView;->setText(Ljava/lang/CharSequence;)V

    .line 594
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->n:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 595
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->o:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 597
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->p:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 598
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->q:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 600
    iput v2, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->t:I

    .line 601
    new-instance v0, Lcom/chase/sig/android/domain/QuickDeposit;

    invoke-direct {v0}, Lcom/chase/sig/android/domain/QuickDeposit;-><init>()V

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->x:Lcom/chase/sig/android/domain/QuickDeposit;

    .line 602
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "quick_deposit"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 603
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->y()V

    .line 604
    return-void
.end method

.method protected final c_()V
    .locals 0

    .prologue
    .line 731
    invoke-super {p0}, Lcom/chase/sig/android/activity/ai;->c_()V

    .line 732
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->finish()V

    .line 733
    return-void
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 632
    iget v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->t:I

    return v0
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1
    .parameter

    .prologue
    .line 667
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ai;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    .line 668
    invoke-direct {p0}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->e()V

    .line 669
    return v0
.end method

.method public final k(I)V
    .locals 0
    .parameter

    .prologue
    .line 628
    iput p1, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->t:I

    .line 629
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 687
    const/16 v0, 0x3e9

    if-ne p1, v0, :cond_0

    const/16 v0, 0xa

    if-ne p2, v0, :cond_0

    .line 688
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->a(Z)V

    .line 690
    :cond_0
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 637
    invoke-virtual {p0, p1}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->j(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 662
    :goto_0
    return v0

    .line 641
    :cond_0
    const/4 v1, 0x4

    if-ne p1, v1, :cond_1

    sget-boolean v1, Lcom/chase/sig/android/c;->a:Z

    if-eqz v1, :cond_1

    .line 644
    new-instance v1, Lcom/chase/sig/android/activity/ha;

    invoke-direct {v1, p0, p1}, Lcom/chase/sig/android/activity/ha;-><init>(Lcom/chase/sig/android/activity/QuickDepositStartActivity;I)V

    invoke-static {p0, v1}, Lcom/chase/sig/android/c;->a(Lcom/chase/sig/android/activity/eb;Lcom/chase/sig/android/c$a;)V

    goto :goto_0

    .line 662
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/chase/sig/android/activity/ai;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0
    .parameter

    .prologue
    .line 545
    invoke-virtual {p0, p1}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->setIntent(Landroid/content/Intent;)V

    .line 546
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v1, 0x0

    .line 334
    invoke-super {p0}, Lcom/chase/sig/android/activity/ai;->onResume()V

    .line 335
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "quick_deposit"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    .line 336
    sget-boolean v0, Lcom/chase/sig/android/c;->a:Z

    if-nez v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/chase/sig/android/activity/AccountsActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v2, 0x400

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-static {v0}, Lcom/chase/sig/android/c;->a(Landroid/content/Intent;)V

    .line 338
    :cond_0
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "quick_deposit"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/QuickDeposit;

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->x:Lcom/chase/sig/android/domain/QuickDeposit;

    .line 345
    :goto_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->x:Lcom/chase/sig/android/domain/QuickDeposit;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickDeposit;->j()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 346
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->n:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->x:Lcom/chase/sig/android/domain/QuickDeposit;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/QuickDeposit;->g()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 347
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->n:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 348
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->p:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 354
    :goto_2
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->x:Lcom/chase/sig/android/domain/QuickDeposit;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickDeposit;->k()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 355
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->o:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->x:Lcom/chase/sig/android/domain/QuickDeposit;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/QuickDeposit;->e()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 356
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->o:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 357
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->q:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 363
    :goto_3
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->k:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setError(Ljava/lang/CharSequence;)V

    .line 364
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->m:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 366
    invoke-direct {p0}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->e()V

    .line 367
    return-void

    :cond_1
    move v0, v1

    .line 335
    goto :goto_0

    .line 342
    :cond_2
    new-instance v0, Lcom/chase/sig/android/domain/QuickDeposit;

    invoke-direct {v0}, Lcom/chase/sig/android/domain/QuickDeposit;-><init>()V

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->x:Lcom/chase/sig/android/domain/QuickDeposit;

    goto :goto_1

    .line 350
    :cond_3
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->n:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 351
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->p:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_2

    .line 359
    :cond_4
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->o:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 360
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->q:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_3
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 674
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ai;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 676
    const-string v0, "qd_account_spinner_selection"

    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->r:Lcom/chase/sig/android/view/JPSpinner;

    invoke-virtual {v1}, Lcom/chase/sig/android/view/JPSpinner;->getSelectedItemPosition()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 678
    const-string v0, "locations_spinner_selection"

    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->s:Lcom/chase/sig/android/view/JPSpinner;

    invoke-virtual {v1}, Lcom/chase/sig/android/view/JPSpinner;->getSelectedItemPosition()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 680
    const-string v0, "qdAccountResponse"

    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->w:Lcom/chase/sig/android/service/quickdeposit/QuickDepositAccountsResponse;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 681
    const-string v0, "response"

    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->v:Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 682
    const-string v0, "submitCounter"

    iget v1, p0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->t:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 683
    return-void
.end method
