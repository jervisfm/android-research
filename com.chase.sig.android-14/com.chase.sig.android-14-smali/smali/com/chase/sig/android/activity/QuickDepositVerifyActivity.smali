.class public Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/QuickDepositVerifyActivity$a;
    }
.end annotation


# instance fields
.field private a:Lcom/chase/sig/android/domain/QuickDeposit;

.field private b:Landroid/widget/EditText;

.field private c:Landroid/widget/EditText;

.field private d:Landroid/widget/TextView;

.field private k:Landroid/widget/TextView;

.field private l:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    .line 196
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;)Lcom/chase/sig/android/domain/QuickDeposit;
    .locals 1
    .parameter

    .prologue
    .line 27
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->a:Lcom/chase/sig/android/domain/QuickDeposit;

    return-object v0
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;[BLjava/lang/String;)V
    .locals 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 27
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/domain/QuickDeposit;->a(Landroid/content/Intent;)Lcom/chase/sig/android/domain/QuickDeposit;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "qd_review_only_mode"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v2, "qd_image_side"

    invoke-virtual {v1, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "quick_deposit"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v0, "image_data"

    invoke-virtual {v1, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;)Landroid/widget/EditText;
    .locals 1
    .parameter

    .prologue
    .line 27
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->b:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic c(Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;)Landroid/widget/EditText;
    .locals 1
    .parameter

    .prologue
    .line 27
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->c:Landroid/widget/EditText;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 12
    .parameter

    .prologue
    const v11, 0x7f0901c5

    const v10, 0x7f0901c4

    const v9, 0x1080016

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 40
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/domain/QuickDeposit;->a(Landroid/content/Intent;)Lcom/chase/sig/android/domain/QuickDeposit;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->a:Lcom/chase/sig/android/domain/QuickDeposit;

    .line 42
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 43
    const-string v1, "response"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;

    .line 44
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->a:Lcom/chase/sig/android/domain/QuickDeposit;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/domain/QuickDeposit;->g(Ljava/lang/String;)V

    .line 45
    const v1, 0x7f030074

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->b(I)V

    .line 46
    const v1, 0x7f0701be

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->setTitle(I)V

    .line 48
    invoke-virtual {p0, v10}, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 49
    iget-object v2, p0, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->a:Lcom/chase/sig/android/domain/QuickDeposit;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/QuickDeposit;->g()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 51
    invoke-virtual {p0, v11}, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 52
    iget-object v2, p0, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->a:Lcom/chase/sig/android/domain/QuickDeposit;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/QuickDeposit;->e()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 54
    const v1, 0x7f0901bd

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 56
    iget-object v2, p0, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->a:Lcom/chase/sig/android/domain/QuickDeposit;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/QuickDeposit;->o()Lcom/chase/sig/android/domain/QuickDepositAccount;

    move-result-object v2

    .line 57
    new-instance v3, Lcom/chase/sig/android/view/a;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "Deposit to"

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/QuickDepositAccount;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/QuickDepositAccount;->d()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v4, v5, v6, v2}, Lcom/chase/sig/android/view/a;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    invoke-virtual {v3}, Lcom/chase/sig/android/view/a;->getView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 63
    const v1, 0x7f0901c0

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->d:Landroid/widget/TextView;

    .line 64
    const v1, 0x7f0901be

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 65
    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    .line 66
    iget-object v2, p0, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->a:Lcom/chase/sig/android/domain/QuickDeposit;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/QuickDeposit;->m()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_6

    .line 67
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->d:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->a:Lcom/chase/sig/android/domain/QuickDeposit;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/QuickDeposit;->n()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 72
    :goto_0
    const v1, 0x7f0901c3

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->k:Landroid/widget/TextView;

    .line 73
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->a:Lcom/chase/sig/android/domain/QuickDeposit;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/QuickDeposit;->i()Lcom/chase/sig/android/util/Dollar;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/util/Dollar;->h()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 74
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->k:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->a:Lcom/chase/sig/android/domain/QuickDeposit;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/QuickDeposit;->i()Lcom/chase/sig/android/util/Dollar;

    move-result-object v2

    invoke-virtual {v2}, Lcom/chase/sig/android/util/Dollar;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 79
    :goto_1
    const v1, 0x7f0901c8

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->b:Landroid/widget/EditText;

    .line 80
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->a:Lcom/chase/sig/android/domain/QuickDeposit;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/QuickDeposit;->h()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 81
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->b:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->a:Lcom/chase/sig/android/domain/QuickDeposit;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/QuickDeposit;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 86
    :goto_2
    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 87
    const v1, 0x7f0901c7

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->e(I)Z

    .line 90
    :cond_0
    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;->k()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 91
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->b:Landroid/widget/EditText;

    invoke-virtual {v1, v9}, Landroid/widget/EditText;->setBackgroundResource(I)V

    .line 92
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->b:Landroid/widget/EditText;

    invoke-virtual {v1, v8}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 93
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->b:Landroid/widget/EditText;

    invoke-virtual {v1, v8}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 100
    :goto_3
    const v1, 0x7f0901cb

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->c:Landroid/widget/EditText;

    .line 101
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->a:Lcom/chase/sig/android/domain/QuickDeposit;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/QuickDeposit;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 102
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->c:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->a:Lcom/chase/sig/android/domain/QuickDeposit;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/QuickDeposit;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 107
    :goto_4
    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;->j()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 108
    const v1, 0x7f0901ca

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->e(I)Z

    .line 111
    :cond_1
    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;->j()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;->d()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 112
    :cond_2
    const v1, 0x7f0901cc

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->l:Landroid/widget/Button;

    .line 115
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->l:Landroid/widget/Button;

    invoke-virtual {v1, v7}, Landroid/widget/Button;->setVisibility(I)V

    .line 117
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->l:Landroid/widget/Button;

    const-class v2, Lcom/chase/sig/android/activity/QuickDepositHelpFindAcctOrRoutingNumber;

    invoke-virtual {p0, v2}, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/a/f;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 123
    :cond_3
    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;->l()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 124
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->c:Landroid/widget/EditText;

    invoke-virtual {v1, v9}, Landroid/widget/EditText;->setBackgroundResource(I)V

    .line 125
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->c:Landroid/widget/EditText;

    invoke-virtual {v1, v8}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 126
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->c:Landroid/widget/EditText;

    invoke-virtual {v1, v8}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 133
    :goto_5
    const v1, 0x7f0901bb

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 134
    new-instance v2, Lcom/chase/sig/android/activity/hb;

    invoke-direct {v2, p0}, Lcom/chase/sig/android/activity/hb;-><init>(Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 144
    const v1, 0x7f0901ba

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 145
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->B()Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 147
    new-instance v1, Lcom/chase/sig/android/activity/hc;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/hc;-><init>(Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;)V

    invoke-virtual {p0, v10, v1}, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 159
    new-instance v1, Lcom/chase/sig/android/activity/hd;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/hd;-><init>(Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;)V

    invoke-virtual {p0, v11, v1}, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 174
    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;->e()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 175
    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->c(Ljava/util/List;)V

    .line 178
    :cond_4
    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;->p()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 179
    const v1, 0x7f070086

    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;->p()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v1, v0, v8}, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->a(ILjava/lang/String;Z)V

    .line 182
    :cond_5
    return-void

    .line 69
    :cond_6
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 76
    :cond_7
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->k:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 83
    :cond_8
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->b:Landroid/widget/EditText;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 95
    :cond_9
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->b:Landroid/widget/EditText;

    invoke-virtual {v1, v7}, Landroid/widget/EditText;->setBackgroundResource(I)V

    .line 96
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->b:Landroid/widget/EditText;

    invoke-virtual {v1, v7}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 97
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->b:Landroid/widget/EditText;

    invoke-virtual {v1, v7}, Landroid/widget/EditText;->setFocusable(Z)V

    goto/16 :goto_3

    .line 104
    :cond_a
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->c:Landroid/widget/EditText;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 128
    :cond_b
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->c:Landroid/widget/EditText;

    invoke-virtual {v1, v7}, Landroid/widget/EditText;->setBackgroundResource(I)V

    .line 129
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->c:Landroid/widget/EditText;

    invoke-virtual {v1, v7}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 130
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickDepositVerifyActivity;->c:Landroid/widget/EditText;

    invoke-virtual {v1, v7}, Landroid/widget/EditText;->setFocusable(Z)V

    goto/16 :goto_5
.end method
