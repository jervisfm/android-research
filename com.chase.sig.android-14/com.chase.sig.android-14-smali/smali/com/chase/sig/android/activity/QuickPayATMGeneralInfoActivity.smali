.class public Lcom/chase/sig/android/activity/QuickPayATMGeneralInfoActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$e;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 3
    .parameter

    .prologue
    .line 14
    const v0, 0x7f03007d

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayATMGeneralInfoActivity;->b(I)V

    .line 16
    const v0, 0x7f0901fb

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayATMGeneralInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 17
    const v1, 0x7f0701fd

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/QuickPayATMGeneralInfoActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    sget-object v2, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 19
    const v0, 0x7f0901fc

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayATMGeneralInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 20
    const v1, 0x7f0701fe

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/QuickPayATMGeneralInfoActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    sget-object v2, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 22
    const v0, 0x7f0901fd

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayATMGeneralInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 23
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPayATMGeneralInfoActivity;->C()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 24
    return-void
.end method
