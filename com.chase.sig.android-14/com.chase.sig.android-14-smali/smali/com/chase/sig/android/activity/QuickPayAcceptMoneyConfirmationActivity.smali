.class public Lcom/chase/sig/android/activity/QuickPayAcceptMoneyConfirmationActivity;
.super Lcom/chase/sig/android/activity/hm;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/chase/sig/android/activity/hm;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 4
    .parameter

    .prologue
    .line 14
    const v0, 0x7f030082

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayAcceptMoneyConfirmationActivity;->b(I)V

    .line 15
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPayAcceptMoneyConfirmationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "quick_pay_transaction"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/QuickPayAcceptMoneyTransaction;

    .line 18
    const v1, 0x7f070228

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/QuickPayAcceptMoneyConfirmationActivity;->setTitle(I)V

    .line 19
    new-instance v2, Lcom/chase/sig/android/view/aa;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPayAcceptMoneyConfirmationActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/ChaseApplication;

    invoke-direct {v2, v3, v0, v1}, Lcom/chase/sig/android/view/aa;-><init>(Landroid/content/Context;Lcom/chase/sig/android/domain/QuickPayAcceptMoneyTransaction;Lcom/chase/sig/android/ChaseApplication;)V

    .line 21
    const v0, 0x7f0901ce

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayAcceptMoneyConfirmationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 22
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 24
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPayAcceptMoneyConfirmationActivity;->d()V

    .line 25
    return-void
.end method
