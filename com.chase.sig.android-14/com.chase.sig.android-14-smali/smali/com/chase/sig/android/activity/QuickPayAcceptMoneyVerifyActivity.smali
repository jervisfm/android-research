.class public Lcom/chase/sig/android/activity/QuickPayAcceptMoneyVerifyActivity;
.super Lcom/chase/sig/android/activity/eb;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$e;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/QuickPayAcceptMoneyVerifyActivity$a;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/chase/sig/android/activity/eb;-><init>()V

    .line 45
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 3
    .parameter

    .prologue
    .line 22
    const v0, 0x7f030075

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayAcceptMoneyVerifyActivity;->b(I)V

    .line 23
    const v0, 0x7f070227

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayAcceptMoneyVerifyActivity;->setTitle(I)V

    .line 24
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPayAcceptMoneyVerifyActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "quick_pay_transaction"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/QuickPayAcceptMoneyTransaction;

    .line 27
    new-instance v2, Lcom/chase/sig/android/view/aa;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/ChaseApplication;

    invoke-direct {v2, p0, v0, v1}, Lcom/chase/sig/android/view/aa;-><init>(Landroid/content/Context;Lcom/chase/sig/android/domain/QuickPayAcceptMoneyTransaction;Lcom/chase/sig/android/ChaseApplication;)V

    .line 29
    const v0, 0x7f0901ce

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayAcceptMoneyVerifyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 31
    const v0, 0x7f0901d0

    new-instance v1, Lcom/chase/sig/android/activity/he;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/he;-><init>(Lcom/chase/sig/android/activity/QuickPayAcceptMoneyVerifyActivity;)V

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/QuickPayAcceptMoneyVerifyActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 42
    const v0, 0x7f0901cf

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPayAcceptMoneyVerifyActivity;->C()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/QuickPayAcceptMoneyVerifyActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 43
    return-void
.end method
