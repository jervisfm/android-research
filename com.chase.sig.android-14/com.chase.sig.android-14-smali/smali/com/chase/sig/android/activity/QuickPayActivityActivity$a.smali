.class public Lcom/chase/sig/android/activity/QuickPayActivityActivity$a;
.super Lcom/chase/sig/android/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/QuickPayActivityActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/d",
        "<",
        "Lcom/chase/sig/android/activity/QuickPayActivityActivity;",
        "Ljava/lang/Object;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 201
    invoke-direct {p0}, Lcom/chase/sig/android/d;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .parameter

    .prologue
    .line 201
    const/4 v0, 0x1

    aget-object v0, p1, v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v0, 0x0

    aget-object v0, p1, v0

    check-cast v0, Lcom/chase/sig/android/domain/QuickPayActivityType;

    iget-object v1, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v1, Lcom/chase/sig/android/activity/QuickPayActivityActivity;

    invoke-virtual {v1}, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->A()Lcom/chase/sig/android/service/n;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/service/n;->j()Lcom/chase/sig/android/service/y;

    invoke-static {v0, v2}, Lcom/chase/sig/android/service/y;->a(Lcom/chase/sig/android/domain/QuickPayActivityType;I)Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->a(Lcom/chase/sig/android/domain/QuickPayActivityType;)V

    return-object v1
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 7
    .parameter

    .prologue
    .line 201
    check-cast p1, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->b(Ljava/util/List;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->k()I

    move-result v1

    invoke-static {v0, v1}, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->b(Lcom/chase/sig/android/activity/QuickPayActivityActivity;I)I

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->j()I

    move-result v1

    invoke-static {v0, v1}, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->c(Lcom/chase/sig/android/activity/QuickPayActivityActivity;I)I

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->a(Lcom/chase/sig/android/activity/QuickPayActivityActivity;Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->l()I

    move-result v1

    invoke-static {v0, v1}, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->d(Lcom/chase/sig/android/activity/QuickPayActivityActivity;I)I

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->j()I

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    invoke-static {v0, v1}, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->a(Lcom/chase/sig/android/activity/QuickPayActivityActivity;Z)Z

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->n()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->b(Lcom/chase/sig/android/activity/QuickPayActivityActivity;Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->c()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->l()I

    move-result v2

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->j()I

    move-result v3

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->k()I

    move-result v5

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->n()Ljava/lang/String;

    move-result-object v6

    invoke-static/range {v0 .. v6}, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->a(Lcom/chase/sig/android/activity/QuickPayActivityActivity;Ljava/util/ArrayList;IILjava/lang/String;ILjava/lang/String;)V

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method
