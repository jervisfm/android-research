.class public Lcom/chase/sig/android/activity/QuickPayActivityActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$e;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/QuickPayActivityActivity$a;
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:I

.field private c:I

.field private d:I

.field private k:I

.field private l:Z

.field private m:Landroid/widget/ListView;

.field private n:Landroid/view/View;

.field private o:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/chase/sig/android/domain/QuickPayActivityItem;",
            ">;"
        }
    .end annotation
.end field

.field private p:Landroid/widget/TextView;

.field private q:Landroid/widget/TextView;

.field private r:Landroid/widget/TextView;

.field private s:Ljava/lang/String;

.field private t:Lcom/chase/sig/android/domain/QuickPayActivityType;

.field private u:Lcom/chase/sig/android/view/JPTabWidget$b;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    .line 69
    new-instance v0, Lcom/chase/sig/android/activity/hf;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/activity/hf;-><init>(Lcom/chase/sig/android/activity/QuickPayActivityActivity;)V

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->u:Lcom/chase/sig/android/view/JPTabWidget$b;

    .line 201
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/QuickPayActivityActivity;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 30
    iput p1, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->k:I

    return p1
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/QuickPayActivityActivity;)Lcom/chase/sig/android/domain/QuickPayActivityType;
    .locals 1
    .parameter

    .prologue
    .line 30
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->t:Lcom/chase/sig/android/domain/QuickPayActivityType;

    return-object v0
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/QuickPayActivityActivity;Lcom/chase/sig/android/domain/QuickPayActivityType;)Lcom/chase/sig/android/domain/QuickPayActivityType;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 30
    iput-object p1, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->t:Lcom/chase/sig/android/domain/QuickPayActivityType;

    return-object p1
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/QuickPayActivityActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 30
    iput-object p1, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->a:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/QuickPayActivityActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 30
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/chase/sig/android/activity/QuickPayViewDetailActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "ACTIVITY_DETAIL_ID"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "ACTIVITY_DETAIL_TYPE"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "ACTIVITY_DETAIL_ORIGIN"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "ACTIVITY_DETAIL_OLD"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "IS_INVOICE"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/QuickPayActivityActivity;Ljava/util/ArrayList;IILjava/lang/String;ILjava/lang/String;)V
    .locals 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 30
    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->a(Ljava/util/ArrayList;IILjava/lang/String;ILjava/lang/String;Z)V

    return-void
.end method

.method private a(Ljava/util/ArrayList;IILjava/lang/String;ILjava/lang/String;Z)V
    .locals 9
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/chase/sig/android/domain/QuickPayActivityItem;",
            ">;II",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 250
    iget-boolean v0, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->l:Z

    if-eqz v0, :cond_1

    .line 251
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->m:Landroid/widget/ListView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 252
    if-eqz p6, :cond_0

    .line 253
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->q:Landroid/widget/TextView;

    invoke-virtual {v0, p6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 255
    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->q:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 321
    :goto_0
    return-void

    .line 259
    :cond_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->m:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 260
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->q:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 261
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->m:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->n:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->removeFooterView(Landroid/view/View;)Z

    .line 262
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->n:Landroid/view/View;

    .line 264
    const/4 v0, 0x1

    if-eq p2, v0, :cond_2

    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->o:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 265
    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->o:Ljava/util/ArrayList;

    .line 266
    iput-object p1, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->o:Ljava/util/ArrayList;

    .line 271
    :cond_3
    :goto_1
    const/4 v0, 0x4

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "date"

    aput-object v1, v4, v0

    const/4 v0, 0x1

    const-string v1, "amount"

    aput-object v1, v4, v0

    const/4 v0, 0x2

    const-string v1, "name"

    aput-object v1, v4, v0

    const/4 v0, 0x3

    const-string v1, "status"

    aput-object v1, v4, v0

    .line 274
    const/4 v0, 0x4

    new-array v5, v0, [I

    fill-array-data v5, :array_0

    .line 278
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 279
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/QuickPayActivityItem;

    .line 280
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 281
    const-string v6, "date"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->m()Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v7

    invoke-virtual {v7}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/chase/sig/android/util/s;->g(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v7

    invoke-static {v7}, Lcom/chase/sig/android/util/s;->c(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v3, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 283
    const-string v6, "amount"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "$"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->n()Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v8

    invoke-virtual {v8}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v3, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 284
    const-string v6, "name"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->o()Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v7

    invoke-virtual {v7}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v3, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 285
    const-string v6, "status"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->p()Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v7

    invoke-virtual {v7}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v3, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 286
    const-string v6, "id"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->q()Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v7

    invoke-virtual {v7}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v3, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 287
    const-string v6, "type"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->r()Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v7

    invoke-virtual {v7}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v3, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 288
    const-string v6, "isinvoicerequest"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->C()Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v7

    if-nez v7, :cond_5

    const-string v0, "false"

    :goto_3
    invoke-interface {v3, v6, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 290
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 267
    :cond_4
    if-nez p7, :cond_3

    .line 268
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->o:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_1

    .line 288
    :cond_5
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->C()Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 293
    :cond_6
    const-string v0, "True"

    invoke-virtual {p4, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 294
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 296
    const v1, 0x7f03007a

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->n:Landroid/view/View;

    .line 297
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->n:Landroid/view/View;

    const v1, 0x7f0901dc

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 298
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " transactions total, "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    mul-int/lit8 v3, p2, 0x19

    sub-int v3, p3, v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " to load..."

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 301
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->m:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->n:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;)V

    .line 303
    :cond_7
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->m:Landroid/widget/ListView;

    new-instance v1, Lcom/chase/sig/android/activity/hg;

    invoke-direct {v1, p0, p5}, Lcom/chase/sig/android/activity/hg;-><init>(Lcom/chase/sig/android/activity/QuickPayActivityActivity;I)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 315
    new-instance v6, Lcom/chase/sig/android/view/af;

    new-instance v0, Landroid/widget/SimpleAdapter;

    const v3, 0x7f03007b

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Landroid/widget/SimpleAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V

    invoke-direct {v6, v0}, Lcom/chase/sig/android/view/af;-><init>(Landroid/widget/ListAdapter;)V

    .line 319
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->m:Landroid/widget/ListView;

    invoke-virtual {v0, v6}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 320
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->m:Landroid/widget/ListView;

    add-int/lit8 v1, p2, -0x1

    mul-int/lit8 v1, v1, 0x19

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    goto/16 :goto_0

    .line 274
    nop

    :array_0
    .array-data 0x4
        0xf3t 0x0t 0x9t 0x7ft
        0xf4t 0x0t 0x9t 0x7ft
        0xddt 0x1t 0x9t 0x7ft
        0xdet 0x1t 0x9t 0x7ft
    .end array-data
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/QuickPayActivityActivity;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 30
    iput-boolean p1, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->l:Z

    return p1
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/QuickPayActivityActivity;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 30
    iput p1, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->c:I

    return p1
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/QuickPayActivityActivity;)Landroid/view/View;
    .locals 1
    .parameter

    .prologue
    .line 30
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->n:Landroid/view/View;

    return-object v0
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/QuickPayActivityActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 30
    iput-object p1, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->s:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/QuickPayActivityActivity;Lcom/chase/sig/android/domain/QuickPayActivityType;)V
    .locals 4
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x1

    .line 30
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->e:Lcom/chase/sig/android/activity/mc;

    const-class v1, Lcom/chase/sig/android/activity/QuickPayActivityActivity$a;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/mc;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/ae;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/activity/QuickPayActivityActivity$a;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/QuickPayActivityActivity$a;->d()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/QuickPayActivityActivity$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/chase/sig/android/activity/QuickPayActivityActivity;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 30
    iput p1, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->b:I

    return p1
.end method

.method static synthetic d(Lcom/chase/sig/android/activity/QuickPayActivityActivity;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 30
    iput p1, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->d:I

    return p1
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 8
    .parameter

    .prologue
    const v3, 0x7f0900f9

    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 106
    const v0, 0x7f030076

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->b(I)V

    .line 107
    const v0, 0x7f0701e2

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->setTitle(I)V

    .line 109
    const v0, 0x7f0901d2

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->p:Landroid/widget/TextView;

    .line 110
    const v0, 0x7f0901d1

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->r:Landroid/widget/TextView;

    .line 111
    const v0, 0x7f0901d4

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->q:Landroid/widget/TextView;

    .line 112
    const v0, 0x7f0901d3

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->m:Landroid/widget/ListView;

    .line 113
    invoke-virtual {p0, v3}, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/JPTabWidget;

    .line 114
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->u:Lcom/chase/sig/android/view/JPTabWidget$b;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/JPTabWidget;->setTabListener(Lcom/chase/sig/android/view/JPTabWidget$b;)V

    .line 116
    invoke-virtual {p0, v3}, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/view/JPTabWidget;

    const v3, 0x7f0701f7

    invoke-virtual {v1, v2, v3}, Lcom/chase/sig/android/view/JPTabWidget;->a(II)V

    const v3, 0x7f0701f8

    invoke-virtual {v1, v7, v3}, Lcom/chase/sig/android/view/JPTabWidget;->a(II)V

    const/4 v3, 0x2

    const v4, 0x7f0701f9

    invoke-virtual {v1, v3, v4}, Lcom/chase/sig/android/view/JPTabWidget;->a(II)V

    const/4 v3, 0x3

    const v4, 0x7f0701fa

    invoke-virtual {v1, v3, v4}, Lcom/chase/sig/android/view/JPTabWidget;->a(II)V

    .line 118
    if-eqz p1, :cond_0

    const-string v1, "bundle_activity"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    if-eqz v1, :cond_0

    move v1, v7

    :goto_0
    if-eqz v1, :cond_1

    .line 120
    const-string v1, "bundle_as_of_header"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 121
    const-string v3, "bundle_activity_type_title"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 123
    const-string v4, "bundle_totalRows"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->b:I

    .line 124
    const-string v4, "bundle_showNext"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->a:Ljava/lang/String;

    .line 125
    const-string v4, "bundle_nextPage"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->c:I

    .line 126
    const-string v4, "bundle_currentPage"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->d:I

    .line 127
    const-string v4, "bundle_selected_tab"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->k:I

    .line 128
    const-string v4, "bundle_empty_message"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->s:Ljava/lang/String;

    .line 130
    iget-object v4, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->p:Landroid/widget/TextView;

    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 131
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->r:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 132
    const-string v1, "bundle_activity"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    iput-object v1, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->o:Ljava/util/ArrayList;

    .line 134
    const-string v1, "activity_type"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/domain/QuickPayActivityType;

    iput-object v1, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->t:Lcom/chase/sig/android/domain/QuickPayActivityType;

    .line 136
    iget v1, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->k:I

    invoke-virtual {v0, v1, v2}, Lcom/chase/sig/android/view/JPTabWidget;->a(IZ)V

    .line 138
    const-string v0, "no_activities"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->l:Z

    .line 140
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->o:Ljava/util/ArrayList;

    iget v2, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->d:I

    iget v3, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->b:I

    iget-object v4, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->a:Ljava/lang/String;

    iget v5, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->c:I

    iget-object v6, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->s:Ljava/lang/String;

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->a(Ljava/util/ArrayList;IILjava/lang/String;ILjava/lang/String;Z)V

    .line 145
    :goto_1
    return-void

    :cond_0
    move v1, v2

    .line 118
    goto :goto_0

    .line 143
    :cond_1
    invoke-virtual {v0, v2, v7}, Lcom/chase/sig/android/view/JPTabWidget;->a(IZ)V

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1
    .parameter

    .prologue
    .line 237
    const v0, 0x7f0901d2

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 238
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 239
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1
    .parameter

    .prologue
    .line 242
    const v0, 0x7f0901d1

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 243
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 244
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 155
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ai;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 157
    const-string v0, "bundle_type_position"

    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->m:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 158
    const-string v0, "bundle_activity"

    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->o:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 159
    const-string v0, "bundle_as_of_header"

    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->p:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    const-string v0, "bundle_activity_type_title"

    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->r:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    const-string v0, "bundle_selected_tab"

    iget v1, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->k:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 163
    const-string v0, "bundle_currentPage"

    iget v1, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->d:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 164
    const-string v0, "bundle_nextPage"

    iget v1, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->c:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 165
    const-string v0, "bundle_totalRows"

    iget v1, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->b:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 166
    const-string v0, "bundle_showNext"

    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    const-string v0, "activity_type"

    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->t:Lcom/chase/sig/android/domain/QuickPayActivityType;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 168
    const-string v0, "no_activities"

    iget-boolean v1, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->l:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 169
    const-string v0, "bundle_empty_message"

    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->s:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    return-void
.end method
