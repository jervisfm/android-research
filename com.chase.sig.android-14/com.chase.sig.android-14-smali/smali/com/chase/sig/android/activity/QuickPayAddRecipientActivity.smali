.class public Lcom/chase/sig/android/activity/QuickPayAddRecipientActivity;
.super Lcom/chase/sig/android/activity/hr;
.source "SourceFile"


# instance fields
.field private k:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/chase/sig/android/activity/hr;-><init>()V

    return-void
.end method

.method private h()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 72
    const/4 v0, 0x5

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickPayAddRecipientActivity;->a:[I

    .line 73
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayAddRecipientActivity;->a:[I

    const v1, 0x7f0901e3

    aput v1, v0, v2

    .line 74
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayAddRecipientActivity;->a:[I

    const v1, 0x7f0901e8

    aput v1, v0, v3

    .line 75
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayAddRecipientActivity;->a:[I

    const v1, 0x7f0901ec

    aput v1, v0, v4

    .line 76
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayAddRecipientActivity;->a:[I

    const v1, 0x7f0901f0

    aput v1, v0, v5

    .line 77
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayAddRecipientActivity;->a:[I

    const v1, 0x7f0901f4

    aput v1, v0, v6

    .line 79
    const/4 v0, 0x5

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickPayAddRecipientActivity;->b:[I

    .line 80
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayAddRecipientActivity;->b:[I

    const v1, 0x7f0901e5

    aput v1, v0, v2

    .line 81
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayAddRecipientActivity;->b:[I

    const v1, 0x7f0901eb

    aput v1, v0, v3

    .line 82
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayAddRecipientActivity;->b:[I

    const v1, 0x7f0901ef

    aput v1, v0, v4

    .line 83
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayAddRecipientActivity;->b:[I

    const v1, 0x7f0901f3

    aput v1, v0, v5

    .line 84
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayAddRecipientActivity;->b:[I

    const v1, 0x7f0901f7

    aput v1, v0, v6

    .line 86
    const/4 v0, 0x5

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickPayAddRecipientActivity;->c:[I

    .line 87
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayAddRecipientActivity;->c:[I

    const v1, 0x7f09020f

    aput v1, v0, v2

    .line 88
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayAddRecipientActivity;->c:[I

    const v1, 0x7f0901e9

    aput v1, v0, v3

    .line 89
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayAddRecipientActivity;->c:[I

    const v1, 0x7f0901ed

    aput v1, v0, v4

    .line 90
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayAddRecipientActivity;->c:[I

    const v1, 0x7f0901f1

    aput v1, v0, v5

    .line 91
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayAddRecipientActivity;->c:[I

    const v1, 0x7f0901f5

    aput v1, v0, v6

    .line 92
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 9
    .parameter

    .prologue
    const v8, 0x7f0901e7

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 23
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPayAddRecipientActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "quick_pay_manage_recipient"

    invoke-static {v0, v1}, Lcom/chase/sig/android/util/e;->b(Landroid/os/Bundle;Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/chase/sig/android/activity/QuickPayAddRecipientActivity;->k:Z

    .line 26
    iget-boolean v0, p0, Lcom/chase/sig/android/activity/QuickPayAddRecipientActivity;->k:Z

    if-eqz v0, :cond_2

    .line 27
    const v0, 0x7f030087

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayAddRecipientActivity;->b(I)V

    .line 32
    :goto_0
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/hr;->a(Landroid/os/Bundle;)V

    .line 34
    const v0, 0x7f070215

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayAddRecipientActivity;->setTitle(I)V

    .line 35
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPayAddRecipientActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "fromContactsActivity"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 38
    const v0, 0x7f0901f9

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayAddRecipientActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 39
    const v1, 0x7f0901f8

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/QuickPayAddRecipientActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 41
    if-eqz v2, :cond_3

    .line 42
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPayAddRecipientActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "recipient"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, Lcom/chase/sig/android/domain/QuickPayRecipient;

    const v3, 0x7f0901e2

    invoke-virtual {p0, v3}, Lcom/chase/sig/android/activity/QuickPayAddRecipientActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/QuickPayRecipient;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/QuickPayRecipient;->f()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    new-array v4, v7, [Landroid/text/InputFilter;

    new-instance v3, Landroid/text/InputFilter$LengthFilter;

    const/16 v5, 0x10

    invoke-direct {v3, v5}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v3, v4, v6

    invoke-virtual {p0, v8}, Lcom/chase/sig/android/activity/QuickPayAddRecipientActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    invoke-virtual {p0, v8}, Lcom/chase/sig/android/activity/QuickPayAddRecipientActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/QuickPayRecipient;->i()Lcom/chase/sig/android/domain/MobilePhoneNumber;

    move-result-object v4

    invoke-virtual {v4}, Lcom/chase/sig/android/domain/MobilePhoneNumber;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v3, p0, Lcom/chase/sig/android/activity/QuickPayAddRecipientActivity;->b:[I

    array-length v3, v3

    if-ne v3, v7, :cond_1

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/QuickPayRecipient;->j()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-le v3, v7, :cond_1

    invoke-direct {p0}, Lcom/chase/sig/android/activity/QuickPayAddRecipientActivity;->h()V

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPayAddRecipientActivity;->e()V

    :cond_1
    move v5, v6

    :goto_1
    invoke-virtual {v2}, Lcom/chase/sig/android/domain/QuickPayRecipient;->j()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v5, v3, :cond_3

    iget-object v3, p0, Lcom/chase/sig/android/activity/QuickPayAddRecipientActivity;->b:[I

    aget v3, v3, v5

    invoke-virtual {p0, v3}, Lcom/chase/sig/android/activity/QuickPayAddRecipientActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/QuickPayRecipient;->j()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/chase/sig/android/domain/Email;

    invoke-virtual {v4}, Lcom/chase/sig/android/domain/Email;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_1

    .line 29
    :cond_2
    const v0, 0x7f03007c

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayAddRecipientActivity;->b(I)V

    goto/16 :goto_0

    .line 45
    :cond_3
    const v2, 0x7f070206

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(I)V

    .line 46
    invoke-virtual {v1, v6}, Landroid/widget/Button;->setVisibility(I)V

    .line 47
    const v0, 0x7f070296

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setText(I)V

    .line 49
    new-instance v0, Lcom/chase/sig/android/activity/hh;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/activity/hh;-><init>(Lcom/chase/sig/android/activity/QuickPayAddRecipientActivity;)V

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 56
    return-void
.end method

.method protected final d()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 60
    iget-boolean v0, p0, Lcom/chase/sig/android/activity/QuickPayAddRecipientActivity;->k:Z

    if-eqz v0, :cond_0

    .line 61
    invoke-direct {p0}, Lcom/chase/sig/android/activity/QuickPayAddRecipientActivity;->h()V

    .line 69
    :goto_0
    return-void

    .line 63
    :cond_0
    new-array v0, v3, [I

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickPayAddRecipientActivity;->a:[I

    .line 64
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayAddRecipientActivity;->a:[I

    const v1, 0x7f0901e3

    aput v1, v0, v2

    .line 66
    new-array v0, v3, [I

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickPayAddRecipientActivity;->b:[I

    .line 67
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayAddRecipientActivity;->b:[I

    const v1, 0x7f0901e5

    aput v1, v0, v2

    goto :goto_0
.end method

.method protected final e()V
    .locals 3

    .prologue
    .line 96
    sget-object v0, Lcom/chase/sig/android/activity/QuickPayAddRecipientActivity;->d:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 97
    sget-object v0, Lcom/chase/sig/android/activity/QuickPayAddRecipientActivity;->d:Ljava/util/HashMap;

    const-string v1, "mobile"

    const v2, 0x7f0901e6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    sget-object v0, Lcom/chase/sig/android/activity/QuickPayAddRecipientActivity;->d:Ljava/util/HashMap;

    const-string v1, "nickname"

    const v2, 0x7f0901e1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    sget-object v0, Lcom/chase/sig/android/activity/QuickPayAddRecipientActivity;->d:Ljava/util/HashMap;

    const-string v1, "email1"

    const v2, 0x7f0901e4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    iget-boolean v0, p0, Lcom/chase/sig/android/activity/QuickPayAddRecipientActivity;->k:Z

    if-eqz v0, :cond_0

    .line 101
    sget-object v0, Lcom/chase/sig/android/activity/QuickPayAddRecipientActivity;->d:Ljava/util/HashMap;

    const-string v1, "email2"

    const v2, 0x7f0901ea

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    sget-object v0, Lcom/chase/sig/android/activity/QuickPayAddRecipientActivity;->d:Ljava/util/HashMap;

    const-string v1, "email3"

    const v2, 0x7f0901ee

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    sget-object v0, Lcom/chase/sig/android/activity/QuickPayAddRecipientActivity;->d:Ljava/util/HashMap;

    const-string v1, "email4"

    const v2, 0x7f0901f2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    sget-object v0, Lcom/chase/sig/android/activity/QuickPayAddRecipientActivity;->d:Ljava/util/HashMap;

    const-string v1, "email5"

    const v2, 0x7f0901f6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    :cond_0
    return-void
.end method

.method public final f()V
    .locals 3

    .prologue
    .line 136
    const v0, 0x7f0901e2

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayAddRecipientActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 138
    const v0, 0x7f0901e7

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayAddRecipientActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 140
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayAddRecipientActivity;->b:[I

    array-length v0, v0

    if-ge v1, v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayAddRecipientActivity;->b:[I

    aget v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayAddRecipientActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    const-string v2, ""

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 140
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 143
    :cond_0
    return-void
.end method
