.class public Lcom/chase/sig/android/activity/QuickPayChooseRecipientActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$e;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/QuickPayChooseRecipientActivity$a;
    }
.end annotation


# instance fields
.field public a:Lcom/chase/sig/android/service/quickpay/QuickPayRecipientListResponse;

.field public b:Landroid/widget/ListView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    .line 57
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/QuickPayChooseRecipientActivity;Lcom/chase/sig/android/domain/QuickPayRecipient;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 22
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "quick_pay_manage_recipient"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/high16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v1, "recipient"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayChooseRecipientActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 4
    .parameter

    .prologue
    .line 37
    const v0, 0x7f03008b

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayChooseRecipientActivity;->b(I)V

    .line 38
    const v0, 0x7f070233

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayChooseRecipientActivity;->setTitle(I)V

    .line 40
    const v0, 0x7f090225

    const-class v1, Lcom/chase/sig/android/activity/QuickPayAddRecipientActivity;

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/QuickPayChooseRecipientActivity;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/a/f;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/activity/a/f;->c()Lcom/chase/sig/android/activity/a/f;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/QuickPayChooseRecipientActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 43
    const v0, 0x7f090226

    const-class v1, Lcom/chase/sig/android/activity/ContactsActivity;

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/QuickPayChooseRecipientActivity;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/a/f;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/activity/a/f;->c()Lcom/chase/sig/android/activity/a/f;

    move-result-object v1

    const-string v2, "quick_pay_manage_recipient"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/chase/sig/android/activity/a/f;->a(Ljava/lang/String;Ljava/io/Serializable;)Lcom/chase/sig/android/activity/a/f;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/QuickPayChooseRecipientActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 48
    const-string v0, "recipients"

    invoke-static {p1, v0}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 49
    const-string v0, "recipients"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/service/quickpay/QuickPayRecipientListResponse;

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickPayChooseRecipientActivity;->a:Lcom/chase/sig/android/service/quickpay/QuickPayRecipientListResponse;

    .line 51
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPayChooseRecipientActivity;->d()V

    .line 55
    :goto_0
    return-void

    .line 53
    :cond_0
    const-class v0, Lcom/chase/sig/android/activity/QuickPayChooseRecipientActivity$a;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/QuickPayChooseRecipientActivity;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final d()V
    .locals 9

    .prologue
    const v1, 0x7f090227

    const/4 v7, 0x3

    const/4 v8, 0x0

    .line 90
    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/QuickPayChooseRecipientActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickPayChooseRecipientActivity;->b:Landroid/widget/ListView;

    .line 92
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayChooseRecipientActivity;->a:Lcom/chase/sig/android/service/quickpay/QuickPayRecipientListResponse;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickpay/QuickPayRecipientListResponse;->a()Ljava/util/ArrayList;

    move-result-object v6

    .line 94
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayChooseRecipientActivity;->a:Lcom/chase/sig/android/service/quickpay/QuickPayRecipientListResponse;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickpay/QuickPayRecipientListResponse;->a()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayChooseRecipientActivity;->a:Lcom/chase/sig/android/service/quickpay/QuickPayRecipientListResponse;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickpay/QuickPayRecipientListResponse;->a()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 95
    :cond_0
    const v0, 0x7f090228

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayChooseRecipientActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 96
    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/QuickPayChooseRecipientActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 139
    :goto_0
    return-void

    .line 100
    :cond_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayChooseRecipientActivity;->b:Landroid/widget/ListView;

    invoke-virtual {v0, v8}, Landroid/widget/ListView;->setVisibility(I)V

    .line 101
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 103
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/QuickPayRecipient;

    .line 104
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 105
    const-string v4, "name"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayRecipient;->a()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    const-string v4, "email"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayRecipient;->d()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    const-string v4, "status"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayRecipient;->l()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 111
    :cond_2
    new-array v4, v7, [Ljava/lang/String;

    const-string v0, "name"

    aput-object v0, v4, v8

    const/4 v0, 0x1

    const-string v1, "email"

    aput-object v1, v4, v0

    const/4 v0, 0x2

    const-string v1, "status"

    aput-object v1, v4, v0

    .line 114
    new-array v5, v7, [I

    fill-array-data v5, :array_0

    .line 118
    new-instance v7, Lcom/chase/sig/android/view/g;

    new-instance v0, Landroid/widget/SimpleAdapter;

    const v3, 0x7f03008c

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Landroid/widget/SimpleAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V

    invoke-direct {v7, v0}, Lcom/chase/sig/android/view/g;-><init>(Landroid/widget/ListAdapter;)V

    .line 122
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayChooseRecipientActivity;->b:Landroid/widget/ListView;

    new-instance v1, Lcom/chase/sig/android/activity/hi;

    invoke-direct {v1, p0, v6}, Lcom/chase/sig/android/activity/hi;-><init>(Lcom/chase/sig/android/activity/QuickPayChooseRecipientActivity;Ljava/util/ArrayList;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 137
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayChooseRecipientActivity;->b:Landroid/widget/ListView;

    invoke-virtual {v0, v8}, Landroid/widget/ListView;->setFooterDividersEnabled(Z)V

    .line 138
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayChooseRecipientActivity;->b:Landroid/widget/ListView;

    invoke-virtual {v0, v7}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0

    .line 114
    nop

    :array_0
    .array-data 0x4
        0x2t 0x2t 0x9t 0x7ft
        0x4t 0x2t 0x9t 0x7ft
        0x29t 0x2t 0x9t 0x7ft
    .end array-data
.end method

.method protected onRestart()V
    .locals 2

    .prologue
    .line 30
    invoke-super {p0}, Lcom/chase/sig/android/activity/ai;->onRestart()V

    .line 31
    const-class v0, Lcom/chase/sig/android/activity/QuickPayChooseRecipientActivity$a;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/QuickPayChooseRecipientActivity;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 32
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 82
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ai;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 84
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayChooseRecipientActivity;->a:Lcom/chase/sig/android/service/quickpay/QuickPayRecipientListResponse;

    if-eqz v0, :cond_0

    .line 85
    const-string v0, "recipients"

    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickPayChooseRecipientActivity;->a:Lcom/chase/sig/android/service/quickpay/QuickPayRecipientListResponse;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 87
    :cond_0
    return-void
.end method
