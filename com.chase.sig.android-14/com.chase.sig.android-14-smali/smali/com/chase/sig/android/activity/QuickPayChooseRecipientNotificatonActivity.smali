.class public Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$e;


# instance fields
.field private a:Landroid/view/LayoutInflater;

.field private b:Lcom/chase/sig/android/domain/QuickPayRecipient;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 23
    iput-object p1, p0, Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;->d:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Landroid/widget/LinearLayout;I)V
    .locals 4
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 23
    move v1, v2

    :goto_0
    if-ge v1, p1, :cond_0

    invoke-virtual {p0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    const v3, 0x7f090181

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 23
    iput-object p1, p0, Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;->c:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic c(Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;)Lcom/chase/sig/android/domain/QuickPayRecipient;
    .locals 1
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;->b:Lcom/chase/sig/android/domain/QuickPayRecipient;

    return-object v0
.end method

.method static synthetic d(Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;)Z
    .locals 3
    .parameter

    .prologue
    .line 23
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "isRequestForMoney"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 13
    .parameter

    .prologue
    const v12, 0x7f09017f

    const v11, 0x7f03006c

    const/4 v7, 0x0

    const v10, 0x7f09022d

    const/16 v9, 0x8

    .line 36
    const v0, 0x7f03008d

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;->b(I)V

    .line 37
    const v0, 0x7f070229

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;->setTitle(I)V

    .line 38
    const-string v0, "layout_inflater"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;->a:Landroid/view/LayoutInflater;

    .line 39
    if-nez p1, :cond_1

    .line 40
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "recipient"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/QuickPayRecipient;

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;->b:Lcom/chase/sig/android/domain/QuickPayRecipient;

    .line 41
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;->b:Lcom/chase/sig/android/domain/QuickPayRecipient;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayRecipient;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;->c:Ljava/lang/String;

    .line 42
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;->b:Lcom/chase/sig/android/domain/QuickPayRecipient;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayRecipient;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;->d:Ljava/lang/String;

    .line 48
    :goto_0
    const v0, 0x7f0901e2

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;->b:Lcom/chase/sig/android/domain/QuickPayRecipient;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/QuickPayRecipient;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 49
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;->b:Lcom/chase/sig/android/domain/QuickPayRecipient;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayRecipient;->j()Ljava/util/List;

    move-result-object v3

    invoke-virtual {p0, v10}, Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    invoke-virtual {p0, v10}, Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    move v6, v7

    :goto_1
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v6, v0, :cond_3

    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/chase/sig/android/domain/Email;

    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;->a:Landroid/view/LayoutInflater;

    const/4 v1, 0x0

    invoke-virtual {v0, v11, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v8

    const v0, 0x7f090181

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/CheckBox;

    const v0, 0x7f090180

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v5}, Lcom/chase/sig/android/domain/Email;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v5}, Lcom/chase/sig/android/domain/Email;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "Default"

    move-object v1, v0

    :goto_2
    invoke-virtual {v8, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;->d:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/common/a/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5}, Lcom/chase/sig/android/domain/Email;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v4, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    new-instance v0, Lcom/chase/sig/android/activity/hk;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/chase/sig/android/activity/hk;-><init>(Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;Landroid/widget/LinearLayout;Ljava/util/List;Landroid/widget/CheckBox;Lcom/chase/sig/android/domain/Email;)V

    invoke-virtual {v4, v0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    add-int/lit8 v0, v6, 0x1

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    const v0, 0x7f090182

    invoke-virtual {v8, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    invoke-virtual {p0, v10}, Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_1

    .line 44
    :cond_1
    const-string v0, "recipient"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/QuickPayRecipient;

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;->b:Lcom/chase/sig/android/domain/QuickPayRecipient;

    .line 45
    const-string v0, "savedMobileNumber"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;->c:Ljava/lang/String;

    .line 46
    const-string v0, "savedEmail"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;->d:Ljava/lang/String;

    goto/16 :goto_0

    .line 49
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Email "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v1, v6, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_2

    .line 50
    :cond_3
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;->b:Lcom/chase/sig/android/domain/QuickPayRecipient;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayRecipient;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;->b:Lcom/chase/sig/android/domain/QuickPayRecipient;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayRecipient;->f()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;->a:Landroid/view/LayoutInflater;

    const/4 v2, 0x0

    invoke-virtual {v0, v11, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const-string v3, "Mobile"

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f090180

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f090182

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f090181

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    new-instance v3, Lcom/chase/sig/android/activity/hl;

    invoke-direct {v3, p0, v1}, Lcom/chase/sig/android/activity/hl;-><init>(Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;->c:Ljava/lang/String;

    if-eqz v1, :cond_4

    const/4 v7, 0x1

    :cond_4
    invoke-virtual {v0, v7}, Landroid/widget/CheckBox;->setChecked(Z)V

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;->n()Landroid/view/ViewGroup;

    move-result-object v0

    const v1, 0x7f09022b

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 52
    :goto_3
    const v0, 0x7f09022e

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 53
    new-instance v1, Lcom/chase/sig/android/activity/hj;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/hj;-><init>(Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 90
    return-void

    .line 50
    :cond_5
    const v0, 0x7f09022a

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f070241

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    const v0, 0x7f09022c

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f09022b

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 0
    .parameter

    .prologue
    .line 106
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 181
    const-string v0, "recipient"

    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;->b:Lcom/chase/sig/android/domain/QuickPayRecipient;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 182
    const-string v0, "savedEmail"

    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 183
    const-string v0, "savedMobileNumber"

    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 184
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ai;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 185
    return-void
.end method
