.class public Lcom/chase/sig/android/activity/QuickPayDeclineConfirmationActivity;
.super Lcom/chase/sig/android/activity/hm;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/chase/sig/android/activity/hm;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 5
    .parameter

    .prologue
    .line 15
    const v0, 0x7f030082

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayDeclineConfirmationActivity;->b(I)V

    .line 16
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPayDeclineConfirmationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "quick_pay_transaction"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/QuickPayDeclineTransaction;

    .line 20
    sget-object v1, Lcom/chase/sig/android/domain/QuickPayActivityType;->d:Lcom/chase/sig/android/domain/QuickPayActivityType;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayDeclineTransaction;->I()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/domain/QuickPayActivityType;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 21
    const v1, 0x7f07021e

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/QuickPayDeclineConfirmationActivity;->setTitle(I)V

    .line 26
    :cond_0
    :goto_0
    new-instance v2, Lcom/chase/sig/android/view/aa;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPayDeclineConfirmationActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/ChaseApplication;

    invoke-direct {v2, v3, v0, v4, v1}, Lcom/chase/sig/android/view/aa;-><init>(Landroid/content/Context;Lcom/chase/sig/android/domain/QuickPayDeclineTransaction;ZLcom/chase/sig/android/ChaseApplication;)V

    .line 28
    const v0, 0x7f0901ce

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayDeclineConfirmationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 29
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 30
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPayDeclineConfirmationActivity;->d()V

    .line 31
    return-void

    .line 22
    :cond_1
    sget-object v1, Lcom/chase/sig/android/domain/QuickPayActivityType;->a:Lcom/chase/sig/android/domain/QuickPayActivityType;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayDeclineTransaction;->I()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/domain/QuickPayActivityType;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 23
    const v1, 0x7f07021f

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/QuickPayDeclineConfirmationActivity;->setTitle(I)V

    goto :goto_0
.end method
