.class public Lcom/chase/sig/android/activity/QuickPayDeclineVerifyActivity$a;
.super Lcom/chase/sig/android/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/QuickPayDeclineVerifyActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/d",
        "<",
        "Lcom/chase/sig/android/activity/QuickPayDeclineVerifyActivity;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/service/quickpay/QuickPayDeclineResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/chase/sig/android/d;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .parameter

    .prologue
    .line 52
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickPayDeclineVerifyActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/QuickPayDeclineVerifyActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "quick_pay_transaction"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/QuickPayDeclineTransaction;

    iget-object v1, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v1, Lcom/chase/sig/android/activity/QuickPayDeclineVerifyActivity;

    const v2, 0x7f09020b

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/activity/QuickPayDeclineVerifyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/QuickPayDeclineTransaction;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v1, Lcom/chase/sig/android/activity/QuickPayDeclineVerifyActivity;

    invoke-virtual {v1}, Lcom/chase/sig/android/activity/QuickPayDeclineVerifyActivity;->A()Lcom/chase/sig/android/service/n;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/service/n;->j()Lcom/chase/sig/android/service/y;

    invoke-static {v0}, Lcom/chase/sig/android/service/y;->a(Lcom/chase/sig/android/domain/QuickPayDeclineTransaction;)Lcom/chase/sig/android/service/quickpay/QuickPayDeclineResponse;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 4
    .parameter

    .prologue
    .line 52
    check-cast p1, Lcom/chase/sig/android/service/quickpay/QuickPayDeclineResponse;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPayDeclineResponse;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickPayDeclineVerifyActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPayDeclineResponse;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/QuickPayDeclineVerifyActivity;->c(Ljava/util/List;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickPayDeclineVerifyActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/QuickPayDeclineVerifyActivity;->A()Lcom/chase/sig/android/service/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/service/n;->j()Lcom/chase/sig/android/service/y;

    const/4 v0, 0x1

    invoke-static {v0}, Lcom/chase/sig/android/service/y;->c(I)Lcom/chase/sig/android/service/quickpay/TodoListResponse;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->e()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickPayDeclineVerifyActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/QuickPayDeclineVerifyActivity;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iput-object v1, v0, Lcom/chase/sig/android/domain/o;->c:Lcom/chase/sig/android/service/quickpay/TodoListResponse;

    :cond_1
    new-instance v2, Landroid/content/Intent;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    const-class v1, Lcom/chase/sig/android/activity/QuickPayDeclineConfirmationActivity;

    invoke-direct {v2, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v0, "decline_details"

    invoke-virtual {v2, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickPayDeclineVerifyActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/QuickPayDeclineVerifyActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "quick_pay_transaction"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/QuickPayDeclineTransaction;

    iget-object v1, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v1, Lcom/chase/sig/android/activity/QuickPayDeclineVerifyActivity;

    const v3, 0x7f09020b

    invoke-virtual {v1, v3}, Lcom/chase/sig/android/activity/QuickPayDeclineVerifyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/QuickPayDeclineTransaction;->a(Ljava/lang/String;)V

    const-string v1, "quick_pay_transaction"

    invoke-virtual {v2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickPayDeclineVerifyActivity;

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/activity/QuickPayDeclineVerifyActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
