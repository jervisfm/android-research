.class public Lcom/chase/sig/android/activity/QuickPayDeclineVerifyActivity;
.super Lcom/chase/sig/android/activity/eb;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$e;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/QuickPayDeclineVerifyActivity$a;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/chase/sig/android/activity/eb;-><init>()V

    .line 52
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 4
    .parameter

    .prologue
    .line 23
    const v0, 0x7f030084

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayDeclineVerifyActivity;->b(I)V

    .line 25
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPayDeclineVerifyActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "quick_pay_transaction"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/QuickPayDeclineTransaction;

    .line 28
    sget-object v1, Lcom/chase/sig/android/domain/QuickPayActivityType;->d:Lcom/chase/sig/android/domain/QuickPayActivityType;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayDeclineTransaction;->I()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/domain/QuickPayActivityType;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 29
    const v1, 0x7f070226

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/QuickPayDeclineVerifyActivity;->setTitle(I)V

    .line 34
    :cond_0
    :goto_0
    new-instance v2, Lcom/chase/sig/android/view/aa;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/ChaseApplication;

    invoke-direct {v2, p0, v0, v3, v1}, Lcom/chase/sig/android/view/aa;-><init>(Landroid/content/Context;Lcom/chase/sig/android/domain/QuickPayDeclineTransaction;ZLcom/chase/sig/android/ChaseApplication;)V

    .line 36
    const v0, 0x7f0901ce

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayDeclineVerifyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 37
    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 39
    const v0, 0x7f09020d

    new-instance v1, Lcom/chase/sig/android/activity/hn;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/hn;-><init>(Lcom/chase/sig/android/activity/QuickPayDeclineVerifyActivity;)V

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/QuickPayDeclineVerifyActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 49
    const v0, 0x7f09020c

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPayDeclineVerifyActivity;->C()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/QuickPayDeclineVerifyActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 50
    return-void

    .line 30
    :cond_1
    sget-object v1, Lcom/chase/sig/android/domain/QuickPayActivityType;->a:Lcom/chase/sig/android/domain/QuickPayActivityType;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayDeclineTransaction;->I()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/domain/QuickPayActivityType;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 31
    const v1, 0x7f070231

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/QuickPayDeclineVerifyActivity;->setTitle(I)V

    goto :goto_0
.end method
