.class public Lcom/chase/sig/android/activity/QuickPayEditRecipientActivity$a;
.super Lcom/chase/sig/android/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/QuickPayEditRecipientActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/d",
        "<",
        "Lcom/chase/sig/android/activity/QuickPayEditRecipientActivity;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/service/quickpay/QuickPayDeleteRecipientResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 145
    invoke-direct {p0}, Lcom/chase/sig/android/d;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 145
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickPayEditRecipientActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/QuickPayEditRecipientActivity;->A()Lcom/chase/sig/android/service/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/service/n;->k()Lcom/chase/sig/android/service/x;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickPayEditRecipientActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/QuickPayEditRecipientActivity;->g()Lcom/chase/sig/android/domain/QuickPayRecipient;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/service/x;->b(Lcom/chase/sig/android/domain/QuickPayRecipient;)Lcom/chase/sig/android/service/quickpay/QuickPayDeleteRecipientResponse;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 2
    .parameter

    .prologue
    .line 145
    check-cast p1, Lcom/chase/sig/android/service/quickpay/QuickPayDeleteRecipientResponse;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPayDeleteRecipientResponse;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickPayEditRecipientActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPayDeleteRecipientResponse;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/QuickPayEditRecipientActivity;->c(Ljava/util/List;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickPayEditRecipientActivity;

    iget-object v1, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v1, Lcom/chase/sig/android/activity/QuickPayEditRecipientActivity;

    invoke-static {v1}, Lcom/chase/sig/android/activity/QuickPayEditRecipientActivity;->a(Lcom/chase/sig/android/activity/QuickPayEditRecipientActivity;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/QuickPayEditRecipientActivity;->showDialog(I)V

    goto :goto_0
.end method
