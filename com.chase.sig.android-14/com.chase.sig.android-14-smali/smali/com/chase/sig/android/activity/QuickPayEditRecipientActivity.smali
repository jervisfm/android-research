.class public Lcom/chase/sig/android/activity/QuickPayEditRecipientActivity;
.super Lcom/chase/sig/android/activity/hr;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/QuickPayEditRecipientActivity$a;
    }
.end annotation


# instance fields
.field private k:I

.field private l:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/chase/sig/android/activity/hr;-><init>()V

    .line 27
    const/16 v0, 0x65

    iput v0, p0, Lcom/chase/sig/android/activity/QuickPayEditRecipientActivity;->k:I

    .line 28
    const/16 v0, 0x66

    iput v0, p0, Lcom/chase/sig/android/activity/QuickPayEditRecipientActivity;->l:I

    .line 145
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/QuickPayEditRecipientActivity;)I
    .locals 1
    .parameter

    .prologue
    .line 25
    iget v0, p0, Lcom/chase/sig/android/activity/QuickPayEditRecipientActivity;->l:I

    return v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 5
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 32
    const v0, 0x7f030087

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayEditRecipientActivity;->b(I)V

    .line 33
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/hr;->a(Landroid/os/Bundle;)V

    .line 35
    const v0, 0x7f070213

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayEditRecipientActivity;->setTitle(I)V

    .line 37
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPayEditRecipientActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "recipient"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/QuickPayRecipient;

    .line 40
    const v1, 0x7f0901e2

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/QuickPayEditRecipientActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayRecipient;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayRecipient;->f()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const v1, 0x7f0901e7

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/QuickPayEditRecipientActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayRecipient;->i()Lcom/chase/sig/android/domain/MobilePhoneNumber;

    move-result-object v2

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/MobilePhoneNumber;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    move v3, v4

    :goto_0
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayRecipient;->j()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v3, v1, :cond_1

    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickPayEditRecipientActivity;->b:[I

    aget v1, v1, v3

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/QuickPayEditRecipientActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayRecipient;->j()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/chase/sig/android/domain/Email;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/Email;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    :cond_1
    const v0, 0x7f0901df

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayEditRecipientActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    iget v1, p0, Lcom/chase/sig/android/activity/QuickPayEditRecipientActivity;->k:I

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/QuickPayEditRecipientActivity;->i(I)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 41
    return-void
.end method

.method protected final d()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 64
    const/4 v0, 0x5

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickPayEditRecipientActivity;->a:[I

    .line 65
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayEditRecipientActivity;->a:[I

    const v1, 0x7f0901e3

    aput v1, v0, v2

    .line 66
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayEditRecipientActivity;->a:[I

    const v1, 0x7f0901e8

    aput v1, v0, v3

    .line 67
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayEditRecipientActivity;->a:[I

    const v1, 0x7f0901ec

    aput v1, v0, v4

    .line 68
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayEditRecipientActivity;->a:[I

    const v1, 0x7f0901f0

    aput v1, v0, v5

    .line 69
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayEditRecipientActivity;->a:[I

    const v1, 0x7f0901f4

    aput v1, v0, v6

    .line 71
    const/4 v0, 0x5

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickPayEditRecipientActivity;->b:[I

    .line 72
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayEditRecipientActivity;->b:[I

    const v1, 0x7f0901e5

    aput v1, v0, v2

    .line 73
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayEditRecipientActivity;->b:[I

    const v1, 0x7f0901eb

    aput v1, v0, v3

    .line 74
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayEditRecipientActivity;->b:[I

    const v1, 0x7f0901ef

    aput v1, v0, v4

    .line 75
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayEditRecipientActivity;->b:[I

    const v1, 0x7f0901f3

    aput v1, v0, v5

    .line 76
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayEditRecipientActivity;->b:[I

    const v1, 0x7f0901f7

    aput v1, v0, v6

    .line 78
    const/4 v0, 0x5

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickPayEditRecipientActivity;->c:[I

    .line 79
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayEditRecipientActivity;->c:[I

    const v1, 0x7f09020f

    aput v1, v0, v2

    .line 80
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayEditRecipientActivity;->c:[I

    const v1, 0x7f0901e9

    aput v1, v0, v3

    .line 81
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayEditRecipientActivity;->c:[I

    const v1, 0x7f0901ed

    aput v1, v0, v4

    .line 82
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayEditRecipientActivity;->c:[I

    const v1, 0x7f0901f1

    aput v1, v0, v5

    .line 83
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayEditRecipientActivity;->c:[I

    const v1, 0x7f0901f5

    aput v1, v0, v6

    .line 84
    return-void
.end method

.method protected final e()V
    .locals 3

    .prologue
    .line 88
    sget-object v0, Lcom/chase/sig/android/activity/QuickPayEditRecipientActivity;->d:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 89
    sget-object v0, Lcom/chase/sig/android/activity/QuickPayEditRecipientActivity;->d:Ljava/util/HashMap;

    const-string v1, "mobile"

    const v2, 0x7f0901e6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    sget-object v0, Lcom/chase/sig/android/activity/QuickPayEditRecipientActivity;->d:Ljava/util/HashMap;

    const-string v1, "nickname"

    const v2, 0x7f0901e1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    sget-object v0, Lcom/chase/sig/android/activity/QuickPayEditRecipientActivity;->d:Ljava/util/HashMap;

    const-string v1, "email1"

    const v2, 0x7f0901e4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    sget-object v0, Lcom/chase/sig/android/activity/QuickPayEditRecipientActivity;->d:Ljava/util/HashMap;

    const-string v1, "email2"

    const v2, 0x7f0901ea

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    sget-object v0, Lcom/chase/sig/android/activity/QuickPayEditRecipientActivity;->d:Ljava/util/HashMap;

    const-string v1, "email3"

    const v2, 0x7f0901ee

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    sget-object v0, Lcom/chase/sig/android/activity/QuickPayEditRecipientActivity;->d:Ljava/util/HashMap;

    const-string v1, "email4"

    const v2, 0x7f0901f2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    sget-object v0, Lcom/chase/sig/android/activity/QuickPayEditRecipientActivity;->d:Ljava/util/HashMap;

    const-string v1, "email5"

    const v2, 0x7f0901f6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 5
    .parameter

    .prologue
    const/4 v4, -0x1

    .line 100
    new-instance v0, Lcom/chase/sig/android/view/k$a;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/view/k$a;-><init>(Landroid/content/Context;)V

    .line 101
    iget v1, p0, Lcom/chase/sig/android/activity/QuickPayEditRecipientActivity;->k:I

    if-ne p1, v1, :cond_0

    .line 102
    const v1, 0x7f07020c

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/k$a;->b(I)Lcom/chase/sig/android/view/k$a;

    move-result-object v1

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/chase/sig/android/view/k$a;->i:Z

    const v2, 0x7f07006b

    new-instance v3, Lcom/chase/sig/android/activity/hp;

    invoke-direct {v3, p0}, Lcom/chase/sig/android/activity/hp;-><init>(Lcom/chase/sig/android/activity/QuickPayEditRecipientActivity;)V

    invoke-virtual {v1, v2, v3}, Lcom/chase/sig/android/view/k$a;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    move-result-object v1

    const v2, 0x7f070201

    new-instance v3, Lcom/chase/sig/android/activity/ho;

    invoke-direct {v3, p0}, Lcom/chase/sig/android/activity/ho;-><init>(Lcom/chase/sig/android/activity/QuickPayEditRecipientActivity;)V

    invoke-virtual {v1, v2, v3}, Lcom/chase/sig/android/view/k$a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    .line 122
    invoke-virtual {v0, v4}, Lcom/chase/sig/android/view/k$a;->d(I)Lcom/chase/sig/android/view/k;

    move-result-object v0

    .line 142
    :goto_0
    return-object v0

    .line 123
    :cond_0
    iget v1, p0, Lcom/chase/sig/android/activity/QuickPayEditRecipientActivity;->l:I

    if-ne p1, v1, :cond_1

    .line 125
    const v1, 0x7f07020d

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/k$a;->b(I)Lcom/chase/sig/android/view/k$a;

    move-result-object v1

    const v2, 0x7f070068

    new-instance v3, Lcom/chase/sig/android/activity/hq;

    invoke-direct {v3, p0}, Lcom/chase/sig/android/activity/hq;-><init>(Lcom/chase/sig/android/activity/QuickPayEditRecipientActivity;)V

    invoke-virtual {v1, v2, v3}, Lcom/chase/sig/android/view/k$a;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    .line 139
    invoke-virtual {v0, v4}, Lcom/chase/sig/android/view/k$a;->d(I)Lcom/chase/sig/android/view/k;

    move-result-object v0

    goto :goto_0

    .line 142
    :cond_1
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/hr;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0
.end method
