.class public Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity$a;
.super Lcom/chase/sig/android/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/d",
        "<",
        "Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;",
        "Ljava/lang/Object;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 141
    invoke-direct {p0}, Lcom/chase/sig/android/d;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .parameter

    .prologue
    .line 141
    const/4 v0, 0x0

    aget-object v0, p1, v0

    check-cast v0, Ljava/lang/String;

    const/4 v1, 0x1

    aget-object v1, p1, v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v1, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v1, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;

    invoke-virtual {v1}, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->A()Lcom/chase/sig/android/service/n;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/service/n;->j()Lcom/chase/sig/android/service/y;

    invoke-static {v0, v2}, Lcom/chase/sig/android/service/y;->a(Ljava/lang/String;I)Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 6
    .parameter

    .prologue
    .line 141
    check-cast p1, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->b(Ljava/util/List;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->k()I

    move-result v1

    invoke-static {v0, v1}, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->a(Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;I)I

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->j()I

    move-result v1

    invoke-static {v0, v1}, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->b(Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;I)I

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->b(Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->l()I

    move-result v1

    invoke-static {v0, v1}, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->c(Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;I)I

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->m()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->a(Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;)Z

    :cond_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->b(Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->n()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->m()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->l()I

    move-result v2

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->j()I

    move-result v3

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->k()I

    move-result v5

    invoke-static/range {v0 .. v5}, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->a(Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;Ljava/util/ArrayList;IILjava/lang/String;I)V

    goto :goto_0
.end method
