.class public Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$e;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity$a;
    }
.end annotation


# instance fields
.field protected a:Lcom/chase/sig/android/domain/m;

.field private b:Ljava/lang/String;

.field private c:I

.field private d:I

.field private k:I

.field private l:Ljava/lang/String;

.field private m:Z

.field private n:Landroid/widget/ListView;

.field private o:Landroid/view/View;

.field private p:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/chase/sig/android/domain/QuickPayPendingTransaction;",
            ">;"
        }
    .end annotation
.end field

.field private q:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    .line 52
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->m:Z

    .line 141
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 30
    iput p1, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->d:I

    return p1
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 30
    iput-object p1, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->l:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;Ljava/util/ArrayList;IILjava/lang/String;I)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 30
    invoke-direct/range {p0 .. p5}, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->a(Ljava/util/ArrayList;IILjava/lang/String;I)V

    return-void
.end method

.method private a(Ljava/util/ArrayList;IILjava/lang/String;I)V
    .locals 14
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/chase/sig/android/domain/QuickPayPendingTransaction;",
            ">;II",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 180
    iget-boolean v1, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->m:Z

    if-nez v1, :cond_a

    .line 182
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->n:Landroid/widget/ListView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setVisibility(I)V

    .line 183
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->q:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 185
    const/4 v1, 0x1

    move/from16 v0, p2

    if-eq v0, v1, :cond_0

    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->p:Ljava/util/ArrayList;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->p:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 186
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->p:Ljava/util/ArrayList;

    .line 187
    iput-object p1, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->p:Ljava/util/ArrayList;

    .line 188
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->n:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->o:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->removeFooterView(Landroid/view/View;)Z

    .line 189
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->o:Landroid/view/View;

    .line 197
    :goto_0
    const/4 v1, 0x4

    new-array v5, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "date"

    aput-object v2, v5, v1

    const/4 v1, 0x1

    const-string v2, "amount"

    aput-object v2, v5, v1

    const/4 v1, 0x2

    const-string v2, "name"

    aput-object v2, v5, v1

    const/4 v1, 0x3

    const-string v2, "frequency_foot_note"

    aput-object v2, v5, v1

    .line 200
    const/4 v1, 0x4

    new-array v6, v1, [I

    fill-array-data v6, :array_0

    .line 204
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 205
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->p:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/domain/QuickPayPendingTransaction;

    .line 206
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 208
    const-string v2, "name"

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/QuickPayPendingTransaction;->v()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v2, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 209
    const-string v2, "amount"

    new-instance v8, Lcom/chase/sig/android/util/Dollar;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/QuickPayPendingTransaction;->r()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Lcom/chase/sig/android/util/Dollar;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Lcom/chase/sig/android/util/Dollar;->h()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v2, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 210
    const-string v2, "date"

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/QuickPayPendingTransaction;->m()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/chase/sig/android/util/s;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v2, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 211
    const-string v2, "transactionId"

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/QuickPayPendingTransaction;->w()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v2, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 213
    invoke-virtual {v1}, Lcom/chase/sig/android/domain/QuickPayPendingTransaction;->X()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 214
    const-string v2, "nextPaymentDate"

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/QuickPayPendingTransaction;->F()Ljava/util/Date;

    move-result-object v8

    invoke-interface {v7, v2, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215
    const-string v2, "noOfPayments"

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/QuickPayPendingTransaction;->H()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v7, v2, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 216
    const-string v2, "%d payments left"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/QuickPayPendingTransaction;->H()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v2, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 219
    invoke-virtual {v1}, Lcom/chase/sig/android/domain/QuickPayPendingTransaction;->C()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 220
    const-string v2, "Unlimited"

    .line 222
    :cond_1
    const-string v8, "frequency_foot_note"

    const-string v9, "%s, %s"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    iget-object v12, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->a:Lcom/chase/sig/android/domain/m;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/QuickPayPendingTransaction;->X()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Lcom/chase/sig/android/domain/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    aput-object v2, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v7, v8, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 228
    :goto_2
    const-string v2, "transaction"

    invoke-interface {v7, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 229
    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 191
    :cond_2
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->p:Ljava/util/ArrayList;

    if-eqz v1, :cond_3

    if-nez p2, :cond_4

    .line 192
    :cond_3
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->p:Ljava/util/ArrayList;

    .line 194
    :cond_4
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->p:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_0

    .line 225
    :cond_5
    const-string v2, "frequency_foot_note"

    const-string v8, "One Time"

    invoke-interface {v7, v2, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 232
    :cond_6
    const-string v1, "True"

    move-object/from16 v0, p4

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 233
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->o:Landroid/view/View;

    if-nez v1, :cond_7

    .line 234
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 237
    const v2, 0x7f03007a

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->o:Landroid/view/View;

    .line 238
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->o:Landroid/view/View;

    const v2, 0x7f0901dc

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 239
    const-string v2, "%d transactions total, %d to load..."

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v4, v7

    const/4 v7, 0x1

    add-int/lit8 v8, p3, -0x19

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v4, v7

    invoke-static {v2, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 241
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->n:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->o:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;)V

    .line 245
    :cond_7
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->o:Landroid/view/View;

    new-instance v2, Lcom/chase/sig/android/activity/ia;

    move/from16 v0, p5

    invoke-direct {v2, p0, v0}, Lcom/chase/sig/android/activity/ia;-><init>(Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;I)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 260
    :cond_8
    :goto_3
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->n:Landroid/widget/ListView;

    new-instance v2, Lcom/chase/sig/android/activity/ib;

    invoke-direct {v2, p0}, Lcom/chase/sig/android/activity/ib;-><init>(Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 279
    new-instance v7, Lcom/chase/sig/android/view/af;

    new-instance v1, Landroid/widget/SimpleAdapter;

    const v4, 0x7f03007b

    move-object v2, p0

    invoke-direct/range {v1 .. v6}, Landroid/widget/SimpleAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V

    invoke-direct {v7, v1}, Lcom/chase/sig/android/view/af;-><init>(Landroid/widget/ListAdapter;)V

    .line 284
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->n:Landroid/widget/ListView;

    invoke-virtual {v1, v7}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 290
    :goto_4
    return-void

    .line 256
    :cond_9
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->o:Landroid/view/View;

    if-eqz v1, :cond_8

    .line 257
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->n:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->o:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->removeFooterView(Landroid/view/View;)Z

    goto :goto_3

    .line 287
    :cond_a
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->n:Landroid/widget/ListView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setVisibility(I)V

    .line 288
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->q:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_4

    .line 200
    nop

    :array_0
    .array-data 0x4
        0xf3t 0x0t 0x9t 0x7ft
        0xf4t 0x0t 0x9t 0x7ft
        0xddt 0x1t 0x9t 0x7ft
        0xdet 0x1t 0x9t 0x7ft
    .end array-data
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;)Z
    .locals 1
    .parameter

    .prologue
    .line 30
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->m:Z

    return v0
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 30
    iput p1, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->c:I

    return p1
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;)Landroid/widget/TextView;
    .locals 1
    .parameter

    .prologue
    .line 30
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->q:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 30
    iput-object p1, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->b:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic c(Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 30
    iput p1, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->k:I

    return p1
.end method

.method static synthetic c(Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 30
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->l:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 6
    .parameter

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 64
    const v0, 0x7f030093

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->b(I)V

    .line 65
    const v0, 0x7f07020f

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->setTitle(I)V

    .line 66
    const v0, 0x7f0901d4

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->q:Landroid/widget/TextView;

    .line 67
    const v0, 0x7f0901d3

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->n:Landroid/widget/ListView;

    .line 69
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->c:Lcom/chase/sig/android/service/quickpay/TodoListResponse;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->t()Lcom/chase/sig/android/domain/m;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->a:Lcom/chase/sig/android/domain/m;

    .line 71
    if-eqz p1, :cond_1

    const-string v0, "bundle_activity"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    if-eqz v0, :cond_2

    .line 72
    const-string v0, "bundle_activity"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->p:Ljava/util/ArrayList;

    .line 75
    const-string v0, "bundle_currentPage"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->k:I

    .line 76
    const-string v0, "bundle_nextPage"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->d:I

    .line 77
    const-string v0, "bundle_totalRows"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->c:I

    .line 78
    const-string v0, "bundle_showNext"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->b:Ljava/lang/String;

    .line 79
    const-string v0, "no_activities"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->m:Z

    .line 81
    const-string v0, "bundle_sort_column"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->l:Ljava/lang/String;

    .line 82
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->p:Ljava/util/ArrayList;

    iget v2, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->k:I

    iget v3, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->c:I

    iget-object v4, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->b:Ljava/lang/String;

    iget v5, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->d:I

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->a(Ljava/util/ArrayList;IILjava/lang/String;I)V

    .line 93
    :cond_0
    :goto_1
    const v0, 0x7f090254

    new-instance v1, Lcom/chase/sig/android/activity/hy;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/hy;-><init>(Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;)V

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 107
    const v0, 0x7f090255

    new-instance v1, Lcom/chase/sig/android/activity/hz;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/hz;-><init>(Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;)V

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 120
    return-void

    :cond_1
    move v0, v2

    .line 71
    goto :goto_0

    .line 86
    :cond_2
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->e:Lcom/chase/sig/android/activity/mc;

    const-class v3, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity$a;

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/activity/mc;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/ae;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity$a;

    .line 87
    invoke-virtual {v0}, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity$a;->d()Z

    move-result v3

    if-nez v3, :cond_0

    .line 88
    const-string v3, "recipient"

    iput-object v3, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->l:Ljava/lang/String;

    .line 89
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->l:Ljava/lang/String;

    aput-object v4, v3, v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v3, v1

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_1
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 128
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ai;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 130
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->e:Lcom/chase/sig/android/activity/mc;

    const-class v1, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity$a;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/mc;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/ae;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity$a;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity$a;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 131
    const-string v0, "bundle_activity"

    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->p:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 132
    const-string v0, "bundle_currentPage"

    iget v1, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->k:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 133
    const-string v0, "bundle_nextPage"

    iget v1, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->d:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 134
    const-string v0, "bundle_totalRows"

    iget v1, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->c:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 135
    const-string v0, "bundle_showNext"

    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    const-string v0, "no_activities"

    iget-boolean v1, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->m:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 137
    const-string v0, "bundle_sort_column"

    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;->l:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    :cond_0
    return-void
.end method
