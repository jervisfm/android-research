.class public Lcom/chase/sig/android/activity/QuickPayPendingTransactionsCancelConfirmationActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$e;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 4
    .parameter

    .prologue
    .line 17
    const v0, 0x7f030081

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsCancelConfirmationActivity;->b(I)V

    .line 18
    const v0, 0x7f070074

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsCancelConfirmationActivity;->setTitle(I)V

    .line 20
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsCancelConfirmationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 21
    const-string v1, "sendTransaction"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/QuickPayPendingTransaction;

    .line 24
    new-instance v2, Lcom/chase/sig/android/view/aa;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsCancelConfirmationActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/ChaseApplication;

    invoke-direct {v2, v3, v0, v1}, Lcom/chase/sig/android/view/aa;-><init>(Landroid/content/Context;Lcom/chase/sig/android/domain/QuickPayPendingTransaction;Lcom/chase/sig/android/ChaseApplication;)V

    .line 26
    const v0, 0x7f090205

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsCancelConfirmationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 27
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 29
    const v0, 0x7f090206

    const-class v1, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsCancelConfirmationActivity;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/a/f;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/activity/a/f;->b()Lcom/chase/sig/android/activity/a/f;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsCancelConfirmationActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 32
    const v0, 0x7f090207

    const-class v1, Lcom/chase/sig/android/activity/QuickPayHomeActivity;

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsCancelConfirmationActivity;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/a/f;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/activity/a/f;->b()Lcom/chase/sig/android/activity/a/f;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsCancelConfirmationActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 33
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 36
    invoke-virtual {p0, p1}, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsCancelConfirmationActivity;->j(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 40
    :goto_0
    return v0

    .line 38
    :cond_0
    const-class v0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;

    invoke-static {p1, p0, v0}, Lcom/chase/sig/android/a/a;->a(ILcom/chase/sig/android/activity/eb;Ljava/lang/Class;)V

    .line 40
    invoke-super {p0, p1, p2}, Lcom/chase/sig/android/activity/ai;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method
