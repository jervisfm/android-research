.class public Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity$a;
.super Lcom/chase/sig/android/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/d",
        "<",
        "Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity;",
        "Ljava/lang/Boolean;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/service/quickpay/QuickPayCancelPendingTransactionResponse;",
        ">;"
    }
.end annotation


# instance fields
.field a:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 282
    invoke-direct {p0}, Lcom/chase/sig/android/d;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .parameter

    .prologue
    .line 282
    check-cast p1, [Ljava/lang/Boolean;

    const/4 v0, 0x0

    aget-object v0, p1, v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity$a;->a:Ljava/lang/Boolean;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity;->A()Lcom/chase/sig/android/service/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/service/n;->j()Lcom/chase/sig/android/service/y;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity;->a(Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity;)Lcom/chase/sig/android/domain/QuickPayPendingTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity$a;->a:Ljava/lang/Boolean;

    invoke-static {v0, v1}, Lcom/chase/sig/android/service/y;->a(Lcom/chase/sig/android/domain/QuickPayTransaction;Ljava/lang/Boolean;)Lcom/chase/sig/android/service/quickpay/QuickPayCancelPendingTransactionResponse;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 3
    .parameter

    .prologue
    .line 282
    check-cast p1, Lcom/chase/sig/android/service/quickpay/QuickPayCancelPendingTransactionResponse;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPayCancelPendingTransactionResponse;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPayCancelPendingTransactionResponse;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity;->c(Ljava/util/List;)V

    :goto_0
    return-void

    :cond_0
    new-instance v1, Landroid/content/Intent;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    const-class v2, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsCancelConfirmationActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPayCancelPendingTransactionResponse;->a()Lcom/chase/sig/android/domain/QuickPayPendingTransaction;

    move-result-object v0

    iget-object v2, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity$a;->a:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "This Payment Canceled"

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/domain/QuickPayPendingTransaction;->f(Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/domain/QuickPayPendingTransaction;->d(Z)V

    :goto_1
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayPendingTransaction;->M()V

    const-string v2, "sendTransaction"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_1
    const-string v2, "Entire Series Canceled"

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/domain/QuickPayPendingTransaction;->f(Ljava/lang/String;)V

    goto :goto_1
.end method
