.class public Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$e;
.implements Lcom/chase/sig/android/c$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity$a;
    }
.end annotation


# instance fields
.field private a:Lcom/chase/sig/android/domain/QuickPayPendingTransaction;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    .line 30
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity;->a:Lcom/chase/sig/android/domain/QuickPayPendingTransaction;

    .line 282
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity;)Lcom/chase/sig/android/domain/QuickPayPendingTransaction;
    .locals 1
    .parameter

    .prologue
    .line 21
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity;->a:Lcom/chase/sig/android/domain/QuickPayPendingTransaction;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 106
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/chase/sig/android/activity/QuickPayHomeActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 108
    const/high16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 109
    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity;->startActivity(Landroid/content/Intent;)V

    .line 110
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 7
    .parameter

    .prologue
    const/16 v6, 0x8

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 34
    const v0, 0x7f030089

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity;->b(I)V

    .line 35
    const v0, 0x7f07020f

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity;->setTitle(I)V

    .line 37
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "sendTransaction"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/QuickPayPendingTransaction;

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity;->a:Lcom/chase/sig/android/domain/QuickPayPendingTransaction;

    .line 41
    new-instance v1, Lcom/chase/sig/android/view/aa;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity;->a:Lcom/chase/sig/android/domain/QuickPayPendingTransaction;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/ChaseApplication;

    invoke-direct {v1, v4, v5, v0}, Lcom/chase/sig/android/view/aa;-><init>(Landroid/content/Context;Lcom/chase/sig/android/domain/QuickPayPendingTransaction;Lcom/chase/sig/android/ChaseApplication;)V

    .line 43
    const v0, 0x7f09021a

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 44
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 46
    const v0, 0x7f09021c

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 47
    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 48
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 50
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity;->a:Lcom/chase/sig/android/domain/QuickPayPendingTransaction;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/QuickPayPendingTransaction;->D()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity;->a:Lcom/chase/sig/android/domain/QuickPayPendingTransaction;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/QuickPayPendingTransaction;->H()I

    move-result v1

    if-gt v1, v2, :cond_0

    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity;->a:Lcom/chase/sig/android/domain/QuickPayPendingTransaction;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/QuickPayPendingTransaction;->C()Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_0
    move v1, v2

    .line 54
    :goto_0
    new-instance v4, Lcom/chase/sig/android/activity/ic;

    invoke-direct {v4, p0, v1}, Lcom/chase/sig/android/activity/ic;-><init>(Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity;Z)V

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 72
    iget-object v4, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity;->a:Lcom/chase/sig/android/domain/QuickPayPendingTransaction;

    invoke-virtual {v4}, Lcom/chase/sig/android/domain/QuickPayPendingTransaction;->P()Z

    move-result v4

    if-nez v4, :cond_1

    .line 73
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 74
    invoke-virtual {v0, v6}, Landroid/widget/Button;->setVisibility(I)V

    .line 77
    :cond_1
    const v0, 0x7f09021b

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 78
    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 79
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 81
    new-instance v2, Lcom/chase/sig/android/activity/ii;

    invoke-direct {v2, p0, v1}, Lcom/chase/sig/android/activity/ii;-><init>(Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity;Z)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 94
    iget-object v2, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity;->a:Lcom/chase/sig/android/domain/QuickPayPendingTransaction;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/QuickPayPendingTransaction;->Q()Z

    move-result v2

    if-nez v2, :cond_2

    .line 95
    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 96
    invoke-virtual {v0, v6}, Landroid/widget/Button;->setVisibility(I)V

    .line 99
    :cond_2
    iget-object v2, p0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity;->a:Lcom/chase/sig/android/domain/QuickPayPendingTransaction;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/QuickPayPendingTransaction;->D()Z

    move-result v2

    if-eqz v2, :cond_3

    if-eqz v1, :cond_3

    .line 100
    const v1, 0x7f07023f

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 102
    :cond_3
    return-void

    :cond_4
    move v1, v3

    .line 50
    goto :goto_0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 5
    .parameter

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x1

    .line 115
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ai;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    .line 117
    new-instance v1, Lcom/chase/sig/android/view/k$a;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/view/k$a;-><init>(Landroid/content/Context;)V

    .line 120
    if-nez p1, :cond_1

    .line 122
    iput-boolean v3, v1, Lcom/chase/sig/android/view/k$a;->i:Z

    const v0, 0x7f07023e

    new-instance v2, Lcom/chase/sig/android/activity/ik;

    invoke-direct {v2, p0}, Lcom/chase/sig/android/activity/ik;-><init>(Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity;)V

    invoke-virtual {v1, v0, v2}, Lcom/chase/sig/android/view/k$a;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    move-result-object v0

    const v2, 0x7f07006d

    new-instance v3, Lcom/chase/sig/android/activity/ij;

    invoke-direct {v3, p0}, Lcom/chase/sig/android/activity/ij;-><init>(Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity;)V

    invoke-virtual {v0, v2, v3}, Lcom/chase/sig/android/view/k$a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    move-result-object v0

    const v2, 0x7f070242

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/view/k$a;->b(I)Lcom/chase/sig/android/view/k$a;

    .line 146
    invoke-virtual {v1, v4}, Lcom/chase/sig/android/view/k$a;->d(I)Lcom/chase/sig/android/view/k;

    move-result-object v0

    .line 279
    :cond_0
    :goto_0
    return-object v0

    .line 148
    :cond_1
    if-ne p1, v3, :cond_2

    .line 149
    const v0, 0x7f07024a

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/view/k$a;->b(I)Lcom/chase/sig/android/view/k$a;

    .line 150
    iput-boolean v3, v1, Lcom/chase/sig/android/view/k$a;->i:Z

    const v0, 0x7f07023d

    new-instance v2, Lcom/chase/sig/android/activity/in;

    invoke-direct {v2, p0}, Lcom/chase/sig/android/activity/in;-><init>(Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity;)V

    invoke-virtual {v1, v0, v2}, Lcom/chase/sig/android/view/k$a;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    move-result-object v0

    const v2, 0x7f07023c

    new-instance v3, Lcom/chase/sig/android/activity/im;

    invoke-direct {v3, p0}, Lcom/chase/sig/android/activity/im;-><init>(Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity;)V

    invoke-virtual {v0, v2, v3}, Lcom/chase/sig/android/view/k$a;->c(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    move-result-object v0

    const v2, 0x7f07023b

    new-instance v3, Lcom/chase/sig/android/activity/il;

    invoke-direct {v3, p0}, Lcom/chase/sig/android/activity/il;-><init>(Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity;)V

    invoke-virtual {v0, v2, v3}, Lcom/chase/sig/android/view/k$a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    .line 176
    invoke-virtual {v1, v4}, Lcom/chase/sig/android/view/k$a;->d(I)Lcom/chase/sig/android/view/k;

    move-result-object v0

    goto :goto_0

    .line 178
    :cond_2
    const/4 v2, 0x2

    if-ne p1, v2, :cond_3

    .line 180
    iput-boolean v3, v1, Lcom/chase/sig/android/view/k$a;->i:Z

    const v0, 0x7f07023a

    new-instance v2, Lcom/chase/sig/android/activity/ip;

    invoke-direct {v2, p0}, Lcom/chase/sig/android/activity/ip;-><init>(Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity;)V

    invoke-virtual {v1, v0, v2}, Lcom/chase/sig/android/view/k$a;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    move-result-object v0

    const v2, 0x7f070239

    new-instance v3, Lcom/chase/sig/android/activity/io;

    invoke-direct {v3, p0}, Lcom/chase/sig/android/activity/io;-><init>(Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity;)V

    invoke-virtual {v0, v2, v3}, Lcom/chase/sig/android/view/k$a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    move-result-object v0

    const v2, 0x7f070238

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/view/k$a;->b(I)Lcom/chase/sig/android/view/k$a;

    .line 205
    invoke-virtual {v1, v4}, Lcom/chase/sig/android/view/k$a;->d(I)Lcom/chase/sig/android/view/k;

    move-result-object v0

    goto :goto_0

    .line 207
    :cond_3
    const/4 v2, 0x3

    if-ne p1, v2, :cond_4

    .line 209
    iput-boolean v3, v1, Lcom/chase/sig/android/view/k$a;->i:Z

    const v0, 0x7f070236

    new-instance v2, Lcom/chase/sig/android/activity/ie;

    invoke-direct {v2, p0}, Lcom/chase/sig/android/activity/ie;-><init>(Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity;)V

    invoke-virtual {v1, v0, v2}, Lcom/chase/sig/android/view/k$a;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    move-result-object v0

    const v2, 0x7f070237

    new-instance v3, Lcom/chase/sig/android/activity/id;

    invoke-direct {v3, p0}, Lcom/chase/sig/android/activity/id;-><init>(Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity;)V

    invoke-virtual {v0, v2, v3}, Lcom/chase/sig/android/view/k$a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    move-result-object v0

    const v2, 0x7f070235

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/view/k$a;->b(I)Lcom/chase/sig/android/view/k$a;

    .line 236
    invoke-virtual {v1, v4}, Lcom/chase/sig/android/view/k$a;->d(I)Lcom/chase/sig/android/view/k;

    move-result-object v0

    goto/16 :goto_0

    .line 238
    :cond_4
    const/4 v2, 0x5

    if-ne p1, v2, :cond_0

    .line 239
    const v0, 0x7f070250

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/view/k$a;->b(I)Lcom/chase/sig/android/view/k$a;

    .line 240
    iput-boolean v3, v1, Lcom/chase/sig/android/view/k$a;->i:Z

    const v0, 0x7f07021a

    new-instance v2, Lcom/chase/sig/android/activity/ih;

    invoke-direct {v2, p0}, Lcom/chase/sig/android/activity/ih;-><init>(Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity;)V

    invoke-virtual {v1, v0, v2}, Lcom/chase/sig/android/view/k$a;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    move-result-object v0

    const v2, 0x7f07021b

    new-instance v3, Lcom/chase/sig/android/activity/ig;

    invoke-direct {v3, p0}, Lcom/chase/sig/android/activity/ig;-><init>(Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity;)V

    invoke-virtual {v0, v2, v3}, Lcom/chase/sig/android/view/k$a;->c(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    move-result-object v0

    const v2, 0x7f07021c

    new-instance v3, Lcom/chase/sig/android/activity/if;

    invoke-direct {v3, p0}, Lcom/chase/sig/android/activity/if;-><init>(Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity;)V

    invoke-virtual {v0, v2, v3}, Lcom/chase/sig/android/view/k$a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    .line 276
    invoke-virtual {v1, v4}, Lcom/chase/sig/android/view/k$a;->d(I)Lcom/chase/sig/android/view/k;

    move-result-object v0

    goto/16 :goto_0
.end method
