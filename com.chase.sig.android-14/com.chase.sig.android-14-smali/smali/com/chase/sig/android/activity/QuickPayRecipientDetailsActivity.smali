.class public Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$e;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity$a;
    }
.end annotation


# instance fields
.field private a:Lcom/chase/sig/android/domain/QuickPayRecipient;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    .line 131
    return-void
.end method

.method private a(ZLcom/chase/sig/android/domain/QuickPayRecipient;)Landroid/view/View$OnClickListener;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 110
    new-instance v0, Lcom/chase/sig/android/activity/is;

    invoke-direct {v0, p0, p1, p2}, Lcom/chase/sig/android/activity/is;-><init>(Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;ZLcom/chase/sig/android/domain/QuickPayRecipient;)V

    return-object v0
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;)Lcom/chase/sig/android/domain/QuickPayRecipient;
    .locals 1
    .parameter

    .prologue
    .line 26
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;->a:Lcom/chase/sig/android/domain/QuickPayRecipient;

    return-object v0
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;Lcom/chase/sig/android/domain/QuickPayRecipient;)Lcom/chase/sig/android/domain/QuickPayRecipient;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 26
    iput-object p1, p0, Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;->a:Lcom/chase/sig/android/domain/QuickPayRecipient;

    return-object p1
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;->d()V

    return-void
.end method

.method private d()V
    .locals 3

    .prologue
    .line 104
    const v0, 0x7f090224

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;->a:Lcom/chase/sig/android/domain/QuickPayRecipient;

    invoke-direct {p0, v1, v2}, Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;->a(ZLcom/chase/sig/android/domain/QuickPayRecipient;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 105
    const v0, 0x7f090212

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;->a:Lcom/chase/sig/android/domain/QuickPayRecipient;

    invoke-direct {p0, v1, v2}, Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;->a(ZLcom/chase/sig/android/domain/QuickPayRecipient;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 106
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 8
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 33
    const v0, 0x7f03008a

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;->b(I)V

    .line 34
    const v0, 0x7f070216

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;->setTitle(I)V

    .line 35
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "recipient"

    invoke-static {p1, v0, v1}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/QuickPayRecipient;

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;->a:Lcom/chase/sig/android/domain/QuickPayRecipient;

    .line 37
    const v0, 0x7f0901e2

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 38
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;->a:Lcom/chase/sig/android/domain/QuickPayRecipient;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/QuickPayRecipient;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 39
    const v0, 0x7f0901e7

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 40
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;->a:Lcom/chase/sig/android/domain/QuickPayRecipient;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/QuickPayRecipient;->i()Lcom/chase/sig/android/domain/MobilePhoneNumber;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 41
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;->a:Lcom/chase/sig/android/domain/QuickPayRecipient;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/QuickPayRecipient;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;->a:Lcom/chase/sig/android/domain/QuickPayRecipient;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/QuickPayRecipient;->f()Ljava/lang/String;

    move-result-object v1

    .line 43
    :goto_0
    invoke-static {v1}, Lcom/chase/sig/android/util/s;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 45
    :cond_0
    const v0, 0x7f09021e

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    .line 46
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;->a:Lcom/chase/sig/android/domain/QuickPayRecipient;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/QuickPayRecipient;->e()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    .line 48
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;->a:Lcom/chase/sig/android/domain/QuickPayRecipient;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/QuickPayRecipient;->e()Ljava/util/List;

    move-result-object v6

    move v3, v4

    .line 49
    :goto_1
    if-ge v3, v5, :cond_2

    .line 50
    invoke-virtual {v0, v3}, Landroid/widget/RadioGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    .line 51
    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/chase/sig/android/domain/Email;

    .line 52
    invoke-virtual {v2}, Lcom/chase/sig/android/domain/Email;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 53
    invoke-virtual {v1, v4}, Landroid/widget/RadioButton;->setVisibility(I)V

    .line 54
    invoke-virtual {v2}, Lcom/chase/sig/android/domain/Email;->b()Z

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 56
    new-instance v2, Lcom/chase/sig/android/activity/iq;

    invoke-direct {v2, p0}, Lcom/chase/sig/android/activity/iq;-><init>(Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/RadioButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 49
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1

    .line 41
    :cond_1
    const-string v1, ""

    goto :goto_0

    .line 79
    :cond_2
    invoke-direct {p0}, Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;->d()V

    .line 81
    const v0, 0x7f09021d

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 83
    new-instance v1, Lcom/chase/sig/android/activity/ir;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/ir;-><init>(Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 95
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 6
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 155
    invoke-virtual {p0, p1}, Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;->j(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 167
    :goto_0
    return v0

    .line 159
    :cond_0
    const/4 v1, 0x4

    if-ne p1, v1, :cond_1

    .line 160
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/chase/sig/android/activity/QuickPayChooseRecipientActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 161
    const/high16 v2, 0x400

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 162
    const-string v2, "quick_pay_manage_recipient"

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "quick_pay_manage_recipient"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 164
    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 167
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/chase/sig/android/activity/ai;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 99
    const-string v0, "recipient"

    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;->a:Lcom/chase/sig/android/domain/QuickPayRecipient;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 100
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ai;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 101
    return-void
.end method
