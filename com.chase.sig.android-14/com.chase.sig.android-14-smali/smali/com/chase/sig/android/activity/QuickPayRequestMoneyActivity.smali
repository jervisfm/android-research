.class public Lcom/chase/sig/android/activity/QuickPayRequestMoneyActivity;
.super Lcom/chase/sig/android/activity/iy;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/chase/sig/android/activity/iy;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    const/16 v1, 0x8

    .line 16
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/iy;->a(Landroid/os/Bundle;)V

    .line 17
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPayRequestMoneyActivity;->f()V

    .line 18
    const v0, 0x7f0701f2

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayRequestMoneyActivity;->setTitle(I)V

    .line 20
    const v0, 0x7f090237

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayRequestMoneyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/JPSpinner;

    .line 21
    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/JPSpinner;->setVisibility(I)V

    .line 22
    const v0, 0x7f090235

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayRequestMoneyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 23
    const v0, 0x7f09023d

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayRequestMoneyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 24
    const v0, 0x7f09023e

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayRequestMoneyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const-string v1, "Due date"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 25
    const v0, 0x7f090231

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayRequestMoneyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const-string v1, "From"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 26
    return-void
.end method

.method protected final d()Lcom/chase/sig/android/domain/QuickPayTransaction;
    .locals 2

    .prologue
    .line 30
    invoke-super {p0}, Lcom/chase/sig/android/activity/iy;->d()Lcom/chase/sig/android/domain/QuickPayTransaction;

    move-result-object v1

    .line 32
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->b(Z)V

    .line 34
    const v0, 0x7f09023f

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayRequestMoneyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 35
    invoke-virtual {v0}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->k(Ljava/lang/String;)V

    .line 36
    return-object v1
.end method

.method protected final e()Z
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x1

    return v0
.end method
