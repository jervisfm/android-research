.class public Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity$a;
.super Lcom/chase/sig/android/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/d",
        "<",
        "Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/service/quickpay/QuickPayTransactionStartResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 275
    invoke-direct {p0}, Lcom/chase/sig/android/d;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .parameter

    .prologue
    .line 275
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;

    iget-object v2, v0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->k:Lcom/chase/sig/android/domain/QuickPayTransaction;

    const-string v1, ""

    const-string v0, ""

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/QuickPayTransaction;->g()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/QuickPayTransaction;->S()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "0"

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/QuickPayTransaction;->w()Ljava/lang/String;

    move-result-object v0

    move-object v2, v1

    move-object v1, v0

    :goto_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->A()Lcom/chase/sig/android/service/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/service/n;->j()Lcom/chase/sig/android/service/y;

    invoke-static {v2, v1}, Lcom/chase/sig/android/service/y;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/service/quickpay/QuickPayTransactionStartResponse;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-virtual {v2}, Lcom/chase/sig/android/domain/QuickPayTransaction;->w()Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    move-object v1, v0

    goto :goto_0

    :cond_1
    move-object v2, v1

    move-object v1, v0

    goto :goto_0
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 7
    .parameter

    .prologue
    .line 275
    check-cast p1, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionStartResponse;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionStartResponse;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionStartResponse;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->b(Ljava/util/List;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionStartResponse;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionStartResponse;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->a(Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionStartResponse;->a()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/service/quickpay/PayFromAccount;

    const/4 v2, 0x1

    iget-object v1, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v1, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;

    invoke-virtual {v1}, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v1

    iget-object v1, v1, Lcom/chase/sig/android/domain/o;->b:Lcom/chase/sig/android/domain/g;

    invoke-interface {v1}, Lcom/chase/sig/android/domain/g;->i()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/domain/IAccount;

    invoke-interface {v1}, Lcom/chase/sig/android/domain/IAccount;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickpay/PayFromAccount;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v1}, Lcom/chase/sig/android/domain/IAccount;->j()Lcom/chase/sig/android/util/Dollar;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/quickpay/PayFromAccount;->a(Lcom/chase/sig/android/util/Dollar;)V

    const/4 v1, 0x0

    :goto_3
    move v2, v1

    goto :goto_2

    :cond_2
    invoke-virtual {v0, v2}, Lcom/chase/sig/android/service/quickpay/PayFromAccount;->b(Z)V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionStartResponse;->a()Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->a(Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->b(Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;)V

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_3
.end method
