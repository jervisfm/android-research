.class public Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;
.super Lcom/chase/sig/android/activity/iy;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity$a;
    }
.end annotation


# instance fields
.field a:Lcom/chase/sig/android/view/JPSpinner;

.field b:Landroid/widget/TextView;

.field c:Landroid/widget/TextView;

.field protected d:Lcom/chase/sig/android/view/x;

.field private n:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/chase/sig/android/service/quickpay/PayFromAccount;",
            ">;"
        }
    .end annotation
.end field

.field private o:Ljava/lang/String;

.field private p:Landroid/widget/CheckBox;

.field private q:Landroid/widget/CheckBox;

.field private r:Landroid/widget/EditText;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/chase/sig/android/activity/iy;-><init>()V

    .line 328
    new-instance v0, Lcom/chase/sig/android/activity/iv;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/activity/iv;-><init>(Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;)V

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->d:Lcom/chase/sig/android/view/x;

    return-void
.end method

.method private G()Lcom/chase/sig/android/view/JPSpinner;
    .locals 1

    .prologue
    .line 45
    const v0, 0x7f090241

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/JPSpinner;

    return-object v0
.end method

.method private H()V
    .locals 2

    .prologue
    .line 68
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->p:Landroid/widget/CheckBox;

    new-instance v1, Lcom/chase/sig/android/activity/it;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/it;-><init>(Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 75
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->q:Landroid/widget/CheckBox;

    new-instance v1, Lcom/chase/sig/android/activity/iu;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/iu;-><init>(Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 82
    return-void
.end method

.method private I()I
    .locals 1

    .prologue
    .line 359
    :try_start_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->r:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 360
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 362
    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/util/ArrayList;Ljava/lang/String;)I
    .locals 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/chase/sig/android/service/quickpay/PayFromAccount;",
            ">;",
            "Ljava/lang/String;",
            ")I"
        }
    .end annotation

    .prologue
    .line 244
    const/4 v0, -0x1

    .line 245
    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/service/quickpay/PayFromAccount;

    .line 246
    add-int/lit8 v1, v1, 0x1

    .line 247
    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickpay/PayFromAccount;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 251
    :cond_1
    return v1
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;)Landroid/widget/EditText;
    .locals 1
    .parameter

    .prologue
    .line 30
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->r:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;Ljava/lang/String;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 30
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->a(Ljava/lang/String;Z)V

    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;Ljava/util/ArrayList;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->a(Ljava/util/ArrayList;)V

    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;Z)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->a(Z)V

    return-void
.end method

.method private a(Ljava/lang/String;Z)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 262
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->o:Ljava/lang/String;

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 268
    :goto_0
    return-void

    .line 266
    :cond_0
    iput-object p1, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->o:Ljava/lang/String;

    .line 267
    const v0, 0x7f070086

    invoke-virtual {p0, v0, p1, p2}, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->a(ILjava/lang/String;Z)V

    goto :goto_0
.end method

.method private a(Ljava/util/ArrayList;)V
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/chase/sig/android/service/quickpay/PayFromAccount;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 228
    iput-object p1, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->n:Ljava/util/ArrayList;

    .line 229
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->a:Lcom/chase/sig/android/view/JPSpinner;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/JPSpinner;->removeAllViews()V

    .line 230
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->a:Lcom/chase/sig/android/view/JPSpinner;

    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->d:Lcom/chase/sig/android/view/x;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/JPSpinner;->a(Lcom/chase/sig/android/view/x;)V

    .line 231
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->a:Lcom/chase/sig/android/view/JPSpinner;

    invoke-static {p0, p1}, Lcom/chase/sig/android/util/a;->a(Landroid/content/Context;Ljava/util/ArrayList;)Landroid/widget/ListAdapter;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/JPSpinner;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 233
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->k:Lcom/chase/sig/android/domain/QuickPayTransaction;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->p()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 234
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->a:Lcom/chase/sig/android/view/JPSpinner;

    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->k:Lcom/chase/sig/android/domain/QuickPayTransaction;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/QuickPayTransaction;->p()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->a(Ljava/util/ArrayList;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/JPSpinner;->setSelection(I)V

    .line 241
    :goto_0
    return-void

    .line 235
    :cond_0
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/ChaseApplication;

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->c:Lcom/chase/sig/android/service/quickpay/TodoListResponse;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->n()Lcom/chase/sig/android/service/quickpay/PayFromAccount;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 236
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->a:Lcom/chase/sig/android/view/JPSpinner;

    invoke-direct {p0, p1}, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->b(Ljava/util/ArrayList;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/JPSpinner;->setSelection(I)V

    .line 237
    invoke-direct {p0, p1}, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->b(Ljava/util/ArrayList;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->k(I)V

    goto :goto_0

    .line 239
    :cond_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->a:Lcom/chase/sig/android/view/JPSpinner;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/JPSpinner;->setSelection(I)V

    goto :goto_0
.end method

.method private a(Z)V
    .locals 2
    .parameter

    .prologue
    const/16 v0, 0x8

    .line 145
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->k:Lcom/chase/sig/android/domain/QuickPayTransaction;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/QuickPayTransaction;->R()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 146
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->p:Landroid/widget/CheckBox;

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 149
    :cond_0
    if-eqz p1, :cond_1

    const/4 v0, 0x0

    .line 151
    :cond_1
    const v1, 0x7f090241

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 152
    const v1, 0x7f090240

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 153
    const v1, 0x7f090242

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 154
    const v1, 0x7f09009d

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 156
    const v0, 0x7f09023e

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 157
    if-eqz p1, :cond_2

    const v1, 0x7f07022c

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 159
    return-void

    .line 157
    :cond_2
    const v1, 0x7f07022b

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private b(Ljava/util/ArrayList;)I
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/chase/sig/android/service/quickpay/PayFromAccount;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 255
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/ChaseApplication;

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->c:Lcom/chase/sig/android/service/quickpay/TodoListResponse;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->n()Lcom/chase/sig/android/service/quickpay/PayFromAccount;

    move-result-object v0

    .line 257
    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickpay/PayFromAccount;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->a(Ljava/util/ArrayList;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->H()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 50
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/iy;->a(Landroid/os/Bundle;)V

    .line 52
    if-eqz p1, :cond_0

    const-string v0, "FOOTNOTE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 53
    const-string v0, "FOOTNOTE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v1}, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->a(Ljava/lang/String;Z)V

    .line 56
    :cond_0
    if-eqz p1, :cond_1

    const-string v0, "PAY_FROM_ACCOUNTS"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "PAY_FROM_ACCOUNTS"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 58
    const-string v0, "PAY_FROM_ACCOUNTS"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->n:Ljava/util/ArrayList;

    .line 60
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->n:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->a(Ljava/util/ArrayList;)V

    .line 61
    invoke-direct {p0}, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->H()V

    .line 65
    :goto_0
    return-void

    .line 63
    :cond_1
    const-class v0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity$a;

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected final d()Lcom/chase/sig/android/domain/QuickPayTransaction;
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 190
    invoke-super {p0}, Lcom/chase/sig/android/activity/iy;->d()Lcom/chase/sig/android/domain/QuickPayTransaction;

    move-result-object v4

    .line 192
    invoke-virtual {v4, v1}, Lcom/chase/sig/android/domain/QuickPayTransaction;->b(Z)V

    .line 193
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->a:Lcom/chase/sig/android/view/JPSpinner;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/JPSpinner;->getSelectedItemPosition()I

    move-result v0

    if-gez v0, :cond_3

    move-object v0, v3

    .line 194
    :goto_0
    if-eqz v0, :cond_0

    .line 195
    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickpay/PayFromAccount;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Lcom/chase/sig/android/domain/QuickPayTransaction;->b(Ljava/lang/String;)V

    .line 196
    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickpay/PayFromAccount;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Lcom/chase/sig/android/domain/QuickPayTransaction;->c(Ljava/lang/String;)V

    .line 197
    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickpay/PayFromAccount;->d()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 198
    const v0, 0x7f090239

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->i(Ljava/lang/String;)V

    .line 204
    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->j()Ljava/util/Date;

    move-result-object v5

    .line 205
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "is_editing"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {v4, v0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->h(Z)V

    .line 206
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->p:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    .line 207
    invoke-virtual {v4, v0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->d(Z)V

    .line 208
    if-eqz v0, :cond_2

    .line 209
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->q:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    .line 210
    invoke-virtual {v4, v0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->c(Z)V

    .line 212
    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->r:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 213
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->r:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v4, v0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->a(I)V

    .line 217
    :cond_1
    invoke-direct {p0}, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->G()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/view/JPSpinner;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 218
    invoke-direct {p0}, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->G()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/view/JPSpinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 219
    iget-object v6, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->m:Lcom/chase/sig/android/domain/m;

    move v2, v1

    :goto_2
    iget-object v1, v6, Lcom/chase/sig/android/domain/m;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v2, v1, :cond_6

    iget-object v1, v6, Lcom/chase/sig/android/domain/m;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/domain/LabeledValue;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/LabeledValue;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, v6, Lcom/chase/sig/android/domain/m;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/domain/LabeledValue;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v1

    :goto_3
    invoke-virtual {v4, v1}, Lcom/chase/sig/android/domain/QuickPayTransaction;->v(Ljava/lang/String;)V

    .line 220
    invoke-virtual {v4, v0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->w(Ljava/lang/String;)V

    .line 223
    :cond_2
    invoke-virtual {v4, v5}, Lcom/chase/sig/android/domain/QuickPayTransaction;->b(Ljava/util/Date;)V

    .line 224
    return-object v4

    .line 193
    :cond_3
    iget-object v2, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->n:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/service/quickpay/PayFromAccount;

    goto/16 :goto_0

    .line 201
    :cond_4
    invoke-virtual {v4, v3}, Lcom/chase/sig/android/domain/QuickPayTransaction;->i(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 219
    :cond_5
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_2

    :cond_6
    move-object v1, v3

    goto :goto_3
.end method

.method protected final e()Z
    .locals 1

    .prologue
    .line 272
    const/4 v0, 0x0

    return v0
.end method

.method protected final f()V
    .locals 5

    .prologue
    const v4, 0x7f090246

    const/16 v3, 0x8

    const/4 v1, 0x0

    .line 86
    invoke-super {p0}, Lcom/chase/sig/android/activity/iy;->f()V

    .line 88
    const v0, 0x7f090237

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/JPSpinner;

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->a:Lcom/chase/sig/android/view/JPSpinner;

    .line 89
    const v0, 0x7f090236

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->b:Landroid/widget/TextView;

    .line 90
    const v0, 0x7f090232

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->c:Landroid/widget/TextView;

    .line 91
    const v0, 0x7f09023d

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->p:Landroid/widget/CheckBox;

    .line 92
    const v0, 0x7f090243

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->r:Landroid/widget/EditText;

    .line 93
    const v0, 0x7f090244

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->q:Landroid/widget/CheckBox;

    .line 95
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "send_money_for_request"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 96
    invoke-virtual {p0, v4}, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const-string v2, "Reset"

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 99
    :cond_0
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->h()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 100
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->p:Landroid/widget/CheckBox;

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setVisibility(I)V

    const v0, 0x7f090233

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->a:Lcom/chase/sig/android/view/JPSpinner;

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/view/JPSpinner;->setVisibility(I)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->b:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->k:Lcom/chase/sig/android/domain/QuickPayTransaction;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/QuickPayTransaction;->G()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f070232

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->setTitle(I)V

    invoke-virtual {p0, v4}, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const-string v2, "Reset"

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 105
    :goto_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->b:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->k:Lcom/chase/sig/android/domain/QuickPayTransaction;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/QuickPayTransaction;->q()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 106
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->k:Lcom/chase/sig/android/domain/QuickPayTransaction;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->D()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->a(Z)V

    .line 107
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->p:Landroid/widget/CheckBox;

    iget-object v2, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->k:Lcom/chase/sig/android/domain/QuickPayTransaction;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/QuickPayTransaction;->D()Z

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 109
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->k:Lcom/chase/sig/android/domain/QuickPayTransaction;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->D()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 110
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->q:Landroid/widget/CheckBox;

    iget-object v2, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->k:Lcom/chase/sig/android/domain/QuickPayTransaction;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/QuickPayTransaction;->C()Z

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    iget-object v2, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->r:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->k:Lcom/chase/sig/android/domain/QuickPayTransaction;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->H()I

    move-result v0

    if-nez v0, :cond_4

    const-string v0, ""

    :goto_1
    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->r:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->k:Lcom/chase/sig/android/domain/QuickPayTransaction;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->C()Z

    move-result v0

    if-nez v0, :cond_5

    const/4 v0, 0x1

    :goto_2
    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setEnabled(Z)V

    invoke-direct {p0}, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->G()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v2

    iget-object v3, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->m:Lcom/chase/sig/android/domain/m;

    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->k:Lcom/chase/sig/android/domain/QuickPayTransaction;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->X()Ljava/lang/String;

    move-result-object v4

    :goto_3
    iget-object v0, v3, Lcom/chase/sig/android/domain/m;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    iget-object v0, v3, Lcom/chase/sig/android/domain/m;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/LabeledValue;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    :goto_4
    invoke-virtual {v2, v1}, Lcom/chase/sig/android/view/JPSpinner;->setSelection(I)V

    .line 113
    :cond_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->k:Lcom/chase/sig/android/domain/QuickPayTransaction;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->x()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 114
    const v0, 0x7f090239

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->k:Lcom/chase/sig/android/domain/QuickPayTransaction;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/QuickPayTransaction;->x()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 116
    :cond_2
    return-void

    .line 102
    :cond_3
    const v0, 0x7f0701f3

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->setTitle(I)V

    goto/16 :goto_0

    .line 110
    :cond_4
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->k:Lcom/chase/sig/android/domain/QuickPayTransaction;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->H()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_5
    move v0, v1

    goto :goto_2

    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_7
    const/4 v1, -0x1

    goto :goto_4
.end method

.method public final g()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 370
    invoke-super {p0}, Lcom/chase/sig/android/activity/iy;->g()Z

    move-result v2

    .line 371
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->h()Z

    move-result v0

    if-nez v0, :cond_2

    .line 372
    const v0, 0x7f090237

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/JPSpinner;

    .line 373
    invoke-virtual {v0}, Lcom/chase/sig/android/view/JPSpinner;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 375
    const v0, 0x7f090235

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->e(I)Z

    move v0, v1

    .line 378
    :goto_0
    iget-object v2, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->p:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 379
    iget-object v2, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->q:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    if-nez v2, :cond_0

    .line 380
    invoke-direct {p0}, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->I()I

    move-result v2

    if-gtz v2, :cond_0

    .line 382
    const v0, 0x7f090242

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->e(I)Z

    move v0, v1

    .line 386
    :cond_0
    invoke-direct {p0}, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->G()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v2

    invoke-virtual {v2}, Lcom/chase/sig/android/view/JPSpinner;->getSelectedItemPosition()I

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_1

    .line 388
    const v0, 0x7f090240

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->e(I)Z

    move v0, v1

    .line 392
    :cond_1
    return v0

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method public final k()V
    .locals 1

    .prologue
    .line 120
    invoke-super {p0}, Lcom/chase/sig/android/activity/iy;->k()V

    .line 121
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->n:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->a(Ljava/util/ArrayList;)V

    .line 122
    return-void
.end method

.method public final k(I)V
    .locals 6
    .parameter

    .prologue
    const v5, 0x7f090239

    const v4, 0x7f090238

    const/16 v3, 0x8

    const v2, 0x7f09023f

    const/4 v1, 0x0

    .line 336
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/service/quickpay/PayFromAccount;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickpay/PayFromAccount;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 337
    invoke-virtual {p0, v4}, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 338
    invoke-virtual {p0, v5}, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 339
    invoke-virtual {p0, v2}, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 340
    invoke-virtual {p0, v2}, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/ChaseApplication;

    invoke-virtual {v1}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v1

    iget-object v1, v1, Lcom/chase/sig/android/domain/o;->c:Lcom/chase/sig/android/service/quickpay/TodoListResponse;

    invoke-virtual {v1}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->j()Ljava/util/Date;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->a(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 346
    :goto_0
    return-void

    .line 342
    :cond_0
    invoke-virtual {p0, v4}, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 343
    invoke-virtual {p0, v5}, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 344
    invoke-virtual {p0, v2}, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 173
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/iy;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 175
    const-string v0, "FOOTNOTE"

    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->o:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    const-string v0, "PAY_FROM_ACCOUNTS"

    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;->n:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 177
    return-void
.end method
