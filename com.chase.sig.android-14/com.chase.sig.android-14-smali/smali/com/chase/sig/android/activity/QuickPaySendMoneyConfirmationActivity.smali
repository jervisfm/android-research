.class public Lcom/chase/sig/android/activity/QuickPaySendMoneyConfirmationActivity;
.super Lcom/chase/sig/android/activity/hm;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/chase/sig/android/activity/hm;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 4
    .parameter

    .prologue
    .line 20
    invoke-static {}, Lcom/chase/sig/android/c;->a()V

    .line 21
    const v0, 0x7f030082

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPaySendMoneyConfirmationActivity;->b(I)V

    .line 22
    const v0, 0x7f070074

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPaySendMoneyConfirmationActivity;->setTitle(I)V

    .line 24
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPaySendMoneyConfirmationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "quickPayConfirm"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionConfirmResponse;

    .line 27
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPaySendMoneyConfirmationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "sendTransaction"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/domain/QuickPayTransaction;

    .line 31
    invoke-virtual {v1}, Lcom/chase/sig/android/domain/QuickPayTransaction;->T()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/QuickPayTransaction;->U()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/QuickPayTransaction;->V()Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x1

    :goto_0
    if-eqz v2, :cond_0

    if-nez p1, :cond_0

    .line 32
    invoke-virtual {v1}, Lcom/chase/sig/android/domain/QuickPayTransaction;->T()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/chase/sig/android/activity/QuickPaySendMoneyConfirmationActivity;->f(Ljava/lang/String;)V

    .line 35
    :cond_0
    invoke-virtual {v1}, Lcom/chase/sig/android/domain/QuickPayTransaction;->z()V

    .line 36
    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionConfirmResponse;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/domain/QuickPayTransaction;->g(Ljava/lang/String;)V

    .line 37
    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionConfirmResponse;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/domain/QuickPayTransaction;->h(Ljava/lang/String;)V

    .line 38
    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionConfirmResponse;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->f(Ljava/lang/String;)V

    .line 40
    new-instance v2, Lcom/chase/sig/android/view/aa;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPaySendMoneyConfirmationActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/ChaseApplication;

    invoke-direct {v2, v3, v1, v0}, Lcom/chase/sig/android/view/aa;-><init>(Landroid/content/Context;Lcom/chase/sig/android/domain/QuickPayTransaction;Lcom/chase/sig/android/ChaseApplication;)V

    .line 42
    const v0, 0x7f0901ce

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPaySendMoneyConfirmationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 43
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 45
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPaySendMoneyConfirmationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "qp_edit_payment"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/QuickPayTransaction;->R()Z

    move-result v0

    if-nez v0, :cond_2

    .line 47
    const-class v0, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsActivity;

    const-string v1, "Pending Transactions"

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/QuickPaySendMoneyConfirmationActivity;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 52
    :goto_1
    return-void

    .line 31
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 50
    :cond_2
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPaySendMoneyConfirmationActivity;->d()V

    goto :goto_1
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 61
    invoke-virtual {p0, p1}, Lcom/chase/sig/android/activity/QuickPaySendMoneyConfirmationActivity;->j(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 70
    :goto_0
    return v0

    .line 65
    :cond_0
    const/4 v1, 0x4

    if-ne p1, v1, :cond_1

    .line 66
    const-class v1, Lcom/chase/sig/android/activity/QuickPayHomeActivity;

    invoke-static {p1, p0, v1}, Lcom/chase/sig/android/a/a;->a(ILcom/chase/sig/android/activity/eb;Ljava/lang/Class;)V

    goto :goto_0

    .line 70
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/chase/sig/android/activity/hm;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method
