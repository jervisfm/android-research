.class public Lcom/chase/sig/android/activity/QuickPaySendMoneyVerifyActivity$a;
.super Lcom/chase/sig/android/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/QuickPaySendMoneyVerifyActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/d",
        "<",
        "Lcom/chase/sig/android/activity/QuickPaySendMoneyVerifyActivity;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionConfirmResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 85
    invoke-direct {p0}, Lcom/chase/sig/android/d;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 85
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickPaySendMoneyVerifyActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/QuickPaySendMoneyVerifyActivity;->A()Lcom/chase/sig/android/service/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/service/n;->j()Lcom/chase/sig/android/service/y;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickPaySendMoneyVerifyActivity;

    iget-object v0, v0, Lcom/chase/sig/android/activity/QuickPaySendMoneyVerifyActivity;->a:Lcom/chase/sig/android/domain/QuickPayTransaction;

    invoke-static {v0}, Lcom/chase/sig/android/service/y;->b(Lcom/chase/sig/android/domain/QuickPayTransaction;)Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionConfirmResponse;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 4
    .parameter

    .prologue
    const/4 v3, 0x1

    .line 85
    check-cast p1, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionConfirmResponse;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionConfirmResponse;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickPaySendMoneyVerifyActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionConfirmResponse;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/QuickPaySendMoneyVerifyActivity;->c(Ljava/util/List;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickPaySendMoneyVerifyActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/QuickPaySendMoneyVerifyActivity;->A()Lcom/chase/sig/android/service/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/service/n;->j()Lcom/chase/sig/android/service/y;

    invoke-static {v3}, Lcom/chase/sig/android/service/y;->c(I)Lcom/chase/sig/android/service/quickpay/TodoListResponse;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->e()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickPaySendMoneyVerifyActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/QuickPaySendMoneyVerifyActivity;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iput-object v1, v0, Lcom/chase/sig/android/domain/o;->c:Lcom/chase/sig/android/service/quickpay/TodoListResponse;

    :cond_1
    new-instance v1, Landroid/content/Intent;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    const-class v2, Lcom/chase/sig/android/activity/QuickPaySendMoneyConfirmationActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v0, "quickPayConfirm"

    invoke-virtual {v1, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v2, "sendTransaction"

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickPaySendMoneyVerifyActivity;

    iget-object v0, v0, Lcom/chase/sig/android/activity/QuickPaySendMoneyVerifyActivity;->a:Lcom/chase/sig/android/domain/QuickPayTransaction;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickPaySendMoneyVerifyActivity;

    iget-object v0, v0, Lcom/chase/sig/android/activity/QuickPaySendMoneyVerifyActivity;->a:Lcom/chase/sig/android/domain/QuickPayTransaction;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->w()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    const-string v0, "qp_edit_payment"

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_2
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickPaySendMoneyVerifyActivity;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/QuickPaySendMoneyVerifyActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
