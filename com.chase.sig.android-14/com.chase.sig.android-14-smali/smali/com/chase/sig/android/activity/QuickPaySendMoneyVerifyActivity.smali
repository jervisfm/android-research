.class public Lcom/chase/sig/android/activity/QuickPaySendMoneyVerifyActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$e;
.implements Lcom/chase/sig/android/c$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/QuickPaySendMoneyVerifyActivity$a;
    }
.end annotation


# instance fields
.field a:Lcom/chase/sig/android/domain/QuickPayTransaction;

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyVerifyActivity;->a:Lcom/chase/sig/android/domain/QuickPayTransaction;

    .line 85
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 124
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/chase/sig/android/activity/QuickPayHomeActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 126
    const/high16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 127
    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPaySendMoneyVerifyActivity;->startActivity(Landroid/content/Intent;)V

    .line 128
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 6
    .parameter

    .prologue
    .line 31
    const v0, 0x7f03008f

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPaySendMoneyVerifyActivity;->b(I)V

    .line 32
    const v0, 0x7f0701ff

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPaySendMoneyVerifyActivity;->setTitle(I)V

    .line 34
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPaySendMoneyVerifyActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "sendTransaction"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/QuickPayTransaction;

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyVerifyActivity;->a:Lcom/chase/sig/android/domain/QuickPayTransaction;

    .line 38
    new-instance v1, Lcom/chase/sig/android/view/aa;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPaySendMoneyVerifyActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyVerifyActivity;->a:Lcom/chase/sig/android/domain/QuickPayTransaction;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/ChaseApplication;

    invoke-direct {v1, v2, v3, v0}, Lcom/chase/sig/android/view/aa;-><init>(Landroid/content/Context;Lcom/chase/sig/android/domain/QuickPayTransaction;Lcom/chase/sig/android/ChaseApplication;)V

    .line 41
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPaySendMoneyVerifyActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "FOOTNOTE"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyVerifyActivity;->b:Ljava/lang/String;

    .line 42
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyVerifyActivity;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f090248

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPaySendMoneyVerifyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPaySendMoneyVerifyActivity;->n()Landroid/view/ViewGroup;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getPaddingLeft()I

    move-result v2

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPaySendMoneyVerifyActivity;->n()Landroid/view/ViewGroup;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getPaddingTop()I

    move-result v3

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPaySendMoneyVerifyActivity;->n()Landroid/view/ViewGroup;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getPaddingRight()I

    move-result v4

    const/16 v5, 0x21

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/view/View;->setPadding(IIII)V

    const v0, 0x7f0701ea

    iget-object v2, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyVerifyActivity;->b:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-virtual {p0, v0, v2, v3}, Lcom/chase/sig/android/activity/QuickPaySendMoneyVerifyActivity;->a(ILjava/lang/String;Z)V

    .line 44
    :cond_0
    const v0, 0x7f0901ce

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPaySendMoneyVerifyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 45
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 47
    const v0, 0x7f09024a

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPaySendMoneyVerifyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 48
    new-instance v1, Lcom/chase/sig/android/activity/iw;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/iw;-><init>(Lcom/chase/sig/android/activity/QuickPaySendMoneyVerifyActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 56
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickPaySendMoneyVerifyActivity;->a:Lcom/chase/sig/android/domain/QuickPayTransaction;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/QuickPayTransaction;->B()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 57
    const v1, 0x7f070203

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/QuickPaySendMoneyVerifyActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 62
    :goto_0
    const v0, 0x7f090249

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPaySendMoneyVerifyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 64
    new-instance v1, Lcom/chase/sig/android/activity/ix;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/ix;-><init>(Lcom/chase/sig/android/activity/QuickPaySendMoneyVerifyActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 74
    return-void

    .line 59
    :cond_1
    const v1, 0x7f0701f3

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/QuickPaySendMoneyVerifyActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
