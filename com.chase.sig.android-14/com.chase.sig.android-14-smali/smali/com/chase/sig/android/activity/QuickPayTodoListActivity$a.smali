.class public Lcom/chase/sig/android/activity/QuickPayTodoListActivity$a;
.super Lcom/chase/sig/android/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/QuickPayTodoListActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/d",
        "<",
        "Lcom/chase/sig/android/activity/QuickPayTodoListActivity;",
        "Ljava/lang/Object;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;",
        ">;"
    }
.end annotation


# instance fields
.field a:Lcom/chase/sig/android/domain/QuickPayTransaction;

.field e:Lcom/chase/sig/android/service/TodoItem;

.field f:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 289
    invoke-direct {p0}, Lcom/chase/sig/android/d;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .parameter

    .prologue
    .line 289
    const/4 v0, 0x0

    aget-object v0, p1, v0

    check-cast v0, Lcom/chase/sig/android/domain/QuickPayTransaction;

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickPayTodoListActivity$a;->a:Lcom/chase/sig/android/domain/QuickPayTransaction;

    const/4 v0, 0x1

    aget-object v0, p1, v0

    check-cast v0, Lcom/chase/sig/android/service/TodoItem;

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickPayTodoListActivity$a;->e:Lcom/chase/sig/android/service/TodoItem;

    const/4 v0, 0x2

    aget-object v0, p1, v0

    check-cast v0, Ljava/lang/Class;

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickPayTodoListActivity$a;->f:Ljava/lang/Class;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->A()Lcom/chase/sig/android/service/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/service/n;->j()Lcom/chase/sig/android/service/y;

    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayTodoListActivity$a;->e:Lcom/chase/sig/android/service/TodoItem;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/TodoItem;->f()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickPayTodoListActivity$a;->e:Lcom/chase/sig/android/service/TodoItem;

    invoke-virtual {v1}, Lcom/chase/sig/android/service/TodoItem;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/domain/QuickPayActivityType;->valueOf(Ljava/lang/String;)Lcom/chase/sig/android/domain/QuickPayActivityType;

    move-result-object v1

    iget-object v2, p0, Lcom/chase/sig/android/activity/QuickPayTodoListActivity$a;->e:Lcom/chase/sig/android/service/TodoItem;

    invoke-virtual {v2}, Lcom/chase/sig/android/service/TodoItem;->h()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/chase/sig/android/service/y;->a(Ljava/lang/String;Lcom/chase/sig/android/domain/QuickPayActivityType;Ljava/lang/String;)Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 5
    .parameter

    .prologue
    const/4 v4, 0x1

    .line 289
    check-cast p1, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->b(Ljava/util/List;)V

    :goto_0
    return-void

    :cond_0
    new-instance v1, Landroid/content/Intent;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    iget-object v2, p0, Lcom/chase/sig/android/activity/QuickPayTodoListActivity$a;->f:Ljava/lang/Class;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v0, "activity_detail_id"

    iget-object v2, p0, Lcom/chase/sig/android/activity/QuickPayTodoListActivity$a;->e:Lcom/chase/sig/android/service/TodoItem;

    invoke-virtual {v2}, Lcom/chase/sig/android/service/TodoItem;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "activity_detail_type"

    iget-object v2, p0, Lcom/chase/sig/android/activity/QuickPayTodoListActivity$a;->e:Lcom/chase/sig/android/service/TodoItem;

    invoke-virtual {v2}, Lcom/chase/sig/android/service/TodoItem;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "ACTIVITY_DETAIL_ORIGIN"

    invoke-virtual {v1, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->c()Ljava/util/ArrayList;

    move-result-object v0

    const-string v2, "quick_pay_details"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    invoke-static {v0}, Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;->a(Ljava/util/ArrayList;)Lcom/chase/sig/android/domain/QuickPayRecipient;

    move-result-object v2

    iget-object v3, p0, Lcom/chase/sig/android/activity/QuickPayTodoListActivity$a;->a:Lcom/chase/sig/android/domain/QuickPayTransaction;

    invoke-static {v0, v3}, Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;->a(Ljava/util/List;Lcom/chase/sig/android/domain/QuickPayTransaction;)Lcom/chase/sig/android/domain/QuickPayTransaction;

    const-string v0, "quick_pay_transaction"

    iget-object v3, p0, Lcom/chase/sig/android/activity/QuickPayTodoListActivity$a;->a:Lcom/chase/sig/android/domain/QuickPayTransaction;

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v0, "recipient"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v0, "send_money_for_request"

    invoke-virtual {v1, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "called_from_todo_list"

    invoke-virtual {v1, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
