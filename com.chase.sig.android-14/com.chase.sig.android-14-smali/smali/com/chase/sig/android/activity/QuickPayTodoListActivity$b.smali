.class public Lcom/chase/sig/android/activity/QuickPayTodoListActivity$b;
.super Lcom/chase/sig/android/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/QuickPayTodoListActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/d",
        "<",
        "Lcom/chase/sig/android/activity/QuickPayTodoListActivity;",
        "Ljava/lang/Object;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/service/quickpay/TodoListResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 264
    invoke-direct {p0}, Lcom/chase/sig/android/d;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .parameter

    .prologue
    .line 264
    const/4 v0, 0x0

    aget-object v0, p1, v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->A()Lcom/chase/sig/android/service/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/service/n;->j()Lcom/chase/sig/android/service/y;

    invoke-static {v1}, Lcom/chase/sig/android/service/y;->c(I)Lcom/chase/sig/android/service/quickpay/TodoListResponse;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 2
    .parameter

    .prologue
    .line 264
    check-cast p1, Lcom/chase/sig/android/service/quickpay/TodoListResponse;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iput-object p1, v0, Lcom/chase/sig/android/domain/o;->c:Lcom/chase/sig/android/service/quickpay/TodoListResponse;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->c()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->a(Lcom/chase/sig/android/activity/QuickPayTodoListActivity;)Z

    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->e()V

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->s()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->s()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->d(Ljava/lang/String;)V

    :cond_1
    return-void
.end method
