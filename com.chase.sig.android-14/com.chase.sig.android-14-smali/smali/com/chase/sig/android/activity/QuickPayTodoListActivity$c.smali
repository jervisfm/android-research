.class public final Lcom/chase/sig/android/activity/QuickPayTodoListActivity$c;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/QuickPayTodoListActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "c"
.end annotation


# instance fields
.field a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/service/TodoItem;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic b:Lcom/chase/sig/android/activity/QuickPayTodoListActivity;


# direct methods
.method public constructor <init>(Lcom/chase/sig/android/activity/QuickPayTodoListActivity;Ljava/util/List;)V
    .locals 0
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/service/TodoItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 146
    iput-object p1, p0, Lcom/chase/sig/android/activity/QuickPayTodoListActivity$c;->b:Lcom/chase/sig/android/activity/QuickPayTodoListActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 147
    iput-object p2, p0, Lcom/chase/sig/android/activity/QuickPayTodoListActivity$c;->a:Ljava/util/List;

    .line 148
    return-void
.end method

.method private a(Landroid/view/View;Lcom/chase/sig/android/service/TodoItem;ILjava/lang/String;Ljava/lang/Class;Lcom/chase/sig/android/domain/QuickPayTransaction;ZI)Landroid/view/View;
    .locals 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/chase/sig/android/service/TodoItem;",
            "I",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<*>;",
            "Lcom/chase/sig/android/domain/QuickPayTransaction;",
            "ZI)",
            "Landroid/view/View;"
        }
    .end annotation

    .prologue
    .line 229
    invoke-virtual {p1, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .line 230
    const/4 v0, 0x1

    invoke-virtual {v7, v0}, Landroid/view/View;->setFocusable(Z)V

    .line 231
    const v0, 0x7f090009

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 232
    const v0, 0x7f0902aa

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 233
    const v0, 0x7f0902aa

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 234
    const v0, 0x7f0902aa

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 235
    new-instance v0, Lcom/chase/sig/android/activity/ji;

    move-object v1, p0

    move v2, p7

    move-object v3, p6

    move-object v4, p2

    move-object v5, p5

    move/from16 v6, p8

    invoke-direct/range {v0 .. v6}, Lcom/chase/sig/android/activity/ji;-><init>(Lcom/chase/sig/android/activity/QuickPayTodoListActivity$c;ZLcom/chase/sig/android/domain/QuickPayTransaction;Lcom/chase/sig/android/service/TodoItem;Ljava/lang/Class;I)V

    invoke-virtual {v7, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 251
    invoke-static {p4}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v7, p4}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 252
    :cond_0
    return-object v7
.end method


# virtual methods
.method public final areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 167
    const/4 v0, 0x0

    return v0
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayTodoListActivity$c;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 157
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayTodoListActivity$c;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2
    .parameter

    .prologue
    .line 162
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayTodoListActivity$c;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/service/TodoItem;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/TodoItem;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .parameter
    .parameter
    .parameter

    .prologue
    .line 177
    .line 178
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayTodoListActivity$c;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/chase/sig/android/service/TodoItem;

    .line 179
    if-nez p2, :cond_2

    .line 180
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayTodoListActivity$c;->b:Lcom/chase/sig/android/activity/QuickPayTodoListActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0300c0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 182
    :goto_0
    const v3, 0x7f090280

    invoke-virtual {v2}, Lcom/chase/sig/android/service/TodoItem;->a()Ljava/lang/String;

    move-result-object v4

    const-class v5, Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;

    sget-object v0, Lcom/chase/sig/android/domain/QuickPayActivityType;->d:Lcom/chase/sig/android/domain/QuickPayActivityType;

    invoke-virtual {v2}, Lcom/chase/sig/android/service/TodoItem;->g()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/chase/sig/android/domain/QuickPayActivityType;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v6, Lcom/chase/sig/android/domain/QuickPayTransaction;

    invoke-direct {v6}, Lcom/chase/sig/android/domain/QuickPayTransaction;-><init>()V

    :goto_1
    const/4 v7, 0x1

    const/4 v8, -0x7

    move-object v0, p0

    invoke-direct/range {v0 .. v8}, Lcom/chase/sig/android/activity/QuickPayTodoListActivity$c;->a(Landroid/view/View;Lcom/chase/sig/android/service/TodoItem;ILjava/lang/String;Ljava/lang/Class;Lcom/chase/sig/android/domain/QuickPayTransaction;ZI)Landroid/view/View;

    .line 185
    sget-object v0, Lcom/chase/sig/android/domain/QuickPayActivityType;->d:Lcom/chase/sig/android/domain/QuickPayActivityType;

    invoke-virtual {v2}, Lcom/chase/sig/android/service/TodoItem;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/domain/QuickPayActivityType;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v6, Lcom/chase/sig/android/domain/QuickPayTransaction;

    invoke-direct {v6}, Lcom/chase/sig/android/domain/QuickPayTransaction;-><init>()V

    const/4 v0, 0x1

    invoke-virtual {v6, v0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->e(Z)V

    const v3, 0x7f0902ba

    invoke-virtual {v2}, Lcom/chase/sig/android/service/TodoItem;->b()Ljava/lang/String;

    move-result-object v4

    const-class v5, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;

    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayTodoListActivity$c;->b:Lcom/chase/sig/android/activity/QuickPayTodoListActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->c:Lcom/chase/sig/android/service/quickpay/TodoListResponse;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->q()Z

    move-result v7

    const/4 v8, -0x8

    move-object v0, p0

    invoke-direct/range {v0 .. v8}, Lcom/chase/sig/android/activity/QuickPayTodoListActivity$c;->a(Landroid/view/View;Lcom/chase/sig/android/service/TodoItem;ILjava/lang/String;Ljava/lang/Class;Lcom/chase/sig/android/domain/QuickPayTransaction;ZI)Landroid/view/View;

    move-result-object v0

    move-object v3, v0

    :goto_2
    const v0, 0x7f0902ac

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v4, -0xff95cd

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {v2}, Lcom/chase/sig/android/service/TodoItem;->c()Lcom/chase/sig/android/util/Dollar;

    move-result-object v4

    invoke-virtual {v4}, Lcom/chase/sig/android/util/Dollar;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0902ab

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v2}, Lcom/chase/sig/android/service/TodoItem;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0902aa

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v3, -0xff95cd

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 186
    const v3, 0x7f0902bb

    invoke-virtual {v2}, Lcom/chase/sig/android/service/TodoItem;->e()Ljava/lang/String;

    move-result-object v4

    const-class v5, Lcom/chase/sig/android/activity/QuickPayDeclineVerifyActivity;

    new-instance v6, Lcom/chase/sig/android/domain/QuickPayDeclineTransaction;

    invoke-direct {v6}, Lcom/chase/sig/android/domain/QuickPayDeclineTransaction;-><init>()V

    const/4 v7, 0x1

    const/4 v8, -0x7

    move-object v0, p0

    invoke-direct/range {v0 .. v8}, Lcom/chase/sig/android/activity/QuickPayTodoListActivity$c;->a(Landroid/view/View;Lcom/chase/sig/android/service/TodoItem;ILjava/lang/String;Ljava/lang/Class;Lcom/chase/sig/android/domain/QuickPayTransaction;ZI)Landroid/view/View;

    .line 190
    return-object v1

    .line 182
    :cond_0
    new-instance v6, Lcom/chase/sig/android/domain/QuickPayAcceptMoneyTransaction;

    invoke-direct {v6}, Lcom/chase/sig/android/domain/QuickPayAcceptMoneyTransaction;-><init>()V

    goto/16 :goto_1

    .line 185
    :cond_1
    const v3, 0x7f0902ba

    invoke-virtual {v2}, Lcom/chase/sig/android/service/TodoItem;->b()Ljava/lang/String;

    move-result-object v4

    const-class v5, Lcom/chase/sig/android/activity/QuickPayAcceptMoneyVerifyActivity;

    new-instance v6, Lcom/chase/sig/android/domain/QuickPayAcceptMoneyTransaction;

    invoke-direct {v6}, Lcom/chase/sig/android/domain/QuickPayAcceptMoneyTransaction;-><init>()V

    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayTodoListActivity$c;->b:Lcom/chase/sig/android/activity/QuickPayTodoListActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->c:Lcom/chase/sig/android/service/quickpay/TodoListResponse;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->r()Z

    move-result v7

    const/16 v8, -0x9

    move-object v0, p0

    invoke-direct/range {v0 .. v8}, Lcom/chase/sig/android/activity/QuickPayTodoListActivity$c;->a(Landroid/view/View;Lcom/chase/sig/android/service/TodoItem;ILjava/lang/String;Ljava/lang/Class;Lcom/chase/sig/android/domain/QuickPayTransaction;ZI)Landroid/view/View;

    move-result-object v0

    move-object v3, v0

    goto :goto_2

    :cond_2
    move-object v1, p2

    goto/16 :goto_0
.end method

.method public final isEnabled(I)Z
    .locals 1
    .parameter

    .prologue
    .line 172
    const/4 v0, 0x0

    return v0
.end method
