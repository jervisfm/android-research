.class public Lcom/chase/sig/android/activity/QuickPayTodoListActivity;
.super Lcom/chase/sig/android/activity/eb;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$e;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/QuickPayTodoListActivity$a;,
        Lcom/chase/sig/android/activity/QuickPayTodoListActivity$b;,
        Lcom/chase/sig/android/activity/QuickPayTodoListActivity$c;
    }
.end annotation


# instance fields
.field private a:Landroid/view/View;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/service/TodoItem;",
            ">;"
        }
    .end annotation
.end field

.field private c:Landroid/widget/ListView;

.field private d:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/chase/sig/android/activity/eb;-><init>()V

    .line 35
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->b:Ljava/util/List;

    .line 289
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/QuickPayTodoListActivity;)Z
    .locals 1
    .parameter

    .prologue
    .line 30
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->d:Z

    return v0
.end method

.method private f()Ljava/lang/String;
    .locals 4

    .prologue
    .line 66
    const-string v0, "To Do List (%d)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v3

    iget-object v3, v3, Lcom/chase/sig/android/domain/o;->c:Lcom/chase/sig/android/service/quickpay/TodoListResponse;

    invoke-virtual {v3}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->d()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 4
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 44
    const v0, 0x7f030091

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->b(I)V

    .line 46
    const v0, 0x7f090251

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->c:Landroid/widget/ListView;

    .line 48
    if-eqz p1, :cond_1

    .line 49
    const v0, 0x7f09024f

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 50
    const-string v0, "todoItems"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->b:Ljava/util/List;

    .line 51
    iput-boolean v3, p0, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->d:Z

    .line 52
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->e()V

    .line 62
    :cond_0
    :goto_0
    const v0, 0x7f09024e

    const-class v1, Lcom/chase/sig/android/activity/QuickPayHomeActivity;

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/a/f;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 63
    return-void

    .line 56
    :cond_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->e:Lcom/chase/sig/android/activity/mc;

    const-class v1, Lcom/chase/sig/android/activity/QuickPayTodoListActivity$b;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/mc;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/ae;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/activity/QuickPayTodoListActivity$b;

    .line 57
    invoke-virtual {v0}, Lcom/chase/sig/android/activity/QuickPayTodoListActivity$b;->d()Z

    move-result v1

    if-nez v1, :cond_0

    .line 58
    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/QuickPayTodoListActivity$b;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public final d()Lcom/chase/sig/android/service/quickpay/TodoListResponse;
    .locals 1

    .prologue
    .line 70
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->c:Lcom/chase/sig/android/service/quickpay/TodoListResponse;

    return-object v0
.end method

.method public final e()V
    .locals 7

    .prologue
    const v6, 0x7f0901dc

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 75
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->c:Lcom/chase/sig/android/service/quickpay/TodoListResponse;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->c:Lcom/chase/sig/android/service/quickpay/TodoListResponse;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->d()I

    move-result v0

    if-lez v0, :cond_0

    .line 76
    const v0, 0x7f09024f

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 77
    invoke-direct {p0}, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 78
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 81
    :cond_0
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->c:Lcom/chase/sig/android/service/quickpay/TodoListResponse;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->d()I

    move-result v0

    if-gtz v0, :cond_1

    .line 82
    const v0, 0x7f090250

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 83
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v1

    iget-object v1, v1, Lcom/chase/sig/android/domain/o;->c:Lcom/chase/sig/android/service/quickpay/TodoListResponse;

    invoke-virtual {v1}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 84
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 140
    :goto_0
    return-void

    .line 88
    :cond_1
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->c:Lcom/chase/sig/android/service/quickpay/TodoListResponse;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->l()I

    move-result v0

    if-eq v0, v4, :cond_2

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->c:Lcom/chase/sig/android/service/quickpay/TodoListResponse;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->l()I

    move-result v0

    if-nez v0, :cond_5

    .line 89
    :cond_2
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->c:Lcom/chase/sig/android/service/quickpay/TodoListResponse;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->c()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->b:Ljava/util/List;

    .line 94
    :cond_3
    :goto_1
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->c:Lcom/chase/sig/android/service/quickpay/TodoListResponse;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->d()I

    move-result v1

    .line 96
    const/16 v0, 0x19

    if-le v1, v0, :cond_7

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->c:Lcom/chase/sig/android/service/quickpay/TodoListResponse;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->k()I

    move-result v0

    if-eqz v0, :cond_7

    .line 97
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->a:Landroid/view/View;

    if-nez v0, :cond_6

    .line 98
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f0300c1

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->a:Landroid/view/View;

    .line 99
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->a:Landroid/view/View;

    const v2, 0x7f0901db

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 100
    const v2, 0x7f0701fc

    invoke-virtual {p0, v2}, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 102
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->a:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 103
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " items total,  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v1, v1, -0x19

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  to load"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 105
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->a:Landroid/view/View;

    new-instance v1, Lcom/chase/sig/android/activity/jh;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/jh;-><init>(Lcom/chase/sig/android/activity/QuickPayTodoListActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 115
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->c:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;)V

    .line 126
    :goto_2
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->c:Landroid/widget/ListView;

    new-instance v1, Lcom/chase/sig/android/activity/QuickPayTodoListActivity$c;

    iget-object v2, p0, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->b:Ljava/util/List;

    invoke-direct {v1, p0, v2}, Lcom/chase/sig/android/activity/QuickPayTodoListActivity$c;-><init>(Lcom/chase/sig/android/activity/QuickPayTodoListActivity;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 127
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->c:Landroid/widget/ListView;

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 128
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->c:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v2

    iget-object v2, v2, Lcom/chase/sig/android/domain/o;->c:Lcom/chase/sig/android/service/quickpay/TodoListResponse;

    invoke-virtual {v2}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->d()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1, v5}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    .line 130
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->c:Lcom/chase/sig/android/service/quickpay/TodoListResponse;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->s()Ljava/lang/String;

    move-result-object v0

    .line 131
    invoke-static {v0}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 132
    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->d(Ljava/lang/String;)V

    .line 134
    const v0, 0x7f09024d

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 135
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getPaddingLeft()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getPaddingTop()I

    move-result v2

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getPaddingRight()I

    move-result v3

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getPaddingLeft()I

    move-result v4

    mul-int/lit8 v4, v4, 0x5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/ViewGroup;->setPadding(IIII)V

    .line 139
    :cond_4
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->c:Landroid/widget/ListView;

    invoke-virtual {v0, v5}, Landroid/widget/ListView;->setVisibility(I)V

    goto/16 :goto_0

    .line 90
    :cond_5
    iget-boolean v0, p0, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->d:Z

    if-eqz v0, :cond_3

    .line 91
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->b:Ljava/util/List;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v1

    iget-object v1, v1, Lcom/chase/sig/android/domain/o;->c:Lcom/chase/sig/android/service/quickpay/TodoListResponse;

    invoke-virtual {v1}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->c()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto/16 :goto_1

    .line 117
    :cond_6
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->c:Lcom/chase/sig/android/service/quickpay/TodoListResponse;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->l()I

    move-result v0

    mul-int/lit8 v0, v0, 0x19

    sub-int v2, v1, v0

    .line 119
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->a:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " items total,  "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  to load"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 123
    :cond_7
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->c:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->removeFooterView(Landroid/view/View;)Z

    goto/16 :goto_2
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 336
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/eb;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 337
    const-string v1, "todoItems"

    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->b:Ljava/util/List;

    check-cast v0, Ljava/io/Serializable;

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 338
    return-void
.end method
