.class public Lcom/chase/sig/android/activity/QuickPayViewDetailActivity$a;
.super Lcom/chase/sig/android/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/QuickPayViewDetailActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/d",
        "<",
        "Lcom/chase/sig/android/activity/QuickPayViewDetailActivity;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 81
    invoke-direct {p0}, Lcom/chase/sig/android/d;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .parameter

    .prologue
    .line 81
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickPayViewDetailActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/QuickPayViewDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "ACTIVITY_DETAIL_ID"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "ACTIVITY_DETAIL_TYPE"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "IS_INVOICE"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickPayViewDetailActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/QuickPayViewDetailActivity;->A()Lcom/chase/sig/android/service/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/service/n;->j()Lcom/chase/sig/android/service/y;

    invoke-static {v2}, Lcom/chase/sig/android/domain/QuickPayActivityType;->valueOf(Ljava/lang/String;)Lcom/chase/sig/android/domain/QuickPayActivityType;

    move-result-object v0

    invoke-static {v1, v0, v3}, Lcom/chase/sig/android/service/y;->a(Ljava/lang/String;Lcom/chase/sig/android/domain/QuickPayActivityType;Ljava/lang/String;)Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;

    move-result-object v0

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Activity cannot start without required extras"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 2
    .parameter

    .prologue
    .line 81
    check-cast p1, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickPayViewDetailActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/QuickPayViewDetailActivity;->b(Ljava/util/List;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickPayViewDetailActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->c()Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, v0, Lcom/chase/sig/android/activity/QuickPayViewDetailActivity;->a:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickPayViewDetailActivity;

    iget-object v1, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v1, Lcom/chase/sig/android/activity/QuickPayViewDetailActivity;

    iget-object v1, v1, Lcom/chase/sig/android/activity/QuickPayViewDetailActivity;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/QuickPayViewDetailActivity;->a(Ljava/util/List;)V

    goto :goto_0
.end method
