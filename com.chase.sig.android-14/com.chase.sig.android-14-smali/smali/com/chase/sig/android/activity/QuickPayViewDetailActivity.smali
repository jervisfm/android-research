.class public Lcom/chase/sig/android/activity/QuickPayViewDetailActivity;
.super Lcom/chase/sig/android/activity/eb;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$e;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/QuickPayViewDetailActivity$a;,
        Lcom/chase/sig/android/activity/QuickPayViewDetailActivity$b;
    }
.end annotation


# instance fields
.field protected a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/chase/sig/android/domain/QuickPayActivityItem;",
            ">;"
        }
    .end annotation
.end field

.field protected b:Z

.field protected c:Z

.field protected d:Lcom/chase/sig/android/domain/QuickPayActivityItem;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/chase/sig/android/activity/eb;-><init>()V

    .line 32
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/chase/sig/android/activity/QuickPayViewDetailActivity;->c:Z

    .line 81
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 38
    const v0, 0x7f030085

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayViewDetailActivity;->b(I)V

    .line 39
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPayViewDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ACTIVITY_DETAIL_ORIGIN"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/chase/sig/android/activity/QuickPayViewDetailActivity;->b:Z

    .line 40
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPayViewDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ACTIVITY_DETAIL_OLD"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/chase/sig/android/activity/QuickPayViewDetailActivity;->c:Z

    .line 41
    if-eqz p1, :cond_1

    .line 42
    const-string v0, "bundle_details"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickPayViewDetailActivity;->a:Ljava/util/ArrayList;

    .line 45
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayViewDetailActivity;->a:Ljava/util/ArrayList;

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayViewDetailActivity;->a(Ljava/util/List;)V

    .line 52
    :cond_0
    :goto_0
    return-void

    .line 47
    :cond_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayViewDetailActivity;->e:Lcom/chase/sig/android/activity/mc;

    const-class v1, Lcom/chase/sig/android/activity/QuickPayViewDetailActivity$a;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/mc;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/ae;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/activity/QuickPayViewDetailActivity$a;

    .line 48
    invoke-virtual {v0}, Lcom/chase/sig/android/activity/QuickPayViewDetailActivity$a;->d()Z

    move-result v1

    if-nez v1, :cond_0

    .line 49
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/QuickPayViewDetailActivity$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3
    .parameter

    .prologue
    .line 146
    new-instance v0, Lcom/chase/sig/android/view/k$a;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/view/k$a;-><init>(Landroid/content/Context;)V

    .line 147
    invoke-virtual {v0, p1}, Lcom/chase/sig/android/view/k$a;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/k$a;

    .line 148
    const-string v1, "OK"

    new-instance v2, Lcom/chase/sig/android/activity/jk;

    invoke-direct {v2, p0}, Lcom/chase/sig/android/activity/jk;-><init>(Lcom/chase/sig/android/activity/QuickPayViewDetailActivity;)V

    invoke-virtual {v0, v1, v2}, Lcom/chase/sig/android/view/k$a;->a(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    .line 155
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/k$a;->d(I)Lcom/chase/sig/android/view/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/view/k;->show()V

    .line 156
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 6
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/QuickPayActivityItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    const v5, 0x7f0902a4

    const/4 v4, 0x0

    .line 114
    if-eqz p1, :cond_2

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 116
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/QuickPayActivityItem;

    sget-object v1, Lcom/chase/sig/android/domain/QuickPayActivityType;->d:Lcom/chase/sig/android/domain/QuickPayActivityType;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/QuickPayActivityType;->a(Lcom/chase/sig/android/domain/QuickPayActivityItem;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-boolean v1, p0, Lcom/chase/sig/android/activity/QuickPayViewDetailActivity;->c:Z

    if-eqz v1, :cond_3

    const v0, 0x7f0701e6

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayViewDetailActivity;->setTitle(I)V

    .line 117
    :cond_0
    :goto_0
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/QuickPayActivityItem;

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickPayViewDetailActivity;->d:Lcom/chase/sig/android/domain/QuickPayActivityItem;

    .line 118
    new-instance v2, Lcom/chase/sig/android/view/aa;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPayViewDetailActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/QuickPayActivityItem;

    iget-boolean v4, p0, Lcom/chase/sig/android/activity/QuickPayViewDetailActivity;->b:Z

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/ChaseApplication;

    invoke-direct {v2, v3, v0, v4, v1}, Lcom/chase/sig/android/view/aa;-><init>(Landroid/content/Context;Lcom/chase/sig/android/domain/QuickPayActivityItem;ZLcom/chase/sig/android/ChaseApplication;)V

    .line 121
    const v0, 0x7f0901ce

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayViewDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 122
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-lez v1, :cond_1

    .line 123
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 125
    :cond_1
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 126
    invoke-virtual {v2, v5}, Lcom/chase/sig/android/view/aa;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 127
    invoke-virtual {p0, v5}, Lcom/chase/sig/android/activity/QuickPayViewDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 128
    new-instance v1, Lcom/chase/sig/android/activity/jj;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/jj;-><init>(Lcom/chase/sig/android/activity/QuickPayViewDetailActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 140
    :cond_2
    return-void

    .line 116
    :cond_3
    sget-object v1, Lcom/chase/sig/android/domain/QuickPayActivityType;->d:Lcom/chase/sig/android/domain/QuickPayActivityType;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/QuickPayActivityType;->a(Lcom/chase/sig/android/domain/QuickPayActivityItem;)Z

    move-result v1

    if-eqz v1, :cond_4

    const v0, 0x7f0701e4

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayViewDetailActivity;->setTitle(I)V

    goto :goto_0

    :cond_4
    sget-object v1, Lcom/chase/sig/android/domain/QuickPayActivityType;->a:Lcom/chase/sig/android/domain/QuickPayActivityType;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/QuickPayActivityType;->a(Lcom/chase/sig/android/domain/QuickPayActivityItem;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-boolean v1, p0, Lcom/chase/sig/android/activity/QuickPayViewDetailActivity;->c:Z

    if-eqz v1, :cond_5

    const v0, 0x7f0701e5

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayViewDetailActivity;->setTitle(I)V

    goto :goto_0

    :cond_5
    sget-object v1, Lcom/chase/sig/android/domain/QuickPayActivityType;->a:Lcom/chase/sig/android/domain/QuickPayActivityType;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/QuickPayActivityType;->a(Lcom/chase/sig/android/domain/QuickPayActivityItem;)Z

    move-result v1

    if-eqz v1, :cond_6

    const v0, 0x7f0701e3

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayViewDetailActivity;->setTitle(I)V

    goto :goto_0

    :cond_6
    sget-object v1, Lcom/chase/sig/android/domain/QuickPayActivityType;->b:Lcom/chase/sig/android/domain/QuickPayActivityType;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/QuickPayActivityType;->a(Lcom/chase/sig/android/domain/QuickPayActivityItem;)Z

    move-result v1

    if-eqz v1, :cond_7

    const v0, 0x7f07022e

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayViewDetailActivity;->setTitle(I)V

    goto/16 :goto_0

    :cond_7
    sget-object v1, Lcom/chase/sig/android/domain/QuickPayActivityType;->c:Lcom/chase/sig/android/domain/QuickPayActivityType;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/QuickPayActivityType;->a(Lcom/chase/sig/android/domain/QuickPayActivityItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f07022f

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayViewDetailActivity;->setTitle(I)V

    goto/16 :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 160
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/eb;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 161
    const-string v0, "bundle_details"

    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickPayViewDetailActivity;->a:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 162
    return-void
.end method
