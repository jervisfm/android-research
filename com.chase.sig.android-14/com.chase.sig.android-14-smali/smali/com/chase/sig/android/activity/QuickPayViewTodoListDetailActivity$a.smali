.class public Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity$a;
.super Lcom/chase/sig/android/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/d",
        "<",
        "Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;",
        "Lcom/chase/sig/android/domain/QuickPayAcceptMoneyTransaction;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/service/quickpay/QuickPayAcceptMoneyResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 192
    invoke-direct {p0}, Lcom/chase/sig/android/d;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 192
    check-cast p1, [Lcom/chase/sig/android/domain/QuickPayAcceptMoneyTransaction;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;->A()Lcom/chase/sig/android/service/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/service/n;->j()Lcom/chase/sig/android/service/y;

    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-static {v0}, Lcom/chase/sig/android/service/y;->a(Lcom/chase/sig/android/domain/QuickPayAcceptMoneyTransaction;)Lcom/chase/sig/android/service/quickpay/QuickPayAcceptMoneyResponse;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 3
    .parameter

    .prologue
    .line 192
    check-cast p1, Lcom/chase/sig/android/service/quickpay/QuickPayAcceptMoneyResponse;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPayAcceptMoneyResponse;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPayAcceptMoneyResponse;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;->b(Ljava/util/List;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;->A()Lcom/chase/sig/android/service/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/service/n;->j()Lcom/chase/sig/android/service/y;

    const/4 v0, 0x1

    invoke-static {v0}, Lcom/chase/sig/android/service/y;->c(I)Lcom/chase/sig/android/service/quickpay/TodoListResponse;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->e()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iput-object v1, v0, Lcom/chase/sig/android/domain/o;->c:Lcom/chase/sig/android/service/quickpay/TodoListResponse;

    :cond_1
    new-instance v1, Landroid/content/Intent;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    const-class v2, Lcom/chase/sig/android/activity/QuickPayAcceptMoneyConfirmationActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "quick_pay_transaction"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/QuickPayTransaction;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->z()V

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPayAcceptMoneyResponse;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/domain/QuickPayTransaction;->f(Ljava/lang/String;)V

    const-string v2, "quick_pay_transaction"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
