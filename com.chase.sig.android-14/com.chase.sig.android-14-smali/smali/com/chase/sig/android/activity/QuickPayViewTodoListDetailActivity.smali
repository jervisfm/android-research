.class public Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;
.super Lcom/chase/sig/android/activity/eb;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$e;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity$a;
    }
.end annotation


# instance fields
.field protected a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/chase/sig/android/domain/QuickPayActivityItem;",
            ">;"
        }
    .end annotation
.end field

.field protected b:Z

.field protected c:Z

.field protected d:Lcom/chase/sig/android/domain/QuickPayActivityItem;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/chase/sig/android/activity/eb;-><init>()V

    .line 33
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;->c:Z

    .line 192
    return-void
.end method

.method public static a(Ljava/util/ArrayList;)Lcom/chase/sig/android/domain/QuickPayRecipient;
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/chase/sig/android/domain/QuickPayActivityItem;",
            ">;)",
            "Lcom/chase/sig/android/domain/QuickPayRecipient;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 146
    new-instance v1, Lcom/chase/sig/android/domain/QuickPayRecipient;

    invoke-direct {v1}, Lcom/chase/sig/android/domain/QuickPayRecipient;-><init>()V

    .line 147
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 148
    invoke-virtual {p0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/QuickPayActivityItem;

    .line 149
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/domain/QuickPayRecipient;->d(Ljava/lang/String;)V

    .line 150
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->o()Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v2

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/domain/QuickPayRecipient;->a(Ljava/lang/String;)V

    .line 151
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->z()Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v2

    if-nez v2, :cond_1

    const-string v0, ""

    .line 153
    :goto_0
    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/QuickPayRecipient;->b(Ljava/lang/String;)V

    .line 155
    :cond_0
    return-object v1

    .line 151
    :cond_1
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->z()Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/util/List;Lcom/chase/sig/android/domain/QuickPayTransaction;)Lcom/chase/sig/android/domain/QuickPayTransaction;
    .locals 4
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/QuickPayActivityItem;",
            ">;",
            "Lcom/chase/sig/android/domain/QuickPayTransaction;",
            ")",
            "Lcom/chase/sig/android/domain/QuickPayTransaction;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 160
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_4

    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 161
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/QuickPayActivityItem;

    .line 162
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/chase/sig/android/domain/QuickPayTransaction;->d(Ljava/lang/String;)V

    .line 164
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->y()Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 165
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->y()Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v2

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/chase/sig/android/domain/QuickPayTransaction;->p(Ljava/lang/String;)V

    .line 168
    :cond_0
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/chase/sig/android/domain/QuickPayTransaction;->c(Ljava/lang/String;)V

    .line 169
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/chase/sig/android/domain/QuickPayTransaction;->g(Ljava/lang/String;)V

    .line 170
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->j()Lcom/chase/sig/android/domain/QuickPayRecipient;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/chase/sig/android/domain/QuickPayTransaction;->a(Lcom/chase/sig/android/domain/QuickPayRecipient;)V

    .line 171
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->u()Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v2

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/chase/sig/android/domain/QuickPayTransaction;->m(Ljava/lang/String;)V

    .line 172
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->C()Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v2

    if-eqz v2, :cond_1

    const-string v2, "true"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->C()Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v3

    invoke-virtual {v3}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x1

    :cond_1
    invoke-virtual {p1, v1}, Lcom/chase/sig/android/domain/QuickPayTransaction;->f(Z)V

    .line 175
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->A()Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v1

    if-eqz v1, :cond_2

    instance-of v1, p1, Lcom/chase/sig/android/domain/QuickPayAcceptMoneyTransaction;

    if-eqz v1, :cond_2

    move-object v1, p1

    .line 177
    check-cast v1, Lcom/chase/sig/android/domain/QuickPayAcceptMoneyTransaction;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->A()Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v2

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/domain/QuickPayAcceptMoneyTransaction;->a(Ljava/lang/String;)V

    .line 181
    :cond_2
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->w()Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 182
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->w()Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/chase/sig/android/domain/QuickPayTransaction;->j(Ljava/lang/String;)V

    .line 185
    :cond_3
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->r()Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/chase/sig/android/domain/QuickPayTransaction;->l(Ljava/lang/String;)V

    .line 186
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->e(Ljava/lang/String;)V

    .line 189
    :cond_4
    return-object p1
.end method

.method private a(Ljava/util/List;)V
    .locals 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/QuickPayActivityItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 113
    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 114
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/QuickPayActivityItem;

    sget-object v1, Lcom/chase/sig/android/domain/QuickPayActivityType;->d:Lcom/chase/sig/android/domain/QuickPayActivityType;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/QuickPayActivityType;->a(Lcom/chase/sig/android/domain/QuickPayActivityItem;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;->c:Z

    if-eqz v1, :cond_2

    const v0, 0x7f0701e6

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;->setTitle(I)V

    .line 115
    :cond_0
    :goto_0
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/QuickPayActivityItem;

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;->d:Lcom/chase/sig/android/domain/QuickPayActivityItem;

    .line 116
    new-instance v2, Lcom/chase/sig/android/view/aa;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/QuickPayActivityItem;

    iget-boolean v4, p0, Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;->b:Z

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/ChaseApplication;

    invoke-direct {v2, v3, v0, v4, v1}, Lcom/chase/sig/android/view/aa;-><init>(Landroid/content/Context;Lcom/chase/sig/android/domain/QuickPayActivityItem;ZLcom/chase/sig/android/ChaseApplication;)V

    .line 118
    const v0, 0x7f0901ce

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 119
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 120
    const v0, 0x7f09024b

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const v1, 0x7f09024c

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iget-object v2, p0, Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;->a:Ljava/util/ArrayList;

    invoke-static {v2}, Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;->a(Ljava/util/ArrayList;)Lcom/chase/sig/android/domain/QuickPayRecipient;

    move-result-object v2

    new-instance v3, Lcom/chase/sig/android/activity/jl;

    invoke-direct {v3, p0}, Lcom/chase/sig/android/activity/jl;-><init>(Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;)V

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget-object v0, Lcom/chase/sig/android/domain/QuickPayActivityType;->d:Lcom/chase/sig/android/domain/QuickPayActivityType;

    iget-object v3, p0, Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;->d:Lcom/chase/sig/android/domain/QuickPayActivityItem;

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/domain/QuickPayActivityType;->a(Lcom/chase/sig/android/domain/QuickPayActivityItem;)Z

    move-result v0

    if-eqz v0, :cond_5

    new-instance v0, Lcom/chase/sig/android/activity/jm;

    invoke-direct {v0, p0, v2}, Lcom/chase/sig/android/activity/jm;-><init>(Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;Lcom/chase/sig/android/domain/QuickPayRecipient;)V

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 121
    :cond_1
    :goto_1
    return-void

    .line 114
    :cond_2
    sget-object v1, Lcom/chase/sig/android/domain/QuickPayActivityType;->d:Lcom/chase/sig/android/domain/QuickPayActivityType;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/QuickPayActivityType;->a(Lcom/chase/sig/android/domain/QuickPayActivityItem;)Z

    move-result v1

    if-eqz v1, :cond_3

    const v0, 0x7f0701e4

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;->setTitle(I)V

    goto :goto_0

    :cond_3
    sget-object v1, Lcom/chase/sig/android/domain/QuickPayActivityType;->a:Lcom/chase/sig/android/domain/QuickPayActivityType;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/QuickPayActivityType;->a(Lcom/chase/sig/android/domain/QuickPayActivityItem;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-boolean v1, p0, Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;->c:Z

    if-eqz v1, :cond_4

    const v0, 0x7f0701e5

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;->setTitle(I)V

    goto :goto_0

    :cond_4
    sget-object v1, Lcom/chase/sig/android/domain/QuickPayActivityType;->a:Lcom/chase/sig/android/domain/QuickPayActivityType;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/QuickPayActivityType;->a(Lcom/chase/sig/android/domain/QuickPayActivityItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0701e3

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;->setTitle(I)V

    goto/16 :goto_0

    .line 120
    :cond_5
    sget-object v0, Lcom/chase/sig/android/domain/QuickPayActivityType;->a:Lcom/chase/sig/android/domain/QuickPayActivityType;

    iget-object v2, p0, Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;->d:Lcom/chase/sig/android/domain/QuickPayActivityItem;

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/domain/QuickPayActivityType;->a(Lcom/chase/sig/android/domain/QuickPayActivityItem;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "Accept Money"

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    new-instance v0, Lcom/chase/sig/android/activity/jn;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/activity/jn;-><init>(Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;)V

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 39
    const v0, 0x7f030090

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;->b(I)V

    .line 40
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ACTIVITY_DETAIL_ORIGIN"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;->b:Z

    .line 41
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "ACTIVITY_DETAIL_OLD"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;->c:Z

    .line 42
    if-eqz p1, :cond_1

    const-string v0, "bundle_details"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 43
    const-string v0, "bundle_details"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;->a:Ljava/util/ArrayList;

    .line 45
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;->a:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;->a(Ljava/util/List;)V

    .line 51
    :cond_0
    :goto_0
    return-void

    .line 46
    :cond_1
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "quick_pay_details"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 47
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "quick_pay_details"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;->a:Ljava/util/ArrayList;

    .line 49
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;->a:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;->a(Ljava/util/List;)V

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 128
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/eb;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 129
    const-string v0, "bundle_details"

    iget-object v1, p0, Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;->a:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 130
    return-void
.end method
