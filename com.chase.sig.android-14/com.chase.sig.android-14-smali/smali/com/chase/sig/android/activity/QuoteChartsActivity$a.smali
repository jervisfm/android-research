.class public Lcom/chase/sig/android/activity/QuoteChartsActivity$a;
.super Lcom/chase/sig/android/b;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/QuoteChartsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/b",
        "<",
        "Lcom/chase/sig/android/activity/b;",
        "Ljava/lang/Object;",
        "Ljava/lang/Void;",
        "Ljava/util/Map",
        "<",
        "Lcom/chase/sig/android/domain/Chart;",
        "Lcom/chase/sig/android/domain/ImageDownloadResponse;",
        ">;>;"
    }
.end annotation


# instance fields
.field protected a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/Chart;",
            ">;"
        }
    .end annotation
.end field

.field protected e:Lcom/chase/sig/android/domain/Chart;


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 86
    invoke-direct {p0}, Lcom/chase/sig/android/b;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6
    .parameter

    .prologue
    const/4 v5, 0x1

    .line 86
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    const/4 v0, 0x0

    aget-object v0, p1, v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuoteChartsActivity$a;->a:Ljava/util/List;

    aget-object v0, p1, v5

    check-cast v0, Lcom/chase/sig/android/domain/Chart;

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuoteChartsActivity$a;->e:Lcom/chase/sig/android/domain/Chart;

    iget-object v0, p0, Lcom/chase/sig/android/activity/QuoteChartsActivity$a;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/Chart;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Chart;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->D(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v1, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v1, Lcom/chase/sig/android/activity/b;

    invoke-virtual {v1}, Lcom/chase/sig/android/activity/b;->A()Lcom/chase/sig/android/service/n;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/service/n;->n()Lcom/chase/sig/android/service/j;

    invoke-static {}, Lcom/chase/sig/android/service/JPService;->b()Ljava/util/HashMap;

    move-result-object v1

    invoke-static {v4, v5, v1}, Lcom/chase/sig/android/service/j;->a(Ljava/lang/String;ZLjava/util/HashMap;)Lcom/chase/sig/android/domain/ImageDownloadResponse;

    move-result-object v1

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-object v2
.end method

.method protected final bridge synthetic a(Ljava/lang/Object;)V
    .locals 2
    .parameter

    .prologue
    .line 86
    check-cast p1, Ljava/util/Map;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/b;

    iget-object v1, p0, Lcom/chase/sig/android/activity/QuoteChartsActivity$a;->e:Lcom/chase/sig/android/domain/Chart;

    invoke-virtual {v0, p1, v1}, Lcom/chase/sig/android/activity/b;->a(Ljava/util/Map;Lcom/chase/sig/android/domain/Chart;)V

    return-void
.end method
