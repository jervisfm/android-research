.class public Lcom/chase/sig/android/activity/QuoteChartsActivity;
.super Lcom/chase/sig/android/activity/b;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/QuoteChartsActivity$a;
    }
.end annotation


# instance fields
.field protected b:Lcom/chase/sig/android/domain/Quote;

.field private c:I

.field private d:Lcom/chase/sig/android/view/JPTabWidget;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 46
    const v0, 0x7f020076

    const v1, 0x7f090151

    invoke-direct {p0, v0, v1}, Lcom/chase/sig/android/activity/b;-><init>(II)V

    .line 36
    const/4 v0, 0x0

    iput v0, p0, Lcom/chase/sig/android/activity/QuoteChartsActivity;->c:I

    .line 47
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/QuoteChartsActivity;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 28
    iput p1, p0, Lcom/chase/sig/android/activity/QuoteChartsActivity;->c:I

    return p1
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/QuoteChartsActivity;)Lcom/chase/sig/android/view/JPTabWidget;
    .locals 1
    .parameter

    .prologue
    .line 28
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuoteChartsActivity;->d:Lcom/chase/sig/android/view/JPTabWidget;

    return-object v0
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/QuoteChartsActivity;I)Lcom/chase/sig/android/domain/Chart;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/chase/sig/android/activity/QuoteChartsActivity;->k(I)Lcom/chase/sig/android/domain/Chart;

    move-result-object v0

    return-object v0
.end method

.method private k(I)Lcom/chase/sig/android/domain/Chart;
    .locals 1
    .parameter

    .prologue
    .line 206
    packed-switch p1, :pswitch_data_0

    .line 212
    sget-object v0, Lcom/chase/sig/android/domain/Chart;->a:Lcom/chase/sig/android/domain/Chart;

    :goto_0
    return-object v0

    .line 208
    :pswitch_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuoteChartsActivity;->b:Lcom/chase/sig/android/domain/Quote;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Quote;->q()Lcom/chase/sig/android/domain/Chart;

    move-result-object v0

    goto :goto_0

    .line 210
    :pswitch_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuoteChartsActivity;->b:Lcom/chase/sig/android/domain/Quote;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Quote;->r()Lcom/chase/sig/android/domain/Chart;

    move-result-object v0

    goto :goto_0

    .line 206
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 9
    .parameter

    .prologue
    const v8, 0x7f09014e

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 51
    const v0, 0x7f030095

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuoteChartsActivity;->b(I)V

    .line 52
    const v0, 0x7f09002c

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuoteChartsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 53
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuoteChartsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 54
    const-string v1, "transaction_object"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/Quote;

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuoteChartsActivity;->b:Lcom/chase/sig/android/domain/Quote;

    .line 55
    if-eqz p1, :cond_1

    const-string v0, "tabSelection"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/chase/sig/android/activity/QuoteChartsActivity;->c:I

    invoke-virtual {p0, p1}, Lcom/chase/sig/android/activity/QuoteChartsActivity;->b(Landroid/os/Bundle;)V

    .line 56
    :cond_0
    :goto_0
    invoke-virtual {p0, v8}, Lcom/chase/sig/android/activity/QuoteChartsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/JPTabWidget;

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuoteChartsActivity;->d:Lcom/chase/sig/android/view/JPTabWidget;

    iget-object v0, p0, Lcom/chase/sig/android/activity/QuoteChartsActivity;->d:Lcom/chase/sig/android/view/JPTabWidget;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuoteChartsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "exchangeName"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/JPTabWidget;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/QuoteChartsActivity;->d:Lcom/chase/sig/android/view/JPTabWidget;

    const v1, 0x7f0702c0

    invoke-virtual {v0, v5, v1}, Lcom/chase/sig/android/view/JPTabWidget;->a(II)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/QuoteChartsActivity;->d:Lcom/chase/sig/android/view/JPTabWidget;

    const v1, 0x7f0702c1

    invoke-virtual {v0, v6, v1}, Lcom/chase/sig/android/view/JPTabWidget;->a(II)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/QuoteChartsActivity;->d:Lcom/chase/sig/android/view/JPTabWidget;

    new-instance v1, Lcom/chase/sig/android/activity/jp;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/jp;-><init>(Lcom/chase/sig/android/activity/QuoteChartsActivity;)V

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/JPTabWidget;->setTabListener(Lcom/chase/sig/android/view/JPTabWidget$b;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/QuoteChartsActivity;->d:Lcom/chase/sig/android/view/JPTabWidget;

    iget v1, p0, Lcom/chase/sig/android/activity/QuoteChartsActivity;->c:I

    invoke-virtual {v0, v1, v6}, Lcom/chase/sig/android/view/JPTabWidget;->a(IZ)V

    .line 57
    invoke-virtual {p0, v8}, Lcom/chase/sig/android/activity/QuoteChartsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/JPTabWidget;

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuoteChartsActivity;->d:Lcom/chase/sig/android/view/JPTabWidget;

    new-instance v0, Landroid/view/GestureDetector;

    new-instance v1, Lcom/chase/sig/android/activity/jq;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/jq;-><init>(Lcom/chase/sig/android/activity/QuoteChartsActivity;)V

    invoke-direct {v0, v1}, Landroid/view/GestureDetector;-><init>(Landroid/view/GestureDetector$OnGestureListener;)V

    const v1, 0x7f090151

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/QuoteChartsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/chase/sig/android/activity/jr;

    invoke-direct {v2, p0, v0}, Lcom/chase/sig/android/activity/jr;-><init>(Lcom/chase/sig/android/activity/QuoteChartsActivity;Landroid/view/GestureDetector;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 58
    const v0, 0x7f0702c2

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuoteChartsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/chase/sig/android/activity/QuoteChartsActivity;->b:Lcom/chase/sig/android/domain/Quote;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Quote;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->w(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const v0, 0x7f090258

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuoteChartsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 59
    return-void

    .line 55
    :cond_1
    new-array v0, v7, [Lcom/chase/sig/android/domain/Chart;

    invoke-direct {p0, v5}, Lcom/chase/sig/android/activity/QuoteChartsActivity;->k(I)Lcom/chase/sig/android/domain/Chart;

    move-result-object v1

    aput-object v1, v0, v5

    invoke-direct {p0, v6}, Lcom/chase/sig/android/activity/QuoteChartsActivity;->k(I)Lcom/chase/sig/android/domain/Chart;

    move-result-object v1

    aput-object v1, v0, v6

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/Chart;

    iget-object v1, p0, Lcom/chase/sig/android/activity/QuoteChartsActivity;->e:Lcom/chase/sig/android/activity/mc;

    const-class v3, Lcom/chase/sig/android/activity/QuoteChartsActivity$a;

    invoke-virtual {v1, v3}, Lcom/chase/sig/android/activity/mc;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/ae;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/activity/QuoteChartsActivity$a;

    invoke-virtual {v1}, Lcom/chase/sig/android/activity/QuoteChartsActivity$a;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v3

    sget-object v4, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-eq v3, v4, :cond_0

    new-array v3, v7, [Ljava/lang/Object;

    aput-object v2, v3, v5

    aput-object v0, v3, v6

    invoke-virtual {v1, v3}, Lcom/chase/sig/android/activity/QuoteChartsActivity$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0
.end method

.method protected final a(Ljava/util/Map;Lcom/chase/sig/android/domain/Chart;)V
    .locals 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/chase/sig/android/domain/Chart;",
            "Lcom/chase/sig/android/domain/ImageDownloadResponse;",
            ">;",
            "Lcom/chase/sig/android/domain/Chart;",
            ")V"
        }
    .end annotation

    .prologue
    .line 124
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 125
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    sget-object v2, Lcom/chase/sig/android/domain/ImageDownloadResponse;->a:Lcom/chase/sig/android/domain/ImageDownloadResponse;

    if-ne v0, v2, :cond_0

    .line 126
    const/16 v0, 0x23

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuoteChartsActivity;->showDialog(I)V

    goto :goto_0

    .line 129
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/chase/sig/android/activity/b;->a(Ljava/util/Map;Lcom/chase/sig/android/domain/Chart;)V

    .line 130
    return-void
.end method

.method protected final c_()V
    .locals 0

    .prologue
    .line 134
    invoke-super {p0}, Lcom/chase/sig/android/activity/b;->c_()V

    .line 135
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuoteChartsActivity;->finish()V

    .line 136
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 4
    .parameter

    .prologue
    .line 140
    new-instance v0, Lcom/chase/sig/android/view/k$a;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/view/k$a;-><init>(Landroid/content/Context;)V

    .line 141
    packed-switch p1, :pswitch_data_0

    .line 152
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/b;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    .line 143
    :pswitch_0
    const v1, 0x7f0702c6

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/k$a;->b(I)Lcom/chase/sig/android/view/k$a;

    move-result-object v1

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/chase/sig/android/view/k$a;->i:Z

    const v2, 0x7f070068

    new-instance v3, Lcom/chase/sig/android/activity/jo;

    invoke-direct {v3, p0}, Lcom/chase/sig/android/activity/jo;-><init>(Lcom/chase/sig/android/activity/QuoteChartsActivity;)V

    invoke-virtual {v1, v2, v3}, Lcom/chase/sig/android/view/k$a;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    move-result-object v1

    const v2, 0x7f0702c5

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/view/k$a;->c(I)Lcom/chase/sig/android/view/k$a;

    .line 150
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/k$a;->d(I)Lcom/chase/sig/android/view/k;

    move-result-object v0

    goto :goto_0

    .line 141
    nop

    :pswitch_data_0
    .packed-switch 0x23
        :pswitch_0
    .end packed-switch
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 63
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/b;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 64
    invoke-virtual {p0, p1}, Lcom/chase/sig/android/activity/QuoteChartsActivity;->c(Landroid/os/Bundle;)V

    .line 65
    const-string v0, "tabSelection"

    iget v1, p0, Lcom/chase/sig/android/activity/QuoteChartsActivity;->c:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 66
    return-void
.end method
