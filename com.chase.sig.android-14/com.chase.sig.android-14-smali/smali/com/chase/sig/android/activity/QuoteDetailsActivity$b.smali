.class public Lcom/chase/sig/android/activity/QuoteDetailsActivity$b;
.super Lcom/chase/sig/android/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/QuoteDetailsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/d",
        "<",
        "Lcom/chase/sig/android/activity/QuoteDetailsActivity;",
        "Ljava/lang/Object;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/domain/n;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 167
    invoke-direct {p0}, Lcom/chase/sig/android/d;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .parameter

    .prologue
    const/4 v3, 0x1

    .line 167
    const/4 v0, 0x0

    aget-object v0, p1, v0

    check-cast v0, Ljava/lang/String;

    aget-object v1, p1, v3

    check-cast v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v2, Lcom/chase/sig/android/activity/QuoteDetailsActivity;

    invoke-virtual {v2}, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->A()Lcom/chase/sig/android/service/n;

    move-result-object v2

    invoke-virtual {v2}, Lcom/chase/sig/android/service/n;->m()Lcom/chase/sig/android/service/aa;

    invoke-static {v0, v1}, Lcom/chase/sig/android/service/aa;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/domain/QuoteResponse;

    move-result-object v1

    new-instance v0, Lcom/chase/sig/android/domain/ImageDownloadResponse;

    invoke-direct {v0}, Lcom/chase/sig/android/domain/ImageDownloadResponse;-><init>()V

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/QuoteResponse;->e()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/QuoteResponse;->b()Lcom/chase/sig/android/domain/Quote;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/Quote;->q()Lcom/chase/sig/android/domain/Chart;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Chart;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->D(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuoteDetailsActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->A()Lcom/chase/sig/android/service/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/service/n;->n()Lcom/chase/sig/android/service/j;

    invoke-static {}, Lcom/chase/sig/android/service/JPService;->b()Ljava/util/HashMap;

    move-result-object v0

    invoke-static {v2, v3, v0}, Lcom/chase/sig/android/service/j;->a(Ljava/lang/String;ZLjava/util/HashMap;)Lcom/chase/sig/android/domain/ImageDownloadResponse;

    move-result-object v0

    :cond_0
    new-instance v2, Lcom/chase/sig/android/domain/n;

    invoke-direct {v2, v1, v0}, Lcom/chase/sig/android/domain/n;-><init>(Lcom/chase/sig/android/domain/QuoteResponse;Lcom/chase/sig/android/domain/ImageDownloadResponse;)V

    return-object v2
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 3
    .parameter

    .prologue
    .line 167
    check-cast p1, Lcom/chase/sig/android/domain/n;

    iget-object v0, p1, Lcom/chase/sig/android/domain/n;->a:Lcom/chase/sig/android/domain/QuoteResponse;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuoteResponse;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuoteDetailsActivity;

    iget-object v1, p1, Lcom/chase/sig/android/domain/n;->a:Lcom/chase/sig/android/domain/QuoteResponse;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/QuoteResponse;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->b(Ljava/util/List;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuoteDetailsActivity;

    iget-object v1, p1, Lcom/chase/sig/android/domain/n;->a:Lcom/chase/sig/android/domain/QuoteResponse;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/QuoteResponse;->b()Lcom/chase/sig/android/domain/Quote;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->a(Lcom/chase/sig/android/activity/QuoteDetailsActivity;Lcom/chase/sig/android/domain/Quote;)Lcom/chase/sig/android/domain/Quote;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuoteDetailsActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->d()V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuoteDetailsActivity;

    iget-object v1, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v1, Lcom/chase/sig/android/activity/QuoteDetailsActivity;

    invoke-static {v1}, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->b(Lcom/chase/sig/android/activity/QuoteDetailsActivity;)Lcom/chase/sig/android/domain/Quote;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/Quote;->q()Lcom/chase/sig/android/domain/Chart;

    move-result-object v1

    iget-object v2, p1, Lcom/chase/sig/android/domain/n;->b:Lcom/chase/sig/android/domain/ImageDownloadResponse;

    invoke-virtual {v0, v1, v2}, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->a(Lcom/chase/sig/android/domain/Chart;Lcom/chase/sig/android/domain/ImageDownloadResponse;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuoteDetailsActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->c(Lcom/chase/sig/android/activity/QuoteDetailsActivity;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuoteDetailsActivity;

    const v1, 0x7f090256

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method
