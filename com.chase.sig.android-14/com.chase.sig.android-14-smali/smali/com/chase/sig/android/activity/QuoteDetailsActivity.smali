.class public Lcom/chase/sig/android/activity/QuoteDetailsActivity;
.super Lcom/chase/sig/android/activity/b;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/QuoteDetailsActivity$b;,
        Lcom/chase/sig/android/activity/QuoteDetailsActivity$a;
    }
.end annotation


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private k:Lcom/chase/sig/android/util/Dollar;

.field private l:Lcom/chase/sig/android/domain/Quote;

.field private m:Ljava/lang/String;

.field private n:Lcom/chase/sig/android/view/detail/DetailView;

.field private o:Lcom/chase/sig/android/service/QuoteNewsResponse;

.field private p:Lcom/chase/sig/android/activity/ag;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/chase/sig/android/activity/ag",
            "<",
            "Lcom/chase/sig/android/activity/QuoteDetailsActivity;",
            ">;"
        }
    .end annotation
.end field

.field private q:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 58
    const v0, 0x7f020077

    const v1, 0x7f090151

    invoke-direct {p0, v0, v1}, Lcom/chase/sig/android/activity/b;-><init>(II)V

    .line 59
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/QuoteDetailsActivity;Lcom/chase/sig/android/domain/Quote;)Lcom/chase/sig/android/domain/Quote;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 39
    iput-object p1, p0, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->l:Lcom/chase/sig/android/domain/Quote;

    return-object p1
.end method

.method private a(ILjava/lang/CharSequence;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 302
    invoke-virtual {p0, p1}, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 303
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 316
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/chase/sig/android/activity/QuoteDetailsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 317
    const-string v1, "ticker_symbol"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 318
    const-string v1, "quantity"

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 319
    const-string v1, "from_quote_search"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 320
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 321
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/QuoteDetailsActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->e()V

    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/QuoteDetailsActivity;Lcom/chase/sig/android/service/QuoteNewsResponse;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 39
    iput-object p1, p0, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->o:Lcom/chase/sig/android/service/QuoteNewsResponse;

    iget-object v0, p0, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->p:Lcom/chase/sig/android/activity/ag;

    iget-object v1, p0, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->n:Lcom/chase/sig/android/view/detail/DetailView;

    invoke-virtual {v0, p0, v1, p1}, Lcom/chase/sig/android/activity/ag;->a(Lcom/chase/sig/android/activity/eb;Lcom/chase/sig/android/view/detail/DetailView;Lcom/chase/sig/android/service/QuoteNewsResponse;)V

    return-void
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/QuoteDetailsActivity;)Lcom/chase/sig/android/domain/Quote;
    .locals 1
    .parameter

    .prologue
    .line 39
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->l:Lcom/chase/sig/android/domain/Quote;

    return-object v0
.end method

.method static synthetic c(Lcom/chase/sig/android/activity/QuoteDetailsActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->f()V

    return-void
.end method

.method private e()V
    .locals 4

    .prologue
    .line 147
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->e:Lcom/chase/sig/android/activity/mc;

    const-class v1, Lcom/chase/sig/android/activity/QuoteDetailsActivity$b;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/mc;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/ae;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/activity/QuoteDetailsActivity$b;

    .line 148
    invoke-virtual {v0}, Lcom/chase/sig/android/activity/QuoteDetailsActivity$b;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v1

    sget-object v2, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-eq v1, v2, :cond_0

    .line 149
    const/4 v1, 0x2

    new-array v2, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->c:Ljava/lang/String;

    :goto_0
    aput-object v1, v2, v3

    const/4 v1, 0x1

    iget-object v3, p0, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->d:Ljava/lang/String;

    aput-object v3, v2, v1

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/activity/QuoteDetailsActivity$b;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 151
    :cond_0
    return-void

    .line 149
    :cond_1
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->b:Ljava/lang/String;

    goto :goto_0
.end method

.method private f()V
    .locals 2

    .prologue
    .line 217
    const v0, 0x7f090151

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 218
    new-instance v1, Lcom/chase/sig/android/activity/jt;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/jt;-><init>(Lcom/chase/sig/android/activity/QuoteDetailsActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 228
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 7
    .parameter

    .prologue
    const v4, 0x7f090256

    const/4 v6, 0x0

    .line 63
    const v0, 0x7f0702b2

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->setTitle(I)V

    .line 64
    const v0, 0x7f030096

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->b(I)V

    .line 65
    invoke-virtual {p0, v4}, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 67
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 68
    const v0, 0x7f0900b3

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 70
    const-string v1, "ticker_symbol"

    invoke-virtual {v2, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 71
    const-string v1, "ticker_symbol"

    invoke-virtual {v2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->b:Ljava/lang/String;

    .line 74
    :cond_0
    const-string v1, "quote_code"

    invoke-virtual {v2, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 75
    const-string v1, "quote_code"

    const/4 v3, 0x0

    invoke-static {v2, v1, v3}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->c:Ljava/lang/String;

    .line 78
    :cond_1
    const-string v1, "quantity"

    invoke-virtual {v2, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 79
    const-string v1, "quantity"

    invoke-virtual {v2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->d:Ljava/lang/String;

    .line 82
    :cond_2
    const-string v1, "gain_loss"

    invoke-virtual {v2, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 83
    const-string v1, "gain_loss"

    invoke-virtual {v2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->m:Ljava/lang/String;

    .line 84
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->m:Ljava/lang/String;

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 85
    new-instance v1, Lcom/chase/sig/android/util/Dollar;

    iget-object v3, p0, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->m:Ljava/lang/String;

    invoke-direct {v1, v3}, Lcom/chase/sig/android/util/Dollar;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->k:Lcom/chase/sig/android/util/Dollar;

    .line 89
    :cond_3
    const-string v1, "from_quote_search"

    invoke-virtual {v2, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 90
    const-string v1, "from_quote_search"

    invoke-virtual {v2, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->q:Z

    .line 94
    :cond_4
    const-class v1, Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/a/f;

    move-result-object v1

    const-string v2, "title"

    invoke-virtual {v0}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/chase/sig/android/activity/a/f;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/activity/a/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 98
    const-string v0, "quote"

    invoke-static {p1, v0}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 99
    const-string v0, "quote"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/Quote;

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->l:Lcom/chase/sig/android/domain/Quote;

    .line 100
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->d()V

    .line 101
    invoke-virtual {p0, p1}, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->b(Landroid/os/Bundle;)V

    .line 102
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->l:Lcom/chase/sig/android/domain/Quote;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Quote;->q()Lcom/chase/sig/android/domain/Chart;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->b(Lcom/chase/sig/android/domain/Chart;)V

    .line 103
    invoke-direct {p0}, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->f()V

    .line 104
    invoke-virtual {p0, v4}, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 109
    :goto_0
    const/16 v0, 0x3e8

    const v1, 0x7f02003d

    const v2, 0x7f02003c

    new-instance v3, Lcom/chase/sig/android/activity/js;

    invoke-direct {v3, p0}, Lcom/chase/sig/android/activity/js;-><init>(Lcom/chase/sig/android/activity/QuoteDetailsActivity;)V

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->a(IIILandroid/view/View$OnClickListener;)V

    .line 117
    const/16 v0, 0x3e9

    const v1, 0x7f020043

    const v2, 0x7f020042

    new-instance v3, Lcom/chase/sig/android/activity/a/f;

    const-class v4, Lcom/chase/sig/android/activity/QuoteSearchActivity;

    invoke-direct {v3, p0, v4}, Lcom/chase/sig/android/activity/a/f;-><init>(Lcom/chase/sig/android/activity/eb;Ljava/lang/Class;)V

    invoke-virtual {v3}, Lcom/chase/sig/android/activity/a/f;->d()Lcom/chase/sig/android/activity/a/f;

    move-result-object v3

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->a(IIILandroid/view/View$OnClickListener;)V

    .line 121
    const v0, 0x7f090152

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 122
    const v1, 0x7f0702b9

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 124
    const v0, 0x7f090115

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->n:Lcom/chase/sig/android/view/detail/DetailView;

    .line 126
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->p:Lcom/chase/sig/android/activity/ag;

    if-nez v0, :cond_5

    .line 127
    new-instance v0, Lcom/chase/sig/android/activity/ag;

    invoke-direct {v0}, Lcom/chase/sig/android/activity/ag;-><init>()V

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->p:Lcom/chase/sig/android/activity/ag;

    .line 130
    :cond_5
    if-eqz p1, :cond_6

    .line 131
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->p:Lcom/chase/sig/android/activity/ag;

    invoke-static {p1}, Lcom/chase/sig/android/activity/ag;->a(Landroid/os/Bundle;)Lcom/chase/sig/android/service/QuoteNewsResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->o:Lcom/chase/sig/android/service/QuoteNewsResponse;

    .line 134
    :cond_6
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->p:Lcom/chase/sig/android/activity/ag;

    iget-object v2, p0, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->o:Lcom/chase/sig/android/service/QuoteNewsResponse;

    iget-object v3, p0, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->n:Lcom/chase/sig/android/view/detail/DetailView;

    const-class v4, Lcom/chase/sig/android/activity/QuoteDetailsActivity$a;

    const/4 v1, 0x1

    new-array v5, v1, [Ljava/lang/String;

    iget-object v1, p0, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->b:Ljava/lang/String;

    aput-object v1, v5, v6

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lcom/chase/sig/android/activity/ag;->a(Lcom/chase/sig/android/activity/eb;Lcom/chase/sig/android/service/QuoteNewsResponse;Lcom/chase/sig/android/view/detail/DetailView;Ljava/lang/Class;[Ljava/lang/String;)V

    .line 136
    return-void

    .line 106
    :cond_7
    invoke-direct {p0}, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->e()V

    goto :goto_0
.end method

.method public final d()V
    .locals 7

    .prologue
    const/4 v4, 0x4

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 242
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->l:Lcom/chase/sig/android/domain/Quote;

    if-nez v0, :cond_0

    .line 299
    :goto_0
    return-void

    .line 246
    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->k:Lcom/chase/sig/android/util/Dollar;

    if-nez v0, :cond_1

    .line 247
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->l:Lcom/chase/sig/android/domain/Quote;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Quote;->g()Lcom/chase/sig/android/util/Dollar;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->k:Lcom/chase/sig/android/util/Dollar;

    .line 250
    :cond_1
    const v0, 0x7f090267

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/TickerValueTextView;

    .line 252
    const v1, 0x7f090268

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 253
    const v2, 0x7f090265

    invoke-virtual {p0, v2}, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 255
    iget-boolean v3, p0, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->q:Z

    if-eqz v3, :cond_3

    .line 256
    invoke-virtual {v0, v4}, Lcom/chase/sig/android/view/TickerValueTextView;->setVisibility(I)V

    .line 257
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 258
    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 275
    :cond_2
    :goto_1
    const v0, 0x7f090263

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/TickerValueTextView;

    .line 278
    const v1, 0x7f090260

    iget-object v2, p0, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->l:Lcom/chase/sig/android/domain/Quote;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/Quote;->i()Lcom/chase/sig/android/util/Dollar;

    move-result-object v2

    invoke-virtual {v2}, Lcom/chase/sig/android/util/Dollar;->h()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->a(ILjava/lang/CharSequence;)V

    .line 279
    invoke-virtual {v0, v5}, Lcom/chase/sig/android/view/TickerValueTextView;->setDisplayPlaceholder(Z)V

    .line 280
    invoke-virtual {v0, v5}, Lcom/chase/sig/android/view/TickerValueTextView;->setShowNotApplicable(Z)V

    .line 281
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->l:Lcom/chase/sig/android/domain/Quote;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/Quote;->b()Lcom/chase/sig/android/util/Dollar;

    move-result-object v1

    iget-object v2, p0, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->l:Lcom/chase/sig/android/domain/Quote;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/Quote;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/chase/sig/android/util/s;->y(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/chase/sig/android/view/TickerValueTextView;->a(Lcom/chase/sig/android/util/Dollar;Ljava/lang/String;)V

    .line 283
    const v0, 0x7f090171

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "As of "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->l:Lcom/chase/sig/android/domain/Quote;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/Quote;->j()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/chase/sig/android/util/s;->w(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->a(ILjava/lang/CharSequence;)V

    .line 285
    const v0, 0x7f09025b

    iget-object v1, p0, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->l:Lcom/chase/sig/android/domain/Quote;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/Quote;->p()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->a(ILjava/lang/CharSequence;)V

    .line 286
    const v0, 0x7f09026b

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->l:Lcom/chase/sig/android/domain/Quote;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/Quote;->m()Ljava/lang/Double;

    move-result-object v2

    const-string v3, "###,###,###"

    invoke-static {v2, v3}, Lcom/chase/sig/android/util/s;->a(Ljava/lang/Double;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->l:Lcom/chase/sig/android/domain/Quote;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/Quote;->a()Ljava/lang/Double;

    move-result-object v2

    const-string v3, "###,###,###"

    invoke-static {v2, v3}, Lcom/chase/sig/android/util/s;->a(Ljava/lang/Double;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->a(ILjava/lang/CharSequence;)V

    .line 291
    const v0, 0x7f090270

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "High: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->l:Lcom/chase/sig/android/domain/Quote;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/Quote;->h()Lcom/chase/sig/android/util/Dollar;

    move-result-object v2

    invoke-static {v2}, Lcom/chase/sig/android/util/s;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->a(ILjava/lang/CharSequence;)V

    .line 293
    const v0, 0x7f090271

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Low: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->l:Lcom/chase/sig/android/domain/Quote;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/Quote;->k()Lcom/chase/sig/android/util/Dollar;

    move-result-object v2

    invoke-static {v2}, Lcom/chase/sig/android/util/s;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->a(ILjava/lang/CharSequence;)V

    .line 295
    const v0, 0x7f090272

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "High: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->l:Lcom/chase/sig/android/domain/Quote;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/Quote;->n()Lcom/chase/sig/android/util/Dollar;

    move-result-object v2

    invoke-static {v2}, Lcom/chase/sig/android/util/s;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->a(ILjava/lang/CharSequence;)V

    .line 297
    const v0, 0x7f090273

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Low: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->l:Lcom/chase/sig/android/domain/Quote;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/Quote;->o()Lcom/chase/sig/android/util/Dollar;

    move-result-object v2

    invoke-static {v2}, Lcom/chase/sig/android/util/s;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->a(ILjava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 260
    :cond_3
    iget-object v3, p0, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->k:Lcom/chase/sig/android/util/Dollar;

    if-eqz v3, :cond_2

    .line 261
    invoke-virtual {v0, v5}, Lcom/chase/sig/android/view/TickerValueTextView;->setDisplayPlaceholder(Z)V

    .line 262
    invoke-virtual {v0, v5}, Lcom/chase/sig/android/view/TickerValueTextView;->setShowNotApplicable(Z)V

    .line 263
    iget-object v3, p0, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->l:Lcom/chase/sig/android/domain/Quote;

    invoke-virtual {v3}, Lcom/chase/sig/android/domain/Quote;->g()Lcom/chase/sig/android/util/Dollar;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/view/TickerValueTextView;->setTickerValue(Lcom/chase/sig/android/util/Dollar;)V

    .line 264
    invoke-virtual {v0, v6}, Lcom/chase/sig/android/view/TickerValueTextView;->setVisibility(I)V

    .line 266
    iget-object v3, p0, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->d:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v3

    const-string v4, "#,##0.00"

    invoke-static {v3, v4}, Lcom/chase/sig/android/util/s;->a(Ljava/lang/Double;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 268
    invoke-virtual {v0}, Lcom/chase/sig/android/view/TickerValueTextView;->getCurrentTextColor()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 269
    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 271
    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    const/16 v0, 0x23

    .line 140
    if-ne p1, v0, :cond_0

    if-ne p2, v0, :cond_0

    .line 142
    invoke-direct {p0}, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->e()V

    .line 144
    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 307
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/b;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 308
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->l:Lcom/chase/sig/android/domain/Quote;

    if-eqz v0, :cond_0

    .line 309
    const-string v0, "quote"

    iget-object v1, p0, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->l:Lcom/chase/sig/android/domain/Quote;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 310
    invoke-virtual {p0, p1}, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->c(Landroid/os/Bundle;)V

    .line 312
    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->p:Lcom/chase/sig/android/activity/ag;

    iget-object v0, p0, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->o:Lcom/chase/sig/android/service/QuoteNewsResponse;

    const-string v1, "quote_news_response"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 313
    return-void
.end method
