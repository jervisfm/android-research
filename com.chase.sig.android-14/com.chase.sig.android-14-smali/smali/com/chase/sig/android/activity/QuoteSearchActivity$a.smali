.class public Lcom/chase/sig/android/activity/QuoteSearchActivity$a;
.super Lcom/chase/sig/android/activity/ae;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/QuoteSearchActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/activity/ae",
        "<",
        "Lcom/chase/sig/android/activity/QuoteSearchActivity;",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/domain/QuoteResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 128
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ae;-><init>()V

    return-void
.end method

.method public static a(Lcom/chase/sig/android/activity/QuoteSearchActivity;)V
    .locals 2
    .parameter

    .prologue
    .line 163
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuoteSearchActivity;->e:Lcom/chase/sig/android/activity/mc;

    const-class v1, Lcom/chase/sig/android/activity/QuoteSearchActivity$a;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/mc;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/ae;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/activity/QuoteSearchActivity$a;

    .line 164
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/QuoteSearchActivity$a;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 165
    iget-boolean v1, v0, Lcom/chase/sig/android/activity/ae;->d:Z

    .line 166
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/QuoteSearchActivity$a;->cancel(Z)Z

    .line 168
    :cond_0
    return-void
.end method

.method public static a(Lcom/chase/sig/android/activity/QuoteSearchActivity;Ljava/lang/String;)Z
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 158
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuoteSearchActivity;->e:Lcom/chase/sig/android/activity/mc;

    const-class v1, Lcom/chase/sig/android/activity/QuoteSearchActivity$a;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/mc;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/ae;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/activity/QuoteSearchActivity$a;

    .line 159
    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/chase/sig/android/activity/QuoteSearchActivity$a;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 128
    check-cast p1, [Ljava/lang/String;

    aget-object v0, p1, v1

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuoteSearchActivity$a;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuoteSearchActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/QuoteSearchActivity;->A()Lcom/chase/sig/android/service/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/service/n;->m()Lcom/chase/sig/android/service/aa;

    aget-object v0, p1, v1

    invoke-static {v0}, Lcom/chase/sig/android/service/aa;->a(Ljava/lang/String;)Lcom/chase/sig/android/domain/QuoteResponse;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 2
    .parameter

    .prologue
    .line 128
    check-cast p1, Lcom/chase/sig/android/domain/QuoteResponse;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuoteSearchActivity;

    iget-object v1, p0, Lcom/chase/sig/android/activity/QuoteSearchActivity$a;->a:Ljava/lang/String;

    invoke-static {v0, v1, p1}, Lcom/chase/sig/android/activity/QuoteSearchActivity;->a(Lcom/chase/sig/android/activity/QuoteSearchActivity;Ljava/lang/String;Lcom/chase/sig/android/domain/QuoteResponse;)V

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/QuoteResponse;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "20160"

    invoke-virtual {p1, v0}, Lcom/chase/sig/android/domain/QuoteResponse;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuoteSearchActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/QuoteResponse;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/QuoteSearchActivity;->c(Ljava/util/List;)V

    :goto_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuoteSearchActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/QuoteSearchActivity;->b(Lcom/chase/sig/android/activity/QuoteSearchActivity;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuoteSearchActivity;

    invoke-static {v0, p1}, Lcom/chase/sig/android/activity/QuoteSearchActivity;->a(Lcom/chase/sig/android/activity/QuoteSearchActivity;Lcom/chase/sig/android/domain/QuoteResponse;)V

    goto :goto_0
.end method

.method protected onPreExecute()V
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/QuoteSearchActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/QuoteSearchActivity;->a(Lcom/chase/sig/android/activity/QuoteSearchActivity;)V

    .line 136
    return-void
.end method
