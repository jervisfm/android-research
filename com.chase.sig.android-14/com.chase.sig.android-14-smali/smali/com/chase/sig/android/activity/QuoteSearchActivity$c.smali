.class final Lcom/chase/sig/android/activity/QuoteSearchActivity$c;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/QuoteSearchActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "c"
.end annotation


# instance fields
.field final a:Ljava/lang/String;

.field final synthetic b:Lcom/chase/sig/android/activity/QuoteSearchActivity;

.field private final c:Ljava/lang/String;

.field private d:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/chase/sig/android/activity/QuoteSearchActivity;Lcom/chase/sig/android/domain/Quote;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 199
    iput-object p1, p0, Lcom/chase/sig/android/activity/QuoteSearchActivity$c;->b:Lcom/chase/sig/android/activity/QuoteSearchActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 200
    invoke-virtual {p2}, Lcom/chase/sig/android/domain/Quote;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuoteSearchActivity$c;->d:Ljava/lang/String;

    .line 201
    invoke-virtual {p2}, Lcom/chase/sig/android/domain/Quote;->l()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuoteSearchActivity$c;->a:Ljava/lang/String;

    .line 202
    invoke-virtual {p2}, Lcom/chase/sig/android/domain/Quote;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuoteSearchActivity$c;->c:Ljava/lang/String;

    .line 203
    return-void
.end method

.method synthetic constructor <init>(Lcom/chase/sig/android/activity/QuoteSearchActivity;Lcom/chase/sig/android/domain/Quote;B)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 194
    invoke-direct {p0, p1, p2}, Lcom/chase/sig/android/activity/QuoteSearchActivity$c;-><init>(Lcom/chase/sig/android/activity/QuoteSearchActivity;Lcom/chase/sig/android/domain/Quote;)V

    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 207
    const-string v0, "%s - %s (%s)"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/chase/sig/android/activity/QuoteSearchActivity$c;->a:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/chase/sig/android/activity/QuoteSearchActivity$c;->d:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/chase/sig/android/activity/QuoteSearchActivity$c;->c:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
