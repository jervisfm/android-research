.class public Lcom/chase/sig/android/activity/QuoteSearchActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$c;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/QuoteSearchActivity$b;,
        Lcom/chase/sig/android/activity/QuoteSearchActivity$c;,
        Lcom/chase/sig/android/activity/QuoteSearchActivity$a;
    }
.end annotation


# instance fields
.field private a:Landroid/widget/EditText;

.field private b:Landroid/widget/ListView;

.field private c:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Lcom/chase/sig/android/activity/QuoteSearchActivity$c;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/chase/sig/android/domain/QuoteResponse;

.field private k:Ljava/lang/String;

.field private l:Landroid/widget/TextView;

.field private final m:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    .line 36
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuoteSearchActivity;->m:Landroid/os/Handler;

    .line 211
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/QuoteSearchActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/chase/sig/android/activity/QuoteSearchActivity;->d()V

    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/QuoteSearchActivity;Lcom/chase/sig/android/domain/QuoteResponse;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/chase/sig/android/activity/QuoteSearchActivity;->a(Lcom/chase/sig/android/domain/QuoteResponse;)V

    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/QuoteSearchActivity;Ljava/lang/String;Lcom/chase/sig/android/domain/QuoteResponse;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Lcom/chase/sig/android/activity/QuoteSearchActivity;->a(Ljava/lang/String;Lcom/chase/sig/android/domain/QuoteResponse;)V

    return-void
.end method

.method private a(Lcom/chase/sig/android/domain/QuoteResponse;)V
    .locals 5
    .parameter

    .prologue
    .line 177
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuoteSearchActivity;->c:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->clear()V

    .line 179
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/QuoteResponse;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "20160"

    invoke-virtual {p1, v0}, Lcom/chase/sig/android/domain/QuoteResponse;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "20160"

    invoke-virtual {p1, v0}, Lcom/chase/sig/android/domain/QuoteResponse;->d(Ljava/lang/String;)Lcom/chase/sig/android/service/IServiceError;

    move-result-object v0

    invoke-interface {v0}, Lcom/chase/sig/android/service/IServiceError;->a()Ljava/lang/String;

    move-result-object v0

    .line 182
    :goto_0
    iget-object v1, p0, Lcom/chase/sig/android/activity/QuoteSearchActivity;->l:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 184
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/QuoteResponse;->a()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 185
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/QuoteResponse;->a()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/Quote;

    .line 186
    iget-object v2, p0, Lcom/chase/sig/android/activity/QuoteSearchActivity;->c:Landroid/widget/ArrayAdapter;

    new-instance v3, Lcom/chase/sig/android/activity/QuoteSearchActivity$c;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v0, v4}, Lcom/chase/sig/android/activity/QuoteSearchActivity$c;-><init>(Lcom/chase/sig/android/activity/QuoteSearchActivity;Lcom/chase/sig/android/domain/Quote;B)V

    invoke-virtual {v2, v3}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    goto :goto_1

    .line 179
    :cond_0
    const-string v0, ""

    goto :goto_0

    .line 190
    :cond_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuoteSearchActivity;->b:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->setSelectionAfterHeaderView()V

    .line 191
    invoke-direct {p0}, Lcom/chase/sig/android/activity/QuoteSearchActivity;->e()V

    .line 192
    return-void
.end method

.method private a(Ljava/lang/String;Lcom/chase/sig/android/domain/QuoteResponse;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 172
    iput-object p1, p0, Lcom/chase/sig/android/activity/QuoteSearchActivity;->k:Ljava/lang/String;

    .line 173
    iput-object p2, p0, Lcom/chase/sig/android/activity/QuoteSearchActivity;->d:Lcom/chase/sig/android/domain/QuoteResponse;

    .line 174
    return-void
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/QuoteSearchActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/chase/sig/android/activity/QuoteSearchActivity;->e()V

    return-void
.end method

.method private d()V
    .locals 3

    .prologue
    .line 93
    const v0, 0x7f0900db

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuoteSearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 95
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-ne v1, v2, :cond_0

    .line 96
    const v1, 0x7f040004

    invoke-static {p0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    .line 97
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 98
    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 100
    :cond_0
    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 103
    const v0, 0x7f0900db

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuoteSearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 105
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 106
    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 107
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 109
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 6
    .parameter

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 40
    const v0, 0x7f07029f

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuoteSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/eb;->f:Ljava/lang/String;

    .line 41
    const v0, 0x7f0702c7

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuoteSearchActivity;->setTitle(I)V

    .line 42
    const v0, 0x7f030097

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuoteSearchActivity;->b(I)V

    .line 44
    const-string v0, "QuoteSearchResponse"

    new-instance v1, Lcom/chase/sig/android/domain/QuoteResponse;

    invoke-direct {v1}, Lcom/chase/sig/android/domain/QuoteResponse;-><init>()V

    invoke-static {p1, v0, v1}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/QuoteResponse;

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuoteSearchActivity;->d:Lcom/chase/sig/android/domain/QuoteResponse;

    .line 47
    const-string v0, "QuoteSearchTerm"

    invoke-static {p1, v0, v4}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuoteSearchActivity;->k:Ljava/lang/String;

    .line 49
    const v0, 0x7f090277

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuoteSearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuoteSearchActivity;->a:Landroid/widget/EditText;

    .line 50
    const v0, 0x7f09027c

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuoteSearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuoteSearchActivity;->l:Landroid/widget/TextView;

    .line 51
    const v0, 0x7f09027a

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuoteSearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuoteSearchActivity;->b:Landroid/widget/ListView;

    .line 52
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuoteSearchActivity;->b:Landroid/widget/ListView;

    const v1, 0x7f09027b

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/QuoteSearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    .line 53
    new-instance v0, Landroid/widget/ArrayAdapter;

    const v1, 0x7f030098

    const v2, 0x7f090015

    invoke-direct {v0, p0, v1, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II)V

    iput-object v0, p0, Lcom/chase/sig/android/activity/QuoteSearchActivity;->c:Landroid/widget/ArrayAdapter;

    .line 55
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuoteSearchActivity;->c:Landroid/widget/ArrayAdapter;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->setNotifyOnChange(Z)V

    .line 59
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuoteSearchActivity;->b:Landroid/widget/ListView;

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/QuoteSearchActivity;->a(Landroid/widget/ListView;)V

    .line 61
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuoteSearchActivity;->b:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/chase/sig/android/activity/QuoteSearchActivity;->c:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 62
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuoteSearchActivity;->b:Landroid/widget/ListView;

    new-instance v1, Lcom/chase/sig/android/activity/ju;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/ju;-><init>(Lcom/chase/sig/android/activity/QuoteSearchActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 73
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuoteSearchActivity;->a:Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/QuoteSearchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0200bc

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    invoke-virtual {v1, v5, v5, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    invoke-virtual {v0, v4, v4, v1, v4}, Landroid/widget/EditText;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 74
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuoteSearchActivity;->a:Landroid/widget/EditText;

    new-instance v1, Lcom/chase/sig/android/activity/QuoteSearchActivity$b;

    iget-object v2, p0, Lcom/chase/sig/android/activity/QuoteSearchActivity;->m:Landroid/os/Handler;

    invoke-direct {v1, p0, v2}, Lcom/chase/sig/android/activity/QuoteSearchActivity$b;-><init>(Lcom/chase/sig/android/activity/QuoteSearchActivity;Landroid/os/Handler;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 76
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuoteSearchActivity;->d:Lcom/chase/sig/android/domain/QuoteResponse;

    invoke-direct {p0, v0}, Lcom/chase/sig/android/activity/QuoteSearchActivity;->a(Lcom/chase/sig/android/domain/QuoteResponse;)V

    .line 77
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3
    .parameter

    .prologue
    .line 112
    invoke-static {p1}, Lcom/chase/sig/android/util/s;->l(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 113
    new-instance v0, Lcom/chase/sig/android/domain/QuoteResponse;

    invoke-direct {v0}, Lcom/chase/sig/android/domain/QuoteResponse;-><init>()V

    .line 114
    invoke-direct {p0, p1, v0}, Lcom/chase/sig/android/activity/QuoteSearchActivity;->a(Ljava/lang/String;Lcom/chase/sig/android/domain/QuoteResponse;)V

    .line 115
    invoke-direct {p0, v0}, Lcom/chase/sig/android/activity/QuoteSearchActivity;->a(Lcom/chase/sig/android/domain/QuoteResponse;)V

    .line 126
    :goto_0
    return-void

    .line 116
    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuoteSearchActivity;->k:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/chase/sig/android/activity/QuoteSearchActivity;->d:Lcom/chase/sig/android/domain/QuoteResponse;

    if-eqz v0, :cond_1

    .line 119
    iget-object v0, p0, Lcom/chase/sig/android/activity/QuoteSearchActivity;->d:Lcom/chase/sig/android/domain/QuoteResponse;

    invoke-direct {p0, v0}, Lcom/chase/sig/android/activity/QuoteSearchActivity;->a(Lcom/chase/sig/android/domain/QuoteResponse;)V

    goto :goto_0

    .line 120
    :cond_1
    invoke-static {p0, p1}, Lcom/chase/sig/android/activity/QuoteSearchActivity$a;->a(Lcom/chase/sig/android/activity/QuoteSearchActivity;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 121
    invoke-direct {p0}, Lcom/chase/sig/android/activity/QuoteSearchActivity;->d()V

    goto :goto_0

    .line 123
    :cond_2
    invoke-static {p0}, Lcom/chase/sig/android/activity/QuoteSearchActivity$a;->a(Lcom/chase/sig/android/activity/QuoteSearchActivity;)V

    .line 124
    const-class v0, Lcom/chase/sig/android/activity/QuoteSearchActivity$a;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/QuoteSearchActivity;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 87
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ai;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 88
    const-string v0, "QuoteSearchResponse"

    iget-object v1, p0, Lcom/chase/sig/android/activity/QuoteSearchActivity;->d:Lcom/chase/sig/android/domain/QuoteResponse;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 89
    const-string v0, "QuoteSearchTerm"

    iget-object v1, p0, Lcom/chase/sig/android/activity/QuoteSearchActivity;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    return-void
.end method
