.class public Lcom/chase/sig/android/activity/ReceiptDetailActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/ReceiptDetailActivity$a;
    }
.end annotation


# instance fields
.field private a:Landroid/widget/Button;

.field private b:Landroid/widget/Button;

.field private c:Lcom/chase/sig/android/domain/Receipt;

.field private d:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    .line 35
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/chase/sig/android/activity/ReceiptDetailActivity;->d:Z

    .line 193
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/ReceiptDetailActivity;)Lcom/chase/sig/android/domain/Receipt;
    .locals 1
    .parameter

    .prologue
    .line 29
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptDetailActivity;->c:Lcom/chase/sig/android/domain/Receipt;

    return-object v0
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/ReceiptDetailActivity;)Z
    .locals 1
    .parameter

    .prologue
    .line 29
    iget-boolean v0, p0, Lcom/chase/sig/android/activity/ReceiptDetailActivity;->d:Z

    return v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 7
    .parameter

    .prologue
    .line 48
    const v0, 0x7f030099

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptDetailActivity;->b(I)V

    .line 49
    const v0, 0x7f070265

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptDetailActivity;->setTitle(I)V

    .line 51
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/ReceiptDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "receipt"

    invoke-static {p1, v0, v1}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/Receipt;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptDetailActivity;->c:Lcom/chase/sig/android/domain/Receipt;

    .line 54
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/ReceiptDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "is_browsing_receipts"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/chase/sig/android/activity/ReceiptDetailActivity;->d:Z

    .line 58
    iget-object v2, p0, Lcom/chase/sig/android/activity/ReceiptDetailActivity;->c:Lcom/chase/sig/android/domain/Receipt;

    const v0, 0x7f09027d

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailView;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v4, "Description"

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/Receipt;->c()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v4, v5}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v4, "Amount"

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/Receipt;->b()Lcom/chase/sig/android/util/Dollar;

    move-result-object v5

    invoke-virtual {v5}, Lcom/chase/sig/android/util/Dollar;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v4, v5}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/ReceiptDetailActivity;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v1

    iget-object v1, v1, Lcom/chase/sig/android/domain/o;->b:Lcom/chase/sig/android/domain/g;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/Receipt;->a()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4}, Lcom/chase/sig/android/domain/g;->a(Ljava/lang/String;)Lcom/chase/sig/android/domain/IAccount;

    move-result-object v1

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/ReceiptDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "show_all_details"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Transaction date"

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/Receipt;->e()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/chase/sig/android/util/s;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Account"

    if-eqz v1, :cond_4

    invoke-interface {v1}, Lcom/chase/sig/android/domain/IAccount;->w()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-direct {v4, v5, v1}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v4, "Status"

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/Receipt;->d()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v4, v5}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    invoke-virtual {v2}, Lcom/chase/sig/android/domain/Receipt;->k()Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v1

    if-eqz v1, :cond_1

    new-instance v1, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v4, "Category"

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/Receipt;->k()Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v5

    invoke-virtual {v5}, Lcom/chase/sig/android/domain/LabeledValue;->a()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v4, v5}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-virtual {v2}, Lcom/chase/sig/android/domain/Receipt;->l()Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v1

    if-eqz v1, :cond_2

    new-instance v1, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v4, "Expense type"

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/Receipt;->l()Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v5

    invoke-virtual {v5}, Lcom/chase/sig/android/domain/LabeledValue;->a()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v4, v5}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    invoke-virtual {v2}, Lcom/chase/sig/android/domain/Receipt;->m()Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v1

    if-eqz v1, :cond_3

    new-instance v1, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v4, "Tax deduction"

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/Receipt;->m()Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v2

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/LabeledValue;->a()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v4, v2}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Lcom/chase/sig/android/view/detail/a;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/chase/sig/android/view/detail/a;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/detail/DetailView;->setRows([Lcom/chase/sig/android/view/detail/a;)V

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/DetailView;->c()V

    .line 60
    const v0, 0x7f09027e

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptDetailActivity;->a:Landroid/widget/Button;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptDetailActivity;->a:Landroid/widget/Button;

    const v1, 0x7f07026c

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptDetailActivity;->a:Landroid/widget/Button;

    new-instance v1, Lcom/chase/sig/android/activity/jx;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/jx;-><init>(Lcom/chase/sig/android/activity/ReceiptDetailActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 61
    const v0, 0x7f09027f

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptDetailActivity;->b:Landroid/widget/Button;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptDetailActivity;->b:Landroid/widget/Button;

    const v1, 0x7f07026b

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptDetailActivity;->b:Landroid/widget/Button;

    new-instance v1, Lcom/chase/sig/android/activity/ka;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/ka;-><init>(Lcom/chase/sig/android/activity/ReceiptDetailActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 62
    const v0, 0x7f0900a2

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/chase/sig/android/activity/jw;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/jw;-><init>(Lcom/chase/sig/android/activity/ReceiptDetailActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 63
    return-void

    .line 58
    :cond_4
    const-string v1, "NA"

    goto/16 :goto_0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 4
    .parameter

    .prologue
    .line 103
    new-instance v0, Lcom/chase/sig/android/view/k$a;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/view/k$a;-><init>(Landroid/content/Context;)V

    .line 105
    packed-switch p1, :pswitch_data_0

    .line 128
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ai;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    .line 107
    :pswitch_0
    const v1, 0x7f070276

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/k$a;->b(I)Lcom/chase/sig/android/view/k$a;

    move-result-object v1

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/chase/sig/android/view/k$a;->i:Z

    const v2, 0x7f07026d

    new-instance v3, Lcom/chase/sig/android/activity/jz;

    invoke-direct {v3, p0}, Lcom/chase/sig/android/activity/jz;-><init>(Lcom/chase/sig/android/activity/ReceiptDetailActivity;)V

    invoke-virtual {v1, v2, v3}, Lcom/chase/sig/android/view/k$a;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    move-result-object v1

    const v2, 0x7f070277

    new-instance v3, Lcom/chase/sig/android/activity/jy;

    invoke-direct {v3, p0}, Lcom/chase/sig/android/activity/jy;-><init>(Lcom/chase/sig/android/activity/ReceiptDetailActivity;)V

    invoke-virtual {v1, v2, v3}, Lcom/chase/sig/android/view/k$a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    .line 125
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/k$a;->d(I)Lcom/chase/sig/android/view/k;

    move-result-object v0

    goto :goto_0

    .line 105
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
