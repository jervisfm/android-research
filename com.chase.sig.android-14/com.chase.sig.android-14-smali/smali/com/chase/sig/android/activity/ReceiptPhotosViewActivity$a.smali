.class final Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$a;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lcom/chase/sig/android/domain/ReceiptPhotoList$ReceiptPhoto;",
        "Ljava/lang/Void;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;

.field private final b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/widget/ImageView;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/widget/ProgressBar;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;Landroid/widget/ImageView;Landroid/widget/ProgressBar;)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 315
    iput-object p1, p0, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$a;->a:Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 318
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$a;->b:Ljava/lang/ref/WeakReference;

    .line 319
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$a;->c:Ljava/lang/ref/WeakReference;

    .line 320
    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .parameter

    .prologue
    .line 311
    check-cast p1, [Lcom/chase/sig/android/domain/ReceiptPhotoList$ReceiptPhoto;

    const/4 v0, 0x0

    aget-object v1, p1, v0

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$a;->a:Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;

    invoke-virtual {v2}, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Display;->getWidth()I

    move-result v3

    invoke-virtual {v2}, Landroid/view/Display;->getHeight()I

    move-result v2

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/ReceiptPhotoList$ReceiptPhoto;->b()[B

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/ReceiptPhotoList$ReceiptPhoto;->b()[B

    move-result-object v0

    invoke-static {v0, v3, v2}, Lcom/chase/sig/android/util/imagebrowser/a;->a([BII)Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$a;->a:Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {}, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;->d()I

    move-result v1

    invoke-static {v0, v1, v3, v2}, Lcom/chase/sig/android/util/imagebrowser/a;->a(Landroid/content/res/Resources;III)Landroid/graphics/Bitmap;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 6
    .parameter

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x0

    .line 311
    check-cast p1, Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$a;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$a;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iget-object v2, p0, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$a;->b:Ljava/lang/ref/WeakReference;

    if-eqz v2, :cond_1

    if-eqz p1, :cond_1

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    check-cast v2, Landroid/graphics/drawable/BitmapDrawable;

    const/4 v3, 0x0

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    :goto_0
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    invoke-static {}, Ljava/lang/System;->gc()V

    :cond_0
    invoke-virtual {v1, v5}, Landroid/widget/ProgressBar;->setVisibility(I)V

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual {v0}, Landroid/widget/ImageView;->invalidate()V

    :cond_1
    if-nez p1, :cond_2

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual {v1, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    :cond_2
    return-void

    :cond_3
    move-object v2, v3

    goto :goto_0
.end method
