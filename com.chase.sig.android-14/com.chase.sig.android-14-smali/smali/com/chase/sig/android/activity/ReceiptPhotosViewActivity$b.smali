.class final Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$b;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lcom/chase/sig/android/domain/ReceiptPhotoList$ReceiptPhoto;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/domain/ReceiptPhotoList$ReceiptPhoto;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;

.field private final b:I

.field private final c:I


# direct methods
.method private constructor <init>(Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;)V
    .locals 1
    .parameter

    .prologue
    const/16 v0, 0x60

    .line 119
    iput-object p1, p0, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$b;->a:Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 120
    iput v0, p0, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$b;->b:I

    .line 121
    iput v0, p0, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$b;->c:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;B)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 119
    invoke-direct {p0, p1}, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$b;-><init>(Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;)V

    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .parameter

    .prologue
    const/16 v4, 0x60

    .line 119
    check-cast p1, [Lcom/chase/sig/android/domain/ReceiptPhotoList$ReceiptPhoto;

    array-length v0, p1

    if-gtz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    aget-object v1, p1, v0

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/ReceiptPhotoList$ReceiptPhoto;->b()[B

    move-result-object v0

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Downloading: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/ReceiptPhotoList$ReceiptPhoto;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$b;->a:Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;->A()Lcom/chase/sig/android/service/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/service/n;->r()Lcom/chase/sig/android/service/ac;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/ReceiptPhotoList$ReceiptPhoto;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/service/ac;->a(Ljava/lang/String;)[B

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-static {v2, v4, v4}, Lcom/chase/sig/android/util/imagebrowser/a;->a([BII)Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$b;->a:Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {}, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;->d()I

    move-result v3

    invoke-static {v0, v3, v4, v4}, Lcom/chase/sig/android/util/imagebrowser/a;->a(Landroid/content/res/Resources;III)Landroid/graphics/Bitmap;

    move-result-object v0

    :cond_1
    iget-object v3, p0, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$b;->a:Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;

    iget-object v3, v3, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;->a:Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$d;

    iget-object v3, v3, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$d;->b:Ljava/util/Map;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/ReceiptPhotoList$ReceiptPhoto;->a()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/domain/ReceiptPhotoList$ReceiptPhoto;->a([B)V

    :goto_1
    move-object v0, v1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$b;->a:Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {}, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;->e()I

    move-result v2

    invoke-static {v0, v2, v4, v4}, Lcom/chase/sig/android/util/imagebrowser/a;->a(Landroid/content/res/Resources;III)Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v2, p0, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$b;->a:Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;

    iget-object v2, v2, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;->a:Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$d;

    iget-object v2, v2, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$d;->b:Ljava/util/Map;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/ReceiptPhotoList$ReceiptPhoto;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "No need to download: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/ReceiptPhotoList$ReceiptPhoto;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    goto :goto_1
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 2
    .parameter

    .prologue
    .line 119
    check-cast p1, Lcom/chase/sig/android/domain/ReceiptPhotoList$ReceiptPhoto;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Done fetching image"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/ReceiptPhotoList$ReceiptPhoto;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$b;->a:Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;

    iget-object v0, v0, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;->b:Landroid/widget/Gallery;

    invoke-virtual {v0}, Landroid/widget/Gallery;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$c;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$c;->notifyDataSetChanged()V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$b;->a:Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;->a(Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;)V

    return-void
.end method
