.class final Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$c;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$c$a;
    }
.end annotation


# instance fields
.field a:Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$d;

.field b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/ReceiptPhotoList$ReceiptPhoto;",
            ">;"
        }
    .end annotation
.end field

.field c:Landroid/content/Context;

.field d:Landroid/widget/Gallery;

.field e:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$d;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 214
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 215
    iput-object p1, p0, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$c;->c:Landroid/content/Context;

    .line 216
    iput-object p2, p0, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$c;->a:Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$d;

    .line 217
    iget-object v0, p2, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$d;->a:Lcom/chase/sig/android/domain/ReceiptPhotoList;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/ReceiptPhotoList;->a()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$c;->b:Ljava/util/List;

    .line 222
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$c;->c:Landroid/content/Context;

    sget-object v1, Lcom/chase/sig/android/e$a;->Gallery1:[I

    invoke-virtual {v0, v1}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 223
    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$c;->e:I

    .line 225
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 227
    return-void
.end method


# virtual methods
.method public final getCount()I
    .locals 1

    .prologue
    .line 231
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$c;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 236
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$c;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2
    .parameter

    .prologue
    .line 243
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x0

    .line 248
    .line 250
    if-nez p2, :cond_0

    .line 252
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$c;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03009b

    invoke-virtual {v0, v1, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 257
    new-instance v1, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$c$a;

    invoke-direct {v1, p2}, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$c$a;-><init>(Landroid/view/View;)V

    .line 258
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 260
    const v0, 0x7f090285

    invoke-virtual {p3, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Gallery;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$c;->d:Landroid/widget/Gallery;

    .line 265
    :goto_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$c;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/ReceiptPhotoList$ReceiptPhoto;

    .line 268
    iget-object v2, p0, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$c;->a:Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$d;

    iget-object v2, v2, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$d;->b:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/ReceiptPhotoList$ReceiptPhoto;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 269
    if-eqz v0, :cond_1

    .line 270
    iget-object v2, v1, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$c$a;->b:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 271
    iget-object v2, v1, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$c$a;->a:Landroid/widget/ImageView;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 272
    iget-object v2, v1, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$c$a;->a:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 279
    :goto_1
    iget-object v0, v1, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$c$a;->a:Landroid/widget/ImageView;

    iget v1, p0, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$c;->e:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 281
    return-object p2

    .line 262
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$c$a;

    move-object v1, v0

    goto :goto_0

    .line 275
    :cond_1
    iget-object v0, v1, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$c$a;->b:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 276
    iget-object v0, v1, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$c$a;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1
.end method
