.class public Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$a;,
        Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$c;,
        Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$b;,
        Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$d;
    }
.end annotation


# static fields
.field private static l:I

.field private static m:I

.field private static n:I


# instance fields
.field a:Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$d;

.field b:Landroid/widget/Gallery;

.field c:Landroid/widget/ImageView;

.field d:Landroid/widget/ProgressBar;

.field k:Lcom/chase/sig/android/domain/ReceiptPhotoList$ReceiptPhoto;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const v1, 0x7f020008

    .line 35
    const v0, 0x7f020084

    sput v0, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;->l:I

    .line 36
    sput v1, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;->m:I

    .line 37
    sput v1, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;->n:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    .line 311
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;)V
    .locals 4
    .parameter

    .prologue
    .line 30
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;->k:Lcom/chase/sig/android/domain/ReceiptPhotoList$ReceiptPhoto;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$a;

    iget-object v1, p0, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;->c:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;->d:Landroid/widget/ProgressBar;

    invoke-direct {v0, p0, v1, v2}, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$a;-><init>(Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;Landroid/widget/ImageView;Landroid/widget/ProgressBar;)V

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/chase/sig/android/domain/ReceiptPhotoList$ReceiptPhoto;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;->k:Lcom/chase/sig/android/domain/ReceiptPhotoList$ReceiptPhoto;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_0
    return-void
.end method

.method static synthetic d()I
    .locals 1

    .prologue
    .line 30
    sget v0, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;->m:I

    return v0
.end method

.method static synthetic e()I
    .locals 1

    .prologue
    .line 30
    sget v0, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;->n:I

    return v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 5
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 56
    const v0, 0x7f03009c

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;->b(I)V

    .line 58
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 62
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;->getLastNonConfigurationInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$d;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;->a:Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$d;

    .line 63
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;->a:Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$d;

    if-nez v0, :cond_0

    if-eqz v1, :cond_0

    .line 64
    new-instance v0, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$d;

    invoke-direct {v0, v4}, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$d;-><init>(B)V

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;->a:Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$d;

    .line 65
    iget-object v2, p0, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;->a:Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$d;

    const-string v0, "receipt_photo_list"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/ReceiptPhotoList;

    iput-object v0, v2, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$d;->a:Lcom/chase/sig/android/domain/ReceiptPhotoList;

    .line 70
    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;->a:Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$d;

    iget-object v0, v0, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$d;->a:Lcom/chase/sig/android/domain/ReceiptPhotoList;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/ReceiptPhotoList;->a()Ljava/util/List;

    move-result-object v0

    .line 71
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/ReceiptPhotoList$ReceiptPhoto;

    .line 72
    new-instance v2, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$b;

    invoke-direct {v2, p0, v4}, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$b;-><init>(Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;B)V

    .line 73
    const/4 v3, 0x1

    new-array v3, v3, [Lcom/chase/sig/android/domain/ReceiptPhotoList$ReceiptPhoto;

    aput-object v0, v3, v4

    invoke-virtual {v2, v3}, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$b;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 76
    :cond_1
    const v0, 0x7f090285

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Gallery;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;->b:Landroid/widget/Gallery;

    const v0, 0x7f0900e9

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;->c:Landroid/widget/ImageView;

    const v0, 0x7f090286

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;->d:Landroid/widget/ProgressBar;

    .line 77
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;->b:Landroid/widget/Gallery;

    new-instance v1, Lcom/chase/sig/android/activity/kb;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/kb;-><init>(Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Gallery;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 78
    return-void
.end method

.method protected final m()V
    .locals 0

    .prologue
    .line 295
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 299
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;->c:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .line 300
    if-eqz v0, :cond_0

    .line 301
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 302
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 304
    :cond_0
    invoke-super {p0}, Lcom/chase/sig/android/activity/ai;->onDestroy()V

    .line 305
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 82
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;->b:Landroid/widget/Gallery;

    new-instance v1, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$c;

    iget-object v2, p0, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;->a:Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$d;

    invoke-direct {v1, p0, v2}, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$c;-><init>(Landroid/content/Context;Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$d;)V

    invoke-virtual {v0, v1}, Landroid/widget/Gallery;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 83
    invoke-super {p0}, Lcom/chase/sig/android/activity/ai;->onResume()V

    .line 84
    return-void
.end method

.method public onRetainNonConfigurationInstance()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 288
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;->a:Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity$d;

    return-object v0
.end method
