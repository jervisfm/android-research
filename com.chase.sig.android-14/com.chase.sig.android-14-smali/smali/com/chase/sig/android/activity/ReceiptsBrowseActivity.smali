.class public Lcom/chase/sig/android/activity/ReceiptsBrowseActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/ReceiptsBrowseActivity$a;
    }
.end annotation


# instance fields
.field private a:Lcom/chase/sig/android/view/detail/DetailView;

.field private b:Lcom/chase/sig/android/service/ListContentResponse;

.field private c:Lcom/chase/sig/android/domain/ReceiptAccountSummary;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    .line 69
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/ReceiptsBrowseActivity;)Lcom/chase/sig/android/service/ListContentResponse;
    .locals 1
    .parameter

    .prologue
    .line 24
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsBrowseActivity;->b:Lcom/chase/sig/android/service/ListContentResponse;

    return-object v0
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/ReceiptsBrowseActivity;Lcom/chase/sig/android/service/ListContentResponse;)Lcom/chase/sig/android/service/ListContentResponse;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 24
    iput-object p1, p0, Lcom/chase/sig/android/activity/ReceiptsBrowseActivity;->b:Lcom/chase/sig/android/service/ListContentResponse;

    return-object p1
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 7
    .parameter

    .prologue
    .line 35
    const v0, 0x7f03009d

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsBrowseActivity;->b(I)V

    .line 36
    const v0, 0x7f070270

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsBrowseActivity;->setTitle(I)V

    .line 38
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/ReceiptsBrowseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 39
    const-string v1, "acctSummary"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 40
    const-string v1, "acctSummary"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/ReceiptAccountSummary;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsBrowseActivity;->c:Lcom/chase/sig/android/domain/ReceiptAccountSummary;

    .line 42
    const v0, 0x7f090287

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsBrowseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsBrowseActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    .line 43
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsBrowseActivity;->c:Lcom/chase/sig/android/domain/ReceiptAccountSummary;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/ReceiptAccountSummary;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/ReceiptAccount;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/ReceiptAccount;->b()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/ReceiptAccount;->d()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " receipts / "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/ReceiptAccount;->a()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " unmatched"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/chase/sig/android/view/detail/DetailRowWithSubItem;

    const/4 v6, 0x0

    invoke-direct {v5, v3, v4, v6}, Lcom/chase/sig/android/view/detail/DetailRowWithSubItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5}, Lcom/chase/sig/android/view/detail/DetailRowWithSubItem;->d()Lcom/chase/sig/android/view/detail/a;

    move-result-object v4

    new-instance v5, Lcom/chase/sig/android/activity/kc;

    invoke-direct {v5, p0, v3, v0}, Lcom/chase/sig/android/activity/kc;-><init>(Lcom/chase/sig/android/activity/ReceiptsBrowseActivity;Ljava/lang/String;Lcom/chase/sig/android/domain/ReceiptAccount;)V

    iput-object v5, v4, Lcom/chase/sig/android/view/detail/a;->h:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lcom/chase/sig/android/view/detail/a;

    iget-object v2, p0, Lcom/chase/sig/android/activity/ReceiptsBrowseActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/chase/sig/android/view/detail/a;

    invoke-virtual {v2, v0}, Lcom/chase/sig/android/view/detail/DetailView;->setRows([Lcom/chase/sig/android/view/detail/a;)V

    .line 46
    :cond_1
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/ReceiptsBrowseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "categories"

    invoke-static {p1, v0, v1}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/service/ListContentResponse;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsBrowseActivity;->b:Lcom/chase/sig/android/service/ListContentResponse;

    .line 48
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsBrowseActivity;->b:Lcom/chase/sig/android/service/ListContentResponse;

    if-nez v0, :cond_2

    .line 49
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsBrowseActivity;->e:Lcom/chase/sig/android/activity/mc;

    const-class v1, Lcom/chase/sig/android/activity/ReceiptsBrowseActivity$a;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/mc;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/ae;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/activity/ReceiptsBrowseActivity$a;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/ReceiptsBrowseActivity$a;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v1

    sget-object v2, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-eq v1, v2, :cond_2

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/ReceiptsBrowseActivity$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 51
    :cond_2
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 54
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ai;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 56
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsBrowseActivity;->b:Lcom/chase/sig/android/service/ListContentResponse;

    if-eqz v0, :cond_0

    .line 57
    const-string v0, "categories"

    iget-object v1, p0, Lcom/chase/sig/android/activity/ReceiptsBrowseActivity;->b:Lcom/chase/sig/android/service/ListContentResponse;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 60
    :cond_0
    return-void
.end method
