.class public Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$a;


# instance fields
.field a:Landroid/hardware/Camera$ShutterCallback;

.field b:Landroid/hardware/Camera$PictureCallback;

.field c:Landroid/hardware/Camera$PictureCallback;

.field private d:Landroid/widget/ImageView;

.field private k:Landroid/widget/FrameLayout;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    .line 123
    new-instance v0, Lcom/chase/sig/android/activity/kg;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/activity/kg;-><init>(Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;)V

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;->a:Landroid/hardware/Camera$ShutterCallback;

    .line 130
    new-instance v0, Lcom/chase/sig/android/activity/kh;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/activity/kh;-><init>(Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;)V

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;->b:Landroid/hardware/Camera$PictureCallback;

    .line 137
    new-instance v0, Lcom/chase/sig/android/activity/ki;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/activity/ki;-><init>(Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;)V

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;->c:Landroid/hardware/Camera$PictureCallback;

    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;->d()V

    return-void
.end method

.method static synthetic a([B)[B
    .locals 4
    .parameter

    .prologue
    .line 32
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    const/16 v1, 0x4000

    new-array v1, v1, [B

    iput-object v1, v0, Landroid/graphics/BitmapFactory$Options;->inTempStorage:[B

    sget-object v1, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object v1, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    const/4 v1, 0x0

    array-length v2, p0

    invoke-static {p0, v1, v2, v0}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v3, 0x1e

    invoke-virtual {v0, v2, v3, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    return-object v1
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;)Landroid/widget/ImageView;
    .locals 1
    .parameter

    .prologue
    .line 32
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;->d:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic c(Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;)Landroid/widget/FrameLayout;
    .locals 1
    .parameter

    .prologue
    .line 32
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;->k:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 118
    const v0, 0x7f090034

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 120
    const v1, 0x7f0700f6

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 121
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 5
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 47
    new-instance v2, Lcom/chase/sig/android/activity/CapturePreview;

    const/16 v0, 0x1e

    sget-object v1, Lcom/chase/sig/android/activity/CapturePreview$Orientation;->b:Lcom/chase/sig/android/activity/CapturePreview$Orientation;

    invoke-direct {v2, p0, v0, v1}, Lcom/chase/sig/android/activity/CapturePreview;-><init>(Landroid/content/Context;ILcom/chase/sig/android/activity/CapturePreview$Orientation;)V

    .line 50
    const v0, 0x7f09003f

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;->k:Landroid/widget/FrameLayout;

    .line 51
    const v0, 0x7f090032

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 52
    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 54
    const v0, 0x7f090036

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 55
    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 57
    const v1, 0x7f07027b

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    .line 58
    const v1, 0x7f09003e

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 60
    const v1, 0x7f09003c

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;->d:Landroid/widget/ImageView;

    .line 62
    new-instance v1, Lcom/chase/sig/android/activity/kd;

    invoke-direct {v1, p0, v0}, Lcom/chase/sig/android/activity/kd;-><init>(Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;Landroid/widget/Button;)V

    .line 84
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 86
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 87
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;->d:Landroid/widget/ImageView;

    new-instance v1, Lcom/chase/sig/android/activity/ke;

    invoke-direct {v1, p0, v2}, Lcom/chase/sig/android/activity/ke;-><init>(Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;Lcom/chase/sig/android/activity/CapturePreview;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 108
    return-void
.end method

.method protected final a_()V
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;->requestWindowFeature(I)Z

    .line 41
    const v0, 0x7f03009f

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;->setContentView(I)V

    .line 42
    return-void
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 198
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "receipt_photo_list"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 199
    const/4 v0, -0x4

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/eb;->showDialog(I)V

    .line 203
    :goto_0
    return-void

    .line 201
    :cond_0
    invoke-super {p0}, Lcom/chase/sig/android/activity/ai;->onBackPressed()V

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 186
    const/16 v0, 0x1b

    if-ne p1, v0, :cond_1

    .line 187
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 188
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;->d:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->performClick()Z

    .line 190
    :cond_0
    const/4 v0, 0x1

    .line 192
    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1, p2}, Lcom/chase/sig/android/activity/ai;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 112
    invoke-super {p0}, Lcom/chase/sig/android/activity/ai;->onResume()V

    .line 114
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;->d()V

    .line 115
    return-void
.end method
