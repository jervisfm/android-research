.class public Lcom/chase/sig/android/activity/ReceiptsCancelActivity$b;
.super Lcom/chase/sig/android/b;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/ReceiptsCancelActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/b",
        "<",
        "Lcom/chase/sig/android/activity/ReceiptsCancelActivity;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/service/content/ContentResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 86
    invoke-direct {p0}, Lcom/chase/sig/android/b;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 86
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/ReceiptsCancelActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/ReceiptsCancelActivity;->A()Lcom/chase/sig/android/service/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/service/n;->p()Lcom/chase/sig/android/service/content/a;

    const-string v0, "cr_cancel.html"

    invoke-static {v0}, Lcom/chase/sig/android/service/content/a;->a(Ljava/lang/String;)Lcom/chase/sig/android/service/content/ContentResponse;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 2
    .parameter

    .prologue
    .line 86
    check-cast p1, Lcom/chase/sig/android/service/content/ContentResponse;

    invoke-super {p0, p1}, Lcom/chase/sig/android/b;->a(Ljava/lang/Object;)V

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/chase/sig/android/service/content/ContentResponse;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/ReceiptsCancelActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/content/ContentResponse;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/ReceiptsCancelActivity;->b(Ljava/util/List;)V

    :cond_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/ReceiptsCancelActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/content/ContentResponse;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/chase/sig/android/activity/ReceiptsCancelActivity;->a(Lcom/chase/sig/android/activity/ReceiptsCancelActivity;Ljava/lang/String;)V

    return-void
.end method

.method protected onCancelled()V
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/ReceiptsCancelActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/ReceiptsCancelActivity;->finish()V

    .line 109
    return-void
.end method
