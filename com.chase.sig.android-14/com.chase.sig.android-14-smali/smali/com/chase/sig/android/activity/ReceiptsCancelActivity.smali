.class public Lcom/chase/sig/android/activity/ReceiptsCancelActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/ReceiptsCancelActivity$a;,
        Lcom/chase/sig/android/activity/ReceiptsCancelActivity$b;
    }
.end annotation


# static fields
.field private static a:I


# instance fields
.field private b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x0

    sput v0, Lcom/chase/sig/android/activity/ReceiptsCancelActivity;->a:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    .line 112
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/ReceiptsCancelActivity;Ljava/lang/String;)V
    .locals 6
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 24
    const v0, 0x7f09028c

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsCancelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    const-string v3, "text/html"

    const-string v4, "utf-8"

    move-object v2, p1

    move-object v5, v1

    invoke-virtual/range {v0 .. v5}, Landroid/webkit/WebView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic d()I
    .locals 1

    .prologue
    .line 24
    sget v0, Lcom/chase/sig/android/activity/ReceiptsCancelActivity;->a:I

    return v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 32
    const v0, 0x7f03009e

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsCancelActivity;->b(I)V

    .line 33
    const v0, 0x7f070261

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsCancelActivity;->setTitle(I)V

    .line 35
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/ReceiptsCancelActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "resultMessage"

    invoke-static {p1, v0, v1}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsCancelActivity;->b:Ljava/lang/String;

    .line 39
    const v0, 0x7f09028c

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsCancelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    .line 40
    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/webkit/WebSettings;->setSaveFormData(Z)V

    .line 41
    new-instance v1, Lcom/chase/sig/android/activity/kj;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/kj;-><init>(Lcom/chase/sig/android/activity/ReceiptsCancelActivity;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 55
    const v0, 0x7f09028a

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/ReceiptsCancelActivity;->C()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/ReceiptsCancelActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 57
    const v0, 0x7f09028b

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsCancelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 58
    new-instance v1, Lcom/chase/sig/android/activity/kk;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/kk;-><init>(Lcom/chase/sig/android/activity/ReceiptsCancelActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 70
    const-class v0, Lcom/chase/sig/android/activity/ReceiptsCancelActivity$b;

    new-array v1, v2, [Ljava/lang/Void;

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/ReceiptsCancelActivity;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 71
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 177
    iput-object p1, p0, Lcom/chase/sig/android/activity/ReceiptsCancelActivity;->b:Ljava/lang/String;

    .line 178
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 4
    .parameter

    .prologue
    .line 142
    sget v0, Lcom/chase/sig/android/activity/ReceiptsCancelActivity;->a:I

    if-ne p1, v0, :cond_0

    .line 143
    new-instance v0, Lcom/chase/sig/android/view/k$a;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/view/k$a;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/chase/sig/android/activity/ReceiptsCancelActivity;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/k$a;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/k$a;

    move-result-object v1

    const v2, 0x7f070068

    new-instance v3, Lcom/chase/sig/android/activity/kl;

    invoke-direct {v3, p0}, Lcom/chase/sig/android/activity/kl;-><init>(Lcom/chase/sig/android/activity/ReceiptsCancelActivity;)V

    invoke-virtual {v1, v2, v3}, Lcom/chase/sig/android/view/k$a;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    new-instance v1, Lcom/chase/sig/android/activity/km;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/km;-><init>(Lcom/chase/sig/android/activity/ReceiptsCancelActivity;)V

    iput-object v1, v0, Lcom/chase/sig/android/view/k$a;->g:Landroid/content/DialogInterface$OnCancelListener;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/k$a;->d(I)Lcom/chase/sig/android/view/k;

    move-result-object v0

    .line 145
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ai;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 75
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ai;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 77
    const-string v0, "resultMessage"

    iget-object v1, p0, Lcom/chase/sig/android/activity/ReceiptsCancelActivity;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    return-void
.end method
