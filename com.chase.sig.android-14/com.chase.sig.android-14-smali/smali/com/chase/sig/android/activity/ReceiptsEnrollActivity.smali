.class public Lcom/chase/sig/android/activity/ReceiptsEnrollActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/ReceiptsEnrollActivity$a;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    .line 64
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/ReceiptsEnrollActivity;Ljava/lang/String;)V
    .locals 6
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 15
    const v0, 0x7f09028f

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsEnrollActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    const-string v3, "text/html"

    const-string v4, "utf-8"

    move-object v2, p1

    move-object v5, v1

    invoke-virtual/range {v0 .. v5}, Landroid/webkit/WebView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 5
    .parameter

    .prologue
    const v4, 0x7f0901a9

    const/4 v1, 0x0

    .line 19
    const v0, 0x7f0300a0

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsEnrollActivity;->b(I)V

    .line 21
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/ReceiptsEnrollActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070257

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/ReceiptsEnrollActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070258

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/chase/sig/android/util/s;->z(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsEnrollActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 24
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->d:Lcom/chase/sig/android/service/SplashResponse;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->d:Lcom/chase/sig/android/service/SplashResponse;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/SplashResponse;->l()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->d:Lcom/chase/sig/android/service/SplashResponse;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/SplashResponse;->s()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsEnrollActivity;->e(Ljava/lang/String;)V

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_0

    .line 25
    const v0, 0x7f09028f

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsEnrollActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    .line 26
    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/webkit/WebSettings;->setSaveFormData(Z)V

    .line 27
    new-instance v2, Lcom/chase/sig/android/activity/kn;

    invoke-direct {v2, p0}, Lcom/chase/sig/android/activity/kn;-><init>(Lcom/chase/sig/android/activity/ReceiptsEnrollActivity;)V

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 41
    const-class v0, Lcom/chase/sig/android/activity/ReceiptsEnrollActivity$a;

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/ReceiptsEnrollActivity;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 43
    invoke-virtual {p0, v4}, Lcom/chase/sig/android/activity/ReceiptsEnrollActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 44
    const v1, 0x7f070256

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/ReceiptsEnrollActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 45
    const-class v0, Lcom/chase/sig/android/activity/ReceiptsSelectPlanActivity;

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsEnrollActivity;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/a/f;

    move-result-object v0

    invoke-virtual {p0, v4, v0}, Lcom/chase/sig/android/activity/ReceiptsEnrollActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 47
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 24
    goto :goto_0
.end method
