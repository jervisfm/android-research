.class public Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity$a;
.super Lcom/chase/sig/android/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/d",
        "<",
        "Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;",
        "Lcom/chase/sig/android/domain/Receipt;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/service/ReceiptsAddResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 708
    invoke-direct {p0}, Lcom/chase/sig/android/d;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 708
    check-cast p1, [Lcom/chase/sig/android/domain/Receipt;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->A()Lcom/chase/sig/android/service/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/service/n;->r()Lcom/chase/sig/android/service/ac;

    move-result-object v0

    aget-object v1, p1, v2

    invoke-virtual {v0, v1, v2}, Lcom/chase/sig/android/service/ac;->a(Lcom/chase/sig/android/domain/Receipt;I)Lcom/chase/sig/android/service/ReceiptsAddResponse;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 3
    .parameter

    .prologue
    .line 708
    check-cast p1, Lcom/chase/sig/android/service/ReceiptsAddResponse;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/ReceiptsAddResponse;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/chase/sig/android/service/ReceiptsAddResponse;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->b(Ljava/util/List;)V

    :goto_0
    return-void

    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/chase/sig/android/activity/TransactionDetailActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v2, 0x400

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
