.class public Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity$b;
.super Lcom/chase/sig/android/b;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/b",
        "<",
        "Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/service/ListContentResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 660
    invoke-direct {p0}, Lcom/chase/sig/android/b;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 660
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->A()Lcom/chase/sig/android/service/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/service/n;->r()Lcom/chase/sig/android/service/ac;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/service/ac;->a()Lcom/chase/sig/android/service/ListContentResponse;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 4
    .parameter

    .prologue
    .line 660
    check-cast p1, Lcom/chase/sig/android/service/ListContentResponse;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/ListContentResponse;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/chase/sig/android/service/ListContentResponse;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->b(Ljava/util/List;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/chase/sig/android/service/ListContentResponse;->a()Ljava/util/LinkedHashMap;

    move-result-object v2

    const-string v1, "categoryFilter"

    invoke-virtual {v2, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/domain/receipt/ReceiptFilterList;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/receipt/ReceiptFilterList;->a()Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->a(Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;Ljava/util/List;)Ljava/util/List;

    const-string v3, "CATEGORY"

    iget-object v1, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v1, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;

    invoke-static {v1}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->e(Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;)Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v3, v1}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->a(Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;Ljava/lang/String;Ljava/util/List;)V

    const-string v1, "taxDeductibleFilter"

    invoke-virtual {v2, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/domain/receipt/ReceiptFilterList;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/receipt/ReceiptFilterList;->a()Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->b(Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;Ljava/util/List;)Ljava/util/List;

    const-string v3, "TAX_EXEMPT"

    iget-object v1, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v1, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;

    invoke-static {v1}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->f(Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;)Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v3, v1}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->a(Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;Ljava/lang/String;Ljava/util/List;)V

    const-string v1, "expenseTypeFilter"

    invoke-virtual {v2, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/domain/receipt/ReceiptFilterList;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/receipt/ReceiptFilterList;->a()Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->c(Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;Ljava/util/List;)Ljava/util/List;

    const-string v2, "EXPENSE_TYPE"

    iget-object v1, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v1, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;

    invoke-static {v1}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->g(Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;)Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v2, v1}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->a(Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;Ljava/lang/String;Ljava/util/List;)V

    invoke-static {v0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->a(Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;)Lcom/chase/sig/android/domain/Receipt;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {v0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->h(Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;)V

    goto :goto_0
.end method

.method protected onCancelled()V
    .locals 0

    .prologue
    .line 704
    invoke-super {p0}, Lcom/chase/sig/android/b;->onCancelled()V

    .line 705
    return-void
.end method
