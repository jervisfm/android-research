.class public Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity$c;
.super Lcom/chase/sig/android/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/d",
        "<",
        "Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;",
        "Lcom/chase/sig/android/domain/Receipt;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/service/l;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 738
    invoke-direct {p0}, Lcom/chase/sig/android/d;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .parameter

    .prologue
    .line 738
    check-cast p1, [Lcom/chase/sig/android/domain/Receipt;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->A()Lcom/chase/sig/android/service/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/service/n;->r()Lcom/chase/sig/android/service/ac;

    const/4 v0, 0x0

    aget-object v0, p1, v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/chase/sig/android/service/ac;->a(Lcom/chase/sig/android/domain/Receipt;Z)Lcom/chase/sig/android/service/l;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 4
    .parameter

    .prologue
    const/high16 v3, 0x400

    .line 738
    check-cast p1, Lcom/chase/sig/android/service/l;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/l;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/chase/sig/android/service/l;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->b(Ljava/util/List;)V

    :goto_0
    return-void

    :cond_0
    invoke-static {v0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->i(Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/chase/sig/android/activity/ReceiptsHomeActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    :goto_1
    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_1
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/chase/sig/android/activity/TransactionDetailActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    goto :goto_1
.end method
