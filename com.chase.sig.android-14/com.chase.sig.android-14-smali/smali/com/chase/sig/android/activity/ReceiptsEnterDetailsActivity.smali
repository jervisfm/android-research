.class public Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;
.super Lcom/chase/sig/android/activity/a;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity$c;,
        Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity$a;,
        Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity$b;
    }
.end annotation


# instance fields
.field private A:Ljava/lang/String;

.field private B:Lcom/chase/sig/android/view/b$a;

.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/IAccount;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/LabeledValue;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/LabeledValue;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/LabeledValue;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private p:Lcom/chase/sig/android/domain/Receipt;

.field private q:Lcom/chase/sig/android/domain/ReceiptPhotoList;

.field private r:Landroid/widget/TextView;

.field private s:Lcom/chase/sig/android/view/detail/DetailView;

.field private t:Landroid/widget/Button;

.field private u:Landroid/widget/Button;

.field private v:Z

.field private w:Z

.field private x:Z

.field private y:Z

.field private final z:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 57
    invoke-direct {p0}, Lcom/chase/sig/android/activity/a;-><init>()V

    .line 70
    const-string v0, ""

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->k:Ljava/lang/String;

    .line 72
    const-string v0, ""

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->m:Ljava/lang/String;

    .line 73
    const-string v0, ""

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->n:Ljava/lang/String;

    .line 83
    iput-boolean v1, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->v:Z

    .line 84
    iput-boolean v1, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->w:Z

    .line 85
    iput-boolean v1, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->x:Z

    .line 86
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->y:Z

    .line 88
    const/16 v0, 0x19

    iput v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->z:I

    .line 90
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->a(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->A:Ljava/lang/String;

    .line 409
    new-instance v0, Lcom/chase/sig/android/activity/kp;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/activity/kp;-><init>(Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;)V

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->B:Lcom/chase/sig/android/view/b$a;

    .line 738
    return-void
.end method

.method private static a(Lcom/chase/sig/android/domain/LabeledValue;Ljava/util/List;)I
    .locals 4
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/chase/sig/android/domain/LabeledValue;",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/LabeledValue;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 543
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    .line 545
    if-eqz p0, :cond_1

    .line 547
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 549
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/LabeledValue;

    .line 550
    invoke-virtual {p0}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v3

    .line 552
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 557
    :goto_1
    return v0

    .line 547
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 557
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;)Lcom/chase/sig/android/domain/Receipt;
    .locals 1
    .parameter

    .prologue
    .line 57
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->p:Lcom/chase/sig/android/domain/Receipt;

    return-object v0
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 57
    iput-object p1, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->b:Ljava/util/List;

    return-object p1
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;Ljava/lang/String;Ljava/util/List;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 57
    invoke-direct {p0, p1, p2}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->a(Ljava/lang/String;Ljava/util/List;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/util/List;)V
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/LabeledValue;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 586
    invoke-virtual {p0, p1}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->b(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/q;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/q;->q()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v0

    .line 587
    invoke-static {p0, p2}, Lcom/chase/sig/android/util/a;->g(Landroid/content/Context;Ljava/util/List;)Landroid/widget/SimpleAdapter;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/JPSpinner;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 589
    return-void
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 57
    iput-object p1, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->c:Ljava/util/List;

    return-object p1
.end method

.method private b(Ljava/lang/String;Ljava/util/List;)V
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/IAccount;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 592
    invoke-virtual {p0, p1}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->b(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/q;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/q;->q()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v0

    .line 593
    invoke-static {p0, p2}, Lcom/chase/sig/android/util/a;->a(Landroid/content/Context;Ljava/util/List;)Landroid/widget/SimpleAdapter;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/JPSpinner;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 594
    return-void
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;)Z
    .locals 1
    .parameter

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->d()Z

    move-result v0

    return v0
.end method

.method static synthetic c(Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 57
    iput-object p1, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->d:Ljava/util/List;

    return-object p1
.end method

.method static synthetic c(Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;)Z
    .locals 1
    .parameter

    .prologue
    .line 57
    iget-boolean v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->v:Z

    return v0
.end method

.method static synthetic d(Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;)Lcom/chase/sig/android/domain/Receipt;
    .locals 4
    .parameter

    .prologue
    .line 57
    new-instance v1, Lcom/chase/sig/android/domain/Receipt;

    invoke-direct {v1}, Lcom/chase/sig/android/domain/Receipt;-><init>()V

    const-string v0, "DESCRIPTION"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/l;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/l;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/Receipt;->b(Ljava/lang/String;)V

    const-string v0, "AMOUNT"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/c;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/c;->q()Lcom/chase/sig/android/util/Dollar;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/Receipt;->a(Lcom/chase/sig/android/util/Dollar;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->b:Ljava/util/List;

    const-string v2, "CATEGORY"

    invoke-virtual {p0, v2}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->b(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/q;

    move-result-object v2

    invoke-virtual {v2}, Lcom/chase/sig/android/view/detail/q;->q()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v2

    invoke-virtual {v2}, Lcom/chase/sig/android/view/JPSpinner;->getSelectedItemPosition()I

    move-result v2

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/LabeledValue;

    new-instance v2, Lcom/chase/sig/android/domain/LabeledValue;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/LabeledValue;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Lcom/chase/sig/android/domain/LabeledValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/domain/Receipt;->a(Lcom/chase/sig/android/domain/LabeledValue;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->d:Ljava/util/List;

    const-string v2, "EXPENSE_TYPE"

    invoke-virtual {p0, v2}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->b(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/q;

    move-result-object v2

    invoke-virtual {v2}, Lcom/chase/sig/android/view/detail/q;->q()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v2

    invoke-virtual {v2}, Lcom/chase/sig/android/view/JPSpinner;->getSelectedItemPosition()I

    move-result v2

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/LabeledValue;

    new-instance v2, Lcom/chase/sig/android/domain/LabeledValue;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/LabeledValue;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Lcom/chase/sig/android/domain/LabeledValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/domain/Receipt;->b(Lcom/chase/sig/android/domain/LabeledValue;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->c:Ljava/util/List;

    const-string v2, "TAX_EXEMPT"

    invoke-virtual {p0, v2}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->b(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/q;

    move-result-object v2

    invoke-virtual {v2}, Lcom/chase/sig/android/view/detail/q;->q()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v2

    invoke-virtual {v2}, Lcom/chase/sig/android/view/JPSpinner;->getSelectedItemPosition()I

    move-result v2

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/LabeledValue;

    new-instance v2, Lcom/chase/sig/android/domain/LabeledValue;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/LabeledValue;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Lcom/chase/sig/android/domain/LabeledValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/domain/Receipt;->c(Lcom/chase/sig/android/domain/LabeledValue;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->q:Lcom/chase/sig/android/domain/ReceiptPhotoList;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/Receipt;->a(Lcom/chase/sig/android/domain/ReceiptPhotoList;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->p:Lcom/chase/sig/android/domain/Receipt;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->p:Lcom/chase/sig/android/domain/Receipt;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Receipt;->j()[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->p:Lcom/chase/sig/android/domain/Receipt;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Receipt;->j()[Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    aget-object v0, v0, v2

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/Receipt;->f(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->p:Lcom/chase/sig/android/domain/Receipt;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Receipt;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/Receipt;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->p:Lcom/chase/sig/android/domain/Receipt;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Receipt;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/Receipt;->e(Ljava/lang/String;)V

    :goto_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->l:Ljava/lang/String;

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->l:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/Receipt;->d(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->k:Ljava/lang/String;

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->k:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/Receipt;->a(Ljava/lang/String;)V

    :goto_1
    return-object v1

    :cond_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->o:Ljava/lang/String;

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->l(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "DATE"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/a;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->o:Ljava/lang/String;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->o:Ljava/lang/String;

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->t(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->b(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->o:Ljava/lang/String;

    :cond_2
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->o:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/Receipt;->c(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const-string v0, "ACCOUNT_NUMBER"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->b(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/q;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/q;->q()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v0

    iget-object v2, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->a:Ljava/util/List;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/JPSpinner;->getSelectedItemPosition()I

    move-result v0

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/IAccount;

    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/Receipt;->a(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private d()Z
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 597
    .line 599
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->y()V

    .line 601
    const/4 v1, 0x0

    .line 603
    :try_start_0
    const-string v0, "ACCOUNT_NUMBER"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/q;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/q;->q()Lcom/chase/sig/android/view/JPSpinner;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    move-object v1, v0

    .line 608
    :goto_0
    const-string v0, "CATEGORY"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/q;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/q;->q()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v4

    .line 609
    const-string v0, "EXPENSE_TYPE"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/q;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/q;->q()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v5

    .line 611
    const-string v0, "TAX_EXEMPT"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/q;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/q;->q()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v6

    .line 613
    if-eqz v1, :cond_8

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->a:Ljava/util/List;

    if-eqz v0, :cond_8

    invoke-virtual {v1}, Lcom/chase/sig/android/view/JPSpinner;->b()Z

    move-result v0

    if-nez v0, :cond_8

    .line 614
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v0

    const-string v1, "ACCOUNT_NUMBER"

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/detail/DetailView;->a(Ljava/lang/String;)V

    move v0, v2

    .line 618
    :goto_1
    invoke-virtual {v4}, Lcom/chase/sig/android/view/JPSpinner;->b()Z

    move-result v1

    if-nez v1, :cond_0

    .line 619
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v0

    const-string v1, "CATEGORY"

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/detail/DetailView;->a(Ljava/lang/String;)V

    move v0, v2

    .line 623
    :cond_0
    invoke-virtual {v5}, Lcom/chase/sig/android/view/JPSpinner;->b()Z

    move-result v1

    if-nez v1, :cond_1

    .line 624
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v0

    const-string v1, "EXPENSE_TYPE"

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/detail/DetailView;->a(Ljava/lang/String;)V

    move v0, v2

    .line 628
    :cond_1
    invoke-virtual {v6}, Lcom/chase/sig/android/view/JPSpinner;->b()Z

    move-result v1

    if-nez v1, :cond_7

    .line 629
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v0

    const-string v1, "TAX_EXEMPT"

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/detail/DetailView;->a(Ljava/lang/String;)V

    move v1, v2

    .line 633
    :goto_2
    iget-boolean v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->w:Z

    if-nez v0, :cond_2

    const-string v0, "DATE"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/d;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/d;->p()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->l(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 635
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v0

    const-string v1, "DATE"

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/detail/DetailView;->a(Ljava/lang/String;)V

    move v1, v2

    .line 639
    :cond_2
    const-string v0, "AMOUNT"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/c;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/c;->n()Z

    move-result v0

    if-nez v0, :cond_3

    .line 640
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v0

    const-string v1, "AMOUNT"

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/detail/DetailView;->a(Ljava/lang/String;)V

    move v1, v2

    .line 645
    :cond_3
    const-string v0, "DESCRIPTION"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/l;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/l;->g()Ljava/lang/String;

    move-result-object v0

    const-string v4, "%"

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    :goto_3
    if-nez v3, :cond_4

    const-string v0, "DESCRIPTION"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/l;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/l;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->l(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 649
    :cond_4
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v0

    const-string v1, "DESCRIPTION"

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/detail/DetailView;->a(Ljava/lang/String;)V

    move v1, v2

    .line 652
    :cond_5
    return v1

    :cond_6
    move v3, v2

    .line 645
    goto :goto_3

    :catch_0
    move-exception v0

    goto/16 :goto_0

    :cond_7
    move v1, v0

    goto :goto_2

    :cond_8
    move v0, v3

    goto/16 :goto_1
.end method

.method static synthetic e(Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 57
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->b:Ljava/util/List;

    return-object v0
.end method

.method static synthetic f(Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 57
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->c:Ljava/util/List;

    return-object v0
.end method

.method static synthetic g(Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 57
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->d:Ljava/util/List;

    return-object v0
.end method

.method static synthetic h(Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;)V
    .locals 5
    .parameter

    .prologue
    .line 57
    const-string v0, "CATEGORY"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/q;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/q;->q()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v1

    const-string v0, "EXPENSE_TYPE"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/q;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/q;->q()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v2

    const-string v0, "TAX_EXEMPT"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/q;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/q;->q()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v0

    iget-object v3, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->p:Lcom/chase/sig/android/domain/Receipt;

    invoke-virtual {v3}, Lcom/chase/sig/android/domain/Receipt;->k()Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v3

    iget-object v4, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->b:Ljava/util/List;

    invoke-static {v3, v4}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->a(Lcom/chase/sig/android/domain/LabeledValue;Ljava/util/List;)I

    move-result v3

    invoke-virtual {v1, v3}, Lcom/chase/sig/android/view/JPSpinner;->setSelection(I)V

    iget-object v1, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->p:Lcom/chase/sig/android/domain/Receipt;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/Receipt;->l()Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v1

    iget-object v3, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->d:Ljava/util/List;

    invoke-static {v1, v3}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->a(Lcom/chase/sig/android/domain/LabeledValue;Ljava/util/List;)I

    move-result v1

    invoke-virtual {v2, v1}, Lcom/chase/sig/android/view/JPSpinner;->setSelection(I)V

    iget-object v1, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->p:Lcom/chase/sig/android/domain/Receipt;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/Receipt;->m()Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v1

    iget-object v2, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->c:Ljava/util/List;

    invoke-static {v1, v2}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->a(Lcom/chase/sig/android/domain/LabeledValue;Ljava/util/List;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/JPSpinner;->setSelection(I)V

    iget-boolean v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->v:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->u:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->s:Lcom/chase/sig/android/view/detail/DetailView;

    const v1, 0x7f0900a6

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->b(Lcom/chase/sig/android/view/detail/DetailView;I)V

    :cond_0
    return-void
.end method

.method static synthetic i(Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;)Z
    .locals 1
    .parameter

    .prologue
    .line 57
    iget-boolean v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->x:Z

    return v0
.end method


# virtual methods
.method protected final a()Lcom/chase/sig/android/view/detail/DetailView;
    .locals 1

    .prologue
    .line 537
    const v0, 0x7f0900a4

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailView;

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 13
    .parameter

    .prologue
    const/4 v12, 0x3

    const/4 v11, 0x2

    const v10, 0x7f0900a6

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 95
    const v0, 0x7f03002d

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->b(I)V

    .line 97
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/o;->b()Lcom/chase/sig/android/domain/CacheActivityData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/CacheActivityData;->b()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/o;->b()Lcom/chase/sig/android/domain/CacheActivityData;

    move-result-object v0

    const-string v3, "selectedAccountId"

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/domain/CacheActivityData;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "selectedAccountId"

    invoke-virtual {v3, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/o;->b()Lcom/chase/sig/android/domain/CacheActivityData;

    move-result-object v0

    const-string v3, "transaction_details"

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/domain/CacheActivityData;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/AccountActivity;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "transaction_details"

    invoke-virtual {v3, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 99
    :cond_0
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    const-string v0, "is_browsing_receipts"

    invoke-virtual {v4, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->x:Z

    const-string v0, "receipt_photo_list"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "receipt_photo_list"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/ReceiptPhotoList;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->q:Lcom/chase/sig/android/domain/ReceiptPhotoList;

    :cond_1
    const-string v0, "selectedAccountId"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "selectedAccountId"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->k:Ljava/lang/String;

    :cond_2
    const-string v0, "transaction_details"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v0, "transaction_details"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/AccountActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/AccountActivity;->c()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->l:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/AccountActivity;->a()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->m:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/AccountActivity;->b()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/AccountActivity;->b()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v3, v2

    :goto_0
    if-ge v3, v6, :cond_6

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/ActivityValue;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/ActivityValue;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/ActivityValue;

    iget-object v0, v0, Lcom/chase/sig/android/domain/ActivityValue;->value:Ljava/lang/String;

    const-string v7, "-"

    invoke-static {v0, v7}, Lcom/chase/sig/android/util/s;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->n:Ljava/lang/String;

    :cond_3
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/ActivityValue;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/ActivityValue;->c()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/ActivityValue;

    iget-object v0, v0, Lcom/chase/sig/android/domain/ActivityValue;->value:Ljava/lang/String;

    if-eqz v0, :cond_5

    const-string v7, "pending"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_5

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->t(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->b(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->o:Ljava/lang/String;

    :cond_4
    :goto_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_5
    iput-boolean v2, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->y:Z

    goto :goto_1

    :cond_6
    iput-boolean v1, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->v:Z

    :cond_7
    const-string v0, "receipt"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    iput-boolean v1, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->w:Z

    const-string v0, "receipt"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/Receipt;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->p:Lcom/chase/sig/android/domain/Receipt;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->p:Lcom/chase/sig/android/domain/Receipt;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Receipt;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->k:Ljava/lang/String;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->p:Lcom/chase/sig/android/domain/Receipt;

    new-instance v3, Lcom/chase/sig/android/util/Dollar;

    iget-object v4, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->p:Lcom/chase/sig/android/domain/Receipt;

    invoke-virtual {v4}, Lcom/chase/sig/android/domain/Receipt;->b()Lcom/chase/sig/android/util/Dollar;

    move-result-object v4

    invoke-virtual {v4}, Lcom/chase/sig/android/util/Dollar;->b()Ljava/math/BigDecimal;

    move-result-object v4

    invoke-virtual {v4}, Ljava/math/BigDecimal;->abs()Ljava/math/BigDecimal;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/chase/sig/android/util/Dollar;-><init>(Ljava/math/BigDecimal;)V

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/domain/Receipt;->a(Lcom/chase/sig/android/util/Dollar;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->p:Lcom/chase/sig/android/domain/Receipt;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Receipt;->f()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->p:Lcom/chase/sig/android/domain/Receipt;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Receipt;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->l:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->v:Z

    .line 101
    :cond_8
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->p:Lcom/chase/sig/android/domain/Receipt;

    if-nez v0, :cond_11

    .line 102
    const v0, 0x7f070268

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->setTitle(I)V

    .line 103
    const v0, 0x7f0900a4

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->s:Lcom/chase/sig/android/view/detail/DetailView;

    iget-object v4, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->s:Lcom/chase/sig/android/view/detail/DetailView;

    const/4 v0, 0x7

    new-array v5, v0, [Lcom/chase/sig/android/view/detail/a;

    new-instance v0, Lcom/chase/sig/android/view/detail/l;

    const-string v3, "Description"

    iget-object v6, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->m:Ljava/lang/String;

    invoke-direct {v0, v3, v6}, Lcom/chase/sig/android/view/detail/l;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "DESCRIPTION"

    iput-object v3, v0, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v0, Lcom/chase/sig/android/view/detail/l;

    const/16 v3, 0x19

    iput v3, v0, Lcom/chase/sig/android/view/detail/l;->a:I

    const-string v3, "Required"

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/view/detail/l;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/l;

    iput-boolean v1, v0, Lcom/chase/sig/android/view/detail/a;->o:Z

    aput-object v0, v5, v2

    new-instance v0, Lcom/chase/sig/android/view/detail/c;

    const-string v3, "Amount $"

    new-instance v6, Lcom/chase/sig/android/util/Dollar;

    iget-object v7, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->n:Ljava/lang/String;

    invoke-direct {v6, v7}, Lcom/chase/sig/android/util/Dollar;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v3, v6}, Lcom/chase/sig/android/view/detail/c;-><init>(Ljava/lang/String;Lcom/chase/sig/android/util/Dollar;)V

    const-string v3, "AMOUNT"

    iput-object v3, v0, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v0, Lcom/chase/sig/android/view/detail/c;

    const-string v3, "Enter amount"

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/view/detail/c;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/c;

    iput-boolean v1, v0, Lcom/chase/sig/android/view/detail/a;->o:Z

    aput-object v0, v5, v1

    new-instance v0, Lcom/chase/sig/android/view/detail/d;

    const-string v3, "Transaction date"

    iget-object v6, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->A:Ljava/lang/String;

    invoke-direct {v0, v3, v6}, Lcom/chase/sig/android/view/detail/d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "DATE"

    iput-object v3, v0, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v0, Lcom/chase/sig/android/view/detail/d;

    invoke-virtual {p0, v2}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->i(I)Landroid/view/View$OnClickListener;

    move-result-object v3

    iput-object v3, v0, Lcom/chase/sig/android/view/detail/d;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/d;->q()Lcom/chase/sig/android/view/detail/d;

    move-result-object v3

    iget-boolean v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->v:Z

    if-eqz v0, :cond_f

    iget-boolean v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->y:Z

    if-eqz v0, :cond_f

    move v0, v1

    :goto_2
    iput-boolean v0, v3, Lcom/chase/sig/android/view/detail/a;->j:Z

    aput-object v3, v5, v11

    new-instance v0, Lcom/chase/sig/android/view/detail/q;

    const-string v3, "Account"

    invoke-direct {v0, v3}, Lcom/chase/sig/android/view/detail/q;-><init>(Ljava/lang/String;)V

    const-string v3, "ACCOUNT_NUMBER"

    iput-object v3, v0, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v0, Lcom/chase/sig/android/view/detail/q;

    const-string v3, "Select account"

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/view/detail/q;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/q;

    iget-object v3, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->k:Ljava/lang/String;

    invoke-static {v3}, Lcom/chase/sig/android/util/s;->l(Ljava/lang/String;)Z

    move-result v3

    iput-boolean v3, v0, Lcom/chase/sig/android/view/detail/a;->o:Z

    check-cast v0, Lcom/chase/sig/android/view/detail/q;

    iget-object v3, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->k:Ljava/lang/String;

    invoke-static {v3}, Lcom/chase/sig/android/util/s;->l(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_10

    move v3, v1

    :goto_3
    iput-boolean v3, v0, Lcom/chase/sig/android/view/detail/a;->j:Z

    aput-object v0, v5, v12

    const/4 v3, 0x4

    new-instance v0, Lcom/chase/sig/android/view/detail/q;

    const-string v6, "Category"

    invoke-direct {v0, v6}, Lcom/chase/sig/android/view/detail/q;-><init>(Ljava/lang/String;)V

    const-string v6, "CATEGORY"

    iput-object v6, v0, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v0, Lcom/chase/sig/android/view/detail/q;

    const-string v6, "Select category"

    invoke-virtual {v0, v6}, Lcom/chase/sig/android/view/detail/q;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/q;

    iput-boolean v1, v0, Lcom/chase/sig/android/view/detail/a;->o:Z

    aput-object v0, v5, v3

    const/4 v3, 0x5

    new-instance v0, Lcom/chase/sig/android/view/detail/q;

    const-string v6, "Expense type"

    invoke-direct {v0, v6}, Lcom/chase/sig/android/view/detail/q;-><init>(Ljava/lang/String;)V

    const-string v6, "EXPENSE_TYPE"

    iput-object v6, v0, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v0, Lcom/chase/sig/android/view/detail/q;

    const-string v6, "Select expense type"

    invoke-virtual {v0, v6}, Lcom/chase/sig/android/view/detail/q;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/q;

    iput-boolean v1, v0, Lcom/chase/sig/android/view/detail/a;->o:Z

    aput-object v0, v5, v3

    const/4 v3, 0x6

    new-instance v0, Lcom/chase/sig/android/view/detail/q;

    const-string v6, "Tax deduction"

    invoke-direct {v0, v6}, Lcom/chase/sig/android/view/detail/q;-><init>(Ljava/lang/String;)V

    const-string v6, "TAX_EXEMPT"

    iput-object v6, v0, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v0, Lcom/chase/sig/android/view/detail/q;

    const-string v6, "Select tax deduction"

    invoke-virtual {v0, v6}, Lcom/chase/sig/android/view/detail/q;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/q;

    iput-boolean v1, v0, Lcom/chase/sig/android/view/detail/a;->o:Z

    aput-object v0, v5, v3

    invoke-virtual {v4, v5}, Lcom/chase/sig/android/view/detail/DetailView;->setRows([Lcom/chase/sig/android/view/detail/a;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->s:Lcom/chase/sig/android/view/detail/DetailView;

    invoke-virtual {p0, v0, v10}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->a(Lcom/chase/sig/android/view/detail/DetailView;I)V

    .line 110
    :goto_4
    const v0, 0x7f07026f

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v0, 0x7f0900a3

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->r:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->r:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 111
    const v0, 0x7f0900a5

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->t:Landroid/widget/Button;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->t:Landroid/widget/Button;

    const v1, 0x7f07006b

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->t:Landroid/widget/Button;

    new-instance v1, Lcom/chase/sig/android/activity/kq;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/kq;-><init>(Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 112
    invoke-virtual {p0, v10}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->u:Landroid/widget/Button;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->p:Lcom/chase/sig/android/domain/Receipt;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->p:Lcom/chase/sig/android/domain/Receipt;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Receipt;->j()[Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_17

    :cond_9
    iget-boolean v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->v:Z

    if-eqz v0, :cond_16

    const v0, 0x7f070269

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_5
    iget-object v1, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->u:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->u:Landroid/widget/Button;

    new-instance v1, Lcom/chase/sig/android/activity/kr;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/kr;-><init>(Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 114
    if-eqz p1, :cond_19

    .line 115
    const-string v0, "DESCRIPTION"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    const-string v0, "DESCRIPTION"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/l;

    const-string v1, "DESCRIPTION"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/detail/l;->b(Ljava/lang/String;)V

    :cond_a
    const-string v0, "AMOUNT"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    const-string v0, "AMOUNT"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/c;

    const-string v1, "AMOUNT"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/detail/c;->b(Ljava/lang/String;)V

    :cond_b
    const-string v0, "DATE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->p:Lcom/chase/sig/android/domain/Receipt;

    if-eqz v0, :cond_18

    const-string v0, "DATE"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v1, "DATE"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/chase/sig/android/view/detail/a;->c:Ljava/lang/String;

    :cond_c
    :goto_6
    const-string v0, "CATEGORY"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->b(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/q;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/q;->q()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v1

    const-string v0, "EXPENSE_TYPE"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->b(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/q;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/q;->q()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v2

    const-string v0, "TAX_EXEMPT"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->b(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/q;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/q;->q()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v3

    const-string v0, "account_list_key"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    const-string v0, "account_list_key"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->a:Ljava/util/List;

    const-string v0, "ACCOUNT_NUMBER"

    iget-object v4, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->a:Ljava/util/List;

    invoke-direct {p0, v0, v4}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->b(Ljava/lang/String;Ljava/util/List;)V

    :cond_d
    const-string v0, "category_list_key"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->b:Ljava/util/List;

    const-string v0, "CATEGORY"

    iget-object v4, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->b:Ljava/util/List;

    invoke-direct {p0, v0, v4}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->a(Ljava/lang/String;Ljava/util/List;)V

    const-string v0, "CATEGORY"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/view/JPSpinner;->setSelection(I)V

    const-string v0, "expense_list_key"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->d:Ljava/util/List;

    const-string v0, "EXPENSE_TYPE"

    iget-object v1, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->d:Ljava/util/List;

    invoke-direct {p0, v0, v1}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->a(Ljava/lang/String;Ljava/util/List;)V

    const-string v0, "EXPENSE_TYPE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v2, v0}, Lcom/chase/sig/android/view/JPSpinner;->setSelection(I)V

    const-string v0, "tax_exempt_list_key"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->c:Ljava/util/List;

    const-string v0, "TAX_EXEMPT"

    iget-object v1, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->c:Ljava/util/List;

    invoke-direct {p0, v0, v1}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->a(Ljava/lang/String;Ljava/util/List;)V

    const-string v0, "TAX_EXEMPT"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v3, v0}, Lcom/chase/sig/android/view/JPSpinner;->setSelection(I)V

    .line 123
    :cond_e
    :goto_7
    return-void

    :cond_f
    move v0, v2

    .line 103
    goto/16 :goto_2

    :cond_10
    move v3, v2

    goto/16 :goto_3

    .line 105
    :cond_11
    const v0, 0x7f07026a

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->setTitle(I)V

    .line 106
    iget-object v4, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->p:Lcom/chase/sig/android/domain/Receipt;

    invoke-virtual {v4}, Lcom/chase/sig/android/domain/Receipt;->e()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_12

    const-string v0, "--"

    move-object v3, v0

    :goto_8
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->b:Lcom/chase/sig/android/domain/g;

    invoke-virtual {v4}, Lcom/chase/sig/android/domain/Receipt;->a()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0, v5}, Lcom/chase/sig/android/domain/g;->a(Ljava/lang/String;)Lcom/chase/sig/android/domain/IAccount;

    move-result-object v5

    const v0, 0x7f0900a4

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->s:Lcom/chase/sig/android/view/detail/DetailView;

    iget-object v6, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->s:Lcom/chase/sig/android/view/detail/DetailView;

    const/4 v0, 0x7

    new-array v7, v0, [Lcom/chase/sig/android/view/detail/a;

    new-instance v0, Lcom/chase/sig/android/view/detail/l;

    const-string v8, "Description"

    invoke-virtual {v4}, Lcom/chase/sig/android/domain/Receipt;->c()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v0, v8, v9}, Lcom/chase/sig/android/view/detail/l;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v8, "DESCRIPTION"

    iput-object v8, v0, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v0, Lcom/chase/sig/android/view/detail/l;

    const/16 v8, 0x19

    iput v8, v0, Lcom/chase/sig/android/view/detail/l;->a:I

    const-string v8, "Required"

    invoke-virtual {v0, v8}, Lcom/chase/sig/android/view/detail/l;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/l;

    iput-boolean v1, v0, Lcom/chase/sig/android/view/detail/a;->o:Z

    aput-object v0, v7, v2

    new-instance v0, Lcom/chase/sig/android/view/detail/c;

    const-string v8, "Amount $"

    invoke-virtual {v4}, Lcom/chase/sig/android/domain/Receipt;->b()Lcom/chase/sig/android/util/Dollar;

    move-result-object v4

    invoke-direct {v0, v8, v4}, Lcom/chase/sig/android/view/detail/c;-><init>(Ljava/lang/String;Lcom/chase/sig/android/util/Dollar;)V

    const-string v4, "AMOUNT"

    iput-object v4, v0, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v0, Lcom/chase/sig/android/view/detail/c;

    const-string v4, "Enter amount"

    invoke-virtual {v0, v4}, Lcom/chase/sig/android/view/detail/c;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/c;

    iput-boolean v1, v0, Lcom/chase/sig/android/view/detail/a;->o:Z

    aput-object v0, v7, v1

    new-instance v0, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v4, "Transaction date"

    invoke-direct {v0, v4, v3}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "DATE"

    iput-object v3, v0, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailRow;

    iget-boolean v3, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->x:Z

    if-nez v3, :cond_13

    move v3, v1

    :goto_9
    iput-boolean v3, v0, Lcom/chase/sig/android/view/detail/a;->j:Z

    aput-object v0, v7, v11

    new-instance v0, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v4, "Account"

    if-eqz v5, :cond_14

    invoke-interface {v5}, Lcom/chase/sig/android/domain/IAccount;->w()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    :goto_a
    invoke-direct {v0, v4, v3}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "ACCOUNT_NUMBER"

    iput-object v3, v0, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailRow;

    iget-boolean v3, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->x:Z

    if-nez v3, :cond_15

    move v3, v1

    :goto_b
    iput-boolean v3, v0, Lcom/chase/sig/android/view/detail/a;->j:Z

    aput-object v0, v7, v12

    const/4 v3, 0x4

    new-instance v0, Lcom/chase/sig/android/view/detail/q;

    const-string v4, "Category"

    invoke-direct {v0, v4}, Lcom/chase/sig/android/view/detail/q;-><init>(Ljava/lang/String;)V

    const-string v4, "CATEGORY"

    iput-object v4, v0, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v0, Lcom/chase/sig/android/view/detail/q;

    const-string v4, "Select category"

    invoke-virtual {v0, v4}, Lcom/chase/sig/android/view/detail/q;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/q;

    iput-boolean v1, v0, Lcom/chase/sig/android/view/detail/a;->o:Z

    aput-object v0, v7, v3

    const/4 v3, 0x5

    new-instance v0, Lcom/chase/sig/android/view/detail/q;

    const-string v4, "Expense type"

    invoke-direct {v0, v4}, Lcom/chase/sig/android/view/detail/q;-><init>(Ljava/lang/String;)V

    const-string v4, "EXPENSE_TYPE"

    iput-object v4, v0, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v0, Lcom/chase/sig/android/view/detail/q;

    const-string v4, "Select expense type"

    invoke-virtual {v0, v4}, Lcom/chase/sig/android/view/detail/q;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/q;

    iput-boolean v1, v0, Lcom/chase/sig/android/view/detail/a;->o:Z

    aput-object v0, v7, v3

    const/4 v3, 0x6

    new-instance v0, Lcom/chase/sig/android/view/detail/q;

    const-string v4, "Tax deduction"

    invoke-direct {v0, v4}, Lcom/chase/sig/android/view/detail/q;-><init>(Ljava/lang/String;)V

    const-string v4, "TAX_EXEMPT"

    iput-object v4, v0, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v0, Lcom/chase/sig/android/view/detail/q;

    const-string v4, "Select tax deduction"

    invoke-virtual {v0, v4}, Lcom/chase/sig/android/view/detail/q;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/q;

    iput-boolean v1, v0, Lcom/chase/sig/android/view/detail/a;->o:Z

    aput-object v0, v7, v3

    invoke-virtual {v6, v7}, Lcom/chase/sig/android/view/detail/DetailView;->setRows([Lcom/chase/sig/android/view/detail/a;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->s:Lcom/chase/sig/android/view/detail/DetailView;

    invoke-virtual {p0, v0, v10}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->a(Lcom/chase/sig/android/view/detail/DetailView;I)V

    .line 107
    const v0, 0x7f0900a2

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f0900a2

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/chase/sig/android/activity/ko;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/ko;-><init>(Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_4

    .line 106
    :cond_12
    invoke-virtual {v4}, Lcom/chase/sig/android/domain/Receipt;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->h(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    goto/16 :goto_8

    :cond_13
    move v3, v2

    goto/16 :goto_9

    :cond_14
    const-string v3, "NA"

    goto/16 :goto_a

    :cond_15
    move v3, v2

    goto/16 :goto_b

    .line 112
    :cond_16
    const v0, 0x7f070071

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_5

    :cond_17
    const v0, 0x7f07026e

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_5

    .line 115
    :cond_18
    const-string v0, "DATE"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/d;

    const-string v1, "DATE"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/detail/d;->b(Ljava/lang/String;)V

    goto/16 :goto_6

    .line 117
    :cond_19
    const-class v0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity$b;

    new-array v1, v2, [Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 118
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->k:Ljava/lang/String;

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->l(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 119
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->b:Lcom/chase/sig/android/domain/g;

    invoke-interface {v0}, Lcom/chase/sig/android/domain/g;->w()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->a:Ljava/util/List;

    .line 120
    const-string v0, "ACCOUNT_NUMBER"

    iget-object v1, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->a:Ljava/util/List;

    invoke-direct {p0, v0, v1}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->b(Ljava/lang/String;Ljava/util/List;)V

    goto/16 :goto_7
.end method

.method public final k()V
    .locals 0

    .prologue
    .line 775
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 805
    const/4 v0, -0x4

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/eb;->showDialog(I)V

    .line 806
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 7
    .parameter

    .prologue
    const/4 v1, 0x5

    const/4 v3, 0x0

    .line 386
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/a;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    .line 388
    packed-switch p1, :pswitch_data_0

    .line 406
    :goto_0
    return-object v0

    .line 391
    :pswitch_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v6

    .line 392
    const/4 v0, 0x1

    invoke-virtual {v6, v1, v0}, Ljava/util/Calendar;->add(II)V

    .line 394
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v5

    .line 395
    const/16 v0, -0x2da

    invoke-virtual {v5, v1, v0}, Ljava/util/Calendar;->add(II)V

    .line 397
    new-instance v0, Lcom/chase/sig/android/view/b;

    iget-object v2, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->B:Lcom/chase/sig/android/view/b$a;

    move-object v1, p0

    move v4, v3

    invoke-direct/range {v0 .. v6}, Lcom/chase/sig/android/view/b;-><init>(Landroid/content/Context;Lcom/chase/sig/android/view/b$a;ZZLjava/util/Calendar;Ljava/util/Calendar;)V

    iget-object v1, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->A:Ljava/lang/String;

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->t(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/b;->a(Ljava/util/Date;)Lcom/chase/sig/android/view/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/view/b;->a()Lcom/chase/sig/android/view/b;

    move-result-object v0

    goto :goto_0

    .line 388
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 318
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/a;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 320
    const-string v0, "right_button_enable_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 321
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->u:Landroid/widget/Button;

    const-string v1, "right_button_enable_state"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 322
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->s:Lcom/chase/sig/android/view/detail/DetailView;

    const v1, 0x7f0900a6

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->b(Lcom/chase/sig/android/view/detail/DetailView;I)V

    .line 324
    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4
    .parameter

    .prologue
    .line 214
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/a;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 216
    const-string v0, "DESCRIPTION"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/l;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/l;->g()Ljava/lang/String;

    move-result-object v0

    .line 218
    invoke-static {v0}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 219
    const-string v1, "DESCRIPTION"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    :cond_0
    const-string v0, "AMOUNT"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/c;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/c;->g()Ljava/lang/String;

    move-result-object v0

    .line 223
    invoke-static {v0}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 224
    const-string v1, "AMOUNT"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    :cond_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->p:Lcom/chase/sig/android/domain/Receipt;

    if-eqz v0, :cond_6

    .line 228
    const-string v0, "DATE"

    iget-object v1, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->p:Lcom/chase/sig/android/domain/Receipt;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/Receipt;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->p:Lcom/chase/sig/android/domain/Receipt;

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->v:Z

    if-eqz v0, :cond_3

    .line 238
    const-string v0, "right_button_enable_state"

    iget-object v1, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->u:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->isEnabled()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 241
    :cond_3
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->p:Lcom/chase/sig/android/domain/Receipt;

    if-nez v0, :cond_4

    .line 242
    const-string v0, "ACCOUNT_NUMBER"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/q;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/q;->q()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v0

    .line 245
    const-string v1, "ACCOUNT_NUMBER"

    invoke-virtual {v0}, Lcom/chase/sig/android/view/JPSpinner;->getPosition()I

    move-result v0

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 248
    :cond_4
    const-string v0, "CATEGORY"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/q;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/q;->q()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v1

    .line 249
    const-string v0, "EXPENSE_TYPE"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/q;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/q;->q()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v2

    .line 251
    const-string v0, "TAX_EXEMPT"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/q;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/q;->q()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v0

    .line 253
    const-string v3, "CATEGORY"

    invoke-virtual {v1}, Lcom/chase/sig/android/view/JPSpinner;->getPosition()I

    move-result v1

    invoke-virtual {p1, v3, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 254
    const-string v1, "EXPENSE_TYPE"

    invoke-virtual {v2}, Lcom/chase/sig/android/view/JPSpinner;->getPosition()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 255
    const-string v1, "TAX_EXEMPT"

    invoke-virtual {v0}, Lcom/chase/sig/android/view/JPSpinner;->getPosition()I

    move-result v0

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 257
    const-string v1, "category_list_key"

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->b:Ljava/util/List;

    check-cast v0, Ljava/io/Serializable;

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 258
    const-string v1, "tax_exempt_list_key"

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->c:Ljava/util/List;

    check-cast v0, Ljava/io/Serializable;

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 259
    const-string v1, "expense_list_key"

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->d:Ljava/util/List;

    check-cast v0, Ljava/io/Serializable;

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 261
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->a:Ljava/util/List;

    if-eqz v0, :cond_5

    .line 263
    const-string v1, "account_list_key"

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->a:Ljava/util/List;

    check-cast v0, Ljava/io/Serializable;

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 265
    :cond_5
    return-void

    .line 231
    :cond_6
    const-string v0, "DATE"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/d;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/d;->g()Ljava/lang/String;

    move-result-object v0

    .line 232
    invoke-static {v0}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 233
    const-string v1, "DATE"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
