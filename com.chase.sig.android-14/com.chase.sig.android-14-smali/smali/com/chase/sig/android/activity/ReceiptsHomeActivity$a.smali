.class public Lcom/chase/sig/android/activity/ReceiptsHomeActivity$a;
.super Lcom/chase/sig/android/activity/ae;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/ReceiptsHomeActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/activity/ae",
        "<",
        "Lcom/chase/sig/android/activity/ReceiptsHomeActivity;",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/service/ReceiptAccountSummaryResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 261
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ae;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 261
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/ReceiptsHomeActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/ReceiptsHomeActivity;->A()Lcom/chase/sig/android/service/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/service/n;->r()Lcom/chase/sig/android/service/ac;

    invoke-static {}, Lcom/chase/sig/android/service/ac;->d()Lcom/chase/sig/android/service/ReceiptAccountSummaryResponse;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 2
    .parameter

    .prologue
    .line 261
    check-cast p1, Lcom/chase/sig/android/service/ReceiptAccountSummaryResponse;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/ReceiptsHomeActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/ReceiptsHomeActivity;->b(Lcom/chase/sig/android/activity/ReceiptsHomeActivity;)V

    invoke-virtual {p1}, Lcom/chase/sig/android/service/ReceiptAccountSummaryResponse;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/ReceiptsHomeActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/ReceiptAccountSummaryResponse;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/ReceiptsHomeActivity;->b(Ljava/util/List;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/ReceiptsHomeActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/ReceiptAccountSummaryResponse;->a()Lcom/chase/sig/android/domain/ReceiptAccountSummary;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/chase/sig/android/activity/ReceiptsHomeActivity;->a(Lcom/chase/sig/android/activity/ReceiptsHomeActivity;Lcom/chase/sig/android/domain/ReceiptAccountSummary;)Lcom/chase/sig/android/domain/ReceiptAccountSummary;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/ReceiptsHomeActivity;

    iget-object v1, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v1, Lcom/chase/sig/android/activity/ReceiptsHomeActivity;

    invoke-static {v1}, Lcom/chase/sig/android/activity/ReceiptsHomeActivity;->a(Lcom/chase/sig/android/activity/ReceiptsHomeActivity;)Lcom/chase/sig/android/domain/ReceiptAccountSummary;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/chase/sig/android/activity/ReceiptsHomeActivity;->b(Lcom/chase/sig/android/activity/ReceiptsHomeActivity;Lcom/chase/sig/android/domain/ReceiptAccountSummary;)V

    goto :goto_0
.end method
