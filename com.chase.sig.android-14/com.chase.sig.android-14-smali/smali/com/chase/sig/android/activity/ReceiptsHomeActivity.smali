.class public Lcom/chase/sig/android/activity/ReceiptsHomeActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/ReceiptsHomeActivity$a;
    }
.end annotation


# instance fields
.field a:Landroid/view/View$OnClickListener;

.field private b:Lcom/chase/sig/android/view/detail/DetailView;

.field private c:Lcom/chase/sig/android/view/detail/DetailView;

.field private d:Lcom/chase/sig/android/domain/ReceiptAccountSummary;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    .line 187
    new-instance v0, Lcom/chase/sig/android/activity/ku;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/activity/ku;-><init>(Lcom/chase/sig/android/activity/ReceiptsHomeActivity;)V

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsHomeActivity;->a:Landroid/view/View$OnClickListener;

    .line 261
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/ReceiptsHomeActivity;)Lcom/chase/sig/android/domain/ReceiptAccountSummary;
    .locals 1
    .parameter

    .prologue
    .line 35
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsHomeActivity;->d:Lcom/chase/sig/android/domain/ReceiptAccountSummary;

    return-object v0
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/ReceiptsHomeActivity;Lcom/chase/sig/android/domain/ReceiptAccountSummary;)Lcom/chase/sig/android/domain/ReceiptAccountSummary;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 35
    iput-object p1, p0, Lcom/chase/sig/android/activity/ReceiptsHomeActivity;->d:Lcom/chase/sig/android/domain/ReceiptAccountSummary;

    return-object p1
.end method

.method private a(Lcom/chase/sig/android/domain/ReceiptAccountSummary;)V
    .locals 6
    .parameter

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 166
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 167
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/ReceiptAccountSummary;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 168
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " total receipts / "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/ReceiptAccountSummary;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 169
    const-string v1, " limit"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 171
    new-instance v1, Lcom/chase/sig/android/view/detail/DetailRowWithSubItem;

    const v2, 0x7f070270

    invoke-virtual {p0, v2}, Lcom/chase/sig/android/activity/ReceiptsHomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    invoke-direct {v1, v2, v0, v3}, Lcom/chase/sig/android/view/detail/DetailRowWithSubItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/ReceiptAccountSummary;->b()I

    move-result v0

    if-lez v0, :cond_0

    .line 176
    iput-boolean v5, v1, Lcom/chase/sig/android/view/detail/a;->m:Z

    .line 177
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsHomeActivity;->a:Landroid/view/View$OnClickListener;

    iput-object v0, v1, Lcom/chase/sig/android/view/detail/a;->h:Landroid/view/View$OnClickListener;

    .line 184
    :goto_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsHomeActivity;->c:Lcom/chase/sig/android/view/detail/DetailView;

    new-array v2, v5, [Lcom/chase/sig/android/view/detail/a;

    aput-object v1, v2, v4

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/view/detail/DetailView;->setRows([Lcom/chase/sig/android/view/detail/a;)V

    .line 185
    return-void

    .line 180
    :cond_0
    iput-boolean v4, v1, Lcom/chase/sig/android/view/detail/a;->m:Z

    iput-boolean v4, v1, Lcom/chase/sig/android/view/detail/a;->q:Z

    .line 181
    const v0, 0x7f060010

    iput v0, v1, Lcom/chase/sig/android/view/detail/a;->e:I

    goto :goto_0
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/ReceiptsHomeActivity;)V
    .locals 2
    .parameter

    .prologue
    .line 35
    const v0, 0x7f0900db

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsHomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/ReceiptsHomeActivity;Lcom/chase/sig/android/domain/ReceiptAccountSummary;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/chase/sig/android/activity/ReceiptsHomeActivity;->a(Lcom/chase/sig/android/domain/ReceiptAccountSummary;)V

    return-void
.end method

.method private d()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 104
    const v0, 0x7f040004

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    const v1, 0x7f0900db

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/ReceiptsHomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 106
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsHomeActivity;->e:Lcom/chase/sig/android/activity/mc;

    const-class v1, Lcom/chase/sig/android/activity/ReceiptsHomeActivity$a;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/mc;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/ae;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/activity/ReceiptsHomeActivity$a;

    .line 107
    invoke-virtual {v0}, Lcom/chase/sig/android/activity/ReceiptsHomeActivity$a;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v1

    sget-object v2, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-eq v1, v2, :cond_0

    .line 108
    new-array v1, v3, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/ReceiptsHomeActivity$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 110
    :cond_0
    return-void
.end method

.method private e()Lcom/chase/sig/android/view/detail/a;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/chase/sig/android/view/detail/a",
            "<",
            "Lcom/chase/sig/android/view/detail/DetailRow;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 147
    new-instance v0, Lcom/chase/sig/android/view/detail/DetailRow;

    const v1, 0x7f070263

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/ReceiptsHomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-direct {v0, v1, v2}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/DetailRow;->d()Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    .line 150
    new-instance v1, Lcom/chase/sig/android/activity/kt;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/kt;-><init>(Lcom/chase/sig/android/activity/ReceiptsHomeActivity;)V

    iput-object v1, v0, Lcom/chase/sig/android/view/detail/a;->h:Landroid/view/View$OnClickListener;

    .line 161
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 6
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 46
    const v0, 0x7f0300a1

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsHomeActivity;->b(I)V

    .line 47
    const v0, 0x7f070254

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsHomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsHomeActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 49
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->d:Lcom/chase/sig/android/service/SplashResponse;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->d:Lcom/chase/sig/android/service/SplashResponse;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/SplashResponse;->l()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->d:Lcom/chase/sig/android/service/SplashResponse;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/SplashResponse;->s()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsHomeActivity;->e(Ljava/lang/String;)V

    move v0, v1

    :goto_0
    if-nez v0, :cond_0

    .line 51
    const v0, 0x7f090291

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsHomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsHomeActivity;->b:Lcom/chase/sig/android/view/detail/DetailView;

    .line 52
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/ReceiptsHomeActivity;->x()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsHomeActivity;->b:Lcom/chase/sig/android/view/detail/DetailView;

    const/4 v3, 0x2

    new-array v3, v3, [Lcom/chase/sig/android/view/detail/a;

    new-instance v4, Lcom/chase/sig/android/view/detail/p;

    const v5, 0x7f070262

    invoke-virtual {p0, v5}, Lcom/chase/sig/android/activity/ReceiptsHomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/chase/sig/android/view/detail/p;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/chase/sig/android/view/detail/p;->d()Lcom/chase/sig/android/view/detail/a;

    move-result-object v4

    new-instance v5, Lcom/chase/sig/android/activity/ks;

    invoke-direct {v5, p0}, Lcom/chase/sig/android/activity/ks;-><init>(Lcom/chase/sig/android/activity/ReceiptsHomeActivity;)V

    iput-object v5, v4, Lcom/chase/sig/android/view/detail/a;->h:Landroid/view/View$OnClickListener;

    aput-object v4, v3, v2

    invoke-direct {p0}, Lcom/chase/sig/android/activity/ReceiptsHomeActivity;->e()Lcom/chase/sig/android/view/detail/a;

    move-result-object v2

    aput-object v2, v3, v1

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/view/detail/DetailView;->setRows([Lcom/chase/sig/android/view/detail/a;)V

    .line 54
    :goto_1
    const v0, 0x7f090292

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsHomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsHomeActivity;->c:Lcom/chase/sig/android/view/detail/DetailView;

    .line 55
    if-eqz p1, :cond_4

    const-string v0, "account_summary_bundle_key"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "account_summary_bundle_key"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/ReceiptAccountSummary;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsHomeActivity;->d:Lcom/chase/sig/android/domain/ReceiptAccountSummary;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsHomeActivity;->d:Lcom/chase/sig/android/domain/ReceiptAccountSummary;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsHomeActivity;->d:Lcom/chase/sig/android/domain/ReceiptAccountSummary;

    invoke-direct {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsHomeActivity;->a(Lcom/chase/sig/android/domain/ReceiptAccountSummary;)V

    .line 57
    :cond_0
    :goto_2
    return-void

    :cond_1
    move v0, v2

    .line 49
    goto :goto_0

    .line 52
    :cond_2
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsHomeActivity;->b:Lcom/chase/sig/android/view/detail/DetailView;

    new-array v1, v1, [Lcom/chase/sig/android/view/detail/a;

    invoke-direct {p0}, Lcom/chase/sig/android/activity/ReceiptsHomeActivity;->e()Lcom/chase/sig/android/view/detail/a;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/detail/DetailView;->setRows([Lcom/chase/sig/android/view/detail/a;)V

    goto :goto_1

    .line 55
    :cond_3
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ReceiptsHomeActivity;->d()V

    goto :goto_2

    :cond_4
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ReceiptsHomeActivity;->d()V

    goto :goto_2
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 202
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 206
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 207
    if-eqz p3, :cond_0

    .line 209
    new-instance v1, Landroid/content/Intent;

    const-class v0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 212
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/ReceiptsHomeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/domain/ReceiptPhotoList;->a(Landroid/content/Intent;)Lcom/chase/sig/android/domain/ReceiptPhotoList;

    move-result-object v2

    .line 215
    const-string v0, "image_uris"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 220
    if-eqz v0, :cond_1

    .line 221
    :try_start_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 222
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/ReceiptsHomeActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-static {v4, v0}, Landroid/provider/MediaStore$Images$Media;->getBitmap(Landroid/content/ContentResolver;Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 223
    new-instance v4, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v4}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 224
    sget-object v5, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v6, 0x1e

    invoke-virtual {v0, v5, v6, v4}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 226
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 227
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 228
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 229
    invoke-virtual {v2, v0}, Lcom/chase/sig/android/domain/ReceiptPhotoList;->a([B)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 235
    :catch_0
    move-exception v0

    .line 237
    :cond_0
    :goto_1
    invoke-super {p0, p1, p2, p3}, Lcom/chase/sig/android/activity/ai;->onActivityResult(IILandroid/content/Intent;)V

    .line 244
    return-void

    .line 232
    :cond_1
    :try_start_1
    const-string v0, "receipt_photo_list"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 233
    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/ReceiptsHomeActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 61
    invoke-super {p0}, Lcom/chase/sig/android/activity/ai;->onResume()V

    .line 62
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 65
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ai;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 67
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsHomeActivity;->d:Lcom/chase/sig/android/domain/ReceiptAccountSummary;

    if-eqz v0, :cond_0

    .line 68
    const-string v0, "account_summary_bundle_key"

    iget-object v1, p0, Lcom/chase/sig/android/activity/ReceiptsHomeActivity;->d:Lcom/chase/sig/android/domain/ReceiptAccountSummary;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 71
    :cond_0
    return-void
.end method
