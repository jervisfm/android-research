.class public Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity$a;
.super Lcom/chase/sig/android/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/d",
        "<",
        "Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/service/l;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 171
    invoke-direct {p0}, Lcom/chase/sig/android/d;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .parameter

    .prologue
    .line 171
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;->d()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;->A()Lcom/chase/sig/android/service/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/service/n;->q()Lcom/chase/sig/android/service/ad;

    const-string v0, "accept"

    invoke-static {v1, v0}, Lcom/chase/sig/android/service/ad;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/service/ReceiptsEnrollmentResponse;

    move-result-object v1

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;->l()V

    return-object v1
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 2
    .parameter

    .prologue
    .line 171
    check-cast p1, Lcom/chase/sig/android/service/l;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/l;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/l;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;->b(Ljava/util/List;)V

    :goto_0
    return-void

    :cond_0
    check-cast p1, Lcom/chase/sig/android/service/ReceiptsEnrollmentResponse;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/ReceiptsEnrollmentResponse;->b()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;->showDialog(I)V

    goto :goto_0
.end method
