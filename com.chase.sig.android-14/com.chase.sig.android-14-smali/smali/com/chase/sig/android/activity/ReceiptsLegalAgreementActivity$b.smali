.class public Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity$b;
.super Lcom/chase/sig/android/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/d",
        "<",
        "Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/service/l;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 141
    invoke-direct {p0}, Lcom/chase/sig/android/d;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .parameter

    .prologue
    .line 141
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;->A()Lcom/chase/sig/android/service/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/service/n;->q()Lcom/chase/sig/android/service/ad;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;->a(Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;->b(Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/chase/sig/android/service/ad;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/service/ReceiptsEnrollmentResponse;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 4
    .parameter

    .prologue
    .line 141
    check-cast p1, Lcom/chase/sig/android/service/l;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/l;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/l;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;->b(Ljava/util/List;)V

    :goto_0
    return-void

    :cond_0
    move-object v0, p1

    check-cast v0, Lcom/chase/sig/android/service/ReceiptsEnrollmentResponse;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/ReceiptsEnrollmentResponse;->d()Lcom/chase/sig/android/domain/Agreement;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/Agreement;->a()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    :goto_1
    check-cast p1, Lcom/chase/sig/android/service/ReceiptsEnrollmentResponse;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/ReceiptsEnrollmentResponse;->c()Lcom/chase/sig/android/domain/EnrollmentForm;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/EnrollmentForm;->a()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/Agreement;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;->h(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/Agreement;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;->c(Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;)Landroid/webkit/WebView;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;->a(Ljava/lang/String;Landroid/webkit/WebView;)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_1
.end method
