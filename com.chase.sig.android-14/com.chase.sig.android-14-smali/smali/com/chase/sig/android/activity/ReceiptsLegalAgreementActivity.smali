.class public Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity$a;,
        Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity$b;
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Landroid/webkit/WebView;

.field private d:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    .line 171
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 26
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Ljava/lang/String;Landroid/webkit/WebView;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 26
    invoke-static {p0, p1}, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;->b(Ljava/lang/String;Landroid/webkit/WebView;)V

    return-void
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 26
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;->a:Ljava/lang/String;

    return-object v0
.end method

.method private static b(Ljava/lang/String;Landroid/webkit/WebView;)V
    .locals 6
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 99
    const-string v3, "text/html"

    const-string v4, "utf-8"

    move-object v0, p1

    move-object v2, p0

    move-object v5, v1

    invoke-virtual/range {v0 .. v5}, Landroid/webkit/WebView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    return-void
.end method

.method static synthetic c(Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;)Landroid/webkit/WebView;
    .locals 1
    .parameter

    .prologue
    .line 26
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;->c:Landroid/webkit/WebView;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 4
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 44
    const v0, 0x7f0300a2

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;->b(I)V

    .line 45
    const v0, 0x7f0701d3

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;->setTitle(I)V

    .line 46
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 47
    const-string v0, "plan_code_id"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;->a:Ljava/lang/String;

    .line 48
    const-string v0, "funding_acct_id"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;->b:Ljava/lang/String;

    .line 50
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "resultMessage"

    invoke-static {p1, v0, v1}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;->l:Ljava/lang/String;

    .line 54
    const v0, 0x7f090296

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/webkit/WebSettings;->setSaveFormData(Z)V

    new-instance v1, Lcom/chase/sig/android/activity/kv;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/kv;-><init>(Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;->c:Landroid/webkit/WebView;

    .line 55
    if-eqz p1, :cond_1

    const-string v0, "formId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "formId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;->d:Ljava/lang/String;

    const-string v0, "legalAgreementUrl"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;->k:Ljava/lang/String;

    const-string v0, "content"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;->m:Ljava/lang/String;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;->m:Ljava/lang/String;

    iget-object v1, p0, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;->c:Landroid/webkit/WebView;

    invoke-static {v0, v1}, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;->b(Ljava/lang/String;Landroid/webkit/WebView;)V

    .line 56
    :cond_0
    :goto_0
    const v0, 0x7f090295

    new-instance v1, Lcom/chase/sig/android/activity/kx;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/kx;-><init>(Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;)V

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 58
    const v0, 0x7f090294

    new-instance v1, Lcom/chase/sig/android/activity/kw;

    invoke-direct {v1, p0, p0}, Lcom/chase/sig/android/activity/kw;-><init>(Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;)V

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 59
    return-void

    .line 55
    :cond_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;->e:Lcom/chase/sig/android/activity/mc;

    const-class v1, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity$b;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/mc;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/ae;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity$b;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity$b;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v1

    sget-object v2, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-eq v1, v2, :cond_0

    new-array v1, v3, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity$b;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 237
    iput-object p1, p0, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;->d:Ljava/lang/String;

    .line 238
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 245
    iput-object p1, p0, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;->k:Ljava/lang/String;

    .line 246
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 253
    iput-object p1, p0, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;->l:Ljava/lang/String;

    .line 254
    return-void
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final h(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 261
    iput-object p1, p0, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;->m:Ljava/lang/String;

    .line 262
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 4
    .parameter

    .prologue
    .line 199
    if-nez p1, :cond_0

    .line 200
    new-instance v0, Lcom/chase/sig/android/view/k$a;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/view/k$a;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/k$a;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/k$a;

    move-result-object v1

    const v2, 0x7f070068

    new-instance v3, Lcom/chase/sig/android/activity/ky;

    invoke-direct {v3, p0}, Lcom/chase/sig/android/activity/ky;-><init>(Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;)V

    invoke-virtual {v1, v2, v3}, Lcom/chase/sig/android/view/k$a;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    new-instance v1, Lcom/chase/sig/android/activity/kz;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/kz;-><init>(Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;)V

    iput-object v1, v0, Lcom/chase/sig/android/view/k$a;->g:Landroid/content/DialogInterface$OnCancelListener;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/k$a;->d(I)Lcom/chase/sig/android/view/k;

    move-result-object v0

    .line 202
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ai;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 75
    invoke-super {p0}, Lcom/chase/sig/android/activity/ai;->onResume()V

    .line 76
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 63
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ai;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 64
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;->d:Ljava/lang/String;

    .line 65
    if-eqz v0, :cond_0

    .line 66
    const-string v1, "formId"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 67
    const-string v0, "legalAgreementUrl"

    iget-object v1, p0, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 68
    const-string v0, "content"

    iget-object v1, p0, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;->m:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 69
    const-string v0, "resultMessage"

    iget-object v1, p0, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;->l:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    :cond_0
    return-void
.end method
