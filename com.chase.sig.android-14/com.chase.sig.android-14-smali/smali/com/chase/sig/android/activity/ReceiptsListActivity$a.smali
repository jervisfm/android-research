.class final Lcom/chase/sig/android/activity/ReceiptsListActivity$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/ReceiptsListActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/ReceiptsListActivity;


# direct methods
.method private constructor <init>(Lcom/chase/sig/android/activity/ReceiptsListActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 833
    iput-object p1, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity$a;->a:Lcom/chase/sig/android/activity/ReceiptsListActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/chase/sig/android/activity/ReceiptsListActivity;B)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 833
    invoke-direct {p0, p1}, Lcom/chase/sig/android/activity/ReceiptsListActivity$a;-><init>(Lcom/chase/sig/android/activity/ReceiptsListActivity;)V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 837
    :try_start_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity$a;->a:Lcom/chase/sig/android/activity/ReceiptsListActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/ReceiptsListActivity;->k(Lcom/chase/sig/android/activity/ReceiptsListActivity;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/Receipt;

    .line 838
    iget-object v1, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity$a;->a:Lcom/chase/sig/android/activity/ReceiptsListActivity;

    invoke-static {v1, v0}, Lcom/chase/sig/android/activity/ReceiptsListActivity;->a(Lcom/chase/sig/android/activity/ReceiptsListActivity;Lcom/chase/sig/android/domain/Receipt;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 839
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1, p2, p3}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/widget/AdapterView;Landroid/view/View;I)V

    return-void

    :catch_0
    move-exception v0

    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1, p2, p3}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/widget/AdapterView;Landroid/view/View;I)V

    throw v0
.end method
