.class public Lcom/chase/sig/android/activity/ReceiptsListActivity$b;
.super Lcom/chase/sig/android/b;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/ReceiptsListActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/b",
        "<",
        "Lcom/chase/sig/android/activity/ReceiptsListActivity;",
        "Lcom/chase/sig/android/service/ReceiptFilter;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/service/ReceiptListResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 760
    invoke-direct {p0}, Lcom/chase/sig/android/b;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .parameter

    .prologue
    .line 760
    check-cast p1, [Lcom/chase/sig/android/service/ReceiptFilter;

    const/4 v0, 0x0

    aget-object v1, p1, v0

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/ReceiptsListActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/ReceiptsListActivity;->A()Lcom/chase/sig/android/service/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/service/n;->r()Lcom/chase/sig/android/service/ac;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/ReceiptsListActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/ReceiptsListActivity;->j(Lcom/chase/sig/android/activity/ReceiptsListActivity;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/util/Hashtable;

    invoke-direct {v2}, Ljava/util/Hashtable;-><init>()V

    const-string v3, "accountId"

    invoke-virtual {v2, v3, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "jsonFilterPayload"

    invoke-static {v1}, Lcom/chase/sig/android/service/JPService$a;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v2}, Lcom/chase/sig/android/service/ac;->a(Ljava/util/Hashtable;)Lcom/chase/sig/android/service/ReceiptListResponse;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 2
    .parameter

    .prologue
    .line 760
    check-cast p1, Lcom/chase/sig/android/service/ReceiptListResponse;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/ReceiptListResponse;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/ReceiptsListActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/ReceiptListResponse;->i()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/ReceiptsListActivity;->b(Ljava/util/List;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/ReceiptsListActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/ReceiptListResponse;->a()Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/chase/sig/android/activity/ReceiptsListActivity;->a(Lcom/chase/sig/android/activity/ReceiptsListActivity;Ljava/util/List;)V

    goto :goto_0
.end method

.method protected onCancelled()V
    .locals 1

    .prologue
    .line 782
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/ReceiptsListActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/ReceiptsListActivity;->finish()V

    .line 783
    return-void
.end method
