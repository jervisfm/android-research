.class public Lcom/chase/sig/android/activity/ReceiptsListActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/ReceiptsListActivity$a;,
        Lcom/chase/sig/android/activity/ReceiptsListActivity$b;
    }
.end annotation


# instance fields
.field private a:Landroid/widget/ListView;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/Receipt;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/chase/sig/android/service/ListContentResponse;

.field private d:Ljava/lang/String;

.field private k:Lcom/chase/sig/android/domain/receipt/ReceiptFilterList;

.field private l:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private m:Ljava/util/Date;

.field private n:Ljava/util/Date;

.field private o:Lcom/chase/sig/android/service/ReceiptFilter;

.field private p:Ljava/lang/String;

.field private q:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/chase/sig/android/domain/LabeledValue;",
            ">;"
        }
    .end annotation
.end field

.field private r:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private s:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/chase/sig/android/domain/LabeledValue;",
            ">;"
        }
    .end annotation
.end field

.field private t:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    .line 833
    return-void
.end method

.method private a(Landroid/widget/Button;)Landroid/view/View$OnClickListener;
    .locals 1
    .parameter

    .prologue
    .line 359
    new-instance v0, Lcom/chase/sig/android/activity/lh;

    invoke-direct {v0, p0, p1}, Lcom/chase/sig/android/activity/lh;-><init>(Lcom/chase/sig/android/activity/ReceiptsListActivity;Landroid/widget/Button;)V

    return-object v0
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/ReceiptsListActivity;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 43
    iput-object p1, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->s:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/ReceiptsListActivity;)Ljava/util/Date;
    .locals 1
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->m:Ljava/util/Date;

    return-object v0
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/ReceiptsListActivity;Ljava/util/Date;)Ljava/util/Date;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 43
    iput-object p1, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->m:Ljava/util/Date;

    return-object p1
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/ReceiptsListActivity;I)V
    .locals 4
    .parameter
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->k:Lcom/chase/sig/android/domain/receipt/ReceiptFilterList;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/receipt/ReceiptFilterList;->a()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/LabeledValue;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/LabeledValue;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/LabeledValue;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->q:Ljava/util/ArrayList;

    new-instance v3, Lcom/chase/sig/android/domain/LabeledValue;

    invoke-direct {v3, v2, v0}, Lcom/chase/sig/android/domain/LabeledValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->r:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->r:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/ReceiptsListActivity;I[Ljava/lang/String;)V
    .locals 2
    .parameter
    .parameter
    .parameter

    .prologue
    .line 43
    aget-object v0, p2, p1

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->d:Ljava/lang/String;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->c:Lcom/chase/sig/android/service/ListContentResponse;

    iget-object v1, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/ListContentResponse;->a(Ljava/lang/String;)Lcom/chase/sig/android/domain/receipt/ReceiptFilterList;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->k:Lcom/chase/sig/android/domain/receipt/ReceiptFilterList;

    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/ReceiptsListActivity;Lcom/chase/sig/android/domain/Receipt;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 43
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/chase/sig/android/activity/ReceiptDetailActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "receipt"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v1, "show_all_details"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "is_browsing_receipts"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsListActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/ReceiptsListActivity;Ljava/lang/String;Ljava/util/Date;Ljava/util/Date;)V
    .locals 4
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 43
    if-nez p1, :cond_0

    if-nez p2, :cond_0

    if-nez p3, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->o:Lcom/chase/sig/android/service/ReceiptFilter;

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/service/ReceiptFilter;->c(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/chase/sig/android/activity/ReceiptsListActivity;->e()V

    :goto_0
    iput-object v3, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->m:Ljava/util/Date;

    iput-object v3, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->n:Ljava/util/Date;

    return-void

    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->o:Lcom/chase/sig/android/service/ReceiptFilter;

    invoke-virtual {v0, p1}, Lcom/chase/sig/android/service/ReceiptFilter;->c(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/chase/sig/android/activity/ReceiptsListActivity;->e()V

    goto :goto_0

    :cond_1
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    const/4 v1, 0x1

    aput-object p3, v0, v1

    invoke-static {p2}, Lcom/chase/sig/android/util/s;->b(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p3}, Lcom/chase/sig/android/util/s;->b(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->o:Lcom/chase/sig/android/service/ReceiptFilter;

    invoke-virtual {v2, v0, v1}, Lcom/chase/sig/android/service/ReceiptFilter;->a(Ljava/lang/String;Ljava/lang/String;)V

    const v0, 0x7f0900f3

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const-string v1, "Dates: From/To"

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/chase/sig/android/activity/ReceiptsListActivity;->e()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/ReceiptsListActivity;Ljava/util/List;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/chase/sig/android/activity/ReceiptsListActivity;->d(Ljava/util/List;)V

    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/ReceiptsListActivity;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 43
    iput-boolean p1, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->t:Z

    return p1
.end method

.method private static a(Ljava/util/List;)[Ljava/lang/String;
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/LabeledValue;",
            ">;)[",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 751
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    new-array v2, v0, [Ljava/lang/String;

    .line 753
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    array-length v0, v2

    if-ge v1, v0, :cond_0

    .line 754
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/LabeledValue;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/LabeledValue;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v1

    .line 753
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 757
    :cond_0
    return-object v2
.end method

.method private a([Ljava/lang/String;)[Z
    .locals 6
    .parameter

    .prologue
    .line 492
    array-length v0, p1

    new-array v2, v0, [Z

    .line 493
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    array-length v0, p1

    if-ge v1, v0, :cond_2

    .line 494
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 495
    :cond_0
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 496
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/LabeledValue;

    .line 497
    iget-object v4, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    aget-object v4, p1, v1

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/LabeledValue;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 499
    const/4 v0, 0x1

    aput-boolean v0, v2, v1

    goto :goto_1

    .line 493
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 503
    :cond_2
    return-object v2
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/ReceiptsListActivity;)Ljava/util/Date;
    .locals 1
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->n:Ljava/util/Date;

    return-object v0
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/ReceiptsListActivity;Ljava/util/Date;)Ljava/util/Date;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 43
    iput-object p1, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->n:Ljava/util/Date;

    return-object p1
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/ReceiptsListActivity;I)V
    .locals 4
    .parameter
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->k:Lcom/chase/sig/android/domain/receipt/ReceiptFilterList;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/receipt/ReceiptFilterList;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/LabeledValue;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/LabeledValue;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v2, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->q:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    :cond_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->r:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->r:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    return-void
.end method

.method static synthetic c(Lcom/chase/sig/android/activity/ReceiptsListActivity;)Lcom/chase/sig/android/service/ReceiptFilter;
    .locals 1
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->o:Lcom/chase/sig/android/service/ReceiptFilter;

    return-object v0
.end method

.method private d()Landroid/app/Dialog;
    .locals 6

    .prologue
    .line 166
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 168
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 171
    new-instance v0, Lcom/chase/sig/android/domain/LabeledValue;

    const-string v1, "No filter"

    const-string v4, ""

    invoke-direct {v0, v1, v4}, Lcom/chase/sig/android/domain/LabeledValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 172
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->c:Lcom/chase/sig/android/service/ListContentResponse;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/ListContentResponse;->a()Ljava/util/LinkedHashMap;

    move-result-object v0

    const-string v1, "dateFilter"

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/receipt/ReceiptFilterList;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/receipt/ReceiptFilterList;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 174
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    .line 175
    add-int/lit8 v0, v4, 0x1

    new-array v5, v0, [Ljava/lang/String;

    .line 176
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    .line 177
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/LabeledValue;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/LabeledValue;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v1

    .line 176
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 180
    :cond_0
    const-string v0, "From / To"

    aput-object v0, v5, v4

    .line 182
    new-instance v0, Lcom/chase/sig/android/activity/le;

    invoke-direct {v0, p0, v4, v3}, Lcom/chase/sig/android/activity/le;-><init>(Lcom/chase/sig/android/activity/ReceiptsListActivity;ILjava/util/List;)V

    .line 203
    invoke-virtual {v2, v5, v0}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "Select a date range"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 205
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Lcom/chase/sig/android/activity/ReceiptsListActivity;)Ljava/util/ArrayList;
    .locals 1
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->l:Ljava/util/ArrayList;

    return-object v0
.end method

.method private d(Ljava/util/List;)V
    .locals 10
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/Receipt;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x0

    .line 788
    iput-object p1, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->b:Ljava/util/List;

    .line 790
    const-string v1, ""

    .line 795
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 796
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->c:Lcom/chase/sig/android/service/ListContentResponse;

    const-string v3, "Category"

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/service/ListContentResponse;->a(Ljava/lang/String;)Lcom/chase/sig/android/domain/receipt/ReceiptFilterList;

    move-result-object v0

    .line 797
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/receipt/ReceiptFilterList;->a()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/o;->a(Ljava/util/List;)Ljava/util/Map;

    move-result-object v3

    .line 800
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/Receipt;

    .line 801
    new-instance v5, Ljava/util/LinkedHashMap;

    invoke-direct {v5}, Ljava/util/LinkedHashMap;-><init>()V

    .line 806
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Receipt;->k()Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 807
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Receipt;->k()Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v1

    .line 808
    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 811
    :cond_0
    const-string v6, "name"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Receipt;->c()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v5, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 812
    const-string v6, "category"

    invoke-interface {v5, v6, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 813
    const-string v6, "amount"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Receipt;->b()Lcom/chase/sig/android/util/Dollar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/util/Dollar;->h()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v5, v6, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 815
    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 818
    :cond_1
    new-array v4, v9, [Ljava/lang/String;

    const-string v0, "name"

    aput-object v0, v4, v8

    const/4 v0, 0x1

    const-string v1, "category"

    aput-object v1, v4, v0

    const/4 v0, 0x2

    const-string v1, "amount"

    aput-object v1, v4, v0

    .line 822
    new-array v5, v9, [I

    fill-array-data v5, :array_0

    .line 826
    new-instance v0, Landroid/widget/SimpleAdapter;

    const v3, 0x7f030056

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Landroid/widget/SimpleAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V

    .line 828
    iget-object v1, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->a:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 829
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->a:Landroid/widget/ListView;

    new-instance v1, Lcom/chase/sig/android/activity/ReceiptsListActivity$a;

    invoke-direct {v1, p0, v8}, Lcom/chase/sig/android/activity/ReceiptsListActivity$a;-><init>(Lcom/chase/sig/android/activity/ReceiptsListActivity;B)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 831
    return-void

    .line 822
    nop

    :array_0
    .array-data 0x4
        0x15t 0x0t 0x9t 0x7ft
        0xa1t 0x0t 0x9t 0x7ft
        0x16t 0x1t 0x9t 0x7ft
    .end array-data
.end method

.method private e()V
    .locals 4

    .prologue
    .line 268
    const-class v0, Lcom/chase/sig/android/activity/ReceiptsListActivity$b;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/chase/sig/android/service/ReceiptFilter;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->o:Lcom/chase/sig/android/service/ReceiptFilter;

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/ReceiptsListActivity;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 269
    return-void
.end method

.method static synthetic e(Lcom/chase/sig/android/activity/ReceiptsListActivity;)V
    .locals 5
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->q:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/domain/LabeledValue;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->s:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/LabeledValue;

    iget-object v2, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->s:Ljava/util/ArrayList;

    new-instance v3, Lcom/chase/sig/android/domain/LabeledValue;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/LabeledValue;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v4, v0}, Lcom/chase/sig/android/domain/LabeledValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    return-void
.end method

.method private f()V
    .locals 6

    .prologue
    .line 550
    const v0, 0x7f090281

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 551
    const v1, 0x7f0900f3

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/ReceiptsListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 553
    iget-object v2, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->o:Lcom/chase/sig/android/service/ReceiptFilter;

    invoke-virtual {v2}, Lcom/chase/sig/android/service/ReceiptFilter;->a()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 554
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v2, "Dates: "

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ""

    iget-object v4, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->o:Lcom/chase/sig/android/service/ReceiptFilter;

    invoke-virtual {v4}, Lcom/chase/sig/android/service/ReceiptFilter;->c()Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->o:Lcom/chase/sig/android/service/ReceiptFilter;

    invoke-virtual {v4}, Lcom/chase/sig/android/service/ReceiptFilter;->c()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-class v5, Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v2, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->c:Lcom/chase/sig/android/service/ListContentResponse;

    const-string v4, "Dates"

    invoke-virtual {v2, v4}, Lcom/chase/sig/android/service/ListContentResponse;->a(Ljava/lang/String;)Lcom/chase/sig/android/domain/receipt/ReceiptFilterList;

    move-result-object v2

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/receipt/ReceiptFilterList;->a()Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Lcom/chase/sig/android/util/o;->a(Ljava/util/List;)Ljava/util/Map;

    move-result-object v2

    iget-object v4, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->o:Lcom/chase/sig/android/service/ReceiptFilter;

    invoke-virtual {v4}, Lcom/chase/sig/android/service/ReceiptFilter;->c()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    :cond_0
    :goto_0
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 555
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Filter: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/chase/sig/android/activity/ReceiptsListActivity;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 560
    :goto_1
    return-void

    .line 554
    :cond_1
    iget-object v4, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->o:Lcom/chase/sig/android/service/ReceiptFilter;

    invoke-virtual {v4}, Lcom/chase/sig/android/service/ReceiptFilter;->c()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-class v5, Lcom/chase/sig/android/service/ReceiptFilter$DateFilter;

    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v2, "From/To"

    goto :goto_0

    .line 557
    :cond_2
    const-string v2, "Dates: "

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 558
    const-string v1, "Filter: No filter"

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method static synthetic f(Lcom/chase/sig/android/activity/ReceiptsListActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ReceiptsListActivity;->f()V

    return-void
.end method

.method static synthetic g(Lcom/chase/sig/android/activity/ReceiptsListActivity;)Lcom/chase/sig/android/domain/receipt/ReceiptFilterList;
    .locals 1
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->k:Lcom/chase/sig/android/domain/receipt/ReceiptFilterList;

    return-object v0
.end method

.method private g()Ljava/lang/String;
    .locals 5

    .prologue
    .line 563
    const-string v1, ""

    .line 564
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 565
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 566
    const/4 v0, 0x0

    move-object v2, v1

    move v1, v0

    .line 567
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 568
    if-nez v1, :cond_0

    .line 569
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 574
    :goto_1
    add-int/lit8 v1, v1, 0x1

    move-object v2, v0

    goto :goto_0

    .line 571
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 572
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_1
    move-object v2, v1

    .line 578
    :cond_2
    return-object v2
.end method

.method static synthetic h(Lcom/chase/sig/android/activity/ReceiptsListActivity;)V
    .locals 4
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->o:Lcom/chase/sig/android/service/ReceiptFilter;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/ReceiptFilter;->b()V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/LabeledValue;

    iget-object v2, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->o:Lcom/chase/sig/android/service/ReceiptFilter;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/chase/sig/android/service/ReceiptFilter;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/LabeledValue;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

.method static synthetic i(Lcom/chase/sig/android/activity/ReceiptsListActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ReceiptsListActivity;->e()V

    return-void
.end method

.method static synthetic j(Lcom/chase/sig/android/activity/ReceiptsListActivity;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->p:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic k(Lcom/chase/sig/android/activity/ReceiptsListActivity;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->b:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 6
    .parameter

    .prologue
    const/4 v5, 0x0

    .line 90
    const v0, 0x7f03009a

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsListActivity;->b(I)V

    .line 91
    const v0, 0x7f070266

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsListActivity;->setTitle(I)V

    .line 93
    const v0, 0x7f090280

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/ReceiptsListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v1, "header_text"

    const-string v3, "N/A"

    invoke-static {v2, v1, v3}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/ReceiptsListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v3, "header_text"

    const-string v4, "N/A"

    invoke-static {v1, v3, v4}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v0, Lcom/chase/sig/android/service/ReceiptFilter;

    invoke-direct {v0}, Lcom/chase/sig/android/service/ReceiptFilter;-><init>()V

    const-string v1, "ALL"

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/ReceiptFilter;->a(Ljava/lang/String;)V

    const-string v1, "current_filters"

    invoke-static {p1, v1, v0}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/service/ReceiptFilter;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->o:Lcom/chase/sig/android/service/ReceiptFilter;

    const-string v0, "account_id"

    invoke-static {v2, v0, v5}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->p:Ljava/lang/String;

    const-string v0, "filter_set"

    invoke-static {v2, v0, v5}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/service/ListContentResponse;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->c:Lcom/chase/sig/android/service/ListContentResponse;

    const-string v0, "receipts"

    invoke-static {p1, v0, v5}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->b:Ljava/util/List;

    const v0, 0x7f090282

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->a:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->b:Ljava/util/List;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->b:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsListActivity;->d(Ljava/util/List;)V

    :goto_0
    const-string v0, "parent_filter_label"

    invoke-static {p1, v0, v5}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->d:Ljava/lang/String;

    const-string v0, "parent_filter"

    invoke-static {p1, v0, v5}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/receipt/ReceiptFilterList;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->k:Lcom/chase/sig/android/domain/receipt/ReceiptFilterList;

    const-string v0, "selected_filter_labels"

    invoke-static {p1, v0, v5}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->l:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->l:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->l:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->l:Ljava/util/ArrayList;

    const-string v1, "No filter"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    const-string v0, "temp_filters"

    invoke-static {p1, v0, v5}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->s:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->s:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->s:Ljava/util/ArrayList;

    :cond_1
    const-string v0, "selectedFromDate"

    invoke-static {p1, v0, v5}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Date;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->m:Ljava/util/Date;

    const-string v0, "selectedToDate"

    invoke-static {p1, v0, v5}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Date;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->n:Ljava/util/Date;

    const v0, 0x7f0900f3

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/chase/sig/android/activity/la;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/la;-><init>(Lcom/chase/sig/android/activity/ReceiptsListActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f090281

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lcom/chase/sig/android/activity/ld;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/ld;-><init>(Lcom/chase/sig/android/activity/ReceiptsListActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/chase/sig/android/activity/ReceiptsListActivity;->f()V

    .line 94
    return-void

    .line 93
    :cond_2
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ReceiptsListActivity;->e()V

    goto :goto_0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 14
    .parameter

    .prologue
    .line 224
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ai;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    .line 226
    packed-switch p1, :pswitch_data_0

    .line 241
    :goto_0
    return-object v0

    .line 228
    :pswitch_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->c:Lcom/chase/sig/android/service/ListContentResponse;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/ListContentResponse;->a()Ljava/util/LinkedHashMap;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    new-array v6, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/receipt/ReceiptFilterList;

    const-string v2, "Dates"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/receipt/ReceiptFilterList;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_11

    add-int/lit8 v2, v1, 0x1

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/receipt/ReceiptFilterList;->b()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v1

    move v0, v2

    :goto_2
    move v1, v0

    goto :goto_1

    :cond_0
    new-instance v7, Lcom/chase/sig/android/activity/lb;

    invoke-direct {v7, p0, v6}, Lcom/chase/sig/android/activity/lb;-><init>(Lcom/chase/sig/android/activity/ReceiptsListActivity;[Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->t:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->o:Lcom/chase/sig/android/service/ReceiptFilter;

    move-object v3, v0

    :goto_3
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    invoke-virtual {v6}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    const/4 v1, 0x0

    move v4, v1

    :goto_4
    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    if-gt v4, v1, :cond_9

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->c:Lcom/chase/sig/android/service/ListContentResponse;

    invoke-virtual {v2}, Lcom/chase/sig/android/service/ListContentResponse;->a()Ljava/util/LinkedHashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v2, v1

    :goto_5
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/domain/receipt/ReceiptFilterList;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/receipt/ReceiptFilterList;->b()Ljava/lang/String;

    move-result-object v5

    aget-object v9, v6, v4

    invoke-virtual {v5, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/receipt/ReceiptFilterList;->a()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_6
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/domain/LabeledValue;

    invoke-virtual {v3}, Lcom/chase/sig/android/service/ReceiptFilter;->a()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    move v5, v2

    :cond_1
    :goto_7
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->s:Ljava/util/ArrayList;

    new-instance v11, Lcom/chase/sig/android/domain/LabeledValue;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/LabeledValue;->a()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v11, v12, v13}, Lcom/chase/sig/android/domain/LabeledValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-nez v5, :cond_5

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v11, v0, v4

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v11, ": "

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/LabeledValue;->a()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v4

    :goto_8
    add-int/lit8 v5, v5, 0x1

    goto :goto_7

    :cond_2
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->r:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    new-instance v0, Lcom/chase/sig/android/service/ReceiptFilter;

    invoke-direct {v0}, Lcom/chase/sig/android/service/ReceiptFilter;-><init>()V

    :goto_9
    move-object v3, v0

    goto/16 :goto_3

    :cond_3
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->o:Lcom/chase/sig/android/service/ReceiptFilter;

    move-object v3, v0

    goto/16 :goto_3

    :cond_4
    new-instance v1, Lcom/chase/sig/android/service/ReceiptFilter;

    invoke-direct {v1}, Lcom/chase/sig/android/service/ReceiptFilter;-><init>()V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/LabeledValue;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/service/ReceiptFilter;->a(Ljava/lang/String;)V

    goto :goto_a

    :cond_5
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v11, v0, v4

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v11, ", "

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/LabeledValue;->a()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v4

    goto :goto_8

    :cond_6
    move v2, v5

    goto/16 :goto_6

    :cond_7
    move v1, v2

    move v2, v1

    goto/16 :goto_5

    :cond_8
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto/16 :goto_4

    :cond_9
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0, v7}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string v1, "Select receipt filter"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    const-string v1, "OK"

    new-instance v2, Lcom/chase/sig/android/activity/lc;

    invoke-direct {v2, p0}, Lcom/chase/sig/android/activity/lc;-><init>(Lcom/chase/sig/android/activity/ReceiptsListActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_0

    .line 231
    :pswitch_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->c:Lcom/chase/sig/android/service/ListContentResponse;

    iget-object v1, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/ListContentResponse;->a(Ljava/lang/String;)Lcom/chase/sig/android/domain/receipt/ReceiptFilterList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/receipt/ReceiptFilterList;->a()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/activity/ReceiptsListActivity;->a(Ljava/util/List;)[Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->q:Ljava/util/ArrayList;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->r:Ljava/util/ArrayList;

    new-instance v1, Lcom/chase/sig/android/activity/lj;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/lj;-><init>(Lcom/chase/sig/android/activity/ReceiptsListActivity;)V

    invoke-direct {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsListActivity;->a([Ljava/lang/String;)[Z

    move-result-object v2

    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setMultiChoiceItems([Ljava/lang/CharSequence;[ZLandroid/content/DialogInterface$OnMultiChoiceClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Select "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    const-string v1, "OK"

    new-instance v2, Lcom/chase/sig/android/activity/lk;

    invoke-direct {v2, p0}, Lcom/chase/sig/android/activity/lk;-><init>(Lcom/chase/sig/android/activity/ReceiptsListActivity;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    new-instance v1, Lcom/chase/sig/android/activity/ll;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/ll;-><init>(Lcom/chase/sig/android/activity/ReceiptsListActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_0

    .line 234
    :pswitch_2
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ReceiptsListActivity;->d()Landroid/app/Dialog;

    move-result-object v0

    goto/16 :goto_0

    .line 237
    :pswitch_3
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v1, 0x7f030021

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    const v0, 0x7f090095

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-direct {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsListActivity;->a(Landroid/widget/Button;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f090096

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    invoke-direct {p0, v1}, Lcom/chase/sig/android/activity/ReceiptsListActivity;->a(Landroid/widget/Button;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->o:Lcom/chase/sig/android/service/ReceiptFilter;

    invoke-virtual {v2}, Lcom/chase/sig/android/service/ReceiptFilter;->c()Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_b

    iget-object v2, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->m:Ljava/util/Date;

    if-nez v2, :cond_a

    const-string v2, "From"

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :cond_a
    iget-object v2, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->n:Ljava/util/Date;

    if-nez v2, :cond_b

    const-string v2, "To"

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :cond_b
    iget-object v2, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->m:Ljava/util/Date;

    if-eqz v2, :cond_e

    iget-object v2, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->m:Ljava/util/Date;

    invoke-static {v2}, Lcom/chase/sig/android/util/s;->a(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :cond_c
    :goto_b
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->n:Ljava/util/Date;

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->n:Ljava/util/Date;

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->a(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :cond_d
    :goto_c
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    new-instance v1, Lcom/chase/sig/android/activity/lf;

    invoke-direct {v1, p0, v3}, Lcom/chase/sig/android/activity/lf;-><init>(Lcom/chase/sig/android/activity/ReceiptsListActivity;Landroid/view/View;)V

    new-instance v2, Lcom/chase/sig/android/activity/lg;

    invoke-direct {v2, p0}, Lcom/chase/sig/android/activity/lg;-><init>(Lcom/chase/sig/android/activity/ReceiptsListActivity;)V

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const-string v5, "OK"

    invoke-virtual {v4, v5, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    const-string v1, "Select a date range"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_0

    :cond_e
    iget-object v2, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->o:Lcom/chase/sig/android/service/ReceiptFilter;

    invoke-virtual {v2}, Lcom/chase/sig/android/service/ReceiptFilter;->c()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_c

    iget-object v2, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->o:Lcom/chase/sig/android/service/ReceiptFilter;

    invoke-virtual {v2}, Lcom/chase/sig/android/service/ReceiptFilter;->c()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-class v4, Lcom/chase/sig/android/service/ReceiptFilter$DateFilter;

    invoke-virtual {v2, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    iget-object v2, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->o:Lcom/chase/sig/android/service/ReceiptFilter;

    invoke-virtual {v2}, Lcom/chase/sig/android/service/ReceiptFilter;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/chase/sig/android/service/ReceiptFilter$DateFilter;

    invoke-virtual {v2}, Lcom/chase/sig/android/service/ReceiptFilter$DateFilter;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/chase/sig/android/util/s;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_b

    :cond_f
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->o:Lcom/chase/sig/android/service/ReceiptFilter;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/ReceiptFilter;->c()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->o:Lcom/chase/sig/android/service/ReceiptFilter;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/ReceiptFilter;->c()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v2, Lcom/chase/sig/android/service/ReceiptFilter$DateFilter;

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->o:Lcom/chase/sig/android/service/ReceiptFilter;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/ReceiptFilter;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/service/ReceiptFilter$DateFilter;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/ReceiptFilter$DateFilter;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_c

    :cond_10
    move-object v0, v1

    goto/16 :goto_9

    :cond_11
    move v0, v1

    goto/16 :goto_2

    .line 226
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 210
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ai;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 212
    const-string v1, "receipts"

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->b:Ljava/util/List;

    check-cast v0, Ljava/io/Serializable;

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 213
    const-string v0, "current_filters"

    iget-object v1, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->o:Lcom/chase/sig/android/service/ReceiptFilter;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 214
    const-string v0, "parent_filter"

    iget-object v1, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->k:Lcom/chase/sig/android/domain/receipt/ReceiptFilterList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 215
    const-string v0, "parent_filter_label"

    iget-object v1, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    const-string v0, "selected_filter_labels"

    iget-object v1, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->l:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 217
    const-string v0, "temp_filters"

    iget-object v1, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->s:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 218
    const-string v0, "selectedFromDate"

    iget-object v1, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->m:Ljava/util/Date;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 219
    const-string v0, "selectedToDate"

    iget-object v1, p0, Lcom/chase/sig/android/activity/ReceiptsListActivity;->n:Ljava/util/Date;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 220
    return-void
.end method
