.class public Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;
.super Lcom/chase/sig/android/activity/eb;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity$a;
    }
.end annotation


# instance fields
.field a:Landroid/view/GestureDetector$OnGestureListener;

.field private b:Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity$a;

.field private c:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/chase/sig/android/activity/eb;-><init>()V

    .line 201
    new-instance v0, Lcom/chase/sig/android/activity/lq;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/activity/lq;-><init>(Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;)V

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;->a:Landroid/view/GestureDetector$OnGestureListener;

    .line 237
    return-void
.end method

.method private static a([B)Landroid/graphics/Bitmap;
    .locals 5
    .parameter

    .prologue
    const/4 v0, 0x0

    const/4 v4, -0x1

    .line 177
    :try_start_0
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 178
    const/4 v2, 0x1

    iput-boolean v2, v1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 179
    const/4 v2, 0x0

    array-length v3, p0

    invoke-static {p0, v2, v3, v1}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 180
    iget-boolean v2, v1, Landroid/graphics/BitmapFactory$Options;->mCancel:Z

    if-nez v2, :cond_0

    iget v2, v1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    if-eq v2, v4, :cond_0

    iget v2, v1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    if-ne v2, v4, :cond_1

    .line 190
    :cond_0
    :goto_0
    return-object v0

    .line 183
    :cond_1
    const/4 v2, 0x0

    iput-boolean v2, v1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 185
    const/4 v2, 0x0

    iput-boolean v2, v1, Landroid/graphics/BitmapFactory$Options;->inDither:Z

    .line 186
    const/4 v2, 0x1

    iput-boolean v2, v1, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    .line 187
    sget-object v2, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object v2, v1, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 188
    const/4 v2, 0x0

    array-length v3, p0

    invoke-static {p0, v2, v3, v1}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 190
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;)V
    .locals 2
    .parameter

    .prologue
    .line 29
    iget-boolean v0, p0, Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;->c:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;->c:Z

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;->b:Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity$a;

    iget-boolean v1, p0, Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;->c:Z

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity$a;->a(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;Landroid/content/Intent;)V
    .locals 4
    .parameter
    .parameter

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "receipt"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/Receipt;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "is_browsing_receipts"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    const-string v2, "receipt"

    invoke-virtual {p1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v0, "is_browsing_receipts"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;)Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity$a;
    .locals 1
    .parameter

    .prologue
    .line 29
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;->b:Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity$a;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 5
    .parameter

    .prologue
    const/4 v4, -0x1

    .line 36
    const v0, 0x7f0300a4

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;->setContentView(I)V

    .line 38
    const v0, 0x7f090031

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 41
    new-instance v1, Landroid/view/GestureDetector;

    iget-object v2, p0, Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;->a:Landroid/view/GestureDetector$OnGestureListener;

    invoke-direct {v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/view/GestureDetector$OnGestureListener;)V

    .line 43
    new-instance v2, Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity$a;

    invoke-direct {v2, p0}, Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity$a;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;->b:Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity$a;

    .line 44
    iget-object v2, p0, Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;->b:Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity$a;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity$a;->a(Z)V

    .line 46
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v2, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 48
    iget-object v3, p0, Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;->b:Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity$a;

    invoke-virtual {v3, v2}, Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity$a;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 50
    iget-object v2, p0, Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;->b:Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity$a;

    new-instance v3, Lcom/chase/sig/android/activity/lm;

    invoke-direct {v3, p0, v1}, Lcom/chase/sig/android/activity/lm;-><init>(Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;Landroid/view/GestureDetector;)V

    invoke-virtual {v2, v3}, Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity$a;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 58
    iget-object v1, p0, Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;->b:Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity$a;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;I)V

    .line 59
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 363
    const/4 v0, -0x4

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/eb;->showDialog(I)V

    .line 364
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 63
    invoke-super {p0}, Lcom/chase/sig/android/activity/eb;->onDestroy()V

    .line 64
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;->b:Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity$a;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity$a;->a()V

    .line 65
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;->b:Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity$a;

    .line 66
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 354
    invoke-virtual {p0, p1}, Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;->j(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 355
    const/4 v0, 0x1

    .line 358
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/chase/sig/android/activity/eb;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onResume()V
    .locals 6

    .prologue
    .line 165
    invoke-super {p0}, Lcom/chase/sig/android/activity/eb;->onResume()V

    .line 167
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "image_data"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    .line 168
    invoke-static {v0}, Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;->a([B)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 170
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const v0, 0x7f090037

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lcom/chase/sig/android/activity/ln;

    invoke-direct {v1, p0, v3}, Lcom/chase/sig/android/activity/ln;-><init>(Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;Landroid/os/Bundle;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f09029a

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const/4 v1, 0x1

    if-eqz v3, :cond_0

    const-string v4, "receipt_curr_photo_number"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v1, "receipt_curr_photo_number"

    invoke-virtual {v3, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const v5, 0x7f0700f7

    invoke-virtual {p0, v5}, Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    add-int/lit8 v5, v1, 0x1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    const/4 v4, 0x3

    if-ge v1, v4, :cond_1

    new-instance v1, Lcom/chase/sig/android/activity/lo;

    invoke-direct {v1, p0, v3}, Lcom/chase/sig/android/activity/lo;-><init>(Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;Landroid/os/Bundle;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    const v0, 0x7f090038

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lcom/chase/sig/android/activity/lp;

    invoke-direct {v1, p0, v3}, Lcom/chase/sig/android/activity/lp;-><init>(Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;Landroid/os/Bundle;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 172
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;->b:Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity$a;

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity$a;->a(Landroid/graphics/Bitmap;)V

    .line 173
    return-void

    .line 170
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0
.end method
