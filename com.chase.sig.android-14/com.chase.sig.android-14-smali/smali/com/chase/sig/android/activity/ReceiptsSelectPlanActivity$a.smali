.class public Lcom/chase/sig/android/activity/ReceiptsSelectPlanActivity$a;
.super Lcom/chase/sig/android/b;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/ReceiptsSelectPlanActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/b",
        "<",
        "Lcom/chase/sig/android/activity/ReceiptsSelectPlanActivity;",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/service/GenericResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 213
    invoke-direct {p0}, Lcom/chase/sig/android/b;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .parameter

    .prologue
    .line 213
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/ReceiptsSelectPlanActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/ReceiptsSelectPlanActivity;->A()Lcom/chase/sig/android/service/n;

    move-result-object v0

    iget-object v1, v0, Lcom/chase/sig/android/service/n;->q:Lcom/chase/sig/android/service/ab;

    if-nez v1, :cond_0

    new-instance v1, Lcom/chase/sig/android/service/ab;

    invoke-direct {v1}, Lcom/chase/sig/android/service/ab;-><init>()V

    iput-object v1, v0, Lcom/chase/sig/android/service/n;->q:Lcom/chase/sig/android/service/ab;

    :cond_0
    iget-object v0, v0, Lcom/chase/sig/android/service/n;->q:Lcom/chase/sig/android/service/ab;

    invoke-static {}, Lcom/chase/sig/android/service/ab;->a()Lcom/chase/sig/android/service/ReceiptPlansResponse;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 2
    .parameter

    .prologue
    .line 213
    check-cast p1, Lcom/chase/sig/android/service/GenericResponse;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/GenericResponse;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/ReceiptsSelectPlanActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/GenericResponse;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/ReceiptsSelectPlanActivity;->b(Ljava/util/List;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/ReceiptsSelectPlanActivity;

    check-cast p1, Lcom/chase/sig/android/service/ReceiptPlansResponse;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/ReceiptPlansResponse;->b()Lcom/chase/sig/android/domain/EnrollmentOptions;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/chase/sig/android/activity/ReceiptsSelectPlanActivity;->a(Lcom/chase/sig/android/activity/ReceiptsSelectPlanActivity;Lcom/chase/sig/android/domain/EnrollmentOptions;)V

    goto :goto_0
.end method

.method protected onCancelled()V
    .locals 0

    .prologue
    .line 239
    invoke-super {p0}, Lcom/chase/sig/android/b;->onCancelled()V

    .line 240
    return-void
.end method
