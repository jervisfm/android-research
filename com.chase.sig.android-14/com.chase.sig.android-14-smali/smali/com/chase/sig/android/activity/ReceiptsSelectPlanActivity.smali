.class public Lcom/chase/sig/android/activity/ReceiptsSelectPlanActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/ReceiptsSelectPlanActivity$a;
    }
.end annotation


# instance fields
.field private a:Lcom/chase/sig/android/view/detail/DetailView;

.field private b:Landroid/widget/TextView;

.field private c:Lcom/chase/sig/android/domain/EnrollmentOptions;

.field private d:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    .line 34
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/chase/sig/android/activity/ReceiptsSelectPlanActivity;->d:Z

    .line 213
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/ReceiptsSelectPlanActivity;Lcom/chase/sig/android/domain/EnrollmentOptions;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/chase/sig/android/activity/ReceiptsSelectPlanActivity;->a(Lcom/chase/sig/android/domain/EnrollmentOptions;)V

    return-void
.end method

.method private a(Lcom/chase/sig/android/domain/EnrollmentOptions;)V
    .locals 8
    .parameter

    .prologue
    .line 102
    iput-object p1, p0, Lcom/chase/sig/android/activity/ReceiptsSelectPlanActivity;->c:Lcom/chase/sig/android/domain/EnrollmentOptions;

    .line 103
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/EnrollmentOptions;->c()Ljava/util/List;

    move-result-object v0

    .line 104
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0}, Lcom/chase/sig/android/activity/ReceiptsSelectPlanActivity;->a(Ljava/util/List;)Z

    move-result v2

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/ReceiptsPricingPlan;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/ReceiptsPricingPlan;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/ReceiptsPricingPlan;->b()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lcom/chase/sig/android/view/detail/DetailRowWithSubItem;

    const/4 v7, 0x0

    invoke-direct {v6, v4, v5, v7}, Lcom/chase/sig/android/view/detail/DetailRowWithSubItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6}, Lcom/chase/sig/android/view/detail/DetailRowWithSubItem;->d()Lcom/chase/sig/android/view/detail/a;

    move-result-object v4

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/ReceiptsPricingPlan;->d()Ljava/lang/String;

    move-result-object v5

    const-string v6, "CRPLAN0"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    if-nez v2, :cond_2

    new-instance v5, Landroid/content/Intent;

    const-class v6, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;

    invoke-direct {v5, p0, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v6, "plan_code_id"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/ReceiptsPricingPlan;->d()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-direct {p0, v4, v5}, Lcom/chase/sig/android/activity/ReceiptsSelectPlanActivity;->a(Lcom/chase/sig/android/view/detail/a;Landroid/content/Intent;)V

    :goto_1
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/ReceiptsPricingPlan;->e()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-static {v4}, Lcom/chase/sig/android/activity/ReceiptsSelectPlanActivity;->a(Lcom/chase/sig/android/view/detail/a;)V

    :cond_0
    iget-object v5, p0, Lcom/chase/sig/android/activity/ReceiptsSelectPlanActivity;->c:Lcom/chase/sig/android/domain/EnrollmentOptions;

    invoke-virtual {v5}, Lcom/chase/sig/android/domain/EnrollmentOptions;->a()I

    move-result v5

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/ReceiptsPricingPlan;->c()I

    move-result v0

    if-lt v5, v0, :cond_1

    invoke-static {v4}, Lcom/chase/sig/android/activity/ReceiptsSelectPlanActivity;->a(Lcom/chase/sig/android/view/detail/a;)V

    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, v4, Lcom/chase/sig/android/view/detail/a;->m:Z

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    new-instance v5, Landroid/content/Intent;

    const-class v6, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;

    invoke-direct {v5, p0, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v6, "pricing_plan"

    invoke-virtual {v5, v6, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v6, "enrollment_option"

    iget-object v7, p0, Lcom/chase/sig/android/activity/ReceiptsSelectPlanActivity;->c:Lcom/chase/sig/android/domain/EnrollmentOptions;

    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v6, "receipts_settings_mode"

    invoke-virtual {v5, v6, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-direct {p0, v4, v5}, Lcom/chase/sig/android/activity/ReceiptsSelectPlanActivity;->a(Lcom/chase/sig/android/view/detail/a;Landroid/content/Intent;)V

    goto :goto_1

    :cond_3
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lcom/chase/sig/android/view/detail/a;

    iget-object v2, p0, Lcom/chase/sig/android/activity/ReceiptsSelectPlanActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/chase/sig/android/view/detail/a;

    invoke-virtual {v2, v0}, Lcom/chase/sig/android/view/detail/DetailView;->setRows([Lcom/chase/sig/android/view/detail/a;)V

    .line 105
    return-void
.end method

.method private static a(Lcom/chase/sig/android/view/detail/a;)V
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/chase/sig/android/view/detail/a",
            "<",
            "Lcom/chase/sig/android/view/detail/DetailRow;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 167
    const v0, 0x7f060010

    iput v0, p0, Lcom/chase/sig/android/view/detail/a;->e:I

    .line 170
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/chase/sig/android/view/detail/a;->h:Landroid/view/View$OnClickListener;

    .line 171
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/chase/sig/android/view/detail/a;->q:Z

    .line 172
    return-void
.end method

.method private a(Lcom/chase/sig/android/view/detail/a;Landroid/content/Intent;)V
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/chase/sig/android/view/detail/a",
            "<",
            "Lcom/chase/sig/android/view/detail/DetailRow;",
            "Ljava/lang/String;",
            ">;",
            "Landroid/content/Intent;",
            ")V"
        }
    .end annotation

    .prologue
    .line 180
    new-instance v0, Lcom/chase/sig/android/activity/lr;

    invoke-direct {v0, p0, p2}, Lcom/chase/sig/android/activity/lr;-><init>(Lcom/chase/sig/android/activity/ReceiptsSelectPlanActivity;Landroid/content/Intent;)V

    iput-object v0, p1, Lcom/chase/sig/android/view/detail/a;->h:Landroid/view/View$OnClickListener;

    .line 187
    return-void
.end method

.method private static a(Ljava/util/List;)Z
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/ReceiptsPricingPlan;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 158
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/ReceiptsPricingPlan;

    .line 159
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/ReceiptsPricingPlan;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 160
    const/4 v0, 0x1

    .line 163
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 38
    const v0, 0x7f0300a5

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsSelectPlanActivity;->b(I)V

    .line 40
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->d:Lcom/chase/sig/android/service/SplashResponse;

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->d:Lcom/chase/sig/android/service/SplashResponse;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/SplashResponse;->l()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->d:Lcom/chase/sig/android/service/SplashResponse;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/SplashResponse;->s()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsSelectPlanActivity;->e(Ljava/lang/String;)V

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    .line 41
    const v0, 0x7f09029c

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsSelectPlanActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsSelectPlanActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    .line 42
    const v0, 0x7f090034

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsSelectPlanActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsSelectPlanActivity;->b:Landroid/widget/TextView;

    .line 44
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/ReceiptsSelectPlanActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 45
    if-eqz v0, :cond_0

    .line 46
    const-string v2, "receipts_settings_mode"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/chase/sig/android/activity/ReceiptsSelectPlanActivity;->d:Z

    .line 49
    :cond_0
    if-eqz p1, :cond_3

    const-string v0, "enrollment_option"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 51
    const-string v0, "enrollment_option"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/EnrollmentOptions;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsSelectPlanActivity;->c:Lcom/chase/sig/android/domain/EnrollmentOptions;

    .line 53
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsSelectPlanActivity;->c:Lcom/chase/sig/android/domain/EnrollmentOptions;

    invoke-direct {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsSelectPlanActivity;->a(Lcom/chase/sig/android/domain/EnrollmentOptions;)V

    .line 63
    :cond_1
    :goto_1
    return-void

    :cond_2
    move v0, v1

    .line 40
    goto :goto_0

    .line 55
    :cond_3
    iget-boolean v0, p0, Lcom/chase/sig/android/activity/ReceiptsSelectPlanActivity;->d:Z

    if-eqz v0, :cond_4

    const v0, 0x7f07025b

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsSelectPlanActivity;->setTitle(I)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsSelectPlanActivity;->b:Landroid/widget/TextView;

    const v2, 0x7f07025d

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 56
    :goto_2
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsSelectPlanActivity;->e:Lcom/chase/sig/android/activity/mc;

    const-class v2, Lcom/chase/sig/android/activity/ReceiptsSelectPlanActivity$a;

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/activity/mc;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/ae;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/activity/ReceiptsSelectPlanActivity$a;

    .line 58
    invoke-virtual {v0}, Lcom/chase/sig/android/activity/ReceiptsSelectPlanActivity$a;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v2

    sget-object v3, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-eq v2, v3, :cond_1

    .line 59
    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/ReceiptsSelectPlanActivity$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_1

    .line 55
    :cond_4
    const v0, 0x7f070259

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsSelectPlanActivity;->setTitle(I)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsSelectPlanActivity;->b:Landroid/widget/TextView;

    const v2, 0x7f07025c

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_2
.end method

.method protected onResume()V
    .locals 3

    .prologue
    const v2, 0x7f09029d

    .line 87
    invoke-super {p0}, Lcom/chase/sig/android/activity/ai;->onResume()V

    .line 89
    iget-boolean v0, p0, Lcom/chase/sig/android/activity/ReceiptsSelectPlanActivity;->d:Z

    if-nez v0, :cond_0

    invoke-virtual {p0, v2}, Lcom/chase/sig/android/activity/ReceiptsSelectPlanActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    :cond_0
    invoke-virtual {p0, v2}, Lcom/chase/sig/android/activity/ReceiptsSelectPlanActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    const v1, 0x7f09029e

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f0702d1

    invoke-virtual {p0, v2}, Lcom/chase/sig/android/activity/ReceiptsSelectPlanActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v1, Lcom/chase/sig/android/activity/ls;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/ls;-><init>(Lcom/chase/sig/android/activity/ReceiptsSelectPlanActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 90
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 94
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ai;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 96
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsSelectPlanActivity;->c:Lcom/chase/sig/android/domain/EnrollmentOptions;

    if-eqz v0, :cond_0

    .line 97
    const-string v0, "enrollment_option"

    iget-object v1, p0, Lcom/chase/sig/android/activity/ReceiptsSelectPlanActivity;->c:Lcom/chase/sig/android/domain/EnrollmentOptions;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 99
    :cond_0
    return-void
.end method
