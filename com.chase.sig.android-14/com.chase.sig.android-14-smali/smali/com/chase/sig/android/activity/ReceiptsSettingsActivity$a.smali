.class public Lcom/chase/sig/android/activity/ReceiptsSettingsActivity$a;
.super Lcom/chase/sig/android/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/d",
        "<",
        "Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/service/ReceiptsEnrollmentResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 253
    invoke-direct {p0}, Lcom/chase/sig/android/d;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .parameter

    .prologue
    .line 253
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;->c(Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;)Lcom/chase/sig/android/domain/ReceiptsPricingPlan;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/ReceiptsPricingPlan;->d()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;->b(Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;)Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/view/JPSpinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    const-string v1, "accountId"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;->A()Lcom/chase/sig/android/service/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/service/n;->q()Lcom/chase/sig/android/service/ad;

    invoke-static {v2, v1}, Lcom/chase/sig/android/service/ad;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/service/ReceiptsEnrollmentResponse;

    move-result-object v1

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;->l()V

    return-object v1
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 2
    .parameter

    .prologue
    .line 253
    check-cast p1, Lcom/chase/sig/android/service/ReceiptsEnrollmentResponse;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/ReceiptsEnrollmentResponse;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/ReceiptsEnrollmentResponse;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;->b(Ljava/util/List;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/chase/sig/android/service/ReceiptsEnrollmentResponse;->b()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;->showDialog(I)V

    goto :goto_0
.end method
