.class public Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/ReceiptsSettingsActivity$a;
    }
.end annotation


# instance fields
.field private a:Lcom/chase/sig/android/view/detail/DetailView;

.field private b:Lcom/chase/sig/android/domain/ReceiptsPricingPlan;

.field private c:Landroid/widget/Button;

.field private d:Ljava/lang/String;

.field private k:Z

.field private l:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    .line 41
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;->k:Z

    .line 253
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;)Z
    .locals 1
    .parameter

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;->k:Z

    return v0
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;)Lcom/chase/sig/android/view/JPSpinner;
    .locals 1
    .parameter

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;->d()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;)Lcom/chase/sig/android/domain/ReceiptsPricingPlan;
    .locals 1
    .parameter

    .prologue
    .line 34
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;->b:Lcom/chase/sig/android/domain/ReceiptsPricingPlan;

    return-object v0
.end method

.method static synthetic d(Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;)Landroid/widget/Button;
    .locals 1
    .parameter

    .prologue
    .line 34
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;->c:Landroid/widget/Button;

    return-object v0
.end method

.method private d()Lcom/chase/sig/android/view/JPSpinner;
    .locals 2

    .prologue
    .line 217
    const v0, 0x7f0902a0

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailView;

    .line 218
    const-string v1, "PAY_FROM"

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/detail/DetailView;->e(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/q;

    .line 219
    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/q;->q()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v0

    .line 220
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 11
    .parameter

    .prologue
    const v10, 0x7f0902a1

    const v9, 0x7f070264

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x2

    .line 47
    const v0, 0x7f0300a6

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;->b(I)V

    .line 49
    const v0, 0x7f0902a0

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    .line 51
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 52
    if-nez v1, :cond_0

    .line 53
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;->finish()V

    .line 56
    :cond_0
    const-string v0, "pricing_plan"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/ReceiptsPricingPlan;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;->b:Lcom/chase/sig/android/domain/ReceiptsPricingPlan;

    .line 57
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;->b:Lcom/chase/sig/android/domain/ReceiptsPricingPlan;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/ReceiptsPricingPlan;->d()Ljava/lang/String;

    move-result-object v0

    const-string v2, "CRPLAN0"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;->l:Z

    .line 60
    const-string v0, "receipts_settings_mode"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;->k:Z

    .line 62
    const-string v0, "enrollment_option"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/EnrollmentOptions;

    .line 64
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/EnrollmentOptions;->b()Ljava/util/List;

    move-result-object v1

    .line 66
    new-instance v2, Lcom/chase/sig/android/view/detail/e;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f07025a

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;->b:Lcom/chase/sig/android/domain/ReceiptsPricingPlan;

    invoke-virtual {v3}, Lcom/chase/sig/android/domain/ReceiptsPricingPlan;->a()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;->b:Lcom/chase/sig/android/domain/ReceiptsPricingPlan;

    invoke-virtual {v4}, Lcom/chase/sig/android/domain/ReceiptsPricingPlan;->b()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v0, v3, v4}, Lcom/chase/sig/android/view/detail/e;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    new-instance v0, Lcom/chase/sig/android/view/detail/q;

    const-string v3, "Pay From"

    invoke-direct {v0, v3}, Lcom/chase/sig/android/view/detail/q;-><init>(Ljava/lang/String;)V

    const-string v3, "PAY_FROM"

    iput-object v3, v0, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v0, Lcom/chase/sig/android/view/detail/q;

    const v3, 0x7f070260

    invoke-virtual {p0, v3}, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/view/detail/q;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/q;

    iget-boolean v3, p0, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;->l:Z

    iput-boolean v3, v0, Lcom/chase/sig/android/view/detail/a;->j:Z

    check-cast v0, Lcom/chase/sig/android/view/detail/q;

    .line 72
    iget-object v3, p0, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    new-array v4, v5, [Lcom/chase/sig/android/view/detail/a;

    aput-object v2, v4, v6

    aput-object v0, v4, v7

    invoke-virtual {v3, v4}, Lcom/chase/sig/android/view/detail/DetailView;->setRows([Lcom/chase/sig/android/view/detail/a;)V

    .line 73
    new-array v4, v5, [Ljava/lang/String;

    const-string v0, "nicknameAndMask"

    aput-object v0, v4, v6

    const-string v0, "balance"

    aput-object v0, v4, v7

    new-array v5, v5, [I

    fill-array-data v5, :array_0

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/FundingAccount;

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/FundingAccount;->b()Lcom/chase/sig/android/util/Dollar;

    move-result-object v6

    invoke-virtual {v6}, Lcom/chase/sig/android/util/Dollar;->g()Ljava/lang/String;

    move-result-object v6

    const-string v7, "nicknameAndMask"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/FundingAccount;->c()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v3, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v7, "balance"

    new-instance v8, Lcom/chase/sig/android/util/Dollar;

    invoke-direct {v8, v6}, Lcom/chase/sig/android/util/Dollar;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Lcom/chase/sig/android/util/Dollar;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v3, v7, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v6, "accountId"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/FundingAccount;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v6, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    new-instance v0, Landroid/widget/SimpleAdapter;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    const v3, 0x7f030007

    invoke-direct/range {v0 .. v5}, Landroid/widget/SimpleAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V

    invoke-direct {p0}, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;->d()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/view/JPSpinner;->setAdapter(Landroid/widget/ListAdapter;)V

    new-instance v0, Lcom/chase/sig/android/activity/lw;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/activity/lw;-><init>(Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;)V

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/view/JPSpinner;->a(Lcom/chase/sig/android/view/x;)V

    .line 75
    iget-boolean v0, p0, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;->k:Z

    if-eqz v0, :cond_3

    invoke-virtual {p0, v9}, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;->setTitle(I)V

    .line 76
    :goto_1
    const v0, 0x7f09004a

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lcom/chase/sig/android/activity/lt;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/lt;-><init>(Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 78
    iget-boolean v0, p0, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;->k:Z

    if-eqz v0, :cond_4

    .line 80
    invoke-virtual {p0, v10}, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;->c:Landroid/widget/Button;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;->c:Landroid/widget/Button;

    invoke-virtual {v0, v9}, Landroid/widget/Button;->setText(I)V

    iget-boolean v0, p0, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;->l:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;->c:Landroid/widget/Button;

    const v1, 0x7f02009d

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    :cond_2
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;->c:Landroid/widget/Button;

    new-instance v1, Lcom/chase/sig/android/activity/lu;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/lu;-><init>(Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 85
    :goto_2
    return-void

    .line 75
    :cond_3
    const v0, 0x7f07025b

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;->setTitle(I)V

    goto :goto_1

    .line 83
    :cond_4
    invoke-virtual {p0, v10}, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;->c:Landroid/widget/Button;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;->c:Landroid/widget/Button;

    new-instance v1, Lcom/chase/sig/android/activity/lv;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/lv;-><init>(Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2

    .line 73
    :array_0
    .array-data 0x4
        0x1bt 0x0t 0x9t 0x7ft
        0x1ct 0x0t 0x9t 0x7ft
    .end array-data
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 250
    iput-object p1, p0, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;->d:Ljava/lang/String;

    .line 251
    return-void
.end method

.method protected final a(Lcom/chase/sig/android/view/JPSpinner;)Z
    .locals 2
    .parameter

    .prologue
    .line 171
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;->y()V

    .line 172
    invoke-virtual {p1}, Lcom/chase/sig/android/view/JPSpinner;->getSelectedItemPosition()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 173
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    const-string v1, "PAY_FROM"

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/detail/DetailView;->a(Ljava/lang/String;)V

    .line 174
    const/4 v0, 0x0

    .line 176
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 4
    .parameter

    .prologue
    .line 224
    if-nez p1, :cond_0

    .line 225
    new-instance v0, Lcom/chase/sig/android/view/k$a;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/view/k$a;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/k$a;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/k$a;

    move-result-object v1

    const v2, 0x7f070068

    new-instance v3, Lcom/chase/sig/android/activity/lx;

    invoke-direct {v3, p0}, Lcom/chase/sig/android/activity/lx;-><init>(Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;)V

    invoke-virtual {v1, v2, v3}, Lcom/chase/sig/android/view/k$a;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/k$a;->d(I)Lcom/chase/sig/android/view/k;

    move-result-object v0

    .line 227
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ai;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 180
    invoke-super {p0}, Lcom/chase/sig/android/activity/ai;->onResume()V

    .line 181
    return-void
.end method
