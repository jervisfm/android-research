.class public Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity$b;
.super Lcom/chase/sig/android/b;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/b",
        "<",
        "Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;",
        "Lcom/chase/sig/android/domain/Receipt;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/service/ReceiptsTransactionMatchResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 180
    invoke-direct {p0}, Lcom/chase/sig/android/b;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 180
    check-cast p1, [Lcom/chase/sig/android/domain/Receipt;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;->A()Lcom/chase/sig/android/service/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/service/n;->r()Lcom/chase/sig/android/service/ac;

    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-static {v0}, Lcom/chase/sig/android/service/ac;->b(Lcom/chase/sig/android/domain/Receipt;)Lcom/chase/sig/android/service/ReceiptsTransactionMatchResponse;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 2
    .parameter

    .prologue
    .line 180
    check-cast p1, Lcom/chase/sig/android/service/ReceiptsTransactionMatchResponse;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/ReceiptsTransactionMatchResponse;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/chase/sig/android/service/ReceiptsTransactionMatchResponse;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;->b(Ljava/util/List;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/chase/sig/android/service/ReceiptsTransactionMatchResponse;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/chase/sig/android/service/ReceiptsTransactionMatchResponse;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;->a(Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;Ljava/lang/String;)Ljava/lang/String;

    invoke-static {v0}, Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;->d(Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;->b(Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/chase/sig/android/service/ReceiptsTransactionMatchResponse;->c()Lcom/chase/sig/android/domain/AccountActivityForReceipt;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/AccountActivityForReceipt;->a()Lcom/chase/sig/android/domain/TransactionsForReceiptPage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/TransactionsForReceiptPage;->a()Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;->a(Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;Ljava/util/List;)V

    goto :goto_0
.end method

.method protected onCancelled()V
    .locals 0

    .prologue
    .line 213
    invoke-super {p0}, Lcom/chase/sig/android/b;->onCancelled()V

    .line 214
    return-void
.end method
