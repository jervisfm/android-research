.class public Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity$c;,
        Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity$a;,
        Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity$b;
    }
.end annotation


# instance fields
.field private a:Landroid/widget/ListView;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/TransactionForReceipt;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/chase/sig/android/domain/Receipt;

.field private d:Z

.field private k:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    .line 37
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;->d:Z

    .line 243
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 27
    iput-object p1, p0, Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;->k:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;Ljava/util/List;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;->a(Ljava/util/List;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 2
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 172
    const v0, 0x7f090299

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 173
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 174
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 176
    const v0, 0x7f090052

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 177
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 178
    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/TransactionForReceipt;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 84
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_1

    .line 95
    :cond_0
    :goto_0
    return-void

    .line 88
    :cond_1
    const v0, 0x7f090298

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;->a:Landroid/widget/ListView;

    .line 90
    new-instance v0, Lcom/chase/sig/android/view/ae;

    invoke-direct {v0, p0, p1}, Lcom/chase/sig/android/view/ae;-><init>(Landroid/content/Context;Ljava/util/List;)V

    .line 92
    iget-object v1, p0, Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;->a:Landroid/widget/ListView;

    new-instance v2, Lcom/chase/sig/android/view/af;

    invoke-direct {v2, v0}, Lcom/chase/sig/android/view/af;-><init>(Landroid/widget/ListAdapter;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 94
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;->a:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;)Z
    .locals 1
    .parameter

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;->d:Z

    return v0
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;)Lcom/chase/sig/android/domain/Receipt;
    .locals 1
    .parameter

    .prologue
    .line 27
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;->c:Lcom/chase/sig/android/domain/Receipt;

    return-object v0
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;->a(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic c(Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;)Landroid/widget/ListView;
    .locals 1
    .parameter

    .prologue
    .line 27
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;->a:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic d(Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 27
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;->k:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 4
    .parameter

    .prologue
    .line 44
    const v0, 0x7f0300a3

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;->b(I)V

    .line 45
    const v0, 0x7f070268

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;->setTitle(I)V

    .line 47
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 48
    const-string v0, "receipt"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 49
    const-string v0, "receipt"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/Receipt;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;->c:Lcom/chase/sig/android/domain/Receipt;

    .line 52
    :cond_0
    const-string v0, "is_editing"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 53
    const-string v0, "is_editing"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;->d:Z

    .line 56
    :cond_1
    if-nez p1, :cond_2

    .line 57
    const-class v0, Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity$b;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/chase/sig/android/domain/Receipt;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;->c:Lcom/chase/sig/android/domain/Receipt;

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 69
    :goto_0
    const v0, 0x7f070273

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v0, 0x7f0900a3

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 71
    const v0, 0x7f070274

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const v0, 0x7f090297

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    new-instance v1, Lcom/chase/sig/android/activity/ly;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/ly;-><init>(Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 72
    const v0, 0x7f090052

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const v1, 0x7f07006c

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    new-instance v1, Lcom/chase/sig/android/activity/lz;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/lz;-><init>(Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 74
    return-void

    .line 59
    :cond_2
    const-string v0, "receipt_bundle_key"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/Receipt;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;->c:Lcom/chase/sig/android/domain/Receipt;

    .line 60
    const-string v0, "trans_matches_bundle_key"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;->b:Ljava/util/List;

    .line 62
    const-string v0, "no_transaction_found_message"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;->k:Ljava/lang/String;

    .line 63
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;->k:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 64
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;->k:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;->a(Ljava/lang/String;)V

    .line 66
    :cond_3
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;->b:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;->a(Ljava/util/List;)V

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 149
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ai;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 151
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;->b:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 152
    const-string v1, "trans_matches_bundle_key"

    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;->b:Ljava/util/List;

    check-cast v0, Ljava/io/Serializable;

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 155
    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;->k:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 156
    const-string v0, "no_transaction_found_message"

    iget-object v1, p0, Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    :cond_1
    const-string v0, "receipt_bundle_key"

    iget-object v1, p0, Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;->c:Lcom/chase/sig/android/domain/Receipt;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 160
    return-void
.end method
