.class public final enum Lcom/chase/sig/android/activity/TeamInfoActivity$CommunicationOptions;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/TeamInfoActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CommunicationOptions"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/chase/sig/android/activity/TeamInfoActivity$CommunicationOptions;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/chase/sig/android/activity/TeamInfoActivity$CommunicationOptions;

.field public static final enum b:Lcom/chase/sig/android/activity/TeamInfoActivity$CommunicationOptions;

.field public static final enum c:Lcom/chase/sig/android/activity/TeamInfoActivity$CommunicationOptions;

.field public static final enum d:Lcom/chase/sig/android/activity/TeamInfoActivity$CommunicationOptions;

.field private static final synthetic e:[Lcom/chase/sig/android/activity/TeamInfoActivity$CommunicationOptions;


# instance fields
.field private final displayName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 250
    new-instance v0, Lcom/chase/sig/android/activity/TeamInfoActivity$CommunicationOptions;

    const-string v1, "CALL"

    const-string v2, "Call"

    invoke-direct {v0, v1, v3, v2}, Lcom/chase/sig/android/activity/TeamInfoActivity$CommunicationOptions;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/activity/TeamInfoActivity$CommunicationOptions;->a:Lcom/chase/sig/android/activity/TeamInfoActivity$CommunicationOptions;

    new-instance v0, Lcom/chase/sig/android/activity/TeamInfoActivity$CommunicationOptions;

    const-string v1, "EMAIL"

    const-string v2, "Send Email"

    invoke-direct {v0, v1, v4, v2}, Lcom/chase/sig/android/activity/TeamInfoActivity$CommunicationOptions;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/activity/TeamInfoActivity$CommunicationOptions;->b:Lcom/chase/sig/android/activity/TeamInfoActivity$CommunicationOptions;

    new-instance v0, Lcom/chase/sig/android/activity/TeamInfoActivity$CommunicationOptions;

    const-string v1, "CONTACT_NEW"

    const-string v2, "Add new contact"

    invoke-direct {v0, v1, v5, v2}, Lcom/chase/sig/android/activity/TeamInfoActivity$CommunicationOptions;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/activity/TeamInfoActivity$CommunicationOptions;->c:Lcom/chase/sig/android/activity/TeamInfoActivity$CommunicationOptions;

    .line 251
    new-instance v0, Lcom/chase/sig/android/activity/TeamInfoActivity$CommunicationOptions;

    const-string v1, "CONTACT_EXISTING"

    const-string v2, "Add to existing contact"

    invoke-direct {v0, v1, v6, v2}, Lcom/chase/sig/android/activity/TeamInfoActivity$CommunicationOptions;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/activity/TeamInfoActivity$CommunicationOptions;->d:Lcom/chase/sig/android/activity/TeamInfoActivity$CommunicationOptions;

    .line 249
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/chase/sig/android/activity/TeamInfoActivity$CommunicationOptions;

    sget-object v1, Lcom/chase/sig/android/activity/TeamInfoActivity$CommunicationOptions;->a:Lcom/chase/sig/android/activity/TeamInfoActivity$CommunicationOptions;

    aput-object v1, v0, v3

    sget-object v1, Lcom/chase/sig/android/activity/TeamInfoActivity$CommunicationOptions;->b:Lcom/chase/sig/android/activity/TeamInfoActivity$CommunicationOptions;

    aput-object v1, v0, v4

    sget-object v1, Lcom/chase/sig/android/activity/TeamInfoActivity$CommunicationOptions;->c:Lcom/chase/sig/android/activity/TeamInfoActivity$CommunicationOptions;

    aput-object v1, v0, v5

    sget-object v1, Lcom/chase/sig/android/activity/TeamInfoActivity$CommunicationOptions;->d:Lcom/chase/sig/android/activity/TeamInfoActivity$CommunicationOptions;

    aput-object v1, v0, v6

    sput-object v0, Lcom/chase/sig/android/activity/TeamInfoActivity$CommunicationOptions;->e:[Lcom/chase/sig/android/activity/TeamInfoActivity$CommunicationOptions;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 255
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 256
    iput-object p3, p0, Lcom/chase/sig/android/activity/TeamInfoActivity$CommunicationOptions;->displayName:Ljava/lang/String;

    .line 257
    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/chase/sig/android/activity/TeamInfoActivity$CommunicationOptions;
    .locals 5
    .parameter

    .prologue
    .line 264
    invoke-static {}, Lcom/chase/sig/android/activity/TeamInfoActivity$CommunicationOptions;->values()[Lcom/chase/sig/android/activity/TeamInfoActivity$CommunicationOptions;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 265
    iget-object v4, v3, Lcom/chase/sig/android/activity/TeamInfoActivity$CommunicationOptions;->displayName:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 266
    return-object v3

    .line 264
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 268
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/chase/sig/android/activity/TeamInfoActivity$CommunicationOptions;
    .locals 1
    .parameter

    .prologue
    .line 249
    const-class v0, Lcom/chase/sig/android/activity/TeamInfoActivity$CommunicationOptions;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/activity/TeamInfoActivity$CommunicationOptions;

    return-object v0
.end method

.method public static values()[Lcom/chase/sig/android/activity/TeamInfoActivity$CommunicationOptions;
    .locals 1

    .prologue
    .line 249
    sget-object v0, Lcom/chase/sig/android/activity/TeamInfoActivity$CommunicationOptions;->e:[Lcom/chase/sig/android/activity/TeamInfoActivity$CommunicationOptions;

    invoke-virtual {v0}, [Lcom/chase/sig/android/activity/TeamInfoActivity$CommunicationOptions;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/chase/sig/android/activity/TeamInfoActivity$CommunicationOptions;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lcom/chase/sig/android/activity/TeamInfoActivity$CommunicationOptions;->displayName:Ljava/lang/String;

    return-object v0
.end method
