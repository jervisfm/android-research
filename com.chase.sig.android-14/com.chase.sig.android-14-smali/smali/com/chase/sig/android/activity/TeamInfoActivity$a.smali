.class public Lcom/chase/sig/android/activity/TeamInfoActivity$a;
.super Lcom/chase/sig/android/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/TeamInfoActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/d",
        "<",
        "Lcom/chase/sig/android/activity/TeamInfoActivity;",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/service/TeamInfoResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 95
    invoke-direct {p0}, Lcom/chase/sig/android/d;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .parameter

    .prologue
    .line 95
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/TeamInfoActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/TeamInfoActivity;->A()Lcom/chase/sig/android/service/n;

    move-result-object v0

    iget-object v1, v0, Lcom/chase/sig/android/service/n;->n:Lcom/chase/sig/android/service/aj;

    if-nez v1, :cond_0

    new-instance v1, Lcom/chase/sig/android/service/aj;

    invoke-direct {v1}, Lcom/chase/sig/android/service/aj;-><init>()V

    iput-object v1, v0, Lcom/chase/sig/android/service/n;->n:Lcom/chase/sig/android/service/aj;

    :cond_0
    iget-object v0, v0, Lcom/chase/sig/android/service/n;->n:Lcom/chase/sig/android/service/aj;

    invoke-static {}, Lcom/chase/sig/android/service/aj;->a()Lcom/chase/sig/android/service/TeamInfoResponse;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 4
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 95
    check-cast p1, Lcom/chase/sig/android/service/TeamInfoResponse;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/TeamInfoResponse;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/TeamInfoActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/TeamInfoResponse;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/TeamInfoActivity;->b(Ljava/util/List;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/chase/sig/android/service/TeamInfoResponse;->a()Ljava/util/List;

    move-result-object v1

    new-instance v0, Lcom/chase/sig/android/activity/TeamInfoActivity$c;

    invoke-direct {v0, v3}, Lcom/chase/sig/android/activity/TeamInfoActivity$c;-><init>(B)V

    invoke-static {v1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/TeamInfoActivity;

    invoke-static {v0, v1}, Lcom/chase/sig/android/activity/TeamInfoActivity;->a(Lcom/chase/sig/android/activity/TeamInfoActivity;Ljava/util/List;)Ljava/util/List;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/TeamInfoActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/TeamInfoResponse;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/chase/sig/android/activity/TeamInfoActivity;->a(Lcom/chase/sig/android/activity/TeamInfoActivity;Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/TeamInfoActivity;

    invoke-virtual {v0, v1, v3}, Lcom/chase/sig/android/activity/TeamInfoActivity;->a(Ljava/util/List;Z)V

    goto :goto_0
.end method
