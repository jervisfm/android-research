.class final Lcom/chase/sig/android/activity/TeamInfoActivity$b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/TeamInfoActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/TeamInfoActivity;


# direct methods
.method private constructor <init>(Lcom/chase/sig/android/activity/TeamInfoActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 353
    iput-object p1, p0, Lcom/chase/sig/android/activity/TeamInfoActivity$b;->a:Lcom/chase/sig/android/activity/TeamInfoActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/chase/sig/android/activity/TeamInfoActivity;B)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 353
    invoke-direct {p0, p1}, Lcom/chase/sig/android/activity/TeamInfoActivity$b;-><init>(Lcom/chase/sig/android/activity/TeamInfoActivity;)V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 8
    .parameter

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x0

    .line 356
    iget-object v0, p0, Lcom/chase/sig/android/activity/TeamInfoActivity$b;->a:Lcom/chase/sig/android/activity/TeamInfoActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/TeamInfoActivity;->b(Lcom/chase/sig/android/activity/TeamInfoActivity;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 357
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 359
    iget-object v0, p0, Lcom/chase/sig/android/activity/TeamInfoActivity$b;->a:Lcom/chase/sig/android/activity/TeamInfoActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/TeamInfoActivity;->c(Lcom/chase/sig/android/activity/TeamInfoActivity;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/Advisor;

    .line 360
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Advisor;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 361
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 365
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 366
    iget-object v0, p0, Lcom/chase/sig/android/activity/TeamInfoActivity$b;->a:Lcom/chase/sig/android/activity/TeamInfoActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/TeamInfoActivity;->d(Lcom/chase/sig/android/activity/TeamInfoActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 367
    iget-object v0, p0, Lcom/chase/sig/android/activity/TeamInfoActivity$b;->a:Lcom/chase/sig/android/activity/TeamInfoActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/TeamInfoActivity;->e(Lcom/chase/sig/android/activity/TeamInfoActivity;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/ListView;->setVisibility(I)V

    .line 368
    iget-object v0, p0, Lcom/chase/sig/android/activity/TeamInfoActivity$b;->a:Lcom/chase/sig/android/activity/TeamInfoActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/chase/sig/android/activity/TeamInfoActivity;->a(Ljava/util/List;Z)V

    .line 373
    :goto_1
    return-void

    .line 370
    :cond_2
    iget-object v0, p0, Lcom/chase/sig/android/activity/TeamInfoActivity$b;->a:Lcom/chase/sig/android/activity/TeamInfoActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/TeamInfoActivity;->e(Lcom/chase/sig/android/activity/TeamInfoActivity;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/widget/ListView;->setVisibility(I)V

    .line 371
    iget-object v0, p0, Lcom/chase/sig/android/activity/TeamInfoActivity$b;->a:Lcom/chase/sig/android/activity/TeamInfoActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/TeamInfoActivity;->d(Lcom/chase/sig/android/activity/TeamInfoActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 377
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 381
    return-void
.end method
