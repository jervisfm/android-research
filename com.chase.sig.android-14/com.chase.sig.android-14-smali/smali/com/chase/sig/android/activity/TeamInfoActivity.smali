.class public Lcom/chase/sig/android/activity/TeamInfoActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/TeamInfoActivity$1;,
        Lcom/chase/sig/android/activity/TeamInfoActivity$b;,
        Lcom/chase/sig/android/activity/TeamInfoActivity$c;,
        Lcom/chase/sig/android/activity/TeamInfoActivity$d;,
        Lcom/chase/sig/android/activity/TeamInfoActivity$CommunicationOptions;,
        Lcom/chase/sig/android/activity/TeamInfoActivity$a;
    }
.end annotation


# static fields
.field private static final a:Lcom/chase/sig/android/activity/cc;


# instance fields
.field private final b:I

.field private final c:I

.field private final d:I

.field private k:Landroid/widget/ListView;

.field private l:Landroid/widget/EditText;

.field private m:Landroid/widget/TextView;

.field private n:Lcom/chase/sig/android/domain/Advisor;

.field private o:Ljava/lang/String;

.field private p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/Advisor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    invoke-static {}, Lcom/chase/sig/android/activity/cc;->b()Lcom/chase/sig/android/activity/cc;

    move-result-object v0

    sput-object v0, Lcom/chase/sig/android/activity/TeamInfoActivity;->a:Lcom/chase/sig/android/activity/cc;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    .line 49
    const/4 v0, 0x0

    iput v0, p0, Lcom/chase/sig/android/activity/TeamInfoActivity;->b:I

    .line 50
    const/4 v0, 0x1

    iput v0, p0, Lcom/chase/sig/android/activity/TeamInfoActivity;->c:I

    .line 51
    const/4 v0, 0x2

    iput v0, p0, Lcom/chase/sig/android/activity/TeamInfoActivity;->d:I

    .line 353
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/TeamInfoActivity;)Lcom/chase/sig/android/domain/Advisor;
    .locals 1
    .parameter

    .prologue
    .line 40
    iget-object v0, p0, Lcom/chase/sig/android/activity/TeamInfoActivity;->n:Lcom/chase/sig/android/domain/Advisor;

    return-object v0
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/TeamInfoActivity;Lcom/chase/sig/android/domain/Advisor;)Lcom/chase/sig/android/domain/Advisor;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 40
    iput-object p1, p0, Lcom/chase/sig/android/activity/TeamInfoActivity;->n:Lcom/chase/sig/android/domain/Advisor;

    return-object p1
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/TeamInfoActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 40
    iput-object p1, p0, Lcom/chase/sig/android/activity/TeamInfoActivity;->o:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/TeamInfoActivity;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 40
    iput-object p1, p0, Lcom/chase/sig/android/activity/TeamInfoActivity;->p:Ljava/util/List;

    return-object p1
.end method

.method private a(Lcom/chase/sig/android/domain/Advisor;)Ljava/util/List;
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/chase/sig/android/domain/Advisor;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 221
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 223
    if-eqz p1, :cond_3

    .line 224
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/Advisor;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 225
    sget-object v1, Lcom/chase/sig/android/activity/TeamInfoActivity$CommunicationOptions;->a:Lcom/chase/sig/android/activity/TeamInfoActivity$CommunicationOptions;

    invoke-virtual {v1}, Lcom/chase/sig/android/activity/TeamInfoActivity$CommunicationOptions;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 228
    :cond_0
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/Advisor;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 229
    sget-object v1, Lcom/chase/sig/android/activity/TeamInfoActivity$CommunicationOptions;->b:Lcom/chase/sig/android/activity/TeamInfoActivity$CommunicationOptions;

    invoke-virtual {v1}, Lcom/chase/sig/android/activity/TeamInfoActivity$CommunicationOptions;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 232
    :cond_1
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/Advisor;->d()Ljava/lang/String;

    sget-object v1, Lcom/chase/sig/android/activity/TeamInfoActivity;->a:Lcom/chase/sig/android/activity/cc;

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/Advisor;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, p0, v2}, Lcom/chase/sig/android/activity/cc;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 233
    sget-object v1, Lcom/chase/sig/android/activity/TeamInfoActivity$CommunicationOptions;->c:Lcom/chase/sig/android/activity/TeamInfoActivity$CommunicationOptions;

    invoke-virtual {v1}, Lcom/chase/sig/android/activity/TeamInfoActivity$CommunicationOptions;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 236
    :cond_2
    sget-object v1, Lcom/chase/sig/android/activity/TeamInfoActivity$CommunicationOptions;->d:Lcom/chase/sig/android/activity/TeamInfoActivity$CommunicationOptions;

    invoke-virtual {v1}, Lcom/chase/sig/android/activity/TeamInfoActivity$CommunicationOptions;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 239
    :cond_3
    return-object v0
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/TeamInfoActivity;Lcom/chase/sig/android/activity/TeamInfoActivity$CommunicationOptions;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 40
    sget-object v0, Lcom/chase/sig/android/activity/TeamInfoActivity$1;->a:[I

    invoke-virtual {p1}, Lcom/chase/sig/android/activity/TeamInfoActivity$CommunicationOptions;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0, v2}, Lcom/chase/sig/android/activity/TeamInfoActivity;->dismissDialog(I)V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.DIAL"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "tel:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/chase/sig/android/activity/TeamInfoActivity;->n:Lcom/chase/sig/android/domain/Advisor;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/Advisor;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/TeamInfoActivity;->a(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/TeamInfoActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Please call "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/chase/sig/android/activity/TeamInfoActivity;->n:Lcom/chase/sig/android/domain/Advisor;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/Advisor;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " at "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/chase/sig/android/activity/TeamInfoActivity;->n:Lcom/chase/sig/android/domain/Advisor;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/Advisor;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/TeamInfoActivity;->f(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0, v2}, Lcom/chase/sig/android/activity/TeamInfoActivity;->dismissDialog(I)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/TeamInfoActivity;->showDialog(I)V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0, v2}, Lcom/chase/sig/android/activity/TeamInfoActivity;->dismissDialog(I)V

    sget-object v0, Lcom/chase/sig/android/activity/TeamInfoActivity;->a:Lcom/chase/sig/android/activity/cc;

    iget-object v1, p0, Lcom/chase/sig/android/activity/TeamInfoActivity;->n:Lcom/chase/sig/android/domain/Advisor;

    iget-object v2, p0, Lcom/chase/sig/android/activity/TeamInfoActivity;->o:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/chase/sig/android/activity/cc;->a(Lcom/chase/sig/android/domain/Advisor;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/TeamInfoActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_3
    invoke-virtual {p0, v2}, Lcom/chase/sig/android/activity/TeamInfoActivity;->dismissDialog(I)V

    sget-object v0, Lcom/chase/sig/android/activity/TeamInfoActivity;->a:Lcom/chase/sig/android/activity/cc;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/cc;->a()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0, v2}, Lcom/chase/sig/android/activity/TeamInfoActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/TeamInfoActivity;)Landroid/widget/EditText;
    .locals 1
    .parameter

    .prologue
    .line 40
    iget-object v0, p0, Lcom/chase/sig/android/activity/TeamInfoActivity;->l:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic c(Lcom/chase/sig/android/activity/TeamInfoActivity;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 40
    iget-object v0, p0, Lcom/chase/sig/android/activity/TeamInfoActivity;->p:Ljava/util/List;

    return-object v0
.end method

.method static synthetic d(Lcom/chase/sig/android/activity/TeamInfoActivity;)Landroid/widget/TextView;
    .locals 1
    .parameter

    .prologue
    .line 40
    iget-object v0, p0, Lcom/chase/sig/android/activity/TeamInfoActivity;->m:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic e(Lcom/chase/sig/android/activity/TeamInfoActivity;)Landroid/widget/ListView;
    .locals 1
    .parameter

    .prologue
    .line 40
    iget-object v0, p0, Lcom/chase/sig/android/activity/TeamInfoActivity;->k:Landroid/widget/ListView;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 5
    .parameter

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 63
    const v0, 0x7f0702ce

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/TeamInfoActivity;->setTitle(I)V

    .line 64
    const v0, 0x7f0300bf

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/TeamInfoActivity;->b(I)V

    .line 66
    const v0, 0x7f0902b9

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/TeamInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/TeamInfoActivity;->k:Landroid/widget/ListView;

    .line 67
    const v0, 0x7f0902b8

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/TeamInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/chase/sig/android/activity/TeamInfoActivity;->l:Landroid/widget/EditText;

    .line 68
    const v0, 0x7f09027b

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/TeamInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/TeamInfoActivity;->m:Landroid/widget/TextView;

    .line 70
    const-string v0, "clickedContact"

    invoke-static {p1, v0, v4}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/Advisor;

    iput-object v0, p0, Lcom/chase/sig/android/activity/TeamInfoActivity;->n:Lcom/chase/sig/android/domain/Advisor;

    .line 71
    const-string v0, "companyName"

    invoke-static {p1, v0, v4}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/chase/sig/android/activity/TeamInfoActivity;->o:Ljava/lang/String;

    .line 72
    const-string v0, "response"

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {p1, v0, v1}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/chase/sig/android/activity/TeamInfoActivity;->p:Ljava/util/List;

    .line 75
    iget-object v0, p0, Lcom/chase/sig/android/activity/TeamInfoActivity;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    const-class v0, Lcom/chase/sig/android/activity/TeamInfoActivity$a;

    new-array v1, v3, [Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/TeamInfoActivity;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 81
    :goto_0
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/TeamInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0200bc

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 82
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 83
    iget-object v1, p0, Lcom/chase/sig/android/activity/TeamInfoActivity;->l:Landroid/widget/EditText;

    invoke-virtual {v1, v4, v4, v0, v4}, Landroid/widget/EditText;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 84
    iget-object v0, p0, Lcom/chase/sig/android/activity/TeamInfoActivity;->l:Landroid/widget/EditText;

    new-instance v1, Lcom/chase/sig/android/activity/TeamInfoActivity$b;

    invoke-direct {v1, p0, v3}, Lcom/chase/sig/android/activity/TeamInfoActivity$b;-><init>(Lcom/chase/sig/android/activity/TeamInfoActivity;B)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 85
    return-void

    .line 78
    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/TeamInfoActivity;->p:Ljava/util/List;

    invoke-virtual {p0, v0, v3}, Lcom/chase/sig/android/activity/TeamInfoActivity;->a(Ljava/util/List;Z)V

    goto :goto_0
.end method

.method public final a(Ljava/util/List;Z)V
    .locals 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/Advisor;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 122
    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 123
    new-instance v0, Lcom/chase/sig/android/activity/TeamInfoActivity$d;

    invoke-direct {v0, p0, p0, p1}, Lcom/chase/sig/android/activity/TeamInfoActivity$d;-><init>(Lcom/chase/sig/android/activity/TeamInfoActivity;Landroid/content/Context;Ljava/util/List;)V

    .line 125
    iget-object v1, p0, Lcom/chase/sig/android/activity/TeamInfoActivity;->k:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 127
    iget-object v1, p0, Lcom/chase/sig/android/activity/TeamInfoActivity;->k:Landroid/widget/ListView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setFooterDividersEnabled(Z)V

    .line 128
    if-eqz p2, :cond_0

    .line 129
    invoke-virtual {v0}, Lcom/chase/sig/android/activity/TeamInfoActivity$d;->notifyDataSetChanged()V

    .line 131
    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/TeamInfoActivity;->k:Landroid/widget/ListView;

    new-instance v1, Lcom/chase/sig/android/activity/md;

    invoke-direct {v1, p0, p1}, Lcom/chase/sig/android/activity/md;-><init>(Lcom/chase/sig/android/activity/TeamInfoActivity;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 142
    :cond_1
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 386
    invoke-super {p0, p1, p2, p3}, Lcom/chase/sig/android/activity/ai;->onActivityResult(IILandroid/content/Intent;)V

    .line 388
    if-nez p1, :cond_0

    .line 389
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 390
    sget-object v0, Lcom/chase/sig/android/activity/TeamInfoActivity;->a:Lcom/chase/sig/android/activity/cc;

    iget-object v1, p0, Lcom/chase/sig/android/activity/TeamInfoActivity;->n:Lcom/chase/sig/android/domain/Advisor;

    iget-object v2, p0, Lcom/chase/sig/android/activity/TeamInfoActivity;->o:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/chase/sig/android/activity/cc;->b(Lcom/chase/sig/android/domain/Advisor;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 391
    const/4 v1, 0x2

    invoke-virtual {v0, p3, v1}, Landroid/content/Intent;->fillIn(Landroid/content/Intent;I)I

    .line 392
    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/TeamInfoActivity;->startActivity(Landroid/content/Intent;)V

    .line 395
    :cond_0
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 6
    .parameter

    .prologue
    const/4 v5, -0x1

    const/4 v3, 0x1

    .line 160
    new-instance v0, Lcom/chase/sig/android/view/k$a;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/view/k$a;-><init>(Landroid/content/Context;)V

    .line 161
    packed-switch p1, :pswitch_data_0

    .line 215
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ai;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    .line 164
    :pswitch_0
    iput-boolean v3, v0, Lcom/chase/sig/android/view/k$a;->i:Z

    const-string v1, "Communication Options"

    iput-object v1, v0, Lcom/chase/sig/android/view/k$a;->b:Ljava/lang/CharSequence;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/chase/sig/android/view/k$a;->d:Landroid/graphics/drawable/Drawable;

    .line 165
    new-instance v1, Landroid/widget/ListView;

    invoke-direct {v1, p0}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    .line 166
    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setId(I)V

    .line 167
    invoke-virtual {v1, v3}, Landroid/widget/ListView;->setClickable(Z)V

    .line 168
    new-instance v2, Landroid/widget/ArrayAdapter;

    const v3, 0x7f030051

    iget-object v4, p0, Lcom/chase/sig/android/activity/TeamInfoActivity;->n:Lcom/chase/sig/android/domain/Advisor;

    invoke-direct {p0, v4}, Lcom/chase/sig/android/activity/TeamInfoActivity;->a(Lcom/chase/sig/android/domain/Advisor;)Ljava/util/List;

    move-result-object v4

    invoke-direct {v2, p0, v3, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 170
    new-instance v2, Lcom/chase/sig/android/activity/me;

    invoke-direct {v2, p0}, Lcom/chase/sig/android/activity/me;-><init>(Lcom/chase/sig/android/activity/TeamInfoActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 179
    iput-object v1, v0, Lcom/chase/sig/android/view/k$a;->k:Landroid/view/View;

    .line 180
    invoke-virtual {v0, v5}, Lcom/chase/sig/android/view/k$a;->d(I)Lcom/chase/sig/android/view/k;

    move-result-object v0

    goto :goto_0

    .line 184
    :pswitch_1
    iput-boolean v3, v0, Lcom/chase/sig/android/view/k$a;->i:Z

    const v1, 0x7f07006b

    new-instance v2, Lcom/chase/sig/android/activity/mg;

    invoke-direct {v2, p0}, Lcom/chase/sig/android/activity/mg;-><init>(Lcom/chase/sig/android/activity/TeamInfoActivity;)V

    invoke-virtual {v0, v1, v2}, Lcom/chase/sig/android/view/k$a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    move-result-object v1

    const v2, 0x7f070068

    new-instance v3, Lcom/chase/sig/android/activity/mf;

    invoke-direct {v3, p0}, Lcom/chase/sig/android/activity/mf;-><init>(Lcom/chase/sig/android/activity/TeamInfoActivity;)V

    invoke-virtual {v1, v2, v3}, Lcom/chase/sig/android/view/k$a;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    .line 210
    const v1, 0x7f0702cf

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/k$a;->b(I)Lcom/chase/sig/android/view/k$a;

    move-result-object v1

    const v2, 0x7f0702d0

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/view/k$a;->c(I)Lcom/chase/sig/android/view/k$a;

    .line 212
    invoke-virtual {v0, v5}, Lcom/chase/sig/android/view/k$a;->d(I)Lcom/chase/sig/android/view/k;

    move-result-object v0

    goto :goto_0

    .line 161
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;)V
    .locals 4
    .parameter
    .parameter

    .prologue
    .line 147
    packed-switch p1, :pswitch_data_0

    .line 156
    :goto_0
    return-void

    .line 150
    :pswitch_0
    check-cast p2, Lcom/chase/sig/android/view/k;

    const/4 v0, 0x2

    invoke-virtual {p2, v0}, Lcom/chase/sig/android/view/k;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 151
    invoke-virtual {v0}, Landroid/widget/ListView;->invalidateViews()V

    .line 152
    new-instance v1, Landroid/widget/ArrayAdapter;

    const v2, 0x1090003

    iget-object v3, p0, Lcom/chase/sig/android/activity/TeamInfoActivity;->n:Lcom/chase/sig/android/domain/Advisor;

    invoke-direct {p0, v3}, Lcom/chase/sig/android/activity/TeamInfoActivity;->a(Lcom/chase/sig/android/domain/Advisor;)Ljava/util/List;

    move-result-object v3

    invoke-direct {v1, p0, v2, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0

    .line 147
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 89
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ai;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 90
    const-string v1, "response"

    iget-object v0, p0, Lcom/chase/sig/android/activity/TeamInfoActivity;->p:Ljava/util/List;

    check-cast v0, Ljava/io/Serializable;

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 91
    const-string v0, "clickedContact"

    iget-object v1, p0, Lcom/chase/sig/android/activity/TeamInfoActivity;->n:Lcom/chase/sig/android/domain/Advisor;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 92
    const-string v0, "companyName"

    iget-object v1, p0, Lcom/chase/sig/android/activity/TeamInfoActivity;->o:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 93
    return-void
.end method
