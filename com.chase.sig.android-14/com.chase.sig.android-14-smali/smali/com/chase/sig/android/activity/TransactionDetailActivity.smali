.class public Lcom/chase/sig/android/activity/TransactionDetailActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/TransactionDetailActivity$a;
    }
.end annotation


# instance fields
.field private a:Lcom/chase/sig/android/domain/AccountActivity;

.field private b:Ljava/lang/String;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/Receipt;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    .line 147
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/TransactionDetailActivity;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 41
    iget-object v0, p0, Lcom/chase/sig/android/activity/TransactionDetailActivity;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/TransactionDetailActivity;Lcom/chase/sig/android/domain/Receipt;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 41
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/chase/sig/android/activity/ReceiptDetailActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "receipt"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    invoke-direct {p0}, Lcom/chase/sig/android/activity/TransactionDetailActivity;->d()V

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/TransactionDetailActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/TransactionDetailActivity;Ljava/lang/String;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/chase/sig/android/activity/TransactionDetailActivity;->d()V

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "selectedAccountId"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v1, 0x4000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/TransactionDetailActivity;->startActivity(Landroid/content/Intent;)V

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/chase/sig/android/activity/TransactionDetailActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "receipt_cancel_warning"

    const v2, 0x7f070278

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "account_nickname_mask"

    iget-object v2, p0, Lcom/chase/sig/android/activity/TransactionDetailActivity;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/chase/sig/android/c;->a(Landroid/content/Intent;)V

    return-void
.end method

.method private d()V
    .locals 3

    .prologue
    .line 238
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/TransactionDetailActivity;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/o;->b()Lcom/chase/sig/android/domain/CacheActivityData;

    move-result-object v0

    .line 240
    const-string v1, "transaction_details"

    iget-object v2, p0, Lcom/chase/sig/android/activity/TransactionDetailActivity;->a:Lcom/chase/sig/android/domain/AccountActivity;

    invoke-virtual {v0, v1, v2}, Lcom/chase/sig/android/domain/CacheActivityData;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 241
    const-string v1, "selectedAccountId"

    iget-object v2, p0, Lcom/chase/sig/android/activity/TransactionDetailActivity;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/chase/sig/android/domain/CacheActivityData;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 242
    const-string v1, "account_nickname_mask"

    iget-object v2, p0, Lcom/chase/sig/android/activity/TransactionDetailActivity;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/chase/sig/android/domain/CacheActivityData;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 243
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 9
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 58
    const v0, 0x7f0700f2

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/TransactionDetailActivity;->setTitle(I)V

    .line 59
    const v0, 0x7f0300c5

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/TransactionDetailActivity;->b(I)V

    .line 61
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/TransactionDetailActivity;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/o;->b()Lcom/chase/sig/android/domain/CacheActivityData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/CacheActivityData;->b()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/TransactionDetailActivity;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/o;->b()Lcom/chase/sig/android/domain/CacheActivityData;

    move-result-object v0

    const-string v1, "transaction_details"

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/CacheActivityData;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/AccountActivity;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/TransactionDetailActivity;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/o;->b()Lcom/chase/sig/android/domain/CacheActivityData;

    move-result-object v1

    const-string v2, "selectedAccountId"

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/domain/CacheActivityData;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/TransactionDetailActivity;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v2

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/o;->b()Lcom/chase/sig/android/domain/CacheActivityData;

    move-result-object v2

    const-string v4, "account_nickname_mask"

    invoke-virtual {v2, v4}, Lcom/chase/sig/android/domain/CacheActivityData;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/TransactionDetailActivity;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v4

    invoke-virtual {v4}, Lcom/chase/sig/android/domain/o;->b()Lcom/chase/sig/android/domain/CacheActivityData;

    move-result-object v4

    invoke-virtual {v4}, Lcom/chase/sig/android/domain/CacheActivityData;->a()V

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/TransactionDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "selectedAccountId"

    invoke-virtual {v4, v5, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/TransactionDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v4, "transaction_details"

    invoke-virtual {v1, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/TransactionDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account_nickname_mask"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 63
    :cond_0
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/TransactionDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "transaction_details"

    invoke-static {p1, v0, v1}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/AccountActivity;

    iput-object v0, p0, Lcom/chase/sig/android/activity/TransactionDetailActivity;->a:Lcom/chase/sig/android/domain/AccountActivity;

    .line 67
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/TransactionDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "selectedAccountId"

    invoke-static {p1, v0, v1}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/chase/sig/android/activity/TransactionDetailActivity;->b:Ljava/lang/String;

    .line 71
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/TransactionDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account_nickname_mask"

    invoke-static {p1, v0, v1}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/chase/sig/android/activity/TransactionDetailActivity;->d:Ljava/lang/String;

    .line 75
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/TransactionDetailActivity;->x()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/ChaseApplication;

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->t()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 76
    const v0, 0x7f090043

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/TransactionDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 77
    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 78
    new-instance v1, Lcom/chase/sig/android/activity/mh;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/mh;-><init>(Lcom/chase/sig/android/activity/TransactionDetailActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 86
    const v0, 0x7f090013

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/TransactionDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/SeparatorView;

    .line 87
    invoke-virtual {v0, v3}, Lcom/chase/sig/android/view/SeparatorView;->setVisibility(I)V

    .line 90
    :cond_1
    const v0, 0x7f0902cc

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/TransactionDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 91
    new-instance v1, Lcom/chase/sig/android/activity/mi;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/mi;-><init>(Lcom/chase/sig/android/activity/TransactionDetailActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 105
    iget-object v1, p0, Lcom/chase/sig/android/activity/TransactionDetailActivity;->a:Lcom/chase/sig/android/domain/AccountActivity;

    const v0, 0x7f0902cf

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/TransactionDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailView;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    new-instance v2, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Description"

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/AccountActivity;->a()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v2, v5, v6}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/AccountActivity;->b()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/AccountActivity;->b()Ljava/util/ArrayList;

    move-result-object v6

    move v2, v3

    :goto_0
    if-ge v2, v5, :cond_2

    new-instance v7, Lcom/chase/sig/android/view/detail/DetailRow;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/domain/ActivityValue;

    iget-object v8, v1, Lcom/chase/sig/android/domain/ActivityValue;->label:Ljava/lang/String;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/domain/ActivityValue;

    iget-object v1, v1, Lcom/chase/sig/android/domain/ActivityValue;->value:Ljava/lang/String;

    invoke-direct {v7, v8, v1}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    :cond_2
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Lcom/chase/sig/android/view/detail/a;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/chase/sig/android/view/detail/a;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/detail/DetailView;->setRows([Lcom/chase/sig/android/view/detail/a;)V

    .line 107
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/TransactionDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "receipts"

    invoke-static {p1, v0, v1}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/chase/sig/android/activity/TransactionDetailActivity;->c:Ljava/util/List;

    .line 111
    iget-object v0, p0, Lcom/chase/sig/android/activity/TransactionDetailActivity;->c:Ljava/util/List;

    if-nez v0, :cond_3

    .line 112
    const-class v0, Lcom/chase/sig/android/activity/TransactionDetailActivity$a;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    iget-object v2, p0, Lcom/chase/sig/android/activity/TransactionDetailActivity;->a:Lcom/chase/sig/android/domain/AccountActivity;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/AccountActivity;->c()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/chase/sig/android/activity/TransactionDetailActivity;->b:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/TransactionDetailActivity;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 117
    :goto_1
    const v0, 0x7f090004

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/TransactionDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/chase/sig/android/activity/TransactionDetailActivity;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 118
    return-void

    .line 114
    :cond_3
    iget-object v0, p0, Lcom/chase/sig/android/activity/TransactionDetailActivity;->c:Ljava/util/List;

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/TransactionDetailActivity;->a(Ljava/util/List;)V

    goto :goto_1
.end method

.method public final a(Ljava/util/List;)V
    .locals 8
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/Receipt;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 170
    iput-object p1, p0, Lcom/chase/sig/android/activity/TransactionDetailActivity;->c:Ljava/util/List;

    .line 172
    const v0, 0x7f0902ce

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/TransactionDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailView;

    .line 174
    iget-object v1, p0, Lcom/chase/sig/android/activity/TransactionDetailActivity;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 175
    iget-object v1, p0, Lcom/chase/sig/android/activity/TransactionDetailActivity;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    new-array v3, v1, [Lcom/chase/sig/android/view/detail/a;

    .line 177
    const v1, 0x7f0902cd

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/TransactionDetailActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 178
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 180
    :goto_0
    array-length v1, v3

    if-ge v2, v1, :cond_0

    .line 181
    iget-object v1, p0, Lcom/chase/sig/android/activity/TransactionDetailActivity;->c:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/domain/Receipt;

    .line 184
    :try_start_0
    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRowWithSubItem;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/Receipt;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/Receipt;->k()Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v6

    invoke-virtual {v6}, Lcom/chase/sig/android/domain/LabeledValue;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/Receipt;->b()Lcom/chase/sig/android/util/Dollar;

    move-result-object v7

    invoke-virtual {v7}, Lcom/chase/sig/android/util/Dollar;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v4, v5, v6, v7}, Lcom/chase/sig/android/view/detail/DetailRowWithSubItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/chase/sig/android/view/detail/DetailRowWithSubItem;->d()Lcom/chase/sig/android/view/detail/a;

    move-result-object v4

    aput-object v4, v3, v2

    .line 188
    aget-object v4, v3, v2

    new-instance v5, Lcom/chase/sig/android/activity/mj;

    invoke-direct {v5, p0, v1}, Lcom/chase/sig/android/activity/mj;-><init>(Lcom/chase/sig/android/activity/TransactionDetailActivity;Lcom/chase/sig/android/domain/Receipt;)V

    iput-object v5, v4, Lcom/chase/sig/android/view/detail/a;->h:Landroid/view/View$OnClickListener;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 190
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 195
    :cond_0
    invoke-virtual {v0, v3}, Lcom/chase/sig/android/view/detail/DetailView;->setRows([Lcom/chase/sig/android/view/detail/a;)V

    .line 196
    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/DetailView;->c()V

    .line 199
    :cond_1
    return-void

    :catch_0
    move-exception v1

    goto :goto_1
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 7
    .parameter
    .parameter
    .parameter

    .prologue
    .line 271
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 275
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 276
    if-eqz p3, :cond_0

    .line 278
    invoke-direct {p0}, Lcom/chase/sig/android/activity/TransactionDetailActivity;->d()V

    .line 281
    new-instance v1, Landroid/content/Intent;

    const-class v0, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 284
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/TransactionDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/domain/ReceiptPhotoList;->a(Landroid/content/Intent;)Lcom/chase/sig/android/domain/ReceiptPhotoList;

    move-result-object v2

    .line 287
    const-string v0, "image_uris"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 292
    if-eqz v0, :cond_1

    .line 293
    :try_start_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 294
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/TransactionDetailActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-static {v4, v0}, Landroid/provider/MediaStore$Images$Media;->getBitmap(Landroid/content/ContentResolver;Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 295
    new-instance v4, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v4}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 296
    sget-object v5, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v6, 0x1e

    invoke-virtual {v0, v5, v6, v4}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 298
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 299
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 300
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 301
    invoke-virtual {v2, v0}, Lcom/chase/sig/android/domain/ReceiptPhotoList;->a([B)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 307
    :catch_0
    move-exception v0

    .line 309
    :cond_0
    :goto_1
    invoke-super {p0, p1, p2, p3}, Lcom/chase/sig/android/activity/ai;->onActivityResult(IILandroid/content/Intent;)V

    .line 316
    return-void

    .line 304
    :cond_1
    :try_start_1
    const-string v0, "receipt_photo_list"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 305
    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/TransactionDetailActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 121
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ai;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 123
    const-string v1, "receipts"

    iget-object v0, p0, Lcom/chase/sig/android/activity/TransactionDetailActivity;->c:Ljava/util/List;

    check-cast v0, Ljava/io/Serializable;

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 124
    return-void
.end method
