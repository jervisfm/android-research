.class public Lcom/chase/sig/android/activity/TransferAuthorizeActivity$a;
.super Lcom/chase/sig/android/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/TransferAuthorizeActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/d",
        "<",
        "Lcom/chase/sig/android/activity/TransferAuthorizeActivity;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/service/movemoney/ServiceResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/chase/sig/android/d;-><init>()V

    return-void
.end method

.method private varargs a()Lcom/chase/sig/android/service/movemoney/ServiceResponse;
    .locals 3

    .prologue
    .line 58
    :try_start_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/TransferAuthorizeActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/TransferAuthorizeActivity;->A()Lcom/chase/sig/android/service/n;

    invoke-static {}, Lcom/chase/sig/android/service/n;->e()Lcom/chase/sig/android/service/transfer/a;

    move-result-object v1

    .line 59
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/TransferAuthorizeActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/TransferAuthorizeActivity;->a(Lcom/chase/sig/android/activity/TransferAuthorizeActivity;)Lcom/chase/sig/android/domain/AccountTransfer;

    move-result-object v0

    sget-object v2, Lcom/chase/sig/android/service/movemoney/RequestFlags;->e:Lcom/chase/sig/android/service/movemoney/RequestFlags;

    invoke-virtual {v1, v0, v2}, Lcom/chase/sig/android/service/transfer/a;->b(Lcom/chase/sig/android/domain/Transaction;Lcom/chase/sig/android/service/movemoney/RequestFlags;)Lcom/chase/sig/android/service/movemoney/ServiceResponse;

    move-result-object v1

    .line 60
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/TransferAuthorizeActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/TransferAuthorizeActivity;->l()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    .line 64
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected final bridge synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/chase/sig/android/activity/TransferAuthorizeActivity$a;->a()Lcom/chase/sig/android/service/movemoney/ServiceResponse;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 3
    .parameter

    .prologue
    .line 51
    check-cast p1, Lcom/chase/sig/android/service/movemoney/ServiceResponse;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/movemoney/ServiceResponse;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/TransferAuthorizeActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/movemoney/ServiceResponse;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/TransferAuthorizeActivity;->c(Ljava/util/List;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/TransferAuthorizeActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/TransferAuthorizeActivity;->a(Lcom/chase/sig/android/activity/TransferAuthorizeActivity;)Lcom/chase/sig/android/domain/AccountTransfer;

    move-result-object v0

    invoke-virtual {p1}, Lcom/chase/sig/android/service/movemoney/ServiceResponse;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/AccountTransfer;->k(Ljava/lang/String;)V

    new-instance v1, Landroid/content/Intent;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/TransferAuthorizeActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/TransferAuthorizeActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    const-class v2, Lcom/chase/sig/android/activity/TransferCompleteActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "transaction_object"

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/TransferAuthorizeActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/TransferAuthorizeActivity;->a(Lcom/chase/sig/android/activity/TransferAuthorizeActivity;)Lcom/chase/sig/android/domain/AccountTransfer;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v2, "queued_errors"

    invoke-virtual {p1}, Lcom/chase/sig/android/service/movemoney/ServiceResponse;->g()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/io/Serializable;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/TransferAuthorizeActivity;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/TransferAuthorizeActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
