.class public Lcom/chase/sig/android/activity/TransferAuthorizeActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$e;
.implements Lcom/chase/sig/android/c$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/TransferAuthorizeActivity$a;
    }
.end annotation


# instance fields
.field private a:Lcom/chase/sig/android/domain/AccountTransfer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    .line 51
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/TransferAuthorizeActivity;)Lcom/chase/sig/android/domain/AccountTransfer;
    .locals 1
    .parameter

    .prologue
    .line 18
    iget-object v0, p0, Lcom/chase/sig/android/activity/TransferAuthorizeActivity;->a:Lcom/chase/sig/android/domain/AccountTransfer;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 45
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 46
    const/high16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 47
    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/TransferAuthorizeActivity;->startActivity(Landroid/content/Intent;)V

    .line 48
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 24
    const v0, 0x7f030060

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/TransferAuthorizeActivity;->b(I)V

    .line 25
    const v0, 0x7f07014c

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/TransferAuthorizeActivity;->setTitle(I)V

    .line 27
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/TransferAuthorizeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "transaction_object"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/AccountTransfer;

    iput-object v0, p0, Lcom/chase/sig/android/activity/TransferAuthorizeActivity;->a:Lcom/chase/sig/android/domain/AccountTransfer;

    .line 29
    const v0, 0x7f090158

    new-instance v1, Lcom/chase/sig/android/activity/mk;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/mk;-><init>(Lcom/chase/sig/android/activity/TransferAuthorizeActivity;)V

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/TransferAuthorizeActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 40
    const v0, 0x7f090159

    invoke-virtual {p0, p0}, Lcom/chase/sig/android/activity/TransferAuthorizeActivity;->a(Lcom/chase/sig/android/c$a;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/TransferAuthorizeActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 41
    return-void
.end method
