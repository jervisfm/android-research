.class public Lcom/chase/sig/android/activity/TransferCompleteActivity;
.super Lcom/chase/sig/android/activity/m;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/TransferCompleteActivity$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/activity/m",
        "<",
        "Lcom/chase/sig/android/domain/AccountTransfer;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/chase/sig/android/activity/m;-><init>()V

    .line 86
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1
    .parameter

    .prologue
    .line 20
    const v0, 0x7f03005e

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/TransferCompleteActivity;->b(I)V

    .line 21
    const v0, 0x7f070074

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/TransferCompleteActivity;->setTitle(I)V

    .line 23
    invoke-static {}, Lcom/chase/sig/android/c;->a()V

    .line 24
    const-class v0, Lcom/chase/sig/android/activity/TransferCompleteActivity$a;

    invoke-virtual {p0, p1, v0}, Lcom/chase/sig/android/activity/TransferCompleteActivity;->a(Landroid/os/Bundle;Ljava/lang/Class;)V

    .line 25
    return-void
.end method

.method protected final a(Z)V
    .locals 9
    .parameter

    .prologue
    const/4 v8, 0x1

    .line 29
    iget-object v0, p0, Lcom/chase/sig/android/activity/TransferCompleteActivity;->a:Lcom/chase/sig/android/domain/Transaction;

    check-cast v0, Lcom/chase/sig/android/domain/AccountTransfer;

    .line 30
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/TransferCompleteActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "hideFrequency"

    invoke-virtual {v1, v2, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 31
    const v1, 0x7f090153

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/TransferCompleteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/view/detail/DetailView;

    .line 32
    const/16 v3, 0xa

    new-array v3, v3, [Lcom/chase/sig/android/view/detail/a;

    const/4 v4, 0x0

    new-instance v5, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v6, "Transaction Number"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/AccountTransfer;->n()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v3, v4

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "From"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/AccountTransfer;->r()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/AccountTransfer;->s()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v3, v8

    const/4 v4, 0x2

    new-instance v5, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v6, "To"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/AccountTransfer;->d_()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/AccountTransfer;->A()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v3, v4

    const/4 v4, 0x3

    new-instance v5, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v6, "Amount"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/AccountTransfer;->e()Lcom/chase/sig/android/util/Dollar;

    move-result-object v7

    invoke-virtual {v7}, Lcom/chase/sig/android/util/Dollar;->h()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v3, v4

    const/4 v4, 0x4

    new-instance v5, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v6, "Send On"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/AccountTransfer;->b()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/chase/sig/android/util/s;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v3, v4

    const/4 v4, 0x5

    new-instance v5, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v6, "Deliver By"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/AccountTransfer;->q()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/chase/sig/android/util/s;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v3, v4

    const/4 v4, 0x6

    new-instance v5, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v6, "Frequency"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/AccountTransfer;->i()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/chase/sig/android/activity/TransferCompleteActivity;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-boolean v2, v5, Lcom/chase/sig/android/view/detail/a;->j:Z

    aput-object v5, v3, v4

    const/4 v4, 0x7

    new-instance v5, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v6, "Remaining Transfers"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/AccountTransfer;->w()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-boolean v2, v5, Lcom/chase/sig/android/view/detail/a;->j:Z

    aput-object v5, v3, v4

    const/16 v2, 0x8

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Status"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/AccountTransfer;->t()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v3, v2

    const/16 v2, 0x9

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Memo"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/AccountTransfer;->o()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v5, v0}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v3, v2

    invoke-virtual {v1, v3}, Lcom/chase/sig/android/view/detail/DetailView;->setRows([Lcom/chase/sig/android/view/detail/a;)V

    .line 46
    const v0, 0x7f07013e

    const-class v1, Lcom/chase/sig/android/activity/TransferHistoryActivity;

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/TransferCompleteActivity;->a(ILjava/lang/Class;)V

    .line 47
    const v0, 0x7f07013c

    const-class v1, Lcom/chase/sig/android/activity/TransferStartActivity;

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/TransferCompleteActivity;->b(ILjava/lang/Class;)V

    .line 49
    invoke-virtual {p0, p1}, Lcom/chase/sig/android/activity/TransferCompleteActivity;->b(Z)V

    .line 50
    return-void
.end method

.method protected final b()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/chase/sig/android/activity/eb;",
            ">;"
        }
    .end annotation

    .prologue
    .line 83
    const-class v0, Lcom/chase/sig/android/activity/TransferHomeActivity;

    return-object v0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 11
    .parameter

    .prologue
    const v10, 0x7f070068

    const/4 v9, -0x1

    const/4 v8, 0x1

    .line 54
    new-instance v1, Lcom/chase/sig/android/view/k$a;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/view/k$a;-><init>(Landroid/content/Context;)V

    .line 55
    packed-switch p1, :pswitch_data_0

    .line 76
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/m;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    .line 57
    :pswitch_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/TransferCompleteActivity;->a:Lcom/chase/sig/android/domain/Transaction;

    check-cast v0, Lcom/chase/sig/android/domain/AccountTransfer;

    .line 58
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/TransferCompleteActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f070148

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/AccountTransfer;->i()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/chase/sig/android/activity/TransferCompleteActivity;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/AccountTransfer;->r()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/AccountTransfer;->s()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    const/4 v5, 0x2

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/AccountTransfer;->d_()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/AccountTransfer;->A()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x3

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/AccountTransfer;->d_()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/AccountTransfer;->A()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-static {v2, v3, v4}, Lcom/chase/sig/android/util/s;->a(Landroid/content/Context;I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 64
    iput-boolean v8, v1, Lcom/chase/sig/android/view/k$a;->i:Z

    new-instance v2, Lcom/chase/sig/android/activity/a/b;

    invoke-direct {v2}, Lcom/chase/sig/android/activity/a/b;-><init>()V

    invoke-virtual {v1, v10, v2}, Lcom/chase/sig/android/view/k$a;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    move-result-object v2

    const v3, 0x7f070147

    invoke-virtual {v2, v3}, Lcom/chase/sig/android/view/k$a;->c(I)Lcom/chase/sig/android/view/k$a;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/chase/sig/android/view/k$a;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/k$a;

    .line 68
    invoke-virtual {v1, v9}, Lcom/chase/sig/android/view/k$a;->d(I)Lcom/chase/sig/android/view/k;

    move-result-object v0

    goto/16 :goto_0

    .line 70
    :pswitch_1
    iput-boolean v8, v1, Lcom/chase/sig/android/view/k$a;->i:Z

    new-instance v0, Lcom/chase/sig/android/activity/a/b;

    invoke-direct {v0}, Lcom/chase/sig/android/activity/a/b;-><init>()V

    invoke-virtual {v1, v10, v0}, Lcom/chase/sig/android/view/k$a;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    move-result-object v0

    const v2, 0x7f070146

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/view/k$a;->b(I)Lcom/chase/sig/android/view/k$a;

    .line 74
    invoke-virtual {v1, v9}, Lcom/chase/sig/android/view/k$a;->d(I)Lcom/chase/sig/android/view/k;

    move-result-object v0

    goto/16 :goto_0

    .line 55
    :pswitch_data_0
    .packed-switch 0xc
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
