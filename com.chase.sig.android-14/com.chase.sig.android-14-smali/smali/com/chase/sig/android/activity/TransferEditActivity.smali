.class public Lcom/chase/sig/android/activity/TransferEditActivity;
.super Lcom/chase/sig/android/activity/h;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/ma;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/TransferEditActivity$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/activity/h",
        "<",
        "Lcom/chase/sig/android/domain/AccountTransfer;",
        ">;",
        "Lcom/chase/sig/android/activity/ma;"
    }
.end annotation


# instance fields
.field private b:Lcom/chase/sig/android/domain/AccountTransfer;

.field private c:Lcom/chase/sig/android/view/detail/d;

.field private d:Lcom/chase/sig/android/view/detail/c;

.field private k:Lcom/chase/sig/android/view/detail/l;

.field private l:Lcom/chase/sig/android/view/detail/o;

.field private m:Lcom/chase/sig/android/view/detail/DetailView;

.field private n:Lcom/chase/sig/android/view/detail/j;

.field private o:Landroid/widget/CheckBox;

.field private p:Landroid/widget/EditText;

.field private q:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 41
    invoke-direct {p0}, Lcom/chase/sig/android/activity/h;-><init>()V

    .line 46
    iput-object v0, p0, Lcom/chase/sig/android/activity/TransferEditActivity;->b:Lcom/chase/sig/android/domain/AccountTransfer;

    .line 47
    iput-object v0, p0, Lcom/chase/sig/android/activity/TransferEditActivity;->c:Lcom/chase/sig/android/view/detail/d;

    .line 48
    iput-object v0, p0, Lcom/chase/sig/android/activity/TransferEditActivity;->d:Lcom/chase/sig/android/view/detail/c;

    .line 49
    iput-object v0, p0, Lcom/chase/sig/android/activity/TransferEditActivity;->k:Lcom/chase/sig/android/view/detail/l;

    .line 50
    iput-object v0, p0, Lcom/chase/sig/android/activity/TransferEditActivity;->l:Lcom/chase/sig/android/view/detail/o;

    .line 51
    iput-object v0, p0, Lcom/chase/sig/android/activity/TransferEditActivity;->m:Lcom/chase/sig/android/view/detail/DetailView;

    .line 52
    iput-object v0, p0, Lcom/chase/sig/android/activity/TransferEditActivity;->n:Lcom/chase/sig/android/view/detail/j;

    .line 53
    iput-object v0, p0, Lcom/chase/sig/android/activity/TransferEditActivity;->o:Landroid/widget/CheckBox;

    .line 54
    iput-object v0, p0, Lcom/chase/sig/android/activity/TransferEditActivity;->p:Landroid/widget/EditText;

    .line 116
    return-void
.end method

.method private D()I
    .locals 1

    .prologue
    .line 102
    :try_start_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/TransferEditActivity;->p:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 104
    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 6
    .parameter

    .prologue
    const/4 v5, 0x0

    .line 59
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/h;->a(Landroid/os/Bundle;)V

    .line 60
    const v0, 0x7f070151

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/TransferEditActivity;->setTitle(I)V

    .line 62
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/TransferEditActivity;->d()Lcom/chase/sig/android/domain/Transaction;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/AccountTransfer;

    iput-object v0, p0, Lcom/chase/sig/android/activity/TransferEditActivity;->b:Lcom/chase/sig/android/domain/AccountTransfer;

    .line 63
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/TransferEditActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "edit_single"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/chase/sig/android/activity/TransferEditActivity;->q:Z

    .line 64
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/TransferEditActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/TransferEditActivity;->m:Lcom/chase/sig/android/view/detail/DetailView;

    .line 66
    new-instance v2, Lcom/chase/sig/android/view/detail/DetailRow;

    const v0, 0x7f070076

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/TransferEditActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/chase/sig/android/activity/TransferEditActivity;->b:Lcom/chase/sig/android/domain/AccountTransfer;

    invoke-virtual {v3}, Lcom/chase/sig/android/domain/AccountTransfer;->r()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/chase/sig/android/activity/TransferEditActivity;->b:Lcom/chase/sig/android/domain/AccountTransfer;

    invoke-virtual {v3}, Lcom/chase/sig/android/domain/AccountTransfer;->s()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v0, v1}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    new-instance v3, Lcom/chase/sig/android/view/detail/DetailRow;

    const v0, 0x7f070075

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/TransferEditActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/chase/sig/android/activity/TransferEditActivity;->b:Lcom/chase/sig/android/domain/AccountTransfer;

    invoke-virtual {v4}, Lcom/chase/sig/android/domain/AccountTransfer;->d_()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, p0, Lcom/chase/sig/android/activity/TransferEditActivity;->b:Lcom/chase/sig/android/domain/AccountTransfer;

    invoke-virtual {v4}, Lcom/chase/sig/android/domain/AccountTransfer;->A()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v0, v1}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    new-instance v0, Lcom/chase/sig/android/view/detail/d;

    const v1, 0x7f07007a

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/TransferEditActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, Lcom/chase/sig/android/activity/TransferEditActivity;->b:Lcom/chase/sig/android/domain/AccountTransfer;

    invoke-virtual {v4}, Lcom/chase/sig/android/domain/AccountTransfer;->q()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/chase/sig/android/util/s;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v1, v4}, Lcom/chase/sig/android/view/detail/d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "DATE"

    iput-object v1, v0, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v0, Lcom/chase/sig/android/view/detail/d;

    invoke-virtual {p0, v5}, Lcom/chase/sig/android/activity/TransferEditActivity;->i(I)Landroid/view/View$OnClickListener;

    move-result-object v1

    iput-object v1, v0, Lcom/chase/sig/android/view/detail/d;->a:Landroid/view/View$OnClickListener;

    iput-object v0, p0, Lcom/chase/sig/android/activity/TransferEditActivity;->c:Lcom/chase/sig/android/view/detail/d;

    .line 77
    iget-object v0, p0, Lcom/chase/sig/android/activity/TransferEditActivity;->b:Lcom/chase/sig/android/domain/AccountTransfer;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/AccountTransfer;->l()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/chase/sig/android/activity/TransferEditActivity;->q:Z

    if-nez v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/chase/sig/android/activity/TransferEditActivity;->b:Lcom/chase/sig/android/domain/AccountTransfer;

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/TransferEditActivity;->a(Lcom/chase/sig/android/domain/Transaction;)Lcom/chase/sig/android/view/detail/o;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/TransferEditActivity;->l:Lcom/chase/sig/android/view/detail/o;

    .line 79
    new-instance v0, Lcom/chase/sig/android/view/detail/r;

    const v1, 0x7f07007e

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/TransferEditActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, Lcom/chase/sig/android/activity/TransferEditActivity;->b:Lcom/chase/sig/android/domain/AccountTransfer;

    invoke-virtual {v4}, Lcom/chase/sig/android/domain/AccountTransfer;->x()Lcom/chase/sig/android/view/f;

    move-result-object v4

    invoke-direct {v0, v1, v4}, Lcom/chase/sig/android/view/detail/r;-><init>(Ljava/lang/String;Lcom/chase/sig/android/view/f;)V

    const-string v1, "REMAINING"

    iput-object v1, v0, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v0, Lcom/chase/sig/android/view/detail/j;

    iput-object v0, p0, Lcom/chase/sig/android/activity/TransferEditActivity;->n:Lcom/chase/sig/android/view/detail/j;

    .line 84
    :cond_0
    new-instance v0, Lcom/chase/sig/android/view/detail/c;

    const v1, 0x7f07007c

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/TransferEditActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v4, p0, Lcom/chase/sig/android/activity/TransferEditActivity;->b:Lcom/chase/sig/android/domain/AccountTransfer;

    invoke-virtual {v4}, Lcom/chase/sig/android/domain/AccountTransfer;->e()Lcom/chase/sig/android/util/Dollar;

    move-result-object v4

    invoke-direct {v0, v1, v4}, Lcom/chase/sig/android/view/detail/c;-><init>(Ljava/lang/String;Lcom/chase/sig/android/util/Dollar;)V

    const-string v1, "AMOUNT"

    iput-object v1, v0, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v0, Lcom/chase/sig/android/view/detail/c;

    iput-object v0, p0, Lcom/chase/sig/android/activity/TransferEditActivity;->d:Lcom/chase/sig/android/view/detail/c;

    .line 87
    new-instance v0, Lcom/chase/sig/android/view/detail/l;

    const v1, 0x7f07007b

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/TransferEditActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    iget-object v1, p0, Lcom/chase/sig/android/activity/TransferEditActivity;->b:Lcom/chase/sig/android/domain/AccountTransfer;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/AccountTransfer;->o()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/chase/sig/android/activity/TransferEditActivity;->b:Lcom/chase/sig/android/domain/AccountTransfer;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/AccountTransfer;->o()Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-direct {v0, v4, v1}, Lcom/chase/sig/android/view/detail/l;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "MEMO"

    iput-object v1, v0, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v0, Lcom/chase/sig/android/view/detail/l;

    const/16 v1, 0x20

    iput v1, v0, Lcom/chase/sig/android/view/detail/l;->a:I

    const v1, 0x7f07007f

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/TransferEditActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/detail/l;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/l;

    iput-object v0, p0, Lcom/chase/sig/android/activity/TransferEditActivity;->k:Lcom/chase/sig/android/view/detail/l;

    .line 92
    iget-object v0, p0, Lcom/chase/sig/android/activity/TransferEditActivity;->m:Lcom/chase/sig/android/view/detail/DetailView;

    const/4 v1, 0x7

    new-array v1, v1, [Lcom/chase/sig/android/view/detail/a;

    aput-object v2, v1, v5

    const/4 v2, 0x1

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/chase/sig/android/activity/TransferEditActivity;->d:Lcom/chase/sig/android/view/detail/c;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/chase/sig/android/activity/TransferEditActivity;->c:Lcom/chase/sig/android/view/detail/d;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget-object v3, p0, Lcom/chase/sig/android/activity/TransferEditActivity;->l:Lcom/chase/sig/android/view/detail/o;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    iget-object v3, p0, Lcom/chase/sig/android/activity/TransferEditActivity;->n:Lcom/chase/sig/android/view/detail/j;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    iget-object v3, p0, Lcom/chase/sig/android/activity/TransferEditActivity;->k:Lcom/chase/sig/android/view/detail/l;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/detail/DetailView;->setRows([Lcom/chase/sig/android/view/detail/a;)V

    .line 94
    iget-object v0, p0, Lcom/chase/sig/android/activity/TransferEditActivity;->b:Lcom/chase/sig/android/domain/AccountTransfer;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/AccountTransfer;->l()Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/chase/sig/android/activity/TransferEditActivity;->q:Z

    if-nez v0, :cond_1

    .line 95
    iget-object v0, p0, Lcom/chase/sig/android/activity/TransferEditActivity;->n:Lcom/chase/sig/android/view/detail/j;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/j;->k()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f09009f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/chase/sig/android/activity/TransferEditActivity;->o:Landroid/widget/CheckBox;

    .line 96
    iget-object v0, p0, Lcom/chase/sig/android/activity/TransferEditActivity;->n:Lcom/chase/sig/android/view/detail/j;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/j;->p()Landroid/widget/EditText;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/TransferEditActivity;->p:Landroid/widget/EditText;

    .line 98
    :cond_1
    return-void

    .line 87
    :cond_2
    const-string v1, ""

    goto :goto_0
.end method

.method protected final synthetic f()Lcom/chase/sig/android/domain/Transaction;
    .locals 4

    .prologue
    .line 41
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/TransferEditActivity;->d()Lcom/chase/sig/android/domain/Transaction;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/AccountTransfer;

    new-instance v1, Lcom/chase/sig/android/util/Dollar;

    iget-object v2, p0, Lcom/chase/sig/android/activity/TransferEditActivity;->m:Lcom/chase/sig/android/view/detail/DetailView;

    const-string v3, "AMOUNT"

    invoke-virtual {v2, v3}, Lcom/chase/sig/android/view/detail/DetailView;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/chase/sig/android/util/Dollar;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/AccountTransfer;->a(Lcom/chase/sig/android/util/Dollar;)V

    iget-object v1, p0, Lcom/chase/sig/android/activity/TransferEditActivity;->c:Lcom/chase/sig/android/view/detail/d;

    invoke-virtual {v1}, Lcom/chase/sig/android/view/detail/d;->p()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->t(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->b(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/AccountTransfer;->n(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/chase/sig/android/activity/TransferEditActivity;->m:Lcom/chase/sig/android/view/detail/DetailView;

    const-string v2, "MEMO"

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/view/detail/DetailView;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/chase/sig/android/activity/TransferEditActivity;->m:Lcom/chase/sig/android/view/detail/DetailView;

    const-string v2, "MEMO"

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/view/detail/DetailView;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/AccountTransfer;->l(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/chase/sig/android/activity/TransferEditActivity;->q:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/chase/sig/android/activity/TransferEditActivity;->o:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "true"

    :goto_1
    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/AccountTransfer;->s(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/chase/sig/android/activity/TransferEditActivity;->l:Lcom/chase/sig/android/view/detail/o;

    invoke-virtual {v1}, Lcom/chase/sig/android/view/detail/o;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/AccountTransfer;->g(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/chase/sig/android/activity/TransferEditActivity;->p:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/AccountTransfer;->r(Ljava/lang/String;)V

    :cond_0
    return-object v0

    :cond_1
    const-string v1, ""

    goto :goto_0

    :cond_2
    const-string v1, "false"

    goto :goto_1
.end method

.method protected final g()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 220
    const/4 v2, 0x1

    .line 221
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/TransferEditActivity;->y()V

    .line 223
    iget-object v0, p0, Lcom/chase/sig/android/activity/TransferEditActivity;->m:Lcom/chase/sig/android/view/detail/DetailView;

    const-string v3, "AMOUNT"

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/view/detail/DetailView;->e(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/c;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/c;->p()Lcom/chase/sig/android/view/AmountView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/view/AmountView;->a()Z

    move-result v0

    if-nez v0, :cond_3

    .line 225
    iget-object v0, p0, Lcom/chase/sig/android/activity/TransferEditActivity;->m:Lcom/chase/sig/android/view/detail/DetailView;

    const-string v2, "AMOUNT"

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/view/detail/DetailView;->a(Ljava/lang/String;)V

    move v0, v1

    .line 228
    :goto_0
    iget-object v2, p0, Lcom/chase/sig/android/activity/TransferEditActivity;->c:Lcom/chase/sig/android/view/detail/d;

    invoke-virtual {v2}, Lcom/chase/sig/android/view/detail/d;->p()Ljava/lang/String;

    move-result-object v2

    .line 229
    invoke-static {v2}, Lcom/chase/sig/android/util/s;->l(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 230
    iget-object v0, p0, Lcom/chase/sig/android/activity/TransferEditActivity;->m:Lcom/chase/sig/android/view/detail/DetailView;

    const-string v2, "DATE"

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/view/detail/DetailView;->a(Ljava/lang/String;)V

    move v0, v1

    .line 234
    :cond_0
    iget-object v2, p0, Lcom/chase/sig/android/activity/TransferEditActivity;->b:Lcom/chase/sig/android/domain/AccountTransfer;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/AccountTransfer;->l()Z

    move-result v2

    if-nez v2, :cond_2

    .line 235
    iget-object v2, p0, Lcom/chase/sig/android/activity/TransferEditActivity;->o:Landroid/widget/CheckBox;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/chase/sig/android/activity/TransferEditActivity;->o:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    if-nez v2, :cond_1

    .line 236
    invoke-direct {p0}, Lcom/chase/sig/android/activity/TransferEditActivity;->D()I

    move-result v2

    if-gtz v2, :cond_1

    .line 238
    iget-object v0, p0, Lcom/chase/sig/android/activity/TransferEditActivity;->m:Lcom/chase/sig/android/view/detail/DetailView;

    const-string v2, "REMAINING"

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/view/detail/DetailView;->a(Ljava/lang/String;)V

    move v0, v1

    .line 242
    :cond_1
    iget-object v2, p0, Lcom/chase/sig/android/activity/TransferEditActivity;->l:Lcom/chase/sig/android/view/detail/o;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/chase/sig/android/activity/TransferEditActivity;->l:Lcom/chase/sig/android/view/detail/o;

    invoke-virtual {v2}, Lcom/chase/sig/android/view/detail/o;->q()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v2

    invoke-virtual {v2}, Lcom/chase/sig/android/view/JPSpinner;->getSelectedItemPosition()I

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_2

    .line 245
    iget-object v0, p0, Lcom/chase/sig/android/activity/TransferEditActivity;->m:Lcom/chase/sig/android/view/detail/DetailView;

    const-string v2, "REMAINING"

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/view/detail/DetailView;->a(Ljava/lang/String;)V

    move v0, v1

    .line 249
    :cond_2
    return v0

    :cond_3
    move v0, v2

    goto :goto_0
.end method

.method protected final h()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/chase/sig/android/activity/TransferEditActivity$a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 215
    const-class v0, Lcom/chase/sig/android/activity/TransferEditActivity$a;

    return-object v0
.end method

.method protected final i()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/chase/sig/android/activity/eb;",
            ">;"
        }
    .end annotation

    .prologue
    .line 210
    const-class v0, Lcom/chase/sig/android/activity/TransferEditVerifyActivity;

    return-object v0
.end method

.method protected final j()Ljava/util/Date;
    .locals 3

    .prologue
    .line 202
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V

    .line 203
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/ChaseApplication;

    const v2, 0x7f070159

    invoke-virtual {p0, v2}, Lcom/chase/sig/android/activity/TransferEditActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/ChaseApplication;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, v1, Landroid/text/format/Time;->hour:I

    .line 205
    iget v0, v1, Landroid/text/format/Time;->hour:I

    invoke-static {v0}, Lcom/chase/sig/android/util/l;->a(I)Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 110
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/h;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 113
    const-string v0, "transaction_object"

    iget-object v1, p0, Lcom/chase/sig/android/activity/TransferEditActivity;->b:Lcom/chase/sig/android/domain/AccountTransfer;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 114
    return-void
.end method
