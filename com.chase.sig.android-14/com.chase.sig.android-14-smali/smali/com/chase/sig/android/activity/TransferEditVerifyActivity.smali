.class public Lcom/chase/sig/android/activity/TransferEditVerifyActivity;
.super Lcom/chase/sig/android/activity/k;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/TransferEditVerifyActivity$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/activity/k",
        "<",
        "Lcom/chase/sig/android/domain/AccountTransfer;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/chase/sig/android/activity/k;-><init>()V

    .line 77
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 20
    const v0, 0x7f0300c7

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/TransferEditVerifyActivity;->b(I)V

    .line 21
    const v0, 0x7f070154

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/TransferEditVerifyActivity;->setTitle(I)V

    .line 23
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/TransferEditVerifyActivity;->h()V

    .line 24
    const v0, 0x7f0902d1

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/TransferEditVerifyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const v1, 0x7f070158

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 26
    const v0, 0x7f0902d2

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/TransferEditVerifyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const v1, 0x7f070157

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 28
    return-void
.end method

.method protected final a(Z)V
    .locals 9
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 32
    const v0, 0x7f0902d0

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/TransferEditVerifyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailView;

    .line 34
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/TransferEditVerifyActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "request_flags"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/service/movemoney/RequestFlags;

    .line 37
    iget-object v2, p0, Lcom/chase/sig/android/activity/TransferEditVerifyActivity;->a:Lcom/chase/sig/android/domain/Transaction;

    check-cast v2, Lcom/chase/sig/android/domain/AccountTransfer;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/AccountTransfer;->l()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Lcom/chase/sig/android/service/movemoney/RequestFlags;->b()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    move v2, v4

    .line 38
    :goto_0
    const/16 v1, 0x8

    new-array v5, v1, [Lcom/chase/sig/android/view/detail/a;

    new-instance v6, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v7, "From"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/chase/sig/android/activity/TransferEditVerifyActivity;->a:Lcom/chase/sig/android/domain/Transaction;

    check-cast v1, Lcom/chase/sig/android/domain/AccountTransfer;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/AccountTransfer;->r()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v1, p0, Lcom/chase/sig/android/activity/TransferEditVerifyActivity;->a:Lcom/chase/sig/android/domain/Transaction;

    check-cast v1, Lcom/chase/sig/android/domain/AccountTransfer;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/AccountTransfer;->s()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v6, v7, v1}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v6, v5, v3

    new-instance v3, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v6, "To"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/chase/sig/android/activity/TransferEditVerifyActivity;->a:Lcom/chase/sig/android/domain/Transaction;

    check-cast v1, Lcom/chase/sig/android/domain/AccountTransfer;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/AccountTransfer;->d_()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v1, p0, Lcom/chase/sig/android/activity/TransferEditVerifyActivity;->a:Lcom/chase/sig/android/domain/Transaction;

    check-cast v1, Lcom/chase/sig/android/domain/AccountTransfer;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/AccountTransfer;->A()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v6, v1}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v5, v4

    const/4 v3, 0x2

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v6, "Amount"

    iget-object v1, p0, Lcom/chase/sig/android/activity/TransferEditVerifyActivity;->a:Lcom/chase/sig/android/domain/Transaction;

    check-cast v1, Lcom/chase/sig/android/domain/AccountTransfer;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/AccountTransfer;->e()Lcom/chase/sig/android/util/Dollar;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/util/Dollar;->h()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v6, v1}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v5, v3

    const/4 v3, 0x3

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v6, "Send On"

    iget-object v1, p0, Lcom/chase/sig/android/activity/TransferEditVerifyActivity;->a:Lcom/chase/sig/android/domain/Transaction;

    check-cast v1, Lcom/chase/sig/android/domain/AccountTransfer;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/AccountTransfer;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v6, v1}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v5, v3

    const/4 v3, 0x4

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v6, "Deliver By"

    iget-object v1, p0, Lcom/chase/sig/android/activity/TransferEditVerifyActivity;->a:Lcom/chase/sig/android/domain/Transaction;

    check-cast v1, Lcom/chase/sig/android/domain/AccountTransfer;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/AccountTransfer;->q()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v6, v1}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v5, v3

    const/4 v3, 0x5

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v6, "Frequency"

    iget-object v1, p0, Lcom/chase/sig/android/activity/TransferEditVerifyActivity;->a:Lcom/chase/sig/android/domain/Transaction;

    check-cast v1, Lcom/chase/sig/android/domain/AccountTransfer;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/AccountTransfer;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/TransferEditVerifyActivity;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v6, v1}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-boolean v2, v4, Lcom/chase/sig/android/view/detail/a;->j:Z

    aput-object v4, v5, v3

    const/4 v3, 0x6

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v6, "Remaining Transfers"

    iget-object v1, p0, Lcom/chase/sig/android/activity/TransferEditVerifyActivity;->a:Lcom/chase/sig/android/domain/Transaction;

    check-cast v1, Lcom/chase/sig/android/domain/AccountTransfer;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/AccountTransfer;->w()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v6, v1}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-boolean v2, v4, Lcom/chase/sig/android/view/detail/a;->j:Z

    aput-object v4, v5, v3

    const/4 v2, 0x7

    new-instance v3, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v4, "Memo"

    iget-object v1, p0, Lcom/chase/sig/android/activity/TransferEditVerifyActivity;->a:Lcom/chase/sig/android/domain/Transaction;

    check-cast v1, Lcom/chase/sig/android/domain/AccountTransfer;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/AccountTransfer;->o()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v4, v1}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v5, v2

    invoke-virtual {v0, v5}, Lcom/chase/sig/android/view/detail/DetailView;->setRows([Lcom/chase/sig/android/view/detail/a;)V

    .line 51
    const v0, 0x7f0902d1

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/TransferEditVerifyActivity;->B()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/TransferEditVerifyActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 53
    const v0, 0x7f0902d2

    new-instance v1, Lcom/chase/sig/android/activity/ml;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/ml;-><init>(Lcom/chase/sig/android/activity/TransferEditVerifyActivity;)V

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/TransferEditVerifyActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 59
    return-void

    :cond_1
    move v2, v3

    .line 37
    goto/16 :goto_0
.end method

.method protected final c()Lcom/chase/sig/android/service/movemoney/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/chase/sig/android/service/movemoney/d",
            "<",
            "Lcom/chase/sig/android/domain/AccountTransfer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 74
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/TransferEditVerifyActivity;->A()Lcom/chase/sig/android/service/n;

    invoke-static {}, Lcom/chase/sig/android/service/n;->e()Lcom/chase/sig/android/service/transfer/a;

    move-result-object v0

    return-object v0
.end method

.method protected final d()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/chase/sig/android/activity/TransferCompleteActivity;",
            ">;"
        }
    .end annotation

    .prologue
    .line 69
    const-class v0, Lcom/chase/sig/android/activity/TransferCompleteActivity;

    return-object v0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 63
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/k;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 64
    const-string v0, "transaction_object"

    iget-object v1, p0, Lcom/chase/sig/android/activity/TransferEditVerifyActivity;->a:Lcom/chase/sig/android/domain/Transaction;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 65
    return-void
.end method
