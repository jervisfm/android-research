.class public Lcom/chase/sig/android/activity/TransferStartActivity;
.super Lcom/chase/sig/android/activity/a;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/TransferStartActivity$a;
    }
.end annotation


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/IAccount;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/IAccount;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/chase/sig/android/domain/AccountTransfer;

.field private d:Lcom/chase/sig/android/view/b$a;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/chase/sig/android/activity/a;-><init>()V

    .line 261
    new-instance v0, Lcom/chase/sig/android/activity/mo;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/activity/mo;-><init>(Lcom/chase/sig/android/activity/TransferStartActivity;)V

    iput-object v0, p0, Lcom/chase/sig/android/activity/TransferStartActivity;->d:Lcom/chase/sig/android/view/b$a;

    .line 333
    return-void
.end method

.method private static a(Ljava/util/List;Ljava/lang/String;)I
    .locals 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/IAccount;",
            ">;",
            "Ljava/lang/String;",
            ")I"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 217
    move v1, v2

    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 218
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/IAccount;

    .line 219
    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 224
    :goto_1
    return v1

    .line 217
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    move v1, v2

    .line 224
    goto :goto_1
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/TransferStartActivity;Lcom/chase/sig/android/domain/AccountTransfer;)Lcom/chase/sig/android/domain/AccountTransfer;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 47
    iput-object p1, p0, Lcom/chase/sig/android/activity/TransferStartActivity;->c:Lcom/chase/sig/android/domain/AccountTransfer;

    return-object p1
.end method

.method static synthetic a(Ljava/util/List;Lcom/chase/sig/android/view/JPSpinner;)Ljava/lang/String;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 47
    invoke-virtual {p1}, Lcom/chase/sig/android/view/JPSpinner;->getSelectedItemPosition()I

    move-result v0

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/IAccount;

    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/os/Bundle;Ljava/lang/String;)Z
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 126
    if-eqz p0, :cond_0

    invoke-virtual {p0, p1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/TransferStartActivity;)Z
    .locals 5
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 47
    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/TransferStartActivity;->y()V

    const-string v0, "PAY_TO"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/TransferStartActivity;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/q;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/q;->q()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v3

    const-string v0, "PAY_FROM"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/TransferStartActivity;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/q;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/q;->q()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v4

    invoke-virtual {v4}, Lcom/chase/sig/android/view/JPSpinner;->b()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/TransferStartActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v0

    const-string v2, "PAY_FROM"

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/view/detail/DetailView;->a(Ljava/lang/String;)V

    move v0, v1

    :goto_0
    invoke-virtual {v3}, Lcom/chase/sig/android/view/JPSpinner;->b()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/TransferStartActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v0

    const-string v2, "PAY_TO"

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/view/detail/DetailView;->a(Ljava/lang/String;)V

    move v0, v1

    :cond_0
    invoke-virtual {v4}, Lcom/chase/sig/android/view/JPSpinner;->b()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v3}, Lcom/chase/sig/android/view/JPSpinner;->b()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v4}, Lcom/chase/sig/android/view/JPSpinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v3}, Lcom/chase/sig/android/view/JPSpinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/TransferStartActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v0

    const-string v2, "PAY_TO"

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/view/detail/DetailView;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/TransferStartActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v0

    const-string v2, "PAY_FROM"

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/view/detail/DetailView;->a(Ljava/lang/String;)V

    move v2, v1

    :goto_1
    const-string v0, "DATE"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/TransferStartActivity;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/d;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/d;->p()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->l(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/TransferStartActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v0

    const-string v2, "DATE"

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/view/detail/DetailView;->a(Ljava/lang/String;)V

    move v2, v1

    :cond_1
    const-string v0, "AMOUNT"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/TransferStartActivity;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/c;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/c;->n()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/TransferStartActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v0

    const-string v2, "AMOUNT"

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/view/detail/DetailView;->a(Ljava/lang/String;)V

    :goto_2
    return v1

    :cond_2
    move v1, v2

    goto :goto_2

    :cond_3
    move v2, v0

    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_0
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/TransferStartActivity;)Lcom/chase/sig/android/util/Dollar;
    .locals 1
    .parameter

    .prologue
    .line 47
    const-string v0, "AMOUNT"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/TransferStartActivity;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/c;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/c;->q()Lcom/chase/sig/android/util/Dollar;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Ljava/util/List;Lcom/chase/sig/android/view/JPSpinner;)Ljava/lang/String;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 47
    invoke-virtual {p1}, Lcom/chase/sig/android/view/JPSpinner;->getSelectedItemPosition()I

    move-result v0

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/IAccount;

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/ChaseApplication;->i()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/chase/sig/android/domain/IAccount;->a(Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Ljava/util/List;Lcom/chase/sig/android/view/JPSpinner;)Ljava/lang/String;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 47
    invoke-virtual {p1}, Lcom/chase/sig/android/view/JPSpinner;->getSelectedItemPosition()I

    move-result v0

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/IAccount;

    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->y()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Lcom/chase/sig/android/activity/TransferStartActivity;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 47
    iget-object v0, p0, Lcom/chase/sig/android/activity/TransferStartActivity;->a:Ljava/util/List;

    return-object v0
.end method

.method static synthetic d(Ljava/util/List;Lcom/chase/sig/android/view/JPSpinner;)Ljava/lang/String;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 47
    invoke-virtual {p1}, Lcom/chase/sig/android/view/JPSpinner;->getSelectedItemPosition()I

    move-result v0

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/IAccount;

    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private d()Ljava/util/Calendar;
    .locals 3

    .prologue
    .line 228
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V

    .line 229
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/ChaseApplication;

    const-string v2, "transfer_cutoff_hour"

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/ChaseApplication;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, v1, Landroid/text/format/Time;->hour:I

    .line 233
    iget v0, v1, Landroid/text/format/Time;->hour:I

    invoke-static {v0}, Lcom/chase/sig/android/util/l;->a(I)Ljava/util/Calendar;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Lcom/chase/sig/android/activity/TransferStartActivity;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 47
    iget-object v0, p0, Lcom/chase/sig/android/activity/TransferStartActivity;->b:Ljava/util/List;

    return-object v0
.end method

.method static synthetic e(Lcom/chase/sig/android/activity/TransferStartActivity;)Lcom/chase/sig/android/domain/AccountTransfer;
    .locals 1
    .parameter

    .prologue
    .line 47
    iget-object v0, p0, Lcom/chase/sig/android/activity/TransferStartActivity;->c:Lcom/chase/sig/android/domain/AccountTransfer;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 8
    .parameter

    .prologue
    const v5, 0x7f07027e

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 57
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/a;->a(Landroid/os/Bundle;)V

    .line 58
    const v0, 0x7f07013a

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/TransferStartActivity;->setTitle(I)V

    .line 60
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->d:Lcom/chase/sig/android/service/SplashResponse;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->d:Lcom/chase/sig/android/service/SplashResponse;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/SplashResponse;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->d:Lcom/chase/sig/android/service/SplashResponse;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/SplashResponse;->p()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/TransferStartActivity;->e(Ljava/lang/String;)V

    .line 123
    :goto_0
    return-void

    .line 63
    :cond_0
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/TransferStartActivity;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->b:Lcom/chase/sig/android/domain/g;

    invoke-interface {v0}, Lcom/chase/sig/android/domain/g;->v()Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/chase/sig/android/activity/TransferStartActivity;->b:Ljava/util/List;

    iget-object v1, p0, Lcom/chase/sig/android/activity/TransferStartActivity;->b:Ljava/util/List;

    invoke-interface {v0}, Lcom/chase/sig/android/domain/g;->y()Ljava/util/List;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-interface {v0}, Lcom/chase/sig/android/domain/g;->x()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/TransferStartActivity;->a:Ljava/util/List;

    .line 65
    iget-object v0, p0, Lcom/chase/sig/android/activity/TransferStartActivity;->b:Ljava/util/List;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/chase/sig/android/activity/TransferStartActivity;->a:Ljava/util/List;

    if-eqz v0, :cond_7

    .line 66
    iget-object v0, p0, Lcom/chase/sig/android/activity/TransferStartActivity;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lcom/chase/sig/android/activity/TransferStartActivity;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    if-nez v0, :cond_1

    move v0, v3

    :goto_1
    if-eqz v0, :cond_2

    .line 67
    const v0, 0x7f07027d

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/TransferStartActivity;->f(I)V

    goto :goto_0

    :cond_1
    move v0, v4

    .line 66
    goto :goto_1

    .line 69
    :cond_2
    iget-object v0, p0, Lcom/chase/sig/android/activity/TransferStartActivity;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_3

    iget-object v0, p0, Lcom/chase/sig/android/activity/TransferStartActivity;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_5

    :cond_3
    move v0, v3

    :goto_2
    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/chase/sig/android/activity/TransferStartActivity;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v3, :cond_6

    iget-object v0, p0, Lcom/chase/sig/android/activity/TransferStartActivity;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v3, :cond_6

    iget-object v0, p0, Lcom/chase/sig/android/activity/TransferStartActivity;->b:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/IAccount;

    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->b()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/chase/sig/android/activity/TransferStartActivity;->a:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/IAccount;

    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v3

    :goto_3
    if-eqz v0, :cond_8

    .line 71
    :cond_4
    invoke-virtual {p0, v5}, Lcom/chase/sig/android/activity/TransferStartActivity;->f(I)V

    goto/16 :goto_0

    :cond_5
    move v0, v4

    .line 69
    goto :goto_2

    :cond_6
    move v0, v4

    goto :goto_3

    .line 75
    :cond_7
    invoke-virtual {p0, v5}, Lcom/chase/sig/android/activity/TransferStartActivity;->f(I)V

    goto/16 :goto_0

    .line 79
    :cond_8
    const v0, 0x7f0902c9

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/TransferStartActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailView;

    const/4 v1, 0x5

    new-array v5, v1, [Lcom/chase/sig/android/view/detail/a;

    new-instance v1, Lcom/chase/sig/android/view/detail/q;

    const-string v2, "From"

    invoke-direct {v1, v2}, Lcom/chase/sig/android/view/detail/q;-><init>(Ljava/lang/String;)V

    const-string v2, "PAY_FROM"

    iput-object v2, v1, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v1, Lcom/chase/sig/android/view/detail/q;

    const v2, 0x7f070152

    invoke-virtual {p0, v2}, Lcom/chase/sig/android/activity/TransferStartActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/view/detail/q;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v1

    aput-object v1, v5, v4

    new-instance v1, Lcom/chase/sig/android/view/detail/q;

    const-string v2, "To"

    invoke-direct {v1, v2}, Lcom/chase/sig/android/view/detail/q;-><init>(Ljava/lang/String;)V

    const-string v2, "PAY_TO"

    iput-object v2, v1, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v1, Lcom/chase/sig/android/view/detail/q;

    const v2, 0x7f070153

    invoke-virtual {p0, v2}, Lcom/chase/sig/android/activity/TransferStartActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/view/detail/q;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v1

    aput-object v1, v5, v3

    const/4 v2, 0x2

    new-instance v1, Lcom/chase/sig/android/view/detail/c;

    const-string v6, "Amount $"

    invoke-direct {v1, v6}, Lcom/chase/sig/android/view/detail/c;-><init>(Ljava/lang/String;)V

    const-string v6, "AMOUNT"

    iput-object v6, v1, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v1, Lcom/chase/sig/android/view/detail/c;

    const-string v6, "Enter Amount"

    invoke-virtual {v1, v6}, Lcom/chase/sig/android/view/detail/c;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v1

    aput-object v1, v5, v2

    const/4 v6, 0x3

    new-instance v1, Lcom/chase/sig/android/view/detail/d;

    const-string v7, "Deliver By"

    if-eqz p1, :cond_a

    const-string v2, "deliverBy"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_a

    const-string v2, "deliverBy"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_4
    invoke-direct {v1, v7, v2}, Lcom/chase/sig/android/view/detail/d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "DATE"

    iput-object v2, v1, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v1, Lcom/chase/sig/android/view/detail/d;

    invoke-virtual {p0, v4}, Lcom/chase/sig/android/activity/TransferStartActivity;->i(I)Landroid/view/View$OnClickListener;

    move-result-object v2

    iput-object v2, v1, Lcom/chase/sig/android/view/detail/d;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v1}, Lcom/chase/sig/android/view/detail/d;->q()Lcom/chase/sig/android/view/detail/d;

    move-result-object v1

    aput-object v1, v5, v6

    const/4 v2, 0x4

    new-instance v1, Lcom/chase/sig/android/view/detail/l;

    const-string v6, "Memo"

    invoke-direct {v1, v6}, Lcom/chase/sig/android/view/detail/l;-><init>(Ljava/lang/String;)V

    const-string v6, "MEMO"

    iput-object v6, v1, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v1, Lcom/chase/sig/android/view/detail/l;

    const-string v6, "Optional"

    invoke-virtual {v1, v6}, Lcom/chase/sig/android/view/detail/l;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/view/detail/l;

    const/16 v6, 0x20

    iput v6, v1, Lcom/chase/sig/android/view/detail/l;->a:I

    aput-object v1, v5, v2

    invoke-virtual {v0, v5}, Lcom/chase/sig/android/view/detail/DetailView;->setRows([Lcom/chase/sig/android/view/detail/a;)V

    .line 91
    const-string v0, "PAY_FROM"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/TransferStartActivity;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/q;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/q;->q()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v1

    .line 92
    iget-object v0, p0, Lcom/chase/sig/android/activity/TransferStartActivity;->a:Ljava/util/List;

    invoke-static {p0, v0}, Lcom/chase/sig/android/util/a;->a(Landroid/content/Context;Ljava/util/List;)Landroid/widget/SimpleAdapter;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/view/JPSpinner;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 95
    const-string v0, "PAY_TO"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/TransferStartActivity;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/q;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/q;->q()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v0

    .line 96
    iget-object v2, p0, Lcom/chase/sig/android/activity/TransferStartActivity;->b:Ljava/util/List;

    invoke-static {p0, v2}, Lcom/chase/sig/android/util/a;->e(Landroid/content/Context;Ljava/util/List;)Landroid/widget/SimpleAdapter;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/view/JPSpinner;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 99
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/TransferStartActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    .line 101
    const-string v2, "isDebitAccount"

    invoke-static {v5, v2}, Lcom/chase/sig/android/activity/TransferStartActivity;->a(Landroid/os/Bundle;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_b

    const-string v2, "isDebitAccount"

    invoke-virtual {v5, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 105
    :goto_5
    if-eqz v3, :cond_c

    move-object v2, v1

    .line 106
    :goto_6
    const/4 v0, -0x1

    .line 108
    const-string v1, "selectedAccountId"

    invoke-static {v5, v1}, Lcom/chase/sig/android/activity/TransferStartActivity;->a(Landroid/os/Bundle;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 109
    const-string v0, "selectedAccountId"

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 111
    if-eqz v3, :cond_d

    .line 112
    iget-object v1, p0, Lcom/chase/sig/android/activity/TransferStartActivity;->a:Ljava/util/List;

    invoke-static {v1, v0}, Lcom/chase/sig/android/activity/TransferStartActivity;->a(Ljava/util/List;Ljava/lang/String;)I

    move-result v0

    .line 119
    :cond_9
    :goto_7
    invoke-virtual {v2, v0}, Lcom/chase/sig/android/view/JPSpinner;->setSelection(I)V

    .line 120
    const v0, 0x7f0902ca

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/TransferStartActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const v1, 0x7f07015b

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    const v0, 0x7f0902cb

    new-instance v1, Lcom/chase/sig/android/activity/mn;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/mn;-><init>(Lcom/chase/sig/android/activity/TransferStartActivity;)V

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/TransferStartActivity;->a(ILandroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 79
    :cond_a
    invoke-direct {p0}, Lcom/chase/sig/android/activity/TransferStartActivity;->d()Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-static {v2}, Lcom/chase/sig/android/util/s;->a(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_4

    :cond_b
    move v3, v4

    .line 101
    goto :goto_5

    :cond_c
    move-object v2, v0

    .line 105
    goto :goto_6

    .line 114
    :cond_d
    iget-object v1, p0, Lcom/chase/sig/android/activity/TransferStartActivity;->b:Ljava/util/List;

    invoke-static {v1, v0}, Lcom/chase/sig/android/activity/TransferStartActivity;->a(Ljava/util/List;Ljava/lang/String;)I

    move-result v3

    .line 115
    const-string v0, "selectedAccountId"

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/TransferStartActivity;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v1

    iget-object v1, v1, Lcom/chase/sig/android/domain/o;->b:Lcom/chase/sig/android/domain/g;

    invoke-interface {v1, v0}, Lcom/chase/sig/android/domain/g;->a(Ljava/lang/String;)Lcom/chase/sig/android/domain/IAccount;

    move-result-object v0

    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->e()Ljava/util/Hashtable;

    move-result-object v0

    const-string v1, "nextPaymentAmount"

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "AMOUNT"

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/TransferStartActivity;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/view/detail/c;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/view/detail/c;->b(Ljava/lang/String;)V

    move v0, v3

    goto :goto_7
.end method

.method public final k()V
    .locals 1

    .prologue
    .line 329
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/TransferStartActivity;->y()V

    .line 330
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/TransferStartActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/DetailView;->a()V

    .line 331
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 3
    .parameter

    .prologue
    .line 249
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/a;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    .line 251
    packed-switch p1, :pswitch_data_0

    .line 258
    :goto_0
    return-object v0

    .line 254
    :pswitch_0
    invoke-direct {p0}, Lcom/chase/sig/android/activity/TransferStartActivity;->d()Ljava/util/Calendar;

    move-result-object v0

    iget-object v1, p0, Lcom/chase/sig/android/activity/TransferStartActivity;->d:Lcom/chase/sig/android/view/b$a;

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/TransferStartActivity;->a(Ljava/util/Calendar;Lcom/chase/sig/android/view/b$a;)Lcom/chase/sig/android/view/b;

    move-result-object v0

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/TransferStartActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v1

    const-string v2, "DATE"

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/view/detail/DetailView;->e(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/view/detail/a;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->t(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/b;->a(Ljava/util/Date;)Lcom/chase/sig/android/view/b;

    move-result-object v0

    goto :goto_0

    .line 251
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0
    .parameter

    .prologue
    .line 392
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/a;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 393
    return-void
.end method
