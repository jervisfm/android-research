.class public Lcom/chase/sig/android/activity/TransferVerifyActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$e;
.implements Lcom/chase/sig/android/c$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/TransferVerifyActivity$a;
    }
.end annotation


# instance fields
.field private a:Lcom/chase/sig/android/view/detail/DetailView;

.field private b:Lcom/chase/sig/android/domain/AccountTransfer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/chase/sig/android/activity/TransferVerifyActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    .line 72
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/TransferVerifyActivity;)Lcom/chase/sig/android/domain/AccountTransfer;
    .locals 1
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, Lcom/chase/sig/android/activity/TransferVerifyActivity;->b:Lcom/chase/sig/android/domain/AccountTransfer;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 111
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 113
    const/high16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 114
    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/TransferVerifyActivity;->startActivity(Landroid/content/Intent;)V

    .line 115
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 8
    .parameter

    .prologue
    const v7, 0x7f090157

    const v6, 0x7f090156

    .line 32
    const v0, 0x7f03005f

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/TransferVerifyActivity;->b(I)V

    .line 33
    const v0, 0x7f070154

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/TransferVerifyActivity;->setTitle(I)V

    .line 35
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/TransferVerifyActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "transaction_object"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/AccountTransfer;

    iput-object v0, p0, Lcom/chase/sig/android/activity/TransferVerifyActivity;->b:Lcom/chase/sig/android/domain/AccountTransfer;

    .line 36
    const v0, 0x7f090153

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/TransferVerifyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/TransferVerifyActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    .line 38
    iget-object v1, p0, Lcom/chase/sig/android/activity/TransferVerifyActivity;->a:Lcom/chase/sig/android/view/detail/DetailView;

    const/4 v0, 0x6

    new-array v2, v0, [Lcom/chase/sig/android/view/detail/a;

    const/4 v0, 0x0

    new-instance v3, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v4, "From"

    iget-object v5, p0, Lcom/chase/sig/android/activity/TransferVerifyActivity;->b:Lcom/chase/sig/android/domain/AccountTransfer;

    invoke-virtual {v5}, Lcom/chase/sig/android/domain/AccountTransfer;->e_()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v2, v0

    const/4 v0, 0x1

    new-instance v3, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v4, "To"

    iget-object v5, p0, Lcom/chase/sig/android/activity/TransferVerifyActivity;->b:Lcom/chase/sig/android/domain/AccountTransfer;

    invoke-virtual {v5}, Lcom/chase/sig/android/domain/AccountTransfer;->d()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v2, v0

    const/4 v0, 0x2

    new-instance v3, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v4, "Amount"

    iget-object v5, p0, Lcom/chase/sig/android/activity/TransferVerifyActivity;->b:Lcom/chase/sig/android/domain/AccountTransfer;

    invoke-virtual {v5}, Lcom/chase/sig/android/domain/AccountTransfer;->e()Lcom/chase/sig/android/util/Dollar;

    move-result-object v5

    invoke-virtual {v5}, Lcom/chase/sig/android/util/Dollar;->h()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v2, v0

    const/4 v0, 0x3

    new-instance v3, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v4, "Send On"

    iget-object v5, p0, Lcom/chase/sig/android/activity/TransferVerifyActivity;->b:Lcom/chase/sig/android/domain/AccountTransfer;

    invoke-virtual {v5}, Lcom/chase/sig/android/domain/AccountTransfer;->b()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/chase/sig/android/util/s;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v2, v0

    const/4 v0, 0x4

    new-instance v3, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v4, "Deliver By"

    iget-object v5, p0, Lcom/chase/sig/android/activity/TransferVerifyActivity;->b:Lcom/chase/sig/android/domain/AccountTransfer;

    invoke-virtual {v5}, Lcom/chase/sig/android/domain/AccountTransfer;->q()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/chase/sig/android/util/s;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v2, v0

    const/4 v3, 0x5

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Memo "

    iget-object v0, p0, Lcom/chase/sig/android/activity/TransferVerifyActivity;->b:Lcom/chase/sig/android/domain/AccountTransfer;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/AccountTransfer;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/TransferVerifyActivity;->b:Lcom/chase/sig/android/domain/AccountTransfer;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/AccountTransfer;->o()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {v4, v5, v0}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/view/detail/DetailView;->setRows([Lcom/chase/sig/android/view/detail/a;)V

    .line 47
    invoke-virtual {p0, v7}, Lcom/chase/sig/android/activity/TransferVerifyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const v1, 0x7f070157

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 48
    invoke-virtual {p0, v6}, Lcom/chase/sig/android/activity/TransferVerifyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const v1, 0x7f070158

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 50
    new-instance v0, Lcom/chase/sig/android/activity/mp;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/activity/mp;-><init>(Lcom/chase/sig/android/activity/TransferVerifyActivity;)V

    invoke-virtual {p0, v7, v0}, Lcom/chase/sig/android/activity/TransferVerifyActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 68
    invoke-virtual {p0, p0}, Lcom/chase/sig/android/activity/TransferVerifyActivity;->a(Lcom/chase/sig/android/c$a;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-virtual {p0, v6, v0}, Lcom/chase/sig/android/activity/TransferVerifyActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 70
    return-void

    .line 38
    :cond_0
    const-string v0, ""

    goto :goto_0
.end method
