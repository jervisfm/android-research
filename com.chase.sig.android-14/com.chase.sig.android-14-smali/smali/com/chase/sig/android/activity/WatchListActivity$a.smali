.class public Lcom/chase/sig/android/activity/WatchListActivity$a;
.super Lcom/chase/sig/android/b;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/WatchListActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/b",
        "<",
        "Lcom/chase/sig/android/activity/WatchListActivity;",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/service/WatchListResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/chase/sig/android/b;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .parameter

    .prologue
    .line 53
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/WatchListActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/WatchListActivity;->A()Lcom/chase/sig/android/service/n;

    move-result-object v0

    iget-object v1, v0, Lcom/chase/sig/android/service/n;->o:Lcom/chase/sig/android/service/ak;

    if-nez v1, :cond_0

    new-instance v1, Lcom/chase/sig/android/service/ak;

    invoke-direct {v1}, Lcom/chase/sig/android/service/ak;-><init>()V

    iput-object v1, v0, Lcom/chase/sig/android/service/n;->o:Lcom/chase/sig/android/service/ak;

    :cond_0
    iget-object v0, v0, Lcom/chase/sig/android/service/n;->o:Lcom/chase/sig/android/service/ak;

    invoke-static {}, Lcom/chase/sig/android/service/ak;->a()Lcom/chase/sig/android/service/WatchListResponse;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 2
    .parameter

    .prologue
    .line 53
    check-cast p1, Lcom/chase/sig/android/service/WatchListResponse;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/WatchListResponse;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/WatchListActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/WatchListResponse;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/WatchListActivity;->b(Ljava/util/List;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/WatchListActivity;

    invoke-static {v0, p1}, Lcom/chase/sig/android/activity/WatchListActivity;->a(Lcom/chase/sig/android/activity/WatchListActivity;Lcom/chase/sig/android/service/WatchListResponse;)Lcom/chase/sig/android/service/WatchListResponse;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/WatchListActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/WatchListResponse;->a()Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/chase/sig/android/activity/WatchListActivity;->a(Lcom/chase/sig/android/activity/WatchListActivity;Ljava/util/List;)V

    goto :goto_0
.end method
