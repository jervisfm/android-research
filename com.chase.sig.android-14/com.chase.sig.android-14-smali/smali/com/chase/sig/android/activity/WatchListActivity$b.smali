.class final Lcom/chase/sig/android/activity/WatchListActivity$b;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/WatchListActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/chase/sig/android/domain/WatchList;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/WatchListActivity;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/WatchList;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Lcom/chase/sig/android/activity/WatchListActivity;Landroid/content/Context;Ljava/util/List;)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 112
    iput-object p1, p0, Lcom/chase/sig/android/activity/WatchListActivity$b;->a:Lcom/chase/sig/android/activity/WatchListActivity;

    .line 113
    const v0, 0x7f0300cc

    invoke-direct {p0, p2, v0, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 114
    iput-object p3, p0, Lcom/chase/sig/android/activity/WatchListActivity$b;->b:Ljava/util/List;

    .line 115
    const-string v0, "layout_inflater"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/chase/sig/android/activity/WatchListActivity$b;->c:Landroid/view/LayoutInflater;

    .line 117
    return-void
.end method

.method private a(Landroid/view/View;Lcom/chase/sig/android/domain/Quote;)V
    .locals 7
    .parameter
    .parameter

    .prologue
    .line 163
    const v3, 0x7f0902e4

    const v4, 0x7f0902e5

    const v5, 0x7f0902e6

    const v6, 0x7f0902e7

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v6}, Lcom/chase/sig/android/activity/WatchListActivity$b;->a(Landroid/view/View;Lcom/chase/sig/android/domain/Quote;IIII)V

    .line 164
    return-void
.end method

.method private a(Landroid/view/View;Lcom/chase/sig/android/domain/Quote;IIII)V
    .locals 3
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 177
    invoke-virtual {p1, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/chase/sig/android/domain/Quote;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 180
    invoke-virtual {p1, p4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/chase/sig/android/domain/Quote;->i()Lcom/chase/sig/android/util/Dollar;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/util/Dollar;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 182
    invoke-virtual {p2}, Lcom/chase/sig/android/domain/Quote;->b()Lcom/chase/sig/android/util/Dollar;

    move-result-object v1

    .line 184
    if-nez v1, :cond_0

    .line 185
    invoke-virtual {p1, p5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/TickerValueTextView;

    new-instance v1, Lcom/chase/sig/android/util/Dollar;

    const-string v2, "0.00"

    invoke-direct {v1, v2}, Lcom/chase/sig/android/util/Dollar;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/chase/sig/android/util/Dollar;->h()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/chase/sig/android/view/TickerValueTextView;->a(Ljava/lang/String;Z)V

    .line 190
    :goto_0
    invoke-virtual {p2}, Lcom/chase/sig/android/domain/Quote;->d()Ljava/lang/String;

    move-result-object v1

    .line 192
    if-nez v1, :cond_1

    .line 193
    invoke-virtual {p1, p6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/TickerValueTextView;

    const-string v1, "0.00"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/chase/sig/android/view/TickerValueTextView;->a(Ljava/lang/String;Z)V

    .line 200
    :goto_1
    new-instance v0, Lcom/chase/sig/android/activity/mq;

    invoke-direct {v0, p0, p2}, Lcom/chase/sig/android/activity/mq;-><init>(Lcom/chase/sig/android/activity/WatchListActivity$b;Lcom/chase/sig/android/domain/Quote;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 207
    return-void

    .line 187
    :cond_0
    invoke-virtual {p1, p5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/TickerValueTextView;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/TickerValueTextView;->setTickerValue(Lcom/chase/sig/android/util/Dollar;)V

    goto :goto_0

    .line 196
    :cond_1
    invoke-virtual {p1, p6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/TickerValueTextView;

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->C(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/chase/sig/android/domain/Quote;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/chase/sig/android/view/TickerValueTextView;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method private b(Landroid/view/View;Lcom/chase/sig/android/domain/Quote;)V
    .locals 7
    .parameter
    .parameter

    .prologue
    .line 167
    const v3, 0x7f0902eb

    const v4, 0x7f0902ec

    const v5, 0x7f0902ed

    const v6, 0x7f0902ee

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v6}, Lcom/chase/sig/android/activity/WatchListActivity$b;->a(Landroid/view/View;Lcom/chase/sig/android/domain/Quote;IIII)V

    .line 168
    return-void
.end method


# virtual methods
.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const v5, 0x7f0902ea

    const v4, 0x7f0902e3

    const/4 v3, 0x0

    .line 122
    iget-object v0, p0, Lcom/chase/sig/android/activity/WatchListActivity$b;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/WatchList;

    .line 124
    if-nez p2, :cond_0

    .line 125
    iget-object v1, p0, Lcom/chase/sig/android/activity/WatchListActivity$b;->c:Landroid/view/LayoutInflater;

    const v2, 0x7f0300cc

    invoke-virtual {v1, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 128
    :cond_0
    const v1, 0x7f0902dc

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 129
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/WatchList;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 131
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/WatchList;->b()Ljava/util/List;

    move-result-object v2

    .line 133
    if-eqz v2, :cond_1

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v6, :cond_1

    .line 135
    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 136
    const v0, 0x7f0902e9

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 138
    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/Quote;

    invoke-direct {p0, v1, v0}, Lcom/chase/sig/android/activity/WatchListActivity$b;->a(Landroid/view/View;Lcom/chase/sig/android/domain/Quote;)V

    .line 159
    :goto_0
    return-object p2

    .line 140
    :cond_1
    if-eqz v2, :cond_2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v7, :cond_2

    .line 142
    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 143
    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 144
    const v0, 0x7f0902f0

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 146
    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/Quote;

    invoke-direct {p0, v1, v0}, Lcom/chase/sig/android/activity/WatchListActivity$b;->a(Landroid/view/View;Lcom/chase/sig/android/domain/Quote;)V

    .line 147
    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-interface {v2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/Quote;

    invoke-direct {p0, v1, v0}, Lcom/chase/sig/android/activity/WatchListActivity$b;->b(Landroid/view/View;Lcom/chase/sig/android/domain/Quote;)V

    goto :goto_0

    .line 151
    :cond_2
    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 152
    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 153
    const v0, 0x7f0902f1

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 155
    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/Quote;

    invoke-direct {p0, v1, v0}, Lcom/chase/sig/android/activity/WatchListActivity$b;->a(Landroid/view/View;Lcom/chase/sig/android/domain/Quote;)V

    .line 156
    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-interface {v2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/Quote;

    invoke-direct {p0, v1, v0}, Lcom/chase/sig/android/activity/WatchListActivity$b;->b(Landroid/view/View;Lcom/chase/sig/android/domain/Quote;)V

    .line 157
    const v0, 0x7f0902f1

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-interface {v2, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/chase/sig/android/domain/Quote;

    const v3, 0x7f0902f2

    const v4, 0x7f0902f3

    const v5, 0x7f0902f4

    const v6, 0x7f0902f5

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/chase/sig/android/activity/WatchListActivity$b;->a(Landroid/view/View;Lcom/chase/sig/android/domain/Quote;IIII)V

    goto/16 :goto_0
.end method
