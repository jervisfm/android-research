.class public Lcom/chase/sig/android/activity/WatchListActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$c;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/WatchListActivity$b;,
        Lcom/chase/sig/android/activity/WatchListActivity$a;
    }
.end annotation


# instance fields
.field private a:Landroid/widget/ListView;

.field private b:Lcom/chase/sig/android/service/WatchListResponse;

.field private c:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    .line 105
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/WatchListActivity;Lcom/chase/sig/android/service/WatchListResponse;)Lcom/chase/sig/android/service/WatchListResponse;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 29
    iput-object p1, p0, Lcom/chase/sig/android/activity/WatchListActivity;->b:Lcom/chase/sig/android/service/WatchListResponse;

    return-object p1
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/WatchListActivity;Ljava/util/List;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/chase/sig/android/activity/WatchListActivity;->a(Ljava/util/List;)V

    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/WatchList;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 89
    if-eqz p1, :cond_0

    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 90
    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/WatchListActivity;->a:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setVisibility(I)V

    .line 91
    iget-object v0, p0, Lcom/chase/sig/android/activity/WatchListActivity;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 103
    :goto_0
    return-void

    .line 94
    :cond_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/WatchListActivity;->a:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 95
    iget-object v0, p0, Lcom/chase/sig/android/activity/WatchListActivity;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 97
    iget-object v0, p0, Lcom/chase/sig/android/activity/WatchListActivity;->a:Landroid/widget/ListView;

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/WatchListActivity;->a(Landroid/widget/ListView;)V

    .line 98
    new-instance v0, Lcom/chase/sig/android/activity/WatchListActivity$b;

    invoke-direct {v0, p0, p0, p1}, Lcom/chase/sig/android/activity/WatchListActivity$b;-><init>(Lcom/chase/sig/android/activity/WatchListActivity;Landroid/content/Context;Ljava/util/List;)V

    .line 100
    iget-object v1, p0, Lcom/chase/sig/android/activity/WatchListActivity;->a:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 38
    const v0, 0x7f0702cd

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/WatchListActivity;->setTitle(I)V

    .line 39
    const v0, 0x7f0300cb

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/WatchListActivity;->b(I)V

    .line 40
    const v0, 0x7f0902da

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/WatchListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/WatchListActivity;->a:Landroid/widget/ListView;

    .line 41
    iget-object v0, p0, Lcom/chase/sig/android/activity/WatchListActivity;->a:Landroid/widget/ListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 42
    const v0, 0x7f09027b

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/WatchListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/WatchListActivity;->c:Landroid/widget/TextView;

    .line 44
    const-string v0, "watch_list_response"

    invoke-static {p1, v0}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 45
    const-string v0, "watch_list_response"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/service/WatchListResponse;

    iput-object v0, p0, Lcom/chase/sig/android/activity/WatchListActivity;->b:Lcom/chase/sig/android/service/WatchListResponse;

    .line 46
    iget-object v0, p0, Lcom/chase/sig/android/activity/WatchListActivity;->b:Lcom/chase/sig/android/service/WatchListResponse;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/WatchListActivity;->b:Lcom/chase/sig/android/service/WatchListResponse;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/WatchListResponse;->a()Ljava/util/List;

    move-result-object v0

    :goto_0
    invoke-direct {p0, v0}, Lcom/chase/sig/android/activity/WatchListActivity;->a(Ljava/util/List;)V

    .line 51
    :goto_1
    return-void

    .line 46
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0

    .line 49
    :cond_1
    const-class v0, Lcom/chase/sig/android/activity/WatchListActivity$a;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/WatchListActivity;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method protected final c_()V
    .locals 0

    .prologue
    .line 77
    invoke-super {p0}, Lcom/chase/sig/android/activity/ai;->c_()V

    .line 78
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/WatchListActivity;->finish()V

    .line 79
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 83
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ai;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 84
    const-string v0, "watch_list_response"

    iget-object v1, p0, Lcom/chase/sig/android/activity/WatchListActivity;->b:Lcom/chase/sig/android/service/WatchListResponse;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 85
    return-void
.end method
