.class public Lcom/chase/sig/android/activity/WireEditActivity$a;
.super Lcom/chase/sig/android/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/WireEditActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/d",
        "<",
        "Lcom/chase/sig/android/activity/WireEditActivity;",
        "Lcom/chase/sig/android/domain/WireTransaction;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 160
    invoke-direct {p0}, Lcom/chase/sig/android/d;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .parameter

    .prologue
    .line 160
    check-cast p1, [Lcom/chase/sig/android/domain/WireTransaction;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/WireEditActivity;

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-static {v0, v1}, Lcom/chase/sig/android/activity/WireEditActivity;->a(Lcom/chase/sig/android/activity/WireEditActivity;Ljava/util/Date;)Ljava/util/Date;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/WireEditActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/WireEditActivity;->A()Lcom/chase/sig/android/service/n;

    invoke-static {}, Lcom/chase/sig/android/service/n;->f()Lcom/chase/sig/android/service/wire/a;

    invoke-static {}, Lcom/chase/sig/android/service/wire/a;->a()Lcom/chase/sig/android/service/wire/WireGetPayeesResponse;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/service/wire/WireGetPayeesResponse;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/Payee;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Payee;->j()Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x0

    aget-object v3, p1, v3

    invoke-virtual {v3}, Lcom/chase/sig/android/domain/WireTransaction;->D()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v1, Lcom/chase/sig/android/activity/WireEditActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Payee;->f()Ljava/util/Date;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/chase/sig/android/activity/WireEditActivity;->a(Lcom/chase/sig/android/activity/WireEditActivity;Ljava/util/Date;)Ljava/util/Date;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method
