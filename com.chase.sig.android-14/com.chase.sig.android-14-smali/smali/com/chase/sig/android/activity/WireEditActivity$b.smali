.class public Lcom/chase/sig/android/activity/WireEditActivity$b;
.super Lcom/chase/sig/android/activity/h$a;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/WireEditActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/activity/h$a",
        "<",
        "Lcom/chase/sig/android/domain/WireTransaction;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 177
    invoke-direct {p0}, Lcom/chase/sig/android/activity/h$a;-><init>()V

    return-void
.end method


# virtual methods
.method protected final a()Lcom/chase/sig/android/service/movemoney/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/chase/sig/android/service/movemoney/d",
            "<",
            "Lcom/chase/sig/android/domain/WireTransaction;",
            ">;"
        }
    .end annotation

    .prologue
    .line 181
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/h;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/h;->A()Lcom/chase/sig/android/service/n;

    invoke-static {}, Lcom/chase/sig/android/service/n;->f()Lcom/chase/sig/android/service/wire/a;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Lcom/chase/sig/android/service/movemoney/ServiceResponse;)V
    .locals 6
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 198
    const-string v0, "70011"

    invoke-virtual {p1, v0}, Lcom/chase/sig/android/service/movemoney/ServiceResponse;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    move v2, v0

    .line 201
    :goto_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/h;

    const v1, 0x7f07011f

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/h;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 203
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 205
    new-instance v4, Lcom/chase/sig/android/service/ServiceError;

    const-string v5, "70011"

    invoke-direct {v4, v5, v0, v3}, Lcom/chase/sig/android/service/ServiceError;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 206
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 208
    invoke-virtual {p0, p1}, Lcom/chase/sig/android/activity/WireEditActivity$b;->b(Lcom/chase/sig/android/service/movemoney/ServiceResponse;)V

    .line 210
    if-nez v2, :cond_0

    invoke-virtual {p1}, Lcom/chase/sig/android/service/movemoney/ServiceResponse;->f()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    if-eqz v2, :cond_3

    iget-object v0, p0, Lcom/chase/sig/android/activity/WireEditActivity$b;->a:Lcom/chase/sig/android/domain/Transaction;

    check-cast v0, Lcom/chase/sig/android/domain/WireTransaction;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/WireTransaction;->l()Z

    move-result v0

    if-nez v0, :cond_3

    .line 213
    :cond_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/h;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/movemoney/ServiceResponse;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/h;->c(Ljava/util/List;)V

    .line 241
    :goto_1
    return-void

    :cond_2
    move v2, v3

    .line 198
    goto :goto_0

    .line 215
    :cond_3
    new-instance v3, Landroid/content/Intent;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/h;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/h;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-class v4, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;

    invoke-direct {v3, v0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 219
    const/high16 v0, 0x400

    invoke-virtual {v3, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 220
    invoke-static {v3}, Lcom/chase/sig/android/c;->a(Landroid/content/Intent;)V

    .line 222
    new-instance v3, Landroid/content/Intent;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/h;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/h;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    const-class v4, Lcom/chase/sig/android/activity/wire/WireEditVerifyActivity;

    invoke-direct {v3, v0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 225
    const-string v4, "transaction_object"

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/h;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/h;->d()Lcom/chase/sig/android/domain/Transaction;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 228
    const-string v4, "request_flags"

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/h;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/h;->c()Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, Lcom/chase/sig/android/service/movemoney/RequestFlags;->d:Lcom/chase/sig/android/service/movemoney/RequestFlags;

    :goto_2
    invoke-virtual {v3, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 231
    if-eqz v2, :cond_5

    .line 232
    const-string v2, "queued_errors"

    move-object v0, v1

    check-cast v0, Ljava/io/Serializable;

    invoke-virtual {v3, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 238
    :goto_3
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/h;

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/activity/h;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 228
    :cond_4
    sget-object v0, Lcom/chase/sig/android/service/movemoney/RequestFlags;->c:Lcom/chase/sig/android/service/movemoney/RequestFlags;

    goto :goto_2

    .line 234
    :cond_5
    const-string v1, "queued_errors"

    invoke-virtual {p1}, Lcom/chase/sig/android/service/movemoney/ServiceResponse;->g()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/io/Serializable;

    invoke-virtual {v3, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    goto :goto_3
.end method

.method protected final bridge synthetic a(Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 177
    check-cast p1, Lcom/chase/sig/android/service/movemoney/ServiceResponse;

    invoke-virtual {p0, p1}, Lcom/chase/sig/android/activity/WireEditActivity$b;->a(Lcom/chase/sig/android/service/movemoney/ServiceResponse;)V

    return-void
.end method

.method protected final b()I
    .locals 1

    .prologue
    .line 186
    const v0, 0x7f070137

    return v0
.end method

.method protected final b(Lcom/chase/sig/android/service/movemoney/ServiceResponse;)V
    .locals 2
    .parameter

    .prologue
    .line 191
    iget-object v0, p0, Lcom/chase/sig/android/activity/WireEditActivity$b;->a:Lcom/chase/sig/android/domain/Transaction;

    check-cast v0, Lcom/chase/sig/android/domain/WireTransaction;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/movemoney/ServiceResponse;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/WireTransaction;->m(Ljava/lang/String;)V

    .line 193
    return-void
.end method
