.class public Lcom/chase/sig/android/activity/WireEditActivity;
.super Lcom/chase/sig/android/activity/h;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/WireEditActivity$b;,
        Lcom/chase/sig/android/activity/WireEditActivity$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/activity/h",
        "<",
        "Lcom/chase/sig/android/domain/WireTransaction;",
        ">;"
    }
.end annotation


# instance fields
.field private b:Ljava/util/Date;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/chase/sig/android/activity/h;-><init>()V

    .line 50
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/chase/sig/android/activity/WireEditActivity;->b:Ljava/util/Date;

    .line 177
    return-void
.end method

.method private D()Z
    .locals 2

    .prologue
    .line 249
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/WireEditActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v0

    const-string v1, "REMAINING"

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/detail/DetailView;->e(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/r;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/r;->q()Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/WireEditActivity;Ljava/util/Date;)Ljava/util/Date;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 48
    iput-object p1, p0, Lcom/chase/sig/android/activity/WireEditActivity;->b:Ljava/util/Date;

    return-object p1
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 14
    .parameter

    .prologue
    const/4 v10, 0x4

    const/4 v13, 0x3

    const/4 v12, 0x2

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 54
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/h;->a(Landroid/os/Bundle;)V

    .line 55
    const v0, 0x7f07012c

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/WireEditActivity;->setTitle(I)V

    .line 57
    const-string v0, "memo_fields_visible"

    invoke-static {p1, v0}, Lcom/chase/sig/android/util/e;->b(Landroid/os/Bundle;Ljava/lang/String;)Z

    move-result v5

    .line 60
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/WireEditActivity;->d()Lcom/chase/sig/android/domain/Transaction;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/WireTransaction;

    .line 62
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/WireEditActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v6

    const/16 v1, 0xa

    new-array v7, v1, [Lcom/chase/sig/android/view/detail/a;

    new-instance v1, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v2, "Recipient"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/WireTransaction;->d()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v1, v2, v8}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v1, v7, v4

    new-instance v1, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v2, "From"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/WireTransaction;->m()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v1, v2, v8}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v1, v7, v3

    new-instance v1, Lcom/chase/sig/android/view/detail/c;

    const-string v2, "Amount $ "

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/WireTransaction;->e()Lcom/chase/sig/android/util/Dollar;

    move-result-object v8

    invoke-direct {v1, v2, v8}, Lcom/chase/sig/android/view/detail/c;-><init>(Ljava/lang/String;Lcom/chase/sig/android/util/Dollar;)V

    const-string v2, "AMOUNT"

    iput-object v2, v1, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    aput-object v1, v7, v12

    new-instance v1, Lcom/chase/sig/android/view/detail/d;

    const-string v2, "Date "

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/WireTransaction;->q()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/chase/sig/android/util/s;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v1, v2, v8}, Lcom/chase/sig/android/view/detail/d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "DATE"

    iput-object v2, v1, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v1, Lcom/chase/sig/android/view/detail/d;

    invoke-virtual {p0, v4}, Lcom/chase/sig/android/activity/WireEditActivity;->i(I)Landroid/view/View$OnClickListener;

    move-result-object v2

    iput-object v2, v1, Lcom/chase/sig/android/view/detail/d;->a:Landroid/view/View$OnClickListener;

    aput-object v1, v7, v13

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/WireEditActivity;->a(Lcom/chase/sig/android/domain/Transaction;)Lcom/chase/sig/android/view/detail/o;

    move-result-object v1

    const-string v2, "FREQUENCY"

    iput-object v2, v1, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v1, Lcom/chase/sig/android/view/detail/q;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/WireEditActivity;->b()Z

    move-result v2

    iput-boolean v2, v1, Lcom/chase/sig/android/view/detail/a;->j:Z

    aput-object v1, v7, v10

    const/4 v2, 0x5

    new-instance v1, Lcom/chase/sig/android/view/detail/r;

    const-string v8, "Remaining Wires"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/WireTransaction;->x()Lcom/chase/sig/android/view/f;

    move-result-object v9

    invoke-direct {v1, v8, v9}, Lcom/chase/sig/android/view/detail/r;-><init>(Ljava/lang/String;Lcom/chase/sig/android/view/f;)V

    const-string v8, "REMAINING"

    iput-object v8, v1, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v1, Lcom/chase/sig/android/view/detail/j;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/WireEditActivity;->b()Z

    move-result v8

    iput-boolean v8, v1, Lcom/chase/sig/android/view/detail/a;->j:Z

    aput-object v1, v7, v2

    const/4 v2, 0x6

    new-instance v1, Lcom/chase/sig/android/view/detail/d;

    const-string v8, ""

    const-string v9, ""

    invoke-direct {v1, v8, v9}, Lcom/chase/sig/android/view/detail/d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v8, "Enter message or memo"

    invoke-virtual {v1, v8}, Lcom/chase/sig/android/view/detail/d;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/view/detail/d;

    const-string v8, "MEMO_FIELDS"

    iput-object v8, v1, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v1, Lcom/chase/sig/android/view/detail/d;

    iput-boolean v3, v1, Lcom/chase/sig/android/view/detail/a;->n:Z

    check-cast v1, Lcom/chase/sig/android/view/detail/d;

    new-instance v8, Lcom/chase/sig/android/activity/a/g;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/WireEditActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v9

    new-array v10, v10, [Ljava/lang/String;

    const-string v11, "MESSAGE_TO_RECIPIENT"

    aput-object v11, v10, v4

    const-string v11, "TO_RECIPIENT_BANK"

    aput-object v11, v10, v3

    const-string v11, "MEMO"

    aput-object v11, v10, v12

    const-string v11, "MEMO_FIELDS"

    aput-object v11, v10, v13

    invoke-direct {v8, v9, v10}, Lcom/chase/sig/android/activity/a/g;-><init>(Lcom/chase/sig/android/view/detail/DetailView;[Ljava/lang/String;)V

    iput-object v8, v1, Lcom/chase/sig/android/view/detail/d;->a:Landroid/view/View$OnClickListener;

    iput-boolean v5, v1, Lcom/chase/sig/android/view/detail/a;->j:Z

    aput-object v1, v7, v2

    const/4 v8, 0x7

    new-instance v1, Lcom/chase/sig/android/view/detail/l;

    const-string v2, "Message To Recipient "

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/WireTransaction;->f()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/chase/sig/android/util/s;->s(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v1, v2, v9}, Lcom/chase/sig/android/view/detail/l;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "Optional"

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/view/detail/l;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/view/detail/l;

    const-string v2, "MESSAGE_TO_RECIPIENT"

    iput-object v2, v1, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v1, Lcom/chase/sig/android/view/detail/l;

    const/16 v2, 0x8c

    iput v2, v1, Lcom/chase/sig/android/view/detail/l;->a:I

    if-nez v5, :cond_0

    move v2, v3

    :goto_0
    iput-boolean v2, v1, Lcom/chase/sig/android/view/detail/a;->j:Z

    aput-object v1, v7, v8

    const/16 v8, 0x8

    new-instance v1, Lcom/chase/sig/android/view/detail/l;

    const-string v2, "To Recipient\'s Bank "

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/WireTransaction;->h()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/chase/sig/android/util/s;->s(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v1, v2, v9}, Lcom/chase/sig/android/view/detail/l;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "Optional"

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/view/detail/l;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/view/detail/l;

    const-string v2, "TO_RECIPIENT_BANK"

    iput-object v2, v1, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v1, Lcom/chase/sig/android/view/detail/l;

    const/16 v2, 0x64

    iput v2, v1, Lcom/chase/sig/android/view/detail/l;->a:I

    if-nez v5, :cond_1

    move v2, v3

    :goto_1
    iput-boolean v2, v1, Lcom/chase/sig/android/view/detail/a;->j:Z

    aput-object v1, v7, v8

    const/16 v8, 0x9

    new-instance v1, Lcom/chase/sig/android/view/detail/l;

    const-string v2, "Memo "

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/WireTransaction;->o()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/chase/sig/android/util/s;->s(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v1, v2, v9}, Lcom/chase/sig/android/view/detail/l;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "Optional"

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/view/detail/l;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/view/detail/l;

    const-string v2, "MEMO"

    iput-object v2, v1, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v1, Lcom/chase/sig/android/view/detail/l;

    const/16 v2, 0x64

    iput v2, v1, Lcom/chase/sig/android/view/detail/l;->a:I

    if-nez v5, :cond_2

    move v2, v3

    :goto_2
    iput-boolean v2, v1, Lcom/chase/sig/android/view/detail/a;->j:Z

    aput-object v1, v7, v8

    invoke-virtual {v6, v7}, Lcom/chase/sig/android/view/detail/DetailView;->setRows([Lcom/chase/sig/android/view/detail/a;)V

    .line 88
    const-string v1, "earliest_payment_date"

    invoke-static {p1, v1}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 89
    const-string v0, "earliest_payment_date"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->t(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/WireEditActivity;->b:Ljava/util/Date;

    .line 94
    :goto_3
    return-void

    :cond_0
    move v2, v4

    .line 62
    goto :goto_0

    :cond_1
    move v2, v4

    goto :goto_1

    :cond_2
    move v2, v4

    goto :goto_2

    .line 92
    :cond_3
    const-class v1, Lcom/chase/sig/android/activity/WireEditActivity$a;

    new-array v2, v3, [Lcom/chase/sig/android/domain/WireTransaction;

    aput-object v0, v2, v4

    invoke-virtual {p0, v1, v2}, Lcom/chase/sig/android/activity/WireEditActivity;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    goto :goto_3
.end method

.method protected final synthetic f()Lcom/chase/sig/android/domain/Transaction;
    .locals 3

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/WireEditActivity;->d()Lcom/chase/sig/android/domain/Transaction;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/WireTransaction;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/WireEditActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v1

    const-string v2, "AMOUNT"

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/view/detail/DetailView;->c(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/util/Dollar;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/WireTransaction;->a(Lcom/chase/sig/android/util/Dollar;)V

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/WireEditActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v1

    const-string v2, "DATE"

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/view/detail/DetailView;->c(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->t(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->b(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/WireTransaction;->n(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/WireEditActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v1

    const-string v2, "MESSAGE_TO_RECIPIENT"

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/view/detail/DetailView;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->s(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/WireTransaction;->e(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/WireEditActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v1

    const-string v2, "TO_RECIPIENT_BANK"

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/view/detail/DetailView;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->s(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/WireTransaction;->f(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/WireEditActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v1

    const-string v2, "MEMO"

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/view/detail/DetailView;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->s(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/WireTransaction;->l(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/WireEditActivity;->b()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/chase/sig/android/activity/WireEditActivity;->D()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "true"

    :goto_0
    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/WireTransaction;->s(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/WireEditActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v1

    const-string v2, "REMAINING"

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/view/detail/DetailView;->e(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/view/detail/r;

    invoke-virtual {v1}, Lcom/chase/sig/android/view/detail/r;->r()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/WireTransaction;->r(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/WireEditActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v1

    const-string v2, "FREQUENCY"

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/view/detail/DetailView;->e(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/view/detail/o;

    invoke-virtual {v1}, Lcom/chase/sig/android/view/detail/o;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/WireTransaction;->g(Ljava/lang/String;)V

    :cond_0
    return-object v0

    :cond_1
    const-string v1, "false"

    goto :goto_0
.end method

.method protected final g()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 128
    const/4 v1, 0x1

    .line 129
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/WireEditActivity;->y()V

    .line 131
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/WireEditActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v0

    const-string v3, "AMOUNT"

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/view/detail/DetailView;->e(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/c;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/c;->p()Lcom/chase/sig/android/view/AmountView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/view/AmountView;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 132
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/WireEditActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v0

    const-string v1, "AMOUNT"

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/detail/DetailView;->a(Ljava/lang/String;)V

    move v1, v2

    .line 136
    :cond_0
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/WireEditActivity;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 137
    invoke-direct {p0}, Lcom/chase/sig/android/activity/WireEditActivity;->D()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/WireEditActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v0

    const-string v3, "REMAINING"

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/view/detail/DetailView;->e(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/r;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/r;->n()Z

    move-result v0

    if-nez v0, :cond_1

    .line 138
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/WireEditActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v0

    const-string v1, "REMAINING"

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/detail/DetailView;->a(Ljava/lang/String;)V

    move v1, v2

    .line 143
    :cond_1
    return v1
.end method

.method protected final h()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/chase/sig/android/activity/WireEditActivity$b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 148
    const-class v0, Lcom/chase/sig/android/activity/WireEditActivity$b;

    return-object v0
.end method

.method protected final i()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/chase/sig/android/activity/eb;",
            ">;"
        }
    .end annotation

    .prologue
    .line 152
    const-class v0, Lcom/chase/sig/android/activity/wire/WireEditVerifyActivity;

    return-object v0
.end method

.method protected final j()Ljava/util/Date;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/chase/sig/android/activity/WireEditActivity;->b:Ljava/util/Date;

    return-object v0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .parameter

    .prologue
    .line 117
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/h;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 118
    const-string v0, "memo_fields_visible"

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/WireEditActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v1

    const-string v2, "MEMO_FIELDS"

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/view/detail/DetailView;->e(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v1

    iget-boolean v1, v1, Lcom/chase/sig/android/view/detail/a;->j:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 120
    iget-object v0, p0, Lcom/chase/sig/android/activity/WireEditActivity;->b:Ljava/util/Date;

    if-eqz v0, :cond_0

    .line 121
    const-string v0, "earliest_payment_date"

    iget-object v1, p0, Lcom/chase/sig/android/activity/WireEditActivity;->b:Ljava/util/Date;

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->a(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    :cond_0
    return-void
.end method
