.class public abstract Lcom/chase/sig/android/activity/a;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$e;
.implements Lcom/chase/sig/android/activity/ma;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    return-void
.end method


# virtual methods
.method protected a()Lcom/chase/sig/android/view/detail/DetailView;
    .locals 1

    .prologue
    .line 44
    const v0, 0x7f0902c9

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/a;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailView;

    return-object v0
.end method

.method protected final a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/chase/sig/android/view/detail/a;",
            ">(",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/a;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/chase/sig/android/view/detail/DetailView;->e(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 28
    const v0, 0x7f0300c4

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/a;->b(I)V

    .line 29
    const v0, 0x7f0902ca

    new-instance v1, Lcom/chase/sig/android/activity/a/a;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/a/a;-><init>(Lcom/chase/sig/android/activity/ma;)V

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/a;->a(ILandroid/view/View$OnClickListener;)V

    .line 30
    return-void
.end method

.method protected final b(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/q;
    .locals 1
    .parameter

    .prologue
    .line 40
    invoke-virtual {p0, p1}, Lcom/chase/sig/android/activity/a;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/q;

    return-object v0
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 61
    if-nez p1, :cond_0

    .line 62
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/a;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v0

    const-string v1, "DATE"

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/detail/DetailView;->e(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/a;->g()Ljava/lang/String;

    move-result-object v0

    .line 63
    invoke-static {v0}, Lcom/chase/sig/android/util/s;->l(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x0

    .line 66
    :goto_0
    check-cast p2, Lcom/chase/sig/android/view/b;

    invoke-virtual {p2, v0}, Lcom/chase/sig/android/view/b;->a(Ljava/util/Date;)Lcom/chase/sig/android/view/b;

    .line 68
    :cond_0
    return-void

    .line 63
    :cond_1
    invoke-static {v0}, Lcom/chase/sig/android/util/s;->t(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    goto :goto_0
.end method

.method protected onRestart()V
    .locals 0

    .prologue
    .line 49
    invoke-super {p0}, Lcom/chase/sig/android/activity/ai;->onRestart()V

    .line 50
    invoke-static {}, Lcom/chase/sig/android/c;->a()V

    .line 51
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 55
    invoke-super {p0}, Lcom/chase/sig/android/activity/ai;->onResume()V

    .line 56
    invoke-static {}, Lcom/chase/sig/android/c;->a()V

    .line 57
    return-void
.end method
