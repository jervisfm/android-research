.class public final Lcom/chase/sig/android/activity/a/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:Lcom/chase/sig/android/activity/eb;

.field private b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Lcom/chase/sig/android/activity/eb;",
            ">;"
        }
    .end annotation
.end field

.field private c:Z

.field private d:Z

.field private e:Landroid/os/Bundle;

.field private f:Z

.field private g:Z


# direct methods
.method public constructor <init>(Lcom/chase/sig/android/activity/eb;Ljava/lang/Class;)V
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/chase/sig/android/activity/eb;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/chase/sig/android/activity/eb;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-boolean v0, p0, Lcom/chase/sig/android/activity/a/f;->c:Z

    .line 16
    iput-boolean v0, p0, Lcom/chase/sig/android/activity/a/f;->d:Z

    .line 17
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/chase/sig/android/activity/a/f;->e:Landroid/os/Bundle;

    .line 22
    iput-object p1, p0, Lcom/chase/sig/android/activity/a/f;->a:Lcom/chase/sig/android/activity/eb;

    .line 23
    iput-object p2, p0, Lcom/chase/sig/android/activity/a/f;->b:Ljava/lang/Class;

    .line 24
    return-void
.end method


# virtual methods
.method public final a()Lcom/chase/sig/android/activity/a/f;
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/chase/sig/android/activity/a/f;->c:Z

    .line 28
    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/io/Serializable;)Lcom/chase/sig/android/activity/a/f;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 77
    iget-object v0, p0, Lcom/chase/sig/android/activity/a/f;->e:Landroid/os/Bundle;

    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 78
    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/activity/a/f;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 72
    iget-object v0, p0, Lcom/chase/sig/android/activity/a/f;->e:Landroid/os/Bundle;

    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    return-object p0
.end method

.method public final b()Lcom/chase/sig/android/activity/a/f;
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/chase/sig/android/activity/a/f;->g:Z

    .line 58
    return-object p0
.end method

.method public final c()Lcom/chase/sig/android/activity/a/f;
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/chase/sig/android/activity/a/f;->d:Z

    .line 63
    return-object p0
.end method

.method public final d()Lcom/chase/sig/android/activity/a/f;
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/chase/sig/android/activity/a/f;->f:Z

    .line 68
    return-object p0
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 3
    .parameter

    .prologue
    .line 33
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/chase/sig/android/activity/a/f;->a:Lcom/chase/sig/android/activity/eb;

    iget-object v2, p0, Lcom/chase/sig/android/activity/a/f;->b:Ljava/lang/Class;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 34
    iget-boolean v1, p0, Lcom/chase/sig/android/activity/a/f;->f:Z

    if-eqz v1, :cond_0

    .line 35
    const/high16 v1, 0x4000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 38
    :cond_0
    iget-boolean v1, p0, Lcom/chase/sig/android/activity/a/f;->g:Z

    if-eqz v1, :cond_1

    .line 39
    const/high16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 42
    :cond_1
    iget-boolean v1, p0, Lcom/chase/sig/android/activity/a/f;->d:Z

    if-eqz v1, :cond_2

    .line 43
    iget-object v1, p0, Lcom/chase/sig/android/activity/a/f;->a:Lcom/chase/sig/android/activity/eb;

    invoke-virtual {v1}, Lcom/chase/sig/android/activity/eb;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/content/Intent;)Landroid/content/Intent;

    .line 46
    :cond_2
    iget-object v1, p0, Lcom/chase/sig/android/activity/a/f;->e:Landroid/os/Bundle;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/chase/sig/android/activity/a/f;->e:Landroid/os/Bundle;

    invoke-virtual {v1}, Landroid/os/Bundle;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 47
    iget-object v1, p0, Lcom/chase/sig/android/activity/a/f;->e:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 50
    :cond_3
    iget-object v1, p0, Lcom/chase/sig/android/activity/a/f;->a:Lcom/chase/sig/android/activity/eb;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/activity/eb;->startActivity(Landroid/content/Intent;)V

    .line 51
    iget-boolean v0, p0, Lcom/chase/sig/android/activity/a/f;->c:Z

    if-eqz v0, :cond_4

    .line 52
    iget-object v0, p0, Lcom/chase/sig/android/activity/a/f;->a:Lcom/chase/sig/android/activity/eb;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/eb;->finish()V

    .line 54
    :cond_4
    return-void
.end method
