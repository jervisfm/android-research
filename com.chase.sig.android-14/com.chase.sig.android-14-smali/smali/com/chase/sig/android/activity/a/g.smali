.class public final Lcom/chase/sig/android/activity/a/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final a:Lcom/chase/sig/android/view/detail/DetailView;

.field private final b:[Ljava/lang/String;


# direct methods
.method public varargs constructor <init>(Lcom/chase/sig/android/view/detail/DetailView;[Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object p1, p0, Lcom/chase/sig/android/activity/a/g;->a:Lcom/chase/sig/android/view/detail/DetailView;

    .line 13
    iput-object p2, p0, Lcom/chase/sig/android/activity/a/g;->b:[Ljava/lang/String;

    .line 14
    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 6
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 19
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    iget-object v2, p0, Lcom/chase/sig/android/activity/a/g;->b:[Ljava/lang/String;

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 20
    iget-object v5, p0, Lcom/chase/sig/android/activity/a/g;->a:Lcom/chase/sig/android/view/detail/DetailView;

    invoke-virtual {v5, v4}, Lcom/chase/sig/android/view/detail/DetailView;->e(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v4

    invoke-virtual {v4}, Lcom/chase/sig/android/view/detail/a;->b()Lcom/chase/sig/android/view/detail/a;

    .line 19
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 23
    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/a/g;->a:Lcom/chase/sig/android/view/detail/DetailView;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/DetailView;->c()V

    .line 24
    iget-object v0, p0, Lcom/chase/sig/android/activity/a/g;->a:Lcom/chase/sig/android/view/detail/DetailView;

    iget-object v2, p0, Lcom/chase/sig/android/activity/a/g;->b:[Ljava/lang/String;

    aget-object v1, v2, v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/detail/DetailView;->e(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/a;->k()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 25
    return-void
.end method
