.class public abstract Lcom/chase/sig/android/activity/ae;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ActivityType:",
        "Lcom/chase/sig/android/activity/eb;",
        "Params:",
        "Ljava/lang/Object;",
        "Progress:",
        "Ljava/lang/Object;",
        "Result:",
        "Ljava/lang/Object;",
        ">",
        "Landroid/os/AsyncTask",
        "<TParams;TProgress;TResult;>;"
    }
.end annotation


# instance fields
.field private a:I

.field public b:Lcom/chase/sig/android/activity/eb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TActivityType;"
        }
    .end annotation
.end field

.field c:Lcom/chase/sig/android/activity/mc;

.field public d:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 16
    const/4 v0, 0x0

    iput v0, p0, Lcom/chase/sig/android/activity/ae;->a:I

    return-void
.end method


# virtual methods
.method protected varargs abstract a([Ljava/lang/Object;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TParams;)TResult;"
        }
    .end annotation
.end method

.method public final a(Lcom/chase/sig/android/activity/eb;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TActivityType;)V"
        }
    .end annotation

    .prologue
    .line 23
    iput-object p1, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    .line 24
    return-void
.end method

.method public final a(Lcom/chase/sig/android/activity/mc;)V
    .locals 0
    .parameter

    .prologue
    .line 31
    iput-object p1, p0, Lcom/chase/sig/android/activity/ae;->c:Lcom/chase/sig/android/activity/mc;

    .line 32
    return-void
.end method

.method protected a(Ljava/lang/Object;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TResult;)V"
        }
    .end annotation

    .prologue
    .line 76
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 39
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/chase/sig/android/activity/ae;->d:Z

    .line 40
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->c:Lcom/chase/sig/android/activity/mc;

    if-eqz v0, :cond_0

    .line 41
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->c:Lcom/chase/sig/android/activity/mc;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    iget-object v0, v0, Lcom/chase/sig/android/activity/mc;->a:Ljava/util/Hashtable;

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    :cond_0
    return-void
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 50
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/ae;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected varargs doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TParams;)TResult;"
        }
    .end annotation

    .prologue
    .line 60
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/chase/sig/android/activity/ae;->a([Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lcom/chase/sig/android/service/JPService$CrossSiteRequestForgeryTokenFailureException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/chase/sig/android/util/ChaseException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 67
    :goto_0
    return-object v0

    .line 63
    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/eb;->d(Z)V

    .line 67
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x1

    return v0
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 120
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/chase/sig/android/activity/ae;->d:Z

    .line 121
    return-void
.end method

.method public onPostExecute(Ljava/lang/Object;)V
    .locals 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TResult;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 80
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 82
    iget-boolean v2, p0, Lcom/chase/sig/android/activity/ae;->d:Z

    if-eqz v2, :cond_1

    .line 83
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Application has told %s to stop."

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v3, p0, Lcom/chase/sig/android/activity/ae;->d:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 98
    :cond_0
    :goto_0
    return-void

    .line 87
    :cond_1
    iget-object v2, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    iget-boolean v2, v2, Lcom/chase/sig/android/activity/eb;->g:Z

    if-eqz v2, :cond_3

    .line 88
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/ae;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 91
    new-array v2, v0, [Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    .line 93
    iget v2, p0, Lcom/chase/sig/android/activity/ae;->a:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/chase/sig/android/activity/ae;->a:I

    iget v2, p0, Lcom/chase/sig/android/activity/ae;->a:I

    const/16 v3, 0x2c6

    if-le v2, v3, :cond_2

    :goto_1
    if-nez v0, :cond_0

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/chase/sig/android/activity/af;

    invoke-direct {v1, p0, p1}, Lcom/chase/sig/android/activity/af;-><init>(Lcom/chase/sig/android/activity/ae;Ljava/lang/Object;)V

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    .line 97
    :cond_3
    invoke-virtual {p0, p1}, Lcom/chase/sig/android/activity/ae;->a(Ljava/lang/Object;)V

    goto :goto_0
.end method
