.class public final Lcom/chase/sig/android/activity/ag;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/chase/sig/android/activity/eb;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/os/Bundle;)Lcom/chase/sig/android/service/QuoteNewsResponse;
    .locals 1
    .parameter

    .prologue
    .line 30
    const-string v0, "quote_news_response"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/service/QuoteNewsResponse;

    return-object v0
.end method


# virtual methods
.method public final varargs a(Lcom/chase/sig/android/activity/eb;Lcom/chase/sig/android/service/QuoteNewsResponse;Lcom/chase/sig/android/view/detail/DetailView;Ljava/lang/Class;[Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lcom/chase/sig/android/service/QuoteNewsResponse;",
            "Lcom/chase/sig/android/view/detail/DetailView;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/chase/sig/android/activity/ae",
            "<*",
            "Ljava/lang/String;",
            "**>;>;[",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 36
    invoke-virtual {p3}, Lcom/chase/sig/android/view/detail/DetailView;->d()V

    .line 38
    if-nez p2, :cond_0

    .line 39
    invoke-virtual {p1, p4, p5}, Lcom/chase/sig/android/activity/eb;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 44
    :goto_0
    return-void

    .line 41
    :cond_0
    invoke-virtual {p0, p1, p3, p2}, Lcom/chase/sig/android/activity/ag;->a(Lcom/chase/sig/android/activity/eb;Lcom/chase/sig/android/view/detail/DetailView;Lcom/chase/sig/android/service/QuoteNewsResponse;)V

    goto :goto_0
.end method

.method public final a(Lcom/chase/sig/android/activity/eb;Lcom/chase/sig/android/view/detail/DetailView;Lcom/chase/sig/android/service/QuoteNewsResponse;)V
    .locals 10
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lcom/chase/sig/android/view/detail/DetailView;",
            "Lcom/chase/sig/android/service/QuoteNewsResponse;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 49
    invoke-virtual {p3}, Lcom/chase/sig/android/service/QuoteNewsResponse;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    new-array v1, v9, [Lcom/chase/sig/android/view/detail/s;

    new-instance v2, Lcom/chase/sig/android/view/detail/s;

    invoke-virtual {p3}, Lcom/chase/sig/android/service/QuoteNewsResponse;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "20165"

    invoke-virtual {p3, v0}, Lcom/chase/sig/android/service/QuoteNewsResponse;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "20165"

    invoke-virtual {p3, v0}, Lcom/chase/sig/android/service/QuoteNewsResponse;->d(Ljava/lang/String;)Lcom/chase/sig/android/service/IServiceError;

    move-result-object v0

    invoke-interface {v0}, Lcom/chase/sig/android/service/IServiceError;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {v2, v0}, Lcom/chase/sig/android/view/detail/s;-><init>(Ljava/lang/String;)V

    aput-object v2, v1, v8

    move-object v0, v1

    .line 53
    :goto_1
    invoke-virtual {p2, v0}, Lcom/chase/sig/android/view/detail/DetailView;->setRows([Lcom/chase/sig/android/view/detail/a;)V

    .line 54
    invoke-virtual {p2, v8}, Lcom/chase/sig/android/view/detail/DetailView;->setVisibility(I)V

    .line 55
    return-void

    .line 49
    :cond_0
    invoke-virtual {p1}, Lcom/chase/sig/android/activity/eb;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0702ca

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {p3}, Lcom/chase/sig/android/service/QuoteNewsResponse;->a()Ljava/util/ArrayList;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/QuoteNewsArticle;

    if-eqz v0, :cond_2

    new-instance v3, Lcom/chase/sig/android/view/detail/t;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuoteNewsArticle;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, " "

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuoteNewsArticle;->b()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/chase/sig/android/util/s;->s(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuoteNewsArticle;->d()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/chase/sig/android/util/s;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v5, v6}, Lcom/chase/sig/android/util/s;->a(Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/chase/sig/android/view/detail/t;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-boolean v9, v3, Lcom/chase/sig/android/view/detail/a;->m:Z

    new-instance v4, Lcom/chase/sig/android/activity/ah;

    invoke-direct {v4, p0, p1, v0}, Lcom/chase/sig/android/activity/ah;-><init>(Lcom/chase/sig/android/activity/ag;Lcom/chase/sig/android/activity/eb;Lcom/chase/sig/android/domain/QuoteNewsArticle;)V

    iput-object v4, v3, Lcom/chase/sig/android/view/detail/a;->h:Landroid/view/View$OnClickListener;

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_3
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/chase/sig/android/view/detail/t;

    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/chase/sig/android/view/detail/t;

    goto/16 :goto_1
.end method
