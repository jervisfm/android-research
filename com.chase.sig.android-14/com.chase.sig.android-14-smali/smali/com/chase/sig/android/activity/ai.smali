.class public abstract Lcom/chase/sig/android/activity/ai;
.super Lcom/chase/sig/android/activity/eb;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/chase/sig/android/activity/eb;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .parameter

    .prologue
    .line 12
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/eb;->onCreate(Landroid/os/Bundle;)V

    .line 14
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/ChaseApplication;

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    .line 16
    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->a:Lcom/chase/sig/android/domain/a;

    if-nez v0, :cond_1

    .line 17
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/ai;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/chase/sig/android/activity/HomeActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 18
    const/high16 v1, 0x1000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 19
    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/ai;->startActivity(Landroid/content/Intent;)V

    .line 21
    :cond_1
    return-void
.end method
