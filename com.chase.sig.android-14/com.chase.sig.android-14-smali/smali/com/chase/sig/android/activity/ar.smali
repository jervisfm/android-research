.class final Lcom/chase/sig/android/activity/ar;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/BillPayHomeActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/BillPayHomeActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 96
    iput-object p1, p0, Lcom/chase/sig/android/activity/ar;->a:Lcom/chase/sig/android/activity/BillPayHomeActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 4
    .parameter

    .prologue
    .line 99
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ar;->a:Lcom/chase/sig/android/activity/BillPayHomeActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/BillPayHomeActivity;->z()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->t()Z

    move-result v0

    if-nez v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/chase/sig/android/activity/ar;->a:Lcom/chase/sig/android/activity/BillPayHomeActivity;

    const v1, 0x7f070174

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/BillPayHomeActivity;->g(I)V

    .line 124
    :goto_0
    return-void

    .line 101
    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/ar;->a:Lcom/chase/sig/android/activity/BillPayHomeActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/BillPayHomeActivity;->s()Lcom/chase/sig/android/service/SplashResponse;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/chase/sig/android/activity/ar;->a:Lcom/chase/sig/android/activity/BillPayHomeActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/BillPayHomeActivity;->s()Lcom/chase/sig/android/service/SplashResponse;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/service/SplashResponse;->m()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 103
    iget-object v0, p0, Lcom/chase/sig/android/activity/ar;->a:Lcom/chase/sig/android/activity/BillPayHomeActivity;

    iget-object v1, p0, Lcom/chase/sig/android/activity/ar;->a:Lcom/chase/sig/android/activity/BillPayHomeActivity;

    invoke-virtual {v1}, Lcom/chase/sig/android/activity/BillPayHomeActivity;->s()Lcom/chase/sig/android/service/SplashResponse;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/service/SplashResponse;->u()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/BillPayHomeActivity;->f(Ljava/lang/String;)V

    goto :goto_0

    .line 105
    :cond_1
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/chase/sig/android/activity/ar;->a:Lcom/chase/sig/android/activity/BillPayHomeActivity;

    const-class v2, Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 108
    const/high16 v1, 0x4000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 109
    const-string v1, "selectedAccountId"

    iget-object v2, p0, Lcom/chase/sig/android/activity/ar;->a:Lcom/chase/sig/android/activity/BillPayHomeActivity;

    invoke-static {v2}, Lcom/chase/sig/android/activity/BillPayHomeActivity;->a(Lcom/chase/sig/android/activity/BillPayHomeActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 113
    iget-object v1, p0, Lcom/chase/sig/android/activity/ar;->a:Lcom/chase/sig/android/activity/BillPayHomeActivity;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/activity/BillPayHomeActivity;->getPreferences(I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 114
    const-string v2, "photoBillPayPayeeShouldShowInstructions"

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 116
    if-eqz v1, :cond_2

    .line 117
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 118
    const-string v2, "FORWARDING_INTENT_KEY"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 119
    iget-object v0, p0, Lcom/chase/sig/android/activity/ar;->a:Lcom/chase/sig/android/activity/BillPayHomeActivity;

    const/16 v2, 0x2a

    invoke-virtual {v0, v2, v1}, Lcom/chase/sig/android/activity/BillPayHomeActivity;->showDialog(ILandroid/os/Bundle;)Z

    goto :goto_0

    .line 121
    :cond_2
    iget-object v1, p0, Lcom/chase/sig/android/activity/ar;->a:Lcom/chase/sig/android/activity/BillPayHomeActivity;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/activity/BillPayHomeActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
