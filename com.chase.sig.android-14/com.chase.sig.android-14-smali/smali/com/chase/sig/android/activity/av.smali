.class final Lcom/chase/sig/android/activity/av;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/domain/Payee;

.field final synthetic b:Lcom/chase/sig/android/activity/BillPayHomeActivity$a;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/BillPayHomeActivity$a;Lcom/chase/sig/android/domain/Payee;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 363
    iput-object p1, p0, Lcom/chase/sig/android/activity/av;->b:Lcom/chase/sig/android/activity/BillPayHomeActivity$a;

    iput-object p2, p0, Lcom/chase/sig/android/activity/av;->a:Lcom/chase/sig/android/domain/Payee;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3
    .parameter

    .prologue
    .line 368
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/chase/sig/android/activity/av;->b:Lcom/chase/sig/android/activity/BillPayHomeActivity$a;

    iget-object v1, v1, Lcom/chase/sig/android/activity/BillPayHomeActivity$a;->a:Lcom/chase/sig/android/activity/BillPayHomeActivity;

    const-class v2, Lcom/chase/sig/android/activity/BillPayAddStartActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 370
    const-string v1, "payee"

    iget-object v2, p0, Lcom/chase/sig/android/activity/av;->a:Lcom/chase/sig/android/domain/Payee;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 371
    const-string v1, "selectedAccountId"

    iget-object v2, p0, Lcom/chase/sig/android/activity/av;->b:Lcom/chase/sig/android/activity/BillPayHomeActivity$a;

    iget-object v2, v2, Lcom/chase/sig/android/activity/BillPayHomeActivity$a;->a:Lcom/chase/sig/android/activity/BillPayHomeActivity;

    invoke-static {v2}, Lcom/chase/sig/android/activity/BillPayHomeActivity;->a(Lcom/chase/sig/android/activity/BillPayHomeActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 373
    const-string v1, "payeeCount"

    iget-object v2, p0, Lcom/chase/sig/android/activity/av;->b:Lcom/chase/sig/android/activity/BillPayHomeActivity$a;

    invoke-static {v2}, Lcom/chase/sig/android/activity/BillPayHomeActivity$a;->a(Lcom/chase/sig/android/activity/BillPayHomeActivity$a;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 374
    iget-object v1, p0, Lcom/chase/sig/android/activity/av;->b:Lcom/chase/sig/android/activity/BillPayHomeActivity$a;

    iget-object v1, v1, Lcom/chase/sig/android/activity/BillPayHomeActivity$a;->a:Lcom/chase/sig/android/activity/BillPayHomeActivity;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/activity/BillPayHomeActivity;->startActivity(Landroid/content/Intent;)V

    .line 375
    return-void
.end method
