.class final Lcom/chase/sig/android/activity/ay;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 116
    iput-object p1, p0, Lcom/chase/sig/android/activity/ay;->a:Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3
    .parameter

    .prologue
    .line 121
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ay;->a:Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Lcom/chase/sig/android/activity/ay;->a:Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->a(Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ay;->a:Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->b(Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;)Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->c()Lcom/chase/sig/android/domain/MerchantPayee;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/MerchantPayee;->a(Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/chase/sig/android/activity/ay;->a:Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;

    const-class v2, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "payee_info"

    iget-object v2, p0, Lcom/chase/sig/android/activity/ay;->a:Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;

    invoke-static {v2}, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->b(Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;)Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v1, "from_merchant_directory"

    iget-object v2, p0, Lcom/chase/sig/android/activity/ay;->a:Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;

    invoke-static {v2}, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->b(Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;)Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->a()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object v1, p0, Lcom/chase/sig/android/activity/ay;->a:Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;

    const/16 v2, 0xa

    invoke-virtual {v1, v0, v2}, Lcom/chase/sig/android/activity/BillPayPayeeAddActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 124
    :cond_0
    return-void
.end method
