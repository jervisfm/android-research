.class public abstract Lcom/chase/sig/android/activity/b;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$c;


# instance fields
.field protected a:Lcom/chase/sig/android/domain/ChartImageCache;

.field private final b:I

.field private final c:I


# direct methods
.method protected constructor <init>(II)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    .line 24
    new-instance v0, Lcom/chase/sig/android/domain/ChartImageCache;

    invoke-direct {v0}, Lcom/chase/sig/android/domain/ChartImageCache;-><init>()V

    iput-object v0, p0, Lcom/chase/sig/android/activity/b;->a:Lcom/chase/sig/android/domain/ChartImageCache;

    .line 25
    iput p1, p0, Lcom/chase/sig/android/activity/b;->b:I

    .line 26
    iput p2, p0, Lcom/chase/sig/android/activity/b;->c:I

    .line 27
    return-void
.end method

.method private a(Landroid/graphics/Bitmap;)V
    .locals 1
    .parameter

    .prologue
    .line 53
    iget v0, p0, Lcom/chase/sig/android/activity/b;->c:I

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/b;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 54
    return-void
.end method

.method private a(Landroid/graphics/Bitmap;Lcom/chase/sig/android/domain/Chart;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/chase/sig/android/activity/b;->a(Landroid/graphics/Bitmap;)V

    .line 49
    iget-object v0, p0, Lcom/chase/sig/android/activity/b;->a:Lcom/chase/sig/android/domain/ChartImageCache;

    invoke-virtual {v0, p2, p1}, Lcom/chase/sig/android/domain/ChartImageCache;->a(Lcom/chase/sig/android/domain/Chart;Landroid/graphics/Bitmap;)V

    .line 50
    return-void
.end method


# virtual methods
.method public final a(Lcom/chase/sig/android/domain/Chart;)V
    .locals 0
    .parameter

    .prologue
    .line 38
    invoke-virtual {p0, p1}, Lcom/chase/sig/android/activity/b;->b(Lcom/chase/sig/android/domain/Chart;)V

    .line 39
    return-void
.end method

.method protected final a(Lcom/chase/sig/android/domain/Chart;Lcom/chase/sig/android/domain/ImageDownloadResponse;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 67
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 68
    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    invoke-virtual {p0, v0, p1}, Lcom/chase/sig/android/activity/b;->a(Ljava/util/Map;Lcom/chase/sig/android/domain/Chart;)V

    .line 70
    return-void
.end method

.method protected a(Ljava/util/Map;Lcom/chase/sig/android/domain/Chart;)V
    .locals 5
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/chase/sig/android/domain/Chart;",
            "Lcom/chase/sig/android/domain/ImageDownloadResponse;",
            ">;",
            "Lcom/chase/sig/android/domain/Chart;",
            ")V"
        }
    .end annotation

    .prologue
    .line 73
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 74
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/domain/Chart;

    .line 75
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/ImageDownloadResponse;

    .line 76
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/ImageDownloadResponse;->b()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 77
    iget-object v3, p0, Lcom/chase/sig/android/activity/b;->a:Lcom/chase/sig/android/domain/ChartImageCache;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/ImageDownloadResponse;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v3, v1, v0}, Lcom/chase/sig/android/domain/ChartImageCache;->a(Lcom/chase/sig/android/domain/Chart;Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 79
    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/b;->a:Lcom/chase/sig/android/domain/ChartImageCache;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/b;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget v4, p0, Lcom/chase/sig/android/activity/b;->b:I

    invoke-static {v3, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lcom/chase/sig/android/domain/ChartImageCache;->a(Lcom/chase/sig/android/domain/Chart;Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 84
    :cond_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/b;->a:Lcom/chase/sig/android/domain/ChartImageCache;

    invoke-virtual {v0, p2}, Lcom/chase/sig/android/domain/ChartImageCache;->b(Lcom/chase/sig/android/domain/Chart;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/chase/sig/android/activity/b;->a(Landroid/graphics/Bitmap;)V

    .line 85
    return-void
.end method

.method protected final a(Lcom/chase/sig/android/domain/ImageDownloadResponse;Lcom/chase/sig/android/domain/Chart;)Z
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 57
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/ImageDownloadResponse;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 58
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/ImageDownloadResponse;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/chase/sig/android/activity/b;->a(Landroid/graphics/Bitmap;Lcom/chase/sig/android/domain/Chart;)V

    .line 59
    const/4 v0, 0x1

    .line 63
    :goto_0
    return v0

    .line 61
    :cond_0
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/b;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, Lcom/chase/sig/android/activity/b;->b:I

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/chase/sig/android/activity/b;->a(Landroid/graphics/Bitmap;Lcom/chase/sig/android/domain/Chart;)V

    .line 63
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final b(Landroid/os/Bundle;)V
    .locals 1
    .parameter

    .prologue
    .line 30
    const-string v0, "chart_images"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/ChartImageCache;

    iput-object v0, p0, Lcom/chase/sig/android/activity/b;->a:Lcom/chase/sig/android/domain/ChartImageCache;

    .line 31
    return-void
.end method

.method protected final b(Lcom/chase/sig/android/domain/Chart;)V
    .locals 1
    .parameter

    .prologue
    .line 42
    iget-object v0, p0, Lcom/chase/sig/android/activity/b;->a:Lcom/chase/sig/android/domain/ChartImageCache;

    invoke-virtual {v0, p1}, Lcom/chase/sig/android/domain/ChartImageCache;->a(Lcom/chase/sig/android/domain/Chart;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 43
    iget-object v0, p0, Lcom/chase/sig/android/activity/b;->a:Lcom/chase/sig/android/domain/ChartImageCache;

    invoke-virtual {v0, p1}, Lcom/chase/sig/android/domain/ChartImageCache;->b(Lcom/chase/sig/android/domain/Chart;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/chase/sig/android/activity/b;->a(Landroid/graphics/Bitmap;)V

    .line 45
    :cond_0
    return-void
.end method

.method protected final c(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 34
    const-string v0, "chart_images"

    iget-object v1, p0, Lcom/chase/sig/android/activity/b;->a:Lcom/chase/sig/android/domain/ChartImageCache;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 35
    return-void
.end method
