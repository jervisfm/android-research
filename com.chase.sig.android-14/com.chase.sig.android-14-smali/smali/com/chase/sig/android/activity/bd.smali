.class public final Lcom/chase/sig/android/activity/bd;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/l;
    .locals 2
    .parameter

    .prologue
    .line 37
    new-instance v0, Lcom/chase/sig/android/view/detail/l;

    const v1, 0x7f070098

    invoke-direct {v0, v1, p0}, Lcom/chase/sig/android/view/detail/l;-><init>(ILjava/lang/String;)V

    const-string v1, "BILLPAY_MESSAGE"

    iput-object v1, v0, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v0, Lcom/chase/sig/android/view/detail/l;

    const v1, 0x7f070099

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/detail/l;->c(I)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/l;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/chase/sig/android/view/detail/a;->p:Z

    check-cast v0, Lcom/chase/sig/android/view/detail/l;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/l;->q()Lcom/chase/sig/android/view/detail/l;

    move-result-object v0

    const/16 v1, 0x20

    iput v1, v0, Lcom/chase/sig/android/view/detail/l;->a:I

    return-object v0
.end method

.method public static b(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/l;
    .locals 2
    .parameter

    .prologue
    .line 50
    new-instance v0, Lcom/chase/sig/android/view/detail/l;

    const v1, 0x7f070097

    invoke-direct {v0, v1, p0}, Lcom/chase/sig/android/view/detail/l;-><init>(ILjava/lang/String;)V

    const-string v1, "PHONE_NUMBER"

    iput-object v1, v0, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v0, Lcom/chase/sig/android/view/detail/l;

    const v1, 0x7f07007f

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/detail/l;->c(I)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/l;

    const/4 v1, 0x5

    iput v1, v0, Lcom/chase/sig/android/view/detail/l;->u:I

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/l;->c()Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/l;

    return-object v0
.end method

.method public static c(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/l;
    .locals 2
    .parameter

    .prologue
    .line 62
    new-instance v0, Lcom/chase/sig/android/view/detail/l;

    const v1, 0x7f07008c

    invoke-direct {v0, v1, p0}, Lcom/chase/sig/android/view/detail/l;-><init>(ILjava/lang/String;)V

    const-string v1, "ZIP_CODE"

    iput-object v1, v0, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v0, Lcom/chase/sig/android/view/detail/l;

    const-string v1, "zipCode"

    iput-object v1, v0, Lcom/chase/sig/android/view/detail/a;->r:Ljava/lang/String;

    check-cast v0, Lcom/chase/sig/android/view/detail/l;

    const v1, 0x7f07008d

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/detail/l;->c(I)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/l;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/chase/sig/android/view/detail/a;->o:Z

    check-cast v0, Lcom/chase/sig/android/view/detail/l;

    const/4 v1, 0x4

    iput v1, v0, Lcom/chase/sig/android/view/detail/l;->u:I

    const/16 v1, 0xa

    iput v1, v0, Lcom/chase/sig/android/view/detail/l;->a:I

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/l;->c()Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/l;

    return-object v0
.end method

.method public static d(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/l;
    .locals 2
    .parameter

    .prologue
    .line 106
    new-instance v0, Lcom/chase/sig/android/view/detail/l;

    const v1, 0x7f07008f

    invoke-direct {v0, v1, p0}, Lcom/chase/sig/android/view/detail/l;-><init>(ILjava/lang/String;)V

    const-string v1, "CITY"

    iput-object v1, v0, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v0, Lcom/chase/sig/android/view/detail/l;

    const v1, 0x7f070090

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/detail/l;->c(I)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/l;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/chase/sig/android/view/detail/a;->o:Z

    check-cast v0, Lcom/chase/sig/android/view/detail/l;

    const-string v1, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890 \'.,-"

    iput-object v1, v0, Lcom/chase/sig/android/view/detail/l;->v:Ljava/lang/String;

    const/16 v1, 0x1e

    iput v1, v0, Lcom/chase/sig/android/view/detail/l;->a:I

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/l;->c()Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/l;

    return-object v0
.end method

.method public static e(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/l;
    .locals 2
    .parameter

    .prologue
    .line 120
    new-instance v0, Lcom/chase/sig/android/view/detail/l;

    const v1, 0x7f070092

    invoke-direct {v0, v1, p0}, Lcom/chase/sig/android/view/detail/l;-><init>(ILjava/lang/String;)V

    const-string v1, "ADDRESS2"

    iput-object v1, v0, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v0, Lcom/chase/sig/android/view/detail/l;

    const v1, 0x7f070093

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/detail/l;->c(I)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/l;

    const-string v1, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890  .,:/$&#\'-"

    iput-object v1, v0, Lcom/chase/sig/android/view/detail/l;->v:Ljava/lang/String;

    const/16 v1, 0x20

    iput v1, v0, Lcom/chase/sig/android/view/detail/l;->a:I

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/l;->c()Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/l;

    return-object v0
.end method

.method public static f(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/l;
    .locals 2
    .parameter

    .prologue
    .line 133
    new-instance v0, Lcom/chase/sig/android/view/detail/l;

    const v1, 0x7f070092

    invoke-direct {v0, v1, p0}, Lcom/chase/sig/android/view/detail/l;-><init>(ILjava/lang/String;)V

    const-string v1, "ADDRESS1"

    iput-object v1, v0, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v0, Lcom/chase/sig/android/view/detail/l;

    const v1, 0x7f070096

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/detail/l;->c(I)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/l;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/chase/sig/android/view/detail/a;->o:Z

    check-cast v0, Lcom/chase/sig/android/view/detail/l;

    const-string v1, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890  .,:/$&#\'-"

    iput-object v1, v0, Lcom/chase/sig/android/view/detail/l;->v:Ljava/lang/String;

    const/16 v1, 0x20

    iput v1, v0, Lcom/chase/sig/android/view/detail/l;->a:I

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/l;->c()Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/l;

    return-object v0
.end method

.method public static g(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/l;
    .locals 2
    .parameter

    .prologue
    .line 147
    new-instance v0, Lcom/chase/sig/android/view/detail/l;

    const v1, 0x7f07008e

    invoke-direct {v0, v1, p0}, Lcom/chase/sig/android/view/detail/l;-><init>(ILjava/lang/String;)V

    const-string v1, "ACCOUNT_NUMBER"

    iput-object v1, v0, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v0, Lcom/chase/sig/android/view/detail/l;

    const v1, 0x7f070080

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/detail/l;->c(I)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/l;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/chase/sig/android/view/detail/a;->p:Z

    check-cast v0, Lcom/chase/sig/android/view/detail/l;

    const/4 v1, 0x3

    iput v1, v0, Lcom/chase/sig/android/view/detail/l;->u:I

    const/16 v1, 0x20

    iput v1, v0, Lcom/chase/sig/android/view/detail/l;->a:I

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/l;->c()Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/l;

    return-object v0
.end method

.method public static h(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/l;
    .locals 2
    .parameter

    .prologue
    .line 161
    new-instance v0, Lcom/chase/sig/android/view/detail/l;

    const v1, 0x7f070197

    invoke-direct {v0, v1, p0}, Lcom/chase/sig/android/view/detail/l;-><init>(ILjava/lang/String;)V

    const-string v1, "PAYEE_NICKNAME"

    iput-object v1, v0, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v0, Lcom/chase/sig/android/view/detail/l;

    const v1, 0x7f07007f

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/detail/l;->c(I)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/l;

    const/16 v1, 0x20

    iput v1, v0, Lcom/chase/sig/android/view/detail/l;->a:I

    const-string v1, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890 .,&()-/\'"

    iput-object v1, v0, Lcom/chase/sig/android/view/detail/l;->v:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/l;->c()Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/l;

    return-object v0
.end method

.method public static i(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/l;
    .locals 2
    .parameter

    .prologue
    .line 174
    new-instance v0, Lcom/chase/sig/android/view/detail/l;

    const v1, 0x7f070091

    invoke-direct {v0, v1, p0}, Lcom/chase/sig/android/view/detail/l;-><init>(ILjava/lang/String;)V

    const-string v1, "PAYEE"

    iput-object v1, v0, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v0, Lcom/chase/sig/android/view/detail/l;

    const v1, 0x7f070196

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/detail/l;->c(I)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/l;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/chase/sig/android/view/detail/a;->o:Z

    check-cast v0, Lcom/chase/sig/android/view/detail/l;

    const/16 v1, 0x20

    iput v1, v0, Lcom/chase/sig/android/view/detail/l;->a:I

    const-string v1, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890 .,&()-/\'"

    iput-object v1, v0, Lcom/chase/sig/android/view/detail/l;->v:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/l;->c()Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/l;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/chase/sig/android/domain/State;)Lcom/chase/sig/android/view/detail/q;
    .locals 2
    .parameter

    .prologue
    .line 78
    const/4 v0, -0x1

    .line 79
    if-eqz p1, :cond_0

    .line 80
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/State;->ordinal()I

    move-result v0

    move v1, v0

    .line 83
    :goto_0
    new-instance v0, Lcom/chase/sig/android/activity/be;

    invoke-direct {v0, p0, v1}, Lcom/chase/sig/android/activity/be;-><init>(Lcom/chase/sig/android/activity/bd;I)V

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/chase/sig/android/view/detail/a;->o:Z

    check-cast v0, Lcom/chase/sig/android/view/detail/q;

    const-string v1, "STATE"

    iput-object v1, v0, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v0, Lcom/chase/sig/android/view/detail/q;

    const-string v1, "state"

    iput-object v1, v0, Lcom/chase/sig/android/view/detail/a;->r:Ljava/lang/String;

    check-cast v0, Lcom/chase/sig/android/view/detail/q;

    const v1, 0x7f070095

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/detail/q;->c(I)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/q;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/q;->c()Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/q;

    return-object v0

    :cond_0
    move v1, v0

    goto :goto_0
.end method
