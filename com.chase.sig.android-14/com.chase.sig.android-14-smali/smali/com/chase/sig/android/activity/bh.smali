.class final Lcom/chase/sig/android/activity/bh;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 108
    iput-object p1, p0, Lcom/chase/sig/android/activity/bh;->a:Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3
    .parameter

    .prologue
    .line 113
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/bh;->a:Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 114
    iget-object v0, p0, Lcom/chase/sig/android/activity/bh;->a:Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->a(Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;)V

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/chase/sig/android/activity/bh;->a:Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;

    const-class v2, Lcom/chase/sig/android/activity/BillPayPayeeEditVerifyActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "payee"

    iget-object v2, p0, Lcom/chase/sig/android/activity/bh;->a:Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;

    invoke-static {v2}, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->b(Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;)Lcom/chase/sig/android/domain/Payee;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/chase/sig/android/activity/bh;->a:Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;

    const/16 v2, 0xc

    invoke-virtual {v1, v0, v2}, Lcom/chase/sig/android/activity/BillPayPayeeEditActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 116
    :cond_0
    return-void
.end method
