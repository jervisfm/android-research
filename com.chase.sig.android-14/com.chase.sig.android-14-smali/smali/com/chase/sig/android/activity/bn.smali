.class final Lcom/chase/sig/android/activity/bn;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Landroid/widget/Button;

.field final synthetic b:Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;Landroid/widget/Button;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 68
    iput-object p1, p0, Lcom/chase/sig/android/activity/bn;->b:Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;

    iput-object p2, p0, Lcom/chase/sig/android/activity/bn;->a:Landroid/widget/Button;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 5
    .parameter

    .prologue
    const v4, 0x7f090034

    const/4 v3, 0x0

    .line 72
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/bn;->b:Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;

    const v1, 0x7f09003d

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 74
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 75
    iget-object v0, p0, Lcom/chase/sig/android/activity/bn;->a:Landroid/widget/Button;

    const-string v2, "Help"

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 76
    iget-object v0, p0, Lcom/chase/sig/android/activity/bn;->b:Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;

    invoke-virtual {v0, v4}, Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 77
    const v2, 0x7f0701a2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 78
    const/4 v0, 0x4

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 79
    iget-object v0, p0, Lcom/chase/sig/android/activity/bn;->b:Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;->a(Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;)Landroid/widget/ImageView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 87
    :goto_0
    return-void

    .line 81
    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/bn;->a:Landroid/widget/Button;

    const-string v2, "Done"

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 82
    iget-object v0, p0, Lcom/chase/sig/android/activity/bn;->b:Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;

    invoke-virtual {v0, v4}, Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 83
    const v2, 0x7f0701a3

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 84
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 85
    iget-object v0, p0, Lcom/chase/sig/android/activity/bn;->b:Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;->a(Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setEnabled(Z)V

    goto :goto_0
.end method
