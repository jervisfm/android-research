.class final Lcom/chase/sig/android/activity/bo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/CapturePreview;

.field final synthetic b:Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;Lcom/chase/sig/android/activity/CapturePreview;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 92
    iput-object p1, p0, Lcom/chase/sig/android/activity/bo;->b:Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;

    iput-object p2, p0, Lcom/chase/sig/android/activity/bo;->a:Lcom/chase/sig/android/activity/CapturePreview;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 2
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 97
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/bo;->b:Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;->a(Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 98
    iget-object v0, p0, Lcom/chase/sig/android/activity/bo;->b:Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;->b(Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;)Landroid/widget/FrameLayout;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 99
    iget-object v0, p0, Lcom/chase/sig/android/activity/bo;->b:Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;->b(Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;)Landroid/widget/FrameLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->requestLayout()V

    .line 101
    iget-object v0, p0, Lcom/chase/sig/android/activity/bo;->a:Lcom/chase/sig/android/activity/CapturePreview;

    iget-object v0, v0, Lcom/chase/sig/android/activity/CapturePreview;->b:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v0

    .line 102
    invoke-virtual {v0, v1}, Landroid/hardware/Camera$Parameters;->setRotation(I)V

    .line 103
    iget-object v1, p0, Lcom/chase/sig/android/activity/bo;->a:Lcom/chase/sig/android/activity/CapturePreview;

    iget-object v1, v1, Lcom/chase/sig/android/activity/CapturePreview;->b:Landroid/hardware/Camera;

    invoke-virtual {v1, v0}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    .line 105
    iget-object v0, p0, Lcom/chase/sig/android/activity/bo;->a:Lcom/chase/sig/android/activity/CapturePreview;

    iget-object v0, v0, Lcom/chase/sig/android/activity/CapturePreview;->b:Landroid/hardware/Camera;

    new-instance v1, Lcom/chase/sig/android/activity/bp;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/bp;-><init>(Lcom/chase/sig/android/activity/bo;)V

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->autoFocus(Landroid/hardware/Camera$AutoFocusCallback;)V

    .line 113
    return-void
.end method
