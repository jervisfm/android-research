.class final Lcom/chase/sig/android/activity/bs;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/hardware/Camera$PictureCallback;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 139
    iput-object p1, p0, Lcom/chase/sig/android/activity/bs;->a:Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPictureTaken([BLandroid/hardware/Camera;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 143
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/chase/sig/android/activity/bs;->a:Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;

    const-class v2, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 146
    const-string v1, "image_data"

    invoke-static {p1}, Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;->a([B)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 147
    const-string v1, "selectedAccountId"

    iget-object v2, p0, Lcom/chase/sig/android/activity/bs;->a:Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;

    invoke-static {v2}, Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;->c(Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 148
    const/high16 v1, 0x4000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 149
    iget-object v1, p0, Lcom/chase/sig/android/activity/bs;->a:Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;->startActivity(Landroid/content/Intent;)V

    .line 150
    return-void
.end method
