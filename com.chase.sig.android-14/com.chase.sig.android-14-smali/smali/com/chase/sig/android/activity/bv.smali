.class final Lcom/chase/sig/android/activity/bv;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 155
    iput-object p1, p0, Lcom/chase/sig/android/activity/bv;->a:Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3
    .parameter

    .prologue
    .line 159
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/chase/sig/android/activity/bv;->a:Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;

    const-class v2, Lcom/chase/sig/android/activity/BillPayPayeeImageCaptureActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 161
    const/high16 v1, 0x4000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 162
    const-string v1, "selectedAccountId"

    iget-object v2, p0, Lcom/chase/sig/android/activity/bv;->a:Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;

    invoke-static {v2}, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;->c(Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 163
    iget-object v1, p0, Lcom/chase/sig/android/activity/bv;->a:Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/activity/BillPayPayeeImageReviewActivity;->startActivity(Landroid/content/Intent;)V

    .line 164
    return-void
.end method
