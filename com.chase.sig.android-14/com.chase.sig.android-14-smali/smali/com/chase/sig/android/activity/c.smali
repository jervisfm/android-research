.class public abstract Lcom/chase/sig/android/activity/c;
.super Lcom/chase/sig/android/activity/n;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/c$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/chase/sig/android/domain/Transaction;",
        ">",
        "Lcom/chase/sig/android/activity/n",
        "<TT;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/chase/sig/android/activity/n;-><init>()V

    .line 139
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/c;Z)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 31
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/c;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/c;->e()Ljava/lang/Class;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "edit_single"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "transaction_object"

    iget-object v2, p0, Lcom/chase/sig/android/activity/c;->a:Lcom/chase/sig/android/domain/Transaction;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/c;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private b(Z)Landroid/content/DialogInterface$OnClickListener;
    .locals 1
    .parameter

    .prologue
    .line 199
    new-instance v0, Lcom/chase/sig/android/activity/g;

    invoke-direct {v0, p0, p1}, Lcom/chase/sig/android/activity/g;-><init>(Lcom/chase/sig/android/activity/c;Z)V

    return-object v0
.end method


# virtual methods
.method public abstract a(Lcom/chase/sig/android/activity/DetailResource;)I
.end method

.method public a()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 55
    const v0, 0x7f09027e

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 56
    iget-object v1, p0, Lcom/chase/sig/android/activity/c;->a:Lcom/chase/sig/android/domain/Transaction;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/Transaction;->u()Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v2

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 57
    iget-object v1, p0, Lcom/chase/sig/android/activity/c;->a:Lcom/chase/sig/android/domain/Transaction;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/Transaction;->l()Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v1, 0xa

    .line 59
    :goto_1
    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/c;->i(I)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 60
    sget-object v1, Lcom/chase/sig/android/activity/DetailResource;->a:Lcom/chase/sig/android/activity/DetailResource;

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/c;->a(Lcom/chase/sig/android/activity/DetailResource;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 62
    const v0, 0x7f09027f

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 63
    sget-object v1, Lcom/chase/sig/android/activity/DetailResource;->h:Lcom/chase/sig/android/activity/DetailResource;

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/c;->a(Lcom/chase/sig/android/activity/DetailResource;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 64
    iget-object v1, p0, Lcom/chase/sig/android/activity/c;->a:Lcom/chase/sig/android/domain/Transaction;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/Transaction;->v()Z

    move-result v1

    if-eqz v1, :cond_2

    :goto_2
    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 65
    new-instance v1, Lcom/chase/sig/android/activity/d;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/d;-><init>(Lcom/chase/sig/android/activity/c;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 76
    return-void

    :cond_0
    move v1, v3

    .line 56
    goto :goto_0

    .line 57
    :cond_1
    const/16 v1, 0xb

    goto :goto_1

    :cond_2
    move v2, v3

    .line 64
    goto :goto_2
.end method

.method protected abstract a(Lcom/chase/sig/android/view/detail/DetailView;Lcom/chase/sig/android/domain/Transaction;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/chase/sig/android/view/detail/DetailView;",
            "TT;)V"
        }
    .end annotation
.end method

.method protected final a(Z)V
    .locals 2
    .parameter

    .prologue
    .line 46
    const v0, 0x7f090153

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailView;

    .line 47
    iget-object v1, p0, Lcom/chase/sig/android/activity/c;->a:Lcom/chase/sig/android/domain/Transaction;

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/c;->a(Lcom/chase/sig/android/view/detail/DetailView;Lcom/chase/sig/android/domain/Transaction;)V

    .line 48
    return-void
.end method

.method protected abstract b()Lcom/chase/sig/android/service/movemoney/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/chase/sig/android/service/movemoney/d",
            "<TT;>;"
        }
    .end annotation
.end method

.method protected abstract c()Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/chase/sig/android/activity/ae",
            "<*",
            "Ljava/lang/Boolean;",
            "**>;>;"
        }
    .end annotation
.end method

.method protected abstract d()Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/chase/sig/android/activity/eb;",
            ">;"
        }
    .end annotation
.end method

.method protected abstract e()Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/chase/sig/android/activity/eb;",
            ">;"
        }
    .end annotation
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 5
    .parameter

    .prologue
    const/4 v4, -0x1

    const/4 v2, 0x1

    .line 81
    new-instance v0, Lcom/chase/sig/android/view/k$a;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/view/k$a;-><init>(Landroid/content/Context;)V

    .line 83
    packed-switch p1, :pswitch_data_0

    .line 132
    :pswitch_0
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/n;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    .line 85
    :pswitch_1
    iput-boolean v2, v0, Lcom/chase/sig/android/view/k$a;->i:Z

    sget-object v1, Lcom/chase/sig/android/activity/DetailResource;->a:Lcom/chase/sig/android/activity/DetailResource;

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/c;->a(Lcom/chase/sig/android/activity/DetailResource;)I

    move-result v1

    invoke-direct {p0, v2}, Lcom/chase/sig/android/activity/c;->b(Z)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/chase/sig/android/view/k$a;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    move-result-object v1

    sget-object v2, Lcom/chase/sig/android/activity/DetailResource;->b:Lcom/chase/sig/android/activity/DetailResource;

    invoke-virtual {p0, v2}, Lcom/chase/sig/android/activity/c;->a(Lcom/chase/sig/android/activity/DetailResource;)I

    move-result v2

    new-instance v3, Lcom/chase/sig/android/activity/a/b;

    invoke-direct {v3}, Lcom/chase/sig/android/activity/a/b;-><init>()V

    invoke-virtual {v1, v2, v3}, Lcom/chase/sig/android/view/k$a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    move-result-object v1

    sget-object v2, Lcom/chase/sig/android/activity/DetailResource;->c:Lcom/chase/sig/android/activity/DetailResource;

    invoke-virtual {p0, v2}, Lcom/chase/sig/android/activity/c;->a(Lcom/chase/sig/android/activity/DetailResource;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/view/k$a;->b(I)Lcom/chase/sig/android/view/k$a;

    .line 93
    invoke-virtual {v0, v4}, Lcom/chase/sig/android/view/k$a;->d(I)Lcom/chase/sig/android/view/k;

    move-result-object v0

    goto :goto_0

    .line 96
    :pswitch_2
    iput-boolean v2, v0, Lcom/chase/sig/android/view/k$a;->i:Z

    sget-object v1, Lcom/chase/sig/android/activity/DetailResource;->e:Lcom/chase/sig/android/activity/DetailResource;

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/c;->a(Lcom/chase/sig/android/activity/DetailResource;)I

    move-result v1

    invoke-direct {p0, v2}, Lcom/chase/sig/android/activity/c;->b(Z)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/chase/sig/android/view/k$a;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    move-result-object v1

    sget-object v2, Lcom/chase/sig/android/activity/DetailResource;->f:Lcom/chase/sig/android/activity/DetailResource;

    invoke-virtual {p0, v2}, Lcom/chase/sig/android/activity/c;->a(Lcom/chase/sig/android/activity/DetailResource;)I

    move-result v2

    const/4 v3, 0x0

    invoke-direct {p0, v3}, Lcom/chase/sig/android/activity/c;->b(Z)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/chase/sig/android/view/k$a;->c(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    move-result-object v1

    sget-object v2, Lcom/chase/sig/android/activity/DetailResource;->g:Lcom/chase/sig/android/activity/DetailResource;

    invoke-virtual {p0, v2}, Lcom/chase/sig/android/activity/c;->a(Lcom/chase/sig/android/activity/DetailResource;)I

    move-result v2

    new-instance v3, Lcom/chase/sig/android/activity/a/b;

    invoke-direct {v3}, Lcom/chase/sig/android/activity/a/b;-><init>()V

    invoke-virtual {v1, v2, v3}, Lcom/chase/sig/android/view/k$a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    move-result-object v1

    sget-object v2, Lcom/chase/sig/android/activity/DetailResource;->d:Lcom/chase/sig/android/activity/DetailResource;

    invoke-virtual {p0, v2}, Lcom/chase/sig/android/activity/c;->a(Lcom/chase/sig/android/activity/DetailResource;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/view/k$a;->b(I)Lcom/chase/sig/android/view/k$a;

    .line 106
    invoke-virtual {v0, v4}, Lcom/chase/sig/android/view/k$a;->d(I)Lcom/chase/sig/android/view/k;

    move-result-object v0

    goto :goto_0

    .line 109
    :pswitch_3
    iput-boolean v2, v0, Lcom/chase/sig/android/view/k$a;->i:Z

    sget-object v1, Lcom/chase/sig/android/activity/DetailResource;->i:Lcom/chase/sig/android/activity/DetailResource;

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/c;->a(Lcom/chase/sig/android/activity/DetailResource;)I

    move-result v1

    new-instance v2, Lcom/chase/sig/android/activity/f;

    invoke-direct {v2, p0}, Lcom/chase/sig/android/activity/f;-><init>(Lcom/chase/sig/android/activity/c;)V

    invoke-virtual {v0, v1, v2}, Lcom/chase/sig/android/view/k$a;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    move-result-object v1

    sget-object v2, Lcom/chase/sig/android/activity/DetailResource;->j:Lcom/chase/sig/android/activity/DetailResource;

    invoke-virtual {p0, v2}, Lcom/chase/sig/android/activity/c;->a(Lcom/chase/sig/android/activity/DetailResource;)I

    move-result v2

    new-instance v3, Lcom/chase/sig/android/activity/e;

    invoke-direct {v3, p0}, Lcom/chase/sig/android/activity/e;-><init>(Lcom/chase/sig/android/activity/c;)V

    invoke-virtual {v1, v2, v3}, Lcom/chase/sig/android/view/k$a;->c(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    move-result-object v1

    sget-object v2, Lcom/chase/sig/android/activity/DetailResource;->g:Lcom/chase/sig/android/activity/DetailResource;

    invoke-virtual {p0, v2}, Lcom/chase/sig/android/activity/c;->a(Lcom/chase/sig/android/activity/DetailResource;)I

    move-result v2

    new-instance v3, Lcom/chase/sig/android/activity/a/b;

    invoke-direct {v3}, Lcom/chase/sig/android/activity/a/b;-><init>()V

    invoke-virtual {v1, v2, v3}, Lcom/chase/sig/android/view/k$a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    move-result-object v1

    sget-object v2, Lcom/chase/sig/android/activity/DetailResource;->k:Lcom/chase/sig/android/activity/DetailResource;

    invoke-virtual {p0, v2}, Lcom/chase/sig/android/activity/c;->a(Lcom/chase/sig/android/activity/DetailResource;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/view/k$a;->b(I)Lcom/chase/sig/android/view/k$a;

    .line 129
    invoke-virtual {v0, v4}, Lcom/chase/sig/android/view/k$a;->d(I)Lcom/chase/sig/android/view/k;

    move-result-object v0

    goto/16 :goto_0

    .line 83
    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
