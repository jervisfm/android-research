.class final Lcom/chase/sig/android/activity/cb;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 102
    iput-object p1, p0, Lcom/chase/sig/android/activity/cb;->a:Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 106
    :try_start_0
    new-instance v2, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;

    invoke-direct {v2}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;-><init>()V

    .line 108
    iget-object v0, p0, Lcom/chase/sig/android/activity/cb;->a:Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->a(Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;)Landroid/widget/SimpleAdapter;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/widget/SimpleAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    const-string v1, "optionId"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 111
    iget-object v1, p0, Lcom/chase/sig/android/activity/cb;->a:Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;

    invoke-static {v1}, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->b(Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/domain/MerchantPayee;

    .line 112
    invoke-virtual {v1}, Lcom/chase/sig/android/domain/MerchantPayee;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 113
    invoke-virtual {v2, v1}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->a(Lcom/chase/sig/android/domain/MerchantPayee;)V

    .line 117
    :cond_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/cb;->a:Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->c(Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->b(Ljava/lang/String;)V

    .line 118
    iget-object v0, p0, Lcom/chase/sig/android/activity/cb;->a:Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->d(Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->a(Ljava/lang/String;)V

    .line 119
    iget-object v0, p0, Lcom/chase/sig/android/activity/cb;->a:Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->e(Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->f(Ljava/lang/String;)V

    .line 120
    iget-object v0, p0, Lcom/chase/sig/android/activity/cb;->a:Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->f(Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->g(Ljava/lang/String;)V

    .line 121
    const/4 v0, 0x1

    invoke-virtual {v2, v0}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->a(Z)V

    .line 123
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/chase/sig/android/activity/cb;->a:Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;

    const-class v3, Lcom/chase/sig/android/activity/BillPayPayeeAddVerifyActivity;

    invoke-direct {v0, v1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 126
    const-string v1, "payee_info"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 128
    iget-object v1, p0, Lcom/chase/sig/android/activity/cb;->a:Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;

    const/16 v2, 0x19

    invoke-virtual {v1, v0, v2}, Lcom/chase/sig/android/activity/BillPayPayeeSearchResultActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 129
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1, p2, p3}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/widget/AdapterView;Landroid/view/View;I)V

    return-void

    :catch_0
    move-exception v0

    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1, p2, p3}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/widget/AdapterView;Landroid/view/View;I)V

    throw v0
.end method
