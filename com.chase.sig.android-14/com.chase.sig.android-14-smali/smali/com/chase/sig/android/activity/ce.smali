.class final Lcom/chase/sig/android/activity/ce;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 60
    iput-object p1, p0, Lcom/chase/sig/android/activity/ce;->a:Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3
    .parameter

    .prologue
    .line 65
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ce;->a:Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;->b()Lcom/chase/sig/android/domain/QuickPayRecipient;

    .line 67
    iget-object v0, p0, Lcom/chase/sig/android/activity/ce;->a:Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ce;->a:Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;->a(Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/chase/sig/android/activity/ce;->a:Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;

    invoke-static {v1}, Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;->a(Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getCheckedItemPositions()Landroid/util/SparseBooleanArray;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;->a(Landroid/widget/ListAdapter;Landroid/util/SparseBooleanArray;)[J

    move-result-object v0

    array-length v0, v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/ce;->a:Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ce;->a:Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;->a(Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/chase/sig/android/activity/ce;->a:Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;

    invoke-static {v1}, Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;->b(Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getCheckedItemPositions()Landroid/util/SparseBooleanArray;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;->a(Landroid/widget/ListAdapter;Landroid/util/SparseBooleanArray;)[J

    move-result-object v0

    array-length v0, v0

    if-gtz v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/chase/sig/android/activity/ce;->a:Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;

    const v1, 0x7f070247

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;->g(I)V

    .line 91
    :goto_0
    return-void

    .line 72
    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/ce;->a:Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ce;->a:Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;->a(Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/chase/sig/android/activity/ce;->a:Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;

    invoke-static {v1}, Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;->a(Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getCheckedItemPositions()Landroid/util/SparseBooleanArray;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;->a(Landroid/widget/ListAdapter;Landroid/util/SparseBooleanArray;)[J

    move-result-object v0

    array-length v0, v0

    const/4 v1, 0x5

    if-le v0, v1, :cond_1

    .line 74
    iget-object v0, p0, Lcom/chase/sig/android/activity/ce;->a:Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;

    const v1, 0x7f070246

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;->g(I)V

    goto :goto_0

    .line 75
    :cond_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/ce;->a:Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;->c(Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;)Lcom/chase/sig/android/domain/PhoneBookContact;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/PhoneBookContact;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 76
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/chase/sig/android/activity/ce;->a:Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;

    const-class v2, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 79
    iget-object v1, p0, Lcom/chase/sig/android/activity/ce;->a:Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;

    invoke-virtual {v1}, Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/content/Intent;)Landroid/content/Intent;

    .line 80
    const-string v1, "recipient"

    iget-object v2, p0, Lcom/chase/sig/android/activity/ce;->a:Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;

    invoke-static {v2}, Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;->d(Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;)Lcom/chase/sig/android/domain/QuickPayRecipient;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 81
    const-string v1, "contact"

    iget-object v2, p0, Lcom/chase/sig/android/activity/ce;->a:Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;

    invoke-static {v2}, Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;->c(Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;)Lcom/chase/sig/android/domain/PhoneBookContact;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 82
    iget-object v1, p0, Lcom/chase/sig/android/activity/ce;->a:Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 84
    :cond_2
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/chase/sig/android/activity/ce;->a:Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;

    const-class v2, Lcom/chase/sig/android/activity/QuickPayAddRecipientActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 87
    iget-object v1, p0, Lcom/chase/sig/android/activity/ce;->a:Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;

    invoke-virtual {v1}, Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/content/Intent;)Landroid/content/Intent;

    .line 88
    const-string v1, "recipient"

    iget-object v2, p0, Lcom/chase/sig/android/activity/ce;->a:Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;

    invoke-static {v2}, Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;->d(Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;)Lcom/chase/sig/android/domain/QuickPayRecipient;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 89
    iget-object v1, p0, Lcom/chase/sig/android/activity/ce;->a:Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method
