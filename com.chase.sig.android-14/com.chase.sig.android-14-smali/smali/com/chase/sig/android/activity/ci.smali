.class final Lcom/chase/sig/android/activity/ci;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 135
    iput-object p1, p0, Lcom/chase/sig/android/activity/ci;->a:Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3
    .parameter

    .prologue
    .line 139
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ci;->a:Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;->b(Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;)Lcom/chase/sig/android/domain/QuickPayRecipient;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/QuickPayRecipient;->a(Lcom/chase/sig/android/domain/MobilePhoneNumber;)V

    .line 140
    iget-object v0, p0, Lcom/chase/sig/android/activity/ci;->a:Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;->a(Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;)Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity$a;->b()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 141
    iget-object v0, p0, Lcom/chase/sig/android/activity/ci;->a:Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;->a(Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;)Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity$a;->a()Lcom/chase/sig/android/domain/RecipientContact;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/RecipientContact;->b()Ljava/lang/String;

    move-result-object v0

    .line 142
    const-string v1, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 143
    iget-object v1, p0, Lcom/chase/sig/android/activity/ci;->a:Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;

    invoke-static {v1}, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;->b(Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;)Lcom/chase/sig/android/domain/QuickPayRecipient;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/QuickPayRecipient;->e(Ljava/lang/String;)V

    .line 144
    iget-object v0, p0, Lcom/chase/sig/android/activity/ci;->a:Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;->b(Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;)Lcom/chase/sig/android/domain/QuickPayRecipient;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayRecipient;->i()Lcom/chase/sig/android/domain/MobilePhoneNumber;

    move-result-object v0

    const-string v1, "Mobile"

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/MobilePhoneNumber;->c(Ljava/lang/String;)V

    .line 145
    iget-object v0, p0, Lcom/chase/sig/android/activity/ci;->a:Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;->b(Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;)Lcom/chase/sig/android/domain/QuickPayRecipient;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayRecipient;->i()Lcom/chase/sig/android/domain/MobilePhoneNumber;

    move-result-object v0

    const-string v1, "0"

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/MobilePhoneNumber;->b(Ljava/lang/String;)V

    .line 148
    :cond_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/chase/sig/android/activity/ci;->a:Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;

    const-class v2, Lcom/chase/sig/android/activity/QuickPayAddRecipientActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 150
    iget-object v1, p0, Lcom/chase/sig/android/activity/ci;->a:Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;

    invoke-virtual {v1}, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/content/Intent;)Landroid/content/Intent;

    .line 151
    const-string v1, "recipient"

    iget-object v2, p0, Lcom/chase/sig/android/activity/ci;->a:Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;

    invoke-static {v2}, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;->b(Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;)Lcom/chase/sig/android/domain/QuickPayRecipient;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 152
    iget-object v1, p0, Lcom/chase/sig/android/activity/ci;->a:Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;->startActivity(Landroid/content/Intent;)V

    .line 153
    return-void
.end method
