.class final Lcom/chase/sig/android/activity/cj;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/ContactUsActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/ContactUsActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 40
    iput-object p1, p0, Lcom/chase/sig/android/activity/cj;->a:Lcom/chase/sig/android/activity/ContactUsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 5
    .parameter

    .prologue
    const v4, 0x7f07005d

    .line 44
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    new-instance v1, Landroid/content/Intent;

    iget-object v0, p0, Lcom/chase/sig/android/activity/cj;->a:Lcom/chase/sig/android/activity/ContactUsActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/ContactUsActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    const-class v2, Lcom/chase/sig/android/activity/ManagedContentActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 48
    iget-object v0, p0, Lcom/chase/sig/android/activity/cj;->a:Lcom/chase/sig/android/activity/ContactUsActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/ContactUsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v2, 0x7f07

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v2, "PBD"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 49
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/chase/sig/android/activity/cj;->a:Lcom/chase/sig/android/activity/ContactUsActivity;

    invoke-virtual {v2}, Lcom/chase/sig/android/activity/ContactUsActivity;->z()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v2

    const-string v3, "mbb"

    invoke-virtual {v2, v3}, Lcom/chase/sig/android/ChaseApplication;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/chase/sig/android/activity/cj;->a:Lcom/chase/sig/android/activity/ContactUsActivity;

    invoke-virtual {v2}, Lcom/chase/sig/android/activity/ContactUsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 58
    :goto_0
    const-string v2, "webUrl"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 59
    const-string v0, "viewTitle"

    const v2, 0x7f0700b5

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 60
    iget-object v0, p0, Lcom/chase/sig/android/activity/cj;->a:Lcom/chase/sig/android/activity/ContactUsActivity;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/ContactUsActivity;->startActivity(Landroid/content/Intent;)V

    .line 61
    return-void

    .line 53
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/chase/sig/android/activity/cj;->a:Lcom/chase/sig/android/activity/ContactUsActivity;

    invoke-virtual {v2}, Lcom/chase/sig/android/activity/ContactUsActivity;->z()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v2

    const-string v3, "content"

    invoke-virtual {v2, v3}, Lcom/chase/sig/android/ChaseApplication;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/chase/sig/android/activity/cj;->a:Lcom/chase/sig/android/activity/ContactUsActivity;

    invoke-virtual {v2}, Lcom/chase/sig/android/activity/ContactUsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
