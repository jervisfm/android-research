.class final Lcom/chase/sig/android/activity/ck;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/ContactsActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/ContactsActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 43
    iput-object p1, p0, Lcom/chase/sig/android/activity/ck;->a:Lcom/chase/sig/android/activity/ContactsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 47
    :try_start_0
    iget-object v2, p0, Lcom/chase/sig/android/activity/ck;->a:Lcom/chase/sig/android/activity/ContactsActivity;

    invoke-static {v2}, Lcom/chase/sig/android/activity/ContactsActivity;->a(Lcom/chase/sig/android/activity/ContactsActivity;)Lcom/chase/sig/android/activity/cc;

    move-result-object v2

    iget-object v3, p0, Lcom/chase/sig/android/activity/ck;->a:Lcom/chase/sig/android/activity/ContactsActivity;

    invoke-virtual {v2, v3, p4, p5}, Lcom/chase/sig/android/activity/cc;->a(Landroid/content/Context;J)Lcom/chase/sig/android/domain/PhoneBookContact;

    move-result-object v3

    .line 49
    invoke-virtual {v3}, Lcom/chase/sig/android/domain/PhoneBookContact;->d()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v3}, Lcom/chase/sig/android/domain/PhoneBookContact;->a()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-le v2, v0, :cond_1

    move v2, v0

    .line 51
    :goto_0
    if-nez v2, :cond_2

    .line 53
    :goto_1
    if-eqz v0, :cond_3

    invoke-virtual {v3}, Lcom/chase/sig/android/domain/PhoneBookContact;->e()Z

    move-result v1

    if-nez v1, :cond_3

    .line 54
    iget-object v0, p0, Lcom/chase/sig/android/activity/ck;->a:Lcom/chase/sig/android/activity/ContactsActivity;

    iget-object v1, p0, Lcom/chase/sig/android/activity/ck;->a:Lcom/chase/sig/android/activity/ContactsActivity;

    invoke-static {v3}, Lcom/chase/sig/android/activity/ContactsActivity;->a(Lcom/chase/sig/android/domain/PhoneBookContact;)Lcom/chase/sig/android/domain/QuickPayRecipient;

    move-result-object v1

    const-class v2, Lcom/chase/sig/android/activity/QuickPayAddRecipientActivity;

    invoke-static {v0, v1, v3, v2}, Lcom/chase/sig/android/activity/ContactsActivity;->a(Lcom/chase/sig/android/activity/ContactsActivity;Lcom/chase/sig/android/domain/QuickPayRecipient;Lcom/chase/sig/android/domain/PhoneBookContact;Ljava/lang/Class;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 65
    :cond_0
    :goto_2
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1, p2, p3}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/widget/AdapterView;Landroid/view/View;I)V

    return-void

    :cond_1
    move v2, v1

    .line 49
    goto :goto_0

    :cond_2
    move v0, v1

    .line 51
    goto :goto_1

    .line 57
    :cond_3
    if-eqz v0, :cond_4

    :try_start_1
    invoke-virtual {v3}, Lcom/chase/sig/android/domain/PhoneBookContact;->e()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 58
    iget-object v0, p0, Lcom/chase/sig/android/activity/ck;->a:Lcom/chase/sig/android/activity/ContactsActivity;

    iget-object v1, p0, Lcom/chase/sig/android/activity/ck;->a:Lcom/chase/sig/android/activity/ContactsActivity;

    invoke-static {v3}, Lcom/chase/sig/android/activity/ContactsActivity;->a(Lcom/chase/sig/android/domain/PhoneBookContact;)Lcom/chase/sig/android/domain/QuickPayRecipient;

    move-result-object v1

    const-class v2, Lcom/chase/sig/android/activity/ContactPhoneNumbersActivity;

    invoke-static {v0, v1, v3, v2}, Lcom/chase/sig/android/activity/ContactsActivity;->a(Lcom/chase/sig/android/activity/ContactsActivity;Lcom/chase/sig/android/domain/QuickPayRecipient;Lcom/chase/sig/android/domain/PhoneBookContact;Ljava/lang/Class;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 65
    :catch_0
    move-exception v0

    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1, p2, p3}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/widget/AdapterView;Landroid/view/View;I)V

    throw v0

    .line 61
    :cond_4
    if-eqz v2, :cond_0

    .line 62
    :try_start_2
    iget-object v0, p0, Lcom/chase/sig/android/activity/ck;->a:Lcom/chase/sig/android/activity/ContactsActivity;

    iget-object v1, p0, Lcom/chase/sig/android/activity/ck;->a:Lcom/chase/sig/android/activity/ContactsActivity;

    invoke-static {v3}, Lcom/chase/sig/android/activity/ContactsActivity;->b(Lcom/chase/sig/android/domain/PhoneBookContact;)Lcom/chase/sig/android/domain/QuickPayRecipient;

    move-result-object v1

    const-class v2, Lcom/chase/sig/android/activity/ContactEmailAddressesActivity;

    invoke-static {v0, v1, v3, v2}, Lcom/chase/sig/android/activity/ContactsActivity;->a(Lcom/chase/sig/android/activity/ContactsActivity;Lcom/chase/sig/android/domain/QuickPayRecipient;Lcom/chase/sig/android/domain/PhoneBookContact;Ljava/lang/Class;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_2
.end method
