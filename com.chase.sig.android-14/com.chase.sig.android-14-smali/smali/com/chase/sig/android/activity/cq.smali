.class final Lcom/chase/sig/android/activity/cq;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/DisclosuresActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/DisclosuresActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 65
    iput-object p1, p0, Lcom/chase/sig/android/activity/cq;->a:Lcom/chase/sig/android/activity/DisclosuresActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 70
    :try_start_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/cq;->a:Lcom/chase/sig/android/activity/DisclosuresActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/DisclosuresActivity;->a(Lcom/chase/sig/android/activity/DisclosuresActivity;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p3, v0, :cond_1

    .line 71
    iget-object v0, p0, Lcom/chase/sig/android/activity/cq;->a:Lcom/chase/sig/android/activity/DisclosuresActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/DisclosuresActivity;->a(Lcom/chase/sig/android/activity/DisclosuresActivity;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/Disclosure;

    .line 72
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/chase/sig/android/activity/cq;->a:Lcom/chase/sig/android/activity/DisclosuresActivity;

    const-class v3, Lcom/chase/sig/android/activity/DisclosuresDetailActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 74
    invoke-static {v1, v0}, Lcom/chase/sig/android/util/e;->a(Landroid/content/Intent;Ljava/io/Serializable;)V

    .line 75
    iget-object v0, p0, Lcom/chase/sig/android/activity/cq;->a:Lcom/chase/sig/android/activity/DisclosuresActivity;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/DisclosuresActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 85
    :cond_0
    :goto_0
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1, p2, p3}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/widget/AdapterView;Landroid/view/View;I)V

    return-void

    .line 77
    :cond_1
    const v0, 0x7f0900bc

    :try_start_1
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 78
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    iget-object v2, p0, Lcom/chase/sig/android/activity/cq;->a:Lcom/chase/sig/android/activity/DisclosuresActivity;

    const v3, 0x7f07028c

    invoke-virtual {v2, v3}, Lcom/chase/sig/android/activity/DisclosuresActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 79
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/chase/sig/android/activity/cq;->a:Lcom/chase/sig/android/activity/DisclosuresActivity;

    const-class v3, Lcom/chase/sig/android/activity/PrivacyNoticesActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 81
    const-string v2, "title"

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 82
    iget-object v0, p0, Lcom/chase/sig/android/activity/cq;->a:Lcom/chase/sig/android/activity/DisclosuresActivity;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/DisclosuresActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 85
    :catch_0
    move-exception v0

    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1, p2, p3}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/widget/AdapterView;Landroid/view/View;I)V

    throw v0
.end method
