.class final Lcom/chase/sig/android/activity/cy;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/EPayHomeActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/EPayHomeActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 30
    iput-object p1, p0, Lcom/chase/sig/android/activity/cy;->a:Lcom/chase/sig/android/activity/EPayHomeActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3
    .parameter

    .prologue
    .line 36
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/cy;->a:Lcom/chase/sig/android/activity/EPayHomeActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/EPayHomeActivity;->a(Lcom/chase/sig/android/activity/EPayHomeActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 37
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/chase/sig/android/activity/cy;->a:Lcom/chase/sig/android/activity/EPayHomeActivity;

    const-class v2, Lcom/chase/sig/android/activity/EPayStartActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 38
    const-string v1, "selectedAccountId"

    iget-object v2, p0, Lcom/chase/sig/android/activity/cy;->a:Lcom/chase/sig/android/activity/EPayHomeActivity;

    invoke-static {v2}, Lcom/chase/sig/android/activity/EPayHomeActivity;->b(Lcom/chase/sig/android/activity/EPayHomeActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 45
    :goto_0
    iget-object v1, p0, Lcom/chase/sig/android/activity/cy;->a:Lcom/chase/sig/android/activity/EPayHomeActivity;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/activity/EPayHomeActivity;->startActivity(Landroid/content/Intent;)V

    .line 46
    return-void

    .line 41
    :cond_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/chase/sig/android/activity/cy;->a:Lcom/chase/sig/android/activity/EPayHomeActivity;

    const-class v2, Lcom/chase/sig/android/activity/EPaySelectToAccount;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 42
    const/high16 v1, 0x4000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method
