.class final Lcom/chase/sig/android/activity/cz;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Ljava/util/List;

.field final synthetic b:Z

.field final synthetic c:Lcom/chase/sig/android/activity/EPaySelectToAccount;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/EPaySelectToAccount;Ljava/util/List;Z)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 44
    iput-object p1, p0, Lcom/chase/sig/android/activity/cz;->c:Lcom/chase/sig/android/activity/EPaySelectToAccount;

    iput-object p2, p0, Lcom/chase/sig/android/activity/cz;->a:Ljava/util/List;

    iput-boolean p3, p0, Lcom/chase/sig/android/activity/cz;->b:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 49
    :try_start_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/cz;->a:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/IAccount;

    .line 51
    iget-boolean v1, p0, Lcom/chase/sig/android/activity/cz;->b:Z

    if-eqz v1, :cond_0

    .line 52
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 53
    const-string v2, "selectedAccountId"

    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 54
    iget-object v0, p0, Lcom/chase/sig/android/activity/cz;->c:Lcom/chase/sig/android/activity/EPaySelectToAccount;

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Lcom/chase/sig/android/activity/EPaySelectToAccount;->setResult(ILandroid/content/Intent;)V

    .line 55
    iget-object v0, p0, Lcom/chase/sig/android/activity/cz;->c:Lcom/chase/sig/android/activity/EPaySelectToAccount;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/EPaySelectToAccount;->finish()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 59
    :goto_0
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1, p2, p3}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/widget/AdapterView;Landroid/view/View;I)V

    return-void

    .line 57
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/chase/sig/android/activity/cz;->c:Lcom/chase/sig/android/activity/EPaySelectToAccount;

    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/chase/sig/android/activity/EPaySelectToAccount;->a(Lcom/chase/sig/android/activity/EPaySelectToAccount;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 59
    :catch_0
    move-exception v0

    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1, p2, p3}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/widget/AdapterView;Landroid/view/View;I)V

    throw v0
.end method
