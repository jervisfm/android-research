.class final Lcom/chase/sig/android/activity/db;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/view/x;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/EPayStartActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/EPayStartActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 398
    iput-object p1, p0, Lcom/chase/sig/android/activity/db;->a:Lcom/chase/sig/android/activity/EPayStartActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 5
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 403
    iget-object v0, p0, Lcom/chase/sig/android/activity/db;->a:Lcom/chase/sig/android/activity/EPayStartActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/EPayStartActivity;->b(Lcom/chase/sig/android/activity/EPayStartActivity;)Landroid/widget/SimpleAdapter;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/SimpleAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 405
    const-string v1, "id"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 406
    const-string v2, "-1"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 407
    iget-object v1, p0, Lcom/chase/sig/android/activity/db;->a:Lcom/chase/sig/android/activity/EPayStartActivity;

    const-string v2, "PAYMENT_OPTIONS"

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/activity/EPayStartActivity;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/view/detail/q;

    iput-boolean v4, v1, Lcom/chase/sig/android/view/detail/a;->n:Z

    .line 408
    iget-object v1, p0, Lcom/chase/sig/android/activity/db;->a:Lcom/chase/sig/android/activity/EPayStartActivity;

    const-string v2, "AMOUNT"

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/activity/EPayStartActivity;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/view/detail/c;

    .line 409
    iput-boolean v3, v1, Lcom/chase/sig/android/view/detail/a;->j:Z

    .line 410
    invoke-virtual {v1}, Lcom/chase/sig/android/view/detail/c;->p()Lcom/chase/sig/android/view/AmountView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/view/AmountView;->requestFocus()Z

    .line 419
    :goto_0
    const-string v1, "note"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->l(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 420
    iget-object v0, p0, Lcom/chase/sig/android/activity/db;->a:Lcom/chase/sig/android/activity/EPayStartActivity;

    const-string v1, "PAYMENT_OPTIONS"

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/EPayStartActivity;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/q;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/q;->k()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0900c7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 423
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 425
    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/db;->a:Lcom/chase/sig/android/activity/EPayStartActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/EPayStartActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/DetailView;->c()V

    .line 426
    return-void

    .line 413
    :cond_1
    iget-object v1, p0, Lcom/chase/sig/android/activity/db;->a:Lcom/chase/sig/android/activity/EPayStartActivity;

    const-string v2, "PAYMENT_OPTIONS"

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/activity/EPayStartActivity;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/view/detail/q;

    iput-boolean v3, v1, Lcom/chase/sig/android/view/detail/a;->n:Z

    .line 414
    iget-object v1, p0, Lcom/chase/sig/android/activity/db;->a:Lcom/chase/sig/android/activity/EPayStartActivity;

    const-string v2, "AMOUNT"

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/activity/EPayStartActivity;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/view/detail/c;

    .line 415
    iput-boolean v4, v1, Lcom/chase/sig/android/view/detail/a;->j:Z

    .line 416
    const-string v2, ""

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/view/detail/c;->b(Ljava/lang/String;)V

    goto :goto_0
.end method
