.class final Lcom/chase/sig/android/activity/dc;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/EPayStartActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/EPayStartActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 429
    iput-object p1, p0, Lcom/chase/sig/android/activity/dc;->a:Lcom/chase/sig/android/activity/EPayStartActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 6
    .parameter

    .prologue
    .line 433
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/dc;->a:Lcom/chase/sig/android/activity/EPayStartActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/EPayStartActivity;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 464
    :cond_0
    :goto_0
    return-void

    .line 437
    :cond_1
    new-instance v2, Lcom/chase/sig/android/domain/EPayment;

    invoke-direct {v2}, Lcom/chase/sig/android/domain/EPayment;-><init>()V

    .line 438
    iget-object v0, p0, Lcom/chase/sig/android/activity/dc;->a:Lcom/chase/sig/android/activity/EPayStartActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/EPayStartActivity;->c(Lcom/chase/sig/android/activity/EPayStartActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/chase/sig/android/domain/EPayment;->d(Ljava/lang/String;)V

    .line 439
    iget-object v0, p0, Lcom/chase/sig/android/activity/dc;->a:Lcom/chase/sig/android/activity/EPayStartActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/EPayStartActivity;->d(Lcom/chase/sig/android/activity/EPayStartActivity;)Lcom/chase/sig/android/domain/IAccount;

    move-result-object v0

    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/chase/sig/android/domain/EPayment;->e(Ljava/lang/String;)V

    .line 440
    iget-object v0, p0, Lcom/chase/sig/android/activity/dc;->a:Lcom/chase/sig/android/activity/EPayStartActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/EPayStartActivity;->f(Lcom/chase/sig/android/activity/EPayStartActivity;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/chase/sig/android/activity/dc;->a:Lcom/chase/sig/android/activity/EPayStartActivity;

    invoke-static {v1}, Lcom/chase/sig/android/activity/EPayStartActivity;->e(Lcom/chase/sig/android/activity/EPayStartActivity;)Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/view/JPSpinner;->getSelectedItemPosition()I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/EPayAccount;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/EPayAccount;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/chase/sig/android/domain/EPayment;->c(Ljava/lang/String;)V

    .line 442
    iget-object v0, p0, Lcom/chase/sig/android/activity/dc;->a:Lcom/chase/sig/android/activity/EPayStartActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/EPayStartActivity;->f(Lcom/chase/sig/android/activity/EPayStartActivity;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/EPayAccount;

    .line 443
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/EPayAccount;->d()Ljava/lang/String;

    move-result-object v4

    iget-object v1, p0, Lcom/chase/sig/android/activity/dc;->a:Lcom/chase/sig/android/activity/EPayStartActivity;

    invoke-static {v1}, Lcom/chase/sig/android/activity/EPayStartActivity;->f(Lcom/chase/sig/android/activity/EPayStartActivity;)Ljava/util/List;

    move-result-object v1

    iget-object v5, p0, Lcom/chase/sig/android/activity/dc;->a:Lcom/chase/sig/android/activity/EPayStartActivity;

    invoke-static {v5}, Lcom/chase/sig/android/activity/EPayStartActivity;->e(Lcom/chase/sig/android/activity/EPayStartActivity;)Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v5

    invoke-virtual {v5}, Lcom/chase/sig/android/view/JPSpinner;->getSelectedItemPosition()I

    move-result v5

    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/domain/EPayAccount;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/EPayAccount;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 445
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/EPayAccount;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/chase/sig/android/domain/EPayment;->h(Ljava/lang/String;)V

    .line 446
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/EPayAccount;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/chase/sig/android/domain/EPayment;->i(Ljava/lang/String;)V

    goto :goto_1

    .line 449
    :cond_3
    iget-object v0, p0, Lcom/chase/sig/android/activity/dc;->a:Lcom/chase/sig/android/activity/EPayStartActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/EPayStartActivity;->c(Lcom/chase/sig/android/activity/EPayStartActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/chase/sig/android/domain/EPayment;->d(Ljava/lang/String;)V

    .line 450
    iget-object v0, p0, Lcom/chase/sig/android/activity/dc;->a:Lcom/chase/sig/android/activity/EPayStartActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/EPayStartActivity;->h(Lcom/chase/sig/android/activity/EPayStartActivity;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/chase/sig/android/activity/dc;->a:Lcom/chase/sig/android/activity/EPayStartActivity;

    invoke-static {v1}, Lcom/chase/sig/android/activity/EPayStartActivity;->g(Lcom/chase/sig/android/activity/EPayStartActivity;)Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/view/JPSpinner;->getSelectedItemPosition()I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/EPaymentOption;

    .line 452
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/EPaymentOption;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/chase/sig/android/domain/EPayment;->f(Ljava/lang/String;)V

    .line 454
    const-string v1, "-1"

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/EPayment;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 455
    iget-object v0, p0, Lcom/chase/sig/android/activity/dc;->a:Lcom/chase/sig/android/activity/EPayStartActivity;

    const-string v1, "AMOUNT"

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/EPayStartActivity;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/c;

    .line 456
    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/c;->q()Lcom/chase/sig/android/util/Dollar;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/chase/sig/android/domain/EPayment;->a(Lcom/chase/sig/android/util/Dollar;)V

    .line 460
    :cond_4
    :goto_2
    iget-object v0, p0, Lcom/chase/sig/android/activity/dc;->a:Lcom/chase/sig/android/activity/EPayStartActivity;

    iget-object v0, v0, Lcom/chase/sig/android/activity/EPayStartActivity;->e:Lcom/chase/sig/android/activity/mc;

    const-class v1, Lcom/chase/sig/android/activity/EPayStartActivity$b;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/mc;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/ae;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/activity/EPayStartActivity$b;

    .line 461
    invoke-virtual {v0}, Lcom/chase/sig/android/activity/EPayStartActivity$b;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v1

    sget-object v3, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-eq v1, v3, :cond_0

    .line 462
    const/4 v1, 0x1

    new-array v1, v1, [Lcom/chase/sig/android/domain/EPayment;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/EPayStartActivity$b;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    .line 457
    :cond_5
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/EPaymentOption;->a()Lcom/chase/sig/android/util/Dollar;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/EPaymentOption;->a()Lcom/chase/sig/android/util/Dollar;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/util/Dollar;->c()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 458
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/EPaymentOption;->a()Lcom/chase/sig/android/util/Dollar;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/chase/sig/android/domain/EPayment;->a(Lcom/chase/sig/android/util/Dollar;)V

    goto :goto_2
.end method
