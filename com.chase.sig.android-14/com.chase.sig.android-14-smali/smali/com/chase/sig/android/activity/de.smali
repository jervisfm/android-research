.class final Lcom/chase/sig/android/activity/de;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/EPayVerifyActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/EPayVerifyActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 37
    iput-object p1, p0, Lcom/chase/sig/android/activity/de;->a:Lcom/chase/sig/android/activity/EPayVerifyActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3
    .parameter

    .prologue
    .line 41
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/chase/sig/android/activity/de;->a:Lcom/chase/sig/android/activity/EPayVerifyActivity;

    invoke-virtual {v1}, Lcom/chase/sig/android/activity/EPayVerifyActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/chase/sig/android/activity/EPayAuthorizeActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 42
    sget-object v1, Lcom/chase/sig/android/activity/EPayVerifyActivity;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/chase/sig/android/activity/de;->a:Lcom/chase/sig/android/activity/EPayVerifyActivity;

    invoke-static {v2}, Lcom/chase/sig/android/activity/EPayVerifyActivity;->a(Lcom/chase/sig/android/activity/EPayVerifyActivity;)Lcom/chase/sig/android/domain/EPayment;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 43
    iget-object v1, p0, Lcom/chase/sig/android/activity/de;->a:Lcom/chase/sig/android/activity/EPayVerifyActivity;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/activity/EPayVerifyActivity;->startActivity(Landroid/content/Intent;)V

    .line 44
    return-void
.end method
