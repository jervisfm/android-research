.class final Lcom/chase/sig/android/activity/dh;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/FindBranchActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/FindBranchActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 106
    iput-object p1, p0, Lcom/chase/sig/android/activity/dh;->a:Lcom/chase/sig/android/activity/FindBranchActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 3
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 111
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1, p2, p3}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;ILandroid/view/KeyEvent;)V

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_1

    const/16 v0, 0x42

    if-ne p2, v0, :cond_1

    .line 114
    iget-object v0, p0, Lcom/chase/sig/android/activity/dh;->a:Lcom/chase/sig/android/activity/FindBranchActivity;

    iget-object v0, v0, Lcom/chase/sig/android/activity/FindBranchActivity;->e:Lcom/chase/sig/android/activity/mc;

    const-class v2, Lcom/chase/sig/android/activity/FindBranchActivity$a;

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/activity/mc;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/ae;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/activity/FindBranchActivity$a;

    .line 115
    invoke-virtual {v0}, Lcom/chase/sig/android/activity/FindBranchActivity$a;->d()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 116
    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/FindBranchActivity$a;->cancel(Z)Z

    .line 119
    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/dh;->a:Lcom/chase/sig/android/activity/FindBranchActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/FindBranchActivity;->c(Lcom/chase/sig/android/activity/FindBranchActivity;)Ljava/util/List;

    move-result-object v0

    iget-object v2, p0, Lcom/chase/sig/android/activity/dh;->a:Lcom/chase/sig/android/activity/FindBranchActivity;

    invoke-static {v2}, Lcom/chase/sig/android/activity/FindBranchActivity;->b(Lcom/chase/sig/android/activity/FindBranchActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 120
    iget-object v0, p0, Lcom/chase/sig/android/activity/dh;->a:Lcom/chase/sig/android/activity/FindBranchActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/FindBranchActivity;->d(Lcom/chase/sig/android/activity/FindBranchActivity;)Landroid/widget/ArrayAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 121
    iget-object v0, p0, Lcom/chase/sig/android/activity/dh;->a:Lcom/chase/sig/android/activity/FindBranchActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/FindBranchActivity;->d(Lcom/chase/sig/android/activity/FindBranchActivity;)Landroid/widget/ArrayAdapter;

    move-result-object v0

    iget-object v2, p0, Lcom/chase/sig/android/activity/dh;->a:Lcom/chase/sig/android/activity/FindBranchActivity;

    invoke-static {v2}, Lcom/chase/sig/android/activity/FindBranchActivity;->b(Lcom/chase/sig/android/activity/FindBranchActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 122
    iget-object v0, p0, Lcom/chase/sig/android/activity/dh;->a:Lcom/chase/sig/android/activity/FindBranchActivity;

    const-class v2, Lcom/chase/sig/android/activity/FindBranchActivity$a;

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v2, v1}, Lcom/chase/sig/android/activity/FindBranchActivity;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 124
    const/4 v0, 0x1

    .line 127
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method
