.class final Lcom/chase/sig/android/activity/dj;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/domain/QuoteNewsArticle;

.field final synthetic b:Lcom/chase/sig/android/activity/HeadlinesActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/HeadlinesActivity;Lcom/chase/sig/android/domain/QuoteNewsArticle;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 59
    iput-object p1, p0, Lcom/chase/sig/android/activity/dj;->b:Lcom/chase/sig/android/activity/HeadlinesActivity;

    iput-object p2, p0, Lcom/chase/sig/android/activity/dj;->a:Lcom/chase/sig/android/domain/QuoteNewsArticle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3
    .parameter

    .prologue
    .line 62
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/chase/sig/android/activity/dj;->b:Lcom/chase/sig/android/activity/HeadlinesActivity;

    const-class v2, Lcom/chase/sig/android/activity/ArticleActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 63
    const-string v1, "article_content"

    iget-object v2, p0, Lcom/chase/sig/android/activity/dj;->a:Lcom/chase/sig/android/domain/QuoteNewsArticle;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 64
    iget-object v1, p0, Lcom/chase/sig/android/activity/dj;->b:Lcom/chase/sig/android/activity/HeadlinesActivity;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/activity/HeadlinesActivity;->startActivity(Landroid/content/Intent;)V

    .line 65
    return-void
.end method
