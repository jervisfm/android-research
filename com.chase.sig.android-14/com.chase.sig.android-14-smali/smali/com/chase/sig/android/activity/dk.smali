.class final Lcom/chase/sig/android/activity/dk;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/ChaseApplication;

.field final synthetic b:Lcom/chase/sig/android/activity/HomeActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/HomeActivity;Lcom/chase/sig/android/ChaseApplication;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 123
    iput-object p1, p0, Lcom/chase/sig/android/activity/dk;->b:Lcom/chase/sig/android/activity/HomeActivity;

    iput-object p2, p0, Lcom/chase/sig/android/activity/dk;->a:Lcom/chase/sig/android/ChaseApplication;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onLongClick(Landroid/view/View;)Z
    .locals 8
    .parameter

    .prologue
    const/4 v7, 0x1

    .line 128
    new-instance v2, Landroid/app/Dialog;

    iget-object v0, p0, Lcom/chase/sig/android/activity/dk;->b:Lcom/chase/sig/android/activity/HomeActivity;

    invoke-direct {v2, v0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 130
    const v0, 0x7f030022

    invoke-virtual {v2, v0}, Landroid/app/Dialog;->setContentView(I)V

    .line 131
    const-string v0, "Debug Settings"

    invoke-virtual {v2, v0}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 133
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 134
    iget-object v0, p0, Lcom/chase/sig/android/activity/dk;->b:Lcom/chase/sig/android/activity/HomeActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/HomeActivity;->z()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    const-string v3, "environments"

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/ChaseApplication;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 137
    new-instance v3, Ljava/util/StringTokenizer;

    const-string v4, ","

    invoke-direct {v3, v0, v4}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    :goto_0
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 140
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 143
    :cond_0
    const v0, 0x7f090097

    invoke-virtual {v2, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    .line 145
    new-instance v3, Landroid/widget/ArrayAdapter;

    iget-object v4, p0, Lcom/chase/sig/android/activity/dk;->b:Lcom/chase/sig/android/activity/HomeActivity;

    const v5, 0x1090008

    invoke-direct {v3, v4, v5, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 148
    const v1, 0x1090009

    invoke-virtual {v3, v1}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 149
    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 151
    iget-object v1, p0, Lcom/chase/sig/android/activity/dk;->a:Lcom/chase/sig/android/ChaseApplication;

    invoke-virtual {v1}, Lcom/chase/sig/android/ChaseApplication;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/widget/ArrayAdapter;->getPosition(Ljava/lang/Object;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 153
    const v1, 0x7f090099

    invoke-virtual {v2, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 154
    const-string v3, "Current ID is \'%s\'"

    new-array v4, v7, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/chase/sig/android/activity/dk;->a:Lcom/chase/sig/android/ChaseApplication;

    invoke-virtual {v6}, Lcom/chase/sig/android/ChaseApplication;->d()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 157
    const v1, 0x7f090098

    invoke-virtual {v2, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v3, Lcom/chase/sig/android/activity/dl;

    invoke-direct {v3, p0, v2, v0}, Lcom/chase/sig/android/activity/dl;-><init>(Lcom/chase/sig/android/activity/dk;Landroid/app/Dialog;Landroid/widget/Spinner;)V

    invoke-virtual {v1, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 216
    const v0, 0x7f09009b

    invoke-virtual {v2, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/chase/sig/android/activity/dn;

    invoke-direct {v1, p0, v2}, Lcom/chase/sig/android/activity/dn;-><init>(Lcom/chase/sig/android/activity/dk;Landroid/app/Dialog;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 231
    const v0, 0x7f09009c

    invoke-virtual {v2, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 232
    iget-object v1, p0, Lcom/chase/sig/android/activity/dk;->b:Lcom/chase/sig/android/activity/HomeActivity;

    invoke-virtual {v1}, Lcom/chase/sig/android/activity/HomeActivity;->z()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/ChaseApplication;->t()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 233
    new-instance v1, Lcom/chase/sig/android/activity/do;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/do;-><init>(Lcom/chase/sig/android/activity/dk;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 241
    const v0, 0x7f09009a

    invoke-virtual {v2, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/chase/sig/android/activity/dp;

    invoke-direct {v1, p0, v2}, Lcom/chase/sig/android/activity/dp;-><init>(Lcom/chase/sig/android/activity/dk;Landroid/app/Dialog;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 258
    invoke-virtual {v2}, Landroid/app/Dialog;->show()V

    .line 259
    return v7
.end method
