.class final Lcom/chase/sig/android/activity/dl;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Landroid/app/Dialog;

.field final synthetic b:Landroid/widget/Spinner;

.field final synthetic c:Lcom/chase/sig/android/activity/dk;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/dk;Landroid/app/Dialog;Landroid/widget/Spinner;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 160
    iput-object p1, p0, Lcom/chase/sig/android/activity/dl;->c:Lcom/chase/sig/android/activity/dk;

    iput-object p2, p0, Lcom/chase/sig/android/activity/dl;->a:Landroid/app/Dialog;

    iput-object p3, p0, Lcom/chase/sig/android/activity/dl;->b:Landroid/widget/Spinner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 6
    .parameter

    .prologue
    .line 164
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/dl;->a:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 166
    iget-object v0, p0, Lcom/chase/sig/android/activity/dl;->b:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 167
    const-string v1, "closed_net_mockey"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 168
    new-instance v1, Landroid/app/Dialog;

    iget-object v2, p0, Lcom/chase/sig/android/activity/dl;->c:Lcom/chase/sig/android/activity/dk;

    iget-object v2, v2, Lcom/chase/sig/android/activity/dk;->b:Lcom/chase/sig/android/activity/HomeActivity;

    invoke-direct {v1, v2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    const-string v2, "Enter Hostname[:Port]"

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    new-instance v2, Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/chase/sig/android/activity/dl;->c:Lcom/chase/sig/android/activity/dk;

    iget-object v3, v3, Lcom/chase/sig/android/activity/dk;->b:Lcom/chase/sig/android/activity/HomeActivity;

    invoke-direct {v2, v3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setOrientation(I)V

    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    const/4 v4, -0x1

    const/4 v5, -0x2

    invoke-direct {v3, v4, v5}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2, v3}, Landroid/app/Dialog;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v3, Landroid/widget/EditText;

    iget-object v4, p0, Lcom/chase/sig/android/activity/dl;->c:Lcom/chase/sig/android/activity/dk;

    iget-object v4, v4, Lcom/chase/sig/android/activity/dk;->b:Lcom/chase/sig/android/activity/HomeActivity;

    invoke-direct {v3, v4}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3}, Landroid/widget/EditText;->setSingleLine()V

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    iget-object v4, p0, Lcom/chase/sig/android/activity/dl;->c:Lcom/chase/sig/android/activity/dk;

    iget-object v4, v4, Lcom/chase/sig/android/activity/dk;->b:Lcom/chase/sig/android/activity/HomeActivity;

    invoke-virtual {v4}, Lcom/chase/sig/android/activity/HomeActivity;->z()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v4

    invoke-virtual {v4}, Lcom/chase/sig/android/ChaseApplication;->g()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    new-instance v4, Landroid/widget/Button;

    iget-object v5, p0, Lcom/chase/sig/android/activity/dl;->c:Lcom/chase/sig/android/activity/dk;

    iget-object v5, v5, Lcom/chase/sig/android/activity/dk;->b:Lcom/chase/sig/android/activity/HomeActivity;

    invoke-direct {v4, v5}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    const-string v5, "OK"

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    new-instance v2, Lcom/chase/sig/android/activity/dm;

    invoke-direct {v2, p0, v3, v1}, Lcom/chase/sig/android/activity/dm;-><init>(Lcom/chase/sig/android/activity/dl;Landroid/widget/EditText;Landroid/app/Dialog;)V

    invoke-virtual {v4, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    .line 171
    :cond_0
    iget-object v1, p0, Lcom/chase/sig/android/activity/dl;->c:Lcom/chase/sig/android/activity/dk;

    iget-object v1, v1, Lcom/chase/sig/android/activity/dk;->b:Lcom/chase/sig/android/activity/HomeActivity;

    invoke-virtual {v1}, Lcom/chase/sig/android/activity/HomeActivity;->z()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/ChaseApplication;->b(Ljava/lang/String;)V

    .line 172
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Environment selected: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/chase/sig/android/activity/dl;->c:Lcom/chase/sig/android/activity/dk;

    iget-object v1, v1, Lcom/chase/sig/android/activity/dk;->a:Lcom/chase/sig/android/ChaseApplication;

    invoke-virtual {v1}, Lcom/chase/sig/android/ChaseApplication;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 173
    return-void

    .line 168
    :cond_1
    const-string v4, "192.168.0.100:8080"

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
