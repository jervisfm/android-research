.class final Lcom/chase/sig/android/activity/dw;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 94
    iput-object p1, p0, Lcom/chase/sig/android/activity/dw;->a:Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3
    .parameter

    .prologue
    .line 98
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    new-instance v1, Landroid/content/Intent;

    iget-object v0, p0, Lcom/chase/sig/android/activity/dw;->a:Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;

    const-class v2, Lcom/chase/sig/android/activity/InvestmentTransactionsFilterActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 100
    const-string v2, "filter_types"

    iget-object v0, p0, Lcom/chase/sig/android/activity/dw;->a:Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->a(Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;)Lcom/chase/sig/android/service/InvestmentTransactionsResponse;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/service/InvestmentTransactionsResponse;->b()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/io/Serializable;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 102
    const-string v0, "range"

    iget-object v2, p0, Lcom/chase/sig/android/activity/dw;->a:Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;

    invoke-static {v2}, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->b(Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 103
    const-string v0, "filter_type"

    iget-object v2, p0, Lcom/chase/sig/android/activity/dw;->a:Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;

    invoke-static {v2}, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->c(Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;)Lcom/chase/sig/android/domain/InvestmentTransaction$FilterType;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 104
    iget-object v0, p0, Lcom/chase/sig/android/activity/dw;->a:Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 105
    return-void
.end method
