.class final Lcom/chase/sig/android/activity/dy;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/chase/sig/android/domain/InvestmentTransaction;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 298
    iput-object p1, p0, Lcom/chase/sig/android/activity/dy;->b:Lcom/chase/sig/android/activity/InvestmentTransactionsActivity;

    iput p2, p0, Lcom/chase/sig/android/activity/dy;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 298
    check-cast p1, Lcom/chase/sig/android/domain/InvestmentTransaction;

    check-cast p2, Lcom/chase/sig/android/domain/InvestmentTransaction;

    iget v0, p0, Lcom/chase/sig/android/activity/dy;->a:I

    packed-switch v0, :pswitch_data_0

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/InvestmentTransaction;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/chase/sig/android/domain/InvestmentTransaction;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    :goto_0
    return v0

    :pswitch_0
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/InvestmentTransaction;->a()Lcom/chase/sig/android/util/Dollar;

    move-result-object v0

    invoke-virtual {p2}, Lcom/chase/sig/android/domain/InvestmentTransaction;->a()Lcom/chase/sig/android/util/Dollar;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/util/Dollar;->compareTo(Ljava/lang/Object;)I

    move-result v0

    goto :goto_0

    :pswitch_1
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/InvestmentTransaction;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/chase/sig/android/domain/InvestmentTransaction;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
