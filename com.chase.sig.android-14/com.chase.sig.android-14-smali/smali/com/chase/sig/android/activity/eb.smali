.class public abstract Lcom/chase/sig/android/activity/eb;
.super Landroid/app/Activity;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/Object;


# instance fields
.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/SimpleDialogInfo;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/chase/sig/android/view/MenuView;

.field protected e:Lcom/chase/sig/android/activity/mc;

.field f:Ljava/lang/String;

.field public g:Z

.field public h:Z

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 107
    const-string v0, "cpcLogo"

    sput-object v0, Lcom/chase/sig/android/activity/eb;->a:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 85
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 109
    iput-object v1, p0, Lcom/chase/sig/android/activity/eb;->e:Lcom/chase/sig/android/activity/mc;

    .line 111
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/chase/sig/android/activity/eb;->b:Ljava/util/List;

    .line 112
    iput-object v1, p0, Lcom/chase/sig/android/activity/eb;->f:Ljava/lang/String;

    .line 114
    iput-boolean v2, p0, Lcom/chase/sig/android/activity/eb;->g:Z

    .line 115
    iput-boolean v2, p0, Lcom/chase/sig/android/activity/eb;->h:Z

    .line 116
    iput-object v1, p0, Lcom/chase/sig/android/activity/eb;->i:Ljava/lang/String;

    .line 117
    iput-object v1, p0, Lcom/chase/sig/android/activity/eb;->j:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/eb;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 85
    iget-object v0, p0, Lcom/chase/sig/android/activity/eb;->b:Ljava/util/List;

    return-object v0
.end method

.method private a(ILjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1232
    new-instance v0, Lcom/chase/sig/android/domain/SimpleDialogInfo;

    invoke-direct {v0, p1, p2, p3}, Lcom/chase/sig/android/domain/SimpleDialogInfo;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    .line 1233
    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/eb;->a(Lcom/chase/sig/android/domain/SimpleDialogInfo;)V

    .line 1234
    return-void
.end method

.method private a(Landroid/os/Bundle;Landroid/view/ViewGroup;)V
    .locals 4
    .parameter
    .parameter

    .prologue
    .line 309
    if-nez p2, :cond_1

    .line 326
    :cond_0
    return-void

    .line 313
    :cond_1
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 314
    invoke-virtual {p2, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 315
    instance-of v0, v1, Landroid/widget/TextView;

    if-eqz v0, :cond_2

    .line 316
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "ERROR_"

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    move-object v0, v1

    .line 317
    check-cast v0, Landroid/widget/TextView;

    .line 318
    invoke-static {v0}, Lcom/chase/sig/android/activity/eb;->a(Landroid/widget/TextView;)Z

    .line 322
    :cond_2
    instance-of v0, v1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_3

    instance-of v0, v1, Lcom/chase/sig/android/view/detail/DetailView;

    if-nez v0, :cond_3

    .line 323
    check-cast v1, Landroid/view/ViewGroup;

    invoke-direct {p0, p1, v1}, Lcom/chase/sig/android/activity/eb;->a(Landroid/os/Bundle;Landroid/view/ViewGroup;)V

    .line 313
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0
.end method

.method private a(Landroid/view/View;)V
    .locals 2
    .parameter

    .prologue
    .line 470
    instance-of v0, p1, Lcom/chase/sig/android/view/JPSpinner;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 471
    check-cast v0, Lcom/chase/sig/android/view/JPSpinner;

    .line 472
    invoke-virtual {v0}, Lcom/chase/sig/android/view/JPSpinner;->c()V

    .line 474
    :cond_0
    instance-of v0, p1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    .line 475
    check-cast p1, Landroid/view/ViewGroup;

    .line 476
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 477
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/chase/sig/android/activity/eb;->a(Landroid/view/View;)V

    .line 476
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 480
    :cond_1
    return-void
.end method

.method private a(Landroid/view/ViewGroup;)V
    .locals 4
    .parameter

    .prologue
    .line 976
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 977
    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 979
    instance-of v0, v1, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 980
    check-cast v0, Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setError(Ljava/lang/CharSequence;)V

    .line 983
    :cond_0
    instance-of v0, v1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    .line 984
    check-cast v1, Landroid/view/ViewGroup;

    invoke-direct {p0, v1}, Lcom/chase/sig/android/activity/eb;->a(Landroid/view/ViewGroup;)V

    .line 976
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 987
    :cond_2
    return-void
.end method

.method private varargs a(Lcom/chase/sig/android/view/MenuView;[I)V
    .locals 5
    .parameter
    .parameter

    .prologue
    .line 544
    array-length v1, p2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget v2, p2, v0

    .line 545
    invoke-virtual {p0, v2}, Lcom/chase/sig/android/activity/eb;->findViewById(I)Landroid/view/View;

    move-result-object v3

    new-instance v4, Lcom/chase/sig/android/activity/eq;

    invoke-direct {v4, p0, p1, v2}, Lcom/chase/sig/android/activity/eq;-><init>(Lcom/chase/sig/android/activity/eb;Lcom/chase/sig/android/view/MenuView;I)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 544
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 554
    :cond_0
    return-void
.end method

.method private a(ZLandroid/widget/Button;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 1458
    if-eqz p1, :cond_0

    .line 1459
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02009d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/Button;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1461
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020058

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/Button;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 1469
    :goto_0
    return-void

    .line 1464
    :cond_0
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0200ff

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/Button;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1466
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020059

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/Button;->setTextColor(Landroid/content/res/ColorStateList;)V

    goto :goto_0
.end method

.method public static a(Landroid/widget/TextView;)Z
    .locals 1
    .parameter

    .prologue
    .line 929
    const-string v0, ""

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setError(Ljava/lang/CharSequence;)V

    .line 930
    const/4 v0, 0x0

    return v0
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/eb;)I
    .locals 1
    .parameter

    .prologue
    .line 85
    invoke-direct {p0}, Lcom/chase/sig/android/activity/eb;->e()I

    move-result v0

    return v0
.end method

.method private b(Lcom/chase/sig/android/domain/SimpleDialogInfo;)Lcom/chase/sig/android/view/k;
    .locals 4
    .parameter

    .prologue
    const v3, 0x7f070068

    .line 351
    new-instance v0, Lcom/chase/sig/android/view/k$a;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/view/k$a;-><init>(Landroid/content/Context;)V

    .line 353
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/SimpleDialogInfo;->b()Ljava/lang/String;

    move-result-object v1

    .line 354
    const-string v2, ".*\\<[^>]+>.*"

    invoke-virtual {v1, v2}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v2

    .line 355
    if-eqz v2, :cond_1

    .line 356
    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/k$a;->b(Ljava/lang/String;)Lcom/chase/sig/android/view/k$a;

    move-result-object v1

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/SimpleDialogInfo;->f()Z

    move-result v2

    iput-boolean v2, v1, Lcom/chase/sig/android/view/k$a;->i:Z

    .line 361
    :goto_0
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/SimpleDialogInfo;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 362
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/SimpleDialogInfo;->e()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/chase/sig/android/view/k$a;->b:Ljava/lang/CharSequence;

    .line 365
    :cond_0
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/SimpleDialogInfo;->d()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 366
    new-instance v1, Lcom/chase/sig/android/activity/ec;

    invoke-direct {v1, p0, p1}, Lcom/chase/sig/android/activity/ec;-><init>(Lcom/chase/sig/android/activity/eb;Lcom/chase/sig/android/domain/SimpleDialogInfo;)V

    invoke-virtual {v0, v3, v1}, Lcom/chase/sig/android/view/k$a;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    .line 376
    new-instance v1, Lcom/chase/sig/android/activity/en;

    invoke-direct {v1, p0, p1}, Lcom/chase/sig/android/activity/en;-><init>(Lcom/chase/sig/android/activity/eb;Lcom/chase/sig/android/domain/SimpleDialogInfo;)V

    iput-object v1, v0, Lcom/chase/sig/android/view/k$a;->g:Landroid/content/DialogInterface$OnCancelListener;

    .line 406
    :goto_1
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/k$a;->d(I)Lcom/chase/sig/android/view/k;

    move-result-object v0

    return-object v0

    .line 358
    :cond_1
    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/k$a;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/k$a;

    move-result-object v1

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/SimpleDialogInfo;->f()Z

    move-result v2

    iput-boolean v2, v1, Lcom/chase/sig/android/view/k$a;->i:Z

    goto :goto_0

    .line 387
    :cond_2
    new-instance v1, Lcom/chase/sig/android/activity/eo;

    invoke-direct {v1, p0, p1}, Lcom/chase/sig/android/activity/eo;-><init>(Lcom/chase/sig/android/activity/eb;Lcom/chase/sig/android/domain/SimpleDialogInfo;)V

    invoke-virtual {v0, v3, v1}, Lcom/chase/sig/android/view/k$a;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    .line 396
    new-instance v1, Lcom/chase/sig/android/activity/ep;

    invoke-direct {v1, p0, p1}, Lcom/chase/sig/android/activity/ep;-><init>(Lcom/chase/sig/android/activity/eb;Lcom/chase/sig/android/domain/SimpleDialogInfo;)V

    iput-object v1, v0, Lcom/chase/sig/android/view/k$a;->g:Landroid/content/DialogInterface$OnCancelListener;

    goto :goto_1
.end method

.method private b(Landroid/os/Bundle;Landroid/view/ViewGroup;)V
    .locals 5
    .parameter
    .parameter

    .prologue
    .line 445
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 446
    invoke-virtual {p2, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 447
    instance-of v0, v1, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 448
    check-cast v0, Landroid/widget/TextView;

    .line 450
    invoke-virtual {v0}, Landroid/widget/TextView;->getError()Ljava/lang/CharSequence;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 451
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "ERROR_"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/widget/TextView;->getId()I

    move-result v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x1

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 455
    :cond_0
    instance-of v0, v1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    .line 456
    check-cast v1, Landroid/view/ViewGroup;

    invoke-direct {p0, p1, v1}, Lcom/chase/sig/android/activity/eb;->b(Landroid/os/Bundle;Landroid/view/ViewGroup;)V

    .line 445
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 459
    :cond_2
    return-void
.end method

.method private static b(Landroid/view/View;)V
    .locals 3
    .parameter

    .prologue
    .line 620
    if-eqz p0, :cond_0

    .line 621
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 622
    const/4 v1, 0x1

    const v2, 0x7f09002d

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 623
    const/16 v1, 0xf

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 624
    invoke-virtual {p0, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 626
    :cond_0
    return-void
.end method

.method private d()Z
    .locals 1

    .prologue
    .line 329
    iget-object v0, p0, Lcom/chase/sig/android/activity/eb;->b:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/eb;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()I
    .locals 1

    .prologue
    .line 528
    instance-of v0, p0, Lcom/chase/sig/android/activity/fk$d;

    if-eqz v0, :cond_0

    .line 529
    const v0, 0x7f0902fc

    .line 539
    :goto_0
    return v0

    .line 530
    :cond_0
    instance-of v0, p0, Lcom/chase/sig/android/activity/fk$e;

    if-eqz v0, :cond_1

    .line 531
    const v0, 0x7f0902fd

    goto :goto_0

    .line 532
    :cond_1
    instance-of v0, p0, Lcom/chase/sig/android/activity/fk$c;

    if-eqz v0, :cond_2

    .line 533
    const v0, 0x7f090112

    goto :goto_0

    .line 534
    :cond_2
    instance-of v0, p0, Lcom/chase/sig/android/activity/fk$b;

    if-eqz v0, :cond_3

    .line 535
    const v0, 0x7f090113

    goto :goto_0

    .line 536
    :cond_3
    instance-of v0, p0, Lcom/chase/sig/android/activity/fk$a;

    if-eqz v0, :cond_4

    .line 537
    const v0, 0x7f0902fe

    goto :goto_0

    .line 539
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f()Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 661
    const v0, 0x7f09002c

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/eb;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method protected static g(Ljava/lang/String;)I
    .locals 2
    .parameter

    .prologue
    .line 1360
    invoke-static {p0}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1361
    new-instance v0, Lcom/chase/sig/android/util/Dollar;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/util/Dollar;-><init>(Ljava/lang/String;)V

    .line 1363
    invoke-virtual {v0}, Lcom/chase/sig/android/util/Dollar;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/chase/sig/android/util/Dollar;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1364
    const v0, 0x7f06001d

    .line 1368
    :goto_0
    return v0

    :cond_0
    const v0, 0x7f06001c

    goto :goto_0
.end method

.method private g()Z
    .locals 1

    .prologue
    .line 850
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/ChaseApplication;

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->b:Lcom/chase/sig/android/domain/g;

    invoke-interface {v0}, Lcom/chase/sig/android/domain/g;->C()Z

    move-result v0

    return v0
.end method

.method private h()Z
    .locals 1

    .prologue
    .line 971
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->b:Lcom/chase/sig/android/domain/g;

    .line 972
    invoke-interface {v0}, Lcom/chase/sig/android/domain/g;->a()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final A()Lcom/chase/sig/android/service/n;
    .locals 1

    .prologue
    .line 1295
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    invoke-static {}, Lcom/chase/sig/android/service/af;->a()Lcom/chase/sig/android/service/n;

    move-result-object v0

    return-object v0
.end method

.method protected final B()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 1331
    new-instance v0, Lcom/chase/sig/android/activity/a/d;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/activity/a/d;-><init>(Lcom/chase/sig/android/activity/eb;)V

    return-object v0
.end method

.method protected final C()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 1344
    new-instance v0, Lcom/chase/sig/android/activity/a/c;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/activity/a/c;-><init>(Landroid/app/Activity;)V

    return-object v0
.end method

.method public final a(Landroid/content/Context;I)Landroid/app/Dialog;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 1113
    new-instance v1, Lcom/chase/sig/android/activity/ej;

    invoke-direct {v1, p0, p1}, Lcom/chase/sig/android/activity/ej;-><init>(Lcom/chase/sig/android/activity/eb;Landroid/content/Context;)V

    .line 1120
    const v0, 0x7f03006b

    invoke-virtual {v1, v0}, Landroid/app/Dialog;->setContentView(I)V

    .line 1122
    const v0, 0x7f090103

    invoke-virtual {v1, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(I)V

    .line 1124
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 1125
    new-instance v0, Lcom/chase/sig/android/activity/ek;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/activity/ek;-><init>(Lcom/chase/sig/android/activity/eb;)V

    invoke-virtual {v1, v0}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 1133
    return-object v1
.end method

.method protected final a(Lcom/chase/sig/android/c$a;)Landroid/view/View$OnClickListener;
    .locals 1
    .parameter

    .prologue
    .line 1336
    new-instance v0, Lcom/chase/sig/android/activity/a/d;

    invoke-direct {v0, p0, p1}, Lcom/chase/sig/android/activity/a/d;-><init>(Lcom/chase/sig/android/activity/eb;Lcom/chase/sig/android/c$a;)V

    return-object v0
.end method

.method protected final a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/a/f;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/chase/sig/android/activity/eb;",
            ">;)",
            "Lcom/chase/sig/android/activity/a/f;"
        }
    .end annotation

    .prologue
    .line 1327
    new-instance v0, Lcom/chase/sig/android/activity/a/f;

    invoke-direct {v0, p0, p1}, Lcom/chase/sig/android/activity/a/f;-><init>(Lcom/chase/sig/android/activity/eb;Ljava/lang/Class;)V

    return-object v0
.end method

.method public final a(Ljava/util/Calendar;Lcom/chase/sig/android/view/b$a;)Lcom/chase/sig/android/view/b;
    .locals 7
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x1

    .line 411
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v6

    .line 412
    const/4 v0, 0x5

    const/16 v1, 0x16e

    invoke-virtual {v6, v0, v1}, Ljava/util/Calendar;->add(II)V

    .line 413
    new-instance v0, Lcom/chase/sig/android/view/b;

    move-object v1, p0

    move-object v2, p2

    move v4, v3

    move-object v5, p1

    invoke-direct/range {v0 .. v6}, Lcom/chase/sig/android/view/b;-><init>(Landroid/content/Context;Lcom/chase/sig/android/view/b$a;ZZLjava/util/Calendar;Ljava/util/Calendar;)V

    return-object v0
.end method

.method protected a(I)V
    .locals 0
    .parameter

    .prologue
    .line 418
    return-void
.end method

.method public final a(II)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 1225
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1227
    const v1, 0x7f0700bf

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v1, v0}, Lcom/chase/sig/android/activity/eb;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 1228
    return-void
.end method

.method public final a(IIILandroid/view/View$OnClickListener;)V
    .locals 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1373
    new-instance v1, Landroid/widget/ImageButton;

    invoke-direct {v1, p0}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    .line 1374
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x2

    const/4 v3, -0x1

    invoke-direct {v0, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1376
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1377
    invoke-virtual {v1, p1}, Landroid/widget/ImageButton;->setId(I)V

    .line 1379
    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 1380
    new-array v2, v5, [I

    const v3, 0x10100a7

    aput v3, v2, v4

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, p3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 1383
    new-array v2, v5, [I

    const v3, 0x101009c

    aput v3, v2, v4

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, p3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 1386
    new-array v2, v4, [I

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 1388
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1389
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f020033

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1392
    invoke-virtual {v1, p4}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1394
    const v0, 0x7f090030

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/eb;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1395
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1396
    return-void
.end method

.method protected final a(ILandroid/view/View$OnClickListener;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 1348
    invoke-virtual {p0, p1}, Lcom/chase/sig/android/activity/eb;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1349
    return-void
.end method

.method public final a(ILcom/chase/sig/android/service/IServiceError;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 1246
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p2}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Lcom/chase/sig/android/service/IServiceError;)V

    invoke-interface {p2}, Lcom/chase/sig/android/service/IServiceError;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/chase/sig/android/activity/eb;->a(ILjava/lang/String;)V

    .line 1247
    return-void
.end method

.method public final a(ILjava/lang/String;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 1256
    new-instance v0, Lcom/chase/sig/android/domain/SimpleDialogInfo;

    invoke-direct {v0, p1, p2}, Lcom/chase/sig/android/domain/SimpleDialogInfo;-><init>(ILjava/lang/String;)V

    .line 1257
    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/eb;->a(Lcom/chase/sig/android/domain/SimpleDialogInfo;)V

    .line 1258
    return-void
.end method

.method public final a(ILjava/lang/String;Z)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 256
    invoke-virtual {p0, p1}, Lcom/chase/sig/android/activity/eb;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p2, p3}, Lcom/chase/sig/android/activity/eb;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 257
    return-void
.end method

.method public final a(ILjava/util/List;)V
    .locals 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/service/IServiceError;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1142
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    .line 1143
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 1144
    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/service/IServiceError;

    .line 1148
    if-nez v1, :cond_0

    .line 1149
    invoke-virtual {p0, p1, v0}, Lcom/chase/sig/android/activity/eb;->a(ILcom/chase/sig/android/service/IServiceError;)V

    .line 1143
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1152
    :cond_1
    return-void
.end method

.method protected final a(Landroid/content/Intent;Lcom/chase/sig/android/domain/QuickPayRecipient;)V
    .locals 4
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 1305
    invoke-virtual {p2}, Lcom/chase/sig/android/domain/QuickPayRecipient;->k()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 1306
    const-class v0, Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;

    .line 1315
    :goto_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1316
    const-string v0, "recipient"

    invoke-virtual {v1, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1317
    const-string v0, "isRequestForMoney"

    const-string v2, "isRequestForMoney"

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1319
    const-string v0, "quick_pay_transaction"

    const-string v2, "quick_pay_transaction"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1321
    invoke-virtual {p1}, Landroid/content/Intent;->getFlags()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1322
    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/eb;->startActivity(Landroid/content/Intent;)V

    .line 1323
    return-void

    .line 1308
    :cond_0
    const/high16 v0, 0x400

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1309
    const-string v0, "isRequestForMoney"

    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1310
    const-class v0, Lcom/chase/sig/android/activity/QuickPayRequestMoneyActivity;

    goto :goto_0

    .line 1312
    :cond_1
    const-class v0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;

    goto :goto_0
.end method

.method public final a(Landroid/content/Intent;Ljava/lang/String;)V
    .locals 5
    .parameter
    .parameter
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1509
    invoke-virtual {p0, v3}, Lcom/chase/sig/android/activity/eb;->getPreferences(I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 1510
    const/4 v1, 0x1

    invoke-interface {v0, p2, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 1512
    if-eqz v1, :cond_0

    .line 1513
    new-instance v1, Lcom/chase/sig/android/view/k$a;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/view/k$a;-><init>(Landroid/content/Context;)V

    .line 1514
    const v2, 0x7f0701cc

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/view/k$a;->a(I)Lcom/chase/sig/android/view/k$a;

    move-result-object v2

    iput-boolean v3, v2, Lcom/chase/sig/android/view/k$a;->i:Z

    const v3, 0x7f0701cb

    invoke-virtual {v2, v3}, Lcom/chase/sig/android/view/k$a;->c(I)Lcom/chase/sig/android/view/k$a;

    move-result-object v2

    const-string v3, "Hide"

    new-instance v4, Lcom/chase/sig/android/activity/em;

    invoke-direct {v4, p0, v0, p2, p1}, Lcom/chase/sig/android/activity/em;-><init>(Lcom/chase/sig/android/activity/eb;Landroid/content/SharedPreferences;Ljava/lang/String;Landroid/content/Intent;)V

    invoke-virtual {v2, v3, v4}, Lcom/chase/sig/android/view/k$a;->a(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    move-result-object v0

    const-string v2, "Remind"

    new-instance v3, Lcom/chase/sig/android/activity/el;

    invoke-direct {v3, p0, p1}, Lcom/chase/sig/android/activity/el;-><init>(Lcom/chase/sig/android/activity/eb;Landroid/content/Intent;)V

    invoke-virtual {v0, v2, v3}, Lcom/chase/sig/android/view/k$a;->b(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    .line 1533
    const/4 v0, -0x1

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/view/k$a;->d(I)Lcom/chase/sig/android/view/k;

    move-result-object v0

    .line 1534
    invoke-virtual {v0}, Lcom/chase/sig/android/view/k;->show()V

    .line 1538
    :goto_0
    return-void

    .line 1536
    :cond_0
    invoke-virtual {p0, p1}, Lcom/chase/sig/android/activity/eb;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public abstract a(Landroid/os/Bundle;)V
.end method

.method protected final a(Landroid/view/View;Lcom/chase/sig/android/domain/Announcement;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 1420
    const v0, 0x7f09002a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p2}, Lcom/chase/sig/android/domain/Announcement;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1423
    const-class v0, Lcom/chase/sig/android/activity/ManagedContentActivity;

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/eb;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/a/f;

    move-result-object v0

    const-string v1, "documentId"

    invoke-virtual {p2}, Lcom/chase/sig/android/domain/Announcement;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/chase/sig/android/activity/a/f;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/activity/a/f;

    move-result-object v0

    const-string v1, "viewTitle"

    const v2, 0x7f0702d6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/chase/sig/android/activity/a/f;->a(Ljava/lang/String;Ljava/io/Serializable;)Lcom/chase/sig/android/activity/a/f;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1427
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1428
    return-void
.end method

.method protected final a(Landroid/widget/ListView;)V
    .locals 6
    .parameter

    .prologue
    const/4 v5, 0x0

    const v4, 0x7f0900b3

    const/16 v3, 0xa

    const/4 v2, 0x5

    .line 1403
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030031

    invoke-virtual {v0, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f060008

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    invoke-virtual {v0, v2, v3, v2, v3}, Landroid/view/View;->setPadding(IIII)V

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {p1, v0, v5, v2}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 1404
    invoke-virtual {p0, v4}, Lcom/chase/sig/android/activity/eb;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1405
    const-class v2, Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;

    invoke-virtual {p0, v2}, Lcom/chase/sig/android/activity/eb;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/a/f;

    move-result-object v2

    const-string v3, "title"

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Lcom/chase/sig/android/activity/a/f;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/activity/a/f;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1407
    return-void
.end method

.method public final a(Lcom/chase/sig/android/domain/SimpleDialogInfo;)V
    .locals 1
    .parameter

    .prologue
    .line 1193
    iget-object v0, p0, Lcom/chase/sig/android/activity/eb;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1194
    invoke-direct {p0, p1}, Lcom/chase/sig/android/activity/eb;->b(Lcom/chase/sig/android/domain/SimpleDialogInfo;)Lcom/chase/sig/android/view/k;

    move-result-object v0

    .line 1196
    :try_start_0
    invoke-virtual {v0}, Lcom/chase/sig/android/view/k;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1199
    :goto_0
    return-void

    .line 1198
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected final a(Lcom/chase/sig/android/view/detail/DetailView;I)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 1433
    new-instance v0, Lcom/chase/sig/android/view/detail/g;

    invoke-direct {v0, p1, p0, p2}, Lcom/chase/sig/android/view/detail/g;-><init>(Lcom/chase/sig/android/view/detail/DetailView;Lcom/chase/sig/android/activity/eb;I)V

    .line 1434
    return-void
.end method

.method public final varargs a(Ljava/lang/Class;[Ljava/lang/Object;)V
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Params:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/chase/sig/android/activity/ae",
            "<*TParams;**>;>;[TParams;)V"
        }
    .end annotation

    .prologue
    .line 1353
    iget-object v0, p0, Lcom/chase/sig/android/activity/eb;->e:Lcom/chase/sig/android/activity/mc;

    invoke-virtual {v0, p1}, Lcom/chase/sig/android/activity/mc;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/ae;

    move-result-object v0

    .line 1354
    invoke-virtual {v0}, Lcom/chase/sig/android/activity/ae;->d()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1355
    invoke-virtual {v0, p2}, Lcom/chase/sig/android/activity/ae;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1357
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v6, -0x1

    const/4 v5, 0x0

    .line 260
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->n()Landroid/view/ViewGroup;

    move-result-object v0

    const v1, 0x7f0900dc

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 261
    if-eqz v0, :cond_0

    .line 262
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->n()Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 265
    :cond_0
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030042

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SlidingDrawer;

    .line 268
    invoke-virtual {v0}, Landroid/widget/SlidingDrawer;->getContent()Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ScrollView;

    .line 269
    invoke-virtual {v1, v5}, Landroid/widget/ScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 270
    new-instance v2, Landroid/widget/TextView;

    invoke-direct {v2, p0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 271
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 272
    const v4, 0x7f06004c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 274
    invoke-static {p2}, Lcom/chase/sig/android/util/s;->A(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 275
    invoke-static {p2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 279
    :goto_0
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 281
    invoke-virtual {v0}, Landroid/widget/SlidingDrawer;->getHandle()Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 282
    invoke-virtual {v1, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 283
    const v2, 0x7f060049

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setTextColor(I)V

    .line 285
    const/4 v1, 0x1

    const/high16 v2, 0x432f

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    invoke-static {v1, v2, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v1, v1

    .line 288
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v6, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 291
    const/16 v1, 0xc

    invoke-virtual {v2, v1, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 292
    invoke-virtual {v0, v5, v5, v5, v5}, Landroid/widget/SlidingDrawer;->setPadding(IIII)V

    .line 294
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->n()Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v1, v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 296
    if-eqz p3, :cond_1

    .line 297
    const/high16 v1, 0x7f04

    invoke-static {p0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    .line 298
    invoke-virtual {v0, v1}, Landroid/widget/SlidingDrawer;->startAnimation(Landroid/view/animation/Animation;)V

    .line 300
    :cond_1
    iput-object p2, p0, Lcom/chase/sig/android/activity/eb;->i:Ljava/lang/String;

    .line 301
    iput-object p1, p0, Lcom/chase/sig/android/activity/eb;->j:Ljava/lang/String;

    .line 302
    return-void

    .line 277
    :cond_2
    invoke-virtual {v2, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public a(ZI)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 1453
    invoke-virtual {p0, p2}, Lcom/chase/sig/android/activity/eb;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-direct {p0, p1, v0}, Lcom/chase/sig/android/activity/eb;->a(ZLandroid/widget/Button;)V

    .line 1454
    return-void
.end method

.method protected final a(Landroid/content/Intent;)Z
    .locals 2
    .parameter

    .prologue
    .line 1549
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/high16 v1, 0x1

    invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 1551
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a_()V
    .locals 3

    .prologue
    const v1, 0x7f0200a2

    .line 126
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/eb;->requestWindowFeature(I)Z

    .line 127
    const v0, 0x7f03000e

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/eb;->setContentView(I)V

    .line 128
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/ChaseApplication;

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/ChaseApplication;

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->b:Lcom/chase/sig/android/domain/g;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/ChaseApplication;

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->b:Lcom/chase/sig/android/domain/g;

    invoke-interface {v0}, Lcom/chase/sig/android/domain/g;->D()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->l(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/chase/sig/android/activity/eb;->a:Ljava/lang/Object;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f0200a1

    :goto_0
    move v1, v0

    :cond_0
    invoke-direct {p0}, Lcom/chase/sig/android/activity/eb;->f()Landroid/view/ViewGroup;

    move-result-object v0

    const v2, 0x7f09002e

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 129
    return-void

    :cond_1
    move v0, v1

    .line 128
    goto :goto_0
.end method

.method public final b(I)V
    .locals 5
    .parameter

    .prologue
    .line 515
    const/4 v0, 0x0

    :try_start_0
    invoke-static {p0, p1, v0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 517
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->n()Landroid/view/ViewGroup;

    move-result-object v1

    .line 519
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 521
    const/4 v3, 0x3

    const v4, 0x7f09002c

    invoke-virtual {v2, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 522
    invoke-virtual {v1, v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 524
    const v0, 0x7f0900b3

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->i()Z

    move-result v0

    if-eqz v0, :cond_2

    const-class v0, Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/eb;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/a/f;

    move-result-object v2

    const v0, 0x7f0900b3

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const-string v3, "title"

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Lcom/chase/sig/android/activity/a/f;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/activity/a/f;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 525
    :cond_1
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->c(Lcom/chase/sig/android/activity/eb;)V

    return-void

    .line 524
    :cond_2
    :try_start_1
    const-class v0, Lcom/chase/sig/android/activity/DisclosuresActivity;

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/eb;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/a/f;

    move-result-object v0

    iget-object v2, p0, Lcom/chase/sig/android/activity/eb;->f:Ljava/lang/String;

    invoke-static {v2}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "dislosure id"

    iget-object v3, p0, Lcom/chase/sig/android/activity/eb;->f:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Lcom/chase/sig/android/activity/a/f;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/activity/a/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/a/f;->d()Lcom/chase/sig/android/activity/a/f;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_0

    .line 525
    :catch_0
    move-exception v0

    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->c(Lcom/chase/sig/android/activity/eb;)V

    throw v0
.end method

.method public final b(Lcom/chase/sig/android/service/IServiceError;)V
    .locals 3
    .parameter

    .prologue
    const/16 v2, -0x270f

    .line 1238
    invoke-interface {p1}, Lcom/chase/sig/android/service/IServiceError;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->l(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1239
    invoke-virtual {p0, v2, p1}, Lcom/chase/sig/android/activity/eb;->a(ILcom/chase/sig/android/service/IServiceError;)V

    .line 1243
    :goto_0
    return-void

    .line 1241
    :cond_0
    invoke-interface {p1}, Lcom/chase/sig/android/service/IServiceError;->b()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Lcom/chase/sig/android/service/IServiceError;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v2, v0, v1}, Lcom/chase/sig/android/activity/eb;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected final b(Lcom/chase/sig/android/view/detail/DetailView;I)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 1438
    new-instance v0, Lcom/chase/sig/android/view/detail/f;

    invoke-direct {v0, p1, p0, p2}, Lcom/chase/sig/android/view/detail/f;-><init>(Lcom/chase/sig/android/view/detail/DetailView;Landroid/app/Activity;I)V

    .line 1439
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1556
    const-string v0, "tel:%s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1557
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.DIAL"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/eb;->a(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/eb;->startActivity(Landroid/content/Intent;)V

    .line 1558
    :goto_0
    return-void

    .line 1557
    :cond_0
    if-eqz p3, :cond_1

    invoke-virtual {p0, p2}, Lcom/chase/sig/android/activity/eb;->e(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0, p2}, Lcom/chase/sig/android/activity/eb;->f(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(Ljava/util/List;)V
    .locals 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/service/IServiceError;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1156
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    .line 1157
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 1158
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/service/IServiceError;

    .line 1162
    if-nez v1, :cond_0

    .line 1163
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {v0}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->b(Lcom/chase/sig/android/service/IServiceError;)V

    new-instance v3, Lcom/chase/sig/android/domain/SimpleDialogInfo;

    invoke-interface {v0}, Lcom/chase/sig/android/service/IServiceError;->b()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0}, Lcom/chase/sig/android/service/IServiceError;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v4, v0}, Lcom/chase/sig/android/domain/SimpleDialogInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v3}, Lcom/chase/sig/android/activity/eb;->a(Lcom/chase/sig/android/domain/SimpleDialogInfo;)V

    .line 1157
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1165
    :cond_0
    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/eb;->b(Lcom/chase/sig/android/service/IServiceError;)V

    goto :goto_1

    .line 1168
    :cond_1
    return-void
.end method

.method public b(ZI)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 1446
    invoke-virtual {p0, p2}, Lcom/chase/sig/android/activity/eb;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-direct {p0, p1, v0}, Lcom/chase/sig/android/activity/eb;->a(ZLandroid/widget/Button;)V

    .line 1447
    return-void
.end method

.method protected b_()Z
    .locals 1

    .prologue
    .line 682
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Ljava/util/List;)V
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/service/IServiceError;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1178
    if-nez p1, :cond_1

    .line 1189
    :cond_0
    return-void

    .line 1182
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/service/IServiceError;

    .line 1184
    :try_start_0
    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/eb;->b(Lcom/chase/sig/android/service/IServiceError;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1186
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final c(Z)V
    .locals 1
    .parameter

    .prologue
    .line 899
    if-eqz p1, :cond_0

    const/16 v0, -0xa

    :goto_0
    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/eb;->showDialog(I)V

    .line 900
    return-void

    .line 899
    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final c(I)Z
    .locals 8
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 725
    const/4 v1, 0x0

    .line 727
    sparse-switch p1, :sswitch_data_0

    move-object v0, v1

    .line 810
    :goto_0
    if-eqz v0, :cond_5

    .line 813
    const/high16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 817
    sget-boolean v1, Lcom/chase/sig/android/c;->a:Z

    if-eqz v1, :cond_4

    .line 818
    new-instance v1, Lcom/chase/sig/android/activity/et;

    invoke-direct {v1, p0, v0}, Lcom/chase/sig/android/activity/et;-><init>(Lcom/chase/sig/android/activity/eb;Landroid/content/Intent;)V

    .line 828
    invoke-static {p0, v1}, Lcom/chase/sig/android/c;->a(Lcom/chase/sig/android/activity/eb;Lcom/chase/sig/android/c$a;)V

    .line 836
    :goto_1
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/ChaseApplication;

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/o;->b()Lcom/chase/sig/android/domain/CacheActivityData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/CacheActivityData;->a()V

    move v0, v2

    .line 845
    :goto_2
    return v0

    .line 729
    :sswitch_0
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v1

    const-class v4, Lcom/chase/sig/android/activity/AccountsActivity;

    invoke-direct {v0, v1, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_0

    .line 732
    :sswitch_1
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v1

    const-class v4, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;

    invoke-direct {v0, v1, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_0

    .line 735
    :sswitch_2
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v1

    const-class v4, Lcom/chase/sig/android/activity/ContactUsActivity;

    invoke-direct {v0, v1, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_0

    .line 738
    :sswitch_3
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v1

    const-class v4, Lcom/chase/sig/android/activity/FindBranchActivity;

    invoke-direct {v0, v1, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_0

    .line 741
    :sswitch_4
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v4

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/ChaseApplication;

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->b:Lcom/chase/sig/android/domain/g;

    invoke-interface {v0}, Lcom/chase/sig/android/domain/g;->e()Ljava/lang/Class;

    move-result-object v0

    invoke-direct {v1, v4, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    move-object v0, v1

    .line 743
    goto :goto_0

    .line 745
    :sswitch_5
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/ChaseApplication;

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->t()Z

    move-result v0

    if-nez v0, :cond_0

    .line 746
    const v0, 0x7f0701bb

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/eb;->g(I)V

    move-object v0, v1

    goto/16 :goto_0

    .line 747
    :cond_0
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->d:Lcom/chase/sig/android/service/SplashResponse;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->d:Lcom/chase/sig/android/service/SplashResponse;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/SplashResponse;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 749
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->d:Lcom/chase/sig/android/service/SplashResponse;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/SplashResponse;->r()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/eb;->f(Ljava/lang/String;)V

    move-object v0, v1

    goto/16 :goto_0

    .line 751
    :cond_1
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v4

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/ChaseApplication;

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->b:Lcom/chase/sig/android/domain/g;

    invoke-interface {v0}, Lcom/chase/sig/android/domain/g;->d()Ljava/lang/Class;

    move-result-object v0

    invoke-direct {v1, v4, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    move-object v0, v1

    .line 753
    goto/16 :goto_0

    .line 755
    :sswitch_6
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v1

    const-class v4, Lcom/chase/sig/android/activity/TeamInfoActivity;

    invoke-direct {v0, v1, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto/16 :goto_0

    .line 758
    :sswitch_7
    new-instance v4, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lcom/chase/sig/android/activity/ManagedContentActivity;

    invoke-direct {v4, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 759
    const-string v5, "webUrl"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/ChaseApplication;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v1, "environment."

    invoke-direct {v7, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/ChaseApplication;

    invoke-virtual {v1}, Lcom/chase/sig/android/ChaseApplication;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, ".mbb"

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/ChaseApplication;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v6, 0x7f07005c

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v5, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 764
    const-string v0, "viewTitle"

    const v1, 0x7f07029e

    invoke-virtual {v4, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-object v0, v4

    .line 765
    goto/16 :goto_0

    .line 767
    :sswitch_8
    new-instance v4, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lcom/chase/sig/android/activity/ManagedContentActivity;

    invoke-direct {v4, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 768
    const-string v5, "webUrl"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/ChaseApplication;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v1, "environment."

    invoke-direct {v7, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/ChaseApplication;

    invoke-virtual {v1}, Lcom/chase/sig/android/ChaseApplication;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, ".content"

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/ChaseApplication;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v6, 0x7f070065

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v5, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 773
    const-string v0, "viewTitle"

    const v1, 0x7f0701ba

    invoke-virtual {v4, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-object v0, v4

    .line 774
    goto/16 :goto_0

    .line 776
    :sswitch_9
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    const-class v4, Lcom/chase/sig/android/activity/DisclosuresActivity;

    invoke-direct {v0, v1, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto/16 :goto_0

    .line 779
    :sswitch_a
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    const-class v4, Lcom/chase/sig/android/activity/MarketsActivity;

    invoke-direct {v0, v1, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto/16 :goto_0

    .line 782
    :sswitch_b
    invoke-direct {p0}, Lcom/chase/sig/android/activity/eb;->g()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 783
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    const-class v4, Lcom/chase/sig/android/activity/ReceiptsSelectPlanActivity;

    invoke-direct {v0, v1, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 784
    const-string v1, "receipts_settings_mode"

    invoke-direct {p0}, Lcom/chase/sig/android/activity/eb;->g()Z

    move-result v4

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto/16 :goto_0

    .line 786
    :cond_2
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    const-class v4, Lcom/chase/sig/android/activity/ReceiptsEnrollActivity;

    invoke-direct {v0, v1, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto/16 :goto_0

    .line 792
    :sswitch_c
    new-instance v0, Lcom/chase/sig/android/activity/es;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/activity/es;-><init>(Lcom/chase/sig/android/activity/eb;)V

    .line 801
    sget-boolean v1, Lcom/chase/sig/android/c;->a:Z

    if-eqz v1, :cond_3

    .line 802
    invoke-static {p0, v0}, Lcom/chase/sig/android/c;->a(Lcom/chase/sig/android/activity/eb;Lcom/chase/sig/android/c$a;)V

    :goto_3
    move v0, v2

    .line 807
    goto/16 :goto_2

    .line 804
    :cond_3
    invoke-virtual {p0, v3}, Lcom/chase/sig/android/activity/eb;->d(Z)V

    goto :goto_3

    .line 832
    :cond_4
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->q()V

    .line 833
    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/eb;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    :cond_5
    move v0, v3

    .line 845
    goto/16 :goto_2

    .line 727
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f090112 -> :sswitch_a
        0x7f090113 -> :sswitch_6
        0x7f0902fc -> :sswitch_0
        0x7f0902fd -> :sswitch_1
        0x7f0902fe -> :sswitch_5
        0x7f0902ff -> :sswitch_4
        0x7f090300 -> :sswitch_3
        0x7f090301 -> :sswitch_c
        0x7f090302 -> :sswitch_2
        0x7f090303 -> :sswitch_8
        0x7f090304 -> :sswitch_7
        0x7f090305 -> :sswitch_9
        0x7f090306 -> :sswitch_b
    .end sparse-switch
.end method

.method protected c_()V
    .locals 3

    .prologue
    .line 1104
    iget-object v0, p0, Lcom/chase/sig/android/activity/eb;->e:Lcom/chase/sig/android/activity/mc;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/mc;->a()Ljava/util/List;

    move-result-object v0

    .line 1105
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/activity/ae;

    .line 1106
    instance-of v2, v0, Lcom/chase/sig/android/b;

    if-eqz v2, :cond_0

    .line 1107
    check-cast v0, Lcom/chase/sig/android/b;

    invoke-virtual {v0}, Lcom/chase/sig/android/b;->a()V

    goto :goto_0

    .line 1110
    :cond_1
    return-void
.end method

.method public final d(I)V
    .locals 2
    .parameter

    .prologue
    .line 938
    invoke-virtual {p0, p1}, Lcom/chase/sig/android/activity/eb;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 939
    check-cast v0, Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setError(Ljava/lang/CharSequence;)V

    .line 940
    return-void
.end method

.method public final d(Ljava/lang/String;)V
    .locals 2
    .parameter

    .prologue
    .line 252
    const v0, 0x7f070086

    const/4 v1, 0x1

    invoke-virtual {p0, v0, p1, v1}, Lcom/chase/sig/android/activity/eb;->a(ILjava/lang/String;Z)V

    .line 253
    return-void
.end method

.method public final d(Z)V
    .locals 3
    .parameter

    .prologue
    .line 1267
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/chase/sig/android/activity/eb;->h:Z

    .line 1268
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v1

    const-class v2, Lcom/chase/sig/android/activity/LogOutActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1269
    const-string v1, "wasBecauseSessionTimedOut"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1270
    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/eb;->startActivity(Landroid/content/Intent;)V

    .line 1285
    return-void
.end method

.method public final e(Ljava/lang/String;)V
    .locals 2
    .parameter

    .prologue
    .line 1204
    new-instance v0, Lcom/chase/sig/android/domain/SimpleDialogInfo;

    const/4 v1, 0x1

    invoke-direct {v0, p1, v1}, Lcom/chase/sig/android/domain/SimpleDialogInfo;-><init>(Ljava/lang/String;Z)V

    .line 1205
    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/eb;->a(Lcom/chase/sig/android/domain/SimpleDialogInfo;)V

    .line 1207
    return-void
.end method

.method public final e(I)Z
    .locals 1
    .parameter

    .prologue
    .line 943
    invoke-virtual {p0, p1}, Lcom/chase/sig/android/activity/eb;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 944
    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lcom/chase/sig/android/activity/eb;->a(Landroid/widget/TextView;)Z

    move-result v0

    return v0
.end method

.method public final f(I)V
    .locals 1
    .parameter

    .prologue
    .line 1211
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/eb;->e(Ljava/lang/String;)V

    .line 1212
    return-void
.end method

.method public final f(Ljava/lang/String;)V
    .locals 1
    .parameter

    .prologue
    .line 1261
    new-instance v0, Lcom/chase/sig/android/domain/SimpleDialogInfo;

    invoke-direct {v0, p1}, Lcom/chase/sig/android/domain/SimpleDialogInfo;-><init>(Ljava/lang/String;)V

    .line 1262
    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/eb;->a(Lcom/chase/sig/android/domain/SimpleDialogInfo;)V

    .line 1263
    return-void
.end method

.method public final g(I)V
    .locals 1
    .parameter

    .prologue
    .line 1216
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/eb;->f(Ljava/lang/String;)V

    .line 1217
    return-void
.end method

.method public final h(I)V
    .locals 1
    .parameter

    .prologue
    .line 1221
    const/16 v0, -0x270f

    invoke-virtual {p0, v0, p1}, Lcom/chase/sig/android/activity/eb;->a(II)V

    .line 1222
    return-void
.end method

.method protected final i(I)Landroid/view/View$OnClickListener;
    .locals 1
    .parameter

    .prologue
    .line 1340
    new-instance v0, Lcom/chase/sig/android/activity/a/e;

    invoke-direct {v0, p0, p1}, Lcom/chase/sig/android/activity/a/e;-><init>(Landroid/app/Activity;I)V

    return-object v0
.end method

.method public final j(I)Z
    .locals 3
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 1477
    const/4 v0, 0x4

    if-ne p1, v0, :cond_2

    .line 1478
    iget-object v0, p0, Lcom/chase/sig/android/activity/eb;->c:Lcom/chase/sig/android/view/MenuView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/eb;->c:Lcom/chase/sig/android/view/MenuView;

    .line 1480
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/chase/sig/android/view/MenuView;->d()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1481
    invoke-virtual {v0}, Lcom/chase/sig/android/view/MenuView;->a()V

    move v0, v1

    .line 1492
    :goto_1
    return v0

    .line 1478
    :cond_0
    const v0, 0x7f09010f

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/eb;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/MenuView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/eb;->c:Lcom/chase/sig/android/view/MenuView;

    goto :goto_0

    .line 1485
    :cond_1
    const v0, 0x7f0900dc

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/eb;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SlidingDrawer;

    .line 1486
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/widget/SlidingDrawer;->isOpened()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1487
    invoke-virtual {v0}, Landroid/widget/SlidingDrawer;->animateClose()V

    move v0, v1

    .line 1488
    goto :goto_1

    .line 1492
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final l()V
    .locals 1

    .prologue
    .line 333
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/ChaseApplication;

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/o;->a()Lcom/chase/sig/android/service/ProfileResponse;

    .line 334
    return-void
.end method

.method protected m()V
    .locals 1

    .prologue
    .line 506
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getLastNonConfigurationInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/activity/mc;

    iput-object v0, p0, Lcom/chase/sig/android/activity/eb;->e:Lcom/chase/sig/android/activity/mc;

    .line 507
    iget-object v0, p0, Lcom/chase/sig/android/activity/eb;->e:Lcom/chase/sig/android/activity/mc;

    if-nez v0, :cond_0

    .line 508
    new-instance v0, Lcom/chase/sig/android/activity/mc;

    invoke-direct {v0}, Lcom/chase/sig/android/activity/mc;-><init>()V

    iput-object v0, p0, Lcom/chase/sig/android/activity/eb;->e:Lcom/chase/sig/android/activity/mc;

    .line 510
    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/eb;->e:Lcom/chase/sig/android/activity/mc;

    invoke-virtual {v0, p0}, Lcom/chase/sig/android/activity/mc;->a(Lcom/chase/sig/android/activity/eb;)V

    .line 511
    return-void
.end method

.method public final n()Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 656
    const v0, 0x7f09002b

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/eb;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method public final o()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 678
    const v0, 0x7f09002f

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/eb;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8
    .parameter

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 202
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 203
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->m()V

    .line 205
    if-eqz p1, :cond_0

    .line 206
    const-string v0, "is_logging_out"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/chase/sig/android/activity/eb;->h:Z

    .line 209
    :cond_0
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->a_()V

    .line 210
    invoke-virtual {p0, p1}, Lcom/chase/sig/android/activity/eb;->a(Landroid/os/Bundle;)V

    .line 212
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    .line 213
    if-nez p1, :cond_2

    const/4 v0, 0x1

    .line 215
    :goto_0
    if-eqz v3, :cond_1

    const-string v4, "queued_errors"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    if-eqz v0, :cond_1

    .line 216
    const-string v0, "queued_errors"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 218
    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/eb;->c(Ljava/util/List;)V

    .line 221
    :cond_1
    if-eqz p1, :cond_3

    const-string v0, "simple_dialogs"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 222
    const-string v0, "simple_dialogs"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/chase/sig/android/activity/eb;->b:Ljava/util/List;

    .line 225
    invoke-direct {p0}, Lcom/chase/sig/android/activity/eb;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 226
    invoke-direct {p0}, Lcom/chase/sig/android/activity/eb;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/chase/sig/android/activity/eb;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/SimpleDialogInfo;

    invoke-direct {p0, v0}, Lcom/chase/sig/android/activity/eb;->b(Lcom/chase/sig/android/domain/SimpleDialogInfo;)Lcom/chase/sig/android/view/k;

    move-result-object v0

    :try_start_0
    invoke-virtual {v0}, Lcom/chase/sig/android/view/k;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_1

    :cond_2
    move v0, v2

    .line 213
    goto :goto_0

    .line 230
    :cond_3
    if-eqz p1, :cond_4

    .line 231
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->n()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/chase/sig/android/activity/eb;->a(Landroid/os/Bundle;Landroid/view/ViewGroup;)V

    .line 234
    :cond_4
    const-string v0, "footnote_message"

    invoke-static {p1, v0}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 235
    const-string v0, "footnote_message"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/eb;->i:Ljava/lang/String;

    .line 236
    const-string v0, "footnote_header"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/chase/sig/android/activity/eb;->i:Ljava/lang/String;

    invoke-virtual {p0, v0, v3, v2}, Lcom/chase/sig/android/activity/eb;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 240
    :cond_5
    invoke-direct {p0}, Lcom/chase/sig/android/activity/eb;->f()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->n()Landroid/view/ViewGroup;

    move-result-object v3

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->b_()Z

    move-result v4

    if-eqz v4, :cond_8

    if-eqz v0, :cond_8

    if-eqz v3, :cond_8

    const v4, 0x7f030053

    invoke-static {p0, v4, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    const v5, 0x7f09002d

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v6, -0x2

    const/4 v7, -0x1

    invoke-direct {v5, v6, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v4, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v5, Lcom/chase/sig/android/activity/er;

    invoke-direct {v5, p0}, Lcom/chase/sig/android/activity/er;-><init>(Lcom/chase/sig/android/activity/eb;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v4, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-static {v0, v4}, Lcom/chase/sig/android/view/an;->a(Landroid/view/View;Landroid/view/View;)V

    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-direct {v2, v0}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v0, 0x3

    const v4, 0x7f09002c

    invoke-virtual {v2, v0, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    const v0, 0x7f030054

    invoke-static {p0, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/MenuView;

    invoke-direct {p0}, Lcom/chase/sig/android/activity/eb;->e()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/MenuView;->setSelectedOptionId(I)V

    invoke-virtual {v3, v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v1, 0x5

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    invoke-direct {p0, v0, v1}, Lcom/chase/sig/android/activity/eb;->a(Lcom/chase/sig/android/view/MenuView;[I)V

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/ChaseApplication;

    invoke-virtual {v1}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v1

    iget-object v1, v1, Lcom/chase/sig/android/domain/o;->b:Lcom/chase/sig/android/domain/g;

    if-eqz v1, :cond_6

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->x()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-direct {p0}, Lcom/chase/sig/android/activity/eb;->h()Z

    move-result v1

    if-nez v1, :cond_7

    :cond_6
    invoke-virtual {v0}, Lcom/chase/sig/android/view/MenuView;->c()V

    :cond_7
    const v1, 0x7f09002e

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/eb;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    sget-object v2, Landroid/widget/ImageView$ScaleType;->FIT_START:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    invoke-static {v1}, Lcom/chase/sig/android/activity/eb;->b(Landroid/view/View;)V

    const v1, 0x7f09002f

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/eb;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-static {v1}, Lcom/chase/sig/android/activity/eb;->b(Landroid/view/View;)V

    :goto_2
    iput-object v0, p0, Lcom/chase/sig/android/activity/eb;->c:Lcom/chase/sig/android/view/MenuView;

    .line 241
    return-void

    :cond_8
    move-object v0, v1

    goto :goto_2

    .line 240
    nop

    :array_0
    .array-data 0x4
        0xfct 0x2t 0x9t 0x7ft
        0xfdt 0x2t 0x9t 0x7ft
        0x12t 0x1t 0x9t 0x7ft
        0x13t 0x1t 0x9t 0x7ft
        0xfet 0x2t 0x9t 0x7ft
    .end array-data
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 6
    .parameter

    .prologue
    const v1, 0x7f07006a

    const/16 v3, -0xa

    const/4 v0, 0x0

    const/4 v5, -0x1

    .line 1001
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v2

    .line 1003
    if-eq p1, v5, :cond_0

    if-ne p1, v3, :cond_2

    .line 1004
    :cond_0
    if-ne p1, v3, :cond_1

    const/4 v0, 0x1

    .line 1005
    :cond_1
    new-instance v1, Lcom/chase/sig/android/activity/eu;

    invoke-direct {v1, p0, p0}, Lcom/chase/sig/android/activity/eu;-><init>(Lcom/chase/sig/android/activity/eb;Landroid/content/Context;)V

    .line 1012
    new-instance v2, Lcom/chase/sig/android/activity/ed;

    invoke-direct {v2, p0}, Lcom/chase/sig/android/activity/ed;-><init>(Lcom/chase/sig/android/activity/eb;)V

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 1020
    invoke-virtual {v1, v0}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 1021
    const v0, 0x7f03006b

    invoke-virtual {v1, v0}, Landroid/app/Dialog;->setContentView(I)V

    move-object v0, v1

    .line 1096
    :goto_0
    return-object v0

    .line 1025
    :cond_2
    const/4 v3, -0x3

    if-ne p1, v3, :cond_3

    .line 1026
    const v0, 0x7f0700b1

    invoke-virtual {p0, p0, v0}, Lcom/chase/sig/android/activity/eb;->a(Landroid/content/Context;I)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    .line 1027
    :cond_3
    const/4 v3, -0x4

    if-ne p1, v3, :cond_5

    .line 1028
    new-instance v2, Lcom/chase/sig/android/view/k$a;

    invoke-direct {v2, p0}, Lcom/chase/sig/android/view/k$a;-><init>(Landroid/content/Context;)V

    .line 1029
    sget-object v3, Lcom/chase/sig/android/c;->b:Landroid/content/Intent;

    if-eqz v3, :cond_4

    sget-object v3, Lcom/chase/sig/android/c;->b:Landroid/content/Intent;

    const-string v4, "receipt_cancel_warning"

    invoke-virtual {v3, v4, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    :cond_4
    invoke-virtual {v2, v1}, Lcom/chase/sig/android/view/k$a;->b(I)Lcom/chase/sig/android/view/k$a;

    move-result-object v1

    iput-boolean v0, v1, Lcom/chase/sig/android/view/k$a;->i:Z

    const v0, 0x7f07006b

    new-instance v3, Lcom/chase/sig/android/activity/ef;

    invoke-direct {v3, p0}, Lcom/chase/sig/android/activity/ef;-><init>(Lcom/chase/sig/android/activity/eb;)V

    invoke-virtual {v1, v0, v3}, Lcom/chase/sig/android/view/k$a;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    move-result-object v0

    const v1, 0x7f07006d

    new-instance v3, Lcom/chase/sig/android/activity/ee;

    invoke-direct {v3, p0}, Lcom/chase/sig/android/activity/ee;-><init>(Lcom/chase/sig/android/activity/eb;)V

    invoke-virtual {v0, v1, v3}, Lcom/chase/sig/android/view/k$a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    .line 1053
    invoke-virtual {v2, v5}, Lcom/chase/sig/android/view/k$a;->d(I)Lcom/chase/sig/android/view/k;

    move-result-object v0

    goto :goto_0

    .line 1055
    :cond_5
    const/4 v1, -0x2

    if-ne p1, v1, :cond_6

    .line 1057
    new-instance v1, Lcom/chase/sig/android/view/k$a;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/view/k$a;-><init>(Landroid/content/Context;)V

    .line 1058
    const v2, 0x7f0700ad

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/view/k$a;->b(I)Lcom/chase/sig/android/view/k$a;

    move-result-object v2

    const v3, 0x7f0700ae

    invoke-virtual {v2, v3}, Lcom/chase/sig/android/view/k$a;->c(I)Lcom/chase/sig/android/view/k$a;

    move-result-object v2

    iput-boolean v0, v2, Lcom/chase/sig/android/view/k$a;->i:Z

    const v0, 0x7f070068

    new-instance v3, Lcom/chase/sig/android/activity/eh;

    invoke-direct {v3, p0}, Lcom/chase/sig/android/activity/eh;-><init>(Lcom/chase/sig/android/activity/eb;)V

    invoke-virtual {v2, v0, v3}, Lcom/chase/sig/android/view/k$a;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    move-result-object v0

    const v2, 0x7f070069

    new-instance v3, Lcom/chase/sig/android/activity/eg;

    invoke-direct {v3, p0}, Lcom/chase/sig/android/activity/eg;-><init>(Lcom/chase/sig/android/activity/eb;)V

    invoke-virtual {v0, v2, v3}, Lcom/chase/sig/android/view/k$a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    .line 1075
    invoke-virtual {v1, v5}, Lcom/chase/sig/android/view/k$a;->d(I)Lcom/chase/sig/android/view/k;

    move-result-object v0

    goto :goto_0

    .line 1076
    :cond_6
    const/16 v0, -0x9

    if-eq p1, v0, :cond_7

    const/4 v0, -0x8

    if-ne p1, v0, :cond_8

    .line 1077
    :cond_7
    new-instance v0, Lcom/chase/sig/android/view/k$a;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/view/k$a;-><init>(Landroid/content/Context;)V

    .line 1078
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v1

    iget-object v1, v1, Lcom/chase/sig/android/domain/o;->c:Lcom/chase/sig/android/service/quickpay/TodoListResponse;

    invoke-virtual {v1}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/k$a;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/k$a;

    .line 1079
    const-string v1, "OK"

    new-instance v2, Lcom/chase/sig/android/activity/ei;

    invoke-direct {v2, p0}, Lcom/chase/sig/android/activity/ei;-><init>(Lcom/chase/sig/android/activity/eb;)V

    invoke-virtual {v0, v1, v2}, Lcom/chase/sig/android/view/k$a;->a(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    .line 1086
    invoke-virtual {v0, v5}, Lcom/chase/sig/android/view/k$a;->d(I)Lcom/chase/sig/android/view/k;

    move-result-object v0

    goto/16 :goto_0

    .line 1087
    :cond_8
    const/16 v0, -0xb

    if-ne p1, v0, :cond_9

    .line 1088
    new-instance v0, Lcom/chase/sig/android/view/k$a;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/view/k$a;-><init>(Landroid/content/Context;)V

    .line 1090
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v1

    iget-object v1, v1, Lcom/chase/sig/android/domain/o;->c:Lcom/chase/sig/android/service/quickpay/TodoListResponse;

    invoke-virtual {v1}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->p()Ljava/lang/String;

    move-result-object v1

    .line 1091
    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/k$a;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/k$a;

    .line 1092
    const-string v1, "OK"

    new-instance v2, Lcom/chase/sig/android/activity/a/b;

    invoke-direct {v2}, Lcom/chase/sig/android/activity/a/b;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/chase/sig/android/view/k$a;->a(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    .line 1093
    invoke-virtual {v0, v5}, Lcom/chase/sig/android/view/k$a;->d(I)Lcom/chase/sig/android/view/k;

    move-result-object v0

    goto/16 :goto_0

    :cond_9
    move-object v0, v2

    .line 1096
    goto/16 :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3
    .parameter

    .prologue
    const v2, 0x7f090306

    .line 689
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/ChaseApplication;

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->a:Lcom/chase/sig/android/domain/a;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/ChaseApplication;

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->b:Lcom/chase/sig/android/domain/g;

    if-nez v0, :cond_1

    .line 690
    :cond_0
    const/4 v0, 0x0

    .line 720
    :goto_0
    return v0

    .line 693
    :cond_1
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const/high16 v1, 0x7f0a

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 695
    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 696
    if-eqz v0, :cond_2

    .line 697
    const v1, 0x7f0700a6

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/eb;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 700
    const v1, 0x7f07009f

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/eb;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitleCondensed(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 703
    :cond_2
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->x()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/chase/sig/android/activity/eb;->h()Z

    move-result v0

    if-nez v0, :cond_4

    .line 704
    :cond_3
    const v0, 0x7f0902fe

    invoke-interface {p1, v0}, Landroid/view/Menu;->removeItem(I)V

    .line 707
    :cond_4
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/ChaseApplication;

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->b:Lcom/chase/sig/android/domain/g;

    invoke-interface {v0}, Lcom/chase/sig/android/domain/g;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 708
    const v0, 0x7f0902ff

    invoke-interface {p1, v0}, Landroid/view/Menu;->removeItem(I)V

    .line 709
    invoke-interface {p1, v2}, Landroid/view/Menu;->removeItem(I)V

    .line 712
    :cond_5
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->i()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 713
    const v0, 0x7f090304

    invoke-interface {p1, v0}, Landroid/view/Menu;->removeItem(I)V

    .line 714
    const v0, 0x7f090305

    invoke-interface {p1, v0}, Landroid/view/Menu;->removeItem(I)V

    .line 716
    :cond_6
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/ChaseApplication;

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->b:Lcom/chase/sig/android/domain/g;

    invoke-interface {v0}, Lcom/chase/sig/android/domain/g;->c()Z

    move-result v0

    if-nez v0, :cond_7

    .line 717
    const v0, 0x7f0902fd

    invoke-interface {p1, v0}, Landroid/view/Menu;->removeItem(I)V

    .line 720
    :cond_7
    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method protected onDestroy()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 463
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    new-array v0, v5, [Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    iget-object v0, p0, Lcom/chase/sig/android/activity/eb;->e:Lcom/chase/sig/android/activity/mc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/eb;->e:Lcom/chase/sig/android/activity/mc;

    iget-object v0, v0, Lcom/chase/sig/android/activity/mc;->a:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/activity/ae;

    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/ae;->f()V

    goto :goto_0

    .line 464
    :cond_0
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->n()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/chase/sig/android/activity/eb;->a(Landroid/view/View;)V

    .line 465
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 466
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 1473
    invoke-virtual {p0, p1}, Lcom/chase/sig/android/activity/eb;->j(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .parameter

    .prologue
    .line 558
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/eb;->c(I)Z

    move-result v0

    return v0
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 158
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/chase/sig/android/activity/eb;->g:Z

    .line 159
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 161
    iget-boolean v0, p0, Lcom/chase/sig/android/activity/eb;->h:Z

    if-nez v0, :cond_0

    .line 162
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->o()Lcom/chase/sig/android/f;

    move-result-object v0

    .line 163
    invoke-virtual {v0}, Lcom/chase/sig/android/f;->interrupt()V

    .line 164
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/chase/sig/android/ChaseApplication;->a(Lcom/chase/sig/android/f;)V

    .line 167
    :cond_0
    new-instance v0, Lcom/chase/sig/android/a;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/a;-><init>(Lcom/chase/sig/android/activity/eb;)V

    .line 168
    invoke-static {v0}, Lcom/chase/sig/android/ChaseApplication;->a(Lcom/chase/sig/android/a;)V

    .line 169
    invoke-virtual {v0}, Lcom/chase/sig/android/a;->start()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 171
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p0}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->b(Lcom/chase/sig/android/activity/eb;)V

    return-void

    :catch_0
    move-exception v0

    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p0}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->b(Lcom/chase/sig/android/activity/eb;)V

    throw v0
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 175
    :try_start_0
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 176
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/chase/sig/android/activity/eb;->g:Z

    .line 178
    iget-boolean v0, p0, Lcom/chase/sig/android/activity/eb;->h:Z

    if-nez v0, :cond_0

    .line 179
    new-instance v0, Lcom/chase/sig/android/f;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/f;-><init>(Lcom/chase/sig/android/activity/eb;)V

    .line 180
    invoke-virtual {v0}, Lcom/chase/sig/android/f;->start()V

    .line 181
    invoke-static {v0}, Lcom/chase/sig/android/ChaseApplication;->a(Lcom/chase/sig/android/f;)V

    .line 184
    :cond_0
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->p()Lcom/chase/sig/android/a;

    move-result-object v0

    .line 185
    if-eqz v0, :cond_1

    .line 187
    invoke-virtual {v0}, Lcom/chase/sig/android/a;->interrupt()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 189
    :cond_1
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p0}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Lcom/chase/sig/android/activity/eb;)V

    return-void

    :catch_0
    move-exception v0

    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p0}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Lcom/chase/sig/android/activity/eb;)V

    throw v0
.end method

.method public onRetainNonConfigurationInstance()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 493
    iget-object v0, p0, Lcom/chase/sig/android/activity/eb;->e:Lcom/chase/sig/android/activity/mc;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/mc;->a(Lcom/chase/sig/android/activity/eb;)V

    .line 494
    iget-object v0, p0, Lcom/chase/sig/android/activity/eb;->e:Lcom/chase/sig/android/activity/mc;

    return-object v0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 422
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 424
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->n()Landroid/view/ViewGroup;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->n()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/chase/sig/android/activity/eb;->b(Landroid/os/Bundle;Landroid/view/ViewGroup;)V

    .line 425
    :cond_0
    invoke-direct {p0}, Lcom/chase/sig/android/activity/eb;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 426
    const-string v1, "simple_dialogs"

    iget-object v0, p0, Lcom/chase/sig/android/activity/eb;->b:Ljava/util/List;

    check-cast v0, Ljava/io/Serializable;

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 428
    :cond_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/eb;->i:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 429
    const-string v0, "footnote_message"

    iget-object v1, p0, Lcom/chase/sig/android/activity/eb;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 430
    const-string v0, "footnote_header"

    iget-object v1, p0, Lcom/chase/sig/android/activity/eb;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 433
    :cond_2
    const-string v0, "is_logging_out"

    iget-boolean v1, p0, Lcom/chase/sig/android/activity/eb;->h:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 434
    return-void
.end method

.method public onSearchRequested()Z
    .locals 1

    .prologue
    .line 193
    const/4 v0, 0x1

    return v0
.end method

.method protected final p()Landroid/content/Intent;
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 855
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/ChaseApplication;

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->b:Lcom/chase/sig/android/domain/g;

    invoke-interface {v0}, Lcom/chase/sig/android/domain/g;->u()Ljava/util/List;

    move-result-object v3

    .line 856
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    .line 858
    :goto_0
    if-eqz v0, :cond_1

    .line 859
    new-instance v1, Landroid/content/Intent;

    const-class v0, Lcom/chase/sig/android/activity/EPayStartActivity;

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 860
    const-string v4, "selectedAccountId"

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/IAccount;

    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-object v0, v1

    .line 865
    :goto_1
    return-object v0

    :cond_0
    move v0, v2

    .line 856
    goto :goto_0

    .line 862
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/chase/sig/android/activity/EPaySelectToAccount;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 863
    const/high16 v1, 0x4000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    goto :goto_1
.end method

.method public final q()V
    .locals 3

    .prologue
    .line 869
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/chase/sig/android/activity/AccountsActivity;

    if-ne v0, v1, :cond_0

    .line 876
    :goto_0
    return-void

    .line 873
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/chase/sig/android/activity/AccountsActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 874
    const/high16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 875
    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/eb;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public final r()Lcom/chase/sig/android/domain/o;
    .locals 1

    .prologue
    .line 882
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/ChaseApplication;

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    return-object v0
.end method

.method public final s()Lcom/chase/sig/android/service/SplashResponse;
    .locals 1

    .prologue
    .line 886
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->d:Lcom/chase/sig/android/service/SplashResponse;

    return-object v0
.end method

.method public setTitle(I)V
    .locals 1
    .parameter

    .prologue
    .line 666
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/eb;->setTitle(Ljava/lang/CharSequence;)V

    .line 667
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 2
    .parameter

    .prologue
    .line 671
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->o()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 673
    const v0, 0x7f09002e

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/eb;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 674
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->o()Landroid/widget/TextView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 675
    return-void
.end method

.method public final t()V
    .locals 1

    .prologue
    .line 894
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/eb;->showDialog(I)V

    .line 895
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/eb;->c(Z)V

    .line 896
    return-void
.end method

.method public final u()V
    .locals 3

    .prologue
    .line 904
    const/4 v0, 0x0

    .line 906
    iget-object v1, p0, Lcom/chase/sig/android/activity/eb;->e:Lcom/chase/sig/android/activity/mc;

    invoke-virtual {v1}, Lcom/chase/sig/android/activity/mc;->a()Ljava/util/List;

    move-result-object v1

    .line 907
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/activity/ae;

    .line 908
    instance-of v0, v0, Lcom/chase/sig/android/d;

    if-eqz v0, :cond_2

    .line 909
    add-int/lit8 v0, v1, 0x1

    :goto_1
    move v1, v0

    goto :goto_0

    .line 913
    :cond_0
    const/4 v0, 0x1

    if-gt v1, v0, :cond_1

    .line 914
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/eb;->removeDialog(I)V

    .line 915
    const/16 v0, -0xa

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/eb;->removeDialog(I)V

    .line 917
    :cond_1
    return-void

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public final v()V
    .locals 1

    .prologue
    .line 920
    const v0, 0x7f07027c

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/eb;->f(I)V

    .line 921
    return-void
.end method

.method public final w()V
    .locals 2

    .prologue
    .line 924
    const v0, 0x7f0701f0

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 925
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 926
    return-void
.end method

.method protected final x()Z
    .locals 2

    .prologue
    .line 952
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v1, 0x4

    if-lt v0, v1, :cond_0

    .line 953
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "android.hardware.camera"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    .line 956
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected final y()V
    .locals 1

    .prologue
    .line 990
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->n()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/chase/sig/android/activity/eb;->a(Landroid/view/ViewGroup;)V

    .line 991
    return-void
.end method

.method public final z()Lcom/chase/sig/android/ChaseApplication;
    .locals 1

    .prologue
    .line 1291
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/ChaseApplication;

    return-object v0
.end method
