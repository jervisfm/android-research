.class final Lcom/chase/sig/android/activity/ex;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/LoginActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/LoginActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 128
    iput-object p1, p0, Lcom/chase/sig/android/activity/ex;->a:Lcom/chase/sig/android/activity/LoginActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 6
    .parameter

    .prologue
    .line 133
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 134
    new-instance v1, Landroid/content/Intent;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ex;->a:Lcom/chase/sig/android/activity/LoginActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/LoginActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    const-class v2, Lcom/chase/sig/android/activity/PrivacyNoticesActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 135
    iget-object v0, p0, Lcom/chase/sig/android/activity/ex;->a:Lcom/chase/sig/android/activity/LoginActivity;

    const v2, 0x7f09014a

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/activity/LoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 136
    const-string v2, "title"

    invoke-virtual {v0}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-object v0, v1

    .line 148
    :goto_0
    iget-object v1, p0, Lcom/chase/sig/android/activity/ex;->a:Lcom/chase/sig/android/activity/LoginActivity;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/activity/LoginActivity;->startActivity(Landroid/content/Intent;)V

    .line 149
    return-void

    .line 138
    :cond_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/chase/sig/android/activity/ex;->a:Lcom/chase/sig/android/activity/LoginActivity;

    invoke-virtual {v1}, Lcom/chase/sig/android/activity/LoginActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/chase/sig/android/activity/ManagedContentActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 139
    const-string v1, "webUrl"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/chase/sig/android/activity/ex;->a:Lcom/chase/sig/android/activity/LoginActivity;

    invoke-virtual {v3}, Lcom/chase/sig/android/activity/LoginActivity;->z()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "environment."

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/chase/sig/android/activity/ex;->a:Lcom/chase/sig/android/activity/LoginActivity;

    invoke-virtual {v5}, Lcom/chase/sig/android/activity/LoginActivity;->z()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v5

    invoke-virtual {v5}, Lcom/chase/sig/android/ChaseApplication;->f()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".mbb"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/chase/sig/android/ChaseApplication;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/chase/sig/android/activity/ex;->a:Lcom/chase/sig/android/activity/LoginActivity;

    invoke-virtual {v3}, Lcom/chase/sig/android/activity/LoginActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f07005c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 145
    const-string v1, "viewTitle"

    const v2, 0x7f07029e

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0
.end method
