.class final Lcom/chase/sig/android/activity/fe;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Landroid/content/pm/PackageInfo;

.field final synthetic b:Lcom/chase/sig/android/activity/LoginActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/LoginActivity;Landroid/content/pm/PackageInfo;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 382
    iput-object p1, p0, Lcom/chase/sig/android/activity/fe;->b:Lcom/chase/sig/android/activity/LoginActivity;

    iput-object p2, p0, Lcom/chase/sig/android/activity/fe;->a:Landroid/content/pm/PackageInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .parameter
    .parameter

    .prologue
    .line 386
    iget-object v0, p0, Lcom/chase/sig/android/activity/fe;->a:Landroid/content/pm/PackageInfo;

    if-eqz v0, :cond_0

    .line 388
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 389
    const-string v1, "android.intent.action.MAIN"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 390
    const-string v1, "android.intent.category.LAUNCHER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 391
    new-instance v1, Landroid/content/ComponentName;

    iget-object v2, p0, Lcom/chase/sig/android/activity/fe;->a:Landroid/content/pm/PackageInfo;

    iget-object v2, v2, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    const-string v3, "com.chase.sig.android.activity.HomeActivity"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 394
    iget-object v1, p0, Lcom/chase/sig/android/activity/fe;->b:Lcom/chase/sig/android/activity/LoginActivity;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/activity/LoginActivity;->startActivity(Landroid/content/Intent;)V

    .line 416
    :goto_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/fe;->b:Lcom/chase/sig/android/activity/LoginActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/LoginActivity;->z()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->s()V

    .line 417
    return-void

    .line 398
    :cond_0
    const/4 v0, 0x0

    .line 399
    iget-object v1, p0, Lcom/chase/sig/android/activity/fe;->b:Lcom/chase/sig/android/activity/LoginActivity;

    const v2, 0x7f070005

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/activity/LoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 405
    const-string v2, "Google_Android_Marketplace"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 406
    const-string v0, "market://details?id=com.jpm.sig.android"

    .line 408
    :cond_1
    const-string v2, "Amazon_Appstore"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 409
    const-string v0, "http://www.amazon.com/gp/mas/dl/android?p=com.jpm.sig.android"

    .line 412
    :cond_2
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 414
    iget-object v0, p0, Lcom/chase/sig/android/activity/fe;->b:Lcom/chase/sig/android/activity/LoginActivity;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/LoginActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
