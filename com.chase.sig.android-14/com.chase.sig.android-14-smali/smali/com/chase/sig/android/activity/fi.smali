.class final Lcom/chase/sig/android/activity/fi;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/MarketsActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/MarketsActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 209
    iput-object p1, p0, Lcom/chase/sig/android/activity/fi;->a:Lcom/chase/sig/android/activity/MarketsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 4
    .parameter

    .prologue
    .line 212
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/fi;->a:Lcom/chase/sig/android/activity/MarketsActivity;

    iget-object v1, p0, Lcom/chase/sig/android/activity/fi;->a:Lcom/chase/sig/android/activity/MarketsActivity;

    invoke-static {v1}, Lcom/chase/sig/android/activity/MarketsActivity;->a(Lcom/chase/sig/android/activity/MarketsActivity;)Lcom/chase/sig/android/view/JPTabWidget;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/view/JPTabWidget;->getSelectedTabId()I

    move-result v1

    invoke-static {v0, v1}, Lcom/chase/sig/android/activity/MarketsActivity;->a(Lcom/chase/sig/android/activity/MarketsActivity;I)Lcom/chase/sig/android/domain/Quote;

    move-result-object v0

    .line 213
    if-eqz v0, :cond_0

    .line 214
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/chase/sig/android/activity/fi;->a:Lcom/chase/sig/android/activity/MarketsActivity;

    const-class v3, Lcom/chase/sig/android/activity/QuoteChartsActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 215
    const-string v2, "transaction_object"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 216
    const-string v0, "exchangeName"

    iget-object v2, p0, Lcom/chase/sig/android/activity/fi;->a:Lcom/chase/sig/android/activity/MarketsActivity;

    iget-object v3, p0, Lcom/chase/sig/android/activity/fi;->a:Lcom/chase/sig/android/activity/MarketsActivity;

    invoke-static {v3}, Lcom/chase/sig/android/activity/MarketsActivity;->a(Lcom/chase/sig/android/activity/MarketsActivity;)Lcom/chase/sig/android/view/JPTabWidget;

    move-result-object v3

    invoke-virtual {v3}, Lcom/chase/sig/android/view/JPTabWidget;->getSelectedTabId()I

    move-result v3

    invoke-static {v2, v3}, Lcom/chase/sig/android/activity/MarketsActivity;->b(Lcom/chase/sig/android/activity/MarketsActivity;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 218
    iget-object v0, p0, Lcom/chase/sig/android/activity/fi;->a:Lcom/chase/sig/android/activity/MarketsActivity;

    const/16 v2, 0x23

    invoke-virtual {v0, v1, v2}, Lcom/chase/sig/android/activity/MarketsActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 220
    :cond_0
    return-void
.end method
