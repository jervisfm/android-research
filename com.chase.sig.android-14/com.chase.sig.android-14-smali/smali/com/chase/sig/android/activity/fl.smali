.class final Lcom/chase/sig/android/activity/fl;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 53
    iput-object p1, p0, Lcom/chase/sig/android/activity/fl;->a:Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 5
    .parameter

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 57
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 84
    :cond_0
    :goto_0
    return-void

    .line 59
    :pswitch_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/fl;->a:Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity;

    iget-object v0, v0, Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity;->e:Lcom/chase/sig/android/activity/mc;

    const-class v1, Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity$a;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/mc;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/ae;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity$a;

    .line 62
    invoke-virtual {v0}, Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity$a;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v1

    sget-object v2, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-eq v1, v2, :cond_0

    .line 63
    iput-boolean v3, v0, Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity$a;->a:Z

    .line 64
    new-array v1, v3, [Ljava/lang/String;

    iget-object v2, p0, Lcom/chase/sig/android/activity/fl;->a:Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity;

    invoke-static {v2}, Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity;->a(Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity;)Lcom/chase/sig/android/domain/MobileAdResponse;

    move-result-object v2

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/MobileAdResponse;->a()Lcom/chase/sig/android/domain/MobileAdResponseContent;

    move-result-object v2

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/MobileAdResponseContent;->b()Lcom/chase/sig/android/domain/ResponseActions;

    move-result-object v2

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/ResponseActions;->b()Lcom/chase/sig/android/domain/ResponseActions$Action;

    move-result-object v2

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/ResponseActions$Action;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 70
    :pswitch_1
    sput-boolean v3, Lcom/chase/sig/android/ChaseApplication;->a:Z

    .line 71
    iget-object v0, p0, Lcom/chase/sig/android/activity/fl;->a:Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity;

    iget-object v0, v0, Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity;->e:Lcom/chase/sig/android/activity/mc;

    const-class v1, Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity$a;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/mc;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/ae;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity$a;

    .line 74
    invoke-virtual {v0}, Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity$a;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v1

    sget-object v2, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-eq v1, v2, :cond_0

    .line 75
    iput-boolean v4, v0, Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity$a;->a:Z

    .line 76
    new-array v1, v3, [Ljava/lang/String;

    iget-object v2, p0, Lcom/chase/sig/android/activity/fl;->a:Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity;

    invoke-static {v2}, Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity;->a(Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity;)Lcom/chase/sig/android/domain/MobileAdResponse;

    move-result-object v2

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/MobileAdResponse;->a()Lcom/chase/sig/android/domain/MobileAdResponseContent;

    move-result-object v2

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/MobileAdResponseContent;->b()Lcom/chase/sig/android/domain/ResponseActions;

    move-result-object v2

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/ResponseActions;->a()Lcom/chase/sig/android/domain/ResponseActions$Action;

    move-result-object v2

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/ResponseActions$Action;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 57
    :pswitch_data_0
    .packed-switch 0x7f090020
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
