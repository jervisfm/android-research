.class final Lcom/chase/sig/android/activity/fm;
.super Landroid/webkit/WebViewClient;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 93
    iput-object p1, p0, Lcom/chase/sig/android/activity/fm;->a:Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 97
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 98
    iget-object v0, p0, Lcom/chase/sig/android/activity/fm;->a:Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity;->u()V

    .line 99
    return-void
.end method

.method public final onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 103
    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 104
    iget-object v0, p0, Lcom/chase/sig/android/activity/fm;->a:Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity;->t()V

    .line 105
    return-void
.end method

.method public final shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 109
    const-string v0, "tel:"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 110
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.DIAL"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 111
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 113
    iget-object v1, p0, Lcom/chase/sig/android/activity/fm;->a:Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity;->a(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 114
    iget-object v1, p0, Lcom/chase/sig/android/activity/fm;->a:Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/activity/MobileAdOfferDetailsActivity;->startActivity(Landroid/content/Intent;)V

    .line 116
    :cond_0
    const/4 v0, 0x1

    .line 122
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
