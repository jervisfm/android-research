.class final Lcom/chase/sig/android/activity/fn;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 194
    iput-object p1, p0, Lcom/chase/sig/android/activity/fn;->a:Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3
    .parameter

    .prologue
    .line 198
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/fn;->a:Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->c:Lcom/chase/sig/android/service/quickpay/TodoListResponse;

    .line 199
    iget-object v1, p0, Lcom/chase/sig/android/activity/fn;->a:Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;

    invoke-static {v1}, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->a(Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 200
    iget-object v0, p0, Lcom/chase/sig/android/activity/fn;->a:Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;

    iget-object v1, p0, Lcom/chase/sig/android/activity/fn;->a:Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;

    invoke-virtual {v1}, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->s()Lcom/chase/sig/android/service/SplashResponse;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/service/SplashResponse;->t()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->f(Ljava/lang/String;)V

    .line 212
    :goto_0
    return-void

    .line 201
    :cond_0
    iget-object v1, p0, Lcom/chase/sig/android/activity/fn;->a:Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;

    invoke-static {v1}, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->b(Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;)Lcom/chase/sig/android/domain/g;

    move-result-object v1

    invoke-interface {v1}, Lcom/chase/sig/android/domain/g;->g()Z

    move-result v1

    if-nez v1, :cond_1

    .line 202
    iget-object v0, p0, Lcom/chase/sig/android/activity/fn;->a:Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;

    const v1, 0x7f0701e7

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->g(I)V

    goto :goto_0

    .line 203
    :cond_1
    iget-object v1, p0, Lcom/chase/sig/android/activity/fn;->a:Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;

    invoke-static {v1}, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->b(Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;)Lcom/chase/sig/android/domain/g;

    move-result-object v1

    invoke-interface {v1}, Lcom/chase/sig/android/domain/g;->h()Z

    move-result v1

    if-nez v1, :cond_2

    .line 204
    iget-object v0, p0, Lcom/chase/sig/android/activity/fn;->a:Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;

    const v1, 0x7f0701e8

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->g(I)V

    goto :goto_0

    .line 205
    :cond_2
    if-nez v0, :cond_3

    .line 206
    iget-object v0, p0, Lcom/chase/sig/android/activity/fn;->a:Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;

    const v1, 0x7f0701e9

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->g(I)V

    goto :goto_0

    .line 207
    :cond_3
    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->e()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 208
    iget-object v1, p0, Lcom/chase/sig/android/activity/fn;->a:Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->g()Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->c(Ljava/util/List;)V

    goto :goto_0

    .line 210
    :cond_4
    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->d()I

    move-result v0

    if-nez v0, :cond_5

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/chase/sig/android/activity/fn;->a:Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;

    const-class v2, Lcom/chase/sig/android/activity/QuickPayHomeActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    :goto_1
    iget-object v1, p0, Lcom/chase/sig/android/activity/fn;->a:Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_5
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/chase/sig/android/activity/fn;->a:Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;

    const-class v2, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "quick_pay_skip_loading_todo_list"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_1
.end method
