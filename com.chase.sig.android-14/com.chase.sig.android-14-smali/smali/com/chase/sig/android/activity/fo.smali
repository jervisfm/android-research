.class final Lcom/chase/sig/android/activity/fo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/PositionDetailActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/PositionDetailActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 36
    iput-object p1, p0, Lcom/chase/sig/android/activity/fo;->a:Lcom/chase/sig/android/activity/PositionDetailActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3
    .parameter

    .prologue
    .line 40
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/chase/sig/android/activity/fo;->a:Lcom/chase/sig/android/activity/PositionDetailActivity;

    const-class v2, Lcom/chase/sig/android/activity/QuoteDetailsActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 41
    const-string v1, "ticker_symbol"

    iget-object v2, p0, Lcom/chase/sig/android/activity/fo;->a:Lcom/chase/sig/android/activity/PositionDetailActivity;

    iget-object v2, v2, Lcom/chase/sig/android/activity/PositionDetailActivity;->a:Lcom/chase/sig/android/domain/Position;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/Position;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 42
    const-string v1, "quote_code"

    iget-object v2, p0, Lcom/chase/sig/android/activity/fo;->a:Lcom/chase/sig/android/activity/PositionDetailActivity;

    iget-object v2, v2, Lcom/chase/sig/android/activity/PositionDetailActivity;->a:Lcom/chase/sig/android/domain/Position;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/Position;->y()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 43
    const-string v1, "quantity"

    iget-object v2, p0, Lcom/chase/sig/android/activity/fo;->a:Lcom/chase/sig/android/activity/PositionDetailActivity;

    iget-object v2, v2, Lcom/chase/sig/android/activity/PositionDetailActivity;->a:Lcom/chase/sig/android/domain/Position;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/Position;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 44
    const-string v1, "gain_loss"

    iget-object v2, p0, Lcom/chase/sig/android/activity/fo;->a:Lcom/chase/sig/android/activity/PositionDetailActivity;

    iget-object v2, v2, Lcom/chase/sig/android/activity/PositionDetailActivity;->a:Lcom/chase/sig/android/domain/Position;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/Position;->d()Lcom/chase/sig/android/util/Dollar;

    move-result-object v2

    invoke-virtual {v2}, Lcom/chase/sig/android/util/Dollar;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 45
    iget-object v1, p0, Lcom/chase/sig/android/activity/fo;->a:Lcom/chase/sig/android/activity/PositionDetailActivity;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/activity/PositionDetailActivity;->startActivity(Landroid/content/Intent;)V

    .line 46
    return-void
.end method
