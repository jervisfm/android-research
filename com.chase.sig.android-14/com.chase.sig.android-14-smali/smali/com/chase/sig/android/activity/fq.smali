.class final Lcom/chase/sig/android/activity/fq;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/PositionsActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/PositionsActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 262
    iput-object p1, p0, Lcom/chase/sig/android/activity/fq;->a:Lcom/chase/sig/android/activity/PositionsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 4
    .parameter

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x0

    .line 267
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    iget-object v1, p0, Lcom/chase/sig/android/activity/fq;->a:Lcom/chase/sig/android/activity/PositionsActivity;

    invoke-static {v1}, Lcom/chase/sig/android/activity/PositionsActivity;->a(Lcom/chase/sig/android/activity/PositionsActivity;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 268
    new-instance v1, Landroid/view/animation/AlphaAnimation;

    const/high16 v2, 0x3f80

    invoke-direct {v1, v3, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 269
    const-wide/16 v2, 0xfa

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 271
    iget-object v2, p0, Lcom/chase/sig/android/activity/fq;->a:Lcom/chase/sig/android/activity/PositionsActivity;

    invoke-static {v2}, Lcom/chase/sig/android/activity/PositionsActivity;->b(Lcom/chase/sig/android/activity/PositionsActivity;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 272
    iget-object v2, p0, Lcom/chase/sig/android/activity/fq;->a:Lcom/chase/sig/android/activity/PositionsActivity;

    invoke-static {v2}, Lcom/chase/sig/android/activity/PositionsActivity;->c(Lcom/chase/sig/android/activity/PositionsActivity;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 273
    iget-object v2, p0, Lcom/chase/sig/android/activity/fq;->a:Lcom/chase/sig/android/activity/PositionsActivity;

    invoke-static {v2}, Lcom/chase/sig/android/activity/PositionsActivity;->d(Lcom/chase/sig/android/activity/PositionsActivity;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 275
    iget-object v2, p0, Lcom/chase/sig/android/activity/fq;->a:Lcom/chase/sig/android/activity/PositionsActivity;

    invoke-static {v2}, Lcom/chase/sig/android/activity/PositionsActivity;->d(Lcom/chase/sig/android/activity/PositionsActivity;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 276
    iget-object v2, p0, Lcom/chase/sig/android/activity/fq;->a:Lcom/chase/sig/android/activity/PositionsActivity;

    invoke-static {v2}, Lcom/chase/sig/android/activity/PositionsActivity;->b(Lcom/chase/sig/android/activity/PositionsActivity;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 277
    iget-object v2, p0, Lcom/chase/sig/android/activity/fq;->a:Lcom/chase/sig/android/activity/PositionsActivity;

    invoke-static {v2}, Lcom/chase/sig/android/activity/PositionsActivity;->c(Lcom/chase/sig/android/activity/PositionsActivity;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 279
    iget-object v1, p0, Lcom/chase/sig/android/activity/fq;->a:Lcom/chase/sig/android/activity/PositionsActivity;

    invoke-static {v1}, Lcom/chase/sig/android/activity/PositionsActivity;->e(Lcom/chase/sig/android/activity/PositionsActivity;)Landroid/widget/Button;

    move-result-object v1

    const v2, 0x7f0200ee

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 295
    :goto_0
    iget-object v1, p0, Lcom/chase/sig/android/activity/fq;->a:Lcom/chase/sig/android/activity/PositionsActivity;

    iget-object v2, p0, Lcom/chase/sig/android/activity/fq;->a:Lcom/chase/sig/android/activity/PositionsActivity;

    invoke-static {v2}, Lcom/chase/sig/android/activity/PositionsActivity;->a(Lcom/chase/sig/android/activity/PositionsActivity;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-static {v1, v0}, Lcom/chase/sig/android/activity/PositionsActivity;->a(Lcom/chase/sig/android/activity/PositionsActivity;Z)Z

    .line 296
    return-void

    .line 283
    :cond_1
    new-instance v1, Landroid/view/animation/AlphaAnimation;

    const v2, 0x3f4ccccd

    invoke-direct {v1, v2, v3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 284
    const-wide/16 v2, 0xc8

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 286
    iget-object v2, p0, Lcom/chase/sig/android/activity/fq;->a:Lcom/chase/sig/android/activity/PositionsActivity;

    invoke-static {v2}, Lcom/chase/sig/android/activity/PositionsActivity;->d(Lcom/chase/sig/android/activity/PositionsActivity;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 287
    iget-object v2, p0, Lcom/chase/sig/android/activity/fq;->a:Lcom/chase/sig/android/activity/PositionsActivity;

    invoke-static {v2}, Lcom/chase/sig/android/activity/PositionsActivity;->b(Lcom/chase/sig/android/activity/PositionsActivity;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 288
    iget-object v2, p0, Lcom/chase/sig/android/activity/fq;->a:Lcom/chase/sig/android/activity/PositionsActivity;

    invoke-static {v2}, Lcom/chase/sig/android/activity/PositionsActivity;->c(Lcom/chase/sig/android/activity/PositionsActivity;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 290
    iget-object v2, p0, Lcom/chase/sig/android/activity/fq;->a:Lcom/chase/sig/android/activity/PositionsActivity;

    invoke-static {v2}, Lcom/chase/sig/android/activity/PositionsActivity;->f(Lcom/chase/sig/android/activity/PositionsActivity;)Landroid/view/animation/Animation$AnimationListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/animation/AlphaAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 292
    iget-object v1, p0, Lcom/chase/sig/android/activity/fq;->a:Lcom/chase/sig/android/activity/PositionsActivity;

    invoke-static {v1}, Lcom/chase/sig/android/activity/PositionsActivity;->e(Lcom/chase/sig/android/activity/PositionsActivity;)Landroid/widget/Button;

    move-result-object v1

    const v2, 0x7f0200ed

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setBackgroundResource(I)V

    goto :goto_0
.end method
