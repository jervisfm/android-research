.class final Lcom/chase/sig/android/activity/fu;
.super Landroid/webkit/WebViewClient;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/PrivacyNoticeDetailActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/PrivacyNoticeDetailActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 116
    iput-object p1, p0, Lcom/chase/sig/android/activity/fu;->a:Lcom/chase/sig/android/activity/PrivacyNoticeDetailActivity;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method


# virtual methods
.method public final shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 4
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x1

    .line 118
    const-string v0, "tel:"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 119
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.DIAL"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 120
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 121
    iget-object v1, p0, Lcom/chase/sig/android/activity/fu;->a:Lcom/chase/sig/android/activity/PrivacyNoticeDetailActivity;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/activity/PrivacyNoticeDetailActivity;->a(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 122
    iget-object v1, p0, Lcom/chase/sig/android/activity/fu;->a:Lcom/chase/sig/android/activity/PrivacyNoticeDetailActivity;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/activity/PrivacyNoticeDetailActivity;->startActivity(Landroid/content/Intent;)V

    .line 133
    :cond_0
    :goto_0
    return v3

    .line 129
    :cond_1
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/chase/sig/android/activity/fu;->a:Lcom/chase/sig/android/activity/PrivacyNoticeDetailActivity;

    invoke-virtual {v1}, Lcom/chase/sig/android/activity/PrivacyNoticeDetailActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/chase/sig/android/activity/ManagedContentActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 130
    const-string v1, "webUrl"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 131
    iget-object v1, p0, Lcom/chase/sig/android/activity/fu;->a:Lcom/chase/sig/android/activity/PrivacyNoticeDetailActivity;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/activity/PrivacyNoticeDetailActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
