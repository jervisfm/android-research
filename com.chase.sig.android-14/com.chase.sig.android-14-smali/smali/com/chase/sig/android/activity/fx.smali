.class final Lcom/chase/sig/android/activity/fx;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 72
    iput-object p1, p0, Lcom/chase/sig/android/activity/fx;->a:Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 77
    :try_start_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/fx;->a:Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;->a(Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    .line 78
    iget-object v0, p0, Lcom/chase/sig/android/activity/fx;->a:Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;->a(Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 80
    const-string v2, "title"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 82
    iget-object v2, p0, Lcom/chase/sig/android/activity/fx;->a:Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;

    const v3, 0x7f07028c

    invoke-virtual {v2, v3}, Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 83
    const v0, 0x7f0900bc

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 84
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/chase/sig/android/activity/fx;->a:Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;

    const-class v3, Lcom/chase/sig/android/activity/PrivacyNoticesActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 87
    const-string v2, "title"

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 88
    iget-object v0, p0, Lcom/chase/sig/android/activity/fx;->a:Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 114
    :cond_0
    :goto_0
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1, p2, p3}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/widget/AdapterView;Landroid/view/View;I)V

    return-void

    .line 94
    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/chase/sig/android/activity/fx;->a:Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;

    const v3, 0x7f070290

    invoke-virtual {v2, v3}, Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/chase/sig/android/activity/fx;->a:Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;

    const v3, 0x7f070291

    invoke-virtual {v2, v3}, Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/chase/sig/android/activity/fx;->a:Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;

    const v3, 0x7f070292

    invoke-virtual {v2, v3}, Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/chase/sig/android/activity/fx;->a:Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;

    const v3, 0x7f070293

    invoke-virtual {v2, v3}, Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    move v0, p3

    .line 104
    :goto_1
    if-eq v0, v1, :cond_0

    .line 105
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/chase/sig/android/activity/fx;->a:Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;

    const-class v2, Lcom/chase/sig/android/activity/PrivateBankingDisclosureDetails;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 108
    const-string v1, "disclosure_document_index"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 111
    iget-object v1, p0, Lcom/chase/sig/android/activity/fx;->a:Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 114
    :catch_0
    move-exception v0

    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1, p2, p3}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/widget/AdapterView;Landroid/view/View;I)V

    throw v0

    :cond_3
    move v0, v1

    goto :goto_1
.end method
