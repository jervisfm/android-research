.class final Lcom/chase/sig/android/activity/fz;
.super Landroid/widget/SimpleAdapter;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;Landroid/content/Context;Ljava/util/List;[Ljava/lang/String;[I)V
    .locals 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 177
    iput-object p1, p0, Lcom/chase/sig/android/activity/fz;->a:Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;

    const v3, 0x7f030035

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Landroid/widget/SimpleAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V

    return-void
.end method

.method private a(I)Z
    .locals 2
    .parameter

    .prologue
    .line 205
    invoke-virtual {p0, p1}, Lcom/chase/sig/android/activity/fz;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 206
    const-string v1, "title"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 207
    invoke-static {v0}, Lcom/chase/sig/android/util/s;->l(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x0

    .line 180
    invoke-super {p0, p1, p2, p3}, Landroid/widget/SimpleAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 181
    const v0, 0x7f0900bd

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 183
    invoke-direct {p0, p1}, Lcom/chase/sig/android/activity/fz;->a(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 184
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 185
    invoke-virtual {v1}, Landroid/view/View;->getPaddingLeft()I

    move-result v0

    invoke-virtual {v1}, Landroid/view/View;->getPaddingRight()I

    move-result v2

    invoke-virtual {v1, v0, v5, v2, v5}, Landroid/view/View;->setPadding(IIII)V

    .line 186
    iget-object v0, p0, Lcom/chase/sig/android/activity/fz;->a:Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f060025

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 194
    :goto_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/fz;->a:Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;->b(Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/fz;->a:Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;->b(Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->n(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 195
    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/fz;->a:Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;->a(Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/ListView;->setFooterDividersEnabled(Z)V

    .line 200
    :goto_1
    return-object v1

    .line 188
    :cond_1
    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 189
    invoke-virtual {v1}, Landroid/view/View;->getPaddingLeft()I

    move-result v0

    invoke-virtual {v1}, Landroid/view/View;->getPaddingLeft()I

    move-result v2

    invoke-virtual {v1}, Landroid/view/View;->getPaddingLeft()I

    move-result v3

    invoke-virtual {v1}, Landroid/view/View;->getPaddingLeft()I

    move-result v4

    invoke-virtual {v1, v0, v2, v3, v4}, Landroid/view/View;->setPadding(IIII)V

    .line 191
    invoke-virtual {v1, v5}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_0

    .line 197
    :cond_2
    iget-object v0, p0, Lcom/chase/sig/android/activity/fz;->a:Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;->a(Lcom/chase/sig/android/activity/PrivateBankingDisclosuresActivity;)Landroid/widget/ListView;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setFooterDividersEnabled(Z)V

    goto :goto_1
.end method

.method public final isEnabled(I)Z
    .locals 1
    .parameter

    .prologue
    .line 212
    invoke-direct {p0, p1}, Lcom/chase/sig/android/activity/fz;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
