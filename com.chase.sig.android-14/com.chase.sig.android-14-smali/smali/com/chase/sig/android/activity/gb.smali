.class final Lcom/chase/sig/android/activity/gb;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/CapturePreview;

.field final synthetic b:Lcom/chase/sig/android/activity/QuickDepositCaptureActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/QuickDepositCaptureActivity;Lcom/chase/sig/android/activity/CapturePreview;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 70
    iput-object p1, p0, Lcom/chase/sig/android/activity/gb;->b:Lcom/chase/sig/android/activity/QuickDepositCaptureActivity;

    iput-object p2, p0, Lcom/chase/sig/android/activity/gb;->a:Lcom/chase/sig/android/activity/CapturePreview;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 2
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 75
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/gb;->b:Lcom/chase/sig/android/activity/QuickDepositCaptureActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/QuickDepositCaptureActivity;->b(Lcom/chase/sig/android/activity/QuickDepositCaptureActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 77
    iget-object v0, p0, Lcom/chase/sig/android/activity/gb;->a:Lcom/chase/sig/android/activity/CapturePreview;

    iget-object v0, v0, Lcom/chase/sig/android/activity/CapturePreview;->b:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v0

    .line 78
    invoke-virtual {v0, v1}, Landroid/hardware/Camera$Parameters;->setRotation(I)V

    .line 79
    iget-object v1, p0, Lcom/chase/sig/android/activity/gb;->a:Lcom/chase/sig/android/activity/CapturePreview;

    iget-object v1, v1, Lcom/chase/sig/android/activity/CapturePreview;->b:Landroid/hardware/Camera;

    invoke-virtual {v1, v0}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    .line 81
    iget-object v0, p0, Lcom/chase/sig/android/activity/gb;->a:Lcom/chase/sig/android/activity/CapturePreview;

    iget-object v0, v0, Lcom/chase/sig/android/activity/CapturePreview;->b:Landroid/hardware/Camera;

    new-instance v1, Lcom/chase/sig/android/activity/gc;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/gc;-><init>(Lcom/chase/sig/android/activity/gb;)V

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->autoFocus(Landroid/hardware/Camera$AutoFocusCallback;)V

    .line 91
    return-void
.end method
