.class final Lcom/chase/sig/android/activity/gf;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/hardware/Camera$PictureCallback;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/QuickDepositCaptureActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/QuickDepositCaptureActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 133
    iput-object p1, p0, Lcom/chase/sig/android/activity/gf;->a:Lcom/chase/sig/android/activity/QuickDepositCaptureActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPictureTaken([BLandroid/hardware/Camera;)V
    .locals 4
    .parameter
    .parameter

    .prologue
    .line 137
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/chase/sig/android/activity/gf;->a:Lcom/chase/sig/android/activity/QuickDepositCaptureActivity;

    const-class v2, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 139
    const-string v1, "qd_image_side"

    iget-object v2, p0, Lcom/chase/sig/android/activity/gf;->a:Lcom/chase/sig/android/activity/QuickDepositCaptureActivity;

    invoke-static {v2}, Lcom/chase/sig/android/activity/QuickDepositCaptureActivity;->c(Lcom/chase/sig/android/activity/QuickDepositCaptureActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 140
    const-string v1, "quick_deposit"

    iget-object v2, p0, Lcom/chase/sig/android/activity/gf;->a:Lcom/chase/sig/android/activity/QuickDepositCaptureActivity;

    invoke-virtual {v2}, Lcom/chase/sig/android/activity/QuickDepositCaptureActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "quick_deposit"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 142
    const-string v1, "image_data"

    invoke-static {p1}, Lcom/chase/sig/android/activity/QuickDepositCaptureActivity;->a([B)[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 143
    iget-object v1, p0, Lcom/chase/sig/android/activity/gf;->a:Lcom/chase/sig/android/activity/QuickDepositCaptureActivity;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/activity/QuickDepositCaptureActivity;->startActivity(Landroid/content/Intent;)V

    .line 144
    return-void
.end method
