.class final Lcom/chase/sig/android/activity/gg;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/QuickDepositCompleteActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/QuickDepositCompleteActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 69
    iput-object p1, p0, Lcom/chase/sig/android/activity/gg;->a:Lcom/chase/sig/android/activity/QuickDepositCompleteActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3
    .parameter

    .prologue
    .line 73
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/chase/sig/android/activity/gg;->a:Lcom/chase/sig/android/activity/QuickDepositCompleteActivity;

    invoke-virtual {v1}, Lcom/chase/sig/android/activity/QuickDepositCompleteActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/chase/sig/android/activity/QuickDepositReceiptActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 74
    const-string v1, "quick_deposit"

    iget-object v2, p0, Lcom/chase/sig/android/activity/gg;->a:Lcom/chase/sig/android/activity/QuickDepositCompleteActivity;

    invoke-static {v2}, Lcom/chase/sig/android/activity/QuickDepositCompleteActivity;->a(Lcom/chase/sig/android/activity/QuickDepositCompleteActivity;)Lcom/chase/sig/android/domain/QuickDeposit;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 75
    iget-object v1, p0, Lcom/chase/sig/android/activity/gg;->a:Lcom/chase/sig/android/activity/QuickDepositCompleteActivity;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/activity/QuickDepositCompleteActivity;->startActivity(Landroid/content/Intent;)V

    .line 76
    return-void
.end method
