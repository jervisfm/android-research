.class final Lcom/chase/sig/android/activity/gi;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 127
    iput-object p1, p0, Lcom/chase/sig/android/activity/gi;->a:Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3
    .parameter

    .prologue
    .line 131
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/chase/sig/android/activity/gi;->a:Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;

    const-class v2, Lcom/chase/sig/android/activity/QuickDepositCaptureActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 134
    const-string v1, "qd_image_side"

    iget-object v2, p0, Lcom/chase/sig/android/activity/gi;->a:Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;

    invoke-static {v2}, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;->a(Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 135
    const-string v1, "quick_deposit"

    iget-object v2, p0, Lcom/chase/sig/android/activity/gi;->a:Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;

    invoke-static {v2}, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;->b(Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;)Lcom/chase/sig/android/domain/QuickDeposit;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 136
    const/high16 v1, 0x4000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 137
    iget-object v1, p0, Lcom/chase/sig/android/activity/gi;->a:Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;->startActivity(Landroid/content/Intent;)V

    .line 138
    return-void
.end method
