.class final Lcom/chase/sig/android/activity/gj;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 143
    iput-object p1, p0, Lcom/chase/sig/android/activity/gj;->a:Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 4
    .parameter

    .prologue
    .line 147
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/gj;->a:Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;->b(Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;)Lcom/chase/sig/android/domain/QuickDeposit;

    move-result-object v0

    .line 148
    iget-object v1, p0, Lcom/chase/sig/android/activity/gj;->a:Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;

    invoke-virtual {v1}, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "qd_image_side"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 149
    const-string v2, "qd_check_front_image"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 150
    iget-object v1, p0, Lcom/chase/sig/android/activity/gj;->a:Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;

    invoke-virtual {v1}, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "image_data"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/QuickDeposit;->b([B)V

    .line 156
    :goto_0
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/chase/sig/android/activity/gj;->a:Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;

    const-class v3, Lcom/chase/sig/android/activity/QuickDepositStartActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 159
    const-string v2, "quick_deposit"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 160
    const/high16 v0, 0x2

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 161
    iget-object v0, p0, Lcom/chase/sig/android/activity/gj;->a:Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;->startActivity(Landroid/content/Intent;)V

    .line 162
    iget-object v0, p0, Lcom/chase/sig/android/activity/gj;->a:Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;->finish()V

    .line 163
    return-void

    .line 153
    :cond_0
    iget-object v1, p0, Lcom/chase/sig/android/activity/gj;->a:Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;

    invoke-virtual {v1}, Lcom/chase/sig/android/activity/QuickDepositReviewImageActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "image_data"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/QuickDeposit;->a([B)V

    goto :goto_0
.end method
