.class final Lcom/chase/sig/android/activity/gu;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/QuickDepositStartActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/QuickDepositStartActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 232
    iput-object p1, p0, Lcom/chase/sig/android/activity/gu;->a:Lcom/chase/sig/android/activity/QuickDepositStartActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3
    .parameter

    .prologue
    .line 238
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/chase/sig/android/activity/gu;->a:Lcom/chase/sig/android/activity/QuickDepositStartActivity;

    const-class v2, Lcom/chase/sig/android/activity/QuickDepositCaptureActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 240
    const-string v1, "qd_image_side"

    const-string v2, "qd_check_front_image"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 241
    const-string v1, "quick_deposit"

    iget-object v2, p0, Lcom/chase/sig/android/activity/gu;->a:Lcom/chase/sig/android/activity/QuickDepositStartActivity;

    invoke-static {v2}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->c(Lcom/chase/sig/android/activity/QuickDepositStartActivity;)Lcom/chase/sig/android/domain/QuickDeposit;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 242
    const/high16 v1, 0x4000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 243
    iget-object v1, p0, Lcom/chase/sig/android/activity/gu;->a:Lcom/chase/sig/android/activity/QuickDepositStartActivity;

    const-string v2, "quickDepositShouldShowInstructions"

    invoke-virtual {v1, v0, v2}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->a(Landroid/content/Intent;Ljava/lang/String;)V

    .line 245
    return-void
.end method
