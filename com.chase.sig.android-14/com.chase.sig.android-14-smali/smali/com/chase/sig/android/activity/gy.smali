.class final Lcom/chase/sig/android/activity/gy;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/QuickDepositStartActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/QuickDepositStartActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 396
    iput-object p1, p0, Lcom/chase/sig/android/activity/gy;->a:Lcom/chase/sig/android/activity/QuickDepositStartActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 4
    .parameter

    .prologue
    .line 401
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/gy;->a:Lcom/chase/sig/android/activity/QuickDepositStartActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->f(Lcom/chase/sig/android/activity/QuickDepositStartActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 403
    iget-object v0, p0, Lcom/chase/sig/android/activity/gy;->a:Lcom/chase/sig/android/activity/QuickDepositStartActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->c(Lcom/chase/sig/android/activity/QuickDepositStartActivity;)Lcom/chase/sig/android/domain/QuickDeposit;

    move-result-object v0

    iget-object v1, p0, Lcom/chase/sig/android/activity/gy;->a:Lcom/chase/sig/android/activity/QuickDepositStartActivity;

    invoke-virtual {v1}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->d()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/QuickDeposit;->a(I)V

    .line 404
    iget-object v0, p0, Lcom/chase/sig/android/activity/gy;->a:Lcom/chase/sig/android/activity/QuickDepositStartActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->c(Lcom/chase/sig/android/activity/QuickDepositStartActivity;)Lcom/chase/sig/android/domain/QuickDeposit;

    move-result-object v0

    iget-object v1, p0, Lcom/chase/sig/android/activity/gy;->a:Lcom/chase/sig/android/activity/QuickDepositStartActivity;

    invoke-static {v1}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->g(Lcom/chase/sig/android/activity/QuickDepositStartActivity;)Lcom/chase/sig/android/view/AmountView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/view/AmountView;->getDollarAmount()Lcom/chase/sig/android/util/Dollar;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/QuickDeposit;->a(Lcom/chase/sig/android/util/Dollar;)V

    .line 406
    iget-object v0, p0, Lcom/chase/sig/android/activity/gy;->a:Lcom/chase/sig/android/activity/QuickDepositStartActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->i(Lcom/chase/sig/android/activity/QuickDepositStartActivity;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/chase/sig/android/activity/gy;->a:Lcom/chase/sig/android/activity/QuickDepositStartActivity;

    invoke-static {v1}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->h(Lcom/chase/sig/android/activity/QuickDepositStartActivity;)Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/view/JPSpinner;->getSelectedItemPosition()I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/QuickDepositAccount;

    .line 408
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickDepositAccount;->d()Ljava/lang/String;

    .line 409
    iget-object v1, p0, Lcom/chase/sig/android/activity/gy;->a:Lcom/chase/sig/android/activity/QuickDepositStartActivity;

    invoke-static {v1}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->c(Lcom/chase/sig/android/activity/QuickDepositStartActivity;)Lcom/chase/sig/android/domain/QuickDeposit;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/QuickDeposit;->a(Lcom/chase/sig/android/domain/QuickDepositAccount;)V

    .line 411
    iget-object v1, p0, Lcom/chase/sig/android/activity/gy;->a:Lcom/chase/sig/android/activity/QuickDepositStartActivity;

    invoke-static {v1}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->j(Lcom/chase/sig/android/activity/QuickDepositStartActivity;)Landroid/widget/ListAdapter;

    move-result-object v1

    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    if-eqz v1, :cond_1

    .line 412
    iget-object v1, p0, Lcom/chase/sig/android/activity/gy;->a:Lcom/chase/sig/android/activity/QuickDepositStartActivity;

    invoke-static {v1}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->a(Lcom/chase/sig/android/activity/QuickDepositStartActivity;)Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/view/JPSpinner;->getSelectedItemPosition()I

    move-result v1

    .line 413
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickDepositAccount;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/QuickDepositAccount$Location;

    .line 415
    iget-object v1, p0, Lcom/chase/sig/android/activity/gy;->a:Lcom/chase/sig/android/activity/QuickDepositStartActivity;

    invoke-static {v1}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->c(Lcom/chase/sig/android/activity/QuickDepositStartActivity;)Lcom/chase/sig/android/domain/QuickDeposit;

    move-result-object v1

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickDepositAccount$Location;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/domain/QuickDeposit;->f(Ljava/lang/String;)V

    .line 416
    iget-object v1, p0, Lcom/chase/sig/android/activity/gy;->a:Lcom/chase/sig/android/activity/QuickDepositStartActivity;

    invoke-static {v1}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->c(Lcom/chase/sig/android/activity/QuickDepositStartActivity;)Lcom/chase/sig/android/domain/QuickDeposit;

    move-result-object v1

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickDepositAccount$Location;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/QuickDeposit;->e(Ljava/lang/String;)V

    .line 422
    :goto_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/gy;->a:Lcom/chase/sig/android/activity/QuickDepositStartActivity;

    iget-object v0, v0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->e:Lcom/chase/sig/android/activity/mc;

    const-class v1, Lcom/chase/sig/android/activity/QuickDepositStartActivity$b;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/mc;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/ae;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/activity/QuickDepositStartActivity$b;

    .line 425
    invoke-virtual {v0}, Lcom/chase/sig/android/activity/QuickDepositStartActivity$b;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v1

    sget-object v2, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-eq v1, v2, :cond_0

    .line 426
    const/4 v1, 0x1

    new-array v1, v1, [Lcom/chase/sig/android/domain/QuickDeposit;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/chase/sig/android/activity/gy;->a:Lcom/chase/sig/android/activity/QuickDepositStartActivity;

    invoke-static {v3}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->c(Lcom/chase/sig/android/activity/QuickDepositStartActivity;)Lcom/chase/sig/android/domain/QuickDeposit;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/QuickDepositStartActivity$b;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 429
    :cond_0
    return-void

    .line 418
    :cond_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/gy;->a:Lcom/chase/sig/android/activity/QuickDepositStartActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->c(Lcom/chase/sig/android/activity/QuickDepositStartActivity;)Lcom/chase/sig/android/domain/QuickDeposit;

    move-result-object v0

    const-string v1, "N/A"

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/QuickDeposit;->f(Ljava/lang/String;)V

    .line 419
    iget-object v0, p0, Lcom/chase/sig/android/activity/gy;->a:Lcom/chase/sig/android/activity/QuickDepositStartActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/QuickDepositStartActivity;->c(Lcom/chase/sig/android/activity/QuickDepositStartActivity;)Lcom/chase/sig/android/domain/QuickDeposit;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/QuickDeposit;->e(Ljava/lang/String;)V

    goto :goto_0
.end method
