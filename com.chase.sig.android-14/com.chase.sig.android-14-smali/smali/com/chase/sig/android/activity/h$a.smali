.class public abstract Lcom/chase/sig/android/activity/h$a;
.super Lcom/chase/sig/android/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/h;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/chase/sig/android/domain/Transaction;",
        ">",
        "Lcom/chase/sig/android/d",
        "<",
        "Lcom/chase/sig/android/activity/h",
        "<TT;>;TT;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/service/movemoney/ServiceResponse;",
        ">;"
    }
.end annotation


# instance fields
.field a:Lcom/chase/sig/android/domain/Transaction;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 125
    invoke-direct {p0}, Lcom/chase/sig/android/d;-><init>()V

    return-void
.end method


# virtual methods
.method protected abstract a()Lcom/chase/sig/android/service/movemoney/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/chase/sig/android/service/movemoney/d",
            "<TT;>;"
        }
    .end annotation
.end method

.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .parameter

    .prologue
    .line 125
    check-cast p1, [Lcom/chase/sig/android/domain/Transaction;

    const/4 v0, 0x0

    aget-object v0, p1, v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/h$a;->a:Lcom/chase/sig/android/domain/Transaction;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/h$a;->a()Lcom/chase/sig/android/service/movemoney/d;

    move-result-object v1

    iget-object v2, p0, Lcom/chase/sig/android/activity/h$a;->a:Lcom/chase/sig/android/domain/Transaction;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/h;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/h;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/chase/sig/android/service/movemoney/RequestFlags;->a:Lcom/chase/sig/android/service/movemoney/RequestFlags;

    :goto_0
    invoke-virtual {v1, v2, v0}, Lcom/chase/sig/android/service/movemoney/d;->a(Lcom/chase/sig/android/domain/Transaction;Lcom/chase/sig/android/service/movemoney/RequestFlags;)Lcom/chase/sig/android/service/movemoney/ServiceResponse;

    move-result-object v0

    return-object v0

    :cond_0
    sget-object v0, Lcom/chase/sig/android/service/movemoney/RequestFlags;->b:Lcom/chase/sig/android/service/movemoney/RequestFlags;

    goto :goto_0
.end method

.method protected a(Lcom/chase/sig/android/service/movemoney/ServiceResponse;)V
    .locals 9
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 146
    invoke-virtual {p1}, Lcom/chase/sig/android/service/movemoney/ServiceResponse;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 147
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/h;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/movemoney/ServiceResponse;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/h;->c(Ljava/util/List;)V

    .line 172
    :goto_0
    return-void

    .line 148
    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/h$a;->a:Lcom/chase/sig/android/domain/Transaction;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Transaction;->l()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {p1}, Lcom/chase/sig/android/service/movemoney/ServiceResponse;->g()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/service/IServiceError;

    sget-object v5, Lcom/chase/sig/android/activity/h;->a:[Ljava/lang/String;

    array-length v6, v5

    move v3, v2

    :goto_1
    if-ge v3, v6, :cond_3

    aget-object v7, v5, v3

    invoke-interface {v0}, Lcom/chase/sig/android/service/IServiceError;->c()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    move v0, v1

    :goto_2
    if-eqz v0, :cond_1

    move v0, v1

    :goto_3
    if-eqz v0, :cond_5

    .line 149
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/h;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/h$a;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/h;->g(I)V

    goto :goto_0

    .line 148
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_2

    :cond_4
    move v0, v2

    goto :goto_3

    .line 152
    :cond_5
    invoke-virtual {p0, p1}, Lcom/chase/sig/android/activity/h$a;->b(Lcom/chase/sig/android/service/movemoney/ServiceResponse;)V

    .line 154
    iget-object v0, p0, Lcom/chase/sig/android/activity/h$a;->a:Lcom/chase/sig/android/domain/Transaction;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/movemoney/ServiceResponse;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/Transaction;->m(Ljava/lang/String;)V

    .line 155
    new-instance v1, Landroid/content/Intent;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/h;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/h;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-class v2, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 158
    const/high16 v0, 0x400

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 159
    invoke-static {v1}, Lcom/chase/sig/android/c;->a(Landroid/content/Intent;)V

    .line 161
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/h;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/h;->i()Ljava/lang/Class;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 163
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/h;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/h;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    iget-object v2, p0, Lcom/chase/sig/android/activity/h$a;->a:Lcom/chase/sig/android/domain/Transaction;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 165
    :cond_6
    const-string v0, "transaction_object"

    iget-object v2, p0, Lcom/chase/sig/android/activity/h$a;->a:Lcom/chase/sig/android/domain/Transaction;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 166
    const-string v2, "request_flags"

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/h;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/h;->c()Z

    move-result v0

    if-eqz v0, :cond_7

    sget-object v0, Lcom/chase/sig/android/service/movemoney/RequestFlags;->d:Lcom/chase/sig/android/service/movemoney/RequestFlags;

    :goto_4
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 168
    const-string v2, "queued_errors"

    invoke-virtual {p1}, Lcom/chase/sig/android/service/movemoney/ServiceResponse;->g()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/io/Serializable;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 170
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/h;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/h;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 166
    :cond_7
    sget-object v0, Lcom/chase/sig/android/service/movemoney/RequestFlags;->c:Lcom/chase/sig/android/service/movemoney/RequestFlags;

    goto :goto_4
.end method

.method protected bridge synthetic a(Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 125
    check-cast p1, Lcom/chase/sig/android/service/movemoney/ServiceResponse;

    invoke-virtual {p0, p1}, Lcom/chase/sig/android/activity/h$a;->a(Lcom/chase/sig/android/service/movemoney/ServiceResponse;)V

    return-void
.end method

.method protected abstract b()I
.end method

.method protected b(Lcom/chase/sig/android/service/movemoney/ServiceResponse;)V
    .locals 0
    .parameter

    .prologue
    .line 195
    return-void
.end method
