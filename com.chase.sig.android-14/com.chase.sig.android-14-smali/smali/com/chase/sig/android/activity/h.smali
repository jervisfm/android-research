.class public abstract Lcom/chase/sig/android/activity/h;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$e;
.implements Lcom/chase/sig/android/activity/ma;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/h$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/chase/sig/android/domain/Transaction;",
        ">",
        "Lcom/chase/sig/android/activity/ai;",
        "Lcom/chase/sig/android/activity/fk$e;",
        "Lcom/chase/sig/android/activity/ma;"
    }
.end annotation


# static fields
.field protected static final a:[Ljava/lang/String;


# instance fields
.field private b:Lcom/chase/sig/android/view/b$a;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 40
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "70011"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "70033"

    aput-object v2, v0, v1

    sput-object v0, Lcom/chase/sig/android/activity/h;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    .line 97
    new-instance v0, Lcom/chase/sig/android/activity/j;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/activity/j;-><init>(Lcom/chase/sig/android/activity/h;)V

    iput-object v0, p0, Lcom/chase/sig/android/activity/h;->b:Lcom/chase/sig/android/view/b$a;

    .line 125
    return-void
.end method


# virtual methods
.method protected final a()Lcom/chase/sig/android/view/detail/DetailView;
    .locals 1

    .prologue
    .line 62
    const v0, 0x7f0902c9

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/h;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailView;

    return-object v0
.end method

.method protected final a(Lcom/chase/sig/android/domain/Transaction;)Lcom/chase/sig/android/view/detail/o;
    .locals 4
    .parameter

    .prologue
    const v1, 0x7f07007d

    .line 217
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/Transaction;->C()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 218
    new-instance v0, Lcom/chase/sig/android/view/detail/o;

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/h;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/Transaction;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/Transaction;->C()Ljava/util/ArrayList;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/chase/sig/android/view/detail/o;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 222
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/chase/sig/android/view/detail/o;

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/h;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/Transaction;->i()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/chase/sig/android/view/detail/o;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 46
    const v0, 0x7f0300c4

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/h;->b(I)V

    .line 48
    const v0, 0x7f0902cb

    new-instance v1, Lcom/chase/sig/android/activity/i;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/i;-><init>(Lcom/chase/sig/android/activity/h;)V

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/h;->a(ILandroid/view/View$OnClickListener;)V

    .line 58
    const v0, 0x7f0902ca

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/h;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/chase/sig/android/activity/a/a;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/a/a;-><init>(Lcom/chase/sig/android/activity/ma;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 59
    return-void
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 66
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/h;->d()Lcom/chase/sig/android/domain/Transaction;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/Transaction;->l()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/h;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "edit_single"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public final c()Z
    .locals 3

    .prologue
    .line 71
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/h;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "edit_single"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method protected final d()Lcom/chase/sig/android/domain/Transaction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 76
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/h;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "transaction_object"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/Transaction;

    return-object v0
.end method

.method protected final e()V
    .locals 4

    .prologue
    .line 81
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/h;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 82
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/h;->h()Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/chase/sig/android/domain/Transaction;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/h;->f()Lcom/chase/sig/android/domain/Transaction;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/h;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 84
    :cond_0
    return-void
.end method

.method protected abstract f()Lcom/chase/sig/android/domain/Transaction;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method protected abstract g()Z
.end method

.method protected abstract h()Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/chase/sig/android/activity/h$a",
            "<TT;>;>;"
        }
    .end annotation
.end method

.method protected abstract i()Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/chase/sig/android/activity/eb;",
            ">;"
        }
    .end annotation
.end method

.method protected abstract j()Ljava/util/Date;
.end method

.method public final k()V
    .locals 1

    .prologue
    .line 200
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/h;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/DetailView;->a()V

    .line 201
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 3
    .parameter

    .prologue
    .line 87
    if-nez p1, :cond_0

    .line 88
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 89
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/h;->j()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 90
    iget-object v1, p0, Lcom/chase/sig/android/activity/h;->b:Lcom/chase/sig/android/view/b$a;

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/h;->a(Ljava/util/Calendar;Lcom/chase/sig/android/view/b$a;)Lcom/chase/sig/android/view/b;

    move-result-object v0

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/h;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v1

    const-string v2, "DATE"

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/view/detail/DetailView;->e(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/view/detail/a;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->t(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/b;->a(Ljava/util/Date;)Lcom/chase/sig/android/view/b;

    move-result-object v0

    .line 94
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ai;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 108
    if-nez p1, :cond_0

    .line 109
    check-cast p2, Lcom/chase/sig/android/view/b;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/h;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v0

    const-string v1, "DATE"

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/detail/DetailView;->e(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/a;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->t(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/chase/sig/android/view/b;->a(Ljava/util/Date;)Lcom/chase/sig/android/view/b;

    .line 112
    :cond_0
    return-void
.end method

.method protected onRestart()V
    .locals 0

    .prologue
    .line 205
    invoke-super {p0}, Lcom/chase/sig/android/activity/ai;->onRestart()V

    .line 206
    invoke-static {}, Lcom/chase/sig/android/c;->a()V

    .line 207
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 211
    invoke-super {p0}, Lcom/chase/sig/android/activity/ai;->onResume()V

    .line 212
    invoke-static {}, Lcom/chase/sig/android/c;->a()V

    .line 213
    return-void
.end method
