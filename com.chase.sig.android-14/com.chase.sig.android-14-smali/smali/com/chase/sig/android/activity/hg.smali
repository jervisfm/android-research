.class final Lcom/chase/sig/android/activity/hg;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/chase/sig/android/activity/QuickPayActivityActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/QuickPayActivityActivity;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 303
    iput-object p1, p0, Lcom/chase/sig/android/activity/hg;->b:Lcom/chase/sig/android/activity/QuickPayActivityActivity;

    iput p2, p0, Lcom/chase/sig/android/activity/hg;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 307
    :try_start_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/hg;->b:Lcom/chase/sig/android/activity/QuickPayActivityActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->b(Lcom/chase/sig/android/activity/QuickPayActivityActivity;)Landroid/view/View;

    move-result-object v0

    if-ne p2, v0, :cond_0

    .line 308
    iget-object v0, p0, Lcom/chase/sig/android/activity/hg;->b:Lcom/chase/sig/android/activity/QuickPayActivityActivity;

    const-class v1, Lcom/chase/sig/android/activity/QuickPayActivityActivity$a;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/chase/sig/android/activity/hg;->b:Lcom/chase/sig/android/activity/QuickPayActivityActivity;

    invoke-static {v4}, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->a(Lcom/chase/sig/android/activity/QuickPayActivityActivity;)Lcom/chase/sig/android/domain/QuickPayActivityType;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget v4, p0, Lcom/chase/sig/android/activity/hg;->a:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->a(Ljava/lang/Class;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 313
    :goto_0
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1, p2, p3}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/widget/AdapterView;Landroid/view/View;I)V

    return-void

    .line 310
    :cond_0
    :try_start_1
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 311
    iget-object v3, p0, Lcom/chase/sig/android/activity/hg;->b:Lcom/chase/sig/android/activity/QuickPayActivityActivity;

    const-string v1, "id"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v2, "type"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v4, "isinvoicerequest"

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v3, v1, v2, v0}, Lcom/chase/sig/android/activity/QuickPayActivityActivity;->a(Lcom/chase/sig/android/activity/QuickPayActivityActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 313
    :catch_0
    move-exception v0

    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1, p2, p3}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/widget/AdapterView;Landroid/view/View;I)V

    throw v0
.end method
