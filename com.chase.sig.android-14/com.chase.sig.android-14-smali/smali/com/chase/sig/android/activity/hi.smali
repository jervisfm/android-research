.class final Lcom/chase/sig/android/activity/hi;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Ljava/util/ArrayList;

.field final synthetic b:Lcom/chase/sig/android/activity/QuickPayChooseRecipientActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/QuickPayChooseRecipientActivity;Ljava/util/ArrayList;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 122
    iput-object p1, p0, Lcom/chase/sig/android/activity/hi;->b:Lcom/chase/sig/android/activity/QuickPayChooseRecipientActivity;

    iput-object p2, p0, Lcom/chase/sig/android/activity/hi;->a:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 126
    :try_start_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/hi;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/QuickPayRecipient;

    .line 127
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayRecipient;->m()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 128
    iget-object v1, p0, Lcom/chase/sig/android/activity/hi;->b:Lcom/chase/sig/android/activity/QuickPayChooseRecipientActivity;

    invoke-virtual {v1}, Lcom/chase/sig/android/activity/QuickPayChooseRecipientActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "quick_pay_manage_recipient"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 131
    iget-object v1, p0, Lcom/chase/sig/android/activity/hi;->b:Lcom/chase/sig/android/activity/QuickPayChooseRecipientActivity;

    invoke-static {v1, v0}, Lcom/chase/sig/android/activity/QuickPayChooseRecipientActivity;->a(Lcom/chase/sig/android/activity/QuickPayChooseRecipientActivity;Lcom/chase/sig/android/domain/QuickPayRecipient;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 135
    :cond_0
    :goto_0
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1, p2, p3}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/widget/AdapterView;Landroid/view/View;I)V

    return-void

    .line 132
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/chase/sig/android/activity/hi;->b:Lcom/chase/sig/android/activity/QuickPayChooseRecipientActivity;

    invoke-virtual {v1}, Lcom/chase/sig/android/activity/QuickPayChooseRecipientActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "quick_pay_transaction"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 133
    iget-object v1, p0, Lcom/chase/sig/android/activity/hi;->b:Lcom/chase/sig/android/activity/QuickPayChooseRecipientActivity;

    iget-object v2, p0, Lcom/chase/sig/android/activity/hi;->b:Lcom/chase/sig/android/activity/QuickPayChooseRecipientActivity;

    invoke-virtual {v2}, Lcom/chase/sig/android/activity/QuickPayChooseRecipientActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/chase/sig/android/activity/QuickPayChooseRecipientActivity;->a(Landroid/content/Intent;Lcom/chase/sig/android/domain/QuickPayRecipient;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 135
    :catch_0
    move-exception v0

    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1, p2, p3}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/widget/AdapterView;Landroid/view/View;I)V

    throw v0
.end method
