.class final Lcom/chase/sig/android/activity/hj;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 53
    iput-object p1, p0, Lcom/chase/sig/android/activity/hj;->a:Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 7
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 57
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/hj;->a:Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;->a(Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/hj;->a:Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;->b(Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 59
    :goto_0
    if-eqz v0, :cond_5

    .line 60
    iget-object v0, p0, Lcom/chase/sig/android/activity/hj;->a:Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;->c(Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;)Lcom/chase/sig/android/domain/QuickPayRecipient;

    move-result-object v0

    iget-object v2, p0, Lcom/chase/sig/android/activity/hj;->a:Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;

    invoke-static {v2}, Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;->b(Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/domain/QuickPayRecipient;->e(Ljava/lang/String;)V

    .line 61
    iget-object v0, p0, Lcom/chase/sig/android/activity/hj;->a:Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;->a(Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/chase/sig/android/activity/hj;->a:Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;->c(Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;)Lcom/chase/sig/android/domain/QuickPayRecipient;

    move-result-object v3

    invoke-virtual {v3}, Lcom/chase/sig/android/domain/QuickPayRecipient;->j()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/Email;

    invoke-static {v2}, Lcom/google/common/a/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Email;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    new-instance v4, Lcom/chase/sig/android/domain/Email;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Email;->b()Z

    move-result v5

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Email;->c()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v2, v5, v0}, Lcom/chase/sig/android/domain/Email;-><init>(Ljava/lang/String;ZLjava/lang/String;)V

    invoke-virtual {v3}, Lcom/chase/sig/android/domain/QuickPayRecipient;->h()V

    invoke-virtual {v3, v4}, Lcom/chase/sig/android/domain/QuickPayRecipient;->a(Lcom/chase/sig/android/domain/Email;)V

    .line 62
    :goto_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/hj;->a:Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;->d(Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-class v0, Lcom/chase/sig/android/activity/QuickPayRequestMoneyActivity;

    .line 64
    :goto_2
    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/chase/sig/android/activity/hj;->a:Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;

    invoke-direct {v2, v3, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 66
    iget-object v0, p0, Lcom/chase/sig/android/activity/hj;->a:Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/content/Intent;->putExtras(Landroid/content/Intent;)Landroid/content/Intent;

    .line 67
    const-string v0, "recipient"

    iget-object v3, p0, Lcom/chase/sig/android/activity/hj;->a:Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;

    invoke-static {v3}, Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;->c(Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;)Lcom/chase/sig/android/domain/QuickPayRecipient;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 68
    const-string v0, "is_editing"

    iget-object v3, p0, Lcom/chase/sig/android/activity/hj;->a:Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;

    invoke-virtual {v3}, Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "is_editing"

    invoke-virtual {v3, v4, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 69
    const/high16 v0, 0x400

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 70
    iget-object v0, p0, Lcom/chase/sig/android/activity/hj;->a:Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;->startActivity(Landroid/content/Intent;)V

    .line 71
    iget-object v0, p0, Lcom/chase/sig/android/activity/hj;->a:Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;->finish()V

    .line 75
    :goto_3
    return-void

    :cond_2
    move v0, v1

    .line 57
    goto/16 :goto_0

    .line 61
    :cond_3
    invoke-virtual {v3}, Lcom/chase/sig/android/domain/QuickPayRecipient;->h()V

    goto :goto_1

    .line 62
    :cond_4
    const-class v0, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;

    goto :goto_2

    .line 73
    :cond_5
    iget-object v0, p0, Lcom/chase/sig/android/activity/hj;->a:Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;

    const v1, 0x7f070249

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;->g(I)V

    goto :goto_3
.end method
