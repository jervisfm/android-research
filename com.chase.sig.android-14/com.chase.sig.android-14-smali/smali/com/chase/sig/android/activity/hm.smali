.class public abstract Lcom/chase/sig/android/activity/hm;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$e;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    .line 15
    return-void
.end method


# virtual methods
.method protected final a(Ljava/lang/Class;Ljava/lang/String;)V
    .locals 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/chase/sig/android/activity/eb;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 23
    const v0, 0x7f090209

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/hm;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 24
    const v1, 0x7f090208

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/hm;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 26
    const-class v2, Lcom/chase/sig/android/activity/QuickPayHomeActivity;

    invoke-virtual {p0, v2}, Lcom/chase/sig/android/activity/hm;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/a/f;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 28
    if-eqz p2, :cond_0

    .line 29
    invoke-virtual {v1, p2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 32
    :cond_0
    invoke-virtual {p0, p1}, Lcom/chase/sig/android/activity/hm;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/a/f;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 33
    return-void
.end method

.method protected final d()V
    .locals 2

    .prologue
    .line 18
    const-class v0, Lcom/chase/sig/android/activity/QuickPayActivityActivity;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/hm;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 19
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 37
    invoke-virtual {p0, p1}, Lcom/chase/sig/android/activity/hm;->j(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 38
    const/4 v0, 0x1

    .line 45
    :goto_0
    return v0

    .line 41
    :cond_0
    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    .line 42
    const-class v0, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;

    invoke-static {p1, p0, v0}, Lcom/chase/sig/android/a/a;->a(ILcom/chase/sig/android/activity/eb;Ljava/lang/Class;)V

    .line 45
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/chase/sig/android/activity/ai;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method
