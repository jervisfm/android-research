.class public Lcom/chase/sig/android/activity/hr$a;
.super Lcom/chase/sig/android/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/hr;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/d",
        "<",
        "Lcom/chase/sig/android/activity/hr;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/service/quickpay/QuickPayAddRecipientResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 320
    invoke-direct {p0}, Lcom/chase/sig/android/d;-><init>()V

    return-void
.end method

.method private static a(Lcom/chase/sig/android/service/IServiceError;)Z
    .locals 2
    .parameter

    .prologue
    .line 356
    invoke-interface {p0}, Lcom/chase/sig/android/service/IServiceError;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/chase/sig/android/activity/hr;->d:Ljava/util/HashMap;

    invoke-interface {p0}, Lcom/chase/sig/android/service/IServiceError;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 320
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/hr;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/hr;->A()Lcom/chase/sig/android/service/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/service/n;->k()Lcom/chase/sig/android/service/x;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/hr;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/hr;->g()Lcom/chase/sig/android/domain/QuickPayRecipient;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/service/x;->a(Lcom/chase/sig/android/domain/QuickPayRecipient;)Lcom/chase/sig/android/service/quickpay/QuickPayAddRecipientResponse;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 8
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 320
    check-cast p1, Lcom/chase/sig/android/service/quickpay/QuickPayAddRecipientResponse;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPayAddRecipientResponse;->e()Z

    move-result v0

    if-eqz v0, :cond_5

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPayAddRecipientResponse;->g()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/service/IServiceError;

    invoke-interface {v0}, Lcom/chase/sig/android/service/IServiceError;->a()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0}, Lcom/chase/sig/android/activity/hr$a;->a(Lcom/chase/sig/android/service/IServiceError;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "mobile"

    invoke-interface {v0}, Lcom/chase/sig/android/service/IServiceError;->c()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    const-string v6, "nickname"

    invoke-interface {v0}, Lcom/chase/sig/android/service/IServiceError;->c()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-nez v1, :cond_1

    if-eqz v6, :cond_3

    :cond_1
    move v1, v2

    :goto_1
    if-eqz v1, :cond_2

    invoke-interface {v3, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v1, Lcom/chase/sig/android/activity/hr;

    invoke-virtual {v1, v5}, Lcom/chase/sig/android/activity/hr;->f(Ljava/lang/String;)V

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    invoke-static {v0}, Lcom/chase/sig/android/activity/hr$a;->a(Lcom/chase/sig/android/service/IServiceError;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v1, Lcom/chase/sig/android/activity/hr;

    sget-object v5, Lcom/chase/sig/android/activity/hr;->d:Ljava/util/HashMap;

    invoke-interface {v0}, Lcom/chase/sig/android/service/IServiceError;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/activity/hr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    invoke-static {v0}, Lcom/chase/sig/android/activity/hr;->a(Landroid/widget/TextView;)Z

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    goto :goto_1

    :cond_4
    move v1, v2

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/hr;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPayAddRecipientResponse;->a()Lcom/chase/sig/android/domain/QuickPayRecipient;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/hr;->a(Lcom/chase/sig/android/domain/QuickPayRecipient;)V

    :cond_6
    return-void
.end method
