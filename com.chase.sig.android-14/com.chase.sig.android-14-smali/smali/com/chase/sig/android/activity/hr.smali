.class public abstract Lcom/chase/sig/android/activity/hr;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$e;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/hr$a;
    }
.end annotation


# static fields
.field protected static final d:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field protected a:[I

.field protected b:[I

.field protected c:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/chase/sig/android/activity/hr;->d:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 41
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    .line 31
    iput-object v0, p0, Lcom/chase/sig/android/activity/hr;->a:[I

    .line 33
    iput-object v0, p0, Lcom/chase/sig/android/activity/hr;->b:[I

    .line 35
    iput-object v0, p0, Lcom/chase/sig/android/activity/hr;->c:[I

    .line 42
    return-void
.end method

.method private static a(Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;
    .locals 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/Email;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 227
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/Email;

    .line 228
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Email;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 229
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Email;->c()Ljava/lang/String;

    move-result-object v0

    .line 233
    :goto_0
    return-object v0

    :cond_1
    const-string v0, "0"

    goto :goto_0
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/hr;)Z
    .locals 8
    .parameter

    .prologue
    const v4, 0x7f0901e2

    const/4 v2, 0x1

    const v5, 0x7f0901e7

    const/4 v3, 0x0

    .line 28
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/hr;->y()V

    invoke-direct {p0, v4}, Lcom/chase/sig/android/activity/hr;->k(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v4}, Lcom/chase/sig/android/activity/hr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->o(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "[0-9 a-zA-Z&\\-\\(\\),\'/.]*"

    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->matches(Ljava/lang/String;Ljava/lang/CharSequence;)Z

    move-result v0

    :goto_0
    if-nez v0, :cond_6

    const v0, 0x7f0901e1

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/hr;->e(I)Z

    move-result v0

    :goto_1
    invoke-direct {p0, v5}, Lcom/chase/sig/android/activity/hr;->k(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->o(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, v5}, Lcom/chase/sig/android/activity/hr;->k(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/domain/MobilePhoneNumber;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v4, 0xa

    if-eq v1, v4, :cond_0

    const v0, 0x7f07024c

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/hr;->g(I)V

    const v0, 0x7f0901e6

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/hr;->e(I)Z

    move-result v0

    :cond_0
    invoke-direct {p0, v5}, Lcom/chase/sig/android/activity/hr;->k(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->n(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const v1, 0x7f0901e5

    invoke-direct {p0, v1}, Lcom/chase/sig/android/activity/hr;->k(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->n(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const v0, 0x7f07024b

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/hr;->g(I)V

    const v0, 0x7f0901e4

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/hr;->e(I)Z

    move-result v0

    :cond_1
    move v5, v3

    move v4, v0

    :goto_2
    iget-object v0, p0, Lcom/chase/sig/android/activity/hr;->b:[I

    array-length v0, v0

    if-ge v5, v0, :cond_4

    iget-object v0, p0, Lcom/chase/sig/android/activity/hr;->b:[I

    aget v0, v0, v5

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/hr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/widget/TextView;

    sget-object v0, Lcom/chase/sig/android/activity/hr;->d:Ljava/util/HashMap;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "email"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v7, v5, 0x1

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/hr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getVisibility()I

    move-result v6

    if-nez v6, :cond_5

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->o(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    const-string v6, "^[\\w-]+((|[\\.][\\w-]+)+)@([\\w-]+[\\.])+[a-zA-Z]{2,6}$"

    invoke-static {v6, v1}, Ljava/util/regex/Pattern;->matches(Ljava/lang/String;Ljava/lang/CharSequence;)Z

    move-result v1

    :goto_3
    if-nez v1, :cond_5

    invoke-static {v0}, Lcom/chase/sig/android/activity/hr;->a(Landroid/widget/TextView;)Z

    move v0, v3

    :goto_4
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    move v4, v0

    goto :goto_2

    :cond_2
    move v0, v3

    goto/16 :goto_0

    :cond_3
    move v1, v2

    goto :goto_3

    :cond_4
    return v4

    :cond_5
    move v0, v4

    goto :goto_4

    :cond_6
    move v0, v2

    goto/16 :goto_1
.end method

.method private k(I)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 241
    invoke-virtual {p0, p1}, Lcom/chase/sig/android/activity/hr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 243
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)V
    .locals 5
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 50
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/hr;->d()V

    .line 51
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/hr;->e()V

    move v1, v2

    .line 52
    :goto_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/hr;->b:[I

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    if-ge v1, v0, :cond_0

    .line 53
    iget-object v0, p0, Lcom/chase/sig/android/activity/hr;->b:[I

    aget v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/hr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 54
    iget-object v3, p0, Lcom/chase/sig/android/activity/hr;->a:[I

    add-int/lit8 v4, v1, 0x1

    aget v3, v3, v4

    new-instance v4, Lcom/chase/sig/android/activity/hu;

    invoke-direct {v4, p0, v3}, Lcom/chase/sig/android/activity/hu;-><init>(Lcom/chase/sig/android/activity/hr;I)V

    invoke-virtual {v0, v4}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 52
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 57
    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/hr;->c:[I

    if-eqz v0, :cond_1

    .line 58
    :goto_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/hr;->c:[I

    array-length v0, v0

    if-ge v2, v0, :cond_1

    .line 59
    iget-object v0, p0, Lcom/chase/sig/android/activity/hr;->c:[I

    aget v0, v0, v2

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/hr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 60
    new-instance v1, Lcom/chase/sig/android/activity/ht;

    invoke-direct {v1, p0, v2}, Lcom/chase/sig/android/activity/ht;-><init>(Lcom/chase/sig/android/activity/hr;I)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 58
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 64
    :cond_1
    const v0, 0x7f0901f9

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/hr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 65
    const v1, 0x7f0901f8

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/hr;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 67
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/hr;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "quick_pay_manage_recipient"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 68
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 69
    const v2, 0x7f070205

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(I)V

    .line 72
    :cond_2
    new-instance v2, Lcom/chase/sig/android/activity/hs;

    invoke-direct {v2, p0}, Lcom/chase/sig/android/activity/hs;-><init>(Lcom/chase/sig/android/activity/hr;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 91
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/hr;->C()Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 92
    return-void
.end method

.method public final a(Lcom/chase/sig/android/domain/QuickPayRecipient;)V
    .locals 5
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 371
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/hr;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "quick_pay_transaction"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 372
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/hr;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/chase/sig/android/activity/hr;->a(Landroid/content/Intent;Lcom/chase/sig/android/domain/QuickPayRecipient;)V

    .line 381
    :cond_0
    :goto_0
    return-void

    .line 373
    :cond_1
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/hr;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "quick_pay_manage_recipient"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/hr;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "fromContactsActivity"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 375
    :cond_2
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/chase/sig/android/activity/QuickPayChooseRecipientActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 376
    const-string v1, "quick_pay_manage_recipient"

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/hr;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "quick_pay_manage_recipient"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 378
    const/high16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 379
    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/hr;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method protected abstract d()V
.end method

.method protected abstract e()V
.end method

.method protected final g()Lcom/chase/sig/android/domain/QuickPayRecipient;
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 187
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/hr;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "recipient"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/QuickPayRecipient;

    .line 189
    if-nez v0, :cond_0

    .line 190
    new-instance v0, Lcom/chase/sig/android/domain/QuickPayRecipient;

    invoke-direct {v0}, Lcom/chase/sig/android/domain/QuickPayRecipient;-><init>()V

    .line 193
    :cond_0
    const v1, 0x7f0901e2

    invoke-direct {p0, v1}, Lcom/chase/sig/android/activity/hr;->k(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/QuickPayRecipient;->a(Ljava/lang/String;)V

    .line 194
    const v1, 0x7f0901e7

    invoke-direct {p0, v1}, Lcom/chase/sig/android/activity/hr;->k(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/domain/MobilePhoneNumber;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayRecipient;->i()Lcom/chase/sig/android/domain/MobilePhoneNumber;

    move-result-object v1

    if-nez v1, :cond_2

    const-string v1, "0"

    :goto_0
    new-instance v4, Lcom/chase/sig/android/domain/MobilePhoneNumber;

    const-string v5, ""

    invoke-direct {v4, v3, v1, v5}, Lcom/chase/sig/android/domain/MobilePhoneNumber;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Lcom/chase/sig/android/domain/QuickPayRecipient;->a(Lcom/chase/sig/android/domain/MobilePhoneNumber;)V

    .line 195
    new-instance v4, Ljava/util/ArrayList;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayRecipient;->j()Ljava/util/List;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayRecipient;->h()V

    iget-object v5, p0, Lcom/chase/sig/android/activity/hr;->b:[I

    array-length v6, v5

    move v3, v2

    :goto_1
    if-ge v3, v6, :cond_4

    aget v1, v5, v3

    invoke-direct {p0, v1}, Lcom/chase/sig/android/activity/hr;->k(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/chase/sig/android/util/s;->o(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v8, Lcom/chase/sig/android/domain/Email;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayRecipient;->e()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_3

    const/4 v1, 0x1

    :goto_2
    invoke-static {v7, v4}, Lcom/chase/sig/android/activity/hr;->a(Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v7, v1, v9}, Lcom/chase/sig/android/domain/Email;-><init>(Ljava/lang/String;ZLjava/lang/String;)V

    invoke-virtual {v0, v8}, Lcom/chase/sig/android/domain/QuickPayRecipient;->a(Lcom/chase/sig/android/domain/Email;)V

    :cond_1
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1

    .line 194
    :cond_2
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayRecipient;->i()Lcom/chase/sig/android/domain/MobilePhoneNumber;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/MobilePhoneNumber;->c()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_3
    move v1, v2

    .line 195
    goto :goto_2

    .line 197
    :cond_4
    return-object v0
.end method

.method protected onResume()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 98
    invoke-super {p0}, Lcom/chase/sig/android/activity/ai;->onResume()V

    move v1, v2

    .line 99
    :goto_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/hr;->b:[I

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lcom/chase/sig/android/activity/hr;->b:[I

    aget v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/hr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->o(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/hr;->a:[I

    add-int/lit8 v3, v1, 0x1

    aget v0, v0, v3

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/hr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 100
    :cond_1
    return-void
.end method
