.class final Lcom/chase/sig/android/activity/ht;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/chase/sig/android/activity/hr;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/hr;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 247
    iput-object p1, p0, Lcom/chase/sig/android/activity/ht;->b:Lcom/chase/sig/android/activity/hr;

    iput p2, p0, Lcom/chase/sig/android/activity/ht;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(I)V
    .locals 2
    .parameter

    .prologue
    .line 281
    iget-object v0, p0, Lcom/chase/sig/android/activity/ht;->b:Lcom/chase/sig/android/activity/hr;

    iget-object v1, p0, Lcom/chase/sig/android/activity/ht;->b:Lcom/chase/sig/android/activity/hr;

    iget-object v1, v1, Lcom/chase/sig/android/activity/hr;->a:[I

    aget v1, v1, p1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/hr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 282
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 283
    iget-object v0, p0, Lcom/chase/sig/android/activity/ht;->b:Lcom/chase/sig/android/activity/hr;

    iget-object v1, p0, Lcom/chase/sig/android/activity/ht;->b:Lcom/chase/sig/android/activity/hr;

    iget-object v1, v1, Lcom/chase/sig/android/activity/hr;->b:[I

    aget v1, v1, p1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/hr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 284
    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 285
    return-void
.end method

.method private b(I)Z
    .locals 1
    .parameter

    .prologue
    .line 300
    iget-object v0, p0, Lcom/chase/sig/android/activity/ht;->b:Lcom/chase/sig/android/activity/hr;

    iget-object v0, v0, Lcom/chase/sig/android/activity/hr;->a:[I

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 6
    .parameter

    .prologue
    const/16 v5, 0x8

    .line 252
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    iget v0, p0, Lcom/chase/sig/android/activity/ht;->a:I

    invoke-direct {p0, v0}, Lcom/chase/sig/android/activity/ht;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/chase/sig/android/activity/ht;->a:I

    iget-object v1, p0, Lcom/chase/sig/android/activity/ht;->b:Lcom/chase/sig/android/activity/hr;

    iget-object v2, p0, Lcom/chase/sig/android/activity/ht;->b:Lcom/chase/sig/android/activity/hr;

    iget-object v2, v2, Lcom/chase/sig/android/activity/hr;->a:[I

    add-int/lit8 v0, v0, 0x1

    aget v0, v2, v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/activity/hr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-ne v0, v5, :cond_2

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_3

    .line 253
    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/ht;->b:Lcom/chase/sig/android/activity/hr;

    iget-object v1, p0, Lcom/chase/sig/android/activity/ht;->b:Lcom/chase/sig/android/activity/hr;

    iget-object v1, v1, Lcom/chase/sig/android/activity/hr;->b:[I

    iget v2, p0, Lcom/chase/sig/android/activity/ht;->a:I

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/hr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 254
    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 259
    :cond_1
    :goto_1
    return-void

    .line 252
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 256
    :cond_3
    iget v0, p0, Lcom/chase/sig/android/activity/ht;->a:I

    move v2, v0

    :goto_2
    iget-object v0, p0, Lcom/chase/sig/android/activity/ht;->b:Lcom/chase/sig/android/activity/hr;

    iget-object v0, v0, Lcom/chase/sig/android/activity/hr;->b:[I

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    if-ge v2, v0, :cond_4

    iget-object v0, p0, Lcom/chase/sig/android/activity/ht;->b:Lcom/chase/sig/android/activity/hr;

    iget-object v1, p0, Lcom/chase/sig/android/activity/ht;->b:Lcom/chase/sig/android/activity/hr;

    iget-object v1, v1, Lcom/chase/sig/android/activity/hr;->b:[I

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/hr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iget-object v1, p0, Lcom/chase/sig/android/activity/ht;->b:Lcom/chase/sig/android/activity/hr;

    iget-object v3, p0, Lcom/chase/sig/android/activity/ht;->b:Lcom/chase/sig/android/activity/hr;

    iget-object v3, v3, Lcom/chase/sig/android/activity/hr;->b:[I

    add-int/lit8 v4, v2, 0x1

    aget v3, v3, v4

    invoke-virtual {v1, v3}, Lcom/chase/sig/android/activity/hr;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 257
    :cond_4
    iget v0, p0, Lcom/chase/sig/android/activity/ht;->a:I

    move v1, v0

    :goto_3
    iget-object v0, p0, Lcom/chase/sig/android/activity/ht;->b:Lcom/chase/sig/android/activity/hr;

    iget-object v0, v0, Lcom/chase/sig/android/activity/hr;->a:[I

    array-length v0, v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lcom/chase/sig/android/activity/ht;->b:Lcom/chase/sig/android/activity/hr;

    iget-object v2, p0, Lcom/chase/sig/android/activity/ht;->b:Lcom/chase/sig/android/activity/hr;

    iget-object v2, v2, Lcom/chase/sig/android/activity/hr;->a:[I

    aget v2, v2, v1

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/activity/hr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-ne v5, v0, :cond_5

    add-int/lit8 v0, v1, -0x1

    invoke-direct {p0, v0}, Lcom/chase/sig/android/activity/ht;->a(I)V

    goto :goto_1

    :cond_5
    invoke-direct {p0, v1}, Lcom/chase/sig/android/activity/ht;->b(I)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/chase/sig/android/activity/ht;->b:Lcom/chase/sig/android/activity/hr;

    iget-object v2, p0, Lcom/chase/sig/android/activity/ht;->b:Lcom/chase/sig/android/activity/hr;

    iget-object v2, v2, Lcom/chase/sig/android/activity/hr;->b:[I

    aget v2, v2, v1

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/activity/hr;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/chase/sig/android/util/s;->n(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-direct {p0, v1}, Lcom/chase/sig/android/activity/ht;->a(I)V

    :cond_6
    :goto_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_7
    const-string v2, ""

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_4
.end method
