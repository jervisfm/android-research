.class final Lcom/chase/sig/android/activity/ic;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Z

.field final synthetic b:Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity;Z)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 54
    iput-object p1, p0, Lcom/chase/sig/android/activity/ic;->b:Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity;

    iput-boolean p2, p0, Lcom/chase/sig/android/activity/ic;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 4
    .parameter

    .prologue
    const/4 v3, 0x1

    .line 58
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    iget-boolean v0, p0, Lcom/chase/sig/android/activity/ic;->a:Z

    if-eqz v0, :cond_0

    .line 59
    iget-object v0, p0, Lcom/chase/sig/android/activity/ic;->b:Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity;->showDialog(I)V

    .line 69
    :goto_0
    return-void

    .line 61
    :cond_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/chase/sig/android/activity/ic;->b:Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity;

    const-class v2, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 64
    const-string v1, "qp_edit_one_payment"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 65
    const-string v1, "is_editing"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 66
    const-string v1, "quick_pay_transaction"

    iget-object v2, p0, Lcom/chase/sig/android/activity/ic;->b:Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity;

    invoke-static {v2}, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity;->a(Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity;)Lcom/chase/sig/android/domain/QuickPayPendingTransaction;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 67
    iget-object v1, p0, Lcom/chase/sig/android/activity/ic;->b:Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
