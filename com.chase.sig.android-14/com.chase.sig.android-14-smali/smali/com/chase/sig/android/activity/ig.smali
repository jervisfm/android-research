.class final Lcom/chase/sig/android/activity/ig;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 257
    iput-object p1, p0, Lcom/chase/sig/android/activity/ig;->a:Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 260
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/chase/sig/android/activity/ig;->a:Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity;

    const-class v2, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 263
    const-string v1, "quick_pay_transaction"

    iget-object v2, p0, Lcom/chase/sig/android/activity/ig;->a:Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity;

    invoke-static {v2}, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity;->a(Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity;)Lcom/chase/sig/android/domain/QuickPayPendingTransaction;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 264
    const-string v1, "is_editing"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 265
    iget-object v1, p0, Lcom/chase/sig/android/activity/ig;->a:Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/activity/QuickPayPendingTransactionsDetailActivity;->startActivity(Landroid/content/Intent;)V

    .line 266
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 267
    return-void
.end method
