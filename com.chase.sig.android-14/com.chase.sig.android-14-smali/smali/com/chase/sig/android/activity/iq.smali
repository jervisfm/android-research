.class final Lcom/chase/sig/android/activity/iq;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 56
    iput-object p1, p0, Lcom/chase/sig/android/activity/iq;->a:Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 6
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 59
    if-eqz p2, :cond_1

    .line 60
    iget-object v0, p0, Lcom/chase/sig/android/activity/iq;->a:Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;->a(Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;)Lcom/chase/sig/android/domain/QuickPayRecipient;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayRecipient;->d()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/chase/sig/android/activity/iq;->a:Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;->a(Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;)Lcom/chase/sig/android/domain/QuickPayRecipient;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayRecipient;->j()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/Email;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Email;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Landroid/widget/CompoundButton;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    invoke-virtual {v0, v4}, Lcom/chase/sig/android/domain/Email;->a(Z)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/iq;->a:Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;->a(Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;)Lcom/chase/sig/android/domain/QuickPayRecipient;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayRecipient;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    move v1, v0

    .line 61
    :goto_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/iq;->a:Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;

    iget-object v0, v0, Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;->e:Lcom/chase/sig/android/activity/mc;

    const-class v3, Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity$a;

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/activity/mc;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/ae;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity$a;

    .line 63
    invoke-virtual {v0}, Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity$a;->d()Z

    move-result v3

    if-nez v3, :cond_1

    if-eqz v1, :cond_1

    .line 64
    new-array v1, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 67
    :cond_1
    return-void

    :cond_2
    move v1, v2

    .line 60
    goto :goto_1
.end method
