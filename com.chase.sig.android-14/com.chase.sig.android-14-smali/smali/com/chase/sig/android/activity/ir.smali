.class final Lcom/chase/sig/android/activity/ir;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 83
    iput-object p1, p0, Lcom/chase/sig/android/activity/ir;->a:Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3
    .parameter

    .prologue
    .line 87
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/chase/sig/android/activity/ir;->a:Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;

    const-class v2, Lcom/chase/sig/android/activity/QuickPayEditRecipientActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 89
    const-string v1, "quick_pay_manage_recipient"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 90
    const-string v1, "recipient"

    iget-object v2, p0, Lcom/chase/sig/android/activity/ir;->a:Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;

    invoke-static {v2}, Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;->a(Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;)Lcom/chase/sig/android/domain/QuickPayRecipient;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 91
    iget-object v1, p0, Lcom/chase/sig/android/activity/ir;->a:Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;->startActivity(Landroid/content/Intent;)V

    .line 92
    return-void
.end method
