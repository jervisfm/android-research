.class final Lcom/chase/sig/android/activity/is;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Z

.field final synthetic b:Lcom/chase/sig/android/domain/QuickPayRecipient;

.field final synthetic c:Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;ZLcom/chase/sig/android/domain/QuickPayRecipient;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 110
    iput-object p1, p0, Lcom/chase/sig/android/activity/is;->c:Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;

    iput-boolean p2, p0, Lcom/chase/sig/android/activity/is;->a:Z

    iput-object p3, p0, Lcom/chase/sig/android/activity/is;->b:Lcom/chase/sig/android/domain/QuickPayRecipient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3
    .parameter

    .prologue
    .line 114
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    iget-boolean v0, p0, Lcom/chase/sig/android/activity/is;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/is;->c:Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->c:Lcom/chase/sig/android/service/quickpay/TodoListResponse;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->o()Z

    move-result v0

    if-nez v0, :cond_0

    .line 115
    iget-object v0, p0, Lcom/chase/sig/android/activity/is;->c:Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;

    const/16 v1, -0xb

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;->showDialog(I)V

    .line 127
    :goto_0
    return-void

    .line 119
    :cond_0
    iget-boolean v0, p0, Lcom/chase/sig/android/activity/is;->a:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/chase/sig/android/activity/is;->c:Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->c:Lcom/chase/sig/android/service/quickpay/TodoListResponse;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->q()Z

    move-result v0

    if-nez v0, :cond_1

    .line 120
    iget-object v0, p0, Lcom/chase/sig/android/activity/is;->c:Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;

    const/4 v1, -0x8

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;->showDialog(I)V

    goto :goto_0

    .line 123
    :cond_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/is;->c:Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 124
    const-string v1, "isRequestForMoney"

    iget-boolean v2, p0, Lcom/chase/sig/android/activity/is;->a:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 125
    iget-object v1, p0, Lcom/chase/sig/android/activity/is;->c:Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;

    iget-object v2, p0, Lcom/chase/sig/android/activity/is;->b:Lcom/chase/sig/android/domain/QuickPayRecipient;

    invoke-virtual {v1, v0, v2}, Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;->a(Landroid/content/Intent;Lcom/chase/sig/android/domain/QuickPayRecipient;)V

    .line 126
    iget-object v0, p0, Lcom/chase/sig/android/activity/is;->c:Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/QuickPayRecipientDetailsActivity;->finish()V

    goto :goto_0
.end method
