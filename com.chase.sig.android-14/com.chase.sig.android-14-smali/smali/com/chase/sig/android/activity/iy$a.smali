.class public Lcom/chase/sig/android/activity/iy$a;
.super Lcom/chase/sig/android/activity/iy$b;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/iy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 496
    invoke-direct {p0}, Lcom/chase/sig/android/activity/iy$b;-><init>()V

    return-void
.end method


# virtual methods
.method protected final a(Lcom/chase/sig/android/service/quickpay/QuickPayRecipientListResponse;)Lcom/chase/sig/android/domain/QuickPayRecipient;
    .locals 6
    .parameter

    .prologue
    .line 499
    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPayRecipientListResponse;->a()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/QuickPayRecipient;

    .line 500
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayRecipient;->j()Ljava/util/List;

    move-result-object v1

    iget-object v3, p0, Lcom/chase/sig/android/activity/iy$a;->a:Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/domain/Email;

    invoke-static {v3}, Lcom/google/common/a/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/Email;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_0

    .line 504
    :goto_1
    return-object v0

    .line 500
    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    .line 504
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 496
    check-cast p1, Lcom/chase/sig/android/service/quickpay/QuickPayRecipientListResponse;

    invoke-virtual {p0, p1}, Lcom/chase/sig/android/activity/iy$a;->b(Lcom/chase/sig/android/service/quickpay/QuickPayRecipientListResponse;)V

    return-void
.end method

.method protected final b(Lcom/chase/sig/android/service/quickpay/QuickPayRecipientListResponse;)V
    .locals 2
    .parameter

    .prologue
    .line 519
    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPayRecipientListResponse;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 520
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/iy;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPayRecipientListResponse;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/iy;->b(Ljava/util/List;)V

    .line 530
    :cond_0
    :goto_0
    return-void

    .line 522
    :cond_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/iy;

    invoke-virtual {p0, p1}, Lcom/chase/sig/android/activity/iy$a;->a(Lcom/chase/sig/android/service/quickpay/QuickPayRecipientListResponse;)Lcom/chase/sig/android/domain/QuickPayRecipient;

    move-result-object v1

    iput-object v1, v0, Lcom/chase/sig/android/activity/iy;->l:Lcom/chase/sig/android/domain/QuickPayRecipient;

    .line 523
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/iy;

    iget-object v0, v0, Lcom/chase/sig/android/activity/iy;->e:Lcom/chase/sig/android/activity/mc;

    const-class v1, Lcom/chase/sig/android/activity/iy$c;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/mc;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/ae;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/activity/iy$c;

    .line 526
    invoke-virtual {v0}, Lcom/chase/sig/android/activity/iy$c;->d()Z

    move-result v1

    if-nez v1, :cond_0

    .line 527
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/iy$c;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method
