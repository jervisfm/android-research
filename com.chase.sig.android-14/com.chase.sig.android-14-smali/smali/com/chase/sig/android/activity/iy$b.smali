.class public Lcom/chase/sig/android/activity/iy$b;
.super Lcom/chase/sig/android/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/iy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/d",
        "<",
        "Lcom/chase/sig/android/activity/iy;",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/service/quickpay/QuickPayRecipientListResponse;",
        ">;"
    }
.end annotation


# instance fields
.field protected a:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 454
    invoke-direct {p0}, Lcom/chase/sig/android/d;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Lcom/chase/sig/android/service/quickpay/QuickPayRecipientListResponse;)Lcom/chase/sig/android/domain/QuickPayRecipient;
    .locals 4
    .parameter

    .prologue
    .line 486
    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPayRecipientListResponse;->a()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/QuickPayRecipient;

    .line 487
    iget-object v2, p0, Lcom/chase/sig/android/activity/iy$b;->a:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/common/a/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayRecipient;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 491
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 454
    check-cast p1, [Ljava/lang/String;

    const/4 v0, 0x0

    aget-object v0, p1, v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/iy$b;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/iy;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/iy;->A()Lcom/chase/sig/android/service/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/service/n;->j()Lcom/chase/sig/android/service/y;

    invoke-static {}, Lcom/chase/sig/android/service/y;->a()Lcom/chase/sig/android/service/quickpay/QuickPayRecipientListResponse;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic a(Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 454
    check-cast p1, Lcom/chase/sig/android/service/quickpay/QuickPayRecipientListResponse;

    invoke-virtual {p0, p1}, Lcom/chase/sig/android/activity/iy$b;->b(Lcom/chase/sig/android/service/quickpay/QuickPayRecipientListResponse;)V

    return-void
.end method

.method protected b(Lcom/chase/sig/android/service/quickpay/QuickPayRecipientListResponse;)V
    .locals 3
    .parameter

    .prologue
    .line 468
    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPayRecipientListResponse;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 469
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/iy;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPayRecipientListResponse;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/iy;->b(Ljava/util/List;)V

    .line 482
    :goto_0
    return-void

    .line 471
    :cond_0
    new-instance v1, Landroid/content/Intent;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    const-class v2, Lcom/chase/sig/android/activity/QuickPayChooseRecipientNotificatonActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 473
    const-string v0, "recipient"

    invoke-virtual {p0, p1}, Lcom/chase/sig/android/activity/iy$b;->a(Lcom/chase/sig/android/service/quickpay/QuickPayRecipientListResponse;)Lcom/chase/sig/android/domain/QuickPayRecipient;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 474
    const-string v2, "saved_recipient"

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/iy;

    iget-object v0, v0, Lcom/chase/sig/android/activity/iy;->l:Lcom/chase/sig/android/domain/QuickPayRecipient;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 475
    const-string v2, "quick_pay_transaction"

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/iy;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/iy;->d()Lcom/chase/sig/android/domain/QuickPayTransaction;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 477
    const-string v0, "is_editing"

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 478
    const-string v2, "isRequestForMoney"

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/iy;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/iy;->e()Z

    move-result v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 479
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/iy;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/iy;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
