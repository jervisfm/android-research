.class public Lcom/chase/sig/android/activity/iy$d;
.super Lcom/chase/sig/android/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/iy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "d"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/d",
        "<",
        "Lcom/chase/sig/android/activity/iy;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;",
        ">;"
    }
.end annotation


# instance fields
.field protected a:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 323
    invoke-direct {p0}, Lcom/chase/sig/android/d;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 323
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/iy;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/iy;->A()Lcom/chase/sig/android/service/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/service/n;->j()Lcom/chase/sig/android/service/y;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/iy;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/iy;->d()Lcom/chase/sig/android/domain/QuickPayTransaction;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/service/y;->a(Lcom/chase/sig/android/domain/QuickPayTransaction;)Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;)V
    .locals 1
    .parameter

    .prologue
    .line 363
    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 364
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/iy;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/iy;->E()V

    .line 366
    :cond_0
    return-void
.end method

.method protected synthetic a(Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 323
    check-cast p1, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;

    invoke-virtual {p0, p1}, Lcom/chase/sig/android/activity/iy$d;->b(Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;)V

    return-void
.end method

.method protected b(Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;)V
    .locals 6
    .parameter

    .prologue
    .line 337
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/iy;

    invoke-static {v0, p1}, Lcom/chase/sig/android/activity/iy;->a(Lcom/chase/sig/android/activity/iy;Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;)Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;

    .line 338
    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 339
    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 340
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/iy;

    iget-object v1, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v1, Lcom/chase/sig/android/activity/iy;

    invoke-virtual {v1}, Lcom/chase/sig/android/activity/iy;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f07024f

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->o()Lcom/chase/sig/android/domain/QuickPayRecipient;

    move-result-object v5

    invoke-virtual {v5}, Lcom/chase/sig/android/domain/QuickPayRecipient;->a()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->o()Lcom/chase/sig/android/domain/QuickPayRecipient;

    move-result-object v5

    invoke-virtual {v5}, Lcom/chase/sig/android/domain/QuickPayRecipient;->d()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Lcom/chase/sig/android/util/s;->a(Landroid/content/Context;I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/iy;->f(Ljava/lang/String;)V

    .line 350
    :goto_0
    iget-boolean v0, p0, Lcom/chase/sig/android/activity/iy$d;->a:Z

    if-nez v0, :cond_2

    .line 360
    :goto_1
    return-void

    .line 344
    :cond_0
    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->r()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 345
    invoke-virtual {p0, p1}, Lcom/chase/sig/android/activity/iy$d;->a(Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;)V

    goto :goto_0

    .line 347
    :cond_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/iy;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/iy;->c(Ljava/util/List;)V

    goto :goto_0

    .line 355
    :cond_2
    invoke-virtual {p1}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->j()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 356
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/iy;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/iy;->showDialog(I)V

    goto :goto_1

    .line 359
    :cond_3
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/iy;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/iy;->D()V

    goto :goto_1
.end method
