.class public abstract Lcom/chase/sig/android/activity/iy;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$e;
.implements Lcom/chase/sig/android/activity/ma;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/iy$c;,
        Lcom/chase/sig/android/activity/iy$a;,
        Lcom/chase/sig/android/activity/iy$b;,
        Lcom/chase/sig/android/activity/iy$d;
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Landroid/widget/Button;

.field private c:Ljava/util/Date;

.field private d:Landroid/widget/TextView;

.field protected k:Lcom/chase/sig/android/domain/QuickPayTransaction;

.field protected l:Lcom/chase/sig/android/domain/QuickPayRecipient;

.field protected m:Lcom/chase/sig/android/domain/m;

.field private n:Ljava/util/Date;

.field private o:Ljava/lang/Object;

.field private p:Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;

.field private q:Lcom/chase/sig/android/view/b$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    sput-object v0, Lcom/chase/sig/android/activity/iy;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    .line 283
    new-instance v0, Lcom/chase/sig/android/activity/je;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/activity/je;-><init>(Lcom/chase/sig/android/activity/iy;)V

    iput-object v0, p0, Lcom/chase/sig/android/activity/iy;->q:Lcom/chase/sig/android/view/b$a;

    .line 533
    return-void
.end method

.method private G()Ljava/util/Date;
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, Lcom/chase/sig/android/activity/iy;->c:Ljava/util/Date;

    if-nez v0, :cond_0

    .line 215
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/ChaseApplication;

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->c:Lcom/chase/sig/android/service/quickpay/TodoListResponse;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->j()Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/iy;->c:Ljava/util/Date;

    .line 219
    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/iy;->c:Ljava/util/Date;

    return-object v0
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/iy;Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;)Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 38
    iput-object p1, p0, Lcom/chase/sig/android/activity/iy;->p:Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;

    return-object p1
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/iy;)V
    .locals 2
    .parameter

    .prologue
    .line 38
    const-class v0, Lcom/chase/sig/android/activity/iy$d;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/iy;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    return-void
.end method

.method protected static i()V
    .locals 0

    .prologue
    .line 293
    return-void
.end method


# virtual methods
.method public final D()V
    .locals 4

    .prologue
    .line 370
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/iy;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 372
    const/high16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 373
    invoke-static {v0}, Lcom/chase/sig/android/c;->a(Landroid/content/Intent;)V

    .line 375
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/chase/sig/android/activity/QuickPaySendMoneyVerifyActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 377
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/iy;->d()Lcom/chase/sig/android/domain/QuickPayTransaction;

    move-result-object v1

    .line 378
    iget-object v2, p0, Lcom/chase/sig/android/activity/iy;->p:Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;

    invoke-virtual {v2}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->o()Lcom/chase/sig/android/domain/QuickPayRecipient;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/domain/QuickPayTransaction;->a(Lcom/chase/sig/android/domain/QuickPayRecipient;)V

    .line 379
    iget-object v2, p0, Lcom/chase/sig/android/activity/iy;->p:Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;

    invoke-virtual {v2}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/domain/QuickPayTransaction;->q(Ljava/lang/String;)V

    .line 380
    iget-object v2, p0, Lcom/chase/sig/android/activity/iy;->p:Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;

    invoke-virtual {v2}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->l()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/domain/QuickPayTransaction;->a(Z)V

    .line 381
    iget-object v2, p0, Lcom/chase/sig/android/activity/iy;->p:Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;

    invoke-virtual {v2}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/domain/QuickPayTransaction;->t(Ljava/lang/String;)V

    .line 382
    iget-object v2, p0, Lcom/chase/sig/android/activity/iy;->p:Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;

    invoke-virtual {v2}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->s()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/domain/QuickPayTransaction;->g(Z)V

    .line 384
    const-string v2, "FOOTNOTE"

    iget-object v3, p0, Lcom/chase/sig/android/activity/iy;->p:Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;

    invoke-virtual {v3}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->n()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 385
    const-string v2, "sendTransaction"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 386
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/chase/sig/android/activity/iy;->p:Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;

    .line 387
    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/iy;->startActivity(Landroid/content/Intent;)V

    .line 388
    return-void
.end method

.method public final E()V
    .locals 6

    .prologue
    .line 397
    iget-object v0, p0, Lcom/chase/sig/android/activity/iy;->p:Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->g()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/service/IServiceError;

    .line 398
    iget-object v2, p0, Lcom/chase/sig/android/activity/iy;->p:Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;

    invoke-virtual {v2}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->o()Lcom/chase/sig/android/domain/QuickPayRecipient;

    move-result-object v2

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/QuickPayRecipient;->d()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/chase/sig/android/view/k$a;

    invoke-direct {v3, p0}, Lcom/chase/sig/android/view/k$a;-><init>(Landroid/content/Context;)V

    invoke-interface {v0}, Lcom/chase/sig/android/service/IServiceError;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/chase/sig/android/view/k$a;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/k$a;

    move-result-object v0

    const v4, 0x7f070068

    new-instance v5, Lcom/chase/sig/android/activity/jg;

    invoke-direct {v5, p0, v2}, Lcom/chase/sig/android/activity/jg;-><init>(Lcom/chase/sig/android/activity/iy;Ljava/lang/String;)V

    invoke-virtual {v0, v4, v5}, Lcom/chase/sig/android/view/k$a;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    move-result-object v0

    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/chase/sig/android/view/k$a;->i:Z

    new-instance v2, Lcom/chase/sig/android/activity/jf;

    invoke-direct {v2, p0}, Lcom/chase/sig/android/activity/jf;-><init>(Lcom/chase/sig/android/activity/iy;)V

    iput-object v2, v0, Lcom/chase/sig/android/view/k$a;->g:Landroid/content/DialogInterface$OnCancelListener;

    const/4 v0, -0x1

    invoke-virtual {v3, v0}, Lcom/chase/sig/android/view/k$a;->d(I)Lcom/chase/sig/android/view/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/view/k;->show()V

    goto :goto_0

    .line 400
    :cond_0
    return-void
.end method

.method protected final F()V
    .locals 4

    .prologue
    .line 424
    iget-object v0, p0, Lcom/chase/sig/android/activity/iy;->e:Lcom/chase/sig/android/activity/mc;

    const-class v1, Lcom/chase/sig/android/activity/iy$a;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/mc;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/ae;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/activity/iy$a;

    .line 426
    invoke-virtual {v0}, Lcom/chase/sig/android/activity/iy$a;->d()Z

    move-result v1

    if-nez v1, :cond_0

    .line 427
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/chase/sig/android/activity/iy;->l:Lcom/chase/sig/android/domain/QuickPayRecipient;

    invoke-virtual {v3}, Lcom/chase/sig/android/domain/QuickPayRecipient;->d()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/iy$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 429
    :cond_0
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 59
    const v0, 0x7f03008e

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/iy;->b(I)V

    .line 60
    const v0, 0x7f09023f

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/iy;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/chase/sig/android/activity/iy;->b:Landroid/widget/Button;

    .line 61
    const v0, 0x7f0900f4

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/iy;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/iy;->d:Landroid/widget/TextView;

    .line 62
    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/iy;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    .line 64
    :cond_0
    if-eqz p1, :cond_1

    .line 65
    const-string v0, "quick_pay_transaction"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/QuickPayTransaction;

    iput-object v0, p0, Lcom/chase/sig/android/activity/iy;->k:Lcom/chase/sig/android/domain/QuickPayTransaction;

    .line 67
    const-string v0, "recipient"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/QuickPayRecipient;

    iput-object v0, p0, Lcom/chase/sig/android/activity/iy;->l:Lcom/chase/sig/android/domain/QuickPayRecipient;

    .line 68
    const-string v0, "saved date"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/Date;

    iput-object v0, p0, Lcom/chase/sig/android/activity/iy;->n:Ljava/util/Date;

    .line 69
    sget-object v0, Lcom/chase/sig/android/activity/iy;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/iy;->o:Ljava/lang/Object;

    .line 70
    const-string v0, "quick pay verify response"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;

    iput-object v0, p0, Lcom/chase/sig/android/activity/iy;->p:Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;

    .line 75
    :cond_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/iy;->k:Lcom/chase/sig/android/domain/QuickPayTransaction;

    if-nez v0, :cond_2

    .line 76
    new-instance v0, Lcom/chase/sig/android/domain/QuickPayTransaction;

    invoke-direct {v0}, Lcom/chase/sig/android/domain/QuickPayTransaction;-><init>()V

    iput-object v0, p0, Lcom/chase/sig/android/activity/iy;->k:Lcom/chase/sig/android/domain/QuickPayTransaction;

    .line 79
    :cond_2
    iget-object v0, p0, Lcom/chase/sig/android/activity/iy;->l:Lcom/chase/sig/android/domain/QuickPayRecipient;

    if-nez v0, :cond_3

    .line 80
    iget-object v0, p0, Lcom/chase/sig/android/activity/iy;->k:Lcom/chase/sig/android/domain/QuickPayTransaction;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->j()Lcom/chase/sig/android/domain/QuickPayRecipient;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/iy;->l:Lcom/chase/sig/android/domain/QuickPayRecipient;

    .line 83
    :cond_3
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/iy;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "qp_edit_one_payment"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 84
    iget-object v0, p0, Lcom/chase/sig/android/activity/iy;->k:Lcom/chase/sig/android/domain/QuickPayTransaction;

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/domain/QuickPayTransaction;->d(Z)V

    .line 87
    :cond_4
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/iy;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "send_money_for_request"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_5

    const v0, 0x7f090233

    new-instance v1, Lcom/chase/sig/android/activity/iz;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/iz;-><init>(Lcom/chase/sig/android/activity/iy;)V

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/iy;->a(ILandroid/view/View$OnClickListener;)V

    :cond_5
    iget-object v0, p0, Lcom/chase/sig/android/activity/iy;->b:Landroid/widget/Button;

    invoke-virtual {p0, v2}, Lcom/chase/sig/android/activity/iy;->i(I)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f090247

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/iy;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lcom/chase/sig/android/activity/ja;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/ja;-><init>(Lcom/chase/sig/android/activity/iy;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f090246

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/iy;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lcom/chase/sig/android/activity/a/a;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/a/a;-><init>(Lcom/chase/sig/android/activity/ma;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 88
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/iy;->f()V

    .line 89
    iget-object v0, p0, Lcom/chase/sig/android/activity/iy;->p:Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/chase/sig/android/activity/iy;->p:Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->r()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 90
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/iy;->E()V

    .line 92
    :cond_6
    return-void
.end method

.method protected d()Lcom/chase/sig/android/domain/QuickPayTransaction;
    .locals 2

    .prologue
    .line 297
    new-instance v1, Lcom/chase/sig/android/domain/QuickPayTransaction;

    invoke-direct {v1}, Lcom/chase/sig/android/domain/QuickPayTransaction;-><init>()V

    .line 298
    iget-object v0, p0, Lcom/chase/sig/android/activity/iy;->k:Lcom/chase/sig/android/domain/QuickPayTransaction;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->w()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->g(Ljava/lang/String;)V

    .line 299
    iget-object v0, p0, Lcom/chase/sig/android/activity/iy;->k:Lcom/chase/sig/android/domain/QuickPayTransaction;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->L()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->o(Ljava/lang/String;)V

    .line 300
    iget-object v0, p0, Lcom/chase/sig/android/activity/iy;->k:Lcom/chase/sig/android/domain/QuickPayTransaction;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->J()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->m(Ljava/lang/String;)V

    .line 301
    iget-object v0, p0, Lcom/chase/sig/android/activity/iy;->k:Lcom/chase/sig/android/domain/QuickPayTransaction;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->K()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->n(Ljava/lang/String;)V

    .line 302
    iget-object v0, p0, Lcom/chase/sig/android/activity/iy;->l:Lcom/chase/sig/android/domain/QuickPayRecipient;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->a(Lcom/chase/sig/android/domain/QuickPayRecipient;)V

    .line 303
    const v0, 0x7f0900f4

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/iy;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->d(Ljava/lang/String;)V

    .line 304
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/iy;->j()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->b(Ljava/util/Date;)V

    .line 305
    const v0, 0x7f090245

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/iy;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 306
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->e(Ljava/lang/String;)V

    .line 307
    iget-object v0, p0, Lcom/chase/sig/android/activity/iy;->k:Lcom/chase/sig/android/domain/QuickPayTransaction;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->R()Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->e(Z)V

    .line 308
    iget-object v0, p0, Lcom/chase/sig/android/activity/iy;->k:Lcom/chase/sig/android/domain/QuickPayTransaction;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->S()Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->f(Z)V

    .line 309
    iget-object v0, p0, Lcom/chase/sig/android/activity/iy;->k:Lcom/chase/sig/android/domain/QuickPayTransaction;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->W()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->u(Ljava/lang/String;)V

    .line 310
    return-object v1
.end method

.method protected abstract e()Z
.end method

.method protected f()V
    .locals 4

    .prologue
    const v1, 0x7f070230

    .line 141
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/iy;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->c:Lcom/chase/sig/android/service/quickpay/TodoListResponse;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->t()Lcom/chase/sig/android/domain/m;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/iy;->m:Lcom/chase/sig/android/domain/m;

    .line 143
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/iy;->h()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 144
    iget-object v0, p0, Lcom/chase/sig/android/activity/iy;->l:Lcom/chase/sig/android/domain/QuickPayRecipient;

    if-nez v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/iy;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/chase/sig/android/activity/iy;->l:Lcom/chase/sig/android/domain/QuickPayRecipient;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/QuickPayRecipient;->d()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    const-string v1, ""

    :goto_1
    iget-object v2, p0, Lcom/chase/sig/android/activity/iy;->l:Lcom/chase/sig/android/domain/QuickPayRecipient;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/QuickPayRecipient;->f()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    const-string v2, ""

    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    const v0, 0x7f090232

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/iy;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 148
    :goto_3
    const v0, 0x7f090241

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/iy;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/JPSpinner;

    .line 149
    new-instance v1, Landroid/widget/ArrayAdapter;

    const v2, 0x7f0300ad

    iget-object v3, p0, Lcom/chase/sig/android/activity/iy;->m:Lcom/chase/sig/android/domain/m;

    invoke-virtual {v3}, Lcom/chase/sig/android/domain/m;->a()[Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, p0, v2, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/JPSpinner;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 151
    iget-object v0, p0, Lcom/chase/sig/android/activity/iy;->k:Lcom/chase/sig/android/domain/QuickPayTransaction;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->r()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_5

    const-string v0, ""

    .line 152
    :goto_4
    iget-object v1, p0, Lcom/chase/sig/android/activity/iy;->d:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 154
    iget-object v0, p0, Lcom/chase/sig/android/activity/iy;->k:Lcom/chase/sig/android/domain/QuickPayTransaction;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->s()Ljava/util/Date;

    move-result-object v0

    if-nez v0, :cond_6

    invoke-direct {p0}, Lcom/chase/sig/android/activity/iy;->G()Ljava/util/Date;

    move-result-object v0

    :goto_5
    iget-object v1, p0, Lcom/chase/sig/android/activity/iy;->n:Ljava/util/Date;

    if-nez v1, :cond_7

    :goto_6
    iget-object v1, p0, Lcom/chase/sig/android/activity/iy;->b:Landroid/widget/Button;

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->a(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 155
    iget-object v0, p0, Lcom/chase/sig/android/activity/iy;->k:Lcom/chase/sig/android/domain/QuickPayTransaction;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->t()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_8

    const-string v0, ""

    :goto_7
    iget-object v1, p0, Lcom/chase/sig/android/activity/iy;->o:Ljava/lang/Object;

    if-nez v1, :cond_9

    :goto_8
    check-cast v0, Ljava/lang/CharSequence;

    const v1, 0x7f090245

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/iy;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 156
    return-void

    .line 144
    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/iy;->l:Lcom/chase/sig/android/domain/QuickPayRecipient;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayRecipient;->a()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "<br><font color=\"#696969\">"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/chase/sig/android/activity/iy;->l:Lcom/chase/sig/android/domain/QuickPayRecipient;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/QuickPayRecipient;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1

    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "<br><font color=\"#696969\">"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/chase/sig/android/activity/iy;->l:Lcom/chase/sig/android/domain/QuickPayRecipient;

    invoke-virtual {v3}, Lcom/chase/sig/android/domain/QuickPayRecipient;->f()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/chase/sig/android/util/s;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_2

    .line 146
    :cond_3
    iget-object v0, p0, Lcom/chase/sig/android/activity/iy;->l:Lcom/chase/sig/android/domain/QuickPayRecipient;

    if-nez v0, :cond_4

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/iy;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    :goto_9
    const v0, 0x7f090233

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/iy;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    :cond_4
    iget-object v0, p0, Lcom/chase/sig/android/activity/iy;->l:Lcom/chase/sig/android/domain/QuickPayRecipient;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayRecipient;->a()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_9

    .line 151
    :cond_5
    iget-object v0, p0, Lcom/chase/sig/android/activity/iy;->k:Lcom/chase/sig/android/domain/QuickPayTransaction;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->r()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_4

    .line 154
    :cond_6
    iget-object v0, p0, Lcom/chase/sig/android/activity/iy;->k:Lcom/chase/sig/android/domain/QuickPayTransaction;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->s()Ljava/util/Date;

    move-result-object v0

    goto/16 :goto_5

    :cond_7
    iget-object v0, p0, Lcom/chase/sig/android/activity/iy;->n:Ljava/util/Date;

    goto/16 :goto_6

    .line 155
    :cond_8
    iget-object v0, p0, Lcom/chase/sig/android/activity/iy;->k:Lcom/chase/sig/android/domain/QuickPayTransaction;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->t()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_7

    :cond_9
    iget-object v0, p0, Lcom/chase/sig/android/activity/iy;->o:Ljava/lang/Object;

    goto/16 :goto_8
.end method

.method public g()Z
    .locals 4

    .prologue
    const v3, 0x7f090231

    .line 433
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/iy;->y()V

    .line 434
    const/4 v0, 0x1

    .line 436
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/iy;->h()Z

    move-result v1

    if-nez v1, :cond_3

    .line 437
    iget-object v1, p0, Lcom/chase/sig/android/activity/iy;->l:Lcom/chase/sig/android/domain/QuickPayRecipient;

    if-nez v1, :cond_2

    .line 438
    invoke-virtual {p0, v3}, Lcom/chase/sig/android/activity/iy;->e(I)Z

    move-result v0

    move v1, v0

    .line 441
    :goto_0
    const v0, 0x7f090233

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/iy;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 442
    const v2, 0x7f070230

    invoke-virtual {p0, v2}, Lcom/chase/sig/android/activity/iy;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 443
    invoke-virtual {p0, v3}, Lcom/chase/sig/android/activity/iy;->e(I)Z

    move-result v1

    .line 447
    :cond_0
    :goto_1
    const v0, 0x7f0900f4

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/iy;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/AmountView;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/AmountView;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 448
    const v0, 0x7f09023a

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/iy;->e(I)Z

    move-result v1

    .line 451
    :cond_1
    return v1

    :cond_2
    move v1, v0

    goto :goto_0

    :cond_3
    move v1, v0

    goto :goto_1
.end method

.method public final h()Z
    .locals 3

    .prologue
    .line 137
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/iy;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "is_editing"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method protected final j()Ljava/util/Date;
    .locals 1

    .prologue
    .line 319
    const v0, 0x7f09023f

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/iy;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 320
    invoke-virtual {v0}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->e(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public k()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 195
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/iy;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "is_editing"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/iy;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "send_money_for_request"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 197
    :cond_0
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/iy;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "quick_pay_transaction"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/QuickPayTransaction;

    iput-object v0, p0, Lcom/chase/sig/android/activity/iy;->k:Lcom/chase/sig/android/domain/QuickPayTransaction;

    .line 200
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/iy;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "qp_edit_one_payment"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 201
    iget-object v0, p0, Lcom/chase/sig/android/activity/iy;->k:Lcom/chase/sig/android/domain/QuickPayTransaction;

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/domain/QuickPayTransaction;->d(Z)V

    .line 206
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/iy;->k:Lcom/chase/sig/android/domain/QuickPayTransaction;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->s()Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/iy;->n:Ljava/util/Date;

    .line 207
    iget-object v0, p0, Lcom/chase/sig/android/activity/iy;->k:Lcom/chase/sig/android/domain/QuickPayTransaction;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->t()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/iy;->o:Ljava/lang/Object;

    .line 208
    iget-object v0, p0, Lcom/chase/sig/android/activity/iy;->k:Lcom/chase/sig/android/domain/QuickPayTransaction;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->j()Lcom/chase/sig/android/domain/QuickPayRecipient;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/iy;->l:Lcom/chase/sig/android/domain/QuickPayRecipient;

    .line 209
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/iy;->y()V

    .line 210
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/iy;->f()V

    .line 211
    return-void

    .line 204
    :cond_2
    new-instance v0, Lcom/chase/sig/android/domain/QuickPayTransaction;

    invoke-direct {v0}, Lcom/chase/sig/android/domain/QuickPayTransaction;-><init>()V

    iput-object v0, p0, Lcom/chase/sig/android/activity/iy;->k:Lcom/chase/sig/android/domain/QuickPayTransaction;

    goto :goto_0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 7
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 237
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ai;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    .line 238
    new-instance v1, Lcom/chase/sig/android/view/k$a;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/view/k$a;-><init>(Landroid/content/Context;)V

    .line 239
    sparse-switch p1, :sswitch_data_0

    .line 280
    :goto_0
    return-object v0

    .line 242
    :sswitch_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v6

    .line 243
    const/4 v0, 0x5

    const/16 v1, 0x16e

    invoke-virtual {v6, v0, v1}, Ljava/util/Calendar;->add(II)V

    .line 246
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v5

    .line 247
    iget-object v0, p0, Lcom/chase/sig/android/activity/iy;->k:Lcom/chase/sig/android/domain/QuickPayTransaction;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->s()Ljava/util/Date;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/iy;->h()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/iy;->k:Lcom/chase/sig/android/domain/QuickPayTransaction;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->s()Ljava/util/Date;

    move-result-object v0

    :goto_1
    invoke-virtual {v5, v0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 250
    new-instance v0, Lcom/chase/sig/android/view/b;

    iget-object v2, p0, Lcom/chase/sig/android/activity/iy;->q:Lcom/chase/sig/android/view/b$a;

    move-object v1, p0

    move v4, v3

    invoke-direct/range {v0 .. v6}, Lcom/chase/sig/android/view/b;-><init>(Landroid/content/Context;Lcom/chase/sig/android/view/b$a;ZZLjava/util/Calendar;Ljava/util/Calendar;)V

    goto :goto_0

    .line 247
    :cond_0
    invoke-direct {p0}, Lcom/chase/sig/android/activity/iy;->G()Ljava/util/Date;

    move-result-object v0

    goto :goto_1

    .line 252
    :sswitch_1
    const/4 v0, 0x1

    iput-boolean v0, v1, Lcom/chase/sig/android/view/k$a;->i:Z

    .line 253
    const v0, 0x7f07006b

    new-instance v2, Lcom/chase/sig/android/activity/jb;

    invoke-direct {v2, p0}, Lcom/chase/sig/android/activity/jb;-><init>(Lcom/chase/sig/android/activity/iy;)V

    invoke-virtual {v1, v0, v2}, Lcom/chase/sig/android/view/k$a;->b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    .line 261
    const v0, 0x7f070200

    new-instance v2, Lcom/chase/sig/android/activity/jc;

    invoke-direct {v2, p0}, Lcom/chase/sig/android/activity/jc;-><init>(Lcom/chase/sig/android/activity/iy;)V

    invoke-virtual {v1, v0, v2}, Lcom/chase/sig/android/view/k$a;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    .line 269
    new-instance v0, Lcom/chase/sig/android/activity/jd;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/activity/jd;-><init>(Lcom/chase/sig/android/activity/iy;)V

    iput-object v0, v1, Lcom/chase/sig/android/view/k$a;->g:Landroid/content/DialogInterface$OnCancelListener;

    .line 276
    iget-object v0, p0, Lcom/chase/sig/android/activity/iy;->p:Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/view/k$a;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/k$a;

    .line 277
    const/4 v0, -0x1

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/view/k$a;->d(I)Lcom/chase/sig/android/view/k;

    move-result-object v0

    goto :goto_0

    .line 239
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x4 -> :sswitch_1
    .end sparse-switch
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 392
    invoke-super {p0}, Lcom/chase/sig/android/activity/ai;->onResume()V

    .line 393
    invoke-static {}, Lcom/chase/sig/android/c;->a()V

    .line 394
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 224
    sget-object v1, Lcom/chase/sig/android/activity/iy;->a:Ljava/lang/String;

    const v0, 0x7f090245

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/iy;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    const-string v0, "saved date"

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/iy;->j()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 227
    const-string v0, "recipient"

    iget-object v1, p0, Lcom/chase/sig/android/activity/iy;->l:Lcom/chase/sig/android/domain/QuickPayRecipient;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 228
    const-string v0, "quick_pay_transaction"

    iget-object v1, p0, Lcom/chase/sig/android/activity/iy;->k:Lcom/chase/sig/android/domain/QuickPayTransaction;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 229
    const-string v0, "quick pay verify response"

    iget-object v1, p0, Lcom/chase/sig/android/activity/iy;->p:Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 231
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ai;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 232
    return-void
.end method
