.class final Lcom/chase/sig/android/activity/iz;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/iy;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/iy;)V
    .locals 0
    .parameter

    .prologue
    .line 96
    iput-object p1, p0, Lcom/chase/sig/android/activity/iz;->a:Lcom/chase/sig/android/activity/iy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3
    .parameter

    .prologue
    .line 99
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/iz;->a:Lcom/chase/sig/android/activity/iy;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/iy;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/chase/sig/android/activity/iz;->a:Lcom/chase/sig/android/activity/iy;

    const-class v1, Lcom/chase/sig/android/activity/iy$b;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/chase/sig/android/activity/iy;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 109
    :goto_0
    return-void

    .line 102
    :cond_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/chase/sig/android/activity/iz;->a:Lcom/chase/sig/android/activity/iy;

    const-class v2, Lcom/chase/sig/android/activity/QuickPayChooseRecipientActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 105
    const-string v1, "quick_pay_transaction"

    iget-object v2, p0, Lcom/chase/sig/android/activity/iz;->a:Lcom/chase/sig/android/activity/iy;

    invoke-virtual {v2}, Lcom/chase/sig/android/activity/iy;->d()Lcom/chase/sig/android/domain/QuickPayTransaction;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 106
    const-string v1, "isRequestForMoney"

    iget-object v2, p0, Lcom/chase/sig/android/activity/iz;->a:Lcom/chase/sig/android/activity/iy;

    invoke-virtual {v2}, Lcom/chase/sig/android/activity/iy;->e()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 107
    iget-object v1, p0, Lcom/chase/sig/android/activity/iz;->a:Lcom/chase/sig/android/activity/iy;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/activity/iy;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
