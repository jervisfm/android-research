.class final Lcom/chase/sig/android/activity/ji;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Z

.field final synthetic b:Lcom/chase/sig/android/domain/QuickPayTransaction;

.field final synthetic c:Lcom/chase/sig/android/service/TodoItem;

.field final synthetic d:Ljava/lang/Class;

.field final synthetic e:I

.field final synthetic f:Lcom/chase/sig/android/activity/QuickPayTodoListActivity$c;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/QuickPayTodoListActivity$c;ZLcom/chase/sig/android/domain/QuickPayTransaction;Lcom/chase/sig/android/service/TodoItem;Ljava/lang/Class;I)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 235
    iput-object p1, p0, Lcom/chase/sig/android/activity/ji;->f:Lcom/chase/sig/android/activity/QuickPayTodoListActivity$c;

    iput-boolean p2, p0, Lcom/chase/sig/android/activity/ji;->a:Z

    iput-object p3, p0, Lcom/chase/sig/android/activity/ji;->b:Lcom/chase/sig/android/domain/QuickPayTransaction;

    iput-object p4, p0, Lcom/chase/sig/android/activity/ji;->c:Lcom/chase/sig/android/service/TodoItem;

    iput-object p5, p0, Lcom/chase/sig/android/activity/ji;->d:Ljava/lang/Class;

    iput p6, p0, Lcom/chase/sig/android/activity/ji;->e:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 4
    .parameter

    .prologue
    .line 239
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    iget-boolean v0, p0, Lcom/chase/sig/android/activity/ji;->a:Z

    if-eqz v0, :cond_1

    .line 240
    iget-object v0, p0, Lcom/chase/sig/android/activity/ji;->f:Lcom/chase/sig/android/activity/QuickPayTodoListActivity$c;

    iget-object v0, v0, Lcom/chase/sig/android/activity/QuickPayTodoListActivity$c;->b:Lcom/chase/sig/android/activity/QuickPayTodoListActivity;

    iget-object v0, v0, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->e:Lcom/chase/sig/android/activity/mc;

    const-class v1, Lcom/chase/sig/android/activity/QuickPayTodoListActivity$a;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/mc;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/ae;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/activity/QuickPayTodoListActivity$a;

    .line 242
    invoke-virtual {v0}, Lcom/chase/sig/android/activity/QuickPayTodoListActivity$a;->d()Z

    move-result v1

    if-nez v1, :cond_0

    .line 243
    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/chase/sig/android/activity/ji;->b:Lcom/chase/sig/android/domain/QuickPayTransaction;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/chase/sig/android/activity/ji;->c:Lcom/chase/sig/android/service/TodoItem;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/chase/sig/android/activity/ji;->d:Ljava/lang/Class;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/QuickPayTodoListActivity$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 248
    :cond_0
    :goto_0
    return-void

    .line 246
    :cond_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/ji;->f:Lcom/chase/sig/android/activity/QuickPayTodoListActivity$c;

    iget-object v0, v0, Lcom/chase/sig/android/activity/QuickPayTodoListActivity$c;->b:Lcom/chase/sig/android/activity/QuickPayTodoListActivity;

    iget v1, p0, Lcom/chase/sig/android/activity/ji;->e:I

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/QuickPayTodoListActivity;->showDialog(I)V

    goto :goto_0
.end method
