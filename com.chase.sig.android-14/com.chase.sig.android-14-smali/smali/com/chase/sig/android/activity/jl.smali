.class final Lcom/chase/sig/android/activity/jl;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 58
    iput-object p1, p0, Lcom/chase/sig/android/activity/jl;->a:Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 4
    .parameter

    .prologue
    .line 61
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    new-instance v0, Lcom/chase/sig/android/domain/QuickPayDeclineTransaction;

    invoke-direct {v0}, Lcom/chase/sig/android/domain/QuickPayDeclineTransaction;-><init>()V

    .line 62
    iget-object v1, p0, Lcom/chase/sig/android/activity/jl;->a:Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;

    iget-object v1, v1, Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;->a:Ljava/util/ArrayList;

    invoke-static {v1, v0}, Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;->a(Ljava/util/List;Lcom/chase/sig/android/domain/QuickPayTransaction;)Lcom/chase/sig/android/domain/QuickPayTransaction;

    .line 63
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/chase/sig/android/activity/jl;->a:Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;

    const-class v3, Lcom/chase/sig/android/activity/QuickPayDeclineVerifyActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 65
    const-string v2, "quick_pay_transaction"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 66
    iget-object v0, p0, Lcom/chase/sig/android/activity/jl;->a:Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;->startActivity(Landroid/content/Intent;)V

    .line 67
    return-void
.end method
