.class final Lcom/chase/sig/android/activity/jm;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/domain/QuickPayRecipient;

.field final synthetic b:Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;Lcom/chase/sig/android/domain/QuickPayRecipient;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 70
    iput-object p1, p0, Lcom/chase/sig/android/activity/jm;->b:Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;

    iput-object p2, p0, Lcom/chase/sig/android/activity/jm;->a:Lcom/chase/sig/android/domain/QuickPayRecipient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 5
    .parameter

    .prologue
    const/4 v4, 0x1

    .line 73
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/jm;->b:Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->c:Lcom/chase/sig/android/service/quickpay/TodoListResponse;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    new-instance v0, Lcom/chase/sig/android/domain/QuickPayTransaction;

    invoke-direct {v0}, Lcom/chase/sig/android/domain/QuickPayTransaction;-><init>()V

    .line 75
    iget-object v1, p0, Lcom/chase/sig/android/activity/jm;->b:Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;

    iget-object v1, v1, Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;->a:Ljava/util/ArrayList;

    invoke-static {v1, v0}, Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;->a(Ljava/util/List;Lcom/chase/sig/android/domain/QuickPayTransaction;)Lcom/chase/sig/android/domain/QuickPayTransaction;

    .line 76
    invoke-virtual {v0, v4}, Lcom/chase/sig/android/domain/QuickPayTransaction;->e(Z)V

    .line 77
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/chase/sig/android/activity/jm;->b:Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;

    const-class v3, Lcom/chase/sig/android/activity/QuickPaySendMoneyActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 79
    const-string v2, "quick_pay_transaction"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 80
    const-string v0, "recipient"

    iget-object v2, p0, Lcom/chase/sig/android/activity/jm;->a:Lcom/chase/sig/android/domain/QuickPayRecipient;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 81
    const-string v0, "send_money_for_request"

    invoke-virtual {v1, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 82
    iget-object v0, p0, Lcom/chase/sig/android/activity/jm;->b:Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;->startActivity(Landroid/content/Intent;)V

    .line 86
    :goto_0
    return-void

    .line 84
    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/jm;->b:Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;

    const/4 v1, -0x8

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;->showDialog(I)V

    goto :goto_0
.end method
