.class final Lcom/chase/sig/android/activity/jn;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 91
    iput-object p1, p0, Lcom/chase/sig/android/activity/jn;->a:Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 4
    .parameter

    .prologue
    .line 94
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/jn;->a:Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->c:Lcom/chase/sig/android/service/quickpay/TodoListResponse;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->r()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 95
    new-instance v1, Lcom/chase/sig/android/domain/QuickPayAcceptMoneyTransaction;

    invoke-direct {v1}, Lcom/chase/sig/android/domain/QuickPayAcceptMoneyTransaction;-><init>()V

    .line 96
    iget-object v0, p0, Lcom/chase/sig/android/activity/jn;->a:Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;

    iget-object v0, v0, Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;->a:Ljava/util/ArrayList;

    invoke-static {v0, v1}, Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;->a(Ljava/util/List;Lcom/chase/sig/android/domain/QuickPayTransaction;)Lcom/chase/sig/android/domain/QuickPayTransaction;

    .line 98
    iget-object v0, p0, Lcom/chase/sig/android/activity/jn;->a:Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;

    iget-object v0, v0, Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;->e:Lcom/chase/sig/android/activity/mc;

    const-class v2, Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity$a;

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/activity/mc;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/ae;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity$a;

    .line 100
    invoke-virtual {v0}, Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity$a;->d()Z

    move-result v2

    if-nez v2, :cond_0

    .line 101
    const/4 v2, 0x1

    new-array v2, v2, [Lcom/chase/sig/android/domain/QuickPayAcceptMoneyTransaction;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 106
    :cond_0
    :goto_0
    return-void

    .line 104
    :cond_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/jn;->a:Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;

    const/16 v1, -0x9

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;->showDialog(I)V

    goto :goto_0
.end method
