.class final Lcom/chase/sig/android/activity/jt;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/QuoteDetailsActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/QuoteDetailsActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 218
    iput-object p1, p0, Lcom/chase/sig/android/activity/jt;->a:Lcom/chase/sig/android/activity/QuoteDetailsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3
    .parameter

    .prologue
    .line 221
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/chase/sig/android/activity/jt;->a:Lcom/chase/sig/android/activity/QuoteDetailsActivity;

    const-class v2, Lcom/chase/sig/android/activity/QuoteChartsActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 222
    const-string v1, "exchangeName"

    iget-object v2, p0, Lcom/chase/sig/android/activity/jt;->a:Lcom/chase/sig/android/activity/QuoteDetailsActivity;

    invoke-static {v2}, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->b(Lcom/chase/sig/android/activity/QuoteDetailsActivity;)Lcom/chase/sig/android/domain/Quote;

    move-result-object v2

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/Quote;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 223
    const-string v1, "transaction_object"

    iget-object v2, p0, Lcom/chase/sig/android/activity/jt;->a:Lcom/chase/sig/android/activity/QuoteDetailsActivity;

    invoke-static {v2}, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->b(Lcom/chase/sig/android/activity/QuoteDetailsActivity;)Lcom/chase/sig/android/domain/Quote;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 224
    iget-object v1, p0, Lcom/chase/sig/android/activity/jt;->a:Lcom/chase/sig/android/activity/QuoteDetailsActivity;

    const/16 v2, 0x23

    invoke-virtual {v1, v0, v2}, Lcom/chase/sig/android/activity/QuoteDetailsActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 226
    return-void
.end method
