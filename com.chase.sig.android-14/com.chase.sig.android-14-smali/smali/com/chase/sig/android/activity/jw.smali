.class final Lcom/chase/sig/android/activity/jw;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/ReceiptDetailActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/ReceiptDetailActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 66
    iput-object p1, p0, Lcom/chase/sig/android/activity/jw;->a:Lcom/chase/sig/android/activity/ReceiptDetailActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 4
    .parameter

    .prologue
    .line 70
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/jw;->a:Lcom/chase/sig/android/activity/ReceiptDetailActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/ReceiptDetailActivity;->a(Lcom/chase/sig/android/activity/ReceiptDetailActivity;)Lcom/chase/sig/android/domain/Receipt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Receipt;->h()Lcom/chase/sig/android/domain/ReceiptPhotoList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/ReceiptPhotoList;->a()Ljava/util/List;

    move-result-object v0

    .line 71
    new-instance v1, Lcom/chase/sig/android/domain/ReceiptPhotoList;

    invoke-direct {v1}, Lcom/chase/sig/android/domain/ReceiptPhotoList;-><init>()V

    .line 72
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/ReceiptPhotoList$ReceiptPhoto;

    .line 75
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/ReceiptPhotoList$ReceiptPhoto;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/ReceiptPhotoList;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 77
    :cond_0
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lcom/chase/sig/android/activity/jw;->a:Lcom/chase/sig/android/activity/ReceiptDetailActivity;

    const-class v3, Lcom/chase/sig/android/activity/ReceiptPhotosViewActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 79
    const-string v2, "receipt_photo_list"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 80
    iget-object v1, p0, Lcom/chase/sig/android/activity/jw;->a:Lcom/chase/sig/android/activity/ReceiptDetailActivity;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/activity/ReceiptDetailActivity;->startActivity(Landroid/content/Intent;)V

    .line 81
    return-void
.end method
