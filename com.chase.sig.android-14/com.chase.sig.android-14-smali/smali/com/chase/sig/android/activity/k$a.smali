.class public Lcom/chase/sig/android/activity/k$a;
.super Lcom/chase/sig/android/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/k;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/chase/sig/android/domain/Transaction;",
        ">",
        "Lcom/chase/sig/android/d",
        "<",
        "Lcom/chase/sig/android/activity/k",
        "<TT;>;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/service/movemoney/ServiceResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/chase/sig/android/d;-><init>()V

    return-void
.end method

.method private varargs a()Lcom/chase/sig/android/service/movemoney/ServiceResponse;
    .locals 3

    .prologue
    .line 32
    :try_start_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/k;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/k;->f()Lcom/chase/sig/android/domain/Transaction;

    move-result-object v1

    .line 33
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/k;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/k;->c()Lcom/chase/sig/android/service/movemoney/d;

    move-result-object v2

    .line 34
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/k;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/k;->g()Lcom/chase/sig/android/service/movemoney/RequestFlags;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Lcom/chase/sig/android/service/movemoney/d;->a(Lcom/chase/sig/android/domain/Transaction;Lcom/chase/sig/android/service/movemoney/RequestFlags;)Lcom/chase/sig/android/service/movemoney/ServiceResponse;

    move-result-object v1

    .line 36
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/k;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/k;->l()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    .line 40
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected final bridge synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/chase/sig/android/activity/k$a;->a()Lcom/chase/sig/android/service/movemoney/ServiceResponse;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 5
    .parameter

    .prologue
    .line 26
    check-cast p1, Lcom/chase/sig/android/service/movemoney/ServiceResponse;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/movemoney/ServiceResponse;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/k;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/movemoney/ServiceResponse;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/k;->c(Ljava/util/List;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/k;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/k;->f()Lcom/chase/sig/android/domain/Transaction;

    move-result-object v1

    invoke-virtual {p1}, Lcom/chase/sig/android/service/movemoney/ServiceResponse;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/Transaction;->k(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/k;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/k;->b()Z

    move-result v2

    new-instance v3, Landroid/content/Intent;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/k;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/k;->getBaseContext()Landroid/content/Context;

    move-result-object v4

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/k;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/k;->d()Ljava/lang/Class;

    move-result-object v0

    invoke-direct {v3, v4, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v0, "transaction_object"

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v1, "queued_errors"

    invoke-virtual {p1}, Lcom/chase/sig/android/service/movemoney/ServiceResponse;->g()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/io/Serializable;

    invoke-virtual {v3, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v0, "hideFrequency"

    invoke-virtual {v3, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/k;

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/activity/k;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
