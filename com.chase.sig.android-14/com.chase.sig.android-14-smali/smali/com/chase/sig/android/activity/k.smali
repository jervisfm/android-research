.class public abstract Lcom/chase/sig/android/activity/k;
.super Lcom/chase/sig/android/activity/n;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/k$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/chase/sig/android/domain/Transaction;",
        ">",
        "Lcom/chase/sig/android/activity/n",
        "<TT;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/chase/sig/android/activity/n;-><init>()V

    .line 26
    return-void
.end method


# virtual methods
.method public final b()Z
    .locals 1

    .prologue
    .line 18
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/k;->f()Lcom/chase/sig/android/domain/Transaction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Transaction;->l()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/k;->g()Lcom/chase/sig/android/service/movemoney/RequestFlags;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/service/movemoney/RequestFlags;->b()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract c()Lcom/chase/sig/android/service/movemoney/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/chase/sig/android/service/movemoney/d",
            "<TT;>;"
        }
    .end annotation
.end method

.method protected abstract d()Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/chase/sig/android/activity/eb;",
            ">;"
        }
    .end annotation
.end method
