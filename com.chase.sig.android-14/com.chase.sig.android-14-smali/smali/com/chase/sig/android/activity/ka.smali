.class final Lcom/chase/sig/android/activity/ka;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/ReceiptDetailActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/ReceiptDetailActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 137
    iput-object p1, p0, Lcom/chase/sig/android/activity/ka;->a:Lcom/chase/sig/android/activity/ReceiptDetailActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3
    .parameter

    .prologue
    .line 141
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/chase/sig/android/activity/ka;->a:Lcom/chase/sig/android/activity/ReceiptDetailActivity;

    const-class v2, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 143
    const-string v1, "receipt"

    iget-object v2, p0, Lcom/chase/sig/android/activity/ka;->a:Lcom/chase/sig/android/activity/ReceiptDetailActivity;

    invoke-static {v2}, Lcom/chase/sig/android/activity/ReceiptDetailActivity;->a(Lcom/chase/sig/android/activity/ReceiptDetailActivity;)Lcom/chase/sig/android/domain/Receipt;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 144
    const-string v1, "is_browsing_receipts"

    iget-object v2, p0, Lcom/chase/sig/android/activity/ka;->a:Lcom/chase/sig/android/activity/ReceiptDetailActivity;

    invoke-static {v2}, Lcom/chase/sig/android/activity/ReceiptDetailActivity;->b(Lcom/chase/sig/android/activity/ReceiptDetailActivity;)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 146
    iget-object v1, p0, Lcom/chase/sig/android/activity/ka;->a:Lcom/chase/sig/android/activity/ReceiptDetailActivity;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/activity/ReceiptDetailActivity;->startActivity(Landroid/content/Intent;)V

    .line 147
    return-void
.end method
