.class final Lcom/chase/sig/android/activity/kc;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lcom/chase/sig/android/domain/ReceiptAccount;

.field final synthetic c:Lcom/chase/sig/android/activity/ReceiptsBrowseActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/ReceiptsBrowseActivity;Ljava/lang/String;Lcom/chase/sig/android/domain/ReceiptAccount;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 100
    iput-object p1, p0, Lcom/chase/sig/android/activity/kc;->c:Lcom/chase/sig/android/activity/ReceiptsBrowseActivity;

    iput-object p2, p0, Lcom/chase/sig/android/activity/kc;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/chase/sig/android/activity/kc;->b:Lcom/chase/sig/android/domain/ReceiptAccount;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3
    .parameter

    .prologue
    .line 105
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/chase/sig/android/activity/kc;->c:Lcom/chase/sig/android/activity/ReceiptsBrowseActivity;

    const-class v2, Lcom/chase/sig/android/activity/ReceiptsListActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 107
    const-string v1, "header_text"

    iget-object v2, p0, Lcom/chase/sig/android/activity/kc;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 109
    const-string v1, "account_id"

    iget-object v2, p0, Lcom/chase/sig/android/activity/kc;->b:Lcom/chase/sig/android/domain/ReceiptAccount;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/ReceiptAccount;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 111
    const-string v1, "filter_set"

    iget-object v2, p0, Lcom/chase/sig/android/activity/kc;->c:Lcom/chase/sig/android/activity/ReceiptsBrowseActivity;

    invoke-static {v2}, Lcom/chase/sig/android/activity/ReceiptsBrowseActivity;->a(Lcom/chase/sig/android/activity/ReceiptsBrowseActivity;)Lcom/chase/sig/android/service/ListContentResponse;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 113
    iget-object v1, p0, Lcom/chase/sig/android/activity/kc;->c:Lcom/chase/sig/android/activity/ReceiptsBrowseActivity;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/activity/ReceiptsBrowseActivity;->startActivity(Landroid/content/Intent;)V

    .line 114
    return-void
.end method
