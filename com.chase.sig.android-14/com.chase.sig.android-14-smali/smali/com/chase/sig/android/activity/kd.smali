.class final Lcom/chase/sig/android/activity/kd;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Landroid/widget/Button;

.field final synthetic b:Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;Landroid/widget/Button;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 62
    iput-object p1, p0, Lcom/chase/sig/android/activity/kd;->b:Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;

    iput-object p2, p0, Lcom/chase/sig/android/activity/kd;->a:Landroid/widget/Button;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 5
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 66
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/kd;->b:Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;

    const v1, 0x7f09003d

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 68
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 69
    iget-object v0, p0, Lcom/chase/sig/android/activity/kd;->a:Landroid/widget/Button;

    iget-object v2, p0, Lcom/chase/sig/android/activity/kd;->b:Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;

    const v3, 0x7f070073

    invoke-virtual {v2, v3}, Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 70
    iget-object v0, p0, Lcom/chase/sig/android/activity/kd;->b:Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;->a(Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;)V

    .line 71
    const/4 v0, 0x4

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 72
    iget-object v0, p0, Lcom/chase/sig/android/activity/kd;->b:Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;->b(Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;)Landroid/widget/ImageView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 81
    :goto_0
    return-void

    .line 75
    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/kd;->a:Landroid/widget/Button;

    iget-object v2, p0, Lcom/chase/sig/android/activity/kd;->b:Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;

    const v3, 0x7f07006c

    invoke-virtual {v2, v3}, Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 76
    iget-object v0, p0, Lcom/chase/sig/android/activity/kd;->b:Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;

    const v2, 0x7f090034

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 77
    const v2, 0x7f07027a

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 78
    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 79
    iget-object v0, p0, Lcom/chase/sig/android/activity/kd;->b:Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;->b(Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setClickable(Z)V

    goto :goto_0
.end method
