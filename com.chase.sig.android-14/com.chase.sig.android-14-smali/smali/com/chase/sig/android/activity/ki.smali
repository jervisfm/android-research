.class final Lcom/chase/sig/android/activity/ki;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/hardware/Camera$PictureCallback;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 137
    iput-object p1, p0, Lcom/chase/sig/android/activity/ki;->a:Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPictureTaken([BLandroid/hardware/Camera;)V
    .locals 5
    .parameter
    .parameter

    .prologue
    .line 141
    new-instance v1, Landroid/content/Intent;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ki;->a:Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;

    const-class v2, Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 143
    iget-object v0, p0, Lcom/chase/sig/android/activity/ki;->a:Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 144
    if-eqz v0, :cond_1

    .line 145
    const-string v2, "receipt_curr_photo_number"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 146
    const-string v2, "receipt_curr_photo_number"

    const-string v3, "receipt_curr_photo_number"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 149
    :cond_0
    const-string v2, "receipt_photo_list"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 150
    const-string v2, "receipt_photo_list"

    const-string v3, "receipt_photo_list"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 154
    :cond_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/ki;->a:Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "receipt"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/Receipt;

    .line 155
    if-eqz v0, :cond_2

    .line 156
    iget-object v2, p0, Lcom/chase/sig/android/activity/ki;->a:Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;

    invoke-virtual {v2}, Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "is_browsing_receipts"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 157
    const-string v3, "receipt"

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 158
    const-string v0, "is_browsing_receipts"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 161
    :cond_2
    const/high16 v0, 0x4000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 162
    const-string v0, "image_data"

    invoke-static {p1}, Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;->a([B)[B

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 163
    iget-object v0, p0, Lcom/chase/sig/android/activity/ki;->a:Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;->startActivity(Landroid/content/Intent;)V

    .line 164
    return-void
.end method
