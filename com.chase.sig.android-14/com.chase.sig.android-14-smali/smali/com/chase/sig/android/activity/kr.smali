.class final Lcom/chase/sig/android/activity/kr;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 450
    iput-object p1, p0, Lcom/chase/sig/android/activity/kr;->a:Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 5
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 454
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/kr;->a:Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->b(Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 458
    iget-object v0, p0, Lcom/chase/sig/android/activity/kr;->a:Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->a(Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;)Lcom/chase/sig/android/domain/Receipt;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/kr;->a:Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->a(Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;)Lcom/chase/sig/android/domain/Receipt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Receipt;->j()[Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    .line 459
    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/kr;->a:Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->c(Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 460
    iget-object v0, p0, Lcom/chase/sig/android/activity/kr;->a:Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;

    const-class v1, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity$a;

    new-array v2, v2, [Lcom/chase/sig/android/domain/Receipt;

    iget-object v3, p0, Lcom/chase/sig/android/activity/kr;->a:Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;

    invoke-static {v3}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->d(Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;)Lcom/chase/sig/android/domain/Receipt;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 475
    :cond_1
    :goto_0
    return-void

    .line 462
    :cond_2
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/chase/sig/android/activity/kr;->a:Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;

    const-class v2, Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 465
    const-string v1, "receipt"

    iget-object v2, p0, Lcom/chase/sig/android/activity/kr;->a:Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;

    invoke-static {v2}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->d(Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;)Lcom/chase/sig/android/domain/Receipt;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 466
    const-string v1, "is_editing"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 468
    iget-object v1, p0, Lcom/chase/sig/android/activity/kr;->a:Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 471
    :cond_3
    iget-object v0, p0, Lcom/chase/sig/android/activity/kr;->a:Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;

    const-class v1, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity$c;

    new-array v2, v2, [Lcom/chase/sig/android/domain/Receipt;

    iget-object v3, p0, Lcom/chase/sig/android/activity/kr;->a:Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;

    invoke-static {v3}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->d(Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;)Lcom/chase/sig/android/domain/Receipt;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    goto :goto_0
.end method
