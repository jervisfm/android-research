.class final Lcom/chase/sig/android/activity/ku;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/ReceiptsHomeActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/ReceiptsHomeActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 187
    iput-object p1, p0, Lcom/chase/sig/android/activity/ku;->a:Lcom/chase/sig/android/activity/ReceiptsHomeActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3
    .parameter

    .prologue
    .line 191
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/chase/sig/android/activity/ku;->a:Lcom/chase/sig/android/activity/ReceiptsHomeActivity;

    const-class v2, Lcom/chase/sig/android/activity/ReceiptsBrowseActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 193
    iget-object v1, p0, Lcom/chase/sig/android/activity/ku;->a:Lcom/chase/sig/android/activity/ReceiptsHomeActivity;

    invoke-static {v1}, Lcom/chase/sig/android/activity/ReceiptsHomeActivity;->a(Lcom/chase/sig/android/activity/ReceiptsHomeActivity;)Lcom/chase/sig/android/domain/ReceiptAccountSummary;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 194
    const-string v1, "acctSummary"

    iget-object v2, p0, Lcom/chase/sig/android/activity/ku;->a:Lcom/chase/sig/android/activity/ReceiptsHomeActivity;

    invoke-static {v2}, Lcom/chase/sig/android/activity/ReceiptsHomeActivity;->a(Lcom/chase/sig/android/activity/ReceiptsHomeActivity;)Lcom/chase/sig/android/domain/ReceiptAccountSummary;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 196
    :cond_0
    iget-object v1, p0, Lcom/chase/sig/android/activity/ku;->a:Lcom/chase/sig/android/activity/ReceiptsHomeActivity;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/activity/ReceiptsHomeActivity;->startActivity(Landroid/content/Intent;)V

    .line 197
    return-void
.end method
