.class public abstract Lcom/chase/sig/android/activity/l$a;
.super Lcom/chase/sig/android/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/l;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/chase/sig/android/domain/Transaction;",
        ">",
        "Lcom/chase/sig/android/d",
        "<",
        "Lcom/chase/sig/android/activity/l",
        "<TT;>;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/service/movemoney/ListServiceResponse",
        "<TT;>;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 143
    invoke-direct {p0}, Lcom/chase/sig/android/d;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Integer;)Lcom/chase/sig/android/service/movemoney/ListServiceResponse;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Integer;",
            ")",
            "Lcom/chase/sig/android/service/movemoney/ListServiceResponse",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 148
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/l$a;->a()Lcom/chase/sig/android/service/movemoney/d;

    move-result-object v0

    .line 149
    const/4 v1, 0x0

    aget-object v1, p1, v1

    .line 150
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/movemoney/d;->c(I)Lcom/chase/sig/android/service/movemoney/ListServiceResponse;

    move-result-object v0

    return-object v0
.end method

.method protected abstract a()Lcom/chase/sig/android/service/movemoney/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/chase/sig/android/service/movemoney/d",
            "<TT;>;"
        }
    .end annotation
.end method

.method protected bridge synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 143
    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/chase/sig/android/activity/l$a;->a([Ljava/lang/Integer;)Lcom/chase/sig/android/service/movemoney/ListServiceResponse;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/chase/sig/android/service/movemoney/ListServiceResponse;)V
    .locals 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/chase/sig/android/service/movemoney/ListServiceResponse",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 157
    invoke-virtual {p1}, Lcom/chase/sig/android/service/movemoney/ListServiceResponse;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 158
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/l;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/movemoney/ListServiceResponse;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/l;->b(Ljava/util/List;)V

    .line 164
    :goto_0
    return-void

    .line 160
    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/l;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/movemoney/ListServiceResponse;->d()Ljava/util/List;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    iput-object v1, v0, Lcom/chase/sig/android/activity/l;->b:Ljava/util/ArrayList;

    .line 161
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/l;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/movemoney/ListServiceResponse;->a()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, Lcom/chase/sig/android/service/movemoney/ListServiceResponse;->b()I

    move-result v2

    invoke-virtual {p1}, Lcom/chase/sig/android/service/movemoney/ListServiceResponse;->c()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/chase/sig/android/activity/l;->a(Ljava/util/List;II)V

    goto :goto_0
.end method

.method protected bridge synthetic a(Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 143
    check-cast p1, Lcom/chase/sig/android/service/movemoney/ListServiceResponse;

    invoke-virtual {p0, p1}, Lcom/chase/sig/android/activity/l$a;->a(Lcom/chase/sig/android/service/movemoney/ListServiceResponse;)V

    return-void
.end method
