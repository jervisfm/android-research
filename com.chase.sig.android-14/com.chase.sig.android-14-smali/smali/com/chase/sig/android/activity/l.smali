.class public abstract Lcom/chase/sig/android/activity/l;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$e;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/l$a;,
        Lcom/chase/sig/android/activity/l$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/chase/sig/android/domain/Transaction;",
        ">",
        "Lcom/chase/sig/android/activity/ai;",
        "Lcom/chase/sig/android/activity/fk$e;"
    }
.end annotation


# instance fields
.field protected a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation
.end field

.field protected b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/chase/sig/android/domain/LabeledValue;",
            ">;"
        }
    .end annotation
.end field

.field protected c:I

.field protected d:I

.field private k:Landroid/widget/ListView;

.field private l:Landroid/widget/TextView;

.field private m:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    .line 35
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/chase/sig/android/activity/l;->a:Ljava/util/List;

    .line 143
    return-void
.end method


# virtual methods
.method protected abstract a()I
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1
    .parameter

    .prologue
    .line 42
    const v0, 0x7f0300c8

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/l;->b(I)V

    .line 43
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/l;->a()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/l;->setTitle(I)V

    .line 45
    const v0, 0x7f0902d5

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/l;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/l;->k:Landroid/widget/ListView;

    .line 46
    const v0, 0x7f090005

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/l;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/chase/sig/android/activity/l;->l:Landroid/widget/TextView;

    .line 47
    return-void
.end method

.method protected a(Lcom/chase/sig/android/domain/Transaction;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 137
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/l;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/l;->c()Ljava/lang/Class;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 138
    const-string v1, "transaction_object"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 139
    const-string v1, "frequency_list"

    iget-object v2, p0, Lcom/chase/sig/android/activity/l;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 140
    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/l;->startActivity(Landroid/content/Intent;)V

    .line 141
    return-void
.end method

.method protected final a(Ljava/util/List;II)V
    .locals 5
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TT;>;II)V"
        }
    .end annotation

    .prologue
    const/16 v3, 0x8

    const/4 v1, 0x0

    .line 92
    iget-object v0, p0, Lcom/chase/sig/android/activity/l;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 94
    iput p2, p0, Lcom/chase/sig/android/activity/l;->c:I

    .line 95
    iput p3, p0, Lcom/chase/sig/android/activity/l;->d:I

    .line 97
    iget v0, p0, Lcom/chase/sig/android/activity/l;->d:I

    if-eq p2, v0, :cond_2

    .line 98
    iget-object v0, p0, Lcom/chase/sig/android/activity/l;->m:Landroid/view/View;

    if-nez v0, :cond_0

    .line 100
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/l;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "layout_inflater"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 103
    const v2, 0x7f030061

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/l;->m:Landroid/view/View;

    .line 104
    iget-object v0, p0, Lcom/chase/sig/android/activity/l;->k:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/chase/sig/android/activity/l;->m:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;)V

    .line 111
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/l;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    .line 113
    :goto_1
    iget-object v4, p0, Lcom/chase/sig/android/activity/l;->l:Landroid/widget/TextView;

    if-eqz v0, :cond_4

    move v2, v3

    :goto_2
    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 115
    iget-object v2, p0, Lcom/chase/sig/android/activity/l;->k:Landroid/widget/ListView;

    if-eqz v0, :cond_1

    move v3, v1

    :cond_1
    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setVisibility(I)V

    .line 116
    iget-object v0, p0, Lcom/chase/sig/android/activity/l;->k:Landroid/widget/ListView;

    new-instance v2, Lcom/chase/sig/android/activity/l$b;

    invoke-direct {v2, p0, v1}, Lcom/chase/sig/android/activity/l$b;-><init>(Lcom/chase/sig/android/activity/l;B)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 117
    iget-object v0, p0, Lcom/chase/sig/android/activity/l;->k:Landroid/widget/ListView;

    new-instance v2, Lcom/chase/sig/android/view/h;

    iget-object v3, p0, Lcom/chase/sig/android/activity/l;->a:Ljava/util/List;

    invoke-direct {v2, p0, v3}, Lcom/chase/sig/android/view/h;-><init>(Landroid/content/Context;Ljava/util/List;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 119
    iget-object v0, p0, Lcom/chase/sig/android/activity/l;->k:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/chase/sig/android/activity/l;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {v0, v2, v1}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    .line 120
    return-void

    .line 107
    :cond_2
    iget-object v0, p0, Lcom/chase/sig/android/activity/l;->m:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lcom/chase/sig/android/activity/l;->k:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/chase/sig/android/activity/l;->m:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->removeFooterView(Landroid/view/View;)Z

    goto :goto_0

    :cond_3
    move v0, v1

    .line 111
    goto :goto_1

    :cond_4
    move v2, v1

    .line 113
    goto :goto_2
.end method

.method protected abstract b()Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/chase/sig/android/activity/l$a",
            "<TT;>;>;"
        }
    .end annotation
.end method

.method protected final b(Landroid/os/Bundle;)V
    .locals 4
    .parameter

    .prologue
    const/4 v3, 0x1

    .line 50
    if-eqz p1, :cond_0

    const-string v0, "payment_items"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 51
    invoke-virtual {p0, p1}, Lcom/chase/sig/android/activity/l;->c(Landroid/os/Bundle;)V

    .line 55
    :goto_0
    return-void

    .line 53
    :cond_0
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/l;->b()Ljava/lang/Class;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/Integer;

    const/4 v2, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/l;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected abstract c()Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/chase/sig/android/activity/eb;",
            ">;"
        }
    .end annotation
.end method

.method protected final c(Landroid/os/Bundle;)V
    .locals 3
    .parameter

    .prologue
    .line 58
    iget-object v0, p0, Lcom/chase/sig/android/activity/l;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 59
    const-string v0, "payment_items"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 60
    iget-object v1, p0, Lcom/chase/sig/android/activity/l;->e:Lcom/chase/sig/android/activity/mc;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/l;->b()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/activity/mc;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/ae;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/activity/l$a;

    invoke-virtual {v1}, Lcom/chase/sig/android/activity/l$a;->d()Z

    move-result v1

    if-nez v1, :cond_0

    .line 61
    const-string v1, "payment_current_page"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    const-string v2, "payment_total_page"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/chase/sig/android/activity/l;->a(Ljava/util/List;II)V

    .line 65
    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 81
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ai;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 83
    const-string v1, "payment_items"

    iget-object v0, p0, Lcom/chase/sig/android/activity/l;->a:Ljava/util/List;

    check-cast v0, Ljava/io/Serializable;

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 84
    const-string v0, "payment_current_page"

    iget v1, p0, Lcom/chase/sig/android/activity/l;->c:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 85
    const-string v0, "payment_total_page"

    iget v1, p0, Lcom/chase/sig/android/activity/l;->d:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 86
    const-string v0, "frequency_list"

    iget-object v1, p0, Lcom/chase/sig/android/activity/l;->b:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 87
    return-void
.end method
