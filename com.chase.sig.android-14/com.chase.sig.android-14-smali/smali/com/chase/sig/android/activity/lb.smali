.class final Lcom/chase/sig/android/activity/lb;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:[Ljava/lang/String;

.field final synthetic b:Lcom/chase/sig/android/activity/ReceiptsListActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/ReceiptsListActivity;[Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 609
    iput-object p1, p0, Lcom/chase/sig/android/activity/lb;->b:Lcom/chase/sig/android/activity/ReceiptsListActivity;

    iput-object p2, p0, Lcom/chase/sig/android/activity/lb;->a:[Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 613
    iget-object v0, p0, Lcom/chase/sig/android/activity/lb;->b:Lcom/chase/sig/android/activity/ReceiptsListActivity;

    invoke-static {v0, v4}, Lcom/chase/sig/android/activity/ReceiptsListActivity;->a(Lcom/chase/sig/android/activity/ReceiptsListActivity;Z)Z

    .line 614
    iget-object v0, p0, Lcom/chase/sig/android/activity/lb;->b:Lcom/chase/sig/android/activity/ReceiptsListActivity;

    iget-object v1, p0, Lcom/chase/sig/android/activity/lb;->a:[Ljava/lang/String;

    invoke-static {v0, p2, v1}, Lcom/chase/sig/android/activity/ReceiptsListActivity;->a(Lcom/chase/sig/android/activity/ReceiptsListActivity;I[Ljava/lang/String;)V

    .line 615
    const-string v0, "ALL"

    iget-object v1, p0, Lcom/chase/sig/android/activity/lb;->b:Lcom/chase/sig/android/activity/ReceiptsListActivity;

    invoke-static {v1}, Lcom/chase/sig/android/activity/ReceiptsListActivity;->g(Lcom/chase/sig/android/activity/ReceiptsListActivity;)Lcom/chase/sig/android/domain/receipt/ReceiptFilterList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/receipt/ReceiptFilterList;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 616
    iget-object v0, p0, Lcom/chase/sig/android/activity/lb;->b:Lcom/chase/sig/android/activity/ReceiptsListActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/ReceiptsListActivity;->c(Lcom/chase/sig/android/activity/ReceiptsListActivity;)Lcom/chase/sig/android/service/ReceiptFilter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/service/ReceiptFilter;->b()V

    .line 617
    iget-object v0, p0, Lcom/chase/sig/android/activity/lb;->b:Lcom/chase/sig/android/activity/ReceiptsListActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/ReceiptsListActivity;->c(Lcom/chase/sig/android/activity/ReceiptsListActivity;)Lcom/chase/sig/android/service/ReceiptFilter;

    move-result-object v0

    const-string v1, "ALL"

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/ReceiptFilter;->a(Ljava/lang/String;)V

    .line 618
    iget-object v0, p0, Lcom/chase/sig/android/activity/lb;->b:Lcom/chase/sig/android/activity/ReceiptsListActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/ReceiptsListActivity;->d(Lcom/chase/sig/android/activity/ReceiptsListActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 619
    iget-object v0, p0, Lcom/chase/sig/android/activity/lb;->b:Lcom/chase/sig/android/activity/ReceiptsListActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/ReceiptsListActivity;->d(Lcom/chase/sig/android/activity/ReceiptsListActivity;)Ljava/util/ArrayList;

    move-result-object v0

    const-string v1, "No filter"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 620
    iget-object v0, p0, Lcom/chase/sig/android/activity/lb;->b:Lcom/chase/sig/android/activity/ReceiptsListActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/ReceiptsListActivity;->f(Lcom/chase/sig/android/activity/ReceiptsListActivity;)V

    .line 621
    iget-object v0, p0, Lcom/chase/sig/android/activity/lb;->b:Lcom/chase/sig/android/activity/ReceiptsListActivity;

    const-class v1, Lcom/chase/sig/android/activity/ReceiptsListActivity$b;

    new-array v2, v2, [Lcom/chase/sig/android/service/ReceiptFilter;

    iget-object v3, p0, Lcom/chase/sig/android/activity/lb;->b:Lcom/chase/sig/android/activity/ReceiptsListActivity;

    invoke-static {v3}, Lcom/chase/sig/android/activity/ReceiptsListActivity;->c(Lcom/chase/sig/android/activity/ReceiptsListActivity;)Lcom/chase/sig/android/service/ReceiptFilter;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/chase/sig/android/activity/ReceiptsListActivity;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 622
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 623
    iget-object v0, p0, Lcom/chase/sig/android/activity/lb;->b:Lcom/chase/sig/android/activity/ReceiptsListActivity;

    invoke-virtual {v0, v4}, Lcom/chase/sig/android/activity/ReceiptsListActivity;->removeDialog(I)V

    .line 629
    :goto_0
    return-void

    .line 625
    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/lb;->b:Lcom/chase/sig/android/activity/ReceiptsListActivity;

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/activity/ReceiptsListActivity;->showDialog(I)V

    .line 626
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 627
    iget-object v0, p0, Lcom/chase/sig/android/activity/lb;->b:Lcom/chase/sig/android/activity/ReceiptsListActivity;

    invoke-virtual {v0, v4}, Lcom/chase/sig/android/activity/ReceiptsListActivity;->removeDialog(I)V

    goto :goto_0
.end method
