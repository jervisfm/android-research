.class final Lcom/chase/sig/android/activity/le;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:I

.field final synthetic b:Ljava/util/List;

.field final synthetic c:Lcom/chase/sig/android/activity/ReceiptsListActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/ReceiptsListActivity;ILjava/util/List;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 183
    iput-object p1, p0, Lcom/chase/sig/android/activity/le;->c:Lcom/chase/sig/android/activity/ReceiptsListActivity;

    iput p2, p0, Lcom/chase/sig/android/activity/le;->a:I

    iput-object p3, p0, Lcom/chase/sig/android/activity/le;->b:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .parameter
    .parameter

    .prologue
    const v1, 0x7f0900f3

    const/4 v4, 0x3

    const/4 v3, 0x0

    .line 187
    iget v0, p0, Lcom/chase/sig/android/activity/le;->a:I

    if-ne p2, v0, :cond_0

    .line 188
    iget-object v0, p0, Lcom/chase/sig/android/activity/le;->c:Lcom/chase/sig/android/activity/ReceiptsListActivity;

    invoke-virtual {v0, v4}, Lcom/chase/sig/android/activity/ReceiptsListActivity;->showDialog(I)V

    .line 200
    :goto_0
    return-void

    .line 189
    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/le;->b:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/LabeledValue;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->n(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 190
    iget-object v0, p0, Lcom/chase/sig/android/activity/le;->c:Lcom/chase/sig/android/activity/ReceiptsListActivity;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/ReceiptsListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const-string v1, "Dates: "

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 191
    iget-object v0, p0, Lcom/chase/sig/android/activity/le;->c:Lcom/chase/sig/android/activity/ReceiptsListActivity;

    invoke-static {v0, v3, v3, v3}, Lcom/chase/sig/android/activity/ReceiptsListActivity;->a(Lcom/chase/sig/android/activity/ReceiptsListActivity;Ljava/lang/String;Ljava/util/Date;Ljava/util/Date;)V

    .line 192
    iget-object v0, p0, Lcom/chase/sig/android/activity/le;->c:Lcom/chase/sig/android/activity/ReceiptsListActivity;

    invoke-virtual {v0, v4}, Lcom/chase/sig/android/activity/ReceiptsListActivity;->removeDialog(I)V

    goto :goto_0

    .line 194
    :cond_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/le;->c:Lcom/chase/sig/android/activity/ReceiptsListActivity;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/ReceiptsListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v1, "Dates: "

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/chase/sig/android/activity/le;->b:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/domain/LabeledValue;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/LabeledValue;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 196
    iget-object v1, p0, Lcom/chase/sig/android/activity/le;->c:Lcom/chase/sig/android/activity/ReceiptsListActivity;

    iget-object v0, p0, Lcom/chase/sig/android/activity/le;->b:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/LabeledValue;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0, v3, v3}, Lcom/chase/sig/android/activity/ReceiptsListActivity;->a(Lcom/chase/sig/android/activity/ReceiptsListActivity;Ljava/lang/String;Ljava/util/Date;Ljava/util/Date;)V

    .line 197
    iget-object v0, p0, Lcom/chase/sig/android/activity/le;->c:Lcom/chase/sig/android/activity/ReceiptsListActivity;

    invoke-virtual {v0, v4}, Lcom/chase/sig/android/activity/ReceiptsListActivity;->removeDialog(I)V

    goto :goto_0
.end method
