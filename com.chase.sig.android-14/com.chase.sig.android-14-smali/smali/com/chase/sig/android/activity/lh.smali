.class final Lcom/chase/sig/android/activity/lh;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Landroid/widget/Button;

.field final synthetic b:Lcom/chase/sig/android/activity/ReceiptsListActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/ReceiptsListActivity;Landroid/widget/Button;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 359
    iput-object p1, p0, Lcom/chase/sig/android/activity/lh;->b:Lcom/chase/sig/android/activity/ReceiptsListActivity;

    iput-object p2, p0, Lcom/chase/sig/android/activity/lh;->a:Landroid/widget/Button;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 8
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 364
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v6

    .line 365
    const/4 v0, 0x5

    invoke-virtual {v6, v0, v1}, Ljava/util/Calendar;->add(II)V

    .line 367
    new-instance v2, Lcom/chase/sig/android/activity/li;

    invoke-direct {v2, p0}, Lcom/chase/sig/android/activity/li;-><init>(Lcom/chase/sig/android/activity/lh;)V

    .line 383
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v5

    .line 384
    const/4 v0, -0x2

    invoke-virtual {v5, v1, v0}, Ljava/util/Calendar;->add(II)V

    .line 386
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    .line 387
    iget-object v1, p0, Lcom/chase/sig/android/activity/lh;->a:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getId()I

    move-result v1

    const v4, 0x7f090095

    if-ne v1, v4, :cond_0

    .line 388
    iget-object v1, p0, Lcom/chase/sig/android/activity/lh;->b:Lcom/chase/sig/android/activity/ReceiptsListActivity;

    invoke-static {v1}, Lcom/chase/sig/android/activity/ReceiptsListActivity;->a(Lcom/chase/sig/android/activity/ReceiptsListActivity;)Ljava/util/Date;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 389
    iget-object v0, p0, Lcom/chase/sig/android/activity/lh;->b:Lcom/chase/sig/android/activity/ReceiptsListActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/ReceiptsListActivity;->a(Lcom/chase/sig/android/activity/ReceiptsListActivity;)Ljava/util/Date;

    move-result-object v0

    .line 393
    :cond_0
    iget-object v1, p0, Lcom/chase/sig/android/activity/lh;->a:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getId()I

    move-result v1

    const v4, 0x7f090096

    if-ne v1, v4, :cond_1

    .line 394
    iget-object v1, p0, Lcom/chase/sig/android/activity/lh;->b:Lcom/chase/sig/android/activity/ReceiptsListActivity;

    invoke-static {v1}, Lcom/chase/sig/android/activity/ReceiptsListActivity;->b(Lcom/chase/sig/android/activity/ReceiptsListActivity;)Ljava/util/Date;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 395
    iget-object v0, p0, Lcom/chase/sig/android/activity/lh;->b:Lcom/chase/sig/android/activity/ReceiptsListActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/ReceiptsListActivity;->b(Lcom/chase/sig/android/activity/ReceiptsListActivity;)Ljava/util/Date;

    move-result-object v0

    move-object v7, v0

    .line 399
    :goto_0
    new-instance v0, Lcom/chase/sig/android/view/b;

    iget-object v1, p0, Lcom/chase/sig/android/activity/lh;->b:Lcom/chase/sig/android/activity/ReceiptsListActivity;

    move v4, v3

    invoke-direct/range {v0 .. v6}, Lcom/chase/sig/android/view/b;-><init>(Landroid/content/Context;Lcom/chase/sig/android/view/b$a;ZZLjava/util/Calendar;Ljava/util/Calendar;)V

    invoke-virtual {v0, v7}, Lcom/chase/sig/android/view/b;->a(Ljava/util/Date;)Lcom/chase/sig/android/view/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/view/b;->a()Lcom/chase/sig/android/view/b;

    move-result-object v0

    .line 403
    invoke-virtual {v0}, Lcom/chase/sig/android/view/b;->show()V

    .line 404
    return-void

    :cond_1
    move-object v7, v0

    goto :goto_0
.end method
