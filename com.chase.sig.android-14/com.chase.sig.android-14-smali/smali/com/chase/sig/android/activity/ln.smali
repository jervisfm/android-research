.class final Lcom/chase/sig/android/activity/ln;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Landroid/os/Bundle;

.field final synthetic b:Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;Landroid/os/Bundle;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 71
    iput-object p1, p0, Lcom/chase/sig/android/activity/ln;->b:Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;

    iput-object p2, p0, Lcom/chase/sig/android/activity/ln;->a:Landroid/os/Bundle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 4
    .parameter

    .prologue
    .line 75
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/chase/sig/android/activity/ln;->b:Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;

    const-class v2, Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 78
    const-string v1, "receipt_photo_list"

    iget-object v2, p0, Lcom/chase/sig/android/activity/ln;->b:Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;

    invoke-virtual {v2}, Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-static {v2}, Lcom/chase/sig/android/domain/ReceiptPhotoList;->a(Landroid/content/Intent;)Lcom/chase/sig/android/domain/ReceiptPhotoList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 81
    iget-object v1, p0, Lcom/chase/sig/android/activity/ln;->a:Landroid/os/Bundle;

    if-eqz v1, :cond_0

    .line 82
    iget-object v1, p0, Lcom/chase/sig/android/activity/ln;->a:Landroid/os/Bundle;

    const-string v2, "receipt_curr_photo_number"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 83
    const-string v1, "receipt_curr_photo_number"

    iget-object v2, p0, Lcom/chase/sig/android/activity/ln;->a:Landroid/os/Bundle;

    const-string v3, "receipt_curr_photo_number"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 87
    :cond_0
    iget-object v1, p0, Lcom/chase/sig/android/activity/ln;->b:Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;

    invoke-static {v1, v0}, Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;->a(Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;Landroid/content/Intent;)V

    .line 89
    const/high16 v1, 0x4000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 90
    iget-object v1, p0, Lcom/chase/sig/android/activity/ln;->b:Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;->startActivity(Landroid/content/Intent;)V

    .line 91
    return-void
.end method
