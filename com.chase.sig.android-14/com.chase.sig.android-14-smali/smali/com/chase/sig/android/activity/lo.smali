.class final Lcom/chase/sig/android/activity/lo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Landroid/os/Bundle;

.field final synthetic b:Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;Landroid/os/Bundle;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 105
    iput-object p1, p0, Lcom/chase/sig/android/activity/lo;->b:Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;

    iput-object p2, p0, Lcom/chase/sig/android/activity/lo;->a:Landroid/os/Bundle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 5
    .parameter

    .prologue
    .line 109
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    new-instance v1, Landroid/content/Intent;

    iget-object v0, p0, Lcom/chase/sig/android/activity/lo;->b:Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;

    const-class v2, Lcom/chase/sig/android/activity/ReceiptsCameraCaptureActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 113
    iget-object v0, p0, Lcom/chase/sig/android/activity/lo;->b:Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/domain/ReceiptPhotoList;->a(Landroid/content/Intent;)Lcom/chase/sig/android/domain/ReceiptPhotoList;

    move-result-object v2

    .line 115
    const/4 v0, 0x1

    .line 116
    iget-object v3, p0, Lcom/chase/sig/android/activity/lo;->a:Landroid/os/Bundle;

    if-eqz v3, :cond_1

    .line 117
    iget-object v3, p0, Lcom/chase/sig/android/activity/lo;->a:Landroid/os/Bundle;

    const-string v4, "receipt_curr_photo_number"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 118
    iget-object v0, p0, Lcom/chase/sig/android/activity/lo;->a:Landroid/os/Bundle;

    const-string v3, "receipt_curr_photo_number"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 121
    :cond_0
    iget-object v3, p0, Lcom/chase/sig/android/activity/lo;->a:Landroid/os/Bundle;

    const-string v4, "image_data"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 122
    iget-object v3, p0, Lcom/chase/sig/android/activity/lo;->a:Landroid/os/Bundle;

    const-string v4, "image_data"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/chase/sig/android/domain/ReceiptPhotoList;->a([B)V

    .line 125
    :cond_1
    iget-object v3, p0, Lcom/chase/sig/android/activity/lo;->b:Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;

    invoke-static {v3, v1}, Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;->a(Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;Landroid/content/Intent;)V

    .line 127
    const-string v3, "receipt_photo_list"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 128
    const-string v2, "receipt_curr_photo_number"

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 129
    const/high16 v0, 0x4000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 130
    iget-object v0, p0, Lcom/chase/sig/android/activity/lo;->b:Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;->startActivity(Landroid/content/Intent;)V

    .line 131
    return-void
.end method
