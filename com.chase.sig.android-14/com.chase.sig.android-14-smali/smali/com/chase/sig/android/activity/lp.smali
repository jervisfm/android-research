.class final Lcom/chase/sig/android/activity/lp;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Landroid/os/Bundle;

.field final synthetic b:Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;Landroid/os/Bundle;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 139
    iput-object p1, p0, Lcom/chase/sig/android/activity/lp;->b:Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;

    iput-object p2, p0, Lcom/chase/sig/android/activity/lp;->a:Landroid/os/Bundle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 4
    .parameter

    .prologue
    .line 143
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/chase/sig/android/activity/lp;->b:Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;

    const-class v2, Lcom/chase/sig/android/activity/ReceiptsEnterDetailsActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 146
    iget-object v1, p0, Lcom/chase/sig/android/activity/lp;->b:Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;

    invoke-virtual {v1}, Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/domain/ReceiptPhotoList;->a(Landroid/content/Intent;)Lcom/chase/sig/android/domain/ReceiptPhotoList;

    move-result-object v1

    .line 148
    iget-object v2, p0, Lcom/chase/sig/android/activity/lp;->a:Landroid/os/Bundle;

    if-eqz v2, :cond_0

    .line 149
    iget-object v2, p0, Lcom/chase/sig/android/activity/lp;->a:Landroid/os/Bundle;

    const-string v3, "image_data"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 150
    iget-object v2, p0, Lcom/chase/sig/android/activity/lp;->a:Landroid/os/Bundle;

    const-string v3, "image_data"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/domain/ReceiptPhotoList;->a([B)V

    .line 153
    :cond_0
    iget-object v2, p0, Lcom/chase/sig/android/activity/lp;->b:Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;

    invoke-static {v2, v0}, Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;->a(Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;Landroid/content/Intent;)V

    .line 155
    const-string v2, "receipt_photo_list"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 156
    const-string v1, "selectedAccountId"

    iget-object v2, p0, Lcom/chase/sig/android/activity/lp;->a:Landroid/os/Bundle;

    const-string v3, "selectedAccountId"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 158
    iget-object v1, p0, Lcom/chase/sig/android/activity/lp;->b:Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/activity/ReceiptsReviewImageActivity;->startActivity(Landroid/content/Intent;)V

    .line 159
    return-void
.end method
