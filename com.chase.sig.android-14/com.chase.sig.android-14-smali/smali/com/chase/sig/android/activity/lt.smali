.class final Lcom/chase/sig/android/activity/lt;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 100
    iput-object p1, p0, Lcom/chase/sig/android/activity/lt;->a:Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3
    .parameter

    .prologue
    .line 107
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/lt;->a:Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;->a(Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 109
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/chase/sig/android/activity/lt;->a:Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;

    const-class v2, Lcom/chase/sig/android/activity/ReceiptsSelectPlanActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 110
    const-string v1, "receipts_settings_mode"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 116
    :goto_0
    const/high16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 117
    iget-object v1, p0, Lcom/chase/sig/android/activity/lt;->a:Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;->startActivity(Landroid/content/Intent;)V

    .line 118
    return-void

    .line 113
    :cond_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/chase/sig/android/activity/lt;->a:Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;

    const-class v2, Lcom/chase/sig/android/activity/ReceiptsEnrollActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_0
.end method
