.class final Lcom/chase/sig/android/activity/lu;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 128
    iput-object p1, p0, Lcom/chase/sig/android/activity/lu;->a:Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3
    .parameter

    .prologue
    .line 133
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/lu;->a:Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;->b(Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;)Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v0

    .line 134
    iget-object v1, p0, Lcom/chase/sig/android/activity/lu;->a:Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;->a(Lcom/chase/sig/android/view/JPSpinner;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/lu;->a:Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;->c(Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;)Lcom/chase/sig/android/domain/ReceiptsPricingPlan;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/ReceiptsPricingPlan;->d()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CRPLAN0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 136
    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/lu;->a:Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;

    iget-object v0, v0, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;->e:Lcom/chase/sig/android/activity/mc;

    const-class v1, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity$a;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/mc;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/ae;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity$a;

    .line 137
    invoke-virtual {v0}, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity$a;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v1

    sget-object v2, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-eq v1, v2, :cond_1

    .line 138
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 141
    :cond_1
    return-void
.end method
