.class final Lcom/chase/sig/android/activity/lv;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 148
    iput-object p1, p0, Lcom/chase/sig/android/activity/lv;->a:Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 4
    .parameter

    .prologue
    .line 153
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/lv;->a:Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;->b(Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;)Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v0

    .line 154
    iget-object v1, p0, Lcom/chase/sig/android/activity/lv;->a:Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;->a(Lcom/chase/sig/android/view/JPSpinner;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 157
    invoke-virtual {v0}, Lcom/chase/sig/android/view/JPSpinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 158
    if-nez v0, :cond_1

    .line 167
    :cond_0
    :goto_0
    return-void

    .line 161
    :cond_1
    const-string v1, "accountId"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 162
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/chase/sig/android/activity/lv;->a:Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;

    const-class v3, Lcom/chase/sig/android/activity/ReceiptsLegalAgreementActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 163
    const-string v2, "plan_code_id"

    iget-object v3, p0, Lcom/chase/sig/android/activity/lv;->a:Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;

    invoke-static {v3}, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;->c(Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;)Lcom/chase/sig/android/domain/ReceiptsPricingPlan;

    move-result-object v3

    invoke-virtual {v3}, Lcom/chase/sig/android/domain/ReceiptsPricingPlan;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 164
    const-string v2, "funding_acct_id"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 165
    iget-object v0, p0, Lcom/chase/sig/android/activity/lv;->a:Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/ReceiptsSettingsActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
