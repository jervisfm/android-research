.class final Lcom/chase/sig/android/activity/lz;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 123
    iput-object p1, p0, Lcom/chase/sig/android/activity/lz;->a:Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 5
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 128
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/lz;->a:Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;->c(Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getCheckedItemPosition()I

    move-result v0

    .line 130
    if-gez v0, :cond_0

    .line 131
    iget-object v0, p0, Lcom/chase/sig/android/activity/lz;->a:Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;

    const v1, 0x7f070275

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;->g(I)V

    .line 144
    :goto_0
    return-void

    .line 133
    :cond_0
    iget-object v1, p0, Lcom/chase/sig/android/activity/lz;->a:Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;

    invoke-static {v1}, Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;->c(Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/TransactionForReceipt;

    .line 136
    iget-object v1, p0, Lcom/chase/sig/android/activity/lz;->a:Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;

    invoke-static {v1}, Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;->b(Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;)Lcom/chase/sig/android/domain/Receipt;

    move-result-object v1

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/TransactionForReceipt;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/Receipt;->d(Ljava/lang/String;)V

    .line 138
    iget-object v0, p0, Lcom/chase/sig/android/activity/lz;->a:Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;->a(Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 139
    iget-object v0, p0, Lcom/chase/sig/android/activity/lz;->a:Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;

    const-class v1, Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity$c;

    new-array v2, v2, [Lcom/chase/sig/android/domain/Receipt;

    iget-object v3, p0, Lcom/chase/sig/android/activity/lz;->a:Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;

    invoke-static {v3}, Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;->b(Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;)Lcom/chase/sig/android/domain/Receipt;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    goto :goto_0

    .line 141
    :cond_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/lz;->a:Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;

    const-class v1, Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity$a;

    new-array v2, v2, [Lcom/chase/sig/android/domain/Receipt;

    iget-object v3, p0, Lcom/chase/sig/android/activity/lz;->a:Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;

    invoke-static {v3}, Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;->b(Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;)Lcom/chase/sig/android/domain/Receipt;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/chase/sig/android/activity/ReceiptsTransactionMatchingActivity;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    goto :goto_0
.end method
