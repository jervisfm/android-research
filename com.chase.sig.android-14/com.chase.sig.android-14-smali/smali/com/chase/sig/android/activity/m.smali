.class public abstract Lcom/chase/sig/android/activity/m;
.super Lcom/chase/sig/android/activity/n;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/chase/sig/android/domain/Transaction;",
        ">",
        "Lcom/chase/sig/android/activity/n",
        "<TT;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/chase/sig/android/activity/n;-><init>()V

    return-void
.end method


# virtual methods
.method protected final a(ILjava/lang/Class;)V
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/Class",
            "<+",
            "Lcom/chase/sig/android/activity/eb;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 18
    const v0, 0x7f090154

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/m;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 19
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 20
    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(I)V

    .line 21
    invoke-virtual {p0, p2}, Lcom/chase/sig/android/activity/m;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/a/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 22
    return-void
.end method

.method protected abstract b()Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/chase/sig/android/activity/eb;",
            ">;"
        }
    .end annotation
.end method

.method protected final b(ILjava/lang/Class;)V
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/Class",
            "<+",
            "Lcom/chase/sig/android/activity/eb;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 26
    const v0, 0x7f090155

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/m;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 27
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 28
    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(I)V

    .line 29
    invoke-virtual {p0, p2}, Lcom/chase/sig/android/activity/m;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/a/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 30
    return-void
.end method

.method protected final b(Z)V
    .locals 3
    .parameter

    .prologue
    .line 45
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/m;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "alert_type"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 46
    if-eqz p1, :cond_0

    if-eqz v0, :cond_0

    .line 47
    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/m;->showDialog(I)V

    .line 49
    :cond_0
    return-void
.end method

.method protected final c()Z
    .locals 3

    .prologue
    .line 52
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/m;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "hideFrequency"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 33
    invoke-virtual {p0, p1}, Lcom/chase/sig/android/activity/m;->j(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 34
    const/4 v0, 0x1

    .line 39
    :goto_0
    return v0

    .line 37
    :cond_0
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/m;->b()Ljava/lang/Class;

    move-result-object v0

    .line 38
    invoke-static {p1, p0, v0}, Lcom/chase/sig/android/a/a;->a(ILcom/chase/sig/android/activity/eb;Ljava/lang/Class;)V

    .line 39
    invoke-super {p0, p1, p2}, Lcom/chase/sig/android/activity/n;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method
