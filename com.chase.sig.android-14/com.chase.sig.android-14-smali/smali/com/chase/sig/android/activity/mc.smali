.class public final Lcom/chase/sig/android/activity/mc;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Class",
            "<*>;",
            "Lcom/chase/sig/android/activity/ae;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lcom/chase/sig/android/activity/eb;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/chase/sig/android/activity/mc;->a:Ljava/util/Hashtable;

    .line 17
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/chase/sig/android/activity/mc;->b:Lcom/chase/sig/android/activity/eb;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/ae;
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/chase/sig/android/activity/ae;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 34
    iget-object v0, p0, Lcom/chase/sig/android/activity/mc;->a:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/chase/sig/android/activity/mc;->a:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/activity/ae;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/ae;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/chase/sig/android/activity/mc;->a:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/activity/ae;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/ae;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_0

    .line 36
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/activity/ae;

    iget-object v1, p0, Lcom/chase/sig/android/activity/mc;->b:Lcom/chase/sig/android/activity/eb;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/ae;->a(Lcom/chase/sig/android/activity/eb;)V

    invoke-virtual {v0, p0}, Lcom/chase/sig/android/activity/ae;->a(Lcom/chase/sig/android/activity/mc;)V

    iget-object v1, p0, Lcom/chase/sig/android/activity/mc;->a:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 38
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/mc;->a:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/activity/ae;

    return-object v0

    .line 34
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method public final a()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/chase/sig/android/activity/ae;",
            ">()",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 65
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 66
    iget-object v0, p0, Lcom/chase/sig/android/activity/mc;->a:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/activity/ae;

    .line 67
    invoke-virtual {v0}, Lcom/chase/sig/android/activity/ae;->d()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 68
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 72
    :cond_1
    return-object v1
.end method

.method public final a(Lcom/chase/sig/android/activity/eb;)V
    .locals 3
    .parameter

    .prologue
    .line 20
    iput-object p1, p0, Lcom/chase/sig/android/activity/mc;->b:Lcom/chase/sig/android/activity/eb;

    .line 21
    iget-object v0, p0, Lcom/chase/sig/android/activity/mc;->a:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/activity/ae;

    .line 22
    iget-object v2, p0, Lcom/chase/sig/android/activity/mc;->b:Lcom/chase/sig/android/activity/eb;

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/activity/ae;->a(Lcom/chase/sig/android/activity/eb;)V

    goto :goto_0

    .line 24
    :cond_0
    return-void
.end method
