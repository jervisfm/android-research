.class final Lcom/chase/sig/android/activity/md;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Ljava/util/List;

.field final synthetic b:Lcom/chase/sig/android/activity/TeamInfoActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/TeamInfoActivity;Ljava/util/List;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 131
    iput-object p1, p0, Lcom/chase/sig/android/activity/md;->b:Lcom/chase/sig/android/activity/TeamInfoActivity;

    iput-object p2, p0, Lcom/chase/sig/android/activity/md;->a:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 135
    :try_start_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/md;->b:Lcom/chase/sig/android/activity/TeamInfoActivity;

    new-instance v1, Lcom/chase/sig/android/domain/Advisor;

    invoke-direct {v1}, Lcom/chase/sig/android/domain/Advisor;-><init>()V

    invoke-static {v0, v1}, Lcom/chase/sig/android/activity/TeamInfoActivity;->a(Lcom/chase/sig/android/activity/TeamInfoActivity;Lcom/chase/sig/android/domain/Advisor;)Lcom/chase/sig/android/domain/Advisor;

    .line 136
    iget-object v1, p0, Lcom/chase/sig/android/activity/md;->b:Lcom/chase/sig/android/activity/TeamInfoActivity;

    iget-object v0, p0, Lcom/chase/sig/android/activity/md;->a:Ljava/util/List;

    invoke-virtual {p1, p2}, Landroid/widget/AdapterView;->getPositionForView(Landroid/view/View;)I

    move-result v2

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/Advisor;

    invoke-static {v1, v0}, Lcom/chase/sig/android/activity/TeamInfoActivity;->a(Lcom/chase/sig/android/activity/TeamInfoActivity;Lcom/chase/sig/android/domain/Advisor;)Lcom/chase/sig/android/domain/Advisor;

    .line 137
    iget-object v0, p0, Lcom/chase/sig/android/activity/md;->b:Lcom/chase/sig/android/activity/TeamInfoActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/TeamInfoActivity;->showDialog(I)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 138
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1, p2, p3}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/widget/AdapterView;Landroid/view/View;I)V

    return-void

    :catch_0
    move-exception v0

    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1, p2, p3}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/widget/AdapterView;Landroid/view/View;I)V

    throw v0
.end method
