.class final Lcom/chase/sig/android/activity/mf;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/TeamInfoActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/TeamInfoActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 191
    iput-object p1, p0, Lcom/chase/sig/android/activity/mf;->a:Lcom/chase/sig/android/activity/TeamInfoActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x0

    .line 195
    iget-object v0, p0, Lcom/chase/sig/android/activity/mf;->a:Lcom/chase/sig/android/activity/TeamInfoActivity;

    invoke-virtual {v0, v4}, Lcom/chase/sig/android/activity/TeamInfoActivity;->dismissDialog(I)V

    .line 196
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SEND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 198
    const-string v1, "plain/text"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 199
    const-string v1, "android.intent.extra.EMAIL"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    iget-object v3, p0, Lcom/chase/sig/android/activity/mf;->a:Lcom/chase/sig/android/activity/TeamInfoActivity;

    invoke-static {v3}, Lcom/chase/sig/android/activity/TeamInfoActivity;->a(Lcom/chase/sig/android/activity/TeamInfoActivity;)Lcom/chase/sig/android/domain/Advisor;

    move-result-object v3

    invoke-virtual {v3}, Lcom/chase/sig/android/domain/Advisor;->b()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 203
    const-string v1, "android.intent.extra.SUBJECT"

    const-string v2, "Subject"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 204
    const-string v1, "android.intent.extra.TEXT"

    const-string v2, "Text"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 206
    iget-object v1, p0, Lcom/chase/sig/android/activity/mf;->a:Lcom/chase/sig/android/activity/TeamInfoActivity;

    const-string v2, "Send mail..."

    invoke-static {v0, v2}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/activity/TeamInfoActivity;->startActivity(Landroid/content/Intent;)V

    .line 207
    return-void
.end method
