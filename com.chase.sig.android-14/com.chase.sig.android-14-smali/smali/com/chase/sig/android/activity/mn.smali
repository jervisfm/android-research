.class final Lcom/chase/sig/android/activity/mn;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/TransferStartActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/TransferStartActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 131
    iput-object p1, p0, Lcom/chase/sig/android/activity/mn;->a:Lcom/chase/sig/android/activity/TransferStartActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 5
    .parameter

    .prologue
    .line 134
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/mn;->a:Lcom/chase/sig/android/activity/TransferStartActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/TransferStartActivity;->a(Lcom/chase/sig/android/activity/TransferStartActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/chase/sig/android/activity/mn;->a:Lcom/chase/sig/android/activity/TransferStartActivity;

    const-string v1, "PAY_TO"

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/TransferStartActivity;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/q;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/q;->q()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v1

    .line 138
    iget-object v0, p0, Lcom/chase/sig/android/activity/mn;->a:Lcom/chase/sig/android/activity/TransferStartActivity;

    const-string v2, "PAY_FROM"

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/activity/TransferStartActivity;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/q;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/q;->q()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v2

    .line 141
    new-instance v3, Lcom/chase/sig/android/domain/AccountTransfer;

    invoke-direct {v3}, Lcom/chase/sig/android/domain/AccountTransfer;-><init>()V

    .line 143
    iget-object v0, p0, Lcom/chase/sig/android/activity/mn;->a:Lcom/chase/sig/android/activity/TransferStartActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/TransferStartActivity;->b(Lcom/chase/sig/android/activity/TransferStartActivity;)Lcom/chase/sig/android/util/Dollar;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/chase/sig/android/domain/AccountTransfer;->a(Lcom/chase/sig/android/util/Dollar;)V

    .line 145
    iget-object v0, p0, Lcom/chase/sig/android/activity/mn;->a:Lcom/chase/sig/android/activity/TransferStartActivity;

    const-string v4, "DATE"

    invoke-virtual {v0, v4}, Lcom/chase/sig/android/activity/TransferStartActivity;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/d;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/d;->p()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->t(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/chase/sig/android/domain/AccountTransfer;->a(Ljava/util/Date;)V

    .line 147
    iget-object v0, p0, Lcom/chase/sig/android/activity/mn;->a:Lcom/chase/sig/android/activity/TransferStartActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/TransferStartActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v0

    const-string v4, "MEMO"

    invoke-virtual {v0, v4}, Lcom/chase/sig/android/view/detail/DetailView;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/chase/sig/android/activity/mn;->a:Lcom/chase/sig/android/activity/TransferStartActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/TransferStartActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v0

    const-string v4, "MEMO"

    invoke-virtual {v0, v4}, Lcom/chase/sig/android/view/detail/DetailView;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v3, v0}, Lcom/chase/sig/android/domain/AccountTransfer;->l(Ljava/lang/String;)V

    .line 149
    iget-object v0, p0, Lcom/chase/sig/android/activity/mn;->a:Lcom/chase/sig/android/activity/TransferStartActivity;

    iget-object v0, p0, Lcom/chase/sig/android/activity/mn;->a:Lcom/chase/sig/android/activity/TransferStartActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/TransferStartActivity;->c(Lcom/chase/sig/android/activity/TransferStartActivity;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/chase/sig/android/activity/TransferStartActivity;->a(Ljava/util/List;Lcom/chase/sig/android/view/JPSpinner;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/chase/sig/android/domain/AccountTransfer;->b(Ljava/lang/String;)V

    .line 150
    iget-object v0, p0, Lcom/chase/sig/android/activity/mn;->a:Lcom/chase/sig/android/activity/TransferStartActivity;

    iget-object v0, p0, Lcom/chase/sig/android/activity/mn;->a:Lcom/chase/sig/android/activity/TransferStartActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/TransferStartActivity;->c(Lcom/chase/sig/android/activity/TransferStartActivity;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/chase/sig/android/activity/TransferStartActivity;->b(Ljava/util/List;Lcom/chase/sig/android/view/JPSpinner;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/chase/sig/android/domain/AccountTransfer;->p(Ljava/lang/String;)V

    .line 153
    iget-object v0, p0, Lcom/chase/sig/android/activity/mn;->a:Lcom/chase/sig/android/activity/TransferStartActivity;

    iget-object v0, p0, Lcom/chase/sig/android/activity/mn;->a:Lcom/chase/sig/android/activity/TransferStartActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/TransferStartActivity;->d(Lcom/chase/sig/android/activity/TransferStartActivity;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/chase/sig/android/activity/TransferStartActivity;->a(Ljava/util/List;Lcom/chase/sig/android/view/JPSpinner;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/chase/sig/android/domain/AccountTransfer;->a(Ljava/lang/String;)V

    .line 154
    iget-object v0, p0, Lcom/chase/sig/android/activity/mn;->a:Lcom/chase/sig/android/activity/TransferStartActivity;

    iget-object v0, p0, Lcom/chase/sig/android/activity/mn;->a:Lcom/chase/sig/android/activity/TransferStartActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/TransferStartActivity;->d(Lcom/chase/sig/android/activity/TransferStartActivity;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/chase/sig/android/activity/TransferStartActivity;->b(Ljava/util/List;Lcom/chase/sig/android/view/JPSpinner;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/chase/sig/android/domain/AccountTransfer;->t(Ljava/lang/String;)V

    .line 156
    iget-object v0, p0, Lcom/chase/sig/android/activity/mn;->a:Lcom/chase/sig/android/activity/TransferStartActivity;

    iget-object v0, p0, Lcom/chase/sig/android/activity/mn;->a:Lcom/chase/sig/android/activity/TransferStartActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/TransferStartActivity;->d(Lcom/chase/sig/android/activity/TransferStartActivity;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/chase/sig/android/activity/TransferStartActivity;->c(Ljava/util/List;Lcom/chase/sig/android/view/JPSpinner;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/chase/sig/android/domain/AccountTransfer;->u(Ljava/lang/String;)V

    .line 158
    iget-object v0, p0, Lcom/chase/sig/android/activity/mn;->a:Lcom/chase/sig/android/activity/TransferStartActivity;

    iget-object v0, p0, Lcom/chase/sig/android/activity/mn;->a:Lcom/chase/sig/android/activity/TransferStartActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/TransferStartActivity;->d(Lcom/chase/sig/android/activity/TransferStartActivity;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/chase/sig/android/activity/TransferStartActivity;->d(Ljava/util/List;Lcom/chase/sig/android/view/JPSpinner;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/chase/sig/android/domain/AccountTransfer;->c(Ljava/lang/String;)V

    .line 160
    iget-object v0, p0, Lcom/chase/sig/android/activity/mn;->a:Lcom/chase/sig/android/activity/TransferStartActivity;

    iget-object v0, p0, Lcom/chase/sig/android/activity/mn;->a:Lcom/chase/sig/android/activity/TransferStartActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/TransferStartActivity;->c(Lcom/chase/sig/android/activity/TransferStartActivity;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/chase/sig/android/activity/TransferStartActivity;->d(Ljava/util/List;Lcom/chase/sig/android/view/JPSpinner;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/chase/sig/android/domain/AccountTransfer;->o(Ljava/lang/String;)V

    .line 163
    iget-object v0, p0, Lcom/chase/sig/android/activity/mn;->a:Lcom/chase/sig/android/activity/TransferStartActivity;

    invoke-static {v0, v3}, Lcom/chase/sig/android/activity/TransferStartActivity;->a(Lcom/chase/sig/android/activity/TransferStartActivity;Lcom/chase/sig/android/domain/AccountTransfer;)Lcom/chase/sig/android/domain/AccountTransfer;

    .line 165
    iget-object v0, p0, Lcom/chase/sig/android/activity/mn;->a:Lcom/chase/sig/android/activity/TransferStartActivity;

    iget-object v0, v0, Lcom/chase/sig/android/activity/TransferStartActivity;->e:Lcom/chase/sig/android/activity/mc;

    const-class v1, Lcom/chase/sig/android/activity/TransferStartActivity$a;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/mc;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/ae;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/activity/TransferStartActivity$a;

    .line 168
    invoke-virtual {v0}, Lcom/chase/sig/android/activity/TransferStartActivity$a;->d()Z

    move-result v1

    if-nez v1, :cond_0

    .line 169
    const/4 v1, 0x1

    new-array v1, v1, [Lcom/chase/sig/android/domain/AccountTransfer;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/chase/sig/android/activity/mn;->a:Lcom/chase/sig/android/activity/TransferStartActivity;

    invoke-static {v3}, Lcom/chase/sig/android/activity/TransferStartActivity;->e(Lcom/chase/sig/android/activity/TransferStartActivity;)Lcom/chase/sig/android/domain/AccountTransfer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/TransferStartActivity$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 172
    :cond_0
    return-void

    .line 147
    :cond_1
    const-string v0, ""

    goto/16 :goto_0
.end method
