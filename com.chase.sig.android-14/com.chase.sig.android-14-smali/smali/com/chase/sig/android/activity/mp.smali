.class final Lcom/chase/sig/android/activity/mp;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/TransferVerifyActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/TransferVerifyActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 50
    iput-object p1, p0, Lcom/chase/sig/android/activity/mp;->a:Lcom/chase/sig/android/activity/TransferVerifyActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 4
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 54
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/mp;->a:Lcom/chase/sig/android/activity/TransferVerifyActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/TransferVerifyActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "INCLUDES_OPTIONAL_PRODUCT_FEE"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 55
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/chase/sig/android/activity/mp;->a:Lcom/chase/sig/android/activity/TransferVerifyActivity;

    const-class v2, Lcom/chase/sig/android/activity/TransferAuthorizeActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 57
    const-string v1, "transaction_object"

    iget-object v2, p0, Lcom/chase/sig/android/activity/mp;->a:Lcom/chase/sig/android/activity/TransferVerifyActivity;

    invoke-static {v2}, Lcom/chase/sig/android/activity/TransferVerifyActivity;->a(Lcom/chase/sig/android/activity/TransferVerifyActivity;)Lcom/chase/sig/android/domain/AccountTransfer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 58
    iget-object v1, p0, Lcom/chase/sig/android/activity/mp;->a:Lcom/chase/sig/android/activity/TransferVerifyActivity;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/activity/TransferVerifyActivity;->startActivity(Landroid/content/Intent;)V

    .line 66
    :cond_0
    :goto_0
    return-void

    .line 60
    :cond_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/mp;->a:Lcom/chase/sig/android/activity/TransferVerifyActivity;

    iget-object v0, v0, Lcom/chase/sig/android/activity/TransferVerifyActivity;->e:Lcom/chase/sig/android/activity/mc;

    const-class v1, Lcom/chase/sig/android/activity/TransferVerifyActivity$a;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/mc;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/ae;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/activity/TransferVerifyActivity$a;

    .line 62
    invoke-virtual {v0}, Lcom/chase/sig/android/activity/TransferVerifyActivity$a;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v1

    sget-object v2, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-eq v1, v2, :cond_0

    .line 63
    new-array v1, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/TransferVerifyActivity$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method
