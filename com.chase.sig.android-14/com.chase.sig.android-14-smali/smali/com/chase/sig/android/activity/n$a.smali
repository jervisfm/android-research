.class public abstract Lcom/chase/sig/android/activity/n$a;
.super Lcom/chase/sig/android/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/n;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/chase/sig/android/domain/Transaction;",
        "S:",
        "Lcom/chase/sig/android/activity/n",
        "<TT;>;>",
        "Lcom/chase/sig/android/d",
        "<TS;",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/service/movemoney/ListServiceResponse",
        "<TT;>;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 113
    invoke-direct {p0}, Lcom/chase/sig/android/d;-><init>()V

    return-void
.end method


# virtual methods
.method protected abstract a()Lcom/chase/sig/android/service/movemoney/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/chase/sig/android/service/movemoney/d",
            "<TT;>;"
        }
    .end annotation
.end method

.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .parameter

    .prologue
    .line 113
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/n$a;->a()Lcom/chase/sig/android/service/movemoney/d;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v1, p1, v1

    iget-object v2, v0, Lcom/chase/sig/android/service/movemoney/d;->b:Lcom/chase/sig/android/service/movemoney/e;

    invoke-static {}, Lcom/chase/sig/android/service/JPService;->c()Ljava/util/Hashtable;

    move-result-object v2

    const-string v3, "paymentId"

    invoke-virtual {v2, v3, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "page"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, v0, Lcom/chase/sig/android/service/movemoney/d;->a:Lcom/chase/sig/android/service/movemoney/g;

    iget-object v1, v1, Lcom/chase/sig/android/service/movemoney/g;->d:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Lcom/chase/sig/android/service/movemoney/d;->a(Ljava/util/Hashtable;Ljava/lang/String;)Lcom/chase/sig/android/service/movemoney/ListServiceResponse;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 5
    .parameter

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 113
    check-cast p1, Lcom/chase/sig/android/service/movemoney/ListServiceResponse;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/movemoney/ListServiceResponse;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "10085"

    invoke-virtual {p1, v0}, Lcom/chase/sig/android/service/movemoney/ListServiceResponse;->d(Ljava/lang/String;)Lcom/chase/sig/android/service/IServiceError;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/n;

    const-string v2, "Note"

    invoke-interface {v1}, Lcom/chase/sig/android/service/IServiceError;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1, v3}, Lcom/chase/sig/android/activity/n;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/n;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/movemoney/ListServiceResponse;->a()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/domain/Transaction;

    invoke-virtual {v0, v1, v3}, Lcom/chase/sig/android/activity/n;->a(Lcom/chase/sig/android/domain/Transaction;Z)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/n;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/movemoney/ListServiceResponse;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/n;->b(Ljava/util/List;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/n;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/n;->l()V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/n;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/movemoney/ListServiceResponse;->a()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/domain/Transaction;

    invoke-virtual {v0, v1, v3}, Lcom/chase/sig/android/activity/n;->a(Lcom/chase/sig/android/domain/Transaction;Z)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/n;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/n;->a()V

    goto :goto_0
.end method
