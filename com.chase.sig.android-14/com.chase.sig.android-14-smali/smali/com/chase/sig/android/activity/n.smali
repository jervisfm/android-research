.class public abstract Lcom/chase/sig/android/activity/n;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$e;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/n$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/chase/sig/android/domain/Transaction;",
        ">",
        "Lcom/chase/sig/android/activity/ai;",
        "Lcom/chase/sig/android/activity/fk$e;"
    }
.end annotation


# instance fields
.field protected a:Lcom/chase/sig/android/domain/Transaction;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field protected b:I

.field protected c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/chase/sig/android/domain/LabeledValue;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    .line 113
    return-void
.end method

.method protected static a(Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/view/detail/DetailColoredValueRow;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 52
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/chase/sig/android/view/detail/DetailColoredValueRow;

    invoke-direct {v0, p0, p1}, Lcom/chase/sig/android/view/detail/DetailColoredValueRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private d()Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/chase/sig/android/domain/LabeledValue;",
            ">;"
        }
    .end annotation

    .prologue
    .line 93
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/n;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "frequency_list"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method


# virtual methods
.method protected final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .parameter

    .prologue
    .line 151
    iget-object v0, p0, Lcom/chase/sig/android/activity/n;->a:Lcom/chase/sig/android/domain/Transaction;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Transaction;->l()Z

    move-result v0

    if-nez v0, :cond_1

    .line 153
    iget-object v0, p0, Lcom/chase/sig/android/activity/n;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 155
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 156
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/LabeledValue;

    .line 158
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 159
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/LabeledValue;->a()Ljava/lang/String;

    move-result-object v0

    .line 166
    :goto_0
    return-object v0

    :cond_1
    const-string v0, "One-Time"

    goto :goto_0
.end method

.method protected a()V
    .locals 0

    .prologue
    .line 45
    return-void
.end method

.method protected final a(Landroid/os/Bundle;Ljava/lang/Class;)V
    .locals 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/chase/sig/android/activity/n$a",
            "<TT;*>;>;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 58
    const-string v0, "alert_type"

    invoke-static {p1, v0}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 59
    const-string v0, "alert_type"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/chase/sig/android/activity/n;->b:I

    .line 64
    :goto_0
    const-string v0, "transaction_object"

    invoke-static {p1, v0}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 65
    const-string v0, "transaction_object"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/Transaction;

    invoke-virtual {p0, v0, v2}, Lcom/chase/sig/android/activity/n;->a(Lcom/chase/sig/android/domain/Transaction;Z)V

    .line 66
    const-string v0, "frequency_list"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/chase/sig/android/activity/n;->c:Ljava/util/ArrayList;

    .line 67
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/n;->a()V

    .line 75
    :goto_1
    return-void

    .line 61
    :cond_0
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/n;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "alert_type"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/chase/sig/android/activity/n;->b:I

    goto :goto_0

    .line 69
    :cond_1
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/n;->f()Lcom/chase/sig/android/domain/Transaction;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/n;->a:Lcom/chase/sig/android/domain/Transaction;

    .line 70
    invoke-direct {p0}, Lcom/chase/sig/android/activity/n;->d()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/chase/sig/android/activity/n;->d()Ljava/util/ArrayList;

    move-result-object v0

    :goto_2
    iput-object v0, p0, Lcom/chase/sig/android/activity/n;->c:Ljava/util/ArrayList;

    .line 73
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    iget-object v1, p0, Lcom/chase/sig/android/activity/n;->a:Lcom/chase/sig/android/domain/Transaction;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/Transaction;->n()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-virtual {p0, p2, v0}, Lcom/chase/sig/android/activity/n;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    goto :goto_1

    .line 70
    :cond_2
    iget-object v0, p0, Lcom/chase/sig/android/activity/n;->a:Lcom/chase/sig/android/domain/Transaction;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Transaction;->C()Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_2
.end method

.method public final a(Lcom/chase/sig/android/domain/Transaction;Z)V
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;Z)V"
        }
    .end annotation

    .prologue
    .line 29
    iput-object p1, p0, Lcom/chase/sig/android/activity/n;->a:Lcom/chase/sig/android/domain/Transaction;

    .line 31
    iget-object v0, p0, Lcom/chase/sig/android/activity/n;->c:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 32
    iget-object v0, p0, Lcom/chase/sig/android/activity/n;->a:Lcom/chase/sig/android/domain/Transaction;

    iget-object v1, p0, Lcom/chase/sig/android/activity/n;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/Transaction;->a(Ljava/util/ArrayList;)V

    .line 36
    :goto_0
    invoke-virtual {p0, p2}, Lcom/chase/sig/android/activity/n;->a(Z)V

    .line 37
    return-void

    .line 34
    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/n;->a:Lcom/chase/sig/android/domain/Transaction;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Transaction;->C()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/n;->c:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method protected abstract a(Z)V
.end method

.method protected final f()Lcom/chase/sig/android/domain/Transaction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 88
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/n;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "transaction_object"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/Transaction;

    return-object v0
.end method

.method protected final g()Lcom/chase/sig/android/service/movemoney/RequestFlags;
    .locals 2

    .prologue
    .line 97
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/n;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "request_flags"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/service/movemoney/RequestFlags;

    return-object v0
.end method

.method protected final h()V
    .locals 2

    .prologue
    .line 101
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/n;->f()Lcom/chase/sig/android/domain/Transaction;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/n;->a(Lcom/chase/sig/android/domain/Transaction;Z)V

    .line 102
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/n;->a()V

    .line 103
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 107
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ai;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 108
    const-string v0, "transaction_object"

    iget-object v1, p0, Lcom/chase/sig/android/activity/n;->a:Lcom/chase/sig/android/domain/Transaction;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 109
    const-string v0, "frequency_list"

    iget-object v1, p0, Lcom/chase/sig/android/activity/n;->c:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 110
    const-string v0, "alert_type"

    iget v1, p0, Lcom/chase/sig/android/activity/n;->b:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 111
    return-void
.end method
