.class final Lcom/chase/sig/android/activity/v;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/AccountsActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/AccountsActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 257
    iput-object p1, p0, Lcom/chase/sig/android/activity/v;->a:Lcom/chase/sig/android/activity/AccountsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 261
    :try_start_0
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/ai;

    .line 263
    iget v1, v0, Lcom/chase/sig/android/view/ai;->e:I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    packed-switch v1, :pswitch_data_0

    .line 291
    :cond_0
    :goto_0
    :pswitch_0
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1, p2, p3}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/widget/AdapterView;Landroid/view/View;I)V

    return-void

    .line 265
    :pswitch_1
    :try_start_1
    iget-boolean v1, v0, Lcom/chase/sig/android/view/ai;->j:Z

    if-eqz v1, :cond_0

    .line 266
    iget-object v1, p0, Lcom/chase/sig/android/activity/v;->a:Lcom/chase/sig/android/activity/AccountsActivity;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/activity/AccountsActivity;->g(Lcom/chase/sig/android/view/ai;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 291
    :catch_0
    move-exception v0

    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1, p2, p3}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/widget/AdapterView;Landroid/view/View;I)V

    throw v0

    .line 270
    :pswitch_2
    :try_start_2
    iget-object v1, p0, Lcom/chase/sig/android/activity/v;->a:Lcom/chase/sig/android/activity/AccountsActivity;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/activity/AccountsActivity;->e(Lcom/chase/sig/android/view/ai;)V

    goto :goto_0

    .line 273
    :pswitch_3
    iget-object v1, p0, Lcom/chase/sig/android/activity/v;->a:Lcom/chase/sig/android/activity/AccountsActivity;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/activity/AccountsActivity;->d(Lcom/chase/sig/android/view/ai;)V

    goto :goto_0

    .line 276
    :pswitch_4
    iget-object v1, p0, Lcom/chase/sig/android/activity/v;->a:Lcom/chase/sig/android/activity/AccountsActivity;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/activity/AccountsActivity;->a(Lcom/chase/sig/android/view/ai;)V

    goto :goto_0

    .line 280
    :pswitch_5
    iget-object v1, p0, Lcom/chase/sig/android/activity/v;->a:Lcom/chase/sig/android/activity/AccountsActivity;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/activity/AccountsActivity;->c(Lcom/chase/sig/android/view/ai;)V

    goto :goto_0

    .line 283
    :pswitch_6
    iget-object v1, p0, Lcom/chase/sig/android/activity/v;->a:Lcom/chase/sig/android/activity/AccountsActivity;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/activity/AccountsActivity;->f(Lcom/chase/sig/android/view/ai;)V

    goto :goto_0

    .line 288
    :pswitch_7
    iget-object v1, p0, Lcom/chase/sig/android/activity/v;->a:Lcom/chase/sig/android/activity/AccountsActivity;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/activity/AccountsActivity;->b(Lcom/chase/sig/android/view/ai;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 263
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_3
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_7
    .end packed-switch
.end method
