.class public Lcom/chase/sig/android/activity/wire/WireAddCompleteActivity;
.super Lcom/chase/sig/android/activity/m;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/activity/m",
        "<",
        "Lcom/chase/sig/android/domain/WireTransaction;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/chase/sig/android/activity/m;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1
    .parameter

    .prologue
    .line 19
    const v0, 0x7f03005e

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/wire/WireAddCompleteActivity;->b(I)V

    .line 20
    const v0, 0x7f070074

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/wire/WireAddCompleteActivity;->setTitle(I)V

    .line 22
    invoke-static {}, Lcom/chase/sig/android/c;->a()V

    .line 23
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/wire/WireAddCompleteActivity;->h()V

    .line 24
    return-void
.end method

.method protected final a(Z)V
    .locals 7
    .parameter

    .prologue
    .line 28
    const v0, 0x7f090153

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/wire/WireAddCompleteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailView;

    .line 29
    iget-object v1, p0, Lcom/chase/sig/android/activity/wire/WireAddCompleteActivity;->a:Lcom/chase/sig/android/domain/Transaction;

    check-cast v1, Lcom/chase/sig/android/domain/WireTransaction;

    .line 31
    const/4 v2, 0x7

    new-array v2, v2, [Lcom/chase/sig/android/view/detail/a;

    const/4 v3, 0x0

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Transaction Number"

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/WireTransaction;->n()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    const/4 v3, 0x1

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Recipient"

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/WireTransaction;->a()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    const/4 v3, 0x2

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "From"

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/WireTransaction;->m()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    const/4 v3, 0x3

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Amount"

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/WireTransaction;->e()Lcom/chase/sig/android/util/Dollar;

    move-result-object v6

    invoke-virtual {v6}, Lcom/chase/sig/android/util/Dollar;->h()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    const/4 v3, 0x4

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Date"

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/WireTransaction;->q()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/chase/sig/android/util/s;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    const/4 v3, 0x5

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Status"

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/WireTransaction;->t()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    const/4 v3, 0x6

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Memo"

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/WireTransaction;->o()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v5, v1}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/view/detail/DetailView;->setRows([Lcom/chase/sig/android/view/detail/a;)V

    .line 40
    const v0, 0x7f070122

    const-class v1, Lcom/chase/sig/android/activity/wire/WireHistoryActivity;

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/wire/WireAddCompleteActivity;->a(ILjava/lang/Class;)V

    .line 41
    const v0, 0x7f070123

    const-class v1, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/wire/WireAddCompleteActivity;->b(ILjava/lang/Class;)V

    .line 42
    return-void
.end method

.method protected final b()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/chase/sig/android/activity/eb;",
            ">;"
        }
    .end annotation

    .prologue
    .line 46
    const-class v0, Lcom/chase/sig/android/activity/wire/WireHomeActivity;

    return-object v0
.end method
