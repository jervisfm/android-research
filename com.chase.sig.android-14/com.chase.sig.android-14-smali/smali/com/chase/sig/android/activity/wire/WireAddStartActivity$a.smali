.class public Lcom/chase/sig/android/activity/wire/WireAddStartActivity$a;
.super Lcom/chase/sig/android/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/wire/WireAddStartActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/d",
        "<",
        "Lcom/chase/sig/android/activity/wire/WireAddStartActivity;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/service/wire/WireGetPayeesResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 317
    invoke-direct {p0}, Lcom/chase/sig/android/d;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 317
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->A()Lcom/chase/sig/android/service/n;

    invoke-static {}, Lcom/chase/sig/android/service/n;->f()Lcom/chase/sig/android/service/wire/a;

    invoke-static {}, Lcom/chase/sig/android/service/wire/a;->a()Lcom/chase/sig/android/service/wire/WireGetPayeesResponse;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 3
    .parameter

    .prologue
    .line 317
    check-cast p1, Lcom/chase/sig/android/service/wire/WireGetPayeesResponse;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/wire/WireGetPayeesResponse;->a()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v1, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;

    invoke-static {v1}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->j(Lcom/chase/sig/android/activity/wire/WireAddStartActivity;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {p1}, Lcom/chase/sig/android/service/wire/WireGetPayeesResponse;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/wire/WireGetPayeesResponse;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->b(Ljava/util/List;)V

    :goto_0
    return-void

    :cond_0
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;

    const v1, 0x7f07027f

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->f(I)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v1, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;

    invoke-static {v1, v0}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->a(Lcom/chase/sig/android/activity/wire/WireAddStartActivity;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->l(Lcom/chase/sig/android/activity/wire/WireAddStartActivity;)V

    goto :goto_0
.end method
