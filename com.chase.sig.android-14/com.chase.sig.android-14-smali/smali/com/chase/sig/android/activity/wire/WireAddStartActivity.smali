.class public Lcom/chase/sig/android/activity/wire/WireAddStartActivity;
.super Lcom/chase/sig/android/activity/a;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/wire/WireAddStartActivity$a;,
        Lcom/chase/sig/android/activity/wire/WireAddStartActivity$b;
    }
.end annotation


# instance fields
.field a:Lcom/chase/sig/android/view/x;

.field private b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/chase/sig/android/domain/Payee;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/chase/sig/android/activity/a;-><init>()V

    .line 176
    new-instance v0, Lcom/chase/sig/android/activity/wire/c;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/activity/wire/c;-><init>(Lcom/chase/sig/android/activity/wire/WireAddStartActivity;)V

    iput-object v0, p0, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->a:Lcom/chase/sig/android/view/x;

    .line 317
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/wire/WireAddStartActivity;Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 65
    invoke-virtual {p0, p1}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/wire/WireAddStartActivity;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 65
    iput-object p1, p0, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->b:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/wire/WireAddStartActivity;)V
    .locals 6
    .parameter

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x0

    .line 65
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->d()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v1

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "MESSAGE_TO_RECIPIENT"

    aput-object v3, v2, v0

    const/4 v3, 0x1

    const-string v4, "TO_RECIPIENT_BANK"

    aput-object v4, v2, v3

    const-string v3, "MEMO"

    aput-object v3, v2, v5

    const/4 v3, 0x3

    const-string v4, "MEMO_FIELDS"

    aput-object v4, v2, v3

    array-length v3, v2

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    invoke-virtual {v1, v4}, Lcom/chase/sig/android/view/detail/DetailView;->e(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v4

    invoke-virtual {v4}, Lcom/chase/sig/android/view/detail/a;->b()Lcom/chase/sig/android/view/detail/a;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Lcom/chase/sig/android/view/detail/DetailView;->c()V

    const-string v0, "MESSAGE_TO_RECIPIENT"

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/view/detail/DetailView;->e(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/a;->k()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    :goto_1
    return-void

    :cond_1
    invoke-virtual {p0, v5}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->showDialog(I)V

    goto :goto_1
.end method

.method static synthetic b(Lcom/chase/sig/android/activity/wire/WireAddStartActivity;)Lcom/chase/sig/android/view/detail/DetailView;
    .locals 1
    .parameter

    .prologue
    .line 65
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Lcom/chase/sig/android/activity/wire/WireAddStartActivity;)Lcom/chase/sig/android/view/detail/DetailView;
    .locals 1
    .parameter

    .prologue
    .line 65
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Lcom/chase/sig/android/activity/wire/WireAddStartActivity;)Lcom/chase/sig/android/view/detail/DetailView;
    .locals 1
    .parameter

    .prologue
    .line 65
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v0

    return-object v0
.end method

.method private e()Lcom/chase/sig/android/view/JPSpinner;
    .locals 1

    .prologue
    .line 154
    const-string v0, "PAY_FROM"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->b(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/q;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/q;->q()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v0

    return-object v0
.end method

.method static synthetic e(Lcom/chase/sig/android/activity/wire/WireAddStartActivity;)Lcom/chase/sig/android/view/detail/DetailView;
    .locals 1
    .parameter

    .prologue
    .line 65
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v0

    return-object v0
.end method

.method static synthetic f(Lcom/chase/sig/android/activity/wire/WireAddStartActivity;)Lcom/chase/sig/android/view/detail/DetailView;
    .locals 1
    .parameter

    .prologue
    .line 65
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v0

    return-object v0
.end method

.method private f()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/chase/sig/android/domain/IAccount;",
            ">;"
        }
    .end annotation

    .prologue
    .line 173
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->b:Lcom/chase/sig/android/domain/g;

    invoke-interface {v0}, Lcom/chase/sig/android/domain/g;->j()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic g(Lcom/chase/sig/android/activity/wire/WireAddStartActivity;)Lcom/chase/sig/android/view/JPSpinner;
    .locals 1
    .parameter

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->i()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v0

    return-object v0
.end method

.method private g()Ljava/util/Calendar;
    .locals 4

    .prologue
    .line 343
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 345
    :try_start_0
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v0, "yyyyMMdd"

    invoke-direct {v2, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 346
    invoke-direct {p0}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->i()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/view/JPSpinner;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 347
    iget-object v0, p0, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->b:Ljava/util/ArrayList;

    invoke-direct {p0}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->i()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v3

    invoke-virtual {v3}, Lcom/chase/sig/android/view/JPSpinner;->getSelectedItemPosition()I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/Payee;

    .line 349
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Payee;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 354
    :cond_0
    :goto_0
    return-object v1

    .line 353
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method static synthetic h(Lcom/chase/sig/android/activity/wire/WireAddStartActivity;)Ljava/util/ArrayList;
    .locals 1
    .parameter

    .prologue
    .line 65
    iget-object v0, p0, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->b:Ljava/util/ArrayList;

    return-object v0
.end method

.method private h()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 360
    invoke-direct {p0}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->e()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v0

    invoke-direct {p0}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->f()Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/chase/sig/android/util/a;->a(Landroid/content/Context;Ljava/util/List;)Landroid/widget/SimpleAdapter;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/JPSpinner;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 362
    invoke-direct {p0}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->e()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/view/JPSpinner;->setSelection(I)V

    .line 363
    invoke-direct {p0}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->i()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v0

    iget-object v1, p0, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->b:Ljava/util/ArrayList;

    invoke-static {p0, v1}, Lcom/chase/sig/android/util/a;->f(Landroid/content/Context;Ljava/util/List;)Landroid/widget/SimpleAdapter;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/JPSpinner;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 365
    invoke-direct {p0}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->i()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/view/JPSpinner;->setSelection(I)V

    .line 367
    return-void
.end method

.method private i()Lcom/chase/sig/android/view/JPSpinner;
    .locals 1

    .prologue
    .line 370
    const-string v0, "PAY_TO"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->b(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/q;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/q;->q()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v0

    return-object v0
.end method

.method static synthetic i(Lcom/chase/sig/android/activity/wire/WireAddStartActivity;)Lcom/chase/sig/android/view/JPSpinner;
    .locals 1
    .parameter

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->e()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v0

    return-object v0
.end method

.method static synthetic j(Lcom/chase/sig/android/activity/wire/WireAddStartActivity;)Ljava/util/ArrayList;
    .locals 1
    .parameter

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->f()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method static synthetic k(Lcom/chase/sig/android/activity/wire/WireAddStartActivity;)V
    .locals 2
    .parameter

    .prologue
    .line 65
    const-string v0, "DATE"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/d;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/d;->p()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->l(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->g()Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->a(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/detail/d;->b(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method static synthetic l(Lcom/chase/sig/android/activity/wire/WireAddStartActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->h()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 12
    .parameter

    .prologue
    const/16 v11, 0x64

    const/4 v10, -0x1

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 74
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/a;->a(Landroid/os/Bundle;)V

    .line 76
    const v0, 0x7f07011a

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->setTitle(I)V

    .line 77
    const v0, 0x7f0902ca

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const v1, 0x7f070296

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 79
    const-string v0, "memo_fields_visible"

    invoke-static {p1, v0}, Lcom/chase/sig/android/util/e;->b(Landroid/os/Bundle;Ljava/lang/String;)Z

    move-result v4

    .line 82
    new-instance v1, Lcom/chase/sig/android/activity/wire/a;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/wire/a;-><init>(Lcom/chase/sig/android/activity/wire/WireAddStartActivity;)V

    .line 89
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v5

    const/16 v0, 0x8

    new-array v6, v0, [Lcom/chase/sig/android/view/detail/a;

    new-instance v0, Lcom/chase/sig/android/view/detail/q;

    const-string v7, "Recipient"

    invoke-direct {v0, v7}, Lcom/chase/sig/android/view/detail/q;-><init>(Ljava/lang/String;)V

    const-string v7, "PAY_TO"

    iput-object v7, v0, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v0, Lcom/chase/sig/android/view/detail/q;

    const v7, 0x7f07011b

    invoke-virtual {p0, v7}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Lcom/chase/sig/android/view/detail/q;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    aput-object v0, v6, v3

    new-instance v0, Lcom/chase/sig/android/view/detail/q;

    const-string v7, "From"

    invoke-direct {v0, v7}, Lcom/chase/sig/android/view/detail/q;-><init>(Ljava/lang/String;)V

    const-string v7, "PAY_FROM"

    iput-object v7, v0, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v0, Lcom/chase/sig/android/view/detail/q;

    const v7, 0x7f07011c

    invoke-virtual {p0, v7}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Lcom/chase/sig/android/view/detail/q;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    aput-object v0, v6, v2

    const/4 v7, 0x2

    new-instance v0, Lcom/chase/sig/android/view/detail/c;

    const-string v8, "Amount $"

    invoke-direct {v0, v8}, Lcom/chase/sig/android/view/detail/c;-><init>(Ljava/lang/String;)V

    const-string v8, "AMOUNT"

    iput-object v8, v0, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v0, Lcom/chase/sig/android/view/detail/c;

    const-string v8, "Enter amount"

    invoke-virtual {v0, v8}, Lcom/chase/sig/android/view/detail/c;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    aput-object v0, v6, v7

    const/4 v7, 0x3

    new-instance v0, Lcom/chase/sig/android/view/detail/d;

    const-string v8, "Date"

    const-string v9, ""

    invoke-direct {v0, v8, v9}, Lcom/chase/sig/android/view/detail/d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v8, "Enter date"

    invoke-virtual {v0, v8}, Lcom/chase/sig/android/view/detail/d;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/d;

    const-string v8, "DATE"

    iput-object v8, v0, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v0, Lcom/chase/sig/android/view/detail/d;

    invoke-virtual {p0, v3}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->i(I)Landroid/view/View$OnClickListener;

    move-result-object v8

    iput-object v8, v0, Lcom/chase/sig/android/view/detail/d;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/d;->q()Lcom/chase/sig/android/view/detail/d;

    move-result-object v0

    aput-object v0, v6, v7

    const/4 v7, 0x4

    new-instance v0, Lcom/chase/sig/android/view/detail/d;

    const-string v8, ""

    const-string v9, ""

    invoke-direct {v0, v8, v9}, Lcom/chase/sig/android/view/detail/d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v8, "Enter message or memo"

    invoke-virtual {v0, v8}, Lcom/chase/sig/android/view/detail/d;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/d;

    const-string v8, "MEMO_FIELDS"

    iput-object v8, v0, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v0, Lcom/chase/sig/android/view/detail/d;

    iput-boolean v2, v0, Lcom/chase/sig/android/view/detail/a;->n:Z

    check-cast v0, Lcom/chase/sig/android/view/detail/d;

    iput-object v1, v0, Lcom/chase/sig/android/view/detail/d;->a:Landroid/view/View$OnClickListener;

    iput-boolean v4, v0, Lcom/chase/sig/android/view/detail/a;->j:Z

    aput-object v0, v6, v7

    const/4 v7, 0x5

    new-instance v0, Lcom/chase/sig/android/view/detail/l;

    const-string v1, "Message To Recipient "

    const-string v8, ""

    invoke-direct {v0, v1, v8}, Lcom/chase/sig/android/view/detail/l;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "Optional"

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/detail/l;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/l;

    const-string v1, "MESSAGE_TO_RECIPIENT"

    iput-object v1, v0, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v0, Lcom/chase/sig/android/view/detail/l;

    const/16 v1, 0x8c

    iput v1, v0, Lcom/chase/sig/android/view/detail/l;->a:I

    if-nez v4, :cond_0

    move v1, v2

    :goto_0
    iput-boolean v1, v0, Lcom/chase/sig/android/view/detail/a;->j:Z

    aput-object v0, v6, v7

    const/4 v7, 0x6

    new-instance v0, Lcom/chase/sig/android/view/detail/l;

    const-string v1, "To Recipient\'s Bank "

    const-string v8, ""

    invoke-direct {v0, v1, v8}, Lcom/chase/sig/android/view/detail/l;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "Optional"

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/detail/l;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/l;

    const-string v1, "TO_RECIPIENT_BANK"

    iput-object v1, v0, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v0, Lcom/chase/sig/android/view/detail/l;

    iput v11, v0, Lcom/chase/sig/android/view/detail/l;->a:I

    if-nez v4, :cond_1

    move v1, v2

    :goto_1
    iput-boolean v1, v0, Lcom/chase/sig/android/view/detail/a;->j:Z

    aput-object v0, v6, v7

    const/4 v1, 0x7

    new-instance v0, Lcom/chase/sig/android/view/detail/l;

    const-string v7, "Memo "

    const-string v8, ""

    invoke-direct {v0, v7, v8}, Lcom/chase/sig/android/view/detail/l;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "Optional"

    invoke-virtual {v0, v7}, Lcom/chase/sig/android/view/detail/l;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/l;

    const-string v7, "MEMO"

    iput-object v7, v0, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    check-cast v0, Lcom/chase/sig/android/view/detail/l;

    iput v11, v0, Lcom/chase/sig/android/view/detail/l;->a:I

    if-nez v4, :cond_2

    :goto_2
    iput-boolean v2, v0, Lcom/chase/sig/android/view/detail/a;->j:Z

    aput-object v0, v6, v1

    invoke-virtual {v5, v6}, Lcom/chase/sig/android/view/detail/DetailView;->setRows([Lcom/chase/sig/android/view/detail/a;)V

    .line 107
    invoke-direct {p0}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->i()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v0

    iget-object v1, p0, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->a:Lcom/chase/sig/android/view/x;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/JPSpinner;->a(Lcom/chase/sig/android/view/x;)V

    .line 109
    const-string v0, "wirePayees"

    invoke-static {p1, v0}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 110
    const-string v0, "wirePayees"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->b:Ljava/util/ArrayList;

    .line 111
    invoke-direct {p0}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->h()V

    .line 119
    :goto_3
    const v0, 0x7f0902cb

    new-instance v1, Lcom/chase/sig/android/activity/wire/b;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/wire/b;-><init>(Lcom/chase/sig/android/activity/wire/WireAddStartActivity;)V

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 151
    return-void

    :cond_0
    move v1, v3

    .line 89
    goto :goto_0

    :cond_1
    move v1, v3

    goto :goto_1

    :cond_2
    move v2, v3

    goto :goto_2

    .line 114
    :cond_3
    invoke-direct {p0}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->e()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v0

    invoke-virtual {v0, v10}, Lcom/chase/sig/android/view/JPSpinner;->setSelection(I)V

    .line 115
    invoke-direct {p0}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->i()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v0

    invoke-virtual {v0, v10}, Lcom/chase/sig/android/view/JPSpinner;->setSelection(I)V

    .line 116
    const-class v0, Lcom/chase/sig/android/activity/wire/WireAddStartActivity$a;

    new-array v1, v3, [Ljava/lang/Void;

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    goto :goto_3
.end method

.method protected final d()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 191
    const/4 v0, 0x1

    .line 192
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->y()V

    .line 194
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v3

    .line 196
    invoke-direct {p0}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->i()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v2

    invoke-virtual {v2}, Lcom/chase/sig/android/view/JPSpinner;->b()Z

    move-result v2

    if-nez v2, :cond_0

    .line 197
    const-string v0, "PAY_TO"

    invoke-virtual {v3, v0}, Lcom/chase/sig/android/view/detail/DetailView;->a(Ljava/lang/String;)V

    move v0, v1

    .line 201
    :cond_0
    invoke-direct {p0}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->e()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v2

    invoke-virtual {v2}, Lcom/chase/sig/android/view/JPSpinner;->b()Z

    move-result v2

    if-nez v2, :cond_3

    .line 202
    const-string v0, "PAY_FROM"

    invoke-virtual {v3, v0}, Lcom/chase/sig/android/view/detail/DetailView;->a(Ljava/lang/String;)V

    move v2, v1

    .line 206
    :goto_0
    const-string v0, "DATE"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/d;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/d;->p()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->l(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 207
    const-string v0, "DATE"

    invoke-virtual {v3, v0}, Lcom/chase/sig/android/view/detail/DetailView;->a(Ljava/lang/String;)V

    move v2, v1

    .line 211
    :cond_1
    const-string v0, "AMOUNT"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/c;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/c;->n()Z

    move-result v0

    if-nez v0, :cond_2

    .line 212
    const-string v0, "AMOUNT"

    invoke-virtual {v3, v0}, Lcom/chase/sig/android/view/detail/DetailView;->a(Ljava/lang/String;)V

    .line 216
    :goto_1
    return v1

    :cond_2
    move v1, v2

    goto :goto_1

    :cond_3
    move v2, v0

    goto :goto_0
.end method

.method public final k()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 220
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->y()V

    .line 221
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v0

    const-string v1, "MESSAGE_TO_RECIPIENT"

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/detail/DetailView;->e(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    iput-boolean v2, v0, Lcom/chase/sig/android/view/detail/a;->j:Z

    .line 222
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v0

    const-string v1, "TO_RECIPIENT_BANK"

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/detail/DetailView;->e(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    iput-boolean v2, v0, Lcom/chase/sig/android/view/detail/a;->j:Z

    .line 223
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v0

    const-string v1, "MEMO"

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/detail/DetailView;->e(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    iput-boolean v2, v0, Lcom/chase/sig/android/view/detail/a;->j:Z

    .line 224
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v0

    const-string v1, "MEMO_FIELDS"

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/detail/DetailView;->e(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/chase/sig/android/view/detail/a;->j:Z

    .line 225
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/DetailView;->a()V

    .line 226
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 8
    .parameter

    .prologue
    const/4 v3, 0x1

    .line 231
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/a;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    .line 233
    packed-switch p1, :pswitch_data_0

    .line 263
    :goto_0
    :pswitch_0
    return-object v0

    .line 236
    :pswitch_1
    new-instance v2, Lcom/chase/sig/android/activity/wire/d;

    invoke-direct {v2, p0}, Lcom/chase/sig/android/activity/wire/d;-><init>(Lcom/chase/sig/android/activity/wire/WireAddStartActivity;)V

    .line 245
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v6

    .line 246
    const/4 v0, 0x2

    const/16 v1, 0xc

    invoke-virtual {v6, v0, v1}, Ljava/util/Calendar;->add(II)V

    .line 248
    new-instance v0, Lcom/chase/sig/android/view/b;

    invoke-direct {p0}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->g()Ljava/util/Calendar;

    move-result-object v5

    const/4 v7, 0x0

    move-object v1, p0

    move v4, v3

    invoke-direct/range {v0 .. v7}, Lcom/chase/sig/android/view/b;-><init>(Landroid/content/Context;Lcom/chase/sig/android/view/b$a;ZZLjava/util/Calendar;Ljava/util/Calendar;Ljava/util/Calendar;)V

    goto :goto_0

    .line 252
    :pswitch_2
    new-instance v0, Lcom/chase/sig/android/view/k$a;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/view/k$a;-><init>(Landroid/content/Context;)V

    .line 253
    const v1, 0x7f07011d

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/k$a;->b(I)Lcom/chase/sig/android/view/k$a;

    move-result-object v1

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/chase/sig/android/view/k$a;->i:Z

    const-string v2, "OK"

    new-instance v3, Lcom/chase/sig/android/activity/wire/e;

    invoke-direct {v3, p0}, Lcom/chase/sig/android/activity/wire/e;-><init>(Lcom/chase/sig/android/activity/wire/WireAddStartActivity;)V

    invoke-virtual {v1, v2, v3}, Lcom/chase/sig/android/view/k$a;->a(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    .line 260
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/k$a;->d(I)Lcom/chase/sig/android/view/k;

    move-result-object v0

    goto :goto_0

    .line 233
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .parameter

    .prologue
    .line 375
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/a;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 377
    const-string v0, "memo_fields_visible"

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->a()Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v1

    const-string v2, "MEMO_FIELDS"

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/view/detail/DetailView;->e(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v1

    iget-boolean v1, v1, Lcom/chase/sig/android/view/detail/a;->j:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 379
    iget-object v0, p0, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 380
    const-string v0, "wirePayees"

    iget-object v1, p0, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->b:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 382
    :cond_0
    return-void
.end method
