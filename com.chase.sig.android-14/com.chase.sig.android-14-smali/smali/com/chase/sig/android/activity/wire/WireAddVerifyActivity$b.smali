.class public Lcom/chase/sig/android/activity/wire/WireAddVerifyActivity$b;
.super Lcom/chase/sig/android/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/wire/WireAddVerifyActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/d",
        "<",
        "Lcom/chase/sig/android/activity/wire/WireAddVerifyActivity;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/service/movemoney/ServiceResponse;",
        ">;"
    }
.end annotation


# instance fields
.field a:Lcom/chase/sig/android/domain/WireTransaction;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/chase/sig/android/d;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .parameter

    .prologue
    .line 79
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/wire/WireAddVerifyActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/wire/WireAddVerifyActivity;->A()Lcom/chase/sig/android/service/n;

    invoke-static {}, Lcom/chase/sig/android/service/n;->f()Lcom/chase/sig/android/service/wire/a;

    move-result-object v1

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/wire/WireAddVerifyActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/wire/WireAddVerifyActivity;->a(Lcom/chase/sig/android/activity/wire/WireAddVerifyActivity;)Lcom/chase/sig/android/domain/WireTransaction;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/wire/WireAddVerifyActivity$b;->a:Lcom/chase/sig/android/domain/WireTransaction;

    iget-object v0, p0, Lcom/chase/sig/android/activity/wire/WireAddVerifyActivity$b;->a:Lcom/chase/sig/android/domain/WireTransaction;

    sget-object v2, Lcom/chase/sig/android/service/movemoney/RequestFlags;->e:Lcom/chase/sig/android/service/movemoney/RequestFlags;

    invoke-virtual {v1, v0, v2}, Lcom/chase/sig/android/service/wire/a;->b(Lcom/chase/sig/android/domain/Transaction;Lcom/chase/sig/android/service/movemoney/RequestFlags;)Lcom/chase/sig/android/service/movemoney/ServiceResponse;

    move-result-object v1

    iget-object v0, p0, Lcom/chase/sig/android/activity/wire/WireAddVerifyActivity$b;->a:Lcom/chase/sig/android/domain/WireTransaction;

    invoke-virtual {v1}, Lcom/chase/sig/android/service/movemoney/ServiceResponse;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/domain/WireTransaction;->k(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/wire/WireAddVerifyActivity$b;->a:Lcom/chase/sig/android/domain/WireTransaction;

    invoke-virtual {v1}, Lcom/chase/sig/android/service/movemoney/ServiceResponse;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/domain/WireTransaction;->q(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/wire/WireAddVerifyActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/wire/WireAddVerifyActivity;->l()V

    return-object v1
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 3
    .parameter

    .prologue
    .line 79
    check-cast p1, Lcom/chase/sig/android/service/movemoney/ServiceResponse;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/movemoney/ServiceResponse;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/wire/WireAddVerifyActivity;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/movemoney/ServiceResponse;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/wire/WireAddVerifyActivity;->c(Ljava/util/List;)V

    :goto_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/wire/WireAddVerifyActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/wire/WireAddVerifyActivity;->u()V

    return-void

    :cond_0
    new-instance v1, Landroid/content/Intent;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    const-class v2, Lcom/chase/sig/android/activity/wire/WireAddCompleteActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v0, "transaction_object"

    iget-object v2, p0, Lcom/chase/sig/android/activity/wire/WireAddVerifyActivity$b;->a:Lcom/chase/sig/android/domain/WireTransaction;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v2, "queued_errors"

    invoke-virtual {p1}, Lcom/chase/sig/android/service/movemoney/ServiceResponse;->g()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/io/Serializable;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    check-cast v0, Lcom/chase/sig/android/activity/wire/WireAddVerifyActivity;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/wire/WireAddVerifyActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
