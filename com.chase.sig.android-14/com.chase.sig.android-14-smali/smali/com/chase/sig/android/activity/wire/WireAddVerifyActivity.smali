.class public Lcom/chase/sig/android/activity/wire/WireAddVerifyActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$e;
.implements Lcom/chase/sig/android/c$a;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/wire/WireAddVerifyActivity$b;,
        Lcom/chase/sig/android/activity/wire/WireAddVerifyActivity$a;
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    .line 29
    const-string v0, ""

    iput-object v0, p0, Lcom/chase/sig/android/activity/wire/WireAddVerifyActivity;->a:Ljava/lang/String;

    .line 79
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/activity/wire/WireAddVerifyActivity;)Lcom/chase/sig/android/domain/WireTransaction;
    .locals 1
    .parameter

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/chase/sig/android/activity/wire/WireAddVerifyActivity;->d()Lcom/chase/sig/android/domain/WireTransaction;

    move-result-object v0

    return-object v0
.end method

.method private d()Lcom/chase/sig/android/domain/WireTransaction;
    .locals 2

    .prologue
    .line 62
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/wire/WireAddVerifyActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "transaction_object"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/WireTransaction;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 114
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/chase/sig/android/activity/PaymentsAndTransfersHomeActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 116
    const/high16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 117
    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/wire/WireAddVerifyActivity;->startActivity(Landroid/content/Intent;)V

    .line 118
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 10
    .parameter

    .prologue
    const v9, 0x7f090157

    const v8, 0x7f090156

    const/4 v7, 0x0

    .line 35
    const v0, 0x7f03005f

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/wire/WireAddVerifyActivity;->b(I)V

    .line 36
    const v0, 0x7f07011e

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/wire/WireAddVerifyActivity;->setTitle(I)V

    .line 38
    invoke-direct {p0}, Lcom/chase/sig/android/activity/wire/WireAddVerifyActivity;->d()Lcom/chase/sig/android/domain/WireTransaction;

    move-result-object v1

    .line 40
    const v0, 0x7f090153

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/wire/WireAddVerifyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/detail/DetailView;

    .line 41
    const/4 v2, 0x7

    new-array v2, v2, [Lcom/chase/sig/android/view/detail/a;

    new-instance v3, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v4, "Recipient"

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/WireTransaction;->d()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v2, v7

    const/4 v3, 0x1

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "From"

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/WireTransaction;->m()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    const/4 v3, 0x2

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Amount"

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/WireTransaction;->e()Lcom/chase/sig/android/util/Dollar;

    move-result-object v6

    invoke-virtual {v6}, Lcom/chase/sig/android/util/Dollar;->h()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    const/4 v3, 0x3

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Date"

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/WireTransaction;->q()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/chase/sig/android/util/s;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    const/4 v3, 0x4

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Message To Recipient"

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/WireTransaction;->f()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    const/4 v3, 0x5

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "To Recipient\'s Bank"

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/WireTransaction;->h()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    const/4 v3, 0x6

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Memo"

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/WireTransaction;->o()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v5, v1}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/view/detail/DetailView;->setRows([Lcom/chase/sig/android/view/detail/a;)V

    .line 49
    if-eqz p1, :cond_0

    const-string v0, "AGREEMENT"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 50
    const-string v0, "AGREEMENT"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/wire/WireAddVerifyActivity;->a:Ljava/lang/String;

    .line 53
    :cond_0
    invoke-virtual {p0, v9}, Lcom/chase/sig/android/activity/wire/WireAddVerifyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const v1, 0x7f070120

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 54
    invoke-virtual {p0, v7}, Lcom/chase/sig/android/activity/wire/WireAddVerifyActivity;->i(I)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-virtual {p0, v9, v0}, Lcom/chase/sig/android/activity/wire/WireAddVerifyActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 56
    invoke-virtual {p0, v8}, Lcom/chase/sig/android/activity/wire/WireAddVerifyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const v1, 0x7f070121

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 57
    invoke-virtual {p0, p0}, Lcom/chase/sig/android/activity/wire/WireAddVerifyActivity;->a(Lcom/chase/sig/android/c$a;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-virtual {p0, v8, v0}, Lcom/chase/sig/android/activity/wire/WireAddVerifyActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 59
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 3
    .parameter

    .prologue
    .line 122
    packed-switch p1, :pswitch_data_0

    .line 129
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ai;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    .line 124
    :pswitch_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/wire/WireAddVerifyActivity;->a:Ljava/lang/String;

    const-class v1, Lcom/chase/sig/android/activity/wire/WireAddVerifyActivity$b;

    const-class v2, Lcom/chase/sig/android/activity/wire/WireAddVerifyActivity$a;

    invoke-static {p0, v0, v1, v2}, Lcom/chase/sig/android/activity/wire/f;->a(Lcom/chase/sig/android/activity/eb;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    .line 127
    :pswitch_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/wire/WireAddVerifyActivity;->a:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/chase/sig/android/activity/wire/f;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    .line 122
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 67
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ai;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 68
    const-string v0, "AGREEMENT"

    iget-object v1, p0, Lcom/chase/sig/android/activity/wire/WireAddVerifyActivity;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    return-void
.end method
