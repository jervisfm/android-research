.class public Lcom/chase/sig/android/activity/wire/WireCompleteActivity;
.super Lcom/chase/sig/android/activity/m;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/wire/WireCompleteActivity$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/activity/m",
        "<",
        "Lcom/chase/sig/android/domain/WireTransaction;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/chase/sig/android/activity/m;-><init>()V

    .line 83
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1
    .parameter

    .prologue
    .line 23
    const v0, 0x7f03005e

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/wire/WireCompleteActivity;->b(I)V

    .line 24
    const v0, 0x7f070074

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/wire/WireCompleteActivity;->setTitle(I)V

    .line 26
    invoke-static {}, Lcom/chase/sig/android/c;->a()V

    .line 27
    const-class v0, Lcom/chase/sig/android/activity/wire/WireCompleteActivity$a;

    invoke-virtual {p0, p1, v0}, Lcom/chase/sig/android/activity/wire/WireCompleteActivity;->a(Landroid/os/Bundle;Ljava/lang/Class;)V

    .line 28
    return-void
.end method

.method protected final a(Z)V
    .locals 7
    .parameter

    .prologue
    .line 32
    iget-object v0, p0, Lcom/chase/sig/android/activity/wire/WireCompleteActivity;->a:Lcom/chase/sig/android/domain/Transaction;

    check-cast v0, Lcom/chase/sig/android/domain/WireTransaction;

    .line 34
    const v1, 0x7f090153

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/wire/WireCompleteActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/view/detail/DetailView;

    .line 35
    const/16 v2, 0x8

    new-array v2, v2, [Lcom/chase/sig/android/view/detail/a;

    const/4 v3, 0x0

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Transaction Number"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/WireTransaction;->n()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    const/4 v3, 0x1

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Recipient"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/WireTransaction;->a()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    const/4 v3, 0x2

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "From"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/WireTransaction;->m()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    const/4 v3, 0x3

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Amount"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/WireTransaction;->e()Lcom/chase/sig/android/util/Dollar;

    move-result-object v6

    invoke-virtual {v6}, Lcom/chase/sig/android/util/Dollar;->h()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    const/4 v3, 0x4

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Date"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/WireTransaction;->q()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/chase/sig/android/util/s;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    const/4 v3, 0x5

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Frequency"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/WireTransaction;->i()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/chase/sig/android/activity/wire/WireCompleteActivity;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/wire/WireCompleteActivity;->c()Z

    move-result v5

    iput-boolean v5, v4, Lcom/chase/sig/android/view/detail/a;->j:Z

    aput-object v4, v2, v3

    const/4 v3, 0x6

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Remaining Wires"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/WireTransaction;->w()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/wire/WireCompleteActivity;->c()Z

    move-result v5

    iput-boolean v5, v4, Lcom/chase/sig/android/view/detail/a;->j:Z

    aput-object v4, v2, v3

    const/4 v3, 0x7

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Status"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/WireTransaction;->t()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v5, v0}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/view/detail/DetailView;->setRows([Lcom/chase/sig/android/view/detail/a;)V

    .line 45
    const v0, 0x7f070122

    const-class v1, Lcom/chase/sig/android/activity/wire/WireHistoryActivity;

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/wire/WireCompleteActivity;->a(ILjava/lang/Class;)V

    .line 46
    const v0, 0x7f070123

    const-class v1, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/wire/WireCompleteActivity;->b(ILjava/lang/Class;)V

    .line 47
    invoke-virtual {p0, p1}, Lcom/chase/sig/android/activity/wire/WireCompleteActivity;->b(Z)V

    .line 48
    return-void
.end method

.method protected final b()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/chase/sig/android/activity/eb;",
            ">;"
        }
    .end annotation

    .prologue
    .line 80
    const-class v0, Lcom/chase/sig/android/activity/wire/WireHomeActivity;

    return-object v0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 10
    .parameter

    .prologue
    const v9, 0x7f070068

    const/4 v8, -0x1

    const/4 v7, 0x1

    .line 52
    new-instance v1, Lcom/chase/sig/android/view/k$a;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/view/k$a;-><init>(Landroid/content/Context;)V

    .line 53
    packed-switch p1, :pswitch_data_0

    .line 73
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/m;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    .line 55
    :pswitch_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/wire/WireCompleteActivity;->a:Lcom/chase/sig/android/domain/Transaction;

    check-cast v0, Lcom/chase/sig/android/domain/WireTransaction;

    .line 56
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/wire/WireCompleteActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f070126

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/WireTransaction;->i()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/chase/sig/android/activity/wire/WireCompleteActivity;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/WireTransaction;->r()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/WireTransaction;->s()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    const/4 v5, 0x2

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/WireTransaction;->d()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x3

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/WireTransaction;->d()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-static {v2, v3, v4}, Lcom/chase/sig/android/util/s;->a(Landroid/content/Context;I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 61
    iput-boolean v7, v1, Lcom/chase/sig/android/view/k$a;->i:Z

    new-instance v2, Lcom/chase/sig/android/activity/a/b;

    invoke-direct {v2}, Lcom/chase/sig/android/activity/a/b;-><init>()V

    invoke-virtual {v1, v9, v2}, Lcom/chase/sig/android/view/k$a;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    move-result-object v2

    const v3, 0x7f070125

    invoke-virtual {v2, v3}, Lcom/chase/sig/android/view/k$a;->c(I)Lcom/chase/sig/android/view/k$a;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/chase/sig/android/view/k$a;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/k$a;

    .line 65
    invoke-virtual {v1, v8}, Lcom/chase/sig/android/view/k$a;->d(I)Lcom/chase/sig/android/view/k;

    move-result-object v0

    goto :goto_0

    .line 67
    :pswitch_1
    iput-boolean v7, v1, Lcom/chase/sig/android/view/k$a;->i:Z

    new-instance v0, Lcom/chase/sig/android/activity/a/b;

    invoke-direct {v0}, Lcom/chase/sig/android/activity/a/b;-><init>()V

    invoke-virtual {v1, v9, v0}, Lcom/chase/sig/android/view/k$a;->a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    move-result-object v0

    const v2, 0x7f070124

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/view/k$a;->b(I)Lcom/chase/sig/android/view/k$a;

    .line 71
    invoke-virtual {v1, v8}, Lcom/chase/sig/android/view/k$a;->d(I)Lcom/chase/sig/android/view/k;

    move-result-object v0

    goto :goto_0

    .line 53
    :pswitch_data_0
    .packed-switch 0xc
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
