.class public Lcom/chase/sig/android/activity/wire/WireDetailActivity;
.super Lcom/chase/sig/android/activity/c;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/wire/WireDetailActivity$1;,
        Lcom/chase/sig/android/activity/wire/WireDetailActivity$b;,
        Lcom/chase/sig/android/activity/wire/WireDetailActivity$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/activity/c",
        "<",
        "Lcom/chase/sig/android/domain/WireTransaction;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/chase/sig/android/activity/c;-><init>()V

    .line 100
    return-void
.end method


# virtual methods
.method public final a(Lcom/chase/sig/android/activity/DetailResource;)I
    .locals 2
    .parameter

    .prologue
    .line 56
    sget-object v0, Lcom/chase/sig/android/activity/wire/WireDetailActivity$1;->a:[I

    invoke-virtual {p1}, Lcom/chase/sig/android/activity/DetailResource;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 80
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 58
    :pswitch_0
    const v0, 0x7f07012c

    goto :goto_0

    .line 60
    :pswitch_1
    const v0, 0x7f07012e

    goto :goto_0

    .line 62
    :pswitch_2
    const v0, 0x7f07012f

    goto :goto_0

    .line 64
    :pswitch_3
    const v0, 0x7f070130

    goto :goto_0

    .line 66
    :pswitch_4
    const v0, 0x7f070131

    goto :goto_0

    .line 68
    :pswitch_5
    const v0, 0x7f070132

    goto :goto_0

    .line 70
    :pswitch_6
    const v0, 0x7f070133

    goto :goto_0

    .line 72
    :pswitch_7
    const v0, 0x7f070085

    goto :goto_0

    .line 74
    :pswitch_8
    const v0, 0x7f070135

    goto :goto_0

    .line 76
    :pswitch_9
    const v0, 0x7f070134

    goto :goto_0

    .line 78
    :pswitch_a
    const v0, 0x7f070136

    goto :goto_0

    .line 56
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method protected final a()V
    .locals 0

    .prologue
    .line 45
    invoke-super {p0}, Lcom/chase/sig/android/activity/c;->a()V

    .line 46
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1
    .parameter

    .prologue
    .line 22
    const v0, 0x7f0300c6

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/wire/WireDetailActivity;->b(I)V

    .line 23
    const v0, 0x7f07012b

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/wire/WireDetailActivity;->setTitle(I)V

    .line 24
    const-class v0, Lcom/chase/sig/android/activity/wire/WireDetailActivity$b;

    invoke-virtual {p0, p1, v0}, Lcom/chase/sig/android/activity/wire/WireDetailActivity;->a(Landroid/os/Bundle;Ljava/lang/Class;)V

    .line 25
    return-void
.end method

.method protected final synthetic a(Lcom/chase/sig/android/view/detail/DetailView;Lcom/chase/sig/android/domain/Transaction;)V
    .locals 5
    .parameter
    .parameter

    .prologue
    .line 18
    check-cast p2, Lcom/chase/sig/android/domain/WireTransaction;

    const/16 v0, 0xc

    new-array v0, v0, [Lcom/chase/sig/android/view/detail/a;

    const/4 v1, 0x0

    new-instance v2, Lcom/chase/sig/android/view/detail/DetailColoredValueRow;

    const-string v3, "Transaction Number"

    invoke-virtual {p2}, Lcom/chase/sig/android/domain/WireTransaction;->n()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/chase/sig/android/view/detail/DetailColoredValueRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Lcom/chase/sig/android/view/detail/DetailColoredValueRow;

    const-string v3, "Recipient"

    invoke-virtual {p2}, Lcom/chase/sig/android/domain/WireTransaction;->d()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/chase/sig/android/view/detail/DetailColoredValueRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-instance v2, Lcom/chase/sig/android/view/detail/DetailColoredValueRow;

    const-string v3, "From"

    invoke-virtual {p2}, Lcom/chase/sig/android/domain/WireTransaction;->m()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/chase/sig/android/view/detail/DetailColoredValueRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x3

    new-instance v2, Lcom/chase/sig/android/view/detail/DetailColoredValueRow;

    const-string v3, "Amount"

    invoke-virtual {p2}, Lcom/chase/sig/android/domain/WireTransaction;->e()Lcom/chase/sig/android/util/Dollar;

    move-result-object v4

    invoke-virtual {v4}, Lcom/chase/sig/android/util/Dollar;->h()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/chase/sig/android/view/detail/DetailColoredValueRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-instance v2, Lcom/chase/sig/android/view/detail/DetailColoredValueRow;

    const-string v3, "Date"

    invoke-virtual {p2}, Lcom/chase/sig/android/domain/WireTransaction;->q()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/chase/sig/android/util/s;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/chase/sig/android/view/detail/DetailColoredValueRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-instance v2, Lcom/chase/sig/android/view/detail/DetailColoredValueRow;

    const-string v3, "Status"

    invoke-virtual {p2}, Lcom/chase/sig/android/domain/WireTransaction;->t()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/chase/sig/android/view/detail/DetailColoredValueRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-instance v2, Lcom/chase/sig/android/view/detail/DetailColoredValueRow;

    const-string v3, "Fed Ref Number"

    invoke-virtual {p2}, Lcom/chase/sig/android/domain/WireTransaction;->b()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/chase/sig/android/view/detail/DetailColoredValueRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-instance v2, Lcom/chase/sig/android/view/detail/DetailColoredValueRow;

    const-string v3, "Frequency"

    invoke-virtual {p2}, Lcom/chase/sig/android/domain/WireTransaction;->i()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/chase/sig/android/activity/wire/WireDetailActivity;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/chase/sig/android/view/detail/DetailColoredValueRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/16 v1, 0x8

    new-instance v2, Lcom/chase/sig/android/view/detail/DetailColoredValueRow;

    const-string v3, "Remaining Wires"

    invoke-virtual {p2}, Lcom/chase/sig/android/domain/WireTransaction;->w()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/chase/sig/android/view/detail/DetailColoredValueRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/chase/sig/android/domain/WireTransaction;->l()Z

    move-result v3

    iput-boolean v3, v2, Lcom/chase/sig/android/view/detail/a;->j:Z

    aput-object v2, v0, v1

    const/16 v1, 0x9

    new-instance v2, Lcom/chase/sig/android/view/detail/DetailColoredValueRow;

    const-string v3, "Message To Recipient"

    invoke-virtual {p2}, Lcom/chase/sig/android/domain/WireTransaction;->f()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/chase/sig/android/view/detail/DetailColoredValueRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/16 v1, 0xa

    new-instance v2, Lcom/chase/sig/android/view/detail/DetailColoredValueRow;

    const-string v3, "To Recipient\'s Bank"

    invoke-virtual {p2}, Lcom/chase/sig/android/domain/WireTransaction;->h()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/chase/sig/android/view/detail/DetailColoredValueRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    const/16 v1, 0xb

    new-instance v2, Lcom/chase/sig/android/view/detail/DetailColoredValueRow;

    const-string v3, "Memo"

    invoke-virtual {p2}, Lcom/chase/sig/android/domain/WireTransaction;->o()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/chase/sig/android/view/detail/DetailColoredValueRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v0, v1

    invoke-virtual {p1, v0}, Lcom/chase/sig/android/view/detail/DetailView;->setRows([Lcom/chase/sig/android/view/detail/a;)V

    return-void
.end method

.method protected final b()Lcom/chase/sig/android/service/movemoney/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/chase/sig/android/service/movemoney/d",
            "<",
            "Lcom/chase/sig/android/domain/WireTransaction;",
            ">;"
        }
    .end annotation

    .prologue
    .line 50
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/wire/WireDetailActivity;->A()Lcom/chase/sig/android/service/n;

    invoke-static {}, Lcom/chase/sig/android/service/n;->f()Lcom/chase/sig/android/service/wire/a;

    move-result-object v0

    return-object v0
.end method

.method protected final c()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/chase/sig/android/activity/ae",
            "<*",
            "Ljava/lang/Boolean;",
            "**>;>;"
        }
    .end annotation

    .prologue
    .line 95
    const-class v0, Lcom/chase/sig/android/activity/wire/WireDetailActivity$a;

    return-object v0
.end method

.method protected final d()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/chase/sig/android/activity/eb;",
            ">;"
        }
    .end annotation

    .prologue
    .line 85
    const-class v0, Lcom/chase/sig/android/activity/wire/WireCompleteActivity;

    return-object v0
.end method

.method protected final e()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/chase/sig/android/activity/eb;",
            ">;"
        }
    .end annotation

    .prologue
    .line 89
    const-class v0, Lcom/chase/sig/android/activity/WireEditActivity;

    return-object v0
.end method
