.class public Lcom/chase/sig/android/activity/wire/WireEditVerifyActivity;
.super Lcom/chase/sig/android/activity/k;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/wire/WireEditVerifyActivity$a;,
        Lcom/chase/sig/android/activity/wire/WireEditVerifyActivity$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/activity/k",
        "<",
        "Lcom/chase/sig/android/domain/WireTransaction;",
        ">;"
    }
.end annotation


# instance fields
.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/chase/sig/android/activity/k;-><init>()V

    .line 80
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 23
    const v0, 0x7f07012d

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/wire/WireEditVerifyActivity;->setTitle(I)V

    .line 24
    const v0, 0x7f0300c7

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/wire/WireEditVerifyActivity;->b(I)V

    .line 25
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/wire/WireEditVerifyActivity;->h()V

    .line 26
    const v0, 0x7f0902d1

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/wire/WireEditVerifyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const v1, 0x7f070121

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 28
    const v0, 0x7f0902d2

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/wire/WireEditVerifyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const v1, 0x7f070120

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 31
    const-string v0, "WIRE_AGREEMENT_MESSAGE"

    invoke-static {p1, v0}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 32
    const-string v0, "WIRE_AGREEMENT_MESSAGE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/activity/wire/WireEditVerifyActivity;->d:Ljava/lang/String;

    .line 34
    :cond_0
    return-void
.end method

.method protected final a(Z)V
    .locals 8
    .parameter

    .prologue
    const/4 v7, 0x0

    .line 38
    iget-object v0, p0, Lcom/chase/sig/android/activity/wire/WireEditVerifyActivity;->a:Lcom/chase/sig/android/domain/Transaction;

    check-cast v0, Lcom/chase/sig/android/domain/WireTransaction;

    .line 40
    const v1, 0x7f0902d0

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/wire/WireEditVerifyActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/view/detail/DetailView;

    .line 41
    const/16 v2, 0x9

    new-array v2, v2, [Lcom/chase/sig/android/view/detail/a;

    new-instance v3, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v4, "Recipient"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/WireTransaction;->d()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v2, v7

    const/4 v3, 0x1

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "From"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/WireTransaction;->m()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    const/4 v3, 0x2

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Amount"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/WireTransaction;->e()Lcom/chase/sig/android/util/Dollar;

    move-result-object v6

    invoke-virtual {v6}, Lcom/chase/sig/android/util/Dollar;->h()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    const/4 v3, 0x3

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Date"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/WireTransaction;->q()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/chase/sig/android/util/s;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    const/4 v3, 0x4

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Frequency"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/WireTransaction;->i()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/chase/sig/android/activity/wire/WireEditVerifyActivity;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/wire/WireEditVerifyActivity;->b()Z

    move-result v5

    iput-boolean v5, v4, Lcom/chase/sig/android/view/detail/a;->j:Z

    aput-object v4, v2, v3

    const/4 v3, 0x5

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Remaining Wires"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/WireTransaction;->w()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/wire/WireEditVerifyActivity;->b()Z

    move-result v5

    iput-boolean v5, v4, Lcom/chase/sig/android/view/detail/a;->j:Z

    aput-object v4, v2, v3

    const/4 v3, 0x6

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Message To Recipient"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/WireTransaction;->f()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    const/4 v3, 0x7

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "To Recipient\'s Bank"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/WireTransaction;->h()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    const/16 v3, 0x8

    new-instance v4, Lcom/chase/sig/android/view/detail/DetailRow;

    const-string v5, "Memo"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/WireTransaction;->o()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v5, v0}, Lcom/chase/sig/android/view/detail/DetailRow;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/view/detail/DetailView;->setRows([Lcom/chase/sig/android/view/detail/a;)V

    .line 53
    const v0, 0x7f0902d2

    invoke-virtual {p0, v7}, Lcom/chase/sig/android/activity/wire/WireEditVerifyActivity;->i(I)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/wire/WireEditVerifyActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 55
    const v0, 0x7f0902d1

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/wire/WireEditVerifyActivity;->B()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/wire/WireEditVerifyActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 57
    return-void
.end method

.method protected final c()Lcom/chase/sig/android/service/movemoney/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/chase/sig/android/service/movemoney/d",
            "<",
            "Lcom/chase/sig/android/domain/WireTransaction;",
            ">;"
        }
    .end annotation

    .prologue
    .line 69
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/wire/WireEditVerifyActivity;->A()Lcom/chase/sig/android/service/n;

    invoke-static {}, Lcom/chase/sig/android/service/n;->f()Lcom/chase/sig/android/service/wire/a;

    move-result-object v0

    return-object v0
.end method

.method protected final d()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/chase/sig/android/activity/wire/WireCompleteActivity;",
            ">;"
        }
    .end annotation

    .prologue
    .line 74
    const-class v0, Lcom/chase/sig/android/activity/wire/WireCompleteActivity;

    return-object v0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 3
    .parameter

    .prologue
    .line 90
    packed-switch p1, :pswitch_data_0

    .line 97
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/k;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    .line 92
    :pswitch_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/wire/WireEditVerifyActivity;->d:Ljava/lang/String;

    const-class v1, Lcom/chase/sig/android/activity/wire/WireEditVerifyActivity$b;

    const-class v2, Lcom/chase/sig/android/activity/wire/WireEditVerifyActivity$a;

    invoke-static {p0, v0, v1, v2}, Lcom/chase/sig/android/activity/wire/f;->a(Lcom/chase/sig/android/activity/eb;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    .line 95
    :pswitch_1
    iget-object v0, p0, Lcom/chase/sig/android/activity/wire/WireEditVerifyActivity;->d:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/chase/sig/android/activity/wire/f;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    .line 90
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 61
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/k;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 62
    iget-object v0, p0, Lcom/chase/sig/android/activity/wire/WireEditVerifyActivity;->d:Ljava/lang/String;

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    const-string v0, "WIRE_AGREEMENT_MESSAGE"

    iget-object v1, p0, Lcom/chase/sig/android/activity/wire/WireEditVerifyActivity;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    :cond_0
    return-void
.end method
