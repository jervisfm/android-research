.class public Lcom/chase/sig/android/activity/wire/WireHomeActivity;
.super Lcom/chase/sig/android/activity/ai;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/activity/fk$e;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/chase/sig/android/activity/ai;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 26
    const v0, 0x7f0300d2

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/wire/WireHomeActivity;->b(I)V

    .line 27
    const v0, 0x7f070127

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/wire/WireHomeActivity;->setTitle(I)V

    .line 29
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->d:Lcom/chase/sig/android/service/SplashResponse;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->d:Lcom/chase/sig/android/service/SplashResponse;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/SplashResponse;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 30
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->d:Lcom/chase/sig/android/service/SplashResponse;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/SplashResponse;->q()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/activity/wire/WireHomeActivity;->e(Ljava/lang/String;)V

    .line 48
    :goto_0
    return-void

    .line 32
    :cond_0
    const v0, 0x7f0902fa

    new-instance v1, Lcom/chase/sig/android/activity/wire/j;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/activity/wire/j;-><init>(Lcom/chase/sig/android/activity/wire/WireHomeActivity;)V

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/wire/WireHomeActivity;->a(ILandroid/view/View$OnClickListener;)V

    .line 46
    const v0, 0x7f0902fb

    const-class v1, Lcom/chase/sig/android/activity/wire/WireHistoryActivity;

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/activity/wire/WireHomeActivity;->a(Ljava/lang/Class;)Lcom/chase/sig/android/activity/a/f;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/activity/wire/WireHomeActivity;->a(ILandroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 4
    .parameter

    .prologue
    .line 51
    invoke-super {p0, p1}, Lcom/chase/sig/android/activity/ai;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    .line 53
    if-nez p1, :cond_0

    .line 54
    new-instance v0, Lcom/chase/sig/android/view/k$a;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/view/k$a;-><init>(Landroid/content/Context;)V

    .line 55
    const v1, 0x7f070281

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/k$a;->b(I)Lcom/chase/sig/android/view/k$a;

    move-result-object v1

    const-string v2, "OK"

    new-instance v3, Lcom/chase/sig/android/activity/a/b;

    invoke-direct {v3}, Lcom/chase/sig/android/activity/a/b;-><init>()V

    invoke-virtual {v1, v2, v3}, Lcom/chase/sig/android/view/k$a;->a(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    .line 57
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/k$a;->d(I)Lcom/chase/sig/android/view/k;

    move-result-object v0

    .line 60
    :cond_0
    return-object v0
.end method
