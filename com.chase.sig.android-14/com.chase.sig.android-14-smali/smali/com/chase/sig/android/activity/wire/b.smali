.class final Lcom/chase/sig/android/activity/wire/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/wire/WireAddStartActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/wire/WireAddStartActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 119
    iput-object p1, p0, Lcom/chase/sig/android/activity/wire/b;->a:Lcom/chase/sig/android/activity/wire/WireAddStartActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 5
    .parameter

    .prologue
    .line 123
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/wire/b;->a:Lcom/chase/sig/android/activity/wire/WireAddStartActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 124
    new-instance v1, Lcom/chase/sig/android/domain/WireTransaction;

    invoke-direct {v1}, Lcom/chase/sig/android/domain/WireTransaction;-><init>()V

    .line 126
    iget-object v0, p0, Lcom/chase/sig/android/activity/wire/b;->a:Lcom/chase/sig/android/activity/wire/WireAddStartActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->b(Lcom/chase/sig/android/activity/wire/WireAddStartActivity;)Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v0

    const-string v2, "AMOUNT"

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/view/detail/DetailView;->c(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/util/Dollar;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/WireTransaction;->a(Lcom/chase/sig/android/util/Dollar;)V

    .line 127
    iget-object v0, p0, Lcom/chase/sig/android/activity/wire/b;->a:Lcom/chase/sig/android/activity/wire/WireAddStartActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->c(Lcom/chase/sig/android/activity/wire/WireAddStartActivity;)Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v0

    const-string v2, "DATE"

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/view/detail/DetailView;->c(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->t(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->b(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/WireTransaction;->n(Ljava/lang/String;)V

    .line 131
    iget-object v0, p0, Lcom/chase/sig/android/activity/wire/b;->a:Lcom/chase/sig/android/activity/wire/WireAddStartActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->d(Lcom/chase/sig/android/activity/wire/WireAddStartActivity;)Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v0

    const-string v2, "MESSAGE_TO_RECIPIENT"

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/view/detail/DetailView;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->s(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/WireTransaction;->e(Ljava/lang/String;)V

    .line 133
    iget-object v0, p0, Lcom/chase/sig/android/activity/wire/b;->a:Lcom/chase/sig/android/activity/wire/WireAddStartActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->e(Lcom/chase/sig/android/activity/wire/WireAddStartActivity;)Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v0

    const-string v2, "TO_RECIPIENT_BANK"

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/view/detail/DetailView;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->s(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/WireTransaction;->f(Ljava/lang/String;)V

    .line 135
    iget-object v0, p0, Lcom/chase/sig/android/activity/wire/b;->a:Lcom/chase/sig/android/activity/wire/WireAddStartActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->f(Lcom/chase/sig/android/activity/wire/WireAddStartActivity;)Lcom/chase/sig/android/view/detail/DetailView;

    move-result-object v0

    const-string v2, "MEMO"

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/view/detail/DetailView;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->s(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/WireTransaction;->l(Ljava/lang/String;)V

    .line 137
    iget-object v0, p0, Lcom/chase/sig/android/activity/wire/b;->a:Lcom/chase/sig/android/activity/wire/WireAddStartActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->h(Lcom/chase/sig/android/activity/wire/WireAddStartActivity;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v2, p0, Lcom/chase/sig/android/activity/wire/b;->a:Lcom/chase/sig/android/activity/wire/WireAddStartActivity;

    invoke-static {v2}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->g(Lcom/chase/sig/android/activity/wire/WireAddStartActivity;)Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v2

    invoke-virtual {v2}, Lcom/chase/sig/android/view/JPSpinner;->getSelectedItemPosition()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/Payee;

    .line 138
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Payee;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/domain/WireTransaction;->v(Ljava/lang/String;)V

    .line 139
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Payee;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/WireTransaction;->a(Ljava/lang/String;)V

    .line 141
    iget-object v0, p0, Lcom/chase/sig/android/activity/wire/b;->a:Lcom/chase/sig/android/activity/wire/WireAddStartActivity;

    invoke-static {v0}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->j(Lcom/chase/sig/android/activity/wire/WireAddStartActivity;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v2, p0, Lcom/chase/sig/android/activity/wire/b;->a:Lcom/chase/sig/android/activity/wire/WireAddStartActivity;

    invoke-static {v2}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->i(Lcom/chase/sig/android/activity/wire/WireAddStartActivity;)Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v2

    invoke-virtual {v2}, Lcom/chase/sig/android/view/JPSpinner;->getSelectedItemPosition()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/IAccount;

    .line 143
    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/domain/WireTransaction;->b(Ljava/lang/String;)V

    .line 144
    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/domain/WireTransaction;->o(Ljava/lang/String;)V

    .line 145
    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->y()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/WireTransaction;->p(Ljava/lang/String;)V

    .line 147
    iget-object v0, p0, Lcom/chase/sig/android/activity/wire/b;->a:Lcom/chase/sig/android/activity/wire/WireAddStartActivity;

    const-class v2, Lcom/chase/sig/android/activity/wire/WireAddStartActivity$b;

    const/4 v3, 0x1

    new-array v3, v3, [Lcom/chase/sig/android/domain/WireTransaction;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-virtual {v0, v2, v3}, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 149
    :cond_0
    return-void
.end method
