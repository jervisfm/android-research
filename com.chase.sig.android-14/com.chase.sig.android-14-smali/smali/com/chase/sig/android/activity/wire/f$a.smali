.class public abstract Lcom/chase/sig/android/activity/wire/f$a;
.super Lcom/chase/sig/android/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/activity/wire/f;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/chase/sig/android/activity/eb;",
        ">",
        "Lcom/chase/sig/android/d",
        "<TT;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/chase/sig/android/service/content/ContentResponse;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/chase/sig/android/d;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 71
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/eb;->A()Lcom/chase/sig/android/service/n;

    invoke-static {}, Lcom/chase/sig/android/service/n;->f()Lcom/chase/sig/android/service/wire/a;

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    invoke-static {}, Lcom/chase/sig/android/service/af;->a()Lcom/chase/sig/android/service/n;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/service/n;->p()Lcom/chase/sig/android/service/content/a;

    const-string v0, "wireAgreement.txt"

    invoke-static {v0}, Lcom/chase/sig/android/service/content/a;->a(Ljava/lang/String;)Lcom/chase/sig/android/service/content/ContentResponse;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 3
    .parameter

    .prologue
    .line 71
    check-cast p1, Lcom/chase/sig/android/service/content/ContentResponse;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/content/ContentResponse;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    invoke-virtual {p1}, Lcom/chase/sig/android/service/content/ContentResponse;->g()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/eb;->c(Ljava/util/List;)V

    :goto_0
    iget-object v0, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/eb;->u()V

    return-void

    :cond_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    const-class v2, Lcom/chase/sig/android/activity/ManagedContentActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "content"

    invoke-virtual {p1}, Lcom/chase/sig/android/service/content/ContentResponse;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/chase/sig/android/activity/ae;->b:Lcom/chase/sig/android/activity/eb;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/activity/eb;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
