.class public final Lcom/chase/sig/android/activity/wire/f;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/activity/wire/f$a;
    }
.end annotation


# direct methods
.method static a(Landroid/content/Context;Ljava/lang/String;)Landroid/app/Dialog;
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 23
    new-instance v0, Lcom/chase/sig/android/view/k$a;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/view/k$a;-><init>(Landroid/content/Context;)V

    .line 24
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/chase/sig/android/view/k$a;->i:Z

    .line 25
    const-string v1, "Close"

    new-instance v2, Lcom/chase/sig/android/activity/a/b;

    invoke-direct {v2}, Lcom/chase/sig/android/activity/a/b;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/chase/sig/android/view/k$a;->a(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    .line 26
    const-string v1, "Wire Agreement"

    iput-object v1, v0, Lcom/chase/sig/android/view/k$a;->b:Ljava/lang/CharSequence;

    .line 28
    invoke-virtual {v0, p1}, Lcom/chase/sig/android/view/k$a;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/k$a;

    .line 29
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/k$a;->d(I)Lcom/chase/sig/android/view/k;

    move-result-object v0

    return-object v0
.end method

.method static a(Lcom/chase/sig/android/activity/eb;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)Landroid/app/Dialog;
    .locals 3
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/chase/sig/android/activity/eb;",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/chase/sig/android/activity/ae",
            "<*",
            "Ljava/lang/Void;",
            "**>;>;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/chase/sig/android/activity/ae",
            "<*",
            "Ljava/lang/Void;",
            "**>;>;)",
            "Landroid/app/Dialog;"
        }
    .end annotation

    .prologue
    .line 36
    new-instance v0, Lcom/chase/sig/android/view/k$a;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/view/k$a;-><init>(Landroid/content/Context;)V

    .line 38
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/chase/sig/android/view/k$a;->i:Z

    .line 40
    const-string v1, "Schedule Wire"

    new-instance v2, Lcom/chase/sig/android/activity/wire/g;

    invoke-direct {v2, p0, p2}, Lcom/chase/sig/android/activity/wire/g;-><init>(Lcom/chase/sig/android/activity/eb;Ljava/lang/Class;)V

    invoke-virtual {v0, v1, v2}, Lcom/chase/sig/android/view/k$a;->a(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    .line 46
    invoke-virtual {p0}, Lcom/chase/sig/android/activity/eb;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v1

    iget-object v1, v1, Lcom/chase/sig/android/domain/o;->b:Lcom/chase/sig/android/domain/g;

    invoke-interface {v1}, Lcom/chase/sig/android/domain/g;->A()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 48
    const-string v1, "See Wire Agreement"

    new-instance v2, Lcom/chase/sig/android/activity/wire/h;

    invoke-direct {v2, p1, p0, p3}, Lcom/chase/sig/android/activity/wire/h;-><init>(Ljava/lang/String;Lcom/chase/sig/android/activity/eb;Ljava/lang/Class;)V

    iput-object v1, v0, Lcom/chase/sig/android/view/k$a;->j:Ljava/lang/String;

    iput-object v2, v0, Lcom/chase/sig/android/view/k$a;->f:Landroid/content/DialogInterface$OnClickListener;

    .line 59
    :cond_0
    const-string v1, "Cancel Wire"

    new-instance v2, Lcom/chase/sig/android/activity/wire/i;

    invoke-direct {v2, p0}, Lcom/chase/sig/android/activity/wire/i;-><init>(Lcom/chase/sig/android/activity/eb;)V

    invoke-virtual {v0, v1, v2}, Lcom/chase/sig/android/view/k$a;->b(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;

    .line 65
    const-string v1, "Note: By selecting \'Schedule Wire\' you agree your wire will be governed- by the terms of the Wire Agreement."

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/k$a;->a(Ljava/lang/String;)Lcom/chase/sig/android/view/k$a;

    .line 68
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/k$a;->d(I)Lcom/chase/sig/android/view/k;

    move-result-object v0

    return-object v0
.end method
