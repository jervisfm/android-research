.class final Lcom/chase/sig/android/activity/wire/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/activity/wire/WireHomeActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/activity/wire/WireHomeActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 32
    iput-object p1, p0, Lcom/chase/sig/android/activity/wire/j;->a:Lcom/chase/sig/android/activity/wire/WireHomeActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3
    .parameter

    .prologue
    .line 35
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    iget-object v0, p0, Lcom/chase/sig/android/activity/wire/j;->a:Lcom/chase/sig/android/activity/wire/WireHomeActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/wire/WireHomeActivity;->r()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->b:Lcom/chase/sig/android/domain/g;

    invoke-interface {v0}, Lcom/chase/sig/android/domain/g;->j()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 37
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 38
    iget-object v0, p0, Lcom/chase/sig/android/activity/wire/j;->a:Lcom/chase/sig/android/activity/wire/WireHomeActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/wire/WireHomeActivity;->showDialog(I)V

    .line 43
    :goto_0
    return-void

    .line 40
    :cond_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/chase/sig/android/activity/wire/j;->a:Lcom/chase/sig/android/activity/wire/WireHomeActivity;

    invoke-virtual {v1}, Lcom/chase/sig/android/activity/wire/WireHomeActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/chase/sig/android/activity/wire/WireAddStartActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 41
    iget-object v1, p0, Lcom/chase/sig/android/activity/wire/j;->a:Lcom/chase/sig/android/activity/wire/WireHomeActivity;

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/activity/wire/WireHomeActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
