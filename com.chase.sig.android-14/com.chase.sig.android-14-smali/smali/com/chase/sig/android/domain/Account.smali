.class public Lcom/chase/sig/android/domain/Account;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/domain/IAccount;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/domain/Account$Value;
    }
.end annotation


# static fields
.field private static final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private accountDetailsList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/f;",
            ">;"
        }
    .end annotation
.end field

.field private details:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private disclosureIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private id:Ljava/lang/String;

.field private isBCCControl:Z

.field private isBCCEmployee:Z

.field private isBCCOfficer:Z

.field private isOmni:Z

.field private mask:Ljava/lang/String;

.field private nickname:Ljava/lang/String;

.field private privileges:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private standInAsofDate:Ljava/lang/String;

.field private subAccountsList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/IAccount;",
            ">;"
        }
    .end annotation
.end field

.field private type:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 36
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "BRC"

    aput-object v1, v0, v3

    const-string v1, "BCL"

    aput-object v1, v0, v4

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/chase/sig/android/domain/Account;->a:Ljava/util/List;

    .line 39
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "ALA"

    aput-object v1, v0, v3

    const-string v1, "ALS"

    aput-object v1, v0, v4

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/chase/sig/android/domain/Account;->b:Ljava/util/List;

    .line 42
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "HMG"

    aput-object v1, v0, v3

    const-string v1, "HEO"

    aput-object v1, v0, v4

    const-string v1, "HEL"

    aput-object v1, v0, v5

    const-string v1, "RCA"

    aput-object v1, v0, v6

    const-string v1, "ILA"

    aput-object v1, v0, v7

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/chase/sig/android/domain/Account;->c:Ljava/util/List;

    .line 46
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "PAC"

    aput-object v1, v0, v3

    const-string v1, "BAC"

    aput-object v1, v0, v4

    const-string v1, "OLC"

    aput-object v1, v0, v5

    const-string v1, "PCC"

    aput-object v1, v0, v6

    const-string v1, "BCC"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "SCC"

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/chase/sig/android/domain/Account;->d:Ljava/util/List;

    .line 53
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "BRK"

    aput-object v1, v0, v3

    const-string v1, "WRP"

    aput-object v1, v0, v4

    const-string v1, "BR2"

    aput-object v1, v0, v5

    const-string v1, "WR2"

    aput-object v1, v0, v6

    const-string v1, "TRS"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "JPF"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "ANU"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "OMN"

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/chase/sig/android/domain/Account;->e:Ljava/util/List;

    .line 59
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "CHK"

    aput-object v1, v0, v3

    const-string v1, "AMA"

    aput-object v1, v0, v4

    const-string v1, "SAV"

    aput-object v1, v0, v5

    const-string v1, "MMA"

    aput-object v1, v0, v6

    const-string v1, "CDA"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "IRA"

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/chase/sig/android/domain/Account;->f:Ljava/util/List;

    .line 64
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "OFF"

    aput-object v1, v0, v3

    const-string v1, "MAN"

    aput-object v1, v0, v4

    const-string v1, "MUT"

    aput-object v1, v0, v5

    const-string v1, "MAR"

    aput-object v1, v0, v6

    const-string v1, "BR2"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "WR2"

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/chase/sig/android/domain/Account;->g:Ljava/util/List;

    .line 69
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "CRF"

    aput-object v1, v0, v3

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/chase/sig/android/domain/Account;->h:Ljava/util/List;

    .line 72
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "PPA"

    aput-object v1, v0, v3

    const-string v1, "PPX"

    aput-object v1, v0, v4

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/chase/sig/android/domain/Account;->i:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    return-void
.end method


# virtual methods
.method public final A()Z
    .locals 2

    .prologue
    .line 401
    iget-object v0, p0, Lcom/chase/sig/android/domain/Account;->type:Ljava/lang/String;

    const-string v1, "RWA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/domain/Account;->type:Ljava/lang/String;

    const-string v1, "ATM"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/Account;->e()Ljava/util/Hashtable;

    move-result-object v0

    const-string v1, "rewardsProgramName"

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 403
    const/4 v0, 0x1

    .line 406
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final B()Ljava/lang/CharSequence;
    .locals 4

    .prologue
    .line 411
    const-string v0, "%s (%s)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/chase/sig/android/domain/Account;->nickname:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/chase/sig/android/domain/Account;->mask:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final C()Z
    .locals 1

    .prologue
    .line 416
    invoke-virtual {p0}, Lcom/chase/sig/android/domain/Account;->G()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final D()Z
    .locals 2

    .prologue
    .line 421
    const-string v0, "CRF"

    iget-object v1, p0, Lcom/chase/sig/android/domain/Account;->type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final E()Z
    .locals 2

    .prologue
    .line 426
    const-string v0, "SCC"

    iget-object v1, p0, Lcom/chase/sig/android/domain/Account;->type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final F()Ljava/lang/String;
    .locals 3

    .prologue
    .line 431
    const/4 v0, 0x0

    .line 432
    const-string v1, "CRF"

    iget-object v2, p0, Lcom/chase/sig/android/domain/Account;->type:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 433
    iget-object v1, p0, Lcom/chase/sig/android/domain/Account;->details:Ljava/util/Hashtable;

    if-eqz v1, :cond_0

    .line 434
    iget-object v0, p0, Lcom/chase/sig/android/domain/Account;->details:Ljava/util/Hashtable;

    const-string v1, "outstanding"

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 437
    :cond_0
    return-object v0
.end method

.method public final G()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 442
    invoke-virtual {p0}, Lcom/chase/sig/android/domain/Account;->s()I

    move-result v1

    if-eq v0, v1, :cond_0

    const/16 v1, 0x8

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/Account;->s()I

    move-result v2

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final H()Z
    .locals 1

    .prologue
    .line 447
    const-string v0, "feature_positions"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/domain/Account;->f(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final I()Z
    .locals 2

    .prologue
    .line 452
    invoke-virtual {p0}, Lcom/chase/sig/android/domain/Account;->e()Ljava/util/Hashtable;

    move-result-object v0

    const-string v1, "txnCounter"

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Withdrawals this period"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/domain/Account;->c(Ljava/lang/String;)Lcom/chase/sig/android/domain/f;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 454
    :cond_0
    const/4 v0, 0x1

    .line 456
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final J()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 472
    iget-object v0, p0, Lcom/chase/sig/android/domain/Account;->privileges:Ljava/util/Set;

    return-object v0
.end method

.method public final K()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/f;",
            ">;"
        }
    .end annotation

    .prologue
    .line 482
    iget-object v0, p0, Lcom/chase/sig/android/domain/Account;->accountDetailsList:Ljava/util/List;

    return-object v0
.end method

.method public final L()Ljava/lang/String;
    .locals 1

    .prologue
    .line 533
    iget-object v0, p0, Lcom/chase/sig/android/domain/Account;->standInAsofDate:Ljava/lang/String;

    return-object v0
.end method

.method public final M()Z
    .locals 2

    .prologue
    .line 542
    invoke-virtual {p0}, Lcom/chase/sig/android/domain/Account;->s()I

    move-result v0

    const/16 v1, 0xa

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/chase/sig/android/domain/Account;->nickname:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Z)Ljava/lang/String;
    .locals 4
    .parameter

    .prologue
    .line 135
    iget-object v0, p0, Lcom/chase/sig/android/domain/Account;->disclosureIds:Ljava/util/Set;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/domain/Account;->disclosureIds:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-lez v0, :cond_0

    if-nez p1, :cond_0

    .line 137
    const-string v0, "%s* (%s)"

    .line 142
    :goto_0
    const/4 v1, 0x2

    new-array v2, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/chase/sig/android/domain/Account;->nickname:Ljava/lang/String;

    if-nez v1, :cond_1

    const-string v1, ""

    :goto_1
    aput-object v1, v2, v3

    const/4 v1, 0x1

    iget-object v3, p0, Lcom/chase/sig/android/domain/Account;->mask:Ljava/lang/String;

    aput-object v3, v2, v1

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 139
    :cond_0
    const-string v0, "%s (%s)"

    goto :goto_0

    .line 142
    :cond_1
    iget-object v1, p0, Lcom/chase/sig/android/domain/Account;->nickname:Ljava/lang/String;

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 157
    iput-object p1, p0, Lcom/chase/sig/android/domain/Account;->mask:Ljava/lang/String;

    .line 158
    return-void
.end method

.method public final a(Ljava/util/Hashtable;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 264
    iput-object p1, p0, Lcom/chase/sig/android/domain/Account;->details:Ljava/util/Hashtable;

    .line 265
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/IAccount;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 362
    iput-object p1, p0, Lcom/chase/sig/android/domain/Account;->subAccountsList:Ljava/util/List;

    .line 363
    return-void
.end method

.method public final a(Ljava/util/Set;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 467
    iput-object p1, p0, Lcom/chase/sig/android/domain/Account;->disclosureIds:Ljava/util/Set;

    .line 468
    return-void
.end method

.method public final varargs a([Ljava/lang/String;)Z
    .locals 5
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 304
    array-length v2, p1

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, p1, v1

    .line 305
    iget-object v4, p0, Lcom/chase/sig/android/domain/Account;->type:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 306
    const/4 v0, 0x1

    .line 310
    :cond_0
    return v0

    .line 304
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/chase/sig/android/domain/Account;->id:Ljava/lang/String;

    return-object v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 172
    iput-object p1, p0, Lcom/chase/sig/android/domain/Account;->id:Ljava/lang/String;

    .line 173
    return-void
.end method

.method public final b(Ljava/util/List;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/f;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 487
    iput-object p1, p0, Lcom/chase/sig/android/domain/Account;->accountDetailsList:Ljava/util/List;

    .line 488
    return-void
.end method

.method public final b(Ljava/util/Set;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 477
    iput-object p1, p0, Lcom/chase/sig/android/domain/Account;->privileges:Ljava/util/Set;

    .line 478
    return-void
.end method

.method public final b(Z)V
    .locals 0
    .parameter

    .prologue
    .line 244
    iput-boolean p1, p0, Lcom/chase/sig/android/domain/Account;->isBCCOfficer:Z

    .line 245
    return-void
.end method

.method public final c(Ljava/lang/String;)Lcom/chase/sig/android/domain/f;
    .locals 3
    .parameter

    .prologue
    .line 186
    iget-object v0, p0, Lcom/chase/sig/android/domain/Account;->accountDetailsList:Ljava/util/List;

    .line 188
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/f;

    .line 189
    invoke-interface {v0}, Lcom/chase/sig/android/domain/f;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 194
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/chase/sig/android/domain/Account;->mask:Ljava/lang/String;

    return-object v0
.end method

.method public final c(Z)V
    .locals 0
    .parameter

    .prologue
    .line 254
    iput-boolean p1, p0, Lcom/chase/sig/android/domain/Account;->isBCCControl:Z

    .line 255
    return-void
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/chase/sig/android/domain/Account;->type:Ljava/lang/String;

    return-object v0
.end method

.method public final d(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 220
    iput-object p1, p0, Lcom/chase/sig/android/domain/Account;->type:Ljava/lang/String;

    .line 221
    return-void
.end method

.method public final d(Z)V
    .locals 0
    .parameter

    .prologue
    .line 259
    iput-boolean p1, p0, Lcom/chase/sig/android/domain/Account;->isBCCEmployee:Z

    .line 260
    return-void
.end method

.method public final e()Ljava/util/Hashtable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 177
    iget-object v0, p0, Lcom/chase/sig/android/domain/Account;->details:Ljava/util/Hashtable;

    if-nez v0, :cond_0

    .line 178
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    .line 181
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/domain/Account;->details:Ljava/util/Hashtable;

    goto :goto_0
.end method

.method public final e(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 249
    iput-object p1, p0, Lcom/chase/sig/android/domain/Account;->nickname:Ljava/lang/String;

    .line 250
    return-void
.end method

.method public final e(Z)V
    .locals 0
    .parameter

    .prologue
    .line 352
    iput-boolean p1, p0, Lcom/chase/sig/android/domain/Account;->isOmni:Z

    .line 353
    return-void
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 199
    invoke-virtual {p0}, Lcom/chase/sig/android/domain/Account;->G()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "feature_activity"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/domain/Account;->f(Ljava/lang/String;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const-string v0, "mobile_activity"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/domain/Account;->f(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method protected final f(Ljava/lang/String;)Z
    .locals 1
    .parameter

    .prologue
    .line 492
    iget-object v0, p0, Lcom/chase/sig/android/domain/Account;->privileges:Ljava/util/Set;

    if-nez v0, :cond_0

    .line 493
    const/4 v0, 0x0

    .line 496
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/domain/Account;->privileges:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final g(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 537
    iput-object p1, p0, Lcom/chase/sig/android/domain/Account;->standInAsofDate:Ljava/lang/String;

    .line 538
    return-void
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 205
    iget-boolean v0, p0, Lcom/chase/sig/android/domain/Account;->isBCCControl:Z

    return v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 210
    iget-boolean v0, p0, Lcom/chase/sig/android/domain/Account;->isBCCEmployee:Z

    return v0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 215
    iget-boolean v0, p0, Lcom/chase/sig/android/domain/Account;->isBCCOfficer:Z

    return v0
.end method

.method public final j()Lcom/chase/sig/android/util/Dollar;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 225
    .line 226
    iget-object v0, p0, Lcom/chase/sig/android/domain/Account;->details:Ljava/util/Hashtable;

    if-eqz v0, :cond_1

    .line 227
    iget-object v0, p0, Lcom/chase/sig/android/domain/Account;->details:Ljava/util/Hashtable;

    const-string v2, "available"

    invoke-virtual {v0, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v2, v0

    .line 229
    :goto_0
    if-nez v2, :cond_0

    move-object v0, v1

    :goto_1
    return-object v0

    :cond_0
    new-instance v0, Lcom/chase/sig/android/util/Dollar;

    invoke-direct {v0, v2}, Lcom/chase/sig/android/util/Dollar;-><init>(Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    move-object v2, v1

    goto :goto_0
.end method

.method public final k()Lcom/chase/sig/android/util/Dollar;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 234
    .line 236
    iget-object v0, p0, Lcom/chase/sig/android/domain/Account;->details:Ljava/util/Hashtable;

    if-eqz v0, :cond_1

    .line 237
    iget-object v0, p0, Lcom/chase/sig/android/domain/Account;->details:Ljava/util/Hashtable;

    const-string v2, "current"

    invoke-virtual {v0, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v2, v0

    .line 239
    :goto_0
    if-nez v2, :cond_0

    move-object v0, v1

    :goto_1
    return-object v0

    :cond_0
    new-instance v0, Lcom/chase/sig/android/util/Dollar;

    invoke-direct {v0, v2}, Lcom/chase/sig/android/util/Dollar;-><init>(Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    move-object v2, v1

    goto :goto_0
.end method

.method public final l()Z
    .locals 2

    .prologue
    .line 269
    iget-object v0, p0, Lcom/chase/sig/android/domain/Account;->privileges:Ljava/util/Set;

    const-string v1, "mobile_blueprint"

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final m()Z
    .locals 2

    .prologue
    .line 274
    iget-object v0, p0, Lcom/chase/sig/android/domain/Account;->privileges:Ljava/util/Set;

    const-string v1, "feature_chasereceipts"

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final n()Z
    .locals 2

    .prologue
    .line 279
    iget-object v0, p0, Lcom/chase/sig/android/domain/Account;->privileges:Ljava/util/Set;

    const-string v1, "mobile_mortgage_bankruptcy"

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final o()Z
    .locals 2

    .prologue
    .line 284
    iget-object v0, p0, Lcom/chase/sig/android/domain/Account;->privileges:Ljava/util/Set;

    const-string v1, "mobile_internal_xfer"

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final p()Z
    .locals 2

    .prologue
    .line 289
    iget-object v0, p0, Lcom/chase/sig/android/domain/Account;->privileges:Ljava/util/Set;

    const-string v1, "mobile_internal_xfer_credit"

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final q()Z
    .locals 2

    .prologue
    .line 294
    iget-object v0, p0, Lcom/chase/sig/android/domain/Account;->privileges:Ljava/util/Set;

    const-string v1, "mobile_epay"

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final r()Z
    .locals 2

    .prologue
    .line 299
    iget-object v0, p0, Lcom/chase/sig/android/domain/Account;->privileges:Ljava/util/Set;

    const-string v1, "mobile_debitcardrewards"

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final s()I
    .locals 2

    .prologue
    .line 316
    sget-object v0, Lcom/chase/sig/android/domain/Account;->f:Ljava/util/List;

    iget-object v1, p0, Lcom/chase/sig/android/domain/Account;->type:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 317
    const/4 v0, 0x7

    .line 342
    :goto_0
    return v0

    .line 318
    :cond_0
    sget-object v0, Lcom/chase/sig/android/domain/Account;->d:Ljava/util/List;

    iget-object v1, p0, Lcom/chase/sig/android/domain/Account;->type:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 320
    iget-object v0, p0, Lcom/chase/sig/android/domain/Account;->type:Ljava/lang/String;

    const-string v1, "BCC"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/chase/sig/android/domain/Account;->isBCCEmployee:Z

    if-eqz v0, :cond_1

    .line 321
    const/4 v0, 0x3

    goto :goto_0

    .line 323
    :cond_1
    const/4 v0, 0x2

    goto :goto_0

    .line 326
    :cond_2
    sget-object v0, Lcom/chase/sig/android/domain/Account;->b:Ljava/util/List;

    iget-object v1, p0, Lcom/chase/sig/android/domain/Account;->type:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 327
    const/4 v0, 0x5

    goto :goto_0

    .line 328
    :cond_3
    sget-object v0, Lcom/chase/sig/android/domain/Account;->g:Ljava/util/List;

    iget-object v1, p0, Lcom/chase/sig/android/domain/Account;->type:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 329
    const/16 v0, 0x8

    goto :goto_0

    .line 330
    :cond_4
    sget-object v0, Lcom/chase/sig/android/domain/Account;->a:Ljava/util/List;

    iget-object v1, p0, Lcom/chase/sig/android/domain/Account;->type:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 331
    const/4 v0, 0x6

    goto :goto_0

    .line 332
    :cond_5
    sget-object v0, Lcom/chase/sig/android/domain/Account;->c:Ljava/util/List;

    iget-object v1, p0, Lcom/chase/sig/android/domain/Account;->type:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 333
    const/4 v0, 0x4

    goto :goto_0

    .line 334
    :cond_6
    sget-object v0, Lcom/chase/sig/android/domain/Account;->e:Ljava/util/List;

    iget-object v1, p0, Lcom/chase/sig/android/domain/Account;->type:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 335
    const/4 v0, 0x1

    goto :goto_0

    .line 336
    :cond_7
    sget-object v0, Lcom/chase/sig/android/domain/Account;->h:Ljava/util/List;

    iget-object v1, p0, Lcom/chase/sig/android/domain/Account;->type:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 337
    const/16 v0, 0x9

    goto :goto_0

    .line 338
    :cond_8
    sget-object v0, Lcom/chase/sig/android/domain/Account;->i:Ljava/util/List;

    iget-object v1, p0, Lcom/chase/sig/android/domain/Account;->type:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 339
    const/16 v0, 0xa

    goto :goto_0

    .line 342
    :cond_9
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final t()Z
    .locals 2

    .prologue
    .line 347
    iget-object v0, p0, Lcom/chase/sig/android/domain/Account;->privileges:Ljava/util/Set;

    const-string v1, "mobile_paybills"

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 501
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 502
    const-string v1, "Account [type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 503
    iget-object v1, p0, Lcom/chase/sig/android/domain/Account;->type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 504
    const-string v1, ", isOmni="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 505
    iget-boolean v1, p0, Lcom/chase/sig/android/domain/Account;->isOmni:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 506
    const-string v1, ", isBCCControl="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 507
    iget-boolean v1, p0, Lcom/chase/sig/android/domain/Account;->isBCCControl:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 508
    const-string v1, ", isBCCEmployee="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 509
    iget-boolean v1, p0, Lcom/chase/sig/android/domain/Account;->isBCCEmployee:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 510
    const-string v1, ", isBCCOfficer="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 511
    iget-boolean v1, p0, Lcom/chase/sig/android/domain/Account;->isBCCOfficer:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 512
    const-string v1, ", mask="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 513
    iget-object v1, p0, Lcom/chase/sig/android/domain/Account;->mask:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 514
    const-string v1, ", disclosureIds="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 515
    iget-object v1, p0, Lcom/chase/sig/android/domain/Account;->disclosureIds:Ljava/util/Set;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 516
    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 517
    iget-object v1, p0, Lcom/chase/sig/android/domain/Account;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 518
    const-string v1, ", nickname="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 519
    iget-object v1, p0, Lcom/chase/sig/android/domain/Account;->nickname:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 520
    const-string v1, ", subAccountsList="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 521
    iget-object v1, p0, Lcom/chase/sig/android/domain/Account;->subAccountsList:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 522
    const-string v1, ", accountDetailsList="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 523
    iget-object v1, p0, Lcom/chase/sig/android/domain/Account;->accountDetailsList:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 524
    const-string v1, ", details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 525
    iget-object v1, p0, Lcom/chase/sig/android/domain/Account;->details:Ljava/util/Hashtable;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 526
    const-string v1, ", privileges="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 527
    iget-object v1, p0, Lcom/chase/sig/android/domain/Account;->privileges:Ljava/util/Set;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 528
    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 529
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final u()Z
    .locals 1

    .prologue
    .line 357
    iget-boolean v0, p0, Lcom/chase/sig/android/domain/Account;->isOmni:Z

    return v0
.end method

.method public final v()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/IAccount;",
            ">;"
        }
    .end annotation

    .prologue
    .line 367
    iget-object v0, p0, Lcom/chase/sig/android/domain/Account;->subAccountsList:Ljava/util/List;

    return-object v0
.end method

.method public final w()Ljava/lang/CharSequence;
    .locals 4

    .prologue
    .line 372
    const-string v0, "%s (%s)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/chase/sig/android/domain/Account;->nickname:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/chase/sig/android/domain/Account;->mask:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final x()Lcom/chase/sig/android/domain/IAccount;
    .locals 3

    .prologue
    .line 378
    iget-boolean v0, p0, Lcom/chase/sig/android/domain/Account;->isBCCOfficer:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/chase/sig/android/domain/Account;->subAccountsList:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 379
    iget-object v0, p0, Lcom/chase/sig/android/domain/Account;->subAccountsList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/IAccount;

    .line 380
    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->g()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 386
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final y()Ljava/lang/CharSequence;
    .locals 4

    .prologue
    .line 391
    const-string v0, "(%s)"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/chase/sig/android/domain/Account;->mask:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final z()Z
    .locals 2

    .prologue
    .line 396
    iget-object v0, p0, Lcom/chase/sig/android/domain/Account;->type:Ljava/lang/String;

    const-string v1, "RWA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/domain/Account;->type:Ljava/lang/String;

    const-string v1, "ATM"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
