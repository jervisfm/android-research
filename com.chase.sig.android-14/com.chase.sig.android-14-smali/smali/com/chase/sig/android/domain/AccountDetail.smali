.class public Lcom/chase/sig/android/domain/AccountDetail;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private failed:Z

.field private id:Ljava/lang/String;

.field private loaded:Z

.field private values:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 28
    iput-object p1, p0, Lcom/chase/sig/android/domain/AccountDetail;->id:Ljava/lang/String;

    .line 29
    return-void
.end method

.method public final a(Ljava/util/Hashtable;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 44
    iput-object p1, p0, Lcom/chase/sig/android/domain/AccountDetail;->values:Ljava/util/Hashtable;

    .line 45
    return-void
.end method

.method public final a(Z)V
    .locals 0
    .parameter

    .prologue
    .line 20
    iput-boolean p1, p0, Lcom/chase/sig/android/domain/AccountDetail;->failed:Z

    .line 21
    return-void
.end method

.method public final b(Z)V
    .locals 0
    .parameter

    .prologue
    .line 36
    iput-boolean p1, p0, Lcom/chase/sig/android/domain/AccountDetail;->loaded:Z

    .line 37
    return-void
.end method
