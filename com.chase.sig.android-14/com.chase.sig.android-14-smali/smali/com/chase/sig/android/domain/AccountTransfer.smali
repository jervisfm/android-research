.class public Lcom/chase/sig/android/domain/AccountTransfer;
.super Lcom/chase/sig/android/domain/Transaction;
.source "SourceFile"


# instance fields
.field private processDate:Ljava/util/Date;

.field private sendOnDate:Ljava/lang/String;

.field private toAccount:Ljava/lang/String;

.field private toAccountNickName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/chase/sig/android/domain/Transaction;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/chase/sig/android/domain/AccountTransfer;->toAccount:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 20
    iput-object p1, p0, Lcom/chase/sig/android/domain/AccountTransfer;->toAccount:Ljava/lang/String;

    .line 21
    return-void
.end method

.method public final a_(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 28
    iput-object p1, p0, Lcom/chase/sig/android/domain/AccountTransfer;->sendOnDate:Ljava/lang/String;

    .line 29
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/chase/sig/android/domain/AccountTransfer;->sendOnDate:Ljava/lang/String;

    return-object v0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 32
    iput-object p1, p0, Lcom/chase/sig/android/domain/AccountTransfer;->toAccountNickName:Ljava/lang/String;

    .line 33
    return-void
.end method

.method public final d()Ljava/lang/String;
    .locals 4

    .prologue
    .line 48
    const-string v0, "%s %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/chase/sig/android/domain/AccountTransfer;->toAccountNickName:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/chase/sig/android/domain/Transaction;->toAccountNumber:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/chase/sig/android/domain/AccountTransfer;->toAccountNickName:Ljava/lang/String;

    return-object v0
.end method

.method public final e_()Ljava/lang/String;
    .locals 4

    .prologue
    .line 52
    const-string v0, "%s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/chase/sig/android/domain/Transaction;->fromAccountMask:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
