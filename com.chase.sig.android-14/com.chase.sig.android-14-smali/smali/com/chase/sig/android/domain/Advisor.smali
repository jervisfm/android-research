.class public Lcom/chase/sig/android/domain/Advisor;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private displayName:Ljava/lang/String;

.field private email:Ljava/lang/String;

.field private firstName:Ljava/lang/String;

.field private lastName:Ljava/lang/String;

.field private middleInitial:Ljava/lang/String;

.field private phone:Ljava/lang/String;

.field private relationship:Ljava/lang/String;

.field private support:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/chase/sig/android/domain/Advisor;->phone:Ljava/lang/String;

    .line 27
    iput-object p2, p0, Lcom/chase/sig/android/domain/Advisor;->email:Ljava/lang/String;

    .line 28
    iput-object p3, p0, Lcom/chase/sig/android/domain/Advisor;->middleInitial:Ljava/lang/String;

    .line 29
    iput-object p4, p0, Lcom/chase/sig/android/domain/Advisor;->relationship:Ljava/lang/String;

    .line 30
    iput-object p5, p0, Lcom/chase/sig/android/domain/Advisor;->firstName:Ljava/lang/String;

    .line 31
    iput-object p6, p0, Lcom/chase/sig/android/domain/Advisor;->lastName:Ljava/lang/String;

    .line 32
    iput-object p7, p0, Lcom/chase/sig/android/domain/Advisor;->displayName:Ljava/lang/String;

    .line 33
    iput-boolean p8, p0, Lcom/chase/sig/android/domain/Advisor;->support:Z

    .line 34
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/chase/sig/android/domain/Advisor;->phone:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/chase/sig/android/domain/Advisor;->email:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/chase/sig/android/domain/Advisor;->relationship:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/chase/sig/android/domain/Advisor;->displayName:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 93
    iget-boolean v0, p0, Lcom/chase/sig/android/domain/Advisor;->support:Z

    return v0
.end method
