.class public Lcom/chase/sig/android/domain/Announcement;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private announcementRequired:Z
    .annotation runtime Lcom/google/gson/chase/annotations/SerializedName;
        a = "flag"
    .end annotation
.end field

.field private message:Ljava/lang/String;

.field private url:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 17
    iget-boolean v0, p0, Lcom/chase/sig/android/domain/Announcement;->announcementRequired:Z

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/chase/sig/android/domain/Announcement;->message:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/chase/sig/android/domain/Announcement;->url:Ljava/lang/String;

    return-object v0
.end method
