.class public Lcom/chase/sig/android/domain/BillPayTransaction;
.super Lcom/chase/sig/android/domain/Transaction;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private amount:Lcom/chase/sig/android/util/Dollar;

.field private frequency:Ljava/lang/String;

.field private fromAccountId:Ljava/lang/String;

.field private payee:Lcom/chase/sig/android/domain/Payee;

.field private payeeId:Ljava/lang/String;

.field private paymentModelId:Ljava/lang/String;

.field private paymentModelToken:Ljava/lang/String;

.field private paymentToken:Ljava/lang/String;

.field private sendOnDate:Ljava/lang/String;

.field private toAccount:Ljava/lang/String;

.field private toAccountNickName:Ljava/lang/String;

.field private transactionNumber:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/chase/sig/android/domain/Transaction;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/chase/sig/android/domain/Payee;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/chase/sig/android/domain/BillPayTransaction;->payee:Lcom/chase/sig/android/domain/Payee;

    return-object v0
.end method

.method public final a(Lcom/chase/sig/android/domain/Payee;)V
    .locals 0
    .parameter

    .prologue
    .line 40
    iput-object p1, p0, Lcom/chase/sig/android/domain/BillPayTransaction;->payee:Lcom/chase/sig/android/domain/Payee;

    .line 41
    return-void
.end method

.method public final a(Lcom/chase/sig/android/util/Dollar;)V
    .locals 0
    .parameter

    .prologue
    .line 64
    iput-object p1, p0, Lcom/chase/sig/android/domain/BillPayTransaction;->amount:Lcom/chase/sig/android/util/Dollar;

    .line 65
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 48
    iput-object p1, p0, Lcom/chase/sig/android/domain/BillPayTransaction;->payeeId:Ljava/lang/String;

    .line 49
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/chase/sig/android/domain/BillPayTransaction;->payeeId:Ljava/lang/String;

    return-object v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 56
    iput-object p1, p0, Lcom/chase/sig/android/domain/BillPayTransaction;->fromAccountId:Ljava/lang/String;

    .line 57
    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/chase/sig/android/domain/BillPayTransaction;->fromAccountId:Ljava/lang/String;

    return-object v0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 76
    iput-object p1, p0, Lcom/chase/sig/android/domain/BillPayTransaction;->sendOnDate:Ljava/lang/String;

    .line 77
    return-void
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/chase/sig/android/domain/BillPayTransaction;->toAccountNickName:Ljava/lang/String;

    return-object v0
.end method

.method public final d(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 85
    iput-object p1, p0, Lcom/chase/sig/android/domain/BillPayTransaction;->paymentToken:Ljava/lang/String;

    .line 86
    return-void
.end method

.method public final e()Lcom/chase/sig/android/util/Dollar;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/chase/sig/android/domain/BillPayTransaction;->amount:Lcom/chase/sig/android/util/Dollar;

    return-object v0
.end method

.method public final e(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 93
    iput-object p1, p0, Lcom/chase/sig/android/domain/BillPayTransaction;->toAccount:Ljava/lang/String;

    .line 94
    return-void
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/chase/sig/android/domain/BillPayTransaction;->sendOnDate:Ljava/lang/String;

    return-object v0
.end method

.method public final f(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 101
    iput-object p1, p0, Lcom/chase/sig/android/domain/BillPayTransaction;->toAccountNickName:Ljava/lang/String;

    .line 102
    return-void
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/chase/sig/android/domain/BillPayTransaction;->paymentToken:Ljava/lang/String;

    return-object v0
.end method

.method public final g(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 109
    iput-object p1, p0, Lcom/chase/sig/android/domain/BillPayTransaction;->frequency:Ljava/lang/String;

    .line 110
    return-void
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/chase/sig/android/domain/BillPayTransaction;->toAccountNickName:Ljava/lang/String;

    return-object v0
.end method

.method public final h(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 117
    iput-object p1, p0, Lcom/chase/sig/android/domain/BillPayTransaction;->paymentModelId:Ljava/lang/String;

    .line 118
    return-void
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/chase/sig/android/domain/BillPayTransaction;->frequency:Ljava/lang/String;

    return-object v0
.end method

.method public final i(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 125
    iput-object p1, p0, Lcom/chase/sig/android/domain/BillPayTransaction;->paymentModelToken:Ljava/lang/String;

    .line 126
    return-void
.end method

.method public final j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/chase/sig/android/domain/BillPayTransaction;->paymentModelId:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/chase/sig/android/domain/BillPayTransaction;->paymentModelToken:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Z
    .locals 2

    .prologue
    .line 134
    iget-object v0, p0, Lcom/chase/sig/android/domain/BillPayTransaction;->frequency:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/domain/BillPayTransaction;->frequency:Ljava/lang/String;

    const-string v1, "One-Time"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 135
    const/4 v0, 0x0

    .line 137
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final m()Ljava/lang/String;
    .locals 2

    .prologue
    .line 142
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/chase/sig/android/domain/Transaction;->fromAccountNickName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/chase/sig/android/domain/Transaction;->fromAccountMask:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
