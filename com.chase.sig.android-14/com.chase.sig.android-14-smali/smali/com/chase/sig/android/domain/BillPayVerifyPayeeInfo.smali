.class public Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private accountNumber:Ljava/lang/String;

.field private amountDue:Ljava/lang/String;

.field private amountDueDate:Ljava/lang/String;

.field private isFromMerchantDirectory:Z

.field private maskedAccountNumber:Ljava/lang/String;

.field private merchantPayee:Lcom/chase/sig/android/domain/MerchantPayee;

.field private message:Ljava/lang/String;

.field private payeeNickName:Ljava/lang/String;

.field private selectedAccountId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    new-instance v0, Lcom/chase/sig/android/domain/MerchantPayee;

    invoke-direct {v0}, Lcom/chase/sig/android/domain/MerchantPayee;-><init>()V

    iput-object v0, p0, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->merchantPayee:Lcom/chase/sig/android/domain/MerchantPayee;

    return-void
.end method


# virtual methods
.method public final a(Lcom/chase/sig/android/domain/MerchantPayee;)V
    .locals 0
    .parameter

    .prologue
    .line 39
    iput-object p1, p0, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->merchantPayee:Lcom/chase/sig/android/domain/MerchantPayee;

    .line 40
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 31
    iput-object p1, p0, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->selectedAccountId:Ljava/lang/String;

    .line 32
    return-void
.end method

.method public final a(Z)V
    .locals 0
    .parameter

    .prologue
    .line 23
    iput-boolean p1, p0, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->isFromMerchantDirectory:Z

    .line 24
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 19
    iget-boolean v0, p0, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->isFromMerchantDirectory:Z

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->selectedAccountId:Ljava/lang/String;

    return-object v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 47
    iput-object p1, p0, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->accountNumber:Ljava/lang/String;

    .line 48
    return-void
.end method

.method public final c()Lcom/chase/sig/android/domain/MerchantPayee;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->merchantPayee:Lcom/chase/sig/android/domain/MerchantPayee;

    return-object v0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 55
    iput-object p1, p0, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->maskedAccountNumber:Ljava/lang/String;

    .line 56
    return-void
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->accountNumber:Ljava/lang/String;

    return-object v0
.end method

.method public final d(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 63
    iput-object p1, p0, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->payeeNickName:Ljava/lang/String;

    .line 64
    return-void
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->maskedAccountNumber:Ljava/lang/String;

    return-object v0
.end method

.method public final e(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 71
    iput-object p1, p0, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->message:Ljava/lang/String;

    .line 72
    return-void
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->payeeNickName:Ljava/lang/String;

    return-object v0
.end method

.method public final f(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 75
    iput-object p1, p0, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->amountDue:Ljava/lang/String;

    .line 76
    return-void
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->message:Ljava/lang/String;

    return-object v0
.end method

.method public final g(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 87
    iput-object p1, p0, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->amountDueDate:Ljava/lang/String;

    .line 88
    return-void
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->amountDue:Ljava/lang/String;

    return-object v0
.end method
