.class public Lcom/chase/sig/android/domain/Chart;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final a:Lcom/chase/sig/android/domain/Chart;


# instance fields
.field private label:Ljava/lang/String;

.field private type:Ljava/lang/String;

.field private url:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 13
    new-instance v0, Lcom/chase/sig/android/domain/Chart;

    const-string v1, ""

    const-string v2, ""

    const-string v3, ""

    invoke-direct {v0, v1, v2, v3}, Lcom/chase/sig/android/domain/Chart;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/Chart;->a:Lcom/chase/sig/android/domain/Chart;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/chase/sig/android/domain/Chart;->label:Ljava/lang/String;

    .line 18
    iput-object p2, p0, Lcom/chase/sig/android/domain/Chart;->url:Ljava/lang/String;

    .line 19
    iput-object p3, p0, Lcom/chase/sig/android/domain/Chart;->type:Ljava/lang/String;

    .line 20
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/chase/sig/android/domain/Chart;->url:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .parameter

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 36
    if-ne p0, p1, :cond_1

    .line 58
    :cond_0
    :goto_0
    return v0

    .line 40
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 41
    goto :goto_0

    .line 44
    :cond_3
    check-cast p1, Lcom/chase/sig/android/domain/Chart;

    .line 46
    iget-object v2, p0, Lcom/chase/sig/android/domain/Chart;->label:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/chase/sig/android/domain/Chart;->label:Ljava/lang/String;

    iget-object v3, p1, Lcom/chase/sig/android/domain/Chart;->label:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 47
    goto :goto_0

    .line 46
    :cond_5
    iget-object v2, p1, Lcom/chase/sig/android/domain/Chart;->label:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 50
    :cond_6
    iget-object v2, p0, Lcom/chase/sig/android/domain/Chart;->type:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/chase/sig/android/domain/Chart;->type:Ljava/lang/String;

    iget-object v3, p1, Lcom/chase/sig/android/domain/Chart;->type:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 51
    goto :goto_0

    .line 50
    :cond_8
    iget-object v2, p1, Lcom/chase/sig/android/domain/Chart;->type:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 54
    :cond_9
    iget-object v2, p0, Lcom/chase/sig/android/domain/Chart;->url:Ljava/lang/String;

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/chase/sig/android/domain/Chart;->url:Ljava/lang/String;

    iget-object v3, p1, Lcom/chase/sig/android/domain/Chart;->url:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 55
    goto :goto_0

    .line 54
    :cond_a
    iget-object v2, p1, Lcom/chase/sig/android/domain/Chart;->url:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 63
    iget-object v0, p0, Lcom/chase/sig/android/domain/Chart;->label:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/chase/sig/android/domain/Chart;->label:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 64
    :goto_0
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/chase/sig/android/domain/Chart;->url:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/chase/sig/android/domain/Chart;->url:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    .line 65
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/chase/sig/android/domain/Chart;->type:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/chase/sig/android/domain/Chart;->type:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 66
    return v0

    :cond_1
    move v0, v1

    .line 63
    goto :goto_0

    :cond_2
    move v0, v1

    .line 64
    goto :goto_1
.end method
