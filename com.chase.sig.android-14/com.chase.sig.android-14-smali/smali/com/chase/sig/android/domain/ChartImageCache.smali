.class public Lcom/chase/sig/android/domain/ChartImageCache;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/chase/sig/android/domain/ChartImageCache;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/chase/sig/android/domain/Chart;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    new-instance v0, Lcom/chase/sig/android/domain/b;

    invoke-direct {v0}, Lcom/chase/sig/android/domain/b;-><init>()V

    sput-object v0, Lcom/chase/sig/android/domain/ChartImageCache;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/chase/sig/android/domain/ChartImageCache;->a:Ljava/util/Map;

    .line 15
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .parameter

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/chase/sig/android/domain/ChartImageCache;-><init>()V

    .line 19
    iget-object v0, p0, Lcom/chase/sig/android/domain/ChartImageCache;->a:Ljava/util/Map;

    const-class v1, Lcom/chase/sig/android/domain/ChartImageCache;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readMap(Ljava/util/Map;Ljava/lang/ClassLoader;)V

    .line 20
    return-void
.end method


# virtual methods
.method public final a(Lcom/chase/sig/android/domain/Chart;Landroid/graphics/Bitmap;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 27
    iget-object v0, p0, Lcom/chase/sig/android/domain/ChartImageCache;->a:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28
    return-void
.end method

.method public final a(Lcom/chase/sig/android/domain/Chart;)Z
    .locals 1
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, Lcom/chase/sig/android/domain/ChartImageCache;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b(Lcom/chase/sig/android/domain/Chart;)Landroid/graphics/Bitmap;
    .locals 1
    .parameter

    .prologue
    .line 31
    iget-object v0, p0, Lcom/chase/sig/android/domain/ChartImageCache;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 45
    iget-object v0, p0, Lcom/chase/sig/android/domain/ChartImageCache;->a:Ljava/util/Map;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    .line 46
    return-void
.end method
