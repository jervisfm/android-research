.class public Lcom/chase/sig/android/domain/ChaseReceiptsFilters;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field categoryFilter:Lcom/chase/sig/android/domain/BaseFilter;
    .annotation runtime Lcom/google/gson/chase/annotations/SerializedName;
        a = "categoryFilter"
    .end annotation
.end field

.field dateFilter:Lcom/chase/sig/android/domain/BaseFilter;
    .annotation runtime Lcom/google/gson/chase/annotations/SerializedName;
        a = "dateFilter"
    .end annotation
.end field

.field expenseTypeFilter:Lcom/chase/sig/android/domain/BaseFilter;
    .annotation runtime Lcom/google/gson/chase/annotations/SerializedName;
        a = "expenseTypeFilter"
    .end annotation
.end field

.field matchFilter:Lcom/chase/sig/android/domain/BaseFilter;
    .annotation runtime Lcom/google/gson/chase/annotations/SerializedName;
        a = "matchFilter"
    .end annotation
.end field

.field receiptFilter:Lcom/chase/sig/android/domain/BaseFilter;
    .annotation runtime Lcom/google/gson/chase/annotations/SerializedName;
        a = "receiptsFilter"
    .end annotation
.end field

.field taxDeductibleFilter:Lcom/chase/sig/android/domain/BaseFilter;
    .annotation runtime Lcom/google/gson/chase/annotations/SerializedName;
        a = "taxDeductibleFilter"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
