.class public Lcom/chase/sig/android/domain/CreditFacilityAccountDetail;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private available:Lcom/chase/sig/android/util/Dollar;

.field private creditLimit:Lcom/chase/sig/android/util/Dollar;

.field private displayType:Ljava/lang/String;

.field private mask:Ljava/lang/String;

.field private name:Ljava/lang/String;

.field private outstandingValue:Lcom/chase/sig/android/util/Dollar;

.field private sortedLoanDetails:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/CreditFacilityAccountLoanDetail;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/chase/sig/android/domain/CreditFacilityAccountDetail;->creditLimit:Lcom/chase/sig/android/util/Dollar;

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->a(Lcom/chase/sig/android/util/Dollar;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/chase/sig/android/util/Dollar;)V
    .locals 0
    .parameter

    .prologue
    .line 27
    iput-object p1, p0, Lcom/chase/sig/android/domain/CreditFacilityAccountDetail;->creditLimit:Lcom/chase/sig/android/util/Dollar;

    .line 28
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 43
    iput-object p1, p0, Lcom/chase/sig/android/domain/CreditFacilityAccountDetail;->displayType:Ljava/lang/String;

    .line 44
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/CreditFacilityAccountLoanDetail;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 71
    iput-object p1, p0, Lcom/chase/sig/android/domain/CreditFacilityAccountDetail;->sortedLoanDetails:Ljava/util/List;

    .line 72
    iget-object v0, p0, Lcom/chase/sig/android/domain/CreditFacilityAccountDetail;->sortedLoanDetails:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 73
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/chase/sig/android/domain/CreditFacilityAccountDetail;->available:Lcom/chase/sig/android/util/Dollar;

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->a(Lcom/chase/sig/android/util/Dollar;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/chase/sig/android/util/Dollar;)V
    .locals 0
    .parameter

    .prologue
    .line 35
    iput-object p1, p0, Lcom/chase/sig/android/domain/CreditFacilityAccountDetail;->available:Lcom/chase/sig/android/util/Dollar;

    .line 36
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 51
    iput-object p1, p0, Lcom/chase/sig/android/domain/CreditFacilityAccountDetail;->mask:Ljava/lang/String;

    .line 52
    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/chase/sig/android/domain/CreditFacilityAccountDetail;->displayType:Ljava/lang/String;

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c(Lcom/chase/sig/android/util/Dollar;)V
    .locals 0
    .parameter

    .prologue
    .line 63
    iput-object p1, p0, Lcom/chase/sig/android/domain/CreditFacilityAccountDetail;->outstandingValue:Lcom/chase/sig/android/util/Dollar;

    .line 64
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 59
    iput-object p1, p0, Lcom/chase/sig/android/domain/CreditFacilityAccountDetail;->name:Ljava/lang/String;

    .line 60
    return-void
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/chase/sig/android/domain/CreditFacilityAccountDetail;->name:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/chase/sig/android/domain/CreditFacilityAccountDetail;->outstandingValue:Lcom/chase/sig/android/util/Dollar;

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->a(Lcom/chase/sig/android/util/Dollar;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/CreditFacilityAccountLoanDetail;",
            ">;"
        }
    .end annotation

    .prologue
    .line 76
    iget-object v0, p0, Lcom/chase/sig/android/domain/CreditFacilityAccountDetail;->sortedLoanDetails:Ljava/util/List;

    return-object v0
.end method
