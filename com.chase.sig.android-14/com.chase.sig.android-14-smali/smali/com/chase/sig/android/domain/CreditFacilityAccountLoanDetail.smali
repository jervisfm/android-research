.class public Lcom/chase/sig/android/domain/CreditFacilityAccountLoanDetail;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/domain/CreditFacilityAccountLoanDetail$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/io/Serializable;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/chase/sig/android/domain/CreditFacilityAccountLoanDetail;",
        ">;"
    }
.end annotation


# instance fields
.field private accruedInt:Lcom/chase/sig/android/domain/CreditFacilityAccountLoanDetail$a;

.field private intRate:Ljava/lang/Double;

.field private loanNumMask:Ljava/lang/String;

.field private loanType:Ljava/lang/String;

.field private maturityDate:Ljava/lang/String;

.field private outstanding:Lcom/chase/sig/android/domain/CreditFacilityAccountLoanDetail$a;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    return-void
.end method

.method private g()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 33
    iget-object v0, p0, Lcom/chase/sig/android/domain/CreditFacilityAccountLoanDetail;->loanNumMask:Ljava/lang/String;

    const-string v1, "..."

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/chase/sig/android/domain/CreditFacilityAccountLoanDetail;->loanNumMask:Ljava/lang/String;

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Double;)V
    .locals 0
    .parameter

    .prologue
    .line 53
    iput-object p1, p0, Lcom/chase/sig/android/domain/CreditFacilityAccountLoanDetail;->intRate:Ljava/lang/Double;

    .line 54
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 37
    iput-object p1, p0, Lcom/chase/sig/android/domain/CreditFacilityAccountLoanDetail;->loanNumMask:Ljava/lang/String;

    .line 38
    return-void
.end method

.method public final a(Ljava/util/Hashtable;)V
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 21
    new-instance v0, Lcom/chase/sig/android/domain/CreditFacilityAccountLoanDetail$a;

    invoke-direct {v0, p0, p1}, Lcom/chase/sig/android/domain/CreditFacilityAccountLoanDetail$a;-><init>(Lcom/chase/sig/android/domain/CreditFacilityAccountLoanDetail;Ljava/util/Hashtable;)V

    iput-object v0, p0, Lcom/chase/sig/android/domain/CreditFacilityAccountLoanDetail;->outstanding:Lcom/chase/sig/android/domain/CreditFacilityAccountLoanDetail$a;

    .line 22
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/chase/sig/android/domain/CreditFacilityAccountLoanDetail;->loanType:Ljava/lang/String;

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 45
    iput-object p1, p0, Lcom/chase/sig/android/domain/CreditFacilityAccountLoanDetail;->loanType:Ljava/lang/String;

    .line 46
    return-void
.end method

.method public final b(Ljava/util/Hashtable;)V
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 25
    new-instance v0, Lcom/chase/sig/android/domain/CreditFacilityAccountLoanDetail$a;

    invoke-direct {v0, p0, p1}, Lcom/chase/sig/android/domain/CreditFacilityAccountLoanDetail$a;-><init>(Lcom/chase/sig/android/domain/CreditFacilityAccountLoanDetail;Ljava/util/Hashtable;)V

    iput-object v0, p0, Lcom/chase/sig/android/domain/CreditFacilityAccountLoanDetail;->accruedInt:Lcom/chase/sig/android/domain/CreditFacilityAccountLoanDetail$a;

    .line 26
    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/chase/sig/android/domain/CreditFacilityAccountLoanDetail;->intRate:Ljava/lang/Double;

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->a(Ljava/lang/Double;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 61
    iput-object p1, p0, Lcom/chase/sig/android/domain/CreditFacilityAccountLoanDetail;->maturityDate:Ljava/lang/String;

    .line 62
    return-void
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 2
    .parameter

    .prologue
    .line 9
    check-cast p1, Lcom/chase/sig/android/domain/CreditFacilityAccountLoanDetail;

    invoke-direct {p0}, Lcom/chase/sig/android/domain/CreditFacilityAccountLoanDetail;->g()Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {p1}, Lcom/chase/sig/android/domain/CreditFacilityAccountLoanDetail;->g()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Integer;->compareTo(Ljava/lang/Integer;)I

    move-result v0

    return v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/chase/sig/android/domain/CreditFacilityAccountLoanDetail;->maturityDate:Ljava/lang/String;

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->k(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/chase/sig/android/domain/CreditFacilityAccountLoanDetail;->outstanding:Lcom/chase/sig/android/domain/CreditFacilityAccountLoanDetail$a;

    iget-object v0, v0, Lcom/chase/sig/android/domain/CreditFacilityAccountLoanDetail$a;->a:Lcom/chase/sig/android/util/Dollar;

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->a(Lcom/chase/sig/android/util/Dollar;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/chase/sig/android/domain/CreditFacilityAccountLoanDetail;->accruedInt:Lcom/chase/sig/android/domain/CreditFacilityAccountLoanDetail$a;

    iget-object v0, v0, Lcom/chase/sig/android/domain/CreditFacilityAccountLoanDetail$a;->a:Lcom/chase/sig/android/util/Dollar;

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->a(Lcom/chase/sig/android/util/Dollar;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
