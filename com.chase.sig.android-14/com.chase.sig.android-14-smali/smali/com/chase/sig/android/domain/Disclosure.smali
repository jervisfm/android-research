.class public Lcom/chase/sig/android/domain/Disclosure;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private disclosureId:Ljava/lang/String;

.field private disclosureText:Ljava/lang/String;

.field private disclosureTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/chase/sig/android/domain/Disclosure;->disclosureId:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 17
    iput-object p1, p0, Lcom/chase/sig/android/domain/Disclosure;->disclosureId:Ljava/lang/String;

    .line 18
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/chase/sig/android/domain/Disclosure;->disclosureText:Ljava/lang/String;

    return-object v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 23
    iput-object p1, p0, Lcom/chase/sig/android/domain/Disclosure;->disclosureText:Ljava/lang/String;

    .line 24
    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/chase/sig/android/domain/Disclosure;->disclosureTitle:Ljava/lang/String;

    return-object v0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 29
    iput-object p1, p0, Lcom/chase/sig/android/domain/Disclosure;->disclosureTitle:Ljava/lang/String;

    .line 30
    return-void
.end method
