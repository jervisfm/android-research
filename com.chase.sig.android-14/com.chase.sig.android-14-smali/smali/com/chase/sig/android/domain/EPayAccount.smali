.class public Lcom/chase/sig/android/domain/EPayAccount;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private id:Ljava/lang/String;

.field private nicknameAndMask:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/chase/sig/android/domain/EPayAccount;->nicknameAndMask:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 17
    iput-object p1, p0, Lcom/chase/sig/android/domain/EPayAccount;->nicknameAndMask:Ljava/lang/String;

    .line 18
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 3

    .prologue
    .line 21
    iget-object v0, p0, Lcom/chase/sig/android/domain/EPayAccount;->nicknameAndMask:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/domain/EPayAccount;->nicknameAndMask:Ljava/lang/String;

    const-string v1, "(..."

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-gez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    .line 22
    const-string v0, ""

    .line 25
    :goto_1
    return-object v0

    .line 21
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 24
    :cond_2
    iget-object v0, p0, Lcom/chase/sig/android/domain/EPayAccount;->nicknameAndMask:Ljava/lang/String;

    const-string v1, "(..."

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 25
    iget-object v1, p0, Lcom/chase/sig/android/domain/EPayAccount;->nicknameAndMask:Ljava/lang/String;

    add-int/lit8 v0, v0, 0x1

    iget-object v2, p0, Lcom/chase/sig/android/domain/EPayAccount;->nicknameAndMask:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 48
    iput-object p1, p0, Lcom/chase/sig/android/domain/EPayAccount;->id:Ljava/lang/String;

    .line 49
    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 3

    .prologue
    .line 29
    iget-object v0, p0, Lcom/chase/sig/android/domain/EPayAccount;->nicknameAndMask:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 30
    const-string v0, ""

    .line 36
    :goto_0
    return-object v0

    .line 32
    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/domain/EPayAccount;->nicknameAndMask:Ljava/lang/String;

    const-string v1, "(..."

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 33
    if-gez v0, :cond_1

    .line 34
    iget-object v0, p0, Lcom/chase/sig/android/domain/EPayAccount;->nicknameAndMask:Ljava/lang/String;

    goto :goto_0

    .line 36
    :cond_1
    iget-object v1, p0, Lcom/chase/sig/android/domain/EPayAccount;->nicknameAndMask:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/chase/sig/android/domain/EPayAccount;->id:Ljava/lang/String;

    return-object v0
.end method
