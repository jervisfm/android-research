.class public Lcom/chase/sig/android/domain/EPayment;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private agreement:Ljava/lang/String;

.field private amount:Lcom/chase/sig/android/util/Dollar;

.field private confirmationNumber:Ljava/lang/String;

.field private deliverByDate:Ljava/lang/String;

.field private formId:Ljava/lang/String;

.field private fromAccountId:Ljava/lang/String;

.field private fromAccountMask:Ljava/lang/String;

.field private fromAccountNickname:Ljava/lang/String;

.field private optionId:Ljava/lang/String;

.field private toAccountId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/chase/sig/android/domain/EPayment;->confirmationNumber:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Lcom/chase/sig/android/util/Dollar;)V
    .locals 0
    .parameter

    .prologue
    .line 51
    iput-object p1, p0, Lcom/chase/sig/android/domain/EPayment;->amount:Lcom/chase/sig/android/util/Dollar;

    .line 52
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 27
    iput-object p1, p0, Lcom/chase/sig/android/domain/EPayment;->confirmationNumber:Ljava/lang/String;

    .line 28
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/chase/sig/android/domain/EPayment;->formId:Ljava/lang/String;

    return-object v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 35
    iput-object p1, p0, Lcom/chase/sig/android/domain/EPayment;->formId:Ljava/lang/String;

    .line 36
    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/chase/sig/android/domain/EPayment;->fromAccountId:Ljava/lang/String;

    return-object v0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 43
    iput-object p1, p0, Lcom/chase/sig/android/domain/EPayment;->fromAccountId:Ljava/lang/String;

    .line 44
    return-void
.end method

.method public final d()Lcom/chase/sig/android/util/Dollar;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/chase/sig/android/domain/EPayment;->amount:Lcom/chase/sig/android/util/Dollar;

    return-object v0
.end method

.method public final d(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 59
    iput-object p1, p0, Lcom/chase/sig/android/domain/EPayment;->deliverByDate:Ljava/lang/String;

    .line 60
    return-void
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/chase/sig/android/domain/EPayment;->deliverByDate:Ljava/lang/String;

    return-object v0
.end method

.method public final e(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 67
    iput-object p1, p0, Lcom/chase/sig/android/domain/EPayment;->toAccountId:Ljava/lang/String;

    .line 68
    return-void
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/chase/sig/android/domain/EPayment;->toAccountId:Ljava/lang/String;

    return-object v0
.end method

.method public final f(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 75
    iput-object p1, p0, Lcom/chase/sig/android/domain/EPayment;->optionId:Ljava/lang/String;

    .line 76
    return-void
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/chase/sig/android/domain/EPayment;->optionId:Ljava/lang/String;

    return-object v0
.end method

.method public final g(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 83
    iput-object p1, p0, Lcom/chase/sig/android/domain/EPayment;->agreement:Ljava/lang/String;

    .line 84
    return-void
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/chase/sig/android/domain/EPayment;->agreement:Ljava/lang/String;

    return-object v0
.end method

.method public final h(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 91
    iput-object p1, p0, Lcom/chase/sig/android/domain/EPayment;->fromAccountMask:Ljava/lang/String;

    .line 92
    return-void
.end method

.method public final i()Ljava/lang/CharSequence;
    .locals 4

    .prologue
    .line 103
    const-string v0, "%s (%s)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/chase/sig/android/domain/EPayment;->fromAccountNickname:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/chase/sig/android/domain/EPayment;->fromAccountMask:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final i(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 99
    iput-object p1, p0, Lcom/chase/sig/android/domain/EPayment;->fromAccountNickname:Ljava/lang/String;

    .line 100
    return-void
.end method
