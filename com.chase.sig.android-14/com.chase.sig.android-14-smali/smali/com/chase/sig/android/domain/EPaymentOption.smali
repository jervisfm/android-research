.class public Lcom/chase/sig/android/domain/EPaymentOption;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private amount:Lcom/chase/sig/android/util/Dollar;

.field private amountRequired:Z

.field private description:Ljava/lang/String;

.field private id:Ljava/lang/String;

.field private label:Ljava/lang/String;

.field private note:Ljava/lang/String;

.field private shortDescription:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/chase/sig/android/util/Dollar;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/chase/sig/android/domain/EPaymentOption;->amount:Lcom/chase/sig/android/util/Dollar;

    return-object v0
.end method

.method public final a(Lcom/chase/sig/android/util/Dollar;)V
    .locals 0
    .parameter

    .prologue
    .line 57
    iput-object p1, p0, Lcom/chase/sig/android/domain/EPaymentOption;->amount:Lcom/chase/sig/android/util/Dollar;

    .line 58
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 73
    iput-object p1, p0, Lcom/chase/sig/android/domain/EPaymentOption;->shortDescription:Ljava/lang/String;

    .line 74
    return-void
.end method

.method public final a(Z)V
    .locals 0
    .parameter

    .prologue
    .line 65
    iput-boolean p1, p0, Lcom/chase/sig/android/domain/EPaymentOption;->amountRequired:Z

    .line 66
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/chase/sig/android/domain/EPaymentOption;->label:Ljava/lang/String;

    return-object v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 81
    iput-object p1, p0, Lcom/chase/sig/android/domain/EPaymentOption;->label:Ljava/lang/String;

    .line 82
    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/chase/sig/android/domain/EPaymentOption;->note:Ljava/lang/String;

    return-object v0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 89
    iput-object p1, p0, Lcom/chase/sig/android/domain/EPaymentOption;->description:Ljava/lang/String;

    .line 90
    return-void
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/chase/sig/android/domain/EPaymentOption;->id:Ljava/lang/String;

    return-object v0
.end method

.method public final d(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 97
    iput-object p1, p0, Lcom/chase/sig/android/domain/EPaymentOption;->note:Ljava/lang/String;

    .line 98
    return-void
.end method

.method public final e(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 117
    iput-object p1, p0, Lcom/chase/sig/android/domain/EPaymentOption;->id:Ljava/lang/String;

    .line 118
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/chase/sig/android/domain/EPaymentOption;->label:Ljava/lang/String;

    return-object v0
.end method
