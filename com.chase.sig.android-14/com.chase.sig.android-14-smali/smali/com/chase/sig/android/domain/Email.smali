.class public Lcom/chase/sig/android/domain/Email;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private email:Ljava/lang/String;

.field private id:Ljava/lang/String;

.field private isDefault:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;ZLjava/lang/String;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/chase/sig/android/domain/Email;->email:Ljava/lang/String;

    .line 19
    iput-boolean p2, p0, Lcom/chase/sig/android/domain/Email;->isDefault:Z

    .line 20
    iput-object p3, p0, Lcom/chase/sig/android/domain/Email;->id:Ljava/lang/String;

    .line 21
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/chase/sig/android/domain/Email;->email:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Z)V
    .locals 0
    .parameter

    .prologue
    .line 40
    iput-boolean p1, p0, Lcom/chase/sig/android/domain/Email;->isDefault:Z

    .line 41
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 36
    iget-boolean v0, p0, Lcom/chase/sig/android/domain/Email;->isDefault:Z

    return v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/chase/sig/android/domain/Email;->id:Ljava/lang/String;

    return-object v0
.end method
