.class public Lcom/chase/sig/android/domain/ImageCaptureData;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private accountNumber:Ljava/lang/String;

.field private addressLine1:Ljava/lang/String;

.field private addressLine2:Ljava/lang/String;

.field private amountDue:Ljava/lang/String;

.field private balanceDueAmount:Ljava/lang/String;

.field private city:Ljava/lang/String;

.field private dueDate:Ljava/lang/String;

.field private invoiceNumber:Ljava/lang/String;

.field private minimumAmountDue:Ljava/lang/String;

.field private name:Ljava/lang/String;

.field private postalCode:Ljava/lang/String;

.field private state:Lcom/chase/sig/android/domain/State;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/chase/sig/android/domain/ImageCaptureData;->name:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Lcom/chase/sig/android/domain/State;)V
    .locals 0
    .parameter

    .prologue
    .line 107
    iput-object p1, p0, Lcom/chase/sig/android/domain/ImageCaptureData;->state:Lcom/chase/sig/android/domain/State;

    .line 108
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 27
    iput-object p1, p0, Lcom/chase/sig/android/domain/ImageCaptureData;->name:Ljava/lang/String;

    .line 28
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/chase/sig/android/domain/ImageCaptureData;->accountNumber:Ljava/lang/String;

    return-object v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 35
    iput-object p1, p0, Lcom/chase/sig/android/domain/ImageCaptureData;->accountNumber:Ljava/lang/String;

    .line 36
    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/chase/sig/android/domain/ImageCaptureData;->amountDue:Ljava/lang/String;

    return-object v0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 43
    iput-object p1, p0, Lcom/chase/sig/android/domain/ImageCaptureData;->balanceDueAmount:Ljava/lang/String;

    .line 44
    return-void
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/chase/sig/android/domain/ImageCaptureData;->dueDate:Ljava/lang/String;

    return-object v0
.end method

.method public final d(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 51
    iput-object p1, p0, Lcom/chase/sig/android/domain/ImageCaptureData;->amountDue:Ljava/lang/String;

    .line 52
    return-void
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/chase/sig/android/domain/ImageCaptureData;->addressLine1:Ljava/lang/String;

    return-object v0
.end method

.method public final e(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 59
    iput-object p1, p0, Lcom/chase/sig/android/domain/ImageCaptureData;->minimumAmountDue:Ljava/lang/String;

    .line 60
    return-void
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/chase/sig/android/domain/ImageCaptureData;->addressLine2:Ljava/lang/String;

    return-object v0
.end method

.method public final f(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 67
    iput-object p1, p0, Lcom/chase/sig/android/domain/ImageCaptureData;->invoiceNumber:Ljava/lang/String;

    .line 68
    return-void
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/chase/sig/android/domain/ImageCaptureData;->city:Ljava/lang/String;

    return-object v0
.end method

.method public final g(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 75
    iput-object p1, p0, Lcom/chase/sig/android/domain/ImageCaptureData;->dueDate:Ljava/lang/String;

    .line 76
    return-void
.end method

.method public final h()Lcom/chase/sig/android/domain/State;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/chase/sig/android/domain/ImageCaptureData;->state:Lcom/chase/sig/android/domain/State;

    return-object v0
.end method

.method public final h(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 83
    iput-object p1, p0, Lcom/chase/sig/android/domain/ImageCaptureData;->addressLine1:Ljava/lang/String;

    .line 84
    return-void
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/chase/sig/android/domain/ImageCaptureData;->postalCode:Ljava/lang/String;

    return-object v0
.end method

.method public final i(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 91
    iput-object p1, p0, Lcom/chase/sig/android/domain/ImageCaptureData;->addressLine2:Ljava/lang/String;

    .line 92
    return-void
.end method

.method public final j(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 99
    iput-object p1, p0, Lcom/chase/sig/android/domain/ImageCaptureData;->city:Ljava/lang/String;

    .line 100
    return-void
.end method

.method public final k(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 115
    iput-object p1, p0, Lcom/chase/sig/android/domain/ImageCaptureData;->postalCode:Ljava/lang/String;

    .line 116
    return-void
.end method
