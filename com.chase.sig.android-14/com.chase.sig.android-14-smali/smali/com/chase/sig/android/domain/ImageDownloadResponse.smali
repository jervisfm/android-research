.class public Lcom/chase/sig/android/domain/ImageDownloadResponse;
.super Lcom/chase/sig/android/service/l;
.source "SourceFile"


# static fields
.field public static a:Lcom/chase/sig/android/domain/ImageDownloadResponse;


# instance fields
.field private bitmap:Landroid/graphics/Bitmap;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    new-instance v0, Lcom/chase/sig/android/domain/ImageDownloadResponse;

    invoke-direct {v0}, Lcom/chase/sig/android/domain/ImageDownloadResponse;-><init>()V

    sput-object v0, Lcom/chase/sig/android/domain/ImageDownloadResponse;->a:Lcom/chase/sig/android/domain/ImageDownloadResponse;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lcom/chase/sig/android/service/l;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/chase/sig/android/domain/ImageDownloadResponse;->bitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public final a(Landroid/graphics/Bitmap;)V
    .locals 0
    .parameter

    .prologue
    .line 18
    iput-object p1, p0, Lcom/chase/sig/android/domain/ImageDownloadResponse;->bitmap:Landroid/graphics/Bitmap;

    .line 19
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 22
    invoke-virtual {p0}, Lcom/chase/sig/android/domain/ImageDownloadResponse;->e()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/domain/ImageDownloadResponse;->bitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
