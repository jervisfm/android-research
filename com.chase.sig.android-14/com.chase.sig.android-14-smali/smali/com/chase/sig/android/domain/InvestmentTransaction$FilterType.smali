.class public Lcom/chase/sig/android/domain/InvestmentTransaction$FilterType;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/domain/InvestmentTransaction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "FilterType"
.end annotation


# static fields
.field static final synthetic a:Z


# instance fields
.field private code:Ljava/lang/String;

.field private name:Ljava/lang/String;

.field private sortOrder:I

.field final synthetic this$0:Lcom/chase/sig/android/domain/InvestmentTransaction;

.field private value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 131
    const-class v0, Lcom/chase/sig/android/domain/InvestmentTransaction;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/chase/sig/android/domain/InvestmentTransaction$FilterType;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/chase/sig/android/domain/InvestmentTransaction;)V
    .locals 0
    .parameter

    .prologue
    .line 131
    iput-object p1, p0, Lcom/chase/sig/android/domain/InvestmentTransaction$FilterType;->this$0:Lcom/chase/sig/android/domain/InvestmentTransaction;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/chase/sig/android/domain/InvestmentTransaction$FilterType;->code:Ljava/lang/String;

    return-object v0
.end method

.method public final a(I)V
    .locals 0
    .parameter

    .prologue
    .line 157
    iput p1, p0, Lcom/chase/sig/android/domain/InvestmentTransaction$FilterType;->sortOrder:I

    .line 158
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 145
    iput-object p1, p0, Lcom/chase/sig/android/domain/InvestmentTransaction$FilterType;->name:Ljava/lang/String;

    .line 146
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/chase/sig/android/domain/InvestmentTransaction$FilterType;->value:Ljava/lang/String;

    return-object v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 149
    iput-object p1, p0, Lcom/chase/sig/android/domain/InvestmentTransaction$FilterType;->code:Ljava/lang/String;

    .line 150
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 165
    iput-object p1, p0, Lcom/chase/sig/android/domain/InvestmentTransaction$FilterType;->value:Ljava/lang/String;

    .line 166
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .parameter

    .prologue
    .line 179
    instance-of v0, p1, Lcom/chase/sig/android/domain/InvestmentTransaction$FilterType;

    if-eqz v0, :cond_1

    .line 180
    check-cast p1, Lcom/chase/sig/android/domain/InvestmentTransaction$FilterType;

    .line 181
    iget-object v0, p1, Lcom/chase/sig/android/domain/InvestmentTransaction$FilterType;->code:Ljava/lang/String;

    iget-object v1, p0, Lcom/chase/sig/android/domain/InvestmentTransaction$FilterType;->code:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/chase/sig/android/domain/InvestmentTransaction$FilterType;->value:Ljava/lang/String;

    iget-object v1, p0, Lcom/chase/sig/android/domain/InvestmentTransaction$FilterType;->value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 184
    :goto_0
    return v0

    .line 181
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 184
    :cond_1
    invoke-super {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 194
    sget-boolean v0, Lcom/chase/sig/android/domain/InvestmentTransaction$FilterType;->a:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "hashCode not designed"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 195
    :cond_0
    const/16 v0, 0x2a

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/chase/sig/android/domain/InvestmentTransaction$FilterType;->name:Ljava/lang/String;

    return-object v0
.end method
