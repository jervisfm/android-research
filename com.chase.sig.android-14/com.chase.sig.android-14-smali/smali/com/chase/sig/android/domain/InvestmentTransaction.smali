.class public Lcom/chase/sig/android/domain/InvestmentTransaction;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/domain/InvestmentTransaction$FilterType;
    }
.end annotation


# instance fields
.field private amount:Lcom/chase/sig/android/util/Dollar;

.field private amountCurrency:Ljava/lang/String;

.field private asOfDate:Ljava/lang/String;

.field private cost:Lcom/chase/sig/android/util/Dollar;

.field private costCurrency:Ljava/lang/String;

.field private date:Ljava/lang/String;

.field private description:Ljava/lang/String;

.field private isIntraDay:Z

.field private isTrade:Z

.field private price:Lcom/chase/sig/android/util/Dollar;

.field private quantity:D

.field private settleDate:Ljava/lang/String;

.field private symbol:Ljava/lang/String;

.field private tradeDate:Ljava/lang/String;

.field private transactionType:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 131
    return-void
.end method


# virtual methods
.method public final a()Lcom/chase/sig/android/util/Dollar;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/chase/sig/android/domain/InvestmentTransaction;->amount:Lcom/chase/sig/android/util/Dollar;

    return-object v0
.end method

.method public final a(D)V
    .locals 0
    .parameter

    .prologue
    .line 64
    iput-wide p1, p0, Lcom/chase/sig/android/domain/InvestmentTransaction;->quantity:D

    .line 65
    return-void
.end method

.method public final a(Lcom/chase/sig/android/util/Dollar;)V
    .locals 0
    .parameter

    .prologue
    .line 32
    iput-object p1, p0, Lcom/chase/sig/android/domain/InvestmentTransaction;->amount:Lcom/chase/sig/android/util/Dollar;

    .line 33
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 40
    iput-object p1, p0, Lcom/chase/sig/android/domain/InvestmentTransaction;->date:Ljava/lang/String;

    .line 41
    return-void
.end method

.method public final a(Z)V
    .locals 0
    .parameter

    .prologue
    .line 48
    iput-boolean p1, p0, Lcom/chase/sig/android/domain/InvestmentTransaction;->isIntraDay:Z

    .line 49
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/chase/sig/android/domain/InvestmentTransaction;->date:Ljava/lang/String;

    return-object v0
.end method

.method public final b(Lcom/chase/sig/android/util/Dollar;)V
    .locals 0
    .parameter

    .prologue
    .line 120
    iput-object p1, p0, Lcom/chase/sig/android/domain/InvestmentTransaction;->price:Lcom/chase/sig/android/util/Dollar;

    .line 121
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 56
    iput-object p1, p0, Lcom/chase/sig/android/domain/InvestmentTransaction;->asOfDate:Ljava/lang/String;

    .line 57
    return-void
.end method

.method public final b(Z)V
    .locals 0
    .parameter

    .prologue
    .line 72
    iput-boolean p1, p0, Lcom/chase/sig/android/domain/InvestmentTransaction;->isTrade:Z

    .line 73
    return-void
.end method

.method public final c()D
    .locals 2

    .prologue
    .line 60
    iget-wide v0, p0, Lcom/chase/sig/android/domain/InvestmentTransaction;->quantity:D

    return-wide v0
.end method

.method public final c(Lcom/chase/sig/android/util/Dollar;)V
    .locals 0
    .parameter

    .prologue
    .line 128
    iput-object p1, p0, Lcom/chase/sig/android/domain/InvestmentTransaction;->cost:Lcom/chase/sig/android/util/Dollar;

    .line 129
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 80
    iput-object p1, p0, Lcom/chase/sig/android/domain/InvestmentTransaction;->transactionType:Ljava/lang/String;

    .line 81
    return-void
.end method

.method public final d(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 88
    iput-object p1, p0, Lcom/chase/sig/android/domain/InvestmentTransaction;->description:Ljava/lang/String;

    .line 89
    return-void
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 68
    iget-boolean v0, p0, Lcom/chase/sig/android/domain/InvestmentTransaction;->isTrade:Z

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/chase/sig/android/domain/InvestmentTransaction;->transactionType:Ljava/lang/String;

    return-object v0
.end method

.method public final e(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 96
    iput-object p1, p0, Lcom/chase/sig/android/domain/InvestmentTransaction;->tradeDate:Ljava/lang/String;

    .line 97
    return-void
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/chase/sig/android/domain/InvestmentTransaction;->description:Ljava/lang/String;

    return-object v0
.end method

.method public final f(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 104
    iput-object p1, p0, Lcom/chase/sig/android/domain/InvestmentTransaction;->settleDate:Ljava/lang/String;

    .line 105
    return-void
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/chase/sig/android/domain/InvestmentTransaction;->tradeDate:Ljava/lang/String;

    return-object v0
.end method

.method public final g(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 112
    iput-object p1, p0, Lcom/chase/sig/android/domain/InvestmentTransaction;->symbol:Ljava/lang/String;

    .line 113
    return-void
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/chase/sig/android/domain/InvestmentTransaction;->settleDate:Ljava/lang/String;

    return-object v0
.end method

.method public final h(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 204
    iput-object p1, p0, Lcom/chase/sig/android/domain/InvestmentTransaction;->amountCurrency:Ljava/lang/String;

    .line 205
    return-void
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/chase/sig/android/domain/InvestmentTransaction;->symbol:Ljava/lang/String;

    return-object v0
.end method

.method public final i(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 212
    iput-object p1, p0, Lcom/chase/sig/android/domain/InvestmentTransaction;->costCurrency:Ljava/lang/String;

    .line 213
    return-void
.end method

.method public final j()Lcom/chase/sig/android/util/Dollar;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/chase/sig/android/domain/InvestmentTransaction;->price:Lcom/chase/sig/android/util/Dollar;

    return-object v0
.end method

.method public final k()Lcom/chase/sig/android/util/Dollar;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/chase/sig/android/domain/InvestmentTransaction;->cost:Lcom/chase/sig/android/util/Dollar;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lcom/chase/sig/android/domain/InvestmentTransaction;->amountCurrency:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcom/chase/sig/android/domain/InvestmentTransaction;->costCurrency:Ljava/lang/String;

    return-object v0
.end method
