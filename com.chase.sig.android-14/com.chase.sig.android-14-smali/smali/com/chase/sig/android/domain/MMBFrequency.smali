.class public final enum Lcom/chase/sig/android/domain/MMBFrequency;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/chase/sig/android/domain/MMBFrequency;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/chase/sig/android/domain/MMBFrequency;

.field public static final enum b:Lcom/chase/sig/android/domain/MMBFrequency;

.field public static final enum c:Lcom/chase/sig/android/domain/MMBFrequency;

.field public static final enum d:Lcom/chase/sig/android/domain/MMBFrequency;

.field public static final enum e:Lcom/chase/sig/android/domain/MMBFrequency;

.field public static final enum f:Lcom/chase/sig/android/domain/MMBFrequency;

.field private static final synthetic g:[Lcom/chase/sig/android/domain/MMBFrequency;


# instance fields
.field private formattedValue:Ljava/lang/String;

.field private labelValue:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 5
    new-instance v0, Lcom/chase/sig/android/domain/MMBFrequency;

    const-string v1, "WEEKLY"

    const-string v2, "WEEKLY"

    const-string v3, "Weekly"

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/chase/sig/android/domain/MMBFrequency;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/MMBFrequency;->a:Lcom/chase/sig/android/domain/MMBFrequency;

    new-instance v0, Lcom/chase/sig/android/domain/MMBFrequency;

    const-string v1, "BIWEEKLY"

    const-string v2, "BIWEEKLY"

    const-string v3, "Every 2 Weeks"

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/chase/sig/android/domain/MMBFrequency;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/MMBFrequency;->b:Lcom/chase/sig/android/domain/MMBFrequency;

    .line 6
    new-instance v0, Lcom/chase/sig/android/domain/MMBFrequency;

    const-string v1, "MONTHLY"

    const-string v2, "MONTHLY"

    const-string v3, "Monthly"

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/chase/sig/android/domain/MMBFrequency;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/MMBFrequency;->c:Lcom/chase/sig/android/domain/MMBFrequency;

    new-instance v0, Lcom/chase/sig/android/domain/MMBFrequency;

    const-string v1, "QUARTERLY"

    const-string v2, "QUARTERLY"

    const-string v3, "Quarterly"

    invoke-direct {v0, v1, v8, v2, v3}, Lcom/chase/sig/android/domain/MMBFrequency;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/MMBFrequency;->d:Lcom/chase/sig/android/domain/MMBFrequency;

    new-instance v0, Lcom/chase/sig/android/domain/MMBFrequency;

    const-string v1, "SEMI_ANNUALLY"

    const-string v2, "SEMI_ANNUALLY"

    const-string v3, "Twice a Year"

    invoke-direct {v0, v1, v9, v2, v3}, Lcom/chase/sig/android/domain/MMBFrequency;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/MMBFrequency;->e:Lcom/chase/sig/android/domain/MMBFrequency;

    .line 7
    new-instance v0, Lcom/chase/sig/android/domain/MMBFrequency;

    const-string v1, "YEARLY"

    const/4 v2, 0x5

    const-string v3, "YEARLY"

    const-string v4, "Yearly"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/chase/sig/android/domain/MMBFrequency;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/MMBFrequency;->f:Lcom/chase/sig/android/domain/MMBFrequency;

    .line 3
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/chase/sig/android/domain/MMBFrequency;

    sget-object v1, Lcom/chase/sig/android/domain/MMBFrequency;->a:Lcom/chase/sig/android/domain/MMBFrequency;

    aput-object v1, v0, v5

    sget-object v1, Lcom/chase/sig/android/domain/MMBFrequency;->b:Lcom/chase/sig/android/domain/MMBFrequency;

    aput-object v1, v0, v6

    sget-object v1, Lcom/chase/sig/android/domain/MMBFrequency;->c:Lcom/chase/sig/android/domain/MMBFrequency;

    aput-object v1, v0, v7

    sget-object v1, Lcom/chase/sig/android/domain/MMBFrequency;->d:Lcom/chase/sig/android/domain/MMBFrequency;

    aput-object v1, v0, v8

    sget-object v1, Lcom/chase/sig/android/domain/MMBFrequency;->e:Lcom/chase/sig/android/domain/MMBFrequency;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lcom/chase/sig/android/domain/MMBFrequency;->f:Lcom/chase/sig/android/domain/MMBFrequency;

    aput-object v2, v0, v1

    sput-object v0, Lcom/chase/sig/android/domain/MMBFrequency;->g:[Lcom/chase/sig/android/domain/MMBFrequency;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 12
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 13
    iput-object p3, p0, Lcom/chase/sig/android/domain/MMBFrequency;->labelValue:Ljava/lang/String;

    .line 14
    iput-object p4, p0, Lcom/chase/sig/android/domain/MMBFrequency;->formattedValue:Ljava/lang/String;

    .line 15
    return-void
.end method

.method public static a(Lcom/chase/sig/android/domain/MMBFrequency;)I
    .locals 1
    .parameter

    .prologue
    .line 25
    if-nez p0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/chase/sig/android/domain/MMBFrequency;->ordinal()I

    move-result v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Lcom/chase/sig/android/domain/MMBFrequency;
    .locals 5
    .parameter

    .prologue
    .line 29
    invoke-static {}, Lcom/chase/sig/android/domain/MMBFrequency;->values()[Lcom/chase/sig/android/domain/MMBFrequency;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 30
    iget-object v4, v0, Lcom/chase/sig/android/domain/MMBFrequency;->labelValue:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 34
    :goto_1
    return-object v0

    .line 29
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 34
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 18
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    sget-object v2, Lcom/chase/sig/android/domain/MMBFrequency;->a:Lcom/chase/sig/android/domain/MMBFrequency;

    iget-object v2, v2, Lcom/chase/sig/android/domain/MMBFrequency;->formattedValue:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/chase/sig/android/domain/MMBFrequency;->b:Lcom/chase/sig/android/domain/MMBFrequency;

    iget-object v2, v2, Lcom/chase/sig/android/domain/MMBFrequency;->formattedValue:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/chase/sig/android/domain/MMBFrequency;->c:Lcom/chase/sig/android/domain/MMBFrequency;

    iget-object v2, v2, Lcom/chase/sig/android/domain/MMBFrequency;->formattedValue:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/chase/sig/android/domain/MMBFrequency;->d:Lcom/chase/sig/android/domain/MMBFrequency;

    iget-object v2, v2, Lcom/chase/sig/android/domain/MMBFrequency;->formattedValue:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/chase/sig/android/domain/MMBFrequency;->e:Lcom/chase/sig/android/domain/MMBFrequency;

    iget-object v2, v2, Lcom/chase/sig/android/domain/MMBFrequency;->formattedValue:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/chase/sig/android/domain/MMBFrequency;->f:Lcom/chase/sig/android/domain/MMBFrequency;

    iget-object v2, v2, Lcom/chase/sig/android/domain/MMBFrequency;->formattedValue:Ljava/lang/String;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/chase/sig/android/domain/MMBFrequency;
    .locals 1
    .parameter

    .prologue
    .line 3
    const-class v0, Lcom/chase/sig/android/domain/MMBFrequency;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/MMBFrequency;

    return-object v0
.end method

.method public static values()[Lcom/chase/sig/android/domain/MMBFrequency;
    .locals 1

    .prologue
    .line 3
    sget-object v0, Lcom/chase/sig/android/domain/MMBFrequency;->g:[Lcom/chase/sig/android/domain/MMBFrequency;

    invoke-virtual {v0}, [Lcom/chase/sig/android/domain/MMBFrequency;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/chase/sig/android/domain/MMBFrequency;

    return-object v0
.end method
