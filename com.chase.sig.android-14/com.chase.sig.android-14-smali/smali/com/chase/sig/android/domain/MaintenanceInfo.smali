.class public Lcom/chase/sig/android/domain/MaintenanceInfo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private announcement:Lcom/chase/sig/android/domain/Announcement;

.field private featuresBlocked:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/chase/sig/android/domain/SplashInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/chase/sig/android/domain/Announcement;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/chase/sig/android/domain/MaintenanceInfo;->announcement:Lcom/chase/sig/android/domain/Announcement;

    return-object v0
.end method

.method public final b()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/chase/sig/android/domain/SplashInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 21
    iget-object v0, p0, Lcom/chase/sig/android/domain/MaintenanceInfo;->featuresBlocked:Ljava/util/ArrayList;

    return-object v0
.end method
