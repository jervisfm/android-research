.class public Lcom/chase/sig/android/domain/MerchantPayee;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private addressLine1:Ljava/lang/String;

.field private addressLine2:Ljava/lang/String;

.field private city:Ljava/lang/String;

.field private name:Ljava/lang/String;

.field private optionId:Ljava/lang/String;

.field private phone:Ljava/lang/String;

.field private state:Lcom/chase/sig/android/domain/State;

.field private zipCode:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/chase/sig/android/domain/MerchantPayee;->optionId:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Lcom/chase/sig/android/domain/State;)V
    .locals 0
    .parameter

    .prologue
    .line 71
    iput-object p1, p0, Lcom/chase/sig/android/domain/MerchantPayee;->state:Lcom/chase/sig/android/domain/State;

    .line 72
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 23
    iput-object p1, p0, Lcom/chase/sig/android/domain/MerchantPayee;->optionId:Ljava/lang/String;

    .line 24
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/chase/sig/android/domain/MerchantPayee;->name:Ljava/lang/String;

    return-object v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 31
    iput-object p1, p0, Lcom/chase/sig/android/domain/MerchantPayee;->name:Ljava/lang/String;

    .line 32
    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/chase/sig/android/domain/MerchantPayee;->phone:Ljava/lang/String;

    return-object v0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 39
    iput-object p1, p0, Lcom/chase/sig/android/domain/MerchantPayee;->phone:Ljava/lang/String;

    .line 40
    return-void
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/chase/sig/android/domain/MerchantPayee;->addressLine1:Ljava/lang/String;

    return-object v0
.end method

.method public final d(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 47
    iput-object p1, p0, Lcom/chase/sig/android/domain/MerchantPayee;->addressLine1:Ljava/lang/String;

    .line 48
    return-void
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/chase/sig/android/domain/MerchantPayee;->addressLine2:Ljava/lang/String;

    return-object v0
.end method

.method public final e(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 55
    iput-object p1, p0, Lcom/chase/sig/android/domain/MerchantPayee;->addressLine2:Ljava/lang/String;

    .line 56
    return-void
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/chase/sig/android/domain/MerchantPayee;->city:Ljava/lang/String;

    return-object v0
.end method

.method public final f(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 63
    iput-object p1, p0, Lcom/chase/sig/android/domain/MerchantPayee;->city:Ljava/lang/String;

    .line 64
    return-void
.end method

.method public final g()Lcom/chase/sig/android/domain/State;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/chase/sig/android/domain/MerchantPayee;->state:Lcom/chase/sig/android/domain/State;

    return-object v0
.end method

.method public final g(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 79
    iput-object p1, p0, Lcom/chase/sig/android/domain/MerchantPayee;->zipCode:Ljava/lang/String;

    .line 80
    return-void
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/chase/sig/android/domain/MerchantPayee;->zipCode:Ljava/lang/String;

    return-object v0
.end method
