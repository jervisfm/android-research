.class public Lcom/chase/sig/android/domain/OneTimePasswordContact;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private id:Ljava/lang/String;

.field private label:Ljava/lang/String;

.field private mask:Ljava/lang/String;

.field private type:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/chase/sig/android/domain/OneTimePasswordContact;->id:Ljava/lang/String;

    .line 22
    iput-object p2, p0, Lcom/chase/sig/android/domain/OneTimePasswordContact;->mask:Ljava/lang/String;

    .line 23
    iput-object p3, p0, Lcom/chase/sig/android/domain/OneTimePasswordContact;->type:Ljava/lang/String;

    .line 24
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/chase/sig/android/domain/OneTimePasswordContact;->id:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/chase/sig/android/domain/OneTimePasswordContact;->mask:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/chase/sig/android/domain/OneTimePasswordContact;->type:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 40
    iget-object v0, p0, Lcom/chase/sig/android/domain/OneTimePasswordContact;->type:Ljava/lang/String;

    const-string v1, "EMAIL"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 41
    const-string v0, "Email"

    iput-object v0, p0, Lcom/chase/sig/android/domain/OneTimePasswordContact;->label:Ljava/lang/String;

    .line 48
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/chase/sig/android/domain/OneTimePasswordContact;->label:Ljava/lang/String;

    return-object v0

    .line 42
    :cond_1
    iget-object v0, p0, Lcom/chase/sig/android/domain/OneTimePasswordContact;->type:Ljava/lang/String;

    const-string v1, "PHONE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 43
    const-string v0, "Call"

    iput-object v0, p0, Lcom/chase/sig/android/domain/OneTimePasswordContact;->label:Ljava/lang/String;

    goto :goto_0

    .line 44
    :cond_2
    iget-object v0, p0, Lcom/chase/sig/android/domain/OneTimePasswordContact;->type:Ljava/lang/String;

    const-string v1, "TEXT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 45
    const-string v0, "Text"

    iput-object v0, p0, Lcom/chase/sig/android/domain/OneTimePasswordContact;->label:Ljava/lang/String;

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 53
    const-string v0, "???"

    .line 55
    iget-object v1, p0, Lcom/chase/sig/android/domain/OneTimePasswordContact;->type:Ljava/lang/String;

    const-string v2, "EMAIL"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 56
    const-string v0, "EMAIL"

    .line 63
    :cond_0
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/chase/sig/android/domain/OneTimePasswordContact;->mask:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 57
    :cond_1
    iget-object v1, p0, Lcom/chase/sig/android/domain/OneTimePasswordContact;->type:Ljava/lang/String;

    const-string v2, "PHONE"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 58
    const-string v0, "CALL"

    goto :goto_0

    .line 59
    :cond_2
    iget-object v1, p0, Lcom/chase/sig/android/domain/OneTimePasswordContact;->type:Ljava/lang/String;

    const-string v2, "TEXT"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 60
    const-string v0, "TEXT"

    goto :goto_0
.end method
