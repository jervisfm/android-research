.class public Lcom/chase/sig/android/domain/Payee;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private accountNumber:Ljava/lang/String;

.field private accountNumberMask:Ljava/lang/String;

.field private address:Lcom/chase/sig/android/domain/PayeeAddress;

.field private bankInstructions:Ljava/lang/String;

.field private earliestPaymentDeliveryDate:Ljava/lang/String;

.field private earliestPaymentSendDate:Ljava/lang/String;

.field private lastPaymentAmount:Lcom/chase/sig/android/util/Dollar;

.field private lastPaymentDate:Ljava/lang/String;

.field private leadTime:I

.field private memo:Ljava/lang/String;

.field private name:Ljava/lang/String;

.field private nickName:Ljava/lang/String;

.field private payeeId:Ljava/lang/String;

.field private payeeInstructions:Ljava/lang/String;

.field private phoneNumber:Ljava/lang/String;

.field private status:Ljava/lang/String;

.field private token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const/4 v0, -0x1

    iput v0, p0, Lcom/chase/sig/android/domain/Payee;->leadTime:I

    .line 27
    new-instance v0, Lcom/chase/sig/android/domain/PayeeAddress;

    invoke-direct {v0}, Lcom/chase/sig/android/domain/PayeeAddress;-><init>()V

    iput-object v0, p0, Lcom/chase/sig/android/domain/Payee;->address:Lcom/chase/sig/android/domain/PayeeAddress;

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/chase/sig/android/domain/Payee;->leadTime:I

    return v0
.end method

.method public final a(Lcom/chase/sig/android/domain/PayeeAddress;)V
    .locals 0
    .parameter

    .prologue
    .line 205
    iput-object p1, p0, Lcom/chase/sig/android/domain/Payee;->address:Lcom/chase/sig/android/domain/PayeeAddress;

    .line 206
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 60
    iput-object p1, p0, Lcom/chase/sig/android/domain/Payee;->accountNumber:Ljava/lang/String;

    .line 61
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lcom/chase/sig/android/domain/Payee;->leadTime:I

    packed-switch v0, :pswitch_data_0

    .line 51
    :pswitch_0
    const-string v0, ""

    :goto_0
    return-object v0

    .line 43
    :pswitch_1
    const-string v0, "Same day"

    goto :goto_0

    .line 45
    :pswitch_2
    const-string v0, "1-day"

    goto :goto_0

    .line 47
    :pswitch_3
    const-string v0, "2-day"

    goto :goto_0

    .line 49
    :pswitch_4
    const-string v0, "5-day"

    goto :goto_0

    .line 41
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 76
    iput-object p1, p0, Lcom/chase/sig/android/domain/Payee;->bankInstructions:Ljava/lang/String;

    .line 77
    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/chase/sig/android/domain/Payee;->accountNumber:Ljava/lang/String;

    return-object v0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 84
    iput-object p1, p0, Lcom/chase/sig/android/domain/Payee;->earliestPaymentDeliveryDate:Ljava/lang/String;

    .line 85
    return-void
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/chase/sig/android/domain/Payee;->accountNumberMask:Ljava/lang/String;

    return-object v0
.end method

.method public final d(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 96
    iput-object p1, p0, Lcom/chase/sig/android/domain/Payee;->earliestPaymentSendDate:Ljava/lang/String;

    .line 97
    return-void
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/chase/sig/android/domain/Payee;->earliestPaymentDeliveryDate:Ljava/lang/String;

    return-object v0
.end method

.method public final e(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 104
    iput-object p1, p0, Lcom/chase/sig/android/domain/Payee;->memo:Ljava/lang/String;

    .line 105
    return-void
.end method

.method public final f()Ljava/util/Date;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/chase/sig/android/domain/Payee;->earliestPaymentDeliveryDate:Ljava/lang/String;

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->g(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public final f(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 112
    iput-object p1, p0, Lcom/chase/sig/android/domain/Payee;->nickName:Ljava/lang/String;

    .line 113
    return-void
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/chase/sig/android/domain/Payee;->memo:Ljava/lang/String;

    return-object v0
.end method

.method public final g(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 120
    iput-object p1, p0, Lcom/chase/sig/android/domain/Payee;->name:Ljava/lang/String;

    .line 121
    return-void
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/chase/sig/android/domain/Payee;->nickName:Ljava/lang/String;

    return-object v0
.end method

.method public final h(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 128
    iput-object p1, p0, Lcom/chase/sig/android/domain/Payee;->payeeId:Ljava/lang/String;

    .line 129
    return-void
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/chase/sig/android/domain/Payee;->name:Ljava/lang/String;

    return-object v0
.end method

.method public final i(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 136
    iput-object p1, p0, Lcom/chase/sig/android/domain/Payee;->payeeInstructions:Ljava/lang/String;

    .line 137
    return-void
.end method

.method public final j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/chase/sig/android/domain/Payee;->payeeId:Ljava/lang/String;

    return-object v0
.end method

.method public final j(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 213
    iput-object p1, p0, Lcom/chase/sig/android/domain/Payee;->phoneNumber:Ljava/lang/String;

    .line 214
    return-void
.end method

.method public final k()Ljava/lang/String;
    .locals 2

    .prologue
    .line 165
    iget-object v0, p0, Lcom/chase/sig/android/domain/Payee;->lastPaymentDate:Ljava/lang/String;

    .line 167
    invoke-static {v0}, Lcom/chase/sig/android/util/s;->l(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "null"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 168
    :cond_0
    const-string v0, "No activity"

    .line 171
    :goto_0
    return-object v0

    :cond_1
    invoke-static {v0}, Lcom/chase/sig/android/util/s;->h(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/chase/sig/android/domain/Payee;->lastPaymentAmount:Lcom/chase/sig/android/util/Dollar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/domain/Payee;->lastPaymentAmount:Lcom/chase/sig/android/util/Dollar;

    invoke-virtual {v0}, Lcom/chase/sig/android/util/Dollar;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->l(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 178
    :cond_0
    const-string v0, "--"

    .line 181
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/chase/sig/android/domain/Payee;->lastPaymentAmount:Lcom/chase/sig/android/util/Dollar;

    invoke-virtual {v0}, Lcom/chase/sig/android/util/Dollar;->h()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/chase/sig/android/domain/Payee;->status:Ljava/lang/String;

    return-object v0
.end method

.method public final n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lcom/chase/sig/android/domain/Payee;->token:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/domain/Payee;->token:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public final o()Lcom/chase/sig/android/domain/PayeeAddress;
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/chase/sig/android/domain/Payee;->address:Lcom/chase/sig/android/domain/PayeeAddress;

    return-object v0
.end method

.method public final p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/chase/sig/android/domain/Payee;->phoneNumber:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/chase/sig/android/domain/Payee;->nickName:Ljava/lang/String;

    return-object v0
.end method
