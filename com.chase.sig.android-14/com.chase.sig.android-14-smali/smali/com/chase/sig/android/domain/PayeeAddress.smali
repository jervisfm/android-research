.class public Lcom/chase/sig/android/domain/PayeeAddress;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private addressLine1:Ljava/lang/String;

.field private addressLine2:Ljava/lang/String;

.field private city:Ljava/lang/String;

.field private countryCode:Ljava/lang/String;

.field private postalCode:Ljava/lang/String;

.field private state:Lcom/chase/sig/android/domain/State;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/chase/sig/android/domain/PayeeAddress;->addressLine1:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Lcom/chase/sig/android/domain/State;)V
    .locals 0
    .parameter

    .prologue
    .line 60
    iput-object p1, p0, Lcom/chase/sig/android/domain/PayeeAddress;->state:Lcom/chase/sig/android/domain/State;

    .line 61
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 20
    iput-object p1, p0, Lcom/chase/sig/android/domain/PayeeAddress;->addressLine1:Ljava/lang/String;

    .line 21
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/chase/sig/android/domain/PayeeAddress;->addressLine2:Ljava/lang/String;

    return-object v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 28
    iput-object p1, p0, Lcom/chase/sig/android/domain/PayeeAddress;->addressLine2:Ljava/lang/String;

    .line 29
    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/chase/sig/android/domain/PayeeAddress;->city:Ljava/lang/String;

    return-object v0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 36
    iput-object p1, p0, Lcom/chase/sig/android/domain/PayeeAddress;->city:Ljava/lang/String;

    .line 37
    return-void
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/chase/sig/android/domain/PayeeAddress;->postalCode:Ljava/lang/String;

    return-object v0
.end method

.method public final d(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 44
    iput-object p1, p0, Lcom/chase/sig/android/domain/PayeeAddress;->postalCode:Ljava/lang/String;

    .line 45
    return-void
.end method

.method public final e()Lcom/chase/sig/android/domain/State;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/chase/sig/android/domain/PayeeAddress;->state:Lcom/chase/sig/android/domain/State;

    return-object v0
.end method
