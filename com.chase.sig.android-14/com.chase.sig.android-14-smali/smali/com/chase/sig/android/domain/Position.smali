.class public Lcom/chase/sig/android/domain/Position;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/io/Serializable;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/chase/sig/android/domain/Position;",
        ">;"
    }
.end annotation


# instance fields
.field private assetClass1:Ljava/lang/String;

.field private assetClass2:Ljava/lang/String;

.field private cost:Lcom/chase/sig/android/util/Dollar;

.field private cusip:Ljava/lang/String;

.field private description:Ljava/lang/String;

.field private isShowAssetClass1:Z

.field private isShowCost:Z

.field private isShowDescription:Z

.field private isShowPrice:Z

.field private isShowPriceDate:Z

.field private isShowQuantity:Z

.field private isShowShortName:Z

.field private isShowSymbol:Z

.field private isShowUnrealizedChange:Z

.field private isShowValue:Z

.field private price:Lcom/chase/sig/android/util/Dollar;

.field private priceAsOfDate:Ljava/lang/String;

.field private quantity:Ljava/lang/String;

.field private quoteCode:Ljava/lang/String;

.field private shortName:Ljava/lang/String;

.field private tickerSymbol:Ljava/lang/String;

.field private unrealizedChange:Lcom/chase/sig/android/util/Dollar;

.field private value:Lcom/chase/sig/android/util/Dollar;

.field private valueChange:Lcom/chase/sig/android/util/Dollar;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/chase/sig/android/util/Dollar;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/chase/sig/android/domain/Position;->value:Lcom/chase/sig/android/util/Dollar;

    return-object v0
.end method

.method public final a(Lcom/chase/sig/android/util/Dollar;)V
    .locals 0
    .parameter

    .prologue
    .line 44
    iput-object p1, p0, Lcom/chase/sig/android/domain/Position;->value:Lcom/chase/sig/android/util/Dollar;

    .line 45
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 52
    iput-object p1, p0, Lcom/chase/sig/android/domain/Position;->shortName:Ljava/lang/String;

    .line 53
    return-void
.end method

.method public final a(Z)V
    .locals 0
    .parameter

    .prologue
    .line 156
    iput-boolean p1, p0, Lcom/chase/sig/android/domain/Position;->isShowAssetClass1:Z

    .line 157
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/chase/sig/android/domain/Position;->shortName:Ljava/lang/String;

    return-object v0
.end method

.method public final b(Lcom/chase/sig/android/util/Dollar;)V
    .locals 0
    .parameter

    .prologue
    .line 60
    iput-object p1, p0, Lcom/chase/sig/android/domain/Position;->price:Lcom/chase/sig/android/util/Dollar;

    .line 61
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 84
    iput-object p1, p0, Lcom/chase/sig/android/domain/Position;->quantity:Ljava/lang/String;

    .line 85
    return-void
.end method

.method public final b(Z)V
    .locals 0
    .parameter

    .prologue
    .line 164
    iput-boolean p1, p0, Lcom/chase/sig/android/domain/Position;->isShowCost:Z

    .line 165
    return-void
.end method

.method public final c()Lcom/chase/sig/android/util/Dollar;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/chase/sig/android/domain/Position;->price:Lcom/chase/sig/android/util/Dollar;

    return-object v0
.end method

.method public final c(Lcom/chase/sig/android/util/Dollar;)V
    .locals 0
    .parameter

    .prologue
    .line 68
    iput-object p1, p0, Lcom/chase/sig/android/domain/Position;->unrealizedChange:Lcom/chase/sig/android/util/Dollar;

    .line 69
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 92
    iput-object p1, p0, Lcom/chase/sig/android/domain/Position;->description:Ljava/lang/String;

    .line 93
    return-void
.end method

.method public final c(Z)V
    .locals 0
    .parameter

    .prologue
    .line 172
    iput-boolean p1, p0, Lcom/chase/sig/android/domain/Position;->isShowDescription:Z

    .line 173
    return-void
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 2
    .parameter

    .prologue
    .line 10
    check-cast p1, Lcom/chase/sig/android/domain/Position;

    iget-object v0, p0, Lcom/chase/sig/android/domain/Position;->shortName:Ljava/lang/String;

    iget-object v1, p1, Lcom/chase/sig/android/domain/Position;->shortName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final d()Lcom/chase/sig/android/util/Dollar;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/chase/sig/android/domain/Position;->unrealizedChange:Lcom/chase/sig/android/util/Dollar;

    return-object v0
.end method

.method public final d(Lcom/chase/sig/android/util/Dollar;)V
    .locals 0
    .parameter

    .prologue
    .line 76
    iput-object p1, p0, Lcom/chase/sig/android/domain/Position;->cost:Lcom/chase/sig/android/util/Dollar;

    .line 77
    return-void
.end method

.method public final d(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 96
    iput-object p1, p0, Lcom/chase/sig/android/domain/Position;->tickerSymbol:Ljava/lang/String;

    .line 97
    return-void
.end method

.method public final d(Z)V
    .locals 0
    .parameter

    .prologue
    .line 180
    iput-boolean p1, p0, Lcom/chase/sig/android/domain/Position;->isShowPrice:Z

    .line 181
    return-void
.end method

.method public final e()Lcom/chase/sig/android/util/Dollar;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/chase/sig/android/domain/Position;->cost:Lcom/chase/sig/android/util/Dollar;

    return-object v0
.end method

.method public final e(Lcom/chase/sig/android/util/Dollar;)V
    .locals 0
    .parameter

    .prologue
    .line 241
    iput-object p1, p0, Lcom/chase/sig/android/domain/Position;->valueChange:Lcom/chase/sig/android/util/Dollar;

    .line 242
    return-void
.end method

.method public final e(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 100
    iput-object p1, p0, Lcom/chase/sig/android/domain/Position;->cusip:Ljava/lang/String;

    .line 101
    return-void
.end method

.method public final e(Z)V
    .locals 0
    .parameter

    .prologue
    .line 188
    iput-boolean p1, p0, Lcom/chase/sig/android/domain/Position;->isShowPriceDate:Z

    .line 189
    return-void
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/chase/sig/android/domain/Position;->quantity:Ljava/lang/String;

    return-object v0
.end method

.method public final f(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 116
    iput-object p1, p0, Lcom/chase/sig/android/domain/Position;->assetClass1:Ljava/lang/String;

    .line 117
    return-void
.end method

.method public final f(Z)V
    .locals 0
    .parameter

    .prologue
    .line 196
    iput-boolean p1, p0, Lcom/chase/sig/android/domain/Position;->isShowQuantity:Z

    .line 197
    return-void
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/chase/sig/android/domain/Position;->description:Ljava/lang/String;

    return-object v0
.end method

.method public final g(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 132
    iput-object p1, p0, Lcom/chase/sig/android/domain/Position;->assetClass2:Ljava/lang/String;

    .line 133
    return-void
.end method

.method public final g(Z)V
    .locals 0
    .parameter

    .prologue
    .line 204
    iput-boolean p1, p0, Lcom/chase/sig/android/domain/Position;->isShowShortName:Z

    .line 205
    return-void
.end method

.method public final h(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 144
    iput-object p1, p0, Lcom/chase/sig/android/domain/Position;->priceAsOfDate:Ljava/lang/String;

    .line 145
    return-void
.end method

.method public final h(Z)V
    .locals 0
    .parameter

    .prologue
    .line 212
    iput-boolean p1, p0, Lcom/chase/sig/android/domain/Position;->isShowSymbol:Z

    .line 213
    return-void
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/chase/sig/android/domain/Position;->tickerSymbol:Ljava/lang/String;

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->l(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    invoke-virtual {p0}, Lcom/chase/sig/android/domain/Position;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/domain/Position;->tickerSymbol:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/domain/Position;->cusip:Ljava/lang/String;

    goto :goto_0
.end method

.method public final i(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 249
    iput-object p1, p0, Lcom/chase/sig/android/domain/Position;->quoteCode:Ljava/lang/String;

    .line 250
    return-void
.end method

.method public final i(Z)V
    .locals 0
    .parameter

    .prologue
    .line 220
    iput-boolean p1, p0, Lcom/chase/sig/android/domain/Position;->isShowUnrealizedChange:Z

    .line 221
    return-void
.end method

.method public final j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 112
    invoke-virtual {p0}, Lcom/chase/sig/android/domain/Position;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Symbol"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "CUSIP"

    goto :goto_0
.end method

.method public final j(Z)V
    .locals 0
    .parameter

    .prologue
    .line 228
    iput-boolean p1, p0, Lcom/chase/sig/android/domain/Position;->isShowValue:Z

    .line 229
    return-void
.end method

.method public final k()Ljava/lang/String;
    .locals 4

    .prologue
    .line 121
    iget-object v0, p0, Lcom/chase/sig/android/domain/Position;->assetClass1:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 122
    const-string v0, "--"

    .line 128
    :goto_0
    return-object v0

    .line 125
    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/domain/Position;->assetClass2:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 126
    iget-object v0, p0, Lcom/chase/sig/android/domain/Position;->assetClass1:Ljava/lang/String;

    goto :goto_0

    .line 128
    :cond_1
    const-string v0, "%s - %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/chase/sig/android/domain/Position;->assetClass1:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/chase/sig/android/domain/Position;->assetClass2:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final l()Lcom/chase/sig/android/domain/PositionType;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/chase/sig/android/domain/Position;->assetClass1:Ljava/lang/String;

    invoke-static {v0}, Lcom/chase/sig/android/domain/PositionType;->a(Ljava/lang/String;)Lcom/chase/sig/android/domain/PositionType;

    move-result-object v0

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/chase/sig/android/domain/Position;->priceAsOfDate:Ljava/lang/String;

    return-object v0
.end method

.method public final n()Z
    .locals 1

    .prologue
    .line 152
    iget-boolean v0, p0, Lcom/chase/sig/android/domain/Position;->isShowAssetClass1:Z

    return v0
.end method

.method public final o()Z
    .locals 1

    .prologue
    .line 160
    iget-boolean v0, p0, Lcom/chase/sig/android/domain/Position;->isShowCost:Z

    return v0
.end method

.method public final p()Z
    .locals 1

    .prologue
    .line 168
    iget-boolean v0, p0, Lcom/chase/sig/android/domain/Position;->isShowDescription:Z

    return v0
.end method

.method public final q()Z
    .locals 1

    .prologue
    .line 176
    iget-boolean v0, p0, Lcom/chase/sig/android/domain/Position;->isShowPrice:Z

    return v0
.end method

.method public final r()Z
    .locals 1

    .prologue
    .line 184
    iget-boolean v0, p0, Lcom/chase/sig/android/domain/Position;->isShowPriceDate:Z

    return v0
.end method

.method public final s()Z
    .locals 1

    .prologue
    .line 192
    iget-boolean v0, p0, Lcom/chase/sig/android/domain/Position;->isShowQuantity:Z

    return v0
.end method

.method public final t()Z
    .locals 1

    .prologue
    .line 200
    iget-boolean v0, p0, Lcom/chase/sig/android/domain/Position;->isShowShortName:Z

    return v0
.end method

.method public final u()Z
    .locals 1

    .prologue
    .line 208
    iget-boolean v0, p0, Lcom/chase/sig/android/domain/Position;->isShowSymbol:Z

    return v0
.end method

.method public final v()Z
    .locals 1

    .prologue
    .line 216
    iget-boolean v0, p0, Lcom/chase/sig/android/domain/Position;->isShowUnrealizedChange:Z

    return v0
.end method

.method public final w()Z
    .locals 1

    .prologue
    .line 224
    iget-boolean v0, p0, Lcom/chase/sig/android/domain/Position;->isShowValue:Z

    return v0
.end method

.method public final x()Lcom/chase/sig/android/util/Dollar;
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Lcom/chase/sig/android/domain/Position;->valueChange:Lcom/chase/sig/android/util/Dollar;

    return-object v0
.end method

.method public final y()Ljava/lang/String;
    .locals 1

    .prologue
    .line 245
    iget-object v0, p0, Lcom/chase/sig/android/domain/Position;->quoteCode:Ljava/lang/String;

    return-object v0
.end method
