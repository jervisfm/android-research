.class public final enum Lcom/chase/sig/android/domain/PositionType;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/chase/sig/android/domain/PositionType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/chase/sig/android/domain/PositionType;

.field public static final enum b:Lcom/chase/sig/android/domain/PositionType;

.field public static final enum c:Lcom/chase/sig/android/domain/PositionType;

.field public static final enum d:Lcom/chase/sig/android/domain/PositionType;

.field public static final enum e:Lcom/chase/sig/android/domain/PositionType;

.field public static final enum f:Lcom/chase/sig/android/domain/PositionType;

.field private static final synthetic g:[Lcom/chase/sig/android/domain/PositionType;


# instance fields
.field private final type:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 4
    new-instance v0, Lcom/chase/sig/android/domain/PositionType;

    const-string v1, "EQUITY"

    const-string v2, "Equity"

    invoke-direct {v0, v1, v4, v2}, Lcom/chase/sig/android/domain/PositionType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/PositionType;->a:Lcom/chase/sig/android/domain/PositionType;

    .line 5
    new-instance v0, Lcom/chase/sig/android/domain/PositionType;

    const-string v1, "FIXED_INCOME_AND_CASH"

    const-string v2, "Fixed Income & Cash"

    invoke-direct {v0, v1, v5, v2}, Lcom/chase/sig/android/domain/PositionType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/PositionType;->b:Lcom/chase/sig/android/domain/PositionType;

    .line 6
    new-instance v0, Lcom/chase/sig/android/domain/PositionType;

    const-string v1, "ALTERNATIVES"

    const-string v2, "Alternative Assets"

    invoke-direct {v0, v1, v6, v2}, Lcom/chase/sig/android/domain/PositionType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/PositionType;->c:Lcom/chase/sig/android/domain/PositionType;

    .line 7
    new-instance v0, Lcom/chase/sig/android/domain/PositionType;

    const-string v1, "DIVERSIFIED_STRATEGIES"

    const-string v2, "Diversified Strategies"

    invoke-direct {v0, v1, v7, v2}, Lcom/chase/sig/android/domain/PositionType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/PositionType;->d:Lcom/chase/sig/android/domain/PositionType;

    .line 8
    new-instance v0, Lcom/chase/sig/android/domain/PositionType;

    const-string v1, "OTHER"

    const-string v2, "Other"

    invoke-direct {v0, v1, v8, v2}, Lcom/chase/sig/android/domain/PositionType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/PositionType;->e:Lcom/chase/sig/android/domain/PositionType;

    .line 9
    new-instance v0, Lcom/chase/sig/android/domain/PositionType;

    const-string v1, "LIABILITIES"

    const/4 v2, 0x5

    const-string v3, "Liabilities"

    invoke-direct {v0, v1, v2, v3}, Lcom/chase/sig/android/domain/PositionType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/PositionType;->f:Lcom/chase/sig/android/domain/PositionType;

    .line 3
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/chase/sig/android/domain/PositionType;

    sget-object v1, Lcom/chase/sig/android/domain/PositionType;->a:Lcom/chase/sig/android/domain/PositionType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/chase/sig/android/domain/PositionType;->b:Lcom/chase/sig/android/domain/PositionType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/chase/sig/android/domain/PositionType;->c:Lcom/chase/sig/android/domain/PositionType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/chase/sig/android/domain/PositionType;->d:Lcom/chase/sig/android/domain/PositionType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/chase/sig/android/domain/PositionType;->e:Lcom/chase/sig/android/domain/PositionType;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/chase/sig/android/domain/PositionType;->f:Lcom/chase/sig/android/domain/PositionType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/chase/sig/android/domain/PositionType;->g:[Lcom/chase/sig/android/domain/PositionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 13
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 14
    iput-object p3, p0, Lcom/chase/sig/android/domain/PositionType;->type:Ljava/lang/String;

    .line 15
    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/chase/sig/android/domain/PositionType;
    .locals 5
    .parameter

    .prologue
    .line 18
    invoke-static {}, Lcom/chase/sig/android/domain/PositionType;->values()[Lcom/chase/sig/android/domain/PositionType;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 19
    iget-object v4, v3, Lcom/chase/sig/android/domain/PositionType;->type:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 20
    return-object v3

    .line 18
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 23
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/chase/sig/android/domain/PositionType;
    .locals 1
    .parameter

    .prologue
    .line 3
    const-class v0, Lcom/chase/sig/android/domain/PositionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/PositionType;

    return-object v0
.end method

.method public static values()[Lcom/chase/sig/android/domain/PositionType;
    .locals 1

    .prologue
    .line 3
    sget-object v0, Lcom/chase/sig/android/domain/PositionType;->g:[Lcom/chase/sig/android/domain/PositionType;

    invoke-virtual {v0}, [Lcom/chase/sig/android/domain/PositionType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/chase/sig/android/domain/PositionType;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/chase/sig/android/domain/PositionType;->type:Ljava/lang/String;

    return-object v0
.end method
