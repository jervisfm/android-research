.class public Lcom/chase/sig/android/domain/QuickDeposit;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private accountNumber:Ljava/lang/String;

.field private amount:Lcom/chase/sig/android/util/Dollar;

.field private availableBalance:Lcom/chase/sig/android/util/Dollar;

.field private checkImageBack:[B

.field private checkImageFront:[B

.field private delayedAmount1:Lcom/chase/sig/android/util/Dollar;

.field private delayedAmount2:Lcom/chase/sig/android/util/Dollar;

.field private delayedAmountAvailableDate1:Ljava/lang/String;

.field private delayedAmountAvailableDate2:Ljava/lang/String;

.field private depositToAccount:Lcom/chase/sig/android/domain/QuickDepositAccount;

.field private effectiveDate:Ljava/util/Date;

.field private imageHeightUsed:I

.field private imageWidthUsed:I

.field private itemSequenceNumber:Ljava/lang/String;

.field private largeDollar:Ljava/lang/String;

.field private locationId:Ljava/lang/String;

.field private locationName:Ljava/lang/String;

.field private onus:Ljava/lang/String;

.field private payingBank:Ljava/lang/String;

.field private presentBalance:Lcom/chase/sig/android/util/Dollar;

.field private readAmount:Lcom/chase/sig/android/util/Dollar;

.field private routingNumber:Ljava/lang/String;

.field private standardBottom:Ljava/lang/String;

.field private submitCounter:I

.field private transactionId:Ljava/lang/String;

.field private updateRequired:Ljava/lang/String;

.field private variableBottom:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Intent;)Lcom/chase/sig/android/domain/QuickDeposit;
    .locals 2
    .parameter

    .prologue
    .line 247
    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_0

    .line 248
    new-instance v0, Lcom/chase/sig/android/domain/QuickDeposit;

    invoke-direct {v0}, Lcom/chase/sig/android/domain/QuickDeposit;-><init>()V

    .line 251
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "quick_deposit"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/QuickDeposit;

    goto :goto_0
.end method

.method private static c([B)Landroid/graphics/Bitmap;
    .locals 7
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 176
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 177
    const/4 v2, 0x4

    iput v2, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 179
    array-length v2, p0

    invoke-static {p0, v1, v2, v0}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 181
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 182
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    .line 184
    const/16 v2, 0x32

    if-ge v4, v2, :cond_0

    .line 199
    :goto_0
    return-object v0

    .line 188
    :cond_0
    const/high16 v2, 0x4248

    int-to-float v5, v4

    div-float/2addr v2, v5

    .line 191
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 194
    invoke-virtual {v5, v2, v2}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 197
    const/4 v6, 0x1

    move v2, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 198
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    move-object v0, v1

    .line 199
    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 75
    iget v0, p0, Lcom/chase/sig/android/domain/QuickDeposit;->submitCounter:I

    return v0
.end method

.method public final a(I)V
    .locals 0
    .parameter

    .prologue
    .line 79
    iput p1, p0, Lcom/chase/sig/android/domain/QuickDeposit;->submitCounter:I

    .line 80
    return-void
.end method

.method public final a(Lcom/chase/sig/android/domain/QuickDepositAccount;)V
    .locals 0
    .parameter

    .prologue
    .line 275
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickDeposit;->depositToAccount:Lcom/chase/sig/android/domain/QuickDepositAccount;

    .line 276
    return-void
.end method

.method public final a(Lcom/chase/sig/android/util/Dollar;)V
    .locals 0
    .parameter

    .prologue
    .line 203
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickDeposit;->amount:Lcom/chase/sig/android/util/Dollar;

    .line 204
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 87
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickDeposit;->transactionId:Ljava/lang/String;

    .line 88
    return-void
.end method

.method public final a([B)V
    .locals 0
    .parameter

    .prologue
    .line 207
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickDeposit;->checkImageBack:[B

    .line 208
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickDeposit;->transactionId:Ljava/lang/String;

    return-object v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 91
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickDeposit;->routingNumber:Ljava/lang/String;

    .line 92
    return-void
.end method

.method public final b([B)V
    .locals 0
    .parameter

    .prologue
    .line 211
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickDeposit;->checkImageFront:[B

    .line 212
    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickDeposit;->accountNumber:Ljava/lang/String;

    return-object v0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 95
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickDeposit;->accountNumber:Ljava/lang/String;

    .line 96
    return-void
.end method

.method public final d(Ljava/lang/String;)V
    .locals 1
    .parameter

    .prologue
    .line 223
    invoke-static {p1}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 224
    invoke-static {p1}, Lcom/chase/sig/android/util/s;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/domain/QuickDeposit;->effectiveDate:Ljava/util/Date;

    .line 229
    :goto_0
    return-void

    .line 226
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/chase/sig/android/domain/QuickDeposit;->effectiveDate:Ljava/util/Date;

    goto :goto_0
.end method

.method public final d()[B
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickDeposit;->checkImageBack:[B

    return-object v0
.end method

.method public final e()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickDeposit;->checkImageBack:[B

    invoke-static {v0}, Lcom/chase/sig/android/domain/QuickDeposit;->c([B)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public final e(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 259
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickDeposit;->locationId:Ljava/lang/String;

    .line 260
    return-void
.end method

.method public final f(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 267
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickDeposit;->locationName:Ljava/lang/String;

    .line 268
    return-void
.end method

.method public final f()[B
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickDeposit;->checkImageFront:[B

    return-object v0
.end method

.method public final g()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickDeposit;->checkImageFront:[B

    invoke-static {v0}, Lcom/chase/sig/android/domain/QuickDeposit;->c([B)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public final g(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 323
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickDeposit;->itemSequenceNumber:Ljava/lang/String;

    .line 324
    return-void
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickDeposit;->routingNumber:Ljava/lang/String;

    return-object v0
.end method

.method public final i()Lcom/chase/sig/android/util/Dollar;
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickDeposit;->amount:Lcom/chase/sig/android/util/Dollar;

    return-object v0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickDeposit;->checkImageFront:[B

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickDeposit;->checkImageBack:[B

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()Lcom/chase/sig/android/util/Dollar;
    .locals 1

    .prologue
    .line 243
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickDeposit;->readAmount:Lcom/chase/sig/android/util/Dollar;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickDeposit;->locationId:Ljava/lang/String;

    return-object v0
.end method

.method public final n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 263
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickDeposit;->locationName:Ljava/lang/String;

    return-object v0
.end method

.method public final o()Lcom/chase/sig/android/domain/QuickDepositAccount;
    .locals 1

    .prologue
    .line 271
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickDeposit;->depositToAccount:Lcom/chase/sig/android/domain/QuickDepositAccount;

    return-object v0
.end method

.method public final p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickDeposit;->itemSequenceNumber:Ljava/lang/String;

    return-object v0
.end method
