.class public Lcom/chase/sig/android/domain/QuickDepositAccount$Authorization;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/domain/QuickDepositAccount;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Authorization"
.end annotation


# instance fields
.field private authorized:Z

.field private deniedReasonCode:Ljava/lang/String;

.field private deniedReasonMessage:Ljava/lang/String;

.field final synthetic this$0:Lcom/chase/sig/android/domain/QuickDepositAccount;


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickDepositAccount$Authorization;->deniedReasonMessage:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 96
    iget-boolean v0, p0, Lcom/chase/sig/android/domain/QuickDepositAccount$Authorization;->authorized:Z

    return v0
.end method
