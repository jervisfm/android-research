.class public Lcom/chase/sig/android/domain/QuickDepositAccount$Location;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/domain/QuickDepositAccount;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Location"
.end annotation


# instance fields
.field private id:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/chase/annotations/SerializedName;
        a = "ulid"
    .end annotation
.end field

.field private name:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/chase/annotations/SerializedName;
        a = "ulidName"
    .end annotation
.end field

.field final synthetic this$0:Lcom/chase/sig/android/domain/QuickDepositAccount;


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickDepositAccount$Location;->id:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickDepositAccount$Location;->name:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickDepositAccount$Location;->name:Ljava/lang/String;

    return-object v0
.end method
