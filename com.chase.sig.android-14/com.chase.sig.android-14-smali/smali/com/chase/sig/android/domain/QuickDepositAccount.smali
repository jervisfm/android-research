.class public Lcom/chase/sig/android/domain/QuickDepositAccount;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/domain/QuickDepositAccount$Location;,
        Lcom/chase/sig/android/domain/QuickDepositAccount$Authorization;
    }
.end annotation


# instance fields
.field private accountId:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/chase/annotations/SerializedName;
        a = "id"
    .end annotation
.end field

.field private accountNickname:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/chase/annotations/SerializedName;
        a = "nickname"
    .end annotation
.end field

.field private authorization:Lcom/chase/sig/android/domain/QuickDepositAccount$Authorization;

.field private availableBalance:Lcom/chase/sig/android/util/Dollar;

.field private locations:Ljava/util/List;
    .annotation runtime Lcom/google/gson/chase/annotations/SerializedName;
        a = "ulidDetails"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/QuickDepositAccount$Location;",
            ">;"
        }
    .end annotation
.end field

.field private mask:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 105
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickDepositAccount;->accountId:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickDepositAccount;->accountNickname:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/QuickDepositAccount$Location;",
            ">;"
        }
    .end annotation

    .prologue
    .line 41
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickDepositAccount;->locations:Ljava/util/List;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickDepositAccount;->mask:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Lcom/chase/sig/android/domain/QuickDepositAccount$Authorization;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickDepositAccount;->authorization:Lcom/chase/sig/android/domain/QuickDepositAccount$Authorization;

    return-object v0
.end method

.method public final f()Lcom/chase/sig/android/util/Dollar;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickDepositAccount;->availableBalance:Lcom/chase/sig/android/util/Dollar;

    return-object v0
.end method
