.class public Lcom/chase/sig/android/domain/QuickPayActivityItem;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/domain/h;
.implements Ljava/io/Serializable;


# instance fields
.field private acceptedOn:Lcom/chase/sig/android/domain/LabeledValue;

.field private accountName:Lcom/chase/sig/android/domain/LabeledValue;

.field private action:Lcom/chase/sig/android/domain/LabeledValue;

.field private amount:Lcom/chase/sig/android/domain/LabeledValue;

.field private atmEligible:Lcom/chase/sig/android/domain/LabeledValue;

.field private date:Lcom/chase/sig/android/domain/LabeledValue;

.field private decline:Lcom/chase/sig/android/domain/LabeledValue;

.field private declineReason:Lcom/chase/sig/android/domain/LabeledValue;

.field private dueDate:Lcom/chase/sig/android/domain/LabeledValue;

.field private frequency:Lcom/chase/sig/android/domain/LabeledValue;

.field private id:Lcom/chase/sig/android/domain/LabeledValue;

.field private invoice:Lcom/chase/sig/android/domain/LabeledValue;

.field private isInvoiceRequest:Lcom/chase/sig/android/domain/LabeledValue;

.field private isSmsEligible:Lcom/chase/sig/android/domain/LabeledValue;

.field private isSuccessfulResentNotification:Z

.field private memo:Lcom/chase/sig/android/domain/LabeledValue;

.field private nextdeliverbydate:Lcom/chase/sig/android/domain/LabeledValue;

.field private nextsendondate:Lcom/chase/sig/android/domain/LabeledValue;

.field private numberofremainingpayments:Lcom/chase/sig/android/domain/LabeledValue;

.field private openended:Lcom/chase/sig/android/domain/LabeledValue;

.field private receivedOn:Lcom/chase/sig/android/domain/LabeledValue;

.field private recipient:Lcom/chase/sig/android/domain/QuickPayRecipient;

.field private recipientCode:Lcom/chase/sig/android/domain/LabeledValue;

.field private repeatingid:Lcom/chase/sig/android/domain/LabeledValue;

.field private resentNotificationMessage:Ljava/lang/String;

.field private senderCode:Lcom/chase/sig/android/domain/LabeledValue;

.field private sentOn:Lcom/chase/sig/android/domain/LabeledValue;

.field private smsReason:Ljava/lang/String;

.field private status:Lcom/chase/sig/android/domain/LabeledValue;

.field private token:Lcom/chase/sig/android/domain/LabeledValue;

.field private type:Lcom/chase/sig/android/domain/LabeledValue;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v0, Lcom/chase/sig/android/domain/QuickPayRecipient;

    invoke-direct {v0}, Lcom/chase/sig/android/domain/QuickPayRecipient;-><init>()V

    iput-object v0, p0, Lcom/chase/sig/android/domain/QuickPayActivityItem;->recipient:Lcom/chase/sig/android/domain/QuickPayRecipient;

    return-void
.end method


# virtual methods
.method public final A()Lcom/chase/sig/android/domain/LabeledValue;
    .locals 1

    .prologue
    .line 288
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayActivityItem;->recipientCode:Lcom/chase/sig/android/domain/LabeledValue;

    return-object v0
.end method

.method public final B()Ljava/lang/String;
    .locals 1

    .prologue
    .line 340
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayActivityItem;->resentNotificationMessage:Ljava/lang/String;

    return-object v0
.end method

.method public final C()Lcom/chase/sig/android/domain/LabeledValue;
    .locals 1

    .prologue
    .line 365
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayActivityItem;->isInvoiceRequest:Lcom/chase/sig/android/domain/LabeledValue;

    return-object v0
.end method

.method public final D()Ljava/lang/String;
    .locals 1

    .prologue
    .line 377
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayActivityItem;->declineReason:Lcom/chase/sig/android/domain/LabeledValue;

    if-eqz v0, :cond_0

    .line 378
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayActivityItem;->declineReason:Lcom/chase/sig/android/domain/LabeledValue;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v0

    .line 380
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 211
    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->o()Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/chase/sig/android/domain/LabeledValue;)V
    .locals 0
    .parameter

    .prologue
    .line 48
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickPayActivityItem;->date:Lcom/chase/sig/android/domain/LabeledValue;

    .line 49
    return-void
.end method

.method public final a(Lcom/chase/sig/android/domain/QuickPayRecipient;)V
    .locals 0
    .parameter

    .prologue
    .line 317
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickPayActivityItem;->recipient:Lcom/chase/sig/android/domain/QuickPayRecipient;

    .line 318
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 336
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickPayActivityItem;->resentNotificationMessage:Ljava/lang/String;

    .line 337
    return-void
.end method

.method public final a(Z)V
    .locals 0
    .parameter

    .prologue
    .line 344
    iput-boolean p1, p0, Lcom/chase/sig/android/domain/QuickPayActivityItem;->isSuccessfulResentNotification:Z

    .line 345
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 3

    .prologue
    .line 206
    new-instance v0, Lcom/chase/sig/android/domain/LabeledValue;

    const-string v1, ""

    iget-object v2, p0, Lcom/chase/sig/android/domain/QuickPayActivityItem;->recipient:Lcom/chase/sig/android/domain/QuickPayRecipient;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/QuickPayRecipient;->d()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/chase/sig/android/domain/LabeledValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/chase/sig/android/domain/LabeledValue;)V
    .locals 0
    .parameter

    .prologue
    .line 56
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickPayActivityItem;->amount:Lcom/chase/sig/android/domain/LabeledValue;

    .line 57
    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayActivityItem;->accountName:Lcom/chase/sig/android/domain/LabeledValue;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c(Lcom/chase/sig/android/domain/LabeledValue;)V
    .locals 0
    .parameter

    .prologue
    .line 68
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickPayActivityItem;->status:Lcom/chase/sig/android/domain/LabeledValue;

    .line 69
    return-void
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayActivityItem;->amount:Lcom/chase/sig/android/domain/LabeledValue;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d(Lcom/chase/sig/android/domain/LabeledValue;)V
    .locals 0
    .parameter

    .prologue
    .line 76
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickPayActivityItem;->id:Lcom/chase/sig/android/domain/LabeledValue;

    .line 77
    return-void
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayActivityItem;->memo:Lcom/chase/sig/android/domain/LabeledValue;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final e(Lcom/chase/sig/android/domain/LabeledValue;)V
    .locals 0
    .parameter

    .prologue
    .line 84
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickPayActivityItem;->type:Lcom/chase/sig/android/domain/LabeledValue;

    .line 85
    return-void
.end method

.method public final f(Lcom/chase/sig/android/domain/LabeledValue;)V
    .locals 0
    .parameter

    .prologue
    .line 92
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickPayActivityItem;->action:Lcom/chase/sig/android/domain/LabeledValue;

    .line 93
    return-void
.end method

.method public final f()Z
    .locals 2

    .prologue
    .line 353
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayActivityItem;->isInvoiceRequest:Lcom/chase/sig/android/domain/LabeledValue;

    if-eqz v0, :cond_0

    .line 354
    const-string v0, "true"

    iget-object v1, p0, Lcom/chase/sig/android/domain/QuickPayActivityItem;->isInvoiceRequest:Lcom/chase/sig/android/domain/LabeledValue;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    .line 357
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 231
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayActivityItem;->id:Lcom/chase/sig/android/domain/LabeledValue;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final g(Lcom/chase/sig/android/domain/LabeledValue;)V
    .locals 0
    .parameter

    .prologue
    .line 100
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickPayActivityItem;->decline:Lcom/chase/sig/android/domain/LabeledValue;

    .line 101
    return-void
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayActivityItem;->status:Lcom/chase/sig/android/domain/LabeledValue;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final h(Lcom/chase/sig/android/domain/LabeledValue;)V
    .locals 0
    .parameter

    .prologue
    .line 112
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickPayActivityItem;->accountName:Lcom/chase/sig/android/domain/LabeledValue;

    .line 113
    return-void
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 310
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayActivityItem;->sentOn:Lcom/chase/sig/android/domain/LabeledValue;

    if-nez v0, :cond_0

    .line 311
    const/4 v0, 0x0

    .line 313
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayActivityItem;->sentOn:Lcom/chase/sig/android/domain/LabeledValue;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final i(Lcom/chase/sig/android/domain/LabeledValue;)V
    .locals 0
    .parameter

    .prologue
    .line 120
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickPayActivityItem;->memo:Lcom/chase/sig/android/domain/LabeledValue;

    .line 121
    return-void
.end method

.method public final j()Lcom/chase/sig/android/domain/QuickPayRecipient;
    .locals 1

    .prologue
    .line 322
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayActivityItem;->recipient:Lcom/chase/sig/android/domain/QuickPayRecipient;

    return-object v0
.end method

.method public final j(Lcom/chase/sig/android/domain/LabeledValue;)V
    .locals 0
    .parameter

    .prologue
    .line 128
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickPayActivityItem;->receivedOn:Lcom/chase/sig/android/domain/LabeledValue;

    .line 129
    return-void
.end method

.method public final k(Lcom/chase/sig/android/domain/LabeledValue;)V
    .locals 0
    .parameter

    .prologue
    .line 136
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickPayActivityItem;->acceptedOn:Lcom/chase/sig/android/domain/LabeledValue;

    .line 137
    return-void
.end method

.method public final k()Z
    .locals 2

    .prologue
    .line 190
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayActivityItem;->atmEligible:Lcom/chase/sig/android/domain/LabeledValue;

    if-nez v0, :cond_1

    .line 191
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayActivityItem;->senderCode:Lcom/chase/sig/android/domain/LabeledValue;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayActivityItem;->senderCode:Lcom/chase/sig/android/domain/LabeledValue;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    .line 193
    :goto_0
    return v0

    .line 191
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 193
    :cond_1
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayActivityItem;->atmEligible:Lcom/chase/sig/android/domain/LabeledValue;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "true"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 327
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayActivityItem;->senderCode:Lcom/chase/sig/android/domain/LabeledValue;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final l(Lcom/chase/sig/android/domain/LabeledValue;)V
    .locals 0
    .parameter

    .prologue
    .line 144
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickPayActivityItem;->token:Lcom/chase/sig/android/domain/LabeledValue;

    .line 145
    return-void
.end method

.method public final m()Lcom/chase/sig/android/domain/LabeledValue;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayActivityItem;->date:Lcom/chase/sig/android/domain/LabeledValue;

    return-object v0
.end method

.method public final m(Lcom/chase/sig/android/domain/LabeledValue;)V
    .locals 0
    .parameter

    .prologue
    .line 152
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickPayActivityItem;->dueDate:Lcom/chase/sig/android/domain/LabeledValue;

    .line 153
    return-void
.end method

.method public final n()Lcom/chase/sig/android/domain/LabeledValue;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayActivityItem;->amount:Lcom/chase/sig/android/domain/LabeledValue;

    return-object v0
.end method

.method public final n(Lcom/chase/sig/android/domain/LabeledValue;)V
    .locals 0
    .parameter

    .prologue
    .line 160
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickPayActivityItem;->invoice:Lcom/chase/sig/android/domain/LabeledValue;

    .line 161
    return-void
.end method

.method public final o()Lcom/chase/sig/android/domain/LabeledValue;
    .locals 3

    .prologue
    .line 60
    new-instance v0, Lcom/chase/sig/android/domain/LabeledValue;

    const-string v1, ""

    iget-object v2, p0, Lcom/chase/sig/android/domain/QuickPayActivityItem;->recipient:Lcom/chase/sig/android/domain/QuickPayRecipient;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/QuickPayRecipient;->a()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/chase/sig/android/domain/LabeledValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public final o(Lcom/chase/sig/android/domain/LabeledValue;)V
    .locals 0
    .parameter

    .prologue
    .line 177
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickPayActivityItem;->sentOn:Lcom/chase/sig/android/domain/LabeledValue;

    .line 178
    return-void
.end method

.method public final p()Lcom/chase/sig/android/domain/LabeledValue;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayActivityItem;->status:Lcom/chase/sig/android/domain/LabeledValue;

    return-object v0
.end method

.method public final p(Lcom/chase/sig/android/domain/LabeledValue;)V
    .locals 0
    .parameter

    .prologue
    .line 185
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickPayActivityItem;->atmEligible:Lcom/chase/sig/android/domain/LabeledValue;

    .line 186
    return-void
.end method

.method public final q()Lcom/chase/sig/android/domain/LabeledValue;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayActivityItem;->id:Lcom/chase/sig/android/domain/LabeledValue;

    return-object v0
.end method

.method public final q(Lcom/chase/sig/android/domain/LabeledValue;)V
    .locals 0
    .parameter

    .prologue
    .line 197
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickPayActivityItem;->senderCode:Lcom/chase/sig/android/domain/LabeledValue;

    .line 198
    return-void
.end method

.method public final r()Lcom/chase/sig/android/domain/LabeledValue;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayActivityItem;->type:Lcom/chase/sig/android/domain/LabeledValue;

    return-object v0
.end method

.method public final r(Lcom/chase/sig/android/domain/LabeledValue;)V
    .locals 0
    .parameter

    .prologue
    .line 252
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickPayActivityItem;->frequency:Lcom/chase/sig/android/domain/LabeledValue;

    .line 253
    return-void
.end method

.method public final s()Lcom/chase/sig/android/domain/LabeledValue;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayActivityItem;->receivedOn:Lcom/chase/sig/android/domain/LabeledValue;

    return-object v0
.end method

.method public final s(Lcom/chase/sig/android/domain/LabeledValue;)V
    .locals 0
    .parameter

    .prologue
    .line 260
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickPayActivityItem;->numberofremainingpayments:Lcom/chase/sig/android/domain/LabeledValue;

    .line 261
    return-void
.end method

.method public final t()Lcom/chase/sig/android/domain/LabeledValue;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayActivityItem;->acceptedOn:Lcom/chase/sig/android/domain/LabeledValue;

    return-object v0
.end method

.method public final t(Lcom/chase/sig/android/domain/LabeledValue;)V
    .locals 0
    .parameter

    .prologue
    .line 268
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickPayActivityItem;->nextsendondate:Lcom/chase/sig/android/domain/LabeledValue;

    .line 269
    return-void
.end method

.method public final u()Lcom/chase/sig/android/domain/LabeledValue;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayActivityItem;->token:Lcom/chase/sig/android/domain/LabeledValue;

    return-object v0
.end method

.method public final u(Lcom/chase/sig/android/domain/LabeledValue;)V
    .locals 0
    .parameter

    .prologue
    .line 276
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickPayActivityItem;->nextdeliverbydate:Lcom/chase/sig/android/domain/LabeledValue;

    .line 277
    return-void
.end method

.method public final v()Lcom/chase/sig/android/domain/LabeledValue;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayActivityItem;->dueDate:Lcom/chase/sig/android/domain/LabeledValue;

    return-object v0
.end method

.method public final v(Lcom/chase/sig/android/domain/LabeledValue;)V
    .locals 0
    .parameter

    .prologue
    .line 292
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickPayActivityItem;->recipientCode:Lcom/chase/sig/android/domain/LabeledValue;

    .line 293
    return-void
.end method

.method public final w()Lcom/chase/sig/android/domain/LabeledValue;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayActivityItem;->invoice:Lcom/chase/sig/android/domain/LabeledValue;

    return-object v0
.end method

.method public final w(Lcom/chase/sig/android/domain/LabeledValue;)V
    .locals 0
    .parameter

    .prologue
    .line 361
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickPayActivityItem;->isInvoiceRequest:Lcom/chase/sig/android/domain/LabeledValue;

    .line 362
    return-void
.end method

.method public final x(Lcom/chase/sig/android/domain/LabeledValue;)V
    .locals 0
    .parameter

    .prologue
    .line 373
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickPayActivityItem;->declineReason:Lcom/chase/sig/android/domain/LabeledValue;

    .line 374
    return-void
.end method

.method public final x()Z
    .locals 2

    .prologue
    .line 170
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayActivityItem;->receivedOn:Lcom/chase/sig/android/domain/LabeledValue;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayActivityItem;->receivedOn:Lcom/chase/sig/android/domain/LabeledValue;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayActivityItem;->acceptedOn:Lcom/chase/sig/android/domain/LabeledValue;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayActivityItem;->acceptedOn:Lcom/chase/sig/android/domain/LabeledValue;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final y()Lcom/chase/sig/android/domain/LabeledValue;
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayActivityItem;->sentOn:Lcom/chase/sig/android/domain/LabeledValue;

    return-object v0
.end method

.method public final z()Lcom/chase/sig/android/domain/LabeledValue;
    .locals 3

    .prologue
    .line 240
    new-instance v0, Lcom/chase/sig/android/domain/LabeledValue;

    const-string v1, ""

    iget-object v2, p0, Lcom/chase/sig/android/domain/QuickPayActivityItem;->recipient:Lcom/chase/sig/android/domain/QuickPayRecipient;

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/QuickPayRecipient;->b()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/chase/sig/android/domain/LabeledValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method
