.class public final enum Lcom/chase/sig/android/domain/QuickPayActivityType;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/chase/sig/android/domain/QuickPayActivityType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/chase/sig/android/domain/QuickPayActivityType;

.field public static final enum b:Lcom/chase/sig/android/domain/QuickPayActivityType;

.field public static final enum c:Lcom/chase/sig/android/domain/QuickPayActivityType;

.field public static final enum d:Lcom/chase/sig/android/domain/QuickPayActivityType;

.field public static final enum e:Lcom/chase/sig/android/domain/QuickPayActivityType;

.field private static final synthetic f:[Lcom/chase/sig/android/domain/QuickPayActivityType;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 4
    new-instance v0, Lcom/chase/sig/android/domain/QuickPayActivityType;

    const-string v1, "MoneyReceived"

    invoke-direct {v0, v1, v2}, Lcom/chase/sig/android/domain/QuickPayActivityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/chase/sig/android/domain/QuickPayActivityType;->a:Lcom/chase/sig/android/domain/QuickPayActivityType;

    new-instance v0, Lcom/chase/sig/android/domain/QuickPayActivityType;

    const-string v1, "MoneySent"

    invoke-direct {v0, v1, v3}, Lcom/chase/sig/android/domain/QuickPayActivityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/chase/sig/android/domain/QuickPayActivityType;->b:Lcom/chase/sig/android/domain/QuickPayActivityType;

    new-instance v0, Lcom/chase/sig/android/domain/QuickPayActivityType;

    const-string v1, "RequestSent"

    invoke-direct {v0, v1, v4}, Lcom/chase/sig/android/domain/QuickPayActivityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/chase/sig/android/domain/QuickPayActivityType;->c:Lcom/chase/sig/android/domain/QuickPayActivityType;

    new-instance v0, Lcom/chase/sig/android/domain/QuickPayActivityType;

    const-string v1, "RequestReceived"

    invoke-direct {v0, v1, v5}, Lcom/chase/sig/android/domain/QuickPayActivityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/chase/sig/android/domain/QuickPayActivityType;->d:Lcom/chase/sig/android/domain/QuickPayActivityType;

    new-instance v0, Lcom/chase/sig/android/domain/QuickPayActivityType;

    const-string v1, "RequestForMoney"

    invoke-direct {v0, v1, v6}, Lcom/chase/sig/android/domain/QuickPayActivityType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/chase/sig/android/domain/QuickPayActivityType;->e:Lcom/chase/sig/android/domain/QuickPayActivityType;

    .line 3
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/chase/sig/android/domain/QuickPayActivityType;

    sget-object v1, Lcom/chase/sig/android/domain/QuickPayActivityType;->a:Lcom/chase/sig/android/domain/QuickPayActivityType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/chase/sig/android/domain/QuickPayActivityType;->b:Lcom/chase/sig/android/domain/QuickPayActivityType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/chase/sig/android/domain/QuickPayActivityType;->c:Lcom/chase/sig/android/domain/QuickPayActivityType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/chase/sig/android/domain/QuickPayActivityType;->d:Lcom/chase/sig/android/domain/QuickPayActivityType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/chase/sig/android/domain/QuickPayActivityType;->e:Lcom/chase/sig/android/domain/QuickPayActivityType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/chase/sig/android/domain/QuickPayActivityType;->f:[Lcom/chase/sig/android/domain/QuickPayActivityType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/chase/sig/android/domain/QuickPayActivityType;
    .locals 1
    .parameter

    .prologue
    .line 3
    const-class v0, Lcom/chase/sig/android/domain/QuickPayActivityType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/QuickPayActivityType;

    return-object v0
.end method

.method public static values()[Lcom/chase/sig/android/domain/QuickPayActivityType;
    .locals 1

    .prologue
    .line 3
    sget-object v0, Lcom/chase/sig/android/domain/QuickPayActivityType;->f:[Lcom/chase/sig/android/domain/QuickPayActivityType;

    invoke-virtual {v0}, [Lcom/chase/sig/android/domain/QuickPayActivityType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/chase/sig/android/domain/QuickPayActivityType;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/chase/sig/android/domain/QuickPayActivityItem;)Z
    .locals 2
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 7
    if-nez p1, :cond_1

    .line 14
    :cond_0
    :goto_0
    return v0

    .line 10
    :cond_1
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->r()Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 14
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->r()Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/domain/QuickPayActivityType;->a(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1
    .parameter

    .prologue
    .line 18
    invoke-static {p1}, Lcom/chase/sig/android/domain/QuickPayActivityType;->valueOf(Ljava/lang/String;)Lcom/chase/sig/android/domain/QuickPayActivityType;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/domain/QuickPayActivityType;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
