.class public Lcom/chase/sig/android/domain/QuickPayDeclineTransaction;
.super Lcom/chase/sig/android/domain/QuickPayTransaction;
.source "SourceFile"


# instance fields
.field private declineReason:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0}, Lcom/chase/sig/android/domain/QuickPayTransaction;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 9
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickPayDeclineTransaction;->declineReason:Ljava/lang/String;

    .line 10
    return-void
.end method

.method public final m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayDeclineTransaction;->declineReason:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
