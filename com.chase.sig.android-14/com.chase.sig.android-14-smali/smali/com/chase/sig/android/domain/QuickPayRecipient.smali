.class public Lcom/chase/sig/android/domain/QuickPayRecipient;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private email:Ljava/lang/String;

.field private id:Ljava/lang/String;

.field private mobileNumber:Ljava/lang/String;

.field private mobilePhoneNumberObj:Lcom/chase/sig/android/domain/MobilePhoneNumber;

.field private name:Ljava/lang/String;

.field private recipientEmails:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/Email;",
            ">;"
        }
    .end annotation
.end field

.field private recipientId:Ljava/lang/String;

.field private status:Ljava/lang/String;

.field private token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    const-string v0, "0"

    iput-object v0, p0, Lcom/chase/sig/android/domain/QuickPayRecipient;->id:Ljava/lang/String;

    .line 15
    const-string v0, "0"

    iput-object v0, p0, Lcom/chase/sig/android/domain/QuickPayRecipient;->token:Ljava/lang/String;

    .line 17
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/chase/sig/android/domain/QuickPayRecipient;->recipientEmails:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayRecipient;->name:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Lcom/chase/sig/android/domain/Email;)V
    .locals 1
    .parameter

    .prologue
    .line 73
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayRecipient;->recipientEmails:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 74
    return-void
.end method

.method public final a(Lcom/chase/sig/android/domain/MobilePhoneNumber;)V
    .locals 0
    .parameter

    .prologue
    .line 112
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickPayRecipient;->mobilePhoneNumberObj:Lcom/chase/sig/android/domain/MobilePhoneNumber;

    .line 113
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 26
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickPayRecipient;->name:Ljava/lang/String;

    .line 27
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayRecipient;->id:Ljava/lang/String;

    return-object v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 34
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickPayRecipient;->id:Ljava/lang/String;

    .line 35
    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayRecipient;->token:Ljava/lang/String;

    return-object v0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 42
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickPayRecipient;->token:Ljava/lang/String;

    .line 43
    return-void
.end method

.method public final d()Ljava/lang/String;
    .locals 3

    .prologue
    .line 54
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayRecipient;->email:Ljava/lang/String;

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->o(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayRecipient;->email:Ljava/lang/String;

    .line 65
    :goto_0
    return-object v0

    .line 57
    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayRecipient;->recipientEmails:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 58
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayRecipient;->recipientEmails:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/Email;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Email;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 60
    :cond_1
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayRecipient;->recipientEmails:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/Email;

    .line 61
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Email;->b()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 62
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Email;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 65
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 50
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickPayRecipient;->email:Ljava/lang/String;

    .line 51
    return-void
.end method

.method public final e()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/Email;",
            ">;"
        }
    .end annotation

    .prologue
    .line 69
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/chase/sig/android/domain/QuickPayRecipient;->recipientEmails:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public final e(Ljava/lang/String;)V
    .locals 3
    .parameter

    .prologue
    .line 91
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayRecipient;->mobilePhoneNumberObj:Lcom/chase/sig/android/domain/MobilePhoneNumber;

    if-nez v0, :cond_0

    .line 92
    new-instance v0, Lcom/chase/sig/android/domain/MobilePhoneNumber;

    const-string v1, ""

    const-string v2, ""

    invoke-direct {v0, p1, v1, v2}, Lcom/chase/sig/android/domain/MobilePhoneNumber;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/chase/sig/android/domain/QuickPayRecipient;->mobilePhoneNumberObj:Lcom/chase/sig/android/domain/MobilePhoneNumber;

    .line 96
    :goto_0
    return-void

    .line 95
    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayRecipient;->mobilePhoneNumberObj:Lcom/chase/sig/android/domain/MobilePhoneNumber;

    invoke-virtual {v0, p1}, Lcom/chase/sig/android/domain/MobilePhoneNumber;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayRecipient;->mobilePhoneNumberObj:Lcom/chase/sig/android/domain/MobilePhoneNumber;

    if-nez v0, :cond_0

    .line 78
    const/4 v0, 0x0

    .line 80
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayRecipient;->mobilePhoneNumberObj:Lcom/chase/sig/android/domain/MobilePhoneNumber;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/MobilePhoneNumber;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final f(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 132
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickPayRecipient;->status:Ljava/lang/String;

    .line 133
    return-void
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayRecipient;->mobilePhoneNumberObj:Lcom/chase/sig/android/domain/MobilePhoneNumber;

    if-nez v0, :cond_0

    .line 85
    const/4 v0, 0x0

    .line 87
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayRecipient;->mobilePhoneNumberObj:Lcom/chase/sig/android/domain/MobilePhoneNumber;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/MobilePhoneNumber;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final g(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 148
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickPayRecipient;->recipientId:Ljava/lang/String;

    .line 149
    return-void
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayRecipient;->recipientEmails:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 109
    return-void
.end method

.method public final i()Lcom/chase/sig/android/domain/MobilePhoneNumber;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayRecipient;->mobilePhoneNumberObj:Lcom/chase/sig/android/domain/MobilePhoneNumber;

    return-object v0
.end method

.method public final j()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/Email;",
            ">;"
        }
    .end annotation

    .prologue
    .line 124
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayRecipient;->recipientEmails:Ljava/util/List;

    return-object v0
.end method

.method public final k()I
    .locals 2

    .prologue
    .line 127
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayRecipient;->mobilePhoneNumberObj:Lcom/chase/sig/android/domain/MobilePhoneNumber;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 128
    :goto_0
    iget-object v1, p0, Lcom/chase/sig/android/domain/QuickPayRecipient;->recipientEmails:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    return v0

    .line 127
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayRecipient;->status:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Z
    .locals 2

    .prologue
    .line 140
    const-string v0, "ACTIVE"

    iget-object v1, p0, Lcom/chase/sig/android/domain/QuickPayRecipient;->status:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
