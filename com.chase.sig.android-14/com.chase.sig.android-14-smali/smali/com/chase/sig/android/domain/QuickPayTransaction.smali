.class public Lcom/chase/sig/android/domain/QuickPayTransaction;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/domain/h;
.implements Ljava/io/Serializable;


# instance fields
.field private CVV:Ljava/lang/String;

.field private accountId:Ljava/lang/String;

.field private accountName:Ljava/lang/String;

.field private accountTypeIndicator:Ljava/lang/String;

.field private amount:Ljava/lang/String;

.field private atmEligible:Z

.field private canCancel:Z

.field private canEdit:Z

.field private deliverByDate:Ljava/util/Date;

.field private displayConfirmation:Z

.field private dueDate:Ljava/util/Date;

.field private formId:Ljava/lang/String;

.field private frequency:Ljava/lang/String;

.field private frequencyLabel:Ljava/lang/String;

.field private invoiceId:Ljava/lang/String;

.field private isEditing:Z

.field private isInvoiceRequest:Z

.field private isRequest:Z

.field private isResponseToARequest:Z

.field private isSubmitted:Z

.field private memo:Ljava/lang/String;

.field private nextPaymentDate:Ljava/util/Date;

.field private nextSendOnDate:Ljava/util/Date;

.field private numberOfRemainingPayments:I

.field private payFromAccountName:Ljava/lang/String;

.field private recipient:Lcom/chase/sig/android/domain/QuickPayRecipient;

.field private repeating:Z

.field private repeatingId:Ljava/lang/String;

.field private repeatingToken:Ljava/lang/String;

.field private requestTransactionId:Ljava/lang/String;

.field private sendOnDate:Ljava/util/Date;

.field private senderCode:Ljava/lang/String;

.field private sentOn:Ljava/lang/String;

.field private smsEligible:Z

.field private smsReason:Ljava/lang/String;

.field private status:Ljava/lang/String;

.field private token:Ljava/lang/String;

.field private transactionId:Ljava/lang/String;

.field private type:Ljava/lang/String;

.field private unlimited:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-boolean v0, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->isSubmitted:Z

    .line 33
    iput-boolean v0, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->repeating:Z

    .line 34
    iput v0, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->numberOfRemainingPayments:I

    .line 35
    iput-boolean v0, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->unlimited:Z

    .line 47
    iput-boolean v1, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->canEdit:Z

    .line 48
    iput-boolean v1, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->canCancel:Z

    .line 49
    iput-boolean v0, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->isResponseToARequest:Z

    .line 50
    iput-boolean v0, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->isInvoiceRequest:Z

    return-void
.end method


# virtual methods
.method public final A()Ljava/lang/String;
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->invoiceId:Ljava/lang/String;

    return-object v0
.end method

.method public final B()Z
    .locals 1

    .prologue
    .line 213
    iget-boolean v0, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->isRequest:Z

    return v0
.end method

.method public final C()Z
    .locals 1

    .prologue
    .line 221
    iget-boolean v0, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->unlimited:Z

    return v0
.end method

.method public final D()Z
    .locals 1

    .prologue
    .line 230
    iget-boolean v0, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->repeating:Z

    return v0
.end method

.method public final E()Ljava/util/Date;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->dueDate:Ljava/util/Date;

    return-object v0
.end method

.method public final F()Ljava/util/Date;
    .locals 1

    .prologue
    .line 242
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->nextPaymentDate:Ljava/util/Date;

    return-object v0
.end method

.method public final G()Ljava/lang/String;
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->payFromAccountName:Ljava/lang/String;

    return-object v0
.end method

.method public final H()I
    .locals 1

    .prologue
    .line 270
    iget v0, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->numberOfRemainingPayments:I

    return v0
.end method

.method public final I()Ljava/lang/String;
    .locals 1

    .prologue
    .line 290
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->type:Ljava/lang/String;

    return-object v0
.end method

.method public final J()Ljava/lang/String;
    .locals 1

    .prologue
    .line 298
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->token:Ljava/lang/String;

    return-object v0
.end method

.method public final K()Ljava/lang/String;
    .locals 1

    .prologue
    .line 302
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->repeatingToken:Ljava/lang/String;

    return-object v0
.end method

.method public final L()Ljava/lang/String;
    .locals 1

    .prologue
    .line 310
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->repeatingId:Ljava/lang/String;

    return-object v0
.end method

.method public final M()V
    .locals 1

    .prologue
    .line 331
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->displayConfirmation:Z

    .line 332
    return-void
.end method

.method public final N()Z
    .locals 1

    .prologue
    .line 335
    iget-boolean v0, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->displayConfirmation:Z

    return v0
.end method

.method public final O()Ljava/lang/String;
    .locals 1

    .prologue
    .line 348
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->formId:Ljava/lang/String;

    return-object v0
.end method

.method public final P()Z
    .locals 1

    .prologue
    .line 360
    iget-boolean v0, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->canEdit:Z

    return v0
.end method

.method public final Q()Z
    .locals 1

    .prologue
    .line 372
    iget-boolean v0, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->canCancel:Z

    return v0
.end method

.method public final R()Z
    .locals 1

    .prologue
    .line 381
    iget-boolean v0, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->isResponseToARequest:Z

    return v0
.end method

.method public final S()Z
    .locals 1

    .prologue
    .line 390
    iget-boolean v0, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->isInvoiceRequest:Z

    return v0
.end method

.method public final T()Ljava/lang/String;
    .locals 1

    .prologue
    .line 403
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->smsReason:Ljava/lang/String;

    return-object v0
.end method

.method public final U()Z
    .locals 1

    .prologue
    .line 412
    iget-boolean v0, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->smsEligible:Z

    return v0
.end method

.method public final V()Z
    .locals 1

    .prologue
    .line 420
    iget-boolean v0, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->isEditing:Z

    return v0
.end method

.method public final W()Ljava/lang/String;
    .locals 1

    .prologue
    .line 428
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->accountTypeIndicator:Ljava/lang/String;

    return-object v0
.end method

.method public final X()Ljava/lang/String;
    .locals 1

    .prologue
    .line 444
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->frequency:Ljava/lang/String;

    return-object v0
.end method

.method public final Y()Ljava/lang/String;
    .locals 1

    .prologue
    .line 448
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->frequencyLabel:Ljava/lang/String;

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->recipient:Lcom/chase/sig/android/domain/QuickPayRecipient;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayRecipient;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)V
    .locals 0
    .parameter

    .prologue
    .line 274
    iput p1, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->numberOfRemainingPayments:I

    .line 275
    return-void
.end method

.method public final a(Lcom/chase/sig/android/domain/QuickPayRecipient;)V
    .locals 0
    .parameter

    .prologue
    .line 65
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->recipient:Lcom/chase/sig/android/domain/QuickPayRecipient;

    .line 66
    return-void
.end method

.method public final a(Ljava/util/Date;)V
    .locals 0
    .parameter

    .prologue
    .line 246
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->nextPaymentDate:Ljava/util/Date;

    .line 247
    return-void
.end method

.method public final a(Z)V
    .locals 0
    .parameter

    .prologue
    .line 159
    iput-boolean p1, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->atmEligible:Z

    .line 160
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->recipient:Lcom/chase/sig/android/domain/QuickPayRecipient;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayRecipient;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 77
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->accountId:Ljava/lang/String;

    .line 78
    return-void
.end method

.method public final b(Ljava/util/Date;)V
    .locals 0
    .parameter

    .prologue
    .line 258
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->sendOnDate:Ljava/util/Date;

    .line 259
    return-void
.end method

.method public final b(Z)V
    .locals 0
    .parameter

    .prologue
    .line 209
    iput-boolean p1, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->isRequest:Z

    .line 210
    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->accountName:Ljava/lang/String;

    return-object v0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 85
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->accountName:Ljava/lang/String;

    .line 86
    return-void
.end method

.method public final c(Ljava/util/Date;)V
    .locals 0
    .parameter

    .prologue
    .line 282
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->nextSendOnDate:Ljava/util/Date;

    .line 283
    return-void
.end method

.method public final c(Z)V
    .locals 0
    .parameter

    .prologue
    .line 217
    iput-boolean p1, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->unlimited:Z

    .line 218
    return-void
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->amount:Ljava/lang/String;

    return-object v0
.end method

.method public final d(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 98
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->amount:Ljava/lang/String;

    .line 99
    return-void
.end method

.method public final d(Z)V
    .locals 0
    .parameter

    .prologue
    .line 225
    iput-boolean p1, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->repeating:Z

    .line 226
    return-void
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->memo:Ljava/lang/String;

    return-object v0
.end method

.method public final e(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 110
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->memo:Ljava/lang/String;

    .line 111
    return-void
.end method

.method public final e(Z)V
    .locals 0
    .parameter

    .prologue
    .line 385
    iput-boolean p1, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->isResponseToARequest:Z

    .line 386
    return-void
.end method

.method public final f(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 126
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->status:Ljava/lang/String;

    .line 127
    return-void
.end method

.method public final f(Z)V
    .locals 0
    .parameter

    .prologue
    .line 394
    iput-boolean p1, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->isInvoiceRequest:Z

    .line 395
    return-void
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 377
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->invoiceId:Ljava/lang/String;

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->o(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->transactionId:Ljava/lang/String;

    return-object v0
.end method

.method public final g(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 134
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->transactionId:Ljava/lang/String;

    .line 135
    return-void
.end method

.method public final g(Z)V
    .locals 0
    .parameter

    .prologue
    .line 407
    iput-boolean p1, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->smsEligible:Z

    .line 408
    return-void
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->status:Ljava/lang/String;

    return-object v0
.end method

.method public final h(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 142
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->senderCode:Ljava/lang/String;

    .line 143
    return-void
.end method

.method public final h(Z)V
    .locals 0
    .parameter

    .prologue
    .line 416
    iput-boolean p1, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->isEditing:Z

    .line 417
    return-void
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 327
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->sentOn:Ljava/lang/String;

    return-object v0
.end method

.method public final i(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 150
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->CVV:Ljava/lang/String;

    .line 151
    return-void
.end method

.method public final j()Lcom/chase/sig/android/domain/QuickPayRecipient;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->recipient:Lcom/chase/sig/android/domain/QuickPayRecipient;

    return-object v0
.end method

.method public final j(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 191
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->invoiceId:Ljava/lang/String;

    .line 192
    return-void
.end method

.method public final k(Ljava/lang/String;)V
    .locals 1
    .parameter

    .prologue
    .line 238
    invoke-static {p1}, Lcom/chase/sig/android/util/s;->t(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->dueDate:Ljava/util/Date;

    .line 239
    return-void
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 155
    iget-boolean v0, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->atmEligible:Z

    return v0
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 340
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->senderCode:Ljava/lang/String;

    return-object v0
.end method

.method public final l(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 286
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->type:Ljava/lang/String;

    .line 287
    return-void
.end method

.method public final m(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 294
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->token:Ljava/lang/String;

    .line 295
    return-void
.end method

.method public final n(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 306
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->repeatingToken:Ljava/lang/String;

    .line 307
    return-void
.end method

.method public final o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->recipient:Lcom/chase/sig/android/domain/QuickPayRecipient;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayRecipient;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final o(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 314
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->repeatingId:Ljava/lang/String;

    .line 315
    return-void
.end method

.method public final p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->accountId:Ljava/lang/String;

    return-object v0
.end method

.method public final p(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 322
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->sentOn:Ljava/lang/String;

    .line 323
    return-void
.end method

.method public final q()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->accountName:Ljava/lang/String;

    return-object v0
.end method

.method public final q(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 344
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->formId:Ljava/lang/String;

    .line 345
    return-void
.end method

.method public final r()Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->amount:Ljava/lang/String;

    return-object v0
.end method

.method public final r(Ljava/lang/String;)V
    .locals 1
    .parameter

    .prologue
    .line 352
    const-string v0, "true"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 353
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->canEdit:Z

    .line 357
    :goto_0
    return-void

    .line 355
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->canEdit:Z

    goto :goto_0
.end method

.method public final s()Ljava/util/Date;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->sendOnDate:Ljava/util/Date;

    return-object v0
.end method

.method public final s(Ljava/lang/String;)V
    .locals 1
    .parameter

    .prologue
    .line 364
    const-string v0, "true"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 365
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->canCancel:Z

    .line 369
    :goto_0
    return-void

    .line 367
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->canCancel:Z

    goto :goto_0
.end method

.method public final t()Ljava/lang/String;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->memo:Ljava/lang/String;

    return-object v0
.end method

.method public final t(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 398
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->smsReason:Ljava/lang/String;

    .line 399
    return-void
.end method

.method public final u()Ljava/lang/String;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->recipient:Lcom/chase/sig/android/domain/QuickPayRecipient;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayRecipient;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final u(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 424
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->accountTypeIndicator:Ljava/lang/String;

    .line 425
    return-void
.end method

.method public final v()Ljava/lang/String;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->recipient:Lcom/chase/sig/android/domain/QuickPayRecipient;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayRecipient;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final v(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 440
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->frequency:Ljava/lang/String;

    .line 441
    return-void
.end method

.method public final w()Ljava/lang/String;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->transactionId:Ljava/lang/String;

    return-object v0
.end method

.method public final w(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 452
    iput-object p1, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->frequencyLabel:Ljava/lang/String;

    .line 453
    return-void
.end method

.method public final x()Ljava/lang/String;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->CVV:Ljava/lang/String;

    return-object v0
.end method

.method public final y()Z
    .locals 1

    .prologue
    .line 163
    iget-boolean v0, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->isSubmitted:Z

    return v0
.end method

.method public final z()V
    .locals 1

    .prologue
    .line 167
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/chase/sig/android/domain/QuickPayTransaction;->isSubmitted:Z

    .line 168
    return-void
.end method
