.class public Lcom/chase/sig/android/domain/Quote;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private averageVolume:Ljava/lang/Double;

.field private change:Lcom/chase/sig/android/util/Dollar;

.field private changeDirection:Ljava/lang/String;

.field private changePercent:Ljava/lang/String;

.field private companyName:Ljava/lang/String;

.field private cusip:Ljava/lang/String;

.field private exchange:Ljava/lang/String;

.field private gainLossToday:Lcom/chase/sig/android/util/Dollar;

.field private high:Lcom/chase/sig/android/util/Dollar;

.field private last:Lcom/chase/sig/android/util/Dollar;
    .annotation runtime Lcom/google/gson/chase/annotations/SerializedName;
        a = "recent"
    .end annotation
.end field

.field private lastTime:Ljava/lang/String;

.field private low:Lcom/chase/sig/android/util/Dollar;

.field private oneDayChart:Lcom/chase/sig/android/domain/Chart;
    .annotation runtime Lcom/google/gson/chase/annotations/SerializedName;
        a = "chart1"
    .end annotation
.end field

.field private oneYearChart:Lcom/chase/sig/android/domain/Chart;
    .annotation runtime Lcom/google/gson/chase/annotations/SerializedName;
        a = "chart2"
    .end annotation
.end field

.field private open:Lcom/chase/sig/android/util/Dollar;

.field private tickerSymbol:Ljava/lang/String;

.field private type:Ljava/lang/String;

.field private volume:Ljava/lang/Double;

.field private week52High:Lcom/chase/sig/android/util/Dollar;

.field private week52Low:Lcom/chase/sig/android/util/Dollar;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Double;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/chase/sig/android/domain/Quote;->averageVolume:Ljava/lang/Double;

    return-object v0
.end method

.method public final b()Lcom/chase/sig/android/util/Dollar;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/chase/sig/android/domain/Quote;->change:Lcom/chase/sig/android/util/Dollar;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/chase/sig/android/domain/Quote;->changeDirection:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/chase/sig/android/domain/Quote;->changePercent:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/chase/sig/android/domain/Quote;->companyName:Ljava/lang/String;

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/chase/sig/android/domain/Quote;->exchange:Ljava/lang/String;

    return-object v0
.end method

.method public final g()Lcom/chase/sig/android/util/Dollar;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/chase/sig/android/domain/Quote;->gainLossToday:Lcom/chase/sig/android/util/Dollar;

    return-object v0
.end method

.method public final h()Lcom/chase/sig/android/util/Dollar;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/chase/sig/android/domain/Quote;->high:Lcom/chase/sig/android/util/Dollar;

    return-object v0
.end method

.method public final i()Lcom/chase/sig/android/util/Dollar;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/chase/sig/android/domain/Quote;->last:Lcom/chase/sig/android/util/Dollar;

    return-object v0
.end method

.method public final j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/chase/sig/android/domain/Quote;->lastTime:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Lcom/chase/sig/android/util/Dollar;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/chase/sig/android/domain/Quote;->low:Lcom/chase/sig/android/util/Dollar;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/chase/sig/android/domain/Quote;->tickerSymbol:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Ljava/lang/Double;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/chase/sig/android/domain/Quote;->volume:Ljava/lang/Double;

    return-object v0
.end method

.method public final n()Lcom/chase/sig/android/util/Dollar;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/chase/sig/android/domain/Quote;->week52High:Lcom/chase/sig/android/util/Dollar;

    return-object v0
.end method

.method public final o()Lcom/chase/sig/android/util/Dollar;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/chase/sig/android/domain/Quote;->week52Low:Lcom/chase/sig/android/util/Dollar;

    return-object v0
.end method

.method public final p()Ljava/lang/String;
    .locals 2

    .prologue
    .line 170
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/chase/sig/android/domain/Quote;->companyName:Ljava/lang/String;

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/chase/sig/android/domain/Quote;->tickerSymbol:Ljava/lang/String;

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final q()Lcom/chase/sig/android/domain/Chart;
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lcom/chase/sig/android/domain/Quote;->oneDayChart:Lcom/chase/sig/android/domain/Chart;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/domain/Quote;->oneDayChart:Lcom/chase/sig/android/domain/Chart;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/chase/sig/android/domain/Chart;->a:Lcom/chase/sig/android/domain/Chart;

    goto :goto_0
.end method

.method public final r()Lcom/chase/sig/android/domain/Chart;
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/chase/sig/android/domain/Quote;->oneYearChart:Lcom/chase/sig/android/domain/Chart;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/domain/Quote;->oneYearChart:Lcom/chase/sig/android/domain/Chart;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/chase/sig/android/domain/Chart;->a:Lcom/chase/sig/android/domain/Chart;

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 176
    invoke-virtual {p0}, Lcom/chase/sig/android/domain/Quote;->p()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
