.class public Lcom/chase/sig/android/domain/QuoteNewsArticle;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private headline:Ljava/lang/String;

.field private headlineId:Ljava/lang/String;

.field private newsContent:Ljava/lang/String;

.field private newsDate:Ljava/lang/String;

.field private source:Ljava/lang/String;

.field private tickerSymbol:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuoteNewsArticle;->tickerSymbol:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuoteNewsArticle;->source:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuoteNewsArticle;->headline:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuoteNewsArticle;->newsDate:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuoteNewsArticle;->newsContent:Ljava/lang/String;

    return-object v0
.end method
