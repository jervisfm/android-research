.class public Lcom/chase/sig/android/domain/QuoteResponse;
.super Lcom/chase/sig/android/service/l;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private quotes:Ljava/util/ArrayList;
    .annotation runtime Lcom/google/gson/chase/annotations/SerializedName;
        a = "stocks"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/chase/sig/android/domain/Quote;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/chase/sig/android/service/l;-><init>()V

    .line 14
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/chase/sig/android/domain/QuoteResponse;->quotes:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/chase/sig/android/domain/Quote;
    .locals 3
    .parameter

    .prologue
    .line 38
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuoteResponse;->quotes:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 41
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 42
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/Quote;

    .line 43
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Quote;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 48
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/chase/sig/android/domain/Quote;",
            ">;"
        }
    .end annotation

    .prologue
    .line 22
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuoteResponse;->quotes:Ljava/util/ArrayList;

    return-object v0
.end method

.method public final a(Lcom/chase/sig/android/domain/Quote;)V
    .locals 2
    .parameter

    .prologue
    .line 30
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/Quote;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/domain/QuoteResponse;->a(Ljava/lang/String;)Lcom/chase/sig/android/domain/Quote;

    move-result-object v0

    .line 31
    if-eqz v0, :cond_0

    .line 32
    iget-object v1, p0, Lcom/chase/sig/android/domain/QuoteResponse;->quotes:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 34
    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuoteResponse;->quotes:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 35
    return-void
.end method

.method public final b()Lcom/chase/sig/android/domain/Quote;
    .locals 2

    .prologue
    .line 52
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuoteResponse;->quotes:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 53
    const/4 v0, 0x0

    .line 56
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/domain/QuoteResponse;->quotes:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/Quote;

    goto :goto_0
.end method
