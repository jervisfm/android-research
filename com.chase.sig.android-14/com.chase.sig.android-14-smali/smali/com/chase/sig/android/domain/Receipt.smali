.class public Lcom/chase/sig/android/domain/Receipt;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private accountId:Ljava/lang/String;

.field private amount:Lcom/chase/sig/android/util/Dollar;

.field private category:Lcom/chase/sig/android/domain/LabeledValue;

.field private collectionId:Ljava/lang/String;

.field private description:Ljava/lang/String;

.field private expenseType:Lcom/chase/sig/android/domain/LabeledValue;

.field private id:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/chase/annotations/SerializedName;
        a = "quickReceiptId"
    .end annotation
.end field

.field private maskedName:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/chase/annotations/SerializedName;
        a = "accountMask"
    .end annotation
.end field

.field private photoIds:[Ljava/lang/String;
    .annotation runtime Lcom/google/gson/chase/annotations/SerializedName;
        a = "quickReceiptIds"
    .end annotation
.end field

.field private profileId:Ljava/lang/String;

.field private receiptPhotoList:Lcom/chase/sig/android/domain/ReceiptPhotoList;

.field private status:Ljava/lang/String;

.field private taxDeductible:Lcom/chase/sig/android/domain/LabeledValue;
    .annotation runtime Lcom/google/gson/chase/annotations/SerializedName;
        a = "taxDeductable"
    .end annotation
.end field

.field private transactionDate:Ljava/lang/String;

.field private transactionId:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/chase/annotations/SerializedName;
        a = "transactionKey"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/chase/sig/android/domain/Receipt;->accountId:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Lcom/chase/sig/android/domain/LabeledValue;)V
    .locals 0
    .parameter

    .prologue
    .line 146
    iput-object p1, p0, Lcom/chase/sig/android/domain/Receipt;->category:Lcom/chase/sig/android/domain/LabeledValue;

    .line 147
    return-void
.end method

.method public final a(Lcom/chase/sig/android/domain/ReceiptPhotoList;)V
    .locals 0
    .parameter

    .prologue
    .line 122
    iput-object p1, p0, Lcom/chase/sig/android/domain/Receipt;->receiptPhotoList:Lcom/chase/sig/android/domain/ReceiptPhotoList;

    .line 123
    return-void
.end method

.method public final a(Lcom/chase/sig/android/util/Dollar;)V
    .locals 0
    .parameter

    .prologue
    .line 62
    iput-object p1, p0, Lcom/chase/sig/android/domain/Receipt;->amount:Lcom/chase/sig/android/util/Dollar;

    .line 63
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 46
    iput-object p1, p0, Lcom/chase/sig/android/domain/Receipt;->accountId:Ljava/lang/String;

    .line 47
    return-void
.end method

.method public final b()Lcom/chase/sig/android/util/Dollar;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/chase/sig/android/domain/Receipt;->amount:Lcom/chase/sig/android/util/Dollar;

    return-object v0
.end method

.method public final b(Lcom/chase/sig/android/domain/LabeledValue;)V
    .locals 0
    .parameter

    .prologue
    .line 154
    iput-object p1, p0, Lcom/chase/sig/android/domain/Receipt;->expenseType:Lcom/chase/sig/android/domain/LabeledValue;

    .line 155
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 70
    iput-object p1, p0, Lcom/chase/sig/android/domain/Receipt;->description:Ljava/lang/String;

    .line 71
    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/chase/sig/android/domain/Receipt;->description:Ljava/lang/String;

    return-object v0
.end method

.method public final c(Lcom/chase/sig/android/domain/LabeledValue;)V
    .locals 0
    .parameter

    .prologue
    .line 162
    iput-object p1, p0, Lcom/chase/sig/android/domain/Receipt;->taxDeductible:Lcom/chase/sig/android/domain/LabeledValue;

    .line 163
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 98
    iput-object p1, p0, Lcom/chase/sig/android/domain/Receipt;->transactionDate:Ljava/lang/String;

    .line 99
    return-void
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/chase/sig/android/domain/Receipt;->status:Ljava/lang/String;

    return-object v0
.end method

.method public final d(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 106
    iput-object p1, p0, Lcom/chase/sig/android/domain/Receipt;->transactionId:Ljava/lang/String;

    .line 107
    return-void
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/chase/sig/android/domain/Receipt;->transactionDate:Ljava/lang/String;

    return-object v0
.end method

.method public final e(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 114
    iput-object p1, p0, Lcom/chase/sig/android/domain/Receipt;->collectionId:Ljava/lang/String;

    .line 115
    return-void
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/chase/sig/android/domain/Receipt;->transactionId:Ljava/lang/String;

    return-object v0
.end method

.method public final f(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 130
    iput-object p1, p0, Lcom/chase/sig/android/domain/Receipt;->id:Ljava/lang/String;

    .line 131
    return-void
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/chase/sig/android/domain/Receipt;->collectionId:Ljava/lang/String;

    return-object v0
.end method

.method public final h()Lcom/chase/sig/android/domain/ReceiptPhotoList;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/chase/sig/android/domain/Receipt;->receiptPhotoList:Lcom/chase/sig/android/domain/ReceiptPhotoList;

    return-object v0
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/chase/sig/android/domain/Receipt;->id:Ljava/lang/String;

    return-object v0
.end method

.method public final j()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/chase/sig/android/domain/Receipt;->photoIds:[Ljava/lang/String;

    return-object v0
.end method

.method public final k()Lcom/chase/sig/android/domain/LabeledValue;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/chase/sig/android/domain/Receipt;->category:Lcom/chase/sig/android/domain/LabeledValue;

    return-object v0
.end method

.method public final l()Lcom/chase/sig/android/domain/LabeledValue;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/chase/sig/android/domain/Receipt;->expenseType:Lcom/chase/sig/android/domain/LabeledValue;

    return-object v0
.end method

.method public final m()Lcom/chase/sig/android/domain/LabeledValue;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/chase/sig/android/domain/Receipt;->taxDeductible:Lcom/chase/sig/android/domain/LabeledValue;

    return-object v0
.end method
