.class public Lcom/chase/sig/android/domain/ReceiptAccount;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private accountId:Ljava/lang/String;

.field private accountName:Ljava/lang/String;

.field private maskedName:Ljava/lang/String;

.field private matchedReceiptCount:I

.field private unMatchedReceiptCount:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 15
    iget v0, p0, Lcom/chase/sig/android/domain/ReceiptAccount;->unMatchedReceiptCount:I

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/chase/sig/android/domain/ReceiptAccount;->maskedName:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/chase/sig/android/domain/ReceiptAccount;->accountId:Ljava/lang/String;

    return-object v0
.end method

.method public final d()I
    .locals 2

    .prologue
    .line 55
    iget v0, p0, Lcom/chase/sig/android/domain/ReceiptAccount;->unMatchedReceiptCount:I

    iget v1, p0, Lcom/chase/sig/android/domain/ReceiptAccount;->matchedReceiptCount:I

    add-int/2addr v0, v1

    return v0
.end method
