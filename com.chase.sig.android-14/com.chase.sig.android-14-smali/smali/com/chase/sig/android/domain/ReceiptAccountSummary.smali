.class public Lcom/chase/sig/android/domain/ReceiptAccountSummary;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private accounts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/ReceiptAccount;",
            ">;"
        }
    .end annotation
.end field

.field private receiptLimit:I

.field private totalReceiptCount:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 15
    iget v0, p0, Lcom/chase/sig/android/domain/ReceiptAccountSummary;->receiptLimit:I

    return v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 21
    iget v0, p0, Lcom/chase/sig/android/domain/ReceiptAccountSummary;->totalReceiptCount:I

    return v0
.end method

.method public final c()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/ReceiptAccount;",
            ">;"
        }
    .end annotation

    .prologue
    .line 27
    iget-object v0, p0, Lcom/chase/sig/android/domain/ReceiptAccountSummary;->accounts:Ljava/util/List;

    return-object v0
.end method
