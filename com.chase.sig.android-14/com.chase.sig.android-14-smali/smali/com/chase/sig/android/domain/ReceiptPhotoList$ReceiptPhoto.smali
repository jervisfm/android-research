.class public Lcom/chase/sig/android/domain/ReceiptPhotoList$ReceiptPhoto;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/domain/ReceiptPhotoList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ReceiptPhoto"
.end annotation


# instance fields
.field id:Ljava/lang/String;

.field photoData:[B


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .parameter

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/chase/sig/android/domain/ReceiptPhotoList$ReceiptPhoto;->id:Ljava/lang/String;

    .line 30
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/chase/sig/android/domain/ReceiptPhotoList$ReceiptPhoto;->photoData:[B

    .line 31
    return-void
.end method

.method public constructor <init>([B)V
    .locals 0
    .parameter

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/chase/sig/android/domain/ReceiptPhotoList$ReceiptPhoto;->photoData:[B

    .line 35
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/chase/sig/android/domain/ReceiptPhotoList$ReceiptPhoto;->id:Ljava/lang/String;

    return-object v0
.end method

.method public final a([B)V
    .locals 0
    .parameter

    .prologue
    .line 50
    iput-object p1, p0, Lcom/chase/sig/android/domain/ReceiptPhotoList$ReceiptPhoto;->photoData:[B

    .line 51
    return-void
.end method

.method public final b()[B
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/chase/sig/android/domain/ReceiptPhotoList$ReceiptPhoto;->photoData:[B

    return-object v0
.end method
