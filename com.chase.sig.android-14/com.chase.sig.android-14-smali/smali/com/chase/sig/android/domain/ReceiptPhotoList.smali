.class public Lcom/chase/sig/android/domain/ReceiptPhotoList;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/domain/ReceiptPhotoList$ReceiptPhoto;
    }
.end annotation


# instance fields
.field private photoList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/chase/sig/android/domain/ReceiptPhotoList$ReceiptPhoto;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/chase/sig/android/domain/ReceiptPhotoList;->photoList:Ljava/util/ArrayList;

    .line 21
    return-void
.end method

.method public static a(Landroid/content/Intent;)Lcom/chase/sig/android/domain/ReceiptPhotoList;
    .locals 3
    .parameter

    .prologue
    .line 78
    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "receipt_photo_list"

    new-instance v2, Lcom/chase/sig/android/domain/ReceiptPhotoList;

    invoke-direct {v2}, Lcom/chase/sig/android/domain/ReceiptPhotoList;-><init>()V

    invoke-static {v0, v1, v2}, Lcom/chase/sig/android/util/e;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/ReceiptPhotoList;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/ReceiptPhotoList$ReceiptPhoto;",
            ">;"
        }
    .end annotation

    .prologue
    .line 56
    iget-object v0, p0, Lcom/chase/sig/android/domain/ReceiptPhotoList;->photoList:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2
    .parameter

    .prologue
    .line 73
    iget-object v0, p0, Lcom/chase/sig/android/domain/ReceiptPhotoList;->photoList:Ljava/util/ArrayList;

    new-instance v1, Lcom/chase/sig/android/domain/ReceiptPhotoList$ReceiptPhoto;

    invoke-direct {v1, p1}, Lcom/chase/sig/android/domain/ReceiptPhotoList$ReceiptPhoto;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 74
    return-void
.end method

.method public final a([B)V
    .locals 2
    .parameter

    .prologue
    .line 69
    iget-object v0, p0, Lcom/chase/sig/android/domain/ReceiptPhotoList;->photoList:Ljava/util/ArrayList;

    new-instance v1, Lcom/chase/sig/android/domain/ReceiptPhotoList$ReceiptPhoto;

    invoke-direct {v1, p1}, Lcom/chase/sig/android/domain/ReceiptPhotoList$ReceiptPhoto;-><init>([B)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 70
    return-void
.end method
