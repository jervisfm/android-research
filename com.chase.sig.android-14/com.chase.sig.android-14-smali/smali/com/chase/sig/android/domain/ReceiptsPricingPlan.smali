.class public Lcom/chase/sig/android/domain/ReceiptsPricingPlan;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field description:Ljava/lang/String;

.field enrolled:Z

.field fee:Lcom/chase/sig/android/util/Dollar;

.field label:Ljava/lang/String;

.field limit:I

.field limitExceeded:Z

.field planCode:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-boolean v0, p0, Lcom/chase/sig/android/domain/ReceiptsPricingPlan;->enrolled:Z

    .line 16
    iput-boolean v0, p0, Lcom/chase/sig/android/domain/ReceiptsPricingPlan;->limitExceeded:Z

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/chase/sig/android/domain/ReceiptsPricingPlan;->label:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/chase/sig/android/domain/ReceiptsPricingPlan;->description:Ljava/lang/String;

    return-object v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 45
    iget v0, p0, Lcom/chase/sig/android/domain/ReceiptsPricingPlan;->limit:I

    return v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/chase/sig/android/domain/ReceiptsPricingPlan;->planCode:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/chase/sig/android/domain/ReceiptsPricingPlan;->enrolled:Z

    return v0
.end method
