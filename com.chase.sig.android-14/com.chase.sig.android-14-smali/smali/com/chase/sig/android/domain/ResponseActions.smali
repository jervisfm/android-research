.class public Lcom/chase/sig/android/domain/ResponseActions;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/domain/ResponseActions$Action;
    }
.end annotation


# instance fields
.field private cancelAction:Lcom/chase/sig/android/domain/ResponseActions$Action;
    .annotation runtime Lcom/google/gson/chase/annotations/SerializedName;
        a = "cancel"
    .end annotation
.end field

.field private continueAction:Lcom/chase/sig/android/domain/ResponseActions$Action;
    .annotation runtime Lcom/google/gson/chase/annotations/SerializedName;
        a = "advance"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    return-void
.end method


# virtual methods
.method public final a()Lcom/chase/sig/android/domain/ResponseActions$Action;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/chase/sig/android/domain/ResponseActions;->cancelAction:Lcom/chase/sig/android/domain/ResponseActions$Action;

    return-object v0
.end method

.method public final b()Lcom/chase/sig/android/domain/ResponseActions$Action;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/chase/sig/android/domain/ResponseActions;->continueAction:Lcom/chase/sig/android/domain/ResponseActions$Action;

    return-object v0
.end method
