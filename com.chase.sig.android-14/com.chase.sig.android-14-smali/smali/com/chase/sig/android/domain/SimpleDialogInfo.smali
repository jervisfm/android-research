.class public Lcom/chase/sig/android/domain/SimpleDialogInfo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private cancellable:Z

.field private finishOnDismiss:Z

.field private id:I

.field private message:Ljava/lang/String;

.field private title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 18
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/chase/sig/android/domain/SimpleDialogInfo;-><init>(Ljava/lang/String;Z)V

    .line 19
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 30
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, p2, v1}, Lcom/chase/sig/android/domain/SimpleDialogInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Z)V

    .line 31
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 42
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/chase/sig/android/domain/SimpleDialogInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Z)V

    .line 43
    return-void
.end method

.method private constructor <init>(ILjava/lang/String;Ljava/lang/String;Z)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    const/4 v0, -0x1

    iput v0, p0, Lcom/chase/sig/android/domain/SimpleDialogInfo;->id:I

    .line 13
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/chase/sig/android/domain/SimpleDialogInfo;->cancellable:Z

    .line 46
    iput p1, p0, Lcom/chase/sig/android/domain/SimpleDialogInfo;->id:I

    .line 47
    iput-object p2, p0, Lcom/chase/sig/android/domain/SimpleDialogInfo;->title:Ljava/lang/String;

    .line 48
    iput-object p3, p0, Lcom/chase/sig/android/domain/SimpleDialogInfo;->message:Ljava/lang/String;

    .line 49
    iput-boolean p4, p0, Lcom/chase/sig/android/domain/SimpleDialogInfo;->finishOnDismiss:Z

    .line 50
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .parameter

    .prologue
    .line 26
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/chase/sig/android/domain/SimpleDialogInfo;-><init>(Ljava/lang/String;Z)V

    .line 27
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    const/4 v0, -0x1

    iput v0, p0, Lcom/chase/sig/android/domain/SimpleDialogInfo;->id:I

    .line 13
    iput-boolean v1, p0, Lcom/chase/sig/android/domain/SimpleDialogInfo;->cancellable:Z

    .line 53
    iput-object p1, p0, Lcom/chase/sig/android/domain/SimpleDialogInfo;->title:Ljava/lang/String;

    .line 54
    iput-object p2, p0, Lcom/chase/sig/android/domain/SimpleDialogInfo;->message:Ljava/lang/String;

    .line 55
    iput-boolean v1, p0, Lcom/chase/sig/android/domain/SimpleDialogInfo;->finishOnDismiss:Z

    .line 56
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 34
    const/16 v0, -0x270f

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1, p1, p2}, Lcom/chase/sig/android/domain/SimpleDialogInfo;-><init>(ILjava/lang/String;Ljava/lang/String;Z)V

    .line 35
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/chase/sig/android/domain/SimpleDialogInfo;->id:I

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/chase/sig/android/domain/SimpleDialogInfo;->message:Ljava/lang/String;

    return-object v0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/chase/sig/android/domain/SimpleDialogInfo;->finishOnDismiss:Z

    .line 76
    return-void
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 79
    iget-boolean v0, p0, Lcom/chase/sig/android/domain/SimpleDialogInfo;->finishOnDismiss:Z

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/chase/sig/android/domain/SimpleDialogInfo;->title:Ljava/lang/String;

    return-object v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 91
    iget-boolean v0, p0, Lcom/chase/sig/android/domain/SimpleDialogInfo;->cancellable:Z

    return v0
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 95
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/chase/sig/android/domain/SimpleDialogInfo;->cancellable:Z

    .line 96
    return-void
.end method
