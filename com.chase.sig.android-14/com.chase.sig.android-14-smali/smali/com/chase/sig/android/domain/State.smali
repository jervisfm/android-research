.class public final enum Lcom/chase/sig/android/domain/State;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/chase/sig/android/domain/State;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum A:Lcom/chase/sig/android/domain/State;

.field public static final enum B:Lcom/chase/sig/android/domain/State;

.field public static final enum C:Lcom/chase/sig/android/domain/State;

.field public static final enum D:Lcom/chase/sig/android/domain/State;

.field public static final enum E:Lcom/chase/sig/android/domain/State;

.field public static final enum F:Lcom/chase/sig/android/domain/State;

.field public static final enum G:Lcom/chase/sig/android/domain/State;

.field public static final enum H:Lcom/chase/sig/android/domain/State;

.field public static final enum I:Lcom/chase/sig/android/domain/State;

.field public static final enum J:Lcom/chase/sig/android/domain/State;

.field public static final enum K:Lcom/chase/sig/android/domain/State;

.field public static final enum L:Lcom/chase/sig/android/domain/State;

.field public static final enum M:Lcom/chase/sig/android/domain/State;

.field public static final enum N:Lcom/chase/sig/android/domain/State;

.field public static final enum O:Lcom/chase/sig/android/domain/State;

.field public static final enum P:Lcom/chase/sig/android/domain/State;

.field public static final enum Q:Lcom/chase/sig/android/domain/State;

.field public static final enum R:Lcom/chase/sig/android/domain/State;

.field public static final enum S:Lcom/chase/sig/android/domain/State;

.field public static final enum T:Lcom/chase/sig/android/domain/State;

.field public static final enum U:Lcom/chase/sig/android/domain/State;

.field public static final enum V:Lcom/chase/sig/android/domain/State;

.field public static final enum W:Lcom/chase/sig/android/domain/State;

.field public static final enum X:Lcom/chase/sig/android/domain/State;

.field public static final enum Y:Lcom/chase/sig/android/domain/State;

.field public static final enum Z:Lcom/chase/sig/android/domain/State;

.field public static final enum a:Lcom/chase/sig/android/domain/State;

.field public static final enum aa:Lcom/chase/sig/android/domain/State;

.field public static final enum ab:Lcom/chase/sig/android/domain/State;

.field public static final enum ac:Lcom/chase/sig/android/domain/State;

.field public static final enum ad:Lcom/chase/sig/android/domain/State;

.field private static final synthetic ae:[Lcom/chase/sig/android/domain/State;

.field public static final enum b:Lcom/chase/sig/android/domain/State;

.field public static final enum c:Lcom/chase/sig/android/domain/State;

.field public static final enum d:Lcom/chase/sig/android/domain/State;

.field public static final enum e:Lcom/chase/sig/android/domain/State;

.field public static final enum f:Lcom/chase/sig/android/domain/State;

.field public static final enum g:Lcom/chase/sig/android/domain/State;

.field public static final enum h:Lcom/chase/sig/android/domain/State;

.field public static final enum i:Lcom/chase/sig/android/domain/State;

.field public static final enum j:Lcom/chase/sig/android/domain/State;

.field public static final enum k:Lcom/chase/sig/android/domain/State;

.field public static final enum l:Lcom/chase/sig/android/domain/State;

.field public static final enum m:Lcom/chase/sig/android/domain/State;

.field public static final enum n:Lcom/chase/sig/android/domain/State;

.field public static final enum o:Lcom/chase/sig/android/domain/State;

.field public static final enum p:Lcom/chase/sig/android/domain/State;

.field public static final enum q:Lcom/chase/sig/android/domain/State;

.field public static final enum r:Lcom/chase/sig/android/domain/State;

.field public static final enum s:Lcom/chase/sig/android/domain/State;

.field public static final enum t:Lcom/chase/sig/android/domain/State;

.field public static final enum u:Lcom/chase/sig/android/domain/State;

.field public static final enum v:Lcom/chase/sig/android/domain/State;

.field public static final enum w:Lcom/chase/sig/android/domain/State;

.field public static final enum x:Lcom/chase/sig/android/domain/State;

.field public static final enum y:Lcom/chase/sig/android/domain/State;

.field public static final enum z:Lcom/chase/sig/android/domain/State;


# instance fields
.field private final displayName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 4
    new-instance v0, Lcom/chase/sig/android/domain/State;

    const-string v1, "AL"

    const-string v2, "Alabama"

    invoke-direct {v0, v1, v4, v2}, Lcom/chase/sig/android/domain/State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/State;->a:Lcom/chase/sig/android/domain/State;

    .line 5
    new-instance v0, Lcom/chase/sig/android/domain/State;

    const-string v1, "AK"

    const-string v2, "Alaska"

    invoke-direct {v0, v1, v5, v2}, Lcom/chase/sig/android/domain/State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/State;->b:Lcom/chase/sig/android/domain/State;

    .line 6
    new-instance v0, Lcom/chase/sig/android/domain/State;

    const-string v1, "AS"

    const-string v2, "American Samoa"

    invoke-direct {v0, v1, v6, v2}, Lcom/chase/sig/android/domain/State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/State;->c:Lcom/chase/sig/android/domain/State;

    .line 7
    new-instance v0, Lcom/chase/sig/android/domain/State;

    const-string v1, "AZ"

    const-string v2, "Arizona"

    invoke-direct {v0, v1, v7, v2}, Lcom/chase/sig/android/domain/State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/State;->d:Lcom/chase/sig/android/domain/State;

    .line 8
    new-instance v0, Lcom/chase/sig/android/domain/State;

    const-string v1, "AR"

    const-string v2, "Arkansas"

    invoke-direct {v0, v1, v8, v2}, Lcom/chase/sig/android/domain/State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/State;->e:Lcom/chase/sig/android/domain/State;

    .line 9
    new-instance v0, Lcom/chase/sig/android/domain/State;

    const-string v1, "CA"

    const/4 v2, 0x5

    const-string v3, "California"

    invoke-direct {v0, v1, v2, v3}, Lcom/chase/sig/android/domain/State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/State;->f:Lcom/chase/sig/android/domain/State;

    .line 10
    new-instance v0, Lcom/chase/sig/android/domain/State;

    const-string v1, "CO"

    const/4 v2, 0x6

    const-string v3, "Colorado"

    invoke-direct {v0, v1, v2, v3}, Lcom/chase/sig/android/domain/State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/State;->g:Lcom/chase/sig/android/domain/State;

    .line 11
    new-instance v0, Lcom/chase/sig/android/domain/State;

    const-string v1, "CT"

    const/4 v2, 0x7

    const-string v3, "Connecticut"

    invoke-direct {v0, v1, v2, v3}, Lcom/chase/sig/android/domain/State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/State;->h:Lcom/chase/sig/android/domain/State;

    .line 12
    new-instance v0, Lcom/chase/sig/android/domain/State;

    const-string v1, "DE"

    const/16 v2, 0x8

    const-string v3, "Delaware"

    invoke-direct {v0, v1, v2, v3}, Lcom/chase/sig/android/domain/State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/State;->i:Lcom/chase/sig/android/domain/State;

    .line 13
    new-instance v0, Lcom/chase/sig/android/domain/State;

    const-string v1, "DC"

    const/16 v2, 0x9

    const-string v3, "District of Columbia"

    invoke-direct {v0, v1, v2, v3}, Lcom/chase/sig/android/domain/State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/State;->j:Lcom/chase/sig/android/domain/State;

    .line 14
    new-instance v0, Lcom/chase/sig/android/domain/State;

    const-string v1, "FL"

    const/16 v2, 0xa

    const-string v3, "Florida"

    invoke-direct {v0, v1, v2, v3}, Lcom/chase/sig/android/domain/State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/State;->k:Lcom/chase/sig/android/domain/State;

    .line 15
    new-instance v0, Lcom/chase/sig/android/domain/State;

    const-string v1, "GA"

    const/16 v2, 0xb

    const-string v3, "Georgia"

    invoke-direct {v0, v1, v2, v3}, Lcom/chase/sig/android/domain/State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/State;->l:Lcom/chase/sig/android/domain/State;

    .line 16
    new-instance v0, Lcom/chase/sig/android/domain/State;

    const-string v1, "GU"

    const/16 v2, 0xc

    const-string v3, "Guam"

    invoke-direct {v0, v1, v2, v3}, Lcom/chase/sig/android/domain/State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/State;->m:Lcom/chase/sig/android/domain/State;

    .line 17
    new-instance v0, Lcom/chase/sig/android/domain/State;

    const-string v1, "HI"

    const/16 v2, 0xd

    const-string v3, "Hawaii"

    invoke-direct {v0, v1, v2, v3}, Lcom/chase/sig/android/domain/State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/State;->n:Lcom/chase/sig/android/domain/State;

    .line 18
    new-instance v0, Lcom/chase/sig/android/domain/State;

    const-string v1, "ID"

    const/16 v2, 0xe

    const-string v3, "Idaho"

    invoke-direct {v0, v1, v2, v3}, Lcom/chase/sig/android/domain/State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/State;->o:Lcom/chase/sig/android/domain/State;

    .line 19
    new-instance v0, Lcom/chase/sig/android/domain/State;

    const-string v1, "IL"

    const/16 v2, 0xf

    const-string v3, "Illinois"

    invoke-direct {v0, v1, v2, v3}, Lcom/chase/sig/android/domain/State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/State;->p:Lcom/chase/sig/android/domain/State;

    .line 20
    new-instance v0, Lcom/chase/sig/android/domain/State;

    const-string v1, "IN"

    const/16 v2, 0x10

    const-string v3, "Indiana"

    invoke-direct {v0, v1, v2, v3}, Lcom/chase/sig/android/domain/State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/State;->q:Lcom/chase/sig/android/domain/State;

    .line 21
    new-instance v0, Lcom/chase/sig/android/domain/State;

    const-string v1, "IA"

    const/16 v2, 0x11

    const-string v3, "Iowa"

    invoke-direct {v0, v1, v2, v3}, Lcom/chase/sig/android/domain/State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/State;->r:Lcom/chase/sig/android/domain/State;

    .line 22
    new-instance v0, Lcom/chase/sig/android/domain/State;

    const-string v1, "KS"

    const/16 v2, 0x12

    const-string v3, "Kansas"

    invoke-direct {v0, v1, v2, v3}, Lcom/chase/sig/android/domain/State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/State;->s:Lcom/chase/sig/android/domain/State;

    .line 23
    new-instance v0, Lcom/chase/sig/android/domain/State;

    const-string v1, "KY"

    const/16 v2, 0x13

    const-string v3, "Kentucky"

    invoke-direct {v0, v1, v2, v3}, Lcom/chase/sig/android/domain/State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/State;->t:Lcom/chase/sig/android/domain/State;

    .line 24
    new-instance v0, Lcom/chase/sig/android/domain/State;

    const-string v1, "LA"

    const/16 v2, 0x14

    const-string v3, "Louisiana"

    invoke-direct {v0, v1, v2, v3}, Lcom/chase/sig/android/domain/State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/State;->u:Lcom/chase/sig/android/domain/State;

    .line 25
    new-instance v0, Lcom/chase/sig/android/domain/State;

    const-string v1, "ME"

    const/16 v2, 0x15

    const-string v3, "Maine"

    invoke-direct {v0, v1, v2, v3}, Lcom/chase/sig/android/domain/State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/State;->v:Lcom/chase/sig/android/domain/State;

    .line 26
    new-instance v0, Lcom/chase/sig/android/domain/State;

    const-string v1, "MD"

    const/16 v2, 0x16

    const-string v3, "Maryland"

    invoke-direct {v0, v1, v2, v3}, Lcom/chase/sig/android/domain/State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/State;->w:Lcom/chase/sig/android/domain/State;

    .line 27
    new-instance v0, Lcom/chase/sig/android/domain/State;

    const-string v1, "MA"

    const/16 v2, 0x17

    const-string v3, "Massachusetts"

    invoke-direct {v0, v1, v2, v3}, Lcom/chase/sig/android/domain/State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/State;->x:Lcom/chase/sig/android/domain/State;

    .line 28
    new-instance v0, Lcom/chase/sig/android/domain/State;

    const-string v1, "MI"

    const/16 v2, 0x18

    const-string v3, "Michigan"

    invoke-direct {v0, v1, v2, v3}, Lcom/chase/sig/android/domain/State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/State;->y:Lcom/chase/sig/android/domain/State;

    .line 29
    new-instance v0, Lcom/chase/sig/android/domain/State;

    const-string v1, "MN"

    const/16 v2, 0x19

    const-string v3, "Minnesota"

    invoke-direct {v0, v1, v2, v3}, Lcom/chase/sig/android/domain/State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/State;->z:Lcom/chase/sig/android/domain/State;

    .line 30
    new-instance v0, Lcom/chase/sig/android/domain/State;

    const-string v1, "MS"

    const/16 v2, 0x1a

    const-string v3, "Mississippi"

    invoke-direct {v0, v1, v2, v3}, Lcom/chase/sig/android/domain/State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/State;->A:Lcom/chase/sig/android/domain/State;

    .line 31
    new-instance v0, Lcom/chase/sig/android/domain/State;

    const-string v1, "MO"

    const/16 v2, 0x1b

    const-string v3, "Missouri"

    invoke-direct {v0, v1, v2, v3}, Lcom/chase/sig/android/domain/State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/State;->B:Lcom/chase/sig/android/domain/State;

    .line 32
    new-instance v0, Lcom/chase/sig/android/domain/State;

    const-string v1, "MT"

    const/16 v2, 0x1c

    const-string v3, "Montana"

    invoke-direct {v0, v1, v2, v3}, Lcom/chase/sig/android/domain/State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/State;->C:Lcom/chase/sig/android/domain/State;

    .line 33
    new-instance v0, Lcom/chase/sig/android/domain/State;

    const-string v1, "NE"

    const/16 v2, 0x1d

    const-string v3, "Nebraska"

    invoke-direct {v0, v1, v2, v3}, Lcom/chase/sig/android/domain/State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/State;->D:Lcom/chase/sig/android/domain/State;

    .line 34
    new-instance v0, Lcom/chase/sig/android/domain/State;

    const-string v1, "NV"

    const/16 v2, 0x1e

    const-string v3, "Nevada"

    invoke-direct {v0, v1, v2, v3}, Lcom/chase/sig/android/domain/State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/State;->E:Lcom/chase/sig/android/domain/State;

    .line 35
    new-instance v0, Lcom/chase/sig/android/domain/State;

    const-string v1, "NH"

    const/16 v2, 0x1f

    const-string v3, "New Hampshire"

    invoke-direct {v0, v1, v2, v3}, Lcom/chase/sig/android/domain/State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/State;->F:Lcom/chase/sig/android/domain/State;

    .line 36
    new-instance v0, Lcom/chase/sig/android/domain/State;

    const-string v1, "NJ"

    const/16 v2, 0x20

    const-string v3, "New Jersey"

    invoke-direct {v0, v1, v2, v3}, Lcom/chase/sig/android/domain/State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/State;->G:Lcom/chase/sig/android/domain/State;

    .line 37
    new-instance v0, Lcom/chase/sig/android/domain/State;

    const-string v1, "NM"

    const/16 v2, 0x21

    const-string v3, "New Mexico"

    invoke-direct {v0, v1, v2, v3}, Lcom/chase/sig/android/domain/State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/State;->H:Lcom/chase/sig/android/domain/State;

    .line 38
    new-instance v0, Lcom/chase/sig/android/domain/State;

    const-string v1, "NY"

    const/16 v2, 0x22

    const-string v3, "New York"

    invoke-direct {v0, v1, v2, v3}, Lcom/chase/sig/android/domain/State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/State;->I:Lcom/chase/sig/android/domain/State;

    .line 39
    new-instance v0, Lcom/chase/sig/android/domain/State;

    const-string v1, "NC"

    const/16 v2, 0x23

    const-string v3, "North Carolina"

    invoke-direct {v0, v1, v2, v3}, Lcom/chase/sig/android/domain/State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/State;->J:Lcom/chase/sig/android/domain/State;

    .line 40
    new-instance v0, Lcom/chase/sig/android/domain/State;

    const-string v1, "ND"

    const/16 v2, 0x24

    const-string v3, "North Dakota"

    invoke-direct {v0, v1, v2, v3}, Lcom/chase/sig/android/domain/State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/State;->K:Lcom/chase/sig/android/domain/State;

    .line 41
    new-instance v0, Lcom/chase/sig/android/domain/State;

    const-string v1, "MP"

    const/16 v2, 0x25

    const-string v3, "Northern Marianas Islands"

    invoke-direct {v0, v1, v2, v3}, Lcom/chase/sig/android/domain/State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/State;->L:Lcom/chase/sig/android/domain/State;

    .line 42
    new-instance v0, Lcom/chase/sig/android/domain/State;

    const-string v1, "OH"

    const/16 v2, 0x26

    const-string v3, "Ohio"

    invoke-direct {v0, v1, v2, v3}, Lcom/chase/sig/android/domain/State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/State;->M:Lcom/chase/sig/android/domain/State;

    .line 43
    new-instance v0, Lcom/chase/sig/android/domain/State;

    const-string v1, "OK"

    const/16 v2, 0x27

    const-string v3, "Oklahoma"

    invoke-direct {v0, v1, v2, v3}, Lcom/chase/sig/android/domain/State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/State;->N:Lcom/chase/sig/android/domain/State;

    .line 44
    new-instance v0, Lcom/chase/sig/android/domain/State;

    const-string v1, "OR"

    const/16 v2, 0x28

    const-string v3, "Oregon"

    invoke-direct {v0, v1, v2, v3}, Lcom/chase/sig/android/domain/State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/State;->O:Lcom/chase/sig/android/domain/State;

    .line 45
    new-instance v0, Lcom/chase/sig/android/domain/State;

    const-string v1, "PA"

    const/16 v2, 0x29

    const-string v3, "Pennsylvania"

    invoke-direct {v0, v1, v2, v3}, Lcom/chase/sig/android/domain/State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/State;->P:Lcom/chase/sig/android/domain/State;

    .line 46
    new-instance v0, Lcom/chase/sig/android/domain/State;

    const-string v1, "PR"

    const/16 v2, 0x2a

    const-string v3, "Puerto Rico"

    invoke-direct {v0, v1, v2, v3}, Lcom/chase/sig/android/domain/State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/State;->Q:Lcom/chase/sig/android/domain/State;

    .line 47
    new-instance v0, Lcom/chase/sig/android/domain/State;

    const-string v1, "RI"

    const/16 v2, 0x2b

    const-string v3, "Rhode Island"

    invoke-direct {v0, v1, v2, v3}, Lcom/chase/sig/android/domain/State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/State;->R:Lcom/chase/sig/android/domain/State;

    .line 48
    new-instance v0, Lcom/chase/sig/android/domain/State;

    const-string v1, "SC"

    const/16 v2, 0x2c

    const-string v3, "South Carolina"

    invoke-direct {v0, v1, v2, v3}, Lcom/chase/sig/android/domain/State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/State;->S:Lcom/chase/sig/android/domain/State;

    .line 49
    new-instance v0, Lcom/chase/sig/android/domain/State;

    const-string v1, "SD"

    const/16 v2, 0x2d

    const-string v3, "South Dakota"

    invoke-direct {v0, v1, v2, v3}, Lcom/chase/sig/android/domain/State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/State;->T:Lcom/chase/sig/android/domain/State;

    .line 50
    new-instance v0, Lcom/chase/sig/android/domain/State;

    const-string v1, "TN"

    const/16 v2, 0x2e

    const-string v3, "Tennessee"

    invoke-direct {v0, v1, v2, v3}, Lcom/chase/sig/android/domain/State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/State;->U:Lcom/chase/sig/android/domain/State;

    .line 51
    new-instance v0, Lcom/chase/sig/android/domain/State;

    const-string v1, "TX"

    const/16 v2, 0x2f

    const-string v3, "Texas"

    invoke-direct {v0, v1, v2, v3}, Lcom/chase/sig/android/domain/State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/State;->V:Lcom/chase/sig/android/domain/State;

    .line 52
    new-instance v0, Lcom/chase/sig/android/domain/State;

    const-string v1, "UT"

    const/16 v2, 0x30

    const-string v3, "Utah"

    invoke-direct {v0, v1, v2, v3}, Lcom/chase/sig/android/domain/State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/State;->W:Lcom/chase/sig/android/domain/State;

    .line 53
    new-instance v0, Lcom/chase/sig/android/domain/State;

    const-string v1, "VT"

    const/16 v2, 0x31

    const-string v3, "Vermont"

    invoke-direct {v0, v1, v2, v3}, Lcom/chase/sig/android/domain/State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/State;->X:Lcom/chase/sig/android/domain/State;

    .line 54
    new-instance v0, Lcom/chase/sig/android/domain/State;

    const-string v1, "VI"

    const/16 v2, 0x32

    const-string v3, "Virgin Islands"

    invoke-direct {v0, v1, v2, v3}, Lcom/chase/sig/android/domain/State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/State;->Y:Lcom/chase/sig/android/domain/State;

    .line 55
    new-instance v0, Lcom/chase/sig/android/domain/State;

    const-string v1, "VA"

    const/16 v2, 0x33

    const-string v3, "Virginia"

    invoke-direct {v0, v1, v2, v3}, Lcom/chase/sig/android/domain/State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/State;->Z:Lcom/chase/sig/android/domain/State;

    .line 56
    new-instance v0, Lcom/chase/sig/android/domain/State;

    const-string v1, "WA"

    const/16 v2, 0x34

    const-string v3, "Washington"

    invoke-direct {v0, v1, v2, v3}, Lcom/chase/sig/android/domain/State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/State;->aa:Lcom/chase/sig/android/domain/State;

    .line 57
    new-instance v0, Lcom/chase/sig/android/domain/State;

    const-string v1, "WV"

    const/16 v2, 0x35

    const-string v3, "West Virginia"

    invoke-direct {v0, v1, v2, v3}, Lcom/chase/sig/android/domain/State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/State;->ab:Lcom/chase/sig/android/domain/State;

    .line 58
    new-instance v0, Lcom/chase/sig/android/domain/State;

    const-string v1, "WI"

    const/16 v2, 0x36

    const-string v3, "Wisconsin"

    invoke-direct {v0, v1, v2, v3}, Lcom/chase/sig/android/domain/State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/State;->ac:Lcom/chase/sig/android/domain/State;

    .line 59
    new-instance v0, Lcom/chase/sig/android/domain/State;

    const-string v1, "WY"

    const/16 v2, 0x37

    const-string v3, "Wyoming"

    invoke-direct {v0, v1, v2, v3}, Lcom/chase/sig/android/domain/State;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/domain/State;->ad:Lcom/chase/sig/android/domain/State;

    .line 3
    const/16 v0, 0x38

    new-array v0, v0, [Lcom/chase/sig/android/domain/State;

    sget-object v1, Lcom/chase/sig/android/domain/State;->a:Lcom/chase/sig/android/domain/State;

    aput-object v1, v0, v4

    sget-object v1, Lcom/chase/sig/android/domain/State;->b:Lcom/chase/sig/android/domain/State;

    aput-object v1, v0, v5

    sget-object v1, Lcom/chase/sig/android/domain/State;->c:Lcom/chase/sig/android/domain/State;

    aput-object v1, v0, v6

    sget-object v1, Lcom/chase/sig/android/domain/State;->d:Lcom/chase/sig/android/domain/State;

    aput-object v1, v0, v7

    sget-object v1, Lcom/chase/sig/android/domain/State;->e:Lcom/chase/sig/android/domain/State;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/chase/sig/android/domain/State;->f:Lcom/chase/sig/android/domain/State;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/chase/sig/android/domain/State;->g:Lcom/chase/sig/android/domain/State;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/chase/sig/android/domain/State;->h:Lcom/chase/sig/android/domain/State;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/chase/sig/android/domain/State;->i:Lcom/chase/sig/android/domain/State;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/chase/sig/android/domain/State;->j:Lcom/chase/sig/android/domain/State;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/chase/sig/android/domain/State;->k:Lcom/chase/sig/android/domain/State;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/chase/sig/android/domain/State;->l:Lcom/chase/sig/android/domain/State;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/chase/sig/android/domain/State;->m:Lcom/chase/sig/android/domain/State;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/chase/sig/android/domain/State;->n:Lcom/chase/sig/android/domain/State;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/chase/sig/android/domain/State;->o:Lcom/chase/sig/android/domain/State;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/chase/sig/android/domain/State;->p:Lcom/chase/sig/android/domain/State;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/chase/sig/android/domain/State;->q:Lcom/chase/sig/android/domain/State;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/chase/sig/android/domain/State;->r:Lcom/chase/sig/android/domain/State;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/chase/sig/android/domain/State;->s:Lcom/chase/sig/android/domain/State;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/chase/sig/android/domain/State;->t:Lcom/chase/sig/android/domain/State;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/chase/sig/android/domain/State;->u:Lcom/chase/sig/android/domain/State;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/chase/sig/android/domain/State;->v:Lcom/chase/sig/android/domain/State;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/chase/sig/android/domain/State;->w:Lcom/chase/sig/android/domain/State;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/chase/sig/android/domain/State;->x:Lcom/chase/sig/android/domain/State;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/chase/sig/android/domain/State;->y:Lcom/chase/sig/android/domain/State;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/chase/sig/android/domain/State;->z:Lcom/chase/sig/android/domain/State;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/chase/sig/android/domain/State;->A:Lcom/chase/sig/android/domain/State;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/chase/sig/android/domain/State;->B:Lcom/chase/sig/android/domain/State;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/chase/sig/android/domain/State;->C:Lcom/chase/sig/android/domain/State;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/chase/sig/android/domain/State;->D:Lcom/chase/sig/android/domain/State;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/chase/sig/android/domain/State;->E:Lcom/chase/sig/android/domain/State;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/chase/sig/android/domain/State;->F:Lcom/chase/sig/android/domain/State;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/chase/sig/android/domain/State;->G:Lcom/chase/sig/android/domain/State;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/chase/sig/android/domain/State;->H:Lcom/chase/sig/android/domain/State;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/chase/sig/android/domain/State;->I:Lcom/chase/sig/android/domain/State;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/chase/sig/android/domain/State;->J:Lcom/chase/sig/android/domain/State;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/chase/sig/android/domain/State;->K:Lcom/chase/sig/android/domain/State;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/chase/sig/android/domain/State;->L:Lcom/chase/sig/android/domain/State;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/chase/sig/android/domain/State;->M:Lcom/chase/sig/android/domain/State;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/chase/sig/android/domain/State;->N:Lcom/chase/sig/android/domain/State;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/chase/sig/android/domain/State;->O:Lcom/chase/sig/android/domain/State;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/chase/sig/android/domain/State;->P:Lcom/chase/sig/android/domain/State;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/chase/sig/android/domain/State;->Q:Lcom/chase/sig/android/domain/State;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/chase/sig/android/domain/State;->R:Lcom/chase/sig/android/domain/State;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/chase/sig/android/domain/State;->S:Lcom/chase/sig/android/domain/State;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/chase/sig/android/domain/State;->T:Lcom/chase/sig/android/domain/State;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/chase/sig/android/domain/State;->U:Lcom/chase/sig/android/domain/State;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lcom/chase/sig/android/domain/State;->V:Lcom/chase/sig/android/domain/State;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lcom/chase/sig/android/domain/State;->W:Lcom/chase/sig/android/domain/State;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lcom/chase/sig/android/domain/State;->X:Lcom/chase/sig/android/domain/State;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Lcom/chase/sig/android/domain/State;->Y:Lcom/chase/sig/android/domain/State;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, Lcom/chase/sig/android/domain/State;->Z:Lcom/chase/sig/android/domain/State;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, Lcom/chase/sig/android/domain/State;->aa:Lcom/chase/sig/android/domain/State;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, Lcom/chase/sig/android/domain/State;->ab:Lcom/chase/sig/android/domain/State;

    aput-object v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, Lcom/chase/sig/android/domain/State;->ac:Lcom/chase/sig/android/domain/State;

    aput-object v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, Lcom/chase/sig/android/domain/State;->ad:Lcom/chase/sig/android/domain/State;

    aput-object v2, v0, v1

    sput-object v0, Lcom/chase/sig/android/domain/State;->ae:[Lcom/chase/sig/android/domain/State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 64
    iput-object p3, p0, Lcom/chase/sig/android/domain/State;->displayName:Ljava/lang/String;

    .line 65
    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/chase/sig/android/domain/State;
    .locals 1
    .parameter

    .prologue
    .line 78
    :try_start_0
    invoke-static {p0}, Lcom/chase/sig/android/domain/State;->valueOf(Ljava/lang/String;)Lcom/chase/sig/android/domain/State;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 81
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;)Lcom/chase/sig/android/domain/State;
    .locals 5
    .parameter

    .prologue
    .line 89
    invoke-static {}, Lcom/chase/sig/android/domain/State;->values()[Lcom/chase/sig/android/domain/State;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 90
    iget-object v4, v0, Lcom/chase/sig/android/domain/State;->displayName:Ljava/lang/String;

    invoke-virtual {p0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 95
    :goto_1
    return-object v0

    .line 89
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 95
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/chase/sig/android/domain/State;
    .locals 1
    .parameter

    .prologue
    .line 3
    const-class v0, Lcom/chase/sig/android/domain/State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/State;

    return-object v0
.end method

.method public static values()[Lcom/chase/sig/android/domain/State;
    .locals 1

    .prologue
    .line 3
    sget-object v0, Lcom/chase/sig/android/domain/State;->ae:[Lcom/chase/sig/android/domain/State;

    invoke-virtual {v0}, [Lcom/chase/sig/android/domain/State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/chase/sig/android/domain/State;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/chase/sig/android/domain/State;->displayName:Ljava/lang/String;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/chase/sig/android/domain/State;->displayName:Ljava/lang/String;

    return-object v0
.end method
