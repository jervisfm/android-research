.class public abstract Lcom/chase/sig/android/domain/Transaction;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private amount:Lcom/chase/sig/android/util/Dollar;

.field private cancellable:Z

.field public deliverByDate:Ljava/lang/String;

.field private editable:Z

.field private formId:Ljava/lang/String;

.field private frequency:Ljava/lang/String;

.field private fromAccountId:Ljava/lang/String;

.field public fromAccountMask:Ljava/lang/String;

.field fromAccountNickName:Ljava/lang/String;

.field private gwoFrequency:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/chase/sig/android/domain/LabeledValue;",
            ">;"
        }
    .end annotation
.end field

.field private isOpenEnded:Z

.field private memo:Ljava/lang/String;

.field private mmbfrequency:Lcom/chase/sig/android/domain/MMBFrequency;

.field private paymentModelId:Ljava/lang/String;

.field private paymentModelToken:Ljava/lang/String;

.field private paymentToken:Ljava/lang/String;

.field private remainingInstances:Ljava/lang/String;

.field private status:Ljava/lang/String;

.field public toAccountMask:Ljava/lang/String;

.field toAccountNumber:Ljava/lang/String;

.field private transactionId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final A()Ljava/lang/String;
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lcom/chase/sig/android/domain/Transaction;->toAccountNumber:Ljava/lang/String;

    return-object v0
.end method

.method public B()Ljava/lang/String;
    .locals 4

    .prologue
    .line 237
    const-string v0, "To %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/Transaction;->d()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final C()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/chase/sig/android/domain/LabeledValue;",
            ">;"
        }
    .end annotation

    .prologue
    .line 241
    iget-object v0, p0, Lcom/chase/sig/android/domain/Transaction;->gwoFrequency:Ljava/util/ArrayList;

    return-object v0
.end method

.method public a(Lcom/chase/sig/android/util/Dollar;)V
    .locals 0
    .parameter

    .prologue
    .line 74
    iput-object p1, p0, Lcom/chase/sig/android/domain/Transaction;->amount:Lcom/chase/sig/android/util/Dollar;

    .line 75
    return-void
.end method

.method public final a(Ljava/util/ArrayList;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/chase/sig/android/domain/LabeledValue;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 245
    iput-object p1, p0, Lcom/chase/sig/android/domain/Transaction;->gwoFrequency:Ljava/util/ArrayList;

    .line 246
    return-void
.end method

.method public final a(Ljava/util/Date;)V
    .locals 1
    .parameter

    .prologue
    .line 94
    invoke-static {p1}, Lcom/chase/sig/android/util/s;->b(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/domain/Transaction;->deliverByDate:Ljava/lang/String;

    .line 95
    return-void
.end method

.method public final a(Z)V
    .locals 0
    .parameter

    .prologue
    .line 130
    iput-boolean p1, p0, Lcom/chase/sig/android/domain/Transaction;->editable:Z

    .line 131
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 66
    iput-object p1, p0, Lcom/chase/sig/android/domain/Transaction;->fromAccountId:Ljava/lang/String;

    .line 67
    return-void
.end method

.method public final b(Z)V
    .locals 0
    .parameter

    .prologue
    .line 138
    iput-boolean p1, p0, Lcom/chase/sig/android/domain/Transaction;->cancellable:Z

    .line 139
    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/chase/sig/android/domain/Transaction;->fromAccountId:Ljava/lang/String;

    return-object v0
.end method

.method public abstract d()Ljava/lang/String;
.end method

.method public d(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 221
    iput-object p1, p0, Lcom/chase/sig/android/domain/Transaction;->paymentToken:Ljava/lang/String;

    .line 222
    return-void
.end method

.method public e()Lcom/chase/sig/android/util/Dollar;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/chase/sig/android/domain/Transaction;->amount:Lcom/chase/sig/android/util/Dollar;

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/chase/sig/android/domain/Transaction;->paymentToken:Ljava/lang/String;

    return-object v0
.end method

.method public g(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 148
    iput-object p1, p0, Lcom/chase/sig/android/domain/Transaction;->frequency:Ljava/lang/String;

    .line 149
    return-void
.end method

.method public h(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 229
    iput-object p1, p0, Lcom/chase/sig/android/domain/Transaction;->paymentModelId:Ljava/lang/String;

    .line 230
    return-void
.end method

.method public i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/chase/sig/android/domain/Transaction;->frequency:Ljava/lang/String;

    return-object v0
.end method

.method public i(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 213
    iput-object p1, p0, Lcom/chase/sig/android/domain/Transaction;->paymentModelToken:Ljava/lang/String;

    .line 214
    return-void
.end method

.method public j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lcom/chase/sig/android/domain/Transaction;->paymentModelId:Ljava/lang/String;

    return-object v0
.end method

.method public final j(Ljava/lang/String;)V
    .locals 1
    .parameter

    .prologue
    .line 42
    invoke-static {p1}, Lcom/chase/sig/android/domain/MMBFrequency;->a(Ljava/lang/String;)Lcom/chase/sig/android/domain/MMBFrequency;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/domain/Transaction;->mmbfrequency:Lcom/chase/sig/android/domain/MMBFrequency;

    .line 43
    return-void
.end method

.method public k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/chase/sig/android/domain/Transaction;->paymentModelToken:Ljava/lang/String;

    return-object v0
.end method

.method public final k(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 58
    iput-object p1, p0, Lcom/chase/sig/android/domain/Transaction;->transactionId:Ljava/lang/String;

    .line 59
    return-void
.end method

.method public final l(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 82
    iput-object p1, p0, Lcom/chase/sig/android/domain/Transaction;->memo:Ljava/lang/String;

    .line 83
    return-void
.end method

.method public l()Z
    .locals 2

    .prologue
    .line 50
    const-string v0, "One-Time"

    iget-object v1, p0, Lcom/chase/sig/android/domain/Transaction;->frequency:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final m(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 90
    iput-object p1, p0, Lcom/chase/sig/android/domain/Transaction;->formId:Ljava/lang/String;

    .line 91
    return-void
.end method

.method public final n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/chase/sig/android/domain/Transaction;->transactionId:Ljava/lang/String;

    return-object v0
.end method

.method public final n(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 102
    iput-object p1, p0, Lcom/chase/sig/android/domain/Transaction;->deliverByDate:Ljava/lang/String;

    .line 103
    return-void
.end method

.method public final o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/chase/sig/android/domain/Transaction;->memo:Ljava/lang/String;

    return-object v0
.end method

.method public final o(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 106
    iput-object p1, p0, Lcom/chase/sig/android/domain/Transaction;->fromAccountNickName:Ljava/lang/String;

    .line 107
    return-void
.end method

.method public final p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/chase/sig/android/domain/Transaction;->formId:Ljava/lang/String;

    return-object v0
.end method

.method public final p(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 110
    iput-object p1, p0, Lcom/chase/sig/android/domain/Transaction;->fromAccountMask:Ljava/lang/String;

    .line 111
    return-void
.end method

.method public final q()Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/chase/sig/android/domain/Transaction;->deliverByDate:Ljava/lang/String;

    return-object v0
.end method

.method public final q(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 122
    iput-object p1, p0, Lcom/chase/sig/android/domain/Transaction;->status:Ljava/lang/String;

    .line 123
    return-void
.end method

.method public final r()Ljava/lang/String;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/chase/sig/android/domain/Transaction;->fromAccountNickName:Ljava/lang/String;

    return-object v0
.end method

.method public final r(Ljava/lang/String;)V
    .locals 1
    .parameter

    .prologue
    .line 156
    const-string v0, "null"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 157
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/chase/sig/android/domain/Transaction;->remainingInstances:Ljava/lang/String;

    .line 161
    :goto_0
    return-void

    .line 159
    :cond_0
    iput-object p1, p0, Lcom/chase/sig/android/domain/Transaction;->remainingInstances:Ljava/lang/String;

    goto :goto_0
.end method

.method public final s()Ljava/lang/String;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/chase/sig/android/domain/Transaction;->fromAccountMask:Ljava/lang/String;

    return-object v0
.end method

.method public final s(Ljava/lang/String;)V
    .locals 1
    .parameter

    .prologue
    .line 185
    const-string v0, "true"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/chase/sig/android/domain/Transaction;->isOpenEnded:Z

    .line 186
    return-void
.end method

.method public final t()Ljava/lang/String;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/chase/sig/android/domain/Transaction;->status:Ljava/lang/String;

    return-object v0
.end method

.method public final t(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 193
    iput-object p1, p0, Lcom/chase/sig/android/domain/Transaction;->toAccountMask:Ljava/lang/String;

    .line 194
    return-void
.end method

.method public final u(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 197
    iput-object p1, p0, Lcom/chase/sig/android/domain/Transaction;->toAccountNumber:Ljava/lang/String;

    .line 198
    return-void
.end method

.method public final u()Z
    .locals 1

    .prologue
    .line 134
    iget-boolean v0, p0, Lcom/chase/sig/android/domain/Transaction;->cancellable:Z

    return v0
.end method

.method public final v()Z
    .locals 1

    .prologue
    .line 142
    iget-boolean v0, p0, Lcom/chase/sig/android/domain/Transaction;->editable:Z

    return v0
.end method

.method public final w()Ljava/lang/String;
    .locals 1

    .prologue
    .line 164
    iget-boolean v0, p0, Lcom/chase/sig/android/domain/Transaction;->isOpenEnded:Z

    if-eqz v0, :cond_0

    .line 165
    const-string v0, "Unlimited"

    .line 167
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/domain/Transaction;->remainingInstances:Ljava/lang/String;

    goto :goto_0
.end method

.method public final x()Lcom/chase/sig/android/view/f;
    .locals 2

    .prologue
    .line 180
    new-instance v0, Lcom/chase/sig/android/view/f;

    invoke-direct {v0}, Lcom/chase/sig/android/view/f;-><init>()V

    iget-boolean v1, p0, Lcom/chase/sig/android/domain/Transaction;->isOpenEnded:Z

    iput-boolean v1, v0, Lcom/chase/sig/android/view/f;->b:Z

    iget-object v1, p0, Lcom/chase/sig/android/domain/Transaction;->remainingInstances:Ljava/lang/String;

    iput-object v1, v0, Lcom/chase/sig/android/view/f;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final y()Z
    .locals 1

    .prologue
    .line 189
    iget-boolean v0, p0, Lcom/chase/sig/android/domain/Transaction;->isOpenEnded:Z

    return v0
.end method

.method public final z()Ljava/lang/String;
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/chase/sig/android/domain/Transaction;->toAccountMask:Ljava/lang/String;

    return-object v0
.end method
