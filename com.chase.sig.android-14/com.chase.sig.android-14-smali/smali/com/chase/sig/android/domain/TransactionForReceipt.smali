.class public Lcom/chase/sig/android/domain/TransactionForReceipt;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private amount:Lcom/chase/sig/android/util/Dollar;

.field private date:Ljava/lang/String;

.field private description:Ljava/lang/String;

.field private transactionId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/chase/sig/android/domain/TransactionForReceipt;->transactionId:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/chase/sig/android/domain/TransactionForReceipt;->description:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/chase/sig/android/domain/TransactionForReceipt;->date:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Lcom/chase/sig/android/util/Dollar;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/chase/sig/android/domain/TransactionForReceipt;->amount:Lcom/chase/sig/android/util/Dollar;

    return-object v0
.end method
