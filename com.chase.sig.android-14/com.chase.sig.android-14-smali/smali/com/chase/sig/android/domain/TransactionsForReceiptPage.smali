.class public Lcom/chase/sig/android/domain/TransactionsForReceiptPage;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private end:Ljava/lang/String;

.field private more:Z

.field private transForReceipt:Ljava/util/ArrayList;
    .annotation runtime Lcom/google/gson/chase/annotations/SerializedName;
        a = "transactions"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/chase/sig/android/domain/TransactionForReceipt;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/chase/sig/android/domain/TransactionForReceipt;",
            ">;"
        }
    .end annotation

    .prologue
    .line 35
    iget-object v0, p0, Lcom/chase/sig/android/domain/TransactionsForReceiptPage;->transForReceipt:Ljava/util/ArrayList;

    return-object v0
.end method
