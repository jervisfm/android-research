.class public Lcom/chase/sig/android/domain/WireTransaction;
.super Lcom/chase/sig/android/domain/Transaction;
.source "SourceFile"


# instance fields
.field private bankMessage:Ljava/lang/String;

.field private fedReference:Ljava/lang/String;

.field private payeeId:Ljava/lang/String;

.field private payeeNickName:Ljava/lang/String;

.field private recipientMessage:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/chase/sig/android/domain/Transaction;-><init>()V

    return-void
.end method


# virtual methods
.method public final D()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/chase/sig/android/domain/WireTransaction;->payeeId:Ljava/lang/String;

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/chase/sig/android/domain/WireTransaction;->payeeNickName:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 24
    iput-object p1, p0, Lcom/chase/sig/android/domain/WireTransaction;->payeeNickName:Ljava/lang/String;

    .line 25
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/chase/sig/android/domain/WireTransaction;->fedReference:Ljava/lang/String;

    return-object v0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 32
    iput-object p1, p0, Lcom/chase/sig/android/domain/WireTransaction;->fedReference:Ljava/lang/String;

    .line 33
    return-void
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/chase/sig/android/domain/WireTransaction;->payeeNickName:Ljava/lang/String;

    return-object v0
.end method

.method public final e(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 40
    iput-object p1, p0, Lcom/chase/sig/android/domain/WireTransaction;->recipientMessage:Ljava/lang/String;

    .line 41
    return-void
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/chase/sig/android/domain/WireTransaction;->recipientMessage:Ljava/lang/String;

    return-object v0
.end method

.method public final f(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 48
    iput-object p1, p0, Lcom/chase/sig/android/domain/WireTransaction;->bankMessage:Ljava/lang/String;

    .line 49
    return-void
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/chase/sig/android/domain/WireTransaction;->bankMessage:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2

    .prologue
    .line 57
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/chase/sig/android/domain/Transaction;->fromAccountNickName:Ljava/lang/String;

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->s(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/chase/sig/android/domain/Transaction;->fromAccountMask:Ljava/lang/String;

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->s(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final v(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 65
    iput-object p1, p0, Lcom/chase/sig/android/domain/WireTransaction;->payeeId:Ljava/lang/String;

    .line 66
    return-void
.end method
