.class public final Lcom/chase/sig/android/domain/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:[Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;[Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/chase/sig/android/domain/a;->c:Ljava/lang/String;

    .line 32
    iput-object p2, p0, Lcom/chase/sig/android/domain/a;->a:Ljava/lang/String;

    .line 33
    iput-object p3, p0, Lcom/chase/sig/android/domain/a;->f:Ljava/lang/String;

    .line 34
    iput-boolean p4, p0, Lcom/chase/sig/android/domain/a;->g:Z

    .line 35
    iput-object p5, p0, Lcom/chase/sig/android/domain/a;->b:Ljava/lang/String;

    .line 36
    iput-object p6, p0, Lcom/chase/sig/android/domain/a;->e:[Ljava/lang/String;

    .line 37
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 64
    iget-object v0, p0, Lcom/chase/sig/android/domain/a;->a:Ljava/lang/String;

    const-string v1, "success"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    const/4 v0, 0x2

    .line 71
    :goto_0
    return v0

    .line 66
    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/domain/a;->a:Ljava/lang/String;

    const-string v1, "secauth"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 67
    const/4 v0, 0x4

    goto :goto_0

    .line 68
    :cond_1
    iget-object v0, p0, Lcom/chase/sig/android/domain/a;->a:Ljava/lang/String;

    const-string v1, "rsasecondcode"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 69
    const/4 v0, 0x5

    goto :goto_0

    .line 71
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 76
    invoke-virtual {p0}, Lcom/chase/sig/android/domain/a;->a()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
