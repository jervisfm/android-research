.class final Lcom/chase/sig/android/domain/j;
.super Ljava/util/HashMap;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashMap",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    .line 11
    const-string v0, "C"

    const-string v1, "Chinese"

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/domain/j;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 12
    const-string v0, "E"

    const-string v1, "English"

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/domain/j;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 13
    const-string v0, "F"

    const-string v1, "French"

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/domain/j;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 14
    const-string v0, "G"

    const-string v1, "German"

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/domain/j;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 15
    const-string v0, "H"

    const-string v1, "Greek"

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/domain/j;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 16
    const-string v0, "I"

    const-string v1, "Italian"

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/domain/j;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 17
    const-string v0, "J"

    const-string v1, "Japanese"

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/domain/j;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 18
    const-string v0, "K"

    const-string v1, "Korean"

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/domain/j;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 19
    const-string v0, "P"

    const-string v1, "Polish"

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/domain/j;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 20
    const-string v0, "Q"

    const-string v1, "Portuguese"

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/domain/j;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 21
    const-string v0, "R"

    const-string v1, "Russian"

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/domain/j;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 22
    const-string v0, "S"

    const-string v1, "Spanish"

    invoke-virtual {p0, v0, v1}, Lcom/chase/sig/android/domain/j;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 24
    return-void
.end method
