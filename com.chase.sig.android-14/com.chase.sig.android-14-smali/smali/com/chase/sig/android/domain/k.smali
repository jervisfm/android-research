.class public Lcom/chase/sig/android/domain/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/domain/g;


# static fields
.field private static final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/e;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/e;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/e;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lcom/chase/sig/android/domain/MaintenanceInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 46
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "mobile_internal_xfer"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "mobile_internal_xfer_eligible"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "mobile_paybills"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "mobile_paybills_eligible"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "mobile_quickpay"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "mobile_quickpay_eligible"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "mobile_epay"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "mobile_epay_eligible"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "mobile_wires"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "mobile_wires_eligible"

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/chase/sig/android/domain/k;->h:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(ZZ)Ljava/util/List;
    .locals 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ)",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/IAccount;",
            ">;"
        }
    .end annotation

    .prologue
    .line 206
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 207
    if-eqz p1, :cond_0

    .line 208
    iget-object v1, p0, Lcom/chase/sig/android/domain/k;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 210
    :cond_0
    if-eqz p2, :cond_1

    .line 211
    iget-object v1, p0, Lcom/chase/sig/android/domain/k;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 213
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 214
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/e;

    .line 215
    invoke-interface {v0}, Lcom/chase/sig/android/domain/e;->a()Ljava/util/ArrayList;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 217
    :cond_2
    return-object v1
.end method

.method private static a(Ljava/util/List;)Z
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/e;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 584
    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 585
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/e;

    .line 586
    invoke-interface {v0}, Lcom/chase/sig/android/domain/e;->a()Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Lcom/chase/sig/android/domain/e;->a()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 587
    const/4 v0, 0x0

    .line 591
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private f(Ljava/lang/String;)Z
    .locals 2
    .parameter

    .prologue
    .line 141
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_eligible"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/chase/sig/android/domain/k;->g(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_pending_enrollment"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/chase/sig/android/domain/k;->g(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/chase/sig/android/domain/k;->g(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 143
    :cond_0
    const/4 v0, 0x1

    .line 145
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g(Ljava/lang/String;)Z
    .locals 1
    .parameter

    .prologue
    .line 172
    iget-object v0, p0, Lcom/chase/sig/android/domain/k;->e:Ljava/util/Set;

    if-nez v0, :cond_0

    .line 173
    const/4 v0, 0x0

    .line 175
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/domain/k;->e:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method private h(Ljava/lang/String;)Ljava/util/List;
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/IAccount;",
            ">;"
        }
    .end annotation

    .prologue
    .line 231
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 234
    iget-object v0, p0, Lcom/chase/sig/android/domain/k;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/e;

    .line 235
    check-cast v0, Lcom/chase/sig/android/domain/c;

    .line 236
    invoke-virtual {v0, p1}, Lcom/chase/sig/android/domain/c;->d(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 238
    :cond_0
    return-object v1
.end method


# virtual methods
.method public final A()Z
    .locals 1

    .prologue
    .line 514
    const-string v0, "mobile_wires_agreement"

    invoke-direct {p0, v0}, Lcom/chase/sig/android/domain/k;->g(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final B()Z
    .locals 1

    .prologue
    .line 519
    const-string v0, "mobile_quickpay"

    invoke-direct {p0, v0}, Lcom/chase/sig/android/domain/k;->f(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final C()Z
    .locals 1

    .prologue
    .line 264
    const-string v0, "feature_chasereceipts"

    invoke-direct {p0, v0}, Lcom/chase/sig/android/domain/k;->g(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final D()Ljava/lang/String;
    .locals 2

    .prologue
    .line 168
    const-string v1, "logoType"

    iget-object v0, p0, Lcom/chase/sig/android/domain/k;->f:Ljava/util/Map;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/domain/k;->f:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/chase/sig/android/domain/k;->f:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_1
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final E()Lcom/chase/sig/android/domain/MaintenanceInfo;
    .locals 1

    .prologue
    .line 524
    iget-object v0, p0, Lcom/chase/sig/android/domain/k;->g:Lcom/chase/sig/android/domain/MaintenanceInfo;

    return-object v0
.end method

.method public final F()Z
    .locals 1

    .prologue
    .line 567
    const-string v0, "feature_jpm_brand"

    invoke-direct {p0, v0}, Lcom/chase/sig/android/domain/k;->g(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final G()Z
    .locals 1

    .prologue
    .line 111
    const-string v0, "mobile_internal_xfer"

    invoke-direct {p0, v0}, Lcom/chase/sig/android/domain/k;->f(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final H()Z
    .locals 1

    .prologue
    .line 116
    const-string v0, "mobile_paybills"

    invoke-direct {p0, v0}, Lcom/chase/sig/android/domain/k;->f(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final I()Z
    .locals 1

    .prologue
    .line 121
    const-string v0, "mobile_wires"

    invoke-direct {p0, v0}, Lcom/chase/sig/android/domain/k;->f(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final J()Z
    .locals 1

    .prologue
    .line 572
    iget-object v0, p0, Lcom/chase/sig/android/domain/k;->b:Ljava/util/List;

    invoke-static {v0}, Lcom/chase/sig/android/domain/k;->a(Ljava/util/List;)Z

    move-result v0

    return v0
.end method

.method public final K()Z
    .locals 1

    .prologue
    .line 577
    iget-object v0, p0, Lcom/chase/sig/android/domain/k;->c:Ljava/util/List;

    invoke-static {v0}, Lcom/chase/sig/android/domain/k;->a(Ljava/util/List;)Z

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/String;)Lcom/chase/sig/android/domain/IAccount;
    .locals 5
    .parameter

    .prologue
    .line 186
    iget-object v0, p0, Lcom/chase/sig/android/domain/k;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/e;

    .line 187
    invoke-interface {v0}, Lcom/chase/sig/android/domain/e;->a()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/IAccount;

    .line 188
    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 201
    :goto_0
    return-object v0

    .line 191
    :cond_2
    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->v()Ljava/util/List;

    move-result-object v3

    .line 192
    if-eqz v3, :cond_1

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_1

    .line 193
    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->v()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/IAccount;

    .line 194
    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    goto :goto_0

    .line 201
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/chase/sig/android/domain/IAccount;)Ljava/lang/String;
    .locals 5
    .parameter

    .prologue
    .line 456
    invoke-interface {p1}, Lcom/chase/sig/android/domain/IAccount;->g()Z

    move-result v0

    if-nez v0, :cond_0

    .line 457
    invoke-interface {p1}, Lcom/chase/sig/android/domain/IAccount;->w()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 460
    :goto_0
    return-object v0

    .line 459
    :cond_0
    invoke-interface {p1}, Lcom/chase/sig/android/domain/IAccount;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/domain/k;->e(Ljava/lang/String;)Lcom/chase/sig/android/domain/IAccount;

    move-result-object v0

    .line 460
    const-string v1, "%s (%s)"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-interface {p1}, Lcom/chase/sig/android/domain/IAccount;->a()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->c()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 81
    const-string v0, "mobile_quick_deposit"

    invoke-direct {p0, v0}, Lcom/chase/sig/android/domain/k;->g(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "mobile_quick_deposit_eligible"

    invoke-direct {p0, v0}, Lcom/chase/sig/android/domain/k;->g(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)Lcom/chase/sig/android/domain/e;
    .locals 2
    .parameter

    .prologue
    .line 329
    new-instance v0, Lcom/chase/sig/android/domain/l;

    invoke-direct {v0, p0, p1}, Lcom/chase/sig/android/domain/l;-><init>(Lcom/chase/sig/android/domain/k;Ljava/lang/String;)V

    .line 335
    new-instance v1, Lcom/chase/sig/android/util/r;

    invoke-direct {v1}, Lcom/chase/sig/android/util/r;-><init>()V

    .line 336
    iget-object v1, p0, Lcom/chase/sig/android/domain/k;->d:Ljava/util/List;

    invoke-static {v1, v0}, Lcom/chase/sig/android/util/r;->a(Ljava/util/List;Lcom/chase/sig/android/util/q;)I

    move-result v0

    .line 337
    iget-object v1, p0, Lcom/chase/sig/android/domain/k;->d:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/e;

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 489
    const-string v0, "feature_chasereceipts"

    invoke-direct {p0, v0}, Lcom/chase/sig/android/domain/k;->g(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "feature_chasereceiptsenroll"

    invoke-direct {p0, v0}, Lcom/chase/sig/android/domain/k;->g(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;)Lcom/chase/sig/android/domain/e;
    .locals 3
    .parameter

    .prologue
    .line 362
    iget-object v0, p0, Lcom/chase/sig/android/domain/k;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/e;

    .line 363
    invoke-interface {v0, p1}, Lcom/chase/sig/android/domain/e;->c(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 367
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 8

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x1

    .line 534
    const-string v5, "PPX"

    invoke-direct {p0, v2, v2}, Lcom/chase/sig/android/domain/k;->a(ZZ)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v3, v4

    move v1, v4

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/IAccount;

    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->d()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    move v1, v2

    :cond_0
    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v2

    :goto_1
    move v3, v0

    goto :goto_0

    :cond_1
    if-eqz v1, :cond_5

    if-nez v3, :cond_5

    move v0, v2

    :goto_2
    if-eqz v0, :cond_3

    sget-object v0, Lcom/chase/sig/android/domain/k;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/chase/sig/android/domain/k;->g(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v2

    :goto_3
    if-eqz v0, :cond_4

    :cond_3
    move v4, v2

    :cond_4
    return v4

    :cond_5
    move v0, v4

    goto :goto_2

    :cond_6
    move v0, v4

    goto :goto_3

    :cond_7
    move v0, v3

    goto :goto_1
.end method

.method public final d(Ljava/lang/String;)Lcom/chase/sig/android/domain/e;
    .locals 3
    .parameter

    .prologue
    .line 372
    iget-object v0, p0, Lcom/chase/sig/android/domain/k;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/e;

    .line 373
    invoke-interface {v0}, Lcom/chase/sig/android/domain/e;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 377
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/chase/sig/android/activity/eb;",
            ">;"
        }
    .end annotation

    .prologue
    .line 86
    const-string v0, "mobile_quick_deposit_eligible"

    invoke-direct {p0, v0}, Lcom/chase/sig/android/domain/k;->g(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    const-class v0, Lcom/chase/sig/android/activity/QuickDepositSignUpActivity;

    .line 92
    :goto_0
    return-object v0

    .line 89
    :cond_0
    const-string v0, "mobile_quick_deposit"

    invoke-direct {p0, v0}, Lcom/chase/sig/android/domain/k;->g(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 90
    const-class v0, Lcom/chase/sig/android/activity/QuickDepositStartActivity;

    goto :goto_0

    .line 92
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e(Ljava/lang/String;)Lcom/chase/sig/android/domain/IAccount;
    .locals 4
    .parameter

    .prologue
    .line 423
    iget-object v0, p0, Lcom/chase/sig/android/domain/k;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/e;

    .line 424
    invoke-interface {v0, p1}, Lcom/chase/sig/android/domain/e;->c(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 425
    invoke-interface {v0}, Lcom/chase/sig/android/domain/e;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 426
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/IAccount;

    .line 427
    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->i()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 433
    :goto_0
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/chase/sig/android/activity/eb;",
            ">;"
        }
    .end annotation

    .prologue
    .line 97
    const-string v0, "feature_chasereceipts"

    invoke-direct {p0, v0}, Lcom/chase/sig/android/domain/k;->g(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 98
    const-class v0, Lcom/chase/sig/android/activity/ReceiptsHomeActivity;

    .line 100
    :goto_0
    return-object v0

    :cond_0
    const-class v0, Lcom/chase/sig/android/activity/ReceiptsEnrollActivity;

    goto :goto_0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/chase/sig/android/domain/k;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 150
    const-string v0, "mobile_quickpay"

    invoke-direct {p0, v0}, Lcom/chase/sig/android/domain/k;->g(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "mobile_quickpay_pending_enrollment"

    invoke-direct {p0, v0}, Lcom/chase/sig/android/domain/k;->g(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 155
    const-string v0, "mobile_quickpay"

    invoke-direct {p0, v0}, Lcom/chase/sig/android/domain/k;->g(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final i()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/IAccount;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 222
    invoke-direct {p0, v0, v0}, Lcom/chase/sig/android/domain/k;->a(ZZ)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final j()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/IAccount;",
            ">;"
        }
    .end annotation

    .prologue
    .line 227
    const-string v0, "mobile_wires"

    invoke-direct {p0, v0}, Lcom/chase/sig/android/domain/k;->h(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 258
    const-string v0, "mobile_paybills_eligible"

    invoke-direct {p0, v0}, Lcom/chase/sig/android/domain/k;->g(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "mobile_paybills"

    invoke-direct {p0, v0}, Lcom/chase/sig/android/domain/k;->g(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 269
    const-string v0, "mobile_add_payee"

    invoke-direct {p0, v0}, Lcom/chase/sig/android/domain/k;->g(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final m()Z
    .locals 1

    .prologue
    .line 274
    const-string v0, "mobile_add_photo_payee"

    invoke-direct {p0, v0}, Lcom/chase/sig/android/domain/k;->g(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final n()Z
    .locals 1

    .prologue
    .line 279
    const-string v0, "mobile_epay"

    invoke-direct {p0, v0}, Lcom/chase/sig/android/domain/k;->f(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final o()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/e;",
            ">;"
        }
    .end annotation

    .prologue
    .line 304
    iget-object v0, p0, Lcom/chase/sig/android/domain/k;->c:Ljava/util/List;

    return-object v0
.end method

.method public final p()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/IAccount;",
            ">;"
        }
    .end annotation

    .prologue
    .line 319
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/chase/sig/android/domain/k;->a(ZZ)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final q()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 342
    invoke-direct {p0, v1, v0}, Lcom/chase/sig/android/domain/k;->a(ZZ)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final r()Z
    .locals 1

    .prologue
    .line 352
    iget-object v0, p0, Lcom/chase/sig/android/domain/k;->c:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/domain/k;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final s()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 357
    invoke-direct {p0, v0, v1}, Lcom/chase/sig/android/domain/k;->a(ZZ)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    move v2, v0

    :goto_0
    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/k;->q()Z

    move-result v2

    if-eqz v2, :cond_1

    :goto_1
    return v0

    :cond_0
    move v2, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final t()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/IAccount;",
            ">;"
        }
    .end annotation

    .prologue
    .line 382
    const-string v0, "mobile_paybills"

    invoke-direct {p0, v0}, Lcom/chase/sig/android/domain/k;->h(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 494
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 495
    const-string v1, "AuthorizedProfile [type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 496
    iget-object v1, p0, Lcom/chase/sig/android/domain/k;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 497
    const-string v1, ", personalCustomers="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 498
    iget-object v1, p0, Lcom/chase/sig/android/domain/k;->b:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 499
    const-string v1, ", nonPersonalCustomers="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 500
    iget-object v1, p0, Lcom/chase/sig/android/domain/k;->c:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 501
    const-string v1, ", privileges="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 502
    iget-object v1, p0, Lcom/chase/sig/android/domain/k;->e:Ljava/util/Set;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 503
    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 504
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final u()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/IAccount;",
            ">;"
        }
    .end annotation

    .prologue
    .line 387
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 388
    const-string v0, "mobile_epay"

    invoke-direct {p0, v0}, Lcom/chase/sig/android/domain/k;->h(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 390
    iget-object v0, p0, Lcom/chase/sig/android/domain/k;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/e;

    .line 391
    invoke-interface {v0}, Lcom/chase/sig/android/domain/e;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 392
    if-eqz v0, :cond_0

    .line 393
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/IAccount;

    .line 394
    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->v()Ljava/util/List;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 395
    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->v()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/IAccount;

    .line 396
    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->J()Ljava/util/Set;

    move-result-object v5

    const-string v6, "mobile_epay"

    invoke-interface {v5, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 397
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 404
    :cond_3
    return-object v1
.end method

.method public final v()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/IAccount;",
            ">;"
        }
    .end annotation

    .prologue
    .line 409
    const-string v0, "mobile_internal_xfer_credit"

    invoke-direct {p0, v0}, Lcom/chase/sig/android/domain/k;->h(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final w()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/IAccount;",
            ">;"
        }
    .end annotation

    .prologue
    .line 413
    const-string v0, "feature_chasereceipts"

    invoke-direct {p0, v0}, Lcom/chase/sig/android/domain/k;->h(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final x()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/IAccount;",
            ">;"
        }
    .end annotation

    .prologue
    .line 418
    const-string v0, "mobile_internal_xfer_debit"

    invoke-direct {p0, v0}, Lcom/chase/sig/android/domain/k;->h(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final y()Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/IAccount;",
            ">;"
        }
    .end annotation

    .prologue
    .line 438
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 439
    iget-object v0, p0, Lcom/chase/sig/android/domain/k;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/e;

    .line 440
    invoke-interface {v0}, Lcom/chase/sig/android/domain/e;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 441
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/IAccount;

    .line 443
    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->d()Ljava/lang/String;

    move-result-object v5

    move-object v1, v0

    .line 444
    check-cast v1, Lcom/chase/sig/android/domain/Account;

    .line 445
    const-string v6, "mobile_internal_xfer_credit"

    invoke-virtual {v1, v6}, Lcom/chase/sig/android/domain/Account;->f(Ljava/lang/String;)Z

    move-result v1

    .line 446
    const-string v6, "HEL"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    const-string v6, "HEO"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    :cond_2
    if-nez v1, :cond_1

    .line 447
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 451
    :cond_3
    return-object v2
.end method

.method public final z()Z
    .locals 1

    .prologue
    .line 509
    const-string v0, "feature_eotm"

    invoke-direct {p0, v0}, Lcom/chase/sig/android/domain/k;->g(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
