.class public final Lcom/chase/sig/android/domain/o;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Lcom/chase/sig/android/domain/a;

.field public b:Lcom/chase/sig/android/domain/g;

.field public c:Lcom/chase/sig/android/service/quickpay/TodoListResponse;

.field public d:Lcom/chase/sig/android/service/SplashResponse;

.field private e:Lcom/chase/sig/android/domain/CacheActivityData;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/chase/sig/android/domain/a;Lcom/chase/sig/android/domain/g;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/chase/sig/android/domain/o;->a:Lcom/chase/sig/android/domain/a;

    .line 28
    iput-object p2, p0, Lcom/chase/sig/android/domain/o;->b:Lcom/chase/sig/android/domain/g;

    .line 29
    invoke-direct {p0}, Lcom/chase/sig/android/domain/o;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 30
    invoke-interface {p2}, Lcom/chase/sig/android/domain/g;->E()Lcom/chase/sig/android/domain/MaintenanceInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/MaintenanceInfo;->b()Ljava/util/ArrayList;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/chase/sig/android/domain/o;->a(Ljava/util/ArrayList;)V

    .line 32
    :cond_0
    return-void
.end method

.method private a(Ljava/util/ArrayList;)V
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/chase/sig/android/domain/SplashInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 84
    new-instance v0, Lcom/chase/sig/android/service/SplashResponse;

    invoke-direct {v0, p1}, Lcom/chase/sig/android/service/SplashResponse;-><init>(Ljava/util/ArrayList;)V

    iput-object v0, p0, Lcom/chase/sig/android/domain/o;->d:Lcom/chase/sig/android/service/SplashResponse;

    .line 85
    return-void
.end method

.method private c()Z
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/chase/sig/android/domain/o;->b:Lcom/chase/sig/android/domain/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/domain/o;->b:Lcom/chase/sig/android/domain/g;

    invoke-interface {v0}, Lcom/chase/sig/android/domain/g;->E()Lcom/chase/sig/android/domain/MaintenanceInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/chase/sig/android/service/ProfileResponse;
    .locals 2

    .prologue
    .line 51
    new-instance v0, Lcom/chase/sig/android/service/n;

    invoke-direct {v0}, Lcom/chase/sig/android/service/n;-><init>()V

    .line 52
    invoke-virtual {v0}, Lcom/chase/sig/android/service/n;->c()Lcom/chase/sig/android/service/w;

    .line 54
    iget-object v0, p0, Lcom/chase/sig/android/domain/o;->a:Lcom/chase/sig/android/domain/a;

    invoke-static {}, Lcom/chase/sig/android/service/w;->a()Lcom/chase/sig/android/service/ProfileResponse;

    move-result-object v0

    .line 55
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/chase/sig/android/service/ProfileResponse;->a()Lcom/chase/sig/android/domain/g;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/chase/sig/android/service/ProfileResponse;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 64
    :cond_0
    :goto_0
    return-object v0

    .line 58
    :cond_1
    invoke-virtual {v0}, Lcom/chase/sig/android/service/ProfileResponse;->a()Lcom/chase/sig/android/domain/g;

    move-result-object v1

    iput-object v1, p0, Lcom/chase/sig/android/domain/o;->b:Lcom/chase/sig/android/domain/g;

    .line 59
    invoke-direct {p0}, Lcom/chase/sig/android/domain/o;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 60
    iget-object v1, p0, Lcom/chase/sig/android/domain/o;->b:Lcom/chase/sig/android/domain/g;

    invoke-interface {v1}, Lcom/chase/sig/android/domain/g;->E()Lcom/chase/sig/android/domain/MaintenanceInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/MaintenanceInfo;->b()Ljava/util/ArrayList;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/chase/sig/android/domain/o;->a(Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method public final b()Lcom/chase/sig/android/domain/CacheActivityData;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/chase/sig/android/domain/o;->e:Lcom/chase/sig/android/domain/CacheActivityData;

    if-nez v0, :cond_0

    .line 94
    new-instance v0, Lcom/chase/sig/android/domain/CacheActivityData;

    invoke-direct {v0}, Lcom/chase/sig/android/domain/CacheActivityData;-><init>()V

    iput-object v0, p0, Lcom/chase/sig/android/domain/o;->e:Lcom/chase/sig/android/domain/CacheActivityData;

    .line 97
    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/domain/o;->e:Lcom/chase/sig/android/domain/CacheActivityData;

    return-object v0
.end method
