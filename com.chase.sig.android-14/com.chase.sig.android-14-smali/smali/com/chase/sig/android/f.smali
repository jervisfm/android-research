.class public final Lcom/chase/sig/android/f;
.super Ljava/lang/Thread;
.source "SourceFile"


# static fields
.field private static a:Ljava/util/Calendar;


# instance fields
.field private b:Lcom/chase/sig/android/activity/eb;


# direct methods
.method public constructor <init>(Lcom/chase/sig/android/activity/eb;)V
    .locals 0
    .parameter

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/chase/sig/android/f;->b:Lcom/chase/sig/android/activity/eb;

    .line 21
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/f;)Lcom/chase/sig/android/activity/eb;
    .locals 1
    .parameter

    .prologue
    .line 7
    iget-object v0, p0, Lcom/chase/sig/android/f;->b:Lcom/chase/sig/android/activity/eb;

    return-object v0
.end method

.method public static a()V
    .locals 1

    .prologue
    .line 75
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    sput-object v0, Lcom/chase/sig/android/f;->a:Ljava/util/Calendar;

    .line 76
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 25
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v2

    .line 30
    :goto_0
    :try_start_0
    invoke-virtual {v2}, Lcom/chase/sig/android/ChaseApplication;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/f;->b:Lcom/chase/sig/android/activity/eb;

    invoke-virtual {v0}, Lcom/chase/sig/android/activity/eb;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 32
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    .line 34
    sget-object v0, Lcom/chase/sig/android/f;->a:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    .line 35
    const/16 v1, 0xd

    const/16 v4, 0x28a

    invoke-virtual {v0, v1, v4}, Ljava/util/Calendar;->add(II)V

    .line 37
    sget-object v1, Lcom/chase/sig/android/f;->a:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Calendar;

    .line 38
    const/16 v4, 0xd

    const/16 v5, 0x2c6

    invoke-virtual {v1, v4, v5}, Ljava/util/Calendar;->add(II)V

    .line 40
    invoke-virtual {v3, v1}, Ljava/util/Calendar;->after(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 42
    iget-object v0, p0, Lcom/chase/sig/android/f;->b:Lcom/chase/sig/android/activity/eb;

    new-instance v1, Lcom/chase/sig/android/g;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/g;-><init>(Lcom/chase/sig/android/f;)V

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/eb;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 62
    :cond_0
    :goto_1
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v0

    if-nez v0, :cond_1

    .line 63
    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 67
    const-wide/16 v0, 0x1388

    :try_start_1
    invoke-virtual {p0, v0, v1}, Ljava/lang/Object;->wait(J)V

    .line 68
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit p0

    throw v0

    .line 72
    :catch_0
    move-exception v0

    :cond_1
    return-void

    .line 51
    :cond_2
    invoke-virtual {v3, v0}, Ljava/util/Calendar;->after(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lcom/chase/sig/android/f;->b:Lcom/chase/sig/android/activity/eb;

    new-instance v1, Lcom/chase/sig/android/h;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/h;-><init>(Lcom/chase/sig/android/f;)V

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/activity/eb;->runOnUiThread(Ljava/lang/Runnable;)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1
.end method
