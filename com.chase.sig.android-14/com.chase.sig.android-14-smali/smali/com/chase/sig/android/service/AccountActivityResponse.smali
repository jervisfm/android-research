.class public Lcom/chase/sig/android/service/AccountActivityResponse;
.super Lcom/chase/sig/android/service/l;
.source "SourceFile"


# instance fields
.field private activity:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/AccountActivity;",
            ">;"
        }
    .end annotation
.end field

.field private end:Ljava/lang/String;

.field private hasMore:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lcom/chase/sig/android/service/l;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/AccountActivity;",
            ">;"
        }
    .end annotation

    .prologue
    .line 14
    iget-object v0, p0, Lcom/chase/sig/android/service/AccountActivityResponse;->activity:Ljava/util/List;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 34
    iput-object p1, p0, Lcom/chase/sig/android/service/AccountActivityResponse;->end:Ljava/lang/String;

    .line 35
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/AccountActivity;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 18
    iput-object p1, p0, Lcom/chase/sig/android/service/AccountActivityResponse;->activity:Ljava/util/List;

    .line 19
    return-void
.end method

.method public final a(Z)V
    .locals 0
    .parameter

    .prologue
    .line 26
    iput-boolean p1, p0, Lcom/chase/sig/android/service/AccountActivityResponse;->hasMore:Z

    .line 27
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 22
    iget-boolean v0, p0, Lcom/chase/sig/android/service/AccountActivityResponse;->hasMore:Z

    return v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/chase/sig/android/service/AccountActivityResponse;->end:Ljava/lang/String;

    return-object v0
.end method
