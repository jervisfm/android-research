.class public Lcom/chase/sig/android/service/AccountRewardsDetailResponse;
.super Lcom/chase/sig/android/service/l;
.source "SourceFile"


# instance fields
.field private accountRewardsDetail:Lcom/chase/sig/android/domain/AccountRewardsDetail;

.field private accountRewardsDetailList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/AccountRewardsDetail;",
            ">;"
        }
    .end annotation
.end field

.field private footNote:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lcom/chase/sig/android/service/l;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/chase/sig/android/domain/AccountRewardsDetail;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/chase/sig/android/service/AccountRewardsDetailResponse;->accountRewardsDetail:Lcom/chase/sig/android/domain/AccountRewardsDetail;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 34
    iput-object p1, p0, Lcom/chase/sig/android/service/AccountRewardsDetailResponse;->footNote:Ljava/lang/String;

    .line 35
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/AccountRewardsDetail;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 22
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 23
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/chase/sig/android/service/AccountRewardsDetailResponse;->accountRewardsDetailList:Ljava/util/List;

    .line 27
    :goto_0
    return-void

    .line 25
    :cond_0
    iput-object p1, p0, Lcom/chase/sig/android/service/AccountRewardsDetailResponse;->accountRewardsDetailList:Ljava/util/List;

    goto :goto_0
.end method

.method public final b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/AccountRewardsDetail;",
            ">;"
        }
    .end annotation

    .prologue
    .line 30
    iget-object v0, p0, Lcom/chase/sig/android/service/AccountRewardsDetailResponse;->accountRewardsDetailList:Ljava/util/List;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/chase/sig/android/service/AccountRewardsDetailResponse;->footNote:Ljava/lang/String;

    return-object v0
.end method
