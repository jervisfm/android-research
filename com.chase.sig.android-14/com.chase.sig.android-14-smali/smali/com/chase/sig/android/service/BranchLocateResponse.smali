.class public Lcom/chase/sig/android/service/BranchLocateResponse;
.super Lcom/chase/sig/android/service/l;
.source "SourceFile"


# instance fields
.field private locations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/BranchLocation;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lcom/chase/sig/android/service/l;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/BranchLocation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 16
    iget-object v0, p0, Lcom/chase/sig/android/service/BranchLocateResponse;->locations:Ljava/util/List;

    return-object v0
.end method

.method public final a(Ljava/util/List;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/BranchLocation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 12
    iput-object p1, p0, Lcom/chase/sig/android/service/BranchLocateResponse;->locations:Ljava/util/List;

    .line 13
    return-void
.end method
