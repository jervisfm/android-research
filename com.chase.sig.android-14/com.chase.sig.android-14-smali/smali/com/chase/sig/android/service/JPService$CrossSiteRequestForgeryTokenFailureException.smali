.class public Lcom/chase/sig/android/service/JPService$CrossSiteRequestForgeryTokenFailureException;
.super Lcom/chase/sig/android/util/ChaseException;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/service/JPService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CrossSiteRequestForgeryTokenFailureException"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 221
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "Forbidden HTTP response received"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    const-string v1, ""

    invoke-direct {p0, v0, v1}, Lcom/chase/sig/android/util/ChaseException;-><init>(Ljava/lang/Exception;Ljava/lang/String;)V

    .line 222
    return-void
.end method
