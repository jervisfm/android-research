.class public final Lcom/chase/sig/android/service/JPService$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/service/JPService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# static fields
.field private static a:Lcom/google/gson/chase/Gson;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 103
    new-instance v0, Lcom/google/gson/chase/GsonBuilder;

    invoke-direct {v0}, Lcom/google/gson/chase/GsonBuilder;-><init>()V

    const-class v1, Lcom/chase/sig/android/util/Dollar;

    new-instance v2, Lcom/chase/sig/android/domain/d;

    invoke-direct {v2}, Lcom/chase/sig/android/domain/d;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/gson/chase/GsonBuilder;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/chase/GsonBuilder;

    move-result-object v0

    const-class v1, Lcom/chase/sig/android/service/IServiceError;

    new-instance v2, Lcom/chase/sig/android/service/ae;

    invoke-direct {v2}, Lcom/chase/sig/android/service/ae;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/gson/chase/GsonBuilder;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/chase/GsonBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/gson/chase/GsonBuilder;->b()Lcom/google/gson/chase/Gson;

    move-result-object v0

    sput-object v0, Lcom/chase/sig/android/service/JPService$a;->a:Lcom/google/gson/chase/Gson;

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 114
    sget-object v0, Lcom/chase/sig/android/service/JPService$a;->a:Lcom/google/gson/chase/Gson;

    invoke-virtual {v0, p0, p1}, Lcom/google/gson/chase/Gson;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 118
    sget-object v0, Lcom/chase/sig/android/service/JPService$a;->a:Lcom/google/gson/chase/Gson;

    invoke-virtual {v0, p0}, Lcom/google/gson/chase/Gson;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
