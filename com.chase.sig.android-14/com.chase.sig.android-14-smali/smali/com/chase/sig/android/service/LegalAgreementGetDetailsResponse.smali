.class public Lcom/chase/sig/android/service/LegalAgreementGetDetailsResponse;
.super Lcom/chase/sig/android/service/l;
.source "SourceFile"


# instance fields
.field private code:Ljava/lang/String;

.field private content:Ljava/lang/String;

.field private status:Ljava/lang/String;

.field private version:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0}, Lcom/chase/sig/android/service/l;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/chase/sig/android/service/LegalAgreementGetDetailsResponse;->content:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 16
    iput-object p1, p0, Lcom/chase/sig/android/service/LegalAgreementGetDetailsResponse;->code:Ljava/lang/String;

    .line 17
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 24
    iput-object p1, p0, Lcom/chase/sig/android/service/LegalAgreementGetDetailsResponse;->status:Ljava/lang/String;

    .line 25
    return-void
.end method

.method public final e(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 32
    iput-object p1, p0, Lcom/chase/sig/android/service/LegalAgreementGetDetailsResponse;->version:Ljava/lang/String;

    .line 33
    return-void
.end method

.method public final f(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 40
    iput-object p1, p0, Lcom/chase/sig/android/service/LegalAgreementGetDetailsResponse;->content:Ljava/lang/String;

    .line 41
    return-void
.end method
