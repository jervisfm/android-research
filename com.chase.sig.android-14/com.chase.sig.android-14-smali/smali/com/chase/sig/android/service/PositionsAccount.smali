.class public Lcom/chase/sig/android/service/PositionsAccount;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private accountUnrealizedChange:Ljava/lang/String;

.field private accountValue:Ljava/lang/String;

.field private asOfDate:Ljava/lang/String;

.field private displayName:Ljava/lang/String;

.field private marketValue:Lcom/chase/sig/android/util/Dollar;

.field private marketValueChange:Lcom/chase/sig/android/util/Dollar;

.field private positions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/Position;",
            ">;"
        }
    .end annotation
.end field

.field private sortedPositions:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Lcom/chase/sig/android/domain/PositionType;",
            "Ljava/util/TreeSet",
            "<",
            "Lcom/chase/sig/android/domain/Position;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/chase/sig/android/service/PositionsAccount;->positions:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/Position;",
            ">;"
        }
    .end annotation

    .prologue
    .line 29
    iget-object v0, p0, Lcom/chase/sig/android/service/PositionsAccount;->positions:Ljava/util/List;

    return-object v0
.end method

.method public final a(Lcom/chase/sig/android/util/Dollar;)V
    .locals 0
    .parameter

    .prologue
    .line 86
    iput-object p1, p0, Lcom/chase/sig/android/service/PositionsAccount;->marketValue:Lcom/chase/sig/android/util/Dollar;

    .line 87
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 50
    iput-object p1, p0, Lcom/chase/sig/android/service/PositionsAccount;->displayName:Ljava/lang/String;

    .line 51
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 6
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/Position;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 33
    iput-object p1, p0, Lcom/chase/sig/android/service/PositionsAccount;->positions:Ljava/util/List;

    .line 34
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/chase/sig/android/service/PositionsAccount;->sortedPositions:Ljava/util/LinkedHashMap;

    .line 36
    invoke-static {}, Lcom/chase/sig/android/domain/PositionType;->values()[Lcom/chase/sig/android/domain/PositionType;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 37
    iget-object v4, p0, Lcom/chase/sig/android/service/PositionsAccount;->sortedPositions:Ljava/util/LinkedHashMap;

    new-instance v5, Ljava/util/TreeSet;

    invoke-direct {v5}, Ljava/util/TreeSet;-><init>()V

    invoke-virtual {v4, v3, v5}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 40
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/chase/sig/android/domain/Position;

    .line 41
    iget-object v0, p0, Lcom/chase/sig/android/service/PositionsAccount;->sortedPositions:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/Position;->l()Lcom/chase/sig/android/domain/PositionType;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/TreeSet;

    invoke-virtual {v0, v1}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 43
    :cond_1
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/chase/sig/android/service/PositionsAccount;->displayName:Ljava/lang/String;

    return-object v0
.end method

.method public final b(Lcom/chase/sig/android/util/Dollar;)V
    .locals 0
    .parameter

    .prologue
    .line 94
    iput-object p1, p0, Lcom/chase/sig/android/service/PositionsAccount;->marketValueChange:Lcom/chase/sig/android/util/Dollar;

    .line 95
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 58
    iput-object p1, p0, Lcom/chase/sig/android/service/PositionsAccount;->asOfDate:Ljava/lang/String;

    .line 59
    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/chase/sig/android/service/PositionsAccount;->asOfDate:Ljava/lang/String;

    return-object v0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 66
    iput-object p1, p0, Lcom/chase/sig/android/service/PositionsAccount;->accountUnrealizedChange:Ljava/lang/String;

    .line 67
    return-void
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/chase/sig/android/service/PositionsAccount;->accountUnrealizedChange:Ljava/lang/String;

    return-object v0
.end method

.method public final d(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 74
    iput-object p1, p0, Lcom/chase/sig/android/service/PositionsAccount;->accountValue:Ljava/lang/String;

    .line 75
    return-void
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/chase/sig/android/service/PositionsAccount;->accountValue:Ljava/lang/String;

    return-object v0
.end method

.method public final f()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Lcom/chase/sig/android/domain/PositionType;",
            "+",
            "Ljava/util/Set",
            "<",
            "Lcom/chase/sig/android/domain/Position;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 78
    iget-object v0, p0, Lcom/chase/sig/android/service/PositionsAccount;->sortedPositions:Ljava/util/LinkedHashMap;

    return-object v0
.end method

.method public final g()Lcom/chase/sig/android/util/Dollar;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/chase/sig/android/service/PositionsAccount;->marketValueChange:Lcom/chase/sig/android/util/Dollar;

    return-object v0
.end method
