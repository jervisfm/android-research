.class public Lcom/chase/sig/android/service/ReceiptFilter;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/service/ReceiptFilter$DateFilter;
    }
.end annotation


# instance fields
.field private dateFilter:Ljava/lang/Object;

.field private filterKeys:Ljava/util/Set;
    .annotation runtime Lcom/google/gson/chase/annotations/SerializedName;
        a = "filters"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 25
    iget-object v0, p0, Lcom/chase/sig/android/service/ReceiptFilter;->filterKeys:Ljava/util/Set;

    if-nez v0, :cond_0

    .line 26
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/chase/sig/android/service/ReceiptFilter;->filterKeys:Ljava/util/Set;

    .line 29
    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/service/ReceiptFilter;->filterKeys:Ljava/util/Set;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1
    .parameter

    .prologue
    .line 17
    invoke-virtual {p0}, Lcom/chase/sig/android/service/ReceiptFilter;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 18
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 41
    iget-object v0, p0, Lcom/chase/sig/android/service/ReceiptFilter;->dateFilter:Ljava/lang/Object;

    if-nez v0, :cond_0

    .line 42
    new-instance v0, Lcom/chase/sig/android/service/ReceiptFilter$DateFilter;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/service/ReceiptFilter$DateFilter;-><init>(Lcom/chase/sig/android/service/ReceiptFilter;)V

    iput-object v0, p0, Lcom/chase/sig/android/service/ReceiptFilter;->dateFilter:Ljava/lang/Object;

    .line 45
    :cond_0
    new-instance v0, Lcom/chase/sig/android/service/ReceiptFilter$DateFilter;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/service/ReceiptFilter$DateFilter;-><init>(Lcom/chase/sig/android/service/ReceiptFilter;)V

    .line 46
    invoke-virtual {v0, p1}, Lcom/chase/sig/android/service/ReceiptFilter$DateFilter;->a(Ljava/lang/String;)V

    .line 47
    invoke-virtual {v0, p2}, Lcom/chase/sig/android/service/ReceiptFilter$DateFilter;->b(Ljava/lang/String;)V

    .line 49
    iput-object v0, p0, Lcom/chase/sig/android/service/ReceiptFilter;->dateFilter:Ljava/lang/Object;

    .line 50
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/chase/sig/android/service/ReceiptFilter;->filterKeys:Ljava/util/Set;

    .line 79
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1
    .parameter

    .prologue
    .line 21
    invoke-virtual {p0}, Lcom/chase/sig/android/service/ReceiptFilter;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 22
    return-void
.end method

.method public final c()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/chase/sig/android/service/ReceiptFilter;->dateFilter:Ljava/lang/Object;

    return-object v0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 1
    .parameter

    .prologue
    .line 33
    iget-object v0, p0, Lcom/chase/sig/android/service/ReceiptFilter;->dateFilter:Ljava/lang/Object;

    if-nez v0, :cond_0

    .line 34
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    iput-object v0, p0, Lcom/chase/sig/android/service/ReceiptFilter;->dateFilter:Ljava/lang/Object;

    .line 37
    :cond_0
    iput-object p1, p0, Lcom/chase/sig/android/service/ReceiptFilter;->dateFilter:Ljava/lang/Object;

    .line 38
    return-void
.end method
