.class public Lcom/chase/sig/android/service/ReceiptListResponse;
.super Lcom/chase/sig/android/service/l;
.source "SourceFile"


# instance fields
.field private receiptCollection:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/chase/sig/android/domain/Receipt;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/chase/sig/android/service/l;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/Receipt;",
            ">;"
        }
    .end annotation

    .prologue
    .line 15
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 16
    iget-object v0, p0, Lcom/chase/sig/android/service/ReceiptListResponse;->receiptCollection:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/Receipt;

    .line 18
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Receipt;->j()[Ljava/lang/String;

    move-result-object v4

    .line 19
    new-instance v5, Lcom/chase/sig/android/domain/ReceiptPhotoList;

    invoke-direct {v5}, Lcom/chase/sig/android/domain/ReceiptPhotoList;-><init>()V

    .line 20
    array-length v6, v4

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v6, :cond_0

    aget-object v7, v4, v1

    .line 21
    invoke-virtual {v5, v7}, Lcom/chase/sig/android/domain/ReceiptPhotoList;->a(Ljava/lang/String;)V

    .line 20
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 24
    :cond_0
    invoke-virtual {v0, v5}, Lcom/chase/sig/android/domain/Receipt;->a(Lcom/chase/sig/android/domain/ReceiptPhotoList;)V

    .line 25
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 27
    :cond_1
    return-object v2
.end method
