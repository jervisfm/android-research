.class public Lcom/chase/sig/android/service/ReceiptsEnrollmentResponse;
.super Lcom/chase/sig/android/service/GenericResponse;
.source "SourceFile"


# instance fields
.field agreement:Lcom/chase/sig/android/domain/Agreement;

.field enrollmentForm:Lcom/chase/sig/android/domain/EnrollmentForm;

.field successMsg:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lcom/chase/sig/android/service/GenericResponse;-><init>()V

    return-void
.end method


# virtual methods
.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/chase/sig/android/service/ReceiptsEnrollmentResponse;->successMsg:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Lcom/chase/sig/android/domain/EnrollmentForm;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/chase/sig/android/service/ReceiptsEnrollmentResponse;->enrollmentForm:Lcom/chase/sig/android/domain/EnrollmentForm;

    return-object v0
.end method

.method public final d()Lcom/chase/sig/android/domain/Agreement;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/chase/sig/android/service/ReceiptsEnrollmentResponse;->agreement:Lcom/chase/sig/android/domain/Agreement;

    return-object v0
.end method
