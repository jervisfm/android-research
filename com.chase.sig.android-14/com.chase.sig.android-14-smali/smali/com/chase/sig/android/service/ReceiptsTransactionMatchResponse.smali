.class public Lcom/chase/sig/android/service/ReceiptsTransactionMatchResponse;
.super Lcom/chase/sig/android/service/GenericResponse;
.source "SourceFile"


# instance fields
.field private acctActivityForReceipt:Lcom/chase/sig/android/domain/AccountActivityForReceipt;
    .annotation runtime Lcom/google/gson/chase/annotations/SerializedName;
        a = "accountActivityRes"
    .end annotation
.end field

.field private emptyListMessage:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/chase/annotations/SerializedName;
        a = "contentEmptyMessage"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Lcom/chase/sig/android/service/GenericResponse;-><init>()V

    return-void
.end method


# virtual methods
.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/chase/sig/android/service/ReceiptsTransactionMatchResponse;->emptyListMessage:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Lcom/chase/sig/android/domain/AccountActivityForReceipt;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/chase/sig/android/service/ReceiptsTransactionMatchResponse;->acctActivityForReceipt:Lcom/chase/sig/android/domain/AccountActivityForReceipt;

    return-object v0
.end method
