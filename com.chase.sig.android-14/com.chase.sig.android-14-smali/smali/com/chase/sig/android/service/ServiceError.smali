.class public Lcom/chase/sig/android/service/ServiceError;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/chase/sig/android/service/IServiceError;


# instance fields
.field private code:Ljava/lang/String;

.field private errorAttributes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/service/ServiceErrorAttribute;",
            ">;"
        }
    .end annotation
.end field

.field private isFatal:Z

.field private message:Ljava/lang/String;

.field private title:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Z)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/service/ServiceErrorAttribute;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0, p1, p2, p4}, Lcom/chase/sig/android/service/ServiceError;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 31
    iput-object p3, p0, Lcom/chase/sig/android/service/ServiceError;->errorAttributes:Ljava/util/List;

    .line 32
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/chase/sig/android/service/ServiceError;->errorAttributes:Ljava/util/List;

    .line 14
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/chase/sig/android/service/ServiceError;->isFatal:Z

    .line 23
    iput-object p1, p0, Lcom/chase/sig/android/service/ServiceError;->code:Ljava/lang/String;

    .line 24
    iput-object p2, p0, Lcom/chase/sig/android/service/ServiceError;->message:Ljava/lang/String;

    .line 25
    iput-boolean p3, p0, Lcom/chase/sig/android/service/ServiceError;->isFatal:Z

    .line 26
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/chase/sig/android/service/ServiceError;->errorAttributes:Ljava/util/List;

    .line 14
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/chase/sig/android/service/ServiceError;->isFatal:Z

    .line 17
    const-string v0, "LOCAL"

    iput-object v0, p0, Lcom/chase/sig/android/service/ServiceError;->code:Ljava/lang/String;

    .line 18
    iput-object p1, p0, Lcom/chase/sig/android/service/ServiceError;->message:Ljava/lang/String;

    .line 19
    iput-boolean p2, p0, Lcom/chase/sig/android/service/ServiceError;->isFatal:Z

    .line 20
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/chase/sig/android/service/ServiceError;->message:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/chase/sig/android/service/ServiceError;->title:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/chase/sig/android/service/ServiceError;->code:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 47
    iget-object v0, p0, Lcom/chase/sig/android/service/ServiceError;->code:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 52
    if-eqz v0, :cond_1

    const-string v1, "10047"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "70033"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "50532"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 57
    :cond_0
    const/4 v0, 0x0

    .line 60
    :goto_0
    return v0

    :cond_1
    iget-boolean v0, p0, Lcom/chase/sig/android/service/ServiceError;->isFatal:Z

    goto :goto_0
.end method

.method public final e()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/service/ServiceErrorAttribute;",
            ">;"
        }
    .end annotation

    .prologue
    .line 65
    iget-object v0, p0, Lcom/chase/sig/android/service/ServiceError;->errorAttributes:Ljava/util/List;

    return-object v0
.end method
