.class public Lcom/chase/sig/android/service/ServiceErrorAttribute;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private final key:Ljava/lang/String;

.field private final value:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/chase/sig/android/service/ServiceErrorAttribute;->key:Ljava/lang/String;

    .line 15
    iput-object p2, p0, Lcom/chase/sig/android/service/ServiceErrorAttribute;->value:Ljava/lang/String;

    .line 16
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/chase/sig/android/service/ServiceErrorAttribute;->key:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 27
    iget-object v0, p0, Lcom/chase/sig/android/service/ServiceErrorAttribute;->value:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 28
    :goto_0
    return v0

    :cond_0
    const-string v0, "invalid"

    iget-object v1, p0, Lcom/chase/sig/android/service/ServiceErrorAttribute;->value:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method
