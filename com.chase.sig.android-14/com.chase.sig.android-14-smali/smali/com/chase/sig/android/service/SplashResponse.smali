.class public Lcom/chase/sig/android/service/SplashResponse;
.super Lcom/chase/sig/android/service/l;
.source "SourceFile"


# instance fields
.field private alertsManagementBlocked:Z

.field private alertsManagementMessage:Ljava/lang/String;

.field private billPayBlocked:Z

.field private billPayMessage:Ljava/lang/String;

.field private ePayBlocked:Z

.field private ePayMessage:Ljava/lang/String;

.field private isSplashEnabled:Z

.field private photoBillPayBlocked:Z

.field private photoBillPayMessage:Ljava/lang/String;

.field private quickDepositBlocked:Z

.field private quickDepositMessage:Ljava/lang/String;

.field private quickPayBlocked:Z

.field private quickPayMessage:Ljava/lang/String;

.field private receiptBlocked:Z

.field private receiptMessage:Ljava/lang/String;

.field private splashUrl:Ljava/lang/String;

.field private transferBlocked:Z

.field private transferMessage:Ljava/lang/String;

.field private url:Ljava/lang/String;

.field private wiresBlocked:Z

.field private wiresMessage:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 85
    invoke-direct {p0}, Lcom/chase/sig/android/service/l;-><init>()V

    .line 20
    iput-boolean v0, p0, Lcom/chase/sig/android/service/SplashResponse;->billPayBlocked:Z

    .line 21
    iput-boolean v0, p0, Lcom/chase/sig/android/service/SplashResponse;->ePayBlocked:Z

    .line 22
    iput-boolean v0, p0, Lcom/chase/sig/android/service/SplashResponse;->transferBlocked:Z

    .line 23
    iput-boolean v0, p0, Lcom/chase/sig/android/service/SplashResponse;->wiresBlocked:Z

    .line 24
    iput-boolean v0, p0, Lcom/chase/sig/android/service/SplashResponse;->quickDepositBlocked:Z

    .line 25
    iput-boolean v0, p0, Lcom/chase/sig/android/service/SplashResponse;->receiptBlocked:Z

    .line 26
    iput-boolean v0, p0, Lcom/chase/sig/android/service/SplashResponse;->quickPayBlocked:Z

    .line 27
    iput-boolean v0, p0, Lcom/chase/sig/android/service/SplashResponse;->photoBillPayBlocked:Z

    .line 30
    iput-boolean v0, p0, Lcom/chase/sig/android/service/SplashResponse;->alertsManagementBlocked:Z

    .line 46
    iput-boolean v0, p0, Lcom/chase/sig/android/service/SplashResponse;->isSplashEnabled:Z

    .line 85
    return-void
.end method

.method public constructor <init>(Ljava/util/ArrayList;)V
    .locals 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/chase/sig/android/domain/SplashInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 48
    invoke-direct {p0}, Lcom/chase/sig/android/service/l;-><init>()V

    .line 20
    iput-boolean v0, p0, Lcom/chase/sig/android/service/SplashResponse;->billPayBlocked:Z

    .line 21
    iput-boolean v0, p0, Lcom/chase/sig/android/service/SplashResponse;->ePayBlocked:Z

    .line 22
    iput-boolean v0, p0, Lcom/chase/sig/android/service/SplashResponse;->transferBlocked:Z

    .line 23
    iput-boolean v0, p0, Lcom/chase/sig/android/service/SplashResponse;->wiresBlocked:Z

    .line 24
    iput-boolean v0, p0, Lcom/chase/sig/android/service/SplashResponse;->quickDepositBlocked:Z

    .line 25
    iput-boolean v0, p0, Lcom/chase/sig/android/service/SplashResponse;->receiptBlocked:Z

    .line 26
    iput-boolean v0, p0, Lcom/chase/sig/android/service/SplashResponse;->quickPayBlocked:Z

    .line 27
    iput-boolean v0, p0, Lcom/chase/sig/android/service/SplashResponse;->photoBillPayBlocked:Z

    .line 30
    iput-boolean v0, p0, Lcom/chase/sig/android/service/SplashResponse;->alertsManagementBlocked:Z

    .line 46
    iput-boolean v0, p0, Lcom/chase/sig/android/service/SplashResponse;->isSplashEnabled:Z

    .line 50
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/SplashInfo;

    .line 51
    if-eqz v0, :cond_0

    .line 53
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/SplashInfo;->a()Ljava/lang/String;

    move-result-object v2

    const-string v3, "billpay"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 55
    iput-boolean v4, p0, Lcom/chase/sig/android/service/SplashResponse;->billPayBlocked:Z

    .line 56
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/SplashInfo;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/service/SplashResponse;->billPayMessage:Ljava/lang/String;

    goto :goto_0

    .line 57
    :cond_1
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/SplashInfo;->a()Ljava/lang/String;

    move-result-object v2

    const-string v3, "epay"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 58
    iput-boolean v4, p0, Lcom/chase/sig/android/service/SplashResponse;->ePayBlocked:Z

    .line 59
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/SplashInfo;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/service/SplashResponse;->ePayMessage:Ljava/lang/String;

    goto :goto_0

    .line 60
    :cond_2
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/SplashInfo;->a()Ljava/lang/String;

    move-result-object v2

    const-string v3, "transfer"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 61
    iput-boolean v4, p0, Lcom/chase/sig/android/service/SplashResponse;->transferBlocked:Z

    .line 62
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/SplashInfo;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/service/SplashResponse;->transferMessage:Ljava/lang/String;

    goto :goto_0

    .line 63
    :cond_3
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/SplashInfo;->a()Ljava/lang/String;

    move-result-object v2

    const-string v3, "wires"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 64
    iput-boolean v4, p0, Lcom/chase/sig/android/service/SplashResponse;->wiresBlocked:Z

    .line 65
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/SplashInfo;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/service/SplashResponse;->wiresMessage:Ljava/lang/String;

    goto :goto_0

    .line 66
    :cond_4
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/SplashInfo;->a()Ljava/lang/String;

    move-result-object v2

    const-string v3, "quickdeposit"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 67
    iput-boolean v4, p0, Lcom/chase/sig/android/service/SplashResponse;->quickDepositBlocked:Z

    .line 68
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/SplashInfo;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/service/SplashResponse;->quickDepositMessage:Ljava/lang/String;

    goto :goto_0

    .line 69
    :cond_5
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/SplashInfo;->a()Ljava/lang/String;

    move-result-object v2

    const-string v3, "quickreceipt"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 70
    iput-boolean v4, p0, Lcom/chase/sig/android/service/SplashResponse;->receiptBlocked:Z

    .line 71
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/SplashInfo;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/service/SplashResponse;->receiptMessage:Ljava/lang/String;

    goto/16 :goto_0

    .line 72
    :cond_6
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/SplashInfo;->a()Ljava/lang/String;

    move-result-object v2

    const-string v3, "quickpay"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 73
    iput-boolean v4, p0, Lcom/chase/sig/android/service/SplashResponse;->quickPayBlocked:Z

    .line 74
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/SplashInfo;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/service/SplashResponse;->quickPayMessage:Ljava/lang/String;

    goto/16 :goto_0

    .line 75
    :cond_7
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/SplashInfo;->a()Ljava/lang/String;

    move-result-object v2

    const-string v3, "photobillpay"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 76
    iput-boolean v4, p0, Lcom/chase/sig/android/service/SplashResponse;->photoBillPayBlocked:Z

    .line 77
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/SplashInfo;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/service/SplashResponse;->photoBillPayMessage:Ljava/lang/String;

    goto/16 :goto_0

    .line 78
    :cond_8
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/SplashInfo;->a()Ljava/lang/String;

    move-result-object v2

    const-string v3, "alertsmanagement"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 79
    iput-boolean v4, p0, Lcom/chase/sig/android/service/SplashResponse;->alertsManagementBlocked:Z

    .line 80
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/SplashInfo;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/service/SplashResponse;->alertsManagementMessage:Ljava/lang/String;

    goto/16 :goto_0

    .line 83
    :cond_9
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 88
    iget-boolean v0, p0, Lcom/chase/sig/android/service/SplashResponse;->quickPayBlocked:Z

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 96
    iget-boolean v0, p0, Lcom/chase/sig/android/service/SplashResponse;->transferBlocked:Z

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 104
    iget-boolean v0, p0, Lcom/chase/sig/android/service/SplashResponse;->billPayBlocked:Z

    return v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 112
    iget-boolean v0, p0, Lcom/chase/sig/android/service/SplashResponse;->quickDepositBlocked:Z

    return v0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 128
    iget-boolean v0, p0, Lcom/chase/sig/android/service/SplashResponse;->ePayBlocked:Z

    return v0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 136
    iget-boolean v0, p0, Lcom/chase/sig/android/service/SplashResponse;->wiresBlocked:Z

    return v0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 144
    iget-boolean v0, p0, Lcom/chase/sig/android/service/SplashResponse;->receiptBlocked:Z

    return v0
.end method

.method public final m()Z
    .locals 1

    .prologue
    .line 152
    iget-boolean v0, p0, Lcom/chase/sig/android/service/SplashResponse;->photoBillPayBlocked:Z

    return v0
.end method

.method public final n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/chase/sig/android/service/SplashResponse;->billPayMessage:Ljava/lang/String;

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/chase/sig/android/service/SplashResponse;->ePayMessage:Ljava/lang/String;

    return-object v0
.end method

.method public final p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/chase/sig/android/service/SplashResponse;->transferMessage:Ljava/lang/String;

    return-object v0
.end method

.method public final q()Ljava/lang/String;
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lcom/chase/sig/android/service/SplashResponse;->wiresMessage:Ljava/lang/String;

    return-object v0
.end method

.method public final r()Ljava/lang/String;
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lcom/chase/sig/android/service/SplashResponse;->quickDepositMessage:Ljava/lang/String;

    return-object v0
.end method

.method public final s()Ljava/lang/String;
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcom/chase/sig/android/service/SplashResponse;->receiptMessage:Ljava/lang/String;

    return-object v0
.end method

.method public final t()Ljava/lang/String;
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lcom/chase/sig/android/service/SplashResponse;->quickPayMessage:Ljava/lang/String;

    return-object v0
.end method

.method public final u()Ljava/lang/String;
    .locals 1

    .prologue
    .line 224
    iget-object v0, p0, Lcom/chase/sig/android/service/SplashResponse;->photoBillPayMessage:Ljava/lang/String;

    return-object v0
.end method

.method public final v()Ljava/lang/String;
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lcom/chase/sig/android/service/SplashResponse;->splashUrl:Ljava/lang/String;

    return-object v0
.end method

.method public final w()Z
    .locals 1

    .prologue
    .line 248
    iget-boolean v0, p0, Lcom/chase/sig/android/service/SplashResponse;->isSplashEnabled:Z

    return v0
.end method
