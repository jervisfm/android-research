.class public Lcom/chase/sig/android/service/TodoItem;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private actionLabel:Ljava/lang/String;

.field private amount:Lcom/chase/sig/android/util/Dollar;

.field private declineLabel:Ljava/lang/String;

.field private header:Ljava/lang/String;

.field private id:Ljava/lang/String;

.field private isInvoiceRequest:Ljava/lang/String;

.field private subLabel:Ljava/lang/String;

.field private typeLabel:Ljava/lang/String;

.field private typeString:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/chase/sig/android/service/TodoItem;->header:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Lcom/chase/sig/android/util/Dollar;)V
    .locals 0
    .parameter

    .prologue
    .line 42
    iput-object p1, p0, Lcom/chase/sig/android/service/TodoItem;->amount:Lcom/chase/sig/android/util/Dollar;

    .line 43
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 26
    iput-object p1, p0, Lcom/chase/sig/android/service/TodoItem;->header:Ljava/lang/String;

    .line 27
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/chase/sig/android/service/TodoItem;->actionLabel:Ljava/lang/String;

    return-object v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 34
    iput-object p1, p0, Lcom/chase/sig/android/service/TodoItem;->actionLabel:Ljava/lang/String;

    .line 35
    return-void
.end method

.method public final c()Lcom/chase/sig/android/util/Dollar;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/chase/sig/android/service/TodoItem;->amount:Lcom/chase/sig/android/util/Dollar;

    return-object v0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 50
    iput-object p1, p0, Lcom/chase/sig/android/service/TodoItem;->subLabel:Ljava/lang/String;

    .line 51
    return-void
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/chase/sig/android/service/TodoItem;->subLabel:Ljava/lang/String;

    return-object v0
.end method

.method public final d(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 58
    iput-object p1, p0, Lcom/chase/sig/android/service/TodoItem;->declineLabel:Ljava/lang/String;

    .line 59
    return-void
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/chase/sig/android/service/TodoItem;->declineLabel:Ljava/lang/String;

    return-object v0
.end method

.method public final e(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 66
    iput-object p1, p0, Lcom/chase/sig/android/service/TodoItem;->id:Ljava/lang/String;

    .line 67
    return-void
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/chase/sig/android/service/TodoItem;->id:Ljava/lang/String;

    return-object v0
.end method

.method public final f(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 74
    iput-object p1, p0, Lcom/chase/sig/android/service/TodoItem;->typeString:Ljava/lang/String;

    .line 75
    return-void
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/chase/sig/android/service/TodoItem;->typeString:Ljava/lang/String;

    return-object v0
.end method

.method public final g(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 82
    iput-object p1, p0, Lcom/chase/sig/android/service/TodoItem;->typeLabel:Ljava/lang/String;

    .line 83
    return-void
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/chase/sig/android/service/TodoItem;->isInvoiceRequest:Ljava/lang/String;

    return-object v0
.end method

.method public final h(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 86
    iput-object p1, p0, Lcom/chase/sig/android/service/TodoItem;->isInvoiceRequest:Ljava/lang/String;

    .line 87
    return-void
.end method
