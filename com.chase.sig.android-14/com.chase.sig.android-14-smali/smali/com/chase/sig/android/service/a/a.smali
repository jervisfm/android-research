.class public final Lcom/chase/sig/android/service/a/a;
.super Lcom/chase/sig/android/domain/k;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 5
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 32
    invoke-direct {p0}, Lcom/chase/sig/android/domain/k;-><init>()V

    .line 35
    :try_start_0
    const-string v0, "profile"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 37
    if-nez v2, :cond_0

    .line 64
    :goto_0
    return-void

    .line 41
    :cond_0
    const-string v0, "maintenance"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 42
    if-eqz v0, :cond_1

    .line 43
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    const-class v3, Lcom/chase/sig/android/domain/MaintenanceInfo;

    invoke-static {v0, v3}, Lcom/chase/sig/android/service/JPService$a;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/MaintenanceInfo;

    iput-object v0, p0, Lcom/chase/sig/android/domain/k;->g:Lcom/chase/sig/android/domain/MaintenanceInfo;

    .line 46
    :cond_1
    const-string v0, "type"

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/domain/k;->a:Ljava/lang/String;

    .line 48
    const-string v0, "miniProfileGroups"

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 49
    const-string v3, "personalCustomers"

    invoke-direct {p0, v0, v3}, Lcom/chase/sig/android/service/a/a;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    .line 51
    const-string v4, "nonPersonalCustomers"

    invoke-direct {p0, v0, v4}, Lcom/chase/sig/android/service/a/a;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 54
    iput-object v3, p0, Lcom/chase/sig/android/domain/k;->b:Ljava/util/List;

    .line 55
    iput-object v0, p0, Lcom/chase/sig/android/domain/k;->c:Ljava/util/List;

    .line 57
    iget-object v0, p0, Lcom/chase/sig/android/domain/k;->b:Ljava/util/List;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/lit8 v3, v0, 0x0

    iget-object v0, p0, Lcom/chase/sig/android/domain/k;->c:Ljava/util/List;

    if-nez v0, :cond_5

    move v0, v1

    :goto_2
    add-int/2addr v0, v3

    if-lez v0, :cond_3

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/chase/sig/android/domain/k;->d:Ljava/util/List;

    iget-object v0, p0, Lcom/chase/sig/android/domain/k;->b:Ljava/util/List;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/chase/sig/android/domain/k;->d:Ljava/util/List;

    iget-object v1, p0, Lcom/chase/sig/android/domain/k;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_2
    iget-object v0, p0, Lcom/chase/sig/android/domain/k;->c:Ljava/util/List;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/chase/sig/android/domain/k;->d:Ljava/util/List;

    iget-object v1, p0, Lcom/chase/sig/android/domain/k;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 59
    :cond_3
    const-string v0, "profilePrivileges"

    const-string v1, "name"

    invoke-static {v2, v0, v1}, Lcom/chase/sig/android/service/a/a;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/domain/k;->e:Ljava/util/Set;

    .line 60
    const-string v0, "profileFeatures"

    invoke-static {v2, v0}, Lcom/chase/sig/android/service/a/a;->c(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/Hashtable;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/domain/k;->f:Ljava/util/Map;

    goto :goto_0

    .line 63
    :catch_0
    move-exception v0

    goto :goto_0

    .line 57
    :cond_4
    iget-object v0, p0, Lcom/chase/sig/android/domain/k;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/chase/sig/android/domain/k;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_2
.end method

.method private a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/List;
    .locals 8
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/e;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 80
    invoke-virtual {p1, p2}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v5

    .line 84
    if-eqz v5, :cond_2

    .line 85
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 86
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v6

    .line 88
    const/4 v0, 0x0

    move v4, v0

    :goto_0
    if-ge v4, v6, :cond_1

    .line 89
    invoke-virtual {v5, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    if-nez v0, :cond_0

    move-object v0, v1

    .line 90
    :goto_1
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 88
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    .line 89
    :cond_0
    new-instance v2, Lcom/chase/sig/android/domain/c;

    invoke-direct {v2}, Lcom/chase/sig/android/domain/c;-><init>()V

    const-string v7, "id"

    invoke-virtual {v0, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v2, v7}, Lcom/chase/sig/android/domain/e;->a(Ljava/lang/String;)V

    const-string v7, "name"

    invoke-virtual {v0, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v2, v7}, Lcom/chase/sig/android/domain/e;->b(Ljava/lang/String;)V

    const-string v7, "accounts"

    invoke-direct {p0, v0, v7}, Lcom/chase/sig/android/service/a/a;->b(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-interface {v2, v0}, Lcom/chase/sig/android/domain/e;->a(Ljava/util/ArrayList;)V

    move-object v0, v2

    goto :goto_1

    :cond_1
    move-object v1, v3

    .line 93
    :cond_2
    return-object v1
.end method

.method private static a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)Ljava/util/Set;
    .locals 5
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 250
    invoke-virtual {p0, p1}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 264
    :cond_0
    return-object v0

    .line 254
    :cond_1
    invoke-virtual {p0, p1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 256
    if-eqz v2, :cond_0

    .line 257
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    .line 258
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 260
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 261
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    invoke-virtual {v4, p2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 260
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private b(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/List;
    .locals 11
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/IAccount;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x0

    .line 124
    invoke-virtual {p1, p2}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 142
    :cond_0
    :goto_0
    return-object v1

    .line 128
    :cond_1
    invoke-virtual {p1, p2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v6

    .line 132
    if-eqz v6, :cond_0

    .line 133
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 134
    invoke-virtual {v6}, Lorg/json/JSONArray;->length()I

    move-result v7

    move v4, v5

    .line 136
    :goto_1
    if-ge v4, v7, :cond_6

    .line 137
    invoke-virtual {v6, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v8

    if-nez v8, :cond_2

    move-object v0, v1

    .line 138
    :goto_2
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 136
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    .line 137
    :cond_2
    new-instance v2, Lcom/chase/sig/android/domain/Account;

    invoke-direct {v2}, Lcom/chase/sig/android/domain/Account;-><init>()V

    const-string v0, "type"

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Lcom/chase/sig/android/domain/IAccount;->d(Ljava/lang/String;)V

    const-string v0, "omni"

    invoke-virtual {v8, v0, v5}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-interface {v2, v0}, Lcom/chase/sig/android/domain/IAccount;->e(Z)V

    const-string v0, "BCCControl"

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-interface {v2, v0}, Lcom/chase/sig/android/domain/IAccount;->c(Z)V

    const-string v0, "BCCEmployee"

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-interface {v2, v0}, Lcom/chase/sig/android/domain/IAccount;->d(Z)V

    const-string v0, "BCCOfficer"

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-interface {v2, v0}, Lcom/chase/sig/android/domain/IAccount;->b(Z)V

    const-string v0, "mask"

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Lcom/chase/sig/android/domain/IAccount;->a(Ljava/lang/String;)V

    const-string v0, "disclosureIds"

    invoke-static {v8, v0}, Lcom/chase/sig/android/service/a/a;->d(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v2, v0}, Lcom/chase/sig/android/domain/IAccount;->a(Ljava/util/Set;)V

    const-string v0, "id"

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Lcom/chase/sig/android/domain/IAccount;->b(Ljava/lang/String;)V

    const-string v0, "nickname"

    invoke-static {v8, v0}, Lcom/chase/sig/android/util/n;->b(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Lcom/chase/sig/android/domain/IAccount;->e(Ljava/lang/String;)V

    const-string v0, "accounts"

    invoke-direct {p0, v8, v0}, Lcom/chase/sig/android/service/a/a;->b(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v2, v0}, Lcom/chase/sig/android/domain/IAccount;->a(Ljava/util/List;)V

    const-string v0, "accountDetailList"

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_4

    move-object v0, v1

    :goto_3
    invoke-interface {v2, v0}, Lcom/chase/sig/android/domain/IAccount;->b(Ljava/util/List;)V

    const-string v0, "details"

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_5

    move-object v0, v1

    :goto_4
    invoke-interface {v2, v0}, Lcom/chase/sig/android/domain/IAccount;->a(Ljava/util/Hashtable;)V

    const-string v0, "accountPrivileges"

    const-string v9, "name"

    invoke-static {v8, v0, v9}, Lcom/chase/sig/android/service/a/a;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v2, v0}, Lcom/chase/sig/android/domain/IAccount;->b(Ljava/util/Set;)V

    const-string v0, "standInAsofDate"

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "standInAsofDate"

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "standInAsofDate"

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Lcom/chase/sig/android/domain/IAccount;->g(Ljava/lang/String;)V

    :cond_3
    move-object v0, v2

    goto/16 :goto_2

    :cond_4
    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    new-instance v9, Lcom/chase/sig/android/service/a/b;

    invoke-direct {v9, p0}, Lcom/chase/sig/android/service/a/b;-><init>(Lcom/chase/sig/android/service/a/a;)V

    invoke-virtual {v9}, Lcom/chase/sig/android/service/a/b;->b()Ljava/lang/reflect/Type;

    move-result-object v9

    new-instance v10, Lcom/google/gson/chase/Gson;

    invoke-direct {v10}, Lcom/google/gson/chase/Gson;-><init>()V

    invoke-virtual {v0}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v0, v9}, Lcom/google/gson/chase/Gson;->a(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    goto :goto_3

    :cond_5
    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    if-eqz v0, :cond_7

    const-string v9, "values"

    invoke-static {v0, v9}, Lcom/chase/sig/android/service/a/a;->c(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/Hashtable;

    move-result-object v0

    goto :goto_4

    :cond_6
    move-object v1, v3

    goto/16 :goto_0

    :cond_7
    move-object v0, v1

    goto :goto_4
.end method

.method private static c(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/Hashtable;
    .locals 5
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 193
    invoke-virtual {p0, p1}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 215
    :cond_0
    :goto_0
    return-object v0

    .line 197
    :cond_1
    invoke-virtual {p0, p1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 200
    if-eqz v2, :cond_0

    .line 201
    new-instance v1, Ljava/util/Hashtable;

    invoke-direct {v1}, Ljava/util/Hashtable;-><init>()V

    .line 207
    invoke-virtual {v2}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v3

    .line 209
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 210
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 211
    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v0, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method private static d(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/Set;
    .locals 5
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 230
    invoke-virtual {p0, p1}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 244
    :cond_0
    return-object v0

    .line 234
    :cond_1
    invoke-virtual {p0, p1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 236
    if-eqz v2, :cond_0

    .line 237
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    .line 238
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 240
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 241
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 240
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
