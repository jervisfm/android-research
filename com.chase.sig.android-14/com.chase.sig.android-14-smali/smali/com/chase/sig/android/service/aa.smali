.class public final Lcom/chase/sig/android/service/aa;
.super Lcom/chase/sig/android/service/JPService;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/chase/sig/android/service/JPService;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/chase/sig/android/domain/QuoteResponse;
    .locals 4
    .parameter

    .prologue
    .line 15
    const v0, 0x7f07002b

    invoke-static {v0}, Lcom/chase/sig/android/service/aa;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 16
    new-instance v1, Lcom/chase/sig/android/domain/QuoteResponse;

    invoke-direct {v1}, Lcom/chase/sig/android/domain/QuoteResponse;-><init>()V

    .line 19
    :try_start_0
    invoke-static {}, Lcom/chase/sig/android/service/aa;->c()Ljava/util/Hashtable;

    move-result-object v2

    .line 20
    const-string v3, "tickerSymbol"

    invoke-virtual {v2, v3, p0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 22
    const-class v3, Lcom/chase/sig/android/domain/QuoteResponse;

    invoke-static {v0, v2, v3}, Lcom/chase/sig/android/service/aa;->a(Ljava/lang/String;Ljava/util/Hashtable;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/QuoteResponse;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 29
    :goto_0
    return-object v0

    .line 24
    :catch_0
    move-exception v0

    .line 25
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/QuoteResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    move-object v0, v1

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/domain/QuoteResponse;
    .locals 7
    .parameter
    .parameter

    .prologue
    .line 33
    const v0, 0x7f07002a

    invoke-static {v0}, Lcom/chase/sig/android/service/aa;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 35
    new-instance v1, Lcom/chase/sig/android/domain/QuoteResponse;

    invoke-direct {v1}, Lcom/chase/sig/android/domain/QuoteResponse;-><init>()V

    .line 38
    :try_start_0
    invoke-static {}, Lcom/chase/sig/android/service/aa;->c()Ljava/util/Hashtable;

    move-result-object v2

    .line 40
    const-string v3, "tickerSymbol"

    const-string v4, "[\"%s\"]"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p0, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    invoke-static {p1}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 43
    const-string v3, "\\."

    invoke-virtual {p1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 44
    const-string v4, "quantity"

    const/4 v5, 0x0

    aget-object v3, v3, v5

    invoke-virtual {v2, v4, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    :cond_0
    const-class v3, Lcom/chase/sig/android/domain/QuoteResponse;

    invoke-static {v0, v2, v3}, Lcom/chase/sig/android/service/aa;->a(Ljava/lang/String;Ljava/util/Hashtable;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/QuoteResponse;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 54
    :goto_0
    return-object v0

    .line 49
    :catch_0
    move-exception v0

    .line 50
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/QuoteResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    move-object v0, v1

    goto :goto_0
.end method
