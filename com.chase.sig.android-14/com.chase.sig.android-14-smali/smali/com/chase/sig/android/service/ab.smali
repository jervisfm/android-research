.class public final Lcom/chase/sig/android/service/ab;
.super Lcom/chase/sig/android/service/JPService;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/chase/sig/android/service/JPService;-><init>()V

    return-void
.end method

.method public static a()Lcom/chase/sig/android/service/ReceiptPlansResponse;
    .locals 5

    .prologue
    .line 19
    const v0, 0x7f070039

    invoke-static {v0}, Lcom/chase/sig/android/service/ab;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 20
    new-instance v1, Lcom/chase/sig/android/service/ReceiptPlansResponse;

    invoke-direct {v1}, Lcom/chase/sig/android/service/ReceiptPlansResponse;-><init>()V

    .line 22
    :try_start_0
    invoke-static {}, Lcom/chase/sig/android/service/ab;->c()Ljava/util/Hashtable;

    move-result-object v2

    .line 23
    const-string v3, "profileId"

    const-string v4, "000"

    invoke-virtual {v2, v3, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 25
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v3

    invoke-static {v3, v0, v2}, Lcom/chase/sig/android/util/m;->a(Lcom/chase/sig/android/ChaseApplication;Ljava/lang/String;Ljava/util/Hashtable;)Lorg/json/JSONObject;

    move-result-object v0

    .line 26
    invoke-virtual {v1, v0}, Lcom/chase/sig/android/service/ReceiptPlansResponse;->b(Lorg/json/JSONObject;)V

    .line 27
    invoke-virtual {v1}, Lcom/chase/sig/android/service/ReceiptPlansResponse;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 37
    :goto_0
    return-object v1

    .line 30
    :cond_0
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lcom/google/gson/chase/GsonBuilder;

    invoke-direct {v2}, Lcom/google/gson/chase/GsonBuilder;-><init>()V

    const-class v3, Lcom/chase/sig/android/util/Dollar;

    new-instance v4, Lcom/chase/sig/android/domain/d;

    invoke-direct {v4}, Lcom/chase/sig/android/domain/d;-><init>()V

    invoke-virtual {v2, v3, v4}, Lcom/google/gson/chase/GsonBuilder;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/chase/GsonBuilder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/gson/chase/GsonBuilder;->b()Lcom/google/gson/chase/Gson;

    move-result-object v2

    const-class v3, Lcom/chase/sig/android/service/ReceiptPlansResponse;

    invoke-virtual {v2, v0, v3}, Lcom/google/gson/chase/Gson;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/service/ReceiptPlansResponse;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :goto_1
    move-object v1, v0

    .line 37
    goto :goto_0

    .line 32
    :catch_0
    move-exception v0

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/service/ReceiptPlansResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    move-object v0, v1

    .line 36
    goto :goto_1

    .line 35
    :catch_1
    move-exception v0

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/service/ReceiptPlansResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    move-object v0, v1

    goto :goto_1
.end method
