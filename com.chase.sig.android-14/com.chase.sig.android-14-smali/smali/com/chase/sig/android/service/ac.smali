.class public final Lcom/chase/sig/android/service/ac;
.super Lcom/chase/sig/android/service/JPService;
.source "SourceFile"


# instance fields
.field private a:Lcom/chase/sig/android/service/p;


# direct methods
.method public constructor <init>(Lcom/chase/sig/android/service/p;)V
    .locals 0
    .parameter

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/chase/sig/android/service/JPService;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/chase/sig/android/service/ac;->a:Lcom/chase/sig/android/service/p;

    .line 26
    return-void
.end method

.method public static a(Ljava/util/Hashtable;)Lcom/chase/sig/android/service/ReceiptListResponse;
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/chase/sig/android/service/ReceiptListResponse;"
        }
    .end annotation

    .prologue
    .line 39
    new-instance v1, Lcom/chase/sig/android/service/ReceiptListResponse;

    invoke-direct {v1}, Lcom/chase/sig/android/service/ReceiptListResponse;-><init>()V

    .line 41
    const v0, 0x7f07003e

    invoke-static {v0}, Lcom/chase/sig/android/service/ac;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 44
    :try_start_0
    invoke-static {}, Lcom/chase/sig/android/service/ac;->c()Ljava/util/Hashtable;

    move-result-object v2

    invoke-virtual {p0, v2}, Ljava/util/Hashtable;->putAll(Ljava/util/Map;)V

    .line 45
    const-class v2, Lcom/chase/sig/android/service/ReceiptListResponse;

    invoke-static {v0, p0, v2}, Lcom/chase/sig/android/service/ac;->a(Ljava/lang/String;Ljava/util/Hashtable;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/service/ReceiptListResponse;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 51
    :goto_0
    return-object v0

    .line 48
    :catch_0
    move-exception v0

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/service/ReceiptListResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    move-object v0, v1

    goto :goto_0
.end method

.method public static a(Lcom/chase/sig/android/domain/Receipt;)Lcom/chase/sig/android/service/l;
    .locals 6
    .parameter

    .prologue
    .line 188
    new-instance v1, Lcom/chase/sig/android/service/l;

    invoke-direct {v1}, Lcom/chase/sig/android/service/l;-><init>()V

    .line 190
    const v0, 0x7f070043

    invoke-static {v0}, Lcom/chase/sig/android/service/ac;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 191
    invoke-static {p0}, Lcom/chase/sig/android/service/ac;->c(Lcom/chase/sig/android/domain/Receipt;)Ljava/util/Hashtable;

    move-result-object v2

    .line 193
    invoke-virtual {p0}, Lcom/chase/sig/android/domain/Receipt;->j()[Ljava/lang/String;

    move-result-object v3

    .line 194
    const-string v4, "quickReceiptId"

    const/4 v5, 0x0

    aget-object v3, v3, v5

    invoke-virtual {v2, v4, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 197
    :try_start_0
    const-class v3, Lcom/chase/sig/android/service/l;

    invoke-static {v0, v2, v3}, Lcom/chase/sig/android/service/ac;->a(Ljava/lang/String;Ljava/util/Hashtable;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/service/l;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 203
    :goto_0
    return-object v0

    .line 200
    :catch_0
    move-exception v0

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/service/l;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    move-object v0, v1

    goto :goto_0
.end method

.method public static a(Lcom/chase/sig/android/domain/Receipt;Z)Lcom/chase/sig/android/service/l;
    .locals 5
    .parameter
    .parameter

    .prologue
    .line 164
    new-instance v1, Lcom/chase/sig/android/service/l;

    invoke-direct {v1}, Lcom/chase/sig/android/service/l;-><init>()V

    .line 166
    const v0, 0x7f070042

    invoke-static {v0}, Lcom/chase/sig/android/service/ac;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 167
    invoke-static {p0}, Lcom/chase/sig/android/service/ac;->c(Lcom/chase/sig/android/domain/Receipt;)Ljava/util/Hashtable;

    move-result-object v2

    .line 168
    const-string v3, "quickReceiptId"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/Receipt;->i()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 170
    invoke-virtual {p0}, Lcom/chase/sig/android/domain/Receipt;->f()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/Receipt;->f()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/chase/sig/android/util/s;->B(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 172
    const-string v3, "transactionId"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/Receipt;->f()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 173
    const-string v3, "collectionId"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/Receipt;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 177
    :cond_0
    :try_start_0
    const-class v3, Lcom/chase/sig/android/service/l;

    invoke-static {v0, v2, v3}, Lcom/chase/sig/android/service/ac;->a(Ljava/lang/String;Ljava/util/Hashtable;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/service/l;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 183
    :goto_0
    return-object v0

    .line 180
    :catch_0
    move-exception v0

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/service/l;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    move-object v0, v1

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)[B
    .locals 7
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 249
    const-string v0, "\\{"

    const-string v2, ""

    invoke-virtual {p0, v0, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "\\}"

    const-string v3, ""

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 251
    new-instance v2, Ljava/util/Hashtable;

    invoke-direct {v2}, Ljava/util/Hashtable;-><init>()V

    .line 252
    const-string v3, "quickReceiptId"

    invoke-virtual {v2, v3, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 254
    const v0, 0x7f070045

    invoke-static {v0}, Lcom/chase/sig/android/service/ac;->a(I)Ljava/lang/String;

    move-result-object v3

    .line 259
    :try_start_0
    invoke-static {}, Lcom/chase/sig/android/service/ac;->c()Ljava/util/Hashtable;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/Hashtable;->putAll(Ljava/util/Map;)V

    .line 260
    new-instance v0, Lcom/chase/sig/android/util/f;

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v4

    invoke-direct {v0, v4}, Lcom/chase/sig/android/util/f;-><init>(Lcom/chase/sig/android/ChaseApplication;)V

    const/4 v4, 0x0

    iput-object v4, v0, Lcom/chase/sig/android/util/f;->a:Ljava/lang/String;

    invoke-static {}, Lcom/chase/sig/android/service/JPService;->c()Ljava/util/Hashtable;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/util/Hashtable;->putAll(Ljava/util/Map;)V

    invoke-static {v4}, Lcom/chase/sig/android/util/f;->a(Ljava/util/Hashtable;)Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v2, v4}, Lcom/chase/sig/android/util/f;->a(Ljava/lang/String;Ljava/lang/String;Z)Lcom/chase/sig/android/util/f$a;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/util/f$a;->b:[B

    if-nez v0, :cond_0

    new-instance v0, Lcom/chase/sig/android/util/ChaseException;

    sget v2, Lcom/chase/sig/android/util/ChaseException;->b:I

    const-string v4, "Did not recieve BYTE response from %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v3, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lcom/chase/sig/android/util/ChaseException;-><init>(ILjava/lang/String;)V

    throw v0

    .line 264
    :catch_0
    move-exception v0

    move-object v0, v1

    .line 268
    :goto_0
    return-object v0

    .line 260
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Service HTTP Response Size: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0
.end method

.method public static b(Lcom/chase/sig/android/domain/Receipt;)Lcom/chase/sig/android/service/ReceiptsTransactionMatchResponse;
    .locals 5
    .parameter

    .prologue
    .line 207
    new-instance v1, Lcom/chase/sig/android/service/ReceiptsTransactionMatchResponse;

    invoke-direct {v1}, Lcom/chase/sig/android/service/ReceiptsTransactionMatchResponse;-><init>()V

    .line 209
    const v0, 0x7f070044

    invoke-static {v0}, Lcom/chase/sig/android/service/ac;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 211
    invoke-static {p0}, Lcom/chase/sig/android/service/ac;->c(Lcom/chase/sig/android/domain/Receipt;)Ljava/util/Hashtable;

    move-result-object v2

    .line 212
    const-string v3, "rows"

    const-string v4, "50"

    invoke-virtual {v2, v3, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 213
    const-string v3, "start"

    const-string v4, "1"

    invoke-virtual {v2, v3, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 216
    :try_start_0
    const-class v3, Lcom/chase/sig/android/service/ReceiptsTransactionMatchResponse;

    invoke-static {v0, v2, v3}, Lcom/chase/sig/android/service/ac;->a(Ljava/lang/String;Ljava/util/Hashtable;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/service/ReceiptsTransactionMatchResponse;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 222
    :goto_0
    return-object v0

    .line 219
    :catch_0
    move-exception v0

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/service/ReceiptsTransactionMatchResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    move-object v0, v1

    goto :goto_0
.end method

.method private static c(Lcom/chase/sig/android/domain/Receipt;)Ljava/util/Hashtable;
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/chase/sig/android/domain/Receipt;",
            ")",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 227
    invoke-static {}, Lcom/chase/sig/android/service/ac;->c()Ljava/util/Hashtable;

    move-result-object v1

    .line 228
    const-string v0, "accountId"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/Receipt;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 229
    const-string v2, "transactionDate"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/Receipt;->e()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v0, ""

    :goto_0
    invoke-virtual {v1, v2, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 231
    const-string v0, "description"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/Receipt;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 232
    const-string v0, "amount"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/Receipt;->b()Lcom/chase/sig/android/util/Dollar;

    move-result-object v2

    invoke-virtual {v2}, Lcom/chase/sig/android/util/Dollar;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 233
    const-string v0, "category"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/Receipt;->k()Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v2

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 235
    invoke-virtual {p0}, Lcom/chase/sig/android/domain/Receipt;->l()Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 236
    const-string v0, "expenseType"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/Receipt;->l()Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v2

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 239
    :cond_0
    invoke-virtual {p0}, Lcom/chase/sig/android/domain/Receipt;->m()Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 240
    const-string v0, "taxDeductable"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/Receipt;->m()Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v2

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 243
    :cond_1
    return-object v1

    .line 229
    :cond_2
    invoke-virtual {p0}, Lcom/chase/sig/android/domain/Receipt;->e()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static d()Lcom/chase/sig/android/service/ReceiptAccountSummaryResponse;
    .locals 4

    .prologue
    .line 60
    new-instance v1, Lcom/chase/sig/android/service/ReceiptAccountSummaryResponse;

    invoke-direct {v1}, Lcom/chase/sig/android/service/ReceiptAccountSummaryResponse;-><init>()V

    .line 62
    const v0, 0x7f070040

    invoke-static {v0}, Lcom/chase/sig/android/service/ac;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 66
    :try_start_0
    invoke-static {}, Lcom/chase/sig/android/service/ac;->c()Ljava/util/Hashtable;

    move-result-object v2

    .line 67
    const-class v3, Lcom/chase/sig/android/service/ReceiptAccountSummaryResponse;

    invoke-static {v0, v2, v3}, Lcom/chase/sig/android/service/ac;->a(Ljava/lang/String;Ljava/util/Hashtable;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/service/ReceiptAccountSummaryResponse;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 74
    :goto_0
    return-object v0

    .line 69
    :catch_0
    move-exception v0

    .line 70
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/service/ReceiptAccountSummaryResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/chase/sig/android/service/ListContentResponse;
    .locals 3

    .prologue
    .line 55
    iget-object v0, p0, Lcom/chase/sig/android/service/ac;->a:Lcom/chase/sig/android/service/p;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "quickreceipts-filters"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/chase/sig/android/service/p;->a([Ljava/lang/String;)Lcom/chase/sig/android/service/ListContentResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/chase/sig/android/domain/Receipt;I)Lcom/chase/sig/android/service/ReceiptsAddResponse;
    .locals 9
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 113
    new-instance v7, Lcom/chase/sig/android/service/ReceiptsAddResponse;

    invoke-direct {v7}, Lcom/chase/sig/android/service/ReceiptsAddResponse;-><init>()V

    .line 115
    const v0, 0x7f070041

    invoke-static {v0}, Lcom/chase/sig/android/service/ac;->a(I)Ljava/lang/String;

    move-result-object v1

    .line 117
    invoke-static {p1}, Lcom/chase/sig/android/service/ac;->c(Lcom/chase/sig/android/domain/Receipt;)Ljava/util/Hashtable;

    move-result-object v2

    .line 119
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/Receipt;->g()Ljava/lang/String;

    move-result-object v0

    .line 120
    invoke-static {v0}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 121
    const-string v3, "collectionId"

    invoke-virtual {v2, v3, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    :cond_0
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/Receipt;->f()Ljava/lang/String;

    move-result-object v0

    .line 125
    invoke-static {v0}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 126
    const-string v3, "transactionId"

    invoke-virtual {v2, v3, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 129
    :cond_1
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/Receipt;->h()Lcom/chase/sig/android/domain/ReceiptPhotoList;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/ReceiptPhotoList;->a()Ljava/util/List;

    move-result-object v8

    .line 131
    new-array v5, v4, [Ljava/lang/String;

    const-string v0, "image/jpeg"

    aput-object v0, v5, v6

    .line 134
    new-array v3, v4, [Ljava/lang/String;

    const-string v0, "receiptImage"

    aput-object v0, v3, v6

    .line 137
    new-array v4, v4, [[B

    invoke-interface {v8, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/ReceiptPhotoList$ReceiptPhoto;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/ReceiptPhotoList$ReceiptPhoto;->b()[B

    move-result-object v0

    aput-object v0, v4, v6

    .line 141
    :try_start_0
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v6

    invoke-virtual {v6}, Lcom/chase/sig/android/ChaseApplication;->h()Ljava/lang/String;

    move-result-object v6

    invoke-static/range {v0 .. v6}, Lcom/chase/sig/android/util/m;->a(Lcom/chase/sig/android/ChaseApplication;Ljava/lang/String;Ljava/util/Hashtable;[Ljava/lang/String;[[B[Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 145
    invoke-virtual {v7, v0}, Lcom/chase/sig/android/service/ReceiptsAddResponse;->b(Lorg/json/JSONObject;)V

    .line 147
    const-string v1, "collectionId"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 149
    const/4 v1, 0x3

    if-ge p2, v1, :cond_2

    add-int/lit8 v1, p2, 0x1

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 150
    invoke-virtual {p1, v0}, Lcom/chase/sig/android/domain/Receipt;->e(Ljava/lang/String;)V

    .line 151
    invoke-virtual {p0, p1, v1}, Lcom/chase/sig/android/service/ac;->a(Lcom/chase/sig/android/domain/Receipt;I)Lcom/chase/sig/android/service/ReceiptsAddResponse;
    :try_end_0
    .catch Lcom/chase/sig/android/util/ChaseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 159
    :cond_2
    :goto_0
    return-object v7

    .line 155
    :catch_0
    move-exception v0

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/chase/sig/android/service/ReceiptsAddResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    goto :goto_0
.end method
