.class public final Lcom/chase/sig/android/service/ad;
.super Lcom/chase/sig/android/service/JPService;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/chase/sig/android/service/JPService;-><init>()V

    return-void
.end method

.method public static a()Lcom/chase/sig/android/service/ReceiptsEnrollmentResponse;
    .locals 4

    .prologue
    .line 125
    const v0, 0x7f07003b

    invoke-static {v0}, Lcom/chase/sig/android/service/ad;->a(I)Ljava/lang/String;

    move-result-object v1

    .line 126
    new-instance v0, Lcom/chase/sig/android/service/ReceiptsEnrollmentResponse;

    invoke-direct {v0}, Lcom/chase/sig/android/service/ReceiptsEnrollmentResponse;-><init>()V

    .line 128
    :try_start_0
    invoke-static {}, Lcom/chase/sig/android/service/ad;->c()Ljava/util/Hashtable;

    move-result-object v2

    .line 130
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v3

    invoke-static {v3, v1, v2}, Lcom/chase/sig/android/util/m;->a(Lcom/chase/sig/android/ChaseApplication;Ljava/lang/String;Ljava/util/Hashtable;)Lorg/json/JSONObject;

    move-result-object v1

    .line 131
    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/ReceiptsEnrollmentResponse;->b(Lorg/json/JSONObject;)V

    .line 132
    invoke-virtual {v0}, Lcom/chase/sig/android/service/ReceiptsEnrollmentResponse;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 142
    :goto_0
    return-object v0

    .line 135
    :cond_0
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/service/ad;->a(Ljava/lang/String;)Lcom/chase/sig/android/service/ReceiptsEnrollmentResponse;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    goto :goto_0

    .line 136
    :catch_0
    move-exception v1

    .line 137
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/ReceiptsEnrollmentResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    goto :goto_0

    .line 139
    :catch_1
    move-exception v1

    .line 140
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/ReceiptsEnrollmentResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;)Lcom/chase/sig/android/service/ReceiptsEnrollmentResponse;
    .locals 1
    .parameter

    .prologue
    .line 58
    const-class v0, Lcom/chase/sig/android/service/ReceiptsEnrollmentResponse;

    invoke-static {p0, v0}, Lcom/chase/sig/android/service/JPService$a;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/service/ReceiptsEnrollmentResponse;

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/service/ReceiptsEnrollmentResponse;
    .locals 5
    .parameter
    .parameter

    .prologue
    .line 22
    const v0, 0x7f07003a

    invoke-static {v0}, Lcom/chase/sig/android/service/ad;->a(I)Ljava/lang/String;

    move-result-object v1

    .line 23
    new-instance v0, Lcom/chase/sig/android/service/ReceiptsEnrollmentResponse;

    invoke-direct {v0}, Lcom/chase/sig/android/service/ReceiptsEnrollmentResponse;-><init>()V

    .line 26
    :try_start_0
    invoke-static {}, Lcom/chase/sig/android/service/ad;->c()Ljava/util/Hashtable;

    move-result-object v2

    .line 27
    const-string v3, "planCode"

    invoke-virtual {v2, v3, p1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    if-eqz p0, :cond_0

    .line 30
    const-string v3, "accountId"

    invoke-virtual {v2, v3, p0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    :cond_0
    const-string v3, "validateOnly"

    const-string v4, "true"

    invoke-virtual {v2, v3, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v3

    invoke-static {v3, v1, v2}, Lcom/chase/sig/android/util/m;->a(Lcom/chase/sig/android/ChaseApplication;Ljava/lang/String;Ljava/util/Hashtable;)Lorg/json/JSONObject;

    move-result-object v1

    .line 37
    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/ReceiptsEnrollmentResponse;->b(Lorg/json/JSONObject;)V

    .line 39
    invoke-virtual {v0}, Lcom/chase/sig/android/service/ReceiptsEnrollmentResponse;->e()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 52
    :goto_0
    return-object v0

    .line 43
    :cond_1
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/service/ad;->a(Ljava/lang/String;)Lcom/chase/sig/android/service/ReceiptsEnrollmentResponse;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    goto :goto_0

    .line 45
    :catch_0
    move-exception v1

    .line 46
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/ReceiptsEnrollmentResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    goto :goto_0

    .line 48
    :catch_1
    move-exception v1

    .line 49
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/ReceiptsEnrollmentResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/service/ReceiptsEnrollmentResponse;
    .locals 5
    .parameter
    .parameter

    .prologue
    .line 65
    const v0, 0x7f07003a

    invoke-static {v0}, Lcom/chase/sig/android/service/ad;->a(I)Ljava/lang/String;

    move-result-object v1

    .line 66
    new-instance v0, Lcom/chase/sig/android/service/ReceiptsEnrollmentResponse;

    invoke-direct {v0}, Lcom/chase/sig/android/service/ReceiptsEnrollmentResponse;-><init>()V

    .line 69
    :try_start_0
    invoke-static {}, Lcom/chase/sig/android/service/ad;->c()Ljava/util/Hashtable;

    move-result-object v2

    .line 70
    const-string v3, "formId"

    invoke-virtual {v2, v3, p0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    const-string v3, "action"

    invoke-virtual {v2, v3, p1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    const-string v3, "validateOnly"

    const-string v4, "false"

    invoke-virtual {v2, v3, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v3

    invoke-static {v3, v1, v2}, Lcom/chase/sig/android/util/m;->a(Lcom/chase/sig/android/ChaseApplication;Ljava/lang/String;Ljava/util/Hashtable;)Lorg/json/JSONObject;

    move-result-object v1

    .line 76
    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/ReceiptsEnrollmentResponse;->b(Lorg/json/JSONObject;)V

    .line 78
    invoke-virtual {v0}, Lcom/chase/sig/android/service/ReceiptsEnrollmentResponse;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 92
    :goto_0
    return-object v0

    .line 83
    :cond_0
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/service/ad;->a(Ljava/lang/String;)Lcom/chase/sig/android/service/ReceiptsEnrollmentResponse;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    goto :goto_0

    .line 85
    :catch_0
    move-exception v1

    .line 86
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/ReceiptsEnrollmentResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    goto :goto_0

    .line 88
    :catch_1
    move-exception v1

    .line 89
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/ReceiptsEnrollmentResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    goto :goto_0
.end method

.method public static c(Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/service/ReceiptsEnrollmentResponse;
    .locals 4
    .parameter
    .parameter

    .prologue
    .line 96
    const v0, 0x7f07003d

    invoke-static {v0}, Lcom/chase/sig/android/service/ad;->a(I)Ljava/lang/String;

    move-result-object v1

    .line 97
    new-instance v0, Lcom/chase/sig/android/service/ReceiptsEnrollmentResponse;

    invoke-direct {v0}, Lcom/chase/sig/android/service/ReceiptsEnrollmentResponse;-><init>()V

    .line 100
    :try_start_0
    invoke-static {}, Lcom/chase/sig/android/service/ad;->c()Ljava/util/Hashtable;

    move-result-object v2

    .line 101
    const-string v3, "planCode"

    invoke-virtual {v2, v3, p0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    if-eqz p1, :cond_0

    .line 104
    const-string v3, "accountId"

    invoke-virtual {v2, v3, p1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    :cond_0
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v3

    invoke-static {v3, v1, v2}, Lcom/chase/sig/android/util/m;->a(Lcom/chase/sig/android/ChaseApplication;Ljava/lang/String;Ljava/util/Hashtable;)Lorg/json/JSONObject;

    move-result-object v1

    .line 109
    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/ReceiptsEnrollmentResponse;->b(Lorg/json/JSONObject;)V

    .line 111
    invoke-virtual {v0}, Lcom/chase/sig/android/service/ReceiptsEnrollmentResponse;->e()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 121
    :goto_0
    return-object v0

    .line 115
    :cond_1
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/service/ad;->a(Ljava/lang/String;)Lcom/chase/sig/android/service/ReceiptsEnrollmentResponse;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 117
    :catch_0
    move-exception v1

    .line 118
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/ReceiptsEnrollmentResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    goto :goto_0
.end method
