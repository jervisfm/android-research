.class public final Lcom/chase/sig/android/service/ae;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/gson/chase/JsonDeserializer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/gson/chase/JsonDeserializer",
        "<",
        "Lcom/chase/sig/android/service/IServiceError;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/gson/chase/JsonElement;Ljava/lang/reflect/Type;Lcom/google/gson/chase/JsonDeserializationContext;)Ljava/lang/Object;
    .locals 4
    .parameter
    .parameter
    .parameter

    .prologue
    .line 11
    check-cast p1, Lcom/google/gson/chase/JsonObject;

    new-instance v0, Lcom/chase/sig/android/service/ServiceError;

    const-string v1, "code"

    invoke-virtual {p1, v1}, Lcom/google/gson/chase/JsonObject;->a(Ljava/lang/String;)Lcom/google/gson/chase/JsonElement;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/gson/chase/JsonElement;->b()Ljava/lang/String;

    move-result-object v1

    const-string v2, "message"

    invoke-virtual {p1, v2}, Lcom/google/gson/chase/JsonObject;->a(Ljava/lang/String;)Lcom/google/gson/chase/JsonElement;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/gson/chase/JsonElement;->b()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/chase/sig/android/service/ServiceError;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    return-object v0
.end method
