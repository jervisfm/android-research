.class public final Lcom/chase/sig/android/service/aj;
.super Lcom/chase/sig/android/service/JPService;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/chase/sig/android/service/JPService;-><init>()V

    return-void
.end method

.method public static a()Lcom/chase/sig/android/service/TeamInfoResponse;
    .locals 16

    .prologue
    const/4 v9, 0x0

    .line 20
    const v0, 0x7f07002d

    invoke-static {v0}, Lcom/chase/sig/android/service/aj;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 22
    new-instance v11, Lcom/chase/sig/android/service/TeamInfoResponse;

    invoke-direct {v11}, Lcom/chase/sig/android/service/TeamInfoResponse;-><init>()V

    .line 24
    invoke-static {}, Lcom/chase/sig/android/service/aj;->c()Ljava/util/Hashtable;

    move-result-object v1

    .line 28
    :try_start_0
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v2

    invoke-static {v2, v0, v1}, Lcom/chase/sig/android/util/m;->a(Lcom/chase/sig/android/ChaseApplication;Ljava/lang/String;Ljava/util/Hashtable;)Lorg/json/JSONObject;

    move-result-object v0

    .line 30
    invoke-virtual {v11, v0}, Lcom/chase/sig/android/service/TeamInfoResponse;->b(Lorg/json/JSONObject;)V

    .line 32
    if-eqz v0, :cond_5

    .line 33
    const-string v1, "contactCompanyField"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v11, v1}, Lcom/chase/sig/android/service/TeamInfoResponse;->a(Ljava/lang/String;)V

    .line 34
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    const-string v1, "advisorView"

    invoke-static {v0, v1}, Lcom/chase/sig/android/util/n;->a(Lorg/json/JSONObject;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "advisorView"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v13

    invoke-virtual {v13}, Lorg/json/JSONArray;->length()I

    move-result v14

    if-lez v14, :cond_4

    move v10, v9

    :goto_0
    if-ge v10, v14, :cond_4

    invoke-virtual {v13, v10}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    if-eqz v6, :cond_1

    const-string v0, "displayName"

    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    new-instance v0, Lcom/chase/sig/android/domain/Advisor;

    const-string v1, "phone"

    invoke-static {v6, v1}, Lcom/chase/sig/android/util/n;->b(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "email"

    invoke-static {v6, v2}, Lcom/chase/sig/android/util/n;->b(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "middleInitial"

    invoke-virtual {v6, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "relationship"

    invoke-virtual {v6, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "firstName"

    invoke-virtual {v6, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v8, "lastName"

    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v7}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    :goto_1
    const-string v15, "Client Service Team"

    invoke-virtual {v15, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_0

    const-string v15, "Private Banking Mobile Support"

    invoke-virtual {v15, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    :cond_0
    const/4 v8, 0x1

    :goto_2
    invoke-direct/range {v0 .. v8}, Lcom/chase/sig/android/domain/Advisor;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v0, v10, 0x1

    move v10, v0

    goto :goto_0

    :cond_2
    const-string v8, ""

    goto :goto_1

    :cond_3
    move v8, v9

    goto :goto_2

    :cond_4
    invoke-virtual {v11, v12}, Lcom/chase/sig/android/service/TeamInfoResponse;->a(Ljava/util/List;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 44
    :cond_5
    :goto_3
    return-object v11

    .line 38
    :catch_0
    move-exception v0

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v11, v0}, Lcom/chase/sig/android/service/TeamInfoResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    goto :goto_3

    .line 41
    :catch_1
    move-exception v0

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v11, v0}, Lcom/chase/sig/android/service/TeamInfoResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    goto :goto_3
.end method
