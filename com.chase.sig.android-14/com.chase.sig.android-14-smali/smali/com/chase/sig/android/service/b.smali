.class public final Lcom/chase/sig/android/service/b;
.super Lcom/chase/sig/android/service/JPService;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/chase/sig/android/service/JPService;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/chase/sig/android/service/AccountDetailResponse;
    .locals 7
    .parameter

    .prologue
    .line 18
    new-instance v2, Lcom/chase/sig/android/service/AccountDetailResponse;

    invoke-direct {v2}, Lcom/chase/sig/android/service/AccountDetailResponse;-><init>()V

    .line 19
    new-instance v3, Lcom/chase/sig/android/domain/AccountDetail;

    invoke-direct {v3}, Lcom/chase/sig/android/domain/AccountDetail;-><init>()V

    .line 20
    const v0, 0x7f07000e

    invoke-static {v0}, Lcom/chase/sig/android/service/b;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 22
    invoke-static {}, Lcom/chase/sig/android/service/b;->c()Ljava/util/Hashtable;

    move-result-object v1

    .line 23
    const-string v4, "accountId"

    invoke-virtual {v1, v4, p0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 26
    :try_start_0
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v4

    invoke-static {v4, v0, v1}, Lcom/chase/sig/android/util/m;->a(Lcom/chase/sig/android/ChaseApplication;Ljava/lang/String;Ljava/util/Hashtable;)Lorg/json/JSONObject;

    move-result-object v0

    .line 27
    invoke-virtual {v2, v0}, Lcom/chase/sig/android/service/AccountDetailResponse;->b(Lorg/json/JSONObject;)V

    .line 29
    const-string v1, "detail"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 31
    if-eqz v0, :cond_0

    .line 32
    const-string v1, "id"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/chase/sig/android/domain/AccountDetail;->a(Ljava/lang/String;)V

    .line 33
    const-string v1, "failed"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v3, v1}, Lcom/chase/sig/android/domain/AccountDetail;->a(Z)V

    .line 34
    const-string v1, "loaded"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v3, v1}, Lcom/chase/sig/android/domain/AccountDetail;->b(Z)V

    .line 36
    new-instance v4, Ljava/util/Hashtable;

    invoke-direct {v4}, Ljava/util/Hashtable;-><init>()V

    .line 37
    const-string v1, "values"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v5

    .line 38
    if-eqz v5, :cond_2

    .line 39
    invoke-virtual {v5}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v0

    check-cast v0, Ljava/util/Iterator;

    .line 41
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 42
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 43
    invoke-virtual {v5, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 45
    invoke-virtual {v4, v1, v6}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 55
    :catch_0
    move-exception v0

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/chase/sig/android/service/AccountDetailResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    .line 59
    :cond_0
    :goto_1
    return-object v2

    .line 48
    :cond_1
    :try_start_1
    invoke-virtual {v3, v4}, Lcom/chase/sig/android/domain/AccountDetail;->a(Ljava/util/Hashtable;)V

    .line 51
    :cond_2
    invoke-virtual {v2, v3}, Lcom/chase/sig/android/service/AccountDetailResponse;->a(Lcom/chase/sig/android/domain/AccountDetail;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method
