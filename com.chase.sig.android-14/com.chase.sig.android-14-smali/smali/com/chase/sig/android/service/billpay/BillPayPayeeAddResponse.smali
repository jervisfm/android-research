.class public Lcom/chase/sig/android/service/billpay/BillPayPayeeAddResponse;
.super Lcom/chase/sig/android/service/l;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private formId:Ljava/lang/String;

.field private payee:Lcom/chase/sig/android/domain/Payee;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/chase/sig/android/service/l;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/chase/sig/android/service/billpay/BillPayPayeeAddResponse;->formId:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Lcom/chase/sig/android/domain/Payee;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/chase/sig/android/service/billpay/BillPayPayeeAddResponse;->payee:Lcom/chase/sig/android/domain/Payee;

    return-object v0
.end method
