.class public Lcom/chase/sig/android/service/billpay/BillPayPayeeEditResponse;
.super Lcom/chase/sig/android/service/l;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private accountNumberMask:Ljava/lang/String;

.field private formId:Ljava/lang/String;

.field private leadTime:I

.field private payeeId:Ljava/lang/String;

.field private status:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 7
    invoke-direct {p0}, Lcom/chase/sig/android/service/l;-><init>()V

    .line 13
    const/4 v0, -0x1

    iput v0, p0, Lcom/chase/sig/android/service/billpay/BillPayPayeeEditResponse;->leadTime:I

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/chase/sig/android/service/billpay/BillPayPayeeEditResponse;->formId:Ljava/lang/String;

    return-object v0
.end method

.method public final a(I)V
    .locals 0
    .parameter

    .prologue
    .line 22
    iput p1, p0, Lcom/chase/sig/android/service/billpay/BillPayPayeeEditResponse;->leadTime:I

    .line 23
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 30
    iput-object p1, p0, Lcom/chase/sig/android/service/billpay/BillPayPayeeEditResponse;->formId:Ljava/lang/String;

    .line 31
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/chase/sig/android/service/billpay/BillPayPayeeEditResponse;->status:Ljava/lang/String;

    return-object v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 38
    iput-object p1, p0, Lcom/chase/sig/android/service/billpay/BillPayPayeeEditResponse;->payeeId:Ljava/lang/String;

    .line 39
    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Lcom/chase/sig/android/service/billpay/BillPayPayeeEditResponse;->leadTime:I

    packed-switch v0, :pswitch_data_0

    .line 60
    :pswitch_0
    const-string v0, ""

    :goto_0
    return-object v0

    .line 52
    :pswitch_1
    const-string v0, "Same day"

    goto :goto_0

    .line 54
    :pswitch_2
    const-string v0, "1-day"

    goto :goto_0

    .line 56
    :pswitch_3
    const-string v0, "2-day"

    goto :goto_0

    .line 58
    :pswitch_4
    const-string v0, "5-day"

    goto :goto_0

    .line 50
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/chase/sig/android/service/billpay/BillPayPayeeEditResponse;->accountNumberMask:Ljava/lang/String;

    return-object v0
.end method

.method public final e(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 46
    iput-object p1, p0, Lcom/chase/sig/android/service/billpay/BillPayPayeeEditResponse;->status:Ljava/lang/String;

    .line 47
    return-void
.end method

.method public final f(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 69
    iput-object p1, p0, Lcom/chase/sig/android/service/billpay/BillPayPayeeEditResponse;->accountNumberMask:Ljava/lang/String;

    .line 70
    return-void
.end method
