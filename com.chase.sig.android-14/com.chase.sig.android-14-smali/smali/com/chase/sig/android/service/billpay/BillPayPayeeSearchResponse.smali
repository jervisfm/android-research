.class public Lcom/chase/sig/android/service/billpay/BillPayPayeeSearchResponse;
.super Lcom/chase/sig/android/service/l;
.source "SourceFile"


# instance fields
.field private capturedData:Lcom/chase/sig/android/domain/ImageCaptureData;

.field private flowIndicator:Ljava/lang/String;

.field private merchantPayees:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/MerchantPayee;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/chase/sig/android/service/l;-><init>()V

    .line 12
    new-instance v0, Lcom/chase/sig/android/domain/ImageCaptureData;

    invoke-direct {v0}, Lcom/chase/sig/android/domain/ImageCaptureData;-><init>()V

    iput-object v0, p0, Lcom/chase/sig/android/service/billpay/BillPayPayeeSearchResponse;->capturedData:Lcom/chase/sig/android/domain/ImageCaptureData;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/chase/sig/android/service/billpay/BillPayPayeeSearchResponse;->flowIndicator:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Lcom/chase/sig/android/domain/ImageCaptureData;)V
    .locals 0
    .parameter

    .prologue
    .line 27
    iput-object p1, p0, Lcom/chase/sig/android/service/billpay/BillPayPayeeSearchResponse;->capturedData:Lcom/chase/sig/android/domain/ImageCaptureData;

    .line 28
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 19
    iput-object p1, p0, Lcom/chase/sig/android/service/billpay/BillPayPayeeSearchResponse;->flowIndicator:Ljava/lang/String;

    .line 20
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/MerchantPayee;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 35
    iput-object p1, p0, Lcom/chase/sig/android/service/billpay/BillPayPayeeSearchResponse;->merchantPayees:Ljava/util/List;

    .line 36
    return-void
.end method

.method public final b()Lcom/chase/sig/android/domain/ImageCaptureData;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/chase/sig/android/service/billpay/BillPayPayeeSearchResponse;->capturedData:Lcom/chase/sig/android/domain/ImageCaptureData;

    return-object v0
.end method

.method public final c()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/MerchantPayee;",
            ">;"
        }
    .end annotation

    .prologue
    .line 31
    iget-object v0, p0, Lcom/chase/sig/android/service/billpay/BillPayPayeeSearchResponse;->merchantPayees:Ljava/util/List;

    return-object v0
.end method
