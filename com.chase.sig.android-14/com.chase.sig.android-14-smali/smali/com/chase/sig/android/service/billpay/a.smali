.class public final Lcom/chase/sig/android/service/billpay/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/lang/String;

.field final b:Ljava/lang/String;

.field c:Ljava/lang/String;

.field final d:Ljava/lang/String;

.field e:Ljava/lang/String;

.field f:Ljava/lang/String;

.field final g:Ljava/lang/String;

.field final h:Ljava/lang/String;

.field final i:Ljava/lang/String;

.field final j:Ljava/lang/String;

.field k:Ljava/lang/String;

.field l:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;)V
    .locals 3
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    iput-object v0, p0, Lcom/chase/sig/android/service/billpay/a;->c:Ljava/lang/String;

    .line 10
    iput-object v0, p0, Lcom/chase/sig/android/service/billpay/a;->e:Ljava/lang/String;

    .line 11
    iput-object v0, p0, Lcom/chase/sig/android/service/billpay/a;->f:Ljava/lang/String;

    .line 16
    iput-object v0, p0, Lcom/chase/sig/android/service/billpay/a;->k:Ljava/lang/String;

    .line 20
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->c()Lcom/chase/sig/android/domain/MerchantPayee;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/MerchantPayee;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/service/billpay/a;->a:Ljava/lang/String;

    .line 21
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/service/billpay/a;->b:Ljava/lang/String;

    .line 22
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/service/billpay/a;->c:Ljava/lang/String;

    .line 23
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->c()Lcom/chase/sig/android/domain/MerchantPayee;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/MerchantPayee;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/service/billpay/a;->d:Ljava/lang/String;

    .line 24
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->c()Lcom/chase/sig/android/domain/MerchantPayee;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/MerchantPayee;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/service/billpay/a;->e:Ljava/lang/String;

    .line 25
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->c()Lcom/chase/sig/android/domain/MerchantPayee;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/MerchantPayee;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/service/billpay/a;->g:Ljava/lang/String;

    .line 26
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->c()Lcom/chase/sig/android/domain/MerchantPayee;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/MerchantPayee;->g()Lcom/chase/sig/android/domain/State;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/State;->name()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/service/billpay/a;->h:Ljava/lang/String;

    .line 27
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->c()Lcom/chase/sig/android/domain/MerchantPayee;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/MerchantPayee;->h()Ljava/lang/String;

    move-result-object v0

    const-string v1, "-"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/service/billpay/a;->i:Ljava/lang/String;

    .line 28
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->c()Lcom/chase/sig/android/domain/MerchantPayee;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/MerchantPayee;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/service/billpay/a;->j:Ljava/lang/String;

    .line 29
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/service/billpay/a;->l:Ljava/lang/String;

    .line 31
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 32
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/BillPayVerifyPayeeInfo;->c()Lcom/chase/sig/android/domain/MerchantPayee;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/MerchantPayee;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/service/billpay/a;->k:Ljava/lang/String;

    .line 34
    :cond_0
    return-void
.end method
