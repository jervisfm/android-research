.class public final Lcom/chase/sig/android/service/billpay/b;
.super Lcom/chase/sig/android/service/movemoney/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/service/movemoney/d",
        "<",
        "Lcom/chase/sig/android/domain/BillPayTransaction;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/chase/sig/android/service/movemoney/d;-><init>()V

    .line 41
    return-void
.end method

.method public constructor <init>(Lcom/chase/sig/android/service/movemoney/g;Lcom/chase/sig/android/service/movemoney/e;Lcom/chase/sig/android/service/movemoney/f;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/chase/sig/android/service/movemoney/g;",
            "Lcom/chase/sig/android/service/movemoney/e;",
            "Lcom/chase/sig/android/service/movemoney/f",
            "<",
            "Lcom/chase/sig/android/domain/BillPayTransaction;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 36
    invoke-direct {p0, p1, p2, p3}, Lcom/chase/sig/android/service/movemoney/d;-><init>(Lcom/chase/sig/android/service/movemoney/g;Lcom/chase/sig/android/service/movemoney/e;Lcom/chase/sig/android/service/movemoney/f;)V

    .line 37
    return-void
.end method

.method private static a(Lorg/json/JSONObject;)Lcom/chase/sig/android/domain/MerchantPayee;
    .locals 3
    .parameter

    .prologue
    .line 221
    new-instance v2, Lcom/chase/sig/android/domain/MerchantPayee;

    invoke-direct {v2}, Lcom/chase/sig/android/domain/MerchantPayee;-><init>()V

    .line 223
    const-string v0, "optionId"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/chase/sig/android/domain/MerchantPayee;->a(Ljava/lang/String;)V

    .line 224
    const-string v0, "name"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/chase/sig/android/domain/MerchantPayee;->b(Ljava/lang/String;)V

    .line 225
    const-string v0, "phone"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/chase/sig/android/domain/MerchantPayee;->c(Ljava/lang/String;)V

    .line 227
    const-string v0, "address"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 228
    const-string v0, "address"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->opt(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/json/JSONObject;

    .line 230
    if-eqz v0, :cond_0

    .line 231
    const-string v1, "addressLine1"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/chase/sig/android/domain/MerchantPayee;->d(Ljava/lang/String;)V

    .line 232
    const-string v1, "addressLine2"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->l(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, ""

    :goto_0
    invoke-virtual {v2, v1}, Lcom/chase/sig/android/domain/MerchantPayee;->e(Ljava/lang/String;)V

    .line 234
    const-string v1, "city"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/chase/sig/android/domain/MerchantPayee;->f(Ljava/lang/String;)V

    .line 235
    const-string v1, "postalCode"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/chase/sig/android/domain/MerchantPayee;->g(Ljava/lang/String;)V

    .line 236
    const-string v1, "state"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/domain/State;->a(Ljava/lang/String;)Lcom/chase/sig/android/domain/State;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/chase/sig/android/domain/MerchantPayee;->a(Lcom/chase/sig/android/domain/State;)V

    .line 240
    :cond_0
    return-object v2

    .line 232
    :cond_1
    const-string v1, "addressLine2"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static a()Lcom/chase/sig/android/service/billpay/BillPayGetPayeesResponse;
    .locals 5

    .prologue
    .line 45
    new-instance v1, Lcom/chase/sig/android/service/billpay/BillPayGetPayeesResponse;

    invoke-direct {v1}, Lcom/chase/sig/android/service/billpay/BillPayGetPayeesResponse;-><init>()V

    .line 48
    const v0, 0x7f07004e

    :try_start_0
    invoke-static {v0}, Lcom/chase/sig/android/service/billpay/b;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 49
    invoke-static {}, Lcom/chase/sig/android/service/billpay/b;->c()Ljava/util/Hashtable;

    move-result-object v2

    .line 51
    const-string v3, "status"

    const-string v4, "[\"Active\",\"Rejected\",\"Pending Review\"]"

    invoke-virtual {v2, v3, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v3

    invoke-static {v3, v0, v2}, Lcom/chase/sig/android/util/m;->a(Lcom/chase/sig/android/ChaseApplication;Ljava/lang/String;Ljava/util/Hashtable;)Lorg/json/JSONObject;

    move-result-object v0

    .line 55
    invoke-virtual {v1, v0}, Lcom/chase/sig/android/service/billpay/BillPayGetPayeesResponse;->b(Lorg/json/JSONObject;)V

    .line 57
    invoke-virtual {v1}, Lcom/chase/sig/android/service/billpay/BillPayGetPayeesResponse;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 68
    :goto_0
    return-object v1

    .line 61
    :cond_0
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lcom/google/gson/chase/GsonBuilder;

    invoke-direct {v2}, Lcom/google/gson/chase/GsonBuilder;-><init>()V

    const-class v3, Lcom/chase/sig/android/util/Dollar;

    new-instance v4, Lcom/chase/sig/android/domain/d;

    invoke-direct {v4}, Lcom/chase/sig/android/domain/d;-><init>()V

    invoke-virtual {v2, v3, v4}, Lcom/google/gson/chase/GsonBuilder;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/chase/GsonBuilder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/gson/chase/GsonBuilder;->b()Lcom/google/gson/chase/Gson;

    move-result-object v2

    const-class v3, Lcom/chase/sig/android/service/billpay/BillPayGetPayeesResponse;

    invoke-virtual {v2, v0, v3}, Lcom/google/gson/chase/Gson;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/service/billpay/BillPayGetPayeesResponse;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    move-object v1, v0

    .line 68
    goto :goto_0

    .line 63
    :catch_0
    move-exception v0

    .line 64
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/service/billpay/BillPayGetPayeesResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    move-object v0, v1

    goto :goto_1
.end method

.method public static a(Lcom/chase/sig/android/service/billpay/a;)Lcom/chase/sig/android/service/billpay/BillPayPayeeAddResponse;
    .locals 7
    .parameter

    .prologue
    .line 131
    new-instance v1, Lcom/chase/sig/android/service/billpay/BillPayPayeeAddResponse;

    invoke-direct {v1}, Lcom/chase/sig/android/service/billpay/BillPayPayeeAddResponse;-><init>()V

    .line 134
    const v0, 0x7f07004b

    :try_start_0
    invoke-static {v0}, Lcom/chase/sig/android/service/billpay/b;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 135
    invoke-static {}, Lcom/chase/sig/android/service/billpay/b;->c()Ljava/util/Hashtable;

    move-result-object v2

    .line 137
    const-string v3, "validateOnly"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    const-string v3, "name"

    iget-object v4, p0, Lcom/chase/sig/android/service/billpay/a;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    const-string v3, "nickName"

    iget-object v4, p0, Lcom/chase/sig/android/service/billpay/a;->b:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    const-string v3, "accountNumber"

    iget-object v4, p0, Lcom/chase/sig/android/service/billpay/a;->c:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/chase/sig/android/service/billpay/b;->b(Ljava/util/Hashtable;Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    const-string v3, "addressLine1"

    iget-object v4, p0, Lcom/chase/sig/android/service/billpay/a;->d:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 145
    const-string v3, "addressLine2"

    iget-object v4, p0, Lcom/chase/sig/android/service/billpay/a;->e:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/chase/sig/android/service/billpay/b;->b(Ljava/util/Hashtable;Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    const-string v3, "addressLine3"

    iget-object v4, p0, Lcom/chase/sig/android/service/billpay/a;->f:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/chase/sig/android/service/billpay/b;->b(Ljava/util/Hashtable;Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    const-string v3, "city"

    iget-object v4, p0, Lcom/chase/sig/android/service/billpay/a;->g:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 151
    const-string v3, "state"

    iget-object v4, p0, Lcom/chase/sig/android/service/billpay/a;->h:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 152
    const-string v3, "zipCode"

    iget-object v4, p0, Lcom/chase/sig/android/service/billpay/a;->i:Ljava/lang/String;

    const-string v5, "-"

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 154
    iget-object v3, p0, Lcom/chase/sig/android/service/billpay/a;->j:Ljava/lang/String;

    invoke-static {v3}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 155
    const-string v3, "phone"

    iget-object v4, p0, Lcom/chase/sig/android/service/billpay/a;->j:Ljava/lang/String;

    const-string v5, "-"

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 159
    :cond_0
    const-string v3, "optionId"

    iget-object v4, p0, Lcom/chase/sig/android/service/billpay/a;->k:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/chase/sig/android/service/billpay/b;->b(Ljava/util/Hashtable;Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    const-string v3, "memo"

    iget-object v4, p0, Lcom/chase/sig/android/service/billpay/a;->l:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/chase/sig/android/service/billpay/b;->b(Ljava/util/Hashtable;Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v3

    invoke-static {v3, v0, v2}, Lcom/chase/sig/android/util/m;->a(Lcom/chase/sig/android/ChaseApplication;Ljava/lang/String;Ljava/util/Hashtable;)Lorg/json/JSONObject;

    move-result-object v0

    .line 163
    invoke-virtual {v1, v0}, Lcom/chase/sig/android/service/billpay/BillPayPayeeAddResponse;->b(Lorg/json/JSONObject;)V

    .line 165
    invoke-virtual {v1}, Lcom/chase/sig/android/service/billpay/BillPayPayeeAddResponse;->e()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 179
    :goto_0
    return-object v1

    .line 169
    :cond_1
    new-instance v2, Lcom/google/gson/chase/Gson;

    invoke-direct {v2}, Lcom/google/gson/chase/Gson;-><init>()V

    .line 171
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    const-class v3, Lcom/chase/sig/android/service/billpay/BillPayPayeeAddResponse;

    invoke-virtual {v2, v0, v3}, Lcom/google/gson/chase/Gson;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/service/billpay/BillPayPayeeAddResponse;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    move-object v1, v0

    .line 179
    goto :goto_0

    .line 174
    :catch_0
    move-exception v0

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/service/billpay/BillPayPayeeAddResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    move-object v0, v1

    goto :goto_1
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/service/billpay/BillPayPayeeAddResponse;
    .locals 5
    .parameter
    .parameter

    .prologue
    .line 191
    new-instance v1, Lcom/chase/sig/android/service/billpay/BillPayPayeeAddResponse;

    invoke-direct {v1}, Lcom/chase/sig/android/service/billpay/BillPayPayeeAddResponse;-><init>()V

    .line 194
    const v0, 0x7f07004b

    :try_start_0
    invoke-static {v0}, Lcom/chase/sig/android/service/billpay/b;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 195
    invoke-static {}, Lcom/chase/sig/android/service/billpay/b;->c()Ljava/util/Hashtable;

    move-result-object v2

    .line 196
    const-string v3, "validateOnly"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 197
    const-string v3, "formId"

    invoke-virtual {v2, v3, p1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 199
    const-string v3, "optionId"

    invoke-static {v2, v3, p0}, Lcom/chase/sig/android/service/billpay/b;->b(Ljava/util/Hashtable;Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v3

    invoke-static {v3, v0, v2}, Lcom/chase/sig/android/util/m;->a(Lcom/chase/sig/android/ChaseApplication;Ljava/lang/String;Ljava/util/Hashtable;)Lorg/json/JSONObject;

    move-result-object v0

    .line 202
    invoke-virtual {v1, v0}, Lcom/chase/sig/android/service/billpay/BillPayPayeeAddResponse;->b(Lorg/json/JSONObject;)V

    .line 204
    invoke-virtual {v1}, Lcom/chase/sig/android/service/billpay/BillPayPayeeAddResponse;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 216
    :goto_0
    return-object v1

    .line 208
    :cond_0
    new-instance v2, Lcom/google/gson/chase/Gson;

    invoke-direct {v2}, Lcom/google/gson/chase/Gson;-><init>()V

    .line 209
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    const-class v3, Lcom/chase/sig/android/service/billpay/BillPayPayeeAddResponse;

    invoke-virtual {v2, v0, v3}, Lcom/google/gson/chase/Gson;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/service/billpay/BillPayPayeeAddResponse;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    move-object v1, v0

    .line 216
    goto :goto_0

    .line 210
    :catch_0
    move-exception v0

    .line 211
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/service/billpay/BillPayPayeeAddResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    move-object v0, v1

    goto :goto_1
.end method

.method public static a(Lcom/chase/sig/android/domain/Payee;)Lcom/chase/sig/android/service/billpay/BillPayPayeeEditResponse;
    .locals 7
    .parameter

    .prologue
    .line 345
    new-instance v0, Lcom/chase/sig/android/service/billpay/BillPayPayeeEditResponse;

    invoke-direct {v0}, Lcom/chase/sig/android/service/billpay/BillPayPayeeEditResponse;-><init>()V

    .line 348
    const v1, 0x7f07004f

    :try_start_0
    invoke-static {v1}, Lcom/chase/sig/android/service/billpay/b;->a(I)Ljava/lang/String;

    move-result-object v1

    .line 349
    invoke-static {}, Lcom/chase/sig/android/service/billpay/b;->c()Ljava/util/Hashtable;

    move-result-object v2

    .line 350
    invoke-virtual {p0}, Lcom/chase/sig/android/domain/Payee;->o()Lcom/chase/sig/android/domain/PayeeAddress;

    move-result-object v3

    .line 352
    const-string v4, "payeeId"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/Payee;->j()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 353
    const-string v4, "token"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/Payee;->n()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 354
    const-string v4, "name"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/Payee;->i()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 355
    const-string v4, "nickName"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/Payee;->h()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 356
    const-string v4, "accountNumber"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/Payee;->c()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/chase/sig/android/util/s;->s(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 357
    const-string v4, "addressLine1"

    invoke-virtual {v3}, Lcom/chase/sig/android/domain/PayeeAddress;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 358
    const-string v4, "addressLine2"

    invoke-virtual {v3}, Lcom/chase/sig/android/domain/PayeeAddress;->b()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/chase/sig/android/util/s;->s(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 359
    const-string v4, "city"

    invoke-virtual {v3}, Lcom/chase/sig/android/domain/PayeeAddress;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 360
    const-string v4, "state"

    invoke-virtual {v3}, Lcom/chase/sig/android/domain/PayeeAddress;->e()Lcom/chase/sig/android/domain/State;

    move-result-object v5

    invoke-virtual {v5}, Lcom/chase/sig/android/domain/State;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 361
    const-string v4, "zipCode"

    invoke-virtual {v3}, Lcom/chase/sig/android/domain/PayeeAddress;->d()Ljava/lang/String;

    move-result-object v3

    const-string v5, "-"

    const-string v6, ""

    invoke-virtual {v3, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v4, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 362
    const-string v3, "phone"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/Payee;->p()Ljava/lang/String;

    move-result-object v4

    const-string v5, "-"

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/chase/sig/android/util/s;->s(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 363
    const-string v3, "memo"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/Payee;->g()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/chase/sig/android/util/s;->s(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 364
    const-string v3, "validateOnly"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 365
    const-string v3, "addressLine3"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 367
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v3

    invoke-static {v3, v1, v2}, Lcom/chase/sig/android/util/m;->a(Lcom/chase/sig/android/ChaseApplication;Ljava/lang/String;Ljava/util/Hashtable;)Lorg/json/JSONObject;

    move-result-object v1

    .line 368
    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/billpay/BillPayPayeeEditResponse;->b(Lorg/json/JSONObject;)V

    .line 370
    invoke-virtual {v0}, Lcom/chase/sig/android/service/billpay/BillPayPayeeEditResponse;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 388
    :goto_0
    return-object v0

    .line 374
    :cond_0
    const-string v2, "formId"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/service/billpay/BillPayPayeeEditResponse;->a(Ljava/lang/String;)V

    .line 376
    const-string v2, "payee"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 377
    const-string v2, "payeeId"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/service/billpay/BillPayPayeeEditResponse;->b(Ljava/lang/String;)V

    .line 378
    const-string v2, "status"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/service/billpay/BillPayPayeeEditResponse;->e(Ljava/lang/String;)V

    .line 379
    const-string v2, "leadTime"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/service/billpay/BillPayPayeeEditResponse;->a(I)V

    .line 380
    const-string v2, "accountNumberMask"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/billpay/BillPayPayeeEditResponse;->f(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 382
    :catch_0
    move-exception v1

    .line 383
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/billpay/BillPayPayeeEditResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/service/billpay/BillPayPayeeSearchResponse;
    .locals 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 87
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 88
    new-instance v0, Lcom/chase/sig/android/service/billpay/BillPayPayeeSearchResponse;

    invoke-direct {v0}, Lcom/chase/sig/android/service/billpay/BillPayPayeeSearchResponse;-><init>()V

    .line 91
    const v1, 0x7f07004a

    :try_start_0
    invoke-static {v1}, Lcom/chase/sig/android/service/billpay/b;->a(I)Ljava/lang/String;

    move-result-object v1

    .line 92
    invoke-static {}, Lcom/chase/sig/android/service/billpay/b;->c()Ljava/util/Hashtable;

    move-result-object v3

    .line 95
    const-string v4, "name"

    invoke-virtual {v3, v4, p0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    const-string v4, "postalCode"

    invoke-virtual {v3, v4, p1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    invoke-static {p2}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 98
    const-string v4, "accountNumber"

    invoke-virtual {v3, v4, p2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    :cond_0
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v4

    invoke-static {v4, v1, v3}, Lcom/chase/sig/android/util/m;->a(Lcom/chase/sig/android/ChaseApplication;Ljava/lang/String;Ljava/util/Hashtable;)Lorg/json/JSONObject;

    move-result-object v1

    .line 103
    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/billpay/BillPayPayeeSearchResponse;->b(Lorg/json/JSONObject;)V

    .line 104
    const-string v3, "flowIndicator"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/service/billpay/BillPayPayeeSearchResponse;->a(Ljava/lang/String;)V

    .line 106
    invoke-virtual {v0}, Lcom/chase/sig/android/service/billpay/BillPayPayeeSearchResponse;->e()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 125
    :goto_0
    return-object v0

    .line 110
    :cond_1
    const-string v3, "payees"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 112
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v1, v4, :cond_2

    .line 113
    invoke-virtual {v3, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 114
    invoke-static {v4}, Lcom/chase/sig/android/service/billpay/b;->a(Lorg/json/JSONObject;)Lcom/chase/sig/android/domain/MerchantPayee;

    move-result-object v4

    .line 115
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 112
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 118
    :cond_2
    invoke-virtual {v0, v2}, Lcom/chase/sig/android/service/billpay/BillPayPayeeSearchResponse;->a(Ljava/util/List;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 120
    :catch_0
    move-exception v1

    .line 121
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/billpay/BillPayPayeeSearchResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    goto :goto_0
.end method

.method public static a([B)Lcom/chase/sig/android/service/billpay/BillPayPayeeSearchResponse;
    .locals 10
    .parameter

    .prologue
    const/4 v8, 0x0

    .line 245
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 247
    new-instance v7, Lcom/chase/sig/android/service/billpay/BillPayPayeeSearchResponse;

    invoke-direct {v7}, Lcom/chase/sig/android/service/billpay/BillPayPayeeSearchResponse;-><init>()V

    .line 250
    const v0, 0x7f07004c

    :try_start_0
    invoke-static {v0}, Lcom/chase/sig/android/service/billpay/b;->a(I)Ljava/lang/String;

    move-result-object v1

    .line 252
    invoke-static {}, Lcom/chase/sig/android/service/billpay/b;->c()Ljava/util/Hashtable;

    move-result-object v2

    .line 254
    const/4 v0, 0x1

    new-array v3, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v4, "frontImageData"

    aput-object v4, v3, v0

    .line 257
    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v4, "image/jpeg"

    aput-object v4, v5, v0

    .line 260
    const/4 v0, 0x1

    new-array v4, v0, [[B

    const/4 v0, 0x0

    aput-object p0, v4, v0

    .line 264
    const-string v0, "frontImageType"

    const-string v6, "JPEG"

    invoke-virtual {v2, v0, v6}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 265
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v6

    invoke-virtual {v6}, Lcom/chase/sig/android/ChaseApplication;->h()Ljava/lang/String;

    move-result-object v6

    invoke-static/range {v0 .. v6}, Lcom/chase/sig/android/util/m;->a(Lcom/chase/sig/android/ChaseApplication;Ljava/lang/String;Ljava/util/Hashtable;[Ljava/lang/String;[[B[Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 269
    invoke-virtual {v7, v1}, Lcom/chase/sig/android/service/billpay/BillPayPayeeSearchResponse;->b(Lorg/json/JSONObject;)V

    .line 270
    const-string v0, "flowIndicator"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/chase/sig/android/service/billpay/BillPayPayeeSearchResponse;->a(Ljava/lang/String;)V

    .line 273
    const-string v0, "detail"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 275
    if-eqz v0, :cond_0

    .line 276
    new-instance v2, Lcom/chase/sig/android/domain/ImageCaptureData;

    invoke-direct {v2}, Lcom/chase/sig/android/domain/ImageCaptureData;-><init>()V

    .line 278
    const-string v3, "balDueAmount"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/chase/sig/android/domain/ImageCaptureData;->c(Ljava/lang/String;)V

    .line 279
    const-string v3, "amountDue"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/chase/sig/android/domain/ImageCaptureData;->d(Ljava/lang/String;)V

    .line 280
    const-string v3, "name"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/chase/sig/android/domain/ImageCaptureData;->a(Ljava/lang/String;)V

    .line 281
    const-string v3, "minDueAmount"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/chase/sig/android/domain/ImageCaptureData;->e(Ljava/lang/String;)V

    .line 282
    const-string v3, "invoiceNum"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/chase/sig/android/domain/ImageCaptureData;->f(Ljava/lang/String;)V

    .line 283
    const-string v3, "accountNumber"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/chase/sig/android/domain/ImageCaptureData;->b(Ljava/lang/String;)V

    .line 284
    const-string v3, "dueDate"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/chase/sig/android/domain/ImageCaptureData;->g(Ljava/lang/String;)V

    .line 287
    const-string v3, "address"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    .line 289
    const-string v0, "addressLine1"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/chase/sig/android/domain/ImageCaptureData;->h(Ljava/lang/String;)V

    .line 290
    const-string v0, "addressLine2"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->l(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, ""

    :goto_0
    invoke-virtual {v2, v0}, Lcom/chase/sig/android/domain/ImageCaptureData;->i(Ljava/lang/String;)V

    .line 292
    const-string v0, "city"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/chase/sig/android/domain/ImageCaptureData;->j(Ljava/lang/String;)V

    .line 293
    const-string v0, "state"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/domain/State;->a(Ljava/lang/String;)Lcom/chase/sig/android/domain/State;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/chase/sig/android/domain/ImageCaptureData;->a(Lcom/chase/sig/android/domain/State;)V

    .line 294
    const-string v0, "postalCode"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/chase/sig/android/domain/ImageCaptureData;->k(Ljava/lang/String;)V

    .line 296
    invoke-virtual {v7, v2}, Lcom/chase/sig/android/service/billpay/BillPayPayeeSearchResponse;->a(Lcom/chase/sig/android/domain/ImageCaptureData;)V

    .line 299
    :cond_0
    invoke-virtual {v7}, Lcom/chase/sig/android/service/billpay/BillPayPayeeSearchResponse;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    move-object v0, v7

    .line 318
    :goto_1
    return-object v0

    .line 290
    :cond_1
    const-string v0, "addressLine2"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 303
    :cond_2
    const-string v0, "payees"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    move v0, v8

    .line 305
    :goto_2
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v0, v2, :cond_3

    .line 306
    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    .line 307
    invoke-static {v2}, Lcom/chase/sig/android/service/billpay/b;->a(Lorg/json/JSONObject;)Lcom/chase/sig/android/domain/MerchantPayee;

    move-result-object v2

    .line 308
    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 305
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 311
    :cond_3
    invoke-virtual {v7, v9}, Lcom/chase/sig/android/service/billpay/BillPayPayeeSearchResponse;->a(Ljava/util/List;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_3
    move-object v0, v7

    .line 318
    goto :goto_1

    .line 314
    :catch_0
    move-exception v0

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/chase/sig/android/service/billpay/BillPayPayeeSearchResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    goto :goto_3
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/service/billpay/DeletePayeeResponse;
    .locals 4
    .parameter
    .parameter

    .prologue
    .line 323
    new-instance v0, Lcom/chase/sig/android/service/billpay/DeletePayeeResponse;

    invoke-direct {v0}, Lcom/chase/sig/android/service/billpay/DeletePayeeResponse;-><init>()V

    .line 326
    const v1, 0x7f07004d

    :try_start_0
    invoke-static {v1}, Lcom/chase/sig/android/service/billpay/b;->a(I)Ljava/lang/String;

    move-result-object v1

    .line 327
    invoke-static {}, Lcom/chase/sig/android/service/billpay/b;->c()Ljava/util/Hashtable;

    move-result-object v2

    .line 328
    const-string v3, "payeeId"

    invoke-virtual {v2, v3, p0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 329
    const-string v3, "token"

    invoke-virtual {v2, v3, p1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 331
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v3

    invoke-static {v3, v1, v2}, Lcom/chase/sig/android/util/m;->a(Lcom/chase/sig/android/ChaseApplication;Ljava/lang/String;Ljava/util/Hashtable;)Lorg/json/JSONObject;

    move-result-object v1

    .line 332
    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/billpay/DeletePayeeResponse;->b(Lorg/json/JSONObject;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 340
    :goto_0
    return-object v0

    .line 334
    :catch_0
    move-exception v1

    .line 335
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/billpay/DeletePayeeResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    goto :goto_0
.end method

.method private static b(Ljava/util/Hashtable;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 184
    invoke-static {p2}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 185
    invoke-virtual {p0, p1, p2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 187
    :cond_0
    return-void
.end method

.method public static c(Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/service/billpay/BillPayPayeeEditResponse;
    .locals 5
    .parameter
    .parameter

    .prologue
    .line 393
    new-instance v0, Lcom/chase/sig/android/service/billpay/BillPayPayeeEditResponse;

    invoke-direct {v0}, Lcom/chase/sig/android/service/billpay/BillPayPayeeEditResponse;-><init>()V

    .line 396
    const v1, 0x7f07004f

    :try_start_0
    invoke-static {v1}, Lcom/chase/sig/android/service/billpay/b;->a(I)Ljava/lang/String;

    move-result-object v1

    .line 397
    invoke-static {}, Lcom/chase/sig/android/service/billpay/b;->c()Ljava/util/Hashtable;

    move-result-object v2

    .line 398
    const-string v3, "validateOnly"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 399
    const-string v3, "formId"

    invoke-virtual {v2, v3, p0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 400
    const-string v3, "token"

    invoke-virtual {v2, v3, p1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 402
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v3

    invoke-static {v3, v1, v2}, Lcom/chase/sig/android/util/m;->a(Lcom/chase/sig/android/ChaseApplication;Ljava/lang/String;Ljava/util/Hashtable;)Lorg/json/JSONObject;

    move-result-object v1

    .line 403
    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/billpay/BillPayPayeeEditResponse;->b(Lorg/json/JSONObject;)V

    .line 405
    invoke-virtual {v0}, Lcom/chase/sig/android/service/billpay/BillPayPayeeEditResponse;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 421
    :goto_0
    return-object v0

    .line 409
    :cond_0
    const-string v2, "formId"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/service/billpay/BillPayPayeeEditResponse;->a(Ljava/lang/String;)V

    .line 411
    const-string v2, "payee"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 412
    const-string v2, "payeeId"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/service/billpay/BillPayPayeeEditResponse;->b(Ljava/lang/String;)V

    .line 413
    const-string v2, "status"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/service/billpay/BillPayPayeeEditResponse;->e(Ljava/lang/String;)V

    .line 414
    const-string v2, "leadTime"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/billpay/BillPayPayeeEditResponse;->a(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 415
    :catch_0
    move-exception v1

    .line 416
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/billpay/BillPayPayeeEditResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    goto :goto_0
.end method
