.class public final Lcom/chase/sig/android/service/c;
.super Lcom/chase/sig/android/service/JPService;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/chase/sig/android/service/JPService;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/chase/sig/android/service/AccountRewardsDetailResponse;
    .locals 13
    .parameter

    .prologue
    .line 20
    new-instance v3, Lcom/chase/sig/android/service/AccountRewardsDetailResponse;

    invoke-direct {v3}, Lcom/chase/sig/android/service/AccountRewardsDetailResponse;-><init>()V

    .line 22
    const v0, 0x7f070010

    invoke-static {v0}, Lcom/chase/sig/android/service/c;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 23
    invoke-static {}, Lcom/chase/sig/android/service/c;->c()Ljava/util/Hashtable;

    move-result-object v1

    .line 25
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 26
    invoke-virtual {v2, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 27
    new-instance v4, Lorg/json/JSONArray;

    invoke-direct {v4, v2}, Lorg/json/JSONArray;-><init>(Ljava/util/Collection;)V

    .line 29
    const-string v2, "accountIds"

    invoke-virtual {v4}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    :try_start_0
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v2

    invoke-static {v2, v0, v1}, Lcom/chase/sig/android/util/m;->a(Lcom/chase/sig/android/ChaseApplication;Ljava/lang/String;Ljava/util/Hashtable;)Lorg/json/JSONObject;

    move-result-object v0

    .line 34
    invoke-virtual {v3, v0}, Lcom/chase/sig/android/service/AccountRewardsDetailResponse;->b(Lorg/json/JSONObject;)V

    .line 35
    invoke-virtual {v3}, Lcom/chase/sig/android/service/AccountRewardsDetailResponse;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    move-object v0, v3

    .line 84
    :goto_0
    return-object v0

    .line 38
    :cond_0
    const-string v1, "footnote"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/chase/sig/android/service/AccountRewardsDetailResponse;->a(Ljava/lang/String;)V

    .line 40
    const-string v1, "rewards"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v5

    .line 41
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v6

    .line 43
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 46
    const/4 v0, 0x0

    move v4, v0

    :goto_1
    if-ge v4, v6, :cond_6

    .line 47
    new-instance v8, Lcom/chase/sig/android/domain/AccountRewardsDetail;

    invoke-direct {v8}, Lcom/chase/sig/android/domain/AccountRewardsDetail;-><init>()V

    .line 48
    new-instance v9, Ljava/util/Hashtable;

    invoke-direct {v9}, Ljava/util/Hashtable;-><init>()V

    .line 49
    invoke-virtual {v5, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v10

    .line 51
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 52
    if-eqz v10, :cond_5

    .line 53
    invoke-virtual {v10}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v0

    check-cast v0, Ljava/util/Iterator;

    .line 55
    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 57
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 60
    const-string v2, "displayFormat"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 61
    invoke-virtual {v10, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    .line 68
    :goto_3
    invoke-virtual {v9, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    new-instance v12, Lcom/chase/sig/android/domain/RewardDetailValue;

    invoke-direct {v12, v1, v2}, Lcom/chase/sig/android/domain/RewardDetailValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v11, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 81
    :catch_0
    move-exception v0

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/chase/sig/android/service/AccountRewardsDetailResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    :goto_4
    move-object v0, v3

    .line 84
    goto :goto_0

    .line 62
    :cond_1
    :try_start_1
    const-string v2, "failed"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 63
    invoke-virtual {v10, v1}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "true"

    goto :goto_3

    :cond_2
    const-string v2, "false"

    goto :goto_3

    .line 65
    :cond_3
    invoke-virtual {v10, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_3

    .line 71
    :cond_4
    invoke-virtual {v8, v9}, Lcom/chase/sig/android/domain/AccountRewardsDetail;->a(Ljava/util/Hashtable;)V

    .line 72
    invoke-virtual {v8, v11}, Lcom/chase/sig/android/domain/AccountRewardsDetail;->a(Ljava/util/ArrayList;)V

    .line 73
    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 46
    :cond_5
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    .line 78
    :cond_6
    invoke-virtual {v3, v7}, Lcom/chase/sig/android/service/AccountRewardsDetailResponse;->a(Ljava/util/List;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_4
.end method
