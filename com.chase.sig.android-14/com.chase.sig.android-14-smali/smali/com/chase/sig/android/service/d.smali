.class public final Lcom/chase/sig/android/service/d;
.super Lcom/chase/sig/android/service/JPService;
.source "SourceFile"


# static fields
.field public static a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-string v0, "25"

    sput-object v0, Lcom/chase/sig/android/service/d;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/chase/sig/android/service/JPService;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/String;)Landroid/location/Location;
    .locals 3
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 95
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    invoke-static {}, Lcom/chase/sig/android/service/af;->a()Lcom/chase/sig/android/service/n;

    move-result-object v0

    .line 96
    iget-object v2, v0, Lcom/chase/sig/android/service/n;->p:Lcom/chase/sig/android/service/GeocoderService;

    if-nez v2, :cond_0

    new-instance v2, Lcom/chase/sig/android/service/GeocoderService;

    invoke-direct {v2}, Lcom/chase/sig/android/service/GeocoderService;-><init>()V

    iput-object v2, v0, Lcom/chase/sig/android/service/n;->p:Lcom/chase/sig/android/service/GeocoderService;

    :cond_0
    iget-object v0, v0, Lcom/chase/sig/android/service/n;->p:Lcom/chase/sig/android/service/GeocoderService;

    .line 100
    :try_start_0
    invoke-virtual {v0, p0}, Lcom/chase/sig/android/service/GeocoderService;->a(Ljava/lang/String;)Lcom/chase/sig/android/service/GeocoderService$GeoLocationResponse;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 103
    :goto_0
    invoke-virtual {v0}, Lcom/chase/sig/android/service/GeocoderService$GeoLocationResponse;->a()Landroid/location/Location;

    move-result-object v0

    .line 108
    instance-of v2, v0, Landroid/location/Location;

    if-eqz v2, :cond_1

    check-cast v0, Landroid/location/Location;

    :goto_1
    return-object v0

    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method public static a(Landroid/location/Location;Ljava/lang/String;)Lcom/chase/sig/android/service/BranchLocateResponse;
    .locals 8
    .parameter
    .parameter

    .prologue
    .line 37
    new-instance v0, Lcom/chase/sig/android/service/BranchLocateResponse;

    invoke-direct {v0}, Lcom/chase/sig/android/service/BranchLocateResponse;-><init>()V

    .line 39
    invoke-static {p1}, Lcom/chase/sig/android/util/s;->n(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 40
    invoke-static {p1}, Lcom/chase/sig/android/service/d;->a(Ljava/lang/String;)Landroid/location/Location;

    move-result-object p0

    .line 43
    :cond_0
    if-nez p0, :cond_1

    .line 90
    :goto_0
    return-object v0

    .line 48
    :cond_1
    const v1, 0x7f07005b

    :try_start_0
    invoke-static {v1}, Lcom/chase/sig/android/service/d;->a(I)Ljava/lang/String;

    move-result-object v1

    .line 50
    new-instance v2, Ljava/util/Hashtable;

    invoke-direct {v2}, Ljava/util/Hashtable;-><init>()V

    .line 51
    if-eqz p0, :cond_2

    .line 52
    const-string v3, "lat"

    new-instance v4, Ljava/lang/Float;

    invoke-virtual {p0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v5

    invoke-direct {v4, v5, v6}, Ljava/lang/Float;-><init>(D)V

    invoke-virtual {v4}, Ljava/lang/Float;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    const-string v3, "lng"

    new-instance v4, Ljava/lang/Float;

    invoke-virtual {p0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v5

    invoke-direct {v4, v5, v6}, Ljava/lang/Float;-><init>(D)V

    invoke-virtual {v4}, Ljava/lang/Float;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    :cond_2
    const-string v3, "maxCount"

    sget-object v4, Lcom/chase/sig/android/service/d;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    invoke-static {p1}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 59
    const-string v3, "address"

    invoke-virtual {v2, v3, p1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    :cond_3
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v3

    invoke-static {v3, v1, v2}, Lcom/chase/sig/android/util/m;->a(Lcom/chase/sig/android/ChaseApplication;Ljava/lang/String;Ljava/util/Hashtable;)Lorg/json/JSONObject;

    move-result-object v1

    .line 64
    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/BranchLocateResponse;->b(Lorg/json/JSONObject;)V

    .line 66
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 68
    const-string v3, "locations"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 70
    if-eqz v3, :cond_4

    .line 72
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v1, v4, :cond_4

    .line 73
    invoke-virtual {v3, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 74
    new-instance v5, Lcom/chase/sig/android/domain/BranchLocation;

    invoke-direct {v5}, Lcom/chase/sig/android/domain/BranchLocation;-><init>()V

    const-string v6, "access"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/chase/sig/android/domain/BranchLocation;->k(Ljava/lang/String;)V

    const-string v6, "address"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/chase/sig/android/domain/BranchLocation;->a(Ljava/lang/String;)V

    const-string v6, "atms"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/chase/sig/android/domain/BranchLocation;->b(Ljava/lang/String;)V

    const-string v6, "bank"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/chase/sig/android/domain/BranchLocation;->c(Ljava/lang/String;)V

    const-string v6, "city"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/chase/sig/android/domain/BranchLocation;->d(Ljava/lang/String;)V

    const-string v6, "distance"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;)D

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Lcom/chase/sig/android/domain/BranchLocation;->a(D)V

    const-string v6, "label"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/chase/sig/android/domain/BranchLocation;->e(Ljava/lang/String;)V

    const-string v6, "lat"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;)D

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Lcom/chase/sig/android/domain/BranchLocation;->b(D)V

    const-string v6, "lng"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;)D

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Lcom/chase/sig/android/domain/BranchLocation;->c(D)V

    const-string v6, "locType"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/chase/sig/android/domain/BranchLocation;->f(Ljava/lang/String;)V

    const-string v6, "name"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/chase/sig/android/domain/BranchLocation;->g(Ljava/lang/String;)V

    const-string v6, "phone"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/chase/sig/android/domain/BranchLocation;->h(Ljava/lang/String;)V

    const-string v6, "state"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/chase/sig/android/domain/BranchLocation;->i(Ljava/lang/String;)V

    const-string v6, "type"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/chase/sig/android/domain/BranchLocation;->l(Ljava/lang/String;)V

    const-string v6, "zip"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/chase/sig/android/domain/BranchLocation;->j(Ljava/lang/String;)V

    const-string v6, "atms"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/chase/sig/android/domain/BranchLocation;->b(Ljava/lang/String;)V

    const-string v6, "driveUpHrs"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v6

    invoke-static {v6}, Lcom/chase/sig/android/service/d;->a(Lorg/json/JSONArray;)Ljava/util/List;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/chase/sig/android/domain/BranchLocation;->a(Ljava/util/List;)V

    const-string v6, "lobbyHrs"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v6

    invoke-static {v6}, Lcom/chase/sig/android/service/d;->a(Lorg/json/JSONArray;)Ljava/util/List;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/chase/sig/android/domain/BranchLocation;->b(Ljava/util/List;)V

    const-string v6, "services"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v6

    invoke-static {v6}, Lcom/chase/sig/android/service/d;->a(Lorg/json/JSONArray;)Ljava/util/List;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/chase/sig/android/domain/BranchLocation;->c(Ljava/util/List;)V

    const-string v6, "languages"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    invoke-static {v4}, Lcom/chase/sig/android/service/d;->a(Lorg/json/JSONArray;)Ljava/util/List;

    move-result-object v4

    invoke-virtual {v5, v4}, Lcom/chase/sig/android/domain/BranchLocation;->d(Ljava/util/List;)V

    .line 75
    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 72
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_1

    .line 79
    :cond_4
    invoke-virtual {v0, v2}, Lcom/chase/sig/android/service/BranchLocateResponse;->a(Ljava/util/List;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/chase/sig/android/util/ChaseException; {:try_start_0 .. :try_end_0} :catch_1

    goto/16 :goto_0

    .line 82
    :catch_0
    move-exception v1

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/BranchLocateResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    goto/16 :goto_0

    .line 86
    :catch_1
    move-exception v1

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/BranchLocateResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    goto/16 :goto_0
.end method

.method private static a(Lorg/json/JSONArray;)Ljava/util/List;
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONArray;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 153
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 154
    if-nez p0, :cond_0

    move-object v0, v1

    .line 162
    :goto_0
    return-object v0

    .line 158
    :cond_0
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p0}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 159
    const-string v2, ""

    invoke-virtual {p0, v0, v2}, Lorg/json/JSONArray;->optString(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 158
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 162
    goto :goto_0
.end method
