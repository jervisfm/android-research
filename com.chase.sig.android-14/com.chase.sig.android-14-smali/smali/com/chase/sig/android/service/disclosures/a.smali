.class public final Lcom/chase/sig/android/service/disclosures/a;
.super Lcom/chase/sig/android/service/JPService;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/chase/sig/android/service/JPService;-><init>()V

    return-void
.end method

.method public static a()Lcom/chase/sig/android/service/disclosures/DisclosuresResponse;
    .locals 8

    .prologue
    .line 18
    new-instance v1, Lcom/chase/sig/android/service/disclosures/DisclosuresResponse;

    invoke-direct {v1}, Lcom/chase/sig/android/service/disclosures/DisclosuresResponse;-><init>()V

    .line 19
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 21
    const v0, 0x7f070054

    invoke-static {v0}, Lcom/chase/sig/android/service/disclosures/a;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 24
    :try_start_0
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v3

    invoke-static {}, Lcom/chase/sig/android/service/disclosures/a;->c()Ljava/util/Hashtable;

    move-result-object v4

    invoke-static {v3, v0, v4}, Lcom/chase/sig/android/util/m;->a(Lcom/chase/sig/android/ChaseApplication;Ljava/lang/String;Ljava/util/Hashtable;)Lorg/json/JSONObject;

    move-result-object v0

    .line 26
    invoke-virtual {v1, v0}, Lcom/chase/sig/android/service/disclosures/DisclosuresResponse;->b(Lorg/json/JSONObject;)V

    .line 28
    const-string v3, "disclosures"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 30
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v4

    .line 31
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v4, :cond_0

    .line 33
    invoke-virtual {v3, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    .line 35
    new-instance v6, Lcom/chase/sig/android/domain/Disclosure;

    invoke-direct {v6}, Lcom/chase/sig/android/domain/Disclosure;-><init>()V

    .line 36
    const-string v7, "id"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/chase/sig/android/domain/Disclosure;->a(Ljava/lang/String;)V

    .line 37
    const-string v7, "title"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/chase/sig/android/domain/Disclosure;->c(Ljava/lang/String;)V

    .line 38
    const-string v7, "text"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Lcom/chase/sig/android/domain/Disclosure;->b(Ljava/lang/String;)V

    .line 40
    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 31
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 43
    :cond_0
    invoke-virtual {v1, v2}, Lcom/chase/sig/android/service/disclosures/DisclosuresResponse;->a(Ljava/util/List;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 50
    :goto_1
    return-object v1

    .line 46
    :catch_0
    move-exception v0

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/service/disclosures/DisclosuresResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    goto :goto_1
.end method
