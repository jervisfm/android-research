.class public final Lcom/chase/sig/android/service/e;
.super Lcom/chase/sig/android/service/JPService;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/chase/sig/android/service/JPService;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/chase/sig/android/service/CreditFacilityAccountDetailResponse;
    .locals 9
    .parameter

    .prologue
    .line 24
    new-instance v0, Lcom/chase/sig/android/service/CreditFacilityAccountDetailResponse;

    invoke-direct {v0}, Lcom/chase/sig/android/service/CreditFacilityAccountDetailResponse;-><init>()V

    .line 25
    new-instance v2, Lcom/chase/sig/android/domain/CreditFacilityAccountDetail;

    invoke-direct {v2}, Lcom/chase/sig/android/domain/CreditFacilityAccountDetail;-><init>()V

    .line 26
    const v1, 0x7f07000f

    invoke-static {v1}, Lcom/chase/sig/android/service/e;->a(I)Ljava/lang/String;

    move-result-object v1

    .line 28
    invoke-static {}, Lcom/chase/sig/android/service/e;->c()Ljava/util/Hashtable;

    move-result-object v3

    .line 29
    const-string v4, "accountId"

    invoke-virtual {v3, v4, p0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    :try_start_0
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v4

    invoke-static {v4, v1, v3}, Lcom/chase/sig/android/util/m;->a(Lcom/chase/sig/android/ChaseApplication;Ljava/lang/String;Ljava/util/Hashtable;)Lorg/json/JSONObject;

    move-result-object v1

    .line 33
    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/CreditFacilityAccountDetailResponse;->b(Lorg/json/JSONObject;)V

    .line 34
    invoke-virtual {v0}, Lcom/chase/sig/android/service/CreditFacilityAccountDetailResponse;->e()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 60
    :goto_0
    return-object v0

    .line 37
    :cond_0
    const-string v3, "creditFacilitySummaryRes"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 39
    new-instance v3, Lcom/chase/sig/android/util/Dollar;

    const-string v4, "creditLimit"

    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/chase/sig/android/util/Dollar;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Lcom/chase/sig/android/domain/CreditFacilityAccountDetail;->a(Lcom/chase/sig/android/util/Dollar;)V

    .line 41
    new-instance v3, Lcom/chase/sig/android/util/Dollar;

    const-string v4, "available"

    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/chase/sig/android/util/Dollar;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Lcom/chase/sig/android/domain/CreditFacilityAccountDetail;->b(Lcom/chase/sig/android/util/Dollar;)V

    .line 42
    const-string v3, "displayType"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/chase/sig/android/domain/CreditFacilityAccountDetail;->a(Ljava/lang/String;)V

    .line 43
    const-string v3, "mask"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/chase/sig/android/domain/CreditFacilityAccountDetail;->b(Ljava/lang/String;)V

    .line 44
    const-string v3, "name"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/chase/sig/android/domain/CreditFacilityAccountDetail;->c(Ljava/lang/String;)V

    .line 46
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 49
    const-string v4, "details"

    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    .line 50
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v1, v5, :cond_1

    .line 51
    invoke-virtual {v4, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    new-instance v6, Lcom/chase/sig/android/domain/CreditFacilityAccountLoanDetail;

    invoke-direct {v6}, Lcom/chase/sig/android/domain/CreditFacilityAccountLoanDetail;-><init>()V

    const-string v7, "loanNumMask"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/chase/sig/android/domain/CreditFacilityAccountLoanDetail;->a(Ljava/lang/String;)V

    const-string v7, "loanType"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/chase/sig/android/domain/CreditFacilityAccountLoanDetail;->b(Ljava/lang/String;)V

    const-string v7, "intRate"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;)D

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/chase/sig/android/domain/CreditFacilityAccountLoanDetail;->a(Ljava/lang/Double;)V

    const-string v7, "maturityDate"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/chase/sig/android/domain/CreditFacilityAccountLoanDetail;->c(Ljava/lang/String;)V

    const-string v7, "outstanding"

    invoke-static {v5, v7}, Lcom/chase/sig/android/service/e;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/Hashtable;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/chase/sig/android/domain/CreditFacilityAccountLoanDetail;->a(Ljava/util/Hashtable;)V

    const-string v7, "accruedInt"

    invoke-static {v5, v7}, Lcom/chase/sig/android/service/e;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/Hashtable;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/chase/sig/android/domain/CreditFacilityAccountLoanDetail;->b(Ljava/util/Hashtable;)V

    const-string v7, "loanType"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Lcom/chase/sig/android/domain/CreditFacilityAccountLoanDetail;->b(Ljava/lang/String;)V

    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 50
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 53
    :cond_1
    invoke-virtual {v2, v3}, Lcom/chase/sig/android/domain/CreditFacilityAccountDetail;->a(Ljava/util/List;)V

    .line 55
    invoke-virtual {v0, v2}, Lcom/chase/sig/android/service/CreditFacilityAccountDetailResponse;->a(Lcom/chase/sig/android/domain/CreditFacilityAccountDetail;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 57
    :catch_0
    move-exception v1

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/CreditFacilityAccountDetailResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    goto/16 :goto_0
.end method

.method private static a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/util/Hashtable;
    .locals 5
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 78
    new-instance v2, Ljava/util/Hashtable;

    invoke-direct {v2}, Ljava/util/Hashtable;-><init>()V

    .line 79
    invoke-virtual {p0, p1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    .line 80
    if-eqz v3, :cond_0

    .line 81
    invoke-virtual {v3}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v0

    check-cast v0, Ljava/util/Iterator;

    .line 82
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 83
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 84
    invoke-virtual {v3, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 85
    invoke-virtual {v2, v1, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 88
    :cond_0
    return-object v2
.end method
