.class public Lcom/chase/sig/android/service/epay/EPayTransaction;
.super Lcom/chase/sig/android/domain/Transaction;
.source "SourceFile"


# instance fields
.field private description:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/chase/sig/android/domain/Transaction;-><init>()V

    return-void
.end method


# virtual methods
.method public final B()Ljava/lang/String;
    .locals 4

    .prologue
    .line 29
    const-string v0, "From %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/chase/sig/android/domain/Transaction;->fromAccountMask:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lcom/chase/sig/android/service/epay/EPayTransaction;->description:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 15
    iput-object p1, p0, Lcom/chase/sig/android/service/epay/EPayTransaction;->description:Ljava/lang/String;

    .line 16
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 19
    iput-object p1, p0, Lcom/chase/sig/android/domain/Transaction;->deliverByDate:Ljava/lang/String;

    .line 20
    return-void
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/chase/sig/android/domain/Transaction;->toAccountMask:Ljava/lang/String;

    return-object v0
.end method
