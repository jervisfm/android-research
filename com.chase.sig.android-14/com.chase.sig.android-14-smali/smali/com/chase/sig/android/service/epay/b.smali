.class public final Lcom/chase/sig/android/service/epay/b;
.super Lcom/chase/sig/android/service/JPService;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/chase/sig/android/service/JPService;-><init>()V

    return-void
.end method

.method public static a(Lcom/chase/sig/android/domain/EPayment;)Lcom/chase/sig/android/service/GenericResponse;
    .locals 4
    .parameter

    .prologue
    .line 29
    const v0, 0x7f070033

    invoke-static {v0}, Lcom/chase/sig/android/service/epay/b;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 31
    invoke-static {}, Lcom/chase/sig/android/service/epay/b;->c()Ljava/util/Hashtable;

    move-result-object v1

    .line 32
    const-string v2, "userId"

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v3

    iget-object v3, v3, Lcom/chase/sig/android/domain/o;->a:Lcom/chase/sig/android/domain/a;

    iget-object v3, v3, Lcom/chase/sig/android/domain/a;->c:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    const-string v2, "accountId"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/EPayment;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    const-string v2, "amount"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/EPayment;->d()Lcom/chase/sig/android/util/Dollar;

    move-result-object v3

    invoke-virtual {v3}, Lcom/chase/sig/android/util/Dollar;->b()Ljava/math/BigDecimal;

    move-result-object v3

    invoke-virtual {v3}, Ljava/math/BigDecimal;->toPlainString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    const-string v2, "date"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/EPayment;->e()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/chase/sig/android/util/s;->t(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v3

    invoke-static {v3}, Lcom/chase/sig/android/util/s;->b(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    const-string v2, "epayAccountId"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/EPayment;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    const-string v2, "optionId"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/EPayment;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    new-instance v2, Lcom/chase/sig/android/service/GenericResponse;

    invoke-direct {v2}, Lcom/chase/sig/android/service/GenericResponse;-><init>()V

    .line 43
    :try_start_0
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v3

    invoke-static {v3, v0, v1}, Lcom/chase/sig/android/util/m;->a(Lcom/chase/sig/android/ChaseApplication;Ljava/lang/String;Ljava/util/Hashtable;)Lorg/json/JSONObject;

    move-result-object v0

    .line 44
    invoke-virtual {v2, v0}, Lcom/chase/sig/android/service/GenericResponse;->b(Lorg/json/JSONObject;)V

    .line 46
    const-string v1, "formId"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/domain/EPayment;->b(Ljava/lang/String;)V

    .line 48
    const-string v1, "agreement"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 49
    if-eqz v0, :cond_0

    .line 50
    const-string v1, "content"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\r"

    const-string v3, ""

    invoke-virtual {v0, v1, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 51
    invoke-virtual {p0, v0}, Lcom/chase/sig/android/domain/EPayment;->g(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 59
    :cond_0
    :goto_0
    return-object v2

    .line 55
    :catch_0
    move-exception v0

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/chase/sig/android/service/GenericResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Lcom/chase/sig/android/service/epay/EPayAccountsAndPaymentOptionsResponse;
    .locals 11
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 64
    const v0, 0x7f070032

    invoke-static {v0}, Lcom/chase/sig/android/service/epay/b;->a(I)Ljava/lang/String;

    move-result-object v2

    .line 66
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 67
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 69
    invoke-static {}, Lcom/chase/sig/android/service/epay/b;->c()Ljava/util/Hashtable;

    move-result-object v5

    .line 70
    const-string v0, "accountId"

    invoke-virtual {v5, v0, p0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    new-instance v0, Lcom/chase/sig/android/service/epay/EPayAccountsAndPaymentOptionsResponse;

    invoke-direct {v0}, Lcom/chase/sig/android/service/epay/EPayAccountsAndPaymentOptionsResponse;-><init>()V

    .line 76
    :try_start_0
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v6

    invoke-static {v6, v2, v5}, Lcom/chase/sig/android/util/m;->a(Lcom/chase/sig/android/ChaseApplication;Ljava/lang/String;Ljava/util/Hashtable;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lcom/chase/sig/android/util/ChaseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 84
    :try_start_1
    invoke-virtual {v0, v5}, Lcom/chase/sig/android/service/epay/EPayAccountsAndPaymentOptionsResponse;->b(Lorg/json/JSONObject;)V

    .line 86
    invoke-virtual {v0}, Lcom/chase/sig/android/service/epay/EPayAccountsAndPaymentOptionsResponse;->e()Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v2

    if-eqz v2, :cond_0

    .line 137
    :goto_0
    return-object v0

    .line 78
    :catch_0
    move-exception v1

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/epay/EPayAccountsAndPaymentOptionsResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    goto :goto_0

    .line 90
    :cond_0
    :try_start_2
    const-string v2, "date"

    invoke-virtual {v5, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/service/epay/EPayAccountsAndPaymentOptionsResponse;->a(Ljava/lang/String;)V

    .line 92
    const-string v2, "accounts"

    invoke-virtual {v5, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v6

    .line 93
    invoke-virtual {v6}, Lorg/json/JSONArray;->length()I

    move-result v7

    move v2, v1

    .line 95
    :goto_1
    if-ge v2, v7, :cond_1

    .line 96
    invoke-virtual {v6, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v8

    .line 98
    new-instance v9, Lcom/chase/sig/android/domain/EPayAccount;

    invoke-direct {v9}, Lcom/chase/sig/android/domain/EPayAccount;-><init>()V

    .line 99
    const-string v10, "id"

    invoke-virtual {v8, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/chase/sig/android/domain/EPayAccount;->b(Ljava/lang/String;)V

    .line 100
    const-string v10, "mask"

    invoke-virtual {v8, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v9, v8}, Lcom/chase/sig/android/domain/EPayAccount;->a(Ljava/lang/String;)V

    .line 102
    invoke-virtual {v3, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 95
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 105
    :cond_1
    const-string v2, "options"

    invoke-virtual {v5, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 106
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v5

    .line 108
    :goto_2
    if-ge v1, v5, :cond_3

    .line 109
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    .line 111
    new-instance v7, Lcom/chase/sig/android/domain/EPaymentOption;

    invoke-direct {v7}, Lcom/chase/sig/android/domain/EPaymentOption;-><init>()V

    .line 113
    const-string v8, "amountRequired"

    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v8

    invoke-virtual {v7, v8}, Lcom/chase/sig/android/domain/EPaymentOption;->a(Z)V

    .line 114
    const-string v8, "description"

    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/chase/sig/android/domain/EPaymentOption;->c(Ljava/lang/String;)V

    .line 115
    const-string v8, "shortDescription"

    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/chase/sig/android/domain/EPaymentOption;->a(Ljava/lang/String;)V

    .line 116
    const-string v8, "label"

    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/chase/sig/android/domain/EPaymentOption;->b(Ljava/lang/String;)V

    .line 117
    const-string v8, "note"

    invoke-static {v6, v8}, Lcom/chase/sig/android/util/s;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/chase/sig/android/domain/EPaymentOption;->d(Ljava/lang/String;)V

    .line 118
    const-string v8, "id"

    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/chase/sig/android/domain/EPaymentOption;->e(Ljava/lang/String;)V

    .line 120
    const-string v8, "amount"

    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 121
    invoke-static {v6}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 122
    new-instance v8, Lcom/chase/sig/android/util/Dollar;

    invoke-direct {v8, v6}, Lcom/chase/sig/android/util/Dollar;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v8}, Lcom/chase/sig/android/domain/EPaymentOption;->a(Lcom/chase/sig/android/util/Dollar;)V

    .line 125
    :cond_2
    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    .line 108
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 129
    :catch_1
    move-exception v1

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/epay/EPayAccountsAndPaymentOptionsResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    goto/16 :goto_0

    .line 134
    :cond_3
    invoke-virtual {v0, v3}, Lcom/chase/sig/android/service/epay/EPayAccountsAndPaymentOptionsResponse;->a(Ljava/util/List;)V

    .line 135
    invoke-virtual {v0, v4}, Lcom/chase/sig/android/service/epay/EPayAccountsAndPaymentOptionsResponse;->c(Ljava/util/List;)V

    goto/16 :goto_0
.end method

.method public static b(Lcom/chase/sig/android/domain/EPayment;)Lcom/chase/sig/android/service/GenericResponse;
    .locals 4
    .parameter

    .prologue
    .line 142
    const v0, 0x7f070030

    invoke-static {v0}, Lcom/chase/sig/android/service/epay/b;->a(I)Ljava/lang/String;

    move-result-object v1

    .line 144
    invoke-static {}, Lcom/chase/sig/android/service/epay/b;->c()Ljava/util/Hashtable;

    move-result-object v2

    .line 146
    const-string v0, "formId"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/EPayment;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 149
    new-instance v0, Lcom/chase/sig/android/service/GenericResponse;

    invoke-direct {v0}, Lcom/chase/sig/android/service/GenericResponse;-><init>()V

    .line 152
    :try_start_0
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v3

    invoke-static {v3, v1, v2}, Lcom/chase/sig/android/util/m;->a(Lcom/chase/sig/android/ChaseApplication;Ljava/lang/String;Ljava/util/Hashtable;)Lorg/json/JSONObject;

    move-result-object v1

    .line 154
    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/GenericResponse;->b(Lorg/json/JSONObject;)V

    .line 155
    const-string v2, "confNum"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/domain/EPayment;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/chase/sig/android/util/ChaseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 163
    :goto_0
    return-object v0

    .line 158
    :catch_0
    move-exception v1

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/GenericResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    goto :goto_0
.end method
