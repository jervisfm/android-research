.class public final Lcom/chase/sig/android/service/g;
.super Lcom/chase/sig/android/service/movemoney/f;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/service/movemoney/f",
        "<",
        "Lcom/chase/sig/android/service/epay/EPayTransaction;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/chase/sig/android/service/movemoney/f;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a(Lorg/json/JSONObject;)Lcom/chase/sig/android/domain/Transaction;
    .locals 3
    .parameter

    .prologue
    .line 11
    new-instance v0, Lcom/chase/sig/android/service/epay/EPayTransaction;

    invoke-direct {v0}, Lcom/chase/sig/android/service/epay/EPayTransaction;-><init>()V

    const-string v1, "cancellable"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/epay/EPayTransaction;->b(Z)V

    new-instance v1, Lcom/chase/sig/android/util/Dollar;

    const-string v2, "amount"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/chase/sig/android/util/Dollar;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/epay/EPayTransaction;->a(Lcom/chase/sig/android/util/Dollar;)V

    const-string v1, "fundingAccountMask"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/epay/EPayTransaction;->p(Ljava/lang/String;)V

    const-string v1, "fundingAccountId"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/epay/EPayTransaction;->b(Ljava/lang/String;)V

    const-string v1, "paymentDate"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/epay/EPayTransaction;->c(Ljava/lang/String;)V

    const-string v1, "description"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/epay/EPayTransaction;->a(Ljava/lang/String;)V

    const-string v1, "confNum"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/epay/EPayTransaction;->k(Ljava/lang/String;)V

    const-string v1, "statusCode"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/epay/EPayTransaction;->q(Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(Lcom/chase/sig/android/service/movemoney/ServiceResponse;Lorg/json/JSONObject;)Lcom/chase/sig/android/service/movemoney/ServiceResponse;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 34
    const/4 v0, 0x0

    return-object v0
.end method
