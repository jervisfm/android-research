.class public final Lcom/chase/sig/android/service/j;
.super Lcom/chase/sig/android/service/JPService;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/chase/sig/android/service/JPService;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;ZLjava/util/HashMap;)Lcom/chase/sig/android/domain/ImageDownloadResponse;
    .locals 7
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/chase/sig/android/domain/ImageDownloadResponse;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 33
    new-instance v0, Lcom/chase/sig/android/domain/ImageDownloadResponse;

    invoke-direct {v0}, Lcom/chase/sig/android/domain/ImageDownloadResponse;-><init>()V

    .line 35
    :try_start_0
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v3

    new-instance v5, Lcom/chase/sig/android/util/f;

    invoke-direct {v5, v3, p2}, Lcom/chase/sig/android/util/f;-><init>(Lcom/chase/sig/android/ChaseApplication;Ljava/util/HashMap;)V

    invoke-virtual {v5, p0}, Lcom/chase/sig/android/util/f;->a(Ljava/lang/String;)Lorg/apache/http/HttpResponse;

    move-result-object v3

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v3, v5, v6

    if-nez v3, :cond_0

    new-instance v1, Lcom/chase/sig/android/util/ChaseException;

    sget v2, Lcom/chase/sig/android/util/ChaseException;->b:I

    const-string v3, "Did not receive response from %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p0, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/chase/sig/android/util/ChaseException;-><init>(ILjava/lang/String;)V

    throw v1
    :try_end_0
    .catch Lcom/chase/sig/android/util/ChaseException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 60
    :catch_0
    move-exception v1

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/ImageDownloadResponse;->b(Landroid/content/Context;)V

    .line 64
    :goto_0
    return-object v0

    .line 36
    :cond_0
    :try_start_1
    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v5

    invoke-interface {v5}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v5

    .line 38
    const/16 v6, 0xc8

    if-eq v5, v6, :cond_1

    .line 39
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/ImageDownloadResponse;->b(Landroid/content/Context;)V
    :try_end_1
    .catch Lcom/chase/sig/android/util/ChaseException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 62
    :catch_1
    move-exception v1

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/ImageDownloadResponse;->b(Landroid/content/Context;)V

    goto :goto_0

    .line 46
    :cond_1
    :try_start_2
    invoke-interface {v3}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v3

    .line 48
    if-eqz p1, :cond_2

    if-eqz v3, :cond_4

    :try_start_3
    invoke-interface {v3}, Lorg/apache/http/HttpEntity;->getContentType()Lorg/apache/http/Header;

    move-result-object v5

    invoke-interface {v5}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v5

    const-string v6, "image/"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_4

    move v5, v1

    :goto_1
    if-eqz v5, :cond_5

    .line 49
    :cond_2
    invoke-interface {v3}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v2

    .line 50
    invoke-static {v2}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 51
    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/ImageDownloadResponse;->a(Landroid/graphics/Bitmap;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 56
    :cond_3
    :try_start_4
    invoke-static {v2}, Lcom/chase/sig/android/service/j;->a(Ljava/io/Closeable;)V

    .line 57
    invoke-static {v3}, Lcom/chase/sig/android/service/j;->a(Lorg/apache/http/HttpEntity;)V
    :try_end_4
    .catch Lcom/chase/sig/android/util/ChaseException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    :cond_4
    move v5, v4

    .line 48
    goto :goto_1

    .line 52
    :cond_5
    if-eqz v3, :cond_6

    :try_start_5
    invoke-static {v3}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "Cache retrieval failed"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_6

    :goto_2
    if-eqz v1, :cond_3

    .line 53
    sget-object v1, Lcom/chase/sig/android/domain/ImageDownloadResponse;->a:Lcom/chase/sig/android/domain/ImageDownloadResponse;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 56
    const/4 v2, 0x0

    :try_start_6
    invoke-static {v2}, Lcom/chase/sig/android/service/j;->a(Ljava/io/Closeable;)V

    .line 57
    invoke-static {v3}, Lcom/chase/sig/android/service/j;->a(Lorg/apache/http/HttpEntity;)V

    move-object v0, v1

    goto :goto_0

    :cond_6
    move v1, v4

    .line 52
    goto :goto_2

    .line 56
    :catchall_0
    move-exception v1

    move-object v3, v2

    :goto_3
    invoke-static {v2}, Lcom/chase/sig/android/service/j;->a(Ljava/io/Closeable;)V

    .line 57
    invoke-static {v3}, Lcom/chase/sig/android/service/j;->a(Lorg/apache/http/HttpEntity;)V

    throw v1
    :try_end_6
    .catch Lcom/chase/sig/android/util/ChaseException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    .line 56
    :catchall_1
    move-exception v1

    goto :goto_3
.end method

.method private static a(Ljava/io/Closeable;)V
    .locals 0
    .parameter

    .prologue
    .line 68
    if-eqz p0, :cond_0

    .line 69
    invoke-interface {p0}, Ljava/io/Closeable;->close()V

    .line 71
    :cond_0
    return-void
.end method

.method private static a(Lorg/apache/http/HttpEntity;)V
    .locals 0
    .parameter

    .prologue
    .line 74
    if-eqz p0, :cond_0

    .line 75
    invoke-interface {p0}, Lorg/apache/http/HttpEntity;->consumeContent()V

    .line 77
    :cond_0
    return-void
.end method
