.class public final Lcom/chase/sig/android/service/k;
.super Lcom/chase/sig/android/service/JPService;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/chase/sig/android/service/JPService;-><init>()V

    return-void
.end method

.method private static a(Lorg/json/JSONObject;)Lcom/chase/sig/android/domain/InvestmentTransaction;
    .locals 7
    .parameter

    .prologue
    .line 125
    new-instance v2, Lcom/chase/sig/android/domain/InvestmentTransaction;

    invoke-direct {v2}, Lcom/chase/sig/android/domain/InvestmentTransaction;-><init>()V

    .line 127
    const-string v0, "description"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 128
    invoke-static {v0}, Lcom/chase/sig/android/util/s;->l(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 129
    const-string v0, ""

    .line 131
    :cond_0
    invoke-virtual {v2, v0}, Lcom/chase/sig/android/domain/InvestmentTransaction;->d(Ljava/lang/String;)V

    .line 133
    const-string v0, "values"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 135
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-ge v1, v0, :cond_d

    .line 136
    invoke-virtual {v3, v1}, Lorg/json/JSONArray;->opt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/json/JSONObject;

    .line 138
    const-string v4, "fieldId"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "amount"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 139
    new-instance v4, Lcom/chase/sig/android/util/Dollar;

    invoke-static {v0}, Lcom/chase/sig/android/service/k;->b(Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object v5

    const-string v6, "value"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/chase/sig/android/util/Dollar;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Lcom/chase/sig/android/domain/InvestmentTransaction;->a(Lcom/chase/sig/android/util/Dollar;)V

    .line 140
    const-string v4, "value"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "currency"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/chase/sig/android/domain/InvestmentTransaction;->h(Ljava/lang/String;)V

    .line 144
    :cond_1
    const-string v4, "fieldId"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "date"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 145
    const-string v4, "value"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/chase/sig/android/domain/InvestmentTransaction;->a(Ljava/lang/String;)V

    .line 148
    :cond_2
    const-string v4, "fieldId"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "is-intraday"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 149
    const-string v4, "value"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v4

    invoke-virtual {v2, v4}, Lcom/chase/sig/android/domain/InvestmentTransaction;->a(Z)V

    .line 152
    :cond_3
    const-string v4, "fieldId"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "as-of-date"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 153
    const-string v4, "value"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/chase/sig/android/domain/InvestmentTransaction;->b(Ljava/lang/String;)V

    .line 156
    :cond_4
    const-string v4, "fieldId"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "quantity"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 157
    const-string v4, "value"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;)D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/chase/sig/android/domain/InvestmentTransaction;->a(D)V

    .line 160
    :cond_5
    const-string v4, "fieldId"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "is-trade"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 161
    const-string v4, "value"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v4

    invoke-virtual {v2, v4}, Lcom/chase/sig/android/domain/InvestmentTransaction;->b(Z)V

    .line 164
    :cond_6
    const-string v4, "fieldId"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "trans-type"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 165
    const-string v4, "value"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/chase/sig/android/domain/InvestmentTransaction;->c(Ljava/lang/String;)V

    .line 168
    :cond_7
    const-string v4, "fieldId"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "settle-date"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 169
    const-string v4, "value"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/chase/sig/android/domain/InvestmentTransaction;->f(Ljava/lang/String;)V

    .line 172
    :cond_8
    const-string v4, "fieldId"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "trade-date"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 173
    const-string v4, "value"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/chase/sig/android/domain/InvestmentTransaction;->e(Ljava/lang/String;)V

    .line 176
    :cond_9
    const-string v4, "fieldId"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "symbol"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 177
    const-string v4, "value"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/chase/sig/android/domain/InvestmentTransaction;->g(Ljava/lang/String;)V

    .line 180
    :cond_a
    const-string v4, "fieldId"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "price"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 181
    new-instance v4, Lcom/chase/sig/android/util/Dollar;

    invoke-static {v0}, Lcom/chase/sig/android/service/k;->b(Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object v5

    const-string v6, "value"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/chase/sig/android/util/Dollar;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Lcom/chase/sig/android/domain/InvestmentTransaction;->b(Lcom/chase/sig/android/util/Dollar;)V

    .line 184
    :cond_b
    const-string v4, "fieldId"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "cost"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 185
    new-instance v4, Lcom/chase/sig/android/util/Dollar;

    invoke-static {v0}, Lcom/chase/sig/android/service/k;->b(Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object v5

    const-string v6, "value"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/chase/sig/android/util/Dollar;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Lcom/chase/sig/android/domain/InvestmentTransaction;->c(Lcom/chase/sig/android/util/Dollar;)V

    .line 186
    const-string v4, "value"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v4, "currency"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/chase/sig/android/domain/InvestmentTransaction;->i(Ljava/lang/String;)V

    .line 135
    :cond_c
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_0

    .line 191
    :cond_d
    return-object v2
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Lcom/chase/sig/android/domain/InvestmentTransaction$FilterType;)Lcom/chase/sig/android/service/InvestmentTransactionsResponse;
    .locals 10
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 35
    const v1, 0x7f070050

    invoke-static {v1}, Lcom/chase/sig/android/service/k;->a(I)Ljava/lang/String;

    move-result-object v1

    .line 37
    new-instance v2, Lcom/chase/sig/android/service/InvestmentTransactionsResponse;

    invoke-direct {v2}, Lcom/chase/sig/android/service/InvestmentTransactionsResponse;-><init>()V

    .line 39
    invoke-static {}, Lcom/chase/sig/android/service/k;->c()Ljava/util/Hashtable;

    move-result-object v3

    .line 41
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v4

    invoke-virtual {v4}, Lcom/chase/sig/android/ChaseApplication;->j()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 42
    const-string v4, "numDaysBack"

    invoke-static {}, Lcom/chase/sig/android/util/l;->b()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    :cond_0
    const-string v4, "accountId"

    invoke-virtual {v3, v4, p0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    const-string v4, "rows"

    const-string v5, "25"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    invoke-static {p1}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 50
    const-string v4, "dateRange"

    invoke-virtual {v3, v4, p1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    :cond_1
    if-eqz p2, :cond_3

    .line 54
    invoke-virtual {p2}, Lcom/chase/sig/android/domain/InvestmentTransaction$FilterType;->a()Ljava/lang/String;

    move-result-object v4

    .line 55
    invoke-static {v4}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 56
    const-string v5, "filterCode"

    invoke-virtual {v3, v5, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    :cond_2
    invoke-virtual {p2}, Lcom/chase/sig/android/domain/InvestmentTransaction$FilterType;->b()Ljava/lang/String;

    move-result-object v4

    .line 60
    invoke-static {v4}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 61
    const-string v5, "filterValue"

    invoke-virtual {v3, v5, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    :cond_3
    :try_start_0
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v4

    invoke-static {v4, v1, v3}, Lcom/chase/sig/android/util/m;->a(Lcom/chase/sig/android/ChaseApplication;Ljava/lang/String;Ljava/util/Hashtable;)Lorg/json/JSONObject;

    move-result-object v3

    .line 69
    invoke-virtual {v2, v3}, Lcom/chase/sig/android/service/InvestmentTransactionsResponse;->b(Lorg/json/JSONObject;)V

    .line 71
    const-string v1, "investmentTypes"

    invoke-static {v3, v1}, Lcom/chase/sig/android/util/n;->a(Lorg/json/JSONObject;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 72
    const-string v1, "investmentTypes"

    invoke-virtual {v3, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    .line 73
    if-eqz v4, :cond_4

    .line 74
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v5

    move v1, v0

    .line 75
    :goto_0
    if-ge v1, v5, :cond_4

    .line 76
    invoke-virtual {v4, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    .line 77
    new-instance v7, Lcom/chase/sig/android/domain/InvestmentTransaction$FilterType;

    new-instance v8, Lcom/chase/sig/android/domain/InvestmentTransaction;

    invoke-direct {v8}, Lcom/chase/sig/android/domain/InvestmentTransaction;-><init>()V

    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v7, v8}, Lcom/chase/sig/android/domain/InvestmentTransaction$FilterType;-><init>(Lcom/chase/sig/android/domain/InvestmentTransaction;)V

    const-string v8, "filterCode"

    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/chase/sig/android/domain/InvestmentTransaction$FilterType;->b(Ljava/lang/String;)V

    const-string v8, "filterName"

    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/chase/sig/android/domain/InvestmentTransaction$FilterType;->a(Ljava/lang/String;)V

    const-string v8, "sortOrdNo"

    const/4 v9, -0x1

    invoke-virtual {v6, v8, v9}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v8

    invoke-virtual {v7, v8}, Lcom/chase/sig/android/domain/InvestmentTransaction$FilterType;->a(I)V

    const-string v8, "filterValue"

    invoke-virtual {v6, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Lcom/chase/sig/android/domain/InvestmentTransaction$FilterType;->c(Ljava/lang/String;)V

    .line 78
    invoke-virtual {v2, v7}, Lcom/chase/sig/android/service/InvestmentTransactionsResponse;->a(Lcom/chase/sig/android/domain/InvestmentTransaction$FilterType;)V

    .line 75
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 83
    :cond_4
    const-string v1, "page"

    invoke-static {v3, v1}, Lcom/chase/sig/android/util/n;->a(Lorg/json/JSONObject;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 84
    const-string v1, "page"

    invoke-virtual {v3, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 85
    const-string v4, "activity"

    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    .line 87
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 88
    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-lez v5, :cond_5

    .line 89
    :goto_1
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v0, v5, :cond_5

    .line 90
    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    invoke-static {v5}, Lcom/chase/sig/android/service/k;->a(Lorg/json/JSONObject;)Lcom/chase/sig/android/domain/InvestmentTransaction;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 89
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 94
    :cond_5
    invoke-virtual {v2, v4}, Lcom/chase/sig/android/service/InvestmentTransactionsResponse;->a(Ljava/util/List;)V

    .line 97
    :cond_6
    const-string v0, "dateRange"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/chase/sig/android/service/InvestmentTransactionsResponse;->a(Ljava/lang/String;)V

    .line 98
    const-string v0, "filterValue"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/chase/sig/android/service/InvestmentTransactionsResponse;->b(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 107
    :goto_2
    return-object v2

    .line 101
    :catch_0
    move-exception v0

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/chase/sig/android/service/InvestmentTransactionsResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    goto :goto_2

    .line 104
    :catch_1
    move-exception v0

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/chase/sig/android/service/InvestmentTransactionsResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    goto :goto_2
.end method

.method private static b(Lorg/json/JSONObject;)Lorg/json/JSONObject;
    .locals 2
    .parameter

    .prologue
    .line 195
    const/4 v0, 0x0

    .line 197
    :try_start_0
    const-string v1, "value"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 201
    :goto_0
    return-object v0

    .line 198
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lorg/json/JSONException;->printStackTrace()V

    goto :goto_0
.end method
