.class public Lcom/chase/sig/android/service/l;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private errors:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/service/IServiceError;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static c(Lorg/json/JSONObject;)Ljava/util/List;
    .locals 7
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/service/ServiceErrorAttribute;",
            ">;"
        }
    .end annotation

    .prologue
    .line 179
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 180
    const-string v0, "attributes"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 181
    if-eqz v3, :cond_0

    .line 182
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 183
    invoke-virtual {v3, v1}, Lorg/json/JSONArray;->opt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/json/JSONObject;

    .line 184
    new-instance v4, Lcom/chase/sig/android/service/ServiceErrorAttribute;

    const-string v5, "key"

    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "value"

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v5, v0}, Lcom/chase/sig/android/service/ServiceErrorAttribute;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 182
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 188
    :cond_0
    return-object v2
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;
    .locals 4
    .parameter

    .prologue
    .line 31
    const-string v0, "2000"

    new-instance v1, Lcom/chase/sig/android/service/ServiceError;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f07027c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-direct {v1, v0, v2, v3}, Lcom/chase/sig/android/service/ServiceError;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/service/l;->a(Lcom/chase/sig/android/service/IServiceError;)V

    .line 37
    return-object p0
.end method

.method public final a(Lcom/chase/sig/android/service/IServiceError;)V
    .locals 1
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, Lcom/chase/sig/android/service/l;->errors:Ljava/util/List;

    if-nez v0, :cond_0

    .line 24
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/chase/sig/android/service/l;->errors:Ljava/util/List;

    .line 27
    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/service/l;->errors:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 28
    return-void
.end method

.method public final a(Lorg/json/JSONObject;)V
    .locals 7
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 93
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/chase/sig/android/service/l;->errors:Ljava/util/List;

    .line 94
    const-string v0, "errors"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 116
    :cond_0
    return-void

    .line 99
    :cond_1
    const-string v0, "errors"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 101
    if-eqz v2, :cond_2

    .line 102
    invoke-virtual {v2}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 103
    iget-object v4, p0, Lcom/chase/sig/android/service/l;->errors:Ljava/util/List;

    new-instance v5, Lcom/chase/sig/android/service/ServiceError;

    const-string v6, ""

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v5, v6, v0, v1}, Lcom/chase/sig/android/service/ServiceError;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 108
    :cond_2
    const-string v0, "errors"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 110
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-lez v0, :cond_0

    move v0, v1

    .line 111
    :goto_1
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 112
    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    .line 113
    iget-object v4, p0, Lcom/chase/sig/android/service/l;->errors:Ljava/util/List;

    new-instance v5, Lcom/chase/sig/android/service/ServiceError;

    const-string v6, ""

    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v5, v6, v3, v1}, Lcom/chase/sig/android/service/ServiceError;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 111
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public final b(Landroid/content/Context;)V
    .locals 0
    .parameter

    .prologue
    .line 41
    invoke-virtual {p0, p1}, Lcom/chase/sig/android/service/l;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    .line 42
    return-void
.end method

.method public final b(Ljava/util/List;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/service/IServiceError;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 151
    iput-object p1, p0, Lcom/chase/sig/android/service/l;->errors:Ljava/util/List;

    .line 152
    return-void
.end method

.method public final b(Lorg/json/JSONObject;)V
    .locals 9
    .parameter

    .prologue
    const/16 v8, 0x12c

    const/4 v2, 0x0

    .line 120
    if-nez p1, :cond_1

    .line 148
    :cond_0
    return-void

    .line 124
    :cond_1
    const-string v0, "errors"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    .line 126
    if-eqz v4, :cond_0

    .line 130
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/chase/sig/android/service/l;->errors:Ljava/util/List;

    move v1, v2

    .line 132
    :goto_0
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 134
    invoke-virtual {v4, v1}, Lorg/json/JSONArray;->opt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/json/JSONObject;

    .line 136
    if-eqz v0, :cond_2

    .line 138
    const-string v3, "severity"

    invoke-virtual {v0, v3, v8}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v3

    .line 139
    if-ne v3, v8, :cond_3

    const/4 v3, 0x1

    .line 141
    :goto_1
    new-instance v5, Lcom/chase/sig/android/service/ServiceError;

    const-string v6, "code"

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "message"

    invoke-virtual {v0, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v0}, Lcom/chase/sig/android/service/l;->c(Lorg/json/JSONObject;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v5, v6, v7, v0, v3}, Lcom/chase/sig/android/service/ServiceError;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Z)V

    .line 145
    iget-object v0, p0, Lcom/chase/sig/android/service/l;->errors:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 132
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_3
    move v3, v2

    .line 139
    goto :goto_1
.end method

.method public final c(Ljava/lang/String;)Z
    .locals 1
    .parameter

    .prologue
    .line 155
    invoke-virtual {p0, p1}, Lcom/chase/sig/android/service/l;->d(Ljava/lang/String;)Lcom/chase/sig/android/service/IServiceError;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(Ljava/lang/String;)Lcom/chase/sig/android/service/IServiceError;
    .locals 3
    .parameter

    .prologue
    .line 163
    new-instance v0, Lcom/chase/sig/android/service/m;

    invoke-direct {v0, p0, p1}, Lcom/chase/sig/android/service/m;-><init>(Lcom/chase/sig/android/service/l;Ljava/lang/String;)V

    new-instance v1, Lcom/chase/sig/android/util/r;

    invoke-direct {v1}, Lcom/chase/sig/android/util/r;-><init>()V

    iget-object v1, p0, Lcom/chase/sig/android/service/l;->errors:Ljava/util/List;

    invoke-static {v1, v0}, Lcom/chase/sig/android/util/r;->a(Ljava/util/List;Lcom/chase/sig/android/util/q;)I

    move-result v1

    if-ltz v1, :cond_0

    iget-object v1, p0, Lcom/chase/sig/android/service/l;->errors:Ljava/util/List;

    new-instance v2, Lcom/chase/sig/android/util/r;

    invoke-direct {v2}, Lcom/chase/sig/android/util/r;-><init>()V

    iget-object v2, p0, Lcom/chase/sig/android/service/l;->errors:Ljava/util/List;

    invoke-static {v2, v0}, Lcom/chase/sig/android/util/r;->a(Ljava/util/List;Lcom/chase/sig/android/util/q;)I

    move-result v0

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/service/IServiceError;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/chase/sig/android/service/l;->errors:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/service/l;->errors:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 59
    iget-object v0, p0, Lcom/chase/sig/android/service/l;->errors:Ljava/util/List;

    if-nez v0, :cond_1

    .line 69
    :cond_0
    :goto_0
    return v2

    :cond_1
    move v1, v2

    .line 63
    :goto_1
    iget-object v0, p0, Lcom/chase/sig/android/service/l;->errors:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/chase/sig/android/service/l;->errors:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/service/IServiceError;

    invoke-interface {v0}, Lcom/chase/sig/android/service/IServiceError;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 65
    const/4 v2, 0x1

    goto :goto_0

    .line 63
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method

.method public final g()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/service/IServiceError;",
            ">;"
        }
    .end annotation

    .prologue
    .line 88
    iget-object v0, p0, Lcom/chase/sig/android/service/l;->errors:Ljava/util/List;

    return-object v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 159
    const-string v0, "2000"

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/service/l;->d(Ljava/lang/String;)Lcom/chase/sig/android/service/IServiceError;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/service/IServiceError;",
            ">;"
        }
    .end annotation

    .prologue
    .line 192
    iget-object v0, p0, Lcom/chase/sig/android/service/l;->errors:Ljava/util/List;

    return-object v0
.end method
