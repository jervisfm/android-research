.class public Lcom/chase/sig/android/service/movemoney/ListServiceResponse;
.super Lcom/chase/sig/android/service/l;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/chase/sig/android/domain/Transaction;",
        ">",
        "Lcom/chase/sig/android/service/l;"
    }
.end annotation


# instance fields
.field private currentPage:I

.field private frequencyList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/LabeledValue;",
            ">;"
        }
    .end annotation
.end field

.field private list:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation
.end field

.field private totalPages:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/chase/sig/android/service/l;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 18
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/chase/sig/android/service/movemoney/ListServiceResponse;->list:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public final a(I)V
    .locals 0
    .parameter

    .prologue
    .line 30
    iput p1, p0, Lcom/chase/sig/android/service/movemoney/ListServiceResponse;->currentPage:I

    .line 31
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 22
    iput-object p1, p0, Lcom/chase/sig/android/service/movemoney/ListServiceResponse;->list:Ljava/util/List;

    .line 23
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 26
    iget v0, p0, Lcom/chase/sig/android/service/movemoney/ListServiceResponse;->currentPage:I

    return v0
.end method

.method public final b(I)V
    .locals 0
    .parameter

    .prologue
    .line 38
    iput p1, p0, Lcom/chase/sig/android/service/movemoney/ListServiceResponse;->totalPages:I

    .line 39
    return-void
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lcom/chase/sig/android/service/movemoney/ListServiceResponse;->totalPages:I

    return v0
.end method

.method public final c(Ljava/util/List;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/LabeledValue;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 46
    iput-object p1, p0, Lcom/chase/sig/android/service/movemoney/ListServiceResponse;->frequencyList:Ljava/util/List;

    .line 47
    return-void
.end method

.method public final d()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/LabeledValue;",
            ">;"
        }
    .end annotation

    .prologue
    .line 42
    iget-object v0, p0, Lcom/chase/sig/android/service/movemoney/ListServiceResponse;->frequencyList:Ljava/util/List;

    return-object v0
.end method
