.class public Lcom/chase/sig/android/service/movemoney/RequestFlags;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final a:Lcom/chase/sig/android/service/movemoney/RequestFlags;

.field public static final b:Lcom/chase/sig/android/service/movemoney/RequestFlags;

.field public static final c:Lcom/chase/sig/android/service/movemoney/RequestFlags;

.field public static final d:Lcom/chase/sig/android/service/movemoney/RequestFlags;

.field public static final e:Lcom/chase/sig/android/service/movemoney/RequestFlags;


# instance fields
.field private forValidation:Z

.field private updateModel:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 9
    new-instance v0, Lcom/chase/sig/android/service/movemoney/RequestFlags;

    invoke-direct {v0}, Lcom/chase/sig/android/service/movemoney/RequestFlags;-><init>()V

    iput-boolean v2, v0, Lcom/chase/sig/android/service/movemoney/RequestFlags;->forValidation:Z

    iput-boolean v1, v0, Lcom/chase/sig/android/service/movemoney/RequestFlags;->updateModel:Z

    sput-object v0, Lcom/chase/sig/android/service/movemoney/RequestFlags;->a:Lcom/chase/sig/android/service/movemoney/RequestFlags;

    .line 10
    new-instance v0, Lcom/chase/sig/android/service/movemoney/RequestFlags;

    invoke-direct {v0}, Lcom/chase/sig/android/service/movemoney/RequestFlags;-><init>()V

    iput-boolean v2, v0, Lcom/chase/sig/android/service/movemoney/RequestFlags;->forValidation:Z

    iput-boolean v2, v0, Lcom/chase/sig/android/service/movemoney/RequestFlags;->updateModel:Z

    sput-object v0, Lcom/chase/sig/android/service/movemoney/RequestFlags;->b:Lcom/chase/sig/android/service/movemoney/RequestFlags;

    .line 11
    new-instance v0, Lcom/chase/sig/android/service/movemoney/RequestFlags;

    invoke-direct {v0}, Lcom/chase/sig/android/service/movemoney/RequestFlags;-><init>()V

    iput-boolean v1, v0, Lcom/chase/sig/android/service/movemoney/RequestFlags;->forValidation:Z

    iput-boolean v2, v0, Lcom/chase/sig/android/service/movemoney/RequestFlags;->updateModel:Z

    sput-object v0, Lcom/chase/sig/android/service/movemoney/RequestFlags;->c:Lcom/chase/sig/android/service/movemoney/RequestFlags;

    .line 12
    new-instance v0, Lcom/chase/sig/android/service/movemoney/RequestFlags;

    invoke-direct {v0}, Lcom/chase/sig/android/service/movemoney/RequestFlags;-><init>()V

    iput-boolean v1, v0, Lcom/chase/sig/android/service/movemoney/RequestFlags;->forValidation:Z

    iput-boolean v1, v0, Lcom/chase/sig/android/service/movemoney/RequestFlags;->updateModel:Z

    sput-object v0, Lcom/chase/sig/android/service/movemoney/RequestFlags;->d:Lcom/chase/sig/android/service/movemoney/RequestFlags;

    .line 13
    new-instance v0, Lcom/chase/sig/android/service/movemoney/RequestFlags;

    invoke-direct {v0}, Lcom/chase/sig/android/service/movemoney/RequestFlags;-><init>()V

    iput-boolean v1, v0, Lcom/chase/sig/android/service/movemoney/RequestFlags;->forValidation:Z

    iput-boolean v1, v0, Lcom/chase/sig/android/service/movemoney/RequestFlags;->updateModel:Z

    sput-object v0, Lcom/chase/sig/android/service/movemoney/RequestFlags;->e:Lcom/chase/sig/android/service/movemoney/RequestFlags;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 21
    iget-boolean v0, p0, Lcom/chase/sig/android/service/movemoney/RequestFlags;->forValidation:Z

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 25
    iget-boolean v0, p0, Lcom/chase/sig/android/service/movemoney/RequestFlags;->updateModel:Z

    return v0
.end method
