.class public Lcom/chase/sig/android/service/movemoney/ServiceResponse;
.super Lcom/chase/sig/android/service/l;
.source "SourceFile"


# instance fields
.field private dueDate:Ljava/lang/String;

.field private formId:Ljava/lang/String;

.field private includesOptionalProductFee:Z

.field private paymentId:Ljava/lang/String;

.field private processDate:Ljava/lang/String;

.field private status:Ljava/lang/String;

.field private transaction:Lcom/chase/sig/android/domain/Transaction;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Lcom/chase/sig/android/service/l;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/chase/sig/android/service/movemoney/ServiceResponse;->processDate:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 39
    iput-object p1, p0, Lcom/chase/sig/android/service/movemoney/ServiceResponse;->processDate:Ljava/lang/String;

    .line 40
    return-void
.end method

.method public final a(Z)V
    .locals 0
    .parameter

    .prologue
    .line 67
    iput-boolean p1, p0, Lcom/chase/sig/android/service/movemoney/ServiceResponse;->includesOptionalProductFee:Z

    .line 68
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/chase/sig/android/service/movemoney/ServiceResponse;->dueDate:Ljava/lang/String;

    return-object v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 43
    iput-object p1, p0, Lcom/chase/sig/android/service/movemoney/ServiceResponse;->dueDate:Ljava/lang/String;

    .line 44
    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/chase/sig/android/service/movemoney/ServiceResponse;->paymentId:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/chase/sig/android/service/movemoney/ServiceResponse;->status:Ljava/lang/String;

    return-object v0
.end method

.method public final e(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 47
    iput-object p1, p0, Lcom/chase/sig/android/service/movemoney/ServiceResponse;->paymentId:Ljava/lang/String;

    .line 48
    return-void
.end method

.method public final f(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 55
    iput-object p1, p0, Lcom/chase/sig/android/service/movemoney/ServiceResponse;->status:Ljava/lang/String;

    .line 56
    return-void
.end method

.method public final g(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 63
    iput-object p1, p0, Lcom/chase/sig/android/service/movemoney/ServiceResponse;->formId:Ljava/lang/String;

    .line 64
    return-void
.end method

.method public final j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/chase/sig/android/service/movemoney/ServiceResponse;->formId:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/chase/sig/android/service/movemoney/ServiceResponse;->includesOptionalProductFee:Z

    return v0
.end method
