.class public final Lcom/chase/sig/android/service/movemoney/a;
.super Lcom/chase/sig/android/service/movemoney/e;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/chase/sig/android/service/movemoney/e;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/chase/sig/android/domain/Transaction;Ljava/lang/String;)Ljava/util/Hashtable;
    .locals 4
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/chase/sig/android/domain/Transaction;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 64
    invoke-super {p0, p1, p2}, Lcom/chase/sig/android/service/movemoney/e;->a(Lcom/chase/sig/android/domain/Transaction;Ljava/lang/String;)Ljava/util/Hashtable;

    move-result-object v1

    .line 66
    check-cast p1, Lcom/chase/sig/android/domain/BillPayTransaction;

    .line 68
    const-string v0, "accountId"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/BillPayTransaction;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    const-string v0, "paymentId"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/BillPayTransaction;->n()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    const-string v0, "paymentToken"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/BillPayTransaction;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    const-string v0, "false"

    .line 74
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/BillPayTransaction;->l()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/BillPayTransaction;->k()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 77
    const-string v0, "true"

    .line 78
    const-string v2, "paymentModelToken"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/BillPayTransaction;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    const-string v2, "paymentModelId"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/BillPayTransaction;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    :cond_0
    const-string v2, "cancelModel"

    invoke-virtual {v1, v2, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    return-object v1
.end method

.method public final a(Lcom/chase/sig/android/domain/Transaction;Ljava/lang/String;Lcom/chase/sig/android/service/movemoney/RequestFlags;)Ljava/util/Hashtable;
    .locals 4
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/chase/sig/android/domain/Transaction;",
            "Ljava/lang/String;",
            "Lcom/chase/sig/android/service/movemoney/RequestFlags;",
            ")",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 15
    invoke-super {p0, p1, p2, p3}, Lcom/chase/sig/android/service/movemoney/e;->a(Lcom/chase/sig/android/domain/Transaction;Ljava/lang/String;Lcom/chase/sig/android/service/movemoney/RequestFlags;)Ljava/util/Hashtable;

    move-result-object v2

    move-object v0, p1

    .line 17
    check-cast v0, Lcom/chase/sig/android/domain/BillPayTransaction;

    .line 19
    const-string v1, "validateOnly"

    invoke-virtual {p3}, Lcom/chase/sig/android/service/movemoney/RequestFlags;->a()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 20
    const-string v1, "amount"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayTransaction;->e()Lcom/chase/sig/android/util/Dollar;

    move-result-object v3

    invoke-virtual {v3}, Lcom/chase/sig/android/util/Dollar;->b()Ljava/math/BigDecimal;

    move-result-object v3

    invoke-virtual {v3}, Ljava/math/BigDecimal;->toPlainString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 21
    const-string v1, "dueDate"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayTransaction;->q()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 22
    const-string v3, "processDate"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayTransaction;->f()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayTransaction;->f()Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {v2, v3, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 24
    const-string v1, "accountId"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayTransaction;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 25
    const-string v1, "payeeId"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayTransaction;->a()Lcom/chase/sig/android/domain/Payee;

    move-result-object v3

    invoke-virtual {v3}, Lcom/chase/sig/android/domain/Payee;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 26
    const-string v1, "memo"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayTransaction;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28
    invoke-virtual {p3}, Lcom/chase/sig/android/service/movemoney/RequestFlags;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 29
    const-string v0, "formId"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/Transaction;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    :cond_0
    return-object v2

    .line 22
    :cond_1
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/BillPayTransaction;->q()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public final b(Lcom/chase/sig/android/domain/Transaction;Ljava/lang/String;Lcom/chase/sig/android/service/movemoney/RequestFlags;)Ljava/util/Hashtable;
    .locals 3
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/chase/sig/android/domain/Transaction;",
            "Ljava/lang/String;",
            "Lcom/chase/sig/android/service/movemoney/RequestFlags;",
            ")",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 38
    invoke-super {p0, p1, p2, p3}, Lcom/chase/sig/android/service/movemoney/e;->b(Lcom/chase/sig/android/domain/Transaction;Ljava/lang/String;Lcom/chase/sig/android/service/movemoney/RequestFlags;)Ljava/util/Hashtable;

    move-result-object v1

    .line 39
    check-cast p1, Lcom/chase/sig/android/domain/BillPayTransaction;

    .line 40
    const-string v0, "paymentId"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/BillPayTransaction;->n()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    const-string v0, "paymentToken"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/BillPayTransaction;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    const-string v0, "amount"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/BillPayTransaction;->e()Lcom/chase/sig/android/util/Dollar;

    move-result-object v2

    invoke-virtual {v2}, Lcom/chase/sig/android/util/Dollar;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    const-string v0, "dueDate"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/BillPayTransaction;->q()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    const-string v2, "memo"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/BillPayTransaction;->o()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v0, ""

    :goto_0
    invoke-virtual {v1, v2, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    const-string v0, "processDate"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/BillPayTransaction;->q()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    const-string v0, "accountId"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/BillPayTransaction;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    const-string v0, "payeeId"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/BillPayTransaction;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    const-string v0, "validateOnly"

    invoke-virtual {p3}, Lcom/chase/sig/android/service/movemoney/RequestFlags;->a()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    const-string v0, "updateModel"

    invoke-virtual {p3}, Lcom/chase/sig/android/service/movemoney/RequestFlags;->b()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    invoke-virtual {p3}, Lcom/chase/sig/android/service/movemoney/RequestFlags;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 52
    const-string v0, "formId"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/BillPayTransaction;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    :cond_0
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/BillPayTransaction;->l()Z

    move-result v0

    if-nez v0, :cond_1

    .line 56
    invoke-static {v1, p1}, Lcom/chase/sig/android/service/movemoney/a;->a(Ljava/util/Hashtable;Lcom/chase/sig/android/domain/Transaction;)V

    .line 58
    :cond_1
    return-object v1

    .line 44
    :cond_2
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/BillPayTransaction;->o()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
