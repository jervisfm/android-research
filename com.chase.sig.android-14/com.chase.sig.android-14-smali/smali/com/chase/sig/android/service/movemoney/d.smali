.class public Lcom/chase/sig/android/service/movemoney/d;
.super Lcom/chase/sig/android/service/JPService;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/chase/sig/android/domain/Transaction;",
        ">",
        "Lcom/chase/sig/android/service/JPService;"
    }
.end annotation


# instance fields
.field public a:Lcom/chase/sig/android/service/movemoney/g;

.field public b:Lcom/chase/sig/android/service/movemoney/e;

.field private c:Lcom/chase/sig/android/service/movemoney/f;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/chase/sig/android/service/movemoney/f",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/chase/sig/android/service/JPService;-><init>()V

    return-void
.end method

.method public constructor <init>(Lcom/chase/sig/android/service/movemoney/g;Lcom/chase/sig/android/service/movemoney/e;Lcom/chase/sig/android/service/movemoney/f;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/chase/sig/android/service/movemoney/g;",
            "Lcom/chase/sig/android/service/movemoney/e;",
            "Lcom/chase/sig/android/service/movemoney/f",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/chase/sig/android/service/JPService;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/chase/sig/android/service/movemoney/d;->a:Lcom/chase/sig/android/service/movemoney/g;

    .line 23
    iput-object p2, p0, Lcom/chase/sig/android/service/movemoney/d;->b:Lcom/chase/sig/android/service/movemoney/e;

    .line 24
    iput-object p3, p0, Lcom/chase/sig/android/service/movemoney/d;->c:Lcom/chase/sig/android/service/movemoney/f;

    .line 25
    return-void
.end method

.method private static a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->a:Lcom/chase/sig/android/domain/a;

    iget-object v0, v0, Lcom/chase/sig/android/domain/a;->c:Ljava/lang/String;

    return-object v0
.end method

.method private b(Ljava/util/Hashtable;Ljava/lang/String;)Lcom/chase/sig/android/service/movemoney/ServiceResponse;
    .locals 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/chase/sig/android/service/movemoney/ServiceResponse;"
        }
    .end annotation

    .prologue
    .line 68
    new-instance v0, Lcom/chase/sig/android/service/movemoney/ServiceResponse;

    invoke-direct {v0}, Lcom/chase/sig/android/service/movemoney/ServiceResponse;-><init>()V

    .line 70
    :try_start_0
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v1

    invoke-static {v1, p2, p1}, Lcom/chase/sig/android/util/m;->a(Lcom/chase/sig/android/ChaseApplication;Ljava/lang/String;Ljava/util/Hashtable;)Lorg/json/JSONObject;

    move-result-object v1

    .line 71
    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/movemoney/ServiceResponse;->a(Lorg/json/JSONObject;)V

    .line 72
    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/movemoney/ServiceResponse;->b(Lorg/json/JSONObject;)V

    .line 73
    iget-object v2, p0, Lcom/chase/sig/android/service/movemoney/d;->c:Lcom/chase/sig/android/service/movemoney/f;

    invoke-virtual {v2, v0, v1}, Lcom/chase/sig/android/service/movemoney/f;->a(Lcom/chase/sig/android/service/movemoney/ServiceResponse;Lorg/json/JSONObject;)Lcom/chase/sig/android/service/movemoney/ServiceResponse;
    :try_end_0
    .catch Lcom/chase/sig/android/util/ChaseException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 83
    :goto_0
    return-object v0

    .line 76
    :catch_0
    move-exception v1

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/movemoney/ServiceResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    goto :goto_0

    .line 79
    :catch_1
    move-exception v1

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/movemoney/ServiceResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    goto :goto_0
.end method


# virtual methods
.method protected final a(Ljava/util/Hashtable;)Lcom/chase/sig/android/service/movemoney/ListServiceResponse;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/chase/sig/android/service/movemoney/ListServiceResponse",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 49
    iget-object v0, p0, Lcom/chase/sig/android/service/movemoney/d;->a:Lcom/chase/sig/android/service/movemoney/g;

    iget-object v0, v0, Lcom/chase/sig/android/service/movemoney/g;->d:Ljava/lang/String;

    invoke-virtual {p0, p1, v0}, Lcom/chase/sig/android/service/movemoney/d;->a(Ljava/util/Hashtable;Ljava/lang/String;)Lcom/chase/sig/android/service/movemoney/ListServiceResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/util/Hashtable;Ljava/lang/String;)Lcom/chase/sig/android/service/movemoney/ListServiceResponse;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/chase/sig/android/service/movemoney/ListServiceResponse",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 56
    :try_start_0
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-static {v0, p2, p1}, Lcom/chase/sig/android/util/m;->a(Lcom/chase/sig/android/ChaseApplication;Ljava/lang/String;Ljava/util/Hashtable;)Lorg/json/JSONObject;

    move-result-object v1

    .line 57
    iget-object v0, p0, Lcom/chase/sig/android/service/movemoney/d;->c:Lcom/chase/sig/android/service/movemoney/f;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/movemoney/f;->b(Lorg/json/JSONObject;)Lcom/chase/sig/android/service/movemoney/ListServiceResponse;

    move-result-object v0

    .line 58
    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/movemoney/ListServiceResponse;->b(Lorg/json/JSONObject;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 64
    :goto_0
    return-object v0

    .line 60
    :catch_0
    move-exception v0

    new-instance v0, Lcom/chase/sig/android/service/movemoney/ListServiceResponse;

    invoke-direct {v0}, Lcom/chase/sig/android/service/movemoney/ListServiceResponse;-><init>()V

    .line 62
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/movemoney/ListServiceResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    goto :goto_0
.end method

.method public final a(Lcom/chase/sig/android/domain/Transaction;)Lcom/chase/sig/android/service/movemoney/ServiceResponse;
    .locals 2
    .parameter

    .prologue
    .line 30
    iget-object v0, p0, Lcom/chase/sig/android/service/movemoney/d;->b:Lcom/chase/sig/android/service/movemoney/e;

    invoke-static {}, Lcom/chase/sig/android/service/movemoney/d;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/chase/sig/android/service/movemoney/e;->a(Lcom/chase/sig/android/domain/Transaction;Ljava/lang/String;)Ljava/util/Hashtable;

    move-result-object v0

    iget-object v1, p0, Lcom/chase/sig/android/service/movemoney/d;->a:Lcom/chase/sig/android/service/movemoney/g;

    iget-object v1, v1, Lcom/chase/sig/android/service/movemoney/g;->a:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/chase/sig/android/service/movemoney/d;->b(Ljava/util/Hashtable;Ljava/lang/String;)Lcom/chase/sig/android/service/movemoney/ServiceResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/chase/sig/android/domain/Transaction;Lcom/chase/sig/android/service/movemoney/RequestFlags;)Lcom/chase/sig/android/service/movemoney/ServiceResponse;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lcom/chase/sig/android/service/movemoney/RequestFlags;",
            ")",
            "Lcom/chase/sig/android/service/movemoney/ServiceResponse;"
        }
    .end annotation

    .prologue
    .line 87
    iget-object v0, p0, Lcom/chase/sig/android/service/movemoney/d;->b:Lcom/chase/sig/android/service/movemoney/e;

    invoke-static {}, Lcom/chase/sig/android/service/movemoney/d;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1, p2}, Lcom/chase/sig/android/service/movemoney/e;->b(Lcom/chase/sig/android/domain/Transaction;Ljava/lang/String;Lcom/chase/sig/android/service/movemoney/RequestFlags;)Ljava/util/Hashtable;

    move-result-object v0

    iget-object v1, p0, Lcom/chase/sig/android/service/movemoney/d;->a:Lcom/chase/sig/android/service/movemoney/g;

    iget-object v1, v1, Lcom/chase/sig/android/service/movemoney/g;->b:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/chase/sig/android/service/movemoney/d;->b(Ljava/util/Hashtable;Ljava/lang/String;)Lcom/chase/sig/android/service/movemoney/ServiceResponse;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/chase/sig/android/domain/Transaction;Lcom/chase/sig/android/service/movemoney/RequestFlags;)Lcom/chase/sig/android/service/movemoney/ServiceResponse;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 95
    :try_start_0
    iget-object v0, p0, Lcom/chase/sig/android/service/movemoney/d;->b:Lcom/chase/sig/android/service/movemoney/e;

    invoke-static {}, Lcom/chase/sig/android/service/movemoney/d;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1, p2}, Lcom/chase/sig/android/service/movemoney/e;->a(Lcom/chase/sig/android/domain/Transaction;Ljava/lang/String;Lcom/chase/sig/android/service/movemoney/RequestFlags;)Ljava/util/Hashtable;
    :try_end_0
    .catch Lcom/chase/sig/android/service/JPService$CrossSiteRequestForgeryTokenFailureException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 102
    iget-object v1, p0, Lcom/chase/sig/android/service/movemoney/d;->a:Lcom/chase/sig/android/service/movemoney/g;

    iget-object v1, v1, Lcom/chase/sig/android/service/movemoney/g;->c:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/chase/sig/android/service/movemoney/d;->b(Ljava/util/Hashtable;Ljava/lang/String;)Lcom/chase/sig/android/service/movemoney/ServiceResponse;

    move-result-object v0

    :goto_0
    return-object v0

    .line 97
    :catch_0
    move-exception v0

    new-instance v0, Lcom/chase/sig/android/service/movemoney/ServiceResponse;

    invoke-direct {v0}, Lcom/chase/sig/android/service/movemoney/ServiceResponse;-><init>()V

    .line 98
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/movemoney/ServiceResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    goto :goto_0
.end method

.method public c(I)Lcom/chase/sig/android/service/movemoney/ListServiceResponse;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/chase/sig/android/service/movemoney/ListServiceResponse",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 35
    iget-object v0, p0, Lcom/chase/sig/android/service/movemoney/d;->b:Lcom/chase/sig/android/service/movemoney/e;

    invoke-virtual {v0, p1}, Lcom/chase/sig/android/service/movemoney/e;->a(I)Ljava/util/Hashtable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/service/movemoney/d;->a(Ljava/util/Hashtable;)Lcom/chase/sig/android/service/movemoney/ListServiceResponse;

    move-result-object v0

    return-object v0
.end method
