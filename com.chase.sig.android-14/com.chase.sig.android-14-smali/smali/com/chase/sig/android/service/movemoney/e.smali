.class public abstract Lcom/chase/sig/android/service/movemoney/e;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/util/Hashtable;Lcom/chase/sig/android/domain/Transaction;)V
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/chase/sig/android/domain/Transaction;",
            ")V"
        }
    .end annotation

    .prologue
    .line 40
    const-string v0, "paymentModelId"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/Transaction;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    const-string v0, "paymentModelToken"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/Transaction;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    const-string v1, "openEnded"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/Transaction;->y()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "true"

    :goto_0
    invoke-virtual {p0, v1, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/Transaction;->y()Z

    move-result v0

    if-nez v0, :cond_0

    .line 44
    const-string v0, "remainingInstances"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/Transaction;->w()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    :cond_0
    const-string v0, "frequency"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/Transaction;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    return-void

    .line 42
    :cond_1
    const-string v0, "false"

    goto :goto_0
.end method


# virtual methods
.method public a(I)Ljava/util/Hashtable;
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 16
    invoke-static {}, Lcom/chase/sig/android/service/JPService;->c()Ljava/util/Hashtable;

    move-result-object v0

    .line 17
    const-string v1, "page"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 18
    const-string v1, "includeFrequencyList"

    const-string v2, "true"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 19
    return-object v0
.end method

.method public a(Lcom/chase/sig/android/domain/Transaction;Ljava/lang/String;)Ljava/util/Hashtable;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/chase/sig/android/domain/Transaction;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 12
    invoke-static {}, Lcom/chase/sig/android/service/JPService;->c()Ljava/util/Hashtable;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/chase/sig/android/domain/Transaction;Ljava/lang/String;Lcom/chase/sig/android/service/movemoney/RequestFlags;)Ljava/util/Hashtable;
    .locals 1
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/chase/sig/android/domain/Transaction;",
            "Ljava/lang/String;",
            "Lcom/chase/sig/android/service/movemoney/RequestFlags;",
            ")",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 32
    invoke-static {}, Lcom/chase/sig/android/service/JPService;->c()Ljava/util/Hashtable;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/chase/sig/android/domain/Transaction;Ljava/lang/String;Lcom/chase/sig/android/service/movemoney/RequestFlags;)Ljava/util/Hashtable;
    .locals 1
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/chase/sig/android/domain/Transaction;",
            "Ljava/lang/String;",
            "Lcom/chase/sig/android/service/movemoney/RequestFlags;",
            ")",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 36
    invoke-static {}, Lcom/chase/sig/android/service/JPService;->c()Ljava/util/Hashtable;

    move-result-object v0

    return-object v0
.end method
