.class public final Lcom/chase/sig/android/service/movemoney/h;
.super Lcom/chase/sig/android/service/movemoney/e;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/chase/sig/android/service/movemoney/e;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/chase/sig/android/domain/Transaction;Ljava/lang/String;)Ljava/util/Hashtable;
    .locals 4
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/chase/sig/android/domain/Transaction;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 15
    invoke-super {p0, p1, p2}, Lcom/chase/sig/android/service/movemoney/e;->a(Lcom/chase/sig/android/domain/Transaction;Ljava/lang/String;)Ljava/util/Hashtable;

    move-result-object v1

    .line 17
    check-cast p1, Lcom/chase/sig/android/domain/AccountTransfer;

    .line 19
    const-string v0, "accountId"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/AccountTransfer;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 20
    const-string v0, "paymentId"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/AccountTransfer;->n()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 21
    const-string v0, "paymentToken"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/AccountTransfer;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 23
    const-string v0, "false"

    .line 27
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/AccountTransfer;->l()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/AccountTransfer;->k()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 28
    const-string v0, "true"

    .line 29
    const-string v2, "paymentModelToken"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/AccountTransfer;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    const-string v2, "paymentModelId"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/AccountTransfer;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    :cond_0
    const-string v2, "cancelModel"

    invoke-virtual {v1, v2, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    return-object v1
.end method

.method public final a(Lcom/chase/sig/android/domain/Transaction;Ljava/lang/String;Lcom/chase/sig/android/service/movemoney/RequestFlags;)Ljava/util/Hashtable;
    .locals 3
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/chase/sig/android/domain/Transaction;",
            "Ljava/lang/String;",
            "Lcom/chase/sig/android/service/movemoney/RequestFlags;",
            ")",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 72
    invoke-super {p0, p1, p2, p3}, Lcom/chase/sig/android/service/movemoney/e;->a(Lcom/chase/sig/android/domain/Transaction;Ljava/lang/String;Lcom/chase/sig/android/service/movemoney/RequestFlags;)Ljava/util/Hashtable;

    move-result-object v0

    .line 74
    check-cast p1, Lcom/chase/sig/android/domain/AccountTransfer;

    .line 76
    invoke-virtual {p3}, Lcom/chase/sig/android/service/movemoney/RequestFlags;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 77
    const-string v1, "validateOnly"

    invoke-virtual {p3}, Lcom/chase/sig/android/service/movemoney/RequestFlags;->a()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    const-string v1, "amount"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/AccountTransfer;->e()Lcom/chase/sig/android/util/Dollar;

    move-result-object v2

    invoke-virtual {v2}, Lcom/chase/sig/android/util/Dollar;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    const-string v1, "dueDate"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/AccountTransfer;->q()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    const-string v1, "memo"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/AccountTransfer;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    const-string v1, "processDate"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/AccountTransfer;->q()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    const-string v1, "creditAccountId"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/AccountTransfer;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    const-string v1, "accountId"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/AccountTransfer;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    :goto_0
    return-object v0

    .line 85
    :cond_0
    const-string v1, "formId"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/AccountTransfer;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    const-string v1, "accountId"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/AccountTransfer;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final b(Lcom/chase/sig/android/domain/Transaction;Ljava/lang/String;Lcom/chase/sig/android/service/movemoney/RequestFlags;)Ljava/util/Hashtable;
    .locals 3
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/chase/sig/android/domain/Transaction;",
            "Ljava/lang/String;",
            "Lcom/chase/sig/android/service/movemoney/RequestFlags;",
            ")",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 41
    invoke-super {p0, p1, p2, p3}, Lcom/chase/sig/android/service/movemoney/e;->b(Lcom/chase/sig/android/domain/Transaction;Ljava/lang/String;Lcom/chase/sig/android/service/movemoney/RequestFlags;)Ljava/util/Hashtable;

    move-result-object v0

    .line 42
    check-cast p1, Lcom/chase/sig/android/domain/AccountTransfer;

    .line 44
    const-string v1, "validateOnly"

    invoke-virtual {p3}, Lcom/chase/sig/android/service/movemoney/RequestFlags;->a()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    const-string v1, "amount"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/AccountTransfer;->e()Lcom/chase/sig/android/util/Dollar;

    move-result-object v2

    invoke-virtual {v2}, Lcom/chase/sig/android/util/Dollar;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    const-string v1, "creditAccountNickName"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/AccountTransfer;->r()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    const-string v1, "dueDate"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/AccountTransfer;->q()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    const-string v1, "fundingAccountId"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/AccountTransfer;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    const-string v1, "memo"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/AccountTransfer;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    const-string v1, "paymentId"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/AccountTransfer;->n()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    const-string v1, "paymentToken"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/AccountTransfer;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    const-string v1, "updateModel"

    invoke-virtual {p3}, Lcom/chase/sig/android/service/movemoney/RequestFlags;->b()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/AccountTransfer;->l()Z

    move-result v1

    if-nez v1, :cond_0

    .line 55
    invoke-static {v0, p1}, Lcom/chase/sig/android/service/movemoney/h;->a(Ljava/util/Hashtable;Lcom/chase/sig/android/domain/Transaction;)V

    .line 58
    :cond_0
    const-string v1, "processDate"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/AccountTransfer;->q()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    const-string v1, "creditAccountId"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/AccountTransfer;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    const-string v1, "accountId"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/AccountTransfer;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    invoke-virtual {p3}, Lcom/chase/sig/android/service/movemoney/RequestFlags;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 63
    const-string v1, "formId"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/AccountTransfer;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    :cond_1
    return-object v0
.end method
