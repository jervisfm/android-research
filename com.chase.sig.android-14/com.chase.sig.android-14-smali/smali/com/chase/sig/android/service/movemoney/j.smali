.class public final Lcom/chase/sig/android/service/movemoney/j;
.super Lcom/chase/sig/android/service/movemoney/e;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/chase/sig/android/service/movemoney/e;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/chase/sig/android/domain/Transaction;Ljava/lang/String;)Ljava/util/Hashtable;
    .locals 4
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/chase/sig/android/domain/Transaction;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 17
    invoke-super {p0, p1, p2}, Lcom/chase/sig/android/service/movemoney/e;->a(Lcom/chase/sig/android/domain/Transaction;Ljava/lang/String;)Ljava/util/Hashtable;

    move-result-object v1

    .line 19
    check-cast p1, Lcom/chase/sig/android/domain/WireTransaction;

    .line 21
    const-string v0, "paymentId"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/WireTransaction;->n()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 22
    const-string v0, "paymentToken"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/WireTransaction;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 24
    const-string v0, "false"

    .line 25
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/WireTransaction;->l()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/WireTransaction;->k()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 26
    const-string v0, "true"

    .line 27
    const-string v2, "paymentModelToken"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/WireTransaction;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28
    const-string v2, "paymentModelId"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/WireTransaction;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    :cond_0
    const-string v2, "cancelModel"

    invoke-virtual {v1, v2, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    return-object v1
.end method

.method public final a(Lcom/chase/sig/android/domain/Transaction;Ljava/lang/String;Lcom/chase/sig/android/service/movemoney/RequestFlags;)Ljava/util/Hashtable;
    .locals 3
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/chase/sig/android/domain/Transaction;",
            "Ljava/lang/String;",
            "Lcom/chase/sig/android/service/movemoney/RequestFlags;",
            ")",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 74
    invoke-super {p0, p1, p2, p3}, Lcom/chase/sig/android/service/movemoney/e;->a(Lcom/chase/sig/android/domain/Transaction;Ljava/lang/String;Lcom/chase/sig/android/service/movemoney/RequestFlags;)Ljava/util/Hashtable;

    move-result-object v0

    .line 76
    check-cast p1, Lcom/chase/sig/android/domain/WireTransaction;

    .line 78
    const-string v1, "validateOnly"

    invoke-virtual {p3}, Lcom/chase/sig/android/service/movemoney/RequestFlags;->a()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    invoke-virtual {p3}, Lcom/chase/sig/android/service/movemoney/RequestFlags;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 81
    const-string v1, "amount"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/WireTransaction;->e()Lcom/chase/sig/android/util/Dollar;

    move-result-object v2

    invoke-virtual {v2}, Lcom/chase/sig/android/util/Dollar;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    const-string v1, "dueDate"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/WireTransaction;->q()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    const-string v1, "recipientMessage"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/WireTransaction;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    const-string v1, "bankMessage"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/WireTransaction;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    const-string v1, "memo"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/WireTransaction;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    const-string v1, "processDate"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/WireTransaction;->q()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    const-string v1, "accountId"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/WireTransaction;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    const-string v1, "payeeId"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/WireTransaction;->D()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    :goto_0
    return-object v0

    .line 93
    :cond_0
    const-string v1, "formId"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/WireTransaction;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    const-string v1, "accountId"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/WireTransaction;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final b(Lcom/chase/sig/android/domain/Transaction;Ljava/lang/String;Lcom/chase/sig/android/service/movemoney/RequestFlags;)Ljava/util/Hashtable;
    .locals 3
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/chase/sig/android/domain/Transaction;",
            "Ljava/lang/String;",
            "Lcom/chase/sig/android/service/movemoney/RequestFlags;",
            ")",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 38
    invoke-super {p0, p1, p2, p3}, Lcom/chase/sig/android/service/movemoney/e;->b(Lcom/chase/sig/android/domain/Transaction;Ljava/lang/String;Lcom/chase/sig/android/service/movemoney/RequestFlags;)Ljava/util/Hashtable;

    move-result-object v0

    .line 39
    check-cast p1, Lcom/chase/sig/android/domain/WireTransaction;

    .line 40
    const-string v1, "amount"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/WireTransaction;->e()Lcom/chase/sig/android/util/Dollar;

    move-result-object v2

    invoke-virtual {v2}, Lcom/chase/sig/android/util/Dollar;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    const-string v1, "accountId"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/WireTransaction;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    const-string v1, "dueDate"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/WireTransaction;->q()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    const-string v1, "processDate"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/WireTransaction;->q()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/WireTransaction;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 46
    const-string v1, "fedReference"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/WireTransaction;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    :cond_0
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/WireTransaction;->l()Z

    move-result v1

    if-nez v1, :cond_1

    .line 50
    invoke-static {v0, p1}, Lcom/chase/sig/android/service/movemoney/j;->a(Ljava/util/Hashtable;Lcom/chase/sig/android/domain/Transaction;)V

    .line 52
    :cond_1
    const-string v1, "recipientMessage"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/WireTransaction;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    const-string v1, "bankMessage"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/WireTransaction;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    const-string v1, "memo"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/WireTransaction;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    const-string v1, "paymentId"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/WireTransaction;->n()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    const-string v1, "payeeId"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/WireTransaction;->D()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    const-string v1, "paymentToken"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/WireTransaction;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    const-string v1, "updateModel"

    invoke-virtual {p3}, Lcom/chase/sig/android/service/movemoney/RequestFlags;->b()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    const-string v1, "validateOnly"

    invoke-virtual {p3}, Lcom/chase/sig/android/service/movemoney/RequestFlags;->a()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    invoke-virtual {p3}, Lcom/chase/sig/android/service/movemoney/RequestFlags;->a()Z

    move-result v1

    if-nez v1, :cond_2

    .line 64
    const-string v1, "formId"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/WireTransaction;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    :cond_2
    return-object v0
.end method
