.class public final Lcom/chase/sig/android/service/o;
.super Lcom/chase/sig/android/service/JPService;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/chase/sig/android/service/JPService;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/chase/sig/android/service/LegalAgreementGetDetailsResponse;
    .locals 5
    .parameter

    .prologue
    .line 15
    const v0, 0x7f070055

    invoke-static {v0}, Lcom/chase/sig/android/service/o;->a(I)Ljava/lang/String;

    move-result-object v1

    .line 16
    new-instance v0, Lcom/chase/sig/android/service/LegalAgreementGetDetailsResponse;

    invoke-direct {v0}, Lcom/chase/sig/android/service/LegalAgreementGetDetailsResponse;-><init>()V

    .line 18
    invoke-static {}, Lcom/chase/sig/android/service/o;->c()Ljava/util/Hashtable;

    move-result-object v2

    .line 20
    :try_start_0
    const-string v3, "productId"

    invoke-virtual {v2, v3, p0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 21
    const-string v3, "segmentId"

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v4

    invoke-virtual {v4}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v4

    iget-object v4, v4, Lcom/chase/sig/android/domain/o;->b:Lcom/chase/sig/android/domain/g;

    invoke-interface {v4}, Lcom/chase/sig/android/domain/g;->f()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 23
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v3

    invoke-static {v3, v1, v2}, Lcom/chase/sig/android/util/m;->a(Lcom/chase/sig/android/ChaseApplication;Ljava/lang/String;Ljava/util/Hashtable;)Lorg/json/JSONObject;

    move-result-object v1

    .line 25
    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/LegalAgreementGetDetailsResponse;->b(Lorg/json/JSONObject;)V

    .line 26
    const-string v2, "code"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/service/LegalAgreementGetDetailsResponse;->a(Ljava/lang/String;)V

    .line 27
    const-string v2, "status"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/service/LegalAgreementGetDetailsResponse;->b(Ljava/lang/String;)V

    .line 28
    const-string v2, "content"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/service/LegalAgreementGetDetailsResponse;->f(Ljava/lang/String;)V

    .line 29
    const-string v2, "version"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/LegalAgreementGetDetailsResponse;->e(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 35
    :goto_0
    return-object v0

    .line 31
    :catch_0
    move-exception v1

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/LegalAgreementGetDetailsResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Z)Lcom/chase/sig/android/service/LegalAgreementUpdateResponse;
    .locals 5
    .parameter
    .parameter

    .prologue
    .line 41
    const v0, 0x7f070056

    invoke-static {v0}, Lcom/chase/sig/android/service/o;->a(I)Ljava/lang/String;

    move-result-object v2

    .line 42
    new-instance v0, Lcom/chase/sig/android/service/LegalAgreementUpdateResponse;

    invoke-direct {v0}, Lcom/chase/sig/android/service/LegalAgreementUpdateResponse;-><init>()V

    .line 44
    invoke-static {}, Lcom/chase/sig/android/service/o;->c()Ljava/util/Hashtable;

    move-result-object v3

    .line 46
    :try_start_0
    const-string v1, "productId"

    invoke-virtual {v3, v1, p0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    const-string v1, "segmentId"

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v4

    invoke-virtual {v4}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v4

    iget-object v4, v4, Lcom/chase/sig/android/domain/o;->b:Lcom/chase/sig/android/domain/g;

    invoke-interface {v4}, Lcom/chase/sig/android/domain/g;->f()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v1, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    const-string v1, "accepted"

    const-string v4, "1"

    invoke-virtual {v3, v1, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    const-string v1, "denied"

    const-string v4, "0"

    invoke-virtual {v3, v1, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    const-string v4, "viewed"

    if-eqz p1, :cond_0

    const-string v1, "1"

    :goto_0
    invoke-virtual {v3, v4, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v1

    invoke-static {v1, v2, v3}, Lcom/chase/sig/android/util/m;->a(Lcom/chase/sig/android/ChaseApplication;Ljava/lang/String;Ljava/util/Hashtable;)Lorg/json/JSONObject;

    move-result-object v1

    .line 53
    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/LegalAgreementUpdateResponse;->b(Lorg/json/JSONObject;)V

    .line 54
    const-string v2, "agreementId"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/LegalAgreementUpdateResponse;->a(Ljava/lang/String;)V

    .line 60
    :goto_1
    return-object v0

    .line 50
    :cond_0
    const-string v1, "0"
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 56
    :catch_0
    move-exception v1

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/LegalAgreementUpdateResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    goto :goto_1
.end method
