.class public Lcom/chase/sig/android/service/quickdeposit/QuickDepositCompleteResponse;
.super Lcom/chase/sig/android/service/l;
.source "SourceFile"


# instance fields
.field private availableBalance:Lcom/chase/sig/android/util/Dollar;

.field private delayedAmount1:Lcom/chase/sig/android/util/Dollar;

.field private delayedAmount2:Lcom/chase/sig/android/util/Dollar;

.field private delayedAmountAvailableDate1:Ljava/lang/String;

.field private delayedAmountAvailableDate2:Ljava/lang/String;

.field private effectiveDate:Ljava/lang/String;

.field private largeDollar:Ljava/lang/String;

.field private onus:Ljava/lang/String;

.field private payingBank:Ljava/lang/String;

.field private presentBalance:Lcom/chase/sig/android/util/Dollar;

.field private standardBottom:Ljava/lang/String;

.field private variableBottom:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lcom/chase/sig/android/service/l;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/chase/sig/android/service/quickdeposit/QuickDepositCompleteResponse;->effectiveDate:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Lcom/chase/sig/android/util/Dollar;)V
    .locals 0
    .parameter

    .prologue
    .line 63
    iput-object p1, p0, Lcom/chase/sig/android/service/quickdeposit/QuickDepositCompleteResponse;->availableBalance:Lcom/chase/sig/android/util/Dollar;

    .line 64
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 59
    iput-object p1, p0, Lcom/chase/sig/android/service/quickdeposit/QuickDepositCompleteResponse;->effectiveDate:Ljava/lang/String;

    .line 60
    return-void
.end method

.method public final b(Lcom/chase/sig/android/util/Dollar;)V
    .locals 0
    .parameter

    .prologue
    .line 67
    iput-object p1, p0, Lcom/chase/sig/android/service/quickdeposit/QuickDepositCompleteResponse;->presentBalance:Lcom/chase/sig/android/util/Dollar;

    .line 68
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 75
    iput-object p1, p0, Lcom/chase/sig/android/service/quickdeposit/QuickDepositCompleteResponse;->delayedAmountAvailableDate1:Ljava/lang/String;

    .line 76
    return-void
.end method

.method public final c(Lcom/chase/sig/android/util/Dollar;)V
    .locals 0
    .parameter

    .prologue
    .line 71
    iput-object p1, p0, Lcom/chase/sig/android/service/quickdeposit/QuickDepositCompleteResponse;->delayedAmount1:Lcom/chase/sig/android/util/Dollar;

    .line 72
    return-void
.end method

.method public final d(Lcom/chase/sig/android/util/Dollar;)V
    .locals 0
    .parameter

    .prologue
    .line 79
    iput-object p1, p0, Lcom/chase/sig/android/service/quickdeposit/QuickDepositCompleteResponse;->delayedAmount2:Lcom/chase/sig/android/util/Dollar;

    .line 80
    return-void
.end method

.method public final e(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 83
    iput-object p1, p0, Lcom/chase/sig/android/service/quickdeposit/QuickDepositCompleteResponse;->delayedAmountAvailableDate2:Ljava/lang/String;

    .line 84
    return-void
.end method

.method public final f(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 91
    iput-object p1, p0, Lcom/chase/sig/android/service/quickdeposit/QuickDepositCompleteResponse;->largeDollar:Ljava/lang/String;

    .line 92
    return-void
.end method

.method public final g(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 99
    iput-object p1, p0, Lcom/chase/sig/android/service/quickdeposit/QuickDepositCompleteResponse;->payingBank:Ljava/lang/String;

    .line 100
    return-void
.end method

.method public final h(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 107
    iput-object p1, p0, Lcom/chase/sig/android/service/quickdeposit/QuickDepositCompleteResponse;->onus:Ljava/lang/String;

    .line 108
    return-void
.end method

.method public final i(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 115
    iput-object p1, p0, Lcom/chase/sig/android/service/quickdeposit/QuickDepositCompleteResponse;->standardBottom:Ljava/lang/String;

    .line 116
    return-void
.end method

.method public final j(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 123
    iput-object p1, p0, Lcom/chase/sig/android/service/quickdeposit/QuickDepositCompleteResponse;->variableBottom:Ljava/lang/String;

    .line 124
    return-void
.end method
