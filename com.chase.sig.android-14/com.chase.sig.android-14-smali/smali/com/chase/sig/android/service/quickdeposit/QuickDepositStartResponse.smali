.class public Lcom/chase/sig/android/service/quickdeposit/QuickDepositStartResponse;
.super Lcom/chase/sig/android/service/l;
.source "SourceFile"


# instance fields
.field private DeltaAmount:Lcom/chase/sig/android/util/Dollar;

.field private hasErrorWithAmount:Z

.field private hasErrorWithDepositAccount:Z

.field private isApprovedForDeposit:Z

.field private maxAmount:Lcom/chase/sig/android/util/Dollar;

.field private minAmount:Lcom/chase/sig/android/util/Dollar;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Lcom/chase/sig/android/service/l;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 0
    .parameter

    .prologue
    .line 28
    iput-boolean p1, p0, Lcom/chase/sig/android/service/quickdeposit/QuickDepositStartResponse;->hasErrorWithAmount:Z

    .line 29
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 24
    iget-boolean v0, p0, Lcom/chase/sig/android/service/quickdeposit/QuickDepositStartResponse;->hasErrorWithAmount:Z

    return v0
.end method

.method public final b(Z)V
    .locals 0
    .parameter

    .prologue
    .line 36
    iput-boolean p1, p0, Lcom/chase/sig/android/service/quickdeposit/QuickDepositStartResponse;->hasErrorWithDepositAccount:Z

    .line 37
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/chase/sig/android/service/quickdeposit/QuickDepositStartResponse;->hasErrorWithDepositAccount:Z

    return v0
.end method

.method public final c(Z)V
    .locals 0
    .parameter

    .prologue
    .line 44
    iput-boolean p1, p0, Lcom/chase/sig/android/service/quickdeposit/QuickDepositStartResponse;->isApprovedForDeposit:Z

    .line 45
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/chase/sig/android/service/quickdeposit/QuickDepositStartResponse;->isApprovedForDeposit:Z

    return v0
.end method
