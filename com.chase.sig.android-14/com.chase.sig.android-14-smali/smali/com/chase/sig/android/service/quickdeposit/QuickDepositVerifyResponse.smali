.class public Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;
.super Lcom/chase/sig/android/service/l;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private footNote:Ljava/lang/String;

.field private hasErrorWithAccountNumber:Z

.field private hasErrorWithAmount:Z

.field private hasErrorWithCheckFrontImage:Z

.field private hasErrorWithRoutingNumber:Z

.field private isAccountNumberEditable:Z

.field private isAmountEditable:Z

.field private isRoutingNumberEditable:Z

.field private itemSequenceNumber:Ljava/lang/String;

.field private readAmount:Lcom/chase/sig/android/util/Dollar;

.field private success:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/chase/sig/android/service/l;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/chase/sig/android/util/Dollar;)V
    .locals 0
    .parameter

    .prologue
    .line 91
    iput-object p1, p0, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;->readAmount:Lcom/chase/sig/android/util/Dollar;

    .line 92
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 107
    iput-object p1, p0, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;->itemSequenceNumber:Ljava/lang/String;

    .line 108
    return-void
.end method

.method public final a(Z)V
    .locals 0
    .parameter

    .prologue
    .line 30
    iput-boolean p1, p0, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;->hasErrorWithAmount:Z

    .line 31
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;->hasErrorWithAmount:Z

    return v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 115
    iput-object p1, p0, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;->footNote:Ljava/lang/String;

    .line 116
    return-void
.end method

.method public final b(Z)V
    .locals 0
    .parameter

    .prologue
    .line 39
    iput-boolean p1, p0, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;->hasErrorWithCheckFrontImage:Z

    .line 40
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;->hasErrorWithCheckFrontImage:Z

    return v0
.end method

.method public final c(Z)V
    .locals 0
    .parameter

    .prologue
    .line 51
    iput-boolean p1, p0, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;->hasErrorWithRoutingNumber:Z

    .line 52
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;->hasErrorWithCheckFrontImage:Z

    return v0
.end method

.method public final d(Z)V
    .locals 0
    .parameter

    .prologue
    .line 59
    iput-boolean p1, p0, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;->hasErrorWithAccountNumber:Z

    .line 60
    return-void
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;->hasErrorWithRoutingNumber:Z

    return v0
.end method

.method public final e(Z)V
    .locals 0
    .parameter

    .prologue
    .line 67
    iput-boolean p1, p0, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;->isAmountEditable:Z

    .line 68
    return-void
.end method

.method public final f(Z)V
    .locals 0
    .parameter

    .prologue
    .line 75
    iput-boolean p1, p0, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;->isRoutingNumberEditable:Z

    .line 76
    return-void
.end method

.method public final g(Z)V
    .locals 0
    .parameter

    .prologue
    .line 83
    iput-boolean p1, p0, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;->isAccountNumberEditable:Z

    .line 84
    return-void
.end method

.method public final h(Z)V
    .locals 0
    .parameter

    .prologue
    .line 95
    iput-boolean p1, p0, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;->success:Z

    .line 96
    return-void
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;->hasErrorWithAccountNumber:Z

    return v0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;->isRoutingNumberEditable:Z

    return v0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 79
    iget-boolean v0, p0, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;->isAccountNumberEditable:Z

    return v0
.end method

.method public final m()Lcom/chase/sig/android/util/Dollar;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;->readAmount:Lcom/chase/sig/android/util/Dollar;

    return-object v0
.end method

.method public final n()Z
    .locals 1

    .prologue
    .line 99
    iget-boolean v0, p0, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;->success:Z

    return v0
.end method

.method public final o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;->itemSequenceNumber:Ljava/lang/String;

    return-object v0
.end method

.method public final p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;->footNote:Ljava/lang/String;

    return-object v0
.end method
