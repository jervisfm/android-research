.class public final Lcom/chase/sig/android/service/quickdeposit/a;
.super Lcom/chase/sig/android/service/JPService;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/chase/sig/android/service/JPService;-><init>()V

    return-void
.end method

.method public static a()Lcom/chase/sig/android/service/quickdeposit/QuickDepositAccountsResponse;
    .locals 5

    .prologue
    .line 24
    new-instance v1, Lcom/chase/sig/android/service/quickdeposit/QuickDepositAccountsResponse;

    invoke-direct {v1}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositAccountsResponse;-><init>()V

    .line 27
    :try_start_0
    invoke-static {}, Lcom/chase/sig/android/service/quickdeposit/a;->c()Ljava/util/Hashtable;

    move-result-object v0

    .line 29
    const v2, 0x7f070057

    invoke-static {v2}, Lcom/chase/sig/android/service/quickdeposit/a;->a(I)Ljava/lang/String;

    move-result-object v2

    .line 30
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v3

    invoke-static {v3, v2, v0}, Lcom/chase/sig/android/util/m;->a(Lcom/chase/sig/android/ChaseApplication;Ljava/lang/String;Ljava/util/Hashtable;)Lorg/json/JSONObject;

    move-result-object v0

    .line 32
    invoke-virtual {v1, v0}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositAccountsResponse;->b(Lorg/json/JSONObject;)V

    .line 34
    invoke-virtual {v1}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositAccountsResponse;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 47
    :goto_0
    return-object v1

    .line 38
    :cond_0
    new-instance v2, Lcom/google/gson/chase/GsonBuilder;

    invoke-direct {v2}, Lcom/google/gson/chase/GsonBuilder;-><init>()V

    const-class v3, Lcom/chase/sig/android/util/Dollar;

    new-instance v4, Lcom/chase/sig/android/domain/d;

    invoke-direct {v4}, Lcom/chase/sig/android/domain/d;-><init>()V

    invoke-virtual {v2, v3, v4}, Lcom/google/gson/chase/GsonBuilder;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/chase/GsonBuilder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/gson/chase/GsonBuilder;->b()Lcom/google/gson/chase/Gson;

    move-result-object v2

    .line 40
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    const-class v3, Lcom/chase/sig/android/service/quickdeposit/QuickDepositAccountsResponse;

    invoke-virtual {v2, v0, v3}, Lcom/google/gson/chase/Gson;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/service/quickdeposit/QuickDepositAccountsResponse;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    move-object v1, v0

    .line 47
    goto :goto_0

    .line 42
    :catch_0
    move-exception v0

    .line 43
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositAccountsResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    move-object v0, v1

    goto :goto_1
.end method

.method public static a(Lcom/chase/sig/android/domain/QuickDeposit;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/service/quickdeposit/QuickDepositCompleteResponse;
    .locals 4
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 157
    new-instance v0, Lcom/chase/sig/android/service/quickdeposit/QuickDepositCompleteResponse;

    invoke-direct {v0}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositCompleteResponse;-><init>()V

    .line 160
    :try_start_0
    invoke-static {}, Lcom/chase/sig/android/service/quickdeposit/a;->c()Ljava/util/Hashtable;

    move-result-object v1

    .line 164
    invoke-static {p2}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {p1}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {p3}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 168
    :cond_0
    const-string v2, "updateRequired"

    const-string v3, "true"

    invoke-virtual {v1, v2, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 169
    const-string v2, "itemSequenceNumber"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickDeposit;->p()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 171
    const-string v2, "correctedAccountNumber"

    invoke-static {v1, v2, p2}, Lcom/chase/sig/android/service/quickdeposit/a;->a(Ljava/util/Hashtable;Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    const-string v2, "correctedRoutingNumber"

    invoke-static {v1, v2, p1}, Lcom/chase/sig/android/service/quickdeposit/a;->a(Ljava/util/Hashtable;Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    const-string v2, "correctedDepositAmount"

    invoke-static {v1, v2, p3}, Lcom/chase/sig/android/service/quickdeposit/a;->a(Ljava/util/Hashtable;Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    :cond_1
    const-string v2, "transactionId"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickDeposit;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 178
    const-string v2, "startDepositAmount"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickDeposit;->i()Lcom/chase/sig/android/util/Dollar;

    move-result-object v3

    invoke-virtual {v3}, Lcom/chase/sig/android/util/Dollar;->b()Ljava/math/BigDecimal;

    move-result-object v3

    invoke-virtual {v3}, Ljava/math/BigDecimal;->toPlainString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 180
    const v2, 0x7f07005a

    invoke-static {v2}, Lcom/chase/sig/android/service/quickdeposit/a;->a(I)Ljava/lang/String;

    move-result-object v2

    .line 182
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v3

    invoke-static {v3, v2, v1}, Lcom/chase/sig/android/util/m;->a(Lcom/chase/sig/android/ChaseApplication;Ljava/lang/String;Ljava/util/Hashtable;)Lorg/json/JSONObject;

    move-result-object v1

    .line 184
    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositCompleteResponse;->b(Lorg/json/JSONObject;)V

    .line 185
    const-string v2, "effectiveDate"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositCompleteResponse;->a(Ljava/lang/String;)V

    .line 186
    new-instance v2, Lcom/chase/sig/android/util/Dollar;

    const-string v3, "availableBalance"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/chase/sig/android/util/Dollar;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositCompleteResponse;->a(Lcom/chase/sig/android/util/Dollar;)V

    .line 187
    new-instance v2, Lcom/chase/sig/android/util/Dollar;

    const-string v3, "presentBalance"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/chase/sig/android/util/Dollar;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositCompleteResponse;->b(Lcom/chase/sig/android/util/Dollar;)V

    .line 188
    const-string v2, "effectiveDate"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositCompleteResponse;->a(Ljava/lang/String;)V

    .line 189
    new-instance v2, Lcom/chase/sig/android/util/Dollar;

    const-string v3, "delayedAmount1"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/chase/sig/android/util/Dollar;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositCompleteResponse;->c(Lcom/chase/sig/android/util/Dollar;)V

    .line 190
    const-string v2, "delayedAmountAvailableDate1"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositCompleteResponse;->b(Ljava/lang/String;)V

    .line 192
    new-instance v2, Lcom/chase/sig/android/util/Dollar;

    const-string v3, "delayedAmount2"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/chase/sig/android/util/Dollar;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositCompleteResponse;->d(Lcom/chase/sig/android/util/Dollar;)V

    .line 193
    const-string v2, "delayedAmountAvailableDate2"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositCompleteResponse;->e(Ljava/lang/String;)V

    .line 195
    const-string v2, "payingBank"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositCompleteResponse;->g(Ljava/lang/String;)V

    .line 196
    const-string v2, "onus"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositCompleteResponse;->h(Ljava/lang/String;)V

    .line 197
    const-string v2, "standardBottom"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositCompleteResponse;->i(Ljava/lang/String;)V

    .line 198
    const-string v2, "variableBottom"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositCompleteResponse;->j(Ljava/lang/String;)V

    .line 199
    const-string v2, "largeDollar"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositCompleteResponse;->f(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/chase/sig/android/util/ChaseException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    .line 209
    :goto_0
    return-object v0

    .line 201
    :catch_0
    move-exception v1

    .line 202
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositCompleteResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    goto :goto_0

    .line 204
    :catch_1
    move-exception v1

    .line 205
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositCompleteResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    goto :goto_0
.end method

.method public static a(Lcom/chase/sig/android/domain/QuickDeposit;)Lcom/chase/sig/android/service/quickdeposit/QuickDepositStartResponse;
    .locals 4
    .parameter

    .prologue
    .line 52
    new-instance v0, Lcom/chase/sig/android/service/quickdeposit/QuickDepositStartResponse;

    invoke-direct {v0}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositStartResponse;-><init>()V

    .line 55
    :try_start_0
    invoke-static {}, Lcom/chase/sig/android/service/quickdeposit/a;->c()Ljava/util/Hashtable;

    move-result-object v1

    .line 56
    const-string v2, "amount"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickDeposit;->i()Lcom/chase/sig/android/util/Dollar;

    move-result-object v3

    invoke-virtual {v3}, Lcom/chase/sig/android/util/Dollar;->b()Ljava/math/BigDecimal;

    move-result-object v3

    invoke-virtual {v3}, Ljava/math/BigDecimal;->toPlainString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    const-string v2, "accountId"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickDeposit;->o()Lcom/chase/sig/android/domain/QuickDepositAccount;

    move-result-object v3

    invoke-virtual {v3}, Lcom/chase/sig/android/domain/QuickDepositAccount;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickDeposit;->m()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 60
    const-string v2, "ulid"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickDeposit;->m()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    :cond_0
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v2

    .line 65
    const v3, 0x7f070058

    invoke-static {v3}, Lcom/chase/sig/android/service/quickdeposit/a;->a(I)Ljava/lang/String;

    move-result-object v3

    .line 66
    invoke-static {v2, v3, v1}, Lcom/chase/sig/android/util/m;->a(Lcom/chase/sig/android/ChaseApplication;Ljava/lang/String;Ljava/util/Hashtable;)Lorg/json/JSONObject;

    move-result-object v1

    .line 68
    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositStartResponse;->b(Lorg/json/JSONObject;)V

    .line 69
    const-string v2, "approvedForDeposit"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositStartResponse;->c(Z)V

    .line 70
    const-string v2, "hasErrorWithAmount"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositStartResponse;->a(Z)V

    .line 71
    const-string v2, "hasErrorWitDepositAccount"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositStartResponse;->b(Z)V

    .line 72
    const-string v2, "transactionId"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/domain/QuickDeposit;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/chase/sig/android/util/ChaseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 79
    :goto_0
    return-object v0

    .line 74
    :catch_0
    move-exception v1

    .line 75
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositStartResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    goto :goto_0
.end method

.method public static b(Lcom/chase/sig/android/domain/QuickDeposit;)Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;
    .locals 10
    .parameter

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 84
    new-instance v9, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;

    invoke-direct {v9}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;-><init>()V

    .line 87
    :try_start_0
    invoke-static {}, Lcom/chase/sig/android/service/quickdeposit/a;->c()Ljava/util/Hashtable;

    move-result-object v2

    .line 88
    const-string v0, "transactionId"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickDeposit;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    const-string v0, "submitCounter"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickDeposit;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    const-string v0, "amount"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickDeposit;->i()Lcom/chase/sig/android/util/Dollar;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/util/Dollar;->b()Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v1}, Ljava/math/BigDecimal;->toPlainString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    const v0, 0x7f070059

    invoke-static {v0}, Lcom/chase/sig/android/service/quickdeposit/a;->a(I)Ljava/lang/String;

    move-result-object v1

    .line 94
    const/4 v0, 0x2

    new-array v3, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v4, "checkImageFront"

    aput-object v4, v3, v0

    const/4 v0, 0x1

    const-string v4, "checkImageBack"

    aput-object v4, v3, v0

    .line 97
    const/4 v0, 0x2

    new-array v5, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v4, "image/jpeg"

    aput-object v4, v5, v0

    const/4 v0, 0x1

    const-string v4, "image/jpeg"

    aput-object v4, v5, v0

    .line 100
    const/4 v0, 0x2

    new-array v4, v0, [[B

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickDeposit;->f()[B

    move-result-object v6

    aput-object v6, v4, v0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickDeposit;->d()[B

    move-result-object v6

    aput-object v6, v4, v0

    .line 104
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v6

    invoke-virtual {v6}, Lcom/chase/sig/android/ChaseApplication;->h()Ljava/lang/String;

    move-result-object v6

    invoke-static/range {v0 .. v6}, Lcom/chase/sig/android/util/m;->a(Lcom/chase/sig/android/ChaseApplication;Ljava/lang/String;Ljava/util/Hashtable;[Ljava/lang/String;[[B[Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 108
    invoke-virtual {v9, v1}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;->b(Lorg/json/JSONObject;)V

    .line 110
    const-string v0, "hasErrorWithAccountNumber"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 111
    invoke-virtual {v9, v0}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;->d(Z)V

    .line 112
    const-string v2, "isAccountNumberEditable"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    if-eqz v0, :cond_5

    :cond_0
    move v0, v8

    :goto_0
    invoke-virtual {v9, v0}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;->g(Z)V

    .line 114
    const-string v0, "isAmountEditable"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {v9, v0}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;->e(Z)V

    .line 116
    const-string v0, "hasErrorWithRoutingNumber"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 117
    invoke-virtual {v9, v0}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;->c(Z)V

    .line 118
    const-string v2, "isRoutingNumberEditable"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    if-eqz v0, :cond_6

    :cond_1
    move v0, v8

    :goto_1
    invoke-virtual {v9, v0}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;->f(Z)V

    .line 120
    const-string v0, "hasErrorWithAmount"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {v9, v0}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;->a(Z)V

    .line 121
    const-string v0, "hasErrorWithCheckImages"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {v9, v0}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;->b(Z)V

    .line 123
    const-string v0, "itemSequenceNumber"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;->a(Ljava/lang/String;)V

    .line 125
    const-string v0, "success"

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {v9, v0}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;->h(Z)V

    .line 127
    const-string v0, "trayContent"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;->b(Ljava/lang/String;)V

    .line 128
    const-string v0, "readRoutingNumber"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/domain/QuickDeposit;->b(Ljava/lang/String;)V

    .line 129
    const-string v0, "readAccountNumber"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/domain/QuickDeposit;->c(Ljava/lang/String;)V

    .line 131
    const-string v0, "depositAmount"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 132
    invoke-static {v0}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 133
    new-instance v2, Lcom/chase/sig/android/util/Dollar;

    invoke-direct {v2, v0}, Lcom/chase/sig/android/util/Dollar;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v2}, Lcom/chase/sig/android/domain/QuickDeposit;->a(Lcom/chase/sig/android/util/Dollar;)V

    .line 136
    :cond_2
    const-string v0, "readAmount"

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 137
    invoke-static {v0}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 138
    new-instance v1, Lcom/chase/sig/android/util/Dollar;

    invoke-direct {v1, v0}, Lcom/chase/sig/android/util/Dollar;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v1}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;->a(Lcom/chase/sig/android/util/Dollar;)V

    .line 141
    :cond_3
    const-string v0, "20452"

    invoke-virtual {v9, v0}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 142
    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickDeposit;->a()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/domain/QuickDeposit;->a(I)V
    :try_end_0
    .catch Lcom/chase/sig/android/util/ChaseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 150
    :cond_4
    :goto_2
    return-object v9

    :cond_5
    move v0, v7

    .line 112
    goto/16 :goto_0

    :cond_6
    move v0, v7

    .line 118
    goto/16 :goto_1

    .line 145
    :catch_0
    move-exception v0

    .line 146
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v9, v0}, Lcom/chase/sig/android/service/quickdeposit/QuickDepositVerifyResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    goto :goto_2
.end method
