.class public Lcom/chase/sig/android/service/quickpay/PayFromAccount;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private balance:Lcom/chase/sig/android/util/Dollar;

.field private displayName:Ljava/lang/String;

.field private id:Ljava/lang/String;

.field private isDDA:Z

.field private maskedAccountNumber:Ljava/lang/String;

.field private nickname:Ljava/lang/String;

.field private requiresCvv:Z

.field private sourceIndicator:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/chase/sig/android/service/quickpay/PayFromAccount;->isDDA:Z

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/chase/sig/android/service/quickpay/PayFromAccount;->displayName:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Lcom/chase/sig/android/util/Dollar;)V
    .locals 0
    .parameter

    .prologue
    .line 54
    iput-object p1, p0, Lcom/chase/sig/android/service/quickpay/PayFromAccount;->balance:Lcom/chase/sig/android/util/Dollar;

    .line 55
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 24
    iput-object p1, p0, Lcom/chase/sig/android/service/quickpay/PayFromAccount;->displayName:Ljava/lang/String;

    .line 25
    return-void
.end method

.method public final a(Z)V
    .locals 0
    .parameter

    .prologue
    .line 62
    iput-boolean p1, p0, Lcom/chase/sig/android/service/quickpay/PayFromAccount;->requiresCvv:Z

    .line 63
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/chase/sig/android/service/quickpay/PayFromAccount;->id:Ljava/lang/String;

    return-object v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 30
    iput-object p1, p0, Lcom/chase/sig/android/service/quickpay/PayFromAccount;->maskedAccountNumber:Ljava/lang/String;

    .line 31
    return-void
.end method

.method public final b(Z)V
    .locals 0
    .parameter

    .prologue
    .line 66
    iput-boolean p1, p0, Lcom/chase/sig/android/service/quickpay/PayFromAccount;->isDDA:Z

    .line 67
    return-void
.end method

.method public final c()Lcom/chase/sig/android/util/Dollar;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/chase/sig/android/service/quickpay/PayFromAccount;->balance:Lcom/chase/sig/android/util/Dollar;

    return-object v0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 36
    iput-object p1, p0, Lcom/chase/sig/android/service/quickpay/PayFromAccount;->id:Ljava/lang/String;

    .line 37
    return-void
.end method

.method public final d(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 42
    iput-object p1, p0, Lcom/chase/sig/android/service/quickpay/PayFromAccount;->nickname:Ljava/lang/String;

    .line 43
    return-void
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 58
    iget-boolean v0, p0, Lcom/chase/sig/android/service/quickpay/PayFromAccount;->requiresCvv:Z

    return v0
.end method

.method public final e(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 48
    iput-object p1, p0, Lcom/chase/sig/android/service/quickpay/PayFromAccount;->sourceIndicator:Ljava/lang/String;

    .line 49
    return-void
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 69
    iget-boolean v0, p0, Lcom/chase/sig/android/service/quickpay/PayFromAccount;->isDDA:Z

    return v0
.end method
