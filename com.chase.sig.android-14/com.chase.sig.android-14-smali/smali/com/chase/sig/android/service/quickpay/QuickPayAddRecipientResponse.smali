.class public Lcom/chase/sig/android/service/quickpay/QuickPayAddRecipientResponse;
.super Lcom/chase/sig/android/service/l;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private recipient:Lcom/chase/sig/android/domain/QuickPayRecipient;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/chase/sig/android/service/l;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/chase/sig/android/domain/QuickPayRecipient;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/chase/sig/android/service/quickpay/QuickPayAddRecipientResponse;->recipient:Lcom/chase/sig/android/domain/QuickPayRecipient;

    return-object v0
.end method

.method public final a(Lcom/chase/sig/android/domain/QuickPayRecipient;)V
    .locals 0
    .parameter

    .prologue
    .line 15
    iput-object p1, p0, Lcom/chase/sig/android/service/quickpay/QuickPayAddRecipientResponse;->recipient:Lcom/chase/sig/android/domain/QuickPayRecipient;

    .line 16
    return-void
.end method
