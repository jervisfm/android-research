.class public Lcom/chase/sig/android/service/quickpay/QuickPayCancelPendingTransactionResponse;
.super Lcom/chase/sig/android/service/l;
.source "SourceFile"


# instance fields
.field private pendingTransaction:Lcom/chase/sig/android/domain/QuickPayPendingTransaction;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Lcom/chase/sig/android/service/l;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/chase/sig/android/domain/QuickPayPendingTransaction;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/chase/sig/android/service/quickpay/QuickPayCancelPendingTransactionResponse;->pendingTransaction:Lcom/chase/sig/android/domain/QuickPayPendingTransaction;

    return-object v0
.end method

.method public final a(Lcom/chase/sig/android/domain/QuickPayPendingTransaction;)V
    .locals 0
    .parameter

    .prologue
    .line 13
    iput-object p1, p0, Lcom/chase/sig/android/service/quickpay/QuickPayCancelPendingTransactionResponse;->pendingTransaction:Lcom/chase/sig/android/domain/QuickPayPendingTransaction;

    .line 14
    return-void
.end method
