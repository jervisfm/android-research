.class public Lcom/chase/sig/android/service/quickpay/QuickPayRecipientListResponse;
.super Lcom/chase/sig/android/service/l;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private contentHeader:Ljava/lang/String;

.field private contentTitle:Ljava/lang/String;

.field private recipients:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/chase/sig/android/domain/QuickPayRecipient;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/chase/sig/android/service/l;-><init>()V

    .line 18
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/chase/sig/android/domain/QuickPayRecipient;",
            ">;"
        }
    .end annotation

    .prologue
    .line 41
    iget-object v0, p0, Lcom/chase/sig/android/service/quickpay/QuickPayRecipientListResponse;->recipients:Ljava/util/ArrayList;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 21
    iput-object p1, p0, Lcom/chase/sig/android/service/quickpay/QuickPayRecipientListResponse;->contentTitle:Ljava/lang/String;

    .line 22
    return-void
.end method

.method public final a(Ljava/util/ArrayList;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/chase/sig/android/domain/QuickPayRecipient;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 37
    iput-object p1, p0, Lcom/chase/sig/android/service/quickpay/QuickPayRecipientListResponse;->recipients:Ljava/util/ArrayList;

    .line 38
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 29
    iput-object p1, p0, Lcom/chase/sig/android/service/quickpay/QuickPayRecipientListResponse;->contentHeader:Ljava/lang/String;

    .line 30
    return-void
.end method
