.class public Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionConfirmResponse;
.super Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;
.source "SourceFile"


# instance fields
.field private senderCode:Ljava/lang/String;

.field private status:Ljava/lang/String;

.field private transactionId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionConfirmResponse;->status:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 11
    iput-object p1, p0, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionConfirmResponse;->status:Ljava/lang/String;

    .line 12
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionConfirmResponse;->transactionId:Ljava/lang/String;

    return-object v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 23
    iput-object p1, p0, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionConfirmResponse;->transactionId:Ljava/lang/String;

    .line 24
    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionConfirmResponse;->senderCode:Ljava/lang/String;

    return-object v0
.end method

.method public final e(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 31
    iput-object p1, p0, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionConfirmResponse;->senderCode:Ljava/lang/String;

    .line 32
    return-void
.end method
