.class public Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;
.super Lcom/chase/sig/android/service/l;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private accountName:Ljava/lang/String;

.field private amount:Ljava/lang/String;

.field private dueDate:Ljava/lang/String;

.field private duplicateMessage:Ljava/lang/String;

.field private feeDisclaimer:Ljava/lang/String;

.field private formId:Ljava/lang/String;

.field private frequency:Ljava/lang/String;

.field private invoiceId:Ljava/lang/String;

.field private isAtmEligible:Z

.field private isDuplicate:Z

.field private isNickNameMatched:Ljava/lang/String;

.field private isWarning:Z

.field private memo:Ljava/lang/String;

.field private numberOfPayments:I

.field private recipient:Lcom/chase/sig/android/domain/QuickPayRecipient;

.field private repeatingPayment:Z

.field private sendOnDate:Ljava/lang/String;

.field private smsEligible:Z

.field private smsReason:Ljava/lang/String;

.field private unlimitedPayments:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/chase/sig/android/service/l;-><init>()V

    .line 28
    const-string v0, ""

    iput-object v0, p0, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->isNickNameMatched:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 0
    .parameter

    .prologue
    .line 130
    iput p1, p0, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->numberOfPayments:I

    .line 131
    return-void
.end method

.method public final a(Lcom/chase/sig/android/domain/QuickPayRecipient;)V
    .locals 0
    .parameter

    .prologue
    .line 154
    iput-object p1, p0, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->recipient:Lcom/chase/sig/android/domain/QuickPayRecipient;

    .line 155
    return-void
.end method

.method public final a(Z)V
    .locals 0
    .parameter

    .prologue
    .line 86
    iput-boolean p1, p0, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->isDuplicate:Z

    .line 87
    return-void
.end method

.method public final b(Z)V
    .locals 0
    .parameter

    .prologue
    .line 106
    iput-boolean p1, p0, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->repeatingPayment:Z

    .line 107
    return-void
.end method

.method public final c(Z)V
    .locals 0
    .parameter

    .prologue
    .line 138
    iput-boolean p1, p0, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->unlimitedPayments:Z

    .line 139
    return-void
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->formId:Ljava/lang/String;

    return-object v0
.end method

.method public final d(Z)V
    .locals 0
    .parameter

    .prologue
    .line 184
    iput-boolean p1, p0, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->isWarning:Z

    .line 185
    return-void
.end method

.method public final e(Z)V
    .locals 0
    .parameter

    .prologue
    .line 192
    iput-boolean p1, p0, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->smsEligible:Z

    .line 193
    return-void
.end method

.method public final f(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 46
    iput-object p1, p0, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->accountName:Ljava/lang/String;

    .line 47
    return-void
.end method

.method public final g(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 54
    iput-object p1, p0, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->amount:Ljava/lang/String;

    .line 55
    return-void
.end method

.method public final h(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 62
    iput-object p1, p0, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->invoiceId:Ljava/lang/String;

    .line 63
    return-void
.end method

.method public final i(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 70
    iput-object p1, p0, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->sendOnDate:Ljava/lang/String;

    .line 71
    return-void
.end method

.method public final j(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 78
    iput-object p1, p0, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->memo:Ljava/lang/String;

    .line 79
    return-void
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->isDuplicate:Z

    return v0
.end method

.method public final k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->duplicateMessage:Ljava/lang/String;

    return-object v0
.end method

.method public final k(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 94
    iput-object p1, p0, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->duplicateMessage:Ljava/lang/String;

    .line 95
    return-void
.end method

.method public final l(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 102
    iput-object p1, p0, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->dueDate:Ljava/lang/String;

    .line 103
    return-void
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 118
    iget-boolean v0, p0, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->isAtmEligible:Z

    return v0
.end method

.method public final m()V
    .locals 1

    .prologue
    .line 122
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->isAtmEligible:Z

    .line 123
    return-void
.end method

.method public final m(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 114
    iput-object p1, p0, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->frequency:Ljava/lang/String;

    .line 115
    return-void
.end method

.method public final n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->feeDisclaimer:Ljava/lang/String;

    return-object v0
.end method

.method public final n(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 146
    iput-object p1, p0, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->feeDisclaimer:Ljava/lang/String;

    .line 147
    return-void
.end method

.method public final o()Lcom/chase/sig/android/domain/QuickPayRecipient;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->recipient:Lcom/chase/sig/android/domain/QuickPayRecipient;

    return-object v0
.end method

.method public final o(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 162
    iput-object p1, p0, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->formId:Ljava/lang/String;

    .line 163
    return-void
.end method

.method public final p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->smsReason:Ljava/lang/String;

    return-object v0
.end method

.method public final p(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 166
    iput-object p1, p0, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->smsReason:Ljava/lang/String;

    .line 167
    return-void
.end method

.method public final q(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 180
    iput-object p1, p0, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->isNickNameMatched:Ljava/lang/String;

    .line 181
    return-void
.end method

.method public final q()Z
    .locals 2

    .prologue
    .line 174
    iget-object v0, p0, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->isNickNameMatched:Ljava/lang/String;

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->isNickNameMatched:Ljava/lang/String;

    const-string v1, "yes"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final r()Z
    .locals 1

    .prologue
    .line 188
    iget-boolean v0, p0, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->isWarning:Z

    return v0
.end method

.method public final s()Z
    .locals 1

    .prologue
    .line 196
    iget-boolean v0, p0, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->smsEligible:Z

    return v0
.end method
