.class public Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;
.super Lcom/chase/sig/android/service/l;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private activities:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/chase/sig/android/domain/QuickPayActivityItem;",
            ">;"
        }
    .end annotation
.end field

.field private contentEmptyMessage:Ljava/lang/String;

.field private contentHeader1:Ljava/lang/String;

.field private contentSubtitle:Ljava/lang/String;

.field private contentTitle:Ljava/lang/String;

.field private currentActivityType:Lcom/chase/sig/android/domain/QuickPayActivityType;

.field private currentPage:I

.field private nextPage:I

.field private rowsLeftToShow:I

.field private showNext:Ljava/lang/String;

.field private totalRows:I

.field private transactions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/chase/sig/android/domain/QuickPayPendingTransaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/chase/sig/android/service/l;-><init>()V

    .line 25
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->activities:Ljava/util/ArrayList;

    .line 26
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->transactions:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->contentSubtitle:Ljava/lang/String;

    return-object v0
.end method

.method public final a(I)V
    .locals 0
    .parameter

    .prologue
    .line 74
    iput p1, p0, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->totalRows:I

    .line 75
    return-void
.end method

.method public final a(Lcom/chase/sig/android/domain/QuickPayActivityType;)V
    .locals 0
    .parameter

    .prologue
    .line 106
    iput-object p1, p0, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->currentActivityType:Lcom/chase/sig/android/domain/QuickPayActivityType;

    .line 107
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 34
    iput-object p1, p0, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->contentTitle:Ljava/lang/String;

    .line 35
    return-void
.end method

.method public final a(Ljava/util/ArrayList;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/chase/sig/android/domain/QuickPayActivityItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 54
    iput-object p1, p0, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->activities:Ljava/util/ArrayList;

    .line 55
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->contentHeader1:Ljava/lang/String;

    return-object v0
.end method

.method public final b(I)V
    .locals 0
    .parameter

    .prologue
    .line 90
    iput p1, p0, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->nextPage:I

    .line 91
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 42
    iput-object p1, p0, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->contentSubtitle:Ljava/lang/String;

    .line 43
    return-void
.end method

.method public final b(Ljava/util/ArrayList;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/chase/sig/android/domain/QuickPayPendingTransaction;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 114
    iput-object p1, p0, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->transactions:Ljava/util/ArrayList;

    .line 115
    return-void
.end method

.method public final c()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/chase/sig/android/domain/QuickPayActivityItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 58
    iget-object v0, p0, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->activities:Ljava/util/ArrayList;

    return-object v0
.end method

.method public final c(I)V
    .locals 0
    .parameter

    .prologue
    .line 98
    iput p1, p0, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->currentPage:I

    .line 99
    return-void
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->showNext:Ljava/lang/String;

    return-object v0
.end method

.method public final e(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 50
    iput-object p1, p0, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->contentHeader1:Ljava/lang/String;

    .line 51
    return-void
.end method

.method public final f(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 66
    iput-object p1, p0, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->showNext:Ljava/lang/String;

    .line 67
    return-void
.end method

.method public final g(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 122
    iput-object p1, p0, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->contentEmptyMessage:Ljava/lang/String;

    .line 123
    return-void
.end method

.method public final j()I
    .locals 1

    .prologue
    .line 70
    iget v0, p0, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->totalRows:I

    return v0
.end method

.method public final k()I
    .locals 1

    .prologue
    .line 86
    iget v0, p0, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->nextPage:I

    return v0
.end method

.method public final l()I
    .locals 1

    .prologue
    .line 94
    iget v0, p0, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->currentPage:I

    return v0
.end method

.method public final m()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/chase/sig/android/domain/QuickPayPendingTransaction;",
            ">;"
        }
    .end annotation

    .prologue
    .line 110
    iget-object v0, p0, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->transactions:Ljava/util/ArrayList;

    return-object v0
.end method

.method public final n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->contentEmptyMessage:Ljava/lang/String;

    return-object v0
.end method
