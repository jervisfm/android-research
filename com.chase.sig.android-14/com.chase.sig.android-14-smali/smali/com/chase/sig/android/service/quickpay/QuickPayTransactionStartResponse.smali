.class public Lcom/chase/sig/android/service/quickpay/QuickPayTransactionStartResponse;
.super Lcom/chase/sig/android/service/l;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private cutoffNotice:Ljava/lang/String;

.field payFromAccounts:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/chase/sig/android/service/quickpay/PayFromAccount;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/chase/sig/android/service/l;-><init>()V

    .line 13
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionStartResponse;->payFromAccounts:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public final a()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/chase/sig/android/service/quickpay/PayFromAccount;",
            ">;"
        }
    .end annotation

    .prologue
    .line 16
    iget-object v0, p0, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionStartResponse;->payFromAccounts:Ljava/util/ArrayList;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 24
    iput-object p1, p0, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionStartResponse;->cutoffNotice:Ljava/lang/String;

    .line 25
    return-void
.end method

.method public final a(Ljava/util/ArrayList;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/chase/sig/android/service/quickpay/PayFromAccount;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 20
    iput-object p1, p0, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionStartResponse;->payFromAccounts:Ljava/util/ArrayList;

    .line 21
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionStartResponse;->cutoffNotice:Ljava/lang/String;

    return-object v0
.end method
