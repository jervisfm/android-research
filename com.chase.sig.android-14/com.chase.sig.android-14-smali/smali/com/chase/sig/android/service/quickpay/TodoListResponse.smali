.class public Lcom/chase/sig/android/service/quickpay/TodoListResponse;
.super Lcom/chase/sig/android/service/l;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private accountMessage:Ljava/lang/String;

.field private canAcceptMoney:Z

.field private canRequestMoney:Z

.field private canSeeAtm:Z

.field private canSendMoney:Z

.field private canViewActivity:Z

.field private canViewTodo:Z

.field private contentEmptyMessage:Ljava/lang/String;

.field private contentHeader:Ljava/lang/String;

.field private contentTitle:Ljava/lang/String;

.field private defaultQuickPayAccount:Lcom/chase/sig/android/service/quickpay/PayFromAccount;

.field private earliestSendOnDate:Ljava/lang/String;

.field private frequencyList:Lcom/chase/sig/android/domain/m;

.field private missingPmtMessage:Ljava/lang/String;

.field private nextPage:I

.field private thisPage:I

.field private todoItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/service/TodoItem;",
            ">;"
        }
    .end annotation
.end field

.field private totalItems:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/chase/sig/android/service/l;-><init>()V

    .line 30
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->todoItems:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 0
    .parameter

    .prologue
    .line 71
    iput p1, p0, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->totalItems:I

    .line 72
    return-void
.end method

.method public final a(Lcom/chase/sig/android/domain/m;)V
    .locals 0
    .parameter

    .prologue
    .line 180
    iput-object p1, p0, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->frequencyList:Lcom/chase/sig/android/domain/m;

    .line 181
    return-void
.end method

.method public final a(Lcom/chase/sig/android/service/quickpay/PayFromAccount;)V
    .locals 0
    .parameter

    .prologue
    .line 115
    iput-object p1, p0, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->defaultQuickPayAccount:Lcom/chase/sig/android/service/quickpay/PayFromAccount;

    .line 116
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 39
    iput-object p1, p0, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->contentTitle:Ljava/lang/String;

    .line 40
    return-void
.end method

.method public final a(Z)V
    .locals 0
    .parameter

    .prologue
    .line 119
    iput-boolean p1, p0, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->canRequestMoney:Z

    .line 121
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 51
    iget-boolean v0, p0, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->canSeeAtm:Z

    return v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->canSeeAtm:Z

    .line 56
    return-void
.end method

.method public final b(I)V
    .locals 0
    .parameter

    .prologue
    .line 91
    iput p1, p0, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->nextPage:I

    .line 92
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 47
    iput-object p1, p0, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->contentHeader:Ljava/lang/String;

    .line 48
    return-void
.end method

.method public final b(Z)V
    .locals 0
    .parameter

    .prologue
    .line 136
    iput-boolean p1, p0, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->canSendMoney:Z

    .line 137
    return-void
.end method

.method public final c()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/service/TodoItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59
    iget-object v0, p0, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->todoItems:Ljava/util/List;

    return-object v0
.end method

.method public final c(I)V
    .locals 0
    .parameter

    .prologue
    .line 99
    iput p1, p0, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->thisPage:I

    .line 100
    return-void
.end method

.method public final c(Z)V
    .locals 0
    .parameter

    .prologue
    .line 144
    iput-boolean p1, p0, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->canAcceptMoney:Z

    .line 145
    return-void
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 75
    iget v0, p0, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->totalItems:I

    return v0
.end method

.method public final d(Z)V
    .locals 0
    .parameter

    .prologue
    .line 152
    iput-boolean p1, p0, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->canViewTodo:Z

    .line 153
    return-void
.end method

.method public final e(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 83
    iput-object p1, p0, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->earliestSendOnDate:Ljava/lang/String;

    .line 84
    return-void
.end method

.method public final e(Z)V
    .locals 0
    .parameter

    .prologue
    .line 160
    iput-boolean p1, p0, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->canViewActivity:Z

    .line 161
    return-void
.end method

.method public final f(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 103
    iput-object p1, p0, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->contentEmptyMessage:Ljava/lang/String;

    .line 104
    return-void
.end method

.method public final g(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 128
    iput-object p1, p0, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->accountMessage:Ljava/lang/String;

    .line 129
    return-void
.end method

.method public final h(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 168
    iput-object p1, p0, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->missingPmtMessage:Ljava/lang/String;

    .line 169
    return-void
.end method

.method public final j()Ljava/util/Date;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->earliestSendOnDate:Ljava/lang/String;

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->g(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public final k()I
    .locals 1

    .prologue
    .line 87
    iget v0, p0, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->nextPage:I

    return v0
.end method

.method public final l()I
    .locals 1

    .prologue
    .line 95
    iget v0, p0, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->thisPage:I

    return v0
.end method

.method public final m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->contentEmptyMessage:Ljava/lang/String;

    return-object v0
.end method

.method public final n()Lcom/chase/sig/android/service/quickpay/PayFromAccount;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->defaultQuickPayAccount:Lcom/chase/sig/android/service/quickpay/PayFromAccount;

    return-object v0
.end method

.method public final o()Z
    .locals 1

    .prologue
    .line 124
    iget-boolean v0, p0, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->canRequestMoney:Z

    return v0
.end method

.method public final p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->accountMessage:Ljava/lang/String;

    return-object v0
.end method

.method public final q()Z
    .locals 1

    .prologue
    .line 140
    iget-boolean v0, p0, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->canSendMoney:Z

    return v0
.end method

.method public final r()Z
    .locals 1

    .prologue
    .line 148
    iget-boolean v0, p0, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->canAcceptMoney:Z

    return v0
.end method

.method public final s()Ljava/lang/String;
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->missingPmtMessage:Ljava/lang/String;

    return-object v0
.end method

.method public final t()Lcom/chase/sig/android/domain/m;
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->frequencyList:Lcom/chase/sig/android/domain/m;

    return-object v0
.end method
