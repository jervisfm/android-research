.class public final Lcom/chase/sig/android/service/r;
.super Lcom/chase/sig/android/service/JPService;
.source "SourceFile"


# instance fields
.field private a:Lcom/chase/sig/android/service/w;


# direct methods
.method public constructor <init>(Lcom/chase/sig/android/service/w;)V
    .locals 0
    .parameter

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/chase/sig/android/service/JPService;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/chase/sig/android/service/r;->a:Lcom/chase/sig/android/service/w;

    .line 28
    return-void
.end method

.method private static a()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 210
    new-instance v0, Lcom/chase/sig/android/util/f;

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/chase/sig/android/util/f;-><init>(Lcom/chase/sig/android/ChaseApplication;)V

    .line 211
    invoke-static {}, Lcom/chase/sig/android/util/f;->b()Ljava/util/List;

    move-result-object v0

    .line 213
    if-nez v0, :cond_0

    move-object v0, v1

    .line 223
    :goto_0
    return-object v0

    .line 217
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/cookie/Cookie;

    .line 218
    invoke-interface {v0}, Lorg/apache/http/cookie/Cookie;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "adtoken"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 219
    invoke-interface {v0}, Lorg/apache/http/cookie/Cookie;->getValue()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 223
    goto :goto_0
.end method

.method private static b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/domain/a;
    .locals 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 128
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v1

    const-string v2, "auth"

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/ChaseApplication;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const v1, 0x7f070008

    invoke-static {v1}, Lcom/chase/sig/android/service/r;->b(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 132
    invoke-static {}, Lcom/chase/sig/android/service/r;->c()Ljava/util/Hashtable;

    move-result-object v2

    .line 134
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    .line 135
    const-string v3, "DEVMAKE=%s&DEVID=%s&DEVOS=%s&DEVMODELVER=%s&DEVOSVER=%s&DEVMODEL=%s&DEVAPPINSTALL=%s&DEVAPPVER=%s"

    const/16 v4, 0x8

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    sget-object v6, Landroid/os/Build;->BRAND:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->d()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "Android"

    aput-object v6, v4, v5

    const/4 v5, 0x3

    sget-object v6, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x4

    sget-object v6, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x5

    sget-object v6, Landroid/os/Build;->MODEL:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x6

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->l()Ljava/util/Date;

    move-result-object v6

    invoke-static {v6}, Lcom/chase/sig/android/util/s;->b(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x7

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->k()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 147
    const-string v3, "auth_mobile_mis"

    invoke-virtual {v2, v3, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 149
    const-string v0, "LOB"

    const-string v3, "RBGLogon"

    invoke-virtual {v2, v0, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 150
    const-string v0, "Referer"

    const-string v3, "https://www.chase.com"

    invoke-virtual {v2, v0, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 151
    const-string v0, "auth_contextId"

    const-string v3, "login"

    invoke-virtual {v2, v0, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 152
    const-string v0, "auth_deviceCookie"

    const-string v3, "adtoken"

    invoke-virtual {v2, v0, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 159
    invoke-static {}, Lcom/chase/sig/android/service/r;->a()Ljava/lang/String;

    move-result-object v0

    .line 160
    invoke-static {v0}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 161
    const-string v3, "auth_deviceId"

    invoke-virtual {v2, v3, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 164
    :cond_0
    invoke-static {p4}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 165
    const-string v0, "auth_tokencode"

    invoke-virtual {v2, v0, p4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 168
    :cond_1
    invoke-static {p5}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 169
    const-string v0, "auth_nexttokencode"

    invoke-virtual {v2, v0, p5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 172
    :cond_2
    const-string v0, "auth_deviceSignature"

    const-string v3, ""

    invoke-virtual {v2, v0, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 173
    const-string v0, "auth_externalData"

    const-string v3, "LOB=RBGLogon"

    invoke-virtual {v2, v0, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 175
    const-string v3, "auth_otp"

    if-nez p2, :cond_5

    const-string v0, ""

    :goto_0
    invoke-virtual {v2, v3, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 176
    const-string v0, "auth_otpprefix"

    if-nez p3, :cond_3

    const-string p3, ""

    :cond_3
    invoke-virtual {v2, v0, p3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 177
    const-string v3, "auth_otpreason"

    if-nez p2, :cond_6

    const-string v0, ""

    :goto_1
    invoke-virtual {v2, v3, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 179
    const-string v0, "auth_userId"

    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 180
    const-string v0, "auth_passwd"

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 181
    const-string v0, "auth_siteId"

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/chase/sig/android/ChaseApplication;->h()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 182
    const-string v0, "type"

    const-string v3, "json"

    invoke-virtual {v2, v0, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 184
    new-instance v0, Lcom/chase/sig/android/util/f;

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/chase/sig/android/util/f;-><init>(Lcom/chase/sig/android/ChaseApplication;)V

    .line 185
    invoke-virtual {v0}, Lcom/chase/sig/android/util/f;->a()V

    .line 187
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-static {v0, v1, v2}, Lcom/chase/sig/android/util/m;->a(Lcom/chase/sig/android/ChaseApplication;Ljava/lang/String;Ljava/util/Hashtable;)Lorg/json/JSONObject;

    move-result-object v0

    .line 189
    const-string v1, "response"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 190
    const-string v1, "smtoken"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 191
    new-instance v1, Ljava/lang/Boolean;

    const-string v4, "newstoken"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Ljava/lang/Boolean;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    .line 193
    const/4 v5, 0x0

    .line 194
    const-string v1, "spid"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 195
    const-string v1, "spid"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 198
    :cond_4
    const/4 v0, 0x2

    new-array v6, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p4, v6, v0

    const/4 v0, 0x1

    aput-object p5, v6, v0

    .line 202
    new-instance v0, Lcom/chase/sig/android/domain/a;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/chase/sig/android/domain/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;[Ljava/lang/String;)V

    return-object v0

    :cond_5
    move-object v0, p2

    .line 175
    goto/16 :goto_0

    .line 177
    :cond_6
    const-string v0, "2"
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 204
    :catch_0
    move-exception v0

    .line 205
    new-instance v1, Lcom/chase/sig/android/util/ChaseException;

    sget v2, Lcom/chase/sig/android/util/ChaseException;->b:I

    const-string v3, "Login service failure."

    invoke-direct {v1, v2, v0, v3}, Lcom/chase/sig/android/util/ChaseException;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/service/GenericResponse;
    .locals 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    const/4 v6, 0x1

    .line 38
    .line 40
    new-instance v1, Lcom/chase/sig/android/service/GenericResponse;

    invoke-direct {v1}, Lcom/chase/sig/android/service/GenericResponse;-><init>()V

    .line 43
    :try_start_0
    invoke-static/range {p1 .. p6}, Lcom/chase/sig/android/service/r;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/domain/a;
    :try_end_0
    .catch Lcom/chase/sig/android/util/ChaseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 53
    :try_start_1
    iget-object v2, v3, Lcom/chase/sig/android/domain/a;->a:Ljava/lang/String;

    const-string v4, "criticalerror"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 54
    new-instance v2, Lcom/chase/sig/android/service/ServiceError;

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v4

    invoke-virtual {v4}, Lcom/chase/sig/android/ChaseApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0700c6

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-direct {v2, v4, v5}, Lcom/chase/sig/android/service/ServiceError;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/service/GenericResponse;->a(Lcom/chase/sig/android/service/IServiceError;)V
    :try_end_1
    .catch Lcom/chase/sig/android/util/ChaseException; {:try_start_1 .. :try_end_1} :catch_1

    .line 65
    :cond_0
    :goto_0
    if-eqz v3, :cond_3

    invoke-virtual {v3}, Lcom/chase/sig/android/domain/a;->b()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 66
    iget-object v0, p0, Lcom/chase/sig/android/service/r;->a:Lcom/chase/sig/android/service/w;

    invoke-static {}, Lcom/chase/sig/android/service/w;->a()Lcom/chase/sig/android/service/ProfileResponse;

    move-result-object v0

    .line 68
    invoke-virtual {v0}, Lcom/chase/sig/android/service/ProfileResponse;->e()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 69
    invoke-virtual {v0}, Lcom/chase/sig/android/service/ProfileResponse;->g()Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/service/GenericResponse;->b(Ljava/util/List;)V

    move-object v0, v1

    .line 84
    :goto_1
    return-object v0

    .line 56
    :cond_1
    :try_start_2
    invoke-virtual {v1}, Lcom/chase/sig/android/service/GenericResponse;->e()Z
    :try_end_2
    .catch Lcom/chase/sig/android/util/ChaseException; {:try_start_2 .. :try_end_2} :catch_1

    move-result v2

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 57
    goto :goto_1

    .line 60
    :catch_0
    move-exception v2

    move-object v3, v0

    .line 61
    :goto_2
    invoke-virtual {v2}, Lcom/chase/sig/android/util/ChaseException;->a()Ljava/lang/String;

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/service/GenericResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    goto :goto_0

    .line 73
    :cond_2
    invoke-virtual {v0}, Lcom/chase/sig/android/service/ProfileResponse;->a()Lcom/chase/sig/android/domain/g;

    move-result-object v0

    .line 75
    invoke-interface {v0}, Lcom/chase/sig/android/domain/g;->i()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-gtz v2, :cond_3

    .line 76
    new-instance v2, Lcom/chase/sig/android/service/ServiceError;

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v4

    const v5, 0x7f0700b3

    invoke-virtual {v4, v5}, Lcom/chase/sig/android/ChaseApplication;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4, v6}, Lcom/chase/sig/android/service/ServiceError;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/service/GenericResponse;->a(Lcom/chase/sig/android/service/IServiceError;)V

    .line 82
    :cond_3
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v2

    new-instance v4, Lcom/chase/sig/android/domain/o;

    invoke-direct {v4, v3, v0}, Lcom/chase/sig/android/domain/o;-><init>(Lcom/chase/sig/android/domain/a;Lcom/chase/sig/android/domain/g;)V

    invoke-virtual {v2, v4}, Lcom/chase/sig/android/ChaseApplication;->a(Lcom/chase/sig/android/domain/o;)V

    move-object v0, v1

    .line 84
    goto :goto_1

    .line 60
    :catch_1
    move-exception v2

    goto :goto_2
.end method
