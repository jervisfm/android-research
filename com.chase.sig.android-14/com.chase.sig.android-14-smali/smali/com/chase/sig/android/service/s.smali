.class public final Lcom/chase/sig/android/service/s;
.super Lcom/chase/sig/android/service/JPService;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/chase/sig/android/service/JPService;-><init>()V

    return-void
.end method

.method public static a()Lcom/chase/sig/android/domain/MobileAdResponse;
    .locals 4

    .prologue
    .line 18
    new-instance v0, Lcom/chase/sig/android/domain/MobileAdResponse;

    invoke-direct {v0}, Lcom/chase/sig/android/domain/MobileAdResponse;-><init>()V

    .line 20
    const v1, 0x7f070062

    :try_start_0
    invoke-static {v1}, Lcom/chase/sig/android/service/s;->a(I)Ljava/lang/String;

    move-result-object v1

    .line 21
    invoke-static {}, Lcom/chase/sig/android/service/s;->c()Ljava/util/Hashtable;

    move-result-object v2

    .line 23
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v3

    invoke-static {v3, v1, v2}, Lcom/chase/sig/android/util/m;->a(Lcom/chase/sig/android/ChaseApplication;Ljava/lang/String;Ljava/util/Hashtable;)Lorg/json/JSONObject;

    move-result-object v1

    .line 24
    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/MobileAdResponse;->b(Lorg/json/JSONObject;)V

    .line 26
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/MobileAdResponse;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 37
    :goto_0
    return-object v0

    .line 30
    :cond_0
    new-instance v0, Lcom/google/gson/chase/Gson;

    invoke-direct {v0}, Lcom/google/gson/chase/Gson;-><init>()V

    .line 31
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/chase/sig/android/domain/MobileAdResponse;

    invoke-virtual {v0, v1, v2}, Lcom/google/gson/chase/Gson;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/MobileAdResponse;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 33
    :catch_0
    move-exception v0

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    aput-object v2, v0, v1

    .line 34
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Lcom/chase/sig/android/domain/MobileAdResponse;
    .locals 4
    .parameter

    .prologue
    .line 41
    new-instance v1, Lcom/chase/sig/android/domain/MobileAdResponse;

    invoke-direct {v1}, Lcom/chase/sig/android/domain/MobileAdResponse;-><init>()V

    .line 44
    :try_start_0
    invoke-static {p0}, Lcom/chase/sig/android/service/s;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 45
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v2

    const-string v3, ""

    invoke-static {v2, v0, v3}, Lcom/chase/sig/android/util/m;->a(Lcom/chase/sig/android/ChaseApplication;Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 46
    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/MobileAdResponse;->b(Lorg/json/JSONObject;)V

    .line 48
    invoke-virtual {v1}, Lcom/chase/sig/android/domain/MobileAdResponse;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 59
    :goto_0
    return-object v1

    .line 52
    :cond_0
    new-instance v2, Lcom/google/gson/chase/Gson;

    invoke-direct {v2}, Lcom/google/gson/chase/Gson;-><init>()V

    .line 53
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    const-class v3, Lcom/chase/sig/android/domain/MobileAdResponse;

    invoke-virtual {v2, v0, v3}, Lcom/google/gson/chase/Gson;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/MobileAdResponse;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    move-object v1, v0

    .line 59
    goto :goto_0

    .line 55
    :catch_0
    move-exception v0

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/MobileAdResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    move-object v0, v1

    goto :goto_1
.end method

.method public static c(Ljava/lang/String;)Lcom/chase/sig/android/domain/MobileAdResponse;
    .locals 4
    .parameter

    .prologue
    .line 64
    new-instance v1, Lcom/chase/sig/android/domain/MobileAdResponse;

    invoke-direct {v1}, Lcom/chase/sig/android/domain/MobileAdResponse;-><init>()V

    .line 65
    invoke-static {p0}, Lcom/chase/sig/android/service/s;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 68
    :try_start_0
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v2

    const-string v3, ""

    invoke-static {v2, v0, v3}, Lcom/chase/sig/android/util/m;->a(Lcom/chase/sig/android/ChaseApplication;Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 69
    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/MobileAdResponse;->b(Lorg/json/JSONObject;)V

    .line 70
    invoke-virtual {v1}, Lcom/chase/sig/android/domain/MobileAdResponse;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 80
    :goto_0
    return-object v1

    .line 73
    :cond_0
    new-instance v2, Lcom/google/gson/chase/Gson;

    invoke-direct {v2}, Lcom/google/gson/chase/Gson;-><init>()V

    .line 74
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    const-class v3, Lcom/chase/sig/android/domain/MobileAdResponse;

    invoke-virtual {v2, v0, v3}, Lcom/google/gson/chase/Gson;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/MobileAdResponse;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    move-object v1, v0

    .line 80
    goto :goto_0

    .line 76
    :catch_0
    move-exception v0

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/domain/MobileAdResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    move-object v0, v1

    goto :goto_1
.end method
