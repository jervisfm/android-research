.class public final Lcom/chase/sig/android/service/t;
.super Lcom/chase/sig/android/service/JPService;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/chase/sig/android/service/JPService;-><init>()V

    return-void
.end method

.method public static a(Lcom/chase/sig/android/domain/o;Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/service/GenericResponse;
    .locals 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 67
    const v0, 0x7f07000b

    invoke-static {v0}, Lcom/chase/sig/android/service/t;->a(I)Ljava/lang/String;

    move-result-object v1

    .line 69
    new-instance v2, Ljava/util/Hashtable;

    invoke-direct {v2}, Ljava/util/Hashtable;-><init>()V

    .line 71
    const-string v0, "applId"

    const-string v3, "GATEWAY"

    invoke-virtual {v2, v0, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    const-string v0, "reason"

    const-string v3, "2"

    invoke-virtual {v2, v0, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    const-string v3, "method"

    const-string v0, "EMAIL"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "T"

    :goto_0
    invoke-virtual {v2, v3, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    const-string v0, "userId"

    iget-object v3, p0, Lcom/chase/sig/android/domain/o;->a:Lcom/chase/sig/android/domain/a;

    iget-object v3, v3, Lcom/chase/sig/android/domain/a;->c:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    const-string v0, "spid"

    iget-object v3, p0, Lcom/chase/sig/android/domain/o;->a:Lcom/chase/sig/android/domain/a;

    iget-object v3, v3, Lcom/chase/sig/android/domain/a;->b:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    const-string v0, "type"

    const-string v3, "json"

    invoke-virtual {v2, v0, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    const-string v0, "version"

    const-string v3, "1"

    invoke-virtual {v2, v0, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    const-string v0, "contactId"

    invoke-virtual {v2, v0, p2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    new-instance v0, Lcom/chase/sig/android/service/GenericResponse;

    invoke-direct {v0}, Lcom/chase/sig/android/service/GenericResponse;-><init>()V

    .line 83
    :try_start_0
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    .line 84
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v4

    invoke-static {v4, v1, v2}, Lcom/chase/sig/android/util/m;->a(Lcom/chase/sig/android/ChaseApplication;Ljava/lang/String;Ljava/util/Hashtable;)Lorg/json/JSONObject;

    move-result-object v1

    .line 86
    const-string v2, "prefix"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 87
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    .line 88
    iget-object v4, p0, Lcom/chase/sig/android/domain/o;->a:Lcom/chase/sig/android/domain/a;

    iput-object v2, v4, Lcom/chase/sig/android/domain/a;->d:Ljava/lang/String;

    .line 91
    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/GenericResponse;->b(Lorg/json/JSONObject;)V

    .line 93
    invoke-static {v2}, Lcom/chase/sig/android/util/s;->l(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 94
    invoke-virtual {v0, v3}, Lcom/chase/sig/android/service/GenericResponse;->b(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 102
    :cond_0
    :goto_1
    return-object v0

    .line 73
    :cond_1
    const-string v0, "PHONE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "V"

    goto :goto_0

    :cond_2
    const-string v0, "TEXT"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "S"

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 98
    :catch_0
    move-exception v1

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/GenericResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    goto :goto_1
.end method

.method public static a(Lcom/chase/sig/android/domain/o;)Lcom/chase/sig/android/service/OneTimePasswordContactsResponse;
    .locals 9
    .parameter

    .prologue
    .line 22
    const v0, 0x7f07000a

    invoke-static {v0}, Lcom/chase/sig/android/service/t;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 24
    invoke-static {}, Lcom/chase/sig/android/service/t;->c()Ljava/util/Hashtable;

    move-result-object v1

    .line 26
    const-string v2, "spid"

    iget-object v3, p0, Lcom/chase/sig/android/domain/o;->a:Lcom/chase/sig/android/domain/a;

    iget-object v3, v3, Lcom/chase/sig/android/domain/a;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27
    const-string v2, "reason"

    const-string v3, "2"

    invoke-virtual {v1, v2, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 30
    :try_start_0
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v2

    invoke-static {v2, v0, v1}, Lcom/chase/sig/android/util/m;->a(Lcom/chase/sig/android/ChaseApplication;Ljava/lang/String;Ljava/util/Hashtable;)Lorg/json/JSONObject;

    move-result-object v0

    .line 31
    new-instance v1, Lcom/chase/sig/android/service/OneTimePasswordContactsResponse;

    invoke-direct {v1}, Lcom/chase/sig/android/service/OneTimePasswordContactsResponse;-><init>()V

    .line 35
    const-string v2, "prefix"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 37
    invoke-virtual {v1, v0}, Lcom/chase/sig/android/service/OneTimePasswordContactsResponse;->b(Lorg/json/JSONObject;)V

    .line 39
    iget-object v3, p0, Lcom/chase/sig/android/domain/o;->a:Lcom/chase/sig/android/domain/a;

    iput-object v2, v3, Lcom/chase/sig/android/domain/a;->d:Ljava/lang/String;

    .line 41
    const-string v2, "contacts"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 42
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 44
    if-eqz v2, :cond_1

    .line 45
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v0, v4, :cond_0

    .line 46
    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 48
    new-instance v5, Lcom/chase/sig/android/domain/OneTimePasswordContact;

    const-string v6, "id"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "mask"

    invoke-virtual {v4, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "type"

    invoke-virtual {v4, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v6, v7, v4}, Lcom/chase/sig/android/domain/OneTimePasswordContact;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 45
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 54
    :cond_0
    invoke-virtual {v1, v3}, Lcom/chase/sig/android/service/OneTimePasswordContactsResponse;->a(Ljava/util/ArrayList;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 57
    :cond_1
    return-object v1

    .line 59
    :catch_0
    move-exception v0

    .line 60
    new-instance v1, Lcom/chase/sig/android/util/ChaseException;

    sget v2, Lcom/chase/sig/android/util/ChaseException;->b:I

    const-string v3, "Failed to communicate with auth GWS."

    invoke-direct {v1, v2, v0, v3}, Lcom/chase/sig/android/util/ChaseException;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    throw v1
.end method
