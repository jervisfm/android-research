.class public final Lcom/chase/sig/android/service/v;
.super Lcom/chase/sig/android/service/JPService;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/chase/sig/android/service/JPService;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/chase/sig/android/service/PositionResponse;
    .locals 9
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 21
    const v1, 0x7f070027

    invoke-static {v1}, Lcom/chase/sig/android/service/v;->a(I)Ljava/lang/String;

    move-result-object v1

    .line 23
    new-instance v2, Lcom/chase/sig/android/service/PositionResponse;

    invoke-direct {v2}, Lcom/chase/sig/android/service/PositionResponse;-><init>()V

    .line 26
    :try_start_0
    invoke-static {}, Lcom/chase/sig/android/service/v;->c()Ljava/util/Hashtable;

    move-result-object v3

    .line 27
    const-string v4, "accountId"

    invoke-virtual {v3, v4, p0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v4

    invoke-static {v4, v1, v3}, Lcom/chase/sig/android/util/m;->a(Lcom/chase/sig/android/ChaseApplication;Ljava/lang/String;Ljava/util/Hashtable;)Lorg/json/JSONObject;

    move-result-object v1

    .line 30
    const-string v3, "accounts"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 31
    invoke-virtual {v2, v1}, Lcom/chase/sig/android/service/PositionResponse;->b(Lorg/json/JSONObject;)V

    .line 33
    if-eqz v3, :cond_4

    const/4 v1, 0x0

    invoke-virtual {v3, v1}, Lorg/json/JSONArray;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_4

    .line 35
    const/4 v1, 0x0

    invoke-virtual {v3, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v1

    .line 36
    const-string v3, "positions"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 38
    new-instance v4, Lcom/chase/sig/android/service/PositionsAccount;

    invoke-direct {v4}, Lcom/chase/sig/android/service/PositionsAccount;-><init>()V

    .line 39
    const-string v5, "accountUnrealizedChange"

    invoke-virtual {v1, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/chase/sig/android/service/PositionsAccount;->c(Ljava/lang/String;)V

    .line 41
    const-string v5, "accountValue"

    invoke-virtual {v1, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/chase/sig/android/service/PositionsAccount;->d(Ljava/lang/String;)V

    .line 42
    const-string v5, "asOfDate"

    invoke-virtual {v1, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/chase/sig/android/service/PositionsAccount;->b(Ljava/lang/String;)V

    .line 43
    const-string v5, "displayName"

    invoke-virtual {v1, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/chase/sig/android/service/PositionsAccount;->a(Ljava/lang/String;)V

    .line 45
    const-string v5, "marketValue"

    invoke-virtual {v1, v5}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 46
    const-string v5, "marketValue"

    invoke-static {v1, v5}, Lcom/chase/sig/android/service/v;->a(Lorg/json/JSONObject;Ljava/lang/String;)Lcom/chase/sig/android/util/Dollar;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/chase/sig/android/service/PositionsAccount;->a(Lcom/chase/sig/android/util/Dollar;)V

    .line 49
    :cond_0
    const-string v5, "marketValueChange"

    invoke-virtual {v1, v5}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 50
    const-string v5, "marketValueChange"

    invoke-static {v1, v5}, Lcom/chase/sig/android/service/v;->a(Lorg/json/JSONObject;Ljava/lang/String;)Lcom/chase/sig/android/util/Dollar;

    move-result-object v1

    invoke-virtual {v4, v1}, Lcom/chase/sig/android/service/PositionsAccount;->b(Lcom/chase/sig/android/util/Dollar;)V

    .line 53
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 55
    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v0, v5, :cond_3

    .line 56
    invoke-virtual {v3, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    new-instance v6, Lcom/chase/sig/android/domain/Position;

    invoke-direct {v6}, Lcom/chase/sig/android/domain/Position;-><init>()V

    const-string v7, "assetClass1"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/chase/sig/android/domain/Position;->f(Ljava/lang/String;)V

    const-string v7, "assetClass2"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/chase/sig/android/domain/Position;->g(Ljava/lang/String;)V

    const-string v7, "priceAsOfDate"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/chase/sig/android/domain/Position;->h(Ljava/lang/String;)V

    const-string v7, "price"

    const-string v8, "value"

    invoke-static {v5, v7, v8}, Lcom/chase/sig/android/service/v;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/util/Dollar;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/chase/sig/android/domain/Position;->b(Lcom/chase/sig/android/util/Dollar;)V

    const-string v7, "shortName"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/chase/sig/android/domain/Position;->a(Ljava/lang/String;)V

    const-string v7, "unrealizedChange"

    const-string v8, "value"

    invoke-static {v5, v7, v8}, Lcom/chase/sig/android/service/v;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/util/Dollar;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/chase/sig/android/domain/Position;->c(Lcom/chase/sig/android/util/Dollar;)V

    const-string v7, "value"

    const-string v8, "value"

    invoke-static {v5, v7, v8}, Lcom/chase/sig/android/service/v;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/util/Dollar;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/chase/sig/android/domain/Position;->a(Lcom/chase/sig/android/util/Dollar;)V

    const-string v7, "value"

    const-string v8, "change"

    invoke-static {v5, v7, v8}, Lcom/chase/sig/android/service/v;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/util/Dollar;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/chase/sig/android/domain/Position;->e(Lcom/chase/sig/android/util/Dollar;)V

    const-string v7, "cost"

    const-string v8, "value"

    invoke-static {v5, v7, v8}, Lcom/chase/sig/android/service/v;->a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/util/Dollar;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/chase/sig/android/domain/Position;->d(Lcom/chase/sig/android/util/Dollar;)V

    const-string v7, "quantity"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/chase/sig/android/domain/Position;->b(Ljava/lang/String;)V

    const-string v7, "tickerSymbol"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/chase/sig/android/domain/Position;->d(Ljava/lang/String;)V

    const-string v7, "cusip"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/chase/sig/android/domain/Position;->e(Ljava/lang/String;)V

    const-string v7, "description"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/chase/sig/android/domain/Position;->c(Ljava/lang/String;)V

    const-string v7, "quoteCode"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    const-string v7, "quoteCode"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/chase/sig/android/domain/Position;->i(Ljava/lang/String;)V

    :cond_2
    const-string v7, "isShowShortName"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v7

    invoke-virtual {v6, v7}, Lcom/chase/sig/android/domain/Position;->g(Z)V

    const-string v7, "isShowPriceDate"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v7

    invoke-virtual {v6, v7}, Lcom/chase/sig/android/domain/Position;->e(Z)V

    const-string v7, "isShowPrice"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v7

    invoke-virtual {v6, v7}, Lcom/chase/sig/android/domain/Position;->d(Z)V

    const-string v7, "isShowCost"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v7

    invoke-virtual {v6, v7}, Lcom/chase/sig/android/domain/Position;->b(Z)V

    const-string v7, "isShowQuantity"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v7

    invoke-virtual {v6, v7}, Lcom/chase/sig/android/domain/Position;->f(Z)V

    const-string v7, "isShowValue"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v7

    invoke-virtual {v6, v7}, Lcom/chase/sig/android/domain/Position;->j(Z)V

    const-string v7, "isShowSymbol"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v7

    invoke-virtual {v6, v7}, Lcom/chase/sig/android/domain/Position;->h(Z)V

    const-string v7, "isShowDescription"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v7

    invoke-virtual {v6, v7}, Lcom/chase/sig/android/domain/Position;->c(Z)V

    const-string v7, "isShowUnrealizedChange"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v7

    invoke-virtual {v6, v7}, Lcom/chase/sig/android/domain/Position;->i(Z)V

    const-string v7, "isShowAssetClass1"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    invoke-virtual {v6, v5}, Lcom/chase/sig/android/domain/Position;->a(Z)V

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 55
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .line 59
    :cond_3
    invoke-virtual {v4, v1}, Lcom/chase/sig/android/service/PositionsAccount;->a(Ljava/util/List;)V

    .line 61
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 62
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 63
    invoke-virtual {v2, v0}, Lcom/chase/sig/android/service/PositionResponse;->a(Ljava/util/List;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 72
    :cond_4
    :goto_1
    return-object v2

    .line 65
    :catch_0
    move-exception v0

    .line 66
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/chase/sig/android/service/PositionResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    goto :goto_1

    .line 68
    :catch_1
    move-exception v0

    .line 69
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/chase/sig/android/service/PositionResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    goto :goto_1
.end method

.method private static a(Lorg/json/JSONObject;Ljava/lang/String;)Lcom/chase/sig/android/util/Dollar;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 112
    invoke-virtual {p0, p1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 113
    new-instance v1, Lcom/chase/sig/android/util/Dollar;

    if-nez v0, :cond_0

    const-string v0, ""

    :cond_0
    invoke-direct {v1, v0}, Lcom/chase/sig/android/util/Dollar;-><init>(Ljava/lang/String;)V

    return-object v1
.end method

.method private static a(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/util/Dollar;
    .locals 2
    .parameter
    .parameter
    .parameter

    .prologue
    .line 118
    invoke-virtual {p0, p1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 119
    new-instance v1, Lcom/chase/sig/android/util/Dollar;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    invoke-direct {v1, v0}, Lcom/chase/sig/android/util/Dollar;-><init>(Ljava/lang/String;)V

    return-object v1

    :cond_0
    invoke-virtual {v0, p2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
