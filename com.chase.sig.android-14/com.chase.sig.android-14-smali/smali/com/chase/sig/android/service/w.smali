.class public final Lcom/chase/sig/android/service/w;
.super Lcom/chase/sig/android/service/JPService;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/chase/sig/android/service/JPService;-><init>()V

    return-void
.end method

.method public static a()Lcom/chase/sig/android/service/ProfileResponse;
    .locals 4

    .prologue
    .line 16
    new-instance v0, Lcom/chase/sig/android/service/ProfileResponse;

    invoke-direct {v0}, Lcom/chase/sig/android/service/ProfileResponse;-><init>()V

    .line 20
    :try_start_0
    invoke-static {}, Lcom/chase/sig/android/service/w;->c()Ljava/util/Hashtable;

    move-result-object v1

    .line 22
    const v2, 0x7f070051

    invoke-static {v2}, Lcom/chase/sig/android/service/w;->a(I)Ljava/lang/String;

    move-result-object v2

    .line 24
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v3

    invoke-static {v3, v2, v1}, Lcom/chase/sig/android/util/m;->a(Lcom/chase/sig/android/ChaseApplication;Ljava/lang/String;Ljava/util/Hashtable;)Lorg/json/JSONObject;

    move-result-object v1

    .line 26
    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/ProfileResponse;->b(Lorg/json/JSONObject;)V

    .line 28
    invoke-virtual {v0}, Lcom/chase/sig/android/service/ProfileResponse;->e()Z

    move-result v2

    if-nez v2, :cond_0

    .line 29
    new-instance v2, Lcom/chase/sig/android/service/a/a;

    invoke-direct {v2, v1}, Lcom/chase/sig/android/service/a/a;-><init>(Lorg/json/JSONObject;)V

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/service/ProfileResponse;->a(Lcom/chase/sig/android/domain/g;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 38
    :cond_0
    :goto_0
    return-object v0

    .line 32
    :catch_0
    move-exception v1

    .line 34
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/ProfileResponse;->b(Landroid/content/Context;)V

    goto :goto_0
.end method
