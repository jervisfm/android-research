.class public final Lcom/chase/sig/android/service/wire/a;
.super Lcom/chase/sig/android/service/movemoney/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/service/movemoney/d",
        "<",
        "Lcom/chase/sig/android/domain/WireTransaction;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/chase/sig/android/service/movemoney/g;Lcom/chase/sig/android/service/movemoney/e;Lcom/chase/sig/android/service/wire/b;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 24
    invoke-direct {p0, p1, p2, p3}, Lcom/chase/sig/android/service/movemoney/d;-><init>(Lcom/chase/sig/android/service/movemoney/g;Lcom/chase/sig/android/service/movemoney/e;Lcom/chase/sig/android/service/movemoney/f;)V

    .line 25
    return-void
.end method

.method public static a()Lcom/chase/sig/android/service/wire/WireGetPayeesResponse;
    .locals 7

    .prologue
    .line 38
    new-instance v1, Lcom/chase/sig/android/service/wire/WireGetPayeesResponse;

    invoke-direct {v1}, Lcom/chase/sig/android/service/wire/WireGetPayeesResponse;-><init>()V

    .line 41
    const v0, 0x7f070052

    :try_start_0
    invoke-static {v0}, Lcom/chase/sig/android/service/wire/a;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 43
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v2

    invoke-static {}, Lcom/chase/sig/android/service/wire/a;->c()Ljava/util/Hashtable;

    move-result-object v3

    invoke-static {v2, v0, v3}, Lcom/chase/sig/android/util/m;->a(Lcom/chase/sig/android/ChaseApplication;Ljava/lang/String;Ljava/util/Hashtable;)Lorg/json/JSONObject;

    move-result-object v0

    .line 46
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 48
    invoke-virtual {v1, v0}, Lcom/chase/sig/android/service/wire/WireGetPayeesResponse;->b(Lorg/json/JSONObject;)V

    .line 50
    const-string v3, "payees"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 51
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 52
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v0, v4, :cond_0

    .line 53
    invoke-virtual {v3, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 54
    new-instance v5, Lcom/chase/sig/android/domain/Payee;

    invoke-direct {v5}, Lcom/chase/sig/android/domain/Payee;-><init>()V

    const-string v6, "accountNumber"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/chase/sig/android/domain/Payee;->a(Ljava/lang/String;)V

    const-string v6, "bankInstructions"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/chase/sig/android/domain/Payee;->b(Ljava/lang/String;)V

    const-string v6, "earliestPaymentDeliveryDate"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/chase/sig/android/domain/Payee;->c(Ljava/lang/String;)V

    const-string v6, "earliestPaymentSendDate"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/chase/sig/android/domain/Payee;->d(Ljava/lang/String;)V

    const-string v6, "memo"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/chase/sig/android/domain/Payee;->e(Ljava/lang/String;)V

    const-string v6, "nickName"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/chase/sig/android/domain/Payee;->f(Ljava/lang/String;)V

    const-string v6, "payeeId"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/chase/sig/android/domain/Payee;->h(Ljava/lang/String;)V

    const-string v6, "payeeInstructions"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Lcom/chase/sig/android/domain/Payee;->i(Ljava/lang/String;)V

    .line 55
    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 52
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 59
    :cond_0
    invoke-virtual {v1, v2}, Lcom/chase/sig/android/service/wire/WireGetPayeesResponse;->a(Ljava/util/List;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 65
    :goto_1
    return-object v1

    .line 61
    :catch_0
    move-exception v0

    .line 62
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/service/wire/WireGetPayeesResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    goto :goto_1
.end method
