.class public final Lcom/chase/sig/android/service/wire/b;
.super Lcom/chase/sig/android/service/movemoney/f;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/service/movemoney/f",
        "<",
        "Lcom/chase/sig/android/domain/WireTransaction;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/chase/sig/android/service/movemoney/f;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic a(Lorg/json/JSONObject;)Lcom/chase/sig/android/domain/Transaction;
    .locals 3
    .parameter

    .prologue
    .line 10
    new-instance v0, Lcom/chase/sig/android/domain/WireTransaction;

    invoke-direct {v0}, Lcom/chase/sig/android/domain/WireTransaction;-><init>()V

    const-string v1, "paymentId"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/WireTransaction;->k(Ljava/lang/String;)V

    const-string v1, "payeeNickName"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/WireTransaction;->a(Ljava/lang/String;)V

    new-instance v1, Lcom/chase/sig/android/util/Dollar;

    const-string v2, "amount"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/chase/sig/android/util/Dollar;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/WireTransaction;->a(Lcom/chase/sig/android/util/Dollar;)V

    const-string v1, "status"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/WireTransaction;->q(Ljava/lang/String;)V

    const-string v1, "dueDate"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/WireTransaction;->n(Ljava/lang/String;)V

    const-string v1, "fundingAccountNickName"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/WireTransaction;->o(Ljava/lang/String;)V

    const-string v1, "fundingAccountNumber"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/WireTransaction;->p(Ljava/lang/String;)V

    const-string v1, "accountId"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/WireTransaction;->b(Ljava/lang/String;)V

    new-instance v1, Lcom/chase/sig/android/util/Dollar;

    const-string v2, "amount"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/chase/sig/android/util/Dollar;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/WireTransaction;->a(Lcom/chase/sig/android/util/Dollar;)V

    const-string v1, "dueDate"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/WireTransaction;->n(Ljava/lang/String;)V

    const-string v1, "status"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/WireTransaction;->q(Ljava/lang/String;)V

    const-string v1, "fedReference"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/WireTransaction;->c(Ljava/lang/String;)V

    const-string v1, "frequency"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/WireTransaction;->g(Ljava/lang/String;)V

    const-string v1, "remainingInstances"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/WireTransaction;->r(Ljava/lang/String;)V

    const-string v1, "recipientMessage"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/WireTransaction;->e(Ljava/lang/String;)V

    const-string v1, "bankMessage"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/WireTransaction;->f(Ljava/lang/String;)V

    const-string v1, "memo"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/WireTransaction;->l(Ljava/lang/String;)V

    const-string v1, "cancellable"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/WireTransaction;->b(Z)V

    const-string v1, "paymentToken"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/WireTransaction;->d(Ljava/lang/String;)V

    const-string v1, "paymentModelId"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/WireTransaction;->h(Ljava/lang/String;)V

    const-string v1, "paymentModelToken"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/WireTransaction;->i(Ljava/lang/String;)V

    const-string v1, "updatable"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/WireTransaction;->a(Z)V

    const-string v1, "payeeId"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/WireTransaction;->v(Ljava/lang/String;)V

    const-string v1, "openEnded"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/WireTransaction;->s(Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(Lcom/chase/sig/android/service/movemoney/ServiceResponse;Lorg/json/JSONObject;)Lcom/chase/sig/android/service/movemoney/ServiceResponse;
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 46
    const-string v0, "payment"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 47
    const-string v0, "payment"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->opt(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/json/JSONObject;

    .line 48
    const-string v1, "processDate"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/chase/sig/android/service/movemoney/ServiceResponse;->a(Ljava/lang/String;)V

    .line 49
    const-string v1, "dueDate"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/chase/sig/android/service/movemoney/ServiceResponse;->b(Ljava/lang/String;)V

    .line 50
    const-string v1, "paymentId"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/chase/sig/android/service/movemoney/ServiceResponse;->e(Ljava/lang/String;)V

    .line 51
    const-string v1, "status"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/chase/sig/android/service/movemoney/ServiceResponse;->f(Ljava/lang/String;)V

    .line 54
    :cond_0
    const-string v0, "formId"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 55
    const-string v0, "formId"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/chase/sig/android/service/movemoney/ServiceResponse;->g(Ljava/lang/String;)V

    .line 57
    :cond_1
    return-object p1
.end method
