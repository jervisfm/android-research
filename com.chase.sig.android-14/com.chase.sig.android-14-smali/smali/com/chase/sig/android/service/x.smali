.class public final Lcom/chase/sig/android/service/x;
.super Lcom/chase/sig/android/service/JPService;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/chase/sig/android/service/JPService;-><init>()V

    return-void
.end method

.method public static a(Lcom/chase/sig/android/domain/QuickPayRecipient;)Lcom/chase/sig/android/service/quickpay/QuickPayAddRecipientResponse;
    .locals 11
    .parameter

    .prologue
    .line 23
    new-instance v2, Lcom/chase/sig/android/service/quickpay/QuickPayAddRecipientResponse;

    invoke-direct {v2}, Lcom/chase/sig/android/service/quickpay/QuickPayAddRecipientResponse;-><init>()V

    .line 24
    const v0, 0x7f070015

    invoke-static {v0}, Lcom/chase/sig/android/service/x;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 29
    const-string v1, "0"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayRecipient;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 30
    const v0, 0x7f070014

    invoke-static {v0}, Lcom/chase/sig/android/service/x;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 34
    :goto_0
    :try_start_0
    invoke-static {}, Lcom/chase/sig/android/service/x;->c()Ljava/util/Hashtable;

    move-result-object v3

    .line 35
    const-string v4, "payload"

    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V

    new-instance v6, Lorg/json/JSONArray;

    invoke-direct {v6}, Lorg/json/JSONArray;-><init>()V

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayRecipient;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->o(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v7, "contact"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayRecipient;->g()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayRecipient;->i()Lcom/chase/sig/android/domain/MobilePhoneNumber;

    move-result-object v7

    invoke-virtual {v7}, Lcom/chase/sig/android/domain/MobilePhoneNumber;->c()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/chase/sig/android/util/s;->o(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    const-string v7, "payeeContactId"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayRecipient;->i()Lcom/chase/sig/android/domain/MobilePhoneNumber;

    move-result-object v8

    invoke-virtual {v8}, Lcom/chase/sig/android/domain/MobilePhoneNumber;->c()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_0
    const-string v7, "type"

    const-string v8, "PHONE"

    invoke-virtual {v0, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v7, "label"

    const-string v8, "SMS"

    invoke-virtual {v0, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    invoke-virtual {v6, v0}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    :cond_1
    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayRecipient;->e()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/Email;

    new-instance v8, Lorg/json/JSONObject;

    invoke-direct {v8}, Lorg/json/JSONObject;-><init>()V

    const-string v9, "contact"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Email;->a()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v9, "payeeContactId"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Email;->c()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v9, "type"

    const-string v10, "EMAIL"

    invoke-virtual {v8, v9, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Email;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "label"

    const-string v9, "PRIMARY"

    invoke-virtual {v8, v0, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :goto_2
    invoke-virtual {v6, v8}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 42
    :catch_0
    move-exception v0

    .line 43
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/chase/sig/android/service/quickpay/QuickPayAddRecipientResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    .line 45
    :cond_2
    :goto_3
    return-object v2

    .line 35
    :cond_3
    :try_start_1
    const-string v0, "label"

    const-string v9, "ADDITIONAL"

    invoke-virtual {v8, v0, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_2

    :cond_4
    const-string v0, "id"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayRecipient;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v0, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "name"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayRecipient;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v0, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "token"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayRecipient;->c()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v0, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "contacts"

    invoke-virtual {v5, v0, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    invoke-virtual {v5}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-static {v0, v1, v3}, Lcom/chase/sig/android/util/m;->a(Lcom/chase/sig/android/ChaseApplication;Ljava/lang/String;Ljava/util/Hashtable;)Lorg/json/JSONObject;

    move-result-object v0

    .line 38
    invoke-virtual {v2, v0}, Lcom/chase/sig/android/service/quickpay/QuickPayAddRecipientResponse;->b(Lorg/json/JSONObject;)V

    .line 40
    invoke-virtual {v2}, Lcom/chase/sig/android/service/quickpay/QuickPayAddRecipientResponse;->e()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "recipient"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    new-instance v1, Lcom/chase/sig/android/service/y;

    invoke-direct {v1}, Lcom/chase/sig/android/service/y;-><init>()V

    invoke-static {v0}, Lcom/chase/sig/android/service/y;->a(Lorg/json/JSONObject;)Lcom/chase/sig/android/domain/QuickPayRecipient;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/chase/sig/android/service/quickpay/QuickPayAddRecipientResponse;->a(Lcom/chase/sig/android/domain/QuickPayRecipient;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3

    :cond_5
    move-object v1, v0

    goto/16 :goto_0
.end method

.method public static b(Lcom/chase/sig/android/domain/QuickPayRecipient;)Lcom/chase/sig/android/service/quickpay/QuickPayDeleteRecipientResponse;
    .locals 4
    .parameter

    .prologue
    .line 99
    new-instance v0, Lcom/chase/sig/android/service/quickpay/QuickPayDeleteRecipientResponse;

    invoke-direct {v0}, Lcom/chase/sig/android/service/quickpay/QuickPayDeleteRecipientResponse;-><init>()V

    .line 102
    :try_start_0
    invoke-static {}, Lcom/chase/sig/android/service/x;->c()Ljava/util/Hashtable;

    move-result-object v1

    .line 103
    const-string v2, "recipientId"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayRecipient;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    const-string v2, "token"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayRecipient;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    const v2, 0x7f070016

    invoke-static {v2}, Lcom/chase/sig/android/service/x;->a(I)Ljava/lang/String;

    move-result-object v2

    .line 107
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v3

    invoke-static {v3, v2, v1}, Lcom/chase/sig/android/util/m;->a(Lcom/chase/sig/android/ChaseApplication;Ljava/lang/String;Ljava/util/Hashtable;)Lorg/json/JSONObject;

    move-result-object v1

    .line 109
    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/quickpay/QuickPayDeleteRecipientResponse;->b(Lorg/json/JSONObject;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 115
    :goto_0
    return-object v0

    .line 111
    :catch_0
    move-exception v1

    .line 112
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/quickpay/QuickPayDeleteRecipientResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    goto :goto_0
.end method
