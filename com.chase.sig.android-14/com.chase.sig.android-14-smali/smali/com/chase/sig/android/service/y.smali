.class public final Lcom/chase/sig/android/service/y;
.super Lcom/chase/sig/android/service/JPService;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/chase/sig/android/service/JPService;-><init>()V

    return-void
.end method

.method public static a(Lcom/chase/sig/android/domain/QuickPayActivityItem;)Lcom/chase/sig/android/domain/QuickPayActivityItem;
    .locals 4
    .parameter

    .prologue
    .line 1047
    const/4 v0, 0x0

    .line 1049
    :try_start_0
    invoke-static {}, Lcom/chase/sig/android/service/y;->c()Ljava/util/Hashtable;

    move-result-object v1

    .line 1050
    const-string v2, "TransactionId"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1051
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v2

    const-string v3, "mbb"

    invoke-virtual {v2, v3}, Lcom/chase/sig/android/ChaseApplication;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f070021

    invoke-static {v3}, Lcom/chase/sig/android/service/JPService;->b(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1052
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v3

    invoke-static {v3, v2, v1}, Lcom/chase/sig/android/util/m;->a(Lcom/chase/sig/android/ChaseApplication;Ljava/lang/String;Ljava/util/Hashtable;)Lorg/json/JSONObject;

    move-result-object v1

    .line 1053
    invoke-static {v1}, Lcom/chase/sig/android/service/y;->g(Lorg/json/JSONObject;)Lcom/chase/sig/android/domain/QuickPayActivityItem;
    :try_end_0
    .catch Lcom/chase/sig/android/util/ChaseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1055
    :goto_0
    return-object v0

    .line 1054
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static a(Lorg/json/JSONObject;)Lcom/chase/sig/android/domain/QuickPayRecipient;
    .locals 8
    .parameter

    .prologue
    .line 784
    new-instance v3, Lcom/chase/sig/android/domain/QuickPayRecipient;

    invoke-direct {v3}, Lcom/chase/sig/android/domain/QuickPayRecipient;-><init>()V

    .line 786
    const-string v0, "name"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/chase/sig/android/domain/QuickPayRecipient;->a(Ljava/lang/String;)V

    .line 787
    const-string v0, "id"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/chase/sig/android/domain/QuickPayRecipient;->b(Ljava/lang/String;)V

    .line 788
    const-string v0, "token"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/chase/sig/android/domain/QuickPayRecipient;->c(Ljava/lang/String;)V

    .line 789
    const-string v0, "status"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/chase/sig/android/domain/QuickPayRecipient;->f(Ljava/lang/String;)V

    .line 790
    const-string v0, "recipientId"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/chase/sig/android/domain/QuickPayRecipient;->g(Ljava/lang/String;)V

    .line 795
    :try_start_0
    const-string v0, "contacts"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 798
    :goto_0
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 806
    :try_start_1
    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->isNull(I)Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v2

    if-eqz v2, :cond_1

    .line 827
    :cond_0
    return-object v3

    .line 797
    :catch_0
    move-exception v0

    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    goto :goto_0

    .line 810
    :cond_1
    :try_start_2
    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v2

    .line 813
    :goto_2
    const-string v4, "PHONE"

    const-string v5, "contactType"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 817
    new-instance v4, Lcom/chase/sig/android/domain/MobilePhoneNumber;

    const-string v5, "value"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "id"

    invoke-virtual {v2, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "label"

    invoke-virtual {v2, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v5, v6, v2}, Lcom/chase/sig/android/domain/MobilePhoneNumber;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 821
    invoke-virtual {v3, v4}, Lcom/chase/sig/android/domain/QuickPayRecipient;->a(Lcom/chase/sig/android/domain/MobilePhoneNumber;)V

    .line 801
    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 812
    :catch_1
    move-exception v2

    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    goto :goto_2

    .line 823
    :cond_2
    const-string v4, "PRIMARY"

    const-string v5, "label"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    new-instance v5, Lcom/chase/sig/android/domain/Email;

    const-string v6, "value"

    invoke-virtual {v2, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    const-string v7, "id"

    invoke-virtual {v2, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v6, v4, v2}, Lcom/chase/sig/android/domain/Email;-><init>(Ljava/lang/String;ZLjava/lang/String;)V

    invoke-virtual {v3, v5}, Lcom/chase/sig/android/domain/QuickPayRecipient;->a(Lcom/chase/sig/android/domain/Email;)V

    goto :goto_3
.end method

.method public static a(Lcom/chase/sig/android/domain/QuickPayAcceptMoneyTransaction;)Lcom/chase/sig/android/service/quickpay/QuickPayAcceptMoneyResponse;
    .locals 5
    .parameter

    .prologue
    .line 250
    new-instance v1, Lcom/chase/sig/android/service/quickpay/QuickPayAcceptMoneyResponse;

    invoke-direct {v1}, Lcom/chase/sig/android/service/quickpay/QuickPayAcceptMoneyResponse;-><init>()V

    .line 252
    const v0, 0x7f07001b

    invoke-static {v0}, Lcom/chase/sig/android/service/y;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 254
    invoke-static {}, Lcom/chase/sig/android/service/y;->c()Ljava/util/Hashtable;

    move-result-object v2

    .line 255
    const-string v3, "transactionId"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayAcceptMoneyTransaction;->w()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 256
    const-string v3, "token"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayAcceptMoneyTransaction;->J()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 259
    :try_start_0
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v3

    invoke-static {v3, v0, v2}, Lcom/chase/sig/android/util/m;->a(Lcom/chase/sig/android/ChaseApplication;Ljava/lang/String;Ljava/util/Hashtable;)Lorg/json/JSONObject;

    move-result-object v2

    .line 260
    new-instance v0, Lcom/chase/sig/android/service/quickpay/QuickPayAcceptMoneyResponse;

    invoke-direct {v0}, Lcom/chase/sig/android/service/quickpay/QuickPayAcceptMoneyResponse;-><init>()V

    const-string v3, "status"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/service/quickpay/QuickPayAcceptMoneyResponse;->a(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/service/quickpay/QuickPayAcceptMoneyResponse;->a(Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lcom/chase/sig/android/util/ChaseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 265
    :goto_0
    return-object v0

    .line 262
    :catch_0
    move-exception v0

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/service/quickpay/QuickPayAcceptMoneyResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    move-object v0, v1

    .line 265
    goto :goto_0
.end method

.method public static a(Lcom/chase/sig/android/domain/QuickPayTransaction;Ljava/lang/Boolean;)Lcom/chase/sig/android/service/quickpay/QuickPayCancelPendingTransactionResponse;
    .locals 4
    .parameter
    .parameter

    .prologue
    .line 999
    new-instance v1, Lcom/chase/sig/android/service/quickpay/QuickPayCancelPendingTransactionResponse;

    invoke-direct {v1}, Lcom/chase/sig/android/service/quickpay/QuickPayCancelPendingTransactionResponse;-><init>()V

    .line 1003
    :try_start_0
    invoke-static {}, Lcom/chase/sig/android/service/y;->c()Ljava/util/Hashtable;

    move-result-object v2

    .line 1004
    const-string v3, "isRepeatingPayment"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->D()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "true"

    :goto_0
    invoke-virtual {v2, v3, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1006
    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->D()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1007
    const-string v3, "transactionId"

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->w()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v2, v3, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1009
    const-string v3, "token"

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->J()Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-virtual {v2, v3, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1015
    :goto_3
    const-string v3, "cancelEntireSeries"

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "false"

    :goto_4
    invoke-virtual {v2, v3, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1016
    const v0, 0x7f070020

    invoke-static {v0}, Lcom/chase/sig/android/service/y;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 1017
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v3

    invoke-static {v3, v0, v2}, Lcom/chase/sig/android/util/m;->a(Lcom/chase/sig/android/ChaseApplication;Ljava/lang/String;Ljava/util/Hashtable;)Lorg/json/JSONObject;

    move-result-object v2

    .line 1019
    new-instance v0, Lcom/chase/sig/android/service/quickpay/QuickPayCancelPendingTransactionResponse;

    invoke-direct {v0}, Lcom/chase/sig/android/service/quickpay/QuickPayCancelPendingTransactionResponse;-><init>()V

    const-string v3, "paymentDetails"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/service/quickpay/QuickPayCancelPendingTransactionResponse;->b(Lorg/json/JSONObject;)V

    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickpay/QuickPayCancelPendingTransactionResponse;->e()Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "paymentDetails"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    invoke-static {v2}, Lcom/chase/sig/android/service/y;->f(Lorg/json/JSONObject;)Lcom/chase/sig/android/domain/QuickPayPendingTransaction;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/service/quickpay/QuickPayCancelPendingTransactionResponse;->a(Lcom/chase/sig/android/domain/QuickPayPendingTransaction;)V

    .line 1025
    :cond_0
    :goto_5
    return-object v0

    .line 1004
    :cond_1
    const-string v0, "false"

    goto :goto_0

    .line 1007
    :cond_2
    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->L()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1009
    :cond_3
    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->K()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 1012
    :cond_4
    const-string v0, "transactionId"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->w()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1013
    const-string v0, "token"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->J()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    .line 1020
    :catch_0
    move-exception v0

    .line 1021
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/service/quickpay/QuickPayCancelPendingTransactionResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    move-object v0, v1

    goto :goto_5

    .line 1015
    :cond_5
    :try_start_1
    const-string v0, "true"
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_4
.end method

.method public static a(Lcom/chase/sig/android/domain/QuickPayDeclineTransaction;)Lcom/chase/sig/android/service/quickpay/QuickPayDeclineResponse;
    .locals 5
    .parameter

    .prologue
    .line 278
    new-instance v1, Lcom/chase/sig/android/service/quickpay/QuickPayDeclineResponse;

    invoke-direct {v1}, Lcom/chase/sig/android/service/quickpay/QuickPayDeclineResponse;-><init>()V

    .line 280
    const v0, 0x7f07001a

    invoke-static {v0}, Lcom/chase/sig/android/service/y;->a(I)Ljava/lang/String;

    move-result-object v2

    .line 282
    invoke-static {}, Lcom/chase/sig/android/service/y;->c()Ljava/util/Hashtable;

    move-result-object v3

    .line 283
    const-string v0, "transactionId"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayDeclineTransaction;->w()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 284
    const-string v0, "activityType"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayDeclineTransaction;->I()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 285
    const-string v0, "declineReason"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayDeclineTransaction;->m()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 286
    const-string v4, "isInvoiceRequest"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayDeclineTransaction;->S()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "true"

    :goto_0
    invoke-virtual {v3, v4, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 287
    const-string v0, "token"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayDeclineTransaction;->J()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 290
    :try_start_0
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-static {v0, v2, v3}, Lcom/chase/sig/android/util/m;->a(Lcom/chase/sig/android/ChaseApplication;Ljava/lang/String;Ljava/util/Hashtable;)Lorg/json/JSONObject;

    move-result-object v2

    .line 292
    new-instance v0, Lcom/chase/sig/android/service/quickpay/QuickPayDeclineResponse;

    invoke-direct {v0}, Lcom/chase/sig/android/service/quickpay/QuickPayDeclineResponse;-><init>()V

    const-string v3, "status"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "Success"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/service/quickpay/QuickPayDeclineResponse;->a(Z)V

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/service/quickpay/QuickPayDeclineResponse;->a(Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lcom/chase/sig/android/util/ChaseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 297
    :goto_1
    return-object v0

    .line 286
    :cond_0
    const-string v0, "false"

    goto :goto_0

    .line 294
    :catch_0
    move-exception v0

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/service/quickpay/QuickPayDeclineResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    move-object v0, v1

    .line 297
    goto :goto_1
.end method

.method public static a()Lcom/chase/sig/android/service/quickpay/QuickPayRecipientListResponse;
    .locals 7

    .prologue
    .line 496
    new-instance v1, Lcom/chase/sig/android/service/quickpay/QuickPayRecipientListResponse;

    invoke-direct {v1}, Lcom/chase/sig/android/service/quickpay/QuickPayRecipientListResponse;-><init>()V

    .line 498
    const v0, 0x7f070022

    :try_start_0
    invoke-static {v0}, Lcom/chase/sig/android/service/y;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 499
    invoke-static {}, Lcom/chase/sig/android/service/y;->c()Ljava/util/Hashtable;

    move-result-object v2

    .line 504
    const-string v3, "pageNumber"

    const-string v4, "1"

    invoke-virtual {v2, v3, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 506
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v3

    invoke-static {v3, v0, v2}, Lcom/chase/sig/android/util/m;->a(Lcom/chase/sig/android/ChaseApplication;Ljava/lang/String;Ljava/util/Hashtable;)Lorg/json/JSONObject;

    move-result-object v3

    .line 507
    new-instance v0, Lcom/chase/sig/android/service/quickpay/QuickPayRecipientListResponse;

    invoke-direct {v0}, Lcom/chase/sig/android/service/quickpay/QuickPayRecipientListResponse;-><init>()V

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/service/quickpay/QuickPayRecipientListResponse;->b(Lorg/json/JSONObject;)V

    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickpay/QuickPayRecipientListResponse;->e()Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "recipients"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    :goto_0
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-ge v2, v6, :cond_0

    invoke-virtual {v4, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    invoke-static {v6}, Lcom/chase/sig/android/service/y;->a(Lorg/json/JSONObject;)Lcom/chase/sig/android/domain/QuickPayRecipient;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    const-string v2, "contentHeader"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/service/quickpay/QuickPayRecipientListResponse;->b(Ljava/lang/String;)V

    const-string v2, "contentTitle"

    invoke-virtual {v3, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/service/quickpay/QuickPayRecipientListResponse;->a(Ljava/lang/String;)V

    invoke-virtual {v0, v5}, Lcom/chase/sig/android/service/quickpay/QuickPayRecipientListResponse;->a(Ljava/util/ArrayList;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 511
    :cond_1
    :goto_1
    return-object v0

    .line 509
    :catch_0
    move-exception v0

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/service/quickpay/QuickPayRecipientListResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    move-object v0, v1

    .line 511
    goto :goto_1
.end method

.method public static a(Lcom/chase/sig/android/domain/QuickPayTransaction;)Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;
    .locals 7
    .parameter

    .prologue
    .line 189
    new-instance v1, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;

    invoke-direct {v1}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;-><init>()V

    .line 192
    const v0, 0x7f07001c

    invoke-static {v0}, Lcom/chase/sig/android/service/y;->a(I)Ljava/lang/String;

    move-result-object v2

    .line 194
    invoke-static {}, Lcom/chase/sig/android/service/y;->c()Ljava/util/Hashtable;

    move-result-object v3

    .line 196
    invoke-static {v3, p0}, Lcom/chase/sig/android/service/y;->a(Ljava/util/Hashtable;Lcom/chase/sig/android/domain/QuickPayTransaction;)V

    .line 198
    const-string v0, "selectedRecipientId"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 200
    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->D()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 201
    const-string v0, "repeatingId"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->L()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v0, v4}, Lcom/chase/sig/android/service/y;->a(Ljava/util/Hashtable;Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    const-string v0, "frequency"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->X()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 203
    const-string v0, "noOfPayments"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->H()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 205
    const-string v4, "unlimitedPayments"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->C()Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "true"

    :goto_0
    invoke-virtual {v3, v4, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 206
    const-string v0, "token"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->K()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v0, v4}, Lcom/chase/sig/android/service/y;->a(Ljava/util/Hashtable;Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    :goto_1
    const-string v0, "selectedAccountId"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->p()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v0, v4}, Lcom/chase/sig/android/service/y;->a(Ljava/util/Hashtable;Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    const-string v0, "accountType"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->W()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v0, v4}, Lcom/chase/sig/android/service/y;->a(Ljava/util/Hashtable;Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    const-string v0, "amount"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->r()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215
    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->s()Ljava/util/Date;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 216
    const-string v0, "sendOnDate"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->s()Ljava/util/Date;

    move-result-object v4

    invoke-static {v4}, Lcom/chase/sig/android/util/s;->b(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 219
    :cond_0
    const-string v0, "recipientEmail"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->j()Lcom/chase/sig/android/domain/QuickPayRecipient;

    move-result-object v4

    invoke-virtual {v4}, Lcom/chase/sig/android/domain/QuickPayRecipient;->d()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v0, v4}, Lcom/chase/sig/android/service/y;->a(Ljava/util/Hashtable;Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    const-string v0, "recipientNickname"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->j()Lcom/chase/sig/android/domain/QuickPayRecipient;

    move-result-object v4

    invoke-virtual {v4}, Lcom/chase/sig/android/domain/QuickPayRecipient;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v0, v4}, Lcom/chase/sig/android/service/y;->a(Ljava/util/Hashtable;Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    const-string v0, "phoneNumber"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->j()Lcom/chase/sig/android/domain/QuickPayRecipient;

    move-result-object v4

    invoke-virtual {v4}, Lcom/chase/sig/android/domain/QuickPayRecipient;->g()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v0, v4}, Lcom/chase/sig/android/service/y;->a(Ljava/util/Hashtable;Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->E()Ljava/util/Date;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 225
    const-string v0, "dueDate"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->E()Ljava/util/Date;

    move-result-object v4

    invoke-static {v4}, Lcom/chase/sig/android/util/s;->b(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 228
    :cond_1
    const-string v0, "memo"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->t()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 229
    const-string v0, "cvv"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->x()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v0, v4}, Lcom/chase/sig/android/service/y;->a(Ljava/util/Hashtable;Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    const-string v4, "isRepeatingPayment"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->D()Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "true"

    :goto_2
    invoke-virtual {v3, v4, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 232
    const-string v4, "invoiceRequest"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->S()Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v0, "true"

    :goto_3
    invoke-virtual {v3, v4, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 234
    const-string v4, "direction"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->B()Z

    move-result v0

    if-eqz v0, :cond_8

    const-string v0, "inbound"

    :goto_4
    invoke-virtual {v3, v4, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 237
    :try_start_0
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-static {v0, v2, v3}, Lcom/chase/sig/android/util/m;->a(Lcom/chase/sig/android/ChaseApplication;Ljava/lang/String;Ljava/util/Hashtable;)Lorg/json/JSONObject;

    move-result-object v2

    .line 238
    new-instance v0, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;

    invoke-direct {v0}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;-><init>()V

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->b(Lorg/json/JSONObject;)V

    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->e()Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "recipient"

    invoke-static {v2, v3}, Lcom/chase/sig/android/util/n;->a(Lorg/json/JSONObject;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "recipient"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    invoke-static {v3}, Lcom/chase/sig/android/service/y;->a(Lorg/json/JSONObject;)Lcom/chase/sig/android/domain/QuickPayRecipient;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->a(Lcom/chase/sig/android/domain/QuickPayRecipient;)V

    :cond_2
    const-string v3, "formId"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->o(Ljava/lang/String;)V

    const-string v3, "accountDisplayName"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->f(Ljava/lang/String;)V

    const-string v3, "amount"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->g(Ljava/lang/String;)V

    const-string v3, "outboundSendOnDate"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->i(Ljava/lang/String;)V

    const-string v3, "inboundDueDate"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->l(Ljava/lang/String;)V

    const-string v3, "outboundMemo"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->j(Ljava/lang/String;)V

    const-string v3, "invoiceId"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->h(Ljava/lang/String;)V

    const-string v3, "isRepeatingPayment"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v3

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->b(Z)V

    const-string v3, "frequency"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->m(Ljava/lang/String;)V

    const-string v3, "noOfPayments"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->a(I)V

    const-string v3, "unlimitedPayments"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v3

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->c(Z)V

    const-string v3, "duplicate"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v3

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->a(Z)V

    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->m()V

    const-string v3, "duplicateMessage"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->k(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->b(Lorg/json/JSONObject;)V

    const-string v3, "smsReason"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->p(Ljava/lang/String;)V

    const-string v3, "true"

    const-string v4, "iSsmsEligible"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->e(Z)V

    const-string v3, "feeDisclaimer"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "isNickameMatched"

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->q(Ljava/lang/String;)V

    const-string v4, "true"

    const-string v5, "isWarning"

    const-string v6, "false"

    invoke-virtual {v2, v5, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->d(Z)V

    invoke-static {v3}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "\r"

    const-string v4, ""

    invoke-virtual {v3, v2, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->n(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/chase/sig/android/util/ChaseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 244
    :cond_3
    :goto_5
    return-object v0

    .line 205
    :cond_4
    const-string v0, "false"

    goto/16 :goto_0

    .line 208
    :cond_5
    const-string v0, "token"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->J()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v0, v4}, Lcom/chase/sig/android/service/y;->a(Ljava/util/Hashtable;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 231
    :cond_6
    const-string v0, "false"

    goto/16 :goto_2

    .line 232
    :cond_7
    const-string v0, "false"

    goto/16 :goto_3

    .line 234
    :cond_8
    const-string v0, "outbound"

    goto/16 :goto_4

    .line 241
    :catch_0
    move-exception v0

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionVerifyResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    move-object v0, v1

    .line 244
    goto :goto_5
.end method

.method public static a(Lcom/chase/sig/android/domain/QuickPayActivityType;I)Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;
    .locals 8
    .parameter
    .parameter

    .prologue
    .line 163
    new-instance v2, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;

    invoke-direct {v2}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;-><init>()V

    .line 166
    :try_start_0
    invoke-static {}, Lcom/chase/sig/android/service/y;->c()Ljava/util/Hashtable;

    move-result-object v0

    .line 167
    const-string v1, "activityType"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayActivityType;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 168
    const-string v1, "pageNumber"

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 169
    const-string v1, "sortByColumn"

    const-string v3, "date"

    invoke-virtual {v0, v1, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 171
    const v1, 0x7f070017

    invoke-static {v1}, Lcom/chase/sig/android/service/y;->a(I)Ljava/lang/String;

    move-result-object v1

    .line 173
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v3

    invoke-static {v3, v1, v0}, Lcom/chase/sig/android/util/m;->a(Lcom/chase/sig/android/ChaseApplication;Ljava/lang/String;Ljava/util/Hashtable;)Lorg/json/JSONObject;

    move-result-object v4

    .line 174
    new-instance v1, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;

    invoke-direct {v1}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;-><init>()V

    const-string v0, "contentHeader1"

    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->e(Ljava/lang/String;)V

    const-string v0, "contentTitle"

    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->b(Ljava/lang/String;)V

    const-string v0, "contentTitle"

    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->a(Ljava/lang/String;)V

    const-string v0, "contentEmptyMessage"

    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->g(Ljava/lang/String;)V

    const-string v0, "transactions"

    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "transactions"

    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v5

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v7

    const/4 v0, 0x0

    move v3, v0

    :goto_0
    if-ge v3, v7, :cond_1

    invoke-virtual {v5, v3}, Lorg/json/JSONArray;->opt(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Lorg/json/JSONObject;

    invoke-static {v0}, Lcom/chase/sig/android/service/y;->c(Lorg/json/JSONObject;)Lcom/chase/sig/android/domain/QuickPayActivityItem;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_1
    invoke-virtual {v1, v6}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->a(Ljava/util/ArrayList;)V

    :cond_2
    const-string v0, "pageInfo"

    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "pageInfo"

    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    if-eqz v0, :cond_3

    const-string v3, "showNext"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->f(Ljava/lang/String;)V

    const-string v3, "totalRows"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v1, v3}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->a(I)V

    const-string v3, "nextPage"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->b(I)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 175
    :cond_3
    :try_start_1
    invoke-virtual {v1, p1}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->c(I)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    move-object v0, v1

    .line 183
    :goto_1
    return-object v0

    .line 176
    :catch_0
    move-exception v0

    move-object v0, v2

    .line 177
    :goto_2
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    goto :goto_1

    .line 179
    :catch_1
    move-exception v0

    move-object v0, v2

    .line 180
    :goto_3
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    goto :goto_1

    .line 179
    :catch_2
    move-exception v0

    move-object v0, v1

    goto :goto_3

    .line 176
    :catch_3
    move-exception v0

    move-object v0, v1

    goto :goto_2
.end method

.method public static a(Ljava/lang/String;I)Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;
    .locals 8
    .parameter
    .parameter

    .prologue
    .line 973
    new-instance v2, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;

    invoke-direct {v2}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;-><init>()V

    .line 977
    :try_start_0
    invoke-static {}, Lcom/chase/sig/android/service/y;->c()Ljava/util/Hashtable;

    move-result-object v0

    .line 978
    const-string v1, "sortByColumn"

    invoke-virtual {v0, v1, p0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 979
    const-string v1, "pageNumber"

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 980
    const-string v1, "listPending"

    const-string v3, "true"

    invoke-virtual {v0, v1, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 982
    const v1, 0x7f07001f

    invoke-static {v1}, Lcom/chase/sig/android/service/y;->a(I)Ljava/lang/String;

    move-result-object v1

    .line 983
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v3

    invoke-static {v3, v1, v0}, Lcom/chase/sig/android/util/m;->a(Lcom/chase/sig/android/ChaseApplication;Ljava/lang/String;Ljava/util/Hashtable;)Lorg/json/JSONObject;

    move-result-object v4

    .line 984
    new-instance v1, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;

    invoke-direct {v1}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;-><init>()V

    const-string v0, "contentHeader"

    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->e(Ljava/lang/String;)V

    const-string v0, "contentTitle"

    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->a(Ljava/lang/String;)V

    const-string v0, "contentEmptyMessage"

    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->g(Ljava/lang/String;)V

    const-string v0, "transactions"

    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "transactions"

    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v5

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v7

    const/4 v0, 0x0

    move v3, v0

    :goto_0
    if-ge v3, v7, :cond_1

    invoke-virtual {v5, v3}, Lorg/json/JSONArray;->opt(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Lorg/json/JSONObject;

    invoke-static {v0}, Lcom/chase/sig/android/service/y;->f(Lorg/json/JSONObject;)Lcom/chase/sig/android/domain/QuickPayPendingTransaction;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_1
    invoke-virtual {v1, v6}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->b(Ljava/util/ArrayList;)V

    :cond_2
    const-string v0, "pageInfo"

    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "pageInfo"

    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    if-eqz v0, :cond_3

    const-string v3, "showNext"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->f(Ljava/lang/String;)V

    const-string v3, "totalRows"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v1, v3}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->a(I)V

    const-string v3, "nextPage"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->b(I)V

    const-string v0, "true"

    invoke-virtual {v1}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {v1}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->k()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->c(I)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :cond_3
    move-object v0, v1

    .line 993
    :goto_1
    return-object v0

    .line 985
    :catch_0
    move-exception v0

    .line 986
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    move-object v0, v2

    .line 991
    goto :goto_1

    .line 988
    :catch_1
    move-exception v0

    .line 989
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    move-object v0, v2

    goto :goto_1
.end method

.method public static a(Ljava/lang/String;Lcom/chase/sig/android/domain/QuickPayActivityType;Ljava/lang/String;)Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;
    .locals 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 447
    new-instance v1, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;

    invoke-direct {v1}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;-><init>()V

    .line 450
    const v0, 0x7f070018

    :try_start_0
    invoke-static {v0}, Lcom/chase/sig/android/service/y;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 451
    invoke-static {}, Lcom/chase/sig/android/service/y;->c()Ljava/util/Hashtable;

    move-result-object v2

    .line 452
    const-string v3, "transactionId"

    invoke-virtual {v2, v3, p0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 453
    const-string v3, "activityType"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/QuickPayActivityType;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 455
    if-eqz p2, :cond_0

    .line 456
    const-string v3, "invoiceRequest"

    invoke-virtual {v2, v3, p2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 459
    :cond_0
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v3

    invoke-static {v3, v0, v2}, Lcom/chase/sig/android/util/m;->a(Lcom/chase/sig/android/ChaseApplication;Ljava/lang/String;Ljava/util/Hashtable;)Lorg/json/JSONObject;

    move-result-object v2

    .line 460
    new-instance v0, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;

    invoke-direct {v0}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;-><init>()V

    const-string v3, "contentHeader1"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->e(Ljava/lang/String;)V

    const-string v3, "contentTitle"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->b(Ljava/lang/String;)V

    const-string v3, "contentTitle"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->a(Ljava/lang/String;)V

    const-string v3, "contentData"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "contentData"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    invoke-static {v2}, Lcom/chase/sig/android/service/y;->c(Lorg/json/JSONObject;)Lcom/chase/sig/android/domain/QuickPayActivityItem;

    move-result-object v2

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->a(Ljava/util/ArrayList;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 465
    :cond_1
    :goto_0
    return-object v0

    .line 462
    :catch_0
    move-exception v0

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionListResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    move-object v0, v1

    .line 465
    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/service/quickpay/QuickPayTransactionStartResponse;
    .locals 6
    .parameter
    .parameter

    .prologue
    .line 470
    new-instance v2, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionStartResponse;

    invoke-direct {v2}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionStartResponse;-><init>()V

    .line 472
    const v0, 0x7f070019

    :try_start_0
    invoke-static {v0}, Lcom/chase/sig/android/service/y;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 473
    invoke-static {}, Lcom/chase/sig/android/service/y;->c()Ljava/util/Hashtable;

    move-result-object v1

    .line 475
    const-string v3, "direction"

    const-string v4, "outbound"

    invoke-virtual {v1, v3, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 477
    invoke-static {p0}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 478
    const-string v3, "transactionid"

    invoke-virtual {v1, v3, p0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 481
    :cond_0
    invoke-static {p1}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 482
    const-string v3, "invoiceId"

    invoke-virtual {v1, v3, p1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 485
    :cond_1
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v3

    invoke-static {v3, v0, v1}, Lcom/chase/sig/android/util/m;->a(Lcom/chase/sig/android/ChaseApplication;Ljava/lang/String;Ljava/util/Hashtable;)Lorg/json/JSONObject;

    move-result-object v0

    .line 487
    new-instance v1, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionStartResponse;

    invoke-direct {v1}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionStartResponse;-><init>()V

    const-string v3, "cutoffNotice"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    const-string v4, "\r"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionStartResponse;->a(Ljava/lang/String;)V

    :cond_2
    const-string v3, "payFromAccounts"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    const-string v3, "payFromAccounts"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v5

    const/4 v0, 0x0

    move v3, v0

    :goto_0
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-ge v3, v0, :cond_4

    invoke-virtual {v5, v3}, Lorg/json/JSONArray;->opt(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    check-cast v0, Lorg/json/JSONObject;

    invoke-static {v0}, Lcom/chase/sig/android/service/y;->b(Lorg/json/JSONObject;)Lcom/chase/sig/android/service/quickpay/PayFromAccount;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_4
    invoke-virtual {v1, v4}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionStartResponse;->a(Ljava/util/ArrayList;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_5
    move-object v0, v1

    .line 492
    :goto_1
    return-object v0

    .line 489
    :catch_0
    move-exception v0

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/chase/sig/android/service/quickpay/QuickPayTransactionStartResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    move-object v0, v2

    .line 492
    goto :goto_1
.end method

.method private static a(Ljava/util/Hashtable;Lcom/chase/sig/android/domain/QuickPayTransaction;)V
    .locals 6
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/chase/sig/android/domain/QuickPayTransaction;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1087
    const/4 v1, 0x0

    .line 1091
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/QuickPayTransaction;->R()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1093
    const-string v2, "0"

    .line 1095
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/QuickPayTransaction;->S()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1096
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/QuickPayTransaction;->A()Ljava/lang/String;

    move-result-object v0

    .line 1101
    :goto_0
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/QuickPayTransaction;->w()Ljava/lang/String;

    move-result-object v1

    move-object v5, v1

    move-object v1, v2

    move-object v2, v0

    move-object v0, v5

    .line 1118
    :goto_1
    const-string v3, "transactionId"

    invoke-static {p0, v3, v1}, Lcom/chase/sig/android/service/y;->a(Ljava/util/Hashtable;Ljava/lang/String;Ljava/lang/String;)V

    .line 1119
    const-string v1, "invoiceId"

    invoke-static {p0, v1, v2}, Lcom/chase/sig/android/service/y;->a(Ljava/util/Hashtable;Ljava/lang/String;Ljava/lang/String;)V

    .line 1120
    const-string v1, "requestTransactionId"

    invoke-static {p0, v1, v0}, Lcom/chase/sig/android/service/y;->a(Ljava/util/Hashtable;Ljava/lang/String;Ljava/lang/String;)V

    .line 1121
    return-void

    .line 1098
    :cond_0
    const-string v0, "0"

    goto :goto_0

    .line 1103
    :cond_1
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/QuickPayTransaction;->D()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/QuickPayTransaction;->V()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1104
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/QuickPayTransaction;->w()Ljava/lang/String;

    move-result-object v2

    .line 1105
    const-string v3, "0"

    .line 1106
    const-string v0, "noOfPayments"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/QuickPayTransaction;->H()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v0, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1108
    const-string v4, "editing"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/QuickPayTransaction;->V()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "true"

    :goto_2
    invoke-virtual {p0, v4, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v0, v1

    move-object v1, v2

    move-object v2, v3

    goto :goto_1

    :cond_2
    const-string v0, "false"

    goto :goto_2

    .line 1109
    :cond_3
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/QuickPayTransaction;->V()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1110
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/QuickPayTransaction;->w()Ljava/lang/String;

    move-result-object v2

    .line 1111
    const-string v3, "0"

    .line 1112
    const-string v4, "editing"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/QuickPayTransaction;->V()Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "true"

    :goto_3
    invoke-virtual {p0, v4, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v0, v1

    move-object v1, v2

    move-object v2, v3

    goto :goto_1

    :cond_4
    const-string v0, "false"

    goto :goto_3

    .line 1114
    :cond_5
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/QuickPayTransaction;->w()Ljava/lang/String;

    move-result-object v0

    .line 1115
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/QuickPayTransaction;->A()Ljava/lang/String;

    move-result-object v2

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    goto :goto_1
.end method

.method private static b(Lorg/json/JSONObject;)Lcom/chase/sig/android/service/quickpay/PayFromAccount;
    .locals 3
    .parameter

    .prologue
    .line 149
    new-instance v0, Lcom/chase/sig/android/service/quickpay/PayFromAccount;

    invoke-direct {v0}, Lcom/chase/sig/android/service/quickpay/PayFromAccount;-><init>()V

    .line 150
    const-string v1, "displayName"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/quickpay/PayFromAccount;->a(Ljava/lang/String;)V

    .line 151
    const-string v1, "id"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/quickpay/PayFromAccount;->c(Ljava/lang/String;)V

    .line 152
    const-string v1, "maskedAccountNumber"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/quickpay/PayFromAccount;->b(Ljava/lang/String;)V

    .line 153
    const-string v1, "nickname"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/quickpay/PayFromAccount;->d(Ljava/lang/String;)V

    .line 154
    const-string v1, "sourceIndicator"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/quickpay/PayFromAccount;->e(Ljava/lang/String;)V

    .line 155
    const-string v1, "requiresCVV"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/quickpay/PayFromAccount;->a(Z)V

    .line 156
    const-string v1, "sourceIndicator"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "ExternalDDA"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/quickpay/PayFromAccount;->b(Z)V

    .line 158
    return-object v0
.end method

.method public static b(Lcom/chase/sig/android/domain/QuickPayTransaction;)Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionConfirmResponse;
    .locals 5
    .parameter

    .prologue
    .line 310
    new-instance v2, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionConfirmResponse;

    invoke-direct {v2}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionConfirmResponse;-><init>()V

    .line 316
    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->V()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 317
    const v0, 0x7f07001e

    invoke-static {v0}, Lcom/chase/sig/android/service/y;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 322
    :goto_0
    invoke-static {}, Lcom/chase/sig/android/service/y;->c()Ljava/util/Hashtable;

    move-result-object v3

    .line 324
    invoke-static {v3, p0}, Lcom/chase/sig/android/service/y;->a(Ljava/util/Hashtable;Lcom/chase/sig/android/domain/QuickPayTransaction;)V

    .line 326
    const-string v1, "formId"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->O()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v1, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 327
    const-string v1, "amount"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->r()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v1, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 328
    const-string v1, "memo"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->t()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v1, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 329
    const-string v1, "selectedAccountId"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->p()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v1, v4}, Lcom/chase/sig/android/service/y;->a(Ljava/util/Hashtable;Ljava/lang/String;Ljava/lang/String;)V

    .line 330
    const-string v1, "selectedRecipientId"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v1, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 331
    const-string v1, "cvv"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->x()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v1, v4}, Lcom/chase/sig/android/service/y;->a(Ljava/util/Hashtable;Ljava/lang/String;Ljava/lang/String;)V

    .line 332
    const-string v4, "direction"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->B()Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, "inbound"

    :goto_1
    invoke-virtual {v3, v4, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 333
    const-string v4, "invoiceRequest"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->S()Z

    move-result v1

    if-eqz v1, :cond_6

    const-string v1, "true"

    :goto_2
    invoke-virtual {v3, v4, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 335
    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->s()Ljava/util/Date;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 336
    const-string v1, "sendOnDate"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->s()Ljava/util/Date;

    move-result-object v4

    invoke-static {v4}, Lcom/chase/sig/android/util/s;->b(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v1, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 339
    :cond_0
    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->E()Ljava/util/Date;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 340
    const-string v1, "dueDate"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->E()Ljava/util/Date;

    move-result-object v4

    invoke-static {v4}, Lcom/chase/sig/android/util/s;->b(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v1, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 343
    :cond_1
    const-string v1, "recipientEmail"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->u()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v1, v4}, Lcom/chase/sig/android/service/y;->a(Ljava/util/Hashtable;Ljava/lang/String;Ljava/lang/String;)V

    .line 344
    const-string v1, "phoneNumber"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->j()Lcom/chase/sig/android/domain/QuickPayRecipient;

    move-result-object v4

    invoke-virtual {v4}, Lcom/chase/sig/android/domain/QuickPayRecipient;->f()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v1, v4}, Lcom/chase/sig/android/service/y;->a(Ljava/util/Hashtable;Ljava/lang/String;Ljava/lang/String;)V

    .line 346
    const-string v1, "recipientNickname"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->v()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v1, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 348
    const-string v4, "isRepeatingPayment"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->D()Z

    move-result v1

    if-eqz v1, :cond_7

    const-string v1, "true"

    :goto_3
    invoke-virtual {v3, v4, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 350
    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->D()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 351
    const-string v1, "frequency"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->X()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v1, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 352
    const-string v1, "noOfPayments"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->H()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v1, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 354
    const-string v4, "unlimitedPayments"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->C()Z

    move-result v1

    if-eqz v1, :cond_8

    const-string v1, "true"

    :goto_4
    invoke-virtual {v3, v4, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 355
    const-string v1, "repeatingId"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->L()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v1, v4}, Lcom/chase/sig/android/service/y;->a(Ljava/util/Hashtable;Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    const-string v1, "token"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->K()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v1, v4}, Lcom/chase/sig/android/service/y;->a(Ljava/util/Hashtable;Ljava/lang/String;Ljava/lang/String;)V

    .line 362
    :goto_5
    :try_start_0
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v1

    invoke-static {v1, v0, v3}, Lcom/chase/sig/android/util/m;->a(Lcom/chase/sig/android/ChaseApplication;Ljava/lang/String;Ljava/util/Hashtable;)Lorg/json/JSONObject;

    move-result-object v1

    .line 363
    new-instance v0, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionConfirmResponse;

    invoke-direct {v0}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionConfirmResponse;-><init>()V

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionConfirmResponse;->b(Lorg/json/JSONObject;)V

    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionConfirmResponse;->e()Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "recipient"

    invoke-static {v1, v3}, Lcom/chase/sig/android/util/n;->a(Lorg/json/JSONObject;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "recipient"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    invoke-static {v3}, Lcom/chase/sig/android/service/y;->a(Lorg/json/JSONObject;)Lcom/chase/sig/android/domain/QuickPayRecipient;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionConfirmResponse;->a(Lcom/chase/sig/android/domain/QuickPayRecipient;)V

    :cond_2
    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionConfirmResponse;->b(Lorg/json/JSONObject;)V

    const-string v3, "accountDisplayname"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionConfirmResponse;->f(Ljava/lang/String;)V

    const-string v3, "amount"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionConfirmResponse;->g(Ljava/lang/String;)V

    const-string v3, "outboundSendOnDate"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionConfirmResponse;->i(Ljava/lang/String;)V

    const-string v3, "memo"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionConfirmResponse;->j(Ljava/lang/String;)V

    const-string v3, "transactionNumber"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionConfirmResponse;->b(Ljava/lang/String;)V

    const-string v3, "status"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionConfirmResponse;->a(Ljava/lang/String;)V

    const-string v3, "senderCode"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionConfirmResponse;->e(Ljava/lang/String;)V

    const-string v3, "isRepeatingPayment"

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v3

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionConfirmResponse;->b(Z)V

    const-string v3, "frequency"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionConfirmResponse;->m(Ljava/lang/String;)V

    const-string v3, "noOfPayments"

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionConfirmResponse;->a(I)V

    const-string v3, "unlimitedPayments"

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionConfirmResponse;->c(Z)V
    :try_end_0
    .catch Lcom/chase/sig/android/util/ChaseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 368
    :cond_3
    :goto_6
    return-object v0

    .line 319
    :cond_4
    const v0, 0x7f07001d

    invoke-static {v0}, Lcom/chase/sig/android/service/y;->a(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 332
    :cond_5
    const-string v1, "outbound"

    goto/16 :goto_1

    .line 333
    :cond_6
    const-string v1, "false"

    goto/16 :goto_2

    .line 348
    :cond_7
    const-string v1, "false"

    goto/16 :goto_3

    .line 354
    :cond_8
    const-string v1, "false"

    goto/16 :goto_4

    .line 358
    :cond_9
    const-string v1, "token"

    invoke-virtual {p0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->J()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v1, v4}, Lcom/chase/sig/android/service/y;->a(Ljava/util/Hashtable;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_5

    .line 365
    :catch_0
    move-exception v0

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/chase/sig/android/service/quickpay/QuickPaySendTransactionConfirmResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    move-object v0, v2

    .line 368
    goto :goto_6
.end method

.method private static c(Lorg/json/JSONObject;)Lcom/chase/sig/android/domain/QuickPayActivityItem;
    .locals 5
    .parameter

    .prologue
    .line 616
    new-instance v0, Lcom/chase/sig/android/domain/QuickPayActivityItem;

    invoke-direct {v0}, Lcom/chase/sig/android/domain/QuickPayActivityItem;-><init>()V

    .line 618
    const-string v1, "date"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/service/y;->d(Lorg/json/JSONObject;)Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v1

    .line 619
    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->a(Lcom/chase/sig/android/domain/LabeledValue;)V

    .line 621
    const-string v1, "amount"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/service/y;->d(Lorg/json/JSONObject;)Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v1

    .line 622
    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->b(Lcom/chase/sig/android/domain/LabeledValue;)V

    .line 624
    const-string v1, "status"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/service/y;->d(Lorg/json/JSONObject;)Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v1

    .line 625
    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->c(Lcom/chase/sig/android/domain/LabeledValue;)V

    .line 627
    const-string v1, "id"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/service/y;->d(Lorg/json/JSONObject;)Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v1

    .line 628
    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->d(Lcom/chase/sig/android/domain/LabeledValue;)V

    .line 630
    const-string v1, "type"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/service/y;->d(Lorg/json/JSONObject;)Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v1

    .line 631
    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->e(Lcom/chase/sig/android/domain/LabeledValue;)V

    .line 633
    const-string v1, "action"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/service/y;->d(Lorg/json/JSONObject;)Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v1

    .line 634
    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->f(Lcom/chase/sig/android/domain/LabeledValue;)V

    .line 636
    const-string v1, "decline"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/service/y;->d(Lorg/json/JSONObject;)Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v1

    .line 637
    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->g(Lcom/chase/sig/android/domain/LabeledValue;)V

    .line 639
    const-string v1, "accountName"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/service/y;->d(Lorg/json/JSONObject;)Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v1

    .line 640
    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->h(Lcom/chase/sig/android/domain/LabeledValue;)V

    .line 642
    const-string v1, "receivedOn"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/service/y;->d(Lorg/json/JSONObject;)Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v1

    .line 643
    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->j(Lcom/chase/sig/android/domain/LabeledValue;)V

    .line 645
    const-string v1, "acceptedOn"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/service/y;->d(Lorg/json/JSONObject;)Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v1

    .line 646
    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->k(Lcom/chase/sig/android/domain/LabeledValue;)V

    .line 648
    const-string v1, "memo"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/service/y;->d(Lorg/json/JSONObject;)Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v1

    .line 649
    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->i(Lcom/chase/sig/android/domain/LabeledValue;)V

    .line 651
    const-string v1, "declineReason"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "declineReason"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    const-string v2, "value"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 654
    const-string v1, "declineReason"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/service/y;->d(Lorg/json/JSONObject;)Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v1

    .line 655
    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->x(Lcom/chase/sig/android/domain/LabeledValue;)V

    .line 658
    :cond_0
    const-string v1, "transactionId"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 659
    const-string v1, "transactionId"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/service/y;->d(Lorg/json/JSONObject;)Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v1

    .line 660
    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->d(Lcom/chase/sig/android/domain/LabeledValue;)V

    .line 663
    :cond_1
    const-string v1, "token"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/service/y;->d(Lorg/json/JSONObject;)Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v1

    .line 664
    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->l(Lcom/chase/sig/android/domain/LabeledValue;)V

    .line 666
    const-string v1, "dueDate"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/service/y;->d(Lorg/json/JSONObject;)Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v1

    .line 667
    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->m(Lcom/chase/sig/android/domain/LabeledValue;)V

    .line 669
    const-string v1, "invoiceNumber"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/service/y;->d(Lorg/json/JSONObject;)Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v1

    .line 670
    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->n(Lcom/chase/sig/android/domain/LabeledValue;)V

    .line 672
    const-string v1, "sentOn"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 673
    const-string v1, "sentOn"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/service/y;->d(Lorg/json/JSONObject;)Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v1

    .line 674
    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->o(Lcom/chase/sig/android/domain/LabeledValue;)V

    .line 677
    :cond_2
    const-string v1, "displayDate"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 678
    if-eqz v1, :cond_3

    .line 679
    new-instance v2, Lcom/chase/sig/android/domain/LabeledValue;

    invoke-direct {v2}, Lcom/chase/sig/android/domain/LabeledValue;-><init>()V

    .line 680
    const-string v3, "label"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/chase/sig/android/domain/LabeledValue;->a(Ljava/lang/String;)V

    .line 681
    const-string v3, "value"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/chase/sig/android/domain/LabeledValue;->b(Ljava/lang/String;)V

    .line 682
    invoke-virtual {v0, v2}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->a(Lcom/chase/sig/android/domain/LabeledValue;)V

    .line 685
    :cond_3
    new-instance v1, Lcom/chase/sig/android/domain/LabeledValue;

    invoke-direct {v1}, Lcom/chase/sig/android/domain/LabeledValue;-><init>()V

    .line 686
    const-string v2, "isatmeligible"

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/domain/LabeledValue;->a(Ljava/lang/String;)V

    .line 687
    const-string v2, "isAtmEligible"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 688
    const-string v2, "isAtmEligible"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    invoke-static {v2}, Lcom/chase/sig/android/service/y;->d(Lorg/json/JSONObject;)Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v2

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/chase/sig/android/domain/LabeledValue;->b(Ljava/lang/String;)V

    .line 690
    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->p(Lcom/chase/sig/android/domain/LabeledValue;)V

    .line 693
    :cond_4
    const-string v1, "senderCode"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 694
    const-string v1, "senderCode"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/service/y;->d(Lorg/json/JSONObject;)Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v1

    .line 695
    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->q(Lcom/chase/sig/android/domain/LabeledValue;)V

    .line 698
    :cond_5
    const-string v1, "recipientCode"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 699
    const-string v1, "recipientCode"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/service/y;->d(Lorg/json/JSONObject;)Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v1

    .line 700
    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->v(Lcom/chase/sig/android/domain/LabeledValue;)V

    .line 703
    :cond_6
    const-string v1, "isInvoiceRequest"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 704
    const-string v1, "isInvoiceRequest"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/service/y;->d(Lorg/json/JSONObject;)Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v1

    .line 705
    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->w(Lcom/chase/sig/android/domain/LabeledValue;)V

    .line 708
    :cond_7
    const-string v1, "sender"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    const-string v1, "sender"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 709
    const-string v1, "sender"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/service/y;->a(Lorg/json/JSONObject;)Lcom/chase/sig/android/domain/QuickPayRecipient;

    move-result-object v1

    .line 710
    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->a(Lcom/chase/sig/android/domain/QuickPayRecipient;)V

    .line 725
    :cond_8
    :goto_0
    const-string v1, "repeatingInfo"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    const-string v1, "repeatingInfo"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 727
    new-instance v1, Lcom/chase/sig/android/domain/LabeledValue;

    const-string v2, "frequency"

    const-string v3, "repeatingInfo"

    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    const-string v4, "frequency"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/chase/sig/android/domain/LabeledValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 730
    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->r(Lcom/chase/sig/android/domain/LabeledValue;)V

    .line 732
    new-instance v1, Lcom/chase/sig/android/domain/LabeledValue;

    const-string v2, "numberOfRemainingPayments"

    const-string v3, "repeatingInfo"

    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    const-string v4, "numberOfRemainingPayments"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/chase/sig/android/domain/LabeledValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 735
    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->s(Lcom/chase/sig/android/domain/LabeledValue;)V

    .line 737
    new-instance v1, Lcom/chase/sig/android/domain/LabeledValue;

    const-string v2, "nextDeliverByDate"

    const-string v3, "repeatingInfo"

    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    const-string v4, "nextDeliverByDate"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/chase/sig/android/domain/LabeledValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 740
    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->u(Lcom/chase/sig/android/domain/LabeledValue;)V

    .line 742
    new-instance v1, Lcom/chase/sig/android/domain/LabeledValue;

    const-string v2, "nextSendonDate"

    const-string v3, "repeatingInfo"

    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    const-string v4, "nextSendonDate"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/chase/sig/android/domain/LabeledValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 745
    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->t(Lcom/chase/sig/android/domain/LabeledValue;)V

    .line 748
    :cond_9
    const-string v1, "payFromAccount"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 749
    if-eqz v1, :cond_a

    .line 750
    new-instance v2, Lcom/chase/sig/android/domain/LabeledValue;

    const-string v3, "Pay To"

    const-string v4, "displayName"

    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v3, v1}, Lcom/chase/sig/android/domain/LabeledValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 751
    invoke-virtual {v0, v2}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->h(Lcom/chase/sig/android/domain/LabeledValue;)V

    .line 753
    :cond_a
    return-object v0

    .line 711
    :cond_b
    const-string v1, "recipient"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_c

    const-string v1, "recipient"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_c

    .line 712
    const-string v1, "recipient"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/service/y;->a(Lorg/json/JSONObject;)Lcom/chase/sig/android/domain/QuickPayRecipient;

    move-result-object v1

    .line 713
    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->a(Lcom/chase/sig/android/domain/QuickPayRecipient;)V

    goto/16 :goto_0

    .line 715
    :cond_c
    const-string v1, "nickName"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 716
    if-eqz v1, :cond_d

    .line 717
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->j()Lcom/chase/sig/android/domain/QuickPayRecipient;

    move-result-object v2

    const-string v3, "value"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/chase/sig/android/domain/QuickPayRecipient;->a(Ljava/lang/String;)V

    .line 719
    :cond_d
    const-string v1, "email"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 720
    if-eqz v1, :cond_8

    .line 721
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->j()Lcom/chase/sig/android/domain/QuickPayRecipient;

    move-result-object v2

    const-string v3, "value"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/chase/sig/android/domain/QuickPayRecipient;->d(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public static c(I)Lcom/chase/sig/android/service/quickpay/TodoListResponse;
    .locals 11
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 52
    new-instance v1, Lcom/chase/sig/android/service/quickpay/TodoListResponse;

    invoke-direct {v1}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;-><init>()V

    .line 54
    :try_start_0
    invoke-static {}, Lcom/chase/sig/android/service/y;->c()Ljava/util/Hashtable;

    move-result-object v0

    .line 55
    const-string v3, "pageNumber"

    invoke-static {p0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    const-string v3, "includeFrequencyList"

    const-string v4, "true"

    invoke-virtual {v0, v3, v4}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    const v3, 0x7f070013

    invoke-static {v3}, Lcom/chase/sig/android/service/y;->a(I)Ljava/lang/String;

    move-result-object v3

    .line 59
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v4

    invoke-static {v4, v3, v0}, Lcom/chase/sig/android/util/m;->a(Lcom/chase/sig/android/ChaseApplication;Ljava/lang/String;Ljava/util/Hashtable;)Lorg/json/JSONObject;

    move-result-object v4

    .line 61
    invoke-virtual {v1, v4}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->b(Lorg/json/JSONObject;)V

    .line 63
    invoke-virtual {v1}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->e()Z

    move-result v0

    if-nez v0, :cond_6

    .line 64
    new-instance v0, Lcom/chase/sig/android/service/quickpay/TodoListResponse;

    invoke-direct {v0}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;-><init>()V

    const-string v3, "contentTitle"

    invoke-virtual {v4, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->a(Ljava/lang/String;)V

    const-string v3, "contentHeader1"

    invoke-virtual {v4, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->b(Ljava/lang/String;)V

    const-string v3, "totalItems"

    invoke-virtual {v4, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->a(I)V

    const-string v3, "contentEmptyMessage"

    invoke-virtual {v4, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->f(Ljava/lang/String;)V

    const-string v3, "accountMessage"

    invoke-virtual {v4, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->g(Ljava/lang/String;)V

    const-string v3, "missingPmtMessage"

    invoke-virtual {v4, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->h(Ljava/lang/String;)V

    const-string v3, "frequencyList"

    invoke-virtual {v4, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "frequencyList"

    invoke-virtual {v4, v3}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v5

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    move v3, v2

    :goto_0
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v7

    if-ge v3, v7, :cond_0

    invoke-virtual {v5, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v7

    new-instance v8, Lcom/chase/sig/android/domain/LabeledValue;

    const-string v9, "label"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v10, "value"

    invoke-virtual {v7, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v8, v9, v7}, Lcom/chase/sig/android/domain/LabeledValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    new-instance v3, Lcom/chase/sig/android/domain/m;

    invoke-direct {v3, v6}, Lcom/chase/sig/android/domain/m;-><init>(Ljava/util/ArrayList;)V

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->a(Lcom/chase/sig/android/domain/m;)V

    :cond_1
    const-string v3, "actions"

    invoke-virtual {v4, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "actions"

    invoke-virtual {v4, v3}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->b()V

    const-string v5, "true"

    const-string v6, "requestMoney"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    invoke-virtual {v0, v5}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->a(Z)V

    const-string v5, "true"

    const-string v6, "sendMoney"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    invoke-virtual {v0, v5}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->b(Z)V

    const-string v5, "true"

    const-string v6, "acceptMoney"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    invoke-virtual {v0, v5}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->c(Z)V

    const-string v5, "true"

    const-string v6, "viewTodo"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    invoke-virtual {v0, v5}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->d(Z)V

    const-string v5, "true"

    const-string v6, "viewActivity"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->e(Z)V

    :cond_2
    const-string v3, "todoItem"

    invoke-virtual {v4, v3}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    :goto_1
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v2, v5, :cond_3

    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    new-instance v6, Lcom/chase/sig/android/service/TodoItem;

    invoke-direct {v6}, Lcom/chase/sig/android/service/TodoItem;-><init>()V

    const-string v7, "header"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/chase/sig/android/service/TodoItem;->a(Ljava/lang/String;)V

    const-string v7, "actionLabel"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/chase/sig/android/service/TodoItem;->b(Ljava/lang/String;)V

    new-instance v7, Lcom/chase/sig/android/util/Dollar;

    const-string v8, "amount"

    invoke-virtual {v5, v8}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/chase/sig/android/util/Dollar;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v7}, Lcom/chase/sig/android/service/TodoItem;->a(Lcom/chase/sig/android/util/Dollar;)V

    const-string v7, "sublabel"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/chase/sig/android/service/TodoItem;->c(Ljava/lang/String;)V

    const-string v7, "declineLabel"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/chase/sig/android/service/TodoItem;->d(Ljava/lang/String;)V

    const-string v7, "id"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/chase/sig/android/service/TodoItem;->e(Ljava/lang/String;)V

    const-string v7, "typeString"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/chase/sig/android/service/TodoItem;->f(Ljava/lang/String;)V

    const-string v7, "typeLabel"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/chase/sig/android/service/TodoItem;->g(Ljava/lang/String;)V

    const-string v7, "isInvoiceRequest"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Lcom/chase/sig/android/service/TodoItem;->h(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->c()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    const-string v2, "totalItems"

    invoke-virtual {v4, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v2

    const/16 v3, 0x19

    if-le v2, v3, :cond_4

    const-string v2, "nextPage"

    invoke-virtual {v4, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->b(I)V

    const-string v2, "thisPage"

    invoke-virtual {v4, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->c(I)V

    :cond_4
    const-string v2, "defaultAccount"

    invoke-virtual {v4, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-static {v2}, Lcom/chase/sig/android/service/y;->b(Lorg/json/JSONObject;)Lcom/chase/sig/android/service/quickpay/PayFromAccount;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->a(Lcom/chase/sig/android/service/quickpay/PayFromAccount;)V

    :cond_5
    const-string v2, "earliestSendOnDate"

    invoke-virtual {v4, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->e(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :goto_2
    move-object v1, v0

    .line 74
    :goto_3
    return-object v1

    .line 67
    :catch_0
    move-exception v0

    .line 68
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    goto :goto_3

    .line 70
    :catch_1
    move-exception v0

    .line 71
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->a(Landroid/content/Context;)Lcom/chase/sig/android/service/l;

    goto :goto_3

    :cond_6
    move-object v0, v1

    goto :goto_2
.end method

.method private static d(Lorg/json/JSONObject;)Lcom/chase/sig/android/domain/LabeledValue;
    .locals 2
    .parameter

    .prologue
    .line 757
    new-instance v0, Lcom/chase/sig/android/domain/LabeledValue;

    invoke-direct {v0}, Lcom/chase/sig/android/domain/LabeledValue;-><init>()V

    if-eqz p0, :cond_0

    const-string v1, "label"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/LabeledValue;->a(Ljava/lang/String;)V

    const-string v1, "value"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->s(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/LabeledValue;->b(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method

.method private static e(Lorg/json/JSONObject;)Ljava/lang/String;
    .locals 2
    .parameter

    .prologue
    .line 837
    const-string v0, ""

    .line 840
    if-eqz p0, :cond_0

    .line 841
    const-string v1, "value"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 843
    :cond_0
    :goto_0
    return-object v0

    .line 841
    :cond_1
    const-string v0, "value"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static f(Lorg/json/JSONObject;)Lcom/chase/sig/android/domain/QuickPayPendingTransaction;
    .locals 4
    .parameter

    .prologue
    .line 849
    new-instance v0, Lcom/chase/sig/android/domain/QuickPayPendingTransaction;

    invoke-direct {v0}, Lcom/chase/sig/android/domain/QuickPayPendingTransaction;-><init>()V

    .line 850
    const-string v1, "transactionId"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 851
    const-string v1, "transactionId"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/service/y;->e(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/QuickPayPendingTransaction;->g(Ljava/lang/String;)V

    .line 854
    :cond_0
    const-string v1, "status"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 855
    const-string v1, "status"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/service/y;->e(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/QuickPayPendingTransaction;->f(Ljava/lang/String;)V

    .line 856
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickPayPendingTransaction;->z()V

    .line 859
    :cond_1
    const-string v1, "canEdit"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 860
    const-string v1, "canEdit"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/service/y;->e(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/QuickPayPendingTransaction;->r(Ljava/lang/String;)V

    .line 863
    :cond_2
    const-string v1, "canCancel"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 864
    const-string v1, "canCancel"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/service/y;->e(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/QuickPayPendingTransaction;->s(Ljava/lang/String;)V

    .line 867
    :cond_3
    const-string v1, "recipient"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 868
    const-string v1, "recipient"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/service/y;->a(Lorg/json/JSONObject;)Lcom/chase/sig/android/domain/QuickPayRecipient;

    move-result-object v1

    .line 869
    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/QuickPayPendingTransaction;->a(Lcom/chase/sig/android/domain/QuickPayRecipient;)V

    .line 872
    :cond_4
    const-string v1, "repeatingInfo"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 873
    const-string v1, "repeatingInfo"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 874
    if-eqz v1, :cond_5

    .line 875
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/domain/QuickPayPendingTransaction;->d(Z)V

    .line 876
    const-string v2, "frequency"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/domain/QuickPayPendingTransaction;->v(Ljava/lang/String;)V

    .line 877
    const-string v2, "true"

    const-string v3, "openEnded"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/domain/QuickPayPendingTransaction;->c(Z)V

    .line 879
    const-string v2, "numberOfRemainingPayments"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/domain/QuickPayPendingTransaction;->a(I)V

    .line 881
    const-string v2, "nextDeliverByDate"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/chase/sig/android/util/s;->g(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/domain/QuickPayPendingTransaction;->a(Ljava/util/Date;)V

    .line 883
    const-string v2, "nextSendonDate"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/chase/sig/android/util/s;->g(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/domain/QuickPayPendingTransaction;->c(Ljava/util/Date;)V

    .line 885
    const-string v2, "token"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/domain/QuickPayPendingTransaction;->n(Ljava/lang/String;)V

    .line 886
    const-string v2, "repeatingId"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/QuickPayPendingTransaction;->o(Ljava/lang/String;)V

    .line 890
    :cond_5
    const-string v1, "displayDate"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/service/y;->e(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/QuickPayPendingTransaction;->a(Ljava/lang/String;)V

    .line 891
    const-string v1, "amount"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/service/y;->e(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/QuickPayPendingTransaction;->d(Ljava/lang/String;)V

    .line 893
    const-string v1, "payFromAccount"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 894
    const-string v2, "displayName"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/domain/QuickPayPendingTransaction;->c(Ljava/lang/String;)V

    .line 895
    const-string v2, "id"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/domain/QuickPayPendingTransaction;->b(Ljava/lang/String;)V

    .line 896
    const-string v2, "accountType"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/QuickPayPendingTransaction;->u(Ljava/lang/String;)V

    .line 898
    const-string v1, "token"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/service/y;->e(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/QuickPayPendingTransaction;->m(Ljava/lang/String;)V

    .line 899
    const-string v1, "memo"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/service/y;->e(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/QuickPayPendingTransaction;->e(Ljava/lang/String;)V

    .line 900
    const-string v1, "sentOn"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/service/y;->e(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->g(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/QuickPayPendingTransaction;->b(Ljava/util/Date;)V

    .line 902
    return-object v0
.end method

.method private static g(Lorg/json/JSONObject;)Lcom/chase/sig/android/domain/QuickPayActivityItem;
    .locals 4
    .parameter

    .prologue
    .line 1061
    const/4 v0, 0x0

    .line 1063
    :try_start_0
    const-string v1, "contentdata"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/service/y;->c(Lorg/json/JSONObject;)Lcom/chase/sig/android/domain/QuickPayActivityItem;

    move-result-object v0

    .line 1064
    const-string v1, "contentdata"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    const-string v2, "paymentnotificationresent"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 1066
    const-string v2, "label"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->a(Ljava/lang/String;)V

    .line 1067
    const-string v2, "true"

    const-string v3, "value"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->a(Z)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1070
    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method
