.class public Lcom/chase/sig/android/util/ChaseException;
.super Ljava/lang/Exception;
.source "SourceFile"


# static fields
.field public static a:I

.field public static b:I


# instance fields
.field private description:Ljava/lang/String;

.field private exception:Ljava/lang/Exception;

.field private severity:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 6
    const/4 v0, 0x0

    sput v0, Lcom/chase/sig/android/util/ChaseException;->a:I

    .line 7
    const/4 v0, 0x1

    sput v0, Lcom/chase/sig/android/util/ChaseException;->b:I

    return-void
.end method

.method public constructor <init>(ILjava/lang/Exception;Ljava/lang/String;)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    .line 9
    sget v0, Lcom/chase/sig/android/util/ChaseException;->a:I

    iput v0, p0, Lcom/chase/sig/android/util/ChaseException;->severity:I

    .line 18
    iput p1, p0, Lcom/chase/sig/android/util/ChaseException;->severity:I

    .line 19
    iput-object p2, p0, Lcom/chase/sig/android/util/ChaseException;->exception:Ljava/lang/Exception;

    .line 20
    iput-object p3, p0, Lcom/chase/sig/android/util/ChaseException;->description:Ljava/lang/String;

    .line 21
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 14
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lcom/chase/sig/android/util/ChaseException;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    .line 15
    return-void
.end method

.method public constructor <init>(Ljava/lang/Exception;Ljava/lang/String;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 24
    sget v0, Lcom/chase/sig/android/util/ChaseException;->b:I

    invoke-direct {p0, v0, p1, p2}, Lcom/chase/sig/android/util/ChaseException;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    .line 25
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/chase/sig/android/util/ChaseException;->description:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 44
    iget-object v0, p0, Lcom/chase/sig/android/util/ChaseException;->exception:Ljava/lang/Exception;

    if-nez v0, :cond_0

    const-string v0, "[Severity=%s; Description=%s"

    new-array v1, v5, [Ljava/lang/Object;

    iget v2, p0, Lcom/chase/sig/android/util/ChaseException;->severity:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/chase/sig/android/util/ChaseException;->description:Ljava/lang/String;

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "[Severity=%s; Description=%s; InnerException=%s"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    iget v2, p0, Lcom/chase/sig/android/util/ChaseException;->severity:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/chase/sig/android/util/ChaseException;->description:Ljava/lang/String;

    aput-object v2, v1, v4

    iget-object v2, p0, Lcom/chase/sig/android/util/ChaseException;->exception:Ljava/lang/Exception;

    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
