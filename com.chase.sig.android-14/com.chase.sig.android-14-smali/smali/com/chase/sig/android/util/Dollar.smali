.class public Lcom/chase/sig/android/util/Dollar;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Comparable;


# static fields
.field private static a:Ljava/text/DecimalFormat;

.field private static b:Ljava/text/DecimalFormat;

.field private static c:Ljava/text/DecimalFormat;


# instance fields
.field private amount:Ljava/math/BigDecimal;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 3
    .parameter

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    if-nez p1, :cond_0

    .line 23
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/chase/sig/android/util/Dollar;->amount:Ljava/math/BigDecimal;

    .line 41
    :goto_0
    return-void

    .line 28
    :cond_0
    :try_start_0
    const-string v0, ","

    const-string v1, ""

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 29
    const-string v1, "$"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 30
    new-instance v1, Ljava/math/BigDecimal;

    invoke-direct {v1, v0}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    .line 32
    invoke-virtual {v1}, Ljava/math/BigDecimal;->scale()I

    move-result v0

    if-nez v0, :cond_1

    .line 33
    const/4 v0, 0x2

    invoke-virtual {v1, v0}, Ljava/math/BigDecimal;->setScale(I)Ljava/math/BigDecimal;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/util/Dollar;->amount:Ljava/math/BigDecimal;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 38
    :catch_0
    move-exception v0

    .line 39
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to format Dollar  "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    goto :goto_0

    .line 35
    :cond_1
    :try_start_1
    iput-object v1, p0, Lcom/chase/sig/android/util/Dollar;->amount:Ljava/math/BigDecimal;
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/math/BigDecimal;)V
    .locals 0
    .parameter

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/chase/sig/android/util/Dollar;->amount:Ljava/math/BigDecimal;

    .line 19
    return-void
.end method

.method private a(Ljava/text/DecimalFormat;)Ljava/lang/String;
    .locals 2
    .parameter

    .prologue
    .line 95
    iget-object v0, p0, Lcom/chase/sig/android/util/Dollar;->amount:Ljava/math/BigDecimal;

    if-nez v0, :cond_0

    .line 96
    const-string v0, ""

    .line 99
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/util/Dollar;->amount:Ljava/math/BigDecimal;

    invoke-virtual {v0}, Ljava/math/BigDecimal;->doubleValue()D

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/chase/sig/android/util/Dollar;->amount:Ljava/math/BigDecimal;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Ljava/math/BigDecimal;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/chase/sig/android/util/Dollar;->amount:Ljava/math/BigDecimal;

    return-object v0
.end method

.method public final c()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 53
    iget-object v1, p0, Lcom/chase/sig/android/util/Dollar;->amount:Ljava/math/BigDecimal;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/chase/sig/android/util/Dollar;->amount:Ljava/math/BigDecimal;

    new-instance v2, Ljava/math/BigDecimal;

    invoke-direct {v2, v0}, Ljava/math/BigDecimal;-><init>(I)V

    invoke-virtual {v1, v2}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v1

    if-lez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public compareTo(Ljava/lang/Object;)I
    .locals 2
    .parameter

    .prologue
    .line 104
    instance-of v0, p1, Lcom/chase/sig/android/util/Dollar;

    if-nez v0, :cond_0

    .line 105
    const/4 v0, 0x0

    .line 109
    :goto_0
    return v0

    .line 108
    :cond_0
    check-cast p1, Lcom/chase/sig/android/util/Dollar;

    .line 109
    iget-object v0, p0, Lcom/chase/sig/android/util/Dollar;->amount:Ljava/math/BigDecimal;

    iget-object v1, p1, Lcom/chase/sig/android/util/Dollar;->amount:Ljava/math/BigDecimal;

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    goto :goto_0
.end method

.method public final d()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 58
    iget-object v1, p0, Lcom/chase/sig/android/util/Dollar;->amount:Ljava/math/BigDecimal;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/chase/sig/android/util/Dollar;->amount:Ljava/math/BigDecimal;

    new-instance v2, Ljava/math/BigDecimal;

    invoke-direct {v2, v0}, Ljava/math/BigDecimal;-><init>(I)V

    invoke-virtual {v1, v2}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v1

    if-gez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/chase/sig/android/util/Dollar;->amount:Ljava/math/BigDecimal;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/util/Dollar;->amount:Ljava/math/BigDecimal;

    invoke-virtual {v0}, Ljava/math/BigDecimal;->toPlainString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final f()Ljava/lang/String;
    .locals 2

    .prologue
    .line 71
    sget-object v0, Lcom/chase/sig/android/util/Dollar;->c:Ljava/text/DecimalFormat;

    if-nez v0, :cond_0

    .line 72
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "###,###,###,##0.00"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/util/Dollar;->c:Ljava/text/DecimalFormat;

    .line 75
    :cond_0
    sget-object v0, Lcom/chase/sig/android/util/Dollar;->c:Ljava/text/DecimalFormat;

    invoke-direct {p0, v0}, Lcom/chase/sig/android/util/Dollar;->a(Ljava/text/DecimalFormat;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2

    .prologue
    .line 79
    sget-object v0, Lcom/chase/sig/android/util/Dollar;->b:Ljava/text/DecimalFormat;

    if-nez v0, :cond_0

    .line 80
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "###########0.00"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/util/Dollar;->b:Ljava/text/DecimalFormat;

    .line 83
    :cond_0
    sget-object v0, Lcom/chase/sig/android/util/Dollar;->b:Ljava/text/DecimalFormat;

    invoke-direct {p0, v0}, Lcom/chase/sig/android/util/Dollar;->a(Ljava/text/DecimalFormat;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final h()Ljava/lang/String;
    .locals 2

    .prologue
    .line 87
    sget-object v0, Lcom/chase/sig/android/util/Dollar;->a:Ljava/text/DecimalFormat;

    if-nez v0, :cond_0

    .line 88
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "$###,###,###,##0.00"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/chase/sig/android/util/Dollar;->a:Ljava/text/DecimalFormat;

    .line 91
    :cond_0
    sget-object v0, Lcom/chase/sig/android/util/Dollar;->a:Ljava/text/DecimalFormat;

    invoke-direct {p0, v0}, Lcom/chase/sig/android/util/Dollar;->a(Ljava/text/DecimalFormat;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    invoke-virtual {p0}, Lcom/chase/sig/android/util/Dollar;->h()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
