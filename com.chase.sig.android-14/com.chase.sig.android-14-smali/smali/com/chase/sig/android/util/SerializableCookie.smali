.class public Lcom/chase/sig/android/util/SerializableCookie;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Externalizable;
.implements Lorg/apache/http/cookie/Cookie;


# instance fields
.field private transient a:Lorg/apache/http/cookie/Cookie;

.field private transient b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    const/4 v0, 0x0

    iput v0, p0, Lcom/chase/sig/android/util/SerializableCookie;->b:I

    .line 19
    return-void
.end method

.method public constructor <init>(Lorg/apache/http/cookie/Cookie;)V
    .locals 1
    .parameter

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    const/4 v0, 0x0

    iput v0, p0, Lcom/chase/sig/android/util/SerializableCookie;->b:I

    .line 24
    iput-object p1, p0, Lcom/chase/sig/android/util/SerializableCookie;->a:Lorg/apache/http/cookie/Cookie;

    .line 25
    return-void
.end method


# virtual methods
.method public getComment()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/chase/sig/android/util/SerializableCookie;->a:Lorg/apache/http/cookie/Cookie;

    invoke-interface {v0}, Lorg/apache/http/cookie/Cookie;->getComment()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCommentURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/chase/sig/android/util/SerializableCookie;->a:Lorg/apache/http/cookie/Cookie;

    invoke-interface {v0}, Lorg/apache/http/cookie/Cookie;->getCommentURL()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDomain()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/chase/sig/android/util/SerializableCookie;->a:Lorg/apache/http/cookie/Cookie;

    invoke-interface {v0}, Lorg/apache/http/cookie/Cookie;->getDomain()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getExpiryDate()Ljava/util/Date;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/chase/sig/android/util/SerializableCookie;->a:Lorg/apache/http/cookie/Cookie;

    invoke-interface {v0}, Lorg/apache/http/cookie/Cookie;->getExpiryDate()Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/chase/sig/android/util/SerializableCookie;->a:Lorg/apache/http/cookie/Cookie;

    invoke-interface {v0}, Lorg/apache/http/cookie/Cookie;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/chase/sig/android/util/SerializableCookie;->a:Lorg/apache/http/cookie/Cookie;

    invoke-interface {v0}, Lorg/apache/http/cookie/Cookie;->getPath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPorts()[I
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/chase/sig/android/util/SerializableCookie;->a:Lorg/apache/http/cookie/Cookie;

    invoke-interface {v0}, Lorg/apache/http/cookie/Cookie;->getPorts()[I

    move-result-object v0

    return-object v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/chase/sig/android/util/SerializableCookie;->a:Lorg/apache/http/cookie/Cookie;

    invoke-interface {v0}, Lorg/apache/http/cookie/Cookie;->getValue()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getVersion()I
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/chase/sig/android/util/SerializableCookie;->a:Lorg/apache/http/cookie/Cookie;

    invoke-interface {v0}, Lorg/apache/http/cookie/Cookie;->getVersion()I

    move-result v0

    return v0
.end method

.method public isExpired(Ljava/util/Date;)Z
    .locals 1
    .parameter

    .prologue
    .line 74
    iget-object v0, p0, Lcom/chase/sig/android/util/SerializableCookie;->a:Lorg/apache/http/cookie/Cookie;

    invoke-interface {v0, p1}, Lorg/apache/http/cookie/Cookie;->isExpired(Ljava/util/Date;)Z

    move-result v0

    return v0
.end method

.method public isPersistent()Z
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/chase/sig/android/util/SerializableCookie;->a:Lorg/apache/http/cookie/Cookie;

    invoke-interface {v0}, Lorg/apache/http/cookie/Cookie;->isPersistent()Z

    move-result v0

    return v0
.end method

.method public isSecure()Z
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/chase/sig/android/util/SerializableCookie;->a:Lorg/apache/http/cookie/Cookie;

    invoke-interface {v0}, Lorg/apache/http/cookie/Cookie;->isSecure()Z

    move-result v0

    return v0
.end method

.method public readExternal(Ljava/io/ObjectInput;)V
    .locals 10
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 135
    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/chase/sig/android/util/SerializableCookie;->b:I

    .line 144
    iget v0, p0, Lcom/chase/sig/android/util/SerializableCookie;->b:I

    and-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_5

    .line 148
    invoke-interface {p1}, Ljava/io/ObjectInput;->readUTF()Ljava/lang/String;

    move-result-object v0

    .line 151
    :goto_0
    iget v2, p0, Lcom/chase/sig/android/util/SerializableCookie;->b:I

    and-int/lit8 v2, v2, 0x2

    if-nez v2, :cond_4

    .line 152
    invoke-interface {p1}, Ljava/io/ObjectInput;->readUTF()Ljava/lang/String;

    move-result-object v2

    .line 155
    :goto_1
    iget v3, p0, Lcom/chase/sig/android/util/SerializableCookie;->b:I

    and-int/lit8 v3, v3, 0x10

    if-nez v3, :cond_3

    .line 156
    new-instance v3, Ljava/util/Date;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readLong()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    .line 159
    :goto_2
    iget v4, p0, Lcom/chase/sig/android/util/SerializableCookie;->b:I

    and-int/lit8 v4, v4, 0x20

    if-nez v4, :cond_2

    .line 160
    invoke-interface {p1}, Ljava/io/ObjectInput;->readUTF()Ljava/lang/String;

    move-result-object v4

    .line 163
    :goto_3
    iget v5, p0, Lcom/chase/sig/android/util/SerializableCookie;->b:I

    and-int/lit8 v5, v5, 0x40

    if-nez v5, :cond_1

    .line 164
    invoke-interface {p1}, Ljava/io/ObjectInput;->readUTF()Ljava/lang/String;

    move-result-object v5

    .line 167
    :goto_4
    iget v6, p0, Lcom/chase/sig/android/util/SerializableCookie;->b:I

    and-int/lit16 v6, v6, 0x80

    if-nez v6, :cond_0

    .line 168
    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v7

    .line 170
    new-array v8, v7, [I

    .line 172
    const/4 v6, 0x0

    :goto_5
    if-ge v6, v7, :cond_0

    .line 173
    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v9

    aput v9, v8, v6

    .line 172
    add-int/lit8 v6, v6, 0x1

    goto :goto_5

    .line 177
    :cond_0
    invoke-interface {p1}, Ljava/io/ObjectInput;->readBoolean()Z

    move-result v6

    .line 178
    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v7

    .line 180
    new-instance v8, Lorg/apache/http/impl/cookie/BasicClientCookie;

    invoke-direct {v8, v0, v2}, Lorg/apache/http/impl/cookie/BasicClientCookie;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    invoke-virtual {v8, v1}, Lorg/apache/http/impl/cookie/BasicClientCookie;->setComment(Ljava/lang/String;)V

    .line 183
    invoke-virtual {v8, v4}, Lorg/apache/http/impl/cookie/BasicClientCookie;->setDomain(Ljava/lang/String;)V

    .line 184
    invoke-virtual {v8, v3}, Lorg/apache/http/impl/cookie/BasicClientCookie;->setExpiryDate(Ljava/util/Date;)V

    .line 185
    invoke-virtual {v8, v5}, Lorg/apache/http/impl/cookie/BasicClientCookie;->setPath(Ljava/lang/String;)V

    .line 186
    invoke-virtual {v8, v6}, Lorg/apache/http/impl/cookie/BasicClientCookie;->setSecure(Z)V

    .line 187
    invoke-virtual {v8, v7}, Lorg/apache/http/impl/cookie/BasicClientCookie;->setVersion(I)V

    .line 189
    iput-object v8, p0, Lcom/chase/sig/android/util/SerializableCookie;->a:Lorg/apache/http/cookie/Cookie;

    .line 190
    return-void

    :cond_1
    move-object v5, v1

    goto :goto_4

    :cond_2
    move-object v4, v1

    goto :goto_3

    :cond_3
    move-object v3, v1

    goto :goto_2

    :cond_4
    move-object v2, v1

    goto :goto_1

    :cond_5
    move-object v0, v1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lcom/chase/sig/android/util/SerializableCookie;->a:Lorg/apache/http/cookie/Cookie;

    if-nez v0, :cond_0

    .line 195
    const-string v0, "null"

    .line 197
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/util/SerializableCookie;->a:Lorg/apache/http/cookie/Cookie;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public writeExternal(Ljava/io/ObjectOutput;)V
    .locals 4
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 98
    iget v0, p0, Lcom/chase/sig/android/util/SerializableCookie;->b:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    .line 100
    iget v0, p0, Lcom/chase/sig/android/util/SerializableCookie;->b:I

    and-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_0

    .line 101
    invoke-virtual {p0}, Lcom/chase/sig/android/util/SerializableCookie;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeUTF(Ljava/lang/String;)V

    .line 104
    :cond_0
    iget v0, p0, Lcom/chase/sig/android/util/SerializableCookie;->b:I

    and-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_1

    .line 105
    invoke-virtual {p0}, Lcom/chase/sig/android/util/SerializableCookie;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeUTF(Ljava/lang/String;)V

    .line 108
    :cond_1
    iget v0, p0, Lcom/chase/sig/android/util/SerializableCookie;->b:I

    and-int/lit8 v0, v0, 0x10

    if-nez v0, :cond_2

    .line 109
    invoke-virtual {p0}, Lcom/chase/sig/android/util/SerializableCookie;->getExpiryDate()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-interface {p1, v2, v3}, Ljava/io/ObjectOutput;->writeLong(J)V

    .line 112
    :cond_2
    iget v0, p0, Lcom/chase/sig/android/util/SerializableCookie;->b:I

    and-int/lit8 v0, v0, 0x20

    if-nez v0, :cond_3

    .line 113
    invoke-virtual {p0}, Lcom/chase/sig/android/util/SerializableCookie;->getDomain()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeUTF(Ljava/lang/String;)V

    .line 116
    :cond_3
    iget v0, p0, Lcom/chase/sig/android/util/SerializableCookie;->b:I

    and-int/lit8 v0, v0, 0x40

    if-nez v0, :cond_4

    .line 117
    invoke-virtual {p0}, Lcom/chase/sig/android/util/SerializableCookie;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeUTF(Ljava/lang/String;)V

    .line 120
    :cond_4
    iget v0, p0, Lcom/chase/sig/android/util/SerializableCookie;->b:I

    and-int/lit16 v0, v0, 0x80

    if-nez v0, :cond_6

    .line 121
    invoke-virtual {p0}, Lcom/chase/sig/android/util/SerializableCookie;->getPorts()[I

    move-result-object v0

    if-nez v0, :cond_5

    move v0, v1

    .line 122
    :goto_0
    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    .line 124
    invoke-virtual {p0}, Lcom/chase/sig/android/util/SerializableCookie;->getPorts()[I

    move-result-object v2

    .line 125
    :goto_1
    if-ge v1, v0, :cond_6

    .line 126
    aget v3, v2, v1

    invoke-interface {p1, v3}, Ljava/io/ObjectOutput;->writeInt(I)V

    .line 125
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 121
    :cond_5
    invoke-virtual {p0}, Lcom/chase/sig/android/util/SerializableCookie;->getPorts()[I

    move-result-object v0

    array-length v0, v0

    goto :goto_0

    .line 130
    :cond_6
    invoke-virtual {p0}, Lcom/chase/sig/android/util/SerializableCookie;->isSecure()Z

    move-result v0

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeBoolean(Z)V

    .line 131
    invoke-virtual {p0}, Lcom/chase/sig/android/util/SerializableCookie;->getVersion()I

    move-result v0

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    .line 132
    return-void
.end method
