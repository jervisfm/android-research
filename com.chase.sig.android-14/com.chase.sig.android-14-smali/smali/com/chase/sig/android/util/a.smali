.class public final Lcom/chase/sig/android/util/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:[Ljava/lang/String;

.field public static final b:[Ljava/lang/String;

.field public static final c:[I

.field public static final d:[I

.field public static final e:[Ljava/lang/String;

.field public static final f:[I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 27
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "nickname"

    aput-object v1, v0, v2

    const-string v1, "amount"

    aput-object v1, v0, v3

    sput-object v0, Lcom/chase/sig/android/util/a;->a:[Ljava/lang/String;

    .line 31
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "nickname"

    aput-object v1, v0, v2

    const-string v1, "mask"

    aput-object v1, v0, v3

    const-string v1, "amount"

    aput-object v1, v0, v4

    sput-object v0, Lcom/chase/sig/android/util/a;->b:[Ljava/lang/String;

    .line 35
    new-array v0, v4, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/chase/sig/android/util/a;->c:[I

    .line 39
    new-array v0, v5, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/chase/sig/android/util/a;->d:[I

    .line 43
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "label"

    aput-object v1, v0, v2

    sput-object v0, Lcom/chase/sig/android/util/a;->e:[Ljava/lang/String;

    .line 47
    new-array v0, v3, [I

    const v1, 0x7f09010c

    aput v1, v0, v2

    sput-object v0, Lcom/chase/sig/android/util/a;->f:[I

    return-void

    .line 35
    :array_0
    .array-data 0x4
        0xct 0x1t 0x9t 0x7ft
        0xdt 0x1t 0x9t 0x7ft
    .end array-data

    .line 39
    :array_1
    .array-data 0x4
        0x19t 0x0t 0x9t 0x7ft
        0x18t 0x0t 0x9t 0x7ft
        0x1at 0x0t 0x9t 0x7ft
    .end array-data
.end method

.method public static a(Landroid/content/Context;Ljava/util/List;Ljava/lang/String;)Landroid/widget/ArrayAdapter;
    .locals 3
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/QuickDepositAccount;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Landroid/widget/ArrayAdapter",
            "<",
            "Lcom/chase/sig/android/domain/QuickDepositAccount$Location;",
            ">;"
        }
    .end annotation

    .prologue
    .line 96
    new-instance v0, Lcom/chase/sig/android/util/r;

    invoke-direct {v0}, Lcom/chase/sig/android/util/r;-><init>()V

    .line 97
    new-instance v0, Lcom/chase/sig/android/util/c;

    invoke-direct {v0, p2}, Lcom/chase/sig/android/util/c;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v0}, Lcom/chase/sig/android/util/r;->a(Ljava/util/List;Lcom/chase/sig/android/util/q;)I

    move-result v0

    .line 105
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/QuickDepositAccount;

    .line 107
    new-instance v1, Landroid/widget/ArrayAdapter;

    const v2, 0x7f0300ad

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickDepositAccount;->c()Ljava/util/List;

    move-result-object v0

    invoke-direct {v1, p0, v2, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    return-object v1
.end method

.method public static a(Landroid/content/Context;Ljava/util/ArrayList;)Landroid/widget/ListAdapter;
    .locals 6
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/chase/sig/android/service/quickpay/PayFromAccount;",
            ">;)",
            "Landroid/widget/ListAdapter;"
        }
    .end annotation

    .prologue
    .line 323
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 325
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/service/quickpay/PayFromAccount;

    .line 328
    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickpay/PayFromAccount;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 329
    const-string v1, ""

    .line 334
    :goto_1
    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickpay/PayFromAccount;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v1}, Lcom/chase/sig/android/util/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    .line 335
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 331
    :cond_0
    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickpay/PayFromAccount;->c()Lcom/chase/sig/android/util/Dollar;

    move-result-object v1

    if-nez v1, :cond_1

    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickpay/PayFromAccount;->c()Lcom/chase/sig/android/util/Dollar;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/util/Dollar;->h()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 337
    :cond_2
    new-instance v0, Landroid/widget/SimpleAdapter;

    const v3, 0x7f030052

    sget-object v4, Lcom/chase/sig/android/util/a;->a:[Ljava/lang/String;

    sget-object v5, Lcom/chase/sig/android/util/a;->c:[I

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Landroid/widget/SimpleAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/util/List;)Landroid/widget/SimpleAdapter;
    .locals 6
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/IAccount;",
            ">;)",
            "Landroid/widget/SimpleAdapter;"
        }
    .end annotation

    .prologue
    .line 53
    new-instance v0, Landroid/widget/SimpleAdapter;

    invoke-static {p0, p1}, Lcom/chase/sig/android/util/a;->h(Landroid/content/Context;Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    const v3, 0x7f030052

    sget-object v4, Lcom/chase/sig/android/util/a;->a:[Ljava/lang/String;

    sget-object v5, Lcom/chase/sig/android/util/a;->c:[I

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Landroid/widget/SimpleAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V

    return-object v0
.end method

.method private static a(Ljava/util/List;)Ljava/util/List;
    .locals 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/Payee;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 140
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 141
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/Payee;

    .line 142
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 143
    const-string v4, "nickname"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/Payee;->h()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 144
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 146
    :cond_0
    return-object v1
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/util/Map;
    .locals 5
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 213
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 214
    const-string v1, "nickname"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215
    if-nez p2, :cond_0

    .line 216
    const-string v1, "amount"

    const-string v2, "N/A"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 221
    :goto_0
    return-object v0

    .line 218
    :cond_0
    const-string v1, "amount"

    const v2, 0x7f07017b

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    invoke-static {p0, v2, v3}, Lcom/chase/sig/android/util/s;->a(Landroid/content/Context;I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;Ljava/util/List;)Landroid/widget/SimpleAdapter;
    .locals 6
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/QuickDepositAccount;",
            ">;)",
            "Landroid/widget/SimpleAdapter;"
        }
    .end annotation

    .prologue
    .line 60
    new-instance v0, Lcom/chase/sig/android/util/b;

    invoke-static {p0, p1}, Lcom/chase/sig/android/util/a;->i(Landroid/content/Context;Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    sget-object v3, Lcom/chase/sig/android/util/a;->b:[Ljava/lang/String;

    sget-object v4, Lcom/chase/sig/android/util/a;->d:[I

    move-object v1, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/chase/sig/android/util/b;-><init>(Landroid/content/Context;Ljava/util/List;[Ljava/lang/String;[ILjava/util/List;)V

    return-object v0
.end method

.method private static b(Ljava/util/List;)Ljava/util/List;
    .locals 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/LabeledValue;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 157
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 158
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/LabeledValue;

    .line 159
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 160
    const-string v4, "label"

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/LabeledValue;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 161
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 163
    :cond_0
    return-object v1
.end method

.method public static c(Landroid/content/Context;Ljava/util/List;)Landroid/widget/SimpleAdapter;
    .locals 6
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/IAccount;",
            ">;)",
            "Landroid/widget/SimpleAdapter;"
        }
    .end annotation

    .prologue
    .line 113
    new-instance v0, Landroid/widget/SimpleAdapter;

    invoke-static {p0, p1}, Lcom/chase/sig/android/util/a;->h(Landroid/content/Context;Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    const v3, 0x7f030052

    sget-object v4, Lcom/chase/sig/android/util/a;->a:[Ljava/lang/String;

    sget-object v5, Lcom/chase/sig/android/util/a;->c:[I

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Landroid/widget/SimpleAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V

    return-object v0
.end method

.method private static c(Ljava/util/List;)Ljava/util/List;
    .locals 7
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/IAccount;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 292
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v2

    .line 294
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 295
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/IAccount;

    .line 296
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 298
    invoke-virtual {v2}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v1

    iget-object v1, v1, Lcom/chase/sig/android/domain/o;->b:Lcom/chase/sig/android/domain/g;

    invoke-interface {v1, v0}, Lcom/chase/sig/android/domain/g;->a(Lcom/chase/sig/android/domain/IAccount;)Ljava/lang/String;

    move-result-object v1

    .line 299
    const-string v6, "nickname"

    invoke-interface {v5, v6, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 301
    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->e()Ljava/util/Hashtable;

    move-result-object v0

    const-string v1, "nextPaymentAmount"

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 304
    const-string v1, "N/A"

    .line 306
    invoke-static {v0}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 307
    new-instance v6, Lcom/chase/sig/android/util/Dollar;

    invoke-direct {v6, v0}, Lcom/chase/sig/android/util/Dollar;-><init>(Ljava/lang/String;)V

    .line 309
    invoke-virtual {v6}, Lcom/chase/sig/android/util/Dollar;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 310
    invoke-virtual {v6}, Lcom/chase/sig/android/util/Dollar;->h()Ljava/lang/String;

    move-result-object v0

    .line 314
    :goto_1
    const-string v1, "amount"

    invoke-interface {v5, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 315
    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 318
    :cond_0
    return-object v3

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method public static d(Landroid/content/Context;Ljava/util/List;)Landroid/widget/SimpleAdapter;
    .locals 6
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/IAccount;",
            ">;)",
            "Landroid/widget/SimpleAdapter;"
        }
    .end annotation

    .prologue
    .line 120
    new-instance v0, Landroid/widget/SimpleAdapter;

    invoke-static {p1}, Lcom/chase/sig/android/util/a;->c(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    const v3, 0x7f030052

    sget-object v4, Lcom/chase/sig/android/util/a;->a:[Ljava/lang/String;

    sget-object v5, Lcom/chase/sig/android/util/a;->c:[I

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Landroid/widget/SimpleAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V

    return-object v0
.end method

.method public static e(Landroid/content/Context;Ljava/util/List;)Landroid/widget/SimpleAdapter;
    .locals 6
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/IAccount;",
            ">;)",
            "Landroid/widget/SimpleAdapter;"
        }
    .end annotation

    .prologue
    .line 127
    new-instance v0, Landroid/widget/SimpleAdapter;

    invoke-static {p0, p1}, Lcom/chase/sig/android/util/a;->j(Landroid/content/Context;Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    const v3, 0x7f030052

    sget-object v4, Lcom/chase/sig/android/util/a;->a:[Ljava/lang/String;

    sget-object v5, Lcom/chase/sig/android/util/a;->c:[I

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Landroid/widget/SimpleAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V

    return-object v0
.end method

.method public static f(Landroid/content/Context;Ljava/util/List;)Landroid/widget/SimpleAdapter;
    .locals 6
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/Payee;",
            ">;)",
            "Landroid/widget/SimpleAdapter;"
        }
    .end annotation

    .prologue
    .line 134
    new-instance v0, Landroid/widget/SimpleAdapter;

    invoke-static {p1}, Lcom/chase/sig/android/util/a;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    const v3, 0x7f030051

    sget-object v4, Lcom/chase/sig/android/util/a;->a:[Ljava/lang/String;

    sget-object v5, Lcom/chase/sig/android/util/a;->c:[I

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Landroid/widget/SimpleAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V

    return-object v0
.end method

.method public static g(Landroid/content/Context;Ljava/util/List;)Landroid/widget/SimpleAdapter;
    .locals 6
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/LabeledValue;",
            ">;)",
            "Landroid/widget/SimpleAdapter;"
        }
    .end annotation

    .prologue
    .line 151
    new-instance v0, Landroid/widget/SimpleAdapter;

    invoke-static {p1}, Lcom/chase/sig/android/util/a;->b(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    const v3, 0x7f030051

    sget-object v4, Lcom/chase/sig/android/util/a;->e:[Ljava/lang/String;

    sget-object v5, Lcom/chase/sig/android/util/a;->f:[I

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Landroid/widget/SimpleAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V

    return-object v0
.end method

.method private static h(Landroid/content/Context;Ljava/util/List;)Ljava/util/List;
    .locals 5
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/IAccount;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 175
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 176
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/IAccount;

    .line 178
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/chase/sig/android/ChaseApplication;->i()Z

    move-result v3

    invoke-interface {v0, v3}, Lcom/chase/sig/android/domain/IAccount;->a(Z)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->j()Lcom/chase/sig/android/util/Dollar;

    move-result-object v4

    if-nez v4, :cond_0

    const/4 v0, 0x0

    :goto_1
    invoke-static {p0, v3, v0}, Lcom/chase/sig/android/util/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    .line 182
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 178
    :cond_0
    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->j()Lcom/chase/sig/android/util/Dollar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/util/Dollar;->h()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 184
    :cond_1
    return-object v1
.end method

.method private static i(Landroid/content/Context;Ljava/util/List;)Ljava/util/List;
    .locals 6
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/QuickDepositAccount;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 189
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 191
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/QuickDepositAccount;

    .line 194
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickDepositAccount;->e()Lcom/chase/sig/android/domain/QuickDepositAccount$Authorization;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/QuickDepositAccount$Authorization;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 195
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickDepositAccount;->f()Lcom/chase/sig/android/util/Dollar;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    .line 202
    :goto_1
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickDepositAccount;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4, v1}, Lcom/chase/sig/android/util/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v1

    .line 204
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "("

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickDepositAccount;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ")"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 205
    const-string v4, "mask"

    invoke-interface {v1, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 206
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 195
    :cond_0
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickDepositAccount;->f()Lcom/chase/sig/android/util/Dollar;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/util/Dollar;->h()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 199
    :cond_1
    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickDepositAccount;->e()Lcom/chase/sig/android/domain/QuickDepositAccount$Authorization;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/QuickDepositAccount$Authorization;->a()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 208
    :cond_2
    return-object v2
.end method

.method private static j(Landroid/content/Context;Ljava/util/List;)Ljava/util/List;
    .locals 8
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/IAccount;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 256
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 257
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/IAccount;

    .line 258
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 259
    const-string v1, "nickname"

    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v5

    invoke-virtual {v5}, Lcom/chase/sig/android/ChaseApplication;->i()Z

    move-result v5

    invoke-interface {v0, v5}, Lcom/chase/sig/android/domain/IAccount;->a(Z)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v1, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 262
    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->s()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 273
    :pswitch_0
    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->k()Lcom/chase/sig/android/util/Dollar;

    move-result-object v0

    move-object v1, v0

    .line 276
    :goto_1
    if-nez v1, :cond_1

    const-string v0, "N/A"

    .line 278
    :goto_2
    if-eqz v1, :cond_0

    .line 279
    new-instance v1, Lcom/chase/sig/android/util/Dollar;

    const-string v5, "0"

    invoke-direct {v1, v5}, Lcom/chase/sig/android/util/Dollar;-><init>(Ljava/lang/String;)V

    .line 282
    :cond_0
    const-string v1, "amount"

    const v5, 0x7f07017b

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v0, v6, v7

    invoke-static {p0, v5, v6}, Lcom/chase/sig/android/util/s;->a(Landroid/content/Context;I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 284
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 265
    :pswitch_1
    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->j()Lcom/chase/sig/android/util/Dollar;

    move-result-object v0

    move-object v1, v0

    .line 266
    goto :goto_1

    .line 270
    :pswitch_2
    invoke-interface {v0}, Lcom/chase/sig/android/domain/IAccount;->k()Lcom/chase/sig/android/util/Dollar;

    move-result-object v0

    move-object v1, v0

    .line 271
    goto :goto_1

    .line 276
    :cond_1
    invoke-virtual {v1}, Lcom/chase/sig/android/util/Dollar;->h()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 286
    :cond_2
    return-object v2

    .line 262
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
