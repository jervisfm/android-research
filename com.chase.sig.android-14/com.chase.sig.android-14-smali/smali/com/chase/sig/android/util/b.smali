.class final Lcom/chase/sig/android/util/b;
.super Landroid/widget/SimpleAdapter;
.source "SourceFile"


# instance fields
.field final synthetic a:Ljava/util/List;


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/util/List;[Ljava/lang/String;[ILjava/util/List;)V
    .locals 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 62
    iput-object p5, p0, Lcom/chase/sig/android/util/b;->a:Ljava/util/List;

    const v3, 0x7f030006

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Landroid/widget/SimpleAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V

    return-void
.end method


# virtual methods
.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .parameter
    .parameter
    .parameter

    .prologue
    .line 70
    invoke-super {p0, p1, p2, p3}, Landroid/widget/SimpleAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 72
    sget-object v0, Lcom/chase/sig/android/util/a;->d:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 73
    sget-object v1, Lcom/chase/sig/android/util/a;->d:[I

    const/4 v2, 0x1

    aget v1, v1, v2

    invoke-virtual {v4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 74
    sget-object v2, Lcom/chase/sig/android/util/a;->d:[I

    const/4 v3, 0x2

    aget v2, v2, v3

    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 76
    if-eqz v0, :cond_0

    .line 77
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/chase/sig/android/ChaseApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 78
    invoke-virtual {p0, p1}, Lcom/chase/sig/android/util/b;->isEnabled(I)Z

    move-result v3

    if-eqz v3, :cond_1

    const v3, 0x7f060005

    .line 81
    :goto_0
    invoke-virtual {v5, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    .line 83
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 84
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 85
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 88
    :cond_0
    return-object v4

    .line 78
    :cond_1
    const v3, 0x7f060007

    goto :goto_0
.end method

.method public final isEnabled(I)Z
    .locals 1
    .parameter

    .prologue
    .line 65
    iget-object v0, p0, Lcom/chase/sig/android/util/b;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/QuickDepositAccount;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickDepositAccount;->e()Lcom/chase/sig/android/domain/QuickDepositAccount$Authorization;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/QuickDepositAccount$Authorization;->b()Z

    move-result v0

    return v0
.end method
