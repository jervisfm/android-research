.class public final Lcom/chase/sig/android/util/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field a:Z

.field b:Ljava/lang/CharSequence;

.field c:Ljava/util/regex/Matcher;

.field final d:D


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/chase/sig/android/util/d;->a:Z

    .line 11
    iput-object v1, p0, Lcom/chase/sig/android/util/d;->b:Ljava/lang/CharSequence;

    .line 12
    iput-object v1, p0, Lcom/chase/sig/android/util/d;->c:Ljava/util/regex/Matcher;

    .line 13
    const-wide v0, 0x416312cfffae147bL

    iput-wide v0, p0, Lcom/chase/sig/android/util/d;->d:D

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 2
    .parameter

    .prologue
    .line 17
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 27
    :cond_0
    :goto_0
    return-void

    .line 19
    :cond_1
    iget-boolean v0, p0, Lcom/chase/sig/android/util/d;->a:Z

    if-nez v0, :cond_0

    .line 20
    const-string v0, "\\d{0,7}[.]\\d{3}"

    invoke-static {v0, p1}, Ljava/util/regex/Pattern;->matches(Ljava/lang/String;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 21
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-interface {p1, v0, v1}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    goto :goto_0

    .line 22
    :cond_2
    const-string v0, "[0-9]{10}+"

    invoke-static {v0, p1}, Ljava/util/regex/Pattern;->matches(Ljava/lang/String;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 23
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x3

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-interface {p1, v0, v1}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    goto :goto_0
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 33
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 4
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x1

    .line 38
    const-string v0, "\\d{1,7}"

    invoke-static {v0, p1}, Ljava/util/regex/Pattern;->matches(Ljava/lang/String;Ljava/lang/CharSequence;)Z

    move-result v0

    if-ne v0, v3, :cond_1

    .line 39
    const-string v0, "\\d{1,7}"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 40
    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/util/d;->c:Ljava/util/regex/Matcher;

    .line 43
    const-string v0, "[.]?\\d{0}"

    iget-object v1, p0, Lcom/chase/sig/android/util/d;->c:Ljava/util/regex/Matcher;

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->regionEnd()I

    move-result v1

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    invoke-interface {p1, v1, v2}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->matches(Ljava/lang/String;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 44
    iput-boolean v3, p0, Lcom/chase/sig/android/util/d;->a:Z

    .line 53
    :cond_0
    :goto_0
    return-void

    .line 47
    :cond_1
    const-string v0, "\\d{1,7}[.]\\d{0,2}"

    invoke-static {v0, p1}, Ljava/util/regex/Pattern;->matches(Ljava/lang/String;Ljava/lang/CharSequence;)Z

    move-result v0

    if-ne v0, v3, :cond_2

    .line 48
    iput-boolean v3, p0, Lcom/chase/sig/android/util/d;->a:Z

    goto :goto_0

    .line 50
    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/chase/sig/android/util/d;->a:Z

    goto :goto_0
.end method
