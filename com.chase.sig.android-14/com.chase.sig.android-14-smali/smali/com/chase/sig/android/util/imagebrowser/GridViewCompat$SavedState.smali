.class Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$SavedState;
.super Landroid/view/View$BaseSavedState;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "SavedState"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$SavedState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field a:I

.field b:Landroid/util/SparseBooleanArray;

.field c:Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 683
    new-instance v0, Lcom/chase/sig/android/util/imagebrowser/b;

    invoke-direct {v0}, Lcom/chase/sig/android/util/imagebrowser/b;-><init>()V

    sput-object v0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$SavedState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 6
    .parameter

    .prologue
    .line 650
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcel;)V

    .line 651
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$SavedState;->a:I

    .line 652
    invoke-virtual {p1}, Landroid/os/Parcel;->readSparseBooleanArray()Landroid/util/SparseBooleanArray;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$SavedState;->b:Landroid/util/SparseBooleanArray;

    .line 653
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 654
    if-lez v1, :cond_0

    .line 655
    new-instance v0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;

    invoke-direct {v0}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;-><init>()V

    iput-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$SavedState;->c:Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;

    .line 656
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 657
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    .line 658
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 659
    iget-object v5, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$SavedState;->c:Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v5, v2, v3, v4}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->a(JLjava/lang/Object;)V

    .line 656
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 662
    :cond_0
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 634
    invoke-direct {p0, p1}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$SavedState;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method constructor <init>(Landroid/os/Parcelable;)V
    .locals 0
    .parameter

    .prologue
    .line 643
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcelable;)V

    .line 644
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 679
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "AbsListView.SavedState{"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " checkState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$SavedState;->b:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 5
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 666
    invoke-super {p0, p1, p2}, Landroid/view/View$BaseSavedState;->writeToParcel(Landroid/os/Parcel;I)V

    .line 667
    iget v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$SavedState;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 668
    iget-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$SavedState;->b:Landroid/util/SparseBooleanArray;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSparseBooleanArray(Landroid/util/SparseBooleanArray;)V

    .line 669
    iget-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$SavedState;->c:Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$SavedState;->c:Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;

    invoke-virtual {v0}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->a()I

    move-result v0

    move v1, v0

    .line 670
    :goto_0
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 671
    :goto_1
    if-ge v2, v1, :cond_1

    .line 672
    iget-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$SavedState;->c:Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->a(I)J

    move-result-wide v3

    invoke-virtual {p1, v3, v4}, Landroid/os/Parcel;->writeLong(J)V

    .line 673
    iget-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$SavedState;->c:Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 671
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_0
    move v1, v2

    .line 669
    goto :goto_0

    .line 675
    :cond_1
    return-void
.end method
