.class final Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Object;


# instance fields
.field private b:Z

.field private c:[J

.field private d:[Ljava/lang/Object;

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 148
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->a:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 155
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;-><init>(B)V

    .line 156
    return-void
.end method

.method private constructor <init>(B)V
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 163
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 149
    iput-boolean v2, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->b:Z

    .line 164
    const/16 v0, 0xa

    invoke-static {v0}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$a;->a(I)I

    move-result v0

    .line 166
    new-array v1, v0, [J

    iput-object v1, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->c:[J

    .line 167
    new-array v0, v0, [Ljava/lang/Object;

    iput-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->d:[Ljava/lang/Object;

    .line 168
    iput v2, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->e:I

    .line 169
    return-void
.end method

.method private static a([JIJ)I
    .locals 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 452
    add-int/lit8 v2, p1, 0x0

    const/4 v0, -0x1

    move v1, v0

    move v0, v2

    .line 454
    :goto_0
    sub-int v2, v0, v1

    const/4 v3, 0x1

    if-le v2, v3, :cond_1

    .line 455
    add-int v2, v0, v1

    div-int/lit8 v2, v2, 0x2

    .line 457
    aget-wide v3, p0, v2

    cmp-long v3, v3, p2

    if-gez v3, :cond_0

    move v1, v2

    .line 458
    goto :goto_0

    :cond_0
    move v0, v2

    .line 460
    goto :goto_0

    .line 464
    :cond_1
    add-int/lit8 v1, p1, 0x0

    if-ne v0, v1, :cond_3

    .line 465
    add-int/lit8 v0, p1, 0x0

    xor-int/lit8 v0, v0, -0x1

    .line 469
    :cond_2
    :goto_1
    return v0

    .line 466
    :cond_3
    aget-wide v1, p0, v0

    cmp-long v1, v1, p2

    if-eqz v1, :cond_2

    .line 469
    xor-int/lit8 v0, v0, -0x1

    goto :goto_1
.end method

.method private c()V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 241
    iget v3, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->e:I

    .line 243
    iget-object v4, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->c:[J

    .line 244
    iget-object v5, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->d:[Ljava/lang/Object;

    move v1, v2

    move v0, v2

    .line 246
    :goto_0
    if-ge v1, v3, :cond_2

    .line 247
    aget-object v6, v5, v1

    .line 249
    sget-object v7, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->a:Ljava/lang/Object;

    if-eq v6, v7, :cond_1

    .line 250
    if-eq v1, v0, :cond_0

    .line 251
    aget-wide v7, v4, v1

    aput-wide v7, v4, v0

    .line 252
    aput-object v6, v5, v0

    .line 255
    :cond_0
    add-int/lit8 v0, v0, 0x1

    .line 246
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 259
    :cond_2
    iput-boolean v2, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->b:Z

    .line 260
    iput v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->e:I

    .line 263
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 323
    iget-boolean v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->b:Z

    if-eqz v0, :cond_0

    .line 324
    invoke-direct {p0}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->c()V

    .line 327
    :cond_0
    iget v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->e:I

    return v0
.end method

.method public final a(I)J
    .locals 2
    .parameter

    .prologue
    .line 336
    iget-boolean v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->b:Z

    if-eqz v0, :cond_0

    .line 337
    invoke-direct {p0}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->c()V

    .line 340
    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->c:[J

    aget-wide v0, v0, p1

    return-wide v0
.end method

.method public final a(J)V
    .locals 3
    .parameter

    .prologue
    .line 221
    iget-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->c:[J

    iget v1, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->e:I

    invoke-static {v0, v1, p1, p2}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->a([JIJ)I

    move-result v0

    .line 223
    if-ltz v0, :cond_0

    .line 224
    iget-object v1, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->d:[Ljava/lang/Object;

    aget-object v1, v1, v0

    sget-object v2, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->a:Ljava/lang/Object;

    if-eq v1, v2, :cond_0

    .line 225
    iget-object v1, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->d:[Ljava/lang/Object;

    sget-object v2, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->a:Ljava/lang/Object;

    aput-object v2, v1, v0

    .line 226
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->b:Z

    .line 229
    :cond_0
    return-void
.end method

.method public final a(JLjava/lang/Object;)V
    .locals 6
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JTE;)V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 271
    iget-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->c:[J

    iget v1, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->e:I

    invoke-static {v0, v1, p1, p2}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->a([JIJ)I

    move-result v0

    .line 273
    if-ltz v0, :cond_0

    .line 274
    iget-object v1, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->d:[Ljava/lang/Object;

    aput-object p3, v1, v0

    .line 316
    :goto_0
    return-void

    .line 276
    :cond_0
    xor-int/lit8 v0, v0, -0x1

    .line 278
    iget v1, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->e:I

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->d:[Ljava/lang/Object;

    aget-object v1, v1, v0

    sget-object v2, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->a:Ljava/lang/Object;

    if-ne v1, v2, :cond_1

    .line 279
    iget-object v1, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->c:[J

    aput-wide p1, v1, v0

    .line 280
    iget-object v1, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->d:[Ljava/lang/Object;

    aput-object p3, v1, v0

    goto :goto_0

    .line 284
    :cond_1
    iget-boolean v1, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->b:Z

    if-eqz v1, :cond_2

    iget v1, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->e:I

    iget-object v2, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->c:[J

    array-length v2, v2

    if-lt v1, v2, :cond_2

    .line 285
    invoke-direct {p0}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->c()V

    .line 288
    iget-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->c:[J

    iget v1, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->e:I

    invoke-static {v0, v1, p1, p2}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->a([JIJ)I

    move-result v0

    xor-int/lit8 v0, v0, -0x1

    .line 291
    :cond_2
    iget v1, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->e:I

    iget-object v2, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->c:[J

    array-length v2, v2

    if-lt v1, v2, :cond_3

    .line 292
    iget v1, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->e:I

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$a;->a(I)I

    move-result v1

    .line 294
    new-array v2, v1, [J

    .line 295
    new-array v1, v1, [Ljava/lang/Object;

    .line 299
    iget-object v3, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->c:[J

    iget-object v4, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->c:[J

    array-length v4, v4

    invoke-static {v3, v5, v2, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 300
    iget-object v3, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->d:[Ljava/lang/Object;

    iget-object v4, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->d:[Ljava/lang/Object;

    array-length v4, v4

    invoke-static {v3, v5, v1, v5, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 302
    iput-object v2, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->c:[J

    .line 303
    iput-object v1, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->d:[Ljava/lang/Object;

    .line 306
    :cond_3
    iget v1, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->e:I

    sub-int/2addr v1, v0

    if-eqz v1, :cond_4

    .line 308
    iget-object v1, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->c:[J

    iget-object v2, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->c:[J

    add-int/lit8 v3, v0, 0x1

    iget v4, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->e:I

    sub-int/2addr v4, v0

    invoke-static {v1, v0, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 309
    iget-object v1, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->d:[Ljava/lang/Object;

    iget-object v2, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->d:[Ljava/lang/Object;

    add-int/lit8 v3, v0, 0x1

    iget v4, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->e:I

    sub-int/2addr v4, v0

    invoke-static {v1, v0, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 312
    :cond_4
    iget-object v1, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->c:[J

    aput-wide p1, v1, v0

    .line 313
    iget-object v1, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->d:[Ljava/lang/Object;

    aput-object p3, v1, v0

    .line 314
    iget v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->e:I

    goto/16 :goto_0
.end method

.method public final b(I)Ljava/lang/Object;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TE;"
        }
    .end annotation

    .prologue
    .line 349
    iget-boolean v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->b:Z

    if-eqz v0, :cond_0

    .line 350
    invoke-direct {p0}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->c()V

    .line 353
    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->d:[Ljava/lang/Object;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public final b()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 406
    iget v2, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->e:I

    .line 407
    iget-object v3, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->d:[Ljava/lang/Object;

    move v0, v1

    .line 409
    :goto_0
    if-ge v0, v2, :cond_0

    .line 410
    const/4 v4, 0x0

    aput-object v4, v3, v0

    .line 409
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 413
    :cond_0
    iput v1, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->e:I

    .line 414
    iput-boolean v1, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->b:Z

    .line 415
    return-void
.end method
