.class public Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;
.super Landroid/widget/GridView;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$SavedState;,
        Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$a;,
        Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;
    }
.end annotation


# static fields
.field private static final e:Ljava/lang/String;

.field private static f:Z

.field private static g:Ljava/lang/reflect/Method;

.field private static h:Ljava/lang/reflect/Method;

.field private static i:Ljava/lang/reflect/Method;

.field private static j:Ljava/lang/reflect/Method;

.field private static k:Ljava/lang/reflect/Method;

.field private static l:Ljava/lang/reflect/Method;

.field private static m:Ljava/lang/reflect/Method;

.field private static n:Ljava/lang/reflect/Method;

.field private static o:Ljava/lang/reflect/Method;


# instance fields
.field a:I

.field b:Landroid/util/SparseBooleanArray;

.field c:Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field d:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    const/4 v5, 0x0

    .line 60
    const-class v0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->e:Ljava/lang/String;

    .line 83
    sput-boolean v1, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->f:Z

    .line 96
    const/4 v0, 0x0

    :try_start_0
    sput-boolean v0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->f:Z

    .line 98
    const-class v0, Landroid/widget/GridView;

    const-string v1, "getChoiceMode"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->n:Ljava/lang/reflect/Method;

    .line 99
    const-class v0, Landroid/widget/GridView;

    const-string v1, "getCheckedItemIds"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->g:Ljava/lang/reflect/Method;

    .line 101
    const-class v0, Landroid/widget/GridView;

    const-string v1, "isItemChecked"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->h:Ljava/lang/reflect/Method;

    .line 104
    const-class v0, Landroid/widget/GridView;

    const-string v1, "getCheckedItemPosition"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->i:Ljava/lang/reflect/Method;

    .line 106
    const-class v0, Landroid/widget/GridView;

    const-string v1, "getCheckedItemPositions"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->j:Ljava/lang/reflect/Method;

    .line 108
    const-class v0, Landroid/widget/GridView;

    const-string v1, "clearChoices"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->k:Ljava/lang/reflect/Method;

    .line 109
    const-class v0, Landroid/widget/GridView;

    const-string v1, "setItemChecked"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    sget-object v4, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->l:Ljava/lang/reflect/Method;

    .line 112
    const-class v0, Landroid/widget/GridView;

    const-string v1, "setChoiceMode"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->m:Ljava/lang/reflect/Method;

    .line 115
    const-class v0, Landroid/widget/GridView;

    const-string v1, "getCheckedItemCount"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->o:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    .line 133
    :goto_0
    return-void

    .line 118
    :catch_0
    move-exception v0

    .line 119
    sget-object v1, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->e:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Running in compatibility mode as \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\' not found"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    sput-boolean v6, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->f:Z

    .line 122
    sput-object v5, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->g:Ljava/lang/reflect/Method;

    .line 123
    sput-object v5, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->h:Ljava/lang/reflect/Method;

    .line 124
    sput-object v5, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->i:Ljava/lang/reflect/Method;

    .line 125
    sput-object v5, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->j:Ljava/lang/reflect/Method;

    .line 126
    sput-object v5, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->k:Ljava/lang/reflect/Method;

    .line 127
    sput-object v5, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->l:Ljava/lang/reflect/Method;

    .line 128
    sput-object v5, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->m:Ljava/lang/reflect/Method;

    .line 129
    sput-object v5, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->n:Ljava/lang/reflect/Method;

    .line 130
    sput-object v5, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->o:Ljava/lang/reflect/Method;

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .parameter

    .prologue
    .line 706
    invoke-direct {p0, p1}, Landroid/widget/GridView;-><init>(Landroid/content/Context;)V

    .line 78
    const/4 v0, 0x2

    iput v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->d:I

    .line 707
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 696
    invoke-direct {p0, p1, p2}, Landroid/widget/GridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 78
    const/4 v0, 0x2

    iput v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->d:I

    .line 697
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 701
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/GridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 78
    const/4 v0, 0x2

    iput v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->d:I

    .line 702
    return-void
.end method


# virtual methods
.method public final a(IZ)V
    .locals 5
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 991
    sget-boolean v0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->f:Z

    if-nez v0, :cond_1

    .line 993
    :try_start_0
    sget-object v0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->l:Ljava/lang/reflect/Method;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1056
    :cond_0
    :goto_0
    return-void

    .line 1007
    :cond_1
    iget v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->d:I

    if-eqz v0, :cond_0

    .line 1011
    iget v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->d:I

    if-ne v0, v3, :cond_6

    .line 1012
    iget-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->b:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v0

    .line 1013
    iget-object v1, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->b:Landroid/util/SparseBooleanArray;

    invoke-virtual {v1, p1, p2}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 1014
    iget-object v1, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->c:Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    invoke-interface {v1}, Landroid/widget/ListAdapter;->hasStableIds()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1015
    if-eqz p2, :cond_4

    .line 1016
    iget-object v1, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->c:Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;

    invoke-virtual {p0}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v2

    invoke-interface {v2, p1}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->a(JLjava/lang/Object;)V

    .line 1021
    :cond_2
    :goto_1
    if-eq v0, p2, :cond_3

    .line 1022
    if-eqz p2, :cond_5

    .line 1023
    iget v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->a:I

    .line 1055
    :cond_3
    :goto_2
    invoke-virtual {p0}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->invalidateViews()V

    goto :goto_0

    .line 1018
    :cond_4
    iget-object v1, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->c:Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;

    invoke-virtual {p0}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v2

    invoke-interface {v2, p1}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->a(J)V

    goto :goto_1

    .line 1025
    :cond_5
    iget v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->a:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->a:I

    goto :goto_2

    .line 1029
    :cond_6
    iget-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->c:Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;

    if-eqz v0, :cond_a

    invoke-virtual {p0}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/ListAdapter;->hasStableIds()Z

    move-result v0

    if-eqz v0, :cond_a

    move v0, v1

    .line 1033
    :goto_3
    if-nez p2, :cond_7

    invoke-virtual {p0, p1}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->a(I)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 1034
    :cond_7
    iget-object v3, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->b:Landroid/util/SparseBooleanArray;

    invoke-virtual {v3}, Landroid/util/SparseBooleanArray;->clear()V

    .line 1035
    if-eqz v0, :cond_8

    .line 1036
    iget-object v3, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->c:Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;

    invoke-virtual {v3}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->b()V

    .line 1042
    :cond_8
    if-eqz p2, :cond_b

    .line 1043
    iget-object v2, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->b:Landroid/util/SparseBooleanArray;

    invoke-virtual {v2, p1, v1}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 1044
    if-eqz v0, :cond_9

    .line 1045
    iget-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->c:Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;

    invoke-virtual {p0}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v2

    invoke-interface {v2, p1}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v4}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->a(JLjava/lang/Object;)V

    .line 1047
    :cond_9
    iput v1, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->a:I

    goto :goto_2

    :cond_a
    move v0, v2

    .line 1029
    goto :goto_3

    .line 1048
    :cond_b
    iget-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->b:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->size()I

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->b:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0, v2}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1049
    :cond_c
    iput v2, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->a:I

    goto :goto_2

    .line 1000
    :catch_0
    move-exception v0

    goto/16 :goto_0

    .line 998
    :catch_1
    move-exception v0

    goto/16 :goto_0

    .line 996
    :catch_2
    move-exception v0

    goto/16 :goto_0
.end method

.method public final a(I)Z
    .locals 5
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 810
    sget-boolean v0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->f:Z

    if-nez v0, :cond_0

    .line 812
    :try_start_0
    sget-object v0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->h:Ljava/lang/reflect/Method;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, p0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 827
    :goto_0
    return v0

    :catch_0
    move-exception v0

    .line 818
    :cond_0
    :goto_1
    iget v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->d:I

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->b:Landroid/util/SparseBooleanArray;

    if-eqz v0, :cond_1

    .line 824
    iget-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->b:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 827
    goto :goto_0

    .line 816
    :catch_1
    move-exception v0

    goto :goto_1

    .line 814
    :catch_2
    move-exception v0

    goto :goto_1
.end method

.method public getCheckedItemCountC()I
    .locals 2

    .prologue
    .line 1280
    sget-boolean v0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->f:Z

    if-nez v0, :cond_0

    .line 1282
    :try_start_0
    sget-object v0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->o:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 1288
    :goto_0
    return v0

    :catch_0
    move-exception v0

    :cond_0
    :goto_1
    iget v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->a:I

    goto :goto_0

    .line 1286
    :catch_1
    move-exception v0

    goto :goto_1

    .line 1284
    :catch_2
    move-exception v0

    goto :goto_1
.end method

.method public getCheckedItemIdsC()[J
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 774
    sget-boolean v0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->f:Z

    if-nez v0, :cond_1

    .line 776
    :try_start_0
    sget-object v0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->g:Ljava/lang/reflect/Method;

    const/4 v2, 0x0

    invoke-virtual {v0, p0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [J

    check-cast v0, [J
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0

    .line 800
    :cond_0
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    .line 782
    :cond_1
    :goto_1
    iget v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->d:I

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->c:Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    if-nez v0, :cond_3

    .line 789
    :cond_2
    new-array v0, v1, [J

    goto :goto_0

    .line 792
    :cond_3
    iget-object v2, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->c:Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;

    .line 793
    invoke-virtual {v2}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->a()I

    move-result v3

    .line 794
    new-array v0, v3, [J

    .line 796
    :goto_2
    if-ge v1, v3, :cond_0

    .line 797
    invoke-virtual {v2, v1}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->a(I)J

    move-result-wide v4

    aput-wide v4, v0, v1

    .line 796
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 780
    :catch_1
    move-exception v0

    goto :goto_1

    .line 778
    :catch_2
    move-exception v0

    goto :goto_1
.end method

.method public getCheckedItemPositionC()I
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 836
    sget-boolean v0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->f:Z

    if-nez v0, :cond_0

    .line 838
    :try_start_0
    sget-object v0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->i:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 854
    :goto_0
    return v0

    :catch_0
    move-exception v0

    .line 844
    :cond_0
    :goto_1
    iget v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->d:I

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->b:Landroid/util/SparseBooleanArray;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->b:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->size()I

    move-result v0

    if-ne v0, v2, :cond_1

    .line 851
    iget-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->b:Landroid/util/SparseBooleanArray;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v0

    goto :goto_0

    .line 854
    :cond_1
    const/4 v0, -0x1

    goto :goto_0

    .line 842
    :catch_1
    move-exception v0

    goto :goto_1

    .line 840
    :catch_2
    move-exception v0

    goto :goto_1
.end method

.method public getCheckedItemPositionsC()Landroid/util/SparseBooleanArray;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 863
    sget-boolean v0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->f:Z

    if-nez v0, :cond_0

    .line 865
    :try_start_0
    sget-object v0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->j:Ljava/lang/reflect/Method;

    const/4 v2, 0x0

    invoke-virtual {v0, p0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/SparseBooleanArray;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0

    .line 880
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    .line 872
    :cond_0
    :goto_1
    iget v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->d:I

    if-eqz v0, :cond_1

    .line 878
    iget-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->b:Landroid/util/SparseBooleanArray;

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 880
    goto :goto_0

    .line 870
    :catch_1
    move-exception v0

    goto :goto_1

    .line 868
    :catch_2
    move-exception v0

    goto :goto_1
.end method

.method public getChoiceModeC()I
    .locals 2

    .prologue
    .line 724
    sget-boolean v0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->f:Z

    if-nez v0, :cond_0

    sget-object v0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->n:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_0

    .line 726
    :try_start_0
    sget-object v0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->n:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 732
    :goto_0
    return v0

    :catch_0
    move-exception v0

    :cond_0
    :goto_1
    iget v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->d:I

    goto :goto_0

    .line 730
    :catch_1
    move-exception v0

    goto :goto_1

    .line 728
    :catch_2
    move-exception v0

    goto :goto_1
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1
    .parameter

    .prologue
    .line 1251
    sget-boolean v0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->f:Z

    if-nez v0, :cond_0

    .line 1252
    invoke-super {p0, p1}, Landroid/widget/GridView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 1272
    :goto_0
    return-void

    .line 1255
    :cond_0
    check-cast p1, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$SavedState;

    .line 1257
    invoke-virtual {p1}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/GridView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 1259
    iget-object v0, p1, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$SavedState;->b:Landroid/util/SparseBooleanArray;

    if-eqz v0, :cond_1

    .line 1260
    iget-object v0, p1, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$SavedState;->b:Landroid/util/SparseBooleanArray;

    iput-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->b:Landroid/util/SparseBooleanArray;

    .line 1263
    :cond_1
    iget-object v0, p1, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$SavedState;->c:Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;

    if-eqz v0, :cond_2

    .line 1264
    iget-object v0, p1, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$SavedState;->c:Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;

    iput-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->c:Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;

    .line 1267
    :cond_2
    iget v0, p1, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$SavedState;->a:I

    iput v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->a:I

    .line 1271
    invoke-virtual {p0}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->invalidateViews()V

    goto :goto_0
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1223
    sget-boolean v0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->f:Z

    if-nez v0, :cond_0

    .line 1224
    invoke-super {p0}, Landroid/widget/GridView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 1246
    :goto_0
    return-object v0

    .line 1228
    :cond_0
    invoke-super {p0}, Landroid/widget/GridView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    .line 1230
    new-instance v0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$SavedState;

    invoke-direct {v0, v2}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 1232
    iget-object v2, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->b:Landroid/util/SparseBooleanArray;

    if-eqz v2, :cond_2

    .line 1233
    iget-object v3, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->b:Landroid/util/SparseBooleanArray;

    new-instance v4, Landroid/util/SparseBooleanArray;

    invoke-direct {v4}, Landroid/util/SparseBooleanArray;-><init>()V

    invoke-virtual {v3}, Landroid/util/SparseBooleanArray;->size()I

    move-result v5

    move v2, v1

    :goto_1
    if-ge v2, v5, :cond_1

    invoke-virtual {v3, v2}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v6

    invoke-virtual {v3, v6}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v7

    invoke-virtual {v4, v6, v7}, Landroid/util/SparseBooleanArray;->put(IZ)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    iput-object v4, v0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$SavedState;->b:Landroid/util/SparseBooleanArray;

    .line 1236
    :cond_2
    iget-object v2, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->c:Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;

    if-eqz v2, :cond_4

    .line 1237
    new-instance v2, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;

    invoke-direct {v2}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;-><init>()V

    .line 1238
    iget-object v3, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->c:Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;

    invoke-virtual {v3}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->a()I

    move-result v3

    .line 1239
    :goto_2
    if-ge v1, v3, :cond_3

    .line 1240
    iget-object v4, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->c:Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;

    invoke-virtual {v4, v1}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->a(I)J

    move-result-wide v4

    iget-object v6, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->c:Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;

    invoke-virtual {v6, v1}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->b(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v2, v4, v5, v6}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->a(JLjava/lang/Object;)V

    .line 1239
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1242
    :cond_3
    iput-object v2, v0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$SavedState;->c:Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;

    .line 1244
    :cond_4
    iget v1, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->a:I

    iput v1, v0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$SavedState;->a:I

    goto :goto_0
.end method

.method public performItemClick(Landroid/view/View;IJ)Z
    .locals 6
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1127
    sget-boolean v2, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->f:Z

    if-nez v2, :cond_0

    .line 1128
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/GridView;->performItemClick(Landroid/view/View;IJ)Z

    move-result v0

    .line 1176
    :goto_0
    return v0

    .line 1132
    :cond_0
    iget v2, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->d:I

    if-eqz v2, :cond_b

    .line 1137
    iget v2, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->d:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    .line 1138
    iget-object v2, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->b:Landroid/util/SparseBooleanArray;

    invoke-virtual {v2, p2, v0}, Landroid/util/SparseBooleanArray;->get(IZ)Z

    move-result v2

    if-nez v2, :cond_1

    move v0, v1

    .line 1139
    :cond_1
    iget-object v2, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->b:Landroid/util/SparseBooleanArray;

    invoke-virtual {v2, p2, v0}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 1140
    iget-object v2, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->c:Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v2

    invoke-interface {v2}, Landroid/widget/ListAdapter;->hasStableIds()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1141
    if-eqz v0, :cond_4

    .line 1142
    iget-object v2, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->c:Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;

    invoke-virtual {p0}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v3

    invoke-interface {v3, p2}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v5}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->a(JLjava/lang/Object;)V

    .line 1147
    :cond_2
    :goto_1
    if-eqz v0, :cond_5

    .line 1148
    iget v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->a:I

    .line 1169
    :cond_3
    :goto_2
    invoke-virtual {p0}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->invalidateViews()V

    .line 1172
    :goto_3
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/GridView;->performItemClick(Landroid/view/View;IJ)Z

    move-result v0

    or-int/2addr v0, v1

    .line 1176
    goto :goto_0

    .line 1144
    :cond_4
    iget-object v2, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->c:Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;

    invoke-virtual {p0}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v3

    invoke-interface {v3, p2}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->a(J)V

    goto :goto_1

    .line 1150
    :cond_5
    iget v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->a:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->a:I

    goto :goto_2

    .line 1152
    :cond_6
    iget v2, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->d:I

    if-ne v2, v1, :cond_3

    .line 1153
    iget-object v2, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->b:Landroid/util/SparseBooleanArray;

    invoke-virtual {v2, p2, v0}, Landroid/util/SparseBooleanArray;->get(IZ)Z

    move-result v2

    if-nez v2, :cond_8

    move v2, v1

    .line 1154
    :goto_4
    if-eqz v2, :cond_9

    .line 1155
    iget-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->b:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->clear()V

    .line 1156
    iget-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->b:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0, p2, v1}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 1157
    iget-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->c:Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;

    if-eqz v0, :cond_7

    invoke-virtual {p0}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/ListAdapter;->hasStableIds()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1158
    iget-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->c:Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;

    invoke-virtual {v0}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->b()V

    .line 1159
    iget-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->c:Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;

    invoke-virtual {p0}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v2

    invoke-interface {v2, p2}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v4}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->a(JLjava/lang/Object;)V

    .line 1161
    :cond_7
    iput v1, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->a:I

    goto :goto_2

    :cond_8
    move v2, v0

    .line 1153
    goto :goto_4

    .line 1162
    :cond_9
    iget-object v2, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->b:Landroid/util/SparseBooleanArray;

    invoke-virtual {v2}, Landroid/util/SparseBooleanArray;->size()I

    move-result v2

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->b:Landroid/util/SparseBooleanArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    move-result v2

    if-nez v2, :cond_3

    .line 1163
    :cond_a
    iput v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->a:I

    goto :goto_2

    :cond_b
    move v1, v0

    goto :goto_3
.end method

.method public bridge synthetic setAdapter(Landroid/widget/Adapter;)V
    .locals 0
    .parameter

    .prologue
    .line 56
    check-cast p1, Landroid/widget/ListAdapter;

    invoke-virtual {p0, p1}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 1
    .parameter

    .prologue
    .line 744
    sget-boolean v0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->f:Z

    if-nez v0, :cond_1

    .line 745
    invoke-super {p0, p1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 766
    :cond_0
    :goto_0
    return-void

    .line 750
    :cond_1
    invoke-super {p0, p1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 751
    if-eqz p1, :cond_2

    .line 752
    iget v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->d:I

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/ListAdapter;->hasStableIds()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->c:Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;

    if-nez v0, :cond_2

    .line 754
    new-instance v0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;

    invoke-direct {v0}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;-><init>()V

    iput-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->c:Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;

    .line 758
    :cond_2
    iget-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->b:Landroid/util/SparseBooleanArray;

    if-eqz v0, :cond_3

    .line 759
    iget-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->b:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->clear()V

    .line 762
    :cond_3
    iget-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->c:Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;

    if-eqz v0, :cond_0

    .line 763
    iget-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->c:Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;

    invoke-virtual {v0}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;->b()V

    goto :goto_0
.end method

.method public setChoiceModeC(I)V
    .locals 4
    .parameter

    .prologue
    .line 1185
    sget-boolean v0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->f:Z

    if-nez v0, :cond_1

    .line 1187
    :try_start_0
    sget-object v0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->m:Ljava/lang/reflect/Method;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1208
    :cond_0
    :goto_0
    return-void

    .line 1199
    :cond_1
    iput p1, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->d:I

    .line 1200
    iget v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->d:I

    if-eqz v0, :cond_0

    .line 1201
    iget-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->b:Landroid/util/SparseBooleanArray;

    if-nez v0, :cond_2

    .line 1202
    new-instance v0, Landroid/util/SparseBooleanArray;

    invoke-direct {v0}, Landroid/util/SparseBooleanArray;-><init>()V

    iput-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->b:Landroid/util/SparseBooleanArray;

    .line 1204
    :cond_2
    iget-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->c:Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/ListAdapter;->hasStableIds()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1205
    new-instance v0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;

    invoke-direct {v0}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;-><init>()V

    iput-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->c:Lcom/chase/sig/android/util/imagebrowser/GridViewCompat$b;

    goto :goto_0

    .line 1193
    :catch_0
    move-exception v0

    goto :goto_0

    .line 1191
    :catch_1
    move-exception v0

    goto :goto_0

    .line 1189
    :catch_2
    move-exception v0

    goto :goto_0
.end method
