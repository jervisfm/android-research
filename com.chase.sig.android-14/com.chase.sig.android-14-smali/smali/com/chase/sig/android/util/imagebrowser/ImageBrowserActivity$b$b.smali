.class final Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$b$b;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Long;",
        "Ljava/lang/Void;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field a:Landroid/content/Context;

.field b:Landroid/widget/ImageView;

.field c:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Landroid/view/View;",
            "Landroid/os/AsyncTask",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Void;",
            "Landroid/graphics/Bitmap;",
            ">;>;"
        }
    .end annotation
.end field

.field d:Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$c",
            "<",
            "Ljava/lang/Long;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field e:Ljava/lang/Long;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/ImageView;Ljava/util/concurrent/ConcurrentHashMap;Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$c;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/widget/ImageView;",
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Landroid/view/View;",
            "Landroid/os/AsyncTask",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Void;",
            "Landroid/graphics/Bitmap;",
            ">;>;",
            "Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$c",
            "<",
            "Ljava/lang/Long;",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 374
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 375
    iput-object p1, p0, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$b$b;->a:Landroid/content/Context;

    .line 376
    iput-object p2, p0, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$b$b;->b:Landroid/widget/ImageView;

    .line 377
    iput-object p3, p0, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$b$b;->c:Ljava/util/concurrent/ConcurrentHashMap;

    .line 378
    iput-object p4, p0, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$b$b;->d:Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$c;

    .line 379
    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .parameter

    .prologue
    .line 365
    check-cast p1, [Ljava/lang/Long;

    const/4 v0, 0x0

    aget-object v0, p1, v0

    iput-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$b$b;->e:Ljava/lang/Long;

    iget-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$b$b;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$b$b;->e:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    const/4 v3, 0x3

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Landroid/provider/MediaStore$Images$Thumbnails;->getThumbnail(Landroid/content/ContentResolver;JILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 2
    .parameter

    .prologue
    .line 365
    check-cast p1, Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$b$b;->d:Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$c;

    iget-object v1, p0, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$b$b;->e:Ljava/lang/Long;

    invoke-virtual {v0, v1, p1}, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$c;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$b$b;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$b$b;->b:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->invalidate()V

    iget-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$b$b;->c:Ljava/util/concurrent/ConcurrentHashMap;

    iget-object v1, p0, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$b$b;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
