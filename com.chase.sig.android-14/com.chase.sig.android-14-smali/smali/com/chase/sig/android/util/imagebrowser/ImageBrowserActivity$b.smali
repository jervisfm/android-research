.class Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$b;
.super Landroid/widget/CursorAdapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$b$b;,
        Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$b$a;
    }
.end annotation


# static fields
.field private static final d:Ljava/lang/String;


# instance fields
.field a:Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;

.field b:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Landroid/view/View;",
            "Landroid/os/AsyncTask",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Void;",
            "Landroid/graphics/Bitmap;",
            ">;>;"
        }
    .end annotation
.end field

.field c:Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$c",
            "<",
            "Ljava/lang/Long;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 235
    const-class v0, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$b;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$b;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .parameter

    .prologue
    .line 281
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Landroid/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;Z)V

    .line 267
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$b;->b:Ljava/util/concurrent/ConcurrentHashMap;

    .line 269
    new-instance v0, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$c;

    invoke-direct {v0}, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$c;-><init>()V

    iput-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$b;->c:Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$c;

    .line 274
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$b;->e:Z

    .line 282
    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 0
    .parameter

    .prologue
    .line 362
    iput-boolean p1, p0, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$b;->e:Z

    .line 363
    return-void
.end method

.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 10
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 286
    const-string v0, "_id"

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 289
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$b$a;

    .line 291
    invoke-interface {p3, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    .line 292
    iget-object v1, p0, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$b;->c:Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$c;

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$c;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    .line 294
    if-eqz v1, :cond_2

    .line 296
    iget-object v2, v0, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$b$a;->a:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 332
    :cond_0
    iget-object v1, p0, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$b;->a:Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;

    invoke-virtual {v1}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->getChoiceModeC()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 338
    iget-object v1, p0, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$b;->a:Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;

    invoke-virtual {v1}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->getCheckedItemPositionsC()Landroid/util/SparseBooleanArray;

    move-result-object v1

    .line 339
    iget-object v0, v0, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$b$a;->b:Landroid/widget/CheckBox;

    invoke-interface {p3}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 342
    :cond_1
    return-void

    .line 300
    :cond_2
    iget-object v1, v0, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$b$a;->a:Landroid/widget/ImageView;

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    move v4, v3

    .line 306
    :goto_0
    if-nez v3, :cond_0

    .line 308
    :try_start_0
    iget-object v1, p0, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$b;->b:Ljava/util/concurrent/ConcurrentHashMap;

    iget-object v7, v0, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$b$a;->a:Landroid/widget/ImageView;

    invoke-virtual {v1, v7}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/AsyncTask;

    .line 309
    if-eqz v1, :cond_3

    .line 314
    const/4 v7, 0x1

    invoke-virtual {v1, v7}, Landroid/os/AsyncTask;->cancel(Z)Z

    .line 318
    :cond_3
    new-instance v1, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$b$b;

    iget-object v7, v0, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$b$a;->a:Landroid/widget/ImageView;

    iget-object v8, p0, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$b;->b:Ljava/util/concurrent/ConcurrentHashMap;

    iget-object v9, p0, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$b;->c:Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$c;

    invoke-direct {v1, p2, v7, v8, v9}, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$b$b;-><init>(Landroid/content/Context;Landroid/widget/ImageView;Ljava/util/concurrent/ConcurrentHashMap;Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$c;)V

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Long;

    const/4 v8, 0x0

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {v1, v7}, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$b$b;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    move-result-object v1

    .line 321
    iget-object v7, p0, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$b;->b:Ljava/util/concurrent/ConcurrentHashMap;

    iget-object v8, v0, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$b$a;->a:Landroid/widget/ImageView;

    invoke-virtual {v7, v8, v1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    move v1, v2

    .line 330
    :goto_1
    add-int/lit8 v3, v4, 0x1

    .line 331
    const/16 v4, 0x64

    if-gt v3, v4, :cond_0

    move v4, v3

    move v3, v1

    goto :goto_0

    .line 324
    :catch_0
    move-exception v1

    move v1, v3

    goto :goto_1
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 346
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030047

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 350
    new-instance v1, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$b$a;

    invoke-direct {v1, v0}, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$b$a;-><init>(Landroid/view/View;)V

    .line 351
    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 353
    check-cast p3, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;

    iput-object p3, p0, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$b;->a:Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;

    .line 354
    return-object v0
.end method
