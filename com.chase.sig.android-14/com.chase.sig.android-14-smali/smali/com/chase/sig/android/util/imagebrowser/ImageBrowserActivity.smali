.class public Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;
.super Landroid/app/Activity;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$c;,
        Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$a;,
        Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$b;
    }
.end annotation


# static fields
.field private static final e:Ljava/lang/String;


# instance fields
.field a:Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;

.field b:Landroid/widget/Button;

.field c:Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$b;

.field d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const-class v0, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;->e:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 471
    return-void
.end method

.method private a()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 86
    const v0, 0x7f0702d7

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;->a:Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;

    invoke-virtual {v2}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->getCheckedItemCountC()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    iget v2, p0, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;->d:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 89
    invoke-virtual {p0, v0}, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 92
    iget-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;->a:Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;

    invoke-virtual {v0}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->getCheckedItemCountC()I

    move-result v0

    if-lez v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;->b:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 98
    :goto_0
    return-void

    .line 95
    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;->b:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;->a()V

    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, -0x1

    .line 413
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    if-ne p2, v2, :cond_1

    .line 414
    if-eqz p3, :cond_0

    .line 417
    const-string v0, "extra_key_is_selected"

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 419
    const-string v1, "extra_key_position"

    invoke-virtual {p3, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 420
    iget-object v2, p0, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;->a:Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;

    invoke-virtual {v2, v1, v0}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->a(IZ)V

    .line 425
    :cond_0
    :goto_0
    return-void

    .line 424
    :cond_1
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 62
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 64
    if-eqz p1, :cond_1

    .line 66
    const-string v0, "extra_key_max_sel_allowed"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;->d:I

    .line 78
    :cond_0
    :goto_0
    const v0, 0x7f030046

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;->setContentView(I)V

    .line 79
    const v0, 0x7f0900e8

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;

    iput-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;->a:Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;

    invoke-virtual {p0}, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;->getLastNonConfigurationInstance()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;->getLastNonConfigurationInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$b;

    iput-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;->c:Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$b;

    :goto_1
    iget-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;->a:Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->setChoiceModeC(I)V

    iget-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;->a:Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;

    iget-object v1, p0, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;->c:Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$b;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->setAdapter(Landroid/widget/ListAdapter;)V

    const v0, 0x7f0900e7

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;->b:Landroid/widget/Button;

    .line 80
    iget-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;->a:Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;

    new-instance v1, Lcom/chase/sig/android/util/imagebrowser/c;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/util/imagebrowser/c;-><init>(Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;)V

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    iget-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;->a:Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;

    new-instance v1, Lcom/chase/sig/android/util/imagebrowser/d;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/util/imagebrowser/d;-><init>(Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;)V

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;->a:Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;

    new-instance v1, Lcom/chase/sig/android/util/imagebrowser/e;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/util/imagebrowser/e;-><init>(Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;)V

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    iget-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;->b:Landroid/widget/Button;

    new-instance v1, Lcom/chase/sig/android/util/imagebrowser/f;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/util/imagebrowser/f;-><init>(Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 82
    new-instance v0, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$a;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$a;-><init>(Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;)V

    new-array v1, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 83
    return-void

    .line 71
    :cond_1
    invoke-virtual {p0}, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 72
    if-eqz v0, :cond_0

    .line 73
    const-string v1, "extra_key_max_sel_allowed"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;->d:I

    goto :goto_0

    .line 79
    :cond_2
    new-instance v0, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$b;

    invoke-virtual {p0}, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$b;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;->c:Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$b;

    goto :goto_1
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 406
    invoke-direct {p0}, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;->a()V

    .line 407
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 408
    return-void
.end method

.method public onRetainNonConfigurationInstance()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 437
    iget-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;->c:Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$b;

    return-object v0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 429
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 430
    const-string v0, "extra_key_max_sel_allowed"

    iget v1, p0, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;->d:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 431
    return-void
.end method
