.class public Lcom/chase/sig/android/util/imagebrowser/ImageViewerActivity;
.super Landroid/app/Activity;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/util/imagebrowser/ImageViewerActivity$a;
    }
.end annotation


# static fields
.field private static final f:Ljava/lang/String;


# instance fields
.field a:Landroid/widget/ImageView;

.field b:Landroid/widget/Button;

.field c:Z

.field d:I

.field e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/chase/sig/android/util/imagebrowser/ImageViewerActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/chase/sig/android/util/imagebrowser/ImageViewerActivity;->f:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 143
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .parameter

    .prologue
    const/4 v2, -0x1

    const/4 v3, 0x0

    .line 52
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 53
    const v0, 0x7f030048

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/util/imagebrowser/ImageViewerActivity;->setContentView(I)V

    .line 55
    if-eqz p1, :cond_1

    .line 56
    const-string v0, "extra_key_is_selected"

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/chase/sig/android/util/imagebrowser/ImageViewerActivity;->c:Z

    .line 57
    const-string v0, "extra_key_position"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/chase/sig/android/util/imagebrowser/ImageViewerActivity;->d:I

    .line 58
    const-string v0, "extra_key_can_select"

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/chase/sig/android/util/imagebrowser/ImageViewerActivity;->e:Z

    .line 68
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/chase/sig/android/util/imagebrowser/ImageViewerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    const v0, 0x7f0900e9

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/util/imagebrowser/ImageViewerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/ImageViewerActivity;->a:Landroid/widget/ImageView;

    new-instance v0, Lcom/chase/sig/android/util/imagebrowser/ImageViewerActivity$a;

    iget-object v2, p0, Lcom/chase/sig/android/util/imagebrowser/ImageViewerActivity;->a:Landroid/widget/ImageView;

    invoke-direct {v0, p0, v2}, Lcom/chase/sig/android/util/imagebrowser/ImageViewerActivity$a;-><init>(Lcom/chase/sig/android/util/imagebrowser/ImageViewerActivity;Landroid/widget/ImageView;)V

    const/4 v2, 0x1

    new-array v2, v2, [Landroid/net/Uri;

    aput-object v1, v2, v3

    invoke-virtual {v0, v2}, Lcom/chase/sig/android/util/imagebrowser/ImageViewerActivity$a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    const v0, 0x7f0900ea

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/util/imagebrowser/ImageViewerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/ImageViewerActivity;->b:Landroid/widget/Button;

    .line 69
    iget-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/ImageViewerActivity;->b:Landroid/widget/Button;

    new-instance v1, Lcom/chase/sig/android/util/imagebrowser/g;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/util/imagebrowser/g;-><init>(Lcom/chase/sig/android/util/imagebrowser/ImageViewerActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 71
    return-void

    .line 60
    :cond_1
    invoke-virtual {p0}, Lcom/chase/sig/android/util/imagebrowser/ImageViewerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 61
    if-eqz v0, :cond_0

    .line 62
    const-string v1, "extra_key_is_selected"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/chase/sig/android/util/imagebrowser/ImageViewerActivity;->c:Z

    .line 63
    const-string v1, "extra_key_position"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/chase/sig/android/util/imagebrowser/ImageViewerActivity;->d:I

    .line 64
    const-string v1, "extra_key_can_select"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/chase/sig/android/util/imagebrowser/ImageViewerActivity;->e:Z

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/ImageViewerActivity;->a:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .line 132
    if-eqz v0, :cond_0

    .line 133
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 134
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 136
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 137
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 123
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 124
    invoke-virtual {p0}, Lcom/chase/sig/android/util/imagebrowser/ImageViewerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const/4 v0, 0x0

    if-eqz v1, :cond_0

    const-string v0, "extra_key_title"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    if-nez v0, :cond_1

    const v0, 0x7f0702da

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/util/imagebrowser/ImageViewerActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_1
    invoke-virtual {p0, v0}, Lcom/chase/sig/android/util/imagebrowser/ImageViewerActivity;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/ImageViewerActivity;->b:Landroid/widget/Button;

    iget-boolean v1, p0, Lcom/chase/sig/android/util/imagebrowser/ImageViewerActivity;->e:Z

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v1, p0, Lcom/chase/sig/android/util/imagebrowser/ImageViewerActivity;->b:Landroid/widget/Button;

    iget-boolean v0, p0, Lcom/chase/sig/android/util/imagebrowser/ImageViewerActivity;->c:Z

    if-eqz v0, :cond_2

    const v0, 0x7f0702d8

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/util/imagebrowser/ImageViewerActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 125
    return-void

    .line 124
    :cond_2
    const v0, 0x7f0702d9

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/util/imagebrowser/ImageViewerActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter

    .prologue
    .line 115
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 116
    const-string v0, "extra_key_is_selected"

    iget-boolean v1, p0, Lcom/chase/sig/android/util/imagebrowser/ImageViewerActivity;->c:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 117
    const-string v0, "extra_key_position"

    iget v1, p0, Lcom/chase/sig/android/util/imagebrowser/ImageViewerActivity;->d:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 118
    const-string v0, "extra_key_can_select"

    iget-boolean v1, p0, Lcom/chase/sig/android/util/imagebrowser/ImageViewerActivity;->e:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 119
    return-void
.end method
