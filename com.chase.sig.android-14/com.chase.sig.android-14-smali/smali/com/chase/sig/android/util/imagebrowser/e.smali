.class final Lcom/chase/sig/android/util/imagebrowser/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemLongClickListener;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 141
    iput-object p1, p0, Lcom/chase/sig/android/util/imagebrowser/e;->a:Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 5
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 147
    iget-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/e;->a:Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;

    iget-object v0, v0, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;->a:Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;

    invoke-virtual {v0}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$b;

    invoke-virtual {v0}, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$b;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 148
    const-string v1, "title"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 149
    invoke-interface {v0, p3}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 150
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 152
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/chase/sig/android/util/imagebrowser/e;->a:Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;

    const-class v3, Lcom/chase/sig/android/util/imagebrowser/ImageViewerActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 153
    sget-object v2, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-static {v2, p4, p5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    .line 155
    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 159
    iget-object v2, p0, Lcom/chase/sig/android/util/imagebrowser/e;->a:Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;

    iget-object v2, v2, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;->a:Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;

    invoke-virtual {v2}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->getCheckedItemCountC()I

    move-result v2

    iget-object v3, p0, Lcom/chase/sig/android/util/imagebrowser/e;->a:Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;

    iget v3, v3, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;->d:I

    if-ge v2, v3, :cond_1

    .line 160
    const-string v2, "extra_key_can_select"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 166
    :cond_0
    :goto_0
    const-string v2, "extra_key_title"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 167
    const-string v0, "extra_key_position"

    invoke-virtual {v1, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 168
    const-string v0, "extra_key_is_selected"

    iget-object v2, p0, Lcom/chase/sig/android/util/imagebrowser/e;->a:Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;

    iget-object v2, v2, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;->a:Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;

    invoke-virtual {v2, p3}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->a(I)Z

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 170
    iget-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/e;->a:Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;

    invoke-virtual {v0, v1, v4}, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 171
    const/4 v0, 0x0

    return v0

    .line 162
    :cond_1
    iget-object v2, p0, Lcom/chase/sig/android/util/imagebrowser/e;->a:Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;

    iget-object v2, v2, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;->a:Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;

    invoke-virtual {v2, p3}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 163
    const-string v2, "extra_key_can_select"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_0
.end method
