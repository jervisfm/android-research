.class final Lcom/chase/sig/android/util/imagebrowser/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 176
    iput-object p1, p0, Lcom/chase/sig/android/util/imagebrowser/f;->a:Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 9
    .parameter

    .prologue
    .line 181
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    iget-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/f;->a:Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;

    iget-object v0, v0, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;->a:Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;

    invoke-virtual {v0}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->getCheckedItemPositionsC()Landroid/util/SparseBooleanArray;

    move-result-object v1

    .line 182
    iget-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/f;->a:Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;

    iget-object v0, v0, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;->a:Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;

    invoke-virtual {v0}, Lcom/chase/sig/android/util/imagebrowser/GridViewCompat;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$b;

    invoke-virtual {v0}, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity$b;->getCursor()Landroid/database/Cursor;

    move-result-object v2

    .line 183
    const-string v0, "_id"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 185
    invoke-virtual {v1}, Landroid/util/SparseBooleanArray;->size()I

    move-result v4

    .line 186
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 187
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v4, :cond_1

    .line 188
    invoke-virtual {v1, v0}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v6

    invoke-virtual {v1, v6}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 189
    invoke-virtual {v1, v0}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v6

    invoke-interface {v2, v6}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 191
    invoke-interface {v2, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 192
    sget-object v8, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-static {v8, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v6

    .line 195
    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 187
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 200
    :cond_1
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 201
    const-string v1, "android.intent.extra.TEXT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 202
    const-string v1, "image_uris"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 203
    iget-object v1, p0, Lcom/chase/sig/android/util/imagebrowser/f;->a:Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;->setResult(ILandroid/content/Intent;)V

    .line 206
    iget-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/f;->a:Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/util/imagebrowser/ImageBrowserActivity;->finish()V

    .line 207
    return-void
.end method
