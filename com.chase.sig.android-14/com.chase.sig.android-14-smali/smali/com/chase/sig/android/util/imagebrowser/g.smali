.class final Lcom/chase/sig/android/util/imagebrowser/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/util/imagebrowser/ImageViewerActivity;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/util/imagebrowser/ImageViewerActivity;)V
    .locals 0
    .parameter

    .prologue
    .line 74
    iput-object p1, p0, Lcom/chase/sig/android/util/imagebrowser/g;->a:Lcom/chase/sig/android/util/imagebrowser/ImageViewerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3
    .parameter

    .prologue
    .line 79
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 80
    const-string v2, "extra_key_is_selected"

    iget-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/g;->a:Lcom/chase/sig/android/util/imagebrowser/ImageViewerActivity;

    iget-boolean v0, v0, Lcom/chase/sig/android/util/imagebrowser/ImageViewerActivity;->c:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 82
    const-string v0, "extra_key_position"

    iget-object v2, p0, Lcom/chase/sig/android/util/imagebrowser/g;->a:Lcom/chase/sig/android/util/imagebrowser/ImageViewerActivity;

    iget v2, v2, Lcom/chase/sig/android/util/imagebrowser/ImageViewerActivity;->d:I

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 83
    iget-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/g;->a:Lcom/chase/sig/android/util/imagebrowser/ImageViewerActivity;

    const/4 v2, -0x1

    invoke-virtual {v0, v2, v1}, Lcom/chase/sig/android/util/imagebrowser/ImageViewerActivity;->setResult(ILandroid/content/Intent;)V

    .line 84
    iget-object v0, p0, Lcom/chase/sig/android/util/imagebrowser/g;->a:Lcom/chase/sig/android/util/imagebrowser/ImageViewerActivity;

    invoke-virtual {v0}, Lcom/chase/sig/android/util/imagebrowser/ImageViewerActivity;->finish()V

    .line 85
    return-void

    .line 80
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
