.class final Lcom/chase/sig/android/util/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/http/cookie/Cookie;


# instance fields
.field final synthetic a:Lorg/apache/http/cookie/Cookie;

.field final synthetic b:Lcom/chase/sig/android/util/f;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/util/f;Lorg/apache/http/cookie/Cookie;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 380
    iput-object p1, p0, Lcom/chase/sig/android/util/j;->b:Lcom/chase/sig/android/util/f;

    iput-object p2, p0, Lcom/chase/sig/android/util/j;->a:Lorg/apache/http/cookie/Cookie;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getComment()Ljava/lang/String;
    .locals 1

    .prologue
    .line 439
    iget-object v0, p0, Lcom/chase/sig/android/util/j;->a:Lorg/apache/http/cookie/Cookie;

    invoke-interface {v0}, Lorg/apache/http/cookie/Cookie;->getComment()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getCommentURL()Ljava/lang/String;
    .locals 1

    .prologue
    .line 434
    iget-object v0, p0, Lcom/chase/sig/android/util/j;->a:Lorg/apache/http/cookie/Cookie;

    invoke-interface {v0}, Lorg/apache/http/cookie/Cookie;->getCommentURL()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getDomain()Ljava/lang/String;
    .locals 1

    .prologue
    .line 429
    const-string v0, ".chase.com"

    return-object v0
.end method

.method public final getExpiryDate()Ljava/util/Date;
    .locals 1

    .prologue
    .line 424
    iget-object v0, p0, Lcom/chase/sig/android/util/j;->a:Lorg/apache/http/cookie/Cookie;

    invoke-interface {v0}, Lorg/apache/http/cookie/Cookie;->getExpiryDate()Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 419
    iget-object v0, p0, Lcom/chase/sig/android/util/j;->a:Lorg/apache/http/cookie/Cookie;

    invoke-interface {v0}, Lorg/apache/http/cookie/Cookie;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 414
    iget-object v0, p0, Lcom/chase/sig/android/util/j;->a:Lorg/apache/http/cookie/Cookie;

    invoke-interface {v0}, Lorg/apache/http/cookie/Cookie;->getPath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getPorts()[I
    .locals 1

    .prologue
    .line 409
    iget-object v0, p0, Lcom/chase/sig/android/util/j;->a:Lorg/apache/http/cookie/Cookie;

    invoke-interface {v0}, Lorg/apache/http/cookie/Cookie;->getPorts()[I

    move-result-object v0

    return-object v0
.end method

.method public final getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 404
    iget-object v0, p0, Lcom/chase/sig/android/util/j;->a:Lorg/apache/http/cookie/Cookie;

    invoke-interface {v0}, Lorg/apache/http/cookie/Cookie;->getValue()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getVersion()I
    .locals 1

    .prologue
    .line 399
    iget-object v0, p0, Lcom/chase/sig/android/util/j;->a:Lorg/apache/http/cookie/Cookie;

    invoke-interface {v0}, Lorg/apache/http/cookie/Cookie;->getVersion()I

    move-result v0

    return v0
.end method

.method public final isExpired(Ljava/util/Date;)Z
    .locals 1
    .parameter

    .prologue
    .line 394
    iget-object v0, p0, Lcom/chase/sig/android/util/j;->a:Lorg/apache/http/cookie/Cookie;

    invoke-interface {v0, p1}, Lorg/apache/http/cookie/Cookie;->isExpired(Ljava/util/Date;)Z

    move-result v0

    return v0
.end method

.method public final isPersistent()Z
    .locals 1

    .prologue
    .line 389
    iget-object v0, p0, Lcom/chase/sig/android/util/j;->a:Lorg/apache/http/cookie/Cookie;

    invoke-interface {v0}, Lorg/apache/http/cookie/Cookie;->isPersistent()Z

    move-result v0

    return v0
.end method

.method public final isSecure()Z
    .locals 1

    .prologue
    .line 384
    iget-object v0, p0, Lcom/chase/sig/android/util/j;->a:Lorg/apache/http/cookie/Cookie;

    invoke-interface {v0}, Lorg/apache/http/cookie/Cookie;->isSecure()Z

    move-result v0

    return v0
.end method
