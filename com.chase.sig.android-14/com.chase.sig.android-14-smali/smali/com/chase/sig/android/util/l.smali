.class public final Lcom/chase/sig/android/util/l;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:[Ljava/util/Calendar;

.field private static b:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x6

    const/4 v6, 0x3

    const/4 v4, 0x4

    const/16 v5, 0x7dc

    .line 16
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/util/GregorianCalendar;

    const/4 v1, 0x0

    new-instance v2, Ljava/util/GregorianCalendar;

    const/16 v3, 0x1c

    invoke-direct {v2, v5, v4, v3}, Ljava/util/GregorianCalendar;-><init>(III)V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Ljava/util/GregorianCalendar;

    invoke-direct {v2, v5, v7, v4}, Ljava/util/GregorianCalendar;-><init>(III)V

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-instance v2, Ljava/util/GregorianCalendar;

    invoke-direct {v2, v5, v8, v6}, Ljava/util/GregorianCalendar;-><init>(III)V

    aput-object v2, v0, v1

    new-instance v1, Ljava/util/GregorianCalendar;

    const/16 v2, 0x9

    invoke-direct {v1, v5, v2, v8}, Ljava/util/GregorianCalendar;-><init>(III)V

    aput-object v1, v0, v6

    new-instance v1, Ljava/util/GregorianCalendar;

    const/16 v2, 0xa

    const/16 v3, 0xc

    invoke-direct {v1, v5, v2, v3}, Ljava/util/GregorianCalendar;-><init>(III)V

    aput-object v1, v0, v4

    const/4 v1, 0x5

    new-instance v2, Ljava/util/GregorianCalendar;

    const/16 v3, 0xa

    const/16 v4, 0x16

    invoke-direct {v2, v5, v3, v4}, Ljava/util/GregorianCalendar;-><init>(III)V

    aput-object v2, v0, v1

    new-instance v1, Ljava/util/GregorianCalendar;

    const/16 v2, 0xb

    const/16 v3, 0x19

    invoke-direct {v1, v5, v2, v3}, Ljava/util/GregorianCalendar;-><init>(III)V

    aput-object v1, v0, v7

    sput-object v0, Lcom/chase/sig/android/util/l;->a:[Ljava/util/Calendar;

    .line 28
    const v0, 0x5265c00

    sput v0, Lcom/chase/sig/android/util/l;->b:I

    return-void
.end method

.method public static a(I)Ljava/util/Calendar;
    .locals 6
    .parameter

    .prologue
    const/4 v5, 0x2

    const/4 v3, 0x7

    const/4 v4, 0x5

    const/4 v1, 0x1

    .line 76
    const-string v0, "America/New_York"

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v2

    .line 77
    const/16 v0, 0xb

    invoke-virtual {v2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    if-lt v0, p0, :cond_0

    invoke-virtual {v2, v4, v1}, Ljava/util/Calendar;->add(II)V

    :cond_0
    :goto_0
    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v0

    if-eq v3, v0, :cond_1

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v0

    if-eq v1, v0, :cond_1

    invoke-static {v2}, Lcom/chase/sig/android/util/l;->a(Ljava/util/Calendar;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    if-nez v0, :cond_2

    invoke-virtual {v2, v4, v1}, Ljava/util/Calendar;->add(II)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v2, v5}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-virtual {v0, v5, v3}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {v2, v4}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-virtual {v0, v4, v3}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {v2, v1}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    return-object v0
.end method

.method public static a()Ljava/util/TimeZone;
    .locals 1

    .prologue
    .line 35
    const-string v0, "America/New_York"

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    return-object v0
.end method

.method public static final a([Ljava/util/Calendar;)V
    .locals 0
    .parameter

    .prologue
    .line 31
    sput-object p0, Lcom/chase/sig/android/util/l;->a:[Ljava/util/Calendar;

    .line 32
    return-void
.end method

.method private static a(Landroid/text/format/Time;)Z
    .locals 7
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 45
    new-instance v4, Landroid/text/format/Time;

    invoke-direct {v4}, Landroid/text/format/Time;-><init>()V

    move v0, v1

    .line 47
    :goto_0
    sget-object v3, Lcom/chase/sig/android/util/l;->a:[Ljava/util/Calendar;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 48
    sget-object v3, Lcom/chase/sig/android/util/l;->a:[Ljava/util/Calendar;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Landroid/text/format/Time;->set(J)V

    .line 50
    iget v3, p0, Landroid/text/format/Time;->year:I

    iget v5, v4, Landroid/text/format/Time;->year:I

    if-ne v3, v5, :cond_1

    iget v3, p0, Landroid/text/format/Time;->month:I

    iget v5, v4, Landroid/text/format/Time;->month:I

    if-ne v3, v5, :cond_1

    iget v3, p0, Landroid/text/format/Time;->monthDay:I

    iget v5, v4, Landroid/text/format/Time;->monthDay:I

    if-ne v3, v5, :cond_1

    move v3, v2

    :goto_1
    if-eqz v3, :cond_2

    move v1, v2

    .line 55
    :cond_0
    return v1

    :cond_1
    move v3, v1

    .line 50
    goto :goto_1

    .line 47
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static a(Ljava/util/Calendar;)Z
    .locals 3
    .parameter

    .prologue
    .line 39
    new-instance v0, Landroid/text/format/Time;

    const-string v1, "America/New_York"

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 40
    invoke-virtual {p0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/text/format/Time;->set(J)V

    .line 41
    invoke-static {v0}, Lcom/chase/sig/android/util/l;->a(Landroid/text/format/Time;)Z

    move-result v0

    return v0
.end method

.method public static a(Ljava/util/Calendar;Ljava/util/Calendar;)Z
    .locals 4
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x5

    const/4 v3, 0x2

    const/4 v0, 0x1

    .line 64
    invoke-virtual {p0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {p1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    if-ne v1, v2, :cond_0

    invoke-virtual {p0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {p1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    if-ne v1, v2, :cond_0

    invoke-virtual {p0, v0}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v2

    if-ne v1, v2, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/util/Date;Ljava/util/Calendar;)Z
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 70
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 71
    invoke-virtual {v0, p0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 72
    invoke-static {v0, p1}, Lcom/chase/sig/android/util/l;->a(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    move-result v0

    return v0
.end method

.method public static b()J
    .locals 2

    .prologue
    .line 114
    const-wide/16 v0, 0x0

    return-wide v0
.end method
