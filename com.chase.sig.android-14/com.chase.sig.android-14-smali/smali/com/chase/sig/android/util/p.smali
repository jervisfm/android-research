.class public final Lcom/chase/sig/android/util/p;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Ljava/io/DataOutputStream;

.field b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/io/OutputStream;Ljava/lang/String;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object v0, p0, Lcom/chase/sig/android/util/p;->a:Ljava/io/DataOutputStream;

    .line 38
    iput-object v0, p0, Lcom/chase/sig/android/util/p;->b:Ljava/lang/String;

    .line 55
    if-nez p1, :cond_0

    .line 56
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Output stream is required."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 58
    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    .line 59
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Boundary stream is required."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 61
    :cond_2
    new-instance v0, Ljava/io/DataOutputStream;

    invoke-direct {v0, p1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v0, p0, Lcom/chase/sig/android/util/p;->a:Ljava/io/DataOutputStream;

    .line 62
    iput-object p2, p0, Lcom/chase/sig/android/util/p;->b:Ljava/lang/String;

    .line 63
    return-void
.end method
