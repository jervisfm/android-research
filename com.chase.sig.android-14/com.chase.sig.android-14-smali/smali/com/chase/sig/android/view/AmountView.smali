.class public Lcom/chase/sig/android/view/AmountView;
.super Landroid/widget/EditText;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 12
    invoke-direct {p0, p1, p2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 13
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 16
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 17
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 21
    new-instance v3, Lcom/chase/sig/android/util/Dollar;

    invoke-virtual {p0}, Lcom/chase/sig/android/view/AmountView;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Lcom/chase/sig/android/util/Dollar;-><init>(Ljava/lang/String;)V

    .line 22
    invoke-virtual {v3}, Lcom/chase/sig/android/util/Dollar;->b()Ljava/math/BigDecimal;

    move-result-object v2

    if-nez v2, :cond_1

    move v2, v1

    .line 23
    :goto_0
    if-eqz v2, :cond_2

    .line 32
    :cond_0
    :goto_1
    return v0

    :cond_1
    move v2, v0

    .line 22
    goto :goto_0

    .line 26
    :cond_2
    invoke-virtual {v3}, Lcom/chase/sig/android/util/Dollar;->b()Ljava/math/BigDecimal;

    move-result-object v2

    invoke-virtual {v2}, Ljava/math/BigDecimal;->scale()I

    move-result v2

    const/4 v4, 0x2

    if-gt v2, v4, :cond_0

    .line 29
    invoke-virtual {v3}, Lcom/chase/sig/android/util/Dollar;->b()Ljava/math/BigDecimal;

    move-result-object v2

    invoke-virtual {v2}, Ljava/math/BigDecimal;->doubleValue()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmpg-double v2, v2, v4

    if-lez v2, :cond_0

    move v0, v1

    .line 32
    goto :goto_1
.end method

.method public getDollarAmount()Lcom/chase/sig/android/util/Dollar;
    .locals 2

    .prologue
    .line 36
    new-instance v0, Lcom/chase/sig/android/util/Dollar;

    invoke-virtual {p0}, Lcom/chase/sig/android/view/AmountView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/chase/sig/android/util/Dollar;-><init>(Ljava/lang/String;)V

    return-object v0
.end method
