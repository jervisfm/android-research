.class public Lcom/chase/sig/android/view/HomeImageButton;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"


# instance fields
.field private a:Landroid/widget/ImageView;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/ImageView;

.field private d:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 8
    .parameter
    .parameter

    .prologue
    const/16 v7, 0xf

    const/4 v6, 0x1

    const/4 v5, -0x1

    const/4 v4, -0x2

    const/4 v3, 0x5

    .line 54
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 56
    const v0, 0x7f0200ad

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/view/HomeImageButton;->setBackgroundResource(I)V

    .line 59
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/chase/sig/android/view/HomeImageButton;->a:Landroid/widget/ImageView;

    .line 60
    iget-object v0, p0, Lcom/chase/sig/android/view/HomeImageButton;->a:Landroid/widget/ImageView;

    const-string v1, "http://chase.com/android"

    const-string v2, "image_src"

    invoke-interface {p2, v1, v2, v5}, Landroid/util/AttributeSet;->getAttributeResourceValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 61
    iget-object v0, p0, Lcom/chase/sig/android/view/HomeImageButton;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v3, v3, v3, v3}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 62
    iget-object v0, p0, Lcom/chase/sig/android/view/HomeImageButton;->a:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 63
    iget-object v0, p0, Lcom/chase/sig/android/view/HomeImageButton;->a:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAdjustViewBounds(Z)V

    .line 64
    iget-object v0, p0, Lcom/chase/sig/android/view/HomeImageButton;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setId(I)V

    .line 66
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 67
    invoke-virtual {v0, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 68
    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 69
    iget-object v1, p0, Lcom/chase/sig/android/view/HomeImageButton;->a:Landroid/widget/ImageView;

    invoke-virtual {p0, v1, v0}, Lcom/chase/sig/android/view/HomeImageButton;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 72
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/chase/sig/android/view/HomeImageButton;->b:Landroid/widget/TextView;

    .line 73
    const-string v0, "http://chase.com/android"

    const-string v1, "label"

    const v2, 0x7f0700bc

    invoke-interface {p2, v0, v1, v2}, Landroid/util/AttributeSet;->getAttributeResourceValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    .line 74
    invoke-virtual {p0}, Lcom/chase/sig/android/view/HomeImageButton;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 76
    const-string v1, "Chase"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 77
    const-string v1, "Find Chase"

    const-string v2, "Find\nChase"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 80
    :cond_0
    iget-object v1, p0, Lcom/chase/sig/android/view/HomeImageButton;->b:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 81
    iget-object v0, p0, Lcom/chase/sig/android/view/HomeImageButton;->b:Landroid/widget/TextView;

    const v1, 0x7f080006

    invoke-virtual {v0, p1, v1}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 82
    iget-object v0, p0, Lcom/chase/sig/android/view/HomeImageButton;->b:Landroid/widget/TextView;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 83
    iget-object v0, p0, Lcom/chase/sig/android/view/HomeImageButton;->b:Landroid/widget/TextView;

    const/high16 v1, 0x41a0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 85
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 86
    iget-object v1, p0, Lcom/chase/sig/android/view/HomeImageButton;->a:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getId()I

    move-result v1

    invoke-virtual {v0, v6, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 87
    invoke-virtual {v0, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 88
    iget-object v1, p0, Lcom/chase/sig/android/view/HomeImageButton;->b:Landroid/widget/TextView;

    invoke-virtual {p0, v1, v0}, Lcom/chase/sig/android/view/HomeImageButton;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 91
    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/chase/sig/android/view/HomeImageButton;->c:Landroid/widget/ImageView;

    .line 92
    iget-object v0, p0, Lcom/chase/sig/android/view/HomeImageButton;->c:Landroid/widget/ImageView;

    const-string v1, "http://chase.com/android"

    const-string v2, "chevron_image_src"

    invoke-interface {p2, v1, v2, v5}, Landroid/util/AttributeSet;->getAttributeResourceValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 93
    iget-object v0, p0, Lcom/chase/sig/android/view/HomeImageButton;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, v3, v3, v3, v3}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 94
    iget-object v0, p0, Lcom/chase/sig/android/view/HomeImageButton;->c:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 95
    iget-object v0, p0, Lcom/chase/sig/android/view/HomeImageButton;->c:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAdjustViewBounds(Z)V

    .line 97
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 98
    iget-object v1, p0, Lcom/chase/sig/android/view/HomeImageButton;->b:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getId()I

    move-result v1

    invoke-virtual {v0, v6, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 99
    invoke-virtual {v0, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 100
    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 102
    iget-object v1, p0, Lcom/chase/sig/android/view/HomeImageButton;->c:Landroid/widget/ImageView;

    invoke-virtual {p0, v1, v0}, Lcom/chase/sig/android/view/HomeImageButton;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 104
    new-instance v0, Landroid/graphics/drawable/ShapeDrawable;

    new-instance v1, Landroid/graphics/drawable/shapes/RectShape;

    invoke-direct {v1}, Landroid/graphics/drawable/shapes/RectShape;-><init>()V

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    .line 105
    invoke-virtual {v0}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v1

    invoke-virtual {p0}, Lcom/chase/sig/android/view/HomeImageButton;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f060026

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 107
    new-instance v1, Lcom/chase/sig/android/view/i;

    invoke-virtual {p0}, Lcom/chase/sig/android/view/HomeImageButton;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, p0, v2, v0}, Lcom/chase/sig/android/view/i;-><init>(Lcom/chase/sig/android/view/HomeImageButton;Landroid/content/Context;Landroid/graphics/drawable/ShapeDrawable;)V

    iput-object v1, p0, Lcom/chase/sig/android/view/HomeImageButton;->d:Landroid/view/View;

    .line 116
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v5, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 117
    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 118
    iget-object v1, p0, Lcom/chase/sig/android/view/HomeImageButton;->d:Landroid/view/View;

    invoke-virtual {p0, v1, v0}, Lcom/chase/sig/android/view/HomeImageButton;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 120
    new-instance v0, Lcom/chase/sig/android/view/j;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/view/j;-><init>(Lcom/chase/sig/android/view/HomeImageButton;)V

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/view/HomeImageButton;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 132
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/view/HomeImageButton;)Landroid/view/View;
    .locals 1
    .parameter

    .prologue
    .line 17
    iget-object v0, p0, Lcom/chase/sig/android/view/HomeImageButton;->d:Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method protected onLayout(ZIIII)V
    .locals 7
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x0

    .line 32
    invoke-super/range {p0 .. p5}, Landroid/widget/RelativeLayout;->onLayout(ZIIII)V

    .line 34
    sub-int v0, p4, p2

    .line 35
    sub-int v1, p5, p3

    .line 36
    div-int/lit8 v2, v1, 0x2

    .line 38
    iget-object v3, p0, Lcom/chase/sig/android/view/HomeImageButton;->a:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/chase/sig/android/view/HomeImageButton;->a:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v4, p2

    invoke-virtual {v3, p2, v6, v4, v1}, Landroid/widget/ImageView;->layout(IIII)V

    .line 40
    iget-object v3, p0, Lcom/chase/sig/android/view/HomeImageButton;->a:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v3, p2

    .line 41
    iget-object v4, p0, Lcom/chase/sig/android/view/HomeImageButton;->b:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/chase/sig/android/view/HomeImageButton;->b:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    sub-int/2addr v2, v5

    iget-object v5, p0, Lcom/chase/sig/android/view/HomeImageButton;->b:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getWidth()I

    move-result v5

    add-int/2addr v5, v3

    invoke-virtual {v4, v3, v2, v5, v1}, Landroid/widget/TextView;->layout(IIII)V

    .line 47
    iget-object v2, p0, Lcom/chase/sig/android/view/HomeImageButton;->c:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/chase/sig/android/view/HomeImageButton;->c:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v3

    sub-int v3, p4, v3

    invoke-virtual {v2, v3, v6, p4, v1}, Landroid/widget/ImageView;->layout(IIII)V

    .line 49
    iget-object v2, p0, Lcom/chase/sig/android/view/HomeImageButton;->d:Landroid/view/View;

    add-int/lit8 v3, v1, -0x2

    invoke-virtual {v2, p2, v3, v0, v1}, Landroid/view/View;->layout(IIII)V

    .line 51
    return-void
.end method
