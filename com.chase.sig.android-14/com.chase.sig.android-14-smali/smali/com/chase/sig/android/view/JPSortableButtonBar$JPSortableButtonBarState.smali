.class public Lcom/chase/sig/android/view/JPSortableButtonBar$JPSortableButtonBarState;
.super Landroid/view/View$BaseSavedState;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/view/JPSortableButtonBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "JPSortableButtonBarState"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/chase/sig/android/view/JPSortableButtonBar$JPSortableButtonBarState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Z

.field private b:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 210
    new-instance v0, Lcom/chase/sig/android/view/p;

    invoke-direct {v0}, Lcom/chase/sig/android/view/p;-><init>()V

    sput-object v0, Lcom/chase/sig/android/view/JPSortableButtonBar$JPSortableButtonBarState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .parameter

    .prologue
    .line 172
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcel;)V

    .line 173
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/chase/sig/android/view/JPSortableButtonBar$JPSortableButtonBarState;->b:I

    .line 174
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lcom/chase/sig/android/view/JPSortableButtonBar$JPSortableButtonBarState;->a:Z

    .line 175
    return-void

    .line 174
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public constructor <init>(Landroid/os/Parcelable;IZ)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 180
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcelable;)V

    .line 182
    iput p2, p0, Lcom/chase/sig/android/view/JPSortableButtonBar$JPSortableButtonBarState;->b:I

    .line 183
    iput-boolean p3, p0, Lcom/chase/sig/android/view/JPSortableButtonBar$JPSortableButtonBarState;->a:Z

    .line 184
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 199
    iget v0, p0, Lcom/chase/sig/android/view/JPSortableButtonBar$JPSortableButtonBarState;->b:I

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 207
    iget-boolean v0, p0, Lcom/chase/sig/android/view/JPSortableButtonBar$JPSortableButtonBarState;->a:Z

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 189
    iget v0, p0, Lcom/chase/sig/android/view/JPSortableButtonBar$JPSortableButtonBarState;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 190
    iget-boolean v0, p0, Lcom/chase/sig/android/view/JPSortableButtonBar$JPSortableButtonBarState;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 191
    invoke-super {p0, p1, p2}, Landroid/view/View$BaseSavedState;->writeToParcel(Landroid/os/Parcel;I)V

    .line 192
    return-void

    .line 190
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
