.class public Lcom/chase/sig/android/view/JPSortableButtonBar;
.super Landroid/widget/LinearLayout;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/view/JPSortableButtonBar$JPSortableButtonBarState;,
        Lcom/chase/sig/android/view/JPSortableButtonBar$a;
    }
.end annotation


# instance fields
.field private a:Landroid/view/View$OnClickListener;

.field private b:Lcom/chase/sig/android/view/JPSortableButtonBar$a;

.field private c:Landroid/widget/Button;

.field private d:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .parameter

    .prologue
    .line 29
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 26
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/chase/sig/android/view/JPSortableButtonBar;->d:Z

    .line 30
    invoke-direct {p0}, Lcom/chase/sig/android/view/JPSortableButtonBar;->a()V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/chase/sig/android/view/JPSortableButtonBar;->d:Z

    .line 35
    invoke-direct {p0}, Lcom/chase/sig/android/view/JPSortableButtonBar;->a()V

    .line 36
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/view/JPSortableButtonBar;)Landroid/widget/Button;
    .locals 1
    .parameter

    .prologue
    .line 14
    iget-object v0, p0, Lcom/chase/sig/android/view/JPSortableButtonBar;->c:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic a(Lcom/chase/sig/android/view/JPSortableButtonBar;Landroid/widget/Button;)Landroid/widget/Button;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 14
    iput-object p1, p0, Lcom/chase/sig/android/view/JPSortableButtonBar;->c:Landroid/widget/Button;

    return-object p1
.end method

.method private a()V
    .locals 1

    .prologue
    .line 40
    new-instance v0, Lcom/chase/sig/android/view/o;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/view/o;-><init>(Lcom/chase/sig/android/view/JPSortableButtonBar;)V

    iput-object v0, p0, Lcom/chase/sig/android/view/JPSortableButtonBar;->a:Landroid/view/View$OnClickListener;

    .line 64
    return-void
.end method

.method private a(Landroid/widget/Button;Z)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 133
    if-eqz p2, :cond_1

    .line 134
    iget-boolean v0, p0, Lcom/chase/sig/android/view/JPSortableButtonBar;->d:Z

    if-eqz v0, :cond_0

    const v0, 0x7f020038

    :goto_0
    invoke-virtual {p1, v0}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 135
    invoke-virtual {p0}, Lcom/chase/sig/android/view/JPSortableButtonBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0200f9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setTextColor(I)V

    .line 140
    :goto_1
    return-void

    .line 134
    :cond_0
    const v0, 0x7f020039

    goto :goto_0

    .line 137
    :cond_1
    const v0, 0x7f020037

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 138
    invoke-virtual {p0}, Lcom/chase/sig/android/view/JPSortableButtonBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0200f6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setTextColor(I)V

    goto :goto_1
.end method

.method static synthetic a(Lcom/chase/sig/android/view/JPSortableButtonBar;Landroid/widget/Button;Z)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Lcom/chase/sig/android/view/JPSortableButtonBar;->a(Landroid/widget/Button;Z)V

    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/view/JPSortableButtonBar;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 14
    iput-boolean p1, p0, Lcom/chase/sig/android/view/JPSortableButtonBar;->d:Z

    return p1
.end method

.method static synthetic b(Lcom/chase/sig/android/view/JPSortableButtonBar;)Z
    .locals 1
    .parameter

    .prologue
    .line 14
    iget-boolean v0, p0, Lcom/chase/sig/android/view/JPSortableButtonBar;->d:Z

    return v0
.end method

.method static synthetic c(Lcom/chase/sig/android/view/JPSortableButtonBar;)Lcom/chase/sig/android/view/JPSortableButtonBar$a;
    .locals 1
    .parameter

    .prologue
    .line 14
    iget-object v0, p0, Lcom/chase/sig/android/view/JPSortableButtonBar;->b:Lcom/chase/sig/android/view/JPSortableButtonBar$a;

    return-object v0
.end method


# virtual methods
.method public final a(II)V
    .locals 3
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 68
    invoke-virtual {p0}, Lcom/chase/sig/android/view/JPSortableButtonBar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0300bd

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 70
    invoke-virtual {p0}, Lcom/chase/sig/android/view/JPSortableButtonBar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 71
    iget-object v1, p0, Lcom/chase/sig/android/view/JPSortableButtonBar;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 73
    invoke-direct {p0, v0, v2}, Lcom/chase/sig/android/view/JPSortableButtonBar;->a(Landroid/widget/Button;Z)V

    .line 74
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    .line 75
    invoke-virtual {p0, v0}, Lcom/chase/sig/android/view/JPSortableButtonBar;->addView(Landroid/view/View;)V

    .line 76
    return-void
.end method

.method public final a(ILjava/lang/Boolean;)V
    .locals 6
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 95
    if-eqz p2, :cond_0

    .line 96
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/chase/sig/android/view/JPSortableButtonBar;->d:Z

    .line 99
    :cond_0
    invoke-virtual {p0}, Lcom/chase/sig/android/view/JPSortableButtonBar;->getChildCount()I

    move-result v4

    move v2, v3

    .line 101
    :goto_0
    if-ge v2, v4, :cond_4

    .line 103
    invoke-virtual {p0, v2}, Lcom/chase/sig/android/view/JPSortableButtonBar;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 104
    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 106
    if-eqz v0, :cond_2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 108
    iget-object v0, p0, Lcom/chase/sig/android/view/JPSortableButtonBar;->c:Landroid/widget/Button;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/chase/sig/android/view/JPSortableButtonBar;->c:Landroid/widget/Button;

    invoke-direct {p0, v0, v3}, Lcom/chase/sig/android/view/JPSortableButtonBar;->a(Landroid/widget/Button;Z)V

    :cond_1
    move-object v0, v1

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/chase/sig/android/view/JPSortableButtonBar;->c:Landroid/widget/Button;

    iget-object v0, p0, Lcom/chase/sig/android/view/JPSortableButtonBar;->c:Landroid/widget/Button;

    const/4 v5, 0x1

    invoke-direct {p0, v0, v5}, Lcom/chase/sig/android/view/JPSortableButtonBar;->a(Landroid/widget/Button;Z)V

    .line 109
    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/chase/sig/android/view/JPSortableButtonBar;->c:Landroid/widget/Button;

    .line 111
    iget-object v1, p0, Lcom/chase/sig/android/view/JPSortableButtonBar;->c:Landroid/widget/Button;

    iget-boolean v0, p0, Lcom/chase/sig/android/view/JPSortableButtonBar;->d:Z

    if-eqz v0, :cond_3

    const v0, 0x7f020038

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 101
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 111
    :cond_3
    const v0, 0x7f020039

    goto :goto_1

    .line 120
    :cond_4
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2
    .parameter

    .prologue
    .line 156
    if-eqz p1, :cond_0

    instance-of v0, p1, Lcom/chase/sig/android/view/JPSortableButtonBar$JPSortableButtonBarState;

    if-eqz v0, :cond_0

    .line 157
    check-cast p1, Lcom/chase/sig/android/view/JPSortableButtonBar$JPSortableButtonBarState;

    .line 158
    invoke-virtual {p1}, Lcom/chase/sig/android/view/JPSortableButtonBar$JPSortableButtonBarState;->b()Z

    move-result v0

    .line 159
    invoke-virtual {p1}, Lcom/chase/sig/android/view/JPSortableButtonBar$JPSortableButtonBarState;->a()I

    move-result v1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/chase/sig/android/view/JPSortableButtonBar;->a(ILjava/lang/Boolean;)V

    .line 160
    invoke-virtual {p1}, Lcom/chase/sig/android/view/JPSortableButtonBar$JPSortableButtonBarState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/LinearLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 164
    :goto_0
    return-void

    .line 162
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    goto :goto_0
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 4

    .prologue
    .line 144
    invoke-super {p0}, Landroid/widget/LinearLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    .line 145
    iget-object v0, p0, Lcom/chase/sig/android/view/JPSortableButtonBar;->c:Landroid/widget/Button;

    if-eqz v0, :cond_0

    .line 146
    iget-object v0, p0, Lcom/chase/sig/android/view/JPSortableButtonBar;->c:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 147
    new-instance v1, Lcom/chase/sig/android/view/JPSortableButtonBar$JPSortableButtonBarState;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-boolean v3, p0, Lcom/chase/sig/android/view/JPSortableButtonBar;->d:Z

    invoke-direct {v1, v2, v0, v3}, Lcom/chase/sig/android/view/JPSortableButtonBar$JPSortableButtonBarState;-><init>(Landroid/os/Parcelable;IZ)V

    move-object v0, v1

    .line 150
    :goto_0
    return-object v0

    :cond_0
    move-object v0, v2

    goto :goto_0
.end method

.method public setSortableButtonBarListener(Lcom/chase/sig/android/view/JPSortableButtonBar$a;)V
    .locals 0
    .parameter

    .prologue
    .line 86
    iput-object p1, p0, Lcom/chase/sig/android/view/JPSortableButtonBar;->b:Lcom/chase/sig/android/view/JPSortableButtonBar$a;

    .line 87
    return-void
.end method
