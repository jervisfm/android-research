.class Lcom/chase/sig/android/view/JPSpinner$JPSpinnerState;
.super Landroid/view/View$BaseSavedState;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/view/JPSpinner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "JPSpinnerState"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/chase/sig/android/view/JPSpinner$JPSpinnerState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:I

.field private b:Z

.field private c:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 267
    new-instance v0, Lcom/chase/sig/android/view/s;

    invoke-direct {v0}, Lcom/chase/sig/android/view/s;-><init>()V

    sput-object v0, Lcom/chase/sig/android/view/JPSpinner$JPSpinnerState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .parameter

    .prologue
    .line 232
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcel;)V

    .line 233
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/chase/sig/android/view/JPSpinner$JPSpinnerState;->a:I

    .line 234
    const/4 v0, 0x1

    new-array v0, v0, [Z

    .line 235
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readBooleanArray([Z)V

    .line 236
    const/4 v1, 0x0

    aget-boolean v0, v0, v1

    iput-boolean v0, p0, Lcom/chase/sig/android/view/JPSpinner$JPSpinnerState;->b:Z

    .line 237
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 222
    invoke-direct {p0, p1}, Lcom/chase/sig/android/view/JPSpinner$JPSpinnerState;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method constructor <init>(Landroid/os/Parcelable;)V
    .locals 0
    .parameter

    .prologue
    .line 228
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcelable;)V

    .line 229
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 256
    iget v0, p0, Lcom/chase/sig/android/view/JPSpinner$JPSpinnerState;->a:I

    return v0
.end method

.method public final a(I)V
    .locals 0
    .parameter

    .prologue
    .line 252
    iput p1, p0, Lcom/chase/sig/android/view/JPSpinner$JPSpinnerState;->a:I

    .line 253
    return-void
.end method

.method public final a(Z)V
    .locals 0
    .parameter

    .prologue
    .line 264
    iput-boolean p1, p0, Lcom/chase/sig/android/view/JPSpinner$JPSpinnerState;->b:Z

    .line 265
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 260
    iget-boolean v0, p0, Lcom/chase/sig/android/view/JPSpinner$JPSpinnerState;->b:Z

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 4
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 241
    invoke-super {p0, p1, p2}, Landroid/view/View$BaseSavedState;->writeToParcel(Landroid/os/Parcel;I)V

    .line 242
    iget v0, p0, Lcom/chase/sig/android/view/JPSpinner$JPSpinnerState;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 243
    new-array v0, v3, [Z

    iget-boolean v1, p0, Lcom/chase/sig/android/view/JPSpinner$JPSpinnerState;->b:Z

    aput-boolean v1, v0, v2

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBooleanArray([Z)V

    .line 246
    new-array v0, v3, [Z

    iget-boolean v1, p0, Lcom/chase/sig/android/view/JPSpinner$JPSpinnerState;->c:Z

    aput-boolean v1, v0, v2

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBooleanArray([Z)V

    .line 249
    return-void
.end method
