.class public Lcom/chase/sig/android/view/JPSpinner;
.super Landroid/widget/LinearLayout;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/view/JPSpinner$JPSpinnerState;
    }
.end annotation


# instance fields
.field private a:I

.field private b:I

.field private c:Landroid/widget/ListAdapter;

.field private d:Landroid/content/DialogInterface$OnClickListener;

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/view/x;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/lang/String;

.field private g:Landroid/app/Dialog;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .parameter

    .prologue
    .line 41
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 27
    const/4 v0, -0x1

    iput v0, p0, Lcom/chase/sig/android/view/JPSpinner;->a:I

    .line 28
    const/4 v0, 0x0

    iput v0, p0, Lcom/chase/sig/android/view/JPSpinner;->b:I

    .line 43
    invoke-direct {p0, p1}, Lcom/chase/sig/android/view/JPSpinner;->a(Landroid/content/Context;)V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 47
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    const/4 v0, -0x1

    iput v0, p0, Lcom/chase/sig/android/view/JPSpinner;->a:I

    .line 28
    iput v2, p0, Lcom/chase/sig/android/view/JPSpinner;->b:I

    .line 49
    sget-object v0, Lcom/chase/sig/android/e$a;->JPSpinner:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 50
    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/chase/sig/android/view/JPSpinner;->f:Ljava/lang/String;

    .line 51
    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Lcom/chase/sig/android/view/JPSpinner;->b:I

    .line 53
    const-string v0, "jp"

    const-string v1, "hint"

    invoke-interface {p2, v0, v1}, Landroid/util/AttributeSet;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 54
    invoke-direct {p0, p1}, Lcom/chase/sig/android/view/JPSpinner;->a(Landroid/content/Context;)V

    .line 55
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/view/JPSpinner;)Landroid/app/Dialog;
    .locals 1
    .parameter

    .prologue
    .line 23
    iget-object v0, p0, Lcom/chase/sig/android/view/JPSpinner;->g:Landroid/app/Dialog;

    return-object v0
.end method

.method static synthetic a(Lcom/chase/sig/android/view/JPSpinner;Landroid/app/Dialog;)Landroid/app/Dialog;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 23
    iput-object p1, p0, Lcom/chase/sig/android/view/JPSpinner;->g:Landroid/app/Dialog;

    return-object p1
.end method

.method static synthetic a(Lcom/chase/sig/android/view/JPSpinner;Landroid/content/Context;)Landroid/app/Dialog;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/chase/sig/android/view/JPSpinner;->b(Landroid/content/Context;)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;)Landroid/widget/TextView;
    .locals 3
    .parameter

    .prologue
    .line 118
    new-instance v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/chase/sig/android/view/JPSpinner;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 119
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 120
    invoke-virtual {p0}, Lcom/chase/sig/android/view/JPSpinner;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060005

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 121
    const/4 v1, 0x2

    const/high16 v2, 0x4170

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 122
    return-object v0
.end method

.method private a(Landroid/content/Context;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, 0x1

    .line 59
    invoke-virtual {p0, v0}, Lcom/chase/sig/android/view/JPSpinner;->setFocusable(Z)V

    .line 60
    invoke-virtual {p0, v0}, Lcom/chase/sig/android/view/JPSpinner;->setClickable(Z)V

    .line 62
    invoke-virtual {p0}, Lcom/chase/sig/android/view/JPSpinner;->a()V

    .line 64
    new-instance v0, Lcom/chase/sig/android/view/q;

    invoke-direct {v0, p0, p1}, Lcom/chase/sig/android/view/q;-><init>(Lcom/chase/sig/android/view/JPSpinner;Landroid/content/Context;)V

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/view/JPSpinner;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 74
    new-instance v0, Lcom/chase/sig/android/view/r;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/view/r;-><init>(Lcom/chase/sig/android/view/JPSpinner;)V

    iput-object v0, p0, Lcom/chase/sig/android/view/JPSpinner;->d:Landroid/content/DialogInterface$OnClickListener;

    .line 84
    return-void
.end method

.method private b(Landroid/content/Context;)Landroid/app/Dialog;
    .locals 6
    .parameter

    .prologue
    const v5, 0x106000b

    .line 88
    iget-object v0, p0, Lcom/chase/sig/android/view/JPSpinner;->c:Landroid/widget/ListAdapter;

    .line 89
    new-instance v0, Lcom/chase/sig/android/view/k$a;

    invoke-direct {v0, p1}, Lcom/chase/sig/android/view/k$a;-><init>(Landroid/content/Context;)V

    .line 93
    invoke-virtual {p0}, Lcom/chase/sig/android/view/JPSpinner;->getHint()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/chase/sig/android/view/k$a;->b:Ljava/lang/CharSequence;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/chase/sig/android/view/k$a;->i:Z

    .line 94
    iget-object v1, p0, Lcom/chase/sig/android/view/JPSpinner;->c:Landroid/widget/ListAdapter;

    iget-object v2, p0, Lcom/chase/sig/android/view/JPSpinner;->d:Landroid/content/DialogInterface$OnClickListener;

    new-instance v3, Landroid/widget/ListView;

    iget-object v4, v0, Lcom/chase/sig/android/view/k$a;->a:Landroid/content/Context;

    invoke-direct {v3, v4}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    iput-object v3, v0, Lcom/chase/sig/android/view/k$a;->h:Landroid/widget/ListView;

    iget-object v3, v0, Lcom/chase/sig/android/view/k$a;->h:Landroid/widget/ListView;

    const v4, 0x1080062

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setSelector(I)V

    iget-object v3, v0, Lcom/chase/sig/android/view/k$a;->h:Landroid/widget/ListView;

    invoke-virtual {v3, v5}, Landroid/widget/ListView;->setBackgroundResource(I)V

    iget-object v3, v0, Lcom/chase/sig/android/view/k$a;->h:Landroid/widget/ListView;

    iget-object v4, v0, Lcom/chase/sig/android/view/k$a;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setCacheColorHint(I)V

    iput-object v2, v0, Lcom/chase/sig/android/view/k$a;->l:Landroid/content/DialogInterface$OnClickListener;

    iget-object v2, v0, Lcom/chase/sig/android/view/k$a;->h:Landroid/widget/ListView;

    invoke-virtual {v2, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 96
    invoke-virtual {p0}, Lcom/chase/sig/android/view/JPSpinner;->getSelectedItemPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/k$a;->d(I)Lcom/chase/sig/android/view/k;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 112
    const/4 v0, -0x1

    iput v0, p0, Lcom/chase/sig/android/view/JPSpinner;->a:I

    .line 113
    invoke-virtual {p0}, Lcom/chase/sig/android/view/JPSpinner;->removeAllViews()V

    .line 114
    iget-object v0, p0, Lcom/chase/sig/android/view/JPSpinner;->f:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/chase/sig/android/view/JPSpinner;->a(Ljava/lang/String;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/view/JPSpinner;->addView(Landroid/view/View;)V

    .line 115
    return-void
.end method

.method public final a(Lcom/chase/sig/android/view/x;)V
    .locals 1
    .parameter

    .prologue
    .line 127
    iget-object v0, p0, Lcom/chase/sig/android/view/JPSpinner;->e:Ljava/util/List;

    if-nez v0, :cond_0

    .line 128
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/chase/sig/android/view/JPSpinner;->e:Ljava/util/List;

    .line 131
    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/view/JPSpinner;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 132
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 185
    iget v0, p0, Lcom/chase/sig/android/view/JPSpinner;->a:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/chase/sig/android/view/JPSpinner;->g:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/view/JPSpinner;->g:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 218
    iget-object v0, p0, Lcom/chase/sig/android/view/JPSpinner;->g:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 220
    :cond_0
    return-void
.end method

.method public getDialog()Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/chase/sig/android/view/JPSpinner;->g:Landroid/app/Dialog;

    return-object v0
.end method

.method public getHint()Ljava/lang/String;
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/chase/sig/android/view/JPSpinner;->f:Ljava/lang/String;

    return-object v0
.end method

.method public getPosition()I
    .locals 1

    .prologue
    .line 100
    iget v0, p0, Lcom/chase/sig/android/view/JPSpinner;->a:I

    return v0
.end method

.method public getSelectedItem()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 177
    invoke-virtual {p0}, Lcom/chase/sig/android/view/JPSpinner;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 178
    iget-object v0, p0, Lcom/chase/sig/android/view/JPSpinner;->c:Landroid/widget/ListAdapter;

    iget v1, p0, Lcom/chase/sig/android/view/JPSpinner;->a:I

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 180
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSelectedItemPosition()I
    .locals 1

    .prologue
    .line 108
    iget v0, p0, Lcom/chase/sig/android/view/JPSpinner;->a:I

    return v0
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1
    .parameter

    .prologue
    .line 201
    instance-of v0, p1, Lcom/chase/sig/android/view/JPSpinner$JPSpinnerState;

    if-nez v0, :cond_1

    .line 202
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 214
    :cond_0
    :goto_0
    return-void

    .line 206
    :cond_1
    check-cast p1, Lcom/chase/sig/android/view/JPSpinner$JPSpinnerState;

    .line 207
    invoke-virtual {p1}, Lcom/chase/sig/android/view/JPSpinner$JPSpinnerState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/LinearLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 209
    invoke-virtual {p1}, Lcom/chase/sig/android/view/JPSpinner$JPSpinnerState;->a()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/view/JPSpinner;->setSelection(I)V

    .line 210
    invoke-virtual {p1}, Lcom/chase/sig/android/view/JPSpinner$JPSpinnerState;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 211
    invoke-virtual {p0}, Lcom/chase/sig/android/view/JPSpinner;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/chase/sig/android/view/JPSpinner;->b(Landroid/content/Context;)Landroid/app/Dialog;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/view/JPSpinner;->g:Landroid/app/Dialog;

    .line 212
    iget-object v0, p0, Lcom/chase/sig/android/view/JPSpinner;->g:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto :goto_0
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .prologue
    .line 190
    invoke-super {p0}, Landroid/widget/LinearLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 192
    new-instance v1, Lcom/chase/sig/android/view/JPSpinner$JPSpinnerState;

    invoke-direct {v1, v0}, Lcom/chase/sig/android/view/JPSpinner$JPSpinnerState;-><init>(Landroid/os/Parcelable;)V

    .line 193
    iget v0, p0, Lcom/chase/sig/android/view/JPSpinner;->a:I

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/view/JPSpinner$JPSpinnerState;->a(I)V

    .line 194
    iget-object v0, p0, Lcom/chase/sig/android/view/JPSpinner;->g:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/view/JPSpinner;->g:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/chase/sig/android/view/JPSpinner$JPSpinnerState;->a(Z)V

    .line 196
    return-object v1

    .line 194
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 0
    .parameter

    .prologue
    .line 104
    iput-object p1, p0, Lcom/chase/sig/android/view/JPSpinner;->c:Landroid/widget/ListAdapter;

    .line 105
    return-void
.end method

.method public setChoiceMode(I)V
    .locals 0
    .parameter

    .prologue
    .line 281
    iput p1, p0, Lcom/chase/sig/android/view/JPSpinner;->b:I

    .line 282
    return-void
.end method

.method public setDialog(Landroid/app/Dialog;)V
    .locals 0
    .parameter

    .prologue
    .line 173
    iput-object p1, p0, Lcom/chase/sig/android/view/JPSpinner;->g:Landroid/app/Dialog;

    .line 174
    return-void
.end method

.method public setHint(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 161
    iput-object p1, p0, Lcom/chase/sig/android/view/JPSpinner;->f:Ljava/lang/String;

    .line 162
    return-void
.end method

.method public setSelection(I)V
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 136
    const/4 v0, -0x1

    if-ne p1, v0, :cond_1

    .line 137
    invoke-virtual {p0}, Lcom/chase/sig/android/view/JPSpinner;->a()V

    .line 158
    :cond_0
    return-void

    .line 141
    :cond_1
    invoke-virtual {p0}, Lcom/chase/sig/android/view/JPSpinner;->removeAllViews()V

    .line 143
    iget v0, p0, Lcom/chase/sig/android/view/JPSpinner;->b:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 145
    iget-object v0, p0, Lcom/chase/sig/android/view/JPSpinner;->c:Landroid/widget/ListAdapter;

    invoke-interface {v0, p1}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/chase/sig/android/view/JPSpinner;->a(Ljava/lang/String;)Landroid/widget/TextView;

    move-result-object v0

    .line 150
    :goto_0
    invoke-virtual {p0, v0}, Lcom/chase/sig/android/view/JPSpinner;->addView(Landroid/view/View;)V

    .line 151
    iput p1, p0, Lcom/chase/sig/android/view/JPSpinner;->a:I

    .line 153
    iget-object v0, p0, Lcom/chase/sig/android/view/JPSpinner;->e:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 154
    iget-object v0, p0, Lcom/chase/sig/android/view/JPSpinner;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/x;

    .line 155
    invoke-interface {v0, p1}, Lcom/chase/sig/android/view/x;->a(I)V

    goto :goto_1

    .line 147
    :cond_2
    iget-object v0, p0, Lcom/chase/sig/android/view/JPSpinner;->c:Landroid/widget/ListAdapter;

    invoke-interface {v0, p1, v2, v2}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method
