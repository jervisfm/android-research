.class public Lcom/chase/sig/android/view/JPTabWidget;
.super Landroid/widget/LinearLayout;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/view/JPTabWidget$a;,
        Lcom/chase/sig/android/view/JPTabWidget$b;
    }
.end annotation


# instance fields
.field private a:Landroid/view/View$OnClickListener;

.field private b:Lcom/chase/sig/android/view/JPTabWidget$b;

.field private c:Landroid/widget/Button;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .parameter

    .prologue
    .line 29
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 30
    invoke-direct {p0}, Lcom/chase/sig/android/view/JPTabWidget;->c()V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    invoke-direct {p0}, Lcom/chase/sig/android/view/JPTabWidget;->c()V

    .line 36
    return-void
.end method

.method private a(Landroid/widget/Button;Z)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 115
    if-eqz p2, :cond_0

    .line 116
    const v0, 0x7f0200f8

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 117
    invoke-virtual {p0}, Lcom/chase/sig/android/view/JPTabWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0200f9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setTextColor(I)V

    .line 122
    :goto_0
    return-void

    .line 119
    :cond_0
    const v0, 0x7f0200f5

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 120
    invoke-virtual {p0}, Lcom/chase/sig/android/view/JPTabWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0200f6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setTextColor(I)V

    goto :goto_0
.end method

.method private c()V
    .locals 1

    .prologue
    .line 39
    const v0, 0x7f0200f2

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/view/JPTabWidget;->setBackgroundResource(I)V

    .line 41
    new-instance v0, Lcom/chase/sig/android/view/t;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/view/t;-><init>(Lcom/chase/sig/android/view/JPTabWidget;)V

    iput-object v0, p0, Lcom/chase/sig/android/view/JPTabWidget;->a:Landroid/view/View$OnClickListener;

    .line 48
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 67
    invoke-virtual {p0}, Lcom/chase/sig/android/view/JPTabWidget;->removeAllViewsInLayout()V

    .line 68
    return-void
.end method

.method public final a(II)V
    .locals 3
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 58
    invoke-virtual {p0}, Lcom/chase/sig/android/view/JPTabWidget;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0300bd

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 59
    invoke-virtual {p0}, Lcom/chase/sig/android/view/JPTabWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 60
    iget-object v1, p0, Lcom/chase/sig/android/view/JPTabWidget;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 61
    invoke-direct {p0, v0, v2}, Lcom/chase/sig/android/view/JPTabWidget;->a(Landroid/widget/Button;Z)V

    .line 62
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    .line 63
    invoke-virtual {p0, v0}, Lcom/chase/sig/android/view/JPTabWidget;->addView(Landroid/view/View;)V

    .line 64
    return-void
.end method

.method public final a(IZ)V
    .locals 6
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 81
    invoke-virtual {p0}, Lcom/chase/sig/android/view/JPTabWidget;->getChildCount()I

    move-result v3

    move v1, v2

    .line 83
    :goto_0
    if-ge v1, v3, :cond_1

    .line 84
    invoke-virtual {p0, v1}, Lcom/chase/sig/android/view/JPTabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 85
    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v4

    .line 87
    if-eqz v4, :cond_2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 89
    invoke-virtual {p0}, Lcom/chase/sig/android/view/JPTabWidget;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/chase/sig/android/view/JPTabWidget;->c:Landroid/widget/Button;

    invoke-direct {p0, v1, v2}, Lcom/chase/sig/android/view/JPTabWidget;->a(Landroid/widget/Button;Z)V

    :cond_0
    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/chase/sig/android/view/JPTabWidget;->c:Landroid/widget/Button;

    iget-object v0, p0, Lcom/chase/sig/android/view/JPTabWidget;->c:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/chase/sig/android/view/JPTabWidget;->a(Landroid/widget/Button;Z)V

    .line 91
    if-eqz p2, :cond_1

    .line 92
    iget-object v0, p0, Lcom/chase/sig/android/view/JPTabWidget;->b:Lcom/chase/sig/android/view/JPTabWidget$b;

    if-eqz v0, :cond_1

    .line 93
    iget-object v1, p0, Lcom/chase/sig/android/view/JPTabWidget;->b:Lcom/chase/sig/android/view/JPTabWidget$b;

    iget-object v0, p0, Lcom/chase/sig/android/view/JPTabWidget;->c:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v1, v0}, Lcom/chase/sig/android/view/JPTabWidget$b;->a(I)V

    .line 100
    :cond_1
    return-void

    .line 83
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3
    .parameter

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/chase/sig/android/view/JPTabWidget;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0300bc

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 52
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 53
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 54
    invoke-virtual {p0, v0}, Lcom/chase/sig/android/view/JPTabWidget;->addView(Landroid/view/View;)V

    .line 55
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/chase/sig/android/view/JPTabWidget;->c:Landroid/widget/Button;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSelectedTabId()I
    .locals 1

    .prologue
    .line 125
    invoke-virtual {p0}, Lcom/chase/sig/android/view/JPTabWidget;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/view/JPTabWidget;->c:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public setTabListener(Lcom/chase/sig/android/view/JPTabWidget$b;)V
    .locals 0
    .parameter

    .prologue
    .line 76
    iput-object p1, p0, Lcom/chase/sig/android/view/JPTabWidget;->b:Lcom/chase/sig/android/view/JPTabWidget$b;

    .line 77
    return-void
.end method
