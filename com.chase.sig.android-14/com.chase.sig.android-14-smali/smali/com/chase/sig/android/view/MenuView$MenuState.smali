.class Lcom/chase/sig/android/view/MenuView$MenuState;
.super Landroid/view/View$BaseSavedState;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/view/MenuView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MenuState"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/chase/sig/android/view/MenuView$MenuState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Z

.field private b:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 184
    new-instance v0, Lcom/chase/sig/android/view/v;

    invoke-direct {v0}, Lcom/chase/sig/android/view/v;-><init>()V

    sput-object v0, Lcom/chase/sig/android/view/MenuView$MenuState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 171
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcel;)V

    .line 163
    iput-boolean v0, p0, Lcom/chase/sig/android/view/MenuView$MenuState;->a:Z

    .line 164
    iput v0, p0, Lcom/chase/sig/android/view/MenuView$MenuState;->b:I

    .line 172
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/chase/sig/android/view/MenuView$MenuState;->a:Z

    .line 173
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/chase/sig/android/view/MenuView$MenuState;->b:I

    .line 174
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcelable;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 167
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcelable;)V

    .line 163
    iput-boolean v0, p0, Lcom/chase/sig/android/view/MenuView$MenuState;->a:Z

    .line 164
    iput v0, p0, Lcom/chase/sig/android/view/MenuView$MenuState;->b:I

    .line 168
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/view/MenuView$MenuState;I)I
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 162
    iput p1, p0, Lcom/chase/sig/android/view/MenuView$MenuState;->b:I

    return p1
.end method

.method static synthetic a(Lcom/chase/sig/android/view/MenuView$MenuState;)Z
    .locals 1
    .parameter

    .prologue
    .line 162
    iget-boolean v0, p0, Lcom/chase/sig/android/view/MenuView$MenuState;->a:Z

    return v0
.end method

.method static synthetic a(Lcom/chase/sig/android/view/MenuView$MenuState;Z)Z
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 162
    iput-boolean p1, p0, Lcom/chase/sig/android/view/MenuView$MenuState;->a:Z

    return p1
.end method

.method static synthetic b(Lcom/chase/sig/android/view/MenuView$MenuState;)I
    .locals 1
    .parameter

    .prologue
    .line 162
    iget v0, p0, Lcom/chase/sig/android/view/MenuView$MenuState;->b:I

    return v0
.end method


# virtual methods
.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 178
    invoke-super {p0, p1, p2}, Landroid/view/View$BaseSavedState;->writeToParcel(Landroid/os/Parcel;I)V

    .line 179
    iget-boolean v0, p0, Lcom/chase/sig/android/view/MenuView$MenuState;->a:Z

    invoke-static {v0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 180
    iget v0, p0, Lcom/chase/sig/android/view/MenuView$MenuState;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 181
    return-void
.end method
