.class public final Lcom/chase/sig/android/view/MenuView;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/view/MenuView$MenuState;
    }
.end annotation


# instance fields
.field private a:[I

.field private b:Landroid/view/animation/Animation;

.field private c:Landroid/view/animation/Animation;

.field private d:Landroid/view/View;

.field private e:Landroid/widget/ImageButton;

.field private f:Z

.field private g:I


# direct methods
.method private e()V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 62
    iget-boolean v0, p0, Lcom/chase/sig/android/view/MenuView;->f:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/chase/sig/android/view/MenuView;->setVisibility(I)V

    .line 63
    iget-boolean v0, p0, Lcom/chase/sig/android/view/MenuView;->f:Z

    if-eqz v0, :cond_1

    move v0, v2

    :goto_1
    invoke-direct {p0, v0}, Lcom/chase/sig/android/view/MenuView;->setMenuButtonLevel(I)V

    .line 64
    iget-boolean v0, p0, Lcom/chase/sig/android/view/MenuView;->f:Z

    if-eqz v0, :cond_2

    iget-object v4, p0, Lcom/chase/sig/android/view/MenuView;->a:[I

    array-length v5, v4

    move v3, v1

    :goto_2
    if-ge v3, v5, :cond_2

    aget v0, v4, v3

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/view/MenuView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-static {v0, v6}, Lcom/chase/sig/android/view/an;->a(Landroid/view/View;Landroid/view/View;)V

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    .line 62
    :cond_0
    const/16 v0, 0x8

    goto :goto_0

    :cond_1
    move v0, v1

    .line 63
    goto :goto_1

    .line 65
    :cond_2
    iget v5, p0, Lcom/chase/sig/android/view/MenuView;->g:I

    iget-object v6, p0, Lcom/chase/sig/android/view/MenuView;->a:[I

    array-length v7, v6

    move v4, v1

    :goto_3
    if-ge v4, v7, :cond_4

    aget v3, v6, v4

    invoke-virtual {p0, v3}, Lcom/chase/sig/android/view/MenuView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CompoundButton;

    if-ne v3, v5, :cond_3

    move v3, v2

    :goto_4
    invoke-virtual {v0, v3}, Landroid/widget/CompoundButton;->setChecked(Z)V

    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_3

    :cond_3
    move v3, v1

    goto :goto_4

    .line 66
    :cond_4
    invoke-virtual {p0}, Lcom/chase/sig/android/view/MenuView;->requestLayout()V

    .line 67
    return-void
.end method

.method private getContainer()Landroid/view/View;
    .locals 2

    .prologue
    .line 52
    iget-object v0, p0, Lcom/chase/sig/android/view/MenuView;->d:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/view/MenuView;->d:Landroid/view/View;

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/chase/sig/android/view/MenuView;->getRootView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f090110

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/view/MenuView;->d:Landroid/view/View;

    goto :goto_0
.end method

.method private getMenuButton()Landroid/widget/ImageButton;
    .locals 2

    .prologue
    .line 57
    iget-object v0, p0, Lcom/chase/sig/android/view/MenuView;->e:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/view/MenuView;->e:Landroid/widget/ImageButton;

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/chase/sig/android/view/MenuView;->getRootView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f09010e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/chase/sig/android/view/MenuView;->e:Landroid/widget/ImageButton;

    goto :goto_0
.end method

.method private setMenuButtonLevel(I)V
    .locals 1
    .parameter

    .prologue
    .line 97
    invoke-direct {p0}, Lcom/chase/sig/android/view/MenuView;->getMenuButton()Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setLevel(I)Z

    .line 98
    invoke-direct {p0}, Lcom/chase/sig/android/view/MenuView;->getMenuButton()Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setLevel(I)Z

    .line 99
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 102
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/chase/sig/android/view/MenuView;->f:Z

    .line 103
    invoke-direct {p0}, Lcom/chase/sig/android/view/MenuView;->getContainer()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/chase/sig/android/view/MenuView;->c:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 104
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 107
    iget-boolean v0, p0, Lcom/chase/sig/android/view/MenuView;->f:Z

    if-eqz v0, :cond_0

    .line 108
    invoke-virtual {p0}, Lcom/chase/sig/android/view/MenuView;->a()V

    .line 112
    :goto_0
    return-void

    .line 110
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/chase/sig/android/view/MenuView;->f:Z

    invoke-direct {p0}, Lcom/chase/sig/android/view/MenuView;->e()V

    invoke-direct {p0}, Lcom/chase/sig/android/view/MenuView;->getContainer()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/chase/sig/android/view/MenuView;->b:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 115
    const v0, 0x7f090114

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/view/MenuView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 116
    return-void
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 156
    iget-boolean v0, p0, Lcom/chase/sig/android/view/MenuView;->f:Z

    return v0
.end method

.method public final dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .parameter

    .prologue
    .line 134
    iget-boolean v0, p0, Lcom/chase/sig/android/view/MenuView;->f:Z

    if-eqz v0, :cond_0

    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getSelectedOptionId()I
    .locals 1

    .prologue
    .line 119
    iget v0, p0, Lcom/chase/sig/android/view/MenuView;->g:I

    return v0
.end method

.method protected final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1
    .parameter

    .prologue
    .line 148
    check-cast p1, Lcom/chase/sig/android/view/MenuView$MenuState;

    .line 149
    invoke-virtual {p1}, Lcom/chase/sig/android/view/MenuView$MenuState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/RelativeLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 150
    invoke-static {p1}, Lcom/chase/sig/android/view/MenuView$MenuState;->a(Lcom/chase/sig/android/view/MenuView$MenuState;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/chase/sig/android/view/MenuView;->f:Z

    .line 151
    invoke-static {p1}, Lcom/chase/sig/android/view/MenuView$MenuState;->b(Lcom/chase/sig/android/view/MenuView$MenuState;)I

    move-result v0

    iput v0, p0, Lcom/chase/sig/android/view/MenuView;->g:I

    .line 152
    invoke-direct {p0}, Lcom/chase/sig/android/view/MenuView;->e()V

    .line 153
    return-void
.end method

.method protected final onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .prologue
    .line 139
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 140
    new-instance v1, Lcom/chase/sig/android/view/MenuView$MenuState;

    invoke-direct {v1, v0}, Lcom/chase/sig/android/view/MenuView$MenuState;-><init>(Landroid/os/Parcelable;)V

    .line 141
    iget-boolean v0, p0, Lcom/chase/sig/android/view/MenuView;->f:Z

    invoke-static {v1, v0}, Lcom/chase/sig/android/view/MenuView$MenuState;->a(Lcom/chase/sig/android/view/MenuView$MenuState;Z)Z

    .line 142
    iget v0, p0, Lcom/chase/sig/android/view/MenuView;->g:I

    invoke-static {v1, v0}, Lcom/chase/sig/android/view/MenuView$MenuState;->a(Lcom/chase/sig/android/view/MenuView$MenuState;I)I

    .line 143
    return-object v1
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .parameter

    .prologue
    .line 128
    invoke-virtual {p0}, Lcom/chase/sig/android/view/MenuView;->a()V

    .line 129
    const/4 v0, 0x1

    return v0
.end method

.method public final setSelectedOptionId(I)V
    .locals 0
    .parameter

    .prologue
    .line 123
    iput p1, p0, Lcom/chase/sig/android/view/MenuView;->g:I

    .line 124
    return-void
.end method
