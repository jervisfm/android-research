.class public Lcom/chase/sig/android/view/ScaledViewGroup;
.super Landroid/widget/LinearLayout;
.source "SourceFile"


# static fields
.field static final synthetic a:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    const-class v0, Lcom/chase/sig/android/view/ScaledViewGroup;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/chase/sig/android/view/ScaledViewGroup;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .parameter

    .prologue
    .line 15
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 16
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 11
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 12
    return-void
.end method


# virtual methods
.method protected onLayout(ZIIII)V
    .locals 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 21
    invoke-virtual {p0}, Lcom/chase/sig/android/view/ScaledViewGroup;->getMeasuredHeight()I

    move-result v0

    .line 22
    invoke-virtual {p0}, Lcom/chase/sig/android/view/ScaledViewGroup;->getMeasuredWidth()I

    move-result v2

    .line 23
    invoke-virtual {p0}, Lcom/chase/sig/android/view/ScaledViewGroup;->getChildCount()I

    move-result v3

    .line 24
    div-int v4, v0, v3

    move v0, v1

    .line 26
    :goto_0
    if-ge v0, v3, :cond_0

    .line 27
    invoke-virtual {p0, v0}, Lcom/chase/sig/android/view/ScaledViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 28
    mul-int v6, v0, v4

    mul-int v7, v0, v4

    add-int/2addr v7, v4

    invoke-virtual {v5, v1, v6, v2, v7}, Landroid/view/View;->layout(IIII)V

    .line 26
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 30
    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 9
    .parameter
    .parameter

    .prologue
    const/high16 v8, 0x4000

    .line 35
    sget-boolean v0, Lcom/chase/sig/android/view/ScaledViewGroup;->a:Z

    if-nez v0, :cond_0

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 36
    :cond_0
    sget-boolean v0, Lcom/chase/sig/android/view/ScaledViewGroup;->a:Z

    if-nez v0, :cond_1

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 38
    :cond_1
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-virtual {p0}, Lcom/chase/sig/android/view/ScaledViewGroup;->getPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/chase/sig/android/view/ScaledViewGroup;->getPaddingRight()I

    move-result v1

    sub-int v1, v0, v1

    .line 40
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-virtual {p0}, Lcom/chase/sig/android/view/ScaledViewGroup;->getPaddingTop()I

    move-result v2

    sub-int/2addr v0, v2

    invoke-virtual {p0}, Lcom/chase/sig/android/view/ScaledViewGroup;->getPaddingBottom()I

    move-result v2

    sub-int v2, v0, v2

    .line 42
    invoke-virtual {p0}, Lcom/chase/sig/android/view/ScaledViewGroup;->getChildCount()I

    move-result v3

    .line 43
    div-int v4, v2, v3

    .line 44
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_3

    .line 45
    invoke-virtual {p0, v0}, Lcom/chase/sig/android/view/ScaledViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 47
    invoke-virtual {v5}, Landroid/view/View;->getVisibility()I

    move-result v6

    const/16 v7, 0x8

    if-eq v6, v7, :cond_2

    .line 48
    invoke-static {v1, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-static {v4, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    invoke-virtual {v5, v6, v7}, Landroid/view/View;->measure(II)V

    .line 44
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 53
    :cond_3
    invoke-virtual {p0, v1, v2}, Lcom/chase/sig/android/view/ScaledViewGroup;->setMeasuredDimension(II)V

    .line 54
    return-void
.end method
