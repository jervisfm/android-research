.class public Lcom/chase/sig/android/view/TickerValueTextView;
.super Landroid/widget/TextView;
.source "SourceFile"


# static fields
.field private static final c:Ljava/lang/Object;


# instance fields
.field private a:Z

.field private b:Z

.field private d:Landroid/text/Spannable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-string v0, "%"

    sput-object v0, Lcom/chase/sig/android/view/TickerValueTextView;->c:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 35
    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 27
    iput-boolean v0, p0, Lcom/chase/sig/android/view/TickerValueTextView;->a:Z

    .line 28
    iput-boolean v0, p0, Lcom/chase/sig/android/view/TickerValueTextView;->b:Z

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 39
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    iput-boolean v0, p0, Lcom/chase/sig/android/view/TickerValueTextView;->a:Z

    .line 28
    iput-boolean v0, p0, Lcom/chase/sig/android/view/TickerValueTextView;->b:Z

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 43
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 27
    iput-boolean v0, p0, Lcom/chase/sig/android/view/TickerValueTextView;->a:Z

    .line 28
    iput-boolean v0, p0, Lcom/chase/sig/android/view/TickerValueTextView;->b:Z

    .line 44
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/view/TickerValueTextView;I)Landroid/graphics/drawable/Drawable;
    .locals 4
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 17
    invoke-virtual {p0}, Lcom/chase/sig/android/view/TickerValueTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/chase/sig/android/util/Dollar;Lcom/chase/sig/android/util/Dollar;Ljava/lang/String;)V
    .locals 5
    .parameter
    .parameter
    .parameter

    .prologue
    .line 65
    new-instance v0, Landroid/text/SpannableString;

    invoke-virtual {p1}, Lcom/chase/sig/android/util/Dollar;->f()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    iput-object v0, p0, Lcom/chase/sig/android/view/TickerValueTextView;->d:Landroid/text/Spannable;

    .line 66
    iget-object v0, p0, Lcom/chase/sig/android/view/TickerValueTextView;->d:Landroid/text/Spannable;

    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    invoke-virtual {p0}, Lcom/chase/sig/android/view/TickerValueTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f06002b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/chase/sig/android/util/Dollar;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v4, 0x21

    invoke-interface {v0, v1, v2, v3, v4}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 69
    invoke-static {p3}, Lcom/chase/sig/android/util/s;->C(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p2, v0}, Lcom/chase/sig/android/view/TickerValueTextView;->a(Lcom/chase/sig/android/util/Dollar;Ljava/lang/String;)V

    .line 71
    return-void
.end method

.method public final a(Lcom/chase/sig/android/util/Dollar;Ljava/lang/String;)V
    .locals 6
    .parameter
    .parameter

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 98
    if-nez p1, :cond_0

    .line 157
    :goto_0
    return-void

    .line 103
    :cond_0
    const v1, 0x7f060010

    .line 105
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 107
    iget-object v2, p0, Lcom/chase/sig/android/view/TickerValueTextView;->d:Landroid/text/Spannable;

    if-eqz v2, :cond_3

    .line 108
    iget-object v2, p0, Lcom/chase/sig/android/view/TickerValueTextView;->d:Landroid/text/Spannable;

    invoke-virtual {p0, v2}, Lcom/chase/sig/android/view/TickerValueTextView;->setText(Ljava/lang/CharSequence;)V

    .line 109
    const-string v2, "&nbsp;&nbsp;&nbsp;"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 114
    :goto_1
    invoke-virtual {p1}, Lcom/chase/sig/android/util/Dollar;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 116
    invoke-static {p2}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 117
    const-string v2, "&nbsp;&nbsp;&nbsp;"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 118
    invoke-virtual {v0, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 119
    sget-object v2, Lcom/chase/sig/android/view/TickerValueTextView;->c:Ljava/lang/Object;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    .line 122
    :cond_1
    invoke-virtual {p1}, Lcom/chase/sig/android/util/Dollar;->c()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 123
    const v1, 0x7f06001f

    .line 124
    const-string v2, "<img src=\"%s\" />"

    new-array v3, v4, [Ljava/lang/Object;

    const-string v4, "positive"

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 143
    :cond_2
    :goto_2
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lcom/chase/sig/android/view/al;

    invoke-direct {v2, p0}, Lcom/chase/sig/android/view/al;-><init>(Lcom/chase/sig/android/view/TickerValueTextView;)V

    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;Landroid/text/Html$ImageGetter;Landroid/text/Html$TagHandler;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/view/TickerValueTextView;->append(Ljava/lang/CharSequence;)V

    .line 156
    invoke-virtual {p0}, Lcom/chase/sig/android/view/TickerValueTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/view/TickerValueTextView;->setTextColor(I)V

    goto :goto_0

    .line 111
    :cond_3
    const-string v2, ""

    invoke-virtual {p0, v2}, Lcom/chase/sig/android/view/TickerValueTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 125
    :cond_4
    invoke-virtual {p1}, Lcom/chase/sig/android/util/Dollar;->d()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 126
    const v1, 0x7f06001e

    .line 127
    const-string v2, "<img src=\"%s\" />"

    new-array v3, v4, [Ljava/lang/Object;

    const-string v4, "negative"

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 128
    :cond_5
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/chase/sig/android/util/s;->l(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 129
    iget-boolean v2, p0, Lcom/chase/sig/android/view/TickerValueTextView;->a:Z

    if-eqz v2, :cond_6

    .line 130
    invoke-virtual {p0}, Lcom/chase/sig/android/view/TickerValueTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070087

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 134
    :goto_3
    iget-boolean v2, p0, Lcom/chase/sig/android/view/TickerValueTextView;->b:Z

    if-eqz v2, :cond_2

    .line 135
    const-string v2, "<img src=\"%s\" />"

    new-array v3, v4, [Ljava/lang/Object;

    const-string v4, "empty"

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 132
    :cond_6
    new-instance v0, Ljava/lang/StringBuffer;

    const-string v2, "--"

    invoke-direct {v0, v2}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    goto :goto_3

    .line 138
    :cond_7
    iget-boolean v2, p0, Lcom/chase/sig/android/view/TickerValueTextView;->b:Z

    if-eqz v2, :cond_2

    .line 139
    const-string v2, "<img src=\"%s\" />"

    new-array v3, v4, [Ljava/lang/Object;

    const-string v4, "empty"

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_2
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .parameter
    .parameter

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 164
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 168
    const v0, 0x7f060010

    .line 170
    invoke-static {p1}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 171
    invoke-virtual {v1, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 172
    sget-object v2, Lcom/chase/sig/android/view/TickerValueTextView;->c:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    .line 175
    :cond_0
    const-string v2, "up"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 176
    const v0, 0x7f06001f

    .line 177
    const-string v2, "<img src=\"%s\" />"

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "positive"

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 187
    :cond_1
    :goto_0
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/chase/sig/android/view/am;

    invoke-direct {v2, p0}, Lcom/chase/sig/android/view/am;-><init>(Lcom/chase/sig/android/view/TickerValueTextView;)V

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;Landroid/text/Html$ImageGetter;Landroid/text/Html$TagHandler;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/chase/sig/android/view/TickerValueTextView;->setText(Ljava/lang/CharSequence;)V

    .line 200
    invoke-virtual {p0}, Lcom/chase/sig/android/view/TickerValueTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/view/TickerValueTextView;->setTextColor(I)V

    .line 201
    return-void

    .line 178
    :cond_2
    const-string v2, "down"

    invoke-virtual {v2, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 179
    const v0, 0x7f06001e

    .line 180
    const-string v2, "<img src=\"%s\" />"

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "negative"

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 182
    :cond_3
    iget-boolean v2, p0, Lcom/chase/sig/android/view/TickerValueTextView;->b:Z

    if-eqz v2, :cond_1

    .line 183
    const-string v2, "<img src=\"%s\" />"

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "empty"

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 5
    .parameter
    .parameter

    .prologue
    .line 74
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0, p1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 76
    if-eqz p2, :cond_0

    .line 77
    sget-object v1, Lcom/chase/sig/android/view/TickerValueTextView;->c:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    .line 80
    :cond_0
    const-string v1, "<img src=\"%s\" />"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "empty"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 82
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/chase/sig/android/view/ak;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/view/ak;-><init>(Lcom/chase/sig/android/view/TickerValueTextView;)V

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;Landroid/text/Html$ImageGetter;Landroid/text/Html$TagHandler;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/view/TickerValueTextView;->append(Ljava/lang/CharSequence;)V

    .line 94
    return-void
.end method

.method public setColor(I)V
    .locals 0
    .parameter

    .prologue
    .line 204
    invoke-virtual {p0, p1}, Lcom/chase/sig/android/view/TickerValueTextView;->setTextColor(I)V

    .line 205
    return-void
.end method

.method public setDisplayPlaceholder(Z)V
    .locals 0
    .parameter

    .prologue
    .line 57
    iput-boolean p1, p0, Lcom/chase/sig/android/view/TickerValueTextView;->b:Z

    .line 58
    return-void
.end method

.method public setShowNotApplicable(Z)V
    .locals 0
    .parameter

    .prologue
    .line 53
    iput-boolean p1, p0, Lcom/chase/sig/android/view/TickerValueTextView;->a:Z

    .line 54
    return-void
.end method

.method public setTickerValue(Lcom/chase/sig/android/util/Dollar;)V
    .locals 1
    .parameter

    .prologue
    .line 61
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/chase/sig/android/view/TickerValueTextView;->a(Lcom/chase/sig/android/util/Dollar;Ljava/lang/String;)V

    .line 62
    return-void
.end method
