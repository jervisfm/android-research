.class public final Lcom/chase/sig/android/view/aa;
.super Landroid/widget/LinearLayout;
.source "SourceFile"


# instance fields
.field private final a:Landroid/view/View;

.field private b:Landroid/view/LayoutInflater;

.field private final c:Lcom/chase/sig/android/ChaseApplication;


# direct methods
.method private constructor <init>(Landroid/content/Context;Lcom/chase/sig/android/ChaseApplication;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 53
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 55
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/chase/sig/android/view/aa;->b:Landroid/view/LayoutInflater;

    .line 56
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/view/aa;->setOrientation(I)V

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/view/aa;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/chase/sig/android/view/aa;->b:Landroid/view/LayoutInflater;

    const v1, 0x7f030079

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/view/aa;->a:Landroid/view/View;

    .line 57
    iput-object p2, p0, Lcom/chase/sig/android/view/aa;->c:Lcom/chase/sig/android/ChaseApplication;

    .line 58
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/chase/sig/android/domain/QuickPayAcceptMoneyTransaction;Lcom/chase/sig/android/ChaseApplication;)V
    .locals 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 134
    invoke-direct {p0, p1, p3}, Lcom/chase/sig/android/view/aa;-><init>(Landroid/content/Context;Lcom/chase/sig/android/ChaseApplication;)V

    .line 136
    invoke-virtual {p2}, Lcom/chase/sig/android/domain/QuickPayAcceptMoneyTransaction;->y()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 137
    const/16 v0, 0x8

    new-array v1, v0, [Landroid/view/View;

    invoke-direct {p0, p2, v2}, Lcom/chase/sig/android/view/aa;->b(Lcom/chase/sig/android/domain/h;Z)Landroid/view/View;

    move-result-object v0

    aput-object v0, v1, v2

    invoke-direct {p0, p2, v3}, Lcom/chase/sig/android/view/aa;->a(Lcom/chase/sig/android/domain/h;Z)Landroid/view/View;

    move-result-object v0

    aput-object v0, v1, v3

    invoke-direct {p0, p2}, Lcom/chase/sig/android/view/aa;->c(Lcom/chase/sig/android/domain/h;)Landroid/view/View;

    move-result-object v0

    aput-object v0, v1, v4

    invoke-direct {p0, p2}, Lcom/chase/sig/android/view/aa;->d(Lcom/chase/sig/android/domain/QuickPayTransaction;)Landroid/view/View;

    move-result-object v0

    aput-object v0, v1, v5

    const-string v0, "Pay to"

    invoke-direct {p0, v0, p2}, Lcom/chase/sig/android/view/aa;->a(Ljava/lang/String;Lcom/chase/sig/android/domain/h;)Landroid/view/View;

    move-result-object v0

    aput-object v0, v1, v6

    const/4 v0, 0x5

    invoke-direct {p0, p2}, Lcom/chase/sig/android/view/aa;->d(Lcom/chase/sig/android/domain/h;)Lcom/chase/sig/android/view/y;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x6

    invoke-direct {p0, p2}, Lcom/chase/sig/android/view/aa;->e(Lcom/chase/sig/android/domain/h;)Lcom/chase/sig/android/view/y;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v2, 0x7

    invoke-virtual {p2}, Lcom/chase/sig/android/domain/QuickPayAcceptMoneyTransaction;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p2}, Lcom/chase/sig/android/view/aa;->a(Lcom/chase/sig/android/domain/QuickPayAcceptMoneyTransaction;)Landroid/view/View;

    move-result-object v0

    :goto_0
    aput-object v0, v1, v2

    invoke-direct {p0, v1}, Lcom/chase/sig/android/view/aa;->a([Landroid/view/View;)V

    .line 151
    :goto_1
    return-void

    .line 137
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 145
    :cond_1
    const/4 v0, 0x7

    new-array v0, v0, [Landroid/view/View;

    invoke-direct {p0, p2, v2}, Lcom/chase/sig/android/view/aa;->a(Lcom/chase/sig/android/domain/h;Z)Landroid/view/View;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-direct {p0, p2}, Lcom/chase/sig/android/view/aa;->c(Lcom/chase/sig/android/domain/h;)Landroid/view/View;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-direct {p0, p2}, Lcom/chase/sig/android/view/aa;->d(Lcom/chase/sig/android/domain/QuickPayTransaction;)Landroid/view/View;

    move-result-object v1

    aput-object v1, v0, v4

    const-string v1, "Pay to"

    invoke-direct {p0, v1, p2}, Lcom/chase/sig/android/view/aa;->a(Ljava/lang/String;Lcom/chase/sig/android/domain/h;)Landroid/view/View;

    move-result-object v1

    aput-object v1, v0, v5

    invoke-direct {p0, p2}, Lcom/chase/sig/android/view/aa;->d(Lcom/chase/sig/android/domain/h;)Lcom/chase/sig/android/view/y;

    move-result-object v1

    aput-object v1, v0, v6

    const/4 v1, 0x5

    invoke-direct {p0, p2}, Lcom/chase/sig/android/view/aa;->e(Lcom/chase/sig/android/domain/h;)Lcom/chase/sig/android/view/y;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    invoke-direct {p0, p2}, Lcom/chase/sig/android/view/aa;->a(Lcom/chase/sig/android/domain/QuickPayAcceptMoneyTransaction;)Landroid/view/View;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lcom/chase/sig/android/view/aa;->a([Landroid/view/View;)V

    goto :goto_1
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/chase/sig/android/domain/QuickPayActivityItem;ZLcom/chase/sig/android/ChaseApplication;)V
    .locals 9
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 63
    invoke-direct {p0, p1, p4}, Lcom/chase/sig/android/view/aa;-><init>(Landroid/content/Context;Lcom/chase/sig/android/ChaseApplication;)V

    .line 65
    sget-object v0, Lcom/chase/sig/android/domain/QuickPayActivityType;->a:Lcom/chase/sig/android/domain/QuickPayActivityType;

    invoke-virtual {v0, p2}, Lcom/chase/sig/android/domain/QuickPayActivityType;->a(Lcom/chase/sig/android/domain/QuickPayActivityItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 66
    const/16 v0, 0xa

    new-array v4, v0, [Landroid/view/View;

    invoke-direct {p0, p2, p3}, Lcom/chase/sig/android/view/aa;->b(Lcom/chase/sig/android/domain/h;Z)Landroid/view/View;

    move-result-object v0

    aput-object v0, v4, v2

    if-nez p3, :cond_5

    move v0, v1

    :goto_0
    invoke-direct {p0, p2, v0}, Lcom/chase/sig/android/view/aa;->a(Lcom/chase/sig/android/domain/h;Z)Landroid/view/View;

    move-result-object v0

    aput-object v0, v4, v1

    invoke-direct {p0, p2}, Lcom/chase/sig/android/view/aa;->c(Lcom/chase/sig/android/domain/h;)Landroid/view/View;

    move-result-object v0

    aput-object v0, v4, v7

    const-string v0, "Pay To"

    invoke-direct {p0, v0, p2}, Lcom/chase/sig/android/view/aa;->a(Ljava/lang/String;Lcom/chase/sig/android/domain/h;)Landroid/view/View;

    move-result-object v0

    aput-object v0, v4, v8

    const/4 v5, 0x4

    invoke-virtual {p2}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->x()Z

    move-result v0

    if-nez v0, :cond_7

    invoke-virtual {p2}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->s()Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-virtual {p2}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->s()Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->o(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p2}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->s()Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->j(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_6

    move v0, v1

    :goto_1
    if-eqz v0, :cond_7

    const-string v0, "Received On"

    invoke-virtual {p2}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->s()Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v6

    invoke-virtual {v6}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/chase/sig/android/util/s;->j(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v0, v6}, Lcom/chase/sig/android/view/aa;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/view/y;

    move-result-object v0

    :goto_2
    aput-object v0, v4, v5

    const/4 v5, 0x5

    invoke-virtual {p2}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->x()Z

    move-result v0

    if-eqz v0, :cond_8

    move-object v0, v3

    :goto_3
    aput-object v0, v4, v5

    const/4 v0, 0x6

    invoke-direct {p0, p2}, Lcom/chase/sig/android/view/aa;->d(Lcom/chase/sig/android/domain/h;)Lcom/chase/sig/android/view/y;

    move-result-object v5

    aput-object v5, v4, v0

    const/4 v0, 0x7

    invoke-direct {p0, p2}, Lcom/chase/sig/android/view/aa;->a(Lcom/chase/sig/android/domain/QuickPayActivityItem;)Landroid/view/View;

    move-result-object v5

    aput-object v5, v4, v0

    const/16 v5, 0x8

    if-eqz p3, :cond_9

    invoke-direct {p0, p2}, Lcom/chase/sig/android/view/aa;->e(Lcom/chase/sig/android/domain/h;)Lcom/chase/sig/android/view/y;

    move-result-object v0

    :goto_4
    aput-object v0, v4, v5

    const/16 v5, 0x9

    if-eqz p3, :cond_a

    new-array v0, v1, [Lcom/chase/sig/android/domain/QuickPayActivityItem;

    aput-object p2, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    new-instance v6, Lcom/chase/sig/android/domain/QuickPayAcceptMoneyTransaction;

    invoke-direct {v6}, Lcom/chase/sig/android/domain/QuickPayAcceptMoneyTransaction;-><init>()V

    invoke-static {v0, v6}, Lcom/chase/sig/android/activity/QuickPayViewTodoListDetailActivity;->a(Ljava/util/List;Lcom/chase/sig/android/domain/QuickPayTransaction;)Lcom/chase/sig/android/domain/QuickPayTransaction;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/QuickPayAcceptMoneyTransaction;

    invoke-direct {p0, v0}, Lcom/chase/sig/android/view/aa;->a(Lcom/chase/sig/android/domain/QuickPayAcceptMoneyTransaction;)Landroid/view/View;

    move-result-object v0

    :goto_5
    aput-object v0, v4, v5

    invoke-direct {p0, v4}, Lcom/chase/sig/android/view/aa;->a([Landroid/view/View;)V

    .line 75
    :cond_0
    sget-object v0, Lcom/chase/sig/android/domain/QuickPayActivityType;->d:Lcom/chase/sig/android/domain/QuickPayActivityType;

    invoke-virtual {v0, p2}, Lcom/chase/sig/android/domain/QuickPayActivityType;->a(Lcom/chase/sig/android/domain/QuickPayActivityItem;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 76
    const/4 v0, 0x7

    new-array v4, v0, [Landroid/view/View;

    invoke-direct {p0, p2, p3}, Lcom/chase/sig/android/view/aa;->b(Lcom/chase/sig/android/domain/h;Z)Landroid/view/View;

    move-result-object v0

    aput-object v0, v4, v2

    invoke-direct {p0, p2}, Lcom/chase/sig/android/view/aa;->c(Lcom/chase/sig/android/domain/h;)Landroid/view/View;

    move-result-object v0

    aput-object v0, v4, v1

    invoke-direct {p0, p2, p3}, Lcom/chase/sig/android/view/aa;->a(Lcom/chase/sig/android/domain/QuickPayActivityItem;Z)Landroid/view/View;

    move-result-object v0

    aput-object v0, v4, v7

    if-nez p3, :cond_b

    move v0, v1

    :goto_6
    invoke-direct {p0, p2, v0}, Lcom/chase/sig/android/view/aa;->a(Lcom/chase/sig/android/domain/h;Z)Landroid/view/View;

    move-result-object v0

    aput-object v0, v4, v8

    const/4 v0, 0x4

    invoke-direct {p0, p2}, Lcom/chase/sig/android/view/aa;->b(Lcom/chase/sig/android/domain/QuickPayActivityItem;)Lcom/chase/sig/android/view/y;

    move-result-object v5

    aput-object v5, v4, v0

    const/4 v0, 0x5

    invoke-direct {p0, p2}, Lcom/chase/sig/android/view/aa;->d(Lcom/chase/sig/android/domain/h;)Lcom/chase/sig/android/view/y;

    move-result-object v5

    aput-object v5, v4, v0

    const/4 v0, 0x6

    invoke-direct {p0, p2}, Lcom/chase/sig/android/view/aa;->a(Lcom/chase/sig/android/domain/QuickPayActivityItem;)Landroid/view/View;

    move-result-object v5

    aput-object v5, v4, v0

    invoke-direct {p0, v4}, Lcom/chase/sig/android/view/aa;->a([Landroid/view/View;)V

    .line 82
    :cond_1
    sget-object v0, Lcom/chase/sig/android/domain/QuickPayActivityType;->b:Lcom/chase/sig/android/domain/QuickPayActivityType;

    invoke-virtual {v0, p2}, Lcom/chase/sig/android/domain/QuickPayActivityType;->a(Lcom/chase/sig/android/domain/QuickPayActivityItem;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 83
    const/16 v0, 0x9

    new-array v4, v0, [Landroid/view/View;

    invoke-direct {p0, p2, p3}, Lcom/chase/sig/android/view/aa;->b(Lcom/chase/sig/android/domain/h;Z)Landroid/view/View;

    move-result-object v0

    aput-object v0, v4, v2

    invoke-direct {p0, p2}, Lcom/chase/sig/android/view/aa;->b(Lcom/chase/sig/android/domain/h;)Landroid/view/View;

    move-result-object v0

    aput-object v0, v4, v1

    invoke-direct {p0, p2, p3}, Lcom/chase/sig/android/view/aa;->a(Lcom/chase/sig/android/domain/QuickPayActivityItem;Z)Landroid/view/View;

    move-result-object v0

    aput-object v0, v4, v7

    const-string v0, "Pay from"

    invoke-direct {p0, v0, p2}, Lcom/chase/sig/android/view/aa;->a(Ljava/lang/String;Lcom/chase/sig/android/domain/h;)Landroid/view/View;

    move-result-object v0

    aput-object v0, v4, v8

    const/4 v5, 0x4

    if-nez p3, :cond_c

    move v0, v1

    :goto_7
    invoke-direct {p0, p2, v0}, Lcom/chase/sig/android/view/aa;->a(Lcom/chase/sig/android/domain/h;Z)Landroid/view/View;

    move-result-object v0

    aput-object v0, v4, v5

    const/4 v0, 0x5

    invoke-direct {p0, p2}, Lcom/chase/sig/android/view/aa;->a(Lcom/chase/sig/android/domain/h;)Landroid/view/View;

    move-result-object v5

    aput-object v5, v4, v0

    const/4 v0, 0x6

    invoke-virtual {p2}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->k()Z

    move-result v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/chase/sig/android/view/aa;->b:Landroid/view/LayoutInflater;

    const v6, 0x7f0300a8

    invoke-virtual {v5, v6, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    :cond_2
    aput-object v3, v4, v0

    const/4 v0, 0x7

    invoke-direct {p0, p2}, Lcom/chase/sig/android/view/aa;->e(Lcom/chase/sig/android/domain/h;)Lcom/chase/sig/android/view/y;

    move-result-object v3

    aput-object v3, v4, v0

    const/16 v0, 0x8

    invoke-direct {p0, p2}, Lcom/chase/sig/android/view/aa;->d(Lcom/chase/sig/android/domain/h;)Lcom/chase/sig/android/view/y;

    move-result-object v3

    aput-object v3, v4, v0

    invoke-direct {p0, v4}, Lcom/chase/sig/android/view/aa;->a([Landroid/view/View;)V

    .line 90
    :cond_3
    sget-object v0, Lcom/chase/sig/android/domain/QuickPayActivityType;->c:Lcom/chase/sig/android/domain/QuickPayActivityType;

    invoke-virtual {v0, p2}, Lcom/chase/sig/android/domain/QuickPayActivityType;->a(Lcom/chase/sig/android/domain/QuickPayActivityItem;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 91
    const/4 v0, 0x6

    new-array v0, v0, [Landroid/view/View;

    invoke-direct {p0, p2, p3}, Lcom/chase/sig/android/view/aa;->b(Lcom/chase/sig/android/domain/h;Z)Landroid/view/View;

    move-result-object v3

    aput-object v3, v0, v2

    invoke-direct {p0, p2}, Lcom/chase/sig/android/view/aa;->b(Lcom/chase/sig/android/domain/h;)Landroid/view/View;

    move-result-object v3

    aput-object v3, v0, v1

    invoke-direct {p0, p2, p3}, Lcom/chase/sig/android/view/aa;->a(Lcom/chase/sig/android/domain/QuickPayActivityItem;Z)Landroid/view/View;

    move-result-object v3

    aput-object v3, v0, v7

    if-nez p3, :cond_d

    :goto_8
    invoke-direct {p0, p2, v1}, Lcom/chase/sig/android/view/aa;->a(Lcom/chase/sig/android/domain/h;Z)Landroid/view/View;

    move-result-object v1

    aput-object v1, v0, v8

    const/4 v1, 0x4

    invoke-direct {p0, p2}, Lcom/chase/sig/android/view/aa;->b(Lcom/chase/sig/android/domain/QuickPayActivityItem;)Lcom/chase/sig/android/view/y;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    invoke-direct {p0, p2}, Lcom/chase/sig/android/view/aa;->d(Lcom/chase/sig/android/domain/h;)Lcom/chase/sig/android/view/y;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lcom/chase/sig/android/view/aa;->a([Landroid/view/View;)V

    .line 97
    :cond_4
    return-void

    :cond_5
    move v0, v2

    .line 66
    goto/16 :goto_0

    :cond_6
    move v0, v2

    goto/16 :goto_1

    :cond_7
    move-object v0, v3

    goto/16 :goto_2

    :cond_8
    const-string v0, "Accepted On"

    invoke-virtual {p2}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->t()Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v6

    invoke-virtual {v6}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/chase/sig/android/util/s;->j(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v0, v6}, Lcom/chase/sig/android/view/aa;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/view/y;

    move-result-object v0

    goto/16 :goto_3

    :cond_9
    move-object v0, v3

    goto/16 :goto_4

    :cond_a
    move-object v0, v3

    goto/16 :goto_5

    :cond_b
    move v0, v2

    .line 76
    goto/16 :goto_6

    :cond_c
    move v0, v2

    .line 83
    goto/16 :goto_7

    :cond_d
    move v1, v2

    .line 91
    goto :goto_8
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/chase/sig/android/domain/QuickPayDeclineTransaction;ZLcom/chase/sig/android/ChaseApplication;)V
    .locals 7
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 116
    invoke-direct {p0, p1, p4}, Lcom/chase/sig/android/view/aa;-><init>(Landroid/content/Context;Lcom/chase/sig/android/ChaseApplication;)V

    .line 118
    sget-object v0, Lcom/chase/sig/android/domain/QuickPayActivityType;->d:Lcom/chase/sig/android/domain/QuickPayActivityType;

    invoke-virtual {p2}, Lcom/chase/sig/android/domain/QuickPayDeclineTransaction;->I()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/QuickPayActivityType;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 119
    const/4 v0, 0x5

    new-array v0, v0, [Landroid/view/View;

    invoke-direct {p0, p2}, Lcom/chase/sig/android/view/aa;->c(Lcom/chase/sig/android/domain/h;)Landroid/view/View;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-direct {p0, p2, v2}, Lcom/chase/sig/android/view/aa;->a(Lcom/chase/sig/android/domain/h;Z)Landroid/view/View;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-direct {p0, p2}, Lcom/chase/sig/android/view/aa;->d(Lcom/chase/sig/android/domain/QuickPayTransaction;)Landroid/view/View;

    move-result-object v1

    aput-object v1, v0, v4

    invoke-direct {p0, p2}, Lcom/chase/sig/android/view/aa;->d(Lcom/chase/sig/android/domain/h;)Lcom/chase/sig/android/view/y;

    move-result-object v1

    aput-object v1, v0, v5

    invoke-direct {p0, p2, p3}, Lcom/chase/sig/android/view/aa;->a(Lcom/chase/sig/android/domain/QuickPayDeclineTransaction;Z)Landroid/view/View;

    move-result-object v1

    aput-object v1, v0, v6

    invoke-direct {p0, v0}, Lcom/chase/sig/android/view/aa;->a([Landroid/view/View;)V

    .line 124
    :cond_0
    sget-object v0, Lcom/chase/sig/android/domain/QuickPayActivityType;->a:Lcom/chase/sig/android/domain/QuickPayActivityType;

    invoke-virtual {p2}, Lcom/chase/sig/android/domain/QuickPayDeclineTransaction;->I()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/QuickPayActivityType;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 125
    const/4 v0, 0x5

    new-array v0, v0, [Landroid/view/View;

    invoke-direct {p0, p2}, Lcom/chase/sig/android/view/aa;->c(Lcom/chase/sig/android/domain/h;)Landroid/view/View;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-direct {p0, p2, v2}, Lcom/chase/sig/android/view/aa;->a(Lcom/chase/sig/android/domain/h;Z)Landroid/view/View;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-direct {p0, p2}, Lcom/chase/sig/android/view/aa;->d(Lcom/chase/sig/android/domain/QuickPayTransaction;)Landroid/view/View;

    move-result-object v1

    aput-object v1, v0, v4

    invoke-direct {p0, p2}, Lcom/chase/sig/android/view/aa;->d(Lcom/chase/sig/android/domain/h;)Lcom/chase/sig/android/view/y;

    move-result-object v1

    aput-object v1, v0, v5

    invoke-direct {p0, p2, p3}, Lcom/chase/sig/android/view/aa;->a(Lcom/chase/sig/android/domain/QuickPayDeclineTransaction;Z)Landroid/view/View;

    move-result-object v1

    aput-object v1, v0, v6

    invoke-direct {p0, v0}, Lcom/chase/sig/android/view/aa;->a([Landroid/view/View;)V

    .line 130
    :cond_1
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/chase/sig/android/domain/QuickPayPendingTransaction;Lcom/chase/sig/android/ChaseApplication;)V
    .locals 7
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 176
    invoke-direct {p0, p1, p3}, Lcom/chase/sig/android/view/aa;-><init>(Landroid/content/Context;Lcom/chase/sig/android/ChaseApplication;)V

    .line 178
    invoke-virtual {p2}, Lcom/chase/sig/android/domain/QuickPayPendingTransaction;->Y()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->l(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 179
    invoke-virtual {p3}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->c:Lcom/chase/sig/android/service/quickpay/TodoListResponse;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->t()Lcom/chase/sig/android/domain/m;

    move-result-object v0

    invoke-virtual {p2}, Lcom/chase/sig/android/domain/QuickPayPendingTransaction;->X()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/domain/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/chase/sig/android/domain/QuickPayPendingTransaction;->w(Ljava/lang/String;)V

    .line 183
    :cond_0
    invoke-virtual {p2}, Lcom/chase/sig/android/domain/QuickPayPendingTransaction;->N()Z

    move-result v0

    if-nez v0, :cond_2

    .line 185
    const/4 v0, 0x7

    new-array v1, v0, [Landroid/view/View;

    invoke-direct {p0, p2}, Lcom/chase/sig/android/view/aa;->b(Lcom/chase/sig/android/domain/h;)Landroid/view/View;

    move-result-object v0

    aput-object v0, v1, v3

    invoke-virtual {p2}, Lcom/chase/sig/android/domain/QuickPayPendingTransaction;->D()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, p2, v3}, Lcom/chase/sig/android/view/aa;->a(Lcom/chase/sig/android/domain/h;Z)Landroid/view/View;

    move-result-object v0

    :goto_0
    aput-object v0, v1, v2

    const-string v0, "Pay from"

    invoke-direct {p0, v0, p2}, Lcom/chase/sig/android/view/aa;->a(Ljava/lang/String;Lcom/chase/sig/android/domain/h;)Landroid/view/View;

    move-result-object v0

    aput-object v0, v1, v4

    invoke-direct {p0, p2}, Lcom/chase/sig/android/view/aa;->b(Lcom/chase/sig/android/domain/QuickPayTransaction;)Landroid/view/View;

    move-result-object v0

    aput-object v0, v1, v5

    invoke-direct {p0, p2}, Lcom/chase/sig/android/view/aa;->a(Lcom/chase/sig/android/domain/QuickPayTransaction;)Landroid/view/View;

    move-result-object v0

    aput-object v0, v1, v6

    const/4 v0, 0x5

    invoke-direct {p0, p2}, Lcom/chase/sig/android/view/aa;->c(Lcom/chase/sig/android/domain/QuickPayTransaction;)Landroid/view/View;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x6

    invoke-direct {p0, p2}, Lcom/chase/sig/android/view/aa;->d(Lcom/chase/sig/android/domain/h;)Lcom/chase/sig/android/view/y;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-direct {p0, v1}, Lcom/chase/sig/android/view/aa;->a([Landroid/view/View;)V

    .line 199
    :goto_1
    return-void

    .line 185
    :cond_1
    invoke-direct {p0, p2, v2}, Lcom/chase/sig/android/view/aa;->a(Lcom/chase/sig/android/domain/h;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 193
    :cond_2
    const/4 v0, 0x7

    new-array v0, v0, [Landroid/view/View;

    invoke-direct {p0, p2}, Lcom/chase/sig/android/view/aa;->b(Lcom/chase/sig/android/domain/h;)Landroid/view/View;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-direct {p0, p2, v2}, Lcom/chase/sig/android/view/aa;->a(Lcom/chase/sig/android/domain/h;Z)Landroid/view/View;

    move-result-object v1

    aput-object v1, v0, v2

    const-string v1, "Pay from"

    invoke-direct {p0, v1, p2}, Lcom/chase/sig/android/view/aa;->a(Ljava/lang/String;Lcom/chase/sig/android/domain/h;)Landroid/view/View;

    move-result-object v1

    aput-object v1, v0, v4

    invoke-direct {p0, p2}, Lcom/chase/sig/android/view/aa;->b(Lcom/chase/sig/android/domain/QuickPayTransaction;)Landroid/view/View;

    move-result-object v1

    aput-object v1, v0, v5

    invoke-direct {p0, p2}, Lcom/chase/sig/android/view/aa;->a(Lcom/chase/sig/android/domain/QuickPayTransaction;)Landroid/view/View;

    move-result-object v1

    aput-object v1, v0, v6

    const/4 v1, 0x5

    invoke-direct {p0, p2}, Lcom/chase/sig/android/view/aa;->c(Lcom/chase/sig/android/domain/QuickPayTransaction;)Landroid/view/View;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    invoke-direct {p0, p2}, Lcom/chase/sig/android/view/aa;->d(Lcom/chase/sig/android/domain/h;)Lcom/chase/sig/android/view/y;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lcom/chase/sig/android/view/aa;->a([Landroid/view/View;)V

    goto :goto_1
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/chase/sig/android/domain/QuickPayTransaction;Lcom/chase/sig/android/ChaseApplication;)V
    .locals 6
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 155
    invoke-direct {p0, p1, p3}, Lcom/chase/sig/android/view/aa;-><init>(Landroid/content/Context;Lcom/chase/sig/android/ChaseApplication;)V

    .line 157
    invoke-virtual {p2}, Lcom/chase/sig/android/domain/QuickPayTransaction;->y()Z

    move-result v2

    .line 159
    invoke-virtual {p2}, Lcom/chase/sig/android/domain/QuickPayTransaction;->Y()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->l(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 160
    invoke-virtual {p3}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->c:Lcom/chase/sig/android/service/quickpay/TodoListResponse;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->t()Lcom/chase/sig/android/domain/m;

    move-result-object v0

    invoke-virtual {p2}, Lcom/chase/sig/android/domain/QuickPayTransaction;->X()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/chase/sig/android/domain/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/chase/sig/android/domain/QuickPayTransaction;->w(Ljava/lang/String;)V

    .line 164
    :cond_0
    const/16 v0, 0xa

    new-array v3, v0, [Landroid/view/View;

    if-eqz v2, :cond_2

    invoke-direct {p0, p2, v4}, Lcom/chase/sig/android/view/aa;->b(Lcom/chase/sig/android/domain/h;Z)Landroid/view/View;

    move-result-object v0

    :goto_0
    aput-object v0, v3, v4

    invoke-direct {p0, p2}, Lcom/chase/sig/android/view/aa;->b(Lcom/chase/sig/android/domain/h;)Landroid/view/View;

    move-result-object v0

    aput-object v0, v3, v5

    const/4 v0, 0x2

    const-string v4, "Pay from"

    invoke-direct {p0, v4, p2}, Lcom/chase/sig/android/view/aa;->a(Ljava/lang/String;Lcom/chase/sig/android/domain/h;)Landroid/view/View;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v0, 0x3

    invoke-direct {p0, p2, v5}, Lcom/chase/sig/android/view/aa;->a(Lcom/chase/sig/android/domain/h;Z)Landroid/view/View;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v4, 0x4

    if-eqz v2, :cond_3

    invoke-direct {p0, p2}, Lcom/chase/sig/android/view/aa;->a(Lcom/chase/sig/android/domain/h;)Landroid/view/View;

    move-result-object v0

    :goto_1
    aput-object v0, v3, v4

    const/4 v4, 0x5

    if-eqz v2, :cond_4

    :cond_1
    :goto_2
    aput-object v1, v3, v4

    const/4 v0, 0x6

    invoke-direct {p0, p2}, Lcom/chase/sig/android/view/aa;->c(Lcom/chase/sig/android/domain/QuickPayTransaction;)Landroid/view/View;

    move-result-object v1

    aput-object v1, v3, v0

    const/4 v0, 0x7

    invoke-direct {p0, p2}, Lcom/chase/sig/android/view/aa;->b(Lcom/chase/sig/android/domain/QuickPayTransaction;)Landroid/view/View;

    move-result-object v1

    aput-object v1, v3, v0

    const/16 v0, 0x8

    invoke-direct {p0, p2}, Lcom/chase/sig/android/view/aa;->a(Lcom/chase/sig/android/domain/QuickPayTransaction;)Landroid/view/View;

    move-result-object v1

    aput-object v1, v3, v0

    const/16 v0, 0x9

    invoke-direct {p0, p2}, Lcom/chase/sig/android/view/aa;->d(Lcom/chase/sig/android/domain/h;)Lcom/chase/sig/android/view/y;

    move-result-object v1

    aput-object v1, v3, v0

    invoke-direct {p0, v3}, Lcom/chase/sig/android/view/aa;->a([Landroid/view/View;)V

    .line 171
    return-void

    :cond_2
    move-object v0, v1

    .line 164
    goto :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_1

    :cond_4
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v0

    iget-object v0, v0, Lcom/chase/sig/android/domain/o;->c:Lcom/chase/sig/android/service/quickpay/TodoListResponse;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p2}, Lcom/chase/sig/android/domain/QuickPayTransaction;->B()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p2}, Lcom/chase/sig/android/domain/QuickPayTransaction;->D()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/chase/sig/android/view/aa;->b:Landroid/view/LayoutInflater;

    const v2, 0x7f030092

    invoke-virtual {v0, v2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f090253

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v2, 0x7f07020e

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    const v0, 0x7f090252

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v2, Lcom/chase/sig/android/view/ad;

    invoke-direct {v2, p0}, Lcom/chase/sig/android/view/ad;-><init>(Lcom/chase/sig/android/view/aa;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2
.end method

.method private a(Lcom/chase/sig/android/domain/QuickPayAcceptMoneyTransaction;)Landroid/view/View;
    .locals 5
    .parameter

    .prologue
    const/4 v0, 0x0

    const v4, 0x7f090200

    const v3, 0x7f0901d5

    .line 251
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/QuickPayAcceptMoneyTransaction;->m()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/QuickPayAcceptMoneyTransaction;->k()Z

    move-result v1

    if-nez v1, :cond_1

    .line 274
    :cond_0
    :goto_0
    return-object v0

    .line 255
    :cond_1
    iget-object v1, p0, Lcom/chase/sig/android/view/aa;->b:Landroid/view/LayoutInflater;

    const v2, 0x7f030092

    invoke-virtual {v1, v2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 257
    const v0, 0x7f090253

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const-string v2, "Get cash at Chase ATM"

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 258
    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const-string v2, "Bring your ATM, credit or debit card..."

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 261
    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 262
    const v0, 0x7f090182

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 263
    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v2, 0x7f0200d1

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 265
    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v2, Lcom/chase/sig/android/view/ab;

    invoke-direct {v2, p0, p1}, Lcom/chase/sig/android/view/ab;-><init>(Lcom/chase/sig/android/view/aa;Lcom/chase/sig/android/domain/QuickPayAcceptMoneyTransaction;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object v0, v1

    .line 274
    goto :goto_0
.end method

.method private a(Lcom/chase/sig/android/domain/QuickPayActivityItem;)Landroid/view/View;
    .locals 2
    .parameter

    .prologue
    .line 100
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->D()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 101
    const-string v0, "Reason for Decline"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->D()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/chase/sig/android/view/aa;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/view/y;

    move-result-object v0

    .line 103
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/chase/sig/android/domain/QuickPayActivityItem;Z)Landroid/view/View;
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 340
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 343
    if-eqz p2, :cond_0

    .line 344
    const-string v0, "Type"

    .line 345
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->w()Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v2

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/LabeledValue;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->w()Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v2

    invoke-virtual {v2}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/chase/sig/android/view/aa;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/view/y;

    move-result-object v0

    .line 352
    :goto_0
    return-object v0

    .line 348
    :cond_0
    const-string v0, "Invoice Number"

    .line 349
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->w()Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/chase/sig/android/view/aa;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/view/y;

    move-result-object v0

    goto :goto_0

    .line 352
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/chase/sig/android/domain/QuickPayDeclineTransaction;Z)Landroid/view/View;
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 371
    if-eqz p2, :cond_0

    .line 372
    const-string v0, "Reason for Decline"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/QuickPayDeclineTransaction;->m()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/chase/sig/android/view/aa;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/view/y;

    move-result-object v0

    .line 378
    :goto_0
    return-object v0

    .line 375
    :cond_0
    invoke-virtual {p0}, Lcom/chase/sig/android/view/aa;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/chase/sig/android/view/aa;->b:Landroid/view/LayoutInflater;

    .line 378
    iget-object v0, p0, Lcom/chase/sig/android/view/aa;->b:Landroid/view/LayoutInflater;

    const v1, 0x7f030083

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Lcom/chase/sig/android/domain/QuickPayTransaction;)Landroid/view/View;
    .locals 2
    .parameter

    .prologue
    .line 203
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/QuickPayTransaction;->D()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/QuickPayTransaction;->H()I

    move-result v0

    if-gez v0, :cond_1

    .line 204
    :cond_0
    const/4 v0, 0x0

    .line 210
    :goto_0
    return-object v0

    .line 206
    :cond_1
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/QuickPayTransaction;->C()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 207
    const-string v0, "Number of payments"

    const-string v1, "Unlimited"

    invoke-direct {p0, v0, v1}, Lcom/chase/sig/android/view/aa;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/view/y;

    move-result-object v0

    goto :goto_0

    .line 210
    :cond_2
    const-string v0, "Number of payments"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/QuickPayTransaction;->H()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/chase/sig/android/view/aa;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/view/y;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Lcom/chase/sig/android/domain/h;)Landroid/view/View;
    .locals 8
    .parameter

    .prologue
    const v7, 0x7f090200

    const/16 v6, 0x12

    const/4 v0, 0x0

    .line 278
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/ChaseApplication;->b()Lcom/chase/sig/android/domain/o;

    move-result-object v1

    iget-object v1, v1, Lcom/chase/sig/android/domain/o;->c:Lcom/chase/sig/android/service/quickpay/TodoListResponse;

    invoke-virtual {v1}, Lcom/chase/sig/android/service/quickpay/TodoListResponse;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 311
    :cond_0
    :goto_0
    return-object v0

    .line 282
    :cond_1
    invoke-interface {p1}, Lcom/chase/sig/android/domain/h;->k()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 285
    iget-object v1, p0, Lcom/chase/sig/android/view/aa;->b:Landroid/view/LayoutInflater;

    const v2, 0x7f030092

    invoke-virtual {v1, v2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 286
    const v0, 0x7f090253

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 287
    const-string v1, "ATM pick-up is Available"

    sget-object v3, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {v0, v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 288
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    check-cast v1, Landroid/text/Spannable;

    .line 289
    new-instance v3, Landroid/text/style/ForegroundColorSpan;

    invoke-virtual {p0}, Lcom/chase/sig/android/view/aa;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f060045

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-direct {v3, v4}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/16 v4, 0xf

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    invoke-interface {v1, v3, v4, v0, v6}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 293
    invoke-virtual {v2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 294
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Give recipient this Sender Code: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/chase/sig/android/domain/h;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v3, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {v0, v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 296
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    check-cast v1, Landroid/text/Spannable;

    .line 297
    new-instance v3, Landroid/text/style/ForegroundColorSpan;

    invoke-virtual {p0}, Lcom/chase/sig/android/view/aa;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f060004

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-direct {v3, v4}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    move-result v4

    invoke-interface {p1}, Lcom/chase/sig/android/domain/h;->l()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    sub-int/2addr v4, v5

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    invoke-interface {v1, v3, v4, v0, v6}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 301
    invoke-virtual {v2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 302
    const v0, 0x7f0901d5

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/chase/sig/android/view/ac;

    invoke-direct {v1, p0, p1}, Lcom/chase/sig/android/view/ac;-><init>(Lcom/chase/sig/android/view/aa;Lcom/chase/sig/android/domain/h;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object v0, v2

    .line 311
    goto/16 :goto_0
.end method

.method private a(Lcom/chase/sig/android/domain/h;Z)Landroid/view/View;
    .locals 4
    .parameter
    .parameter

    .prologue
    .line 440
    invoke-interface {p1}, Lcom/chase/sig/android/domain/h;->h()Ljava/lang/String;

    move-result-object v1

    .line 442
    invoke-interface {p1}, Lcom/chase/sig/android/domain/h;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 443
    const-string v0, "Invoice Amount"

    new-instance v2, Lcom/chase/sig/android/util/Dollar;

    invoke-interface {p1}, Lcom/chase/sig/android/domain/h;->d()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/chase/sig/android/util/Dollar;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/chase/sig/android/util/Dollar;->h()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lcom/chase/sig/android/view/aa;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/view/y;

    move-result-object v0

    .line 449
    :goto_0
    if-eqz p2, :cond_0

    invoke-interface {p1}, Lcom/chase/sig/android/domain/h;->h()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/chase/sig/android/util/s;->o(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 450
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/y;->a(Ljava/lang/String;)V

    .line 453
    :cond_0
    return-object v0

    .line 446
    :cond_1
    const-string v0, "Amount"

    new-instance v2, Lcom/chase/sig/android/util/Dollar;

    invoke-interface {p1}, Lcom/chase/sig/android/domain/h;->d()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/chase/sig/android/util/Dollar;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/chase/sig/android/util/Dollar;->h()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lcom/chase/sig/android/view/aa;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/view/y;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Lcom/chase/sig/android/domain/h;)Landroid/view/View;
    .locals 7
    .parameter
    .parameter

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 383
    invoke-interface {p2}, Lcom/chase/sig/android/domain/h;->c()Ljava/lang/String;

    move-result-object v2

    .line 384
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v1

    if-gtz v1, :cond_1

    .line 393
    :cond_0
    :goto_0
    return-object v0

    .line 387
    :cond_1
    invoke-static {v2}, Lcom/chase/sig/android/util/s;->l(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    move-object v1, v0

    .line 389
    :goto_1
    if-eqz v1, :cond_0

    .line 390
    new-instance v0, Lcom/chase/sig/android/view/y;

    invoke-virtual {p0}, Lcom/chase/sig/android/view/aa;->getContext()Landroid/content/Context;

    move-result-object v2

    aget-object v3, v1, v5

    aget-object v1, v1, v4

    invoke-direct {v0, v2, p1, v3, v1}, Lcom/chase/sig/android/view/y;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 387
    :cond_2
    const-string v1, "\\(\\.+\\d\\d\\d\\d\\)$"

    invoke-static {v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v3

    const-string v1, ""

    invoke-virtual {v2, v3, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    new-array v1, v6, [Ljava/lang/String;

    aput-object v2, v1, v4

    aput-object v3, v1, v5

    goto :goto_1

    :cond_3
    new-array v1, v6, [Ljava/lang/String;

    aput-object v2, v1, v4

    const-string v2, ""

    aput-object v2, v1, v5

    goto :goto_1
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/view/y;
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 543
    new-instance v0, Lcom/chase/sig/android/view/y;

    invoke-virtual {p0}, Lcom/chase/sig/android/view/aa;->getContext()Landroid/content/Context;

    move-result-object v1

    if-eqz p2, :cond_0

    const-string v2, ""

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const-string p2, "NA"

    :cond_1
    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, p2, v2}, Lcom/chase/sig/android/view/y;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private a([Landroid/view/View;)V
    .locals 6
    .parameter

    .prologue
    const v5, 0x7f090182

    const/4 v2, 0x0

    .line 471
    iget-object v0, p0, Lcom/chase/sig/android/view/aa;->a:Landroid/view/View;

    const v1, 0x7f0901da

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 473
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 474
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 476
    instance-of v3, v1, Lcom/chase/sig/android/view/y;

    if-eqz v3, :cond_0

    .line 477
    check-cast v1, Lcom/chase/sig/android/view/y;

    .line 478
    iget-object v1, v1, Lcom/chase/sig/android/view/y;->a:Landroid/view/View;

    invoke-virtual {v1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 482
    :cond_0
    const/4 v1, 0x0

    .line 483
    array-length v4, p1

    move v3, v2

    :goto_0
    if-ge v3, v4, :cond_2

    aget-object v2, p1, v3

    .line 485
    if-eqz v2, :cond_1

    .line 486
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    move-object v1, v2

    .line 483
    :cond_1
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 493
    :cond_2
    if-eqz v1, :cond_3

    instance-of v0, v1, Lcom/chase/sig/android/view/y;

    if-eqz v0, :cond_3

    move-object v0, v1

    .line 494
    check-cast v0, Lcom/chase/sig/android/view/y;

    iget-object v0, v0, Lcom/chase/sig/android/view/y;->a:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 496
    :cond_3
    return-void
.end method

.method private b(Lcom/chase/sig/android/domain/QuickPayTransaction;)Landroid/view/View;
    .locals 2
    .parameter

    .prologue
    .line 216
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/QuickPayTransaction;->D()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/QuickPayTransaction;->H()I

    move-result v0

    if-gez v0, :cond_1

    .line 217
    :cond_0
    const/4 v0, 0x0

    .line 220
    :goto_0
    return-object v0

    :cond_1
    const-string v0, "Frequency"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/QuickPayTransaction;->Y()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/chase/sig/android/view/aa;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/view/y;

    move-result-object v0

    goto :goto_0
.end method

.method private b(Lcom/chase/sig/android/domain/h;)Landroid/view/View;
    .locals 2
    .parameter

    .prologue
    .line 419
    const-string v0, "To"

    invoke-interface {p1}, Lcom/chase/sig/android/domain/h;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/chase/sig/android/view/aa;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/view/y;

    move-result-object v0

    .line 420
    invoke-interface {p1}, Lcom/chase/sig/android/domain/h;->j()Lcom/chase/sig/android/domain/QuickPayRecipient;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/QuickPayRecipient;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 421
    invoke-interface {p1}, Lcom/chase/sig/android/domain/h;->j()Lcom/chase/sig/android/domain/QuickPayRecipient;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/QuickPayRecipient;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/y;->a(Ljava/lang/String;)V

    .line 424
    :cond_0
    invoke-interface {p1}, Lcom/chase/sig/android/domain/h;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 425
    invoke-interface {p1}, Lcom/chase/sig/android/domain/h;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/y;->a(Ljava/lang/String;)V

    .line 427
    :cond_1
    return-object v0
.end method

.method private b(Lcom/chase/sig/android/domain/h;Z)Landroid/view/View;
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 459
    if-eqz p2, :cond_0

    .line 460
    const/4 v0, 0x0

    .line 467
    :goto_0
    return-object v0

    .line 463
    :cond_0
    const-string v0, "Transaction #"

    invoke-interface {p1}, Lcom/chase/sig/android/domain/h;->g()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/chase/sig/android/view/aa;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/view/y;

    move-result-object v0

    .line 465
    invoke-virtual {p0}, Lcom/chase/sig/android/view/aa;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060046

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/y;->setValueColor(I)V

    goto :goto_0
.end method

.method private b(Lcom/chase/sig/android/domain/QuickPayActivityItem;)Lcom/chase/sig/android/view/y;
    .locals 2
    .parameter

    .prologue
    .line 508
    const-string v0, "Due Date"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/QuickPayActivityItem;->v()Lcom/chase/sig/android/domain/LabeledValue;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->j(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/chase/sig/android/view/aa;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/view/y;

    move-result-object v0

    return-object v0
.end method

.method private c(Lcom/chase/sig/android/domain/QuickPayTransaction;)Landroid/view/View;
    .locals 3
    .parameter

    .prologue
    .line 225
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/QuickPayTransaction;->s()Ljava/util/Date;

    move-result-object v0

    .line 226
    if-eqz v0, :cond_3

    .line 227
    invoke-virtual {p0}, Lcom/chase/sig/android/view/aa;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 228
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/QuickPayTransaction;->B()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 229
    const-string v1, "Due date"

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->a(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lcom/chase/sig/android/view/aa;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/view/y;

    move-result-object v0

    .line 240
    :goto_0
    return-object v0

    .line 230
    :cond_0
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/QuickPayTransaction;->V()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 231
    const v2, 0x7f07022c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->a(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lcom/chase/sig/android/view/aa;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/view/y;

    move-result-object v0

    goto :goto_0

    .line 232
    :cond_1
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/QuickPayTransaction;->D()Z

    move-result v2

    if-nez v2, :cond_2

    .line 233
    const v2, 0x7f07022b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->a(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lcom/chase/sig/android/view/aa;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/view/y;

    move-result-object v0

    goto :goto_0

    .line 235
    :cond_2
    const v2, 0x7f07022d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->a(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lcom/chase/sig/android/view/aa;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/view/y;

    move-result-object v0

    goto :goto_0

    .line 240
    :cond_3
    const-string v0, "Due date"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/QuickPayTransaction;->E()Ljava/util/Date;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->a(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/chase/sig/android/view/aa;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/view/y;

    move-result-object v0

    goto :goto_0
.end method

.method private c(Lcom/chase/sig/android/domain/h;)Landroid/view/View;
    .locals 2
    .parameter

    .prologue
    .line 431
    const-string v0, "From"

    invoke-interface {p1}, Lcom/chase/sig/android/domain/h;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/chase/sig/android/view/aa;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/view/y;

    move-result-object v0

    .line 432
    invoke-interface {p1}, Lcom/chase/sig/android/domain/h;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->o(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 433
    invoke-interface {p1}, Lcom/chase/sig/android/domain/h;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/y;->a(Ljava/lang/String;)V

    .line 435
    :cond_0
    return-object v0
.end method

.method private d(Lcom/chase/sig/android/domain/QuickPayTransaction;)Landroid/view/View;
    .locals 2
    .parameter

    .prologue
    .line 245
    invoke-virtual {p1}, Lcom/chase/sig/android/domain/QuickPayTransaction;->A()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Invoice Number"

    invoke-virtual {p1}, Lcom/chase/sig/android/domain/QuickPayTransaction;->A()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/chase/sig/android/view/aa;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/view/y;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d(Lcom/chase/sig/android/domain/h;)Lcom/chase/sig/android/view/y;
    .locals 5
    .parameter

    .prologue
    .line 513
    invoke-interface {p1}, Lcom/chase/sig/android/domain/h;->e()Ljava/lang/String;

    move-result-object v0

    .line 514
    invoke-static {v0}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 516
    :goto_0
    new-instance v1, Lcom/chase/sig/android/view/y;

    invoke-virtual {p0}, Lcom/chase/sig/android/view/aa;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "Memo"

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, v0, v4}, Lcom/chase/sig/android/view/y;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v1

    .line 514
    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method private e(Lcom/chase/sig/android/domain/h;)Lcom/chase/sig/android/view/y;
    .locals 2
    .parameter

    .prologue
    .line 521
    invoke-interface {p1}, Lcom/chase/sig/android/domain/h;->i()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 522
    const-string v0, "Sent On"

    const-string v1, ""

    invoke-direct {p0, v0, v1}, Lcom/chase/sig/android/view/aa;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/view/y;

    move-result-object v0

    .line 524
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "Sent On"

    invoke-interface {p1}, Lcom/chase/sig/android/domain/h;->i()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/chase/sig/android/view/aa;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/chase/sig/android/view/y;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final getView()Landroid/view/View;
    .locals 1

    .prologue
    .line 548
    iget-object v0, p0, Lcom/chase/sig/android/view/aa;->a:Landroid/view/View;

    return-object v0
.end method
