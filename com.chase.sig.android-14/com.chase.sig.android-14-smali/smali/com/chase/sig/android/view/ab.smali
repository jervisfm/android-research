.class final Lcom/chase/sig/android/view/ab;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/domain/QuickPayAcceptMoneyTransaction;

.field final synthetic b:Lcom/chase/sig/android/view/aa;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/view/aa;Lcom/chase/sig/android/domain/QuickPayAcceptMoneyTransaction;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 265
    iput-object p1, p0, Lcom/chase/sig/android/view/ab;->b:Lcom/chase/sig/android/view/aa;

    iput-object p2, p0, Lcom/chase/sig/android/view/ab;->a:Lcom/chase/sig/android/domain/QuickPayAcceptMoneyTransaction;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3
    .parameter

    .prologue
    .line 268
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/chase/sig/android/view/ab;->b:Lcom/chase/sig/android/view/aa;

    invoke-virtual {v1}, Lcom/chase/sig/android/view/aa;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/chase/sig/android/activity/AtmRecipientInfoActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 269
    const/high16 v1, 0x1000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 270
    const-string v1, "quick_pay_transaction"

    iget-object v2, p0, Lcom/chase/sig/android/view/ab;->a:Lcom/chase/sig/android/domain/QuickPayAcceptMoneyTransaction;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 271
    iget-object v1, p0, Lcom/chase/sig/android/view/ab;->b:Lcom/chase/sig/android/view/aa;

    invoke-virtual {v1}, Lcom/chase/sig/android/view/aa;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 272
    return-void
.end method
