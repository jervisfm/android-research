.class final Lcom/chase/sig/android/view/ac;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/domain/h;

.field final synthetic b:Lcom/chase/sig/android/view/aa;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/view/aa;Lcom/chase/sig/android/domain/h;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 302
    iput-object p1, p0, Lcom/chase/sig/android/view/ac;->b:Lcom/chase/sig/android/view/aa;

    iput-object p2, p0, Lcom/chase/sig/android/view/ac;->a:Lcom/chase/sig/android/domain/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 3
    .parameter

    .prologue
    .line 305
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    new-instance v1, Landroid/content/Intent;

    iget-object v0, p0, Lcom/chase/sig/android/view/ac;->b:Lcom/chase/sig/android/view/aa;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/aa;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v2, Lcom/chase/sig/android/activity/AtmSenderInfoActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 306
    const/high16 v0, 0x1000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 307
    const-string v2, "quick_pay_transaction"

    iget-object v0, p0, Lcom/chase/sig/android/view/ac;->a:Lcom/chase/sig/android/domain/h;

    check-cast v0, Ljava/io/Serializable;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 308
    iget-object v0, p0, Lcom/chase/sig/android/view/ac;->b:Lcom/chase/sig/android/view/aa;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/aa;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 309
    return-void
.end method
