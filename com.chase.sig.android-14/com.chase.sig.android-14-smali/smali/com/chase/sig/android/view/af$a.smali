.class final Lcom/chase/sig/android/view/af$a;
.super Landroid/widget/Filter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/view/af;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/chase/sig/android/view/af;


# direct methods
.method private constructor <init>(Lcom/chase/sig/android/view/af;)V
    .locals 0
    .parameter

    .prologue
    .line 87
    iput-object p1, p0, Lcom/chase/sig/android/view/af$a;->a:Lcom/chase/sig/android/view/af;

    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/chase/sig/android/view/af;B)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 87
    invoke-direct {p0, p1}, Lcom/chase/sig/android/view/af$a;-><init>(Lcom/chase/sig/android/view/af;)V

    return-void
.end method


# virtual methods
.method protected final performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .locals 14
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 91
    new-instance v5, Landroid/widget/Filter$FilterResults;

    invoke-direct {v5}, Landroid/widget/Filter$FilterResults;-><init>()V

    .line 93
    iget-object v0, p0, Lcom/chase/sig/android/view/af$a;->a:Lcom/chase/sig/android/view/af;

    invoke-static {v0}, Lcom/chase/sig/android/view/af;->a(Lcom/chase/sig/android/view/af;)Ljava/util/ArrayList;

    move-result-object v0

    if-nez v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/chase/sig/android/view/af$a;->a:Lcom/chase/sig/android/view/af;

    new-instance v1, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/chase/sig/android/view/af$a;->a:Lcom/chase/sig/android/view/af;

    invoke-static {v3}, Lcom/chase/sig/android/view/af;->b(Lcom/chase/sig/android/view/af;)Ljava/util/List;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v0, v1}, Lcom/chase/sig/android/view/af;->a(Lcom/chase/sig/android/view/af;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 97
    :cond_0
    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_2

    .line 98
    :cond_1
    iget-object v0, p0, Lcom/chase/sig/android/view/af$a;->a:Lcom/chase/sig/android/view/af;

    invoke-static {v0}, Lcom/chase/sig/android/view/af;->a(Lcom/chase/sig/android/view/af;)Ljava/util/ArrayList;

    move-result-object v0

    .line 99
    iput-object v0, v5, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 100
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, v5, Landroid/widget/Filter$FilterResults;->count:I

    .line 137
    :goto_0
    return-object v5

    .line 102
    :cond_2
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v6

    .line 104
    iget-object v0, p0, Lcom/chase/sig/android/view/af$a;->a:Lcom/chase/sig/android/view/af;

    invoke-static {v0}, Lcom/chase/sig/android/view/af;->a(Lcom/chase/sig/android/view/af;)Ljava/util/ArrayList;

    move-result-object v7

    .line 105
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v8

    .line 107
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9, v8}, Ljava/util/ArrayList;-><init>(I)V

    move v4, v2

    .line 109
    :goto_1
    if-ge v4, v8, :cond_6

    .line 110
    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 111
    if-eqz v0, :cond_5

    .line 113
    iget-object v1, p0, Lcom/chase/sig/android/view/af$a;->a:Lcom/chase/sig/android/view/af;

    invoke-static {v1}, Lcom/chase/sig/android/view/af;->c(Lcom/chase/sig/android/view/af;)[I

    move-result-object v1

    array-length v10, v1

    move v3, v2

    .line 115
    :goto_2
    if-ge v3, v10, :cond_5

    .line 116
    iget-object v1, p0, Lcom/chase/sig/android/view/af$a;->a:Lcom/chase/sig/android/view/af;

    invoke-static {v1}, Lcom/chase/sig/android/view/af;->d(Lcom/chase/sig/android/view/af;)[Ljava/lang/String;

    move-result-object v1

    aget-object v1, v1, v3

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 118
    const-string v11, " "

    invoke-virtual {v1, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    .line 119
    array-length v12, v11

    move v1, v2

    .line 121
    :goto_3
    if-ge v1, v12, :cond_3

    .line 122
    aget-object v13, v11, v1

    .line 124
    invoke-virtual {v13}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 125
    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 115
    :cond_3
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_2

    .line 121
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 109
    :cond_5
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    .line 133
    :cond_6
    iput-object v9, v5, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 134
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, v5, Landroid/widget/Filter$FilterResults;->count:I

    goto :goto_0
.end method

.method protected final publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 144
    iget-object v1, p0, Lcom/chase/sig/android/view/af$a;->a:Lcom/chase/sig/android/view/af;

    iget-object v0, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    invoke-static {v1, v0}, Lcom/chase/sig/android/view/af;->a(Lcom/chase/sig/android/view/af;Ljava/util/List;)Ljava/util/List;

    .line 145
    iget v0, p2, Landroid/widget/Filter$FilterResults;->count:I

    if-lez v0, :cond_0

    .line 146
    iget-object v0, p0, Lcom/chase/sig/android/view/af$a;->a:Lcom/chase/sig/android/view/af;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/af;->notifyDataSetChanged()V

    .line 150
    :goto_0
    return-void

    .line 148
    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/view/af$a;->a:Lcom/chase/sig/android/view/af;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/af;->notifyDataSetInvalidated()V

    goto :goto_0
.end method
