.class public Lcom/chase/sig/android/view/af;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"

# interfaces
.implements Landroid/widget/Filterable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/view/af$a;
    }
.end annotation


# instance fields
.field protected final a:Landroid/widget/ListAdapter;

.field private b:Lcom/chase/sig/android/view/af$a;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<+",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;>;"
        }
    .end annotation
.end field

.field private d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;>;"
        }
    .end annotation
.end field

.field private e:[I

.field private f:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/widget/ListAdapter;)V
    .locals 0
    .parameter

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/chase/sig/android/view/af;->a:Landroid/widget/ListAdapter;

    .line 26
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/view/af;)Ljava/util/ArrayList;
    .locals 1
    .parameter

    .prologue
    .line 15
    iget-object v0, p0, Lcom/chase/sig/android/view/af;->d:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic a(Lcom/chase/sig/android/view/af;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 15
    iput-object p1, p0, Lcom/chase/sig/android/view/af;->d:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic a(Lcom/chase/sig/android/view/af;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 15
    iput-object p1, p0, Lcom/chase/sig/android/view/af;->c:Ljava/util/List;

    return-object p1
.end method

.method static synthetic b(Lcom/chase/sig/android/view/af;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 15
    iget-object v0, p0, Lcom/chase/sig/android/view/af;->c:Ljava/util/List;

    return-object v0
.end method

.method static synthetic c(Lcom/chase/sig/android/view/af;)[I
    .locals 1
    .parameter

    .prologue
    .line 15
    iget-object v0, p0, Lcom/chase/sig/android/view/af;->e:[I

    return-object v0
.end method

.method static synthetic d(Lcom/chase/sig/android/view/af;)[Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 15
    iget-object v0, p0, Lcom/chase/sig/android/view/af;->f:[Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/chase/sig/android/view/af;->a:Landroid/widget/ListAdapter;

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    return v0
.end method

.method public getFilter()Landroid/widget/Filter;
    .locals 2

    .prologue
    .line 53
    iget-object v0, p0, Lcom/chase/sig/android/view/af;->b:Lcom/chase/sig/android/view/af$a;

    if-nez v0, :cond_0

    .line 54
    new-instance v0, Lcom/chase/sig/android/view/af$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/chase/sig/android/view/af$a;-><init>(Lcom/chase/sig/android/view/af;B)V

    iput-object v0, p0, Lcom/chase/sig/android/view/af;->b:Lcom/chase/sig/android/view/af$a;

    .line 56
    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/view/af;->b:Lcom/chase/sig/android/view/af$a;

    return-object v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, Lcom/chase/sig/android/view/af;->a:Landroid/widget/ListAdapter;

    invoke-interface {v0, p1}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .parameter

    .prologue
    .line 48
    iget-object v0, p0, Lcom/chase/sig/android/view/af;->a:Landroid/widget/ListAdapter;

    invoke-interface {v0, p1}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 62
    iget-object v2, p0, Lcom/chase/sig/android/view/af;->a:Landroid/widget/ListAdapter;

    invoke-interface {v2, p1, p2, p3}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 63
    check-cast p3, Landroid/widget/ListView;

    .line 64
    invoke-virtual {p3, v0}, Landroid/widget/ListView;->setFooterDividersEnabled(Z)V

    .line 65
    invoke-virtual {p0}, Lcom/chase/sig/android/view/af;->getCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ne p1, v3, :cond_0

    move v0, v1

    .line 67
    :cond_0
    invoke-virtual {p0}, Lcom/chase/sig/android/view/af;->getCount()I

    move-result v3

    if-ne v3, v1, :cond_1

    .line 68
    const v0, 0x7f0200d3

    invoke-virtual {v2, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 84
    :goto_0
    return-object v2

    .line 69
    :cond_1
    if-nez p1, :cond_2

    .line 71
    const v0, 0x7f0200d2

    invoke-virtual {v2, v0}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0

    .line 72
    :cond_2
    if-eqz v0, :cond_3

    invoke-virtual {p3}, Landroid/widget/ListView;->getFooterViewsCount()I

    move-result v0

    if-nez v0, :cond_3

    .line 74
    const v0, 0x7f0200d1

    invoke-virtual {v2, v0}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0

    .line 76
    :cond_3
    invoke-virtual {p3}, Landroid/widget/ListView;->getFooterViewsCount()I

    .line 78
    const v0, 0x7f0200d0

    invoke-virtual {v2, v0}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0
.end method
