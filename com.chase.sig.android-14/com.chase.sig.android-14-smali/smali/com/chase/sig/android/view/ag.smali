.class public final Lcom/chase/sig/android/view/ag;
.super Landroid/app/Dialog;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/view/ag$a;,
        Lcom/chase/sig/android/view/ag$b;
    }
.end annotation


# instance fields
.field private a:Lcom/chase/sig/android/view/ag$a;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;Lcom/chase/sig/android/view/ag$a;)V
    .locals 3
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/domain/OneTimePasswordContact;",
            ">;",
            "Lcom/chase/sig/android/view/ag$a;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x3

    .line 23
    invoke-direct {p0, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 24
    iput-object p3, p0, Lcom/chase/sig/android/view/ag;->a:Lcom/chase/sig/android/view/ag$a;

    .line 26
    if-eqz p2, :cond_0

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 27
    invoke-virtual {p0, v1}, Lcom/chase/sig/android/view/ag;->requestWindowFeature(I)Z

    .line 28
    const v0, 0x7f03001e

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/view/ag;->setContentView(I)V

    .line 29
    const v0, 0x7f0200b0

    invoke-virtual {p0, v1, v0}, Lcom/chase/sig/android/view/ag;->setFeatureDrawableResource(II)V

    .line 31
    const v0, 0x7f0700ca

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/view/ag;->setTitle(I)V

    .line 32
    const v0, 0x7f09007d

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/view/ag;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 33
    new-instance v1, Lcom/chase/sig/android/view/ap;

    invoke-direct {v1, p1, p2}, Lcom/chase/sig/android/view/ap;-><init>(Landroid/content/Context;Ljava/util/List;)V

    .line 35
    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 36
    new-instance v1, Lcom/chase/sig/android/view/ag$b;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/chase/sig/android/view/ag$b;-><init>(Lcom/chase/sig/android/view/ag;B)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 38
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/view/ag;)Lcom/chase/sig/android/view/ag$a;
    .locals 1
    .parameter

    .prologue
    .line 16
    iget-object v0, p0, Lcom/chase/sig/android/view/ag;->a:Lcom/chase/sig/android/view/ag$a;

    return-object v0
.end method
