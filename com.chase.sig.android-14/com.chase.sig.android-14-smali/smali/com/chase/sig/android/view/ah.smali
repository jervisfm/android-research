.class public final Lcom/chase/sig/android/view/ah;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/chase/sig/android/view/ai;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/chase/sig/android/view/ai;",
            ">;"
        }
    .end annotation
.end field

.field private b:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/chase/sig/android/view/ai;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 26
    const v0, 0x7f0300ba

    invoke-direct {p0, p1, v0, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 28
    iput-object p2, p0, Lcom/chase/sig/android/view/ah;->a:Ljava/util/ArrayList;

    .line 29
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/chase/sig/android/view/ah;->b:Landroid/view/LayoutInflater;

    .line 30
    return-void
.end method

.method private a(I)I
    .locals 2
    .parameter

    .prologue
    .line 139
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/chase/sig/android/view/ah;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/chase/sig/android/view/ah;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f060001

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public final getItemViewType(I)I
    .locals 4
    .parameter

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 35
    iget-object v0, p0, Lcom/chase/sig/android/view/ah;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/ai;

    iget v3, v0, Lcom/chase/sig/android/view/ai;->a:I

    packed-switch v3, :pswitch_data_0

    :pswitch_0
    new-array v2, v2, [Ljava/lang/Object;

    iget v0, v0, Lcom/chase/sig/android/view/ai;->a:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v1

    const/4 v0, -0x1

    :goto_0
    return v0

    :pswitch_1
    move v0, v1

    goto :goto_0

    :pswitch_2
    move v0, v2

    goto :goto_0

    :pswitch_3
    const/4 v0, 0x2

    goto :goto_0

    :pswitch_4
    const/4 v0, 0x3

    goto :goto_0

    :pswitch_5
    const/4 v0, 0x4

    goto :goto_0

    :pswitch_6
    const/4 v0, 0x5

    goto :goto_0

    :pswitch_7
    const/4 v0, 0x6

    goto :goto_0

    :pswitch_8
    const/4 v0, 0x7

    goto :goto_0

    :pswitch_9
    const/16 v0, 0x8

    goto :goto_0

    :pswitch_a
    const/16 v0, 0x9

    goto :goto_0

    :pswitch_b
    const/16 v0, 0xa

    goto :goto_0

    :pswitch_c
    const/16 v0, 0xb

    goto :goto_0

    :pswitch_d
    const/16 v0, 0xc

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f0300ab
        :pswitch_d
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_8
        :pswitch_4
        :pswitch_7
        :pswitch_c
        :pswitch_2
        :pswitch_3
        :pswitch_6
        :pswitch_b
        :pswitch_a
        :pswitch_5
        :pswitch_1
        :pswitch_9
    .end packed-switch
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .parameter
    .parameter
    .parameter

    .prologue
    const v8, 0x7f0902aa

    const v7, 0x7f0300b0

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 47
    const/4 v1, 0x0

    .line 49
    iget-object v0, p0, Lcom/chase/sig/android/view/ah;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/ai;

    .line 51
    invoke-virtual {v0}, Lcom/chase/sig/android/view/ai;->a()Z

    move-result v3

    .line 52
    if-eqz p2, :cond_4

    .line 53
    check-cast p2, Lcom/chase/sig/android/view/aj;

    .line 55
    if-nez v3, :cond_1

    .line 56
    invoke-virtual {p2}, Lcom/chase/sig/android/view/aj;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/chase/sig/android/view/aj$a;

    .line 58
    iget v2, v0, Lcom/chase/sig/android/view/ai;->a:I

    if-ne v2, v7, :cond_0

    .line 59
    iget-object v2, v1, Lcom/chase/sig/android/view/aj$a;->a:Landroid/widget/TextView;

    sget-object v4, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 60
    iget-object v2, v1, Lcom/chase/sig/android/view/aj$a;->a:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 63
    :cond_0
    iget-object v2, v1, Lcom/chase/sig/android/view/aj$a;->a:Landroid/widget/TextView;

    sget-object v4, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 64
    iget-object v2, v1, Lcom/chase/sig/android/view/aj$a;->a:Landroid/widget/TextView;

    iget-object v4, v0, Lcom/chase/sig/android/view/ai;->b:Ljava/lang/String;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 65
    iget-object v2, v1, Lcom/chase/sig/android/view/aj$a;->b:Landroid/widget/TextView;

    iget-object v4, v0, Lcom/chase/sig/android/view/ai;->c:Ljava/lang/String;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 66
    iget-object v2, v1, Lcom/chase/sig/android/view/aj$a;->b:Landroid/widget/TextView;

    iget v4, v0, Lcom/chase/sig/android/view/ai;->k:I

    invoke-direct {p0, v4}, Lcom/chase/sig/android/view/ah;->a(I)I

    move-result v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 68
    iget-object v2, v1, Lcom/chase/sig/android/view/aj$a;->d:Landroid/widget/TextView;

    if-eqz v2, :cond_1

    .line 69
    iget-object v2, v1, Lcom/chase/sig/android/view/aj$a;->d:Landroid/widget/TextView;

    iget-object v4, v0, Lcom/chase/sig/android/view/ai;->g:Ljava/lang/String;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 70
    iget-object v2, v1, Lcom/chase/sig/android/view/aj$a;->d:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 116
    :cond_1
    :goto_0
    if-nez v3, :cond_2

    .line 118
    iget-boolean v2, v0, Lcom/chase/sig/android/view/ai;->f:Z

    if-eqz v2, :cond_9

    .line 119
    iget-object v1, v1, Lcom/chase/sig/android/view/aj$a;->c:Landroid/widget/ImageView;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 125
    :cond_2
    :goto_1
    iget-boolean v0, v0, Lcom/chase/sig/android/view/ai;->i:Z

    if-eqz v0, :cond_3

    .line 126
    invoke-virtual {p2, v8}, Lcom/chase/sig/android/view/aj;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 127
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 128
    invoke-static {v1}, Lcom/chase/sig/android/util/s;->p(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 129
    invoke-static {v1}, Lcom/chase/sig/android/util/s;->q(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 130
    invoke-static {v1}, Lcom/chase/sig/android/util/s;->r(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 131
    sget-object v2, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v0, v2, v6}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 132
    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 135
    :cond_3
    return-object p2

    .line 74
    :cond_4
    new-instance p2, Lcom/chase/sig/android/view/aj;

    invoke-virtual {p0}, Lcom/chase/sig/android/view/ah;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v4, p0, Lcom/chase/sig/android/view/ah;->b:Landroid/view/LayoutInflater;

    invoke-direct {p2, v2, v0, v4}, Lcom/chase/sig/android/view/aj;-><init>(Landroid/content/Context;Lcom/chase/sig/android/view/ai;Landroid/view/LayoutInflater;)V

    .line 76
    iget v2, v0, Lcom/chase/sig/android/view/ai;->a:I

    const v4, 0x7f0300b9

    if-ne v2, v4, :cond_5

    .line 77
    invoke-virtual {p2, v6}, Lcom/chase/sig/android/view/aj;->setFocusable(Z)V

    .line 78
    invoke-virtual {p2, v6}, Lcom/chase/sig/android/view/aj;->setClickable(Z)V

    .line 81
    :cond_5
    invoke-virtual {p2, v6}, Lcom/chase/sig/android/view/aj;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 83
    if-nez v3, :cond_1

    .line 84
    new-instance v2, Lcom/chase/sig/android/view/aj$a;

    invoke-direct {v2}, Lcom/chase/sig/android/view/aj$a;-><init>()V

    .line 86
    invoke-virtual {v4, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v2, Lcom/chase/sig/android/view/aj$a;->a:Landroid/widget/TextView;

    .line 88
    iget v1, v0, Lcom/chase/sig/android/view/ai;->a:I

    if-ne v1, v7, :cond_6

    .line 89
    iget-object v1, v2, Lcom/chase/sig/android/view/aj$a;->a:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 90
    iget-object v1, v2, Lcom/chase/sig/android/view/aj$a;->a:Landroid/widget/TextView;

    sget-object v5, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 93
    :cond_6
    const v1, 0x7f0902ac

    invoke-virtual {v4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v2, Lcom/chase/sig/android/view/aj$a;->b:Landroid/widget/TextView;

    .line 94
    const v1, 0x7f090009

    invoke-virtual {v4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, v2, Lcom/chase/sig/android/view/aj$a;->c:Landroid/widget/ImageView;

    .line 96
    const v1, 0x7f0902ab

    invoke-virtual {v4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 97
    if-eqz v1, :cond_7

    .line 98
    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v2, Lcom/chase/sig/android/view/aj$a;->d:Landroid/widget/TextView;

    .line 100
    iget-object v1, v0, Lcom/chase/sig/android/view/ai;->g:Ljava/lang/String;

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 101
    iget-object v1, v2, Lcom/chase/sig/android/view/aj$a;->d:Landroid/widget/TextView;

    iget-object v4, v0, Lcom/chase/sig/android/view/ai;->g:Ljava/lang/String;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 102
    iget-object v1, v2, Lcom/chase/sig/android/view/aj$a;->d:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 108
    :cond_7
    :goto_2
    invoke-virtual {p2, v2}, Lcom/chase/sig/android/view/aj;->setTag(Ljava/lang/Object;)V

    .line 110
    iget-object v1, v2, Lcom/chase/sig/android/view/aj$a;->a:Landroid/widget/TextView;

    iget-object v4, v0, Lcom/chase/sig/android/view/ai;->b:Ljava/lang/String;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 111
    iget-object v1, v2, Lcom/chase/sig/android/view/aj$a;->b:Landroid/widget/TextView;

    iget-object v4, v0, Lcom/chase/sig/android/view/ai;->c:Ljava/lang/String;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 112
    iget-object v1, v2, Lcom/chase/sig/android/view/aj$a;->b:Landroid/widget/TextView;

    iget v4, v0, Lcom/chase/sig/android/view/ai;->k:I

    invoke-direct {p0, v4}, Lcom/chase/sig/android/view/ah;->a(I)I

    move-result v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setTextColor(I)V

    move-object v1, v2

    goto/16 :goto_0

    .line 104
    :cond_8
    iget-object v1, v2, Lcom/chase/sig/android/view/aj$a;->d:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    .line 121
    :cond_9
    iget-object v1, v1, Lcom/chase/sig/android/view/aj$a;->c:Landroid/widget/ImageView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_1
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 41
    const/16 v0, 0xd

    return v0
.end method
