.class final Lcom/chase/sig/android/view/al;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/text/Html$ImageGetter;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/view/TickerValueTextView;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/view/TickerValueTextView;)V
    .locals 0
    .parameter

    .prologue
    .line 143
    iput-object p1, p0, Lcom/chase/sig/android/view/al;->a:Lcom/chase/sig/android/view/TickerValueTextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 2
    .parameter

    .prologue
    .line 146
    const-string v0, "positive"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 147
    iget-object v0, p0, Lcom/chase/sig/android/view/al;->a:Lcom/chase/sig/android/view/TickerValueTextView;

    const v1, 0x7f02009e

    invoke-static {v0, v1}, Lcom/chase/sig/android/view/TickerValueTextView;->a(Lcom/chase/sig/android/view/TickerValueTextView;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 152
    :goto_0
    return-object v0

    .line 148
    :cond_0
    const-string v0, "negative"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 149
    iget-object v0, p0, Lcom/chase/sig/android/view/al;->a:Lcom/chase/sig/android/view/TickerValueTextView;

    const v1, 0x7f0200ec

    invoke-static {v0, v1}, Lcom/chase/sig/android/view/TickerValueTextView;->a(Lcom/chase/sig/android/view/TickerValueTextView;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 152
    :cond_1
    iget-object v0, p0, Lcom/chase/sig/android/view/al;->a:Lcom/chase/sig/android/view/TickerValueTextView;

    const v1, 0x7f02008c

    invoke-static {v0, v1}, Lcom/chase/sig/android/view/TickerValueTextView;->a(Lcom/chase/sig/android/view/TickerValueTextView;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method
