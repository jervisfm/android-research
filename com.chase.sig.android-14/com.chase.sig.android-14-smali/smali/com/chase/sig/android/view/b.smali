.class public final Lcom/chase/sig/android/view/b;
.super Landroid/app/Dialog;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/view/b$a;
    }
.end annotation


# static fields
.field private static e:Ljava/lang/String;


# instance fields
.field private a:Ljava/util/Calendar;

.field private b:Ljava/util/Calendar;

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/widget/TextView;

.field private f:Lcom/chase/sig/android/view/b$a;

.field private g:Ljava/text/SimpleDateFormat;

.field private h:Ljava/util/Calendar;

.field private i:Ljava/util/Date;

.field private j:Z

.field private k:Z

.field private l:Ljava/lang/Boolean;

.field private m:Ljava/util/Calendar;

.field private n:Ljava/util/Calendar;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/chase/sig/android/view/b$a;ZZLjava/util/Calendar;Ljava/util/Calendar;)V
    .locals 8
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 57
    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/chase/sig/android/view/b;-><init>(Landroid/content/Context;Lcom/chase/sig/android/view/b$a;ZZLjava/util/Calendar;Ljava/util/Calendar;Ljava/util/Calendar;)V

    .line 58
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/chase/sig/android/view/b$a;ZZLjava/util/Calendar;Ljava/util/Calendar;Ljava/util/Calendar;)V
    .locals 3
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v2, 0x1

    .line 64
    invoke-direct {p0, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 43
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "MMM dd, yyyy"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/chase/sig/android/view/b;->g:Ljava/text/SimpleDateFormat;

    .line 66
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/view/b;->a:Ljava/util/Calendar;

    .line 68
    iput-object p6, p0, Lcom/chase/sig/android/view/b;->b:Ljava/util/Calendar;

    .line 69
    iput-boolean p3, p0, Lcom/chase/sig/android/view/b;->j:Z

    .line 70
    iput-boolean p4, p0, Lcom/chase/sig/android/view/b;->k:Z

    .line 72
    invoke-virtual {p5}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/view/b;->i:Ljava/util/Date;

    .line 73
    iput-object p7, p0, Lcom/chase/sig/android/view/b;->n:Ljava/util/Calendar;

    .line 74
    iput-object p2, p0, Lcom/chase/sig/android/view/b;->f:Lcom/chase/sig/android/view/b$a;

    .line 75
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/view/b;->l:Ljava/lang/Boolean;

    .line 77
    invoke-virtual {p0, v2}, Lcom/chase/sig/android/view/b;->requestWindowFeature(I)Z

    .line 78
    const v0, 0x7f030019

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/view/b;->setContentView(I)V

    .line 80
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070294

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/chase/sig/android/view/b;->e:Ljava/lang/String;

    .line 82
    const v0, 0x7f090065

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/view/b;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/chase/sig/android/view/b;->d:Landroid/widget/TextView;

    .line 83
    const v0, 0x7f090060

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/view/b;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/chase/sig/android/view/b;->c:Landroid/widget/TextView;

    .line 85
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/view/b;->h:Ljava/util/Calendar;

    .line 86
    iget-object v0, p0, Lcom/chase/sig/android/view/b;->h:Ljava/util/Calendar;

    const/4 v1, 0x5

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 88
    iput-object p5, p0, Lcom/chase/sig/android/view/b;->m:Ljava/util/Calendar;

    .line 89
    iget-object v0, p0, Lcom/chase/sig/android/view/b;->m:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/chase/sig/android/view/b;->b(Ljava/util/Date;)V

    .line 91
    const v0, 0x7f09005f

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/view/b;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/chase/sig/android/view/c;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/view/c;-><init>(Lcom/chase/sig/android/view/b;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 101
    const v0, 0x7f090061

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/view/b;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/chase/sig/android/view/d;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/view/d;-><init>(Lcom/chase/sig/android/view/b;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 111
    new-instance v0, Lcom/chase/sig/android/view/e;

    invoke-direct {v0, p0}, Lcom/chase/sig/android/view/e;-><init>(Lcom/chase/sig/android/view/b;)V

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/view/b;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 124
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/view/b;)Ljava/util/Calendar;
    .locals 1
    .parameter

    .prologue
    .line 26
    iget-object v0, p0, Lcom/chase/sig/android/view/b;->h:Ljava/util/Calendar;

    return-object v0
.end method

.method private a(Ljava/lang/Class;Landroid/view/ViewGroup;)Ljava/util/List;
    .locals 4
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Landroid/view/ViewGroup;",
            ")",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 340
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 342
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 343
    invoke-virtual {p2, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 344
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 345
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 348
    :cond_0
    instance-of v3, v0, Landroid/view/ViewGroup;

    if-eqz v3, :cond_1

    .line 349
    check-cast v0, Landroid/view/ViewGroup;

    invoke-direct {p0, p1, v0}, Lcom/chase/sig/android/view/b;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 342
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 352
    :cond_2
    return-object v2
.end method

.method static synthetic a(Lcom/chase/sig/android/view/b;Ljava/util/Date;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/chase/sig/android/view/b;->d(Ljava/util/Date;)V

    return-void
.end method

.method private a(Ljava/util/Calendar;)Z
    .locals 4
    .parameter

    .prologue
    const/4 v3, 0x7

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 317
    iget-object v2, p0, Lcom/chase/sig/android/view/b;->m:Ljava/util/Calendar;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/chase/sig/android/view/b;->m:Ljava/util/Calendar;

    invoke-virtual {p1, v2}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    iget-object v2, p0, Lcom/chase/sig/android/view/b;->b:Ljava/util/Calendar;

    invoke-virtual {p1, v2}, Ljava/util/Calendar;->after(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 335
    :cond_1
    :goto_0
    return v0

    .line 324
    :cond_2
    iget-boolean v2, p0, Lcom/chase/sig/android/view/b;->k:Z

    if-eqz v2, :cond_3

    .line 325
    invoke-virtual {p1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    .line 326
    if-eq v2, v3, :cond_1

    if-eq v2, v1, :cond_1

    .line 331
    :cond_3
    iget-boolean v2, p0, Lcom/chase/sig/android/view/b;->j:Z

    if-eqz v2, :cond_4

    invoke-static {p1}, Lcom/chase/sig/android/util/l;->a(Ljava/util/Calendar;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_4
    move v0, v1

    .line 335
    goto :goto_0
.end method

.method private static b(Landroid/widget/TextView;)Ljava/util/Date;
    .locals 1
    .parameter

    .prologue
    .line 271
    invoke-virtual {p0}, Landroid/widget/TextView;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 272
    const/4 v0, 0x0

    .line 275
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Landroid/widget/TextView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Date;

    goto :goto_0
.end method

.method private b()V
    .locals 6

    .prologue
    const/4 v2, 0x2

    const/4 v5, 0x5

    const/4 v4, 0x1

    .line 159
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 160
    iget-object v0, p0, Lcom/chase/sig/android/view/b;->h:Ljava/util/Calendar;

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v0

    invoke-virtual {v1, v2, v0}, Ljava/util/Calendar;->set(II)V

    .line 161
    invoke-virtual {v1, v5, v4}, Ljava/util/Calendar;->set(II)V

    .line 162
    iget-object v0, p0, Lcom/chase/sig/android/view/b;->h:Ljava/util/Calendar;

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v0

    invoke-virtual {v1, v4, v0}, Ljava/util/Calendar;->set(II)V

    .line 164
    const/4 v0, 0x7

    invoke-virtual {v1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x7

    add-int/lit8 v0, v0, -0x1

    rem-int/lit8 v0, v0, 0x7

    .line 165
    neg-int v0, v0

    invoke-virtual {v1, v5, v0}, Ljava/util/Calendar;->add(II)V

    .line 167
    const-class v2, Landroid/widget/TextView;

    const v0, 0x7f090063

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/view/b;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-direct {p0, v2, v0}, Lcom/chase/sig/android/view/b;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Ljava/util/List;

    move-result-object v0

    .line 169
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 170
    invoke-virtual {v1, v5}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v3

    .line 171
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 172
    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 173
    invoke-direct {p0, v1}, Lcom/chase/sig/android/view/b;->a(Ljava/util/Calendar;)Z

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 174
    invoke-direct {p0, v0}, Lcom/chase/sig/android/view/b;->c(Landroid/widget/TextView;)V

    .line 175
    invoke-virtual {v1, v5, v4}, Ljava/util/Calendar;->add(II)V

    goto :goto_0

    .line 177
    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/chase/sig/android/view/b;)V
    .locals 0
    .parameter

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/chase/sig/android/view/b;->b()V

    return-void
.end method

.method private b(Ljava/util/Date;)V
    .locals 3
    .parameter

    .prologue
    .line 144
    iget-object v0, p0, Lcom/chase/sig/android/view/b;->d:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/chase/sig/android/view/b;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Lcom/chase/sig/android/util/s;->a(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 145
    return-void
.end method

.method private c(Ljava/util/Date;)Ljava/lang/String;
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x2

    .line 180
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 181
    invoke-virtual {v0, p1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 183
    invoke-direct {p0, v0}, Lcom/chase/sig/android/view/b;->a(Ljava/util/Calendar;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 184
    const-string v0, "BLACK_OUT"

    .line 199
    :goto_0
    return-object v0

    .line 187
    :cond_0
    iget-object v1, p0, Lcom/chase/sig/android/view/b;->i:Ljava/util/Date;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/chase/sig/android/view/b;->i:Ljava/util/Date;

    invoke-static {v1, v0}, Lcom/chase/sig/android/util/l;->a(Ljava/util/Date;Ljava/util/Calendar;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 188
    const-string v0, "LAST_SELECTED"

    goto :goto_0

    .line 191
    :cond_1
    iget-object v1, p0, Lcom/chase/sig/android/view/b;->n:Ljava/util/Calendar;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/chase/sig/android/view/b;->n:Ljava/util/Calendar;

    invoke-static {v1, v0}, Lcom/chase/sig/android/util/l;->a(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 192
    const-string v0, "DUE_DATE"

    goto :goto_0

    .line 195
    :cond_2
    iget-object v1, p0, Lcom/chase/sig/android/view/b;->h:Ljava/util/Calendar;

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v0

    if-eq v1, v0, :cond_3

    .line 196
    const-string v0, "OTHER_MONTH"

    goto :goto_0

    .line 199
    :cond_3
    const-string v0, "AVAILABLE"

    goto :goto_0
.end method

.method private c(Landroid/widget/TextView;)V
    .locals 3
    .parameter

    .prologue
    .line 279
    invoke-static {p1}, Lcom/chase/sig/android/view/b;->b(Landroid/widget/TextView;)Ljava/util/Date;

    move-result-object v0

    .line 280
    if-nez v0, :cond_1

    .line 308
    :cond_0
    :goto_0
    return-void

    .line 284
    :cond_1
    invoke-direct {p0, v0}, Lcom/chase/sig/android/view/b;->c(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 285
    invoke-virtual {p0}, Lcom/chase/sig/android/view/b;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060037

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 287
    const-string v1, "BLACK_OUT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 288
    const v1, 0x7f020068

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 291
    :cond_2
    const-string v1, "AVAILABLE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 292
    const v1, 0x7f020066

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 295
    :cond_3
    const-string v1, "OTHER_MONTH"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 296
    const v1, 0x7f020065

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 299
    :cond_4
    const-string v1, "LAST_SELECTED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 300
    const v1, 0x7f02006f

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 303
    :cond_5
    const-string v1, "DUE_DATE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 304
    invoke-virtual {p0}, Lcom/chase/sig/android/view/b;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f060044

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 306
    const v0, 0x7f02006a

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method private d(Ljava/util/Date;)V
    .locals 2
    .parameter

    .prologue
    .line 216
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "MMMM yyyy"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 217
    invoke-virtual {v0, p1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 218
    iget-object v1, p0, Lcom/chase/sig/android/view/b;->c:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 219
    return-void
.end method


# virtual methods
.method public final a()Lcom/chase/sig/android/view/b;
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 356
    const v0, 0x7f090064

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/view/b;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/chase/sig/android/view/b;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 357
    return-object p0
.end method

.method public final a(Ljava/util/Date;)Lcom/chase/sig/android/view/b;
    .locals 1
    .parameter

    .prologue
    .line 153
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/view/b;->m:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object p1

    :cond_0
    iput-object p1, p0, Lcom/chase/sig/android/view/b;->i:Ljava/util/Date;

    .line 154
    iget-object v0, p0, Lcom/chase/sig/android/view/b;->i:Ljava/util/Date;

    invoke-direct {p0, v0}, Lcom/chase/sig/android/view/b;->b(Ljava/util/Date;)V

    .line 155
    return-object p0
.end method

.method public final a(Landroid/widget/TextView;)V
    .locals 3
    .parameter

    .prologue
    .line 222
    invoke-virtual {p1}, Landroid/widget/TextView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Date;

    iput-object v0, p0, Lcom/chase/sig/android/view/b;->i:Ljava/util/Date;

    .line 223
    iget-object v0, p0, Lcom/chase/sig/android/view/b;->d:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/chase/sig/android/view/b;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/chase/sig/android/view/b;->i:Ljava/util/Date;

    invoke-static {v2}, Lcom/chase/sig/android/util/s;->a(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 225
    iget-object v0, p0, Lcom/chase/sig/android/view/b;->f:Lcom/chase/sig/android/view/b$a;

    iget-object v1, p0, Lcom/chase/sig/android/view/b;->i:Ljava/util/Date;

    invoke-interface {v0, v1}, Lcom/chase/sig/android/view/b$a;->a(Ljava/util/Date;)V

    .line 226
    invoke-virtual {p0}, Lcom/chase/sig/android/view/b;->dismiss()V

    .line 227
    return-void
.end method

.method protected final onCreate(Landroid/os/Bundle;)V
    .locals 1
    .parameter

    .prologue
    .line 129
    if-eqz p1, :cond_0

    const-string v0, "LAST_DISPLAYED"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 130
    const-string v0, "LAST_DISPLAYED"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    iput-object v0, p0, Lcom/chase/sig/android/view/b;->h:Ljava/util/Calendar;

    .line 131
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/view/b;->l:Ljava/lang/Boolean;

    .line 133
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    .line 134
    return-void
.end method

.method public final onFocusChange(Landroid/view/View;Z)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 205
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Date;

    .line 206
    new-instance v1, Ljava/lang/StringBuffer;

    sget-object v2, Lcom/chase/sig/android/view/b;->e:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 207
    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 208
    iget-object v2, p0, Lcom/chase/sig/android/view/b;->g:Ljava/text/SimpleDateFormat;

    invoke-virtual {v2, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 210
    iget-object v2, p0, Lcom/chase/sig/android/view/b;->d:Landroid/widget/TextView;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 212
    invoke-direct {p0, v0}, Lcom/chase/sig/android/view/b;->d(Ljava/util/Date;)V

    .line 213
    return-void
.end method

.method public final onSaveInstanceState()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 138
    invoke-super {p0}, Landroid/app/Dialog;->onSaveInstanceState()Landroid/os/Bundle;

    move-result-object v0

    .line 139
    const-string v1, "LAST_DISPLAYED"

    iget-object v2, p0, Lcom/chase/sig/android/view/b;->h:Ljava/util/Calendar;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 140
    return-object v0
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7
    .parameter

    .prologue
    const/4 v6, 0x1

    .line 244
    const-class v1, Landroid/widget/TextView;

    const v0, 0x7f090063

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/view/b;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-direct {p0, v1, v0}, Lcom/chase/sig/android/view/b;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Ljava/util/List;

    move-result-object v0

    .line 246
    const/4 v1, 0x0

    .line 247
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 248
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 249
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 250
    invoke-direct {p0, v0}, Lcom/chase/sig/android/view/b;->c(Landroid/widget/TextView;)V

    .line 251
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-eqz v3, :cond_3

    :goto_1
    move-object v1, v0

    .line 254
    goto :goto_0

    .line 256
    :cond_0
    if-eqz v1, :cond_1

    .line 257
    invoke-static {v1}, Lcom/chase/sig/android/view/b;->b(Landroid/widget/TextView;)Ljava/util/Date;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/chase/sig/android/view/b;->c(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "BLACK_OUT"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 267
    :cond_1
    :goto_2
    return v6

    .line 261
    :cond_2
    const v0, 0x7f06003a

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 262
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-ne v0, v6, :cond_1

    .line 263
    invoke-virtual {p0, v1}, Lcom/chase/sig/android/view/b;->a(Landroid/widget/TextView;)V

    goto :goto_2

    :cond_3
    move-object v0, v1

    goto :goto_1
.end method

.method public final show()V
    .locals 3

    .prologue
    .line 231
    iget-object v0, p0, Lcom/chase/sig/android/view/b;->l:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 232
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/view/b;->l:Ljava/lang/Boolean;

    .line 237
    :goto_0
    iget-object v0, p0, Lcom/chase/sig/android/view/b;->h:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/chase/sig/android/view/b;->d(Ljava/util/Date;)V

    .line 238
    invoke-direct {p0}, Lcom/chase/sig/android/view/b;->b()V

    .line 239
    invoke-super {p0}, Landroid/app/Dialog;->show()V

    .line 240
    return-void

    .line 234
    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/view/b;->h:Ljava/util/Calendar;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/chase/sig/android/view/b;->i:Ljava/util/Date;

    invoke-virtual {v2}, Ljava/util/Date;->getYear()I

    move-result v2

    add-int/lit16 v2, v2, 0x76c

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 235
    iget-object v0, p0, Lcom/chase/sig/android/view/b;->h:Ljava/util/Calendar;

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/chase/sig/android/view/b;->i:Ljava/util/Date;

    invoke-virtual {v2}, Ljava/util/Date;->getMonth()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    goto :goto_0
.end method
