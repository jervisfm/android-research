.class public Lcom/chase/sig/android/view/detail/DetailRow;
.super Lcom/chase/sig/android/view/detail/a;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/view/detail/a",
        "<",
        "Lcom/chase/sig/android/view/detail/DetailRow;",
        "Ljava/lang/String;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 13
    invoke-direct {p0, p1, p2}, Lcom/chase/sig/android/view/detail/a;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    .line 14
    return-void
.end method


# virtual methods
.method protected final a(Landroid/content/Context;)V
    .locals 3
    .parameter

    .prologue
    .line 32
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Lcom/chase/sig/android/view/detail/a;)V

    invoke-virtual {p0}, Lcom/chase/sig/android/view/detail/DetailRow;->p()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p0}, Lcom/chase/sig/android/view/detail/DetailRow;->h()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 33
    return-void
.end method

.method final synthetic a(Ljava/lang/Object;)V
    .locals 2
    .parameter

    .prologue
    .line 10
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0}, Lcom/chase/sig/android/view/detail/DetailRow;->p()Landroid/widget/TextView;

    move-result-object v0

    invoke-static {p1}, Lcom/chase/sig/android/util/s;->s(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    return v0
.end method

.method public final e(I)Lcom/chase/sig/android/view/detail/DetailRow;
    .locals 0
    .parameter

    .prologue
    .line 26
    iput p1, p0, Lcom/chase/sig/android/view/detail/DetailRow;->f:I

    .line 27
    return-object p0
.end method

.method public final p()Landroid/widget/TextView;
    .locals 2

    .prologue
    .line 22
    iget-object v0, p0, Lcom/chase/sig/android/view/detail/DetailRow;->t:Landroid/view/View;

    iget v1, p0, Lcom/chase/sig/android/view/detail/DetailRow;->s:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 43
    return-void
.end method
