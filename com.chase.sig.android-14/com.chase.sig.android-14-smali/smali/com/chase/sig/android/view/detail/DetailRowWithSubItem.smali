.class public Lcom/chase/sig/android/view/detail/DetailRowWithSubItem;
.super Lcom/chase/sig/android/view/detail/a;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/view/detail/a",
        "<",
        "Lcom/chase/sig/android/view/detail/DetailRow;",
        "Ljava/lang/String;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# instance fields
.field a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 17
    invoke-direct {p0, p1, p3}, Lcom/chase/sig/android/view/detail/a;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    .line 14
    const-string v0, ""

    iput-object v0, p0, Lcom/chase/sig/android/view/detail/DetailRowWithSubItem;->a:Ljava/lang/String;

    .line 18
    iput-object p2, p0, Lcom/chase/sig/android/view/detail/DetailRowWithSubItem;->a:Ljava/lang/String;

    .line 19
    return-void
.end method

.method private p()Landroid/widget/TextView;
    .locals 2

    .prologue
    .line 32
    iget-object v0, p0, Lcom/chase/sig/android/view/detail/DetailRowWithSubItem;->t:Landroid/view/View;

    iget v1, p0, Lcom/chase/sig/android/view/detail/DetailRowWithSubItem;->s:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method protected final a(Landroid/content/Context;)V
    .locals 3
    .parameter

    .prologue
    .line 37
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Lcom/chase/sig/android/view/detail/a;)V

    invoke-direct {p0}, Lcom/chase/sig/android/view/detail/DetailRowWithSubItem;->p()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p0}, Lcom/chase/sig/android/view/detail/DetailRowWithSubItem;->h()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 38
    iget-object v0, p0, Lcom/chase/sig/android/view/detail/DetailRowWithSubItem;->t:Landroid/view/View;

    const v1, 0x7f0900a1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/chase/sig/android/view/detail/DetailRowWithSubItem;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 39
    return-void
.end method

.method final synthetic a(Ljava/lang/Object;)V
    .locals 2
    .parameter

    .prologue
    .line 11
    check-cast p1, Ljava/lang/String;

    invoke-direct {p0}, Lcom/chase/sig/android/view/detail/DetailRowWithSubItem;->p()Landroid/widget/TextView;

    move-result-object v0

    invoke-static {p1}, Lcom/chase/sig/android/util/s;->s(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    return v0
.end method

.method public final j()I
    .locals 1

    .prologue
    .line 28
    const v0, 0x7f03002c

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 49
    return-void
.end method
