.class public Lcom/chase/sig/android/view/detail/DetailView;
.super Landroid/widget/LinearLayout;
.source "SourceFile"


# instance fields
.field private a:Landroid/view/LayoutInflater;

.field private b:[Lcom/chase/sig/android/view/detail/a;

.field private c:Ljava/util/Random;

.field private d:Z

.field private e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    .line 29
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/chase/sig/android/view/detail/DetailView;->c:Ljava/util/Random;

    .line 25
    iput-boolean v1, p0, Lcom/chase/sig/android/view/detail/DetailView;->d:Z

    .line 26
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/chase/sig/android/view/detail/DetailView;->e:Ljava/util/ArrayList;

    .line 30
    invoke-virtual {p0, v1}, Lcom/chase/sig/android/view/detail/DetailView;->setOrientation(I)V

    .line 31
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/chase/sig/android/view/detail/DetailView;->a:Landroid/view/LayoutInflater;

    .line 32
    return-void
.end method

.method private a(I)V
    .locals 8
    .parameter

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 146
    iget-object v0, p0, Lcom/chase/sig/android/view/detail/DetailView;->b:[Lcom/chase/sig/android/view/detail/a;

    aget-object v3, v0, p1

    .line 147
    invoke-virtual {v3}, Lcom/chase/sig/android/view/detail/a;->k()Landroid/view/View;

    move-result-object v4

    .line 148
    invoke-direct {p0, p1}, Lcom/chase/sig/android/view/detail/DetailView;->b(I)Z

    move-result v5

    move v0, v1

    :goto_0
    iget-object v6, p0, Lcom/chase/sig/android/view/detail/DetailView;->b:[Lcom/chase/sig/android/view/detail/a;

    array-length v6, v6

    if-ge v0, v6, :cond_6

    iget-object v6, p0, Lcom/chase/sig/android/view/detail/DetailView;->b:[Lcom/chase/sig/android/view/detail/a;

    aget-object v6, v6, v0

    iget-boolean v6, v6, Lcom/chase/sig/android/view/detail/a;->j:Z

    if-nez v6, :cond_5

    if-ne p1, v0, :cond_4

    const/4 v0, 0x1

    :goto_1
    iget-boolean v6, p0, Lcom/chase/sig/android/view/detail/DetailView;->d:Z

    if-eqz v6, :cond_a

    if-eqz v0, :cond_7

    if-eqz v5, :cond_7

    const v0, 0x7f0200d3

    :goto_2
    invoke-virtual {v4, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 150
    const v0, 0x7f090015

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 151
    iget-boolean v5, v3, Lcom/chase/sig/android/view/detail/a;->n:Z

    if-eqz v5, :cond_b

    .line 152
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 157
    :goto_3
    const v0, 0x7f090013

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 158
    invoke-virtual {p0}, Lcom/chase/sig/android/view/detail/DetailView;->b()Z

    move-result v0

    if-eqz v0, :cond_c

    iget-boolean v0, v3, Lcom/chase/sig/android/view/detail/a;->l:Z

    if-nez v0, :cond_c

    .line 159
    invoke-virtual {v5, v2}, Landroid/view/View;->setVisibility(I)V

    .line 164
    :goto_4
    invoke-direct {p0, p1}, Lcom/chase/sig/android/view/detail/DetailView;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 165
    invoke-virtual {v5, v2}, Landroid/view/View;->setVisibility(I)V

    .line 168
    :cond_0
    const v0, 0x7f090009

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 169
    if-eqz v6, :cond_1

    .line 170
    iget-boolean v0, v3, Lcom/chase/sig/android/view/detail/a;->m:Z

    if-eqz v0, :cond_d

    move v0, v1

    :goto_5
    invoke-virtual {v6, v0}, Landroid/view/View;->setVisibility(I)V

    .line 173
    :cond_1
    const v0, 0x7f09009f

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 174
    if-eqz v0, :cond_2

    .line 175
    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 176
    const v0, 0x7f09009d

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 177
    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v7, 0x3

    invoke-virtual {v6}, Landroid/view/View;->getId()I

    move-result v6

    invoke-virtual {v0, v7, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    invoke-virtual {v5, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 180
    :cond_2
    iget-object v0, v3, Lcom/chase/sig/android/view/detail/a;->h:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 181
    iget-boolean v0, v3, Lcom/chase/sig/android/view/detail/a;->j:Z

    if-eqz v0, :cond_3

    move v1, v2

    :cond_3
    invoke-virtual {v4, v1}, Landroid/view/View;->setVisibility(I)V

    .line 182
    return-void

    :cond_4
    move v0, v1

    .line 148
    goto :goto_1

    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    :cond_6
    move v0, v1

    goto/16 :goto_1

    :cond_7
    if-eqz v0, :cond_8

    const v0, 0x7f0200d2

    goto/16 :goto_2

    :cond_8
    if-eqz v5, :cond_9

    const v0, 0x7f0200d1

    goto/16 :goto_2

    :cond_9
    const v0, 0x7f0200d0

    goto/16 :goto_2

    :cond_a
    invoke-direct {p0}, Lcom/chase/sig/android/view/detail/DetailView;->getBackgroundResourceForStraightCorners()I

    move-result v0

    goto/16 :goto_2

    .line 154
    :cond_b
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_3

    .line 161
    :cond_c
    invoke-virtual {v5, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_4

    .line 170
    :cond_d
    const/4 v0, 0x4

    goto :goto_5
.end method

.method private b(I)Z
    .locals 3
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 195
    iget-object v1, p0, Lcom/chase/sig/android/view/detail/DetailView;->b:[Lcom/chase/sig/android/view/detail/a;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    :goto_0
    if-ltz v1, :cond_0

    .line 196
    iget-object v2, p0, Lcom/chase/sig/android/view/detail/DetailView;->b:[Lcom/chase/sig/android/view/detail/a;

    aget-object v2, v2, v1

    iget-boolean v2, v2, Lcom/chase/sig/android/view/detail/a;->j:Z

    if-nez v2, :cond_1

    .line 197
    if-ne p1, v1, :cond_0

    const/4 v0, 0x1

    .line 201
    :cond_0
    return v0

    .line 195
    :cond_1
    add-int/lit8 v1, v1, -0x1

    goto :goto_0
.end method

.method private c(I)V
    .locals 2
    .parameter

    .prologue
    .line 270
    invoke-direct {p0, p1}, Lcom/chase/sig/android/view/detail/DetailView;->d(I)Landroid/widget/TextView;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setError(Ljava/lang/CharSequence;)V

    .line 271
    return-void
.end method

.method private d(I)Landroid/widget/TextView;
    .locals 2
    .parameter

    .prologue
    .line 278
    invoke-virtual {p0, p1}, Lcom/chase/sig/android/view/detail/DetailView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 279
    const v1, 0x7f090015

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private g(Ljava/lang/String;)I
    .locals 2
    .parameter

    .prologue
    .line 257
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/chase/sig/android/view/detail/DetailView;->b:[Lcom/chase/sig/android/view/detail/a;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 258
    iget-object v1, p0, Lcom/chase/sig/android/view/detail/DetailView;->b:[Lcom/chase/sig/android/view/detail/a;

    aget-object v1, v1, v0

    iget-object v1, v1, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    if-ne v1, p1, :cond_0

    .line 262
    :goto_1
    return v0

    .line 257
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 262
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private getBackgroundResourceForStraightCorners()I
    .locals 1

    .prologue
    .line 245
    const v0, 0x7f0200d0

    return v0
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 54
    iget-object v1, p0, Lcom/chase/sig/android/view/detail/DetailView;->b:[Lcom/chase/sig/android/view/detail/a;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 55
    invoke-virtual {v3}, Lcom/chase/sig/android/view/detail/a;->a()V

    .line 54
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 57
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1
    .parameter

    .prologue
    .line 249
    invoke-direct {p0, p1}, Lcom/chase/sig/android/view/detail/DetailView;->g(Ljava/lang/String;)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/chase/sig/android/view/detail/DetailView;->c(I)V

    .line 250
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2
    .parameter

    .prologue
    .line 253
    invoke-direct {p0, p1}, Lcom/chase/sig/android/view/detail/DetailView;->g(Ljava/lang/String;)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/chase/sig/android/view/detail/DetailView;->d(I)Landroid/widget/TextView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setError(Ljava/lang/CharSequence;)V

    .line 254
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 295
    const/4 v0, 0x0

    return v0
.end method

.method public final c(Ljava/lang/String;)Ljava/lang/Object;
    .locals 2
    .parameter

    .prologue
    .line 283
    iget-object v0, p0, Lcom/chase/sig/android/view/detail/DetailView;->b:[Lcom/chase/sig/android/view/detail/a;

    invoke-direct {p0, p1}, Lcom/chase/sig/android/view/detail/DetailView;->g(Ljava/lang/String;)I

    move-result v1

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/a;->f()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 299
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/chase/sig/android/view/detail/DetailView;->b:[Lcom/chase/sig/android/view/detail/a;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 300
    invoke-direct {p0, v0}, Lcom/chase/sig/android/view/detail/DetailView;->a(I)V

    .line 299
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 302
    :cond_0
    return-void
.end method

.method public final d(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .parameter

    .prologue
    .line 287
    iget-object v0, p0, Lcom/chase/sig/android/view/detail/DetailView;->b:[Lcom/chase/sig/android/view/detail/a;

    invoke-direct {p0, p1}, Lcom/chase/sig/android/view/detail/DetailView;->g(Ljava/lang/String;)I

    move-result v1

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/a;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 305
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/chase/sig/android/view/detail/DetailView;->d:Z

    .line 306
    return-void
.end method

.method public final e(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;
    .locals 2
    .parameter

    .prologue
    .line 291
    iget-object v0, p0, Lcom/chase/sig/android/view/detail/DetailView;->b:[Lcom/chase/sig/android/view/detail/a;

    invoke-direct {p0, p1}, Lcom/chase/sig/android/view/detail/DetailView;->g(Ljava/lang/String;)I

    move-result v1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public final f(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;
    .locals 5
    .parameter

    .prologue
    .line 313
    iget-object v2, p0, Lcom/chase/sig/android/view/detail/DetailView;->b:[Lcom/chase/sig/android/view/detail/a;

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 314
    iget-object v4, v0, Lcom/chase/sig/android/view/detail/a;->r:Ljava/lang/String;

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 318
    :goto_1
    return-object v0

    .line 313
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 318
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public getRows()[Lcom/chase/sig/android/view/detail/a;
    .locals 1

    .prologue
    .line 309
    iget-object v0, p0, Lcom/chase/sig/android/view/detail/DetailView;->b:[Lcom/chase/sig/android/view/detail/a;

    return-object v0
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 6
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 61
    instance-of v0, p1, Lcom/chase/sig/android/view/detail/DetailViewState;

    if-eqz v0, :cond_3

    .line 62
    check-cast p1, Lcom/chase/sig/android/view/detail/DetailViewState;

    .line 63
    iget-object v0, p0, Lcom/chase/sig/android/view/detail/DetailView;->b:[Lcom/chase/sig/android/view/detail/a;

    if-eqz v0, :cond_2

    .line 64
    invoke-virtual {p1}, Lcom/chase/sig/android/view/detail/DetailViewState;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 65
    iget-object v0, p0, Lcom/chase/sig/android/view/detail/DetailView;->b:[Lcom/chase/sig/android/view/detail/a;

    array-length v0, v0

    iget-object v2, p0, Lcom/chase/sig/android/view/detail/DetailView;->e:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-static {v2, v4}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v4

    move v2, v1

    .line 69
    :goto_0
    if-ge v2, v4, :cond_0

    .line 70
    iget-object v0, p0, Lcom/chase/sig/android/view/detail/DetailView;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/view/detail/DetailView;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v5, v0}, Landroid/view/View;->setId(I)V

    .line 71
    iget-object v0, p0, Lcom/chase/sig/android/view/detail/DetailView;->b:[Lcom/chase/sig/android/view/detail/a;

    aget-object v5, v0, v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v5, v0}, Lcom/chase/sig/android/view/detail/a;->a(I)V

    .line 69
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 74
    :cond_0
    iput-object v3, p0, Lcom/chase/sig/android/view/detail/DetailView;->e:Ljava/util/ArrayList;

    .line 76
    invoke-virtual {p1}, Lcom/chase/sig/android/view/detail/DetailViewState;->b()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 77
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/chase/sig/android/view/detail/DetailView;->c(I)V

    goto :goto_1

    .line 80
    :cond_1
    invoke-virtual {p1}, Lcom/chase/sig/android/view/detail/DetailViewState;->c()Ljava/util/ArrayList;

    move-result-object v2

    .line 81
    :goto_2
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 82
    iget-object v0, p0, Lcom/chase/sig/android/view/detail/DetailView;->b:[Lcom/chase/sig/android/view/detail/a;

    aget-object v3, v0, v1

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {v3, v0}, Lcom/chase/sig/android/view/detail/a;->a(Landroid/os/Parcelable;)V

    .line 81
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 85
    :cond_2
    invoke-virtual {p1}, Lcom/chase/sig/android/view/detail/DetailViewState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/LinearLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 89
    :goto_3
    return-void

    .line 87
    :cond_3
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    goto :goto_3
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 36
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 37
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 39
    iget-object v0, p0, Lcom/chase/sig/android/view/detail/DetailView;->b:[Lcom/chase/sig/android/view/detail/a;

    if-eqz v0, :cond_2

    move v0, v1

    .line 40
    :goto_0
    iget-object v2, p0, Lcom/chase/sig/android/view/detail/DetailView;->b:[Lcom/chase/sig/android/view/detail/a;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 41
    invoke-direct {p0, v0}, Lcom/chase/sig/android/view/detail/DetailView;->d(I)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/TextView;->getError()Ljava/lang/CharSequence;

    move-result-object v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :goto_1
    if-eqz v2, :cond_0

    .line 42
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 45
    :cond_0
    iget-object v2, p0, Lcom/chase/sig/android/view/detail/DetailView;->b:[Lcom/chase/sig/android/view/detail/a;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/chase/sig/android/view/detail/a;->o()Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 40
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v2, v1

    .line 41
    goto :goto_1

    .line 49
    :cond_2
    new-instance v0, Lcom/chase/sig/android/view/detail/DetailViewState;

    invoke-super {p0}, Landroid/widget/LinearLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    iget-object v2, p0, Lcom/chase/sig/android/view/detail/DetailView;->e:Ljava/util/ArrayList;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/chase/sig/android/view/detail/DetailViewState;-><init>(Landroid/os/Parcelable;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    return-object v0
.end method

.method public varargs setRows([Lcom/chase/sig/android/view/detail/a;)V
    .locals 9
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 92
    invoke-virtual {p0}, Lcom/chase/sig/android/view/detail/DetailView;->removeAllViews()V

    .line 93
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    array-length v3, p1

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, p1, v0

    if-eqz v4, :cond_0

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    new-array v0, v1, [Lcom/chase/sig/android/view/detail/a;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/chase/sig/android/view/detail/a;

    iput-object v0, p0, Lcom/chase/sig/android/view/detail/DetailView;->b:[Lcom/chase/sig/android/view/detail/a;

    .line 95
    iget-object v0, p0, Lcom/chase/sig/android/view/detail/DetailView;->b:[Lcom/chase/sig/android/view/detail/a;

    array-length v3, v0

    move v2, v1

    .line 97
    :goto_1
    if-ge v2, v3, :cond_6

    .line 98
    iget-object v0, p0, Lcom/chase/sig/android/view/detail/DetailView;->b:[Lcom/chase/sig/android/view/detail/a;

    aget-object v4, v0, v2

    .line 99
    invoke-virtual {v4}, Lcom/chase/sig/android/view/detail/a;->l()V

    .line 101
    iget-object v0, p0, Lcom/chase/sig/android/view/detail/DetailView;->a:Landroid/view/LayoutInflater;

    invoke-virtual {v4}, Lcom/chase/sig/android/view/detail/a;->j()I

    move-result v5

    const/4 v6, 0x0

    invoke-virtual {v0, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 103
    iget-object v0, v4, Lcom/chase/sig/android/view/detail/a;->h:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    .line 105
    :goto_2
    invoke-virtual {v5, v0}, Landroid/view/View;->setClickable(Z)V

    .line 106
    invoke-virtual {v5, v0}, Landroid/view/View;->setFocusable(Z)V

    .line 107
    const v6, 0x7f090009

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 109
    if-eqz v6, :cond_2

    .line 110
    if-eqz v0, :cond_5

    move v0, v1

    :goto_3
    invoke-virtual {v6, v0}, Landroid/view/View;->setVisibility(I)V

    .line 113
    :cond_2
    const v0, 0x7f090015

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 114
    iget-object v6, v4, Lcom/chase/sig/android/view/detail/a;->c:Ljava/lang/String;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 115
    invoke-virtual {p0}, Lcom/chase/sig/android/view/detail/DetailView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    iget v7, v4, Lcom/chase/sig/android/view/detail/a;->e:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setTextColor(I)V

    .line 118
    const v0, 0x7f090014

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 120
    iget-boolean v6, v4, Lcom/chase/sig/android/view/detail/a;->q:Z

    if-nez v6, :cond_3

    .line 122
    invoke-virtual {v5, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 128
    :cond_3
    iget-object v6, p0, Lcom/chase/sig/android/view/detail/DetailView;->c:Ljava/util/Random;

    invoke-virtual {v6}, Ljava/util/Random;->nextInt()I

    move-result v6

    .line 129
    if-ltz v6, :cond_3

    invoke-virtual {p0, v6}, Lcom/chase/sig/android/view/detail/DetailView;->findViewById(I)Landroid/view/View;

    move-result-object v7

    if-nez v7, :cond_3

    .line 134
    iget-object v7, p0, Lcom/chase/sig/android/view/detail/DetailView;->e:Ljava/util/ArrayList;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 136
    invoke-virtual {v0, v6}, Landroid/view/View;->setId(I)V

    .line 137
    invoke-virtual {v4, v6}, Lcom/chase/sig/android/view/detail/a;->a(I)V

    .line 139
    invoke-virtual {p0, v5}, Lcom/chase/sig/android/view/detail/DetailView;->addView(Landroid/view/View;)V

    .line 140
    invoke-virtual {p0}, Lcom/chase/sig/android/view/detail/DetailView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v4, v5, v0}, Lcom/chase/sig/android/view/detail/a;->a(Landroid/view/View;Landroid/content/Context;)V

    .line 141
    invoke-direct {p0, v2}, Lcom/chase/sig/android/view/detail/DetailView;->a(I)V

    .line 97
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_1

    :cond_4
    move v0, v1

    .line 103
    goto :goto_2

    .line 110
    :cond_5
    const/4 v0, 0x4

    goto :goto_3

    .line 143
    :cond_6
    return-void
.end method
