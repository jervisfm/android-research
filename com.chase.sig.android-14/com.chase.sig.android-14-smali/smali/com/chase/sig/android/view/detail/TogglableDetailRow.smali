.class public Lcom/chase/sig/android/view/detail/TogglableDetailRow;
.super Lcom/chase/sig/android/view/detail/a;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/view/detail/a",
        "<",
        "Lcom/chase/sig/android/view/detail/DetailRow;",
        "Ljava/lang/String;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# instance fields
.field private a:Z

.field private u:Ljava/lang/String;

.field private v:Ljava/lang/String;

.field private w:Ljava/lang/String;

.field private x:Ljava/lang/String;

.field private y:Landroid/os/Bundle;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Landroid/os/Handler$Callback;Landroid/os/Bundle;)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 32
    invoke-direct {p0, p1, p2}, Lcom/chase/sig/android/view/detail/a;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    .line 20
    iput-boolean v0, p0, Lcom/chase/sig/android/view/detail/TogglableDetailRow;->a:Z

    .line 34
    iput-boolean v0, p0, Lcom/chase/sig/android/view/detail/a;->m:Z

    .line 36
    iput-object p4, p0, Lcom/chase/sig/android/view/detail/TogglableDetailRow;->y:Landroid/os/Bundle;

    .line 37
    iput-object p1, p0, Lcom/chase/sig/android/view/detail/TogglableDetailRow;->u:Ljava/lang/String;

    .line 38
    iput-object p2, p0, Lcom/chase/sig/android/view/detail/TogglableDetailRow;->w:Ljava/lang/String;

    .line 40
    iput-object p1, p0, Lcom/chase/sig/android/view/detail/a;->c:Ljava/lang/String;

    .line 41
    new-instance v0, Lcom/chase/sig/android/view/detail/v;

    invoke-direct {v0, p0, p3}, Lcom/chase/sig/android/view/detail/v;-><init>(Lcom/chase/sig/android/view/detail/TogglableDetailRow;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/chase/sig/android/view/detail/a;->h:Landroid/view/View$OnClickListener;

    .line 42
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/view/detail/TogglableDetailRow;)Landroid/os/Bundle;
    .locals 1
    .parameter

    .prologue
    .line 16
    iget-object v0, p0, Lcom/chase/sig/android/view/detail/TogglableDetailRow;->y:Landroid/os/Bundle;

    return-object v0
.end method

.method private r()Landroid/widget/TextView;
    .locals 2

    .prologue
    .line 75
    iget-object v0, p0, Lcom/chase/sig/android/view/detail/TogglableDetailRow;->t:Landroid/view/View;

    iget v1, p0, Lcom/chase/sig/android/view/detail/TogglableDetailRow;->s:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method protected final a(Landroid/content/Context;)V
    .locals 3
    .parameter

    .prologue
    .line 80
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Lcom/chase/sig/android/view/detail/a;)V

    iget-object v0, p0, Lcom/chase/sig/android/view/detail/TogglableDetailRow;->t:Landroid/view/View;

    const v1, 0x7f090015

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f06001b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 82
    return-void
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 2
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 116
    invoke-super {p0, p1}, Lcom/chase/sig/android/view/detail/a;->a(Landroid/os/Parcelable;)V

    .line 117
    instance-of v1, p1, Landroid/os/Bundle;

    if-eqz v1, :cond_1

    .line 118
    check-cast p1, Landroid/os/Bundle;

    .line 119
    const-string v1, "toggle_state"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    iput-boolean v0, p0, Lcom/chase/sig/android/view/detail/TogglableDetailRow;->a:Z

    .line 120
    invoke-virtual {p0}, Lcom/chase/sig/android/view/detail/TogglableDetailRow;->p()V

    .line 122
    :cond_1
    return-void
.end method

.method final synthetic a(Ljava/lang/Object;)V
    .locals 2
    .parameter

    .prologue
    .line 16
    check-cast p1, Ljava/lang/String;

    invoke-direct {p0}, Lcom/chase/sig/android/view/detail/TogglableDetailRow;->r()Landroid/widget/TextView;

    move-result-object v0

    invoke-static {p1}, Lcom/chase/sig/android/util/s;->s(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 45
    iput-object p1, p0, Lcom/chase/sig/android/view/detail/TogglableDetailRow;->v:Ljava/lang/String;

    .line 46
    iput-object p2, p0, Lcom/chase/sig/android/view/detail/TogglableDetailRow;->x:Ljava/lang/String;

    .line 47
    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 126
    const/4 v0, 0x0

    return v0
.end method

.method public final o()Landroid/os/Parcelable;
    .locals 3

    .prologue
    .line 109
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 110
    const-string v1, "toggle_state"

    iget-boolean v2, p0, Lcom/chase/sig/android/view/detail/TogglableDetailRow;->a:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 111
    return-object v0
.end method

.method public final p()V
    .locals 3

    .prologue
    .line 98
    iget-boolean v0, p0, Lcom/chase/sig/android/view/detail/TogglableDetailRow;->a:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/chase/sig/android/view/detail/TogglableDetailRow;->a:Z

    .line 99
    iget-boolean v0, p0, Lcom/chase/sig/android/view/detail/TogglableDetailRow;->a:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/chase/sig/android/view/detail/TogglableDetailRow;->v:Ljava/lang/String;

    move-object v1, v0

    :goto_1
    iput-object v1, p0, Lcom/chase/sig/android/view/detail/a;->c:Ljava/lang/String;

    iget-object v0, p0, Lcom/chase/sig/android/view/detail/TogglableDetailRow;->t:Landroid/view/View;

    const v2, 0x7f090015

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 100
    iget-boolean v0, p0, Lcom/chase/sig/android/view/detail/TogglableDetailRow;->a:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/chase/sig/android/view/detail/TogglableDetailRow;->x:Ljava/lang/String;

    :goto_2
    invoke-virtual {p0, v0}, Lcom/chase/sig/android/view/detail/TogglableDetailRow;->b(Ljava/lang/Object;)V

    invoke-direct {p0}, Lcom/chase/sig/android/view/detail/TogglableDetailRow;->r()Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 101
    return-void

    .line 98
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 99
    :cond_1
    iget-object v0, p0, Lcom/chase/sig/android/view/detail/TogglableDetailRow;->u:Ljava/lang/String;

    move-object v1, v0

    goto :goto_1

    .line 100
    :cond_2
    iget-object v0, p0, Lcom/chase/sig/android/view/detail/TogglableDetailRow;->w:Ljava/lang/String;

    goto :goto_2
.end method

.method public final q()Z
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/chase/sig/android/view/detail/TogglableDetailRow;->v:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/view/detail/TogglableDetailRow;->x:Ljava/lang/String;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 132
    return-void
.end method
