.class public abstract Lcom/chase/sig/android/view/detail/a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/chase/sig/android/view/detail/a;",
        "ValueType:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field protected d:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TValueType;"
        }
    .end annotation
.end field

.field public e:I

.field protected f:I

.field protected g:Ljava/lang/String;

.field public h:Landroid/view/View$OnClickListener;

.field public i:I

.field public j:Z

.field protected k:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TValueType;"
        }
    .end annotation
.end field

.field public l:Z

.field public m:Z

.field public n:Z

.field public o:Z

.field public p:Z

.field public q:Z

.field public r:Ljava/lang/String;

.field protected s:I

.field protected t:Landroid/view/View;


# direct methods
.method public constructor <init>(ILjava/lang/Object;)V
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITValueType;)V"
        }
    .end annotation

    .prologue
    .line 39
    invoke-static {p1}, Lcom/chase/sig/android/view/detail/a;->d(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/chase/sig/android/view/detail/a;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    .line 40
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "TValueType;)V"
        }
    .end annotation

    .prologue
    const v1, 0x7f060002

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    const-string v0, "UNDEFINED"

    iput-object v0, p0, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    .line 24
    const v0, 0x7f030023

    iput v0, p0, Lcom/chase/sig/android/view/detail/a;->i:I

    .line 32
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/chase/sig/android/view/detail/a;->q:Z

    .line 35
    const v0, 0x7f090014

    iput v0, p0, Lcom/chase/sig/android/view/detail/a;->s:I

    .line 43
    iput-object p1, p0, Lcom/chase/sig/android/view/detail/a;->c:Ljava/lang/String;

    .line 44
    iput-object p2, p0, Lcom/chase/sig/android/view/detail/a;->d:Ljava/lang/Object;

    .line 45
    iput-object p2, p0, Lcom/chase/sig/android/view/detail/a;->k:Ljava/lang/Object;

    .line 46
    iput v1, p0, Lcom/chase/sig/android/view/detail/a;->e:I

    .line 47
    iput v1, p0, Lcom/chase/sig/android/view/detail/a;->f:I

    .line 48
    return-void
.end method

.method protected static d(I)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 255
    invoke-static {}, Lcom/chase/sig/android/ChaseApplication;->a()Lcom/chase/sig/android/ChaseApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/ChaseApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 182
    iput-object p1, p0, Lcom/chase/sig/android/view/detail/a;->g:Ljava/lang/String;

    .line 183
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/chase/sig/android/view/detail/a;->k:Ljava/lang/Object;

    iput-object v0, p0, Lcom/chase/sig/android/view/detail/a;->d:Ljava/lang/Object;

    .line 56
    iget-object v0, p0, Lcom/chase/sig/android/view/detail/a;->k:Ljava/lang/Object;

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/view/detail/a;->a(Ljava/lang/Object;)V

    .line 57
    return-void
.end method

.method public final a(I)V
    .locals 0
    .parameter

    .prologue
    .line 121
    iput p1, p0, Lcom/chase/sig/android/view/detail/a;->s:I

    .line 122
    return-void
.end method

.method protected abstract a(Landroid/content/Context;)V
.end method

.method public a(Landroid/os/Parcelable;)V
    .locals 0
    .parameter

    .prologue
    .line 322
    return-void
.end method

.method protected final a(Landroid/view/View;Landroid/content/Context;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 221
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p0, p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Lcom/chase/sig/android/view/detail/a;Landroid/view/View;)V

    iput-object p1, p0, Lcom/chase/sig/android/view/detail/a;->t:Landroid/view/View;

    .line 222
    invoke-virtual {p0, p2}, Lcom/chase/sig/android/view/detail/a;->a(Landroid/content/Context;)V

    .line 223
    iget-object v0, p0, Lcom/chase/sig/android/view/detail/a;->k:Ljava/lang/Object;

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/view/detail/a;->a(Ljava/lang/Object;)V

    .line 224
    return-void
.end method

.method public a(Lcom/chase/sig/android/view/detail/g$b;)V
    .locals 0
    .parameter

    .prologue
    .line 312
    return-void
.end method

.method public a(Lcom/chase/sig/android/view/x;)V
    .locals 0
    .parameter

    .prologue
    .line 314
    return-void
.end method

.method abstract a(Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TValueType;)V"
        }
    .end annotation
.end method

.method public final b()Lcom/chase/sig/android/view/detail/a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 76
    iget-boolean v0, p0, Lcom/chase/sig/android/view/detail/a;->j:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/chase/sig/android/view/detail/a;->j:Z

    .line 77
    return-object p0

    .line 76
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(I)Lcom/chase/sig/android/view/detail/a;
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    .prologue
    .line 134
    iput p1, p0, Lcom/chase/sig/android/view/detail/a;->f:I

    .line 135
    return-object p0
.end method

.method protected b(Ljava/lang/Object;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TValueType;)V"
        }
    .end annotation

    .prologue
    .line 162
    iput-object p1, p0, Lcom/chase/sig/android/view/detail/a;->d:Ljava/lang/Object;

    .line 163
    return-void
.end method

.method public final c()Lcom/chase/sig/android/view/detail/a;
    .locals 1

    .prologue
    .line 99
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/chase/sig/android/view/detail/a;->l:Z

    return-object p0
.end method

.method public final c(I)Lcom/chase/sig/android/view/detail/a;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    .prologue
    .line 187
    invoke-static {p1}, Lcom/chase/sig/android/view/detail/a;->d(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/view/detail/a;->g:Ljava/lang/String;

    .line 188
    return-object p0
.end method

.method public final d()Lcom/chase/sig/android/view/detail/a;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/chase/sig/android/view/detail/a",
            "<TT;TValueType;>;"
        }
    .end annotation

    .prologue
    .line 108
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/chase/sig/android/view/detail/a;->m:Z

    return-object p0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 125
    iget v0, p0, Lcom/chase/sig/android/view/detail/a;->s:I

    return v0
.end method

.method public f()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/chase/sig/android/view/detail/a;->d:Ljava/lang/Object;

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 166
    invoke-virtual {p0}, Lcom/chase/sig/android/view/detail/a;->f()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/chase/sig/android/view/detail/a;->f()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 174
    iget v0, p0, Lcom/chase/sig/android/view/detail/a;->f:I

    return v0
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lcom/chase/sig/android/view/detail/a;->g:Ljava/lang/String;

    return-object v0
.end method

.method public j()I
    .locals 1

    .prologue
    .line 200
    iget v0, p0, Lcom/chase/sig/android/view/detail/a;->i:I

    return v0
.end method

.method public k()Landroid/view/View;
    .locals 1

    .prologue
    .line 229
    iget-object v0, p0, Lcom/chase/sig/android/view/detail/a;->t:Landroid/view/View;

    return-object v0
.end method

.method public final l()V
    .locals 1

    .prologue
    .line 250
    const v0, 0x7f090014

    iput v0, p0, Lcom/chase/sig/android/view/detail/a;->s:I

    .line 251
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/chase/sig/android/view/detail/a;->t:Landroid/view/View;

    .line 252
    return-void
.end method

.method public final m()Z
    .locals 1

    .prologue
    .line 267
    iget-boolean v0, p0, Lcom/chase/sig/android/view/detail/a;->o:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/chase/sig/android/view/detail/a;->p:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public n()Z
    .locals 1

    .prologue
    .line 299
    iget-boolean v0, p0, Lcom/chase/sig/android/view/detail/a;->o:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/chase/sig/android/view/detail/a;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/util/s;->l(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 300
    const/4 v0, 0x0

    .line 303
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public o()Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 317
    const/4 v0, 0x0

    return-object v0
.end method
