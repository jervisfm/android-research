.class public final Lcom/chase/sig/android/view/detail/c;
.super Lcom/chase/sig/android/view/detail/a;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/view/detail/a",
        "<",
        "Lcom/chase/sig/android/view/detail/c;",
        "Lcom/chase/sig/android/util/Dollar;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .parameter

    .prologue
    .line 12
    new-instance v0, Lcom/chase/sig/android/util/Dollar;

    const-string v1, ""

    invoke-direct {v0, v1}, Lcom/chase/sig/android/util/Dollar;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, p1, v0}, Lcom/chase/sig/android/view/detail/c;-><init>(Ljava/lang/String;Lcom/chase/sig/android/util/Dollar;)V

    .line 13
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/chase/sig/android/util/Dollar;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Lcom/chase/sig/android/view/detail/a;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    .line 17
    return-void
.end method

.method private a(Lcom/chase/sig/android/util/Dollar;)V
    .locals 2
    .parameter

    .prologue
    .line 53
    invoke-virtual {p0}, Lcom/chase/sig/android/view/detail/c;->p()Lcom/chase/sig/android/view/AmountView;

    move-result-object v0

    invoke-virtual {p1}, Lcom/chase/sig/android/util/Dollar;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/AmountView;->setText(Ljava/lang/CharSequence;)V

    .line 54
    return-void
.end method


# virtual methods
.method protected final a(Landroid/content/Context;)V
    .locals 2
    .parameter

    .prologue
    .line 48
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Lcom/chase/sig/android/view/detail/a;)V

    invoke-virtual {p0}, Lcom/chase/sig/android/view/detail/c;->p()Lcom/chase/sig/android/view/AmountView;

    move-result-object v0

    iget-object v1, p0, Lcom/chase/sig/android/view/detail/c;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/AmountView;->setHint(Ljava/lang/CharSequence;)V

    .line 49
    return-void
.end method

.method public final a(Lcom/chase/sig/android/view/detail/g$b;)V
    .locals 1
    .parameter

    .prologue
    .line 58
    invoke-virtual {p0}, Lcom/chase/sig/android/view/detail/c;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 59
    invoke-virtual {p0}, Lcom/chase/sig/android/view/detail/c;->p()Lcom/chase/sig/android/view/AmountView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/chase/sig/android/view/AmountView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 61
    :cond_0
    return-void
.end method

.method final bridge synthetic a(Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 9
    check-cast p1, Lcom/chase/sig/android/util/Dollar;

    invoke-direct {p0, p1}, Lcom/chase/sig/android/view/detail/c;->a(Lcom/chase/sig/android/util/Dollar;)V

    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1
    .parameter

    .prologue
    .line 43
    new-instance v0, Lcom/chase/sig/android/util/Dollar;

    invoke-direct {v0, p1}, Lcom/chase/sig/android/util/Dollar;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/chase/sig/android/view/detail/c;->a(Lcom/chase/sig/android/util/Dollar;)V

    .line 44
    return-void
.end method

.method public final f()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0}, Lcom/chase/sig/android/view/detail/c;->p()Lcom/chase/sig/android/view/AmountView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/view/AmountView;->getDollarAmount()Lcom/chase/sig/android/util/Dollar;

    move-result-object v0

    return-object v0
.end method

.method public final j()I
    .locals 1

    .prologue
    .line 21
    const v0, 0x7f03000c

    return v0
.end method

.method public final n()Z
    .locals 1

    .prologue
    .line 39
    invoke-virtual {p0}, Lcom/chase/sig/android/view/detail/c;->p()Lcom/chase/sig/android/view/AmountView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/view/AmountView;->a()Z

    move-result v0

    return v0
.end method

.method public final p()Lcom/chase/sig/android/view/AmountView;
    .locals 2

    .prologue
    .line 25
    invoke-virtual {p0}, Lcom/chase/sig/android/view/detail/c;->k()Landroid/view/View;

    move-result-object v0

    iget v1, p0, Lcom/chase/sig/android/view/detail/c;->s:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/AmountView;

    return-object v0
.end method

.method public final q()Lcom/chase/sig/android/util/Dollar;
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/chase/sig/android/view/detail/c;->p()Lcom/chase/sig/android/view/AmountView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/view/AmountView;->getDollarAmount()Lcom/chase/sig/android/util/Dollar;

    move-result-object v0

    return-object v0
.end method
