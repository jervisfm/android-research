.class public final Lcom/chase/sig/android/view/detail/d;
.super Lcom/chase/sig/android/view/detail/a;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/view/detail/a",
        "<",
        "Lcom/chase/sig/android/view/detail/d;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Landroid/view/View$OnClickListener;

.field private u:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 15
    invoke-direct {p0, p1, p2}, Lcom/chase/sig/android/view/detail/a;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    .line 12
    const/4 v0, -0x1

    iput v0, p0, Lcom/chase/sig/android/view/detail/d;->u:I

    .line 16
    return-void
.end method

.method private r()Landroid/widget/Button;
    .locals 2

    .prologue
    .line 46
    iget-object v0, p0, Lcom/chase/sig/android/view/detail/d;->t:Landroid/view/View;

    iget v1, p0, Lcom/chase/sig/android/view/detail/d;->s:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    return-object v0
.end method


# virtual methods
.method protected final a(Landroid/content/Context;)V
    .locals 2
    .parameter

    .prologue
    .line 34
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Lcom/chase/sig/android/view/detail/a;)V

    invoke-direct {p0}, Lcom/chase/sig/android/view/detail/d;->r()Landroid/widget/Button;

    move-result-object v0

    iget-object v1, p0, Lcom/chase/sig/android/view/detail/d;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setHint(Ljava/lang/CharSequence;)V

    .line 36
    iget-object v0, p0, Lcom/chase/sig/android/view/detail/d;->a:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    .line 37
    invoke-direct {p0}, Lcom/chase/sig/android/view/detail/d;->r()Landroid/widget/Button;

    move-result-object v0

    iget-object v1, p0, Lcom/chase/sig/android/view/detail/d;->a:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 40
    :cond_0
    iget v0, p0, Lcom/chase/sig/android/view/detail/d;->u:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 41
    invoke-direct {p0}, Lcom/chase/sig/android/view/detail/d;->r()Landroid/widget/Button;

    move-result-object v0

    iget v1, p0, Lcom/chase/sig/android/view/detail/d;->u:I

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 43
    :cond_1
    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 9
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/chase/sig/android/view/detail/d;->b(Ljava/lang/String;)V

    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1
    .parameter

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/chase/sig/android/view/detail/d;->r()Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 52
    return-void
.end method

.method public final synthetic f()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 9
    invoke-virtual {p0}, Lcom/chase/sig/android/view/detail/d;->p()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final j()I
    .locals 1

    .prologue
    .line 24
    const v0, 0x7f030024

    return v0
.end method

.method public final p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/chase/sig/android/view/detail/d;->t:Landroid/view/View;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/chase/sig/android/view/detail/d;->k:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Lcom/chase/sig/android/view/detail/d;->r()Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final q()Lcom/chase/sig/android/view/detail/d;
    .locals 1

    .prologue
    .line 60
    const v0, 0x7f020063

    iput v0, p0, Lcom/chase/sig/android/view/detail/d;->u:I

    return-object p0
.end method
