.class public final Lcom/chase/sig/android/view/detail/f;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/view/detail/f$b;,
        Lcom/chase/sig/android/view/detail/f$a;
    }
.end annotation


# instance fields
.field private a:Lcom/chase/sig/android/view/detail/DetailView;

.field private b:I

.field private c:Landroid/app/Activity;


# direct methods
.method public constructor <init>(Lcom/chase/sig/android/view/detail/DetailView;Landroid/app/Activity;I)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/chase/sig/android/view/detail/f;->a:Lcom/chase/sig/android/view/detail/DetailView;

    .line 24
    iput p3, p0, Lcom/chase/sig/android/view/detail/f;->b:I

    .line 25
    iput-object p2, p0, Lcom/chase/sig/android/view/detail/f;->c:Landroid/app/Activity;

    .line 26
    invoke-direct {p0}, Lcom/chase/sig/android/view/detail/f;->a()V

    .line 27
    return-void
.end method

.method private a()V
    .locals 7

    .prologue
    .line 30
    iget-object v0, p0, Lcom/chase/sig/android/view/detail/f;->a:Lcom/chase/sig/android/view/detail/DetailView;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/DetailView;->getRows()[Lcom/chase/sig/android/view/detail/a;

    move-result-object v3

    .line 31
    new-instance v4, Lcom/chase/sig/android/view/detail/f$a;

    invoke-direct {v4, p0}, Lcom/chase/sig/android/view/detail/f$a;-><init>(Lcom/chase/sig/android/view/detail/f;)V

    .line 33
    array-length v5, v3

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v5, :cond_3

    aget-object v1, v3, v2

    .line 34
    const-class v0, Lcom/chase/sig/android/view/detail/q;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 35
    check-cast v0, Lcom/chase/sig/android/view/detail/q;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/q;->q()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v0

    new-instance v6, Lcom/chase/sig/android/view/detail/f$b;

    invoke-direct {v6, p0}, Lcom/chase/sig/android/view/detail/f$b;-><init>(Lcom/chase/sig/android/view/detail/f;)V

    invoke-virtual {v0, v6}, Lcom/chase/sig/android/view/JPSpinner;->a(Lcom/chase/sig/android/view/x;)V

    .line 39
    :cond_0
    instance-of v0, v1, Lcom/chase/sig/android/view/detail/l;

    if-eqz v0, :cond_1

    move-object v0, v1

    .line 40
    check-cast v0, Lcom/chase/sig/android/view/detail/l;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/l;->p()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 42
    :cond_1
    instance-of v0, v1, Lcom/chase/sig/android/view/detail/c;

    if-eqz v0, :cond_2

    .line 43
    check-cast v1, Lcom/chase/sig/android/view/detail/c;

    invoke-virtual {v1}, Lcom/chase/sig/android/view/detail/c;->p()Lcom/chase/sig/android/view/AmountView;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/chase/sig/android/view/AmountView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 33
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 46
    :cond_3
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/view/detail/f;)V
    .locals 2
    .parameter

    .prologue
    .line 10
    iget-object v0, p0, Lcom/chase/sig/android/view/detail/f;->c:Landroid/app/Activity;

    iget v1, p0, Lcom/chase/sig/android/view/detail/f;->b:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    return-void
.end method
