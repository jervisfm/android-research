.class public final Lcom/chase/sig/android/view/detail/g;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/view/detail/g$a;,
        Lcom/chase/sig/android/view/detail/g$b;
    }
.end annotation


# instance fields
.field private a:Lcom/chase/sig/android/view/detail/DetailView;

.field private b:Lcom/chase/sig/android/activity/eb;

.field private c:I

.field private d:Lcom/chase/sig/android/view/detail/g$a;

.field private e:Lcom/chase/sig/android/view/detail/g$b;


# direct methods
.method public constructor <init>(Lcom/chase/sig/android/view/detail/DetailView;Lcom/chase/sig/android/activity/eb;I)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/chase/sig/android/view/detail/g;->a:Lcom/chase/sig/android/view/detail/DetailView;

    .line 26
    iput-object p2, p0, Lcom/chase/sig/android/view/detail/g;->b:Lcom/chase/sig/android/activity/eb;

    .line 27
    iput p3, p0, Lcom/chase/sig/android/view/detail/g;->c:I

    .line 28
    invoke-direct {p0}, Lcom/chase/sig/android/view/detail/g;->a()V

    .line 29
    return-void
.end method

.method private a()V
    .locals 5

    .prologue
    .line 32
    iget-object v0, p0, Lcom/chase/sig/android/view/detail/g;->a:Lcom/chase/sig/android/view/detail/DetailView;

    invoke-virtual {v0}, Lcom/chase/sig/android/view/detail/DetailView;->getRows()[Lcom/chase/sig/android/view/detail/a;

    move-result-object v1

    .line 33
    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 34
    const-class v4, Lcom/chase/sig/android/view/detail/q;

    invoke-virtual {v4, v3}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 35
    new-instance v4, Lcom/chase/sig/android/view/detail/g$a;

    invoke-direct {v4, p0}, Lcom/chase/sig/android/view/detail/g$a;-><init>(Lcom/chase/sig/android/view/detail/g;)V

    iput-object v4, p0, Lcom/chase/sig/android/view/detail/g;->d:Lcom/chase/sig/android/view/detail/g$a;

    .line 36
    iget-object v4, p0, Lcom/chase/sig/android/view/detail/g;->d:Lcom/chase/sig/android/view/detail/g$a;

    invoke-virtual {v3, v4}, Lcom/chase/sig/android/view/detail/a;->a(Lcom/chase/sig/android/view/x;)V

    .line 38
    :cond_0
    new-instance v4, Lcom/chase/sig/android/view/detail/g$b;

    invoke-direct {v4, p0}, Lcom/chase/sig/android/view/detail/g$b;-><init>(Lcom/chase/sig/android/view/detail/g;)V

    iput-object v4, p0, Lcom/chase/sig/android/view/detail/g;->e:Lcom/chase/sig/android/view/detail/g$b;

    .line 39
    iget-object v4, p0, Lcom/chase/sig/android/view/detail/g;->e:Lcom/chase/sig/android/view/detail/g$b;

    invoke-virtual {v3, v4}, Lcom/chase/sig/android/view/detail/a;->a(Lcom/chase/sig/android/view/detail/g$b;)V

    .line 33
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 41
    :cond_1
    iget-object v0, p0, Lcom/chase/sig/android/view/detail/g;->b:Lcom/chase/sig/android/activity/eb;

    invoke-direct {p0}, Lcom/chase/sig/android/view/detail/g;->b()Z

    move-result v1

    iget v2, p0, Lcom/chase/sig/android/view/detail/g;->c:I

    invoke-virtual {v0, v1, v2}, Lcom/chase/sig/android/activity/eb;->b(ZI)V

    .line 42
    return-void
.end method

.method static synthetic a(Lcom/chase/sig/android/view/detail/g;)V
    .locals 3
    .parameter

    .prologue
    .line 10
    iget-object v0, p0, Lcom/chase/sig/android/view/detail/g;->b:Lcom/chase/sig/android/activity/eb;

    invoke-direct {p0}, Lcom/chase/sig/android/view/detail/g;->b()Z

    move-result v1

    iget v2, p0, Lcom/chase/sig/android/view/detail/g;->c:I

    invoke-virtual {v0, v1, v2}, Lcom/chase/sig/android/activity/eb;->a(ZI)V

    return-void
.end method

.method private b()Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 49
    iget-object v2, p0, Lcom/chase/sig/android/view/detail/g;->a:Lcom/chase/sig/android/view/detail/DetailView;

    invoke-virtual {v2}, Lcom/chase/sig/android/view/detail/DetailView;->getRows()[Lcom/chase/sig/android/view/detail/a;

    move-result-object v4

    .line 50
    array-length v5, v4

    move v3, v0

    :goto_0
    if-ge v3, v5, :cond_2

    aget-object v2, v4, v3

    .line 51
    iget-boolean v6, v2, Lcom/chase/sig/android/view/detail/a;->o:Z

    if-eqz v6, :cond_0

    invoke-virtual {v2}, Lcom/chase/sig/android/view/detail/a;->g()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/chase/sig/android/util/s;->l(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v2, v1

    :goto_1
    if-eqz v2, :cond_1

    .line 55
    :goto_2
    return v0

    :cond_0
    move v2, v0

    .line 51
    goto :goto_1

    .line 50
    :cond_1
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    :cond_2
    move v0, v1

    .line 55
    goto :goto_2
.end method
