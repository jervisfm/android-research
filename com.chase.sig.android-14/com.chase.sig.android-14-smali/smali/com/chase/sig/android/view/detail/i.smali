.class public final Lcom/chase/sig/android/view/detail/i;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Lcom/chase/sig/android/view/detail/DetailView;


# direct methods
.method public constructor <init>(Lcom/chase/sig/android/view/detail/DetailView;)V
    .locals 0
    .parameter

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/chase/sig/android/view/detail/i;->a:Lcom/chase/sig/android/view/detail/DetailView;

    .line 15
    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;)V
    .locals 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/service/IServiceError;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 36
    if-nez p1, :cond_1

    .line 50
    :cond_0
    return-void

    .line 40
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/service/IServiceError;

    .line 41
    invoke-interface {v0}, Lcom/chase/sig/android/service/IServiceError;->e()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/service/ServiceErrorAttribute;

    .line 42
    invoke-virtual {v0}, Lcom/chase/sig/android/service/ServiceErrorAttribute;->b()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 43
    iget-object v3, p0, Lcom/chase/sig/android/view/detail/i;->a:Lcom/chase/sig/android/view/detail/DetailView;

    invoke-virtual {v0}, Lcom/chase/sig/android/service/ServiceErrorAttribute;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/chase/sig/android/view/detail/DetailView;->f(Ljava/lang/String;)Lcom/chase/sig/android/view/detail/a;

    move-result-object v0

    .line 44
    if-eqz v0, :cond_3

    .line 45
    iget-object v3, p0, Lcom/chase/sig/android/view/detail/i;->a:Lcom/chase/sig/android/view/detail/DetailView;

    iget-object v0, v0, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    invoke-virtual {v3, v0}, Lcom/chase/sig/android/view/detail/DetailView;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a()Z
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 19
    const/4 v0, 0x1

    .line 21
    iget-object v2, p0, Lcom/chase/sig/android/view/detail/i;->a:Lcom/chase/sig/android/view/detail/DetailView;

    invoke-virtual {v2}, Lcom/chase/sig/android/view/detail/DetailView;->getRows()[Lcom/chase/sig/android/view/detail/a;

    move-result-object v3

    .line 22
    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 23
    invoke-virtual {v5}, Lcom/chase/sig/android/view/detail/a;->n()Z

    move-result v6

    if-nez v6, :cond_0

    .line 24
    iget-object v0, p0, Lcom/chase/sig/android/view/detail/i;->a:Lcom/chase/sig/android/view/detail/DetailView;

    iget-object v5, v5, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    invoke-virtual {v0, v5}, Lcom/chase/sig/android/view/detail/DetailView;->a(Ljava/lang/String;)V

    move v0, v1

    .line 22
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 27
    :cond_0
    iget-object v6, p0, Lcom/chase/sig/android/view/detail/i;->a:Lcom/chase/sig/android/view/detail/DetailView;

    iget-object v5, v5, Lcom/chase/sig/android/view/detail/a;->b:Ljava/lang/String;

    invoke-virtual {v6, v5}, Lcom/chase/sig/android/view/detail/DetailView;->b(Ljava/lang/String;)V

    goto :goto_1

    .line 31
    :cond_1
    return v0
.end method
