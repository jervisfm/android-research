.class public Lcom/chase/sig/android/view/detail/j;
.super Lcom/chase/sig/android/view/detail/a;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/view/detail/a",
        "<",
        "Lcom/chase/sig/android/view/detail/j;",
        "Lcom/chase/sig/android/view/f;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/chase/sig/android/view/f;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Lcom/chase/sig/android/view/detail/a;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    .line 18
    return-void
.end method


# virtual methods
.method protected a(Landroid/content/Context;)V
    .locals 1
    .parameter

    .prologue
    .line 37
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Lcom/chase/sig/android/view/detail/a;)V

    return-void
.end method

.method final synthetic a(Ljava/lang/Object;)V
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x0

    .line 13
    check-cast p1, Lcom/chase/sig/android/view/f;

    invoke-virtual {p0}, Lcom/chase/sig/android/view/detail/j;->q()Landroid/widget/CheckBox;

    move-result-object v0

    iget-boolean v1, p1, Lcom/chase/sig/android/view/f;->b:Z

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    invoke-virtual {p0}, Lcom/chase/sig/android/view/detail/j;->p()Landroid/widget/EditText;

    move-result-object v0

    iget-object v1, p1, Lcom/chase/sig/android/view/f;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-boolean v0, p1, Lcom/chase/sig/android/view/f;->b:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/chase/sig/android/view/detail/j;->q()Landroid/widget/CheckBox;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    invoke-virtual {p0}, Lcom/chase/sig/android/view/detail/j;->p()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setEnabled(Z)V

    invoke-virtual {p0}, Lcom/chase/sig/android/view/detail/j;->p()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setFocusable(Z)V

    invoke-virtual {p0}, Lcom/chase/sig/android/view/detail/j;->p()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    :cond_0
    invoke-virtual {p0}, Lcom/chase/sig/android/view/detail/j;->q()Landroid/widget/CheckBox;

    move-result-object v0

    new-instance v1, Lcom/chase/sig/android/view/detail/k;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/view/detail/k;-><init>(Lcom/chase/sig/android/view/detail/j;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    return-void
.end method

.method public final f()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 27
    invoke-virtual {p0}, Lcom/chase/sig/android/view/detail/j;->p()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    return-object v0
.end method

.method public final j()I
    .locals 1

    .prologue
    .line 22
    const v0, 0x7f030025

    return v0
.end method

.method public final k()Landroid/view/View;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/chase/sig/android/view/detail/j;->t:Landroid/view/View;

    return-object v0
.end method

.method public final p()Landroid/widget/EditText;
    .locals 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/chase/sig/android/view/detail/j;->t:Landroid/view/View;

    iget v1, p0, Lcom/chase/sig/android/view/detail/j;->s:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    return-object v0
.end method

.method public final q()Landroid/widget/CheckBox;
    .locals 2

    .prologue
    .line 44
    iget-object v0, p0, Lcom/chase/sig/android/view/detail/j;->t:Landroid/view/View;

    const v1, 0x7f09009f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    return-object v0
.end method
