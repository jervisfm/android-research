.class final Lcom/chase/sig/android/view/detail/l$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/view/detail/l;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/chase/sig/android/view/detail/l;

.field private b:Z

.field private c:Z

.field private d:I

.field private e:Z


# direct methods
.method private constructor <init>(Lcom/chase/sig/android/view/detail/l;)V
    .locals 0
    .parameter

    .prologue
    .line 266
    iput-object p1, p0, Lcom/chase/sig/android/view/detail/l$a;->a:Lcom/chase/sig/android/view/detail/l;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/chase/sig/android/view/detail/l;B)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 266
    invoke-direct {p0, p1}, Lcom/chase/sig/android/view/detail/l$a;-><init>(Lcom/chase/sig/android/view/detail/l;)V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 5
    .parameter

    .prologue
    const/4 v4, 0x5

    const/4 v1, 0x0

    .line 277
    iget-boolean v0, p0, Lcom/chase/sig/android/view/detail/l$a;->b:Z

    if-nez v0, :cond_5

    .line 278
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/chase/sig/android/view/detail/l$a;->b:Z

    .line 282
    iget-boolean v0, p0, Lcom/chase/sig/android/view/detail/l$a;->c:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/chase/sig/android/view/detail/l$a;->d:I

    if-lez v0, :cond_0

    .line 283
    iget-boolean v0, p0, Lcom/chase/sig/android/view/detail/l$a;->e:Z

    if-eqz v0, :cond_1

    .line 284
    iget v0, p0, Lcom/chase/sig/android/view/detail/l$a;->d:I

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 285
    iget v0, p0, Lcom/chase/sig/android/view/detail/l$a;->d:I

    add-int/lit8 v0, v0, -0x1

    iget v2, p0, Lcom/chase/sig/android/view/detail/l$a;->d:I

    invoke-interface {p1, v0, v2}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    .line 292
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/chase/sig/android/view/detail/l$a;->a:Lcom/chase/sig/android/view/detail/l;

    move v0, v1

    :goto_1
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v2

    if-ge v0, v2, :cond_3

    invoke-interface {p1, v0}, Landroid/text/Editable;->charAt(I)C

    move-result v2

    const/16 v3, 0x2d

    if-ne v2, v3, :cond_2

    add-int/lit8 v2, v0, 0x1

    invoke-interface {p1, v0, v2}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    goto :goto_1

    .line 287
    :cond_1
    iget v0, p0, Lcom/chase/sig/android/view/detail/l$a;->d:I

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 288
    iget v0, p0, Lcom/chase/sig/android/view/detail/l$a;->d:I

    iget v2, p0, Lcom/chase/sig/android/view/detail/l$a;->d:I

    add-int/lit8 v2, v2, 0x1

    invoke-interface {p1, v0, v2}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    goto :goto_0

    .line 292
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    if-le v0, v4, :cond_4

    const-string v0, "-"

    invoke-interface {p1, v4, v0}, Landroid/text/Editable;->insert(ILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 294
    :cond_4
    iput-boolean v1, p0, Lcom/chase/sig/android/view/detail/l$a;->b:Z

    .line 296
    :cond_5
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 6
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 309
    iget-boolean v0, p0, Lcom/chase/sig/android/view/detail/l$a;->b:Z

    if-nez v0, :cond_0

    .line 311
    invoke-static {p1}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v0

    .line 312
    invoke-static {p1}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v1

    .line 313
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-le v2, v4, :cond_2

    if-ne p3, v4, :cond_2

    if-nez p4, :cond_2

    invoke-interface {p1, p2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    const/16 v3, 0x2d

    if-ne v2, v3, :cond_2

    if-ne v0, v1, :cond_2

    .line 318
    iput-boolean v4, p0, Lcom/chase/sig/android/view/detail/l$a;->c:Z

    .line 319
    iput p2, p0, Lcom/chase/sig/android/view/detail/l$a;->d:I

    .line 321
    add-int/lit8 v1, p2, 0x1

    if-ne v0, v1, :cond_1

    .line 322
    iput-boolean v4, p0, Lcom/chase/sig/android/view/detail/l$a;->e:Z

    .line 330
    :cond_0
    :goto_0
    return-void

    .line 324
    :cond_1
    iput-boolean v5, p0, Lcom/chase/sig/android/view/detail/l$a;->e:Z

    goto :goto_0

    .line 327
    :cond_2
    iput-boolean v5, p0, Lcom/chase/sig/android/view/detail/l$a;->c:Z

    goto :goto_0
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 335
    return-void
.end method
