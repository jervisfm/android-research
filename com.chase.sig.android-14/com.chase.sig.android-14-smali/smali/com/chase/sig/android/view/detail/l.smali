.class public final Lcom/chase/sig/android/view/detail/l;
.super Lcom/chase/sig/android/view/detail/a;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/view/detail/l$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/view/detail/a",
        "<",
        "Lcom/chase/sig/android/view/detail/l;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field public a:I

.field public u:I

.field public v:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 62
    invoke-direct {p0, p1, p2}, Lcom/chase/sig/android/view/detail/a;-><init>(ILjava/lang/Object;)V

    .line 24
    iput v0, p0, Lcom/chase/sig/android/view/detail/l;->a:I

    .line 25
    iput v0, p0, Lcom/chase/sig/android/view/detail/l;->u:I

    .line 27
    const-string v0, ""

    iput-object v0, p0, Lcom/chase/sig/android/view/detail/l;->v:Ljava/lang/String;

    .line 63
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .parameter

    .prologue
    .line 50
    const-string v0, ""

    invoke-direct {p0, p1, v0}, Lcom/chase/sig/android/view/detail/l;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 58
    invoke-direct {p0, p1, p2}, Lcom/chase/sig/android/view/detail/a;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    .line 24
    iput v0, p0, Lcom/chase/sig/android/view/detail/l;->a:I

    .line 25
    iput v0, p0, Lcom/chase/sig/android/view/detail/l;->u:I

    .line 27
    const-string v0, ""

    iput-object v0, p0, Lcom/chase/sig/android/view/detail/l;->v:Ljava/lang/String;

    .line 59
    return-void
.end method


# virtual methods
.method protected final a(Landroid/content/Context;)V
    .locals 4
    .parameter

    .prologue
    const/4 v3, 0x0

    .line 85
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Lcom/chase/sig/android/view/detail/a;)V

    invoke-virtual {p0}, Lcom/chase/sig/android/view/detail/l;->p()Landroid/widget/EditText;

    move-result-object v0

    iget-object v1, p0, Lcom/chase/sig/android/view/detail/l;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 89
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget v1, p0, Lcom/chase/sig/android/view/detail/l;->a:I

    if-lez v1, :cond_0

    new-instance v1, Landroid/text/InputFilter$LengthFilter;

    iget v2, p0, Lcom/chase/sig/android/view/detail/l;->a:I

    invoke-direct {v1, v2}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    iget v1, p0, Lcom/chase/sig/android/view/detail/l;->u:I

    packed-switch v1, :pswitch_data_0

    :goto_0
    iget-object v1, p0, Lcom/chase/sig/android/view/detail/l;->v:Ljava/lang/String;

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->m(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Lcom/chase/sig/android/view/detail/m;

    invoke-direct {v1, p0}, Lcom/chase/sig/android/view/detail/m;-><init>(Lcom/chase/sig/android/view/detail/l;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-virtual {p0}, Lcom/chase/sig/android/view/detail/l;->p()Landroid/widget/EditText;

    move-result-object v1

    new-array v2, v3, [Landroid/text/InputFilter;

    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/InputFilter;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 92
    const/4 v0, 0x0

    iget v1, p0, Lcom/chase/sig/android/view/detail/l;->u:I

    packed-switch v1, :pswitch_data_1

    :goto_1
    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/chase/sig/android/view/detail/l;->p()Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    invoke-virtual {p0}, Lcom/chase/sig/android/view/detail/l;->p()Landroid/widget/EditText;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    .line 93
    :cond_2
    return-void

    .line 89
    :pswitch_0
    const-string v1, "1234567890-"

    iput-object v1, p0, Lcom/chase/sig/android/view/detail/l;->v:Ljava/lang/String;

    const/16 v1, 0xa

    iput v1, p0, Lcom/chase/sig/android/view/detail/l;->a:I

    goto :goto_0

    :pswitch_1
    const-string v1, "1234567890-"

    iput-object v1, p0, Lcom/chase/sig/android/view/detail/l;->v:Ljava/lang/String;

    const/16 v1, 0xc

    iput v1, p0, Lcom/chase/sig/android/view/detail/l;->a:I

    invoke-virtual {p0}, Lcom/chase/sig/android/view/detail/l;->p()Landroid/widget/EditText;

    move-result-object v1

    new-instance v2, Lcom/chase/sig/android/view/detail/n;

    invoke-direct {v2, p0}, Lcom/chase/sig/android/view/detail/n;-><init>(Lcom/chase/sig/android/view/detail/l;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    goto :goto_0

    :pswitch_2
    const-string v1, "1234567890-,."

    iput-object v1, p0, Lcom/chase/sig/android/view/detail/l;->v:Ljava/lang/String;

    goto :goto_0

    :pswitch_3
    const-string v1, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890 .,()-\'"

    iput-object v1, p0, Lcom/chase/sig/android/view/detail/l;->v:Ljava/lang/String;

    goto :goto_0

    :pswitch_4
    const-string v1, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890 -"

    iput-object v1, p0, Lcom/chase/sig/android/view/detail/l;->v:Ljava/lang/String;

    goto :goto_0

    .line 92
    :pswitch_5
    new-instance v0, Landroid/telephony/PhoneNumberFormattingTextWatcher;

    invoke-direct {v0}, Landroid/telephony/PhoneNumberFormattingTextWatcher;-><init>()V

    goto :goto_1

    :pswitch_6
    new-instance v0, Lcom/chase/sig/android/view/detail/l$a;

    invoke-direct {v0, p0, v3}, Lcom/chase/sig/android/view/detail/l$a;-><init>(Lcom/chase/sig/android/view/detail/l;B)V

    goto :goto_1

    .line 89
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 92
    :pswitch_data_1
    .packed-switch 0x4
        :pswitch_6
        :pswitch_5
    .end packed-switch
.end method

.method public final a(Lcom/chase/sig/android/view/detail/g$b;)V
    .locals 1
    .parameter

    .prologue
    .line 256
    invoke-virtual {p0}, Lcom/chase/sig/android/view/detail/l;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 257
    invoke-virtual {p0}, Lcom/chase/sig/android/view/detail/l;->p()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 259
    :cond_0
    return-void
.end method

.method final synthetic a(Ljava/lang/Object;)V
    .locals 1
    .parameter

    .prologue
    .line 22
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0}, Lcom/chase/sig/android/view/detail/l;->p()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final bridge synthetic b(Ljava/lang/Object;)V
    .locals 0
    .parameter

    .prologue
    .line 22
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/chase/sig/android/view/detail/l;->b(Ljava/lang/String;)V

    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1
    .parameter

    .prologue
    .line 76
    invoke-virtual {p0}, Lcom/chase/sig/android/view/detail/l;->p()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 77
    return-void
.end method

.method public final f()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 72
    invoke-virtual {p0}, Lcom/chase/sig/android/view/detail/l;->p()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    return-object v0
.end method

.method public final j()I
    .locals 1

    .prologue
    .line 67
    const v0, 0x7f030037

    return v0
.end method

.method public final n()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 233
    invoke-super {p0}, Lcom/chase/sig/android/view/detail/a;->n()Z

    move-result v1

    if-nez v1, :cond_1

    .line 234
    const/4 v0, 0x0

    .line 250
    :cond_0
    :goto_0
    return v0

    .line 237
    :cond_1
    invoke-virtual {p0}, Lcom/chase/sig/android/view/detail/l;->g()Ljava/lang/String;

    move-result-object v1

    .line 239
    iget-boolean v2, p0, Lcom/chase/sig/android/view/detail/a;->o:Z

    if-nez v2, :cond_2

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->l(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 243
    :cond_2
    iget v2, p0, Lcom/chase/sig/android/view/detail/l;->u:I

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 245
    :pswitch_0
    invoke-static {v1}, Lcom/chase/sig/android/util/s;->u(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    .line 247
    :pswitch_1
    invoke-static {v1}, Lcom/chase/sig/android/util/s;->v(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    .line 243
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final p()Landroid/widget/EditText;
    .locals 2

    .prologue
    .line 80
    iget-object v0, p0, Lcom/chase/sig/android/view/detail/l;->t:Landroid/view/View;

    iget v1, p0, Lcom/chase/sig/android/view/detail/l;->s:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    return-object v0
.end method

.method public final q()Lcom/chase/sig/android/view/detail/l;
    .locals 1

    .prologue
    .line 206
    const/4 v0, 0x2

    iput v0, p0, Lcom/chase/sig/android/view/detail/l;->u:I

    return-object p0
.end method
