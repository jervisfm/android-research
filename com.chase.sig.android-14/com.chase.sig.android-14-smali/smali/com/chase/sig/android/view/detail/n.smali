.class final Lcom/chase/sig/android/view/detail/n;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/view/detail/l;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/view/detail/l;)V
    .locals 0
    .parameter

    .prologue
    .line 168
    iput-object p1, p0, Lcom/chase/sig/android/view/detail/n;->a:Lcom/chase/sig/android/view/detail/l;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 5
    .parameter
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 172
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1, p2, p3}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;ILandroid/view/KeyEvent;)V

    check-cast p1, Landroid/widget/EditText;

    .line 173
    invoke-virtual {p1}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 175
    :goto_0
    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    .line 176
    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v3

    const/16 v4, 0xb

    if-le v3, v4, :cond_0

    const/4 v3, 0x7

    if-lt p2, v3, :cond_3

    const/16 v3, 0x10

    if-gt p2, v3, :cond_3

    move v3, v1

    :goto_1
    if-nez v3, :cond_1

    :cond_0
    if-eqz v0, :cond_4

    const/16 v0, 0x8

    if-ne p2, v0, :cond_4

    .line 182
    :cond_1
    :goto_2
    return v1

    :cond_2
    move v0, v2

    .line 173
    goto :goto_0

    :cond_3
    move v3, v2

    .line 176
    goto :goto_1

    :cond_4
    move v1, v2

    .line 182
    goto :goto_2
.end method
