.class public final Lcom/chase/sig/android/view/detail/o;
.super Lcom/chase/sig/android/view/detail/q;
.source "SourceFile"


# instance fields
.field private a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/chase/sig/android/domain/LabeledValue;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 18
    invoke-static {p2}, Lcom/chase/sig/android/domain/MMBFrequency;->a(Ljava/lang/String;)Lcom/chase/sig/android/domain/MMBFrequency;

    move-result-object v0

    invoke-static {v0}, Lcom/chase/sig/android/domain/MMBFrequency;->a(Lcom/chase/sig/android/domain/MMBFrequency;)I

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/chase/sig/android/view/detail/q;-><init>(Ljava/lang/String;I)V

    .line 19
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/chase/sig/android/domain/LabeledValue;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 31
    invoke-static {p2, p3}, Lcom/chase/sig/android/view/detail/o;->a(Ljava/lang/String;Ljava/util/ArrayList;)I

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/chase/sig/android/view/detail/q;-><init>(Ljava/lang/String;I)V

    .line 32
    iput-object p3, p0, Lcom/chase/sig/android/view/detail/o;->a:Ljava/util/ArrayList;

    .line 33
    return-void
.end method

.method private static a(Ljava/lang/String;Ljava/util/ArrayList;)I
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/chase/sig/android/domain/LabeledValue;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 64
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 65
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/LabeledValue;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 70
    :goto_1
    return v1

    .line 64
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 70
    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method

.method private r()[Ljava/lang/String;
    .locals 5

    .prologue
    .line 97
    iget-object v0, p0, Lcom/chase/sig/android/view/detail/o;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v3, v0, [Ljava/lang/String;

    .line 98
    iget-object v0, p0, Lcom/chase/sig/android/view/detail/o;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 100
    const/4 v0, 0x0

    move v1, v0

    .line 101
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 102
    add-int/lit8 v2, v1, 0x1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/LabeledValue;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/LabeledValue;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v1

    move v1, v2

    goto :goto_0

    .line 105
    :cond_0
    return-object v3
.end method


# virtual methods
.method protected final a(Landroid/content/Context;)V
    .locals 5
    .parameter

    .prologue
    const v4, 0x7f0300ad

    .line 37
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Lcom/chase/sig/android/view/detail/a;)V

    invoke-super {p0, p1}, Lcom/chase/sig/android/view/detail/q;->a(Landroid/content/Context;)V

    .line 38
    invoke-virtual {p0}, Lcom/chase/sig/android/view/detail/o;->q()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v1

    iget-object v0, p0, Lcom/chase/sig/android/view/detail/o;->a:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    new-instance v0, Landroid/widget/ArrayAdapter;

    iget-object v2, p0, Lcom/chase/sig/android/view/detail/o;->t:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {p0}, Lcom/chase/sig/android/view/detail/o;->r()[Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v4, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    :goto_0
    invoke-virtual {v1, v0}, Lcom/chase/sig/android/view/JPSpinner;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 39
    invoke-virtual {p0}, Lcom/chase/sig/android/view/detail/o;->q()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v1

    iget-object v0, p0, Lcom/chase/sig/android/view/detail/o;->d:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/chase/sig/android/view/JPSpinner;->setSelection(I)V

    .line 40
    return-void

    .line 38
    :cond_0
    new-instance v0, Landroid/widget/ArrayAdapter;

    iget-object v2, p0, Lcom/chase/sig/android/view/detail/o;->t:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {}, Lcom/chase/sig/android/domain/MMBFrequency;->a()[Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v4, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final p()Ljava/lang/String;
    .locals 2

    .prologue
    .line 51
    iget-object v0, p0, Lcom/chase/sig/android/view/detail/o;->a:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/chase/sig/android/view/detail/o;->q()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v1

    invoke-virtual {v1}, Lcom/chase/sig/android/view/JPSpinner;->getSelectedItemPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/domain/LabeledValue;

    invoke-virtual {v0}, Lcom/chase/sig/android/domain/LabeledValue;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
