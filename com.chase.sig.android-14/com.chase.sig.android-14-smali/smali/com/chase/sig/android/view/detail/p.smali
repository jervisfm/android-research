.class public final Lcom/chase/sig/android/view/detail/p;
.super Lcom/chase/sig/android/view/detail/a;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/view/detail/a",
        "<",
        "Lcom/chase/sig/android/view/detail/DetailRow;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private a:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .parameter

    .prologue
    .line 16
    const-string v0, ""

    invoke-direct {p0, p1, v0}, Lcom/chase/sig/android/view/detail/a;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    .line 13
    const/4 v0, -0x1

    iput v0, p0, Lcom/chase/sig/android/view/detail/p;->a:I

    .line 17
    const v0, 0x7f0200d7

    iput v0, p0, Lcom/chase/sig/android/view/detail/p;->a:I

    .line 18
    return-void
.end method

.method private p()Landroid/widget/ImageView;
    .locals 2

    .prologue
    .line 36
    iget-object v0, p0, Lcom/chase/sig/android/view/detail/p;->t:Landroid/view/View;

    const v1, 0x7f0900a0

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method private q()Landroid/widget/TextView;
    .locals 2

    .prologue
    .line 41
    iget-object v0, p0, Lcom/chase/sig/android/view/detail/p;->t:Landroid/view/View;

    iget v1, p0, Lcom/chase/sig/android/view/detail/p;->s:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method protected final a(Landroid/content/Context;)V
    .locals 3
    .parameter

    .prologue
    .line 46
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Lcom/chase/sig/android/view/detail/a;)V

    invoke-direct {p0}, Lcom/chase/sig/android/view/detail/p;->q()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p0}, Lcom/chase/sig/android/view/detail/p;->h()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 47
    return-void
.end method

.method final synthetic a(Ljava/lang/Object;)V
    .locals 2
    .parameter

    .prologue
    .line 11
    check-cast p1, Ljava/lang/String;

    invoke-direct {p0}, Lcom/chase/sig/android/view/detail/p;->p()Landroid/widget/ImageView;

    move-result-object v0

    iget v1, p0, Lcom/chase/sig/android/view/detail/p;->a:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-direct {p0}, Lcom/chase/sig/android/view/detail/p;->p()Landroid/widget/ImageView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-direct {p0}, Lcom/chase/sig/android/view/detail/p;->q()Landroid/widget/TextView;

    move-result-object v0

    invoke-static {p1}, Lcom/chase/sig/android/util/s;->s(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final j()I
    .locals 1

    .prologue
    .line 31
    const v0, 0x7f030026

    return v0
.end method
