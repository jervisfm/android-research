.class public Lcom/chase/sig/android/view/detail/q;
.super Lcom/chase/sig/android/view/detail/a;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/view/detail/a",
        "<",
        "Lcom/chase/sig/android/view/detail/q;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Lcom/chase/sig/android/view/x;


# direct methods
.method public constructor <init>(I)V
    .locals 2
    .parameter

    .prologue
    .line 18
    const v0, 0x7f070094

    invoke-static {v0}, Lcom/chase/sig/android/view/detail/q;->d(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/chase/sig/android/view/detail/a;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    .line 19
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .parameter

    .prologue
    .line 22
    const/4 v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/chase/sig/android/view/detail/a;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    .line 23
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 26
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/chase/sig/android/view/detail/a;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    .line 27
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;)V
    .locals 2
    .parameter

    .prologue
    .line 36
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Lcom/chase/sig/android/view/detail/a;)V

    invoke-virtual {p0}, Lcom/chase/sig/android/view/detail/q;->q()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v1

    iget-object v0, p0, Lcom/chase/sig/android/view/detail/q;->g:Ljava/lang/String;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/chase/sig/android/view/detail/a;->c:Ljava/lang/String;

    :goto_0
    invoke-virtual {v1, v0}, Lcom/chase/sig/android/view/JPSpinner;->setHint(Ljava/lang/String;)V

    .line 37
    iget-object v0, p0, Lcom/chase/sig/android/view/detail/q;->a:Lcom/chase/sig/android/view/x;

    if-eqz v0, :cond_0

    .line 38
    invoke-virtual {p0}, Lcom/chase/sig/android/view/detail/q;->q()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v0

    iget-object v1, p0, Lcom/chase/sig/android/view/detail/q;->a:Lcom/chase/sig/android/view/x;

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/JPSpinner;->a(Lcom/chase/sig/android/view/x;)V

    .line 40
    :cond_0
    return-void

    .line 36
    :cond_1
    iget-object v0, p0, Lcom/chase/sig/android/view/detail/q;->g:Ljava/lang/String;

    goto :goto_0
.end method

.method public final a(Lcom/chase/sig/android/view/x;)V
    .locals 1
    .parameter

    .prologue
    .line 67
    invoke-virtual {p0}, Lcom/chase/sig/android/view/detail/q;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 68
    invoke-virtual {p0}, Lcom/chase/sig/android/view/detail/q;->q()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/chase/sig/android/view/JPSpinner;->a(Lcom/chase/sig/android/view/x;)V

    .line 70
    :cond_0
    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;)V
    .locals 2
    .parameter

    .prologue
    .line 9
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0}, Lcom/chase/sig/android/view/detail/q;->q()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/JPSpinner;->setSelection(I)V

    return-void
.end method

.method public final b(Lcom/chase/sig/android/view/x;)Lcom/chase/sig/android/view/detail/q;
    .locals 0
    .parameter

    .prologue
    .line 47
    iput-object p1, p0, Lcom/chase/sig/android/view/detail/q;->a:Lcom/chase/sig/android/view/x;

    .line 48
    return-object p0
.end method

.method public final f()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 58
    invoke-virtual {p0}, Lcom/chase/sig/android/view/detail/q;->q()Lcom/chase/sig/android/view/JPSpinner;

    move-result-object v0

    invoke-virtual {v0}, Lcom/chase/sig/android/view/JPSpinner;->getSelectedItemPosition()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 59
    const/4 v0, 0x0

    .line 62
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lcom/chase/sig/android/view/detail/a;->f()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final j()I
    .locals 1

    .prologue
    .line 31
    const v0, 0x7f030027

    return v0
.end method

.method public final q()Lcom/chase/sig/android/view/JPSpinner;
    .locals 2

    .prologue
    .line 43
    iget-object v0, p0, Lcom/chase/sig/android/view/detail/q;->t:Landroid/view/View;

    iget v1, p0, Lcom/chase/sig/android/view/detail/q;->s:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/JPSpinner;

    return-object v0
.end method
