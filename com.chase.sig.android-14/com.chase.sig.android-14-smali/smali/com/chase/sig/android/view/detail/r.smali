.class public final Lcom/chase/sig/android/view/detail/r;
.super Lcom/chase/sig/android/view/detail/j;
.source "SourceFile"


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/chase/sig/android/view/f;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Lcom/chase/sig/android/view/detail/j;-><init>(Ljava/lang/String;Lcom/chase/sig/android/view/f;)V

    .line 15
    return-void
.end method


# virtual methods
.method protected final a(Landroid/content/Context;)V
    .locals 4
    .parameter

    .prologue
    .line 19
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Lcom/chase/sig/android/view/detail/a;)V

    invoke-super {p0, p1}, Lcom/chase/sig/android/view/detail/j;->a(Landroid/content/Context;)V

    .line 22
    invoke-virtual {p0}, Lcom/chase/sig/android/view/detail/r;->p()Landroid/widget/EditText;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    .line 25
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/text/InputFilter;

    .line 26
    const/4 v1, 0x0

    new-instance v2, Landroid/text/InputFilter$LengthFilter;

    const/4 v3, 0x3

    invoke-direct {v2, v3}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v2, v0, v1

    .line 27
    invoke-virtual {p0}, Lcom/chase/sig/android/view/detail/r;->p()Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 28
    return-void
.end method

.method public final n()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 36
    invoke-virtual {p0}, Lcom/chase/sig/android/view/detail/r;->r()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/chase/sig/android/util/s;->l(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 47
    :cond_0
    :goto_0
    return v0

    .line 40
    :cond_1
    invoke-virtual {p0}, Lcom/chase/sig/android/view/detail/r;->r()Ljava/lang/String;

    move-result-object v1

    const-string v2, "0"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 44
    invoke-virtual {p0}, Lcom/chase/sig/android/view/detail/r;->r()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    if-ltz v1, :cond_0

    .line 47
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final r()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/chase/sig/android/view/detail/r;->p()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
