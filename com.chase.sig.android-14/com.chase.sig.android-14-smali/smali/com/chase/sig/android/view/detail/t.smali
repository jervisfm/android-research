.class public final Lcom/chase/sig/android/view/detail/t;
.super Lcom/chase/sig/android/view/detail/a;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/view/detail/a",
        "<",
        "Lcom/chase/sig/android/view/detail/t;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private u:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 14
    const-string v0, ""

    invoke-direct {p0, p1, v0}, Lcom/chase/sig/android/view/detail/a;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    .line 15
    iput-object p2, p0, Lcom/chase/sig/android/view/detail/t;->a:Ljava/lang/String;

    .line 16
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;B)V
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Lcom/chase/sig/android/view/detail/t;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 20
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/chase/sig/android/view/detail/t;->u:Z

    .line 21
    return-void
.end method


# virtual methods
.method protected final a(Landroid/content/Context;)V
    .locals 2
    .parameter

    .prologue
    .line 39
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Lcom/chase/sig/android/view/detail/a;)V

    iget-object v0, p0, Lcom/chase/sig/android/view/detail/t;->t:Landroid/view/View;

    const v1, 0x7f0900a1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/chase/sig/android/view/detail/t;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 40
    iget-object v0, p0, Lcom/chase/sig/android/view/detail/t;->t:Landroid/view/View;

    const v1, 0x7f090015

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 41
    iget-boolean v1, p0, Lcom/chase/sig/android/view/detail/t;->u:Z

    if-nez v1, :cond_0

    .line 42
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 43
    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 45
    :cond_0
    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;)V
    .locals 2
    .parameter

    .prologue
    .line 9
    check-cast p1, Ljava/lang/String;

    iget-object v0, p0, Lcom/chase/sig/android/view/detail/t;->t:Landroid/view/View;

    iget v1, p0, Lcom/chase/sig/android/view/detail/t;->s:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final j()I
    .locals 1

    .prologue
    .line 25
    const v0, 0x7f030029

    return v0
.end method
