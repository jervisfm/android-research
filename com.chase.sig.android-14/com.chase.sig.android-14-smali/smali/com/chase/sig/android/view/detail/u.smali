.class public final Lcom/chase/sig/android/view/detail/u;
.super Lcom/chase/sig/android/view/detail/a;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chase/sig/android/view/detail/a",
        "<",
        "Lcom/chase/sig/android/view/detail/u;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    const/4 v0, 0x0

    .line 15
    invoke-direct {p0, p1, p2}, Lcom/chase/sig/android/view/detail/a;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    .line 12
    iput-boolean v0, p0, Lcom/chase/sig/android/view/detail/u;->a:Z

    .line 16
    iput-boolean v0, p0, Lcom/chase/sig/android/view/detail/u;->a:Z

    .line 17
    return-void
.end method

.method private p()Lcom/chase/sig/android/view/TickerValueTextView;
    .locals 2

    .prologue
    .line 33
    iget-object v0, p0, Lcom/chase/sig/android/view/detail/u;->t:Landroid/view/View;

    iget v1, p0, Lcom/chase/sig/android/view/detail/u;->s:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/TickerValueTextView;

    return-object v0
.end method


# virtual methods
.method protected final a(Landroid/content/Context;)V
    .locals 1
    .parameter

    .prologue
    .line 30
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Lcom/chase/sig/android/view/detail/a;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;)V
    .locals 2
    .parameter

    .prologue
    .line 10
    check-cast p1, Ljava/lang/String;

    invoke-direct {p0}, Lcom/chase/sig/android/view/detail/u;->p()Lcom/chase/sig/android/view/TickerValueTextView;

    move-result-object v0

    iget-boolean v1, p0, Lcom/chase/sig/android/view/detail/u;->a:Z

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/TickerValueTextView;->setDisplayPlaceholder(Z)V

    invoke-direct {p0}, Lcom/chase/sig/android/view/detail/u;->p()Lcom/chase/sig/android/view/TickerValueTextView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/TickerValueTextView;->setShowNotApplicable(Z)V

    invoke-direct {p0}, Lcom/chase/sig/android/view/detail/u;->p()Lcom/chase/sig/android/view/TickerValueTextView;

    move-result-object v0

    new-instance v1, Lcom/chase/sig/android/util/Dollar;

    invoke-direct {v1, p1}, Lcom/chase/sig/android/util/Dollar;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/chase/sig/android/view/TickerValueTextView;->setTickerValue(Lcom/chase/sig/android/util/Dollar;)V

    return-void
.end method

.method public final j()I
    .locals 1

    .prologue
    .line 21
    const v0, 0x7f03002a

    return v0
.end method
