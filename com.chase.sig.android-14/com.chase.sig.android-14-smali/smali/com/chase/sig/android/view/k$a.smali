.class public final Lcom/chase/sig/android/view/k$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/chase/sig/android/view/k;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field final a:Landroid/content/Context;

.field public b:Ljava/lang/CharSequence;

.field public c:Ljava/lang/CharSequence;

.field public d:Landroid/graphics/drawable/Drawable;

.field public e:I

.field public f:Landroid/content/DialogInterface$OnClickListener;

.field public g:Landroid/content/DialogInterface$OnCancelListener;

.field h:Landroid/widget/ListView;

.field public i:Z

.field public j:Ljava/lang/String;

.field public k:Landroid/view/View;

.field l:Landroid/content/DialogInterface$OnClickListener;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Landroid/view/View;

.field private p:I

.field private final q:I

.field private final r:I

.field private final s:I

.field private t:Landroid/content/DialogInterface$OnClickListener;

.field private u:Landroid/content/DialogInterface$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .parameter

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    const/4 v0, 0x0

    iput v0, p0, Lcom/chase/sig/android/view/k$a;->p:I

    .line 52
    iput v1, p0, Lcom/chase/sig/android/view/k$a;->e:I

    .line 54
    iput v1, p0, Lcom/chase/sig/android/view/k$a;->q:I

    .line 56
    iput v2, p0, Lcom/chase/sig/android/view/k$a;->r:I

    .line 58
    iput v2, p0, Lcom/chase/sig/android/view/k$a;->s:I

    .line 67
    iput-boolean v1, p0, Lcom/chase/sig/android/view/k$a;->i:Z

    .line 74
    iput-object p1, p0, Lcom/chase/sig/android/view/k$a;->a:Landroid/content/Context;

    .line 75
    return-void
.end method

.method private a(ILjava/lang/String;Landroid/content/DialogInterface$OnClickListener;Landroid/app/Dialog;Landroid/view/View;IZ)V
    .locals 2
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 300
    invoke-virtual {p5, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 301
    invoke-virtual {v0, p2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 302
    iget v0, p0, Lcom/chase/sig/android/view/k$a;->p:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/chase/sig/android/view/k$a;->p:I

    .line 303
    if-eqz p7, :cond_0

    .line 304
    invoke-virtual {p5, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lcom/chase/sig/android/view/m;

    invoke-direct {v1, p0, p3, p4, p6}, Lcom/chase/sig/android/view/m;-><init>(Lcom/chase/sig/android/view/k$a;Landroid/content/DialogInterface$OnClickListener;Landroid/app/Dialog;I)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 327
    :goto_0
    return-void

    .line 316
    :cond_0
    invoke-virtual {p5, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lcom/chase/sig/android/view/n;

    invoke-direct {v1, p0, p3, p4, p6}, Lcom/chase/sig/android/view/n;-><init>(Lcom/chase/sig/android/view/k$a;Landroid/content/DialogInterface$OnClickListener;Landroid/app/Dialog;I)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(I)Lcom/chase/sig/android/view/k$a;
    .locals 1
    .parameter

    .prologue
    .line 87
    iget-object v0, p0, Lcom/chase/sig/android/view/k$a;->a:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/view/k$a;->b(Ljava/lang/String;)Lcom/chase/sig/android/view/k$a;

    move-result-object v0

    return-object v0
.end method

.method public final a(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 158
    iget-object v0, p0, Lcom/chase/sig/android/view/k$a;->a:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/chase/sig/android/view/k$a;->m:Ljava/lang/String;

    .line 159
    iput-object p2, p0, Lcom/chase/sig/android/view/k$a;->t:Landroid/content/DialogInterface$OnClickListener;

    .line 160
    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/chase/sig/android/view/k$a;
    .locals 1
    .parameter

    .prologue
    .line 82
    :try_start_0
    iput-object p1, p0, Lcom/chase/sig/android/view/k$a;->c:Ljava/lang/CharSequence;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 83
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p0}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Lcom/chase/sig/android/view/k$a;)V

    return-object p0

    :catch_0
    move-exception v0

    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p0}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Lcom/chase/sig/android/view/k$a;)V

    throw v0
.end method

.method public final a(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 165
    iput-object p1, p0, Lcom/chase/sig/android/view/k$a;->m:Ljava/lang/String;

    .line 166
    iput-object p2, p0, Lcom/chase/sig/android/view/k$a;->t:Landroid/content/DialogInterface$OnClickListener;

    .line 167
    return-object p0
.end method

.method public final b(I)Lcom/chase/sig/android/view/k$a;
    .locals 1
    .parameter

    .prologue
    .line 96
    :try_start_0
    iget-object v0, p0, Lcom/chase/sig/android/view/k$a;->a:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/view/k$a;->c:Ljava/lang/CharSequence;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 97
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p0}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Lcom/chase/sig/android/view/k$a;)V

    return-object p0

    :catch_0
    move-exception v0

    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p0}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Lcom/chase/sig/android/view/k$a;)V

    throw v0
.end method

.method public final b(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 172
    iget-object v0, p0, Lcom/chase/sig/android/view/k$a;->a:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/chase/sig/android/view/k$a;->n:Ljava/lang/String;

    .line 173
    iput-object p2, p0, Lcom/chase/sig/android/view/k$a;->u:Landroid/content/DialogInterface$OnClickListener;

    .line 174
    return-object p0
.end method

.method public final b(Ljava/lang/String;)Lcom/chase/sig/android/view/k$a;
    .locals 1
    .parameter

    .prologue
    .line 91
    invoke-static {p1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lcom/chase/sig/android/view/k$a;->c:Ljava/lang/CharSequence;

    .line 92
    return-object p0
.end method

.method public final b(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 179
    iput-object p1, p0, Lcom/chase/sig/android/view/k$a;->n:Ljava/lang/String;

    .line 180
    iput-object p2, p0, Lcom/chase/sig/android/view/k$a;->u:Landroid/content/DialogInterface$OnClickListener;

    .line 181
    return-object p0
.end method

.method public final c(I)Lcom/chase/sig/android/view/k$a;
    .locals 1
    .parameter

    .prologue
    .line 101
    iget-object v0, p0, Lcom/chase/sig/android/view/k$a;->a:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/chase/sig/android/view/k$a;->b:Ljava/lang/CharSequence;

    .line 102
    return-object p0
.end method

.method public final c(ILandroid/content/DialogInterface$OnClickListener;)Lcom/chase/sig/android/view/k$a;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 186
    iget-object v0, p0, Lcom/chase/sig/android/view/k$a;->a:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/chase/sig/android/view/k$a;->j:Ljava/lang/String;

    .line 187
    iput-object p2, p0, Lcom/chase/sig/android/view/k$a;->f:Landroid/content/DialogInterface$OnClickListener;

    .line 188
    return-object p0
.end method

.method public final d(I)Lcom/chase/sig/android/view/k;
    .locals 8
    .parameter

    .prologue
    .line 208
    iget-object v0, p0, Lcom/chase/sig/android/view/k$a;->a:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 211
    new-instance v4, Lcom/chase/sig/android/view/k;

    iget-object v1, p0, Lcom/chase/sig/android/view/k$a;->a:Landroid/content/Context;

    invoke-direct {v4, v1}, Lcom/chase/sig/android/view/k;-><init>(Landroid/content/Context;)V

    .line 212
    const v1, 0x7f03004e

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 214
    const v1, 0x7f090106

    invoke-virtual {v5, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 215
    iget v2, p0, Lcom/chase/sig/android/view/k$a;->e:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_5

    .line 218
    const v2, 0x7f03004f

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 222
    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 224
    iget-object v0, p0, Lcom/chase/sig/android/view/k$a;->m:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 225
    const v1, 0x7f090108

    iget-object v2, p0, Lcom/chase/sig/android/view/k$a;->m:Ljava/lang/String;

    iget-object v3, p0, Lcom/chase/sig/android/view/k$a;->t:Landroid/content/DialogInterface$OnClickListener;

    const/4 v6, -0x1

    const/4 v7, 0x1

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/chase/sig/android/view/k$a;->a(ILjava/lang/String;Landroid/content/DialogInterface$OnClickListener;Landroid/app/Dialog;Landroid/view/View;IZ)V

    .line 231
    :goto_1
    iget-object v0, p0, Lcom/chase/sig/android/view/k$a;->n:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 232
    const v1, 0x7f09010a

    iget-object v2, p0, Lcom/chase/sig/android/view/k$a;->n:Ljava/lang/String;

    iget-object v3, p0, Lcom/chase/sig/android/view/k$a;->u:Landroid/content/DialogInterface$OnClickListener;

    const/4 v6, -0x2

    const/4 v7, 0x1

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/chase/sig/android/view/k$a;->a(ILjava/lang/String;Landroid/content/DialogInterface$OnClickListener;Landroid/app/Dialog;Landroid/view/View;IZ)V

    .line 238
    :goto_2
    iget-object v0, p0, Lcom/chase/sig/android/view/k$a;->j:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 239
    const v1, 0x7f090109

    iget-object v2, p0, Lcom/chase/sig/android/view/k$a;->j:Ljava/lang/String;

    iget-object v3, p0, Lcom/chase/sig/android/view/k$a;->f:Landroid/content/DialogInterface$OnClickListener;

    const/4 v6, -0x3

    const/4 v7, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/chase/sig/android/view/k$a;->a(ILjava/lang/String;Landroid/content/DialogInterface$OnClickListener;Landroid/app/Dialog;Landroid/view/View;IZ)V

    .line 245
    :goto_3
    iget-object v0, p0, Lcom/chase/sig/android/view/k$a;->c:Ljava/lang/CharSequence;

    if-eqz v0, :cond_9

    const v0, 0x7f090103

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/chase/sig/android/view/k$a;->c:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 247
    :cond_0
    :goto_4
    iget-object v0, p0, Lcom/chase/sig/android/view/k$a;->b:Ljava/lang/CharSequence;

    if-eqz v0, :cond_1

    .line 248
    const v0, 0x7f0900fd

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 249
    const v0, 0x7f090039

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/chase/sig/android/view/k$a;->b:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 251
    :cond_1
    iget-object v0, p0, Lcom/chase/sig/android/view/k$a;->d:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_a

    .line 252
    const v0, 0x7f0900ff

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/chase/sig/android/view/k$a;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 257
    :goto_5
    iget-boolean v0, p0, Lcom/chase/sig/android/view/k$a;->i:Z

    invoke-virtual {v4, v0}, Lcom/chase/sig/android/view/k;->setCancelable(Z)V

    .line 258
    iget-boolean v0, p0, Lcom/chase/sig/android/view/k$a;->i:Z

    if-eqz v0, :cond_2

    .line 259
    iget-object v0, p0, Lcom/chase/sig/android/view/k$a;->g:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v4, v0}, Lcom/chase/sig/android/view/k;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 262
    :cond_2
    iget-object v0, p0, Lcom/chase/sig/android/view/k$a;->l:Landroid/content/DialogInterface$OnClickListener;

    if-eqz v0, :cond_3

    .line 263
    iget-object v0, p0, Lcom/chase/sig/android/view/k$a;->h:Landroid/widget/ListView;

    new-instance v1, Lcom/chase/sig/android/view/l;

    invoke-direct {v1, p0, v4}, Lcom/chase/sig/android/view/l;-><init>(Lcom/chase/sig/android/view/k$a;Lcom/chase/sig/android/view/k;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 271
    iget-object v0, p0, Lcom/chase/sig/android/view/k$a;->h:Landroid/widget/ListView;

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->setSelection(I)V

    .line 272
    iget-object v0, p0, Lcom/chase/sig/android/view/k$a;->h:Landroid/widget/ListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 273
    iget-object v0, p0, Lcom/chase/sig/android/view/k$a;->h:Landroid/widget/ListView;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 274
    iget-object v0, p0, Lcom/chase/sig/android/view/k$a;->h:Landroid/widget/ListView;

    iput-object v0, p0, Lcom/chase/sig/android/view/k$a;->k:Landroid/view/View;

    .line 277
    :cond_3
    iget-object v0, p0, Lcom/chase/sig/android/view/k$a;->k:Landroid/view/View;

    if-eqz v0, :cond_4

    const v0, 0x7f090105

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/chase/sig/android/view/k$a;->k:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    const v0, 0x7f090105

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f090104

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f090101

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 278
    :cond_4
    iget v0, p0, Lcom/chase/sig/android/view/k$a;->p:I

    packed-switch v0, :pswitch_data_0

    .line 279
    :goto_6
    invoke-virtual {v4, v5}, Lcom/chase/sig/android/view/k;->setContentView(Landroid/view/View;)V

    .line 281
    return-object v4

    .line 220
    :cond_5
    const v2, 0x7f030050

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_0

    .line 228
    :cond_6
    const v0, 0x7f090108

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 235
    :cond_7
    const v0, 0x7f09010a

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_2

    .line 242
    :cond_8
    const v0, 0x7f090109

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_3

    .line 245
    :cond_9
    iget-object v0, p0, Lcom/chase/sig/android/view/k$a;->o:Landroid/view/View;

    if-eqz v0, :cond_0

    const v0, 0x7f090103

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    const v0, 0x7f090103

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/chase/sig/android/view/k$a;->o:Landroid/view/View;

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    const/4 v3, -0x2

    const/4 v6, -0x2

    invoke-direct {v2, v3, v6}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_4

    .line 254
    :cond_a
    const v0, 0x7f0900ff

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_5

    .line 278
    :pswitch_0
    const v0, 0x7f090106

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_6

    :pswitch_1
    const v0, 0x7f090107

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f09010b

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_6

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
