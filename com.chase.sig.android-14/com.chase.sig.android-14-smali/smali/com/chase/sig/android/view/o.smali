.class final Lcom/chase/sig/android/view/o;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/chase/sig/android/view/JPSortableButtonBar;


# direct methods
.method constructor <init>(Lcom/chase/sig/android/view/JPSortableButtonBar;)V
    .locals 0
    .parameter

    .prologue
    .line 40
    iput-object p1, p0, Lcom/chase/sig/android/view/o;->a:Lcom/chase/sig/android/view/JPSortableButtonBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 4
    .parameter

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 45
    invoke-static {}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a()Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;

    invoke-static {p1}, Lcom/chase/sig/analytics/BehaviorAnalyticsAspect;->a(Landroid/view/View;)V

    iget-object v0, p0, Lcom/chase/sig/android/view/o;->a:Lcom/chase/sig/android/view/JPSortableButtonBar;

    invoke-static {v0}, Lcom/chase/sig/android/view/JPSortableButtonBar;->a(Lcom/chase/sig/android/view/JPSortableButtonBar;)Landroid/widget/Button;

    move-result-object v0

    if-ne v0, p1, :cond_3

    .line 46
    iget-object v3, p0, Lcom/chase/sig/android/view/o;->a:Lcom/chase/sig/android/view/JPSortableButtonBar;

    iget-object v0, p0, Lcom/chase/sig/android/view/o;->a:Lcom/chase/sig/android/view/JPSortableButtonBar;

    invoke-static {v0}, Lcom/chase/sig/android/view/JPSortableButtonBar;->b(Lcom/chase/sig/android/view/JPSortableButtonBar;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v3, v0}, Lcom/chase/sig/android/view/JPSortableButtonBar;->a(Lcom/chase/sig/android/view/JPSortableButtonBar;Z)Z

    .line 51
    :goto_1
    iget-object v0, p0, Lcom/chase/sig/android/view/o;->a:Lcom/chase/sig/android/view/JPSortableButtonBar;

    invoke-static {v0}, Lcom/chase/sig/android/view/JPSortableButtonBar;->a(Lcom/chase/sig/android/view/JPSortableButtonBar;)Landroid/widget/Button;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lcom/chase/sig/android/view/o;->a:Lcom/chase/sig/android/view/JPSortableButtonBar;

    iget-object v3, p0, Lcom/chase/sig/android/view/o;->a:Lcom/chase/sig/android/view/JPSortableButtonBar;

    invoke-static {v3}, Lcom/chase/sig/android/view/JPSortableButtonBar;->a(Lcom/chase/sig/android/view/JPSortableButtonBar;)Landroid/widget/Button;

    move-result-object v3

    invoke-static {v0, v3, v2}, Lcom/chase/sig/android/view/JPSortableButtonBar;->a(Lcom/chase/sig/android/view/JPSortableButtonBar;Landroid/widget/Button;Z)V

    .line 55
    :cond_0
    iget-object v0, p0, Lcom/chase/sig/android/view/o;->a:Lcom/chase/sig/android/view/JPSortableButtonBar;

    check-cast p1, Landroid/widget/Button;

    invoke-static {v0, p1}, Lcom/chase/sig/android/view/JPSortableButtonBar;->a(Lcom/chase/sig/android/view/JPSortableButtonBar;Landroid/widget/Button;)Landroid/widget/Button;

    .line 56
    iget-object v0, p0, Lcom/chase/sig/android/view/o;->a:Lcom/chase/sig/android/view/JPSortableButtonBar;

    iget-object v2, p0, Lcom/chase/sig/android/view/o;->a:Lcom/chase/sig/android/view/JPSortableButtonBar;

    invoke-static {v2}, Lcom/chase/sig/android/view/JPSortableButtonBar;->a(Lcom/chase/sig/android/view/JPSortableButtonBar;)Landroid/widget/Button;

    move-result-object v2

    invoke-static {v0, v2, v1}, Lcom/chase/sig/android/view/JPSortableButtonBar;->a(Lcom/chase/sig/android/view/JPSortableButtonBar;Landroid/widget/Button;Z)V

    .line 58
    iget-object v0, p0, Lcom/chase/sig/android/view/o;->a:Lcom/chase/sig/android/view/JPSortableButtonBar;

    invoke-static {v0}, Lcom/chase/sig/android/view/JPSortableButtonBar;->c(Lcom/chase/sig/android/view/JPSortableButtonBar;)Lcom/chase/sig/android/view/JPSortableButtonBar$a;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 59
    iget-object v0, p0, Lcom/chase/sig/android/view/o;->a:Lcom/chase/sig/android/view/JPSortableButtonBar;

    invoke-static {v0}, Lcom/chase/sig/android/view/JPSortableButtonBar;->c(Lcom/chase/sig/android/view/JPSortableButtonBar;)Lcom/chase/sig/android/view/JPSortableButtonBar$a;

    move-result-object v1

    iget-object v0, p0, Lcom/chase/sig/android/view/o;->a:Lcom/chase/sig/android/view/JPSortableButtonBar;

    invoke-static {v0}, Lcom/chase/sig/android/view/JPSortableButtonBar;->a(Lcom/chase/sig/android/view/JPSortableButtonBar;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Button;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v2, p0, Lcom/chase/sig/android/view/o;->a:Lcom/chase/sig/android/view/JPSortableButtonBar;

    invoke-static {v2}, Lcom/chase/sig/android/view/JPSortableButtonBar;->b(Lcom/chase/sig/android/view/JPSortableButtonBar;)Z

    move-result v2

    invoke-interface {v1, v0, v2}, Lcom/chase/sig/android/view/JPSortableButtonBar$a;->a(IZ)V

    .line 62
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 46
    goto :goto_0

    .line 48
    :cond_3
    iget-object v0, p0, Lcom/chase/sig/android/view/o;->a:Lcom/chase/sig/android/view/JPSortableButtonBar;

    invoke-static {v0, v2}, Lcom/chase/sig/android/view/JPSortableButtonBar;->a(Lcom/chase/sig/android/view/JPSortableButtonBar;Z)Z

    goto :goto_1
.end method
