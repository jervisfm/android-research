.class public abstract Lcom/chase/sig/android/view/w;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/chase/sig/android/view/w$a;
    }
.end annotation


# static fields
.field private static b:I


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/chase/sig/android/view/w$a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const/4 v0, 0x0

    sput v0, Lcom/chase/sig/android/view/w;->b:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 16
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/chase/sig/android/view/w;->a:Ljava/util/List;

    .line 117
    return-void
.end method


# virtual methods
.method protected abstract a(Ljava/lang/String;)Landroid/view/View;
.end method

.method public final a(Ljava/lang/String;Landroid/widget/Adapter;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 20
    iget-object v0, p0, Lcom/chase/sig/android/view/w;->a:Ljava/util/List;

    new-instance v1, Lcom/chase/sig/android/view/w$a;

    invoke-direct {v1, p0, p1, p2}, Lcom/chase/sig/android/view/w$a;-><init>(Lcom/chase/sig/android/view/w;Ljava/lang/String;Landroid/widget/Adapter;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 21
    return-void
.end method

.method public getCount()I
    .locals 3

    .prologue
    .line 42
    const/4 v0, 0x0

    .line 44
    iget-object v1, p0, Lcom/chase/sig/android/view/w;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/w$a;

    .line 45
    iget-object v0, v0, Lcom/chase/sig/android/view/w$a;->b:Landroid/widget/Adapter;

    invoke-interface {v0}, Landroid/widget/Adapter;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_0

    .line 48
    :cond_0
    return v1
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 3
    .parameter

    .prologue
    .line 24
    iget-object v0, p0, Lcom/chase/sig/android/view/w;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/w$a;

    .line 25
    if-nez p1, :cond_0

    .line 38
    :goto_1
    return-object v0

    .line 29
    :cond_0
    iget-object v2, v0, Lcom/chase/sig/android/view/w$a;->b:Landroid/widget/Adapter;

    invoke-interface {v2}, Landroid/widget/Adapter;->getCount()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    .line 31
    if-ge p1, v2, :cond_1

    .line 32
    iget-object v0, v0, Lcom/chase/sig/android/view/w$a;->b:Landroid/widget/Adapter;

    add-int/lit8 v1, p1, -0x1

    invoke-interface {v0, v1}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_1

    .line 35
    :cond_1
    sub-int/2addr p1, v2

    .line 36
    goto :goto_0

    .line 38
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public getItemId(I)J
    .locals 2
    .parameter

    .prologue
    .line 114
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 4
    .parameter

    .prologue
    .line 62
    sget v0, Lcom/chase/sig/android/view/w;->b:I

    add-int/lit8 v0, v0, 0x1

    .line 64
    iget-object v1, p0, Lcom/chase/sig/android/view/w;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/w$a;

    .line 65
    if-nez p1, :cond_0

    .line 66
    sget v0, Lcom/chase/sig/android/view/w;->b:I

    .line 79
    :goto_1
    return v0

    .line 69
    :cond_0
    iget-object v3, v0, Lcom/chase/sig/android/view/w$a;->b:Landroid/widget/Adapter;

    invoke-interface {v3}, Landroid/widget/Adapter;->getCount()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    .line 71
    if-ge p1, v3, :cond_1

    .line 72
    iget-object v0, v0, Lcom/chase/sig/android/view/w$a;->b:Landroid/widget/Adapter;

    add-int/lit8 v2, p1, -0x1

    invoke-interface {v0, v2}, Landroid/widget/Adapter;->getItemViewType(I)I

    move-result v0

    add-int/2addr v0, v1

    goto :goto_1

    .line 75
    :cond_1
    sub-int/2addr p1, v3

    .line 76
    iget-object v0, v0, Lcom/chase/sig/android/view/w$a;->b:Landroid/widget/Adapter;

    invoke-interface {v0}, Landroid/widget/Adapter;->getViewTypeCount()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    .line 77
    goto :goto_0

    .line 79
    :cond_2
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .parameter
    .parameter
    .parameter

    .prologue
    .line 92
    iget-object v0, p0, Lcom/chase/sig/android/view/w;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/w$a;

    .line 95
    if-nez p1, :cond_0

    .line 96
    iget-object v0, v0, Lcom/chase/sig/android/view/w$a;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/chase/sig/android/view/w;->a(Ljava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 109
    :goto_1
    return-object v0

    .line 99
    :cond_0
    iget-object v2, v0, Lcom/chase/sig/android/view/w$a;->b:Landroid/widget/Adapter;

    invoke-interface {v2}, Landroid/widget/Adapter;->getCount()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    .line 101
    if-ge p1, v2, :cond_1

    .line 102
    iget-object v0, v0, Lcom/chase/sig/android/view/w$a;->b:Landroid/widget/Adapter;

    add-int/lit8 v1, p1, -0x1

    invoke-interface {v0, v1, p2, p3}, Landroid/widget/Adapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_1

    .line 105
    :cond_1
    sub-int/2addr p1, v2

    .line 106
    goto :goto_0

    .line 109
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public getViewTypeCount()I
    .locals 3

    .prologue
    .line 52
    const/4 v0, 0x1

    .line 54
    iget-object v1, p0, Lcom/chase/sig/android/view/w;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/chase/sig/android/view/w$a;

    .line 55
    iget-object v0, v0, Lcom/chase/sig/android/view/w$a;->b:Landroid/widget/Adapter;

    invoke-interface {v0}, Landroid/widget/Adapter;->getViewTypeCount()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_0

    .line 58
    :cond_0
    return v1
.end method

.method public isEnabled(I)Z
    .locals 2
    .parameter

    .prologue
    .line 87
    invoke-virtual {p0, p1}, Lcom/chase/sig/android/view/w;->getItemViewType(I)I

    move-result v0

    sget v1, Lcom/chase/sig/android/view/w;->b:I

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
