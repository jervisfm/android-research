.class final Lcom/google/common/a/b;
.super Lcom/google/common/a/a;
.source "SourceFile"


# instance fields
.field final synthetic b:Lcom/google/common/a/a;


# direct methods
.method constructor <init>(Lcom/google/common/a/a;Lcom/google/common/a/a;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 208
    iput-object p1, p0, Lcom/google/common/a/b;->b:Lcom/google/common/a/a;

    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, Lcom/google/common/a/a;-><init>(Lcom/google/common/a/a;B)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Appendable;Ljava/lang/Iterable;)Ljava/lang/Appendable;
    .locals 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A::",
            "Ljava/lang/Appendable;",
            ">(TA;",
            "Ljava/lang/Iterable",
            "<*>;)TA;"
        }
    .end annotation

    .prologue
    .line 211
    const-string v0, "appendable"

    invoke-static {p1, v0}, Lcom/google/common/a/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 212
    const-string v0, "parts"

    invoke-static {p2, v0}, Lcom/google/common/a/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 213
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 214
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 215
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 216
    if-eqz v1, :cond_0

    .line 217
    iget-object v2, p0, Lcom/google/common/a/b;->b:Lcom/google/common/a/a;

    invoke-static {v1}, Lcom/google/common/a/a;->a(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/lang/Appendable;->append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;

    .line 221
    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 222
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 223
    if-eqz v1, :cond_1

    .line 224
    iget-object v2, p0, Lcom/google/common/a/b;->b:Lcom/google/common/a/a;

    iget-object v2, v2, Lcom/google/common/a/a;->a:Ljava/lang/String;

    invoke-interface {p1, v2}, Ljava/lang/Appendable;->append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;

    .line 225
    iget-object v2, p0, Lcom/google/common/a/b;->b:Lcom/google/common/a/a;

    invoke-static {v1}, Lcom/google/common/a/a;->a(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/lang/Appendable;->append(Ljava/lang/CharSequence;)Ljava/lang/Appendable;

    goto :goto_0

    .line 228
    :cond_2
    return-object p1
.end method
