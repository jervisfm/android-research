.class public abstract Lcom/google/common/b/a;
.super Lcom/google/common/b/h;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/common/b/a$1;,
        Lcom/google/common/b/a$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/common/b/h",
        "<TT;>;"
    }
.end annotation


# instance fields
.field a:Lcom/google/common/b/a$a;

.field private b:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/google/common/b/h;-><init>()V

    .line 63
    sget-object v0, Lcom/google/common/b/a$a;->b:Lcom/google/common/b/a$a;

    iput-object v0, p0, Lcom/google/common/b/a;->a:Lcom/google/common/b/a$a;

    .line 66
    return-void
.end method


# virtual methods
.method protected abstract a()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method public final hasNext()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 128
    iget-object v2, p0, Lcom/google/common/b/a;->a:Lcom/google/common/b/a$a;

    sget-object v3, Lcom/google/common/b/a$a;->d:Lcom/google/common/b/a$a;

    if-eq v2, v3, :cond_0

    move v2, v1

    :goto_0
    if-nez v2, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    move v2, v0

    goto :goto_0

    .line 129
    :cond_1
    sget-object v2, Lcom/google/common/b/a$1;->a:[I

    iget-object v3, p0, Lcom/google/common/b/a;->a:Lcom/google/common/b/a$a;

    invoke-virtual {v3}, Lcom/google/common/b/a$a;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 136
    sget-object v2, Lcom/google/common/b/a$a;->d:Lcom/google/common/b/a$a;

    iput-object v2, p0, Lcom/google/common/b/a;->a:Lcom/google/common/b/a$a;

    invoke-virtual {p0}, Lcom/google/common/b/a;->a()Ljava/lang/Object;

    move-result-object v2

    iput-object v2, p0, Lcom/google/common/b/a;->b:Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/common/b/a;->a:Lcom/google/common/b/a$a;

    sget-object v3, Lcom/google/common/b/a$a;->c:Lcom/google/common/b/a$a;

    if-eq v2, v3, :cond_2

    sget-object v0, Lcom/google/common/b/a$a;->a:Lcom/google/common/b/a$a;

    iput-object v0, p0, Lcom/google/common/b/a;->a:Lcom/google/common/b/a$a;

    move v0, v1

    :cond_2
    :goto_1
    :pswitch_0
    return v0

    :pswitch_1
    move v0, v1

    .line 133
    goto :goto_1

    .line 129
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final next()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 151
    invoke-virtual {p0}, Lcom/google/common/b/a;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 152
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 154
    :cond_0
    sget-object v0, Lcom/google/common/b/a$a;->b:Lcom/google/common/b/a$a;

    iput-object v0, p0, Lcom/google/common/b/a;->a:Lcom/google/common/b/a$a;

    .line 155
    iget-object v0, p0, Lcom/google/common/b/a;->b:Ljava/lang/Object;

    return-object v0
.end method
