.class public final enum Lcom/google/gson/chase/FieldNamingPolicy;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/gson/chase/FieldNamingPolicy;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/gson/chase/FieldNamingPolicy;

.field public static final enum b:Lcom/google/gson/chase/FieldNamingPolicy;

.field public static final enum c:Lcom/google/gson/chase/FieldNamingPolicy;

.field public static final enum d:Lcom/google/gson/chase/FieldNamingPolicy;

.field private static final synthetic f:[Lcom/google/gson/chase/FieldNamingPolicy;


# instance fields
.field private final e:Lcom/google/gson/chase/FieldNamingStrategy2;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 39
    new-instance v0, Lcom/google/gson/chase/FieldNamingPolicy;

    const-string v1, "UPPER_CAMEL_CASE"

    new-instance v2, Lcom/google/gson/chase/ModifyFirstLetterNamingPolicy;

    sget-object v3, Lcom/google/gson/chase/ModifyFirstLetterNamingPolicy$LetterModifier;->a:Lcom/google/gson/chase/ModifyFirstLetterNamingPolicy$LetterModifier;

    invoke-direct {v2, v3}, Lcom/google/gson/chase/ModifyFirstLetterNamingPolicy;-><init>(Lcom/google/gson/chase/ModifyFirstLetterNamingPolicy$LetterModifier;)V

    invoke-direct {v0, v1, v4, v2}, Lcom/google/gson/chase/FieldNamingPolicy;-><init>(Ljava/lang/String;ILcom/google/gson/chase/FieldNamingStrategy2;)V

    sput-object v0, Lcom/google/gson/chase/FieldNamingPolicy;->a:Lcom/google/gson/chase/FieldNamingPolicy;

    .line 55
    new-instance v0, Lcom/google/gson/chase/FieldNamingPolicy;

    const-string v1, "UPPER_CAMEL_CASE_WITH_SPACES"

    new-instance v2, Lcom/google/gson/chase/UpperCamelCaseSeparatorNamingPolicy;

    const-string v3, " "

    invoke-direct {v2, v3}, Lcom/google/gson/chase/UpperCamelCaseSeparatorNamingPolicy;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1, v5, v2}, Lcom/google/gson/chase/FieldNamingPolicy;-><init>(Ljava/lang/String;ILcom/google/gson/chase/FieldNamingStrategy2;)V

    sput-object v0, Lcom/google/gson/chase/FieldNamingPolicy;->b:Lcom/google/gson/chase/FieldNamingPolicy;

    .line 69
    new-instance v0, Lcom/google/gson/chase/FieldNamingPolicy;

    const-string v1, "LOWER_CASE_WITH_UNDERSCORES"

    new-instance v2, Lcom/google/gson/chase/LowerCamelCaseSeparatorNamingPolicy;

    const-string v3, "_"

    invoke-direct {v2, v3}, Lcom/google/gson/chase/LowerCamelCaseSeparatorNamingPolicy;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1, v6, v2}, Lcom/google/gson/chase/FieldNamingPolicy;-><init>(Ljava/lang/String;ILcom/google/gson/chase/FieldNamingStrategy2;)V

    sput-object v0, Lcom/google/gson/chase/FieldNamingPolicy;->c:Lcom/google/gson/chase/FieldNamingPolicy;

    .line 88
    new-instance v0, Lcom/google/gson/chase/FieldNamingPolicy;

    const-string v1, "LOWER_CASE_WITH_DASHES"

    new-instance v2, Lcom/google/gson/chase/LowerCamelCaseSeparatorNamingPolicy;

    const-string v3, "-"

    invoke-direct {v2, v3}, Lcom/google/gson/chase/LowerCamelCaseSeparatorNamingPolicy;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1, v7, v2}, Lcom/google/gson/chase/FieldNamingPolicy;-><init>(Ljava/lang/String;ILcom/google/gson/chase/FieldNamingStrategy2;)V

    sput-object v0, Lcom/google/gson/chase/FieldNamingPolicy;->d:Lcom/google/gson/chase/FieldNamingPolicy;

    .line 28
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/gson/chase/FieldNamingPolicy;

    sget-object v1, Lcom/google/gson/chase/FieldNamingPolicy;->a:Lcom/google/gson/chase/FieldNamingPolicy;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/gson/chase/FieldNamingPolicy;->b:Lcom/google/gson/chase/FieldNamingPolicy;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/gson/chase/FieldNamingPolicy;->c:Lcom/google/gson/chase/FieldNamingPolicy;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/gson/chase/FieldNamingPolicy;->d:Lcom/google/gson/chase/FieldNamingPolicy;

    aput-object v1, v0, v7

    sput-object v0, Lcom/google/gson/chase/FieldNamingPolicy;->f:[Lcom/google/gson/chase/FieldNamingPolicy;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/google/gson/chase/FieldNamingStrategy2;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/chase/FieldNamingStrategy2;",
            ")V"
        }
    .end annotation

    .prologue
    .line 92
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 93
    iput-object p3, p0, Lcom/google/gson/chase/FieldNamingPolicy;->e:Lcom/google/gson/chase/FieldNamingStrategy2;

    .line 94
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/gson/chase/FieldNamingPolicy;
    .locals 1
    .parameter

    .prologue
    .line 28
    const-class v0, Lcom/google/gson/chase/FieldNamingPolicy;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/gson/chase/FieldNamingPolicy;

    return-object v0
.end method

.method public static final values()[Lcom/google/gson/chase/FieldNamingPolicy;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/google/gson/chase/FieldNamingPolicy;->f:[Lcom/google/gson/chase/FieldNamingPolicy;

    invoke-virtual {v0}, [Lcom/google/gson/chase/FieldNamingPolicy;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/gson/chase/FieldNamingPolicy;

    return-object v0
.end method
