.class Lcom/google/gson/chase/Gson$1;
.super Lcom/google/gson/chase/internal/bind/ReflectiveTypeAdapterFactory;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/gson/chase/FieldNamingStrategy2;

.field final synthetic b:Lcom/google/gson/chase/Gson;


# direct methods
.method constructor <init>(Lcom/google/gson/chase/Gson;Lcom/google/gson/chase/internal/ConstructorConstructor;Lcom/google/gson/chase/FieldNamingStrategy2;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 223
    iput-object p1, p0, Lcom/google/gson/chase/Gson$1;->b:Lcom/google/gson/chase/Gson;

    iput-object p3, p0, Lcom/google/gson/chase/Gson$1;->a:Lcom/google/gson/chase/FieldNamingStrategy2;

    invoke-direct {p0, p2}, Lcom/google/gson/chase/internal/bind/ReflectiveTypeAdapterFactory;-><init>(Lcom/google/gson/chase/internal/ConstructorConstructor;)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Class;Ljava/lang/reflect/Field;)Ljava/lang/String;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/reflect/Field;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 213
    iget-object v0, p0, Lcom/google/gson/chase/Gson$1;->a:Lcom/google/gson/chase/FieldNamingStrategy2;

    new-instance v1, Lcom/google/gson/chase/FieldAttributes;

    invoke-direct {v1, p1, p2}, Lcom/google/gson/chase/FieldAttributes;-><init>(Ljava/lang/Class;Ljava/lang/reflect/Field;)V

    invoke-interface {v0, v1}, Lcom/google/gson/chase/FieldNamingStrategy2;->a(Lcom/google/gson/chase/FieldAttributes;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/Class;Ljava/lang/reflect/Field;)Z
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/reflect/Field;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 217
    iget-object v0, p0, Lcom/google/gson/chase/Gson$1;->b:Lcom/google/gson/chase/Gson;

    invoke-static {v0}, Lcom/google/gson/chase/Gson;->a(Lcom/google/gson/chase/Gson;)Lcom/google/gson/chase/ExclusionStrategy;

    move-result-object v0

    .line 218
    invoke-virtual {p2}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/gson/chase/ExclusionStrategy;->a(Ljava/lang/Class;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/gson/chase/FieldAttributes;

    invoke-direct {v1, p1, p2}, Lcom/google/gson/chase/FieldAttributes;-><init>(Ljava/lang/Class;Ljava/lang/reflect/Field;)V

    invoke-interface {v0, v1}, Lcom/google/gson/chase/ExclusionStrategy;->a(Lcom/google/gson/chase/FieldAttributes;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Ljava/lang/Class;Ljava/lang/reflect/Field;)Z
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/reflect/Field;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 224
    iget-object v0, p0, Lcom/google/gson/chase/Gson$1;->b:Lcom/google/gson/chase/Gson;

    invoke-static {v0}, Lcom/google/gson/chase/Gson;->b(Lcom/google/gson/chase/Gson;)Lcom/google/gson/chase/ExclusionStrategy;

    move-result-object v0

    .line 225
    invoke-virtual {p2}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/gson/chase/ExclusionStrategy;->a(Ljava/lang/Class;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/gson/chase/FieldAttributes;

    invoke-direct {v1, p1, p2}, Lcom/google/gson/chase/FieldAttributes;-><init>(Ljava/lang/Class;Ljava/lang/reflect/Field;)V

    invoke-interface {v0, v1}, Lcom/google/gson/chase/ExclusionStrategy;->a(Lcom/google/gson/chase/FieldAttributes;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
