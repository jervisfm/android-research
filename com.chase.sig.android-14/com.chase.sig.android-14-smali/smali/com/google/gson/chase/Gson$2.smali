.class Lcom/google/gson/chase/Gson$2;
.super Lcom/google/gson/chase/internal/bind/TypeAdapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/chase/internal/bind/TypeAdapter",
        "<",
        "Ljava/lang/Number;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/gson/chase/Gson;


# direct methods
.method constructor <init>(Lcom/google/gson/chase/Gson;)V
    .locals 0
    .parameter

    .prologue
    .line 292
    iput-object p1, p0, Lcom/google/gson/chase/Gson$2;->a:Lcom/google/gson/chase/Gson;

    invoke-direct {p0}, Lcom/google/gson/chase/internal/bind/TypeAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/gson/chase/stream/JsonReader;)Ljava/lang/Object;
    .locals 2
    .parameter

    .prologue
    .line 292
    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonReader;->f()Lcom/google/gson/chase/stream/JsonToken;

    move-result-object v0

    sget-object v1, Lcom/google/gson/chase/stream/JsonToken;->i:Lcom/google/gson/chase/stream/JsonToken;

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonReader;->j()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonReader;->k()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    goto :goto_0
.end method

.method public final synthetic a(Lcom/google/gson/chase/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 292
    check-cast p2, Ljava/lang/Number;

    if-nez p2, :cond_0

    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonWriter;->f()Lcom/google/gson/chase/stream/JsonWriter;

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p2}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v0

    iget-object v2, p0, Lcom/google/gson/chase/Gson$2;->a:Lcom/google/gson/chase/Gson;

    invoke-static {v0, v1}, Lcom/google/gson/chase/Gson;->a(D)V

    invoke-virtual {p1, p2}, Lcom/google/gson/chase/stream/JsonWriter;->a(Ljava/lang/Number;)Lcom/google/gson/chase/stream/JsonWriter;

    goto :goto_0
.end method
