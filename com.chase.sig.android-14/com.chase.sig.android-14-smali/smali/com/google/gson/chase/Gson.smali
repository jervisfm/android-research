.class public final Lcom/google/gson/chase/Gson;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;

.field static final b:Lcom/google/gson/chase/AnonymousAndLocalClassExclusionStrategy;

.field static final c:Lcom/google/gson/chase/SyntheticFieldExclusionStrategy;

.field static final d:Lcom/google/gson/chase/ModifierBasedExclusionStrategy;

.field static final e:Lcom/google/gson/chase/FieldNamingStrategy2;

.field private static final f:Lcom/google/gson/chase/ExclusionStrategy;


# instance fields
.field private final g:Lcom/google/gson/chase/ExclusionStrategy;

.field private final h:Lcom/google/gson/chase/ExclusionStrategy;

.field private final i:Lcom/google/gson/chase/internal/ConstructorConstructor;

.field private final j:Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap",
            "<",
            "Lcom/google/gson/chase/JsonSerializer",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final k:Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap",
            "<",
            "Lcom/google/gson/chase/JsonDeserializer",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final l:Z

.field private final m:Z

.field private final n:Z

.field private final o:Z

.field private final p:Lcom/google/gson/chase/internal/bind/MiniGson;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 104
    new-instance v0, Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;

    invoke-direct {v0}, Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;-><init>()V

    invoke-virtual {v0}, Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;->a()Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;

    move-result-object v0

    sput-object v0, Lcom/google/gson/chase/Gson;->a:Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;

    .line 110
    new-instance v0, Lcom/google/gson/chase/AnonymousAndLocalClassExclusionStrategy;

    invoke-direct {v0}, Lcom/google/gson/chase/AnonymousAndLocalClassExclusionStrategy;-><init>()V

    sput-object v0, Lcom/google/gson/chase/Gson;->b:Lcom/google/gson/chase/AnonymousAndLocalClassExclusionStrategy;

    .line 112
    new-instance v0, Lcom/google/gson/chase/SyntheticFieldExclusionStrategy;

    invoke-direct {v0}, Lcom/google/gson/chase/SyntheticFieldExclusionStrategy;-><init>()V

    sput-object v0, Lcom/google/gson/chase/Gson;->c:Lcom/google/gson/chase/SyntheticFieldExclusionStrategy;

    .line 114
    new-instance v0, Lcom/google/gson/chase/ModifierBasedExclusionStrategy;

    const/4 v1, 0x2

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    invoke-direct {v0, v1}, Lcom/google/gson/chase/ModifierBasedExclusionStrategy;-><init>([I)V

    sput-object v0, Lcom/google/gson/chase/Gson;->d:Lcom/google/gson/chase/ModifierBasedExclusionStrategy;

    .line 116
    new-instance v0, Lcom/google/gson/chase/SerializedNameAnnotationInterceptingNamingPolicy;

    new-instance v1, Lcom/google/gson/chase/JavaFieldNamingPolicy;

    invoke-direct {v1}, Lcom/google/gson/chase/JavaFieldNamingPolicy;-><init>()V

    invoke-direct {v0, v1}, Lcom/google/gson/chase/SerializedNameAnnotationInterceptingNamingPolicy;-><init>(Lcom/google/gson/chase/FieldNamingStrategy2;)V

    sput-object v0, Lcom/google/gson/chase/Gson;->e:Lcom/google/gson/chase/FieldNamingStrategy2;

    .line 119
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sget-object v1, Lcom/google/gson/chase/Gson;->b:Lcom/google/gson/chase/AnonymousAndLocalClassExclusionStrategy;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v1, Lcom/google/gson/chase/Gson;->c:Lcom/google/gson/chase/SyntheticFieldExclusionStrategy;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v1, Lcom/google/gson/chase/Gson;->d:Lcom/google/gson/chase/ModifierBasedExclusionStrategy;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/google/gson/chase/DisjunctionExclusionStrategy;

    invoke-direct {v1, v0}, Lcom/google/gson/chase/DisjunctionExclusionStrategy;-><init>(Ljava/util/Collection;)V

    sput-object v1, Lcom/google/gson/chase/Gson;->f:Lcom/google/gson/chase/ExclusionStrategy;

    return-void

    .line 114
    :array_0
    .array-data 0x4
        0x80t 0x0t 0x0t 0x0t
        0x8t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method public constructor <init>()V
    .locals 15

    .prologue
    const/4 v5, 0x0

    .line 176
    sget-object v1, Lcom/google/gson/chase/Gson;->f:Lcom/google/gson/chase/ExclusionStrategy;

    sget-object v2, Lcom/google/gson/chase/Gson;->f:Lcom/google/gson/chase/ExclusionStrategy;

    sget-object v3, Lcom/google/gson/chase/Gson;->e:Lcom/google/gson/chase/FieldNamingStrategy2;

    sget-object v4, Lcom/google/gson/chase/Gson;->a:Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;

    sget-object v6, Lcom/google/gson/chase/Gson;->a:Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;

    sget-object v7, Lcom/google/gson/chase/Gson;->a:Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;

    const/4 v10, 0x1

    sget-object v13, Lcom/google/gson/chase/LongSerializationPolicy;->a:Lcom/google/gson/chase/LongSerializationPolicy;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v14

    move-object v0, p0

    move v8, v5

    move v9, v5

    move v11, v5

    move v12, v5

    invoke-direct/range {v0 .. v14}, Lcom/google/gson/chase/Gson;-><init>(Lcom/google/gson/chase/ExclusionStrategy;Lcom/google/gson/chase/ExclusionStrategy;Lcom/google/gson/chase/FieldNamingStrategy2;Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;ZLcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;ZZZZZLcom/google/gson/chase/LongSerializationPolicy;Ljava/util/List;)V

    .line 180
    return-void
.end method

.method constructor <init>(Lcom/google/gson/chase/ExclusionStrategy;Lcom/google/gson/chase/ExclusionStrategy;Lcom/google/gson/chase/FieldNamingStrategy2;Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;ZLcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;ZZZZZLcom/google/gson/chase/LongSerializationPolicy;Ljava/util/List;)V
    .locals 6
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/chase/ExclusionStrategy;",
            "Lcom/google/gson/chase/ExclusionStrategy;",
            "Lcom/google/gson/chase/FieldNamingStrategy2;",
            "Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap",
            "<",
            "Lcom/google/gson/chase/InstanceCreator",
            "<*>;>;Z",
            "Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap",
            "<",
            "Lcom/google/gson/chase/JsonSerializer",
            "<*>;>;",
            "Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap",
            "<",
            "Lcom/google/gson/chase/JsonDeserializer",
            "<*>;>;ZZZZZ",
            "Lcom/google/gson/chase/LongSerializationPolicy;",
            "Ljava/util/List",
            "<",
            "Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 191
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 192
    iput-object p1, p0, Lcom/google/gson/chase/Gson;->g:Lcom/google/gson/chase/ExclusionStrategy;

    .line 193
    iput-object p2, p0, Lcom/google/gson/chase/Gson;->h:Lcom/google/gson/chase/ExclusionStrategy;

    .line 194
    new-instance v1, Lcom/google/gson/chase/internal/ConstructorConstructor;

    invoke-direct {v1, p4}, Lcom/google/gson/chase/internal/ConstructorConstructor;-><init>(Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;)V

    iput-object v1, p0, Lcom/google/gson/chase/Gson;->i:Lcom/google/gson/chase/internal/ConstructorConstructor;

    .line 195
    iput-boolean p5, p0, Lcom/google/gson/chase/Gson;->l:Z

    .line 196
    iput-object p6, p0, Lcom/google/gson/chase/Gson;->j:Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;

    .line 197
    iput-object p7, p0, Lcom/google/gson/chase/Gson;->k:Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;

    .line 198
    iput-boolean p9, p0, Lcom/google/gson/chase/Gson;->n:Z

    .line 199
    move/from16 v0, p10

    iput-boolean v0, p0, Lcom/google/gson/chase/Gson;->m:Z

    .line 200
    move/from16 v0, p11

    iput-boolean v0, p0, Lcom/google/gson/chase/Gson;->o:Z

    .line 209
    new-instance v2, Lcom/google/gson/chase/Gson$1;

    iget-object v1, p0, Lcom/google/gson/chase/Gson;->i:Lcom/google/gson/chase/internal/ConstructorConstructor;

    invoke-direct {v2, p0, v1, p3}, Lcom/google/gson/chase/Gson$1;-><init>(Lcom/google/gson/chase/Gson;Lcom/google/gson/chase/internal/ConstructorConstructor;Lcom/google/gson/chase/FieldNamingStrategy2;)V

    .line 230
    new-instance v1, Lcom/google/gson/chase/internal/bind/MiniGson$Builder;

    invoke-direct {v1}, Lcom/google/gson/chase/internal/bind/MiniGson$Builder;-><init>()V

    invoke-virtual {v1}, Lcom/google/gson/chase/internal/bind/MiniGson$Builder;->a()Lcom/google/gson/chase/internal/bind/MiniGson$Builder;

    move-result-object v1

    sget-object v3, Lcom/google/gson/chase/internal/bind/TypeAdapters;->w:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    invoke-virtual {v1, v3}, Lcom/google/gson/chase/internal/bind/MiniGson$Builder;->a(Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;)Lcom/google/gson/chase/internal/bind/MiniGson$Builder;

    move-result-object v1

    sget-object v3, Lcom/google/gson/chase/internal/bind/TypeAdapters;->k:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    invoke-virtual {v1, v3}, Lcom/google/gson/chase/internal/bind/MiniGson$Builder;->a(Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;)Lcom/google/gson/chase/internal/bind/MiniGson$Builder;

    move-result-object v1

    sget-object v3, Lcom/google/gson/chase/internal/bind/TypeAdapters;->e:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    invoke-virtual {v1, v3}, Lcom/google/gson/chase/internal/bind/MiniGson$Builder;->a(Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;)Lcom/google/gson/chase/internal/bind/MiniGson$Builder;

    move-result-object v1

    sget-object v3, Lcom/google/gson/chase/internal/bind/TypeAdapters;->g:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    invoke-virtual {v1, v3}, Lcom/google/gson/chase/internal/bind/MiniGson$Builder;->a(Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;)Lcom/google/gson/chase/internal/bind/MiniGson$Builder;

    move-result-object v1

    sget-object v3, Lcom/google/gson/chase/internal/bind/TypeAdapters;->i:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    invoke-virtual {v1, v3}, Lcom/google/gson/chase/internal/bind/MiniGson$Builder;->a(Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;)Lcom/google/gson/chase/internal/bind/MiniGson$Builder;

    move-result-object v3

    sget-object v4, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    const-class v5, Ljava/lang/Long;

    sget-object v1, Lcom/google/gson/chase/LongSerializationPolicy;->a:Lcom/google/gson/chase/LongSerializationPolicy;

    move-object/from16 v0, p13

    if-ne v0, v1, :cond_0

    sget-object v1, Lcom/google/gson/chase/internal/bind/TypeAdapters;->l:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    :goto_0
    invoke-static {v4, v5, v1}, Lcom/google/gson/chase/internal/bind/TypeAdapters;->a(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/gson/chase/internal/bind/TypeAdapter;)Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/google/gson/chase/internal/bind/MiniGson$Builder;->a(Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;)Lcom/google/gson/chase/internal/bind/MiniGson$Builder;

    move-result-object v3

    sget-object v4, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    const-class v5, Ljava/lang/Double;

    if-eqz p12, :cond_1

    sget-object v1, Lcom/google/gson/chase/internal/bind/TypeAdapters;->p:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    :goto_1
    invoke-static {v4, v5, v1}, Lcom/google/gson/chase/internal/bind/TypeAdapters;->a(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/gson/chase/internal/bind/TypeAdapter;)Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/google/gson/chase/internal/bind/MiniGson$Builder;->a(Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;)Lcom/google/gson/chase/internal/bind/MiniGson$Builder;

    move-result-object v3

    sget-object v4, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    const-class v5, Ljava/lang/Float;

    if-eqz p12, :cond_2

    sget-object v1, Lcom/google/gson/chase/internal/bind/TypeAdapters;->n:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    :goto_2
    invoke-static {v4, v5, v1}, Lcom/google/gson/chase/internal/bind/TypeAdapters;->a(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/gson/chase/internal/bind/TypeAdapter;)Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/google/gson/chase/internal/bind/MiniGson$Builder;->a(Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;)Lcom/google/gson/chase/internal/bind/MiniGson$Builder;

    move-result-object v1

    new-instance v3, Lcom/google/gson/chase/internal/bind/ExcludedTypeAdapterFactory;

    invoke-direct {v3, p2, p1}, Lcom/google/gson/chase/internal/bind/ExcludedTypeAdapterFactory;-><init>(Lcom/google/gson/chase/ExclusionStrategy;Lcom/google/gson/chase/ExclusionStrategy;)V

    invoke-virtual {v1, v3}, Lcom/google/gson/chase/internal/bind/MiniGson$Builder;->a(Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;)Lcom/google/gson/chase/internal/bind/MiniGson$Builder;

    move-result-object v1

    sget-object v3, Lcom/google/gson/chase/internal/bind/TypeAdapters;->s:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    invoke-virtual {v1, v3}, Lcom/google/gson/chase/internal/bind/MiniGson$Builder;->a(Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;)Lcom/google/gson/chase/internal/bind/MiniGson$Builder;

    move-result-object v1

    sget-object v3, Lcom/google/gson/chase/internal/bind/TypeAdapters;->u:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    invoke-virtual {v1, v3}, Lcom/google/gson/chase/internal/bind/MiniGson$Builder;->a(Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;)Lcom/google/gson/chase/internal/bind/MiniGson$Builder;

    move-result-object v1

    sget-object v3, Lcom/google/gson/chase/internal/bind/TypeAdapters;->y:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    invoke-virtual {v1, v3}, Lcom/google/gson/chase/internal/bind/MiniGson$Builder;->a(Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;)Lcom/google/gson/chase/internal/bind/MiniGson$Builder;

    move-result-object v1

    sget-object v3, Lcom/google/gson/chase/internal/bind/TypeAdapters;->A:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    invoke-virtual {v1, v3}, Lcom/google/gson/chase/internal/bind/MiniGson$Builder;->a(Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;)Lcom/google/gson/chase/internal/bind/MiniGson$Builder;

    move-result-object v1

    const-class v3, Ljava/math/BigDecimal;

    new-instance v4, Lcom/google/gson/chase/internal/bind/BigDecimalTypeAdapter;

    invoke-direct {v4}, Lcom/google/gson/chase/internal/bind/BigDecimalTypeAdapter;-><init>()V

    invoke-virtual {v1, v3, v4}, Lcom/google/gson/chase/internal/bind/MiniGson$Builder;->a(Ljava/lang/Class;Lcom/google/gson/chase/internal/bind/TypeAdapter;)Lcom/google/gson/chase/internal/bind/MiniGson$Builder;

    move-result-object v1

    const-class v3, Ljava/math/BigInteger;

    new-instance v4, Lcom/google/gson/chase/internal/bind/BigIntegerTypeAdapter;

    invoke-direct {v4}, Lcom/google/gson/chase/internal/bind/BigIntegerTypeAdapter;-><init>()V

    invoke-virtual {v1, v3, v4}, Lcom/google/gson/chase/internal/bind/MiniGson$Builder;->a(Ljava/lang/Class;Lcom/google/gson/chase/internal/bind/TypeAdapter;)Lcom/google/gson/chase/internal/bind/MiniGson$Builder;

    move-result-object v1

    sget-object v3, Lcom/google/gson/chase/internal/bind/TypeAdapters;->P:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    invoke-virtual {v1, v3}, Lcom/google/gson/chase/internal/bind/MiniGson$Builder;->a(Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;)Lcom/google/gson/chase/internal/bind/MiniGson$Builder;

    move-result-object v1

    sget-object v3, Lcom/google/gson/chase/internal/bind/ObjectTypeAdapter;->a:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    invoke-virtual {v1, v3}, Lcom/google/gson/chase/internal/bind/MiniGson$Builder;->a(Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;)Lcom/google/gson/chase/internal/bind/MiniGson$Builder;

    move-result-object v3

    .line 254
    invoke-interface/range {p14 .. p14}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    .line 255
    invoke-virtual {v3, v1}, Lcom/google/gson/chase/internal/bind/MiniGson$Builder;->a(Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;)Lcom/google/gson/chase/internal/bind/MiniGson$Builder;

    goto :goto_3

    .line 230
    :cond_0
    new-instance v1, Lcom/google/gson/chase/Gson$4;

    invoke-direct {v1, p0}, Lcom/google/gson/chase/Gson$4;-><init>(Lcom/google/gson/chase/Gson;)V

    goto/16 :goto_0

    :cond_1
    new-instance v1, Lcom/google/gson/chase/Gson$2;

    invoke-direct {v1, p0}, Lcom/google/gson/chase/Gson$2;-><init>(Lcom/google/gson/chase/Gson;)V

    goto :goto_1

    :cond_2
    new-instance v1, Lcom/google/gson/chase/Gson$3;

    invoke-direct {v1, p0}, Lcom/google/gson/chase/Gson$3;-><init>(Lcom/google/gson/chase/Gson;)V

    goto :goto_2

    .line 258
    :cond_3
    new-instance v1, Lcom/google/gson/chase/GsonToMiniGsonTypeAdapterFactory;

    invoke-direct {v1, p0, p6, p7}, Lcom/google/gson/chase/GsonToMiniGsonTypeAdapterFactory;-><init>(Lcom/google/gson/chase/Gson;Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;)V

    invoke-virtual {v3, v1}, Lcom/google/gson/chase/internal/bind/MiniGson$Builder;->a(Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;)Lcom/google/gson/chase/internal/bind/MiniGson$Builder;

    move-result-object v1

    new-instance v4, Lcom/google/gson/chase/internal/bind/CollectionTypeAdapterFactory;

    iget-object v5, p0, Lcom/google/gson/chase/Gson;->i:Lcom/google/gson/chase/internal/ConstructorConstructor;

    invoke-direct {v4, v5}, Lcom/google/gson/chase/internal/bind/CollectionTypeAdapterFactory;-><init>(Lcom/google/gson/chase/internal/ConstructorConstructor;)V

    invoke-virtual {v1, v4}, Lcom/google/gson/chase/internal/bind/MiniGson$Builder;->a(Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;)Lcom/google/gson/chase/internal/bind/MiniGson$Builder;

    move-result-object v1

    sget-object v4, Lcom/google/gson/chase/internal/bind/TypeAdapters;->C:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    invoke-virtual {v1, v4}, Lcom/google/gson/chase/internal/bind/MiniGson$Builder;->a(Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;)Lcom/google/gson/chase/internal/bind/MiniGson$Builder;

    move-result-object v1

    sget-object v4, Lcom/google/gson/chase/internal/bind/TypeAdapters;->E:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    invoke-virtual {v1, v4}, Lcom/google/gson/chase/internal/bind/MiniGson$Builder;->a(Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;)Lcom/google/gson/chase/internal/bind/MiniGson$Builder;

    move-result-object v1

    sget-object v4, Lcom/google/gson/chase/internal/bind/TypeAdapters;->I:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    invoke-virtual {v1, v4}, Lcom/google/gson/chase/internal/bind/MiniGson$Builder;->a(Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;)Lcom/google/gson/chase/internal/bind/MiniGson$Builder;

    move-result-object v1

    sget-object v4, Lcom/google/gson/chase/internal/bind/TypeAdapters;->N:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    invoke-virtual {v1, v4}, Lcom/google/gson/chase/internal/bind/MiniGson$Builder;->a(Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;)Lcom/google/gson/chase/internal/bind/MiniGson$Builder;

    move-result-object v1

    sget-object v4, Lcom/google/gson/chase/internal/bind/TypeAdapters;->G:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    invoke-virtual {v1, v4}, Lcom/google/gson/chase/internal/bind/MiniGson$Builder;->a(Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;)Lcom/google/gson/chase/internal/bind/MiniGson$Builder;

    move-result-object v1

    sget-object v4, Lcom/google/gson/chase/internal/bind/TypeAdapters;->b:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    invoke-virtual {v1, v4}, Lcom/google/gson/chase/internal/bind/MiniGson$Builder;->a(Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;)Lcom/google/gson/chase/internal/bind/MiniGson$Builder;

    move-result-object v1

    sget-object v4, Lcom/google/gson/chase/internal/bind/DateTypeAdapter;->a:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    invoke-virtual {v1, v4}, Lcom/google/gson/chase/internal/bind/MiniGson$Builder;->a(Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;)Lcom/google/gson/chase/internal/bind/MiniGson$Builder;

    move-result-object v1

    sget-object v4, Lcom/google/gson/chase/internal/bind/TypeAdapters;->L:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    invoke-virtual {v1, v4}, Lcom/google/gson/chase/internal/bind/MiniGson$Builder;->a(Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;)Lcom/google/gson/chase/internal/bind/MiniGson$Builder;

    move-result-object v1

    sget-object v4, Lcom/google/gson/chase/internal/bind/TimeTypeAdapter;->a:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    invoke-virtual {v1, v4}, Lcom/google/gson/chase/internal/bind/MiniGson$Builder;->a(Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;)Lcom/google/gson/chase/internal/bind/MiniGson$Builder;

    move-result-object v1

    sget-object v4, Lcom/google/gson/chase/internal/bind/SqlDateTypeAdapter;->a:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    invoke-virtual {v1, v4}, Lcom/google/gson/chase/internal/bind/MiniGson$Builder;->a(Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;)Lcom/google/gson/chase/internal/bind/MiniGson$Builder;

    move-result-object v1

    sget-object v4, Lcom/google/gson/chase/internal/bind/TypeAdapters;->J:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    invoke-virtual {v1, v4}, Lcom/google/gson/chase/internal/bind/MiniGson$Builder;->a(Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;)Lcom/google/gson/chase/internal/bind/MiniGson$Builder;

    move-result-object v1

    new-instance v4, Lcom/google/gson/chase/internal/bind/MapTypeAdapterFactory;

    iget-object v5, p0, Lcom/google/gson/chase/Gson;->i:Lcom/google/gson/chase/internal/ConstructorConstructor;

    invoke-direct {v4, v5, p8}, Lcom/google/gson/chase/internal/bind/MapTypeAdapterFactory;-><init>(Lcom/google/gson/chase/internal/ConstructorConstructor;Z)V

    invoke-virtual {v1, v4}, Lcom/google/gson/chase/internal/bind/MiniGson$Builder;->a(Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;)Lcom/google/gson/chase/internal/bind/MiniGson$Builder;

    move-result-object v1

    sget-object v4, Lcom/google/gson/chase/internal/bind/ArrayTypeAdapter;->a:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    invoke-virtual {v1, v4}, Lcom/google/gson/chase/internal/bind/MiniGson$Builder;->a(Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;)Lcom/google/gson/chase/internal/bind/MiniGson$Builder;

    move-result-object v1

    sget-object v4, Lcom/google/gson/chase/internal/bind/TypeAdapters;->Q:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    invoke-virtual {v1, v4}, Lcom/google/gson/chase/internal/bind/MiniGson$Builder;->a(Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;)Lcom/google/gson/chase/internal/bind/MiniGson$Builder;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/google/gson/chase/internal/bind/MiniGson$Builder;->a(Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;)Lcom/google/gson/chase/internal/bind/MiniGson$Builder;

    .line 277
    invoke-virtual {v3}, Lcom/google/gson/chase/internal/bind/MiniGson$Builder;->b()Lcom/google/gson/chase/internal/bind/MiniGson;

    move-result-object v1

    iput-object v1, p0, Lcom/google/gson/chase/Gson;->p:Lcom/google/gson/chase/internal/bind/MiniGson;

    .line 278
    return-void
.end method

.method static synthetic a(Lcom/google/gson/chase/Gson;)Lcom/google/gson/chase/ExclusionStrategy;
    .locals 1
    .parameter

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/gson/chase/Gson;->h:Lcom/google/gson/chase/ExclusionStrategy;

    return-object v0
.end method

.method private a(Ljava/io/Writer;)Lcom/google/gson/chase/stream/JsonWriter;
    .locals 2
    .parameter

    .prologue
    .line 557
    iget-boolean v0, p0, Lcom/google/gson/chase/Gson;->n:Z

    if-eqz v0, :cond_0

    .line 558
    const-string v0, ")]}\'\n"

    invoke-virtual {p1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    .line 560
    :cond_0
    new-instance v0, Lcom/google/gson/chase/stream/JsonWriter;

    invoke-direct {v0, p1}, Lcom/google/gson/chase/stream/JsonWriter;-><init>(Ljava/io/Writer;)V

    .line 561
    iget-boolean v1, p0, Lcom/google/gson/chase/Gson;->o:Z

    if-eqz v1, :cond_1

    .line 562
    const-string v1, "  "

    invoke-virtual {v0, v1}, Lcom/google/gson/chase/stream/JsonWriter;->c(Ljava/lang/String;)V

    .line 564
    :cond_1
    iget-boolean v1, p0, Lcom/google/gson/chase/Gson;->l:Z

    invoke-virtual {v0, v1}, Lcom/google/gson/chase/stream/JsonWriter;->d(Z)V

    .line 565
    return-object v0
.end method

.method private a(Lcom/google/gson/chase/stream/JsonReader;Ljava/lang/reflect/Type;)Ljava/lang/Object;
    .locals 4
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/gson/chase/stream/JsonReader;",
            "Ljava/lang/reflect/Type;",
            ")TT;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 713
    .line 714
    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonReader;->o()Z

    move-result v2

    .line 715
    invoke-virtual {p1, v1}, Lcom/google/gson/chase/stream/JsonReader;->a(Z)V

    .line 717
    :try_start_0
    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonReader;->f()Lcom/google/gson/chase/stream/JsonToken;

    .line 718
    const/4 v1, 0x0

    .line 719
    iget-object v0, p0, Lcom/google/gson/chase/Gson;->p:Lcom/google/gson/chase/internal/bind/MiniGson;

    invoke-static {p2}, Lcom/google/gson/chase/reflect/TypeToken;->a(Ljava/lang/reflect/Type;)Lcom/google/gson/chase/reflect/TypeToken;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/gson/chase/internal/bind/MiniGson;->a(Lcom/google/gson/chase/reflect/TypeToken;)Lcom/google/gson/chase/internal/bind/TypeAdapter;

    move-result-object v0

    .line 720
    invoke-virtual {v0, p1}, Lcom/google/gson/chase/internal/bind/TypeAdapter;->a(Lcom/google/gson/chase/stream/JsonReader;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    .line 736
    invoke-virtual {p1, v2}, Lcom/google/gson/chase/stream/JsonReader;->a(Z)V

    :goto_0
    return-object v0

    .line 721
    :catch_0
    move-exception v0

    .line 726
    if-eqz v1, :cond_0

    .line 727
    invoke-virtual {p1, v2}, Lcom/google/gson/chase/stream/JsonReader;->a(Z)V

    const/4 v0, 0x0

    goto :goto_0

    .line 729
    :cond_0
    :try_start_1
    new-instance v1, Lcom/google/gson/chase/JsonSyntaxException;

    invoke-direct {v1, v0}, Lcom/google/gson/chase/JsonSyntaxException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 736
    :catchall_0
    move-exception v0

    invoke-virtual {p1, v2}, Lcom/google/gson/chase/stream/JsonReader;->a(Z)V

    throw v0

    .line 730
    :catch_1
    move-exception v0

    .line 731
    :try_start_2
    new-instance v1, Lcom/google/gson/chase/JsonSyntaxException;

    invoke-direct {v1, v0}, Lcom/google/gson/chase/JsonSyntaxException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 732
    :catch_2
    move-exception v0

    .line 734
    new-instance v1, Lcom/google/gson/chase/JsonSyntaxException;

    invoke-direct {v1, v0}, Lcom/google/gson/chase/JsonSyntaxException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method

.method static synthetic a(D)V
    .locals 3
    .parameter

    .prologue
    .line 102
    invoke-static {p0, p1}, Ljava/lang/Double;->isNaN(D)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0, p1}, Ljava/lang/Double;->isInfinite(D)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a valid double value as per JSON specification. To override this behavior, use GsonBuilder.serializeSpecialDoubleValues() method."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-void
.end method

.method static synthetic b(Lcom/google/gson/chase/Gson;)Lcom/google/gson/chase/ExclusionStrategy;
    .locals 1
    .parameter

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/gson/chase/Gson;->g:Lcom/google/gson/chase/ExclusionStrategy;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 608
    invoke-virtual {p0, p1, p2}, Lcom/google/gson/chase/Gson;->a(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    .line 609
    invoke-static {p2}, Lcom/google/gson/chase/internal/Primitives;->a(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;
    .locals 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/lang/reflect/Type;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 632
    if-nez p1, :cond_1

    .line 633
    const/4 v0, 0x0

    .line 637
    :cond_0
    return-object v0

    .line 635
    :cond_1
    new-instance v0, Ljava/io/StringReader;

    invoke-direct {v0, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    .line 636
    new-instance v1, Lcom/google/gson/chase/stream/JsonReader;

    invoke-direct {v1, v0}, Lcom/google/gson/chase/stream/JsonReader;-><init>(Ljava/io/Reader;)V

    invoke-direct {p0, v1, p2}, Lcom/google/gson/chase/Gson;->a(Lcom/google/gson/chase/stream/JsonReader;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-virtual {v1}, Lcom/google/gson/chase/stream/JsonReader;->f()Lcom/google/gson/chase/stream/JsonToken;

    move-result-object v1

    sget-object v2, Lcom/google/gson/chase/stream/JsonToken;->j:Lcom/google/gson/chase/stream/JsonToken;

    if-eq v1, v2, :cond_0

    new-instance v0, Lcom/google/gson/chase/JsonIOException;

    const-string v1, "JSON document was not fully consumed."

    invoke-direct {v0, v1}, Lcom/google/gson/chase/JsonIOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Lcom/google/gson/chase/stream/MalformedJsonException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/gson/chase/JsonSyntaxException;

    invoke-direct {v1, v0}, Lcom/google/gson/chase/JsonSyntaxException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    new-instance v1, Lcom/google/gson/chase/JsonIOException;

    invoke-direct {v1, v0}, Lcom/google/gson/chase/JsonIOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Ljava/lang/Object;)Ljava/lang/String;
    .locals 7
    .parameter

    .prologue
    .line 423
    if-nez p1, :cond_0

    .line 424
    sget-object v0, Lcom/google/gson/chase/JsonNull;->a:Lcom/google/gson/chase/JsonNull;

    new-instance v1, Ljava/io/StringWriter;

    invoke-direct {v1}, Ljava/io/StringWriter;-><init>()V

    :try_start_0
    invoke-static {v1}, Lcom/google/gson/chase/internal/Streams;->a(Ljava/lang/Appendable;)Ljava/io/Writer;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/gson/chase/Gson;->a(Ljava/io/Writer;)Lcom/google/gson/chase/stream/JsonWriter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/gson/chase/stream/JsonWriter;->g()Z

    move-result v3

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Lcom/google/gson/chase/stream/JsonWriter;->b(Z)V

    invoke-virtual {v2}, Lcom/google/gson/chase/stream/JsonWriter;->h()Z

    move-result v4

    iget-boolean v5, p0, Lcom/google/gson/chase/Gson;->m:Z

    invoke-virtual {v2, v5}, Lcom/google/gson/chase/stream/JsonWriter;->c(Z)V

    invoke-virtual {v2}, Lcom/google/gson/chase/stream/JsonWriter;->i()Z

    move-result v5

    iget-boolean v6, p0, Lcom/google/gson/chase/Gson;->l:Z

    invoke-virtual {v2, v6}, Lcom/google/gson/chase/stream/JsonWriter;->d(Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    invoke-static {v0, v2}, Lcom/google/gson/chase/internal/Streams;->a(Lcom/google/gson/chase/JsonElement;Lcom/google/gson/chase/stream/JsonWriter;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :try_start_2
    invoke-virtual {v2, v3}, Lcom/google/gson/chase/stream/JsonWriter;->b(Z)V

    invoke-virtual {v2, v4}, Lcom/google/gson/chase/stream/JsonWriter;->c(Z)V

    invoke-virtual {v2, v5}, Lcom/google/gson/chase/stream/JsonWriter;->d(Z)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    invoke-virtual {v1}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v0

    .line 426
    :goto_0
    return-object v0

    .line 424
    :catch_0
    move-exception v0

    :try_start_3
    new-instance v1, Lcom/google/gson/chase/JsonIOException;

    invoke-direct {v1, v0}, Lcom/google/gson/chase/JsonIOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception v0

    :try_start_4
    invoke-virtual {v2, v3}, Lcom/google/gson/chase/stream/JsonWriter;->b(Z)V

    invoke-virtual {v2, v4}, Lcom/google/gson/chase/stream/JsonWriter;->c(Z)V

    invoke-virtual {v2, v5}, Lcom/google/gson/chase/stream/JsonWriter;->d(Z)V

    throw v0
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    :catch_1
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 426
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    new-instance v1, Ljava/io/StringWriter;

    invoke-direct {v1}, Ljava/io/StringWriter;-><init>()V

    :try_start_5
    invoke-static {v1}, Lcom/google/gson/chase/internal/Streams;->a(Ljava/lang/Appendable;)Ljava/io/Writer;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/gson/chase/Gson;->a(Ljava/io/Writer;)Lcom/google/gson/chase/stream/JsonWriter;

    move-result-object v2

    iget-object v3, p0, Lcom/google/gson/chase/Gson;->p:Lcom/google/gson/chase/internal/bind/MiniGson;

    invoke-static {v0}, Lcom/google/gson/chase/reflect/TypeToken;->a(Ljava/lang/reflect/Type;)Lcom/google/gson/chase/reflect/TypeToken;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/gson/chase/internal/bind/MiniGson;->a(Lcom/google/gson/chase/reflect/TypeToken;)Lcom/google/gson/chase/internal/bind/TypeAdapter;

    move-result-object v0

    invoke-virtual {v2}, Lcom/google/gson/chase/stream/JsonWriter;->g()Z

    move-result v3

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Lcom/google/gson/chase/stream/JsonWriter;->b(Z)V

    invoke-virtual {v2}, Lcom/google/gson/chase/stream/JsonWriter;->h()Z

    move-result v4

    iget-boolean v5, p0, Lcom/google/gson/chase/Gson;->m:Z

    invoke-virtual {v2, v5}, Lcom/google/gson/chase/stream/JsonWriter;->c(Z)V

    invoke-virtual {v2}, Lcom/google/gson/chase/stream/JsonWriter;->i()Z

    move-result v5

    iget-boolean v6, p0, Lcom/google/gson/chase/Gson;->l:Z

    invoke-virtual {v2, v6}, Lcom/google/gson/chase/stream/JsonWriter;->d(Z)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    :try_start_6
    invoke-virtual {v0, v2, p1}, Lcom/google/gson/chase/internal/bind/TypeAdapter;->a(Lcom/google/gson/chase/stream/JsonWriter;Ljava/lang/Object;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    :try_start_7
    invoke-virtual {v2, v3}, Lcom/google/gson/chase/stream/JsonWriter;->b(Z)V

    invoke-virtual {v2, v4}, Lcom/google/gson/chase/stream/JsonWriter;->c(Z)V

    invoke-virtual {v2, v5}, Lcom/google/gson/chase/stream/JsonWriter;->d(Z)V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    invoke-virtual {v1}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :catch_2
    move-exception v0

    :try_start_8
    new-instance v1, Lcom/google/gson/chase/JsonIOException;

    invoke-direct {v1, v0}, Lcom/google/gson/chase/JsonIOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    :catchall_1
    move-exception v0

    :try_start_9
    invoke-virtual {v2, v3}, Lcom/google/gson/chase/stream/JsonWriter;->b(Z)V

    invoke-virtual {v2, v4}, Lcom/google/gson/chase/stream/JsonWriter;->c(Z)V

    invoke-virtual {v2, v5}, Lcom/google/gson/chase/stream/JsonWriter;->d(Z)V

    throw v0
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_3

    :catch_3
    move-exception v0

    new-instance v1, Lcom/google/gson/chase/JsonIOException;

    invoke-direct {v1, v0}, Lcom/google/gson/chase/JsonIOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 788
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "{serializeNulls:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/google/gson/chase/Gson;->l:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",serializers:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/gson/chase/Gson;->j:Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",deserializers:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/gson/chase/Gson;->k:Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ",instanceCreators:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/gson/chase/Gson;->i:Lcom/google/gson/chase/internal/ConstructorConstructor;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 798
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
