.class public final Lcom/google/gson/chase/GsonBuilder;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Lcom/google/gson/chase/InnerClassExclusionStrategy;

.field private static final b:Lcom/google/gson/chase/ExposeAnnotationDeserializationExclusionStrategy;

.field private static final c:Lcom/google/gson/chase/ExposeAnnotationSerializationExclusionStrategy;


# instance fields
.field private final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/gson/chase/ExclusionStrategy;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/gson/chase/ExclusionStrategy;",
            ">;"
        }
    .end annotation
.end field

.field private f:D

.field private g:Lcom/google/gson/chase/ModifierBasedExclusionStrategy;

.field private h:Z

.field private i:Z

.field private j:Lcom/google/gson/chase/LongSerializationPolicy;

.field private k:Lcom/google/gson/chase/FieldNamingStrategy2;

.field private final l:Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap",
            "<",
            "Lcom/google/gson/chase/InstanceCreator",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final m:Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap",
            "<",
            "Lcom/google/gson/chase/JsonSerializer",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final n:Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap",
            "<",
            "Lcom/google/gson/chase/JsonDeserializer",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private p:Z

.field private q:Ljava/lang/String;

.field private r:I

.field private s:I

.field private t:Z

.field private u:Z

.field private v:Z

.field private w:Z

.field private x:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 69
    new-instance v0, Lcom/google/gson/chase/InnerClassExclusionStrategy;

    invoke-direct {v0}, Lcom/google/gson/chase/InnerClassExclusionStrategy;-><init>()V

    sput-object v0, Lcom/google/gson/chase/GsonBuilder;->a:Lcom/google/gson/chase/InnerClassExclusionStrategy;

    .line 72
    new-instance v0, Lcom/google/gson/chase/ExposeAnnotationDeserializationExclusionStrategy;

    invoke-direct {v0}, Lcom/google/gson/chase/ExposeAnnotationDeserializationExclusionStrategy;-><init>()V

    sput-object v0, Lcom/google/gson/chase/GsonBuilder;->b:Lcom/google/gson/chase/ExposeAnnotationDeserializationExclusionStrategy;

    .line 75
    new-instance v0, Lcom/google/gson/chase/ExposeAnnotationSerializationExclusionStrategy;

    invoke-direct {v0}, Lcom/google/gson/chase/ExposeAnnotationSerializationExclusionStrategy;-><init>()V

    sput-object v0, Lcom/google/gson/chase/GsonBuilder;->c:Lcom/google/gson/chase/ExposeAnnotationSerializationExclusionStrategy;

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/gson/chase/GsonBuilder;->d:Ljava/util/Set;

    .line 80
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/gson/chase/GsonBuilder;->e:Ljava/util/Set;

    .line 92
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/gson/chase/GsonBuilder;->o:Ljava/util/List;

    .line 98
    iput-boolean v2, p0, Lcom/google/gson/chase/GsonBuilder;->t:Z

    .line 112
    iget-object v0, p0, Lcom/google/gson/chase/GsonBuilder;->e:Ljava/util/Set;

    sget-object v1, Lcom/google/gson/chase/Gson;->b:Lcom/google/gson/chase/AnonymousAndLocalClassExclusionStrategy;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 113
    iget-object v0, p0, Lcom/google/gson/chase/GsonBuilder;->e:Ljava/util/Set;

    sget-object v1, Lcom/google/gson/chase/Gson;->c:Lcom/google/gson/chase/SyntheticFieldExclusionStrategy;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 114
    iget-object v0, p0, Lcom/google/gson/chase/GsonBuilder;->d:Ljava/util/Set;

    sget-object v1, Lcom/google/gson/chase/Gson;->b:Lcom/google/gson/chase/AnonymousAndLocalClassExclusionStrategy;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 115
    iget-object v0, p0, Lcom/google/gson/chase/GsonBuilder;->d:Ljava/util/Set;

    sget-object v1, Lcom/google/gson/chase/Gson;->c:Lcom/google/gson/chase/SyntheticFieldExclusionStrategy;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 118
    const-wide/high16 v0, -0x4010

    iput-wide v0, p0, Lcom/google/gson/chase/GsonBuilder;->f:D

    .line 119
    iput-boolean v3, p0, Lcom/google/gson/chase/GsonBuilder;->h:Z

    .line 120
    iput-boolean v2, p0, Lcom/google/gson/chase/GsonBuilder;->w:Z

    .line 121
    iput-boolean v3, p0, Lcom/google/gson/chase/GsonBuilder;->v:Z

    .line 122
    sget-object v0, Lcom/google/gson/chase/Gson;->d:Lcom/google/gson/chase/ModifierBasedExclusionStrategy;

    iput-object v0, p0, Lcom/google/gson/chase/GsonBuilder;->g:Lcom/google/gson/chase/ModifierBasedExclusionStrategy;

    .line 123
    iput-boolean v2, p0, Lcom/google/gson/chase/GsonBuilder;->i:Z

    .line 124
    sget-object v0, Lcom/google/gson/chase/LongSerializationPolicy;->a:Lcom/google/gson/chase/LongSerializationPolicy;

    iput-object v0, p0, Lcom/google/gson/chase/GsonBuilder;->j:Lcom/google/gson/chase/LongSerializationPolicy;

    .line 125
    sget-object v0, Lcom/google/gson/chase/Gson;->e:Lcom/google/gson/chase/FieldNamingStrategy2;

    iput-object v0, p0, Lcom/google/gson/chase/GsonBuilder;->k:Lcom/google/gson/chase/FieldNamingStrategy2;

    .line 126
    new-instance v0, Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;

    invoke-direct {v0}, Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;-><init>()V

    iput-object v0, p0, Lcom/google/gson/chase/GsonBuilder;->l:Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;

    .line 127
    new-instance v0, Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;

    invoke-direct {v0}, Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;-><init>()V

    iput-object v0, p0, Lcom/google/gson/chase/GsonBuilder;->m:Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;

    .line 128
    new-instance v0, Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;

    invoke-direct {v0}, Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;-><init>()V

    iput-object v0, p0, Lcom/google/gson/chase/GsonBuilder;->n:Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;

    .line 129
    iput-boolean v2, p0, Lcom/google/gson/chase/GsonBuilder;->p:Z

    .line 130
    iput v4, p0, Lcom/google/gson/chase/GsonBuilder;->r:I

    .line 131
    iput v4, p0, Lcom/google/gson/chase/GsonBuilder;->s:I

    .line 132
    iput-boolean v2, p0, Lcom/google/gson/chase/GsonBuilder;->u:Z

    .line 133
    iput-boolean v2, p0, Lcom/google/gson/chase/GsonBuilder;->x:Z

    .line 134
    return-void
.end method

.method private static a(Ljava/lang/Class;Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;Ljava/lang/Object;)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<*>;",
            "Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap",
            "<TT;>;TT;)V"
        }
    .end annotation

    .prologue
    .line 712
    invoke-virtual {p1, p0}, Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;->a(Ljava/lang/reflect/Type;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 713
    invoke-virtual {p1, p0, p2}, Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    .line 715
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/gson/chase/GsonBuilder;
    .locals 1

    .prologue
    .line 278
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/gson/chase/GsonBuilder;->t:Z

    .line 279
    return-object p0
.end method

.method public final a(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/chase/GsonBuilder;
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 495
    instance-of v0, p2, Lcom/google/gson/chase/JsonSerializer;

    if-nez v0, :cond_0

    instance-of v0, p2, Lcom/google/gson/chase/JsonDeserializer;

    if-nez v0, :cond_0

    instance-of v0, p2, Lcom/google/gson/chase/InstanceCreator;

    if-nez v0, :cond_0

    instance-of v0, p2, Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/gson/chase/internal/$Gson$Preconditions;->a(Z)V

    invoke-static {p1}, Lcom/google/gson/chase/internal/Primitives;->a(Ljava/lang/reflect/Type;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p1}, Lcom/google/gson/chase/internal/Primitives;->b(Ljava/lang/reflect/Type;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot register type adapters for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    instance-of v0, p2, Lcom/google/gson/chase/InstanceCreator;

    if-eqz v0, :cond_4

    move-object v0, p2

    check-cast v0, Lcom/google/gson/chase/InstanceCreator;

    iget-object v1, p0, Lcom/google/gson/chase/GsonBuilder;->l:Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;

    invoke-virtual {v1, p1, v0}, Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    :cond_4
    instance-of v0, p2, Lcom/google/gson/chase/JsonSerializer;

    if-eqz v0, :cond_5

    move-object v0, p2

    check-cast v0, Lcom/google/gson/chase/JsonSerializer;

    iget-object v1, p0, Lcom/google/gson/chase/GsonBuilder;->m:Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;

    invoke-virtual {v1, p1, v0}, Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    :cond_5
    instance-of v0, p2, Lcom/google/gson/chase/JsonDeserializer;

    if-eqz v0, :cond_6

    move-object v0, p2

    check-cast v0, Lcom/google/gson/chase/JsonDeserializer;

    iget-object v1, p0, Lcom/google/gson/chase/GsonBuilder;->n:Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;

    new-instance v2, Lcom/google/gson/chase/JsonDeserializerExceptionWrapper;

    invoke-direct {v2, v0}, Lcom/google/gson/chase/JsonDeserializerExceptionWrapper;-><init>(Lcom/google/gson/chase/JsonDeserializer;)V

    invoke-virtual {v1, p1, v2}, Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)V

    :cond_6
    instance-of v0, p2, Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/gson/chase/GsonBuilder;->o:Ljava/util/List;

    check-cast p2, Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_7
    return-object p0
.end method

.method public final b()Lcom/google/gson/chase/Gson;
    .locals 15

    .prologue
    const/4 v10, 0x2

    .line 659
    new-instance v2, Ljava/util/LinkedList;

    iget-object v0, p0, Lcom/google/gson/chase/GsonBuilder;->e:Ljava/util/Set;

    invoke-direct {v2, v0}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    .line 661
    new-instance v3, Ljava/util/LinkedList;

    iget-object v0, p0, Lcom/google/gson/chase/GsonBuilder;->d:Ljava/util/Set;

    invoke-direct {v3, v0}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    .line 663
    iget-object v0, p0, Lcom/google/gson/chase/GsonBuilder;->g:Lcom/google/gson/chase/ModifierBasedExclusionStrategy;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 664
    iget-object v0, p0, Lcom/google/gson/chase/GsonBuilder;->g:Lcom/google/gson/chase/ModifierBasedExclusionStrategy;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 666
    iget-boolean v0, p0, Lcom/google/gson/chase/GsonBuilder;->h:Z

    if-nez v0, :cond_0

    .line 667
    sget-object v0, Lcom/google/gson/chase/GsonBuilder;->a:Lcom/google/gson/chase/InnerClassExclusionStrategy;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 668
    sget-object v0, Lcom/google/gson/chase/GsonBuilder;->a:Lcom/google/gson/chase/InnerClassExclusionStrategy;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 670
    :cond_0
    iget-wide v0, p0, Lcom/google/gson/chase/GsonBuilder;->f:D

    const-wide/high16 v4, -0x4010

    cmpl-double v0, v0, v4

    if-eqz v0, :cond_1

    .line 671
    new-instance v0, Lcom/google/gson/chase/VersionExclusionStrategy;

    iget-wide v4, p0, Lcom/google/gson/chase/GsonBuilder;->f:D

    invoke-direct {v0, v4, v5}, Lcom/google/gson/chase/VersionExclusionStrategy;-><init>(D)V

    .line 673
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 674
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 676
    :cond_1
    iget-boolean v0, p0, Lcom/google/gson/chase/GsonBuilder;->i:Z

    if-eqz v0, :cond_2

    .line 677
    sget-object v0, Lcom/google/gson/chase/GsonBuilder;->b:Lcom/google/gson/chase/ExposeAnnotationDeserializationExclusionStrategy;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 678
    sget-object v0, Lcom/google/gson/chase/GsonBuilder;->c:Lcom/google/gson/chase/ExposeAnnotationSerializationExclusionStrategy;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 680
    :cond_2
    iget-object v1, p0, Lcom/google/gson/chase/GsonBuilder;->q:Ljava/lang/String;

    iget v4, p0, Lcom/google/gson/chase/GsonBuilder;->r:I

    iget v5, p0, Lcom/google/gson/chase/GsonBuilder;->s:I

    iget-object v6, p0, Lcom/google/gson/chase/GsonBuilder;->m:Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;

    iget-object v7, p0, Lcom/google/gson/chase/GsonBuilder;->n:Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;

    const/4 v0, 0x0

    if-eqz v1, :cond_5

    const-string v8, ""

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_5

    new-instance v0, Lcom/google/gson/chase/DefaultTypeAdapters$DefaultDateTypeAdapter;

    invoke-direct {v0, v1}, Lcom/google/gson/chase/DefaultTypeAdapters$DefaultDateTypeAdapter;-><init>(Ljava/lang/String;)V

    :cond_3
    :goto_0
    if-eqz v0, :cond_4

    const-class v1, Ljava/util/Date;

    invoke-static {v1, v6, v0}, Lcom/google/gson/chase/GsonBuilder;->a(Ljava/lang/Class;Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;Ljava/lang/Object;)V

    const-class v1, Ljava/util/Date;

    invoke-static {v1, v7, v0}, Lcom/google/gson/chase/GsonBuilder;->a(Ljava/lang/Class;Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;Ljava/lang/Object;)V

    const-class v1, Ljava/sql/Timestamp;

    invoke-static {v1, v6, v0}, Lcom/google/gson/chase/GsonBuilder;->a(Ljava/lang/Class;Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;Ljava/lang/Object;)V

    const-class v1, Ljava/sql/Timestamp;

    invoke-static {v1, v7, v0}, Lcom/google/gson/chase/GsonBuilder;->a(Ljava/lang/Class;Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;Ljava/lang/Object;)V

    const-class v1, Ljava/sql/Date;

    invoke-static {v1, v6, v0}, Lcom/google/gson/chase/GsonBuilder;->a(Ljava/lang/Class;Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;Ljava/lang/Object;)V

    const-class v1, Ljava/sql/Date;

    invoke-static {v1, v7, v0}, Lcom/google/gson/chase/GsonBuilder;->a(Ljava/lang/Class;Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;Ljava/lang/Object;)V

    .line 682
    :cond_4
    new-instance v0, Lcom/google/gson/chase/Gson;

    new-instance v1, Lcom/google/gson/chase/DisjunctionExclusionStrategy;

    invoke-direct {v1, v2}, Lcom/google/gson/chase/DisjunctionExclusionStrategy;-><init>(Ljava/util/Collection;)V

    new-instance v2, Lcom/google/gson/chase/DisjunctionExclusionStrategy;

    invoke-direct {v2, v3}, Lcom/google/gson/chase/DisjunctionExclusionStrategy;-><init>(Ljava/util/Collection;)V

    iget-object v3, p0, Lcom/google/gson/chase/GsonBuilder;->k:Lcom/google/gson/chase/FieldNamingStrategy2;

    iget-object v4, p0, Lcom/google/gson/chase/GsonBuilder;->l:Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;

    invoke-virtual {v4}, Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;->b()Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;->a()Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;

    move-result-object v4

    iget-boolean v5, p0, Lcom/google/gson/chase/GsonBuilder;->p:Z

    iget-object v6, p0, Lcom/google/gson/chase/GsonBuilder;->m:Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;

    invoke-virtual {v6}, Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;->b()Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;->a()Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;

    move-result-object v6

    iget-object v7, p0, Lcom/google/gson/chase/GsonBuilder;->n:Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;

    invoke-virtual {v7}, Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;->b()Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;->a()Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;

    move-result-object v7

    iget-boolean v8, p0, Lcom/google/gson/chase/GsonBuilder;->t:Z

    iget-boolean v9, p0, Lcom/google/gson/chase/GsonBuilder;->x:Z

    iget-boolean v10, p0, Lcom/google/gson/chase/GsonBuilder;->v:Z

    iget-boolean v11, p0, Lcom/google/gson/chase/GsonBuilder;->w:Z

    iget-boolean v12, p0, Lcom/google/gson/chase/GsonBuilder;->u:Z

    iget-object v13, p0, Lcom/google/gson/chase/GsonBuilder;->j:Lcom/google/gson/chase/LongSerializationPolicy;

    iget-object v14, p0, Lcom/google/gson/chase/GsonBuilder;->o:Ljava/util/List;

    invoke-direct/range {v0 .. v14}, Lcom/google/gson/chase/Gson;-><init>(Lcom/google/gson/chase/ExclusionStrategy;Lcom/google/gson/chase/ExclusionStrategy;Lcom/google/gson/chase/FieldNamingStrategy2;Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;ZLcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;ZZZZZLcom/google/gson/chase/LongSerializationPolicy;Ljava/util/List;)V

    return-object v0

    .line 680
    :cond_5
    if-eq v4, v10, :cond_3

    if-eq v5, v10, :cond_3

    new-instance v0, Lcom/google/gson/chase/DefaultTypeAdapters$DefaultDateTypeAdapter;

    invoke-direct {v0, v4, v5}, Lcom/google/gson/chase/DefaultTypeAdapters$DefaultDateTypeAdapter;-><init>(II)V

    goto :goto_0
.end method
