.class Lcom/google/gson/chase/GsonToMiniGsonTypeAdapterFactory$3;
.super Lcom/google/gson/chase/internal/bind/TypeAdapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/chase/internal/bind/TypeAdapter",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/gson/chase/JsonDeserializer;

.field final synthetic b:Ljava/lang/reflect/Type;

.field final synthetic c:Lcom/google/gson/chase/JsonSerializer;

.field final synthetic d:Lcom/google/gson/chase/internal/bind/MiniGson;

.field final synthetic e:Lcom/google/gson/chase/reflect/TypeToken;

.field final synthetic f:Lcom/google/gson/chase/GsonToMiniGsonTypeAdapterFactory;

.field private g:Lcom/google/gson/chase/internal/bind/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/chase/internal/bind/TypeAdapter",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/google/gson/chase/GsonToMiniGsonTypeAdapterFactory;Lcom/google/gson/chase/JsonDeserializer;Ljava/lang/reflect/Type;Lcom/google/gson/chase/JsonSerializer;Lcom/google/gson/chase/internal/bind/MiniGson;Lcom/google/gson/chase/reflect/TypeToken;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 101
    iput-object p1, p0, Lcom/google/gson/chase/GsonToMiniGsonTypeAdapterFactory$3;->f:Lcom/google/gson/chase/GsonToMiniGsonTypeAdapterFactory;

    iput-object p2, p0, Lcom/google/gson/chase/GsonToMiniGsonTypeAdapterFactory$3;->a:Lcom/google/gson/chase/JsonDeserializer;

    iput-object p3, p0, Lcom/google/gson/chase/GsonToMiniGsonTypeAdapterFactory$3;->b:Ljava/lang/reflect/Type;

    iput-object p4, p0, Lcom/google/gson/chase/GsonToMiniGsonTypeAdapterFactory$3;->c:Lcom/google/gson/chase/JsonSerializer;

    iput-object p5, p0, Lcom/google/gson/chase/GsonToMiniGsonTypeAdapterFactory$3;->d:Lcom/google/gson/chase/internal/bind/MiniGson;

    iput-object p6, p0, Lcom/google/gson/chase/GsonToMiniGsonTypeAdapterFactory$3;->e:Lcom/google/gson/chase/reflect/TypeToken;

    invoke-direct {p0}, Lcom/google/gson/chase/internal/bind/TypeAdapter;-><init>()V

    return-void
.end method

.method private a()Lcom/google/gson/chase/internal/bind/TypeAdapter;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/gson/chase/internal/bind/TypeAdapter",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/gson/chase/GsonToMiniGsonTypeAdapterFactory$3;->g:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    .line 103
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/gson/chase/GsonToMiniGsonTypeAdapterFactory$3;->d:Lcom/google/gson/chase/internal/bind/MiniGson;

    iget-object v1, p0, Lcom/google/gson/chase/GsonToMiniGsonTypeAdapterFactory$3;->f:Lcom/google/gson/chase/GsonToMiniGsonTypeAdapterFactory;

    iget-object v2, p0, Lcom/google/gson/chase/GsonToMiniGsonTypeAdapterFactory$3;->e:Lcom/google/gson/chase/reflect/TypeToken;

    invoke-virtual {v0, v1, v2}, Lcom/google/gson/chase/internal/bind/MiniGson;->a(Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;Lcom/google/gson/chase/reflect/TypeToken;)Lcom/google/gson/chase/internal/bind/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/google/gson/chase/GsonToMiniGsonTypeAdapterFactory$3;->g:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/gson/chase/stream/JsonReader;)Ljava/lang/Object;
    .locals 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/chase/stream/JsonReader;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/gson/chase/GsonToMiniGsonTypeAdapterFactory$3;->a:Lcom/google/gson/chase/JsonDeserializer;

    if-nez v0, :cond_0

    .line 79
    invoke-direct {p0}, Lcom/google/gson/chase/GsonToMiniGsonTypeAdapterFactory$3;->a()Lcom/google/gson/chase/internal/bind/TypeAdapter;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/gson/chase/internal/bind/TypeAdapter;->a(Lcom/google/gson/chase/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v0

    .line 85
    :goto_0
    return-object v0

    .line 81
    :cond_0
    invoke-static {p1}, Lcom/google/gson/chase/internal/Streams;->a(Lcom/google/gson/chase/stream/JsonReader;)Lcom/google/gson/chase/JsonElement;

    move-result-object v0

    .line 82
    invoke-virtual {v0}, Lcom/google/gson/chase/JsonElement;->j()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 83
    const/4 v0, 0x0

    goto :goto_0

    .line 85
    :cond_1
    iget-object v1, p0, Lcom/google/gson/chase/GsonToMiniGsonTypeAdapterFactory$3;->a:Lcom/google/gson/chase/JsonDeserializer;

    iget-object v2, p0, Lcom/google/gson/chase/GsonToMiniGsonTypeAdapterFactory$3;->b:Ljava/lang/reflect/Type;

    iget-object v3, p0, Lcom/google/gson/chase/GsonToMiniGsonTypeAdapterFactory$3;->f:Lcom/google/gson/chase/GsonToMiniGsonTypeAdapterFactory;

    invoke-static {v3}, Lcom/google/gson/chase/GsonToMiniGsonTypeAdapterFactory;->a(Lcom/google/gson/chase/GsonToMiniGsonTypeAdapterFactory;)Lcom/google/gson/chase/JsonDeserializationContext;

    move-result-object v3

    invoke-interface {v1, v0, v2, v3}, Lcom/google/gson/chase/JsonDeserializer;->a(Lcom/google/gson/chase/JsonElement;Ljava/lang/reflect/Type;Lcom/google/gson/chase/JsonDeserializationContext;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/gson/chase/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/chase/stream/JsonWriter;",
            "TT;)V"
        }
    .end annotation

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/gson/chase/GsonToMiniGsonTypeAdapterFactory$3;->c:Lcom/google/gson/chase/JsonSerializer;

    if-nez v0, :cond_0

    .line 90
    invoke-direct {p0}, Lcom/google/gson/chase/GsonToMiniGsonTypeAdapterFactory$3;->a()Lcom/google/gson/chase/internal/bind/TypeAdapter;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/gson/chase/internal/bind/TypeAdapter;->a(Lcom/google/gson/chase/stream/JsonWriter;Ljava/lang/Object;)V

    .line 99
    :goto_0
    return-void

    .line 93
    :cond_0
    if-nez p2, :cond_1

    .line 94
    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonWriter;->f()Lcom/google/gson/chase/stream/JsonWriter;

    goto :goto_0

    .line 97
    :cond_1
    iget-object v0, p0, Lcom/google/gson/chase/GsonToMiniGsonTypeAdapterFactory$3;->c:Lcom/google/gson/chase/JsonSerializer;

    iget-object v1, p0, Lcom/google/gson/chase/GsonToMiniGsonTypeAdapterFactory$3;->b:Ljava/lang/reflect/Type;

    iget-object v1, p0, Lcom/google/gson/chase/GsonToMiniGsonTypeAdapterFactory$3;->f:Lcom/google/gson/chase/GsonToMiniGsonTypeAdapterFactory;

    invoke-static {v1}, Lcom/google/gson/chase/GsonToMiniGsonTypeAdapterFactory;->b(Lcom/google/gson/chase/GsonToMiniGsonTypeAdapterFactory;)Lcom/google/gson/chase/JsonSerializationContext;

    invoke-interface {v0, p2}, Lcom/google/gson/chase/JsonSerializer;->a(Ljava/lang/Object;)Lcom/google/gson/chase/JsonElement;

    move-result-object v0

    .line 98
    invoke-static {v0, p1}, Lcom/google/gson/chase/internal/Streams;->a(Lcom/google/gson/chase/JsonElement;Lcom/google/gson/chase/stream/JsonWriter;)V

    goto :goto_0
.end method
