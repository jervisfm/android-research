.class final Lcom/google/gson/chase/GsonToMiniGsonTypeAdapterFactory;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;


# instance fields
.field private final a:Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap",
            "<",
            "Lcom/google/gson/chase/JsonSerializer",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap",
            "<",
            "Lcom/google/gson/chase/JsonDeserializer",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final c:Lcom/google/gson/chase/JsonDeserializationContext;

.field private final d:Lcom/google/gson/chase/JsonSerializationContext;


# direct methods
.method public constructor <init>(Lcom/google/gson/chase/Gson;Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/chase/Gson;",
            "Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap",
            "<",
            "Lcom/google/gson/chase/JsonSerializer",
            "<*>;>;",
            "Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap",
            "<",
            "Lcom/google/gson/chase/JsonDeserializer",
            "<*>;>;)V"
        }
    .end annotation

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p2, p0, Lcom/google/gson/chase/GsonToMiniGsonTypeAdapterFactory;->a:Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;

    .line 38
    iput-object p3, p0, Lcom/google/gson/chase/GsonToMiniGsonTypeAdapterFactory;->b:Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;

    .line 40
    new-instance v0, Lcom/google/gson/chase/GsonToMiniGsonTypeAdapterFactory$1;

    invoke-direct {v0, p0, p1}, Lcom/google/gson/chase/GsonToMiniGsonTypeAdapterFactory$1;-><init>(Lcom/google/gson/chase/GsonToMiniGsonTypeAdapterFactory;Lcom/google/gson/chase/Gson;)V

    iput-object v0, p0, Lcom/google/gson/chase/GsonToMiniGsonTypeAdapterFactory;->c:Lcom/google/gson/chase/JsonDeserializationContext;

    .line 46
    new-instance v0, Lcom/google/gson/chase/GsonToMiniGsonTypeAdapterFactory$2;

    invoke-direct {v0, p0, p1}, Lcom/google/gson/chase/GsonToMiniGsonTypeAdapterFactory$2;-><init>(Lcom/google/gson/chase/GsonToMiniGsonTypeAdapterFactory;Lcom/google/gson/chase/Gson;)V

    iput-object v0, p0, Lcom/google/gson/chase/GsonToMiniGsonTypeAdapterFactory;->d:Lcom/google/gson/chase/JsonSerializationContext;

    .line 54
    return-void
.end method

.method static synthetic a(Lcom/google/gson/chase/GsonToMiniGsonTypeAdapterFactory;)Lcom/google/gson/chase/JsonDeserializationContext;
    .locals 1
    .parameter

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/gson/chase/GsonToMiniGsonTypeAdapterFactory;->c:Lcom/google/gson/chase/JsonDeserializationContext;

    return-object v0
.end method

.method static synthetic b(Lcom/google/gson/chase/GsonToMiniGsonTypeAdapterFactory;)Lcom/google/gson/chase/JsonSerializationContext;
    .locals 1
    .parameter

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/gson/chase/GsonToMiniGsonTypeAdapterFactory;->d:Lcom/google/gson/chase/JsonSerializationContext;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/gson/chase/internal/bind/MiniGson;Lcom/google/gson/chase/reflect/TypeToken;)Lcom/google/gson/chase/internal/bind/TypeAdapter;
    .locals 7
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/gson/chase/internal/bind/MiniGson;",
            "Lcom/google/gson/chase/reflect/TypeToken",
            "<TT;>;)",
            "Lcom/google/gson/chase/internal/bind/TypeAdapter",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 57
    invoke-virtual {p2}, Lcom/google/gson/chase/reflect/TypeToken;->b()Ljava/lang/reflect/Type;

    move-result-object v3

    .line 60
    iget-object v0, p0, Lcom/google/gson/chase/GsonToMiniGsonTypeAdapterFactory;->a:Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;

    invoke-virtual {v0, v3, v1}, Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;->a(Ljava/lang/reflect/Type;Z)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/gson/chase/JsonSerializer;

    .line 63
    iget-object v0, p0, Lcom/google/gson/chase/GsonToMiniGsonTypeAdapterFactory;->b:Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;

    invoke-virtual {v0, v3, v1}, Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;->a(Ljava/lang/reflect/Type;Z)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/gson/chase/JsonDeserializer;

    .line 66
    if-nez v4, :cond_0

    if-nez v2, :cond_0

    .line 67
    const/4 v0, 0x0

    .line 70
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/gson/chase/GsonToMiniGsonTypeAdapterFactory$3;

    move-object v1, p0

    move-object v5, p1

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/gson/chase/GsonToMiniGsonTypeAdapterFactory$3;-><init>(Lcom/google/gson/chase/GsonToMiniGsonTypeAdapterFactory;Lcom/google/gson/chase/JsonDeserializer;Ljava/lang/reflect/Type;Lcom/google/gson/chase/JsonSerializer;Lcom/google/gson/chase/internal/bind/MiniGson;Lcom/google/gson/chase/reflect/TypeToken;)V

    goto :goto_0
.end method
