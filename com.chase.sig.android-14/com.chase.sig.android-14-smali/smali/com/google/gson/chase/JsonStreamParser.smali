.class public final Lcom/google/gson/chase/JsonStreamParser;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Lcom/google/gson/chase/JsonElement;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/google/gson/chase/stream/JsonReader;

.field private final b:Ljava/lang/Object;


# direct methods
.method private a()Lcom/google/gson/chase/JsonElement;
    .locals 3

    .prologue
    .line 83
    invoke-virtual {p0}, Lcom/google/gson/chase/JsonStreamParser;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 84
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 88
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/gson/chase/JsonStreamParser;->a:Lcom/google/gson/chase/stream/JsonReader;

    invoke-static {v0}, Lcom/google/gson/chase/internal/Streams;->a(Lcom/google/gson/chase/stream/JsonReader;)Lcom/google/gson/chase/JsonElement;
    :try_end_0
    .catch Ljava/lang/StackOverflowError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/gson/chase/JsonParseException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    return-object v0

    .line 89
    :catch_0
    move-exception v0

    .line 90
    new-instance v1, Lcom/google/gson/chase/JsonParseException;

    const-string v2, "Failed parsing JSON source to Json"

    invoke-direct {v1, v2, v0}, Lcom/google/gson/chase/JsonParseException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 91
    :catch_1
    move-exception v0

    .line 92
    new-instance v1, Lcom/google/gson/chase/JsonParseException;

    const-string v2, "Failed parsing JSON source to Json"

    invoke-direct {v1, v2, v0}, Lcom/google/gson/chase/JsonParseException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 93
    :catch_2
    move-exception v0

    .line 94
    invoke-virtual {v0}, Lcom/google/gson/chase/JsonParseException;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    instance-of v1, v1, Ljava/io/EOFException;

    if-eqz v1, :cond_1

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    :cond_1
    throw v0
.end method


# virtual methods
.method public final hasNext()Z
    .locals 3

    .prologue
    .line 104
    iget-object v1, p0, Lcom/google/gson/chase/JsonStreamParser;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 106
    :try_start_0
    iget-object v0, p0, Lcom/google/gson/chase/JsonStreamParser;->a:Lcom/google/gson/chase/stream/JsonReader;

    invoke-virtual {v0}, Lcom/google/gson/chase/stream/JsonReader;->f()Lcom/google/gson/chase/stream/JsonToken;

    move-result-object v0

    sget-object v2, Lcom/google/gson/chase/stream/JsonToken;->j:Lcom/google/gson/chase/stream/JsonToken;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Lcom/google/gson/chase/stream/MalformedJsonException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    if-eq v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    :try_start_1
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 107
    :catch_0
    move-exception v0

    .line 108
    new-instance v2, Lcom/google/gson/chase/JsonSyntaxException;

    invoke-direct {v2, v0}, Lcom/google/gson/chase/JsonSyntaxException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    .line 112
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 109
    :catch_1
    move-exception v0

    .line 110
    :try_start_2
    new-instance v2, Lcom/google/gson/chase/JsonIOException;

    invoke-direct {v2, v0}, Lcom/google/gson/chase/JsonIOException;-><init>(Ljava/lang/Throwable;)V

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method

.method public final synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/google/gson/chase/JsonStreamParser;->a()Lcom/google/gson/chase/JsonElement;

    move-result-object v0

    return-object v0
.end method

.method public final remove()V
    .locals 1

    .prologue
    .line 121
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
