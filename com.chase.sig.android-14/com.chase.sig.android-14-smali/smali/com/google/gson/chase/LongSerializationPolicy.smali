.class public final enum Lcom/google/gson/chase/LongSerializationPolicy;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/gson/chase/LongSerializationPolicy$1;,
        Lcom/google/gson/chase/LongSerializationPolicy$StringStrategy;,
        Lcom/google/gson/chase/LongSerializationPolicy$DefaultStrategy;,
        Lcom/google/gson/chase/LongSerializationPolicy$Strategy;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/gson/chase/LongSerializationPolicy;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/gson/chase/LongSerializationPolicy;

.field public static final enum b:Lcom/google/gson/chase/LongSerializationPolicy;

.field private static final synthetic d:[Lcom/google/gson/chase/LongSerializationPolicy;


# instance fields
.field private final c:Lcom/google/gson/chase/LongSerializationPolicy$Strategy;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 34
    new-instance v0, Lcom/google/gson/chase/LongSerializationPolicy;

    const-string v1, "DEFAULT"

    new-instance v2, Lcom/google/gson/chase/LongSerializationPolicy$DefaultStrategy;

    invoke-direct {v2, v3}, Lcom/google/gson/chase/LongSerializationPolicy$DefaultStrategy;-><init>(B)V

    invoke-direct {v0, v1, v3, v2}, Lcom/google/gson/chase/LongSerializationPolicy;-><init>(Ljava/lang/String;ILcom/google/gson/chase/LongSerializationPolicy$Strategy;)V

    sput-object v0, Lcom/google/gson/chase/LongSerializationPolicy;->a:Lcom/google/gson/chase/LongSerializationPolicy;

    .line 41
    new-instance v0, Lcom/google/gson/chase/LongSerializationPolicy;

    const-string v1, "STRING"

    new-instance v2, Lcom/google/gson/chase/LongSerializationPolicy$StringStrategy;

    invoke-direct {v2, v3}, Lcom/google/gson/chase/LongSerializationPolicy$StringStrategy;-><init>(B)V

    invoke-direct {v0, v1, v4, v2}, Lcom/google/gson/chase/LongSerializationPolicy;-><init>(Ljava/lang/String;ILcom/google/gson/chase/LongSerializationPolicy$Strategy;)V

    sput-object v0, Lcom/google/gson/chase/LongSerializationPolicy;->b:Lcom/google/gson/chase/LongSerializationPolicy;

    .line 27
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/gson/chase/LongSerializationPolicy;

    sget-object v1, Lcom/google/gson/chase/LongSerializationPolicy;->a:Lcom/google/gson/chase/LongSerializationPolicy;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/gson/chase/LongSerializationPolicy;->b:Lcom/google/gson/chase/LongSerializationPolicy;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/gson/chase/LongSerializationPolicy;->d:[Lcom/google/gson/chase/LongSerializationPolicy;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/google/gson/chase/LongSerializationPolicy$Strategy;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/chase/LongSerializationPolicy$Strategy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 46
    iput-object p3, p0, Lcom/google/gson/chase/LongSerializationPolicy;->c:Lcom/google/gson/chase/LongSerializationPolicy$Strategy;

    .line 47
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/gson/chase/LongSerializationPolicy;
    .locals 1
    .parameter

    .prologue
    .line 27
    const-class v0, Lcom/google/gson/chase/LongSerializationPolicy;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/gson/chase/LongSerializationPolicy;

    return-object v0
.end method

.method public static final values()[Lcom/google/gson/chase/LongSerializationPolicy;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/google/gson/chase/LongSerializationPolicy;->d:[Lcom/google/gson/chase/LongSerializationPolicy;

    invoke-virtual {v0}, [Lcom/google/gson/chase/LongSerializationPolicy;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/gson/chase/LongSerializationPolicy;

    return-object v0
.end method
