.class final Lcom/google/gson/chase/SerializedNameAnnotationInterceptingNamingPolicy;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/gson/chase/FieldNamingStrategy2;


# instance fields
.field private final a:Lcom/google/gson/chase/FieldNamingStrategy2;


# direct methods
.method constructor <init>(Lcom/google/gson/chase/FieldNamingStrategy2;)V
    .locals 0
    .parameter

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/google/gson/chase/SerializedNameAnnotationInterceptingNamingPolicy;->a:Lcom/google/gson/chase/FieldNamingStrategy2;

    .line 42
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/gson/chase/FieldAttributes;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 45
    const-class v0, Lcom/google/gson/chase/annotations/SerializedName;

    invoke-virtual {p1, v0}, Lcom/google/gson/chase/FieldAttributes;->a(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/google/gson/chase/annotations/SerializedName;

    .line 46
    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/gson/chase/SerializedNameAnnotationInterceptingNamingPolicy;->a:Lcom/google/gson/chase/FieldNamingStrategy2;

    invoke-interface {v0, p1}, Lcom/google/gson/chase/FieldNamingStrategy2;->a(Lcom/google/gson/chase/FieldAttributes;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v0}, Lcom/google/gson/chase/annotations/SerializedName;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
