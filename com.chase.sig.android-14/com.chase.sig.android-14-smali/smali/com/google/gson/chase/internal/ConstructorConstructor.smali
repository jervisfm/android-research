.class public final Lcom/google/gson/chase/internal/ConstructorConstructor;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap",
            "<",
            "Lcom/google/gson/chase/InstanceCreator",
            "<*>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 46
    new-instance v0, Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;

    invoke-direct {v0}, Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;-><init>()V

    invoke-direct {p0, v0}, Lcom/google/gson/chase/internal/ConstructorConstructor;-><init>(Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;)V

    .line 47
    return-void
.end method

.method public constructor <init>(Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap",
            "<",
            "Lcom/google/gson/chase/InstanceCreator",
            "<*>;>;)V"
        }
    .end annotation

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/google/gson/chase/internal/ConstructorConstructor;->a:Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;

    .line 43
    return-void
.end method

.method private a(Ljava/lang/Class;)Lcom/google/gson/chase/internal/ObjectConstructor;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<-TT;>;)",
            "Lcom/google/gson/chase/internal/ObjectConstructor",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 82
    const/4 v0, 0x0

    :try_start_0
    new-array v0, v0, [Ljava/lang/Class;

    invoke-virtual {p1, v0}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v1

    .line 83
    invoke-virtual {v1}, Ljava/lang/reflect/Constructor;->isAccessible()Z

    move-result v0

    if-nez v0, :cond_0

    .line 84
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Ljava/lang/reflect/Constructor;->setAccessible(Z)V

    .line 86
    :cond_0
    new-instance v0, Lcom/google/gson/chase/internal/ConstructorConstructor$2;

    invoke-direct {v0, p0, v1}, Lcom/google/gson/chase/internal/ConstructorConstructor$2;-><init>(Lcom/google/gson/chase/internal/ConstructorConstructor;Ljava/lang/reflect/Constructor;)V
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    .line 106
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/gson/chase/reflect/TypeToken;)Lcom/google/gson/chase/internal/ObjectConstructor;
    .locals 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/gson/chase/reflect/TypeToken",
            "<TT;>;)",
            "Lcom/google/gson/chase/internal/ObjectConstructor",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 50
    invoke-virtual {p1}, Lcom/google/gson/chase/reflect/TypeToken;->b()Ljava/lang/reflect/Type;

    move-result-object v2

    .line 51
    invoke-virtual {p1}, Lcom/google/gson/chase/reflect/TypeToken;->a()Ljava/lang/Class;

    move-result-object v1

    .line 56
    iget-object v0, p0, Lcom/google/gson/chase/internal/ConstructorConstructor;->a:Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;->a(Ljava/lang/reflect/Type;Z)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/gson/chase/InstanceCreator;

    .line 58
    if-eqz v0, :cond_1

    .line 59
    new-instance v1, Lcom/google/gson/chase/internal/ConstructorConstructor$1;

    invoke-direct {v1, p0, v0, v2}, Lcom/google/gson/chase/internal/ConstructorConstructor$1;-><init>(Lcom/google/gson/chase/internal/ConstructorConstructor;Lcom/google/gson/chase/InstanceCreator;Ljava/lang/reflect/Type;)V

    move-object v0, v1

    .line 77
    :cond_0
    :goto_0
    return-object v0

    .line 66
    :cond_1
    invoke-direct {p0, v1}, Lcom/google/gson/chase/internal/ConstructorConstructor;->a(Ljava/lang/Class;)Lcom/google/gson/chase/internal/ObjectConstructor;

    move-result-object v0

    .line 67
    if-nez v0, :cond_0

    .line 71
    const-class v0, Ljava/util/Collection;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-class v0, Ljava/util/SortedSet;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Lcom/google/gson/chase/internal/ConstructorConstructor$3;

    invoke-direct {v0, p0}, Lcom/google/gson/chase/internal/ConstructorConstructor$3;-><init>(Lcom/google/gson/chase/internal/ConstructorConstructor;)V

    .line 72
    :goto_1
    if-nez v0, :cond_0

    .line 77
    new-instance v0, Lcom/google/gson/chase/internal/ConstructorConstructor$8;

    invoke-direct {v0, p0, v1, v2}, Lcom/google/gson/chase/internal/ConstructorConstructor$8;-><init>(Lcom/google/gson/chase/internal/ConstructorConstructor;Ljava/lang/Class;Ljava/lang/reflect/Type;)V

    goto :goto_0

    .line 71
    :cond_2
    const-class v0, Ljava/util/Set;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Lcom/google/gson/chase/internal/ConstructorConstructor$4;

    invoke-direct {v0, p0}, Lcom/google/gson/chase/internal/ConstructorConstructor$4;-><init>(Lcom/google/gson/chase/internal/ConstructorConstructor;)V

    goto :goto_1

    :cond_3
    const-class v0, Ljava/util/Queue;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v0, Lcom/google/gson/chase/internal/ConstructorConstructor$5;

    invoke-direct {v0, p0}, Lcom/google/gson/chase/internal/ConstructorConstructor$5;-><init>(Lcom/google/gson/chase/internal/ConstructorConstructor;)V

    goto :goto_1

    :cond_4
    new-instance v0, Lcom/google/gson/chase/internal/ConstructorConstructor$6;

    invoke-direct {v0, p0}, Lcom/google/gson/chase/internal/ConstructorConstructor$6;-><init>(Lcom/google/gson/chase/internal/ConstructorConstructor;)V

    goto :goto_1

    :cond_5
    const-class v0, Ljava/util/Map;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_6

    new-instance v0, Lcom/google/gson/chase/internal/ConstructorConstructor$7;

    invoke-direct {v0, p0}, Lcom/google/gson/chase/internal/ConstructorConstructor$7;-><init>(Lcom/google/gson/chase/internal/ConstructorConstructor;)V

    goto :goto_1

    :cond_6
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/gson/chase/internal/ConstructorConstructor;->a:Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;

    invoke-virtual {v0}, Lcom/google/gson/chase/internal/ParameterizedTypeHandlerMap;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
