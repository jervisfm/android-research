.class public final Lcom/google/gson/chase/internal/Streams;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/gson/chase/internal/Streams$1;,
        Lcom/google/gson/chase/internal/Streams$AppendableWriter;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    return-void
.end method

.method public static a(Lcom/google/gson/chase/stream/JsonReader;)Lcom/google/gson/chase/JsonElement;
    .locals 2
    .parameter

    .prologue
    .line 40
    const/4 v1, 0x1

    .line 42
    :try_start_0
    invoke-virtual {p0}, Lcom/google/gson/chase/stream/JsonReader;->f()Lcom/google/gson/chase/stream/JsonToken;

    .line 43
    const/4 v1, 0x0

    .line 44
    sget-object v0, Lcom/google/gson/chase/internal/bind/TypeAdapters;->O:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    invoke-virtual {v0, p0}, Lcom/google/gson/chase/internal/bind/TypeAdapter;->a(Lcom/google/gson/chase/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/gson/chase/JsonElement;
    :try_end_0
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/gson/chase/stream/MalformedJsonException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_3

    .line 51
    :goto_0
    return-object v0

    .line 45
    :catch_0
    move-exception v0

    .line 50
    if-eqz v1, :cond_0

    .line 51
    sget-object v0, Lcom/google/gson/chase/JsonNull;->a:Lcom/google/gson/chase/JsonNull;

    goto :goto_0

    .line 53
    :cond_0
    new-instance v1, Lcom/google/gson/chase/JsonIOException;

    invoke-direct {v1, v0}, Lcom/google/gson/chase/JsonIOException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 54
    :catch_1
    move-exception v0

    .line 55
    new-instance v1, Lcom/google/gson/chase/JsonSyntaxException;

    invoke-direct {v1, v0}, Lcom/google/gson/chase/JsonSyntaxException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 56
    :catch_2
    move-exception v0

    .line 57
    new-instance v1, Lcom/google/gson/chase/JsonIOException;

    invoke-direct {v1, v0}, Lcom/google/gson/chase/JsonIOException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 58
    :catch_3
    move-exception v0

    .line 59
    new-instance v1, Lcom/google/gson/chase/JsonSyntaxException;

    invoke-direct {v1, v0}, Lcom/google/gson/chase/JsonSyntaxException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static a(Ljava/lang/Appendable;)Ljava/io/Writer;
    .locals 2
    .parameter

    .prologue
    .line 71
    instance-of v0, p0, Ljava/io/Writer;

    if-eqz v0, :cond_0

    check-cast p0, Ljava/io/Writer;

    :goto_0
    return-object p0

    :cond_0
    new-instance v0, Lcom/google/gson/chase/internal/Streams$AppendableWriter;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/gson/chase/internal/Streams$AppendableWriter;-><init>(Ljava/lang/Appendable;B)V

    move-object p0, v0

    goto :goto_0
.end method

.method public static a(Lcom/google/gson/chase/JsonElement;Lcom/google/gson/chase/stream/JsonWriter;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 67
    sget-object v0, Lcom/google/gson/chase/internal/bind/TypeAdapters;->O:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    invoke-virtual {v0, p1, p0}, Lcom/google/gson/chase/internal/bind/TypeAdapter;->a(Lcom/google/gson/chase/stream/JsonWriter;Ljava/lang/Object;)V

    .line 68
    return-void
.end method
