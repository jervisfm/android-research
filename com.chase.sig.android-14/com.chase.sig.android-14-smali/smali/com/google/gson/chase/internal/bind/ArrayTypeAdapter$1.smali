.class Lcom/google/gson/chase/internal/bind/ArrayTypeAdapter$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/gson/chase/internal/bind/MiniGson;Lcom/google/gson/chase/reflect/TypeToken;)Lcom/google/gson/chase/internal/bind/TypeAdapter;
    .locals 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/gson/chase/internal/bind/MiniGson;",
            "Lcom/google/gson/chase/reflect/TypeToken",
            "<TT;>;)",
            "Lcom/google/gson/chase/internal/bind/TypeAdapter",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 39
    invoke-virtual {p2}, Lcom/google/gson/chase/reflect/TypeToken;->b()Ljava/lang/reflect/Type;

    move-result-object v1

    .line 40
    instance-of v0, v1, Ljava/lang/reflect/GenericArrayType;

    if-nez v0, :cond_1

    instance-of v0, v1, Ljava/lang/Class;

    if-eqz v0, :cond_0

    move-object v0, v1

    check-cast v0, Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->isArray()Z

    move-result v0

    if-nez v0, :cond_1

    .line 41
    :cond_0
    const/4 v0, 0x0

    .line 49
    :goto_0
    return-object v0

    .line 44
    :cond_1
    invoke-static {v1}, Lcom/google/gson/chase/internal/$Gson$Types;->d(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v1

    .line 45
    invoke-static {v1}, Lcom/google/gson/chase/reflect/TypeToken;->a(Ljava/lang/reflect/Type;)Lcom/google/gson/chase/reflect/TypeToken;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/gson/chase/internal/bind/MiniGson;->a(Lcom/google/gson/chase/reflect/TypeToken;)Lcom/google/gson/chase/internal/bind/TypeAdapter;

    move-result-object v2

    .line 47
    new-instance v0, Lcom/google/gson/chase/internal/bind/ArrayTypeAdapter;

    invoke-static {v1}, Lcom/google/gson/chase/internal/$Gson$Types;->b(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, p1, v2, v1}, Lcom/google/gson/chase/internal/bind/ArrayTypeAdapter;-><init>(Lcom/google/gson/chase/internal/bind/MiniGson;Lcom/google/gson/chase/internal/bind/TypeAdapter;Ljava/lang/Class;)V

    goto :goto_0
.end method
