.class final Lcom/google/gson/chase/internal/bind/CollectionTypeAdapterFactory$Adapter;
.super Lcom/google/gson/chase/internal/bind/TypeAdapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/gson/chase/internal/bind/CollectionTypeAdapterFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "Adapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/gson/chase/internal/bind/TypeAdapter",
        "<",
        "Ljava/util/Collection",
        "<TE;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/gson/chase/internal/bind/CollectionTypeAdapterFactory;

.field private final b:Lcom/google/gson/chase/internal/bind/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/chase/internal/bind/TypeAdapter",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final c:Lcom/google/gson/chase/internal/ObjectConstructor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/chase/internal/ObjectConstructor",
            "<+",
            "Ljava/util/Collection",
            "<TE;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/gson/chase/internal/bind/CollectionTypeAdapterFactory;Lcom/google/gson/chase/internal/bind/MiniGson;Ljava/lang/reflect/Type;Lcom/google/gson/chase/internal/bind/TypeAdapter;Lcom/google/gson/chase/internal/ObjectConstructor;)V
    .locals 1
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/chase/internal/bind/MiniGson;",
            "Ljava/lang/reflect/Type;",
            "Lcom/google/gson/chase/internal/bind/TypeAdapter",
            "<TE;>;",
            "Lcom/google/gson/chase/internal/ObjectConstructor",
            "<+",
            "Ljava/util/Collection",
            "<TE;>;>;)V"
        }
    .end annotation

    .prologue
    .line 63
    iput-object p1, p0, Lcom/google/gson/chase/internal/bind/CollectionTypeAdapterFactory$Adapter;->a:Lcom/google/gson/chase/internal/bind/CollectionTypeAdapterFactory;

    invoke-direct {p0}, Lcom/google/gson/chase/internal/bind/TypeAdapter;-><init>()V

    .line 64
    new-instance v0, Lcom/google/gson/chase/internal/bind/TypeAdapterRuntimeTypeWrapper;

    invoke-direct {v0, p2, p4, p3}, Lcom/google/gson/chase/internal/bind/TypeAdapterRuntimeTypeWrapper;-><init>(Lcom/google/gson/chase/internal/bind/MiniGson;Lcom/google/gson/chase/internal/bind/TypeAdapter;Ljava/lang/reflect/Type;)V

    iput-object v0, p0, Lcom/google/gson/chase/internal/bind/CollectionTypeAdapterFactory$Adapter;->b:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    .line 66
    iput-object p5, p0, Lcom/google/gson/chase/internal/bind/CollectionTypeAdapterFactory$Adapter;->c:Lcom/google/gson/chase/internal/ObjectConstructor;

    .line 67
    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/gson/chase/stream/JsonReader;)Ljava/lang/Object;
    .locals 2
    .parameter

    .prologue
    .line 57
    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonReader;->f()Lcom/google/gson/chase/stream/JsonToken;

    move-result-object v0

    sget-object v1, Lcom/google/gson/chase/stream/JsonToken;->i:Lcom/google/gson/chase/stream/JsonToken;

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonReader;->j()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/gson/chase/internal/bind/CollectionTypeAdapterFactory$Adapter;->c:Lcom/google/gson/chase/internal/ObjectConstructor;

    invoke-interface {v0}, Lcom/google/gson/chase/internal/ObjectConstructor;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonReader;->a()V

    :goto_1
    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonReader;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/gson/chase/internal/bind/CollectionTypeAdapterFactory$Adapter;->b:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    invoke-virtual {v1, p1}, Lcom/google/gson/chase/internal/bind/TypeAdapter;->a(Lcom/google/gson/chase/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonReader;->b()V

    goto :goto_0
.end method

.method public final synthetic a(Lcom/google/gson/chase/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 57
    check-cast p2, Ljava/util/Collection;

    if-nez p2, :cond_0

    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonWriter;->f()Lcom/google/gson/chase/stream/JsonWriter;

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonWriter;->b()Lcom/google/gson/chase/stream/JsonWriter;

    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Lcom/google/gson/chase/internal/bind/CollectionTypeAdapterFactory$Adapter;->b:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    invoke-virtual {v2, p1, v1}, Lcom/google/gson/chase/internal/bind/TypeAdapter;->a(Lcom/google/gson/chase/stream/JsonWriter;Ljava/lang/Object;)V

    goto :goto_1

    :cond_1
    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonWriter;->c()Lcom/google/gson/chase/stream/JsonWriter;

    goto :goto_0
.end method
