.class public final Lcom/google/gson/chase/internal/bind/CollectionTypeAdapterFactory;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/gson/chase/internal/bind/CollectionTypeAdapterFactory$Adapter;
    }
.end annotation


# instance fields
.field private final a:Lcom/google/gson/chase/internal/ConstructorConstructor;


# direct methods
.method public constructor <init>(Lcom/google/gson/chase/internal/ConstructorConstructor;)V
    .locals 0
    .parameter

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/google/gson/chase/internal/bind/CollectionTypeAdapterFactory;->a:Lcom/google/gson/chase/internal/ConstructorConstructor;

    .line 38
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/gson/chase/internal/bind/MiniGson;Lcom/google/gson/chase/reflect/TypeToken;)Lcom/google/gson/chase/internal/bind/TypeAdapter;
    .locals 6
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/gson/chase/internal/bind/MiniGson;",
            "Lcom/google/gson/chase/reflect/TypeToken",
            "<TT;>;)",
            "Lcom/google/gson/chase/internal/bind/TypeAdapter",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 41
    invoke-virtual {p2}, Lcom/google/gson/chase/reflect/TypeToken;->b()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 43
    invoke-virtual {p2}, Lcom/google/gson/chase/reflect/TypeToken;->a()Ljava/lang/Class;

    move-result-object v1

    .line 44
    const-class v2, Ljava/util/Collection;

    invoke-virtual {v2, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 45
    const/4 v0, 0x0

    .line 54
    :goto_0
    return-object v0

    .line 48
    :cond_0
    invoke-static {v0, v1}, Lcom/google/gson/chase/internal/$Gson$Types;->a(Ljava/lang/reflect/Type;Ljava/lang/Class;)Ljava/lang/reflect/Type;

    move-result-object v3

    .line 49
    invoke-static {v3}, Lcom/google/gson/chase/reflect/TypeToken;->a(Ljava/lang/reflect/Type;)Lcom/google/gson/chase/reflect/TypeToken;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/gson/chase/internal/bind/MiniGson;->a(Lcom/google/gson/chase/reflect/TypeToken;)Lcom/google/gson/chase/internal/bind/TypeAdapter;

    move-result-object v4

    .line 50
    iget-object v0, p0, Lcom/google/gson/chase/internal/bind/CollectionTypeAdapterFactory;->a:Lcom/google/gson/chase/internal/ConstructorConstructor;

    invoke-virtual {v0, p2}, Lcom/google/gson/chase/internal/ConstructorConstructor;->a(Lcom/google/gson/chase/reflect/TypeToken;)Lcom/google/gson/chase/internal/ObjectConstructor;

    move-result-object v5

    .line 53
    new-instance v0, Lcom/google/gson/chase/internal/bind/CollectionTypeAdapterFactory$Adapter;

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/gson/chase/internal/bind/CollectionTypeAdapterFactory$Adapter;-><init>(Lcom/google/gson/chase/internal/bind/CollectionTypeAdapterFactory;Lcom/google/gson/chase/internal/bind/MiniGson;Ljava/lang/reflect/Type;Lcom/google/gson/chase/internal/bind/TypeAdapter;Lcom/google/gson/chase/internal/ObjectConstructor;)V

    goto :goto_0
.end method
