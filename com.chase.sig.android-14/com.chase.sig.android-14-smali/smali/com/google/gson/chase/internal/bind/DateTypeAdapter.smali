.class public final Lcom/google/gson/chase/internal/bind/DateTypeAdapter;
.super Lcom/google/gson/chase/internal/bind/TypeAdapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/chase/internal/bind/TypeAdapter",
        "<",
        "Ljava/util/Date;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;


# instance fields
.field private final b:Ljava/text/DateFormat;

.field private final c:Ljava/text/DateFormat;

.field private final d:Ljava/text/DateFormat;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    new-instance v0, Lcom/google/gson/chase/internal/bind/DateTypeAdapter$1;

    invoke-direct {v0}, Lcom/google/gson/chase/internal/bind/DateTypeAdapter$1;-><init>()V

    sput-object v0, Lcom/google/gson/chase/internal/bind/DateTypeAdapter;->a:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v1, 0x2

    .line 38
    invoke-direct {p0}, Lcom/google/gson/chase/internal/bind/TypeAdapter;-><init>()V

    .line 46
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {v1, v1, v0}, Ljava/text/DateFormat;->getDateTimeInstance(IILjava/util/Locale;)Ljava/text/DateFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/google/gson/chase/internal/bind/DateTypeAdapter;->b:Ljava/text/DateFormat;

    .line 48
    invoke-static {v1, v1}, Ljava/text/DateFormat;->getDateTimeInstance(II)Ljava/text/DateFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/google/gson/chase/internal/bind/DateTypeAdapter;->c:Ljava/text/DateFormat;

    .line 50
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MM-dd\'T\'HH:mm:ss\'Z\'"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    const-string v1, "UTC"

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    iput-object v0, p0, Lcom/google/gson/chase/internal/bind/DateTypeAdapter;->d:Ljava/text/DateFormat;

    return-void
.end method

.method private declared-synchronized a(Ljava/lang/String;)Ljava/util/Date;
    .locals 2
    .parameter

    .prologue
    .line 68
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/gson/chase/internal/bind/DateTypeAdapter;->c:Ljava/text/DateFormat;

    invoke-virtual {v0, p1}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 76
    :goto_0
    monitor-exit p0

    return-object v0

    :catch_0
    move-exception v0

    .line 72
    :try_start_1
    iget-object v0, p0, Lcom/google/gson/chase/internal/bind/DateTypeAdapter;->b:Ljava/text/DateFormat;

    invoke-virtual {v0, p1}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0
    .catch Ljava/text/ParseException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    goto :goto_0

    :catch_1
    move-exception v0

    .line 76
    :try_start_2
    iget-object v0, p0, Lcom/google/gson/chase/internal/bind/DateTypeAdapter;->d:Ljava/text/DateFormat;

    invoke-virtual {v0, p1}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
    .catch Ljava/text/ParseException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v0

    goto :goto_0

    .line 77
    :catch_2
    move-exception v0

    .line 78
    :try_start_3
    new-instance v1, Lcom/google/gson/chase/JsonSyntaxException;

    invoke-direct {v1, p1, v0}, Lcom/google/gson/chase/JsonSyntaxException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 68
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(Lcom/google/gson/chase/stream/JsonWriter;Ljava/util/Date;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 83
    monitor-enter p0

    if-nez p2, :cond_0

    .line 84
    :try_start_0
    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonWriter;->f()Lcom/google/gson/chase/stream/JsonWriter;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 89
    :goto_0
    monitor-exit p0

    return-void

    .line 87
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/gson/chase/internal/bind/DateTypeAdapter;->b:Ljava/text/DateFormat;

    invoke-virtual {v0, p2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 88
    invoke-virtual {p1, v0}, Lcom/google/gson/chase/stream/JsonWriter;->b(Ljava/lang/String;)Lcom/google/gson/chase/stream/JsonWriter;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 83
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final synthetic a(Lcom/google/gson/chase/stream/JsonReader;)Ljava/lang/Object;
    .locals 2
    .parameter

    .prologue
    .line 38
    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonReader;->f()Lcom/google/gson/chase/stream/JsonToken;

    move-result-object v0

    sget-object v1, Lcom/google/gson/chase/stream/JsonToken;->i:Lcom/google/gson/chase/stream/JsonToken;

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonReader;->j()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonReader;->h()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/gson/chase/internal/bind/DateTypeAdapter;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/gson/chase/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 38
    check-cast p2, Ljava/util/Date;

    invoke-direct {p0, p1, p2}, Lcom/google/gson/chase/internal/bind/DateTypeAdapter;->a(Lcom/google/gson/chase/stream/JsonWriter;Ljava/util/Date;)V

    return-void
.end method
