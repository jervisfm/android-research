.class Lcom/google/gson/chase/internal/bind/ExcludedTypeAdapterFactory$1;
.super Lcom/google/gson/chase/internal/bind/TypeAdapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/chase/internal/bind/TypeAdapter",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Z

.field final synthetic b:Z

.field final synthetic c:Lcom/google/gson/chase/internal/bind/MiniGson;

.field final synthetic d:Lcom/google/gson/chase/reflect/TypeToken;

.field final synthetic e:Lcom/google/gson/chase/internal/bind/ExcludedTypeAdapterFactory;

.field private f:Lcom/google/gson/chase/internal/bind/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/chase/internal/bind/TypeAdapter",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/google/gson/chase/internal/bind/ExcludedTypeAdapterFactory;ZZLcom/google/gson/chase/internal/bind/MiniGson;Lcom/google/gson/chase/reflect/TypeToken;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 71
    iput-object p1, p0, Lcom/google/gson/chase/internal/bind/ExcludedTypeAdapterFactory$1;->e:Lcom/google/gson/chase/internal/bind/ExcludedTypeAdapterFactory;

    iput-boolean p2, p0, Lcom/google/gson/chase/internal/bind/ExcludedTypeAdapterFactory$1;->a:Z

    iput-boolean p3, p0, Lcom/google/gson/chase/internal/bind/ExcludedTypeAdapterFactory$1;->b:Z

    iput-object p4, p0, Lcom/google/gson/chase/internal/bind/ExcludedTypeAdapterFactory$1;->c:Lcom/google/gson/chase/internal/bind/MiniGson;

    iput-object p5, p0, Lcom/google/gson/chase/internal/bind/ExcludedTypeAdapterFactory$1;->d:Lcom/google/gson/chase/reflect/TypeToken;

    invoke-direct {p0}, Lcom/google/gson/chase/internal/bind/TypeAdapter;-><init>()V

    return-void
.end method

.method private a()Lcom/google/gson/chase/internal/bind/TypeAdapter;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/gson/chase/internal/bind/TypeAdapter",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/gson/chase/internal/bind/ExcludedTypeAdapterFactory$1;->f:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    .line 73
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/gson/chase/internal/bind/ExcludedTypeAdapterFactory$1;->c:Lcom/google/gson/chase/internal/bind/MiniGson;

    iget-object v1, p0, Lcom/google/gson/chase/internal/bind/ExcludedTypeAdapterFactory$1;->e:Lcom/google/gson/chase/internal/bind/ExcludedTypeAdapterFactory;

    iget-object v2, p0, Lcom/google/gson/chase/internal/bind/ExcludedTypeAdapterFactory$1;->d:Lcom/google/gson/chase/reflect/TypeToken;

    invoke-virtual {v0, v1, v2}, Lcom/google/gson/chase/internal/bind/MiniGson;->a(Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;Lcom/google/gson/chase/reflect/TypeToken;)Lcom/google/gson/chase/internal/bind/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/google/gson/chase/internal/bind/ExcludedTypeAdapterFactory$1;->f:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/gson/chase/stream/JsonReader;)Ljava/lang/Object;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/chase/stream/JsonReader;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 56
    iget-boolean v0, p0, Lcom/google/gson/chase/internal/bind/ExcludedTypeAdapterFactory$1;->a:Z

    if-eqz v0, :cond_0

    .line 57
    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonReader;->n()V

    .line 58
    const/4 v0, 0x0

    .line 60
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Lcom/google/gson/chase/internal/bind/ExcludedTypeAdapterFactory$1;->a()Lcom/google/gson/chase/internal/bind/TypeAdapter;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/gson/chase/internal/bind/TypeAdapter;->a(Lcom/google/gson/chase/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/gson/chase/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/chase/stream/JsonWriter;",
            "TT;)V"
        }
    .end annotation

    .prologue
    .line 64
    iget-boolean v0, p0, Lcom/google/gson/chase/internal/bind/ExcludedTypeAdapterFactory$1;->b:Z

    if-eqz v0, :cond_0

    .line 65
    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonWriter;->f()Lcom/google/gson/chase/stream/JsonWriter;

    .line 69
    :goto_0
    return-void

    .line 68
    :cond_0
    invoke-direct {p0}, Lcom/google/gson/chase/internal/bind/ExcludedTypeAdapterFactory$1;->a()Lcom/google/gson/chase/internal/bind/TypeAdapter;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/gson/chase/internal/bind/TypeAdapter;->a(Lcom/google/gson/chase/stream/JsonWriter;Ljava/lang/Object;)V

    goto :goto_0
.end method
