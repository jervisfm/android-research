.class public final Lcom/google/gson/chase/internal/bind/ExcludedTypeAdapterFactory;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;


# instance fields
.field private final a:Lcom/google/gson/chase/ExclusionStrategy;

.field private final b:Lcom/google/gson/chase/ExclusionStrategy;


# direct methods
.method public constructor <init>(Lcom/google/gson/chase/ExclusionStrategy;Lcom/google/gson/chase/ExclusionStrategy;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/google/gson/chase/internal/bind/ExcludedTypeAdapterFactory;->a:Lcom/google/gson/chase/ExclusionStrategy;

    .line 36
    iput-object p2, p0, Lcom/google/gson/chase/internal/bind/ExcludedTypeAdapterFactory;->b:Lcom/google/gson/chase/ExclusionStrategy;

    .line 37
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/gson/chase/internal/bind/MiniGson;Lcom/google/gson/chase/reflect/TypeToken;)Lcom/google/gson/chase/internal/bind/TypeAdapter;
    .locals 6
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/gson/chase/internal/bind/MiniGson;",
            "Lcom/google/gson/chase/reflect/TypeToken",
            "<TT;>;)",
            "Lcom/google/gson/chase/internal/bind/TypeAdapter",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 40
    invoke-virtual {p2}, Lcom/google/gson/chase/reflect/TypeToken;->a()Ljava/lang/Class;

    move-result-object v0

    .line 41
    iget-object v1, p0, Lcom/google/gson/chase/internal/bind/ExcludedTypeAdapterFactory;->a:Lcom/google/gson/chase/ExclusionStrategy;

    invoke-interface {v1, v0}, Lcom/google/gson/chase/ExclusionStrategy;->a(Ljava/lang/Class;)Z

    move-result v3

    .line 42
    iget-object v1, p0, Lcom/google/gson/chase/internal/bind/ExcludedTypeAdapterFactory;->b:Lcom/google/gson/chase/ExclusionStrategy;

    invoke-interface {v1, v0}, Lcom/google/gson/chase/ExclusionStrategy;->a(Ljava/lang/Class;)Z

    move-result v2

    .line 44
    if-nez v3, :cond_0

    if-nez v2, :cond_0

    .line 45
    const/4 v0, 0x0

    .line 48
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/gson/chase/internal/bind/ExcludedTypeAdapterFactory$1;

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/gson/chase/internal/bind/ExcludedTypeAdapterFactory$1;-><init>(Lcom/google/gson/chase/internal/bind/ExcludedTypeAdapterFactory;ZZLcom/google/gson/chase/internal/bind/MiniGson;Lcom/google/gson/chase/reflect/TypeToken;)V

    goto :goto_0
.end method
