.class public final Lcom/google/gson/chase/internal/bind/MapTypeAdapterFactory;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/gson/chase/internal/bind/MapTypeAdapterFactory$Adapter;
    }
.end annotation


# instance fields
.field private final a:Lcom/google/gson/chase/internal/ConstructorConstructor;

.field private final b:Z


# direct methods
.method public constructor <init>(Lcom/google/gson/chase/internal/ConstructorConstructor;Z)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 107
    iput-object p1, p0, Lcom/google/gson/chase/internal/bind/MapTypeAdapterFactory;->a:Lcom/google/gson/chase/internal/ConstructorConstructor;

    .line 108
    iput-boolean p2, p0, Lcom/google/gson/chase/internal/bind/MapTypeAdapterFactory;->b:Z

    .line 109
    return-void
.end method

.method static synthetic a(Lcom/google/gson/chase/internal/bind/MapTypeAdapterFactory;)Z
    .locals 1
    .parameter

    .prologue
    .line 101
    iget-boolean v0, p0, Lcom/google/gson/chase/internal/bind/MapTypeAdapterFactory;->b:Z

    return v0
.end method


# virtual methods
.method public final a(Lcom/google/gson/chase/internal/bind/MiniGson;Lcom/google/gson/chase/reflect/TypeToken;)Lcom/google/gson/chase/internal/bind/TypeAdapter;
    .locals 8
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/gson/chase/internal/bind/MiniGson;",
            "Lcom/google/gson/chase/reflect/TypeToken",
            "<TT;>;)",
            "Lcom/google/gson/chase/internal/bind/TypeAdapter",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 112
    invoke-virtual {p2}, Lcom/google/gson/chase/reflect/TypeToken;->b()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 114
    invoke-virtual {p2}, Lcom/google/gson/chase/reflect/TypeToken;->a()Ljava/lang/Class;

    move-result-object v1

    .line 115
    const-class v2, Ljava/util/Map;

    invoke-virtual {v2, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 116
    const/4 v0, 0x0

    .line 129
    :goto_0
    return-object v0

    .line 119
    :cond_0
    invoke-static {v0}, Lcom/google/gson/chase/internal/$Gson$Types;->b(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v1

    .line 120
    invoke-static {v0, v1}, Lcom/google/gson/chase/internal/$Gson$Types;->b(Ljava/lang/reflect/Type;Ljava/lang/Class;)[Ljava/lang/reflect/Type;

    move-result-object v1

    .line 121
    aget-object v0, v1, v3

    sget-object v2, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    if-eq v0, v2, :cond_1

    const-class v2, Ljava/lang/Boolean;

    if-ne v0, v2, :cond_2

    :cond_1
    sget-object v4, Lcom/google/gson/chase/internal/bind/TypeAdapters;->d:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    .line 122
    :goto_1
    aget-object v0, v1, v5

    invoke-static {v0}, Lcom/google/gson/chase/reflect/TypeToken;->a(Ljava/lang/reflect/Type;)Lcom/google/gson/chase/reflect/TypeToken;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/gson/chase/internal/bind/MiniGson;->a(Lcom/google/gson/chase/reflect/TypeToken;)Lcom/google/gson/chase/internal/bind/TypeAdapter;

    move-result-object v6

    .line 123
    iget-object v0, p0, Lcom/google/gson/chase/internal/bind/MapTypeAdapterFactory;->a:Lcom/google/gson/chase/internal/ConstructorConstructor;

    invoke-virtual {v0, p2}, Lcom/google/gson/chase/internal/ConstructorConstructor;->a(Lcom/google/gson/chase/reflect/TypeToken;)Lcom/google/gson/chase/internal/ObjectConstructor;

    move-result-object v7

    .line 127
    new-instance v0, Lcom/google/gson/chase/internal/bind/MapTypeAdapterFactory$Adapter;

    aget-object v3, v1, v3

    aget-object v5, v1, v5

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/gson/chase/internal/bind/MapTypeAdapterFactory$Adapter;-><init>(Lcom/google/gson/chase/internal/bind/MapTypeAdapterFactory;Lcom/google/gson/chase/internal/bind/MiniGson;Ljava/lang/reflect/Type;Lcom/google/gson/chase/internal/bind/TypeAdapter;Ljava/lang/reflect/Type;Lcom/google/gson/chase/internal/bind/TypeAdapter;Lcom/google/gson/chase/internal/ObjectConstructor;)V

    goto :goto_0

    .line 121
    :cond_2
    invoke-static {v0}, Lcom/google/gson/chase/reflect/TypeToken;->a(Ljava/lang/reflect/Type;)Lcom/google/gson/chase/reflect/TypeToken;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/gson/chase/internal/bind/MiniGson;->a(Lcom/google/gson/chase/reflect/TypeToken;)Lcom/google/gson/chase/internal/bind/TypeAdapter;

    move-result-object v4

    goto :goto_1
.end method
