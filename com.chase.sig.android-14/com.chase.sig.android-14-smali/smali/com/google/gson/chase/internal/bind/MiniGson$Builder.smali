.class public final Lcom/google/gson/chase/internal/bind/MiniGson$Builder;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/gson/chase/internal/bind/MiniGson;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field a:Z

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 173
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 174
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/gson/chase/internal/bind/MiniGson$Builder;->b:Ljava/util/List;

    .line 175
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/gson/chase/internal/bind/MiniGson$Builder;->a:Z

    return-void
.end method

.method static synthetic a(Lcom/google/gson/chase/internal/bind/MiniGson$Builder;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 173
    iget-object v0, p0, Lcom/google/gson/chase/internal/bind/MiniGson$Builder;->b:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/gson/chase/internal/bind/MiniGson$Builder;
    .locals 1

    .prologue
    .line 183
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/gson/chase/internal/bind/MiniGson$Builder;->a:Z

    .line 184
    return-object p0
.end method

.method public final a(Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;)Lcom/google/gson/chase/internal/bind/MiniGson$Builder;
    .locals 1
    .parameter

    .prologue
    .line 178
    iget-object v0, p0, Lcom/google/gson/chase/internal/bind/MiniGson$Builder;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 179
    return-object p0
.end method

.method public final a(Ljava/lang/Class;Lcom/google/gson/chase/internal/bind/TypeAdapter;)Lcom/google/gson/chase/internal/bind/MiniGson$Builder;
    .locals 2
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Lcom/google/gson/chase/internal/bind/TypeAdapter",
            "<TT;>;)",
            "Lcom/google/gson/chase/internal/bind/MiniGson$Builder;"
        }
    .end annotation

    .prologue
    .line 188
    iget-object v0, p0, Lcom/google/gson/chase/internal/bind/MiniGson$Builder;->b:Ljava/util/List;

    invoke-static {p1, p2}, Lcom/google/gson/chase/internal/bind/TypeAdapters;->a(Ljava/lang/Class;Lcom/google/gson/chase/internal/bind/TypeAdapter;)Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 189
    return-object p0
.end method

.method public final b()Lcom/google/gson/chase/internal/bind/MiniGson;
    .locals 2

    .prologue
    .line 203
    new-instance v0, Lcom/google/gson/chase/internal/bind/MiniGson;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/gson/chase/internal/bind/MiniGson;-><init>(Lcom/google/gson/chase/internal/bind/MiniGson$Builder;B)V

    return-object v0
.end method
