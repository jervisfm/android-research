.class public final Lcom/google/gson/chase/internal/bind/ObjectTypeAdapter;
.super Lcom/google/gson/chase/internal/bind/TypeAdapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/gson/chase/internal/bind/ObjectTypeAdapter$2;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/chase/internal/bind/TypeAdapter",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;


# instance fields
.field private final b:Lcom/google/gson/chase/internal/bind/MiniGson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    new-instance v0, Lcom/google/gson/chase/internal/bind/ObjectTypeAdapter$1;

    invoke-direct {v0}, Lcom/google/gson/chase/internal/bind/ObjectTypeAdapter$1;-><init>()V

    sput-object v0, Lcom/google/gson/chase/internal/bind/ObjectTypeAdapter;->a:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    return-void
.end method

.method private constructor <init>(Lcom/google/gson/chase/internal/bind/MiniGson;)V
    .locals 0
    .parameter

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/google/gson/chase/internal/bind/TypeAdapter;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/google/gson/chase/internal/bind/ObjectTypeAdapter;->b:Lcom/google/gson/chase/internal/bind/MiniGson;

    .line 48
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/gson/chase/internal/bind/MiniGson;B)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/google/gson/chase/internal/bind/ObjectTypeAdapter;-><init>(Lcom/google/gson/chase/internal/bind/MiniGson;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/gson/chase/stream/JsonReader;)Ljava/lang/Object;
    .locals 3
    .parameter

    .prologue
    .line 51
    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonReader;->f()Lcom/google/gson/chase/stream/JsonToken;

    move-result-object v0

    .line 52
    sget-object v1, Lcom/google/gson/chase/internal/bind/ObjectTypeAdapter$2;->a:[I

    invoke-virtual {v0}, Lcom/google/gson/chase/stream/JsonToken;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 85
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 54
    :pswitch_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 55
    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonReader;->a()V

    .line 56
    :goto_0
    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonReader;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 57
    invoke-virtual {p0, p1}, Lcom/google/gson/chase/internal/bind/ObjectTypeAdapter;->a(Lcom/google/gson/chase/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 59
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonReader;->b()V

    .line 82
    :goto_1
    return-object v0

    .line 63
    :pswitch_1
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 64
    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonReader;->c()V

    .line 65
    :goto_2
    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonReader;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 66
    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonReader;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p1}, Lcom/google/gson/chase/internal/bind/ObjectTypeAdapter;->a(Lcom/google/gson/chase/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 68
    :cond_1
    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonReader;->d()V

    goto :goto_1

    .line 72
    :pswitch_2
    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonReader;->h()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 75
    :pswitch_3
    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonReader;->k()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    goto :goto_1

    .line 78
    :pswitch_4
    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonReader;->i()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_1

    .line 81
    :pswitch_5
    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonReader;->j()V

    .line 82
    const/4 v0, 0x0

    goto :goto_1

    .line 52
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final a(Lcom/google/gson/chase/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 90
    if-nez p2, :cond_0

    .line 91
    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonWriter;->f()Lcom/google/gson/chase/stream/JsonWriter;

    .line 103
    :goto_0
    return-void

    .line 95
    :cond_0
    iget-object v0, p0, Lcom/google/gson/chase/internal/bind/ObjectTypeAdapter;->b:Lcom/google/gson/chase/internal/bind/MiniGson;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/gson/chase/internal/bind/MiniGson;->a(Ljava/lang/Class;)Lcom/google/gson/chase/internal/bind/TypeAdapter;

    move-result-object v0

    .line 96
    instance-of v1, v0, Lcom/google/gson/chase/internal/bind/ObjectTypeAdapter;

    if-eqz v1, :cond_1

    .line 97
    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonWriter;->d()Lcom/google/gson/chase/stream/JsonWriter;

    .line 98
    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonWriter;->e()Lcom/google/gson/chase/stream/JsonWriter;

    goto :goto_0

    .line 102
    :cond_1
    invoke-virtual {v0, p1, p2}, Lcom/google/gson/chase/internal/bind/TypeAdapter;->a(Lcom/google/gson/chase/stream/JsonWriter;Ljava/lang/Object;)V

    goto :goto_0
.end method
