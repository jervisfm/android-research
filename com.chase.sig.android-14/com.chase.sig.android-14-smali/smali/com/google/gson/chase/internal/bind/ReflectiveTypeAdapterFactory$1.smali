.class Lcom/google/gson/chase/internal/bind/ReflectiveTypeAdapterFactory$1;
.super Lcom/google/gson/chase/internal/bind/ReflectiveTypeAdapterFactory$BoundField;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/gson/chase/internal/bind/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/chase/internal/bind/TypeAdapter",
            "<*>;"
        }
    .end annotation
.end field

.field final synthetic b:Lcom/google/gson/chase/internal/bind/MiniGson;

.field final synthetic c:Lcom/google/gson/chase/reflect/TypeToken;

.field final synthetic d:Ljava/lang/reflect/Field;

.field final synthetic e:Z

.field final synthetic f:Lcom/google/gson/chase/internal/bind/ReflectiveTypeAdapterFactory;


# direct methods
.method constructor <init>(Lcom/google/gson/chase/internal/bind/ReflectiveTypeAdapterFactory;Ljava/lang/String;ZZLcom/google/gson/chase/internal/bind/MiniGson;Lcom/google/gson/chase/reflect/TypeToken;Ljava/lang/reflect/Field;Z)V
    .locals 2
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 84
    iput-object p1, p0, Lcom/google/gson/chase/internal/bind/ReflectiveTypeAdapterFactory$1;->f:Lcom/google/gson/chase/internal/bind/ReflectiveTypeAdapterFactory;

    iput-object p5, p0, Lcom/google/gson/chase/internal/bind/ReflectiveTypeAdapterFactory$1;->b:Lcom/google/gson/chase/internal/bind/MiniGson;

    iput-object p6, p0, Lcom/google/gson/chase/internal/bind/ReflectiveTypeAdapterFactory$1;->c:Lcom/google/gson/chase/reflect/TypeToken;

    iput-object p7, p0, Lcom/google/gson/chase/internal/bind/ReflectiveTypeAdapterFactory$1;->d:Ljava/lang/reflect/Field;

    iput-boolean p8, p0, Lcom/google/gson/chase/internal/bind/ReflectiveTypeAdapterFactory$1;->e:Z

    invoke-direct {p0, p2, p3, p4}, Lcom/google/gson/chase/internal/bind/ReflectiveTypeAdapterFactory$BoundField;-><init>(Ljava/lang/String;ZZ)V

    .line 75
    iget-object v0, p0, Lcom/google/gson/chase/internal/bind/ReflectiveTypeAdapterFactory$1;->b:Lcom/google/gson/chase/internal/bind/MiniGson;

    iget-object v1, p0, Lcom/google/gson/chase/internal/bind/ReflectiveTypeAdapterFactory$1;->c:Lcom/google/gson/chase/reflect/TypeToken;

    invoke-virtual {v0, v1}, Lcom/google/gson/chase/internal/bind/MiniGson;->a(Lcom/google/gson/chase/reflect/TypeToken;)Lcom/google/gson/chase/internal/bind/TypeAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/google/gson/chase/internal/bind/ReflectiveTypeAdapterFactory$1;->a:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    return-void
.end method


# virtual methods
.method final a(Lcom/google/gson/chase/stream/JsonReader;Ljava/lang/Object;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/gson/chase/internal/bind/ReflectiveTypeAdapterFactory$1;->a:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/chase/internal/bind/TypeAdapter;->a(Lcom/google/gson/chase/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v0

    .line 87
    if-nez v0, :cond_0

    iget-boolean v1, p0, Lcom/google/gson/chase/internal/bind/ReflectiveTypeAdapterFactory$1;->e:Z

    if-nez v1, :cond_1

    .line 88
    :cond_0
    iget-object v1, p0, Lcom/google/gson/chase/internal/bind/ReflectiveTypeAdapterFactory$1;->d:Ljava/lang/reflect/Field;

    invoke-virtual {v1, p2, v0}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 90
    :cond_1
    return-void
.end method

.method final a(Lcom/google/gson/chase/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 5
    .parameter
    .parameter

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/gson/chase/internal/bind/ReflectiveTypeAdapterFactory$1;->d:Ljava/lang/reflect/Field;

    invoke-virtual {v0, p2}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 80
    new-instance v1, Lcom/google/gson/chase/internal/bind/TypeAdapterRuntimeTypeWrapper;

    iget-object v2, p0, Lcom/google/gson/chase/internal/bind/ReflectiveTypeAdapterFactory$1;->b:Lcom/google/gson/chase/internal/bind/MiniGson;

    iget-object v3, p0, Lcom/google/gson/chase/internal/bind/ReflectiveTypeAdapterFactory$1;->a:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    iget-object v4, p0, Lcom/google/gson/chase/internal/bind/ReflectiveTypeAdapterFactory$1;->c:Lcom/google/gson/chase/reflect/TypeToken;

    invoke-virtual {v4}, Lcom/google/gson/chase/reflect/TypeToken;->b()Ljava/lang/reflect/Type;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/google/gson/chase/internal/bind/TypeAdapterRuntimeTypeWrapper;-><init>(Lcom/google/gson/chase/internal/bind/MiniGson;Lcom/google/gson/chase/internal/bind/TypeAdapter;Ljava/lang/reflect/Type;)V

    .line 82
    invoke-virtual {v1, p1, v0}, Lcom/google/gson/chase/internal/bind/TypeAdapter;->a(Lcom/google/gson/chase/stream/JsonWriter;Ljava/lang/Object;)V

    .line 83
    return-void
.end method
