.class abstract Lcom/google/gson/chase/internal/bind/ReflectiveTypeAdapterFactory$BoundField;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/gson/chase/internal/bind/ReflectiveTypeAdapterFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x408
    name = "BoundField"
.end annotation


# instance fields
.field final g:Ljava/lang/String;

.field final h:Z

.field final i:Z


# direct methods
.method protected constructor <init>(Ljava/lang/String;ZZ)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 131
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 132
    iput-object p1, p0, Lcom/google/gson/chase/internal/bind/ReflectiveTypeAdapterFactory$BoundField;->g:Ljava/lang/String;

    .line 133
    iput-boolean p2, p0, Lcom/google/gson/chase/internal/bind/ReflectiveTypeAdapterFactory$BoundField;->h:Z

    .line 134
    iput-boolean p3, p0, Lcom/google/gson/chase/internal/bind/ReflectiveTypeAdapterFactory$BoundField;->i:Z

    .line 135
    return-void
.end method


# virtual methods
.method abstract a(Lcom/google/gson/chase/stream/JsonReader;Ljava/lang/Object;)V
.end method

.method abstract a(Lcom/google/gson/chase/stream/JsonWriter;Ljava/lang/Object;)V
.end method
