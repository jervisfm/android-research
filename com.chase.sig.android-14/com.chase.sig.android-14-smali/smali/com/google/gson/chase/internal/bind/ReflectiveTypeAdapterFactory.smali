.class public Lcom/google/gson/chase/internal/bind/ReflectiveTypeAdapterFactory;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/gson/chase/internal/bind/ReflectiveTypeAdapterFactory$Adapter;,
        Lcom/google/gson/chase/internal/bind/ReflectiveTypeAdapterFactory$BoundField;
    }
.end annotation


# instance fields
.field private final a:Lcom/google/gson/chase/internal/ConstructorConstructor;


# direct methods
.method public constructor <init>(Lcom/google/gson/chase/internal/ConstructorConstructor;)V
    .locals 0
    .parameter

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/google/gson/chase/internal/bind/ReflectiveTypeAdapterFactory;->a:Lcom/google/gson/chase/internal/ConstructorConstructor;

    .line 43
    return-void
.end method

.method private a(Lcom/google/gson/chase/internal/bind/MiniGson;Lcom/google/gson/chase/reflect/TypeToken;Ljava/lang/Class;)Ljava/util/Map;
    .locals 15
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/chase/internal/bind/MiniGson;",
            "Lcom/google/gson/chase/reflect/TypeToken",
            "<*>;",
            "Ljava/lang/Class",
            "<*>;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/gson/chase/internal/bind/ReflectiveTypeAdapterFactory$BoundField;",
            ">;"
        }
    .end annotation

    .prologue
    .line 96
    new-instance v10, Ljava/util/LinkedHashMap;

    invoke-direct {v10}, Ljava/util/LinkedHashMap;-><init>()V

    .line 97
    invoke-virtual/range {p3 .. p3}, Ljava/lang/Class;->isInterface()Z

    move-result v1

    if-eqz v1, :cond_0

    move-object v1, v10

    .line 123
    :goto_0
    return-object v1

    .line 101
    :cond_0
    invoke-virtual/range {p2 .. p2}, Lcom/google/gson/chase/reflect/TypeToken;->b()Ljava/lang/reflect/Type;

    move-result-object v12

    .line 102
    :goto_1
    const-class v1, Ljava/lang/Object;

    move-object/from16 v0, p3

    if-eq v0, v1, :cond_4

    .line 103
    invoke-virtual/range {p3 .. p3}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object v13

    .line 104
    const/4 v1, 0x1

    invoke-static {v13, v1}, Ljava/lang/reflect/AccessibleObject;->setAccessible([Ljava/lang/reflect/AccessibleObject;Z)V

    .line 105
    array-length v14, v13

    const/4 v1, 0x0

    move v11, v1

    :goto_2
    if-ge v11, v14, :cond_3

    aget-object v8, v13, v11

    .line 106
    move-object/from16 v0, p3

    invoke-virtual {p0, v0, v8}, Lcom/google/gson/chase/internal/bind/ReflectiveTypeAdapterFactory;->b(Ljava/lang/Class;Ljava/lang/reflect/Field;)Z

    move-result v4

    .line 107
    move-object/from16 v0, p3

    invoke-virtual {p0, v0, v8}, Lcom/google/gson/chase/internal/bind/ReflectiveTypeAdapterFactory;->c(Ljava/lang/Class;Ljava/lang/reflect/Field;)Z

    move-result v5

    .line 108
    if-nez v4, :cond_1

    if-eqz v5, :cond_2

    .line 109
    :cond_1
    invoke-virtual/range {p2 .. p2}, Lcom/google/gson/chase/reflect/TypeToken;->b()Ljava/lang/reflect/Type;

    move-result-object v1

    invoke-virtual {v8}, Ljava/lang/reflect/Field;->getGenericType()Ljava/lang/reflect/Type;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-static {v1, v0, v2}, Lcom/google/gson/chase/internal/$Gson$Types;->a(Ljava/lang/reflect/Type;Ljava/lang/Class;Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v1

    .line 112
    move-object/from16 v0, p3

    invoke-virtual {p0, v0, v8}, Lcom/google/gson/chase/internal/bind/ReflectiveTypeAdapterFactory;->a(Ljava/lang/Class;Ljava/lang/reflect/Field;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1}, Lcom/google/gson/chase/reflect/TypeToken;->a(Ljava/lang/reflect/Type;)Lcom/google/gson/chase/reflect/TypeToken;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/gson/chase/reflect/TypeToken;->a()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1}, Lcom/google/gson/chase/internal/Primitives;->a(Ljava/lang/reflect/Type;)Z

    move-result v9

    new-instance v1, Lcom/google/gson/chase/internal/bind/ReflectiveTypeAdapterFactory$1;

    move-object v2, p0

    move-object/from16 v6, p1

    invoke-direct/range {v1 .. v9}, Lcom/google/gson/chase/internal/bind/ReflectiveTypeAdapterFactory$1;-><init>(Lcom/google/gson/chase/internal/bind/ReflectiveTypeAdapterFactory;Ljava/lang/String;ZZLcom/google/gson/chase/internal/bind/MiniGson;Lcom/google/gson/chase/reflect/TypeToken;Ljava/lang/reflect/Field;Z)V

    .line 114
    iget-object v2, v1, Lcom/google/gson/chase/internal/bind/ReflectiveTypeAdapterFactory$BoundField;->g:Ljava/lang/String;

    invoke-interface {v10, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/gson/chase/internal/bind/ReflectiveTypeAdapterFactory$BoundField;

    .line 115
    if-eqz v1, :cond_2

    .line 116
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " declares multiple JSON fields named "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v1, v1, Lcom/google/gson/chase/internal/bind/ReflectiveTypeAdapterFactory$BoundField;->g:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 105
    :cond_2
    add-int/lit8 v1, v11, 0x1

    move v11, v1

    goto :goto_2

    .line 120
    :cond_3
    invoke-virtual/range {p2 .. p2}, Lcom/google/gson/chase/reflect/TypeToken;->b()Ljava/lang/reflect/Type;

    move-result-object v1

    invoke-virtual/range {p3 .. p3}, Ljava/lang/Class;->getGenericSuperclass()Ljava/lang/reflect/Type;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-static {v1, v0, v2}, Lcom/google/gson/chase/internal/$Gson$Types;->a(Ljava/lang/reflect/Type;Ljava/lang/Class;Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v1

    invoke-static {v1}, Lcom/google/gson/chase/reflect/TypeToken;->a(Ljava/lang/reflect/Type;)Lcom/google/gson/chase/reflect/TypeToken;

    move-result-object p2

    .line 121
    invoke-virtual/range {p2 .. p2}, Lcom/google/gson/chase/reflect/TypeToken;->a()Ljava/lang/Class;

    move-result-object p3

    goto/16 :goto_1

    :cond_4
    move-object v1, v10

    .line 123
    goto/16 :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/gson/chase/internal/bind/MiniGson;Lcom/google/gson/chase/reflect/TypeToken;)Lcom/google/gson/chase/internal/bind/TypeAdapter;
    .locals 4
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/gson/chase/internal/bind/MiniGson;",
            "Lcom/google/gson/chase/reflect/TypeToken",
            "<TT;>;)",
            "Lcom/google/gson/chase/internal/bind/TypeAdapter",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 58
    invoke-virtual {p2}, Lcom/google/gson/chase/reflect/TypeToken;->a()Ljava/lang/Class;

    move-result-object v1

    .line 60
    const-class v0, Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 61
    const/4 v0, 0x0

    .line 65
    :goto_0
    return-object v0

    .line 64
    :cond_0
    iget-object v0, p0, Lcom/google/gson/chase/internal/bind/ReflectiveTypeAdapterFactory;->a:Lcom/google/gson/chase/internal/ConstructorConstructor;

    invoke-virtual {v0, p2}, Lcom/google/gson/chase/internal/ConstructorConstructor;->a(Lcom/google/gson/chase/reflect/TypeToken;)Lcom/google/gson/chase/internal/ObjectConstructor;

    move-result-object v2

    .line 65
    new-instance v0, Lcom/google/gson/chase/internal/bind/ReflectiveTypeAdapterFactory$Adapter;

    invoke-direct {p0, p1, p2, v1}, Lcom/google/gson/chase/internal/bind/ReflectiveTypeAdapterFactory;->a(Lcom/google/gson/chase/internal/bind/MiniGson;Lcom/google/gson/chase/reflect/TypeToken;Ljava/lang/Class;)Ljava/util/Map;

    move-result-object v1

    const/4 v3, 0x0

    invoke-direct {v0, p0, v2, v1, v3}, Lcom/google/gson/chase/internal/bind/ReflectiveTypeAdapterFactory$Adapter;-><init>(Lcom/google/gson/chase/internal/bind/ReflectiveTypeAdapterFactory;Lcom/google/gson/chase/internal/ObjectConstructor;Ljava/util/Map;B)V

    goto :goto_0
.end method

.method protected a(Ljava/lang/Class;Ljava/lang/reflect/Field;)Ljava/lang/String;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/reflect/Field;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 54
    invoke-virtual {p2}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected b(Ljava/lang/Class;Ljava/lang/reflect/Field;)Z
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/reflect/Field;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 46
    invoke-virtual {p2}, Ljava/lang/reflect/Field;->isSynthetic()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected c(Ljava/lang/Class;Ljava/lang/reflect/Field;)Z
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/reflect/Field;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 50
    invoke-virtual {p2}, Ljava/lang/reflect/Field;->isSynthetic()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
