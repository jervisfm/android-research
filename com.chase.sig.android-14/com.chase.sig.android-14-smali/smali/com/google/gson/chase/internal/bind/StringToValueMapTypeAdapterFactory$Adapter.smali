.class final Lcom/google/gson/chase/internal/bind/StringToValueMapTypeAdapterFactory$Adapter;
.super Lcom/google/gson/chase/internal/bind/TypeAdapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/gson/chase/internal/bind/StringToValueMapTypeAdapterFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "Adapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/gson/chase/internal/bind/TypeAdapter",
        "<",
        "Ljava/util/Map",
        "<",
        "Ljava/lang/String;",
        "TV;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/gson/chase/internal/bind/StringToValueMapTypeAdapterFactory;

.field private final b:Lcom/google/gson/chase/internal/bind/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/chase/internal/bind/TypeAdapter",
            "<TV;>;"
        }
    .end annotation
.end field

.field private final c:Lcom/google/gson/chase/internal/ObjectConstructor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/chase/internal/ObjectConstructor",
            "<+",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "TV;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/gson/chase/internal/bind/StringToValueMapTypeAdapterFactory;Lcom/google/gson/chase/internal/bind/TypeAdapter;Lcom/google/gson/chase/internal/ObjectConstructor;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/chase/internal/bind/TypeAdapter",
            "<TV;>;",
            "Lcom/google/gson/chase/internal/ObjectConstructor",
            "<+",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "TV;>;>;)V"
        }
    .end annotation

    .prologue
    .line 71
    iput-object p1, p0, Lcom/google/gson/chase/internal/bind/StringToValueMapTypeAdapterFactory$Adapter;->a:Lcom/google/gson/chase/internal/bind/StringToValueMapTypeAdapterFactory;

    invoke-direct {p0}, Lcom/google/gson/chase/internal/bind/TypeAdapter;-><init>()V

    .line 72
    iput-object p2, p0, Lcom/google/gson/chase/internal/bind/StringToValueMapTypeAdapterFactory$Adapter;->b:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    .line 73
    iput-object p3, p0, Lcom/google/gson/chase/internal/bind/StringToValueMapTypeAdapterFactory$Adapter;->c:Lcom/google/gson/chase/internal/ObjectConstructor;

    .line 74
    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/gson/chase/stream/JsonReader;)Ljava/lang/Object;
    .locals 3
    .parameter

    .prologue
    .line 66
    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonReader;->f()Lcom/google/gson/chase/stream/JsonToken;

    move-result-object v0

    sget-object v1, Lcom/google/gson/chase/stream/JsonToken;->i:Lcom/google/gson/chase/stream/JsonToken;

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonReader;->j()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/gson/chase/internal/bind/StringToValueMapTypeAdapterFactory$Adapter;->c:Lcom/google/gson/chase/internal/ObjectConstructor;

    invoke-interface {v0}, Lcom/google/gson/chase/internal/ObjectConstructor;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonReader;->c()V

    :goto_1
    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonReader;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonReader;->g()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/gson/chase/internal/bind/StringToValueMapTypeAdapterFactory$Adapter;->b:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    invoke-virtual {v2, p1}, Lcom/google/gson/chase/internal/bind/TypeAdapter;->a(Lcom/google/gson/chase/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_1
    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonReader;->d()V

    goto :goto_0
.end method

.method public final synthetic a(Lcom/google/gson/chase/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 66
    check-cast p2, Ljava/util/Map;

    if-nez p2, :cond_0

    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonWriter;->f()Lcom/google/gson/chase/stream/JsonWriter;

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonWriter;->d()Lcom/google/gson/chase/stream/JsonWriter;

    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, Lcom/google/gson/chase/stream/JsonWriter;->a(Ljava/lang/String;)Lcom/google/gson/chase/stream/JsonWriter;

    iget-object v1, p0, Lcom/google/gson/chase/internal/bind/StringToValueMapTypeAdapterFactory$Adapter;->b:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, p1, v0}, Lcom/google/gson/chase/internal/bind/TypeAdapter;->a(Lcom/google/gson/chase/stream/JsonWriter;Ljava/lang/Object;)V

    goto :goto_1

    :cond_1
    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonWriter;->e()Lcom/google/gson/chase/stream/JsonWriter;

    goto :goto_0
.end method
