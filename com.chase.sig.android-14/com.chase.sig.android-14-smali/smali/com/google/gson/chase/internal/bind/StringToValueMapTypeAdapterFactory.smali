.class public final Lcom/google/gson/chase/internal/bind/StringToValueMapTypeAdapterFactory;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/gson/chase/internal/bind/StringToValueMapTypeAdapterFactory$Adapter;
    }
.end annotation


# instance fields
.field private final a:Lcom/google/gson/chase/internal/ConstructorConstructor;


# direct methods
.method public constructor <init>(Lcom/google/gson/chase/internal/ConstructorConstructor;)V
    .locals 0
    .parameter

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/google/gson/chase/internal/bind/StringToValueMapTypeAdapterFactory;->a:Lcom/google/gson/chase/internal/ConstructorConstructor;

    .line 39
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/gson/chase/internal/bind/MiniGson;Lcom/google/gson/chase/reflect/TypeToken;)Lcom/google/gson/chase/internal/bind/TypeAdapter;
    .locals 4
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/gson/chase/internal/bind/MiniGson;",
            "Lcom/google/gson/chase/reflect/TypeToken",
            "<TT;>;)",
            "Lcom/google/gson/chase/internal/bind/TypeAdapter",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 42
    invoke-virtual {p2}, Lcom/google/gson/chase/reflect/TypeToken;->b()Ljava/lang/reflect/Type;

    move-result-object v1

    .line 43
    instance-of v2, v1, Ljava/lang/reflect/ParameterizedType;

    if-nez v2, :cond_1

    .line 63
    :cond_0
    :goto_0
    return-object v0

    .line 47
    :cond_1
    invoke-virtual {p2}, Lcom/google/gson/chase/reflect/TypeToken;->a()Ljava/lang/Class;

    move-result-object v2

    .line 48
    const-class v3, Ljava/util/Map;

    invoke-virtual {v3, v2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 52
    invoke-static {v1, v2}, Lcom/google/gson/chase/internal/$Gson$Types;->b(Ljava/lang/reflect/Type;Ljava/lang/Class;)[Ljava/lang/reflect/Type;

    move-result-object v1

    .line 53
    const/4 v2, 0x0

    aget-object v2, v1, v2

    const-class v3, Ljava/lang/String;

    if-ne v2, v3, :cond_0

    .line 56
    const/4 v0, 0x1

    aget-object v0, v1, v0

    invoke-static {v0}, Lcom/google/gson/chase/reflect/TypeToken;->a(Ljava/lang/reflect/Type;)Lcom/google/gson/chase/reflect/TypeToken;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/gson/chase/internal/bind/MiniGson;->a(Lcom/google/gson/chase/reflect/TypeToken;)Lcom/google/gson/chase/internal/bind/TypeAdapter;

    move-result-object v1

    .line 58
    iget-object v0, p0, Lcom/google/gson/chase/internal/bind/StringToValueMapTypeAdapterFactory;->a:Lcom/google/gson/chase/internal/ConstructorConstructor;

    invoke-virtual {v0, p2}, Lcom/google/gson/chase/internal/ConstructorConstructor;->a(Lcom/google/gson/chase/reflect/TypeToken;)Lcom/google/gson/chase/internal/ObjectConstructor;

    move-result-object v2

    .line 62
    new-instance v0, Lcom/google/gson/chase/internal/bind/StringToValueMapTypeAdapterFactory$Adapter;

    invoke-direct {v0, p0, v1, v2}, Lcom/google/gson/chase/internal/bind/StringToValueMapTypeAdapterFactory$Adapter;-><init>(Lcom/google/gson/chase/internal/bind/StringToValueMapTypeAdapterFactory;Lcom/google/gson/chase/internal/bind/TypeAdapter;Lcom/google/gson/chase/internal/ObjectConstructor;)V

    goto :goto_0
.end method
