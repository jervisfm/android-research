.class public final Lcom/google/gson/chase/internal/bind/TimeTypeAdapter;
.super Lcom/google/gson/chase/internal/bind/TypeAdapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/chase/internal/bind/TypeAdapter",
        "<",
        "Ljava/sql/Time;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;


# instance fields
.field private final b:Ljava/text/DateFormat;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    new-instance v0, Lcom/google/gson/chase/internal/bind/TimeTypeAdapter$1;

    invoke-direct {v0}, Lcom/google/gson/chase/internal/bind/TimeTypeAdapter$1;-><init>()V

    sput-object v0, Lcom/google/gson/chase/internal/bind/TimeTypeAdapter;->a:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/gson/chase/internal/bind/TypeAdapter;-><init>()V

    .line 45
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "hh:mm:ss a"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/gson/chase/internal/bind/TimeTypeAdapter;->b:Ljava/text/DateFormat;

    return-void
.end method

.method private declared-synchronized a(Lcom/google/gson/chase/stream/JsonWriter;Ljava/sql/Time;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 61
    monitor-enter p0

    if-nez p2, :cond_0

    const/4 v0, 0x0

    :goto_0
    :try_start_0
    invoke-virtual {p1, v0}, Lcom/google/gson/chase/stream/JsonWriter;->b(Ljava/lang/String;)Lcom/google/gson/chase/stream/JsonWriter;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 62
    monitor-exit p0

    return-void

    .line 61
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/gson/chase/internal/bind/TimeTypeAdapter;->b:Ljava/text/DateFormat;

    invoke-virtual {v0, p2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized b(Lcom/google/gson/chase/stream/JsonReader;)Ljava/sql/Time;
    .locals 3
    .parameter

    .prologue
    .line 48
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonReader;->f()Lcom/google/gson/chase/stream/JsonToken;

    move-result-object v0

    sget-object v1, Lcom/google/gson/chase/stream/JsonToken;->i:Lcom/google/gson/chase/stream/JsonToken;

    if-ne v0, v1, :cond_0

    .line 49
    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonReader;->j()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 50
    const/4 v0, 0x0

    .line 54
    :goto_0
    monitor-exit p0

    return-object v0

    .line 53
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/gson/chase/internal/bind/TimeTypeAdapter;->b:Ljava/text/DateFormat;

    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonReader;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    .line 54
    new-instance v0, Ljava/sql/Time;

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Ljava/sql/Time;-><init>(J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0
    .catch Ljava/text/ParseException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 55
    :catch_0
    move-exception v0

    .line 56
    :try_start_2
    new-instance v1, Lcom/google/gson/chase/JsonSyntaxException;

    invoke-direct {v1, v0}, Lcom/google/gson/chase/JsonSyntaxException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 48
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final synthetic a(Lcom/google/gson/chase/stream/JsonReader;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/google/gson/chase/internal/bind/TimeTypeAdapter;->b(Lcom/google/gson/chase/stream/JsonReader;)Ljava/sql/Time;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/gson/chase/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 37
    check-cast p2, Ljava/sql/Time;

    invoke-direct {p0, p1, p2}, Lcom/google/gson/chase/internal/bind/TimeTypeAdapter;->a(Lcom/google/gson/chase/stream/JsonWriter;Ljava/sql/Time;)V

    return-void
.end method
