.class public abstract Lcom/google/gson/chase/internal/bind/TypeAdapter;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcom/google/gson/chase/JsonElement;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Lcom/google/gson/chase/JsonElement;"
        }
    .end annotation

    .prologue
    .line 57
    :try_start_0
    new-instance v0, Lcom/google/gson/chase/internal/bind/JsonElementWriter;

    invoke-direct {v0}, Lcom/google/gson/chase/internal/bind/JsonElementWriter;-><init>()V

    .line 58
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/gson/chase/internal/bind/JsonElementWriter;->b(Z)V

    .line 59
    invoke-virtual {p0, v0, p1}, Lcom/google/gson/chase/internal/bind/TypeAdapter;->a(Lcom/google/gson/chase/stream/JsonWriter;Ljava/lang/Object;)V

    .line 60
    invoke-virtual {v0}, Lcom/google/gson/chase/internal/bind/JsonElementWriter;->a()Lcom/google/gson/chase/JsonElement;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 61
    :catch_0
    move-exception v0

    .line 62
    new-instance v1, Lcom/google/gson/chase/JsonIOException;

    invoke-direct {v1, v0}, Lcom/google/gson/chase/JsonIOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Lcom/google/gson/chase/JsonElement;)Ljava/lang/Object;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/chase/JsonElement;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 68
    :try_start_0
    new-instance v0, Lcom/google/gson/chase/internal/bind/JsonElementReader;

    invoke-direct {v0, p1}, Lcom/google/gson/chase/internal/bind/JsonElementReader;-><init>(Lcom/google/gson/chase/JsonElement;)V

    .line 69
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/gson/chase/stream/JsonReader;->a(Z)V

    .line 70
    invoke-virtual {p0, v0}, Lcom/google/gson/chase/internal/bind/TypeAdapter;->a(Lcom/google/gson/chase/stream/JsonReader;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 71
    :catch_0
    move-exception v0

    .line 72
    new-instance v1, Lcom/google/gson/chase/JsonIOException;

    invoke-direct {v1, v0}, Lcom/google/gson/chase/JsonIOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public abstract a(Lcom/google/gson/chase/stream/JsonReader;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/chase/stream/JsonReader;",
            ")TT;"
        }
    .end annotation
.end method

.method public abstract a(Lcom/google/gson/chase/stream/JsonWriter;Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/chase/stream/JsonWriter;",
            "TT;)V"
        }
    .end annotation
.end method
