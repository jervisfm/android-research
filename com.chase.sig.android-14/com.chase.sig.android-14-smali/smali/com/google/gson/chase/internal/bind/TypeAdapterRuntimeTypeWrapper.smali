.class final Lcom/google/gson/chase/internal/bind/TypeAdapterRuntimeTypeWrapper;
.super Lcom/google/gson/chase/internal/bind/TypeAdapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/gson/chase/internal/bind/TypeAdapter",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/google/gson/chase/internal/bind/MiniGson;

.field private final b:Lcom/google/gson/chase/internal/bind/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/chase/internal/bind/TypeAdapter",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final c:Ljava/lang/reflect/Type;


# direct methods
.method constructor <init>(Lcom/google/gson/chase/internal/bind/MiniGson;Lcom/google/gson/chase/internal/bind/TypeAdapter;Ljava/lang/reflect/Type;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/chase/internal/bind/MiniGson;",
            "Lcom/google/gson/chase/internal/bind/TypeAdapter",
            "<TT;>;",
            "Ljava/lang/reflect/Type;",
            ")V"
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/gson/chase/internal/bind/TypeAdapter;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/google/gson/chase/internal/bind/TypeAdapterRuntimeTypeWrapper;->a:Lcom/google/gson/chase/internal/bind/MiniGson;

    .line 32
    iput-object p2, p0, Lcom/google/gson/chase/internal/bind/TypeAdapterRuntimeTypeWrapper;->b:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    .line 33
    iput-object p3, p0, Lcom/google/gson/chase/internal/bind/TypeAdapterRuntimeTypeWrapper;->c:Ljava/lang/reflect/Type;

    .line 34
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/gson/chase/stream/JsonReader;)Ljava/lang/Object;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/chase/stream/JsonReader;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/gson/chase/internal/bind/TypeAdapterRuntimeTypeWrapper;->b:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/chase/internal/bind/TypeAdapter;->a(Lcom/google/gson/chase/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/gson/chase/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/gson/chase/stream/JsonWriter;",
            "TT;)V"
        }
    .end annotation

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/gson/chase/internal/bind/TypeAdapterRuntimeTypeWrapper;->b:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    .line 51
    iget-object v1, p0, Lcom/google/gson/chase/internal/bind/TypeAdapterRuntimeTypeWrapper;->c:Ljava/lang/reflect/Type;

    invoke-static {v1, p2}, Lcom/google/gson/chase/internal/bind/Reflection;->a(Ljava/lang/reflect/Type;Ljava/lang/Object;)Ljava/lang/reflect/Type;

    move-result-object v1

    .line 52
    iget-object v2, p0, Lcom/google/gson/chase/internal/bind/TypeAdapterRuntimeTypeWrapper;->c:Ljava/lang/reflect/Type;

    if-eq v1, v2, :cond_0

    .line 53
    iget-object v0, p0, Lcom/google/gson/chase/internal/bind/TypeAdapterRuntimeTypeWrapper;->a:Lcom/google/gson/chase/internal/bind/MiniGson;

    invoke-static {v1}, Lcom/google/gson/chase/reflect/TypeToken;->a(Ljava/lang/reflect/Type;)Lcom/google/gson/chase/reflect/TypeToken;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/gson/chase/internal/bind/MiniGson;->a(Lcom/google/gson/chase/reflect/TypeToken;)Lcom/google/gson/chase/internal/bind/TypeAdapter;

    move-result-object v0

    .line 54
    instance-of v1, v0, Lcom/google/gson/chase/internal/bind/ReflectiveTypeAdapterFactory$Adapter;

    if-eqz v1, :cond_0

    .line 56
    iget-object v1, p0, Lcom/google/gson/chase/internal/bind/TypeAdapterRuntimeTypeWrapper;->b:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    instance-of v1, v1, Lcom/google/gson/chase/internal/bind/ReflectiveTypeAdapterFactory$Adapter;

    if-nez v1, :cond_0

    .line 60
    iget-object v0, p0, Lcom/google/gson/chase/internal/bind/TypeAdapterRuntimeTypeWrapper;->b:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    .line 63
    :cond_0
    invoke-virtual {v0, p1, p2}, Lcom/google/gson/chase/internal/bind/TypeAdapter;->a(Lcom/google/gson/chase/stream/JsonWriter;Ljava/lang/Object;)V

    .line 67
    return-void
.end method
