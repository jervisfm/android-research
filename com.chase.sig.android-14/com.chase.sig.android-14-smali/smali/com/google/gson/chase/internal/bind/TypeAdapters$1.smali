.class Lcom/google/gson/chase/internal/bind/TypeAdapters$1;
.super Lcom/google/gson/chase/internal/bind/TypeAdapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/chase/internal/bind/TypeAdapter",
        "<",
        "Ljava/util/BitSet;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 94
    invoke-direct {p0}, Lcom/google/gson/chase/internal/bind/TypeAdapter;-><init>()V

    return-void
.end method

.method private static b(Lcom/google/gson/chase/stream/JsonReader;)Ljava/util/BitSet;
    .locals 7
    .parameter

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 54
    invoke-virtual {p0}, Lcom/google/gson/chase/stream/JsonReader;->f()Lcom/google/gson/chase/stream/JsonToken;

    move-result-object v0

    sget-object v1, Lcom/google/gson/chase/stream/JsonToken;->i:Lcom/google/gson/chase/stream/JsonToken;

    if-ne v0, v1, :cond_0

    .line 55
    invoke-virtual {p0}, Lcom/google/gson/chase/stream/JsonReader;->j()V

    .line 56
    const/4 v0, 0x0

    .line 91
    :goto_0
    return-object v0

    .line 59
    :cond_0
    new-instance v4, Ljava/util/BitSet;

    invoke-direct {v4}, Ljava/util/BitSet;-><init>()V

    .line 60
    invoke-virtual {p0}, Lcom/google/gson/chase/stream/JsonReader;->a()V

    .line 62
    invoke-virtual {p0}, Lcom/google/gson/chase/stream/JsonReader;->f()Lcom/google/gson/chase/stream/JsonToken;

    move-result-object v0

    move v1, v2

    .line 63
    :goto_1
    sget-object v5, Lcom/google/gson/chase/stream/JsonToken;->b:Lcom/google/gson/chase/stream/JsonToken;

    if-eq v0, v5, :cond_4

    .line 65
    sget-object v5, Lcom/google/gson/chase/internal/bind/TypeAdapters$29;->a:[I

    invoke-virtual {v0}, Lcom/google/gson/chase/stream/JsonToken;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 82
    new-instance v1, Lcom/google/gson/chase/JsonSyntaxException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid bitset value type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/gson/chase/JsonSyntaxException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 67
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/gson/chase/stream/JsonReader;->m()I

    move-result v0

    if-eqz v0, :cond_2

    move v0, v3

    .line 84
    :goto_2
    if-eqz v0, :cond_1

    .line 85
    invoke-virtual {v4, v1}, Ljava/util/BitSet;->set(I)V

    .line 87
    :cond_1
    add-int/lit8 v1, v1, 0x1

    .line 88
    invoke-virtual {p0}, Lcom/google/gson/chase/stream/JsonReader;->f()Lcom/google/gson/chase/stream/JsonToken;

    move-result-object v0

    goto :goto_1

    :cond_2
    move v0, v2

    .line 67
    goto :goto_2

    .line 70
    :pswitch_1
    invoke-virtual {p0}, Lcom/google/gson/chase/stream/JsonReader;->i()Z

    move-result v0

    goto :goto_2

    .line 73
    :pswitch_2
    invoke-virtual {p0}, Lcom/google/gson/chase/stream/JsonReader;->h()Ljava/lang/String;

    move-result-object v0

    .line 75
    :try_start_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-eqz v0, :cond_3

    move v0, v3

    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_2

    .line 77
    :catch_0
    move-exception v1

    new-instance v1, Lcom/google/gson/chase/JsonSyntaxException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error: Expecting: bitset number value (1, 0), Found: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/gson/chase/JsonSyntaxException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 90
    :cond_4
    invoke-virtual {p0}, Lcom/google/gson/chase/stream/JsonReader;->b()V

    move-object v0, v4

    .line 91
    goto :goto_0

    .line 65
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final synthetic a(Lcom/google/gson/chase/stream/JsonReader;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 94
    invoke-static {p1}, Lcom/google/gson/chase/internal/bind/TypeAdapters$1;->b(Lcom/google/gson/chase/stream/JsonReader;)Ljava/util/BitSet;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/gson/chase/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 4
    .parameter
    .parameter

    .prologue
    const/4 v1, 0x0

    .line 94
    check-cast p2, Ljava/util/BitSet;

    if-nez p2, :cond_0

    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonWriter;->f()Lcom/google/gson/chase/stream/JsonWriter;

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonWriter;->b()Lcom/google/gson/chase/stream/JsonWriter;

    move v0, v1

    :goto_1
    invoke-virtual {p2}, Ljava/util/BitSet;->length()I

    move-result v2

    if-ge v0, v2, :cond_2

    invoke-virtual {p2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :goto_2
    int-to-long v2, v2

    invoke-virtual {p1, v2, v3}, Lcom/google/gson/chase/stream/JsonWriter;->a(J)Lcom/google/gson/chase/stream/JsonWriter;

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move v2, v1

    goto :goto_2

    :cond_2
    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonWriter;->c()Lcom/google/gson/chase/stream/JsonWriter;

    goto :goto_0
.end method
