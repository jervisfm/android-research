.class Lcom/google/gson/chase/internal/bind/TypeAdapters$19$1;
.super Lcom/google/gson/chase/internal/bind/TypeAdapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/chase/internal/bind/TypeAdapter",
        "<",
        "Ljava/sql/Timestamp;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/gson/chase/internal/bind/TypeAdapter;

.field final synthetic b:Lcom/google/gson/chase/internal/bind/TypeAdapters$19;


# direct methods
.method constructor <init>(Lcom/google/gson/chase/internal/bind/TypeAdapters$19;Lcom/google/gson/chase/internal/bind/TypeAdapter;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 466
    iput-object p1, p0, Lcom/google/gson/chase/internal/bind/TypeAdapters$19$1;->b:Lcom/google/gson/chase/internal/bind/TypeAdapters$19;

    iput-object p2, p0, Lcom/google/gson/chase/internal/bind/TypeAdapters$19$1;->a:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    invoke-direct {p0}, Lcom/google/gson/chase/internal/bind/TypeAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/gson/chase/stream/JsonReader;)Ljava/lang/Object;
    .locals 4
    .parameter

    .prologue
    .line 466
    iget-object v0, p0, Lcom/google/gson/chase/internal/bind/TypeAdapters$19$1;->a:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    invoke-virtual {v0, p1}, Lcom/google/gson/chase/internal/bind/TypeAdapter;->a(Lcom/google/gson/chase/stream/JsonReader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Date;

    if-eqz v0, :cond_0

    new-instance v1, Ljava/sql/Timestamp;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Ljava/sql/Timestamp;-><init>(J)V

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/gson/chase/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 466
    check-cast p2, Ljava/sql/Timestamp;

    iget-object v0, p0, Lcom/google/gson/chase/internal/bind/TypeAdapters$19$1;->a:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    invoke-virtual {v0, p1, p2}, Lcom/google/gson/chase/internal/bind/TypeAdapter;->a(Lcom/google/gson/chase/stream/JsonWriter;Ljava/lang/Object;)V

    return-void
.end method
