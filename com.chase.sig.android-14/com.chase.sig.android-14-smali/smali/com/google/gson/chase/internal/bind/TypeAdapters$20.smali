.class Lcom/google/gson/chase/internal/bind/TypeAdapters$20;
.super Lcom/google/gson/chase/internal/bind/TypeAdapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/chase/internal/bind/TypeAdapter",
        "<",
        "Ljava/util/Calendar;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 516
    invoke-direct {p0}, Lcom/google/gson/chase/internal/bind/TypeAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/gson/chase/stream/JsonReader;)Ljava/lang/Object;
    .locals 9
    .parameter

    .prologue
    const/4 v6, 0x0

    .line 516
    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonReader;->f()Lcom/google/gson/chase/stream/JsonToken;

    move-result-object v0

    sget-object v1, Lcom/google/gson/chase/stream/JsonToken;->i:Lcom/google/gson/chase/stream/JsonToken;

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonReader;->j()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonReader;->c()V

    move v5, v6

    move v4, v6

    move v3, v6

    move v2, v6

    move v1, v6

    :cond_1
    :goto_1
    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonReader;->f()Lcom/google/gson/chase/stream/JsonToken;

    move-result-object v0

    sget-object v7, Lcom/google/gson/chase/stream/JsonToken;->d:Lcom/google/gson/chase/stream/JsonToken;

    if-eq v0, v7, :cond_7

    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonReader;->g()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonReader;->m()I

    move-result v0

    const-string v8, "year"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    move v1, v0

    goto :goto_1

    :cond_2
    const-string v8, "month"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    move v2, v0

    goto :goto_1

    :cond_3
    const-string v8, "dayOfMonth"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    move v3, v0

    goto :goto_1

    :cond_4
    const-string v8, "hourOfDay"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    move v4, v0

    goto :goto_1

    :cond_5
    const-string v8, "minute"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    move v5, v0

    goto :goto_1

    :cond_6
    const-string v8, "second"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    move v6, v0

    goto :goto_1

    :cond_7
    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonReader;->d()V

    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct/range {v0 .. v6}, Ljava/util/GregorianCalendar;-><init>(IIIIII)V

    goto :goto_0
.end method

.method public final synthetic a(Lcom/google/gson/chase/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 2
    .parameter
    .parameter

    .prologue
    .line 516
    check-cast p2, Ljava/util/Calendar;

    if-nez p2, :cond_0

    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonWriter;->f()Lcom/google/gson/chase/stream/JsonWriter;

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonWriter;->d()Lcom/google/gson/chase/stream/JsonWriter;

    const-string v0, "year"

    invoke-virtual {p1, v0}, Lcom/google/gson/chase/stream/JsonWriter;->a(Ljava/lang/String;)Lcom/google/gson/chase/stream/JsonWriter;

    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/gson/chase/stream/JsonWriter;->a(J)Lcom/google/gson/chase/stream/JsonWriter;

    const-string v0, "month"

    invoke-virtual {p1, v0}, Lcom/google/gson/chase/stream/JsonWriter;->a(Ljava/lang/String;)Lcom/google/gson/chase/stream/JsonWriter;

    const/4 v0, 0x2

    invoke-virtual {p2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/gson/chase/stream/JsonWriter;->a(J)Lcom/google/gson/chase/stream/JsonWriter;

    const-string v0, "dayOfMonth"

    invoke-virtual {p1, v0}, Lcom/google/gson/chase/stream/JsonWriter;->a(Ljava/lang/String;)Lcom/google/gson/chase/stream/JsonWriter;

    const/4 v0, 0x5

    invoke-virtual {p2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/gson/chase/stream/JsonWriter;->a(J)Lcom/google/gson/chase/stream/JsonWriter;

    const-string v0, "hourOfDay"

    invoke-virtual {p1, v0}, Lcom/google/gson/chase/stream/JsonWriter;->a(Ljava/lang/String;)Lcom/google/gson/chase/stream/JsonWriter;

    const/16 v0, 0xb

    invoke-virtual {p2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/gson/chase/stream/JsonWriter;->a(J)Lcom/google/gson/chase/stream/JsonWriter;

    const-string v0, "minute"

    invoke-virtual {p1, v0}, Lcom/google/gson/chase/stream/JsonWriter;->a(Ljava/lang/String;)Lcom/google/gson/chase/stream/JsonWriter;

    const/16 v0, 0xc

    invoke-virtual {p2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/gson/chase/stream/JsonWriter;->a(J)Lcom/google/gson/chase/stream/JsonWriter;

    const-string v0, "second"

    invoke-virtual {p1, v0}, Lcom/google/gson/chase/stream/JsonWriter;->a(Ljava/lang/String;)Lcom/google/gson/chase/stream/JsonWriter;

    const/16 v0, 0xd

    invoke-virtual {p2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {p1, v0, v1}, Lcom/google/gson/chase/stream/JsonWriter;->a(J)Lcom/google/gson/chase/stream/JsonWriter;

    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonWriter;->e()Lcom/google/gson/chase/stream/JsonWriter;

    goto :goto_0
.end method
