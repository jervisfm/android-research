.class Lcom/google/gson/chase/internal/bind/TypeAdapters$22;
.super Lcom/google/gson/chase/internal/bind/TypeAdapter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/gson/chase/internal/bind/TypeAdapter",
        "<",
        "Lcom/google/gson/chase/JsonElement;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 616
    invoke-direct {p0}, Lcom/google/gson/chase/internal/bind/TypeAdapter;-><init>()V

    return-void
.end method

.method private a(Lcom/google/gson/chase/stream/JsonWriter;Lcom/google/gson/chase/JsonElement;)V
    .locals 3
    .parameter
    .parameter

    .prologue
    .line 617
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/google/gson/chase/JsonElement;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 618
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonWriter;->f()Lcom/google/gson/chase/stream/JsonWriter;

    .line 642
    :goto_0
    return-void

    .line 619
    :cond_1
    invoke-virtual {p2}, Lcom/google/gson/chase/JsonElement;->i()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 620
    invoke-virtual {p2}, Lcom/google/gson/chase/JsonElement;->m()Lcom/google/gson/chase/JsonPrimitive;

    move-result-object v0

    .line 621
    invoke-virtual {v0}, Lcom/google/gson/chase/JsonPrimitive;->o()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 622
    invoke-virtual {v0}, Lcom/google/gson/chase/JsonPrimitive;->a()Ljava/lang/Number;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/gson/chase/stream/JsonWriter;->a(Ljava/lang/Number;)Lcom/google/gson/chase/stream/JsonWriter;

    goto :goto_0

    .line 623
    :cond_2
    invoke-virtual {v0}, Lcom/google/gson/chase/JsonPrimitive;->n()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 624
    invoke-virtual {v0}, Lcom/google/gson/chase/JsonPrimitive;->f()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/gson/chase/stream/JsonWriter;->a(Z)Lcom/google/gson/chase/stream/JsonWriter;

    goto :goto_0

    .line 626
    :cond_3
    invoke-virtual {v0}, Lcom/google/gson/chase/JsonPrimitive;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/gson/chase/stream/JsonWriter;->b(Ljava/lang/String;)Lcom/google/gson/chase/stream/JsonWriter;

    goto :goto_0

    .line 629
    :cond_4
    invoke-virtual {p2}, Lcom/google/gson/chase/JsonElement;->g()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 630
    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonWriter;->b()Lcom/google/gson/chase/stream/JsonWriter;

    .line 631
    invoke-virtual {p2}, Lcom/google/gson/chase/JsonElement;->l()Lcom/google/gson/chase/JsonArray;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/gson/chase/JsonArray;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/gson/chase/JsonElement;

    .line 632
    invoke-direct {p0, p1, v0}, Lcom/google/gson/chase/internal/bind/TypeAdapters$22;->a(Lcom/google/gson/chase/stream/JsonWriter;Lcom/google/gson/chase/JsonElement;)V

    goto :goto_1

    .line 634
    :cond_5
    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonWriter;->c()Lcom/google/gson/chase/stream/JsonWriter;

    goto :goto_0

    .line 636
    :cond_6
    invoke-virtual {p2}, Lcom/google/gson/chase/JsonElement;->h()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 637
    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonWriter;->d()Lcom/google/gson/chase/stream/JsonWriter;

    .line 638
    invoke-virtual {p2}, Lcom/google/gson/chase/JsonElement;->k()Lcom/google/gson/chase/JsonObject;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/gson/chase/JsonObject;->n()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 639
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, Lcom/google/gson/chase/stream/JsonWriter;->a(Ljava/lang/String;)Lcom/google/gson/chase/stream/JsonWriter;

    .line 640
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/gson/chase/JsonElement;

    invoke-direct {p0, p1, v0}, Lcom/google/gson/chase/internal/bind/TypeAdapters$22;->a(Lcom/google/gson/chase/stream/JsonWriter;Lcom/google/gson/chase/JsonElement;)V

    goto :goto_2

    .line 642
    :cond_7
    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonWriter;->e()Lcom/google/gson/chase/stream/JsonWriter;

    goto/16 :goto_0

    .line 645
    :cond_8
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Couldn\'t write "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private b(Lcom/google/gson/chase/stream/JsonReader;)Lcom/google/gson/chase/JsonElement;
    .locals 3
    .parameter

    .prologue
    .line 580
    sget-object v0, Lcom/google/gson/chase/internal/bind/TypeAdapters$29;->a:[I

    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonReader;->f()Lcom/google/gson/chase/stream/JsonToken;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/gson/chase/stream/JsonToken;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 612
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 582
    :pswitch_0
    new-instance v0, Lcom/google/gson/chase/JsonPrimitive;

    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonReader;->h()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/gson/chase/JsonPrimitive;-><init>(Ljava/lang/String;)V

    .line 606
    :goto_0
    return-object v0

    .line 584
    :pswitch_1
    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonReader;->h()Ljava/lang/String;

    move-result-object v1

    .line 585
    new-instance v0, Lcom/google/gson/chase/JsonPrimitive;

    new-instance v2, Lcom/google/gson/chase/internal/LazilyParsedNumber;

    invoke-direct {v2, v1}, Lcom/google/gson/chase/internal/LazilyParsedNumber;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v2}, Lcom/google/gson/chase/JsonPrimitive;-><init>(Ljava/lang/Number;)V

    goto :goto_0

    .line 587
    :pswitch_2
    new-instance v0, Lcom/google/gson/chase/JsonPrimitive;

    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonReader;->i()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/gson/chase/JsonPrimitive;-><init>(Ljava/lang/Boolean;)V

    goto :goto_0

    .line 589
    :pswitch_3
    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonReader;->j()V

    .line 590
    sget-object v0, Lcom/google/gson/chase/JsonNull;->a:Lcom/google/gson/chase/JsonNull;

    goto :goto_0

    .line 592
    :pswitch_4
    new-instance v0, Lcom/google/gson/chase/JsonArray;

    invoke-direct {v0}, Lcom/google/gson/chase/JsonArray;-><init>()V

    .line 593
    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonReader;->a()V

    .line 594
    :goto_1
    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonReader;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 595
    invoke-direct {p0, p1}, Lcom/google/gson/chase/internal/bind/TypeAdapters$22;->b(Lcom/google/gson/chase/stream/JsonReader;)Lcom/google/gson/chase/JsonElement;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/gson/chase/JsonArray;->a(Lcom/google/gson/chase/JsonElement;)V

    goto :goto_1

    .line 597
    :cond_0
    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonReader;->b()V

    goto :goto_0

    .line 600
    :pswitch_5
    new-instance v0, Lcom/google/gson/chase/JsonObject;

    invoke-direct {v0}, Lcom/google/gson/chase/JsonObject;-><init>()V

    .line 601
    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonReader;->c()V

    .line 602
    :goto_2
    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonReader;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 603
    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonReader;->g()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1}, Lcom/google/gson/chase/internal/bind/TypeAdapters$22;->b(Lcom/google/gson/chase/stream/JsonReader;)Lcom/google/gson/chase/JsonElement;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/gson/chase/JsonObject;->a(Ljava/lang/String;Lcom/google/gson/chase/JsonElement;)V

    goto :goto_2

    .line 605
    :cond_1
    invoke-virtual {p1}, Lcom/google/gson/chase/stream/JsonReader;->d()V

    goto :goto_0

    .line 580
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method


# virtual methods
.method public final synthetic a(Lcom/google/gson/chase/stream/JsonReader;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 616
    invoke-direct {p0, p1}, Lcom/google/gson/chase/internal/bind/TypeAdapters$22;->b(Lcom/google/gson/chase/stream/JsonReader;)Lcom/google/gson/chase/JsonElement;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/gson/chase/stream/JsonWriter;Ljava/lang/Object;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 616
    check-cast p2, Lcom/google/gson/chase/JsonElement;

    invoke-direct {p0, p1, p2}, Lcom/google/gson/chase/internal/bind/TypeAdapters$22;->a(Lcom/google/gson/chase/stream/JsonWriter;Lcom/google/gson/chase/JsonElement;)V

    return-void
.end method
