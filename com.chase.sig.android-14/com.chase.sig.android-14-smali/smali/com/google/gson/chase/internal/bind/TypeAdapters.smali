.class public final Lcom/google/gson/chase/internal/bind/TypeAdapters;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/gson/chase/internal/bind/TypeAdapters$29;,
        Lcom/google/gson/chase/internal/bind/TypeAdapters$EnumTypeAdapter;
    }
.end annotation


# static fields
.field public static final A:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

.field public static final B:Lcom/google/gson/chase/internal/bind/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/chase/internal/bind/TypeAdapter",
            "<",
            "Ljava/net/URL;",
            ">;"
        }
    .end annotation
.end field

.field public static final C:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

.field public static final D:Lcom/google/gson/chase/internal/bind/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/chase/internal/bind/TypeAdapter",
            "<",
            "Ljava/net/URI;",
            ">;"
        }
    .end annotation
.end field

.field public static final E:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

.field public static final F:Lcom/google/gson/chase/internal/bind/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/chase/internal/bind/TypeAdapter",
            "<",
            "Ljava/net/InetAddress;",
            ">;"
        }
    .end annotation
.end field

.field public static final G:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

.field public static final H:Lcom/google/gson/chase/internal/bind/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/chase/internal/bind/TypeAdapter",
            "<",
            "Ljava/util/UUID;",
            ">;"
        }
    .end annotation
.end field

.field public static final I:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

.field public static final J:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

.field public static final K:Lcom/google/gson/chase/internal/bind/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/chase/internal/bind/TypeAdapter",
            "<",
            "Ljava/util/Calendar;",
            ">;"
        }
    .end annotation
.end field

.field public static final L:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

.field public static final M:Lcom/google/gson/chase/internal/bind/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/chase/internal/bind/TypeAdapter",
            "<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field public static final N:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

.field public static final O:Lcom/google/gson/chase/internal/bind/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/chase/internal/bind/TypeAdapter",
            "<",
            "Lcom/google/gson/chase/JsonElement;",
            ">;"
        }
    .end annotation
.end field

.field public static final P:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

.field public static final Q:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

.field public static final a:Lcom/google/gson/chase/internal/bind/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/chase/internal/bind/TypeAdapter",
            "<",
            "Ljava/util/BitSet;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

.field public static final c:Lcom/google/gson/chase/internal/bind/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/chase/internal/bind/TypeAdapter",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final d:Lcom/google/gson/chase/internal/bind/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/chase/internal/bind/TypeAdapter",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final e:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

.field public static final f:Lcom/google/gson/chase/internal/bind/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/chase/internal/bind/TypeAdapter",
            "<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final g:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

.field public static final h:Lcom/google/gson/chase/internal/bind/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/chase/internal/bind/TypeAdapter",
            "<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final i:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

.field public static final j:Lcom/google/gson/chase/internal/bind/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/chase/internal/bind/TypeAdapter",
            "<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final k:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

.field public static final l:Lcom/google/gson/chase/internal/bind/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/chase/internal/bind/TypeAdapter",
            "<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final m:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

.field public static final n:Lcom/google/gson/chase/internal/bind/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/chase/internal/bind/TypeAdapter",
            "<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final o:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

.field public static final p:Lcom/google/gson/chase/internal/bind/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/chase/internal/bind/TypeAdapter",
            "<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final q:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

.field public static final r:Lcom/google/gson/chase/internal/bind/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/chase/internal/bind/TypeAdapter",
            "<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field public static final s:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

.field public static final t:Lcom/google/gson/chase/internal/bind/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/chase/internal/bind/TypeAdapter",
            "<",
            "Ljava/lang/Character;",
            ">;"
        }
    .end annotation
.end field

.field public static final u:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

.field public static final v:Lcom/google/gson/chase/internal/bind/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/chase/internal/bind/TypeAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final w:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

.field public static final x:Lcom/google/gson/chase/internal/bind/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/chase/internal/bind/TypeAdapter",
            "<",
            "Ljava/lang/StringBuilder;",
            ">;"
        }
    .end annotation
.end field

.field public static final y:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

.field public static final z:Lcom/google/gson/chase/internal/bind/TypeAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/gson/chase/internal/bind/TypeAdapter",
            "<",
            "Ljava/lang/StringBuffer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 52
    new-instance v0, Lcom/google/gson/chase/internal/bind/TypeAdapters$1;

    invoke-direct {v0}, Lcom/google/gson/chase/internal/bind/TypeAdapters$1;-><init>()V

    sput-object v0, Lcom/google/gson/chase/internal/bind/TypeAdapters;->a:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    .line 109
    const-class v0, Ljava/util/BitSet;

    sget-object v1, Lcom/google/gson/chase/internal/bind/TypeAdapters;->a:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    invoke-static {v0, v1}, Lcom/google/gson/chase/internal/bind/TypeAdapters;->a(Ljava/lang/Class;Lcom/google/gson/chase/internal/bind/TypeAdapter;)Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    move-result-object v0

    sput-object v0, Lcom/google/gson/chase/internal/bind/TypeAdapters;->b:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    .line 111
    new-instance v0, Lcom/google/gson/chase/internal/bind/TypeAdapters$2;

    invoke-direct {v0}, Lcom/google/gson/chase/internal/bind/TypeAdapters$2;-><init>()V

    sput-object v0, Lcom/google/gson/chase/internal/bind/TypeAdapters;->c:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    .line 137
    new-instance v0, Lcom/google/gson/chase/internal/bind/TypeAdapters$3;

    invoke-direct {v0}, Lcom/google/gson/chase/internal/bind/TypeAdapters$3;-><init>()V

    sput-object v0, Lcom/google/gson/chase/internal/bind/TypeAdapters;->d:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    .line 151
    sget-object v0, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const-class v1, Ljava/lang/Boolean;

    sget-object v2, Lcom/google/gson/chase/internal/bind/TypeAdapters;->c:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    invoke-static {v0, v1, v2}, Lcom/google/gson/chase/internal/bind/TypeAdapters;->a(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/gson/chase/internal/bind/TypeAdapter;)Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    move-result-object v0

    sput-object v0, Lcom/google/gson/chase/internal/bind/TypeAdapters;->e:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    .line 154
    new-instance v0, Lcom/google/gson/chase/internal/bind/TypeAdapters$4;

    invoke-direct {v0}, Lcom/google/gson/chase/internal/bind/TypeAdapters$4;-><init>()V

    sput-object v0, Lcom/google/gson/chase/internal/bind/TypeAdapters;->f:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    .line 174
    sget-object v0, Ljava/lang/Byte;->TYPE:Ljava/lang/Class;

    const-class v1, Ljava/lang/Byte;

    sget-object v2, Lcom/google/gson/chase/internal/bind/TypeAdapters;->f:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    invoke-static {v0, v1, v2}, Lcom/google/gson/chase/internal/bind/TypeAdapters;->a(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/gson/chase/internal/bind/TypeAdapter;)Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    move-result-object v0

    sput-object v0, Lcom/google/gson/chase/internal/bind/TypeAdapters;->g:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    .line 177
    new-instance v0, Lcom/google/gson/chase/internal/bind/TypeAdapters$5;

    invoke-direct {v0}, Lcom/google/gson/chase/internal/bind/TypeAdapters$5;-><init>()V

    sput-object v0, Lcom/google/gson/chase/internal/bind/TypeAdapters;->h:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    .line 196
    sget-object v0, Ljava/lang/Short;->TYPE:Ljava/lang/Class;

    const-class v1, Ljava/lang/Short;

    sget-object v2, Lcom/google/gson/chase/internal/bind/TypeAdapters;->h:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    invoke-static {v0, v1, v2}, Lcom/google/gson/chase/internal/bind/TypeAdapters;->a(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/gson/chase/internal/bind/TypeAdapter;)Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    move-result-object v0

    sput-object v0, Lcom/google/gson/chase/internal/bind/TypeAdapters;->i:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    .line 199
    new-instance v0, Lcom/google/gson/chase/internal/bind/TypeAdapters$6;

    invoke-direct {v0}, Lcom/google/gson/chase/internal/bind/TypeAdapters$6;-><init>()V

    sput-object v0, Lcom/google/gson/chase/internal/bind/TypeAdapters;->j:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    .line 218
    sget-object v0, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const-class v1, Ljava/lang/Integer;

    sget-object v2, Lcom/google/gson/chase/internal/bind/TypeAdapters;->j:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    invoke-static {v0, v1, v2}, Lcom/google/gson/chase/internal/bind/TypeAdapters;->a(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/gson/chase/internal/bind/TypeAdapter;)Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    move-result-object v0

    sput-object v0, Lcom/google/gson/chase/internal/bind/TypeAdapters;->k:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    .line 221
    new-instance v0, Lcom/google/gson/chase/internal/bind/TypeAdapters$7;

    invoke-direct {v0}, Lcom/google/gson/chase/internal/bind/TypeAdapters$7;-><init>()V

    sput-object v0, Lcom/google/gson/chase/internal/bind/TypeAdapters;->l:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    .line 240
    sget-object v0, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    const-class v1, Ljava/lang/Long;

    sget-object v2, Lcom/google/gson/chase/internal/bind/TypeAdapters;->l:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    invoke-static {v0, v1, v2}, Lcom/google/gson/chase/internal/bind/TypeAdapters;->a(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/gson/chase/internal/bind/TypeAdapter;)Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    move-result-object v0

    sput-object v0, Lcom/google/gson/chase/internal/bind/TypeAdapters;->m:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    .line 243
    new-instance v0, Lcom/google/gson/chase/internal/bind/TypeAdapters$8;

    invoke-direct {v0}, Lcom/google/gson/chase/internal/bind/TypeAdapters$8;-><init>()V

    sput-object v0, Lcom/google/gson/chase/internal/bind/TypeAdapters;->n:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    .line 258
    sget-object v0, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    const-class v1, Ljava/lang/Float;

    sget-object v2, Lcom/google/gson/chase/internal/bind/TypeAdapters;->n:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    invoke-static {v0, v1, v2}, Lcom/google/gson/chase/internal/bind/TypeAdapters;->a(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/gson/chase/internal/bind/TypeAdapter;)Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    move-result-object v0

    sput-object v0, Lcom/google/gson/chase/internal/bind/TypeAdapters;->o:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    .line 261
    new-instance v0, Lcom/google/gson/chase/internal/bind/TypeAdapters$9;

    invoke-direct {v0}, Lcom/google/gson/chase/internal/bind/TypeAdapters$9;-><init>()V

    sput-object v0, Lcom/google/gson/chase/internal/bind/TypeAdapters;->p:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    .line 276
    sget-object v0, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    const-class v1, Ljava/lang/Double;

    sget-object v2, Lcom/google/gson/chase/internal/bind/TypeAdapters;->p:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    invoke-static {v0, v1, v2}, Lcom/google/gson/chase/internal/bind/TypeAdapters;->a(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/gson/chase/internal/bind/TypeAdapter;)Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    move-result-object v0

    sput-object v0, Lcom/google/gson/chase/internal/bind/TypeAdapters;->q:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    .line 279
    new-instance v0, Lcom/google/gson/chase/internal/bind/TypeAdapters$10;

    invoke-direct {v0}, Lcom/google/gson/chase/internal/bind/TypeAdapters$10;-><init>()V

    sput-object v0, Lcom/google/gson/chase/internal/bind/TypeAdapters;->r:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    .line 299
    const-class v0, Ljava/lang/Number;

    sget-object v1, Lcom/google/gson/chase/internal/bind/TypeAdapters;->r:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    invoke-static {v0, v1}, Lcom/google/gson/chase/internal/bind/TypeAdapters;->a(Ljava/lang/Class;Lcom/google/gson/chase/internal/bind/TypeAdapter;)Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    move-result-object v0

    sput-object v0, Lcom/google/gson/chase/internal/bind/TypeAdapters;->s:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    .line 301
    new-instance v0, Lcom/google/gson/chase/internal/bind/TypeAdapters$11;

    invoke-direct {v0}, Lcom/google/gson/chase/internal/bind/TypeAdapters$11;-><init>()V

    sput-object v0, Lcom/google/gson/chase/internal/bind/TypeAdapters;->t:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    .line 316
    sget-object v0, Ljava/lang/Character;->TYPE:Ljava/lang/Class;

    const-class v1, Ljava/lang/Character;

    sget-object v2, Lcom/google/gson/chase/internal/bind/TypeAdapters;->t:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    invoke-static {v0, v1, v2}, Lcom/google/gson/chase/internal/bind/TypeAdapters;->a(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/gson/chase/internal/bind/TypeAdapter;)Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    move-result-object v0

    sput-object v0, Lcom/google/gson/chase/internal/bind/TypeAdapters;->u:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    .line 319
    new-instance v0, Lcom/google/gson/chase/internal/bind/TypeAdapters$12;

    invoke-direct {v0}, Lcom/google/gson/chase/internal/bind/TypeAdapters$12;-><init>()V

    sput-object v0, Lcom/google/gson/chase/internal/bind/TypeAdapters;->v:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    .line 339
    const-class v0, Ljava/lang/String;

    sget-object v1, Lcom/google/gson/chase/internal/bind/TypeAdapters;->v:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    invoke-static {v0, v1}, Lcom/google/gson/chase/internal/bind/TypeAdapters;->a(Ljava/lang/Class;Lcom/google/gson/chase/internal/bind/TypeAdapter;)Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    move-result-object v0

    sput-object v0, Lcom/google/gson/chase/internal/bind/TypeAdapters;->w:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    .line 341
    new-instance v0, Lcom/google/gson/chase/internal/bind/TypeAdapters$13;

    invoke-direct {v0}, Lcom/google/gson/chase/internal/bind/TypeAdapters$13;-><init>()V

    sput-object v0, Lcom/google/gson/chase/internal/bind/TypeAdapters;->x:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    .line 356
    const-class v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/google/gson/chase/internal/bind/TypeAdapters;->x:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    invoke-static {v0, v1}, Lcom/google/gson/chase/internal/bind/TypeAdapters;->a(Ljava/lang/Class;Lcom/google/gson/chase/internal/bind/TypeAdapter;)Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    move-result-object v0

    sput-object v0, Lcom/google/gson/chase/internal/bind/TypeAdapters;->y:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    .line 359
    new-instance v0, Lcom/google/gson/chase/internal/bind/TypeAdapters$14;

    invoke-direct {v0}, Lcom/google/gson/chase/internal/bind/TypeAdapters$14;-><init>()V

    sput-object v0, Lcom/google/gson/chase/internal/bind/TypeAdapters;->z:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    .line 374
    const-class v0, Ljava/lang/StringBuffer;

    sget-object v1, Lcom/google/gson/chase/internal/bind/TypeAdapters;->z:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    invoke-static {v0, v1}, Lcom/google/gson/chase/internal/bind/TypeAdapters;->a(Ljava/lang/Class;Lcom/google/gson/chase/internal/bind/TypeAdapter;)Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    move-result-object v0

    sput-object v0, Lcom/google/gson/chase/internal/bind/TypeAdapters;->A:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    .line 377
    new-instance v0, Lcom/google/gson/chase/internal/bind/TypeAdapters$15;

    invoke-direct {v0}, Lcom/google/gson/chase/internal/bind/TypeAdapters$15;-><init>()V

    sput-object v0, Lcom/google/gson/chase/internal/bind/TypeAdapters;->B:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    .line 393
    const-class v0, Ljava/net/URL;

    sget-object v1, Lcom/google/gson/chase/internal/bind/TypeAdapters;->B:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    invoke-static {v0, v1}, Lcom/google/gson/chase/internal/bind/TypeAdapters;->a(Ljava/lang/Class;Lcom/google/gson/chase/internal/bind/TypeAdapter;)Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    move-result-object v0

    sput-object v0, Lcom/google/gson/chase/internal/bind/TypeAdapters;->C:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    .line 395
    new-instance v0, Lcom/google/gson/chase/internal/bind/TypeAdapters$16;

    invoke-direct {v0}, Lcom/google/gson/chase/internal/bind/TypeAdapters$16;-><init>()V

    sput-object v0, Lcom/google/gson/chase/internal/bind/TypeAdapters;->D:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    .line 415
    const-class v0, Ljava/net/URI;

    sget-object v1, Lcom/google/gson/chase/internal/bind/TypeAdapters;->D:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    invoke-static {v0, v1}, Lcom/google/gson/chase/internal/bind/TypeAdapters;->a(Ljava/lang/Class;Lcom/google/gson/chase/internal/bind/TypeAdapter;)Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    move-result-object v0

    sput-object v0, Lcom/google/gson/chase/internal/bind/TypeAdapters;->E:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    .line 417
    new-instance v0, Lcom/google/gson/chase/internal/bind/TypeAdapters$17;

    invoke-direct {v0}, Lcom/google/gson/chase/internal/bind/TypeAdapters$17;-><init>()V

    sput-object v0, Lcom/google/gson/chase/internal/bind/TypeAdapters;->F:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    .line 432
    const-class v0, Ljava/net/InetAddress;

    sget-object v1, Lcom/google/gson/chase/internal/bind/TypeAdapters;->F:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    new-instance v2, Lcom/google/gson/chase/internal/bind/TypeAdapters$28;

    invoke-direct {v2, v0, v1}, Lcom/google/gson/chase/internal/bind/TypeAdapters$28;-><init>(Ljava/lang/Class;Lcom/google/gson/chase/internal/bind/TypeAdapter;)V

    sput-object v2, Lcom/google/gson/chase/internal/bind/TypeAdapters;->G:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    .line 435
    new-instance v0, Lcom/google/gson/chase/internal/bind/TypeAdapters$18;

    invoke-direct {v0}, Lcom/google/gson/chase/internal/bind/TypeAdapters$18;-><init>()V

    sput-object v0, Lcom/google/gson/chase/internal/bind/TypeAdapters;->H:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    .line 450
    const-class v0, Ljava/util/UUID;

    sget-object v1, Lcom/google/gson/chase/internal/bind/TypeAdapters;->H:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    invoke-static {v0, v1}, Lcom/google/gson/chase/internal/bind/TypeAdapters;->a(Ljava/lang/Class;Lcom/google/gson/chase/internal/bind/TypeAdapter;)Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    move-result-object v0

    sput-object v0, Lcom/google/gson/chase/internal/bind/TypeAdapters;->I:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    .line 452
    new-instance v0, Lcom/google/gson/chase/internal/bind/TypeAdapters$19;

    invoke-direct {v0}, Lcom/google/gson/chase/internal/bind/TypeAdapters$19;-><init>()V

    sput-object v0, Lcom/google/gson/chase/internal/bind/TypeAdapters;->J:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    .line 473
    new-instance v0, Lcom/google/gson/chase/internal/bind/TypeAdapters$20;

    invoke-direct {v0}, Lcom/google/gson/chase/internal/bind/TypeAdapters$20;-><init>()V

    sput-object v0, Lcom/google/gson/chase/internal/bind/TypeAdapters;->K:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    .line 538
    const-class v0, Ljava/util/Calendar;

    const-class v1, Ljava/util/GregorianCalendar;

    sget-object v2, Lcom/google/gson/chase/internal/bind/TypeAdapters;->K:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    new-instance v3, Lcom/google/gson/chase/internal/bind/TypeAdapters$27;

    invoke-direct {v3, v0, v1, v2}, Lcom/google/gson/chase/internal/bind/TypeAdapters$27;-><init>(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/gson/chase/internal/bind/TypeAdapter;)V

    sput-object v3, Lcom/google/gson/chase/internal/bind/TypeAdapters;->L:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    .line 541
    new-instance v0, Lcom/google/gson/chase/internal/bind/TypeAdapters$21;

    invoke-direct {v0}, Lcom/google/gson/chase/internal/bind/TypeAdapters$21;-><init>()V

    sput-object v0, Lcom/google/gson/chase/internal/bind/TypeAdapters;->M:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    .line 576
    const-class v0, Ljava/util/Locale;

    sget-object v1, Lcom/google/gson/chase/internal/bind/TypeAdapters;->M:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    invoke-static {v0, v1}, Lcom/google/gson/chase/internal/bind/TypeAdapters;->a(Ljava/lang/Class;Lcom/google/gson/chase/internal/bind/TypeAdapter;)Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    move-result-object v0

    sput-object v0, Lcom/google/gson/chase/internal/bind/TypeAdapters;->N:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    .line 578
    new-instance v0, Lcom/google/gson/chase/internal/bind/TypeAdapters$22;

    invoke-direct {v0}, Lcom/google/gson/chase/internal/bind/TypeAdapters$22;-><init>()V

    sput-object v0, Lcom/google/gson/chase/internal/bind/TypeAdapters;->O:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    .line 650
    const-class v0, Lcom/google/gson/chase/JsonElement;

    sget-object v1, Lcom/google/gson/chase/internal/bind/TypeAdapters;->O:Lcom/google/gson/chase/internal/bind/TypeAdapter;

    invoke-static {v0, v1}, Lcom/google/gson/chase/internal/bind/TypeAdapters;->a(Ljava/lang/Class;Lcom/google/gson/chase/internal/bind/TypeAdapter;)Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    move-result-object v0

    sput-object v0, Lcom/google/gson/chase/internal/bind/TypeAdapters;->P:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    .line 672
    const-class v0, Ljava/lang/Enum;

    new-instance v1, Lcom/google/gson/chase/internal/bind/TypeAdapters$23;

    invoke-direct {v1, v0}, Lcom/google/gson/chase/internal/bind/TypeAdapters$23;-><init>(Ljava/lang/Class;)V

    sput-object v1, Lcom/google/gson/chase/internal/bind/TypeAdapters;->Q:Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Class;Lcom/google/gson/chase/internal/bind/TypeAdapter;)Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;
    .locals 1
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TT:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TTT;>;",
            "Lcom/google/gson/chase/internal/bind/TypeAdapter",
            "<TTT;>;)",
            "Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;"
        }
    .end annotation

    .prologue
    .line 697
    new-instance v0, Lcom/google/gson/chase/internal/bind/TypeAdapters$25;

    invoke-direct {v0, p0, p1}, Lcom/google/gson/chase/internal/bind/TypeAdapters$25;-><init>(Ljava/lang/Class;Lcom/google/gson/chase/internal/bind/TypeAdapter;)V

    return-object v0
.end method

.method public static a(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/gson/chase/internal/bind/TypeAdapter;)Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;
    .locals 1
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TT:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TTT;>;",
            "Ljava/lang/Class",
            "<TTT;>;",
            "Lcom/google/gson/chase/internal/bind/TypeAdapter",
            "<-TTT;>;)",
            "Lcom/google/gson/chase/internal/bind/TypeAdapter$Factory;"
        }
    .end annotation

    .prologue
    .line 710
    new-instance v0, Lcom/google/gson/chase/internal/bind/TypeAdapters$26;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/gson/chase/internal/bind/TypeAdapters$26;-><init>(Ljava/lang/Class;Ljava/lang/Class;Lcom/google/gson/chase/internal/bind/TypeAdapter;)V

    return-object v0
.end method
