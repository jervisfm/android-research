package com.chase.sig.analytics;


import java.util.ArrayList;
import java.util.Hashtable;

import org.json.JSONObject;

import android.content.Context;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.chase.sig.android.ChaseApplication;
import com.chase.sig.android.R;
import com.chase.sig.android.activity.JPActivity;
import com.chase.sig.android.service.IServiceError;
import com.chase.sig.android.service.JPService;
import com.chase.sig.android.util.JSONClient;
import com.chase.sig.android.util.StringUtil;
import com.chase.sig.android.view.JPDialog;
import com.chase.sig.android.view.StylizedListItemView;
import com.chase.sig.android.view.JPDialog.Builder;
import com.chase.sig.android.view.detail.AbstractDetailRow;

public aspect BehaviorAnalyticsAspect {
	
    private static final String GWO_PATH = "/gws/";
    
	/*******************************************************
	 * 			Tracks the active activity (SCREEN)
	 ******************************************************/
    
	pointcut captureVisibleScreen(JPActivity jpActivity) : 
	target(jpActivity)
	&& within(com.chase.sig.android.activity.JPActivity)
	&& execution(void JPActivity.onResume());
	
	after(JPActivity jpActivity) : captureVisibleScreen(jpActivity)
	{
	   
	    AspectAnalyticsUtil.buildCurrentScreenToTrack(jpActivity);
	}
	
	pointcut setScreenOnPause(JPActivity jpActivity) : 
	target(jpActivity)
	&& within(com.chase.sig.android.activity.JPActivity)
	&& execution(void JPActivity.onPause());
	
    after(JPActivity jpActivity) : setScreenOnPause(jpActivity)
    {
       
        AspectAnalyticsUtil.setScreenForTrackingData(jpActivity);
    }
	
	/*******************************************************
	 * 			Tracks the view CLICKS/TOUCHES by the user
	 *******************************************************/

	@SuppressWarnings("rawtypes")
    pointcut setContentDescriptorForAbstractDetailRowChildren(AbstractDetailRow detailRow, View rowView, Context context) : 
    target(detailRow)
    && args (rowView, context)
    && within (com.chase.sig.android.view.detail.AbstractDetailRow)
    && execution (void AbstractDetailRow.onRowViewCreated(View, Context));
	
    before(@SuppressWarnings("rawtypes") AbstractDetailRow detailRow, View rowView, Context context)
    : setContentDescriptorForAbstractDetailRowChildren(detailRow, rowView, context) {
        
        TextView label = (TextView) rowView.findViewById(R.id.label);
        String labelAssociatedToClickableWidget = (String) label.getText();
        
        if (StringUtil.isNullOrEmpty(labelAssociatedToClickableWidget)) {
            labelAssociatedToClickableWidget = detailRow.getHint();
        }
        
        View view = rowView.findViewById(detailRow.getValueViewId());
        if (view != null) {
            view.setContentDescription(labelAssociatedToClickableWidget);
        }
    }
   
    @SuppressWarnings("rawtypes")
    pointcut injectOnTouchForAbstractDetailRowChildren(AbstractDetailRow detailRow, Context context) : 
    target(detailRow)
    && args (context)
    && within (com.chase.sig.android.view.detail.AbstractDetailRow+)
    && !within (com.chase.sig.android.view.detail.AbstractDetailRow)
    && execution (protected void onRowViewCreated(Context));
    
    before(@SuppressWarnings("rawtypes") AbstractDetailRow detailRow, Context context)
        : injectOnTouchForAbstractDetailRowChildren(detailRow, context) {
        
        View rowView = detailRow.getRowView();

        View valueView = rowView.findViewById(detailRow.getValueViewId());
        if (valueView != null) {
            
            valueView.setOnTouchListener(new OnTouchListener() {
                
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return false;
                }
            });
            
            valueView.setOnKeyListener(new OnKeyListener() {
                
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    return false;
                }
            });
            
        }
       
    }
    

	pointcut injectOnTouchandOnKeyForEditTextAndCheckBox(JPActivity jpActivity) : target(jpActivity)
	&& within(com.chase.sig.android.activity.JPActivity) 
	&& execution(void JPActivity.setMainView(int));
	
	after(JPActivity jpActivity) : injectOnTouchandOnKeyForEditTextAndCheckBox(jpActivity)
	{
		ViewGroup viewGroup = jpActivity.getMainView();
	
		ArrayList<View> viewList = AspectAnalyticsUtil.extractViewsFromLayout(viewGroup,
		    new ArrayList<View>());
		
		for (View view : viewList) {
			if (view instanceof EditText || view instanceof CheckBox) {	
				view.setOnTouchListener(new OnTouchListener() {
					
					@Override
					public boolean onTouch(View v, MotionEvent event) {
						return false;
					}
				});
				
				view.setOnKeyListener(new OnKeyListener() {
					
					@Override
					public boolean onKey(View v, int keyCode, KeyEvent event) {
						return false;
					}
				});
			}
		}
	}

	pointcut captureKeyEventsForEditTextOrCheckBox(View view, int keyCode, KeyEvent event) 
	: args (view, keyCode, event)
	&& execution(* View.OnKeyListener.onKey(View, int, KeyEvent));
	
	before(View view, int keyCode, KeyEvent event) : captureKeyEventsForEditTextOrCheckBox(view, keyCode, event) {

	    String labelValue = null;

	    if (view instanceof CheckBox && keyCode == KeyEvent.KEYCODE_DPAD_CENTER 
	                                 && event.getAction() == KeyEvent.ACTION_DOWN){
	        
			labelValue = AspectAnalyticsUtil.getValueFromWidgetInstance(view);
		}
	    else if(view instanceof EditText){
	        
	        if (AspectAnalyticsUtil.validateEditTextForTrackingData(((EditText)view), event, keyCode)) {
	        
    	        labelValue = AspectAnalyticsUtil.getValueFromWidgetInstance(view);
	        }
	    }

	    AspectAnalyticsUtil.setFieldForTrackingData(labelValue);
		
	}
	
	pointcut captureViewOnTouch(View.OnTouchListener viewOnTouchImplementor, View view, MotionEvent event) : target(viewOnTouchImplementor) 
	&& args(view, event) 
	&& execution(* View.OnTouchListener.onTouch(View, MotionEvent));

	before(View.OnTouchListener viewOnTouchImplementor, View view, MotionEvent event) : captureViewOnTouch(viewOnTouchImplementor, view,event)
	{
		String labelValue = null;
		
		if (event.getAction() == MotionEvent.ACTION_UP){
		    
		    if(view instanceof EditText){
	            
	            if (AspectAnalyticsUtil.isNotCurrentEditText((EditText)view)) {
	            
	                labelValue = AspectAnalyticsUtil.getValueFromWidgetInstance(view);
	            }
	        } else if (view instanceof CheckBox){
			
	            labelValue = AspectAnalyticsUtil.getValueFromWidgetInstance(view);
	        }
		}

		AspectAnalyticsUtil.setFieldForTrackingData(labelValue);
		
	}
	
	pointcut captureViewOnClick(View.OnClickListener viewOnClickImplementor, View view) : 
	target(viewOnClickImplementor) 
	&& args(view) 
	&& within (android.view.View.OnClickListener+)
	&& execution(* View.OnClickListener.onClick(View));
	
	before(View.OnClickListener viewOnClickImplementor, View view) : captureViewOnClick(viewOnClickImplementor, view)
	{	
		String labelValue = null;
		
		labelValue = AspectAnalyticsUtil.getValueFromWidgetInstance(view);
		
		AspectAnalyticsUtil.setFieldForTrackingData(labelValue);
	}
	
	pointcut captureOnItemClick(AdapterView.OnItemClickListener onItemClickImplementor, AdapterView<?> parent, View view, int position, long id) :
	target(onItemClickImplementor) 
	&& args(parent, view, position, id) 
	&& execution(* AdapterView.OnItemClickListener.*(..));
	// scenerio where extractor was used, looks for stylized list view
	after(AdapterView.OnItemClickListener onItemClickImplementor, AdapterView<?> parent, View view, int position, long id) : captureOnItemClick(onItemClickImplementor, parent, view, position, id)
	{	
		String labelValue = AspectAnalyticsUtil.UNRECOGNIZABLE;
		
		if (view instanceof StylizedListItemView) {
		    labelValue = AspectAnalyticsUtil.getValueFromStylizedListItem(parent, position);
		} else {
		    labelValue = AspectAnalyticsUtil.getValueFromWidgetInstance(view);
		}
		
		if (!labelValue.equalsIgnoreCase(AspectAnalyticsUtil.UNRECOGNIZABLE)) {
			
			ScreenAnalyticsData scrAnalyticsData = AspectAnalyticsUtil.getLastVisitedScreen();
			
			String acctId = AspectAnalyticsUtil.getAccountIDForCurrentScreenFromView(view);
			
			if (acctId != null) {
				scrAnalyticsData.setAccountId(acctId);
			}
			
			FieldAnalyticsData fieldAnalyticsData = new FieldAnalyticsData(labelValue);
			scrAnalyticsData.addField(fieldAnalyticsData);
		}
	}
	
	/*******************************************************
	 * 			Tracks the ERRORS displayed
	 *******************************************************/	

	pointcut captureMessageForJPDialog(JPDialog.Builder jpDialogBuilder) :
	target(jpDialogBuilder)
	&& execution(public Builder JPDialog.Builder.setMessage(..));
	
	after (JPDialog.Builder jpDialogBuilder) : captureMessageForJPDialog(jpDialogBuilder) {
	   AspectAnalyticsUtil.setErrorIntoScreenForTrackingData(jpDialogBuilder.getMessageAsString());
	}
	
	pointcut captureIServiceErrors(JPActivity jpActivity, int id, IServiceError message) : 
	target(jpActivity)
	&& args(id, message)
	&& execution(public void showError(int, IServiceError));
	
	before (JPActivity jpActivity, int id, IServiceError message)
	    : captureIServiceErrors(jpActivity, id, message) {
	    
		AspectAnalyticsUtil.setLocalIServiceCode(message.getCode());
	}

	pointcut captureIServiceErrorsWithFinish(JPActivity jpActivity, IServiceError message) : 
	target(jpActivity)
	&& args(message)
	&& execution(void showErrorAndFinish(IServiceError));
	
	before (JPActivity jpActivity, IServiceError message)
	    : captureIServiceErrorsWithFinish(jpActivity, message) {
	    
		AspectAnalyticsUtil.setLocalIServiceCode(message.getCode());
	}

	pointcut captureIServiceErrorsWithNonCancellableError(JPActivity jpActivity, int id, IServiceError message) : 
	target(jpActivity)
	&& args(id, message)
	&& execution(void showNonCancellableError(int, IServiceError));
	
	before (JPActivity jpActivity, int id, IServiceError message)
	    : captureIServiceErrorsWithNonCancellableError(jpActivity, id, message) {
		AspectAnalyticsUtil.setLocalIServiceCode(message.getCode());
	}

	/*******************************************************
	 * 			Insert Tracking Data to Request Parameters
	 *******************************************************/


	pointcut insertTrackingDataIntoRequest(ChaseApplication application, String uri, Hashtable<String, String> requestParams) 
	:within (com.chase.sig.android.util.JSONClient)
	&& args (application, uri, requestParams)
	&& execution (public static JSONObject JSONClient.connect(ChaseApplication, String, Hashtable<String, String>));
	
	before (ChaseApplication application, String uri, Hashtable<String, String> requestParams) 
	: insertTrackingDataIntoRequest(application, uri, requestParams) {
		
	    setAnalyticsDataToGWORequest(requestParams, uri);
	}
	
    pointcut insertTrackingDataIntoMultiPostRequest(ChaseApplication application,String serviceUrl, Hashtable<String, String> formData, String[] fileNames,
        byte[][] attachments, String[] mimeTypes, String channelId)
    :within (com.chase.sig.android.util.JSONClient)
    && args(application, serviceUrl, formData, fileNames, attachments, mimeTypes, channelId)
    && execution (public static JSONObject JSONClient.sendMultiPartPost(ChaseApplication, String, Hashtable<String, String>, String[], 
                                                                byte[][], String[], String));

    before(ChaseApplication application, String serviceUrl, Hashtable<String, String> formData,
        String[] fileNames, byte[][] attachments, String[] mimeTypes, String channelId)
    : insertTrackingDataIntoMultiPostRequest(application, serviceUrl, formData, fileNames, attachments, mimeTypes, channelId) {
        
        setAnalyticsDataToGWORequest(formData, serviceUrl);
    }
    
    pointcut insertTrackingDataIntoPostRequest(JPService jpService, String uri, Hashtable<String, String> request, Class<?> clazz)
    : target(jpService) 
    && within(com.chase.sig.android.service.JPService)
    && args(uri, request, clazz)
    && execution(protected * post(String, Hashtable<String, String>, Class<*>));
    
    before(JPService jpService, String uri, Hashtable<String, String> request, Class<?> clazz)
    : insertTrackingDataIntoPostRequest(jpService, uri, request, clazz) {
        
        setAnalyticsDataToGWORequest(request, uri);
    }
    
    private void setAnalyticsDataToGWORequest(Hashtable<String, String> requestParameters, String uri) {
        
        if (uri.contains(GWO_PATH)) {
            
            AspectAnalyticsUtil.setBandwidthForTrackingData();
            AspectAnalyticsUtil.setTrackingDataToRequest(requestParameters);
        }
    }
}
