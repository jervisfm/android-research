package android.support.v4.accessibilityservice;

abstract interface AccessibilityServiceInfoCompat$AccessibilityServiceInfoVersionImpl{
    abstract public boolean getCanRetrieveWindowContent(android.accessibilityservice.AccessibilityServiceInfo arg);
    
    
    abstract public String getDescription(android.accessibilityservice.AccessibilityServiceInfo arg);
    
    
    abstract public String getId(android.accessibilityservice.AccessibilityServiceInfo arg);
    
    
    abstract public android.content.pm.ResolveInfo getResolveInfo(android.accessibilityservice.AccessibilityServiceInfo arg);
    
    
    abstract public String getSettingsActivityName(android.accessibilityservice.AccessibilityServiceInfo arg);
}