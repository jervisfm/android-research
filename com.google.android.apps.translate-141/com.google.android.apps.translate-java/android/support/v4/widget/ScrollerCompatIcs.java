package android.support.v4.widget;

import android.widget.Scroller;

class ScrollerCompatIcs
{
  public static float getCurrVelocity(Scroller paramScroller)
  {
    return paramScroller.getCurrVelocity();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     android.support.v4.widget.ScrollerCompatIcs
 * JD-Core Version:    0.6.2
 */