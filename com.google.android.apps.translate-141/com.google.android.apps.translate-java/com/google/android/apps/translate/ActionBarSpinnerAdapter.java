package com.google.android.apps.translate;

import android.app.ActionBar;
import android.app.ActionBar.OnNavigationListener;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import com.google.android.apps.translate.conversation.ConversationFragment;
import com.google.android.apps.translate.editor.PreImeAutoCompleteTextView.OnKeyPreImeListener;
import com.google.android.apps.translate.history.HistoryFragment;
import com.google.android.apps.translate.translation.TranslateEntry;
import com.google.android.apps.translate.translation.TranslateFragment;
import com.google.android.apps.translate.translation.TranslateFragmentWrapper;
import com.google.android.apps.translate.translation.TranslateHelper;

public class ActionBarSpinnerAdapter extends ArrayAdapter<String>
  implements SpinnerAdapter, ActionBar.OnNavigationListener
{
  public static final String CONVERSATION_FRAGMENT_TAG = "conversation";
  public static final int CONVERSATION_ITEM_POSITION = 1;
  public static final String FAVORITE_FRAGMENT_TAG = "favorite";
  public static final int FAVORITE_ITEM_POSITION = 3;
  public static final String HISTORY_FRAGMENT_TAG = "history";
  public static final int HISTORY_ITEM_POSITION = 2;
  private static final boolean SHOW_SMS = false;
  public static final String SMS_PICKER_FRAGMENT_TAG = "sms";
  public static final int SMS_PICKER_ITEM_POSITION = 4;
  private static final String TAG = "ActionBarSpinnerAdapter";
  public static final String TRANSATE_FRAGMENT_TAG = "translate";
  public static final int TRANSLATE_ITEM_POSITION;
  public static final boolean USE_HISTORY_STACK;
  private Activity mActivity;
  private ConversationFragment mConversationFragment;
  private String mConversationItem;
  private Fragment mCurrentFragment;
  private HistoryFragment mFavoriteFragment;
  private String mFavoriteItem;
  private HistoryFragment mHistoryFragment;
  private String mHistoryItem;
  private int mSelectedPosition = -1;
  private SmsPickerFragment mSmsPickerFragment;
  private String mSmsPickerItem;
  private boolean mTabMenuMode = false;
  private TranslateFragment mTranslateFragment;
  private TranslateFragmentWrapper mTranslateFragmentWrapper;
  private String mTranslateItem;

  public ActionBarSpinnerAdapter(Context paramContext, int paramInt1, int paramInt2, boolean paramBoolean, TranslateFragmentWrapper paramTranslateFragmentWrapper)
  {
    super(paramContext, paramInt1, paramInt2);
    Logger.d("ActionBarSpinnerAdapter", "ActionBarSpinnerAdapter");
    this.mActivity = ((Activity)paramContext);
    this.mTranslateFragmentWrapper = paramTranslateFragmentWrapper;
    this.mTranslateItem = this.mActivity.getString(R.string.btn_translate);
    this.mConversationItem = this.mActivity.getString(R.string.menu_conversation_mode);
    this.mHistoryItem = this.mActivity.getString(R.string.label_history);
    this.mFavoriteItem = this.mActivity.getString(R.string.label_favorites);
    this.mSmsPickerItem = this.mActivity.getString(R.string.title_sms_translation);
    clear();
    add(this.mTranslateItem);
    add(this.mConversationItem);
    add(this.mHistoryItem);
    add(this.mFavoriteItem);
    setTabMenuMode(paramBoolean);
    onNavigationItemSelected(0, 0L);
  }

  private void postProcessPreviousFragment(int paramInt1, int paramInt2)
  {
    switch (paramInt1)
    {
    case 0:
    case 4:
    default:
    case 1:
    case 2:
    case 3:
    }
    do
    {
      do
      {
        do
          return;
        while (paramInt1 == paramInt2);
        this.mConversationFragment = null;
        return;
      }
      while (paramInt1 == paramInt2);
      this.mHistoryFragment = null;
      return;
    }
    while (paramInt1 == paramInt2);
    this.mFavoriteFragment = null;
  }

  public static void removeActionBarTitle(ActionBar paramActionBar)
  {
    Logger.d("ActionBarSpinnerAdapter", "removeActionBarTitle");
    paramActionBar.setTitle("");
    paramActionBar.setDisplayShowCustomEnabled(false);
    paramActionBar.setDisplayShowTitleEnabled(false);
  }

  public static void setActionBarTitle(Activity paramActivity, String paramString)
  {
    Logger.d("ActionBarSpinnerAdapter", "setActionBarTitle");
    ActionBar localActionBar = paramActivity.getActionBar();
    localActionBar.setDisplayShowCustomEnabled(true);
    localActionBar.setDisplayShowTitleEnabled(false);
    TextView localTextView = (TextView)((LayoutInflater)paramActivity.getSystemService("layout_inflater")).inflate(R.layout.action_bar_spinner_item, null);
    localTextView.setText(paramString);
    localActionBar.setCustomView(localTextView);
  }

  private void startConversationFragment(TranslateEntry paramTranslateEntry)
  {
    Logger.d("ActionBarSpinnerAdapter", "startConversationFragment");
    if (this.mConversationFragment == null)
      this.mConversationFragment = new ConversationFragment(paramTranslateEntry);
    switchFragment(R.id.fragment_container, this.mConversationFragment, "conversation");
  }

  private void startFavoriteFragment()
  {
    if (this.mFavoriteFragment == null)
      this.mFavoriteFragment = new HistoryFragment(false, true);
    switchFragment(R.id.fragment_container, this.mFavoriteFragment, "favorite");
  }

  private void startHistoryFragment()
  {
    if (this.mHistoryFragment == null)
      this.mHistoryFragment = new HistoryFragment(true, true);
    switchFragment(R.id.fragment_container, this.mHistoryFragment, "history");
  }

  private void startSmsPickerFragment()
  {
    Logger.d("ActionBarSpinnerAdapter", "startSmsPickerFragment");
    if (this.mSmsPickerFragment == null)
      this.mSmsPickerFragment = new SmsPickerFragment();
    switchFragment(R.id.fragment_container, this.mSmsPickerFragment, "sms");
  }

  private void startTranslateFragment()
  {
    Logger.d("ActionBarSpinnerAdapter", "startTranslateFragment");
    if (this.mTranslateFragment == null)
      this.mTranslateFragment = new TranslateFragment(this.mTranslateFragmentWrapper, this);
    switchFragment(R.id.fragment_container, this.mTranslateFragment, "translate");
  }

  private void switchFragment(int paramInt, Fragment paramFragment, String paramString)
  {
    FragmentTransaction localFragmentTransaction = this.mActivity.getFragmentManager().beginTransaction();
    localFragmentTransaction.replace(paramInt, paramFragment, paramString);
    localFragmentTransaction.commit();
    this.mCurrentFragment = paramFragment;
  }

  public Fragment getCurrentFragment()
  {
    return this.mCurrentFragment;
  }

  public View getDropDownView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    View localView = super.getDropDownView(paramInt, paramView, paramViewGroup);
    TextView localTextView = (TextView)localView.findViewById(R.id.spinner_item_text_view);
    int i = localTextView.getPaddingRight();
    localTextView.setPadding(i, i, i, i);
    localView.setBackgroundResource(R.drawable.bg_action_bar);
    switch (paramInt)
    {
    default:
      return localView;
    case 0:
      localTextView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.spinner_icon_translate_bw, 0, 0, 0);
      return localView;
    case 1:
      localTextView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.spinner_icon_conversation_bw, 0, 0, 0);
      return localView;
    case 2:
      localTextView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.spinner_icon_history_bw, 0, 0, 0);
      return localView;
    case 3:
      localTextView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.spinner_icon_starred_bw, 0, 0, 0);
      return localView;
    case 4:
    }
    localTextView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.spinner_icon_sms_translation_bw, 0, 0, 0);
    return localView;
  }

  public int getSelectedNavigationIndex()
  {
    if ((this.mCurrentFragment == null) || ((this.mCurrentFragment instanceof TranslateFragment)));
    do
    {
      return 0;
      if ((this.mCurrentFragment instanceof ConversationFragment))
        return 1;
      if ((this.mCurrentFragment instanceof HistoryFragment))
      {
        if (((HistoryFragment)this.mCurrentFragment).isHistoryMode())
          return 2;
        return 3;
      }
    }
    while (!(this.mCurrentFragment instanceof SmsPickerFragment));
    return 4;
  }

  public TranslateFragment getTranslateFragment()
  {
    return this.mTranslateFragment;
  }

  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    if (this.mTabMenuMode)
      return getDropDownView(paramInt, paramView, paramViewGroup);
    return super.getView(paramInt, paramView, paramViewGroup);
  }

  public boolean isTabMenuMode()
  {
    return this.mTabMenuMode;
  }

  public boolean onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    Logger.d("ActionBarSpinnerAdapter", "onActivityResult");
    switch (this.mSelectedPosition)
    {
    default:
      return false;
    case 0:
    case 1:
    }
    if (this.mCurrentFragment != null)
      this.mCurrentFragment.onActivityResult(paramInt1, paramInt2, paramIntent);
    return true;
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if (((this.mCurrentFragment instanceof PreImeAutoCompleteTextView.OnKeyPreImeListener)) && (((PreImeAutoCompleteTextView.OnKeyPreImeListener)this.mCurrentFragment).onKeyPreIme(paramInt, paramKeyEvent)))
      return true;
    if (paramInt == 4)
      this.mActivity.getActionBar();
    return false;
  }

  public boolean onNavigationItemSelected(int paramInt, long paramLong)
  {
    Logger.d("ActionBarSpinnerAdapter", "onNavigationItemSelected itemPosition=" + paramInt + " itemId=" + paramLong + " item=" + (String)getItem(paramInt));
    postProcessPreviousFragment(this.mSelectedPosition, paramInt);
    if (this.mSelectedPosition == paramInt)
      return true;
    this.mSelectedPosition = paramInt;
    switch (paramInt)
    {
    default:
      return false;
    case 0:
      startTranslateFragment();
      return true;
    case 1:
      startConversationFragment(this.mTranslateFragment.getHelper().getInitialTranslationEntry());
      return true;
    case 2:
      startHistoryFragment();
      return true;
    case 3:
      startFavoriteFragment();
      return true;
    case 4:
    }
    startSmsPickerFragment();
    return true;
  }

  public void setTabMenuMode(boolean paramBoolean)
  {
    this.mTabMenuMode = paramBoolean;
    SdkVersionWrapper.getWrapper().setActionBarTitle(this.mActivity, this);
    LinearLayout localLinearLayout = (LinearLayout)this.mActivity.getWindow().getDecorView().findViewById(R.id.tab_menu_fake_list);
    if (paramBoolean);
    for (int i = 4; ; i = 8)
    {
      localLinearLayout.setVisibility(i);
      return;
    }
  }

  public void setTabWidth()
  {
    Logger.d("ActionBarSpinnerAdapter", "setTabWidth");
    if (!this.mTabMenuMode)
    {
      Logger.d("ActionBarSpinnerAdapter", "setTabWidth BYE!");
      return;
    }
    final View localView = this.mActivity.getWindow().getDecorView();
    final LinearLayout localLinearLayout = (LinearLayout)localView.findViewById(R.id.tab_menu_fragment_container);
    localLinearLayout.post(new Runnable()
    {
      public void run()
      {
        int i = localView.findViewById(R.id.tab_menu_fake_list).getWidth();
        if (i > 0)
        {
          ViewGroup.LayoutParams localLayoutParams = localLinearLayout.getLayoutParams();
          localLayoutParams.width = i;
          localLinearLayout.setLayoutParams(localLayoutParams);
          localView.invalidate();
        }
      }
    });
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.ActionBarSpinnerAdapter
 * JD-Core Version:    0.6.2
 */