package com.google.android.apps.translate;

import java.util.regex.Pattern;

public class Constants
{
  public static final String AFRIKAANS_SHORT_NAME = "af";
  public static final String ALBANIAN_SHORT_NAME = "sq";
  public static final String APP_DESCRIPTION_NAME = "AndroidTranslate";
  public static final String APP_PACKAGE_NAME = "com.google.android.apps.translate";
  public static final String ARABIC_SHORT_NAME = "ar";
  public static final String ARMENIAN_SHORT_NAME = "hy";
  public static final String AUTO_SHORT_NAME = "auto";
  public static final String AZERBAIJANI_SHORT_NAME = "az";
  public static final String BASQUE_SHORT_NAME = "eu";
  public static final String BELARUSIAN_SHORT_NAME = "be";
  public static final String BENGALI_SHORT_NAME = "bn";
  public static final String BOSNIAN_SHORT_NAME = "bs";
  public static final String BROADCAST_LANGUAGES_CHANGED = "com.google.android.apps.translate.broadcast.LANGUAGES_CHANGED";
  public static final String BULGARIAN_SHORT_NAME = "bg";
  public static final String CATALAN_SHORT_NAME = "ca";
  public static final String CHINESE_MANDARIN_CN = "cmn-Hans-CN";
  public static final String CHINESE_MANDARIN_TW = "cmn-Hant-TW";
  public static final String CHINESE_SIMPLIFIED_SHORT_NAME = "zh-CN";
  public static final String CHINESE_TRADITIONAL_SHORT_NAME = "zh-TW";
  public static final String CHINESE_YUE_HK = "yue-Hant-HK";
  public static final Pattern CJK_PATTERN = Pattern.compile("[\\p{InHangulJamo}\\p{InHangulCompatibilityJamo}\\p{InHangulSyllables}\\p{InCjkRadicalsSupplement}\\p{InKangxiRadicals}\\p{InIdeographicDescriptionCharacters}\\p{InCjkSymbolsAndPunctuation}\\p{InEnclosedCjkLettersAndMonths}\\p{InCjkUnifiedIdeographs}\\p{InCjkUnifiedIdeographsExtensionA}\\p{InCjkUnifiedIdeographsExtensionB}\\p{InHiragana}\\p{InKatakana}]");
  public static final String CJK_REGEX = "[\\p{InHangulJamo}\\p{InHangulCompatibilityJamo}\\p{InHangulSyllables}\\p{InCjkRadicalsSupplement}\\p{InKangxiRadicals}\\p{InIdeographicDescriptionCharacters}\\p{InCjkSymbolsAndPunctuation}\\p{InEnclosedCjkLettersAndMonths}\\p{InCjkUnifiedIdeographs}\\p{InCjkUnifiedIdeographsExtensionA}\\p{InCjkUnifiedIdeographsExtensionB}\\p{InHiragana}\\p{InKatakana}]";
  public static final String CJK_REGEX_UNBRACKED = "\\p{InHangulJamo}\\p{InHangulCompatibilityJamo}\\p{InHangulSyllables}\\p{InCjkRadicalsSupplement}\\p{InKangxiRadicals}\\p{InIdeographicDescriptionCharacters}\\p{InCjkSymbolsAndPunctuation}\\p{InEnclosedCjkLettersAndMonths}\\p{InCjkUnifiedIdeographs}\\p{InCjkUnifiedIdeographsExtensionA}\\p{InCjkUnifiedIdeographsExtensionB}\\p{InHiragana}\\p{InKatakana}";
  public static final String CROATIAN_SHORT_NAME = "hr";
  public static final String CZECH_SHORT_NAME = "cs";
  public static final String DANISH_SHORT_NAME = "da";
  public static final int DEF_VERSION_CODE = 0;
  public static final String DEF_VERSION_NAME = "0.0.0";
  public static final String DUTCH_SHORT_NAME = "nl";
  public static final boolean ENABLE_SUGGEST = true;
  public static final String ENGLISH_SHORT_NAME = "en";
  public static final String ESPERANTO_SHORT_NAME = "eo";
  public static final String ESTONIAN_SHORT_NAME = "et";
  public static final String FILIPINO_SHORT_NAME = "tl";
  public static final String FINNISH_SHORT_NAME = "fi";
  public static final String FRENCH_SHORT_NAME = "fr";
  public static final String GALICIAN_SHORT_NAME = "gl";
  public static final String GEORGIAN_SHORT_NAME = "ka";
  public static final String GERMAN_SHORT_NAME = "de";
  public static final String GREEK_SHORT_NAME = "el";
  public static final String GUJARATI_SHORT_NAME = "gu";
  public static final String HAITIAN_CREOLE_SHORT_NAME = "ht";
  public static final String HEBREW_SHORT_NAME = "iw";
  public static final String HINDI_SHORT_NAME = "hi";
  public static final String HUNGARIAN_SHORT_NAME = "hu";
  public static final String ICELANDIC_SHORT_NAME = "is";
  public static final String INDONESIAN_SHORT_NAME = "id";
  public static final String IRISH_SHORT_NAME = "ga";
  public static final String ITALIAN_SHORT_NAME = "it";
  public static final String JAPANESE_SHORT_NAME = "ja";
  public static final String KANNADA_SHORT_NAME = "kn";
  public static final String KEY_CURRENT_TAB = "key_current_tab";
  public static final String KEY_CURRENT_TRANSLATION = "key_current_translation";
  public static final String KEY_FLUSH_ON_PAUSE = "flush_on_pause";
  public static final String KEY_FROM_FLOATING_WINDOW = "key_from_floating_window";
  public static final String KEY_HISTORY_MODE = "history";
  public static final String KEY_IS_SOURCE_LEFT_LANGUAGE = "key_is_source_left_language";
  public static final String KEY_LANGUAGE_FROM = "key_language_from";
  public static final String KEY_LANGUAGE_FROM_LONG_NAME = "key_language_from_long_name";
  public static final String KEY_LANGUAGE_FROM_SHORT_NAME = "key_language_from_short_name";
  public static final String KEY_LANGUAGE_TO = "key_language_to";
  public static final String KEY_LANGUAGE_TO_LONG_NAME = "key_language_to_long_name";
  public static final String KEY_LANGUAGE_TO_SHORT_NAME = "key_language_to_short_name";
  public static final String KEY_NO_TRANSITION_ANIMATION = "key_no_transition_animation";
  public static final String KEY_PENDING_INTENT = "key_pending_intent";
  public static final String KEY_SUPERSIZE_BY_GESTURE = "key_supersize_by_gesture";
  public static final String KEY_TEXT_INPUT = "key_text_input";
  public static final String KEY_TEXT_OUTPUT = "key_text_output";
  public static final String KEY_TRANSLATE_TEXT = "key_translate_text";
  public static final String KEY_VOICE_EDIT = "key_voice_edit";
  public static final String KOREAN_SHORT_NAME = "ko";
  public static final String KURDISH_SHORT_NAME = "ku";
  public static final String LAO_SHORT_NAME = "lo";
  public static final String LATIN_SHORT_NAME = "la";
  public static final String LATVIAN_SHORT_NAME = "lv";
  public static final String LITHUANIAN_SHORT_NAME = "lt";
  public static final boolean LOAD_FROM_DB_ONLY_AFTER_FAILURE = true;
  public static final String MACEDONIAN_SHORT_NAME = "mk";
  public static final String MALAY_SHORT_NAME = "ms";
  public static final String MALTESE_SHORT_NAME = "mt";
  public static final int MAXIMUM_SUGGESTION_ITEMS = 20;
  public static final String NON_CJK_REGEX = "[^\\p{InHangulJamo}\\p{InHangulCompatibilityJamo}\\p{InHangulSyllables}\\p{InCjkRadicalsSupplement}\\p{InKangxiRadicals}\\p{InIdeographicDescriptionCharacters}\\p{InCjkSymbolsAndPunctuation}\\p{InEnclosedCjkLettersAndMonths}\\p{InCjkUnifiedIdeographs}\\p{InCjkUnifiedIdeographsExtensionA}\\p{InCjkUnifiedIdeographsExtensionB}\\p{InHiragana}\\p{InKatakana}]";
  public static final String NORWEGIAN_SHORT_NAME = "no";
  public static final String NULL_SHORT_NAME = "null";
  public static final int ORDER_ALPHABETICAL = 0;
  public static final int ORDER_BY_TIME = 1;
  public static final String PERSIAN_SHORT_NAME = "fa";
  public static final String[] PICO_SUPPORTED_LANGUAGES = { "de", "en", "es", "fr", "it" };
  public static final String POLISH_SHORT_NAME = "pl";
  public static final String PORTUGUESE_SHORT_NAME = "pt";
  public static final int REQUEST_FROM_SETTINGS_TO_EULA = 160;
  public static final int REQUEST_HANDWRITING_INPUT = 130;
  public static final int REQUEST_LOAD_IMAGE = 182;
  public static final int REQUEST_S2S_CONVERSATION_INPUT = 181;
  public static final int REQUEST_TRANSLATE_SMS = 170;
  public static final int REQUEST_VOICE_INPUT = 100;
  public static final int RESULT_VOICE_INPUT_NOT_SUPPORTED = 101;
  public static final String ROMANIAN_SHORT_NAME = "ro";
  public static final String RUSSIAN_SHORT_NAME = "ru";
  public static final String SELECT_SHORT_NAME = "select";
  public static final String SERBIAN_SHORT_NAME = "sr";
  public static final boolean SHOW_TRANSLATION_IN_SUGGEST = false;
  public static final String SLOVAK_SHORT_NAME = "sk";
  public static final String SLOVENIAN_SHORT_NAME = "sl";
  public static final String SPANISH_SHORT_NAME = "es";
  public static final String SWAHILI_SHORT_NAME = "sw";
  public static final String SWEDISH_SHORT_NAME = "sv";
  public static final String TAMIL_SHORT_NAME = "ta";
  public static final String TELUGU_SHORT_NAME = "te";
  public static final ExternalFonts.style[] TEXT_APPEARANCE;
  public static final float TEXT_SIZE_UNCHANGED = 0.0F;
  public static final String THAI_SHORT_NAME = "th";
  public static final String TRANSLATE_DATA_SEPARATOR = "\t";
  public static final String TURKISH_SHORT_NAME = "tr";
  public static final String UKRAINIAN_SHORT_NAME = "uk";
  public static final String URDU_SHORT_NAME = "ur";
  public static final String VIETNAMESE_SHORT_NAME = "vi";
  public static final String WELSH_SHORT_NAME = "cy";
  public static final String YIDDISH_SHORT_NAME = "yi";

  static
  {
    ExternalFonts.style[] arrayOfstyle = new ExternalFonts.style[4];
    arrayOfstyle[0] = ExternalFonts.style.TextAppearance_Medium;
    arrayOfstyle[1] = ExternalFonts.style.TextAppearance_Default;
    arrayOfstyle[2] = ExternalFonts.style.TextAppearance_Default;
    arrayOfstyle[3] = ExternalFonts.style.TextAppearance_Medium;
    TEXT_APPEARANCE = arrayOfstyle;
  }

  public static enum AppearanceType
  {
    static
    {
      ENTRY_SUMMARY = new AppearanceType("ENTRY_SUMMARY", 1);
      DICTIONARY_RESULT = new AppearanceType("DICTIONARY_RESULT", 2);
      INPUT_PANEL = new AppearanceType("INPUT_PANEL", 3);
      TRANSLATE_TO = new AppearanceType("TRANSLATE_TO", 4);
      UNCHANGED = new AppearanceType("UNCHANGED", 5);
      AppearanceType[] arrayOfAppearanceType = new AppearanceType[6];
      arrayOfAppearanceType[0] = ENTRY_TITLE;
      arrayOfAppearanceType[1] = ENTRY_SUMMARY;
      arrayOfAppearanceType[2] = DICTIONARY_RESULT;
      arrayOfAppearanceType[3] = INPUT_PANEL;
      arrayOfAppearanceType[4] = TRANSLATE_TO;
      arrayOfAppearanceType[5] = UNCHANGED;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.Constants
 * JD-Core Version:    0.6.2
 */