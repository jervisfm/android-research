package com.google.android.apps.translate;

import android.content.Context;
import android.os.SystemClock;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;

public class CsiTimer
{
  private static final String ACTION_PARAM = "action";
  private static final String ACTION_VAR = "&action=";
  public static final String CSI_ACTION_APPLICATION_LOAD = "app";
  public static final String CSI_ACTION_HOME_LOAD = "home";
  public static final String CSI_ACTION_NETWORK_TTS = "nts";
  public static final String CSI_ACTION_TRANSLATE = "trs";
  public static final String CSI_INTERVAL_INIT_TIME = "init";
  public static final String CSI_INTERVAL_PROCESSING_TIME = "t";
  private static final int DEFAULT_CSI_VERSION = 2;
  private static final String EMPTY_PARAM_VALUE = "";
  private static final boolean ENABLE_CSI_REPORT = true;
  private static final String INTERVAL_PARAM = "it";
  private static final String INTERVAL_VAR = "&it=";
  private static final String SERVER_URL = "http://csi.gstatic.com/csi?";
  private static final String SERVICE_NAME = "atrans";
  private static final String SERVICE_PARAM = "s";
  private static final String SERVICE_VAR = "&s=";
  private static final String TAG = "CsiTimer";
  private static final String VERSION_PARAM = "v";
  private static final String VERSION_VAR = "v=";
  private final String mActionName;
  private final HashMap<String, String> mIntervalTimes = Maps.newHashMap();
  private final String mServerUrl;
  private final String mServiceName;
  private final HashMap<String, Long> mStartTimes = Maps.newHashMap();

  public CsiTimer(CsiTimer paramCsiTimer)
  {
    this(paramCsiTimer.mServiceName, paramCsiTimer.mServerUrl, paramCsiTimer.mActionName);
    Logger.d("CsiTimer", "reset oldaction=" + paramCsiTimer.getActionName());
  }

  public CsiTimer(CsiTimer paramCsiTimer, String paramString)
  {
    this(paramCsiTimer.mServiceName, paramCsiTimer.mServerUrl, paramString);
    Logger.d("CsiTimer", "reset oldaction=" + paramCsiTimer.getActionName() + " action=" + paramString);
  }

  public CsiTimer(String paramString)
  {
    this("atrans", "http://csi.gstatic.com/csi?", paramString);
    Logger.d("CsiTimer", "reset action=" + paramString);
  }

  public CsiTimer(String paramString1, String paramString2, String paramString3)
  {
    this.mServiceName = ((String)Preconditions.checkNotNull(paramString1));
    this.mServerUrl = ((String)Preconditions.checkNotNull(paramString2));
    this.mActionName = ((String)Preconditions.checkNotNull(paramString3));
  }

  private void beginInternal(Long paramLong, boolean paramBoolean, String[] paramArrayOfString)
  {
    int i = paramArrayOfString.length;
    for (int j = 0; j < i; j++)
    {
      String str = paramArrayOfString[j];
      Logger.d("CsiTimer", "beginInternal action=" + this.mActionName + " interval=" + str);
      if ((paramBoolean) || (!this.mStartTimes.containsKey(str)))
      {
        this.mStartTimes.put(str, paramLong);
        log("start -- (" + this.mStartTimes.size() + ") -- " + str + " : " + paramLong);
      }
    }
  }

  private String buildUrl(String paramString)
  {
    StringBuilder localStringBuilder = new StringBuilder(this.mServerUrl);
    localStringBuilder.append("v=").append(2);
    localStringBuilder.append("&s=").append(this.mServiceName);
    if (!this.mActionName.equals(""))
      localStringBuilder.append("&action=").append(this.mActionName);
    if (!this.mIntervalTimes.isEmpty())
    {
      localStringBuilder.append("&it=");
      Iterator localIterator = this.mIntervalTimes.entrySet().iterator();
      while (localIterator.hasNext())
      {
        Map.Entry localEntry = (Map.Entry)localIterator.next();
        localStringBuilder.append((String)localEntry.getKey() + "." + (String)localEntry.getValue() + ",");
      }
      localStringBuilder.deleteCharAt(-1 + localStringBuilder.length());
    }
    localStringBuilder.append(paramString);
    return localStringBuilder.toString();
  }

  private void log(String paramString)
  {
    Logger.d(this.mActionName + " | " + paramString);
  }

  private void log(String paramString, Exception paramException)
  {
    Logger.d(this.mActionName + " | " + paramString, paramException);
  }

  private boolean runReport(HttpClient paramHttpClient, String paramString)
  {
    long l1 = SystemClock.elapsedRealtime();
    if (this.mIntervalTimes.isEmpty())
      return false;
    String str = buildUrl(paramString);
    log("report -- url = " + str);
    HttpGet localHttpGet = new HttpGet(str);
    try
    {
      HttpResponse localHttpResponse = paramHttpClient.execute(localHttpGet);
      if (localHttpResponse.getStatusLine().getStatusCode() != 204)
      {
        log("report -- not 204 response :: " + localHttpResponse.getStatusLine().toString());
        return false;
      }
    }
    catch (ClientProtocolException localClientProtocolException)
    {
      log("report -- exception 1 :: ", localClientProtocolException);
      return false;
    }
    catch (IOException localIOException)
    {
      log("report -- exception 2 :: ", localIOException);
      return false;
    }
    long l2 = SystemClock.elapsedRealtime();
    log("report -- success (time " + (l2 - l1) + ")");
    return true;
  }

  public void begin(Long paramLong, String[] paramArrayOfString)
  {
    beginInternal(paramLong, true, paramArrayOfString);
  }

  public void begin(String[] paramArrayOfString)
  {
    beginInternal(Long.valueOf(SystemClock.elapsedRealtime()), false, paramArrayOfString);
  }

  public void end(String[] paramArrayOfString)
  {
    long l1 = SystemClock.elapsedRealtime();
    int i = paramArrayOfString.length;
    for (int j = 0; j < i; j++)
    {
      String str = paramArrayOfString[j];
      Logger.d("CsiTimer", "end action=" + this.mActionName + " interval=" + str);
      if ((this.mStartTimes.containsKey(str)) && (!this.mIntervalTimes.containsKey(str)))
      {
        long l2 = l1 - ((Long)this.mStartTimes.get(str)).longValue();
        this.mIntervalTimes.put(str, Long.toString(l2));
        log("end -- add(" + this.mIntervalTimes.size() + ") -- " + str + " : " + l2);
      }
    }
  }

  public String getActionName()
  {
    return this.mActionName;
  }

  public void recordInterval(String paramString, int paramInt)
  {
    this.mIntervalTimes.put(paramString, Integer.toString(paramInt));
    log("interval -- add(" + this.mIntervalTimes.size() + ") -- " + paramString + " : " + paramInt);
  }

  public void report(Context paramContext)
  {
    report(Util.newHttpClient(Util.generateUserAgentName(paramContext)));
  }

  public void report(HttpClient paramHttpClient)
  {
    report(paramHttpClient, "");
  }

  public void report(final HttpClient paramHttpClient, final String paramString)
  {
    Logger.d("CsiTimer", "report action=" + this.mActionName);
    new Thread(new Runnable()
    {
      public void run()
      {
        CsiTimer.this.runReport(paramHttpClient, paramString);
      }
    }).start();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.CsiTimer
 * JD-Core Version:    0.6.2
 */