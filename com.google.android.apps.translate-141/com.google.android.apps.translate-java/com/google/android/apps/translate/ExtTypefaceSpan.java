package com.google.android.apps.translate;

import android.graphics.Paint;
import android.graphics.Typeface;
import android.text.TextPaint;
import android.text.style.MetricAffectingSpan;

public class ExtTypefaceSpan extends MetricAffectingSpan
{
  private boolean mIsChangeTextSize;
  private float mTextSize;
  private Typeface mTypeface = null;

  public ExtTypefaceSpan(Typeface paramTypeface)
  {
    set(paramTypeface, 0.0F);
  }

  public ExtTypefaceSpan(Typeface paramTypeface, float paramFloat)
  {
    set(paramTypeface, paramFloat);
  }

  private void applyTypeface(Paint paramPaint)
  {
    Typeface localTypeface = paramPaint.getTypeface();
    if (localTypeface == null);
    for (int i = 0; ; i = localTypeface.getStyle())
    {
      int j = i & (0xFFFFFFFF ^ this.mTypeface.getStyle());
      if ((j & 0x1) != 0)
        paramPaint.setFakeBoldText(true);
      if ((j & 0x2) != 0)
        paramPaint.setTextSkewX(-0.25F);
      if (this.mIsChangeTextSize)
        paramPaint.setTextSize(this.mTextSize);
      paramPaint.setTypeface(this.mTypeface);
      return;
    }
  }

  public void set(Typeface paramTypeface, float paramFloat)
  {
    this.mTypeface = ((Typeface)Preconditions.checkNotNull(paramTypeface));
    if (paramFloat > 0.0F);
    for (boolean bool = true; ; bool = false)
    {
      this.mIsChangeTextSize = bool;
      this.mTextSize = paramFloat;
      return;
    }
  }

  public void updateDrawState(TextPaint paramTextPaint)
  {
    applyTypeface(paramTextPaint);
  }

  public void updateMeasureState(TextPaint paramTextPaint)
  {
    applyTypeface(paramTextPaint);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.ExtTypefaceSpan
 * JD-Core Version:    0.6.2
 */