package com.google.android.apps.translate;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.text.style.TextAppearanceSpan;

public class ExternalFonts
{
  private static final ExternalFonts ARABIC = new ExternalFonts("ScheherazadeRegOT.ttf");
  private static final float ARABIC_FONT_SCALE = 1.25F;
  private static final ExternalFonts DEFAULT = new ExternalFonts(Typeface.DEFAULT);
  private static final ExternalFonts HEBREW = new ExternalFonts("SILEOTSR.ttf");
  private static final float HEBREW_FONT_SCALE = 1.0F;
  private static AssetManager sAssets = null;
  private final String mFontName;
  private float[] mTextSize;
  private Typeface mTtf = null;

  public ExternalFonts(Typeface paramTypeface)
  {
    this("default");
    this.mTtf = paramTypeface;
  }

  public ExternalFonts(String paramString)
  {
    this.mFontName = paramString;
  }

  private AssetManager getAssets()
  {
    return sAssets;
  }

  private static ExternalFonts getFontByLanguage(Language paramLanguage)
  {
    if (paramLanguage == null)
      return DEFAULT;
    return getFontByShortName(paramLanguage.getShortName());
  }

  public static ExternalFonts getFontByShortName(String paramString)
  {
    if (SdkVersionWrapper.getWrapper().useExternalFontsForUnsupportedLanguages())
    {
      if ("ar".equals(paramString))
        return ARABIC;
      if ("iw".equals(paramString))
        return HEBREW;
    }
    return DEFAULT;
  }

  private int getIndexOfTextSize(Constants.AppearanceType paramAppearanceType)
  {
    return Constants.TEXT_APPEARANCE[paramAppearanceType.ordinal()].ordinal();
  }

  public static boolean hasAssets()
  {
    return sAssets != null;
  }

  public static void initialize(AssetManager paramAssetManager, Context paramContext)
  {
    sAssets = (AssetManager)Preconditions.checkNotNull(paramAssetManager);
    int[] arrayOfInt = new int[5];
    arrayOfInt[0] = R.style.TextAppearance_Small;
    arrayOfInt[1] = R.style.TextAppearance_Default;
    arrayOfInt[2] = R.style.TextAppearance_Medium;
    arrayOfInt[3] = R.style.TextAppearance_Large;
    arrayOfInt[4] = R.style.TextAppearance_Huge;
    int i = arrayOfInt.length;
    DEFAULT.mTextSize = new float[i];
    for (int j = 0; j < i; j++)
    {
      TextAppearanceSpan localTextAppearanceSpan = new TextAppearanceSpan(paramContext, arrayOfInt[j]);
      DEFAULT.mTextSize[j] = localTextAppearanceSpan.getTextSize();
    }
    ARABIC.setDefaultTextSize(1.25F);
    HEBREW.setDefaultTextSize(1.0F);
  }

  public static boolean isUsingExternalFont(Language paramLanguage)
  {
    return getFontByLanguage(paramLanguage) != DEFAULT;
  }

  private void setDefaultTextSize(float paramFloat)
  {
    int i = DEFAULT.mTextSize.length;
    this.mTextSize = new float[i];
    for (int j = 0; j < i; j++)
      this.mTextSize[j] = (paramFloat * DEFAULT.mTextSize[j]);
  }

  public float getTextSize(Constants.AppearanceType paramAppearanceType)
  {
    if (paramAppearanceType == Constants.AppearanceType.UNCHANGED)
      return 0.0F;
    return this.mTextSize[getIndexOfTextSize(paramAppearanceType)];
  }

  public Typeface getTypeface()
  {
    if ((this.mTtf == null) && (hasAssets()))
      this.mTtf = Typeface.createFromAsset(getAssets(), this.mFontName);
    return this.mTtf;
  }

  public static enum style
  {
    static
    {
      TextAppearance_Medium = new style("TextAppearance_Medium", 1);
      TextAppearance_Default = new style("TextAppearance_Default", 2);
      TextAppearance_Large = new style("TextAppearance_Large", 3);
      TextAppearance_Huge = new style("TextAppearance_Huge", 4);
      style[] arrayOfstyle = new style[5];
      arrayOfstyle[0] = TextAppearance_Small;
      arrayOfstyle[1] = TextAppearance_Medium;
      arrayOfstyle[2] = TextAppearance_Default;
      arrayOfstyle[3] = TextAppearance_Large;
      arrayOfstyle[4] = TextAppearance_Huge;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.ExternalFonts
 * JD-Core Version:    0.6.2
 */