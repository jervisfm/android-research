package com.google.android.apps.translate;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.text.method.LinkMovementMethod;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import com.google.android.apps.translate.translation.TranslateFragmentWrapper;
import com.google.android.apps.translate.translation.TranslateHelper;
import java.util.ArrayList;
import java.util.Locale;

public class HomeActivity extends Activity
{
  private static final String BULLET_MARK = "•";
  private static final boolean EULA_TEST = false;
  private static final String TAG = "HomeActivity";
  private CsiTimer mCsiTimer;
  private Handler mHandler = new Handler();
  private boolean mInitRun;
  private final BroadcastReceiver mOnVoiceSupportedLanguagesResult = new BroadcastReceiver()
  {
    public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
    {
      Bundle localBundle = getResultExtras(true);
      ArrayList localArrayList = localBundle.getStringArrayList("android.speech.extra.SUPPORTED_LANGUAGES");
      String str1 = localBundle.getString("android.speech.extra.LANGUAGE_PREFERENCE");
      if (localArrayList != null)
        Profile.setSupportedVoiceLanguages(paramAnonymousContext, localArrayList);
      String str2;
      if (str1 != null)
      {
        str2 = VoiceInputHelper.getShortLanguageNameFromVoiceInputLanguage(str1);
        if (str2 != null)
        {
          Profile.setVoiceInputLanguage(paramAnonymousContext, str2, str1);
          if (!str2.equals("zh-TW"))
            break label77;
          Profile.setVoiceInputLanguage(paramAnonymousContext, "zh-CN", str1);
        }
      }
      label77: 
      while (!str2.equals("zh-CN"))
        return;
      Profile.setVoiceInputLanguage(paramAnonymousContext, "zh-TW", str1);
    }
  };
  private boolean mShowEula = false;
  private TranslateFragmentWrapper mTranslateFragment;
  private TranslateHelper mTranslateHelper;

  private void checkVersion()
  {
    int i = Profile.getCurrentVersionCode(this);
    int j = Util.getVersionCode(this);
    Logger.d("version code: " + j + ", " + i);
    if (i != j)
    {
      Object[] arrayOfObject = new Object[2];
      arrayOfObject[0] = Integer.valueOf(i);
      arrayOfObject[1] = Integer.valueOf(j);
      Logger.d(this, String.format("version code changed (from %d to %d)", arrayOfObject));
      if (i <= 10)
        Profile.resetFirstRunFlags(this);
      Profile.setCurrentVersionCode(this, j);
    }
  }

  public static String getWelcomeText(Context paramContext)
  {
    return String.format(paramContext.getString(R.string.welcome_text), new Object[] { "•", "•", "•", "•" });
  }

  private void reportCsiMetrics()
  {
    Logger.d("HomeActivity", "reportCsiMetrics");
    this.mCsiTimer.end(new String[] { "init" });
    this.mCsiTimer.report(this);
    this.mCsiTimer = new CsiTimer("home");
    ((TranslateApplication)getApplication()).endAndReportAppCsiTimer();
  }

  private void showEulaDialog()
  {
    try
    {
      View localView = ((LayoutInflater)getSystemService("layout_inflater")).inflate(R.layout.eula, null);
      ((TextView)localView.findViewById(R.id.welcome_text)).setText(getWelcomeText(this));
      ((TextView)localView.findViewById(R.id.tos_link)).setMovementMethod(LinkMovementMethod.getInstance());
      AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
      localBuilder.setTitle(getString(R.string.app_name));
      localBuilder.setIcon(R.drawable.icon);
      localBuilder.setView(localView);
      localBuilder.setInverseBackgroundForced(true);
      localBuilder.setOnCancelListener(new DialogInterface.OnCancelListener()
      {
        public void onCancel(DialogInterface paramAnonymousDialogInterface)
        {
          HomeActivity.this.finish();
        }
      });
      localBuilder.setPositiveButton(getString(R.string.btn_accept), new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          Profile.setEulaAccepted(HomeActivity.this, true);
          if (SdkVersionWrapper.getWrapper().useFragments());
          while (true)
          {
            paramAnonymousDialogInterface.dismiss();
            return;
            HomeActivity.this.startTranslateActivity();
          }
        }
      });
      localBuilder.setNegativeButton(getString(R.string.btn_decline), new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          HomeActivity.this.finish();
        }
      });
      localBuilder.create().show();
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  private void startTranslateActivity()
  {
    SdkVersionWrapper.getWrapper().setActionBarTitle(this, null);
    Util.openTranslateActivity(this, getIntent());
    finish();
  }

  private void stopCsiTimers()
  {
    Logger.d("HomeActivity", "stopCsiTimers");
    if ((getIntent().hasCategory("android.intent.category.LAUNCHER")) && (this.mInitRun) && (!this.mShowEula))
      this.mHandler.post(new Runnable()
      {
        public void run()
        {
          HomeActivity.this.reportCsiMetrics();
          HomeActivity.access$102(HomeActivity.this, false);
        }
      });
  }

  void clearEulaAcceptBit()
  {
    Profile.clearEulaAccepted(this);
  }

  public TranslateFragmentWrapper getTranslateFragmentWrapper()
  {
    return this.mTranslateFragment;
  }

  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    Logger.d("HomeActivity", "request: " + paramInt1 + ", result code: " + paramInt2);
    if (SdkVersionWrapper.getWrapper().useFragments())
      if (this.mTranslateFragment != null)
        this.mTranslateFragment.onActivityResult(paramInt1, paramInt2, paramIntent);
    while (this.mTranslateHelper == null)
      return;
    this.mTranslateHelper.onActivityResult(paramInt1, paramInt2, paramIntent);
  }

  public void onConfigurationChanged(Configuration paramConfiguration)
  {
    super.onConfigurationChanged(paramConfiguration);
    if (this.mTranslateFragment != null)
      this.mTranslateFragment.onConfigurationChanged(paramConfiguration);
  }

  public boolean onContextItemSelected(MenuItem paramMenuItem)
  {
    Logger.d("HomeActivity", "onContextItemSelected");
    return false;
  }

  protected void onCreate(Bundle paramBundle)
  {
    Logger.d("HomeActivity", "onCreate");
    this.mInitRun = true;
    this.mCsiTimer = new CsiTimer("home");
    this.mCsiTimer.begin(new String[] { "init" });
    super.onCreate(paramBundle);
    checkVersion();
    setVolumeControlStream(3);
    setContentView(getLayoutInflater().inflate(R.layout.home_activity, null));
    if (SdkVersionWrapper.getWrapper().useFragments())
    {
      this.mTranslateFragment = new TranslateFragmentWrapper(this);
      this.mTranslateHelper = this.mTranslateFragment.getHelper();
      if (!Profile.isEulaAccepted(this))
      {
        this.mShowEula = true;
        showEulaDialog();
        return;
      }
      stopCsiTimers();
      return;
    }
    if (!Profile.isEulaAccepted(this))
    {
      this.mShowEula = true;
      showEulaDialog();
      return;
    }
    stopCsiTimers();
    startTranslateActivity();
  }

  public void onCreateContextMenu(ContextMenu paramContextMenu, View paramView, ContextMenu.ContextMenuInfo paramContextMenuInfo)
  {
    Logger.d("HomeActivity", "onCreateContextMenu");
    super.onCreateContextMenu(paramContextMenu, paramView, paramContextMenuInfo);
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    Logger.d("HomeActivity", "onCreateOptionsMenu");
    return super.onCreateOptionsMenu(paramMenu);
  }

  protected void onDestroy()
  {
    Logger.d("HomeActivity", "onDestroy");
    super.onDestroy();
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    Logger.d("HomeActivity", "onKeyDown");
    if ((SdkVersionWrapper.getWrapper().useFragments()) && (this.mTranslateFragment.onKeyDown(paramInt, paramKeyEvent)))
      return true;
    return super.onKeyDown(paramInt, paramKeyEvent);
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    Logger.d("HomeActivity", "onOptionsItemSelected");
    if ((this.mTranslateFragment != null) && (this.mTranslateFragment.onOptionsItemSelected(paramMenuItem)))
      return true;
    return super.onOptionsItemSelected(paramMenuItem);
  }

  protected void onPause()
  {
    Logger.d("HomeActivity", "onPause");
    super.onPause();
    if (this.mTranslateHelper != null)
      this.mTranslateHelper.onPause();
    this.mInitRun = false;
    ((TranslateApplication)getApplication()).resetAppCsiTimer();
  }

  public boolean onPrepareOptionsMenu(Menu paramMenu)
  {
    Logger.d("HomeActivity", "onPrepareOptionsMenu");
    return super.onPrepareOptionsMenu(paramMenu);
  }

  protected void onResume()
  {
    Logger.d("HomeActivity", "onResume");
    if (this.mTranslateFragment != null)
      this.mTranslateFragment.onResume();
    super.onResume();
  }

  protected void onStart()
  {
    Logger.d("HomeActivity", "onStart");
    super.onStart();
    Translate.setAcceptLanguage(Locale.getDefault().toString());
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.HomeActivity
 * JD-Core Version:    0.6.2
 */