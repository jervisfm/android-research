package com.google.android.apps.translate;

import java.io.Serializable;

public class Language
  implements Serializable, Comparable<Language>
{
  private static final long serialVersionUID = 1L;
  private String mLongName;
  private String mShortName;

  public Language(String paramString1, String paramString2)
  {
    this.mLongName = ((String)Preconditions.checkNotNull(paramString2));
    this.mShortName = ((String)Preconditions.checkNotNull(paramString1));
  }

  public int compareTo(Language paramLanguage)
  {
    return this.mLongName.compareTo(paramLanguage.mLongName);
  }

  public boolean equals(Object paramObject)
  {
    if (this == paramObject);
    Language localLanguage;
    do
    {
      return true;
      if (!(paramObject instanceof Language))
        return false;
      localLanguage = (Language)paramObject;
    }
    while ((this.mLongName.equals(localLanguage.mLongName)) && (this.mShortName.equals(localLanguage.mShortName)));
    return false;
  }

  public String getLongName()
  {
    return this.mLongName;
  }

  public String getShortName()
  {
    return this.mShortName;
  }

  public int hashCode()
  {
    return this.mLongName.hashCode() ^ this.mShortName.hashCode();
  }

  public String toString()
  {
    return this.mLongName;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.Language
 * JD-Core Version:    0.6.2
 */