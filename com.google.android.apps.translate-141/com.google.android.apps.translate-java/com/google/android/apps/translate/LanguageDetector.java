package com.google.android.apps.translate;

import android.text.Spannable;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public final class LanguageDetector
{
  private static final HashMap<String, Integer> lANGUAGE_INDEX_MAP = new HashMap();

  static
  {
    for (int i = 0; i < LanguageDetectorData.LANGUAGES.length; i++)
      lANGUAGE_INDEX_MAP.put(LanguageDetectorData.LANGUAGES[i], Integer.valueOf(i));
  }

  public static ArrayList<Language> detect(String paramString, List<Language> paramList)
  {
    if ((!TextUtils.isEmpty(paramString)) && (paramList != null) && (!paramList.isEmpty()))
    {
      ArrayList localArrayList = getLangInfoList(paramList);
      if (!localArrayList.isEmpty())
        return doDetect(paramString, localArrayList);
    }
    return new ArrayList();
  }

  private static ArrayList<Language> doDetect(String paramString, ArrayList<LangInfo> paramArrayList)
  {
    int[] arrayOfInt = getAllScriptIds(paramArrayList);
    int i = paramString.length();
    int j = 0;
    while (j < i)
    {
      int k = paramString.codePointAt(j);
      if (Character.isLetter(k))
      {
        int m = getCodePointScriptMask(k, arrayOfInt);
        Iterator localIterator2 = paramArrayList.iterator();
        while (localIterator2.hasNext())
        {
          LangInfo localLangInfo2 = (LangInfo)localIterator2.next();
          if ((m & localLangInfo2.scriptMask) != 0)
            localLangInfo2.codePointCount = (1 + localLangInfo2.codePointCount);
        }
      }
      j += Character.charCount(k);
    }
    Collections.sort(paramArrayList);
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator1 = paramArrayList.iterator();
    while (localIterator1.hasNext())
    {
      LangInfo localLangInfo1 = (LangInfo)localIterator1.next();
      if (localLangInfo1.codePointCount > 0)
        localArrayList.add(localLangInfo1.language);
    }
    return localArrayList;
  }

  private static int[] getAllScriptIds(ArrayList<LangInfo> paramArrayList)
  {
    BitSet localBitSet = new BitSet();
    int i = paramArrayList.size();
    for (int j = 0; j < i; j++)
    {
      int[] arrayOfInt2 = ((LangInfo)paramArrayList.get(j)).scriptIds;
      int i1 = arrayOfInt2.length;
      for (int i2 = 0; i2 < i1; i2++)
        localBitSet.set(arrayOfInt2[i2]);
    }
    int[] arrayOfInt1 = new int[localBitSet.cardinality()];
    int k = localBitSet.nextSetBit(0);
    int n;
    for (int m = 0; k >= 0; m = n)
    {
      n = m + 1;
      arrayOfInt1[m] = k;
      k = localBitSet.nextSetBit(k + 1);
    }
    return arrayOfInt1;
  }

  private static int getCodePointScriptMask(int paramInt, int[] paramArrayOfInt)
  {
    int i = 0;
    int j = paramArrayOfInt.length;
    int k = 0;
    if (k < j)
    {
      int m = paramArrayOfInt[k];
      int n = Arrays.binarySearch(LanguageDetectorData.SCRIPT_CODE_RANGE_STARTS[m], paramInt);
      if (n >= 0)
        i |= 1 << m;
      while (true)
      {
        k++;
        break;
        int i1 = -(n + 1);
        int[] arrayOfInt = LanguageDetectorData.SCRIPT_CODE_RANGE_ENDS[m];
        if ((i1 > 0) && (i1 <= arrayOfInt.length) && (paramInt <= arrayOfInt[(i1 - 1)]))
          i |= 1 << m;
      }
    }
    return i;
  }

  private static LangInfo getLangInfo(Language paramLanguage)
  {
    String str = paramLanguage.getShortName();
    int i = str.indexOf('-');
    if (i < 0)
      i = str.indexOf('_');
    if (i > 0)
      str = str.substring(0, i);
    Integer localInteger = (Integer)lANGUAGE_INDEX_MAP.get(str);
    if (localInteger == null)
      return null;
    return new LangInfo(paramLanguage, LanguageDetectorData.LANGUAGE_SCRIPT_IDS[localInteger.intValue()], LanguageDetectorData.LANGUAGE_SCRIPT_MASKS[localInteger.intValue()], LanguageDetectorData.LANGUAGE_CODE_COUNTS[localInteger.intValue()]);
  }

  private static ArrayList<LangInfo> getLangInfoList(List<Language> paramList)
  {
    ArrayList localArrayList = new ArrayList();
    int i = paramList.size();
    for (int j = 0; j < i; j++)
    {
      LangInfo localLangInfo = getLangInfo((Language)paramList.get(j));
      if (localLangInfo != null)
        localArrayList.add(localLangInfo);
    }
    return localArrayList;
  }

  private static class LangInfo
    implements Comparable<LangInfo>
  {
    public int codePointCount;
    public final Language language;
    public final int[] scriptIds;
    public final int scriptMask;
    public final int totalCodePointCount;

    LangInfo(Language paramLanguage, int[] paramArrayOfInt, int paramInt1, int paramInt2)
    {
      this.language = paramLanguage;
      this.scriptIds = paramArrayOfInt;
      this.scriptMask = paramInt1;
      this.totalCodePointCount = paramInt2;
      this.codePointCount = 0;
    }

    public int compareTo(LangInfo paramLangInfo)
    {
      int i = paramLangInfo.codePointCount - this.codePointCount;
      if (i == 0)
        i = this.totalCodePointCount - paramLangInfo.totalCodePointCount;
      return i;
    }
  }

  public static abstract class LanguageSpanApplier
  {
    protected abstract void applyLanguageSpan(Spannable paramSpannable, int paramInt1, int paramInt2, Language paramLanguage);

    public final void applyLanguageSpans(Spannable paramSpannable, Language paramLanguage)
    {
      LanguageDetector.LangInfo localLangInfo = LanguageDetector.getLangInfo(paramLanguage);
      if (localLangInfo == null);
      int i;
      int j;
      do
      {
        return;
        int[] arrayOfInt = localLangInfo.scriptIds;
        String str = paramSpannable.toString();
        i = str.length();
        j = -1;
        int k = 0;
        if (k < i)
        {
          int m = str.codePointAt(k);
          int n = -1;
          if ((LanguageDetector.getCodePointScriptMask(m, arrayOfInt) & localLangInfo.scriptMask) != 0)
            if (j == -1)
              j = k;
          while (true)
          {
            if ((j != -1) && (n != -1))
            {
              applyLanguageSpan(paramSpannable, j, n, paramLanguage);
              j = -1;
            }
            k += Character.charCount(m);
            break;
            n = k;
          }
        }
      }
      while (j == -1);
      applyLanguageSpan(paramSpannable, j, i, paramLanguage);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.LanguageDetector
 * JD-Core Version:    0.6.2
 */