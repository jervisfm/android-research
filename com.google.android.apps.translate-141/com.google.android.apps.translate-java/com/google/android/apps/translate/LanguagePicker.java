package com.google.android.apps.translate;

import android.app.Activity;
import android.content.Context;
import android.text.SpannableStringBuilder;
import android.text.style.TextAppearanceSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import com.google.android.apps.translate.translation.BaseTranslateHelper;
import com.google.android.apps.translate.tts.NetworkTts;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

public class LanguagePicker
  implements AdapterView.OnItemSelectedListener
{
  private static final String[] NO_FONT_SUPPORT_LANGUAGES;
  private static final boolean SHOW_ALL_LANGUAGES_FOR_CONVERSATION = false;
  private static final String TAG = "LanguagePicker";
  private static final boolean USE_CONV_LANG_PROFILE;
  private static HashSet<String> mUnsupportedLanguages = new HashSet();
  private final Activity mActivity;
  private List<Language> mAndroidSupportedSourceLanguageList;
  private List<Language> mAndroidSupportedTargetLanguageList;
  private List<Language> mConvSupportedLangList;
  private boolean mIsConversationMode = false;
  private Languages mLanguages;
  private final OnLanguagePairSelectedListener mOnLanguagePairSelectedListener;
  private List<LanguageDropDownItem> mSourceItems;
  private Language mSourceLanguage;
  private final Spinner mSourceSpinner;
  private List<LanguageDropDownItem> mTargetItems;
  private Language mTargetLanguage;
  private final Spinner mTargetSpinner;

  static
  {
    NO_FONT_SUPPORT_LANGUAGES = new String[] { "hi", "fa", "th", "yi" };
    for (String str : NO_FONT_SUPPORT_LANGUAGES)
      mUnsupportedLanguages.add(str);
  }

  public LanguagePicker(Activity paramActivity, Spinner paramSpinner1, Spinner paramSpinner2, OnLanguagePairSelectedListener paramOnLanguagePairSelectedListener)
  {
    this.mActivity = paramActivity;
    this.mLanguages = LanguagesFactory.get().getLanguages(paramActivity, Locale.getDefault());
    updateLanguageList(this.mLanguages);
    updateCurrentLanguagePair(this.mLanguages.getDefaultFromLanguage(), this.mLanguages.getDefaultToLanguage());
    this.mSourceSpinner = paramSpinner1;
    this.mTargetSpinner = paramSpinner2;
    this.mSourceSpinner.setOnItemSelectedListener(this);
    this.mTargetSpinner.setOnItemSelectedListener(this);
    this.mOnLanguagePairSelectedListener = paramOnLanguagePairSelectedListener;
  }

  private static List<Language> filterConversationLanguages(VoiceInputHelper paramVoiceInputHelper, List<Language> paramList)
  {
    ArrayList localArrayList = Lists.newArrayList();
    Iterator localIterator = paramList.iterator();
    while (localIterator.hasNext())
    {
      Language localLanguage = (Language)localIterator.next();
      if ((paramVoiceInputHelper.isConversationLanguage(localLanguage)) && (NetworkTts.isLanguageAvailable(Util.languageShortNameToLocale(localLanguage.getShortName()))))
        localArrayList.add(localLanguage);
    }
    return localArrayList;
  }

  private List<LanguageDropDownItem> generateDropDownItems(boolean paramBoolean, List<Language> paramList1, List<Language> paramList2)
  {
    ArrayList localArrayList = Lists.newArrayList();
    if (paramBoolean)
      localArrayList.add(new LanguageDropDownItem(new Language("select", this.mActivity.getString(R.string.hint_language_selector)), LanguagePicker.LanguageDropDownItem.LanguageSpec.SELECT));
    Iterator localIterator1 = paramList2.iterator();
    while (localIterator1.hasNext())
      localArrayList.add(new LanguageDropDownItem((Language)localIterator1.next(), LanguagePicker.LanguageDropDownItem.LanguageSpec.RECENTLY_USED));
    Iterator localIterator2 = paramList1.iterator();
    while (localIterator2.hasNext())
      localArrayList.add(new LanguageDropDownItem((Language)localIterator2.next(), LanguagePicker.LanguageDropDownItem.LanguageSpec.NORMAL));
    return localArrayList;
  }

  private List<Language> getAndroidSupportedLanguages(List<Language> paramList)
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = paramList.iterator();
    while (localIterator.hasNext())
    {
      Language localLanguage = (Language)localIterator.next();
      if (!mUnsupportedLanguages.contains(localLanguage.getShortName()))
        localArrayList.add(localLanguage);
    }
    return localArrayList;
  }

  private List<Language> getRecentLeftConversationLanguage(List<Language> paramList)
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = Profile.getRecentFromLanguages(this.mActivity, this.mLanguages).iterator();
    while (localIterator.hasNext())
    {
      Language localLanguage = (Language)localIterator.next();
      if (this.mConvSupportedLangList.contains(localLanguage))
        localArrayList.add(BaseTranslateHelper.getToLanguageGivenFromLanguage(paramList, this.mLanguages, localLanguage, false));
    }
    return localArrayList;
  }

  private List<Language> getRecentRightConversationLanguage()
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = Profile.getRecentToLanguages(this.mActivity, this.mLanguages).iterator();
    while (localIterator.hasNext())
    {
      Language localLanguage = (Language)localIterator.next();
      if (this.mConvSupportedLangList.contains(localLanguage))
        localArrayList.add(localLanguage);
    }
    return localArrayList;
  }

  private void setLanguagePairFromSpinners()
  {
    Logger.d("LanguagePicker", "setLanguagePairFromSpinners");
    setLanguagePairToSpinners(((LanguageDropDownItem)this.mSourceSpinner.getSelectedItem()).getLanguage(), ((LanguageDropDownItem)this.mTargetSpinner.getSelectedItem()).getLanguage());
  }

  private void updateCurrentLanguagePair(Language paramLanguage1, Language paramLanguage2)
  {
    this.mSourceLanguage = paramLanguage1;
    this.mTargetLanguage = paramLanguage2;
  }

  private void updateLanguageSpinner(Spinner paramSpinner, List<LanguageDropDownItem> paramList, Language paramLanguage)
  {
    for (int i = 0; ; i++)
      if ((i >= paramList.size()) || (((LanguageDropDownItem)paramList.get(i)).getLanguage().equals(paramLanguage)))
      {
        if (i == paramList.size())
          i = 0;
        paramSpinner.setSelection(i);
        ((LanguageAdapter)paramSpinner.getAdapter()).notifyDataSetChanged();
        return;
      }
  }

  private void updateLanguageSpinners()
  {
    if (this.mIsConversationMode)
    {
      List localList1 = getRecentRightConversationLanguage();
      List localList2 = getRecentLeftConversationLanguage(localList1);
      updateRecentDropDownItems(this.mSourceItems, localList2);
      ((LanguageAdapter)this.mSourceSpinner.getAdapter()).notifyDataSetChanged();
      updateLanguageSpinner(this.mSourceSpinner, this.mSourceItems, this.mSourceLanguage);
      updateRecentDropDownItems(this.mTargetItems, localList1);
    }
    while (true)
    {
      ((LanguageAdapter)this.mTargetSpinner.getAdapter()).notifyDataSetChanged();
      updateLanguageSpinner(this.mTargetSpinner, this.mTargetItems, this.mTargetLanguage);
      return;
      updateRecentDropDownItems(this.mSourceItems, Profile.getRecentFromLanguages(this.mActivity, this.mLanguages));
      ((LanguageAdapter)this.mSourceSpinner.getAdapter()).notifyDataSetChanged();
      updateLanguageSpinner(this.mSourceSpinner, this.mSourceItems, this.mSourceLanguage);
      updateRecentDropDownItems(this.mTargetItems, Profile.getRecentToLanguages(this.mActivity, this.mLanguages));
    }
  }

  private void updateRecentDropDownItems(List<LanguageDropDownItem> paramList, List<Language> paramList1)
  {
    ArrayList localArrayList = Lists.newArrayList();
    Object localObject = null;
    boolean bool = Util.isAutoDetectLanguage(this.mSourceLanguage);
    int i = 0;
    if (bool)
      i = 1;
    if ((paramList.size() > 0) && (((LanguageDropDownItem)paramList.get(0)).getSpec() == LanguagePicker.LanguageDropDownItem.LanguageSpec.SELECT))
      localArrayList.add(paramList.get(0));
    Iterator localIterator1 = paramList1.iterator();
    while (localIterator1.hasNext())
      localArrayList.add(new LanguageDropDownItem((Language)localIterator1.next(), LanguagePicker.LanguageDropDownItem.LanguageSpec.RECENTLY_USED));
    Iterator localIterator2 = paramList.iterator();
    while (localIterator2.hasNext())
    {
      LanguageDropDownItem localLanguageDropDownItem = (LanguageDropDownItem)localIterator2.next();
      if (localLanguageDropDownItem.getSpec() == LanguagePicker.LanguageDropDownItem.LanguageSpec.NORMAL)
        if ((i != 0) && (Util.isAutoDetectLanguage(localLanguageDropDownItem.getLanguage())))
          localObject = localLanguageDropDownItem;
        else
          localArrayList.add(localLanguageDropDownItem);
    }
    paramList.clear();
    if (localObject != null)
      paramList.add(localObject);
    paramList.addAll(localArrayList);
  }

  public Language getFromLanguage()
  {
    return this.mSourceLanguage;
  }

  public Language getToLanguage()
  {
    return this.mTargetLanguage;
  }

  public void onItemSelected(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    Logger.d("LanguagePicker", "onItemSelected");
    setLanguagePairFromSpinners();
    if (this.mOnLanguagePairSelectedListener != null)
      this.mOnLanguagePairSelectedListener.onLanguagePairSelected(this.mSourceLanguage, this.mTargetLanguage, true, true);
  }

  public void onNothingSelected(AdapterView<?> paramAdapterView)
  {
  }

  public void setLanguagePairToSpinners(Language paramLanguage1, Language paramLanguage2)
  {
    if ((!Util.isSelectLanguage(paramLanguage1.getShortName())) && (!Util.isSelectLanguage(paramLanguage2.getShortName())))
    {
      if (!this.mIsConversationMode)
        break label57;
      Language localLanguage = BaseTranslateHelper.getFromLanguageGivenToLanguage(this.mLanguages, paramLanguage1, false);
      Profile.setLanguagePair(this.mActivity, localLanguage, paramLanguage2);
    }
    while (true)
    {
      updateCurrentLanguagePair(paramLanguage1, paramLanguage2);
      updateLanguageSpinners();
      return;
      label57: Profile.setLanguagePair(this.mActivity, paramLanguage1, paramLanguage2);
    }
  }

  public void setupLanguageSpinners()
  {
    Language[] arrayOfLanguage = Profile.getLanguagePair(this.mActivity, this.mLanguages);
    if ((arrayOfLanguage.length > 1) && (arrayOfLanguage[0] != null) && (arrayOfLanguage[1] != null))
      updateCurrentLanguagePair(arrayOfLanguage[0], arrayOfLanguage[1]);
    this.mSourceItems = generateDropDownItems(false, this.mAndroidSupportedSourceLanguageList, Profile.getRecentFromLanguages(this.mActivity, this.mLanguages));
    LanguageAdapter localLanguageAdapter1 = new LanguageAdapter(this.mActivity, R.layout.language_spinner, this.mSourceItems);
    this.mSourceSpinner.setAdapter(localLanguageAdapter1);
    updateLanguageSpinner(this.mSourceSpinner, this.mSourceItems, this.mSourceLanguage);
    this.mTargetItems = generateDropDownItems(false, this.mAndroidSupportedTargetLanguageList, Profile.getRecentToLanguages(this.mActivity, this.mLanguages));
    LanguageAdapter localLanguageAdapter2 = new LanguageAdapter(this.mActivity, R.layout.language_spinner, this.mTargetItems);
    this.mTargetSpinner.setAdapter(localLanguageAdapter2);
    updateLanguageSpinner(this.mTargetSpinner, this.mTargetItems, this.mTargetLanguage);
  }

  public void setupLanguageSpinnersForConversationMode(VoiceInputHelper paramVoiceInputHelper, Language paramLanguage1, Language paramLanguage2)
  {
    Logger.d("LanguagePicker", "setupLanguageSpinnersForConversationMode");
    this.mIsConversationMode = true;
    this.mConvSupportedLangList = filterConversationLanguages(paramVoiceInputHelper, this.mAndroidSupportedTargetLanguageList);
    Profile.getLanguagePair(this.mActivity, this.mLanguages);
    boolean bool1 = true;
    boolean bool2 = true;
    if ((paramLanguage1 != null) && (this.mConvSupportedLangList.contains(paramLanguage1)))
    {
      bool1 = false;
      if ((paramLanguage2 == null) || (!this.mConvSupportedLangList.contains(paramLanguage2)))
        break label272;
      bool2 = false;
    }
    while (true)
    {
      List localList1 = getRecentRightConversationLanguage();
      List localList2 = getRecentLeftConversationLanguage(localList1);
      updateCurrentLanguagePair(paramLanguage1, paramLanguage2);
      List localList3 = filterConversationLanguages(paramVoiceInputHelper, localList2);
      this.mSourceItems = generateDropDownItems(bool1, this.mConvSupportedLangList, localList3);
      LanguageAdapter localLanguageAdapter1 = new LanguageAdapter(this.mActivity, R.layout.language_spinner, this.mSourceItems);
      this.mSourceSpinner.setAdapter(localLanguageAdapter1);
      updateLanguageSpinner(this.mSourceSpinner, this.mSourceItems, this.mSourceLanguage);
      List localList4 = filterConversationLanguages(paramVoiceInputHelper, localList1);
      this.mTargetItems = generateDropDownItems(bool2, this.mConvSupportedLangList, localList4);
      LanguageAdapter localLanguageAdapter2 = new LanguageAdapter(this.mActivity, R.layout.language_spinner, this.mTargetItems);
      this.mTargetSpinner.setAdapter(localLanguageAdapter2);
      updateLanguageSpinner(this.mTargetSpinner, this.mTargetItems, this.mTargetLanguage);
      return;
      if (!bool1)
        break;
      paramLanguage1 = new Language("select", this.mActivity.getString(R.string.hint_language_selector));
      break;
      label272: if (bool2)
        paramLanguage2 = new Language("select", this.mActivity.getString(R.string.hint_language_selector));
    }
  }

  public void updateLanguageList(Languages paramLanguages)
  {
    this.mLanguages = paramLanguages;
    this.mAndroidSupportedSourceLanguageList = new ArrayList(this.mLanguages.getSupportedFromLanguages());
    this.mAndroidSupportedTargetLanguageList = new ArrayList(this.mLanguages.getSupportedToLanguages());
  }

  private class LanguageAdapter extends ArrayAdapter<LanguagePicker.LanguageDropDownItem>
  {
    private final LayoutInflater mInflater;

    public LanguageAdapter(int paramList, List<LanguagePicker.LanguageDropDownItem> arg3)
    {
      super(i, localList);
      this.mInflater = ((LayoutInflater)paramList.getSystemService("layout_inflater"));
    }

    public View getDropDownView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      LanguagePicker.LanguageDropDownItem localLanguageDropDownItem = (LanguagePicker.LanguageDropDownItem)getItem(paramInt);
      if (localLanguageDropDownItem == null)
        return null;
      if (paramView != null);
      for (View localView = paramView; ; localView = this.mInflater.inflate(R.layout.language_dropdown, paramViewGroup, false))
      {
        SpannableStringBuilder localSpannableStringBuilder = new SpannableStringBuilder();
        String str = localLanguageDropDownItem.getLanguage().toString();
        localSpannableStringBuilder.append(str);
        localSpannableStringBuilder.setSpan(new TextAppearanceSpan(LanguagePicker.this.mActivity, 16973892), 0, str.length(), 33);
        if (localLanguageDropDownItem.getSpec() == LanguagePicker.LanguageDropDownItem.LanguageSpec.RECENTLY_USED)
        {
          localSpannableStringBuilder.append("\n");
          localSpannableStringBuilder.append(LanguagePicker.this.mActivity.getString(localLanguageDropDownItem.getSpec().getResourceId()));
          localSpannableStringBuilder.setSpan(new TextAppearanceSpan(LanguagePicker.this.mActivity, 16973894), str.length(), localSpannableStringBuilder.length(), 33);
        }
        ((TextView)localView.findViewById(16908308)).setText(localSpannableStringBuilder);
        return localView;
      }
    }
  }

  public static class LanguageDropDownItem
    implements Comparable<LanguageDropDownItem>
  {
    private final Language language;
    private final LanguageSpec spec;

    public LanguageDropDownItem(Language paramLanguage, LanguageSpec paramLanguageSpec)
    {
      this.language = paramLanguage;
      this.spec = paramLanguageSpec;
    }

    public int compareTo(LanguageDropDownItem paramLanguageDropDownItem)
    {
      if (this.spec.getPriority() == paramLanguageDropDownItem.getSpec().getPriority())
        return this.language.compareTo(paramLanguageDropDownItem.getLanguage());
      return this.spec.getPriority() - paramLanguageDropDownItem.getSpec().getPriority();
    }

    public Language getLanguage()
    {
      return this.language;
    }

    public LanguageSpec getSpec()
    {
      return this.spec;
    }

    public String toString()
    {
      return this.language.toString();
    }

    public static enum LanguageSpec
    {
      private final int priority;
      private final int resourceId;

      static
      {
        LanguageSpec[] arrayOfLanguageSpec = new LanguageSpec[3];
        arrayOfLanguageSpec[0] = NORMAL;
        arrayOfLanguageSpec[1] = RECENTLY_USED;
        arrayOfLanguageSpec[2] = SELECT;
      }

      private LanguageSpec(int paramInt1, int paramInt2)
      {
        this.priority = paramInt1;
        this.resourceId = paramInt2;
      }

      public int getPriority()
      {
        return this.priority;
      }

      public int getResourceId()
      {
        return this.resourceId;
      }
    }
  }

  public static abstract interface OnLanguagePairSelectedListener
  {
    public abstract void onLanguagePairSelected(Language paramLanguage1, Language paramLanguage2, boolean paramBoolean1, boolean paramBoolean2);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.LanguagePicker
 * JD-Core Version:    0.6.2
 */