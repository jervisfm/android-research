package com.google.android.apps.translate;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public final class Languages
{
  private static final String[] DEFAULT_ALPHA_LANGUAGES;
  private static final String DEFAULT_FROM = "en";
  private static final List<Language> DEFAULT_FROM_LANGUAGES;
  private static final String[][] DEFAULT_FROM_LANGUAGES_SPEC = { { "auto", "Detect language" }, { "af", "Afrikaans" }, { "sq", "Albanian" }, { "ar", "Arabic" }, { "hy", "Armenian" }, { "az", "Azerbaijani" }, { "eu", "Basque" }, { "be", "Belarusian" }, { "bn", "Bengali" }, { "bg", "Bulgarian" }, { "ca", "Catalan" }, { "zh-CN", "Chinese" }, { "hr", "Croatian" }, { "cs", "Czech" }, { "da", "Danish" }, { "nl", "Dutch" }, { "en", "English" }, { "eo", "Esperanto" }, { "et", "Estonian" }, { "tl", "Filipino" }, { "fi", "Finnish" }, { "fr", "French" }, { "gl", "Galician" }, { "ka", "Georgian" }, { "de", "German" }, { "el", "Greek" }, { "gu", "Gujarati" }, { "ht", "Haitian Creole" }, { "iw", "Hebrew" }, { "hi", "Hindi" }, { "hu", "Hungarian" }, { "is", "Icelandic" }, { "id", "Indonesian" }, { "ga", "Irish" }, { "it", "Italian" }, { "ja", "Japanese" }, { "kn", "Kannada" }, { "ko", "Korean" }, { "lo", "Lao" }, { "la", "Latin" }, { "lv", "Latvian" }, { "lt", "Lithuanian" }, { "mk", "Macedonian" }, { "ms", "Malay" }, { "mt", "Maltese" }, { "no", "Norwegian" }, { "fa", "Persian" }, { "pl", "Polish" }, { "pt", "Portuguese" }, { "ro", "Romanian" }, { "ru", "Russian" }, { "sr", "Serbian" }, { "sk", "Slovak" }, { "sl", "Slovenian" }, { "es", "Spanish" }, { "sw", "Swahili" }, { "sv", "Swedish" }, { "ta", "Tamil" }, { "te", "Telugu" }, { "th", "Thai" }, { "tr", "Turkish" }, { "uk", "Ukrainian" }, { "ur", "Urdu" }, { "vi", "Vietnamese" }, { "cy", "Welsh" }, { "yi", "Yiddish" } };
  private static final String DEFAULT_TO = "es";
  private static final List<Language> DEFAULT_TO_LANGUAGES;
  private static final String[][] DEFAULT_TO_LANGUAGES_SPEC = { { "af", "Afrikaans" }, { "sq", "Albanian" }, { "ar", "Arabic" }, { "hy", "Armenian" }, { "az", "Azerbaijani" }, { "eu", "Basque" }, { "be", "Belarusian" }, { "bn", "Bengali" }, { "bg", "Bulgarian" }, { "ca", "Catalan" }, { "zh-CN", "Chinese (Simplified)" }, { "zh-TW", "Chinese (Traditional)" }, { "hr", "Croatian" }, { "cs", "Czech" }, { "da", "Danish" }, { "nl", "Dutch" }, { "en", "English" }, { "eo", "Esperanto" }, { "et", "Estonian" }, { "tl", "Filipino" }, { "fi", "Finnish" }, { "fr", "French" }, { "gl", "Galician" }, { "ka", "Georgian" }, { "de", "German" }, { "el", "Greek" }, { "gu", "Gujarati" }, { "ht", "Haitian Creole" }, { "iw", "Hebrew" }, { "hi", "Hindi" }, { "hu", "Hungarian" }, { "is", "Icelandic" }, { "id", "Indonesian" }, { "ga", "Irish" }, { "it", "Italian" }, { "ja", "Japanese" }, { "kn", "Kannada" }, { "ko", "Korean" }, { "lo", "Lao" }, { "la", "Latin" }, { "lv", "Latvian" }, { "lt", "Lithuanian" }, { "mk", "Macedonian" }, { "ms", "Malay" }, { "mt", "Maltese" }, { "no", "Norwegian" }, { "fa", "Persian" }, { "pl", "Polish" }, { "pt", "Portuguese" }, { "ro", "Romanian" }, { "ru", "Russian" }, { "sr", "Serbian" }, { "sk", "Slovak" }, { "sl", "Slovenian" }, { "es", "Spanish" }, { "sw", "Swahili" }, { "sv", "Swedish" }, { "ta", "Tamil" }, { "te", "Telugu" }, { "th", "Thai" }, { "tr", "Turkish" }, { "uk", "Ukrainian" }, { "ur", "Urdu" }, { "vi", "Vietnamese" }, { "cy", "Welsh" }, { "yi", "Yiddish" } };
  private static final String DUMP_LANGUAGE_SEPARATOR = "\t";
  private static Set<String> sAlphaLanguages;
  private List<Language> mFromLanguages = Lists.newLinkedList();
  private Map<String, Language> mFromLanguagesInShort = Maps.newHashMap();
  private Map<String, String> mFromLanguagesLongToShort = Maps.newHashMap();
  private List<Language> mToLanguages = Lists.newLinkedList();
  private Map<String, Language> mToLanguagesInShort = Maps.newHashMap();
  private Map<String, String> mToLanguagesLongToShort = Maps.newHashMap();

  static
  {
    DEFAULT_ALPHA_LANGUAGES = new String[] { "hy", "az", "eu", "ka", "la", "ur" };
    DEFAULT_FROM_LANGUAGES = Lists.newLinkedList();
    DEFAULT_TO_LANGUAGES = Lists.newLinkedList();
    sAlphaLanguages = Sets.newHashSet();
    for (String[] arrayOfString5 : DEFAULT_FROM_LANGUAGES_SPEC)
      DEFAULT_FROM_LANGUAGES.add(new Language(arrayOfString5[0], arrayOfString5[1]));
    for (String[] arrayOfString4 : DEFAULT_TO_LANGUAGES_SPEC)
      DEFAULT_TO_LANGUAGES.add(new Language(arrayOfString4[0], arrayOfString4[1]));
    for (String str : DEFAULT_ALPHA_LANGUAGES)
      sAlphaLanguages.add(str);
  }

  public Languages(String paramString)
  {
    List localList = Arrays.asList(paramString.split("\t"));
    this.mFromLanguages = Util.generateFromLanguagesFromList(localList);
    this.mToLanguages = Util.generateToLanguagesFromList(localList);
    setLanguageMaps();
  }

  public Languages(List<Language> paramList1, List<Language> paramList2)
  {
    this.mFromLanguages = paramList1;
    this.mToLanguages = paramList2;
    setLanguageMaps();
  }

  public static Language getBestChineseToLanguage(List<Language> paramList, Languages paramLanguages)
  {
    Language localLanguage = guessChinese(paramList, paramLanguages);
    if (localLanguage == null)
      localLanguage = getDefaultChineseToLanguage(paramLanguages);
    return localLanguage;
  }

  public static Language getDefaultChineseFromLanguage(Languages paramLanguages)
  {
    return paramLanguages.getFromLanguageByShortName("zh-CN");
  }

  public static Language getDefaultChineseToLanguage(Languages paramLanguages)
  {
    return paramLanguages.getToLanguageByShortName("zh-CN");
  }

  public static List<Language> getDefaultFromLanguages()
  {
    return Collections.unmodifiableList(DEFAULT_FROM_LANGUAGES);
  }

  public static List<Language> getDefaultToLanguages()
  {
    return Collections.unmodifiableList(DEFAULT_TO_LANGUAGES);
  }

  private static Language guessChinese(List<Language> paramList, Languages paramLanguages)
  {
    Language localLanguage = guessChineseFromRecentTargets(paramList, paramLanguages);
    if (localLanguage == null)
      localLanguage = guessChineseFromPhoneLocale(paramLanguages);
    return localLanguage;
  }

  private static Language guessChineseFromPhoneLocale(Languages paramLanguages)
  {
    Language localLanguage = paramLanguages.getToLanguageByShortName(Util.getLanguageShortNameByLocale());
    if ((localLanguage != null) && (isChinese(localLanguage)))
      return localLanguage;
    return null;
  }

  private static Language guessChineseFromRecentTargets(List<Language> paramList, Languages paramLanguages)
  {
    if (paramList != null)
    {
      Iterator localIterator = paramList.iterator();
      while (localIterator.hasNext())
      {
        Language localLanguage = (Language)localIterator.next();
        if (isChinese(localLanguage))
          return localLanguage;
      }
    }
    return null;
  }

  public static boolean isAlphaLanguage(Language paramLanguage)
  {
    return sAlphaLanguages.contains(paramLanguage.getShortName());
  }

  public static boolean isChinese(Language paramLanguage)
  {
    return isChinese(paramLanguage.getShortName());
  }

  public static boolean isChinese(String paramString)
  {
    return (paramString != null) && ((paramString.startsWith("zh-")) || (paramString.equals("zh")));
  }

  public static void setAlphaLanguages(List<String> paramList)
  {
    List localList = Util.generateAlphaLanguagesFromList(paramList);
    HashSet localHashSet = Sets.newHashSet();
    Iterator localIterator = localList.iterator();
    while (localIterator.hasNext())
      localHashSet.add(((Language)localIterator.next()).getShortName());
    sAlphaLanguages = localHashSet;
  }

  private void setLanguageMaps()
  {
    Iterator localIterator1 = this.mFromLanguages.iterator();
    while (localIterator1.hasNext())
    {
      Language localLanguage2 = (Language)localIterator1.next();
      this.mFromLanguagesLongToShort.put(localLanguage2.getLongName(), localLanguage2.getShortName());
      this.mFromLanguagesInShort.put(localLanguage2.getShortName(), localLanguage2);
    }
    Iterator localIterator2 = this.mToLanguages.iterator();
    while (localIterator2.hasNext())
    {
      Language localLanguage1 = (Language)localIterator2.next();
      this.mToLanguagesLongToShort.put(localLanguage1.getLongName(), localLanguage1.getShortName());
      this.mToLanguagesInShort.put(localLanguage1.getShortName(), localLanguage1);
    }
  }

  public String dumpLanguages()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    Iterator localIterator1 = this.mFromLanguages.iterator();
    while (localIterator1.hasNext())
    {
      Language localLanguage2 = (Language)localIterator1.next();
      localStringBuilder.append(Translate.generateLanguageTuple("sl", localLanguage2.getShortName(), localLanguage2.getLongName())).append("\t");
    }
    Iterator localIterator2 = this.mToLanguages.iterator();
    while (localIterator2.hasNext())
    {
      Language localLanguage1 = (Language)localIterator2.next();
      localStringBuilder.append(Translate.generateLanguageTuple("tl", localLanguage1.getShortName(), localLanguage1.getLongName())).append("\t");
    }
    return localStringBuilder.toString();
  }

  public String getDefaultFrom()
  {
    return "en";
  }

  public Language getDefaultFromLanguage()
  {
    return getFromLanguageByShortName(getDefaultFrom());
  }

  public String getDefaultTo()
  {
    if (this.mToLanguagesInShort.keySet().contains("es"))
      return "es";
    return "en";
  }

  public Language getDefaultToLanguage()
  {
    return getToLanguageByShortName(getDefaultTo());
  }

  public Language getFromLanguageByLongName(String paramString)
  {
    String str = (String)this.mFromLanguagesLongToShort.get(paramString);
    if (str == null)
      return null;
    return getFromLanguageByShortName(str);
  }

  public Language getFromLanguageByShortName(String paramString)
  {
    return (Language)this.mFromLanguagesInShort.get(paramString);
  }

  public String getFromLanguageLongName(String paramString)
  {
    Language localLanguage = getFromLanguageByShortName(paramString);
    if (localLanguage != null)
      return localLanguage.getLongName();
    return null;
  }

  public String getFromLanguageShortName(String paramString)
  {
    return (String)this.mFromLanguagesLongToShort.get(paramString);
  }

  public List<Language> getSupportedFromLanguages()
  {
    return Collections.unmodifiableList(this.mFromLanguages);
  }

  public List<Language> getSupportedToLanguages()
  {
    return Collections.unmodifiableList(this.mToLanguages);
  }

  public Language getToLanguageByLongName(String paramString)
  {
    String str = (String)this.mToLanguagesLongToShort.get(paramString);
    if (str == null)
      return null;
    return getToLanguageByShortName(str);
  }

  public Language getToLanguageByShortName(String paramString)
  {
    return (Language)this.mToLanguagesInShort.get(paramString);
  }

  public String getToLanguageLongName(String paramString)
  {
    Language localLanguage = getToLanguageByShortName(paramString);
    if (localLanguage != null)
      return localLanguage.getLongName();
    return null;
  }

  public String getToLanguageShortName(String paramString)
  {
    return (String)this.mToLanguagesLongToShort.get(paramString);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.Languages
 * JD-Core Version:    0.6.2
 */