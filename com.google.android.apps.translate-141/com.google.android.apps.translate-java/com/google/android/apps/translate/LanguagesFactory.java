package com.google.android.apps.translate;

import android.content.Context;
import android.text.TextUtils;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicReferenceArray;

public class LanguagesFactory
{
  private static final int CACHE_START_NDX = 1;
  private static final int FROM_TO_LANGS_NDX = 0;
  private static final int LANGS_OBJ_NUM = 2;
  private static LanguagesFactory sInst;
  private AtomicReferenceArray<LanguagesInfo> mLanguages = new AtomicReferenceArray(2);

  private LanguagesFactory()
  {
    clearCaches();
  }

  private void cacheLanguages(LanguagesInfo paramLanguagesInfo)
  {
    this.mLanguages.set(1, paramLanguagesInfo);
  }

  private void clearCaches()
  {
    this.mLanguages.set(1, null);
  }

  public static LanguagesFactory get()
  {
    if (sInst == null);
    try
    {
      if (sInst == null)
        sInst = new LanguagesFactory();
      return sInst;
    }
    finally
    {
    }
  }

  public final Languages getLanguages(Context paramContext, Locale paramLocale)
  {
    return getLanguages(paramContext, paramLocale, true);
  }

  public final Languages getLanguages(Context paramContext, Locale paramLocale, boolean paramBoolean)
  {
    for (int i = 0; i < 2; i++)
    {
      LanguagesInfo localLanguagesInfo = (LanguagesInfo)this.mLanguages.get(i);
      if ((localLanguagesInfo != null) && (localLanguagesInfo.locale.equals(paramLocale)))
        return localLanguagesInfo.languages;
    }
    String str = Profile.getLanguageList(paramContext, paramLocale);
    if (!TextUtils.isEmpty(str))
    {
      Languages localLanguages1 = new Languages(str);
      cacheLanguages(new LanguagesInfo(localLanguages1, paramLocale));
      return localLanguages1;
    }
    if (paramBoolean);
    for (Languages localLanguages2 = new Languages(Languages.getDefaultFromLanguages(), Languages.getDefaultToLanguages()); ; localLanguages2 = null)
      return localLanguages2;
  }

  public void refreshLanguagesFromProfile(Context paramContext)
  {
    Languages localLanguages = Util.getLanguageListFromProfile(paramContext);
    this.mLanguages.set(0, new LanguagesInfo(localLanguages, Locale.getDefault()));
    clearCaches();
  }

  private static class LanguagesInfo
  {
    public Languages languages;
    public Locale locale;

    public LanguagesInfo(Languages paramLanguages, Locale paramLocale)
    {
      this.languages = paramLanguages;
      this.locale = paramLocale;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.LanguagesFactory
 * JD-Core Version:    0.6.2
 */