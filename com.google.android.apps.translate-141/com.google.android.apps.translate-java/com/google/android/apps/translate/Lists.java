package com.google.android.apps.translate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;

public class Lists
{
  public static <E> ArrayList<E> newArrayList()
  {
    return new ArrayList();
  }

  public static <E> ArrayList<E> newArrayList(E[] paramArrayOfE)
  {
    ArrayList localArrayList = new ArrayList(5 + 110 * paramArrayOfE.length / 100);
    Collections.addAll(localArrayList, paramArrayOfE);
    return localArrayList;
  }

  public static <E> LinkedList<E> newLinkedList()
  {
    return new LinkedList();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.Lists
 * JD-Core Version:    0.6.2
 */