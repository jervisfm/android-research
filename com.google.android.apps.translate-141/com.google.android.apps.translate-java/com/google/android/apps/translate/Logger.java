package com.google.android.apps.translate;

import android.util.Log;
import java.util.HashMap;

public class Logger
{
  public static final int DEBUG = 8;
  private static final boolean DEBUG_ALL = false;
  public static final int ERROR = 1;
  public static final int INFO = 4;
  public static final int NONE = 0;
  public static final boolean T = false;
  public static final boolean V = false;
  public static final int VERBOSE = 16;
  public static final int WARNING = 2;
  private static HashMap<String, Long> sLastTimes;
  private static int sLevel = 1;
  private static String sTag = "Logger";

  public static void d(Object paramObject, String paramString)
  {
    if ((0x8 & sLevel) != 0)
      Log.d(paramObject.getClass().getSimpleName(), paramString);
  }

  public static void d(Object paramObject, String paramString, Throwable paramThrowable)
  {
    if ((0x8 & sLevel) != 0)
      Log.d(paramObject.getClass().getSimpleName(), paramString, paramThrowable);
  }

  public static void d(String paramString)
  {
    if ((0x8 & sLevel) != 0)
      Log.d(sTag, paramString);
  }

  public static void d(String paramString1, String paramString2)
  {
    if ((0x8 & sLevel) != 0)
      Log.d(paramString1, paramString2);
  }

  public static void d(String paramString1, String paramString2, Throwable paramThrowable)
  {
    if ((0x8 & sLevel) != 0)
      Log.d(paramString1, paramString2, paramThrowable);
  }

  public static void d(String paramString, Throwable paramThrowable)
  {
    if ((0x8 & sLevel) != 0)
      Log.d(sTag, paramString, paramThrowable);
  }

  public static void e(Object paramObject, String paramString)
  {
    if ((0x1 & sLevel) != 0)
      Log.e(paramObject.getClass().getSimpleName(), paramString);
  }

  public static void e(Object paramObject, String paramString, Throwable paramThrowable)
  {
    if ((0x1 & sLevel) != 0)
      Log.e(paramObject.getClass().getSimpleName(), paramString, paramThrowable);
  }

  public static void e(String paramString)
  {
    if ((0x1 & sLevel) != 0)
      Log.e(sTag, paramString);
  }

  public static void e(String paramString1, String paramString2)
  {
    if ((0x1 & sLevel) != 0)
      Log.e(paramString1, paramString2);
  }

  public static void e(String paramString1, String paramString2, Throwable paramThrowable)
  {
    if ((0x1 & sLevel) != 0)
      Log.e(paramString1, paramString2, paramThrowable);
  }

  public static void e(String paramString, Throwable paramThrowable)
  {
    if ((0x1 & sLevel) != 0)
      Log.e(sTag, paramString, paramThrowable);
  }

  public static void i(Object paramObject, String paramString)
  {
    if ((0x4 & sLevel) != 0)
      Log.i(paramObject.getClass().getSimpleName(), paramString);
  }

  public static void i(Object paramObject, String paramString, Throwable paramThrowable)
  {
    if ((0x4 & sLevel) != 0)
      Log.i(paramObject.getClass().getSimpleName(), paramString, paramThrowable);
  }

  public static void i(String paramString)
  {
    if ((0x4 & sLevel) != 0)
      Log.i(sTag, paramString);
  }

  public static void i(String paramString1, String paramString2)
  {
    if ((0x4 & sLevel) != 0)
      Log.i(paramString1, paramString2);
  }

  public static void i(String paramString1, String paramString2, Throwable paramThrowable)
  {
    if ((0x4 & sLevel) != 0)
      Log.i(paramString1, paramString2, paramThrowable);
  }

  public static void i(String paramString, Throwable paramThrowable)
  {
    if ((0x4 & sLevel) != 0)
      Log.i(sTag, paramString, paramThrowable);
  }

  public static boolean isDebug()
  {
    return (0x8 & sLevel) != 0;
  }

  public static boolean isError()
  {
    return (0x1 & sLevel) != 0;
  }

  public static boolean isInfo()
  {
    return (0x4 & sLevel) != 0;
  }

  public static boolean isWarning()
  {
    return (0x2 & sLevel) != 0;
  }

  public static void setLogLevel(int paramInt)
  {
    sLevel = 0;
    switch (paramInt)
    {
    default:
      return;
    case 16:
      sLevel = 0x10 | sLevel;
    case 8:
      sLevel = 0x8 | sLevel;
    case 4:
      sLevel = 0x4 | sLevel;
    case 2:
      sLevel = 0x2 | sLevel;
    case 1:
    }
    sLevel = 0x1 | sLevel;
  }

  public static void setTag(String paramString)
  {
    if (paramString != null)
      sTag = paramString;
  }

  public static void t(String paramString)
  {
  }

  public static void v(Object paramObject, String paramString)
  {
    if ((0x10 & sLevel) != 0)
      Log.v(paramObject.getClass().getSimpleName(), paramString);
  }

  public static void v(Object paramObject, String paramString, Throwable paramThrowable)
  {
    if ((0x10 & sLevel) != 0)
      Log.v(paramObject.getClass().getSimpleName(), paramString, paramThrowable);
  }

  public static void v(String paramString)
  {
    if ((0x10 & sLevel) != 0)
      Log.v(sTag, paramString);
  }

  public static void v(String paramString1, String paramString2)
  {
    if ((0x10 & sLevel) != 0)
      Log.v(paramString1, paramString2);
  }

  public static void v(String paramString1, String paramString2, Throwable paramThrowable)
  {
    if ((0x10 & sLevel) != 0)
      Log.v(paramString1, paramString2, paramThrowable);
  }

  public static void v(String paramString, Throwable paramThrowable)
  {
    if ((0x10 & sLevel) != 0)
      Log.v(sTag, paramString, paramThrowable);
  }

  public static void w(Object paramObject, String paramString)
  {
    if ((0x2 & sLevel) != 0)
      Log.w(paramObject.getClass().getSimpleName(), paramString);
  }

  public static void w(Object paramObject, String paramString, Throwable paramThrowable)
  {
    if ((0x2 & sLevel) != 0)
      Log.w(paramObject.getClass().getSimpleName(), paramString, paramThrowable);
  }

  public static void w(String paramString)
  {
    if ((0x2 & sLevel) != 0)
      Log.w(sTag, paramString);
  }

  public static void w(String paramString1, String paramString2)
  {
    if ((0x2 & sLevel) != 0)
      Log.w(paramString1, paramString2);
  }

  public static void w(String paramString1, String paramString2, Throwable paramThrowable)
  {
    if ((0x2 & sLevel) != 0)
      Log.w(paramString1, paramString2, paramThrowable);
  }

  public static void w(String paramString, Throwable paramThrowable)
  {
    if ((0x2 & sLevel) != 0)
      Log.w(sTag, paramString, paramThrowable);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.Logger
 * JD-Core Version:    0.6.2
 */