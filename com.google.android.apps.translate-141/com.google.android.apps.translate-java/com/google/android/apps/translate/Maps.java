package com.google.android.apps.translate;

import java.util.HashMap;
import java.util.TreeMap;

public class Maps
{
  public static <K, V> HashMap<K, V> newHashMap()
  {
    return new HashMap();
  }

  public static <K, V> TreeMap<K, V> newTreeMap()
  {
    return new TreeMap();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.Maps
 * JD-Core Version:    0.6.2
 */