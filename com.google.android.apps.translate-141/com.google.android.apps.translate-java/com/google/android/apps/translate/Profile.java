package com.google.android.apps.translate;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import com.google.android.apps.translate.translation.TranslateEntry;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

public class Profile
{
  public static final String DEFAULT_CAMERA_FRONTEND_URL = "http://www.google.com";
  private static final String DEFAULT_EN_FROM = "en";
  private static final String DEFAULT_EN_TO = "es";
  private static final String DEFAULT_OTHER_FROM = "en";
  private static final String EMPTY_ENTRY = "";
  public static final String KEY_CONVERSATION_LANGUAGE_LEFT = "key_convlang_left";
  public static final String KEY_CONVERSATION_LANGUAGE_RIGHT = "key_convlang_right";
  public static final String KEY_CURRENT_VERSION_CODE = "key_version_code";
  public static final String KEY_DEBUG_CAMERA_FRONTEND_URL = "key_debug_camera_input_frontend_url";
  public static final String KEY_ENABLE_CAMERA_LOGGING = "key_enable_camera_logging";
  public static final String KEY_ENABLE_CONVERSATION_CONFIRM = "key_enable_conversation_confirm";
  public static final String KEY_ENABLE_DUAL_MODE = "key_enable_dual_mode";
  public static final String KEY_ENABLE_INSTANT_TRANSLATION = "key_enable_instant_translation";
  public static final String KEY_ENABLE_OFFLINE_TRANSLATE = "key_enable_offline_translate_mode";
  public static final String KEY_ENABLE_STREAMING_CONVERSATION = "key_enable_streaming_conversation";
  public static final String KEY_EULA_ACCEPTED = "key_eula_accepted";
  public static final String KEY_FAVORITE_ORDER = "key_favorite_order";
  public static final String KEY_HANDWRITING_LANGUAGE_DEFVALUE_PREFIX = "default_handwriting_language_";
  public static final String KEY_HISTORY_ORDER = "key_history_order";
  public static final String KEY_HOME_FIRST_RUN = "key_home_first_run";
  public static final String KEY_INPUT_TEXT = "key_input_text";
  public static final String KEY_LANGUAGE_FROM = "key_language_from";
  public static final String KEY_LANGUAGE_LIST_WITH_LOCALE = "key_language_list_with_locale";
  public static final String KEY_LANGUAGE_TO = "key_language_to";
  public static final String KEY_LAST_TRANSLATION_FROM_LANG = "key_last_translation_from_lang";
  public static final String KEY_LAST_TRANSLATION_INPUT = "key_last_translation_input";
  public static final String KEY_LAST_TRANSLATION_IS_AUTO = "key_last_translation_is_auto";
  public static final String KEY_LAST_TRANSLATION_OUTPUT = "key_last_translation_output";
  public static final String KEY_LAST_TRANSLATION_TO_LANG = "key_last_translation_to_lang";
  public static final String KEY_PREFER_NETWORK_TTS = "key_prefer_network_tts";
  public static final String KEY_RECENT_CONVERSATION_LANGUAGE_LEFT = "key_recent_convlang_left";
  public static final String KEY_RECENT_CONVERSATION_LANGUAGE_RIGHT = "key_recent_convlang_right";
  public static final String KEY_RECENT_LANGUAGE_FROM = "key_recent_language_from";
  public static final String KEY_RECENT_LANGUAGE_TO = "key_recent_language_to";
  public static final String KEY_SHOW_CAMERA_LOGGING_DIALOG = "key_show_camera_logging_dialog";
  public static final String KEY_SUPPORTED_VOICE_INPUT_LANGS = "key_supported_voice_input_langs";
  public static final String KEY_TRANSLATE_SETTING = "key_title_settings";
  private static final String LANGUAGE_SEPARATOR = "\t";
  private static final String LOGMSG_CANNOT_GET_DEFAULT_PREFVALUE = "Failed to get the default preference value for key=";
  private static final int MAX_RECENT_PAIRS = 3;
  private static final String PAIR_SEPARATOR = "\t";
  private static final String TAG = "Profile";

  private static void addRecentLanguage(Context paramContext, Language paramLanguage, String paramString)
  {
    if ((paramLanguage == null) || (Util.isAutoDetectLanguage(paramLanguage)))
      return;
    String[] arrayOfString = PreferenceManager.getDefaultSharedPreferences(paramContext).getString(paramString, "").split("\t");
    SharedPreferences.Editor localEditor = PreferenceManager.getDefaultSharedPreferences(paramContext).edit();
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(paramLanguage.getShortName());
    int i = 1;
    int j = arrayOfString.length;
    for (int k = 0; ; k++)
    {
      String str;
      if (k < j)
      {
        str = arrayOfString[k];
        if (i < 3);
      }
      else
      {
        localEditor.putString(paramString, localStringBuilder.toString());
        localEditor.commit();
        return;
      }
      if (!paramLanguage.getShortName().equals(str))
      {
        localStringBuilder.append("\t").append(str);
        i++;
      }
    }
  }

  public static void clearEulaAccepted(Context paramContext)
  {
    PreferenceManager.getDefaultSharedPreferences(paramContext).edit().remove("key_eula_accepted").commit();
  }

  public static String getCameraInputFrontendUrl(Context paramContext)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    if (TranslateApplication.isReleaseBuild(paramContext));
    for (String str = "http://www.google.com"; ; str = PreferenceManager.getDefaultSharedPreferences(paramContext).getString("key_debug_camera_input_frontend_url", "http://www.google.com"))
    {
      Logger.d("getCameraInputFrontendUrl pref_url=" + str);
      if ((!TextUtils.isEmpty(str)) && (!str.startsWith("http://")))
        localStringBuilder.append("http://");
      localStringBuilder.append(str);
      return localStringBuilder.toString();
    }
  }

  public static List<String> getCameraInputFrontendUrls(Context paramContext)
  {
    ArrayList localArrayList = Lists.newArrayList();
    String[] arrayOfString = getFrontendUrls(paramContext);
    localArrayList.add("http://www.google.com");
    if (arrayOfString != null)
    {
      int i = arrayOfString.length;
      for (int j = 0; j < i; j++)
        localArrayList.add(arrayOfString[j]);
    }
    return localArrayList;
  }

  public static boolean getCameraLogging(Context paramContext)
  {
    return PreferenceManager.getDefaultSharedPreferences(paramContext).getBoolean("key_enable_camera_logging", Boolean.valueOf(paramContext.getString(R.string.default_enable_camera_logging)).booleanValue());
  }

  public static boolean getConversationInputConfirm(Context paramContext)
  {
    return PreferenceManager.getDefaultSharedPreferences(paramContext).getBoolean("key_enable_conversation_confirm", Boolean.valueOf(paramContext.getString(R.string.default_enable_conversation_confirm)).booleanValue());
  }

  public static Language[] getConversationLanguagePair(Context paramContext, Languages paramLanguages)
  {
    SharedPreferences localSharedPreferences = PreferenceManager.getDefaultSharedPreferences(paramContext);
    String str1 = localSharedPreferences.getString("key_convlang_left", getDefaultFromLanguage(paramContext, paramLanguages));
    String str2 = localSharedPreferences.getString("key_convlang_right", getDefaultToLanguage(paramContext, paramLanguages));
    Language[] arrayOfLanguage = new Language[2];
    arrayOfLanguage[0] = paramLanguages.getFromLanguageByShortName(str1);
    arrayOfLanguage[1] = paramLanguages.getToLanguageByShortName(str2);
    Logger.d("profile conversation from: " + str1 + ", to: " + str2);
    return arrayOfLanguage;
  }

  public static int getCurrentVersionCode(Context paramContext)
  {
    return PreferenceManager.getDefaultSharedPreferences(paramContext).getInt("key_version_code", 0);
  }

  private static String getDefaultFromLanguage(Context paramContext, Languages paramLanguages)
  {
    if (isEnglishLocale(Locale.getDefault()))
      return "en";
    return "en";
  }

  private static String getDefaultToLanguage(Context paramContext, Languages paramLanguages)
  {
    Locale localLocale = Locale.getDefault();
    String str;
    if (isEnglishLocale(localLocale))
      str = "es";
    do
    {
      return str;
      str = Util.getLanguageShortNameByLocale(localLocale);
    }
    while (paramLanguages.getToLanguageByShortName(str) != null);
    return "es";
  }

  public static boolean getDualMode(Context paramContext)
  {
    return PreferenceManager.getDefaultSharedPreferences(paramContext).getBoolean("key_enable_dual_mode", Boolean.valueOf(paramContext.getString(R.string.default_enable_dual_mode)).booleanValue());
  }

  public static int getFavoriteOrder(Context paramContext)
  {
    return PreferenceManager.getDefaultSharedPreferences(paramContext).getInt("key_favorite_order", 0);
  }

  public static String getFromShortLangNameFromHandwritingLangShortName(Languages paramLanguages, String paramString)
  {
    if (paramString.equals(Locale.CHINESE.getLanguage()))
      paramString = Languages.getDefaultChineseFromLanguage(paramLanguages).getShortName();
    return paramString;
  }

  private static String[] getFrontendUrls(Context paramContext)
  {
    return paramContext.getResources().getStringArray(R.array.frontend_domains);
  }

  public static String getHandwritingLangShortNameFromFromShortLangName(String paramString)
  {
    if ((paramString.equals("zh-CN")) || (paramString.equals("zh-TW")))
      paramString = Locale.CHINESE.getLanguage();
    return paramString;
  }

  private static String getHandwritingLanguageDefaultValueKey(String paramString)
  {
    return "default_handwriting_language_" + paramString.replace("-", "_");
  }

  public static int getHistoryOrder(Context paramContext)
  {
    return PreferenceManager.getDefaultSharedPreferences(paramContext).getInt("key_history_order", 1);
  }

  public static String getInputText(Context paramContext)
  {
    return PreferenceManager.getDefaultSharedPreferences(paramContext).getString("key_input_text", "");
  }

  public static boolean getInstantTranslation(Context paramContext)
  {
    return PreferenceManager.getDefaultSharedPreferences(paramContext).getBoolean("key_enable_instant_translation", Boolean.valueOf(paramContext.getString(R.string.default_enable_instant_translation)).booleanValue());
  }

  public static String getLanguageList(Context paramContext)
  {
    return getLanguageList(paramContext, Locale.getDefault());
  }

  public static String getLanguageList(Context paramContext, Locale paramLocale)
  {
    String str = "key_language_list_with_locale_" + Util.getLanguageShortNameByLocale(paramLocale);
    return PreferenceManager.getDefaultSharedPreferences(paramContext).getString(str, "");
  }

  public static Language[] getLanguagePair(Context paramContext, Languages paramLanguages)
  {
    SharedPreferences localSharedPreferences = PreferenceManager.getDefaultSharedPreferences(paramContext);
    String str1 = localSharedPreferences.getString("key_language_from", getDefaultFromLanguage(paramContext, paramLanguages));
    String str2 = localSharedPreferences.getString("key_language_to", getDefaultToLanguage(paramContext, paramLanguages));
    Language[] arrayOfLanguage = new Language[2];
    arrayOfLanguage[0] = paramLanguages.getFromLanguageByShortName(str1);
    arrayOfLanguage[1] = paramLanguages.getToLanguageByShortName(str2);
    Logger.d("profile from: " + str1 + ", to: " + str2);
    return arrayOfLanguage;
  }

  public static TranslateEntry getLastTranslation(Context paramContext, Languages paramLanguages)
  {
    TranslateEntry localTranslateEntry;
    if ((paramContext == null) || (paramLanguages == null))
      localTranslateEntry = null;
    do
    {
      return localTranslateEntry;
      Logger.d("Profile", "getLastTranslation");
      SharedPreferences localSharedPreferences = PreferenceManager.getDefaultSharedPreferences(paramContext);
      localTranslateEntry = TranslateEntry.build(localSharedPreferences.getString("key_last_translation_from_lang", ""), localSharedPreferences.getString("key_last_translation_to_lang", ""), localSharedPreferences.getString("key_last_translation_input", ""), localSharedPreferences.getString("key_last_translation_output", ""), localSharedPreferences.getBoolean("key_last_translation_is_auto", false), paramLanguages);
    }
    while ((localTranslateEntry != null) && (localTranslateEntry.hasInputText()) && (localTranslateEntry.hasSourceAndTargetLanguages()));
    return null;
  }

  public static boolean getOfflineTranslate(Context paramContext)
  {
    return PreferenceManager.getDefaultSharedPreferences(paramContext).getBoolean("key_enable_offline_translate_mode", Boolean.valueOf(paramContext.getString(R.string.default_enable_offline_translate_mode)).booleanValue());
  }

  public static boolean getPreferNetworkTts(Context paramContext)
  {
    return PreferenceManager.getDefaultSharedPreferences(paramContext).getBoolean("key_prefer_network_tts", Boolean.valueOf(paramContext.getString(R.string.default_prefer_network_tts)).booleanValue());
  }

  private static List<Language> getRecentConversationLanguages(Context paramContext, String paramString, Languages paramLanguages)
  {
    String str1 = PreferenceManager.getDefaultSharedPreferences(paramContext).getString(paramString, "");
    ArrayList localArrayList = Lists.newArrayList();
    String[] arrayOfString = str1.split("\t");
    int i = arrayOfString.length;
    int j = 0;
    if (j < i)
    {
      String str2 = arrayOfString[j];
      Language localLanguage;
      if (paramString == "key_recent_convlang_left")
        localLanguage = paramLanguages.getFromLanguageByShortName(str2);
      while (true)
      {
        if (localLanguage != null)
          localArrayList.add(localLanguage);
        j++;
        break;
        localLanguage = null;
        if (paramString == "key_recent_convlang_right")
          localLanguage = paramLanguages.getToLanguageByShortName(str2);
      }
    }
    return localArrayList;
  }

  public static List<Language> getRecentConversationLeftLanguages(Context paramContext, Languages paramLanguages)
  {
    return getRecentConversationLanguages(paramContext, "key_recent_convlang_left", paramLanguages);
  }

  public static List<Language> getRecentConversationRightLanguages(Context paramContext, Languages paramLanguages)
  {
    return getRecentConversationLanguages(paramContext, "key_recent_convlang_right", paramLanguages);
  }

  public static List<Language> getRecentFromLanguages(Context paramContext, Languages paramLanguages)
  {
    return getRecentLanguages(paramContext, "key_recent_language_from", paramLanguages);
  }

  private static List<Language> getRecentLanguages(Context paramContext, String paramString, Languages paramLanguages)
  {
    String str1 = PreferenceManager.getDefaultSharedPreferences(paramContext).getString(paramString, "");
    ArrayList localArrayList = Lists.newArrayList();
    String[] arrayOfString = str1.split("\t");
    int i = arrayOfString.length;
    int j = 0;
    if (j < i)
    {
      String str2 = arrayOfString[j];
      Language localLanguage;
      if (paramString == "key_recent_language_from")
        localLanguage = paramLanguages.getFromLanguageByShortName(str2);
      while (true)
      {
        if (localLanguage != null)
          localArrayList.add(localLanguage);
        j++;
        break;
        localLanguage = null;
        if (paramString == "key_recent_language_to")
          localLanguage = paramLanguages.getToLanguageByShortName(str2);
      }
    }
    return localArrayList;
  }

  public static List<Language> getRecentToLanguages(Context paramContext, Languages paramLanguages)
  {
    return getRecentLanguages(paramContext, "key_recent_language_to", paramLanguages);
  }

  public static boolean getShowCameraLoggingDialog(Context paramContext)
  {
    return PreferenceManager.getDefaultSharedPreferences(paramContext).getBoolean("key_show_camera_logging_dialog", Boolean.valueOf(paramContext.getString(R.string.default_show_camera_logging_dialog)).booleanValue());
  }

  public static boolean getStreamingConversation(Context paramContext)
  {
    return PreferenceManager.getDefaultSharedPreferences(paramContext).getBoolean("key_enable_streaming_conversation", Boolean.valueOf(paramContext.getString(R.string.default_enable_streaming_conversation)).booleanValue());
  }

  public static ArrayList<String> getSupportedVoiceLanguages(Context paramContext)
  {
    String str = PreferenceManager.getDefaultSharedPreferences(paramContext).getString("key_supported_voice_input_langs", "");
    if (!str.equals(""))
      return Lists.newArrayList(str.split("\t"));
    return Lists.newArrayList();
  }

  public static String getVoiceInputLanguage(Context paramContext, String paramString)
  {
    return PreferenceManager.getDefaultSharedPreferences(paramContext).getString(paramString, "");
  }

  private static boolean isEnglishLocale(Locale paramLocale)
  {
    return Locale.ENGLISH.getLanguage().equals(paramLocale.getLanguage());
  }

  public static boolean isEulaAccepted(Context paramContext)
  {
    return PreferenceManager.getDefaultSharedPreferences(paramContext).getBoolean("key_eula_accepted", false);
  }

  public static boolean isHandwritingSupported(Context paramContext, Language paramLanguage)
  {
    if ((paramLanguage == null) || (!SdkVersionWrapper.getWrapper().isHandwritingSupported()))
      return false;
    String str = getHandwritingLanguageDefaultValueKey(getHandwritingLangShortNameFromFromShortLangName(paramLanguage.getShortName()));
    try
    {
      R.string.class.getField(str);
      return true;
    }
    catch (SecurityException localSecurityException)
    {
      Logger.e("Failed to get the default preference value for key=" + str, localSecurityException);
      return false;
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      Logger.e("Failed to get the default preference value for key=" + str, localIllegalArgumentException);
      return false;
    }
    catch (NoSuchFieldException localNoSuchFieldException)
    {
    }
    return false;
  }

  public static boolean isHomeFirstRun(Context paramContext)
  {
    return PreferenceManager.getDefaultSharedPreferences(paramContext).getBoolean("key_home_first_run", false);
  }

  public static void resetFirstRunFlags(Context paramContext)
  {
    setHomeFirstRun(paramContext, false);
    setEulaAccepted(paramContext, false);
  }

  public static void setCameraInputFrontendUrl(Context paramContext, String paramString)
  {
    PreferenceManager.getDefaultSharedPreferences(paramContext).edit().putString("key_debug_camera_input_frontend_url", paramString).commit();
  }

  public static void setCameraLogging(Context paramContext, boolean paramBoolean)
  {
    PreferenceManager.getDefaultSharedPreferences(paramContext).edit().putBoolean("key_enable_camera_logging", paramBoolean).commit();
  }

  public static void setConversationLanguagePair(Context paramContext, Language paramLanguage1, Language paramLanguage2)
  {
    if ((paramLanguage1 == null) && (paramLanguage2 == null))
      return;
    SharedPreferences.Editor localEditor = PreferenceManager.getDefaultSharedPreferences(paramContext).edit();
    if (paramLanguage1 != null)
    {
      localEditor.putString("key_convlang_left", paramLanguage1.getShortName());
      addRecentLanguage(paramContext, paramLanguage1, "key_recent_convlang_left");
    }
    if (paramLanguage2 != null)
    {
      localEditor.putString("key_convlang_right", paramLanguage2.getShortName());
      addRecentLanguage(paramContext, paramLanguage2, "key_recent_convlang_right");
    }
    localEditor.commit();
  }

  public static void setCurrentVersionCode(Context paramContext, int paramInt)
  {
    PreferenceManager.getDefaultSharedPreferences(paramContext).edit().putInt("key_version_code", paramInt).commit();
  }

  public static void setEulaAccepted(Context paramContext, boolean paramBoolean)
  {
    PreferenceManager.getDefaultSharedPreferences(paramContext).edit().putBoolean("key_eula_accepted", paramBoolean).commit();
  }

  public static void setFavoriteOrder(Context paramContext, int paramInt)
  {
    PreferenceManager.getDefaultSharedPreferences(paramContext).edit().putInt("key_favorite_order", paramInt).commit();
  }

  public static void setHistoryOrder(Context paramContext, int paramInt)
  {
    PreferenceManager.getDefaultSharedPreferences(paramContext).edit().putInt("key_history_order", paramInt).commit();
  }

  public static boolean setHomeFirstRun(Context paramContext, boolean paramBoolean)
  {
    return PreferenceManager.getDefaultSharedPreferences(paramContext).edit().putBoolean("key_home_first_run", paramBoolean).commit();
  }

  public static void setInputText(Context paramContext, String paramString)
  {
    PreferenceManager.getDefaultSharedPreferences(paramContext).edit().putString("key_input_text", paramString).commit();
  }

  public static void setLanguageList(Context paramContext, String paramString, Locale paramLocale)
  {
    String str = "key_language_list_with_locale_" + Util.getLanguageShortNameByLocale(paramLocale);
    PreferenceManager.getDefaultSharedPreferences(paramContext).edit().putString(str, paramString).commit();
  }

  public static void setLanguagePair(Context paramContext, Language paramLanguage1, Language paramLanguage2)
  {
    if ((paramLanguage1 == null) || (paramLanguage2 == null))
      return;
    SharedPreferences.Editor localEditor = PreferenceManager.getDefaultSharedPreferences(paramContext).edit();
    localEditor.putString("key_language_from", paramLanguage1.getShortName());
    localEditor.putString("key_language_to", paramLanguage2.getShortName());
    localEditor.commit();
    addRecentLanguage(paramContext, paramLanguage1, "key_recent_language_from");
    addRecentLanguage(paramContext, paramLanguage2, "key_recent_language_to");
  }

  public static void setLastTranslation(Context paramContext, TranslateEntry paramTranslateEntry)
  {
    Logger.d("Profile", "setLastTranslation");
    if (paramContext == null)
      return;
    String str1 = "";
    String str2 = "";
    String str3 = "";
    String str4 = "";
    boolean bool1 = false;
    if (paramTranslateEntry != null)
    {
      boolean bool2 = paramTranslateEntry.hasInputText();
      bool1 = false;
      if (bool2)
      {
        boolean bool3 = paramTranslateEntry.hasSourceAndTargetLanguages();
        bool1 = false;
        if (bool3)
        {
          str1 = paramTranslateEntry.inputText;
          str2 = paramTranslateEntry.outputText;
          str3 = paramTranslateEntry.fromLanguage.getShortName();
          str4 = paramTranslateEntry.toLanguage.getShortName();
          bool1 = paramTranslateEntry.isAutoLanguage;
        }
      }
    }
    PreferenceManager.getDefaultSharedPreferences(paramContext).edit().putString("key_last_translation_input", str1).putString("key_last_translation_output", str2).putString("key_last_translation_from_lang", str3).putString("key_last_translation_to_lang", str4).putBoolean("key_last_translation_is_auto", bool1).commit();
  }

  public static void setShowCameraLoggingDialog(Context paramContext, boolean paramBoolean)
  {
    PreferenceManager.getDefaultSharedPreferences(paramContext).edit().putBoolean("key_show_camera_logging_dialog", paramBoolean).commit();
  }

  public static void setSupportedVoiceLanguages(Context paramContext, ArrayList<String> paramArrayList)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    Iterator localIterator = paramArrayList.iterator();
    while (localIterator.hasNext())
    {
      localStringBuilder.append((String)localIterator.next());
      localStringBuilder.append("\t");
    }
    PreferenceManager.getDefaultSharedPreferences(paramContext).edit().putString("key_supported_voice_input_langs", localStringBuilder.toString()).commit();
  }

  public static void setVoiceInputLanguage(Context paramContext, String paramString1, String paramString2)
  {
    PreferenceManager.getDefaultSharedPreferences(paramContext).edit().putString(paramString1, paramString2).commit();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.Profile
 * JD-Core Version:    0.6.2
 */