package com.google.android.apps.translate;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.text.TextUtils;

public class QsbSuggestionProvider extends ContentProvider
{
  public static final String AUTHORITY = "com.google.android.apps.translate.QsbSuggestionProvider";
  public static final String TRANSLATE_MIME_TYPE = "vnd.android.cursor.dir/vnd.com.google.android.apps.translateword";
  private static final int TRANSLATE_WORD;
  private static final UriMatcher sURIMatcher = buildUriMatcher();

  private static UriMatcher buildUriMatcher()
  {
    UriMatcher localUriMatcher = new UriMatcher(-1);
    localUriMatcher.addURI("com.google.android.apps.translate.QsbSuggestionProvider", "search_suggest_query/*", 0);
    return localUriMatcher;
  }

  public int delete(Uri paramUri, String paramString, String[] paramArrayOfString)
  {
    throw new UnsupportedOperationException();
  }

  public String getType(Uri paramUri)
  {
    switch (sURIMatcher.match(paramUri))
    {
    default:
      throw new IllegalArgumentException("Unknown URL " + paramUri);
    case 0:
    }
    return "vnd.android.cursor.dir/vnd.com.google.android.apps.translateword";
  }

  public Uri insert(Uri paramUri, ContentValues paramContentValues)
  {
    throw new UnsupportedOperationException();
  }

  public boolean onCreate()
  {
    return true;
  }

  public Cursor query(Uri paramUri, String[] paramArrayOfString1, String paramString1, String[] paramArrayOfString2, String paramString2)
  {
    if ((paramArrayOfString2 == null) || (paramArrayOfString2.length == 0))
      throw new IllegalArgumentException("selectionArgs must be provided for the Uri: " + paramUri);
    String str = paramArrayOfString2[0];
    MatrixCursor localMatrixCursor = new MatrixCursor(new String[] { "_id", "suggest_text_1", "suggest_intent_query" });
    if (!TextUtils.isEmpty(str))
    {
      Object[] arrayOfObject = new Object[3];
      arrayOfObject[0] = Integer.valueOf(0);
      arrayOfObject[1] = str;
      arrayOfObject[2] = str;
      localMatrixCursor.addRow(arrayOfObject);
    }
    return localMatrixCursor;
  }

  public int update(Uri paramUri, ContentValues paramContentValues, String paramString, String[] paramArrayOfString)
  {
    throw new UnsupportedOperationException();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.QsbSuggestionProvider
 * JD-Core Version:    0.6.2
 */