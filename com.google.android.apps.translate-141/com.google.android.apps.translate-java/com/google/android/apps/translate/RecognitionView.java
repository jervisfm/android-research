package com.google.android.apps.translate;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.CornerPathEffect;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;
import com.google.android.apps.translate.asreditor.SoundIndicator;
import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.ShortBuffer;

public class RecognitionView extends RelativeLayout
{
  private static final int INIT = 0;
  private static final int LISTENING = 1;
  private static final int STARTING = 3;
  private static final String TAG = "RecognitionView";
  private static final int WORKING = 2;
  private Context mContext;
  private Drawable mError;
  private final Drawable mErrorBorder;
  private ImageView mGotoNextStateButton;
  private ImageView mImage;
  private Drawable mInitializing;
  private final Drawable mListeningBorder;
  private View mPopupLayout;
  private View mProgress;
  private SoundIndicator mSoundIndicator;
  private ToggleButton mStartButton;
  private int mState = 0;
  private TextView mText;
  private Handler mUiHandler;
  private final Drawable mWorkingBorder;
  private ImageView mWorkingWave;

  public RecognitionView(Context paramContext)
  {
    this(paramContext, null);
  }

  public RecognitionView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    this.mContext = paramContext;
    Resources localResources = this.mContext.getResources();
    this.mListeningBorder = localResources.getDrawable(R.drawable.vs_dialog_red);
    this.mWorkingBorder = localResources.getDrawable(R.drawable.vs_dialog_blue);
    this.mErrorBorder = localResources.getDrawable(R.drawable.vs_dialog_yellow);
    this.mInitializing = localResources.getDrawable(R.drawable.mic_slash);
    this.mError = localResources.getDrawable(R.drawable.caution);
  }

  private static int getAverageAbs(ShortBuffer paramShortBuffer, int paramInt1, int paramInt2, int paramInt3)
  {
    int i = paramInt1 + paramInt2 * paramInt3;
    int j = i + paramInt3;
    int k = 0;
    for (int m = i; m < j; m++)
      k += Math.abs(paramShortBuffer.get(m));
    return k / paramInt3;
  }

  private void prepareDialog(CharSequence paramCharSequence1, Drawable paramDrawable, CharSequence paramCharSequence2)
  {
    switch (this.mState)
    {
    default:
      Logger.w("RecognitionView", "Unknown state " + this.mState);
    case 0:
    case 1:
    case 2:
    case 3:
    }
    while (true)
    {
      this.mPopupLayout.requestLayout();
      return;
      this.mText.setVisibility(4);
      this.mProgress.setVisibility(8);
      this.mImage.setVisibility(0);
      this.mImage.setImageResource(R.drawable.mic_slash);
      this.mWorkingWave.setVisibility(8);
      this.mSoundIndicator.setVisibility(8);
      this.mSoundIndicator.stop();
      this.mPopupLayout.setBackgroundDrawable(this.mListeningBorder);
      continue;
      this.mText.setVisibility(0);
      this.mText.setText(paramCharSequence1);
      this.mProgress.setVisibility(8);
      this.mImage.setVisibility(8);
      this.mWorkingWave.setVisibility(8);
      this.mSoundIndicator.setVisibility(0);
      this.mSoundIndicator.start();
      this.mPopupLayout.setBackgroundDrawable(this.mListeningBorder);
      continue;
      this.mText.setVisibility(0);
      this.mText.setText(paramCharSequence1);
      this.mProgress.setVisibility(0);
      this.mImage.setVisibility(8);
      this.mWorkingWave.setVisibility(0);
      this.mSoundIndicator.setVisibility(8);
      this.mSoundIndicator.stop();
      this.mPopupLayout.setBackgroundDrawable(this.mWorkingBorder);
      continue;
      this.mText.setVisibility(0);
      this.mText.setText(paramCharSequence1);
      this.mProgress.setVisibility(8);
      this.mImage.setVisibility(0);
      this.mImage.setImageResource(R.drawable.caution);
      this.mWorkingWave.setVisibility(8);
      this.mSoundIndicator.setVisibility(8);
      this.mSoundIndicator.stop();
      this.mPopupLayout.setBackgroundDrawable(this.mErrorBorder);
    }
  }

  private void showWave(ShortBuffer paramShortBuffer, int paramInt1, int paramInt2)
  {
    int i = ((View)this.mWorkingWave.getParent()).getWidth();
    int j = ((View)this.mWorkingWave.getParent()).getHeight();
    if ((i <= 0) || (j <= 0))
      return;
    Bitmap localBitmap = Bitmap.createBitmap(i, j, Bitmap.Config.ARGB_8888);
    Canvas localCanvas = new Canvas(localBitmap);
    Paint localPaint = new Paint();
    localPaint.setColor(-1);
    localPaint.setAntiAlias(true);
    localPaint.setStyle(Paint.Style.STROKE);
    localPaint.setAlpha(80);
    localPaint.setPathEffect(new CornerPathEffect(3.0F));
    int k = paramShortBuffer.remaining();
    int m;
    float f1;
    int i2;
    Path localPath;
    float f2;
    int i3;
    label204: int i4;
    if (paramInt2 == 0)
    {
      m = k;
      int n = paramInt1 - 2000;
      if (n < 0)
        n = 0;
      int i1 = (m - n) / 200;
      f1 = 1.0F * i / i1;
      i2 = j / 2;
      localPath = new Path();
      localCanvas.translate(0.0F, i2);
      f2 = 0.0F;
      localPath.moveTo(0.0F, 0.0F);
      i3 = 0;
      if (i3 >= i1)
        break label304;
      i4 = getAverageAbs(paramShortBuffer, n, i3, 200);
      if ((i3 & 0x1) != 0)
        break label298;
    }
    label298: for (int i5 = -1; ; i5 = 1)
    {
      float f3 = Math.min(i2, 0.0001525879F * (i4 * j)) * i5;
      localPath.lineTo(f2, f3);
      f2 += f1;
      localPath.lineTo(f2, f3);
      i3++;
      break label204;
      m = Math.min(paramInt2, k);
      break;
    }
    label304: if (f1 > 4.0F)
      localPaint.setStrokeWidth(2.0F);
    while (true)
    {
      localCanvas.drawPath(localPath, localPaint);
      this.mWorkingWave.setImageBitmap(localBitmap);
      return;
      localPaint.setStrokeWidth(Math.max(0, (int)(f1 - 0.05D)));
    }
  }

  public void finish()
  {
    this.mUiHandler.post(new Runnable()
    {
      public void run()
      {
        RecognitionView.this.mSoundIndicator.stop();
        RecognitionView.this.mStartButton.setChecked(false);
      }
    });
  }

  public void init(CompoundButton.OnCheckedChangeListener paramOnCheckedChangeListener)
  {
    this.mPopupLayout = findViewById(R.id.popup_layout);
    this.mUiHandler = new Handler();
    ((LayoutInflater)this.mContext.getSystemService("layout_inflater"));
    setVisibility(0);
    this.mImage = ((ImageView)findViewById(R.id.image));
    this.mWorkingWave = ((ImageView)findViewById(R.id.working_wave));
    this.mProgress = findViewById(R.id.progress);
    this.mSoundIndicator = ((SoundIndicator)findViewById(R.id.sound_indicator));
    this.mStartButton = ((ToggleButton)findViewById(R.id.start_recognition_button));
    this.mStartButton.setChecked(true);
    this.mStartButton.setOnCheckedChangeListener(paramOnCheckedChangeListener);
    this.mText = ((TextView)findViewById(R.id.text));
    this.mGotoNextStateButton = ((ImageView)findViewById(R.id.image));
    this.mGotoNextStateButton.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
      }
    });
  }

  public void restoreState()
  {
    this.mUiHandler.post(new Runnable()
    {
      public void run()
      {
        if (RecognitionView.this.mState == 2)
        {
          ((ProgressBar)RecognitionView.this.mProgress).setIndeterminate(false);
          ((ProgressBar)RecognitionView.this.mProgress).setIndeterminate(true);
        }
      }
    });
  }

  public void showError(final String paramString)
  {
    this.mUiHandler.post(new Runnable()
    {
      public void run()
      {
        RecognitionView.access$002(RecognitionView.this, 3);
        RecognitionView.this.prepareDialog(paramString, RecognitionView.this.mError, RecognitionView.this.mContext.getText(R.string.ok));
      }
    });
  }

  public void showInitializing()
  {
    this.mUiHandler.post(new Runnable()
    {
      public void run()
      {
        RecognitionView.access$002(RecognitionView.this, 0);
        RecognitionView.this.prepareDialog(RecognitionView.this.mContext.getText(R.string.voice_initializing), RecognitionView.this.mInitializing, RecognitionView.this.mContext.getText(R.string.cancel));
      }
    });
  }

  public void showListening()
  {
    Logger.d("RecognitionView", "#showListening");
    this.mUiHandler.post(new Runnable()
    {
      public void run()
      {
        RecognitionView.access$002(RecognitionView.this, 1);
        RecognitionView.this.prepareDialog(RecognitionView.this.mContext.getText(R.string.voice_listening), null, RecognitionView.this.mContext.getText(R.string.cancel));
      }
    });
  }

  public void showWaiting()
  {
    Logger.d("RecognitionView", "#showWaiting");
    this.mUiHandler.post(new Runnable()
    {
      public void run()
      {
        RecognitionView.access$002(RecognitionView.this, 0);
        RecognitionView.this.prepareDialog(RecognitionView.this.mContext.getText(R.string.voice_waiting), null, RecognitionView.this.mContext.getText(R.string.start_voice_input));
      }
    });
  }

  public void showWorking(final ByteArrayOutputStream paramByteArrayOutputStream, final int paramInt1, final int paramInt2)
  {
    this.mUiHandler.post(new Runnable()
    {
      public void run()
      {
        RecognitionView.access$002(RecognitionView.this, 2);
        RecognitionView.this.prepareDialog(RecognitionView.this.mContext.getText(R.string.voice_working), null, RecognitionView.this.mContext.getText(R.string.cancel));
        ShortBuffer localShortBuffer = ByteBuffer.wrap(paramByteArrayOutputStream.toByteArray()).order(ByteOrder.nativeOrder()).asShortBuffer();
        localShortBuffer.position(0);
        paramByteArrayOutputStream.reset();
        RecognitionView.this.showWave(localShortBuffer, paramInt1 / 2, paramInt2 / 2);
      }
    });
  }

  public void updateVoiceMeter(float paramFloat)
  {
    this.mSoundIndicator.setRmsdB(paramFloat);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.RecognitionView
 * JD-Core Version:    0.6.2
 */