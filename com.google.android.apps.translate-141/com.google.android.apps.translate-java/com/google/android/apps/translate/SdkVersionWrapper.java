package com.google.android.apps.translate;

import android.app.ActionBar;
import android.app.Activity;
import android.content.ClipData;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import android.view.Window;
import android.widget.TextView;
import com.google.android.apps.translate.home.TabMenuFragment;
import com.google.userfeedback.android.api.UserFeedback;
import com.google.userfeedback.android.api.UserFeedbackSpec;

public class SdkVersionWrapper
{
  private static final String FEEDBACK_CATEGORY_TAG = "com.google.android.apps.translate.USER_INITIATED_FEEDBACK_REPORT";
  private static final String FEEDBACK_LOG_FILTER = "AndroidRuntime:V *:S";
  private static final String TAG = "SdkVersionWrapper";
  private static final WrapperBase sWrappper = createWrapperBySdkVersion();

  private static WrapperBase createWrapperBySdkVersion()
  {
    switch (Util.getSdkVersion())
    {
    default:
      return new Version14(null);
    case 3:
      return new WrapperBase();
    case 4:
      return new Version4(null);
    case 5:
    case 6:
      return new Version5(null);
    case 7:
    case 8:
    case 9:
    case 10:
      return new Version7(null);
    case 11:
    case 12:
    case 13:
    }
    return new Version11(null);
  }

  public static WrapperBase getWrapper()
  {
    return sWrappper;
  }

  private static class Version11 extends SdkVersionWrapper.Version7
  {
    private Version11()
    {
      super();
    }

    public void copyToClipBoard(Context paramContext, String paramString)
    {
      ((android.content.ClipboardManager)paramContext.getSystemService("clipboard")).setPrimaryClip(ClipData.newPlainText(paramString, paramString));
    }

    public void invalidateOptionsMenu(Activity paramActivity)
    {
      paramActivity.invalidateOptionsMenu();
    }

    public void setActionBarTitle(Activity paramActivity, ActionBarSpinnerAdapter paramActionBarSpinnerAdapter)
    {
      ActionBar localActionBar = paramActivity.getActionBar();
      if (paramActionBarSpinnerAdapter == null)
      {
        localActionBar.setNavigationMode(0);
        localActionBar.setListNavigationCallbacks(null, null);
        ActionBarSpinnerAdapter.setActionBarTitle(paramActivity, TranslateBaseActivity.getTitle(paramActivity));
        return;
      }
      if (paramActionBarSpinnerAdapter.isTabMenuMode())
      {
        TabMenuFragment.setTabMenuFragmentVisibility(paramActivity, true);
        localActionBar.setNavigationMode(0);
        localActionBar.setListNavigationCallbacks(null, null);
        ActionBarSpinnerAdapter.setActionBarTitle(paramActivity, TranslateBaseActivity.getTitle(paramActivity, paramActionBarSpinnerAdapter.getCurrentFragment()));
        paramActionBarSpinnerAdapter.setTabWidth();
        return;
      }
      TabMenuFragment.setTabMenuFragmentVisibility(paramActivity, false);
      localActionBar.setNavigationMode(1);
      localActionBar.setListNavigationCallbacks(paramActionBarSpinnerAdapter, paramActionBarSpinnerAdapter);
      localActionBar.setSelectedNavigationItem(paramActionBarSpinnerAdapter.getSelectedNavigationIndex());
      ActionBarSpinnerAdapter.removeActionBarTitle(localActionBar);
    }

    public void setHomeButton(Activity paramActivity, boolean paramBoolean)
    {
      paramActivity.getActionBar().setDisplayHomeAsUpEnabled(false);
    }

    public void setTextIsSelectable(TextView paramTextView, boolean paramBoolean)
    {
      paramTextView.setTextIsSelectable(paramBoolean);
    }

    public boolean useFragments()
    {
      return true;
    }
  }

  private static class Version14 extends SdkVersionWrapper.Version11
  {
    private Version14()
    {
      super();
    }

    public void sendFeedback(Activity paramActivity, boolean paramBoolean)
    {
      if ((paramBoolean) || (!TranslateApplication.isReleaseBuild(paramActivity)))
      {
        super.sendFeedback(paramActivity, paramBoolean);
        return;
      }
      paramActivity.bindService(new Intent("android.intent.action.BUG_REPORT"), new ServiceConnection()
      {
        public void onServiceConnected(ComponentName paramAnonymousComponentName, IBinder paramAnonymousIBinder)
        {
          try
          {
            paramAnonymousIBinder.transact(1, Parcel.obtain(), null, 0);
            return;
          }
          catch (RemoteException localRemoteException)
          {
            Logger.e("SdkVersionWrapper", "Failed to send feedback. " + localRemoteException.getMessage());
          }
        }

        public void onServiceDisconnected(ComponentName paramAnonymousComponentName)
        {
        }
      }
      , 1);
    }

    public void setHomeButton(Activity paramActivity, boolean paramBoolean)
    {
      ActionBar localActionBar = paramActivity.getActionBar();
      localActionBar.setHomeButtonEnabled(false);
      localActionBar.setDisplayHomeAsUpEnabled(false);
    }

    public boolean useExternalFontsForUnsupportedLanguages()
    {
      return false;
    }
  }

  private static class Version4 extends SdkVersionWrapper.WrapperBase
  {
    public boolean isHandwritingSupported()
    {
      return true;
    }

    public boolean isLargeScreen(Context paramContext)
    {
      return (0xF & paramContext.getResources().getConfiguration().screenLayout) >= 3;
    }
  }

  private static class Version5 extends SdkVersionWrapper.Version4
  {
    private Version5()
    {
      super();
    }

    public void removeTransitionAnimation(Activity paramActivity)
    {
      paramActivity.overridePendingTransition(0, 0);
    }

    public void setIntentNoAnimation(Intent paramIntent)
    {
      paramIntent.addFlags(65536);
    }
  }

  private static class Version7 extends SdkVersionWrapper.Version5
  {
    private Version7()
    {
      super();
    }

    public boolean hasFeatureTelephony(Context paramContext)
    {
      if (paramContext == null)
        return true;
      return paramContext.getPackageManager().hasSystemFeature("android.hardware.telephony");
    }
  }

  public static class WrapperBase
  {
    public void copyToClipBoard(Context paramContext, String paramString)
    {
      ((android.text.ClipboardManager)paramContext.getSystemService("clipboard")).setText(paramString);
    }

    public boolean hasFeatureTelephony(Context paramContext)
    {
      return true;
    }

    public void invalidateOptionsMenu(Activity paramActivity)
    {
    }

    public boolean isHandwritingSupported()
    {
      return false;
    }

    public boolean isLargeScreen(Context paramContext)
    {
      return false;
    }

    public void removeTransitionAnimation(Activity paramActivity)
    {
    }

    public void sendFeedback(Activity paramActivity, boolean paramBoolean)
    {
      UserFeedbackSpec localUserFeedbackSpec = new UserFeedbackSpec(paramActivity, paramActivity.getWindow().getDecorView(), "AndroidRuntime:V *:S", "com.google.android.apps.translate.USER_INITIATED_FEEDBACK_REPORT");
      localUserFeedbackSpec.setScreenshotEnabled(paramBoolean);
      new UserFeedback().startFeedback(localUserFeedbackSpec);
    }

    public void setActionBarTitle(Activity paramActivity, ActionBarSpinnerAdapter paramActionBarSpinnerAdapter)
    {
    }

    public void setHomeButton(Activity paramActivity, boolean paramBoolean)
    {
    }

    public void setIntentNoAnimation(Intent paramIntent)
    {
    }

    public void setTextIsSelectable(TextView paramTextView, boolean paramBoolean)
    {
    }

    public boolean useExternalFontsForUnsupportedLanguages()
    {
      return true;
    }

    public boolean useFragments()
    {
      return false;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.SdkVersionWrapper
 * JD-Core Version:    0.6.2
 */