package com.google.android.apps.translate;

import java.util.HashSet;
import java.util.TreeSet;

public class Sets
{
  public static <T> HashSet<T> newHashSet()
  {
    return new HashSet();
  }

  public static <T> TreeSet<T> newTreeSet()
  {
    return new TreeSet();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.Sets
 * JD-Core Version:    0.6.2
 */