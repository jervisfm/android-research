package com.google.android.apps.translate;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.preference.PreferenceGroup;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.text.Html;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.google.android.apps.translate.offline.ModelManagementActivity;
import com.google.android.apps.unveil.textinput.TextInputView;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class SettingsActivity extends PreferenceActivity
{
  private static final String LANGUAGE_PREFIX = "lang_";
  private static final String PACKAGE_NAME = "com.google.android.apps.translate";
  private static final String STRING_TYPE = "string";
  public static final boolean SUPPORT_VOICE_LANG_PREF = true;
  private static final String TAG = "SettingsActivity";
  Languages mLanguageList;
  private final BroadcastReceiver mOnLanguagesChanged = new BroadcastReceiver()
  {
    public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
    {
      SettingsActivity.this.refreshLanguageList();
    }
  };
  VoiceInputHelper mVoiceInputHelper;

  public static String getAboutInfoText(Context paramContext)
  {
    String str = paramContext.getString(R.string.about_info);
    Object[] arrayOfObject = new Object[2];
    arrayOfObject[0] = Util.getVersionName(paramContext);
    arrayOfObject[1] = Integer.valueOf(Util.getVersionCode(paramContext));
    return String.format(str, arrayOfObject);
  }

  private List<Language> getVoiceInputLangaugesFromStrings(List<String> paramList)
  {
    ArrayList localArrayList = Lists.newArrayList();
    Iterator localIterator = paramList.iterator();
    while (localIterator.hasNext())
    {
      String str1 = (String)localIterator.next();
      String str2 = getVoiceInputLanguageName(str1);
      if (!TextUtils.isEmpty(str2))
        localArrayList.add(new Language(str1, str2));
    }
    Collections.sort(localArrayList);
    return localArrayList;
  }

  private String getVoiceInputLanguageName(String paramString)
  {
    String str1 = getVoiceInputLanguageNameFromResourceFile(paramString);
    if (!TextUtils.isEmpty(str1))
      return str1;
    if (VoiceInputHelper.isDogfoodVoiceInputLanguage(paramString))
    {
      String str2 = VoiceInputHelper.getVoiceInputLanguageNameFromDogfoodName(paramString);
      String str3 = getVoiceInputLanguageNameFromResourceFile(str2);
      if ((!TextUtils.isEmpty(str3)) && (!str3.equals(str2)))
        return str3 + " : " + paramString;
      return paramString;
    }
    Logger.d("SettingsActivity", "Unrecognized voice input language: [" + paramString + "]");
    return "";
  }

  private String getVoiceInputLanguageNameFromResourceFile(String paramString)
  {
    int i = getResources().getIdentifier("lang_" + paramString.toLowerCase().replace('-', '_'), "string", "com.google.android.apps.translate");
    if (i != 0)
      return getString(i);
    return "";
  }

  private void openAboutDialog()
  {
    String str = getAboutInfoText(this);
    View localView = View.inflate(this, R.layout.about, null);
    ((TextView)localView.findViewById(R.id.about_info)).setText(Html.fromHtml(str));
    TextView localTextView = (TextView)localView.findViewById(R.id.feedback);
    localTextView.setText(Html.fromHtml("<u>" + getString(R.string.label_send_feedback) + "</u>"));
    localTextView.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        SdkVersionWrapper.getWrapper().sendFeedback(SettingsActivity.this, false);
      }
    });
    ((TextView)localView.findViewById(R.id.tos_link)).setMovementMethod(LinkMovementMethod.getInstance());
    ((TextView)localView.findViewById(R.id.privacy_link)).setMovementMethod(LinkMovementMethod.getInstance());
    new AlertDialog.Builder(this).setTitle(getString(R.string.app_name)).setView(localView).setIcon(R.drawable.icon).setPositiveButton(getString(17039370), new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        paramAnonymousDialogInterface.dismiss();
      }
    }).show();
  }

  private void openOfflineModelManagement()
  {
    startActivity(new Intent(this, ModelManagementActivity.class));
  }

  private void refreshLanguageList()
  {
    this.mLanguageList = Util.getLanguageListFromProfile(this);
  }

  private void setCameraInputFrontendUrlChecked(ListView paramListView, List<String> paramList, String paramString)
  {
    int i = paramList.size();
    for (int j = 0; ; j++)
      if (j < i)
      {
        if ((((String)paramList.get(j)).equals(paramString)) && (paramListView.getCheckedItemPosition() != j))
          paramListView.setItemChecked(j, true);
      }
      else
        return;
  }

  private void setCameraInputFrontendUrlOnClickListener(final Dialog paramDialog, final ListView paramListView)
  {
    paramListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
    {
      public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
      {
        Profile.setCameraInputFrontendUrl(SettingsActivity.this, (String)paramListView.getItemAtPosition(paramAnonymousInt));
        SettingsActivity.this.setupCameraInputDebugPreferences();
        paramDialog.dismiss();
      }
    });
  }

  private void setCameraInputFrontendUrlsListView(ListView paramListView, List<String> paramList)
  {
    ArrayAdapter localArrayAdapter = new ArrayAdapter(this, R.layout.debug_camera_input_frontend_url, R.id.debug_camera_input_frontend_url_text, paramList);
    paramListView.setDivider(getResources().getDrawable(17301522));
    paramListView.setAdapter(localArrayAdapter);
    paramListView.setChoiceMode(1);
    setCameraInputFrontendUrlChecked(paramListView, paramList, Profile.getCameraInputFrontendUrl(this));
  }

  private void setVoiceInputLanguageChecked(ListView paramListView, List<Language> paramList, String paramString)
  {
    int i = paramList.size();
    for (int j = 0; ; j++)
      if (j < i)
      {
        if ((((Language)paramList.get(j)).getShortName().equals(paramString)) && (paramListView.getCheckedItemPosition() != j))
          paramListView.setItemChecked(j, true);
      }
      else
        return;
  }

  private void setVoiceInputLanguageOnClickListener(final Dialog paramDialog, final ListView paramListView, final String paramString)
  {
    paramListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
    {
      public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
      {
        Profile.setVoiceInputLanguage(SettingsActivity.this, paramString, ((Language)paramListView.getItemAtPosition(paramAnonymousInt)).getShortName());
        SettingsActivity.this.setupVoiceInputPreferences();
        paramDialog.dismiss();
      }
    });
  }

  private void setVoiceInputLanguagesListView(ListView paramListView, String paramString, List<Language> paramList)
  {
    ArrayAdapter localArrayAdapter = new ArrayAdapter(this, R.layout.voice_input_language, R.id.text_language_name, paramList);
    paramListView.setDivider(getResources().getDrawable(17301522));
    paramListView.setAdapter(localArrayAdapter);
    paramListView.setChoiceMode(1);
    setVoiceInputLanguageChecked(paramListView, paramList, Profile.getVoiceInputLanguage(this, paramString));
  }

  private void setupCameraInputDebugPreferences()
  {
    Preference localPreference = findPreference("key_debug_camera_frontend_url");
    localPreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener()
    {
      public boolean onPreferenceClick(Preference paramAnonymousPreference)
      {
        SettingsActivity.this.showCameraInputFrontendUrlListDialog();
        return true;
      }
    });
    String str = Profile.getCameraInputFrontendUrl(this);
    if (!str.equals(""))
    {
      localPreference.setSummary(str);
      return;
    }
    localPreference.setSummary("Use default frontend URL");
  }

  private void setupPreferenceListeners()
  {
    getPreferenceManager().findPreference(getString(R.string.key_settings_about)).setOnPreferenceClickListener(new Preference.OnPreferenceClickListener()
    {
      public boolean onPreferenceClick(Preference paramAnonymousPreference)
      {
        SettingsActivity.this.openAboutDialog();
        return true;
      }
    });
    if (ModelManagementActivity.isOfflineTranslateSupported(this))
      getPreferenceManager().findPreference(getString(R.string.key_manage_offline_model)).setOnPreferenceClickListener(new Preference.OnPreferenceClickListener()
      {
        public boolean onPreferenceClick(Preference paramAnonymousPreference)
        {
          SettingsActivity.this.openOfflineModelManagement();
          return true;
        }
      });
  }

  private void setupVoiceInputPreferences()
  {
    PreferenceGroup localPreferenceGroup = (PreferenceGroup)findPreference(getString(R.string.key_settings_voice_input));
    localPreferenceGroup.removeAll();
    PreferenceCategory localPreferenceCategory = new PreferenceCategory(this);
    localPreferenceCategory.setTitle(getString(R.string.title_voice_input_locale_settings));
    localPreferenceGroup.addPreference(localPreferenceCategory);
    List localList1 = this.mVoiceInputHelper.getTranslateLanguageList(this.mLanguageList);
    Collections.sort(localList1);
    Iterator localIterator = localList1.iterator();
    while (localIterator.hasNext())
    {
      Language localLanguage = (Language)localIterator.next();
      List localList2 = this.mVoiceInputHelper.getVoiceInputLanguageList(localLanguage.getShortName());
      if ((localLanguage != null) && (localList2.size() > 1) && (this.mVoiceInputHelper.isVoiceInputAvailable(localLanguage)))
      {
        Preference localPreference = new Preference(this);
        localPreference.setTitle(localLanguage.getLongName());
        localPreference.setKey(localLanguage.getShortName());
        localPreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener()
        {
          public boolean onPreferenceClick(Preference paramAnonymousPreference)
          {
            SettingsActivity.this.showVoiceInputLanguagesListDialog(paramAnonymousPreference.getKey());
            return true;
          }
        });
        String str = Profile.getVoiceInputLanguage(this, localLanguage.getShortName());
        if (!str.equals(""))
          localPreference.setSummary(getVoiceInputLanguageName(str));
        while (true)
        {
          localPreferenceCategory.addPreference(localPreference);
          break;
          localPreference.setSummary(R.string.summary_voice_input_locale_default);
        }
      }
    }
    if (localPreferenceGroup.getPreferenceCount() == 0)
      localPreferenceGroup.setEnabled(false);
  }

  private void showCameraInputFrontendUrlListDialog()
  {
    List localList = Profile.getCameraInputFrontendUrls(this);
    if (localList == null)
      return;
    View localView = View.inflate(this, R.layout.debug_camera_frontend_urls, null);
    ListView localListView = (ListView)localView.findViewById(R.id.debug_camera_input_frontend_urls);
    setCameraInputFrontendUrlsListView(localListView, localList);
    AlertDialog localAlertDialog = new AlertDialog.Builder(this).setView(localView).setTitle("[DEBUG] Set Camera Input Frontend URL").setNegativeButton(17039360, new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        paramAnonymousDialogInterface.dismiss();
      }
    }).create();
    setCameraInputFrontendUrlOnClickListener(localAlertDialog, localListView);
    localAlertDialog.show();
  }

  private void showVoiceInputLanguagesListDialog(String paramString)
  {
    List localList = this.mVoiceInputHelper.getVoiceInputLanguageList(paramString);
    if ((localList == null) || (localList.size() == 1))
      return;
    View localView = View.inflate(this, R.layout.change_languages, null);
    ListView localListView = (ListView)localView.findViewById(R.id.voice_input_languages);
    setVoiceInputLanguagesListView(localListView, paramString, getVoiceInputLangaugesFromStrings(localList));
    AlertDialog localAlertDialog = new AlertDialog.Builder(this).setView(localView).setTitle(R.string.title_voice_input_languages).setIcon(17301659).setNegativeButton(17039360, new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
      {
        paramAnonymousDialogInterface.dismiss();
      }
    }).create();
    setVoiceInputLanguageOnClickListener(localAlertDialog, localListView, paramString);
    localAlertDialog.show();
  }

  protected void onCreate(Bundle paramBundle)
  {
    TranslateApplication localTranslateApplication = (TranslateApplication)getApplication();
    this.mVoiceInputHelper = localTranslateApplication.getVoiceInputHelper();
    refreshLanguageList();
    TranslateBaseActivity.beforeSetContentView(this);
    super.onCreate(paramBundle);
    addPreferencesFromResource(R.xml.settings_activity);
    TranslateBaseActivity.afterSetContentView(this);
    if (!ModelManagementActivity.isOfflineTranslateSupported(this))
      ((PreferenceScreen)findPreference("key_root_settings")).removePreference(findPreference("key_offline_translate_settings"));
    if (!TextInputView.isSupported(this))
      ((PreferenceCategory)findPreference("key_title_settings")).removePreference(findPreference("key_enable_camera_logging"));
    setupVoiceInputPreferences();
    if (!localTranslateApplication.isReleaseBuild())
      setupCameraInputDebugPreferences();
    while (true)
    {
      setupPreferenceListeners();
      registerReceiver(this.mOnLanguagesChanged, new IntentFilter("com.google.android.apps.translate.broadcast.LANGUAGES_CHANGED"));
      return;
      ((PreferenceCategory)findPreference("key_title_settings")).removePreference(findPreference("key_debug_camera_frontend_url"));
    }
  }

  protected void onDestroy()
  {
    unregisterReceiver(this.mOnLanguagesChanged);
    super.onDestroy();
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    switch (paramMenuItem.getItemId())
    {
    default:
      return super.onOptionsItemSelected(paramMenuItem);
    case 16908332:
    }
    Util.openHomeActivity(this);
    return true;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.SettingsActivity
 * JD-Core Version:    0.6.2
 */