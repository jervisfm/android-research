package com.google.android.apps.translate;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;

public class SmsPickerActivity extends ListActivity
  implements AdapterView.OnItemClickListener
{
  private SmsPickerHelper mSmsPickerHelper = new SmsPickerHelper();

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    this.mSmsPickerHelper.onCreate(this);
    LinearLayout localLinearLayout = (LinearLayout)getLayoutInflater().inflate(R.layout.sms_picker_activity, null);
    TranslateBaseActivity.beforeSetContentView(this);
    setContentView(localLinearLayout);
    TranslateBaseActivity.afterSetContentView(this);
    this.mSmsPickerHelper.init(localLinearLayout);
    setListAdapter(this.mSmsPickerHelper.getListAdapter());
    getListView().setOnItemClickListener(this);
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    if (this.mSmsPickerHelper.onCreateOptionsMenu(paramMenu, getMenuInflater()))
      return true;
    return super.onCreateOptionsMenu(paramMenu);
  }

  public void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    String str = this.mSmsPickerHelper.onItemClick(paramAdapterView, paramView, paramInt, paramLong);
    if (!TextUtils.isEmpty(str))
      setResult(-1, new Intent().putExtra("android.intent.extra.TEXT", str));
    finish();
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    switch (paramMenuItem.getItemId())
    {
    default:
      if (this.mSmsPickerHelper.onOptionsItemSelected(paramMenuItem))
        return true;
      break;
    case 16908332:
      Util.openHomeActivity(this);
      return true;
    }
    return super.onOptionsItemSelected(paramMenuItem);
  }

  public boolean onPrepareOptionsMenu(Menu paramMenu)
  {
    this.mSmsPickerHelper.onPrepareOptionsMenu(paramMenu);
    return super.onPrepareOptionsMenu(paramMenu);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.SmsPickerActivity
 * JD-Core Version:    0.6.2
 */