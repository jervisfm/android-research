package com.google.android.apps.translate;

import android.app.Activity;
import android.app.ListFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import com.google.android.apps.translate.translation.TranslateFragment;

public class SmsPickerFragment extends ListFragment
  implements AdapterView.OnItemClickListener
{
  private static final String TAG = "SmsPickerFragment";
  private Activity mActivity;
  private SmsPickerHelper mSmsPickerHelper = new SmsPickerHelper();

  public void onActivityCreated(Bundle paramBundle)
  {
    Logger.d("SmsPickerFragment", "onActivityCreated");
    super.onActivityCreated(paramBundle);
    this.mActivity = getActivity();
    this.mSmsPickerHelper.onCreate(this.mActivity);
    this.mSmsPickerHelper.init((LinearLayout)getView());
    setListAdapter(this.mSmsPickerHelper.getListAdapter());
    getListView().setOnItemClickListener(this);
  }

  public void onCreate(Bundle paramBundle)
  {
    Logger.d("SmsPickerFragment", "onCreate");
    super.onCreate(paramBundle);
    setHasOptionsMenu(true);
  }

  public void onCreateOptionsMenu(Menu paramMenu, MenuInflater paramMenuInflater)
  {
    Logger.d("SmsPickerFragment", "onCreateOptionsMenu");
    if (this.mSmsPickerHelper.onCreateOptionsMenu(paramMenu, paramMenuInflater))
      return;
    super.onCreateOptionsMenu(paramMenu, paramMenuInflater);
  }

  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
    return paramLayoutInflater.inflate(R.layout.sms_picker_activity, null);
  }

  public void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    UserActivityMgr.get().setTranslationSource(UserActivityMgr.RequestSource.SMS).setTranslationInputMethod(UserActivityMgr.InputMethod.UNKNOWN);
    TranslateFragment.startTranslateFragment(this.mActivity, this.mSmsPickerHelper.onItemClick(paramAdapterView, paramView, paramInt, paramLong), null, null, true);
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    Logger.d("SmsPickerFragment", "onOptionsItemSelected");
    if ((this.mSmsPickerHelper != null) && (this.mSmsPickerHelper.onOptionsItemSelected(paramMenuItem)))
    {
      this.mActivity.invalidateOptionsMenu();
      return true;
    }
    return super.onOptionsItemSelected(paramMenuItem);
  }

  public void onPrepareOptionsMenu(Menu paramMenu)
  {
    Logger.d("SmsPickerFragment", "onPrepareOptionsMenu");
    this.mSmsPickerHelper.onPrepareOptionsMenu(paramMenu);
    super.onPrepareOptionsMenu(paramMenu);
  }

  public void onResume()
  {
    Logger.d("SmsPickerFragment", "onResume");
    super.onResume();
    SdkVersionWrapper.getWrapper().setHomeButton(this.mActivity, true);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.SmsPickerFragment
 * JD-Core Version:    0.6.2
 */