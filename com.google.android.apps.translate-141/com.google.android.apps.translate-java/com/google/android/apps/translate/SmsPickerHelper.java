package com.google.android.apps.translate;

import android.app.Activity;
import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ProgressBar;
import android.widget.SimpleCursorAdapter;
import android.widget.SimpleCursorAdapter.ViewBinder;
import android.widget.TextView;

public class SmsPickerHelper
  implements SimpleCursorAdapter.ViewBinder
{
  private static final String ADDRESS = "address";
  private static final String BODY = "body";
  private static final String DATE = "date";
  private static final String[] PROJECTION = { "_id", "address", "date", "body" };
  private static final Uri SMS_CONTENT_URI = Uri.parse("content://sms/inbox");
  private static final String TAG = "SmsPickerHelper";
  private Activity mActivity;
  int mAddressColumnIndex;
  int mBodyColumnIndex;
  Cursor mCursor;
  int mDateColumnIndex;
  TextView mEmptyMessage;
  private SimpleCursorAdapter mListAdapter;
  ProgressBar mProgressBar;
  private QueryHandler mQueryHandler;

  private void startQuerySMS()
  {
    this.mListAdapter.changeCursor(null);
    this.mProgressBar.setVisibility(0);
    this.mEmptyMessage.setText(R.string.msg_loading);
    this.mQueryHandler.startQuery(0, null, SMS_CONTENT_URI, PROJECTION, null, null, null);
  }

  public ListAdapter getListAdapter()
  {
    return this.mListAdapter;
  }

  void init(LinearLayout paramLinearLayout)
  {
    Logger.d("SmsPickerHelper", "init");
    this.mProgressBar = ((ProgressBar)paramLinearLayout.findViewById(R.id.progress));
    this.mEmptyMessage = ((TextView)paramLinearLayout.findViewById(R.id.msg_empty));
    this.mQueryHandler = new QueryHandler(this.mActivity.getContentResolver());
    Activity localActivity = this.mActivity;
    int i = R.layout.sms_item;
    String[] arrayOfString = { "address", "date", "body" };
    int[] arrayOfInt = new int[3];
    arrayOfInt[0] = R.id.sms_person;
    arrayOfInt[1] = R.id.sms_date;
    arrayOfInt[2] = R.id.sms_body;
    this.mListAdapter = new SimpleCursorAdapter(localActivity, i, null, arrayOfString, arrayOfInt);
    this.mListAdapter.setViewBinder(this);
    startQuerySMS();
  }

  void onCreate(Activity paramActivity)
  {
    this.mActivity = paramActivity;
  }

  public boolean onCreateOptionsMenu(Menu paramMenu, MenuInflater paramMenuInflater)
  {
    paramMenuInflater.inflate(R.menu.sms_picker_activity_menu, paramMenu);
    return true;
  }

  public String onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong)
  {
    Cursor localCursor = this.mCursor;
    if (localCursor.moveToPosition(paramInt))
      return localCursor.getString(this.mBodyColumnIndex);
    return "";
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    int i = paramMenuItem.getItemId();
    int j = R.id.menu_feedback;
    boolean bool = false;
    if (i == j)
    {
      SdkVersionWrapper.getWrapper().sendFeedback(this.mActivity, false);
      bool = true;
    }
    return bool;
  }

  public void onPrepareOptionsMenu(Menu paramMenu)
  {
  }

  public boolean setViewValue(View paramView, Cursor paramCursor, int paramInt)
  {
    if (paramInt == this.mAddressColumnIndex)
      ((TextView)paramView).setText(paramCursor.getString(this.mAddressColumnIndex));
    while (true)
    {
      return true;
      if (paramInt == this.mDateColumnIndex)
        ((TextView)paramView).setText(Util.formatTimeStampString(this.mActivity, paramCursor.getLong(paramInt)));
      else if (paramInt == this.mBodyColumnIndex)
        ((TextView)paramView).setText(paramCursor.getString(paramInt));
    }
  }

  private class QueryHandler extends AsyncQueryHandler
  {
    public QueryHandler(ContentResolver arg2)
    {
      super();
    }

    protected void onQueryComplete(int paramInt, Object paramObject, Cursor paramCursor)
    {
      if (SmsPickerHelper.this.mActivity.isFinishing())
      {
        paramCursor.close();
        return;
      }
      if (paramCursor.getCount() == 0)
      {
        SmsPickerHelper.this.mProgressBar.setVisibility(8);
        SmsPickerHelper.this.mEmptyMessage.setText(R.string.msg_no_sms);
        return;
      }
      if (SmsPickerHelper.this.mCursor != null)
        SmsPickerHelper.this.mActivity.stopManagingCursor(SmsPickerHelper.this.mCursor);
      SmsPickerHelper.this.mAddressColumnIndex = paramCursor.getColumnIndex("address");
      SmsPickerHelper.this.mDateColumnIndex = paramCursor.getColumnIndex("date");
      SmsPickerHelper.this.mBodyColumnIndex = paramCursor.getColumnIndex("body");
      SmsPickerHelper.this.mCursor = paramCursor;
      SmsPickerHelper.this.mActivity.startManagingCursor(paramCursor);
      SmsPickerHelper.this.mListAdapter.changeCursor(paramCursor);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.SmsPickerHelper
 * JD-Core Version:    0.6.2
 */