package com.google.android.apps.translate;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

public class SupersizeTextActivity extends Activity
{
  public static final boolean SUPPORT_ROTATE_TO_SUPERTEXT = false;
  private static final String TAG = "SupersizeTextActivity";
  private View mExitBtn;
  private Language mFromLanguage;
  private SupersizeTextViewHelper mSupersizeHelper;
  private String mSupersizeText;
  private TextView mTextView;
  private Language mToLanguage;
  private String mTranslateInputText;
  private boolean mTriggeredByGesture = false;

  private Rect findAvailableSpace()
  {
    Display localDisplay = ((WindowManager)getSystemService("window")).getDefaultDisplay();
    int i = localDisplay.getHeight();
    return new Rect(0, 0, localDisplay.getWidth() - (this.mTextView.getPaddingLeft() + this.mTextView.getPaddingRight()) - (this.mExitBtn.getPaddingLeft() + this.mExitBtn.getPaddingRight()), i - (this.mTextView.getPaddingTop() + this.mTextView.getPaddingBottom()) - (this.mExitBtn.getPaddingTop() + this.mExitBtn.getPaddingBottom()));
  }

  private boolean readIntent(Intent paramIntent)
  {
    this.mToLanguage = ((Language)paramIntent.getSerializableExtra("key_language_to"));
    this.mFromLanguage = ((Language)paramIntent.getSerializableExtra("key_language_from"));
    this.mTranslateInputText = paramIntent.getStringExtra("key_text_input");
    this.mSupersizeText = paramIntent.getStringExtra("key_translate_text");
    this.mTriggeredByGesture = paramIntent.getBooleanExtra("key_supersize_by_gesture", false);
    return (this.mToLanguage != null) && (this.mFromLanguage != null) && (this.mTranslateInputText != null) && (this.mSupersizeText != null);
  }

  public void onBackPressed()
  {
    finish();
  }

  public void onConfigurationChanged(Configuration paramConfiguration)
  {
    Logger.d("SupersizeTextActivity", "onConfigurationChanged");
    super.onConfigurationChanged(paramConfiguration);
    if ((this.mTriggeredByGesture) && (paramConfiguration.orientation == 1))
      finish();
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    requestWindowFeature(1);
    getWindow().setFlags(1024, 1024);
    setContentView(R.layout.supersize_text_activity);
    this.mTextView = ((TextView)findViewById(R.id.supersize_text));
    this.mExitBtn = findViewById(R.id.btn_exit);
    this.mExitBtn.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        SupersizeTextActivity.this.onBackPressed();
      }
    });
    Rect localRect = findAvailableSpace();
    this.mSupersizeHelper = new SupersizeTextViewHelper(this, this.mTextView, localRect.width(), localRect.height());
    if (!setSupersizeText(getIntent()))
      finish();
    if (this.mTriggeredByGesture)
      setRequestedOrientation(4);
  }

  protected void onDestroy()
  {
    this.mSupersizeHelper.onDestroy();
    super.onDestroy();
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if ((Util.getSdkVersion() < 5) && (paramInt == 4) && (paramKeyEvent.getRepeatCount() == 0))
      onBackPressed();
    return super.onKeyDown(paramInt, paramKeyEvent);
  }

  boolean setSupersizeText(Intent paramIntent)
  {
    Preconditions.checkNotNull(paramIntent);
    if (!readIntent(paramIntent))
      return false;
    ExternalFonts localExternalFonts = ExternalFonts.getFontByShortName(this.mToLanguage.getShortName());
    this.mTextView.setTypeface(localExternalFonts.getTypeface());
    SupersizeTextViewHelper localSupersizeTextViewHelper = this.mSupersizeHelper;
    String str = this.mSupersizeText;
    Language[] arrayOfLanguage = new Language[2];
    arrayOfLanguage[0] = this.mFromLanguage;
    arrayOfLanguage[1] = this.mToLanguage;
    localSupersizeTextViewHelper.resizeText(str, arrayOfLanguage);
    return true;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.SupersizeTextActivity
 * JD-Core Version:    0.6.2
 */