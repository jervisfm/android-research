package com.google.android.apps.translate;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Layout.Alignment;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.text.method.MovementMethod;
import android.text.method.ScrollingMovementMethod;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.ScaleGestureDetector.SimpleOnScaleGestureListener;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.TextView;
import android.widget.ZoomButtonsController;
import android.widget.ZoomButtonsController.OnZoomListener;

public class SupersizeTextViewHelper
{
  private static final int DEFAULT_FONT_SIZE = 60;
  private static final int SCALE_MIN_FONT_SIZE = 15;
  private static final float ZOOM_IN_SCALE_FACTOR = 1.1F;
  private static final float ZOOM_OUT_SCALE_FACTOR = 0.9F;
  private static final int ZOOM_SPEED = 100;
  private float mCalculatedFont = 60.0F;
  private final Context mContext;
  private int mMaxFontSizeInPixel = 60;
  private float mScaleFactor = 1.0F;
  private final TextView mTextView;
  private final View.OnTouchListener mTriggerZoomButtonsListener = new View.OnTouchListener()
  {
    public boolean onTouch(View paramAnonymousView, MotionEvent paramAnonymousMotionEvent)
    {
      if (!SupersizeTextViewHelper.this.mZoomController.isVisible())
      {
        SupersizeTextViewHelper.this.mZoomController.setVisible(true);
        SupersizeTextViewHelper.this.updateZoomButtonsEnabled();
      }
      return false;
    }
  };
  private final int mViewHeight;
  private final int mViewWidth;
  private ZoomButtonsController mZoomController;
  private float mZoomMaxScale;
  private float mZoomMinScale;

  public SupersizeTextViewHelper(Context paramContext, TextView paramTextView, int paramInt1, int paramInt2)
  {
    this.mContext = ((Context)Preconditions.checkNotNull(paramContext));
    this.mTextView = ((TextView)Preconditions.checkNotNull(paramTextView));
    if (paramInt1 > 0);
    for (boolean bool1 = true; ; bool1 = false)
    {
      Preconditions.checkArguments(bool1);
      boolean bool2 = false;
      if (paramInt2 > 0)
        bool2 = true;
      Preconditions.checkArguments(bool2);
      this.mViewWidth = paramInt1;
      this.mViewHeight = paramInt2;
      this.mMaxFontSizeInPixel = this.mViewWidth;
      enableTextScrolling(this.mTextView, true);
      setupZoomSupport();
      return;
    }
  }

  private static void calcTextLayoutInfo(TextLayoutInfo paramTextLayoutInfo, String paramString, int paramInt1, TextPaint paramTextPaint, int paramInt2)
  {
    if (paramInt2 > 0);
    for (boolean bool = true; ; bool = false)
    {
      Preconditions.checkArguments(bool);
      paramTextPaint.setTextSize(paramInt2);
      StaticLayout localStaticLayout = new StaticLayout(paramString, paramTextPaint, paramInt1, Layout.Alignment.ALIGN_NORMAL, 1.0F, 0.0F, false);
      paramTextLayoutInfo.requiredHeight = localStaticLayout.getHeight();
      paramTextLayoutInfo.requiredLines = localStaticLayout.getLineCount();
      return;
    }
  }

  private static TextPaint createTestPaint(Typeface paramTypeface, int paramInt)
  {
    TextPaint localTextPaint = new TextPaint();
    localTextPaint.setTypeface(paramTypeface);
    localTextPaint.setTextSize(paramInt);
    return localTextPaint;
  }

  private static void enableTextScrolling(TextView paramTextView, boolean paramBoolean)
  {
    if (paramBoolean);
    for (MovementMethod localMovementMethod = ScrollingMovementMethod.getInstance(); ; localMovementMethod = null)
    {
      paramTextView.setMovementMethod(localMovementMethod);
      return;
    }
  }

  private boolean isFontSizeTooBig(TextLayoutInfo paramTextLayoutInfo)
  {
    return paramTextLayoutInfo.requiredHeight > this.mViewHeight;
  }

  private TextLayoutInfo pickFontInRect()
  {
    TextLayoutInfo localTextLayoutInfo = new TextLayoutInfo(60, 1, 60);
    TextPaint localTextPaint = createTestPaint(this.mTextView.getTypeface(), localTextLayoutInfo.fontSize);
    String str = this.mTextView.getText().toString();
    int i = localTextLayoutInfo.fontSize;
    int j = this.mViewWidth;
    while (i < j)
    {
      int k = i + (1 + (j - i)) / 2;
      calcTextLayoutInfo(localTextLayoutInfo, str, this.mViewWidth, localTextPaint, k);
      if (isFontSizeTooBig(localTextLayoutInfo))
        j = k - 1;
      else
        i = k;
    }
    localTextLayoutInfo.fontSize = i;
    return localTextLayoutInfo;
  }

  private void setupZoomButtons()
  {
    this.mZoomController = new ZoomButtonsController(this.mTextView);
    this.mZoomController.setZoomSpeed(100L);
    this.mZoomController.setAutoDismissed(true);
    this.mZoomController.setOnZoomListener(new ZoomButtonsController.OnZoomListener()
    {
      public void onVisibilityChanged(boolean paramAnonymousBoolean)
      {
        if (!paramAnonymousBoolean)
          SupersizeTextViewHelper.this.mTextView.setOnTouchListener(SupersizeTextViewHelper.this.mTriggerZoomButtonsListener);
      }

      public void onZoom(boolean paramAnonymousBoolean)
      {
        SupersizeTextViewHelper localSupersizeTextViewHelper = SupersizeTextViewHelper.this;
        if (paramAnonymousBoolean);
        for (float f = 1.1F; ; f = 0.9F)
        {
          localSupersizeTextViewHelper.zoomSupersizeText(f);
          SupersizeTextViewHelper.this.updateZoomButtonsEnabled();
          return;
        }
      }
    });
    this.mTextView.setOnTouchListener(this.mTriggerZoomButtonsListener);
  }

  private void setupZoomSupport()
  {
    if (Util.getSdkVersion() < 8)
    {
      setupZoomButtons();
      return;
    }
    final ScaleGestureDetector localScaleGestureDetector = new ScaleGestureDetector(this.mContext, new ScaleListener(null));
    this.mTextView.setOnTouchListener(new View.OnTouchListener()
    {
      public boolean onTouch(View paramAnonymousView, MotionEvent paramAnonymousMotionEvent)
      {
        SupersizeTextViewHelper.this.mTextView.onTouchEvent(paramAnonymousMotionEvent);
        localScaleGestureDetector.onTouchEvent(paramAnonymousMotionEvent);
        return true;
      }
    });
  }

  private void updateZoomButtonsEnabled()
  {
    boolean bool1 = true;
    Preconditions.checkNotNull(this.mZoomController);
    ZoomButtonsController localZoomButtonsController1 = this.mZoomController;
    boolean bool2;
    ZoomButtonsController localZoomButtonsController2;
    if (this.mScaleFactor < this.mZoomMaxScale)
    {
      bool2 = bool1;
      localZoomButtonsController1.setZoomInEnabled(bool2);
      localZoomButtonsController2 = this.mZoomController;
      if (this.mScaleFactor <= this.mZoomMinScale)
        break label67;
    }
    while (true)
    {
      localZoomButtonsController2.setZoomOutEnabled(bool1);
      return;
      bool2 = false;
      break;
      label67: bool1 = false;
    }
  }

  private void zoomSupersizeText(float paramFloat)
  {
    this.mScaleFactor = (paramFloat * this.mScaleFactor);
    this.mScaleFactor = Math.max(Math.min(this.mScaleFactor, this.mZoomMaxScale), this.mZoomMinScale);
    this.mTextView.setTextSize(0, this.mCalculatedFont * this.mScaleFactor);
    this.mTextView.setGravity(17);
  }

  public void onDestroy()
  {
    if (this.mZoomController != null)
      this.mZoomController.setVisible(false);
  }

  public void resizeText(String paramString, Language[] paramArrayOfLanguage)
  {
    Preconditions.checkNotNull(paramArrayOfLanguage);
    if (paramArrayOfLanguage.length > 0);
    for (boolean bool = true; ; bool = false)
    {
      Preconditions.checkNotNull(Boolean.valueOf(bool));
      Preconditions.checkNotNull(paramString);
      this.mTextView.setText(paramString);
      TextLayoutInfo localTextLayoutInfo = pickFontInRect();
      if (localTextLayoutInfo.requiredLines == 1)
        this.mTextView.setGravity(17);
      this.mCalculatedFont = localTextLayoutInfo.fontSize;
      this.mScaleFactor = 1.0F;
      this.mZoomMinScale = (15.0F / this.mCalculatedFont);
      this.mZoomMaxScale = (this.mMaxFontSizeInPixel / this.mCalculatedFont);
      this.mTextView.setTextSize(0, this.mCalculatedFont);
      Util.setTextAndFont(this.mTextView, paramString, paramArrayOfLanguage, Constants.AppearanceType.UNCHANGED, false);
      return;
    }
  }

  private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener
  {
    private ScaleListener()
    {
    }

    public boolean onScale(ScaleGestureDetector paramScaleGestureDetector)
    {
      SupersizeTextViewHelper.this.zoomSupersizeText(paramScaleGestureDetector.getScaleFactor());
      return true;
    }
  }

  private static class TextLayoutInfo
  {
    int fontSize;
    int requiredHeight;
    int requiredLines;

    public TextLayoutInfo(int paramInt1, int paramInt2, int paramInt3)
    {
      this.requiredHeight = paramInt1;
      this.requiredLines = paramInt2;
      this.fontSize = paramInt3;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.SupersizeTextViewHelper
 * JD-Core Version:    0.6.2
 */