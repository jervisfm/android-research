package com.google.android.apps.translate;

import android.text.TextUtils;
import android.text.TextUtils.SimpleStringSplitter;
import com.google.android.apps.translate.history.Entry;
import com.google.android.apps.translate.offline.TranslateOfflineUtil;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Translate
{
  public static final String ALPHA_LANG_PARAM = "al";
  private static final String ALPHA_VALUE = "1";
  private static final String CB_VALUE = "1";
  private static final String DICTIONARY_FIELD = "dict";
  private static final String DICTIONARY_SYNONYMS = "terms";
  private static final String DICTIONARY_TYPE = "pos";
  private static final String ENCODING = "UTF-8";
  public static final int ERROR_EMPTY = -1;
  public static final int ERROR_EMPTY_WITH_LANGID = -4;
  public static final int ERROR_INPUT_TEXT_UTF8_ENCODING = -101;
  public static final int ERROR_JSON_PARSING = -301;
  public static final String ERROR_MSG_PREFIX = " E";
  public static final int ERROR_NETWORK = -2;
  public static final int ERROR_NETWORK_HTTP = -201;
  public static final int ERROR_NONE = 0;
  private static final String FIND_LANG_VAR = "&hl=";
  public static final String INPUT_ENCODING_PARAM = "ie";
  private static final String INPUT_ENCODING_VAR = "&ie=";
  private static final String LANGID_FIELD = "ld_result";
  private static final char LANGID_SEPARATOR = ',';
  private static final String LANGID_SRCLANGS_SUB_FIELD = "srclangs";
  public static final String LOCALE_LANG_PARAM = "hl";
  private static final String LOCALE_LANG_VAR = "&hl=";
  public static final String NAME_SEPARATOR = ":";
  public static final String OTF_DEFAULT_PARAM = "&otf=1";
  public static final String OTF_INSTANT_PARAM = "&otf=2";
  public static final String OTF_NEW_INSTANT_PARAM = "&otf=3";
  public static final String OTF_PARAM = "otf";
  public static final String OUTPUT_ENCODING_PARAM = "oe";
  private static final String OUTPUT_ENCODING_VAR = "&oe=";
  private static final String SOURCE_LANGUAGE_FIELD = "src";
  public static final String SOURCE_LANG_PARAM = "sl";
  private static final String SOURCE_LANG_VAR = "&sl=";
  private static final String SPELLCORRECTION_FIELD = "spell";
  private static final String SPELLCORRECTION_SUB_FIELD = "spell_html_res";
  private static final String SRC_TRANSLITERATION_FIELD = "src_translit";
  private static final String TAG = "Translate";
  public static final String TARGET_LANG_PARAM = "tl";
  private static final String TARGET_LANG_VAR = "&tl=";
  private static final String TEXT_VAR = "&text=";
  public static final String TRANSLATE_HOST = "translate.google.com";
  public static final String TRANSLATE_PAGE_PARAM = "u";
  public static final String TRANSLATE_PAGE_PATH = "translate";
  public static final String TRANSLATE_QUERY_DELIM_PATTERN = "\\|";
  public static final String TRANSLATE_QUERY_PARAM = "q";
  public static final String TRANSLATE_SCHEME = "http";
  public static final String TRANSLATE_WEB_MIMETYPE = "text/html";
  private static final String TRANSLATION_DATA_FIELD = "sentences";
  private static final String TRANSLATION_FIELD = "trans";
  private static final String TRG_TRANSLITERATION_FIELD = "translit";
  private static final String URL_LANGUAGE = "http://translate.google.com/translate_a/l?client=at&cb=1&alpha=1";
  private static final String URL_TRANSLATE = "http://translate.google.com/translate_a/t?client=at&sc=1&v=2.0";
  private static String mAcceptLanguage = "en";
  private static String mUserAgent = null;

  private static List<String> extractLanguages(String paramString)
    throws Exception
  {
    LinkedList localLinkedList = new LinkedList();
    String str1 = paramString.trim();
    if ((!str1.startsWith("1(")) || (!str1.endsWith(")")));
    while (true)
    {
      return localLinkedList;
      String str2 = str1.substring(1 + "1".length(), -1 + str1.length());
      try
      {
        JSONArray localJSONArray1 = new JSONArray(str2.replace('{', '[').replace('}', ']').replace(':', ','));
        verifyJsonArrayText(localJSONArray1);
        parseJsonLanguageList("sl", getSubArrayBykeyName(localJSONArray1, "sl", false), localLinkedList);
        parseJsonLanguageList("tl", getSubArrayBykeyName(localJSONArray1, "tl", false), localLinkedList);
        JSONArray localJSONArray2 = getSubArrayBykeyName(localJSONArray1, "al", true);
        if (localJSONArray2 == null)
          continue;
        parseJsonLanguageList("al", localJSONArray2, localLinkedList);
        return localLinkedList;
      }
      catch (Exception localException)
      {
        Logger.e("TranslateService", "JSON error.", localException);
        throw localException;
      }
    }
  }

  private static List<String> extractSuggestions(String paramString)
    throws Exception
  {
    LinkedList localLinkedList = new LinkedList();
    try
    {
      JSONArray localJSONArray = new JSONArray(paramString).getJSONArray(1);
      int i = localJSONArray.length();
      for (int j = 0; j < i; j++)
        localLinkedList.add(localJSONArray.getString(j));
    }
    catch (JSONException localJSONException)
    {
      Logger.e("TranslateService", "JSON error.", localJSONException);
      throw localJSONException;
    }
    return localLinkedList;
  }

  private static String fetchUrl(String paramString)
    throws ClientProtocolException, IOException
  {
    if (mUserAgent == null)
      throw new RuntimeException("No user-agent");
    HttpResponse localHttpResponse = Util.newHttpClient(mUserAgent).execute(new HttpGet(paramString));
    String str = "";
    if (localHttpResponse != null)
      str = Util.readStringFromHttpResponse(localHttpResponse);
    return str;
  }

  private static String fetchUrl1(String paramString)
    throws Exception
  {
    InputStream localInputStream = null;
    try
    {
      HttpResponse localHttpResponse = new DefaultHttpClient().execute(new HttpGet(paramString.toString()));
      localInputStream = localHttpResponse.getEntity().getContent();
      StatusLine localStatusLine = localHttpResponse.getStatusLine();
      if (localStatusLine.getStatusCode() == 200)
      {
        String str = toString(localInputStream);
        return str;
      }
      throw new RuntimeException(localStatusLine.toString());
    }
    finally
    {
      if (localInputStream != null)
        localInputStream.close();
    }
  }

  private static String formatOfflineOutput(String paramString1, String paramString2, String paramString3)
  {
    try
    {
      JSONObject localJSONObject1 = new JSONObject();
      localJSONObject1.put("trans", paramString3);
      localJSONObject1.put("src_translit", "");
      localJSONObject1.put("translit", "");
      JSONArray localJSONArray = new JSONArray();
      localJSONArray.put(localJSONObject1);
      JSONObject localJSONObject2 = new JSONObject();
      localJSONObject2.put("sentences", localJSONArray);
      localJSONObject2.put("src", paramString2);
      String str = formatTranslation(paramString2, localJSONObject2.toString());
      return str;
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
      Logger.d("Exception in formatOfflineOutput.");
    }
    return "";
  }

  private static String formatTranslation(String paramString1, String paramString2)
    throws JSONException
  {
    String str1 = paramString2.trim();
    if ((str1 == null) || (str1.equals("")))
      return "";
    if ((str1.startsWith("\"")) && (str1.endsWith("\"")))
      return str1.substring(1, -1 + str1.length()).replace("\\n", "\n");
    Logger.d("Translate", "formatTranslation raw=" + str1);
    JSONObject localJSONObject1 = new JSONObject(str1);
    StringBuilder localStringBuilder1 = new StringBuilder();
    StringBuilder localStringBuilder2 = new StringBuilder();
    StringBuilder localStringBuilder3 = new StringBuilder();
    StringBuilder localStringBuilder4 = new StringBuilder();
    StringBuilder localStringBuilder5 = new StringBuilder();
    if (localJSONObject1.has("sentences"))
    {
      int n = localJSONObject1.getJSONArray("sentences").length();
      for (int i1 = 0; i1 < n; i1++)
      {
        JSONObject localJSONObject4 = localJSONObject1.getJSONArray("sentences").getJSONObject(i1);
        localStringBuilder1.append(localJSONObject4.getString("trans"));
        localStringBuilder2.append(localJSONObject4.getString("src_translit"));
        localStringBuilder3.append(localJSONObject4.getString("translit"));
      }
    }
    if (localJSONObject1.has("spell"))
    {
      JSONObject localJSONObject3 = localJSONObject1.getJSONObject("spell");
      if (localJSONObject3 != null)
        localStringBuilder4.append(localJSONObject3.getString("spell_html_res"));
    }
    if (localJSONObject1.has("ld_result"))
      TranslateResult.appendTranslateResultLangIdsFieldFromLandIdJson(localStringBuilder5, localJSONObject1.getJSONObject("ld_result"));
    if (Util.isAutoDetectLanguage(paramString1))
      paramString1 = localJSONObject1.getString("src");
    StringBuilder localStringBuilder6 = new StringBuilder(paramString1);
    localStringBuilder6.append("\t");
    localStringBuilder6.append(localStringBuilder1);
    localStringBuilder6.append("\t");
    localStringBuilder6.append(localStringBuilder3);
    localStringBuilder6.append("\t");
    try
    {
      if (localJSONObject1.has("dict"))
      {
        JSONArray localJSONArray1 = localJSONObject1.getJSONArray("dict");
        int i = localJSONArray1.length();
        if (i > 0)
        {
          StringBuilder localStringBuilder7 = new StringBuilder();
          for (int j = 0; j < i; j++)
          {
            JSONObject localJSONObject2 = localJSONArray1.getJSONObject(j);
            String str2 = localJSONObject2.getString("pos");
            if (!TextUtils.isEmpty(str2))
              localStringBuilder7.append(str2).append(":\n");
            JSONArray localJSONArray2 = localJSONObject2.getJSONArray("terms");
            int k = localJSONArray2.length();
            for (int m = 1; m <= k; m++)
              localStringBuilder7.append(" ").append(m).append(". ").append(localJSONArray2.getString(m - 1)).append("\n");
            localStringBuilder7.append("\n");
          }
          localStringBuilder6.append(localStringBuilder7);
        }
      }
      label534: localStringBuilder6.append("\t");
      localStringBuilder6.append(localStringBuilder2);
      localStringBuilder6.append("\t");
      localStringBuilder6.append(localStringBuilder4);
      localStringBuilder6.append("\t");
      localStringBuilder6.append(localStringBuilder5);
      return localStringBuilder6.toString();
    }
    catch (JSONException localJSONException)
    {
      break label534;
    }
  }

  private static String generateErrorResult(int paramInt)
  {
    return String.valueOf(paramInt);
  }

  public static String generateLanguageTuple(String paramString1, String paramString2, String paramString3)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(paramString1).append(":").append(paramString2).append(":").append(paramString3);
    return localStringBuilder.toString();
  }

  public static int getResultCode(String paramString)
  {
    int j;
    if (TextUtils.isEmpty(paramString))
      j = -1;
    String[] arrayOfString;
    int i;
    do
    {
      return j;
      arrayOfString = paramString.split("\t", 3);
      if ((arrayOfString.length > 1) && (TextUtils.isEmpty(arrayOfString[1])))
      {
        if (new TranslateResult(paramString, true).hasDetectedSrcShorLangName())
          return -4;
        return -1;
      }
      i = arrayOfString.length;
      j = 0;
    }
    while (i != 1);
    return Integer.parseInt(arrayOfString[0]);
  }

  private static JSONArray getSubArrayBykeyName(JSONArray paramJSONArray, String paramString, boolean paramBoolean)
    throws JSONException
  {
    int i = paramJSONArray.length();
    for (int j = 0; j < i; j += 2)
      if (paramString.equals(paramJSONArray.getString(j)))
      {
        if (j >= i - 1)
          throw new JSONException("SubArray for : \"" + paramString + "\" does not exist.");
        return paramJSONArray.getJSONArray(j + 1);
      }
    if (!paramBoolean)
    {
      StringBuilder localStringBuilder = new StringBuilder();
      for (int k = 0; k < i; k += 2)
      {
        localStringBuilder.append(paramJSONArray.getString(k));
        localStringBuilder.append(" ");
      }
      throw new JSONException("Cannot find \"" + paramString + "\". All the key list: " + localStringBuilder.toString());
    }
    return null;
  }

  public static List<String> languages(String paramString)
    throws Exception
  {
    return retrieveLanguages(paramString);
  }

  public static String offlineTranslate(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    return formatOfflineOutput(paramString1, paramString2, TranslateOfflineUtil.translate(paramString2, paramString3, paramString1));
  }

  private static void parseJsonLanguageList(String paramString, JSONArray paramJSONArray, List<String> paramList)
    throws JSONException
  {
    int i = paramJSONArray.length();
    for (int j = 0; j < i; j += 2)
      paramList.add(generateLanguageTuple(paramString, paramJSONArray.getString(j), paramJSONArray.getString(j + 1)));
  }

  private static String prepareTranslateUrl(String paramString1, String paramString2, String paramString3, String paramString4)
    throws UnsupportedEncodingException
  {
    StringBuilder localStringBuilder = new StringBuilder("http://translate.google.com/translate_a/t?client=at&sc=1&v=2.0");
    localStringBuilder.append("&sl=").append(paramString2);
    localStringBuilder.append("&tl=").append(paramString3);
    localStringBuilder.append("&hl=").append(mAcceptLanguage);
    localStringBuilder.append("&ie=").append("UTF-8");
    localStringBuilder.append("&oe=").append("UTF-8");
    localStringBuilder.append("&text=").append(URLEncoder.encode(paramString1, "UTF-8"));
    if ((paramString4 != null) && (!paramString4.equals("")))
      localStringBuilder.append(paramString4);
    return localStringBuilder.toString();
  }

  private static List<String> retrieveLanguages(String paramString)
    throws Exception
  {
    try
    {
      StringBuilder localStringBuilder = new StringBuilder("http://translate.google.com/translate_a/l?client=at&cb=1&alpha=1");
      localStringBuilder.append("&hl=").append(paramString);
      localStringBuilder.append("&ie=").append("UTF-8");
      localStringBuilder.append("&oe=").append("UTF-8");
      Logger.d("Translate", "url=" + localStringBuilder.toString());
      List localList = extractLanguages(fetchUrl(localStringBuilder.toString()));
      return localList;
    }
    catch (Exception localException)
    {
      Logger.e("TranslateService", "Error during languages retrieval.", localException);
      throw localException;
    }
  }

  // ERROR //
  private static String retrieveTranslation(String paramString1, String paramString2, String paramString3, String paramString4, boolean paramBoolean)
  {
    // Byte code:
    //   0: new 477	com/google/android/apps/translate/CsiTimer
    //   3: dup
    //   4: ldc_w 479
    //   7: invokespecial 480	com/google/android/apps/translate/CsiTimer:<init>	(Ljava/lang/String;)V
    //   10: astore 5
    //   12: aload 5
    //   14: iconst_1
    //   15: anewarray 173	java/lang/String
    //   18: dup
    //   19: iconst_0
    //   20: ldc_w 482
    //   23: aastore
    //   24: invokevirtual 486	com/google/android/apps/translate/CsiTimer:begin	([Ljava/lang/String;)V
    //   27: aload_0
    //   28: aload_1
    //   29: aload_2
    //   30: aload_3
    //   31: invokestatic 488	com/google/android/apps/translate/Translate:prepareTranslateUrl	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   34: astore 7
    //   36: invokestatic 491	com/google/android/apps/translate/Logger:isDebug	()Z
    //   39: ifeq +29 -> 68
    //   42: ldc 106
    //   44: new 354	java/lang/StringBuilder
    //   47: dup
    //   48: invokespecial 355	java/lang/StringBuilder:<init>	()V
    //   51: ldc_w 493
    //   54: invokevirtual 361	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   57: aload 7
    //   59: invokevirtual 361	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   62: invokevirtual 362	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   65: invokestatic 365	com/google/android/apps/translate/Logger:d	(Ljava/lang/String;Ljava/lang/String;)V
    //   68: aload 7
    //   70: invokestatic 469	com/google/android/apps/translate/Translate:fetchUrl	(Ljava/lang/String;)Ljava/lang/String;
    //   73: astore 10
    //   75: aload 10
    //   77: astore 11
    //   79: iload 4
    //   81: ifeq +98 -> 179
    //   84: new 318	org/json/JSONObject
    //   87: dup
    //   88: aload 11
    //   90: invokespecial 366	org/json/JSONObject:<init>	(Ljava/lang/String;)V
    //   93: astore 14
    //   95: new 318	org/json/JSONObject
    //   98: dup
    //   99: invokespecial 319	org/json/JSONObject:<init>	()V
    //   102: astore 15
    //   104: aload 15
    //   106: ldc 142
    //   108: new 354	java/lang/StringBuilder
    //   111: dup
    //   112: invokespecial 355	java/lang/StringBuilder:<init>	()V
    //   115: ldc_w 349
    //   118: invokevirtual 361	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   121: aload_1
    //   122: aload_2
    //   123: aload_0
    //   124: invokestatic 453	com/google/android/apps/translate/offline/TranslateOfflineUtil:translate	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   127: invokevirtual 361	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   130: invokevirtual 362	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   133: invokevirtual 323	org/json/JSONObject:put	(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    //   136: pop
    //   137: aload 15
    //   139: ldc 103
    //   141: ldc_w 273
    //   144: invokevirtual 323	org/json/JSONObject:put	(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    //   147: pop
    //   148: aload 15
    //   150: ldc 145
    //   152: ldc_w 273
    //   155: invokevirtual 323	org/json/JSONObject:put	(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    //   158: pop
    //   159: aload 14
    //   161: ldc 139
    //   163: invokevirtual 372	org/json/JSONObject:getJSONArray	(Ljava/lang/String;)Lorg/json/JSONArray;
    //   166: aload 15
    //   168: invokevirtual 327	org/json/JSONArray:put	(Ljava/lang/Object;)Lorg/json/JSONArray;
    //   171: pop
    //   172: aload 14
    //   174: invokevirtual 328	org/json/JSONObject:toString	()Ljava/lang/String;
    //   177: astore 11
    //   179: aload_1
    //   180: aload 11
    //   182: invokestatic 332	com/google/android/apps/translate/Translate:formatTranslation	(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   185: astore 13
    //   187: aload 5
    //   189: iconst_1
    //   190: anewarray 173	java/lang/String
    //   193: dup
    //   194: iconst_0
    //   195: ldc_w 482
    //   198: aastore
    //   199: invokevirtual 496	com/google/android/apps/translate/CsiTimer:end	([Ljava/lang/String;)V
    //   202: aload 5
    //   204: getstatic 157	com/google/android/apps/translate/Translate:mUserAgent	Ljava/lang/String;
    //   207: invokestatic 262	com/google/android/apps/translate/Util:newHttpClient	(Ljava/lang/String;)Lorg/apache/http/client/HttpClient;
    //   210: invokevirtual 500	com/google/android/apps/translate/CsiTimer:report	(Lorg/apache/http/client/HttpClient;)V
    //   213: aload 13
    //   215: areturn
    //   216: astore 6
    //   218: ldc 219
    //   220: ldc_w 502
    //   223: aload 6
    //   225: invokestatic 227	com/google/android/apps/translate/Logger:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    //   228: bipush 155
    //   230: invokestatic 504	com/google/android/apps/translate/Translate:generateErrorResult	(I)Ljava/lang/String;
    //   233: areturn
    //   234: astore 9
    //   236: ldc 219
    //   238: ldc_w 506
    //   241: aload 9
    //   243: invokestatic 227	com/google/android/apps/translate/Logger:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    //   246: sipush -201
    //   249: invokestatic 504	com/google/android/apps/translate/Translate:generateErrorResult	(I)Ljava/lang/String;
    //   252: areturn
    //   253: astore 8
    //   255: ldc 219
    //   257: ldc_w 508
    //   260: aload 8
    //   262: invokestatic 227	com/google/android/apps/translate/Logger:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    //   265: bipush 254
    //   267: invokestatic 504	com/google/android/apps/translate/Translate:generateErrorResult	(I)Ljava/lang/String;
    //   270: areturn
    //   271: astore 12
    //   273: ldc 219
    //   275: ldc_w 510
    //   278: aload 12
    //   280: invokestatic 227	com/google/android/apps/translate/Logger:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    //   283: sipush -301
    //   286: invokestatic 504	com/google/android/apps/translate/Translate:generateErrorResult	(I)Ljava/lang/String;
    //   289: areturn
    //
    // Exception table:
    //   from	to	target	type
    //   27	68	216	java/io/UnsupportedEncodingException
    //   68	75	234	org/apache/http/client/ClientProtocolException
    //   68	75	253	java/io/IOException
    //   84	179	271	org/json/JSONException
    //   179	213	271	org/json/JSONException
  }

  public static void setAcceptLanguage(String paramString)
  {
    mAcceptLanguage = paramString;
  }

  public static void setUserAgent(String paramString)
  {
    mUserAgent = paramString;
  }

  private static String toString(InputStream paramInputStream)
    throws Exception
  {
    StringBuilder localStringBuilder = new StringBuilder();
    if (paramInputStream != null)
      try
      {
        BufferedReader localBufferedReader = new BufferedReader(new InputStreamReader(paramInputStream, "UTF-8"));
        while (true)
        {
          String str = localBufferedReader.readLine();
          if (str == null)
            break;
          localStringBuilder.append(str).append('\n');
        }
      }
      catch (Exception localException)
      {
        Logger.e("TranslateService", "Error reading translation stream.", localException);
      }
    return localStringBuilder.toString();
  }

  public static String translate(String paramString1, String paramString2, String paramString3, String paramString4, boolean paramBoolean)
    throws Exception
  {
    return retrieveTranslation(paramString1, paramString2, paramString3, paramString4, paramBoolean);
  }

  private static void verifyJsonArrayText(JSONArray paramJSONArray)
    throws JSONException
  {
    if (getSubArrayBykeyName(paramJSONArray, "sl", false).length() % 2 != 0)
      throw new JSONException("Incorrect number of pairs of param: sl");
    if (getSubArrayBykeyName(paramJSONArray, "tl", false).length() % 2 != 0)
      throw new JSONException("Incorrect number of pairs of param: tl");
    JSONArray localJSONArray = getSubArrayBykeyName(paramJSONArray, "al", true);
    if ((localJSONArray != null) && (localJSONArray.length() % 2 != 0))
      throw new JSONException("Incorrect number of pairs of param: al");
  }

  public static class TranslateResult
  {
    private static final int DETECTED_SRCLANG = 5;
    private static final int DICTIONARY_RESULT = 2;
    private static final int RESULT_PART_NUM = 6;
    private static final int SPELL_CORRECTION = 4;
    private static final int SRC_TRANSLITERATION = 3;
    private static final int TRANSLATE_TEXT = 0;
    private static final int TRG_TRANSLITERATION = 1;
    private final String[] mParts;

    public TranslateResult(Entry paramEntry)
    {
      this(paramEntry.getOutputText());
    }

    public TranslateResult(String paramString)
    {
      this(paramString, false);
    }

    public TranslateResult(String paramString, boolean paramBoolean)
    {
      Logger.d("Translate", "resultText=" + paramString);
      String[] arrayOfString = Util.splitTranslateResult(paramString);
      int k;
      if ((paramBoolean) && (arrayOfString.length > i))
      {
        this.mParts = new String[-1 + arrayOfString.length];
        k = 0;
      }
      while (k < this.mParts.length)
      {
        this.mParts[k] = arrayOfString[(k + 1)];
        k++;
        continue;
        this.mParts = arrayOfString;
      }
      if (this.mParts.length <= 6);
      while (true)
      {
        Preconditions.checkArguments(i);
        return;
        int j = 0;
      }
    }

    // ERROR //
    public static void appendTranslateResultLangIdsFieldFromLandIdJson(StringBuilder paramStringBuilder, JSONObject paramJSONObject)
    {
      // Byte code:
      //   0: aload_1
      //   1: ifnonnull +4 -> 5
      //   4: return
      //   5: aload_1
      //   6: ldc 80
      //   8: invokevirtual 86	org/json/JSONObject:getJSONArray	(Ljava/lang/String;)Lorg/json/JSONArray;
      //   11: astore_3
      //   12: aload_3
      //   13: ifnull -9 -> 4
      //   16: aload_3
      //   17: invokevirtual 92	org/json/JSONArray:length	()I
      //   20: istore 4
      //   22: iconst_0
      //   23: istore 5
      //   25: iload 5
      //   27: iload 4
      //   29: if_icmpge -25 -> 4
      //   32: aload_0
      //   33: aload_3
      //   34: iload 5
      //   36: invokevirtual 96	org/json/JSONArray:getString	(I)Ljava/lang/String;
      //   39: invokevirtual 49	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   42: pop
      //   43: iload 5
      //   45: iload 4
      //   47: iconst_1
      //   48: isub
      //   49: if_icmpge +10 -> 59
      //   52: aload_0
      //   53: bipush 44
      //   55: invokevirtual 99	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
      //   58: pop
      //   59: iinc 5 1
      //   62: goto -37 -> 25
      //   65: astore_2
      //   66: ldc 101
      //   68: aload_2
      //   69: invokestatic 105	com/google/android/apps/translate/Logger:e	(Ljava/lang/String;Ljava/lang/Throwable;)V
      //   72: return
      //   73: astore 6
      //   75: new 42	java/lang/StringBuilder
      //   78: dup
      //   79: invokespecial 43	java/lang/StringBuilder:<init>	()V
      //   82: ldc 107
      //   84: invokevirtual 49	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   87: iload 5
      //   89: invokevirtual 110	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
      //   92: invokevirtual 52	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   95: aload 6
      //   97: invokestatic 105	com/google/android/apps/translate/Logger:e	(Ljava/lang/String;Ljava/lang/Throwable;)V
      //   100: return
      //
      // Exception table:
      //   from	to	target	type
      //   5	12	65	org/json/JSONException
      //   32	43	73	org/json/JSONException
    }

    private final String getPart(int paramInt)
    {
      if (paramInt < this.mParts.length)
        return this.mParts[paramInt];
      return "";
    }

    private final boolean hasPart(int paramInt)
    {
      return paramInt < this.mParts.length;
    }

    public final String getDetectedSrcShortLangName(String paramString)
    {
      if (!TextUtils.isEmpty(getPart(5)))
      {
        TextUtils.SimpleStringSplitter localSimpleStringSplitter = new TextUtils.SimpleStringSplitter(',');
        localSimpleStringSplitter.setString(getPart(5));
        Object localObject = null;
        Iterator localIterator = localSimpleStringSplitter.iterator();
        while (localIterator.hasNext())
        {
          str = (String)localIterator.next();
          if (localObject == null)
            localObject = str;
          if (TextUtils.equals(str, paramString))
            localObject = "";
        }
        while (localObject != null)
        {
          String str;
          return localObject;
        }
        return "";
      }
      return "";
    }

    public final String getDictionaryResult()
    {
      return getPart(2);
    }

    public final String getSpellCorrection()
    {
      return getPart(4);
    }

    public final String getSrcTransliteration()
    {
      return getPart(3);
    }

    public final String getTranslateText()
    {
      return getPart(0);
    }

    public final String getTrgTransliteration()
    {
      return getPart(1);
    }

    public final boolean hasDetectedSrcShorLangName()
    {
      return hasPart(5);
    }

    public final boolean hasDictionaryResult()
    {
      return hasPart(2);
    }

    public final boolean hasSrcTransliteration()
    {
      return hasPart(3);
    }

    public final boolean hasTranslateText()
    {
      return hasPart(0);
    }

    public final boolean hasTrgTransliteration()
    {
      return hasPart(1);
    }

    public boolean stripOnMemoryAttributes()
    {
      int i = this.mParts.length;
      boolean bool1 = false;
      if (i > 4)
      {
        boolean bool2 = TextUtils.isEmpty(this.mParts[4]);
        bool1 = false;
        if (!bool2)
        {
          this.mParts[4] = "";
          bool1 = true;
        }
      }
      if ((i > 5) && (!TextUtils.isEmpty(this.mParts[5])))
      {
        this.mParts[5] = "";
        bool1 = true;
      }
      return bool1;
    }

    public String toString()
    {
      return TextUtils.join("\t", this.mParts);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.Translate
 * JD-Core Version:    0.6.2
 */