package com.google.android.apps.translate;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import com.google.android.apps.translate.editor.InstantTranslateHandler;
import com.google.android.apps.translate.tts.DonutTtsCallback;
import com.google.android.apps.translate.tts.MyTts;
import java.util.ArrayList;
import java.util.Iterator;

public class TranslateApplication extends Application
  implements DonutTtsCallback
{
  private boolean mConvShowIntroMessage = true;
  private CsiTimer mCsiTimer;
  private InstantTranslateHandler mInstantTranslateHandler;
  private final BroadcastReceiver mOnTtsChanged = new BroadcastReceiver()
  {
    public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
    {
      TranslateApplication.this.mTts.init();
    }
  };
  private final BroadcastReceiver mOnVoiceSupportedLanguagesResult = new BroadcastReceiver()
  {
    public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
    {
      Bundle localBundle = getResultExtras(true);
      ArrayList localArrayList = localBundle.getStringArrayList("android.speech.extra.SUPPORTED_LANGUAGES");
      if ((Logger.isDebug()) && (localArrayList != null))
      {
        Iterator localIterator = localArrayList.iterator();
        while (localIterator.hasNext())
        {
          String str3 = (String)localIterator.next();
          Logger.d("voicelang=" + str3);
        }
      }
      String str1 = localBundle.getString("android.speech.extra.LANGUAGE_PREFERENCE");
      if (localArrayList != null)
        Profile.setSupportedVoiceLanguages(paramAnonymousContext, localArrayList);
      String str2;
      if (str1 != null)
      {
        str2 = VoiceInputHelper.getShortLanguageNameFromVoiceInputLanguage(str1);
        if ((str2 != null) && (Profile.getVoiceInputLanguage(paramAnonymousContext, str2).equals("")))
        {
          Profile.setVoiceInputLanguage(paramAnonymousContext, str2, str1);
          if ((!str2.equals("zh-TW")) || (!Profile.getVoiceInputLanguage(paramAnonymousContext, "zh-CN").equals("")))
            break label171;
          Profile.setVoiceInputLanguage(paramAnonymousContext, "zh-CN", str1);
        }
      }
      label171: 
      while ((!str2.equals("zh-CN")) || (!Profile.getVoiceInputLanguage(paramAnonymousContext, "zh-TW").equals("")))
        return;
      Profile.setVoiceInputLanguage(paramAnonymousContext, "zh-TW", str1);
    }
  };
  private String mPackageName;
  private SharedPreferences mPreferences;
  final SharedPreferences.OnSharedPreferenceChangeListener mSharedPreferenceChangeListener = new SharedPreferences.OnSharedPreferenceChangeListener()
  {
    public void onSharedPreferenceChanged(SharedPreferences paramAnonymousSharedPreferences, String paramAnonymousString)
    {
      if ("key_supported_voice_input_langs".equals(paramAnonymousString))
        TranslateApplication.this.reloadSupportedVoiceInputLanguages();
    }
  };
  private boolean mShowIntroMessage = true;
  TranslateManager mTranslateManager;
  private MyTts mTts;
  private VoiceInputHelper mVoiceInputHelper;

  private void initializeInstantTranslation()
  {
    this.mInstantTranslateHandler = new InstantTranslateHandler();
  }

  private void initializeTranslateManager()
  {
    this.mTranslateManager = new TranslateManagerImpl(this);
    this.mTranslateManager.initialize();
  }

  private void initializeTts()
  {
    Logger.d("initialize TTS");
    this.mTts = new MyTts(this);
    this.mTts.setCallback(this);
  }

  public static boolean isReleaseBuild(Context paramContext)
  {
    return isReleaseBuild(paramContext.getPackageName());
  }

  private static boolean isReleaseBuild(String paramString)
  {
    return (TextUtils.isEmpty(paramString)) || (paramString.equals("com.google.android.apps.translate"));
  }

  private void reloadSupportedVoiceInputLanguages()
  {
    this.mVoiceInputHelper.reloadVoiceInputLanguageMap();
  }

  public void endAndReportAppCsiTimer()
  {
    this.mCsiTimer.end(new String[] { "init" });
    this.mCsiTimer.report(this);
    this.mCsiTimer = new CsiTimer("app");
  }

  public boolean getConvShowIntroMessage()
  {
    boolean bool1 = this.mConvShowIntroMessage;
    boolean bool2 = false;
    if (bool1)
    {
      this.mConvShowIntroMessage = false;
      bool2 = true;
    }
    return bool2;
  }

  public InstantTranslateHandler getInstantTranslateHandler()
  {
    return this.mInstantTranslateHandler;
  }

  public MyTts getMyTts()
  {
    return this.mTts;
  }

  public boolean getShowIntroMessage()
  {
    boolean bool1 = this.mShowIntroMessage;
    boolean bool2 = false;
    if (bool1)
    {
      this.mShowIntroMessage = false;
      bool2 = true;
    }
    return bool2;
  }

  public TranslateManager getTranslateManager()
  {
    return this.mTranslateManager;
  }

  public VoiceInputHelper getVoiceInputHelper()
  {
    return this.mVoiceInputHelper;
  }

  public boolean isReleaseBuild()
  {
    return isReleaseBuild(this.mPackageName);
  }

  public void onConfigurationChanged(Configuration paramConfiguration)
  {
    super.onConfigurationChanged(paramConfiguration);
    if (TextUtils.isEmpty(Profile.getLanguageList(this)))
      Util.getLanguagesFromServerAsync(this);
  }

  public void onCreate()
  {
    this.mCsiTimer = new CsiTimer("app");
    this.mCsiTimer.begin(new String[] { "init" });
    super.onCreate();
    VoiceInputHelper.getSupportedLanguages(this, this.mOnVoiceSupportedLanguagesResult);
    this.mVoiceInputHelper = new VoiceInputHelper(this);
    this.mPackageName = getApplicationContext().getPackageName();
    if (!isReleaseBuild())
      Logger.setLogLevel(8);
    initializeTranslateManager();
    initializeTts();
    initializeInstantTranslation();
    this.mPreferences = PreferenceManager.getDefaultSharedPreferences(this);
    this.mPreferences.registerOnSharedPreferenceChangeListener(this.mSharedPreferenceChangeListener);
    ExternalFonts.initialize(getAssets(), getApplicationContext());
    Translate.setUserAgent(Util.generateUserAgentName(this));
    Util.getLanguagesFromServerAsync(this);
    IntentFilter localIntentFilter = new IntentFilter();
    localIntentFilter.addAction("android.intent.action.PACKAGE_ADDED");
    localIntentFilter.addAction("android.intent.action.PACKAGE_REMOVED");
    localIntentFilter.addDataScheme("package");
    registerReceiver(this.mOnTtsChanged, localIntentFilter);
  }

  public void onInit(boolean paramBoolean)
  {
  }

  public void onTerminate()
  {
    super.onTerminate();
    this.mPreferences.unregisterOnSharedPreferenceChangeListener(this.mSharedPreferenceChangeListener);
    unregisterReceiver(this.mOnTtsChanged);
    if (this.mTranslateManager != null)
    {
      this.mTranslateManager.deinitialize();
      this.mTranslateManager = null;
    }
    if (this.mTts != null)
    {
      this.mTts.shutdown();
      this.mTts = null;
    }
  }

  public void resetAppCsiTimer()
  {
    this.mCsiTimer = new CsiTimer("app");
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.TranslateApplication
 * JD-Core Version:    0.6.2
 */