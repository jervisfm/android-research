package com.google.android.apps.translate;

import android.app.Activity;
import android.app.Fragment;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.TextView;
import com.google.android.apps.translate.conversation.ConversationActivity;
import com.google.android.apps.translate.conversation.ConversationFragment;
import com.google.android.apps.translate.history.HistoryActivity;
import com.google.android.apps.translate.history.HistoryFragment;
import com.google.android.apps.translate.translation.TranslateActivity;
import com.google.android.apps.translate.translation.TranslateFragment;

public class TranslateBaseActivity extends Activity
{
  public static void afterSetContentView(Activity paramActivity)
  {
    if (!Util.isHoneycombCompatible())
    {
      paramActivity.getWindow().setFeatureInt(7, R.layout.translate_title);
      ((TextView)paramActivity.findViewById(R.id.translate_title)).setText(getTitle(paramActivity));
      return;
    }
    SdkVersionWrapper.getWrapper().setActionBarTitle(paramActivity, null);
  }

  public static void beforeSetContentView(Activity paramActivity)
  {
    if (!Util.isHoneycombCompatible())
      paramActivity.requestWindowFeature(7);
  }

  public static String getTitle(Activity paramActivity)
  {
    if ((paramActivity instanceof SettingsActivity))
      return paramActivity.getString(R.string.menu_settings);
    if ((paramActivity instanceof ConversationActivity))
      return paramActivity.getString(R.string.menu_conversation_mode);
    if ((paramActivity instanceof HistoryActivity))
    {
      if (((HistoryActivity)paramActivity).isHistoryMode())
        return paramActivity.getString(R.string.label_history);
      return paramActivity.getString(R.string.label_favorites);
    }
    if ((paramActivity instanceof SmsPickerActivity))
      return paramActivity.getString(R.string.menu_translate_sms);
    if (((paramActivity instanceof HomeActivity)) || ((paramActivity instanceof TranslateActivity)))
      return paramActivity.getString(R.string.btn_translate);
    return "";
  }

  public static String getTitle(Activity paramActivity, Fragment paramFragment)
  {
    if ((paramActivity != null) && (paramFragment != null))
    {
      if ((paramFragment instanceof ConversationFragment))
        return paramActivity.getString(R.string.menu_conversation_mode);
      if ((paramFragment instanceof HistoryFragment))
      {
        if (((HistoryFragment)paramFragment).isHistoryMode())
          return paramActivity.getString(R.string.label_history);
        return paramActivity.getString(R.string.label_favorites);
      }
      if ((paramFragment instanceof SmsPickerFragment))
        return paramActivity.getString(R.string.menu_translate_sms);
      if ((paramFragment instanceof TranslateFragment))
        return paramActivity.getString(R.string.btn_translate);
    }
    return getTitle(paramActivity);
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    switch (paramMenuItem.getItemId())
    {
    default:
      return super.onOptionsItemSelected(paramMenuItem);
    case 16908332:
    }
    Util.openHomeActivity(this);
    return true;
  }

  public void setContentView(int paramInt)
  {
    beforeSetContentView(this);
    super.setContentView(paramInt);
    afterSetContentView(this);
  }

  public void setContentView(View paramView)
  {
    beforeSetContentView(this);
    super.setContentView(paramView);
    afterSetContentView(this);
  }

  public void setContentView(View paramView, ViewGroup.LayoutParams paramLayoutParams)
  {
    beforeSetContentView(this);
    super.setContentView(paramView, paramLayoutParams);
    afterSetContentView(this);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.TranslateBaseActivity
 * JD-Core Version:    0.6.2
 */