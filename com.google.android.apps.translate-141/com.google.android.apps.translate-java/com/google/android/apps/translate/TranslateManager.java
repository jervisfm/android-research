package com.google.android.apps.translate;

public abstract interface TranslateManager
{
  public abstract void deinitialize();

  public abstract String doTranslate(String paramString);

  public abstract void initialize();

  public abstract void setInstantTransalte(boolean paramBoolean);

  public abstract void setLanguagePair(Language paramLanguage1, Language paramLanguage2);
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.TranslateManager
 * JD-Core Version:    0.6.2
 */