package com.google.android.apps.translate;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.ConditionVariable;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.text.TextUtils;
import com.google.android.apps.translate.history.BaseDb;
import com.google.android.apps.translate.history.CacheDb;
import com.google.android.apps.translate.history.Entry;
import com.google.android.apps.translate.translation.TranslateEntry;

public class TranslateManagerImpl
  implements TranslateManager
{
  private static final int SERVICE_CONNECTION_TIMEOUT_SEC = 5;
  private static final String TAG = "TranslateManagerImpl";
  private BaseDb mCacheDb;
  private final ConditionVariable mCondVar = new ConditionVariable();
  private final Context mContext;
  private Language mFromLanguage;
  private boolean mIsConnected = false;
  private boolean mIsInstantTranslation = false;
  private LoggingTranslationRequestHandler mLogRequestHandler;
  private Thread mLoggingRequestThread = new Thread()
  {
    public void run()
    {
      Looper.prepare();
      TranslateManagerImpl.access$002(TranslateManagerImpl.this, new TranslateManagerImpl.LoggingTranslationRequestHandler(TranslateManagerImpl.this, null));
      Looper.loop();
      Logger.d("TranslateManagerImpl", "BYE!");
    }
  };
  private String mPreviousInstantTransleQuery = "";
  private Language mToLanguage;
  private final ServiceConnection mTranslateConn = new ServiceConnection()
  {
    public void onServiceConnected(ComponentName paramAnonymousComponentName, IBinder paramAnonymousIBinder)
    {
      TranslateManagerImpl.access$302(TranslateManagerImpl.this, ITranslate.Stub.asInterface(paramAnonymousIBinder));
      TranslateManagerImpl.access$402(TranslateManagerImpl.this, true);
      TranslateManagerImpl.this.mCondVar.open();
    }

    public void onServiceDisconnected(ComponentName paramAnonymousComponentName)
    {
      TranslateManagerImpl.access$402(TranslateManagerImpl.this, false);
    }
  };
  private ITranslate mTranslateService;

  public TranslateManagerImpl(Context paramContext)
  {
    this.mContext = paramContext;
  }

  private boolean connectToTranslateService()
  {
    Intent localIntent = new Intent(this.mContext, TranslateService.class);
    localIntent.setAction("android.intent.action.VIEW");
    this.mContext.bindService(localIntent, this.mTranslateConn, 1);
    this.mCondVar.close();
    this.mCondVar.block(5000L);
    return this.mIsConnected;
  }

  private String doTranslateFromServiceSync(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    String str1;
    try
    {
      String str2 = this.mTranslateService.translate(paramString4, paramString2, paramString3, paramString1.toString());
      str1 = str2;
      int i = Translate.getResultCode(str1);
      switch (i)
      {
      default:
        Logger.d("TranslateManagerImpl", "Received error=" + i);
        return str1;
      case -4:
      case 0:
      }
    }
    catch (RemoteException localRemoteException)
    {
      while (true)
      {
        Logger.e("TranslateManagerImpl", "Translation error", localRemoteException);
        str1 = null;
      }
      Logger.d("TranslateManagerImpl", "Received result=" + str1);
      this.mCacheDb.add(new Entry(paramString2, paramString3, paramString4, str1));
    }
    return str1;
  }

  public void deinitialize()
  {
    this.mContext.unbindService(this.mTranslateConn);
  }

  public String doTranslate(String paramString)
  {
    this.mCondVar.block(5000L);
    if (this.mTranslateService == null)
    {
      initialize();
      return "";
    }
    String str1 = TranslateEntry.normalizeInputText(paramString);
    if (TextUtils.isEmpty(str1))
      return "";
    int i = 0;
    StringBuilder localStringBuilder = new StringBuilder();
    if (this.mIsInstantTranslation)
    {
      this.mIsInstantTranslation = false;
      if ((!TextUtils.isEmpty(this.mPreviousInstantTransleQuery)) && (str1.startsWith(this.mPreviousInstantTransleQuery)))
        localStringBuilder.append("&otf=2");
    }
    while (true)
    {
      this.mPreviousInstantTransleQuery = str1;
      Entry localEntry = CacheDb.getCachedEntry(this.mCacheDb, this.mFromLanguage, this.mToLanguage, str1);
      if (localEntry == null)
        break;
      String str2 = localEntry.getOutputText();
      if (TextUtils.isEmpty(str2))
        break;
      if ((i != 0) && (this.mLogRequestHandler != null))
      {
        Logger.d("TranslateManagerImpl", "Sending a logging translation request. inputText=" + str1);
        this.mLogRequestHandler.sendTranslationRequest(localStringBuilder.toString(), this.mFromLanguage.getShortName(), this.mToLanguage.getShortName(), str1);
      }
      Logger.d("TranslateManagerImpl", "Use cache entry. inputText=" + str1);
      return str2;
      localStringBuilder.append("&otf=3");
      i = 0;
      continue;
      localStringBuilder.append(UserActivityMgr.get().getCurrTranslationExtraParams());
      UserActivityMgr.get().prepareNewTranslation();
      localStringBuilder.append("&otf=1");
      i = 1;
    }
    Logger.d("TranslateManagerImpl", "Call translate service. inputText=" + str1);
    return doTranslateFromServiceSync(localStringBuilder.toString(), this.mFromLanguage.getShortName(), this.mToLanguage.getShortName(), str1);
  }

  public void initialize()
  {
    this.mIsInstantTranslation = false;
    this.mCacheDb = CacheDb.open(this.mContext);
    if (!this.mLoggingRequestThread.isAlive())
      this.mLoggingRequestThread.start();
    new Thread(new Runnable()
    {
      public void run()
      {
        TranslateManagerImpl.this.connectToTranslateService();
      }
    }).start();
  }

  public void setInstantTransalte(boolean paramBoolean)
  {
    this.mIsInstantTranslation = paramBoolean;
  }

  public void setLanguagePair(Language paramLanguage1, Language paramLanguage2)
  {
    this.mFromLanguage = paramLanguage1;
    this.mToLanguage = paramLanguage2;
  }

  private class LoggingTranslationRequestHandler extends Handler
  {
    private static final int MSG_SEND_TRANSLATE_REQUEST = 1;

    private LoggingTranslationRequestHandler()
    {
    }

    public void handleMessage(Message paramMessage)
    {
      switch (paramMessage.what)
      {
      default:
        if (Logger.isDebug())
          throw new IllegalArgumentException("Invalid message=" + paramMessage.what);
        break;
      case 1:
        TranslateManagerImpl.LoggingTranslationRequestHandlerObject localLoggingTranslationRequestHandlerObject = (TranslateManagerImpl.LoggingTranslationRequestHandlerObject)paramMessage.obj;
        Logger.d("TranslateManagerImpl", "Sending a translation request for logging. text=" + TranslateManagerImpl.LoggingTranslationRequestHandlerObject.access$600(localLoggingTranslationRequestHandlerObject));
        TranslateManagerImpl.this.doTranslateFromServiceSync(TranslateManagerImpl.LoggingTranslationRequestHandlerObject.access$700(localLoggingTranslationRequestHandlerObject), TranslateManagerImpl.LoggingTranslationRequestHandlerObject.access$800(localLoggingTranslationRequestHandlerObject), TranslateManagerImpl.LoggingTranslationRequestHandlerObject.access$900(localLoggingTranslationRequestHandlerObject), TranslateManagerImpl.LoggingTranslationRequestHandlerObject.access$600(localLoggingTranslationRequestHandlerObject));
      }
    }

    public void sendTranslationRequest(String paramString1, String paramString2, String paramString3, String paramString4)
    {
      TranslateManagerImpl.LoggingTranslationRequestHandlerObject localLoggingTranslationRequestHandlerObject = new TranslateManagerImpl.LoggingTranslationRequestHandlerObject(TranslateManagerImpl.this, null);
      TranslateManagerImpl.LoggingTranslationRequestHandlerObject.access$702(localLoggingTranslationRequestHandlerObject, paramString1);
      TranslateManagerImpl.LoggingTranslationRequestHandlerObject.access$802(localLoggingTranslationRequestHandlerObject, paramString2);
      TranslateManagerImpl.LoggingTranslationRequestHandlerObject.access$902(localLoggingTranslationRequestHandlerObject, paramString3);
      TranslateManagerImpl.LoggingTranslationRequestHandlerObject.access$602(localLoggingTranslationRequestHandlerObject, paramString4);
      sendMessage(obtainMessage(1, 0, 0, localLoggingTranslationRequestHandlerObject));
    }
  }

  private class LoggingTranslationRequestHandlerObject
  {
    private String mExtraParams;
    private String mFromLangShortName;
    private String mInputText;
    private String mToLangShortName;

    private LoggingTranslationRequestHandlerObject()
    {
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.TranslateManagerImpl
 * JD-Core Version:    0.6.2
 */