package com.google.android.apps.translate;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import com.google.android.apps.translate.offline.ModelManagementActivity;
import com.google.android.apps.translate.offline.ProfilesAdapter;

public class TranslateService extends Service
{
  public static final String TAG = "TranslateService";
  private static final String[] TRANSLATE_ACTIONS = { "android.intent.action.GET_CONTENT", "android.intent.action.PICK", "android.intent.action.VIEW" };
  private SharedPreferences localProfiles;
  private final ITranslate.Stub mBinder = new ITranslate.Stub()
  {
    public String translate(String paramAnonymousString1, String paramAnonymousString2, String paramAnonymousString3, String paramAnonymousString4)
    {
      try
      {
        Logger.d("from=" + paramAnonymousString2);
        Logger.d("to=" + paramAnonymousString3);
        Context localContext = TranslateService.this.getApplicationContext();
        if (Logger.isDebug())
        {
          Logger.d("TranslateService", "Profile.getOfflineTranslate=" + Profile.getOfflineTranslate(TranslateService.this.getApplicationContext()));
          Logger.d("TranslateService", "isNetworkAvailable=" + Util.isNetworkAvailable(localContext));
          Logger.d("TranslateService", "ProfilesAdapter.hasLocalModelFiles=" + ProfilesAdapter.hasLocalModelFiles(TranslateService.this.localProfiles, paramAnonymousString2, paramAnonymousString3));
        }
        if ((ModelManagementActivity.isOfflineTranslateSupported(localContext)) && ((Profile.getOfflineTranslate(localContext)) || (!Util.isNetworkAvailable(localContext))) && (ProfilesAdapter.hasLocalModelFiles(TranslateService.this.localProfiles, paramAnonymousString2, paramAnonymousString3)))
        {
          Logger.d("TranslateService", "Translate.offlineTranslate");
          return Translate.offlineTranslate(paramAnonymousString1, paramAnonymousString2, paramAnonymousString3, paramAnonymousString4);
        }
        if ((ModelManagementActivity.isOfflineTranslateSupported(localContext)) && (Profile.getDualMode(localContext)) && (ProfilesAdapter.hasLocalModelFiles(TranslateService.this.localProfiles, paramAnonymousString2, paramAnonymousString3)))
        {
          Logger.d("TranslateService", "Translate.translate DUAL MODE");
          return Translate.translate(paramAnonymousString1, paramAnonymousString2, paramAnonymousString3, paramAnonymousString4, true);
        }
        Logger.d("TranslateService", "Translate.translate ONLINE");
        String str = Translate.translate(paramAnonymousString1, paramAnonymousString2, paramAnonymousString3, paramAnonymousString4, false);
        return str;
      }
      catch (Exception localException)
      {
        Logger.e("TranslateService", "Failed to perform translation: " + localException.getMessage());
      }
      return null;
    }
  };

  public IBinder onBind(Intent paramIntent)
  {
    for (int i = 0; i < TRANSLATE_ACTIONS.length; i++)
      if (TRANSLATE_ACTIONS[i].equals(paramIntent.getAction()))
        return this.mBinder;
    return null;
  }

  public void onCreate()
  {
    super.onCreate();
    ((TranslateApplication)getApplication()).resetAppCsiTimer();
    this.localProfiles = getApplicationContext().getSharedPreferences("localModelProfiles", 0);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.TranslateService
 * JD-Core Version:    0.6.2
 */