package com.google.android.apps.translate;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class UserActivityMgr
{
  public static final boolean DEBUG = false;
  private static final String INPUT_METHOD_VAR = "&inputm=";
  private static final String INPUT_SOURCE_VAR = "&source=";
  private static final String INTERVAL_COUNT_SEPARATOR = ",";
  private static final String INTERVAL_COUNT_TAG_VALUE_SEPARATOR = ".";
  private static final String INTERVAL_COUNT_VAR = "&ic=";
  private static final String PREV_SOURCE_LANG_VAR = "&psl=";
  private static final String PREV_TARGET_LANG_VAR = "&ptl=";
  private static final String SWAP_VAR = "&swap=";
  private static final String TAG = "UserActivityMgr";
  private static final String VALUE_NOT_SET = null;
  private static Map<IntervalCountTag, Boolean> sIcTagToImpressionMap = Maps.newHashMap();
  private static UserActivityMgr sInstance = new UserActivityMgr();
  private TranslationActivity mTranslation = new TranslationActivity();

  public static UserActivityMgr get()
  {
    return sInstance;
  }

  public static void setLanguageChanges(Language paramLanguage1, Language paramLanguage2, Language paramLanguage3, Language paramLanguage4)
  {
    if (Logger.isDebug())
    {
      Preconditions.checkNotNull(paramLanguage1);
      Preconditions.checkNotNull(paramLanguage2);
      Preconditions.checkNotNull(paramLanguage3);
      Preconditions.checkNotNull(paramLanguage4);
    }
    if ((!paramLanguage3.equals(paramLanguage1)) || (!paramLanguage4.equals(paramLanguage2)));
    for (int i = 1; ; i = 0)
    {
      if (i != 0)
      {
        if (!paramLanguage1.equals(paramLanguage3))
          get().setTranslationSource(RequestSource.SL_CHANGE, paramLanguage1.getShortName());
        if (!paramLanguage2.equals(paramLanguage4))
          get().setTranslationSource(RequestSource.TL_CHANGE, paramLanguage2.getShortName());
        get().setTranslationInputMethod(InputMethod.UNKNOWN);
      }
      return;
    }
  }

  public static void setLanguageChangesForConversation(Language paramLanguage1, Language paramLanguage2, Language paramLanguage3, Language paramLanguage4)
  {
  }

  public String getCurrTranslationExtraParams()
  {
    StringBuilder localStringBuilder1;
    StringBuilder localStringBuilder2;
    while (true)
    {
      RequestSource localRequestSource;
      try
      {
        localStringBuilder1 = new StringBuilder();
        if (this.mTranslation.inputm != InputMethod.UNKNOWN)
          localStringBuilder1.append("&inputm=").append(this.mTranslation.inputm.ordinal());
        localRequestSource = this.mTranslation.source;
        if (localRequestSource != RequestSource.UNKNOWN)
        {
          if (localRequestSource.paramValue() != VALUE_NOT_SET)
            break label227;
          str2 = this.mTranslation.sourceCgiParamData;
          if (str2 != VALUE_NOT_SET)
            localStringBuilder1.append(localRequestSource.paramName()).append(str2);
        }
        localStringBuilder2 = new StringBuilder();
        Iterator localIterator = this.mTranslation.intervalCountMap.keySet().iterator();
        if (!localIterator.hasNext())
          break;
        IntervalCountTag localIntervalCountTag = (IntervalCountTag)localIterator.next();
        int i = ((Integer)this.mTranslation.intervalCountMap.get(localIntervalCountTag)).intValue();
        if (i == 0)
          continue;
        if (localStringBuilder2.length() > 0)
          localStringBuilder2.append(",");
        localStringBuilder2.append(localIntervalCountTag.tagName());
        localStringBuilder2.append(".");
        localStringBuilder2.append(i);
        continue;
      }
      finally
      {
      }
      label227: String str2 = localRequestSource.paramValue();
    }
    if (localStringBuilder2.length() > 0)
    {
      localStringBuilder1.append("&ic=").append(localStringBuilder2);
      this.mTranslation.intervalCountMap.clear();
    }
    Logger.d("UserActivityMgr", "getCurrTranslationExtraParams subUrl=" + localStringBuilder1.toString());
    String str1 = localStringBuilder1.toString();
    return str1;
  }

  public int getIntervalCount(IntervalCountTag paramIntervalCountTag)
  {
    try
    {
      if (this.mTranslation.intervalCountMap.containsKey(paramIntervalCountTag))
      {
        int j = ((Integer)this.mTranslation.intervalCountMap.get(paramIntervalCountTag)).intValue();
        i = j;
        return i;
      }
      int i = 0;
    }
    finally
    {
    }
  }

  public RequestSource getTranslationSource()
  {
    try
    {
      RequestSource localRequestSource = this.mTranslation.source;
      return localRequestSource;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public UserActivityMgr incrementIntervalCount(IntervalCountTag paramIntervalCountTag, int paramInt)
  {
    int i = paramInt;
    try
    {
      if (this.mTranslation.intervalCountMap.containsKey(paramIntervalCountTag))
        i += ((Integer)this.mTranslation.intervalCountMap.get(paramIntervalCountTag)).intValue();
      this.mTranslation.intervalCountMap.put(paramIntervalCountTag, Integer.valueOf(i));
      Logger.d("UserActivityMgr", "setTranslationInputMethod mTranslation.intervalCountMap['" + paramIntervalCountTag + "']=" + i);
      return this;
    }
    finally
    {
    }
  }

  public void prepareNewTranslation()
  {
    try
    {
      Logger.d("UserActivityMgr", "prepareNewTranslation");
      this.mTranslation = new TranslationActivity();
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public UserActivityMgr setImpression(IntervalCountTag paramIntervalCountTag, boolean paramBoolean)
  {
    try
    {
      boolean bool;
      UserActivityMgr localUserActivityMgr2;
      if (sIcTagToImpressionMap.containsKey(paramIntervalCountTag))
      {
        bool = ((Boolean)sIcTagToImpressionMap.get(paramIntervalCountTag)).booleanValue();
        sIcTagToImpressionMap.put(paramIntervalCountTag, Boolean.valueOf(paramBoolean));
        if ((bool) || (!paramBoolean))
          break label77;
        localUserActivityMgr2 = incrementIntervalCount(paramIntervalCountTag, 1);
      }
      label77: for (UserActivityMgr localUserActivityMgr1 = localUserActivityMgr2; ; localUserActivityMgr1 = this)
      {
        return localUserActivityMgr1;
        bool = false;
        break;
      }
    }
    finally
    {
    }
  }

  public UserActivityMgr setTranslationInputMethod(InputMethod paramInputMethod)
  {
    try
    {
      if (Logger.isDebug())
        Preconditions.checkNotNull(paramInputMethod);
      this.mTranslation.inputm = paramInputMethod;
      Logger.d("UserActivityMgr", "setTranslationInputMethod mTranslation.inputm=" + this.mTranslation.inputm);
      return this;
    }
    finally
    {
    }
  }

  public UserActivityMgr setTranslationSource(RequestSource paramRequestSource)
  {
    try
    {
      UserActivityMgr localUserActivityMgr = setTranslationSource(paramRequestSource, VALUE_NOT_SET);
      return localUserActivityMgr;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public UserActivityMgr setTranslationSource(RequestSource paramRequestSource, String paramString)
  {
    try
    {
      this.mTranslation.source = paramRequestSource;
      this.mTranslation.sourceCgiParamData = paramString;
      Logger.d("UserActivityMgr", "setTranslationInputMethod mTranslation.source=" + this.mTranslation.source);
      Logger.d("UserActivityMgr", "setTranslationInputMethod mTranslation.sourceCgiParamData=" + this.mTranslation.sourceCgiParamData);
      return this;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public static enum InputMethod
  {
    static
    {
      TRANSLITERATION = new InputMethod("TRANSLITERATION", 1);
      VIRTUAL_KEYBOARD = new InputMethod("VIRTUAL_KEYBOARD", 2);
      SPEECH = new InputMethod("SPEECH", 3);
      POSTEDIT = new InputMethod("POSTEDIT", 4);
      HANDWRITING = new InputMethod("HANDWRITING", 5);
      CAMERA = new InputMethod("CAMERA", 6);
      InputMethod[] arrayOfInputMethod = new InputMethod[7];
      arrayOfInputMethod[0] = UNKNOWN;
      arrayOfInputMethod[1] = TRANSLITERATION;
      arrayOfInputMethod[2] = VIRTUAL_KEYBOARD;
      arrayOfInputMethod[3] = SPEECH;
      arrayOfInputMethod[4] = POSTEDIT;
      arrayOfInputMethod[5] = HANDWRITING;
      arrayOfInputMethod[6] = CAMERA;
    }
  }

  public static enum IntervalCountTag
  {
    private final String mTagName;

    static
    {
      FAVORITES_VIEW_ITEM_EXPANSIONS = new IntervalCountTag("FAVORITES_VIEW_ITEM_EXPANSIONS", 2, "fvrie");
      HISTORY_VIEW_ITEM_EXPANSIONS = new IntervalCountTag("HISTORY_VIEW_ITEM_EXPANSIONS", 3, "hsrie");
      SPELL_CORRECTION_SHOWN_IN_EDIT_MODE = new IntervalCountTag("SPELL_CORRECTION_SHOWN_IN_EDIT_MODE", 4, "iescs");
      LANGID_SHOWN_IN_EDIT_MODE = new IntervalCountTag("LANGID_SHOWN_IN_EDIT_MODE", 5, "ielid");
      SPELL_CORRECTION_SHOWN_ON_CHIP_VIEW = new IntervalCountTag("SPELL_CORRECTION_SHOWN_ON_CHIP_VIEW", 6, "icscs");
      LANGID_SHOWN_ON_CHIP_VIEW = new IntervalCountTag("LANGID_SHOWN_ON_CHIP_VIEW", 7, "iclid");
      HISTORY_SHOWN_IN_EDIT_MODE = new IntervalCountTag("HISTORY_SHOWN_IN_EDIT_MODE", 8, "iehst");
      INSTANT_TRANSLATION_SHOWN = new IntervalCountTag("INSTANT_TRANSLATION_SHOWN", 9, "ipit");
      SPELL_CORRECTION_CLICKED_IN_EDIT_MODE = new IntervalCountTag("SPELL_CORRECTION_CLICKED_IN_EDIT_MODE", 10, "cescs");
      LANGID_CLICKED_IN_EDIT_MODE = new IntervalCountTag("LANGID_CLICKED_IN_EDIT_MODE", 11, "celid");
      SPELL_CORRECTION_CLICKED_ON_CHIP_VIEW = new IntervalCountTag("SPELL_CORRECTION_CLICKED_ON_CHIP_VIEW", 12, "ccscs");
      LANGID_CLICKED_ON_CHIP_VIEW = new IntervalCountTag("LANGID_CLICKED_ON_CHIP_VIEW", 13, "cclid");
      HISTORY_CLICKED_IN_EDIT_MODE = new IntervalCountTag("HISTORY_CLICKED_IN_EDIT_MODE", 14, "cehst");
      INSTANT_TRANSLATION_CLICKED = new IntervalCountTag("INSTANT_TRANSLATION_CLICKED", 15, "epit");
      WEB_TRANSLATION_REQUESTS = new IntervalCountTag("WEB_TRANSLATION_REQUESTS", 16, "wbtr");
      CHIPVIEW_SRC_COPY_CLICKS = new IntervalCountTag("CHIPVIEW_SRC_COPY_CLICKS", 17, "cvscp");
      CHIPVIEW_TRG_COPY_CLICKS = new IntervalCountTag("CHIPVIEW_TRG_COPY_CLICKS", 18, "cvtcp");
      CHIPVIEW_SRC_SHARE_CLICKS = new IntervalCountTag("CHIPVIEW_SRC_SHARE_CLICKS", 19, "cvsshr");
      CHIPVIEW_TRG_SHARE_CLICKS = new IntervalCountTag("CHIPVIEW_TRG_SHARE_CLICKS", 20, "cvtshr");
      CHIPVIEW_TRG_SUPERSIZE_CLICKS = new IntervalCountTag("CHIPVIEW_TRG_SUPERSIZE_CLICKS", 21, "cvtspsz");
      CHIPVIEW_SRC_TTS_CLICKS = new IntervalCountTag("CHIPVIEW_SRC_TTS_CLICKS", 22, "cvstts");
      CHIPVIEW_TRG_TTS_CLICKS = new IntervalCountTag("CHIPVIEW_TRG_TTS_CLICKS", 23, "cvttts");
      CHIPVIEW_SRC_SEARCH_CLICKS = new IntervalCountTag("CHIPVIEW_SRC_SEARCH_CLICKS", 24, "cvssrch");
      CHIPVIEW_TRG_SEARCH_CLICKS = new IntervalCountTag("CHIPVIEW_TRG_SEARCH_CLICKS", 25, "cvtsrch");
      CHIPVIEW_SRC_TRANSLATE_CLICKS = new IntervalCountTag("CHIPVIEW_SRC_TRANSLATE_CLICKS", 26, "cvstran");
      CHIPVIEW_TRG_TRANSLATE_CLICKS = new IntervalCountTag("CHIPVIEW_TRG_TRANSLATE_CLICKS", 27, "cvttran");
      STARS_MINUS_UNSTARS = new IntervalCountTag("STARS_MINUS_UNSTARS", 28, "smus");
      IntervalCountTag[] arrayOfIntervalCountTag = new IntervalCountTag[29];
      arrayOfIntervalCountTag[0] = FAVORITES_VIEWS;
      arrayOfIntervalCountTag[1] = HISTORY_VIEWS;
      arrayOfIntervalCountTag[2] = FAVORITES_VIEW_ITEM_EXPANSIONS;
      arrayOfIntervalCountTag[3] = HISTORY_VIEW_ITEM_EXPANSIONS;
      arrayOfIntervalCountTag[4] = SPELL_CORRECTION_SHOWN_IN_EDIT_MODE;
      arrayOfIntervalCountTag[5] = LANGID_SHOWN_IN_EDIT_MODE;
      arrayOfIntervalCountTag[6] = SPELL_CORRECTION_SHOWN_ON_CHIP_VIEW;
      arrayOfIntervalCountTag[7] = LANGID_SHOWN_ON_CHIP_VIEW;
      arrayOfIntervalCountTag[8] = HISTORY_SHOWN_IN_EDIT_MODE;
      arrayOfIntervalCountTag[9] = INSTANT_TRANSLATION_SHOWN;
      arrayOfIntervalCountTag[10] = SPELL_CORRECTION_CLICKED_IN_EDIT_MODE;
      arrayOfIntervalCountTag[11] = LANGID_CLICKED_IN_EDIT_MODE;
      arrayOfIntervalCountTag[12] = SPELL_CORRECTION_CLICKED_ON_CHIP_VIEW;
      arrayOfIntervalCountTag[13] = LANGID_CLICKED_ON_CHIP_VIEW;
      arrayOfIntervalCountTag[14] = HISTORY_CLICKED_IN_EDIT_MODE;
      arrayOfIntervalCountTag[15] = INSTANT_TRANSLATION_CLICKED;
      arrayOfIntervalCountTag[16] = WEB_TRANSLATION_REQUESTS;
      arrayOfIntervalCountTag[17] = CHIPVIEW_SRC_COPY_CLICKS;
      arrayOfIntervalCountTag[18] = CHIPVIEW_TRG_COPY_CLICKS;
      arrayOfIntervalCountTag[19] = CHIPVIEW_SRC_SHARE_CLICKS;
      arrayOfIntervalCountTag[20] = CHIPVIEW_TRG_SHARE_CLICKS;
      arrayOfIntervalCountTag[21] = CHIPVIEW_TRG_SUPERSIZE_CLICKS;
      arrayOfIntervalCountTag[22] = CHIPVIEW_SRC_TTS_CLICKS;
      arrayOfIntervalCountTag[23] = CHIPVIEW_TRG_TTS_CLICKS;
      arrayOfIntervalCountTag[24] = CHIPVIEW_SRC_SEARCH_CLICKS;
      arrayOfIntervalCountTag[25] = CHIPVIEW_TRG_SEARCH_CLICKS;
      arrayOfIntervalCountTag[26] = CHIPVIEW_SRC_TRANSLATE_CLICKS;
      arrayOfIntervalCountTag[27] = CHIPVIEW_TRG_TRANSLATE_CLICKS;
      arrayOfIntervalCountTag[28] = STARS_MINUS_UNSTARS;
    }

    private IntervalCountTag(String paramString)
    {
      this.mTagName = paramString;
    }

    public String tagName()
    {
      return this.mTagName;
    }
  }

  public static enum RequestSource
  {
    private final String mParamName;
    private final String mParamValue;

    static
    {
      SMS = new RequestSource("SMS", 1, "&source=", "sms");
      SWAP = new RequestSource("SWAP", 2, "&swap=", "1");
      SL_CHANGE = new RequestSource("SL_CHANGE", 3, "&psl=", UserActivityMgr.VALUE_NOT_SET);
      TL_CHANGE = new RequestSource("TL_CHANGE", 4, "&ptl=", UserActivityMgr.VALUE_NOT_SET);
      SUGGEST = new RequestSource("SUGGEST", 5, "&source=", "suggest");
      TWS_SPELL_CORRECTION_IN_EDIT_MODE = new RequestSource("TWS_SPELL_CORRECTION_IN_EDIT_MODE", 6, "&source=", "tescs");
      TWS_LANGID_IN_EDIT_MODE = new RequestSource("TWS_LANGID_IN_EDIT_MODE", 7, "&source=", "telid");
      TWS_SPELL_CORRECTION_ON_CHIP_VIEW = new RequestSource("TWS_SPELL_CORRECTION_ON_CHIP_VIEW", 8, "&source=", "tcscs");
      TWS_LANGID_ON_CHIP_VIEW = new RequestSource("TWS_LANGID_ON_CHIP_VIEW", 9, "&source=", "tclid");
      CHIPVIEW_SRCTEXT = new RequestSource("CHIPVIEW_SRCTEXT", 10, "&source=", "cvstx");
      CHIPVIEW_TRGTEXT = new RequestSource("CHIPVIEW_TRGTEXT", 11, "&source=", "cvttx");
      CONV = new RequestSource("CONV", 12, "&source=", "conv");
      CONV_EDIT = new RequestSource("CONV_EDIT", 13, "&source=", "conv-edit");
      HISTORY_VIEW = new RequestSource("HISTORY_VIEW", 14, "&source=", "hist");
      FAVORITES_VIEW = new RequestSource("FAVORITES_VIEW", 15, "&source=", "fvrt");
      ACTION_VIEW = new RequestSource("ACTION_VIEW", 16, "&source=", "actvw");
      ACTION_SEND = new RequestSource("ACTION_SEND", 17, "&source=", "actsnd");
      ACTION_SEARCH = new RequestSource("ACTION_SEARCH", 18, "&source=", "actsrch");
      EDIT = new RequestSource("EDIT", 19, "&source=", "edit");
      RequestSource[] arrayOfRequestSource = new RequestSource[20];
      arrayOfRequestSource[0] = UNKNOWN;
      arrayOfRequestSource[1] = SMS;
      arrayOfRequestSource[2] = SWAP;
      arrayOfRequestSource[3] = SL_CHANGE;
      arrayOfRequestSource[4] = TL_CHANGE;
      arrayOfRequestSource[5] = SUGGEST;
      arrayOfRequestSource[6] = TWS_SPELL_CORRECTION_IN_EDIT_MODE;
      arrayOfRequestSource[7] = TWS_LANGID_IN_EDIT_MODE;
      arrayOfRequestSource[8] = TWS_SPELL_CORRECTION_ON_CHIP_VIEW;
      arrayOfRequestSource[9] = TWS_LANGID_ON_CHIP_VIEW;
      arrayOfRequestSource[10] = CHIPVIEW_SRCTEXT;
      arrayOfRequestSource[11] = CHIPVIEW_TRGTEXT;
      arrayOfRequestSource[12] = CONV;
      arrayOfRequestSource[13] = CONV_EDIT;
      arrayOfRequestSource[14] = HISTORY_VIEW;
      arrayOfRequestSource[15] = FAVORITES_VIEW;
      arrayOfRequestSource[16] = ACTION_VIEW;
      arrayOfRequestSource[17] = ACTION_SEND;
      arrayOfRequestSource[18] = ACTION_SEARCH;
      arrayOfRequestSource[19] = EDIT;
    }

    private RequestSource(String paramString1, String paramString2)
    {
      this.mParamName = paramString1;
      this.mParamValue = paramString2;
    }

    public String paramName()
    {
      return this.mParamName;
    }

    public String paramValue()
    {
      return this.mParamValue;
    }
  }

  private static class TranslationActivity
  {
    public UserActivityMgr.InputMethod inputm = UserActivityMgr.InputMethod.UNKNOWN;
    public Map<UserActivityMgr.IntervalCountTag, Integer> intervalCountMap = Maps.newHashMap();
    public UserActivityMgr.RequestSource source = UserActivityMgr.RequestSource.UNKNOWN;
    public String sourceCgiParamData = UserActivityMgr.VALUE_NOT_SET;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.UserActivityMgr
 * JD-Core Version:    0.6.2
 */