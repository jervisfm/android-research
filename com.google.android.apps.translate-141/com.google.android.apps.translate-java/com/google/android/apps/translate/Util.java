package com.google.android.apps.translate;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.os.Build.VERSION;
import android.os.IBinder;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.text.format.Time;
import android.text.style.CharacterStyle;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.apps.translate.conversation.ConversationActivity;
import com.google.android.apps.translate.history.Entry;
import com.google.android.apps.translate.history.FavoriteDb;
import com.google.android.apps.translate.history.HistoryActivity;
import com.google.android.apps.translate.history.HistoryDb;
import com.google.android.apps.translate.translation.ChipView.ChipPart;
import com.google.android.apps.translate.translation.TranslateActivity;
import com.google.android.apps.translate.translation.TranslateEntry;
import java.io.IOException;
import java.io.InputStream;
import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.zip.GZIPInputStream;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.ClientConnectionManagerFactory;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;

public class Util
{
  public static final int HTTP_REQUEST_TIMEOUT_MILLIS = 20000;
  public static final String LANGUAGE_PAIR_TEXT_SEPARATOR = " » ";
  private static final int MIN_LANGUAGE_COUNT = 50;
  private static final int NETWORK_BUFFER_SIZE = 512;
  private static final String TAG = "Util";
  private static final String USER_AGENT = "AndroidTranslate";
  private static boolean mCameraLoggingConfirmed = false;
  private static HttpClient sHttpClient;
  private static final int sSdkVersion = Integer.parseInt(Build.VERSION.SDK);

  public static CharSequence applyStylesToTextRange(CharSequence paramCharSequence, String paramString, CharacterStyle[] paramArrayOfCharacterStyle1, CharacterStyle[] paramArrayOfCharacterStyle2, CharacterStyle[] paramArrayOfCharacterStyle3)
  {
    int i = paramString.length();
    String str = paramCharSequence.toString();
    int j = i + str.indexOf(paramString);
    int k = str.indexOf(paramString, j);
    Object localObject;
    int i1;
    int i2;
    if ((j >= 0) && (k >= 0))
    {
      localObject = new SpannableStringBuilder(paramCharSequence);
      int m = paramArrayOfCharacterStyle1.length;
      for (int n = 0; n < m; n++)
        ((SpannableStringBuilder)localObject).setSpan(paramArrayOfCharacterStyle1[n], j, k, 33);
      ((SpannableStringBuilder)localObject).delete(k, k + i);
      ((SpannableStringBuilder)localObject).delete(j - i, j);
      if ((paramArrayOfCharacterStyle2 != null) && (j - i > 0))
      {
        int i3 = paramArrayOfCharacterStyle2.length;
        for (int i4 = 0; i4 < i3; i4++)
          ((SpannableStringBuilder)localObject).setSpan(paramArrayOfCharacterStyle2[i4], 0, j - i, 33);
      }
      if ((paramArrayOfCharacterStyle3 != null) && (k + i < str.length()))
      {
        i1 = paramArrayOfCharacterStyle3.length;
        i2 = 0;
      }
    }
    else
    {
      while (i2 < i1)
      {
        ((SpannableStringBuilder)localObject).setSpan(paramArrayOfCharacterStyle3[i2], k - i, str.length() - i * 2, 33);
        i2++;
        continue;
        localObject = paramCharSequence;
      }
    }
    return localObject;
  }

  public static void copyToClipBoard(Activity paramActivity, String paramString, ChipView.ChipPart paramChipPart)
  {
    SdkVersionWrapper.getWrapper().copyToClipBoard(paramActivity, paramString);
    UserActivityMgr localUserActivityMgr = UserActivityMgr.get();
    if (paramChipPart == ChipView.ChipPart.INPUT_TEXT);
    for (UserActivityMgr.IntervalCountTag localIntervalCountTag = UserActivityMgr.IntervalCountTag.CHIPVIEW_SRC_COPY_CLICKS; ; localIntervalCountTag = UserActivityMgr.IntervalCountTag.CHIPVIEW_TRG_COPY_CLICKS)
    {
      localUserActivityMgr.incrementIntervalCount(localIntervalCountTag, 1);
      return;
    }
  }

  private static void detectAndSetFonts(Spannable paramSpannable, Language[] paramArrayOfLanguage, Constants.AppearanceType paramAppearanceType)
  {
    LinkedList localLinkedList = null;
    if (paramArrayOfLanguage != null)
    {
      int k = paramArrayOfLanguage.length;
      localLinkedList = null;
      if (k > 0)
      {
        localLinkedList = new LinkedList();
        int m = paramArrayOfLanguage.length;
        for (int n = 0; n < m; n++)
        {
          Language localLanguage = paramArrayOfLanguage[n];
          if ((ExternalFonts.isUsingExternalFont(localLanguage)) && (!localLinkedList.contains(localLanguage)))
            localLinkedList.add(localLanguage);
        }
      }
    }
    ExtTypefaceSpan[] arrayOfExtTypefaceSpan;
    int i;
    int j;
    if ((localLinkedList == null) || (localLinkedList.isEmpty()))
    {
      arrayOfExtTypefaceSpan = (ExtTypefaceSpan[])paramSpannable.getSpans(0, paramSpannable.length(), ExtTypefaceSpan.class);
      if (arrayOfExtTypefaceSpan != null)
      {
        i = arrayOfExtTypefaceSpan.length;
        j = 0;
      }
    }
    else
    {
      while (j < i)
      {
        paramSpannable.removeSpan(arrayOfExtTypefaceSpan[j]);
        j++;
        continue;
        LanguageDetector.LanguageSpanApplier local3 = new LanguageDetector.LanguageSpanApplier()
        {
          public void applyLanguageSpan(Spannable paramAnonymousSpannable, int paramAnonymousInt1, int paramAnonymousInt2, Language paramAnonymousLanguage)
          {
            ExternalFonts localExternalFonts = ExternalFonts.getFontByShortName(paramAnonymousLanguage.getShortName());
            ExtTypefaceSpan[] arrayOfExtTypefaceSpan = (ExtTypefaceSpan[])paramAnonymousSpannable.getSpans(paramAnonymousInt1, paramAnonymousInt2, ExtTypefaceSpan.class);
            ExtTypefaceSpan localExtTypefaceSpan;
            if ((arrayOfExtTypefaceSpan == null) || (arrayOfExtTypefaceSpan.length == 0))
              localExtTypefaceSpan = new ExtTypefaceSpan(localExternalFonts.getTypeface(), localExternalFonts.getTextSize(this.val$type));
            while (true)
            {
              paramAnonymousSpannable.setSpan(localExtTypefaceSpan, paramAnonymousInt1, paramAnonymousInt2, 33);
              return;
              localExtTypefaceSpan = arrayOfExtTypefaceSpan[0];
              localExtTypefaceSpan.set(localExternalFonts.getTypeface(), localExternalFonts.getTextSize(this.val$type));
            }
          }
        };
        Iterator localIterator = localLinkedList.iterator();
        while (localIterator.hasNext())
          local3.applyLanguageSpans(paramSpannable, (Language)localIterator.next());
      }
    }
  }

  public static void detectAndSetFonts(TextView paramTextView, Language[] paramArrayOfLanguage, Constants.AppearanceType paramAppearanceType)
  {
    try
    {
      Spannable localSpannable = (Spannable)paramTextView.getText();
      detectAndSetFonts(localSpannable, paramArrayOfLanguage, paramAppearanceType);
      return;
    }
    catch (ClassCastException localClassCastException)
    {
    }
  }

  public static List<String> determineSupportedLanguages(String paramString)
  {
    try
    {
      List localList = Translate.languages(paramString);
      return localList;
    }
    catch (Exception localException)
    {
      Logger.d("Util", "Could not initialize Languages: " + localException.getMessage());
    }
    return null;
  }

  public static boolean filterMatches(String paramString1, String paramString2)
  {
    int i = paramString2.toLowerCase().indexOf(paramString1.toLowerCase());
    switch (i)
    {
    default:
      BreakIterator localBreakIterator = BreakIterator.getWordInstance();
      localBreakIterator.setText(paramString2);
      return localBreakIterator.isBoundary(i);
    case -1:
      return false;
    case 0:
    }
    return true;
  }

  public static int findLanguagePosition(List<Language> paramList, Language paramLanguage)
  {
    int i = paramList.size();
    for (int j = 0; j < i; j++)
      if (((Language)paramList.get(j)).equals(paramLanguage))
        return j;
    return -1;
  }

  public static String formatTimeStampString(Context paramContext, long paramLong)
  {
    return formatTimeStampString(paramContext, paramLong, false);
  }

  public static String formatTimeStampString(Context paramContext, long paramLong, boolean paramBoolean)
  {
    Time localTime1 = new Time();
    localTime1.set(paramLong);
    Time localTime2 = new Time();
    localTime2.setToNow();
    int i;
    if (localTime1.year != localTime2.year)
      i = 0x80B00 | 0x14;
    while (true)
    {
      if (paramBoolean)
        i |= 17;
      return DateUtils.formatDateTime(paramContext, paramLong, i);
      if (localTime1.yearDay != localTime2.yearDay)
        i = 0x80B00 | 0x10;
      else
        i = 0x80B00 | 0x1;
    }
  }

  public static List<Language> generateAlphaLanguagesFromList(List<String> paramList)
  {
    List localList = generateLanguagesFromList(paramList, "al");
    Logger.d("Util", localList.size() + " alpha-languages supported: " + localList.toString());
    return localList;
  }

  public static List<Language> generateFromLanguagesFromList(List<String> paramList)
  {
    List localList = generateLanguagesFromList(paramList, "sl");
    Logger.d("Util", localList.size() + " from-languages supported: " + localList.toString());
    return localList;
  }

  public static String generateLanguagePairText(String paramString1, String paramString2)
  {
    return paramString1 + " » " + paramString2;
  }

  private static List<Language> generateLanguagesFromList(List<String> paramList, String paramString)
  {
    LinkedList localLinkedList = Lists.newLinkedList();
    Iterator localIterator = paramList.iterator();
    while (localIterator.hasNext())
    {
      String[] arrayOfString = ((String)localIterator.next()).split(":");
      if ((arrayOfString.length == 3) && (paramString.equals(arrayOfString[0])))
        localLinkedList.add(new Language(arrayOfString[1], arrayOfString[2]));
    }
    return localLinkedList;
  }

  public static String generateLongPairText(Language paramLanguage1, Language paramLanguage2)
  {
    return generateLanguagePairText(paramLanguage1.getLongName(), paramLanguage2.getLongName());
  }

  public static List<Language> generateToLanguagesFromList(List<String> paramList)
  {
    List localList = generateLanguagesFromList(paramList, "tl");
    Logger.d("Util", localList.size() + " to-languages supported: " + localList.toString());
    return localList;
  }

  public static String generateUserAgentName(Context paramContext)
  {
    return "AndroidTranslate/" + getVersionName(paramContext) + " " + Build.VERSION.RELEASE;
  }

  public static HttpResponse getHttp(String paramString)
    throws ClientProtocolException, IOException
  {
    return getHttpClient().execute(new HttpGet(paramString));
  }

  public static HttpClient getHttpClient()
  {
    try
    {
      if (sHttpClient == null)
        sHttpClient = newHttpClient(null);
      HttpClient localHttpClient = sHttpClient;
      return localHttpClient;
    }
    finally
    {
    }
  }

  public static List<Entry> getInputSuggestions(Context paramContext, String paramString1, String paramString2, String paramString3)
  {
    List localList1 = FavoriteDb.getAllByATime(paramContext, 20, paramString3);
    List localList2 = HistoryDb.getAllByATime(paramContext, 20, paramString3);
    int i;
    HashSet localHashSet;
    ArrayList localArrayList;
    int k;
    label70: List localList3;
    if (!isAutoDetectLanguage(paramString1))
    {
      i = 1;
      localHashSet = new HashSet();
      localArrayList = new ArrayList();
      List[] arrayOfList = { localList1, localList2 };
      int j = arrayOfList.length;
      k = 0;
      if (k >= j)
        break label197;
      localList3 = arrayOfList[k];
      if ((localList3 != null) && (!localList3.isEmpty()))
        break label111;
    }
    while (true)
    {
      k++;
      break label70;
      i = 0;
      break;
      label111: Iterator localIterator = localList3.iterator();
      while (localIterator.hasNext())
      {
        Entry localEntry = (Entry)localIterator.next();
        String str = localEntry.getInputText();
        if ((!localHashSet.contains(str)) && ((i == 0) || (localEntry.getFromLanguageShortName().equals(paramString1))))
        {
          localHashSet.add(str);
          localArrayList.add(localEntry);
        }
      }
    }
    label197: if (localArrayList.size() <= 20)
      return localArrayList;
    return localArrayList.subList(0, 20);
  }

  public static Languages getLanguageListFromProfile(Context paramContext)
  {
    String str = Profile.getLanguageList(paramContext);
    if (!TextUtils.isEmpty(str))
      return new Languages(str);
    return new Languages(Languages.getDefaultFromLanguages(), Languages.getDefaultToLanguages());
  }

  public static String getLanguageShortNameByLocale()
  {
    return getLanguageShortNameByLocale(Locale.getDefault());
  }

  public static String getLanguageShortNameByLocale(Locale paramLocale)
  {
    Logger.d("locale: " + paramLocale.toString());
    if (Locale.TAIWAN.equals(paramLocale))
    {
      Logger.d("locale is TAIWAN");
      return "zh-TW";
    }
    if (Locale.CHINA.equals(paramLocale))
    {
      Logger.d("locale is CHINA");
      return "zh-CN";
    }
    return paramLocale.getLanguage();
  }

  public static boolean getLanguagesFromServer(Context paramContext)
  {
    return getLanguagesFromServer(paramContext, Locale.getDefault());
  }

  public static boolean getLanguagesFromServer(Context paramContext, Locale paramLocale)
  {
    List localList = determineSupportedLanguages(getLanguageShortNameByLocale(paramLocale));
    if (localList != null)
    {
      Languages localLanguages = new Languages(generateFromLanguagesFromList(localList), generateToLanguagesFromList(localList));
      if (isNewLanguageListQualified(localLanguages))
      {
        Languages.setAlphaLanguages(localList);
        String str = localLanguages.dumpLanguages();
        if (!str.equals(Profile.getLanguageList(paramContext, paramLocale)))
        {
          Profile.setLanguageList(paramContext, str, paramLocale);
          return true;
        }
      }
    }
    return false;
  }

  public static void getLanguagesFromServerAsync(Context paramContext)
  {
    new Thread()
    {
      public void run()
      {
        if (Util.getLanguagesFromServer(this.val$context))
        {
          LanguagesFactory.get().refreshLanguagesFromProfile(this.val$context);
          this.val$context.sendBroadcast(new Intent("com.google.android.apps.translate.broadcast.LANGUAGES_CHANGED"));
        }
      }
    }
    .start();
  }

  public static int getLocalizedStringId(Context paramContext, int paramInt, Language paramLanguage)
  {
    Resources localResources = paramContext.getResources();
    String str1 = localResources.getResourceName(paramInt) + '_';
    String str2 = paramLanguage.getShortName().replace('-', '_').toLowerCase();
    int i = localResources.getIdentifier(str1 + str2, null, null);
    if ((i == 0) && (str2.length() > 3))
      i = localResources.getIdentifier(str1 + str2.substring(0, 2), null, null);
    if (i == 0)
      return paramInt;
    return i;
  }

  public static InputStream getResponseInputStream(HttpResponse paramHttpResponse)
    throws IOException
  {
    HttpEntity localHttpEntity = paramHttpResponse.getEntity();
    if (localHttpEntity != null)
    {
      Object localObject = localHttpEntity.getContent();
      Header localHeader = localHttpEntity.getContentEncoding();
      if ((localHeader != null) && ("gzip".equals(localHeader.getValue())))
        localObject = new GZIPInputStream((InputStream)localObject);
      return localObject;
    }
    return null;
  }

  public static int getSdkVersion()
  {
    return sSdkVersion;
  }

  public static int getVersionCode(Context paramContext)
  {
    try
    {
      int i = paramContext.getPackageManager().getPackageInfo(paramContext.getPackageName(), 0).versionCode;
      return i;
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
      Logger.d("Util", "Package not found: " + paramContext.getPackageName());
    }
    return 0;
  }

  public static String getVersionName(Context paramContext)
  {
    try
    {
      String str = paramContext.getPackageManager().getPackageInfo(paramContext.getPackageName(), 0).versionName;
      return str;
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
      if (Logger.isDebug())
        Logger.d("Util", "Package not found: " + paramContext.getPackageName());
    }
    return "0.0.0";
  }

  public static boolean hasSmsSupport(Context paramContext)
  {
    return SdkVersionWrapper.getWrapper().hasFeatureTelephony(paramContext);
  }

  public static boolean hasSpeechRecognizer(Context paramContext)
  {
    boolean bool1 = paramContext.getPackageManager().queryIntentActivities(new Intent("android.speech.action.RECOGNIZE_SPEECH"), 0).isEmpty();
    boolean bool2 = false;
    if (!bool1)
      bool2 = true;
    return bool2;
  }

  public static void hideSoftKeyboard(Activity paramActivity, IBinder paramIBinder)
  {
    Logger.d("Util", "hideSoftInputFromWindow");
    ((InputMethodManager)paramActivity.getSystemService("input_method")).hideSoftInputFromWindow(paramIBinder, 0);
  }

  public static boolean isAutoDetectLanguage(Language paramLanguage)
  {
    if (paramLanguage == null)
      return false;
    return isAutoDetectLanguage(paramLanguage.getShortName());
  }

  public static boolean isAutoDetectLanguage(String paramString)
  {
    return "auto".equals(paramString);
  }

  public static boolean isFloatingActivity(Activity paramActivity)
  {
    if (paramActivity == null);
    Intent localIntent;
    do
    {
      return false;
      localIntent = paramActivity.getIntent();
    }
    while ((localIntent == null) || (localIntent.getAction() == null) || (localIntent.getAction().equals("android.intent.action.MAIN")));
    return true;
  }

  public static boolean isHoneycombCompatible()
  {
    return getSdkVersion() >= 11;
  }

  public static boolean isNetworkAvailable(Context paramContext)
  {
    return ((ConnectivityManager)paramContext.getSystemService("connectivity")).getActiveNetworkInfo() != null;
  }

  private static boolean isNewLanguageListQualified(Languages paramLanguages)
  {
    return (paramLanguages.getSupportedFromLanguages().size() > 50) && (paramLanguages.getSupportedToLanguages().size() > 50);
  }

  public static boolean isSelectLanguage(String paramString)
  {
    return "select".equals(paramString);
  }

  public static boolean isStarredTranslation(Activity paramActivity, Entry paramEntry)
  {
    return FavoriteDb.contains(paramActivity, paramEntry);
  }

  public static Locale languageShortNameToLocale(String paramString)
  {
    int i = paramString.indexOf('-');
    if (i == -1)
      return new Locale(paramString);
    return new Locale(paramString.substring(0, i), paramString.substring(i + 1));
  }

  public static HttpClient newHttpClient(String paramString)
  {
    DefaultHttpClient localDefaultHttpClient = new DefaultHttpClient();
    HttpParams localHttpParams = localDefaultHttpClient.getParams();
    HttpConnectionParams.setSoTimeout(localHttpParams, 20000);
    localHttpParams.setParameter("http.connection-manager.factory-object", new ClientConnectionManagerFactory()
    {
      public ClientConnectionManager newInstance(HttpParams paramAnonymousHttpParams, SchemeRegistry paramAnonymousSchemeRegistry)
      {
        return new ThreadSafeClientConnManager(paramAnonymousHttpParams, paramAnonymousSchemeRegistry);
      }
    });
    ArrayList localArrayList = new ArrayList();
    localArrayList.add(new BasicHeader("Accept-Charset", "UTF-8"));
    localArrayList.add(new BasicHeader("Accept-Encoding", "gzip"));
    localHttpParams.setParameter("http.default-headers", localArrayList);
    setUserAgentToHttpClient(localDefaultHttpClient, paramString);
    return localDefaultHttpClient;
  }

  public static void openConversationActivity(Activity paramActivity, TranslateEntry paramTranslateEntry)
  {
    Intent localIntent = new Intent(paramActivity, ConversationActivity.class);
    if (paramTranslateEntry.hasInputText())
      localIntent.putExtra("key_current_translation", paramTranslateEntry.inputText);
    if (paramTranslateEntry.fromLanguage != null)
      localIntent.putExtra("key_language_from", paramTranslateEntry.fromLanguage);
    if (paramTranslateEntry.toLanguage != null)
      localIntent.putExtra("key_language_to", paramTranslateEntry.toLanguage);
    paramActivity.startActivity(localIntent);
  }

  public static void openFavoriteActivity(Activity paramActivity)
  {
    Intent localIntent = new Intent(paramActivity, HistoryActivity.class);
    localIntent.putExtra("history", false);
    localIntent.putExtra("flush_on_pause", true);
    paramActivity.startActivity(localIntent);
  }

  public static void openHistoryActivity(Activity paramActivity)
  {
    Intent localIntent = new Intent(paramActivity, HistoryActivity.class);
    localIntent.putExtra("history", true);
    localIntent.putExtra("flush_on_pause", true);
    paramActivity.startActivity(localIntent);
  }

  public static void openHomeActivity(Activity paramActivity)
  {
    Intent localIntent = new Intent(paramActivity, HomeActivity.class);
    localIntent.setFlags(67108864);
    paramActivity.startActivity(localIntent);
  }

  public static void openSmsTranslationActivity(Activity paramActivity)
  {
    paramActivity.startActivityForResult(new Intent(paramActivity, SmsPickerActivity.class), 170);
  }

  public static void openSupersizeTextActivity(Activity paramActivity, TranslateEntry paramTranslateEntry, boolean paramBoolean)
  {
    if (paramTranslateEntry == null)
      return;
    Intent localIntent = new Intent(paramActivity, SupersizeTextActivity.class);
    localIntent.putExtra("key_translate_text", new Translate.TranslateResult(paramTranslateEntry.outputText).getTranslateText());
    localIntent.putExtra("key_text_input", paramTranslateEntry.inputText);
    localIntent.putExtra("key_language_to", paramTranslateEntry.toLanguage);
    localIntent.putExtra("key_language_from", paramTranslateEntry.fromLanguage);
    localIntent.putExtra("key_supersize_by_gesture", paramBoolean);
    paramActivity.startActivity(localIntent);
    UserActivityMgr.get().incrementIntervalCount(UserActivityMgr.IntervalCountTag.CHIPVIEW_TRG_SUPERSIZE_CLICKS, 1);
  }

  public static void openTranslateActivity(Activity paramActivity, Intent paramIntent)
  {
    paramIntent.setClass(paramActivity, TranslateActivity.class);
    if (paramActivity.getClass() == HomeActivity.class)
      setNoTransitionAnimation(paramIntent);
    paramActivity.startActivity(paramIntent);
  }

  public static void openTranslateActivity(Activity paramActivity, String paramString1, String paramString2, String paramString3, String paramString4)
  {
    Intent localIntent = new Intent(paramActivity, TranslateActivity.class);
    localIntent.putExtra("key_text_input", paramString1);
    localIntent.putExtra("key_text_output", paramString2);
    localIntent.putExtra("key_language_from", paramString3);
    localIntent.putExtra("key_language_to", paramString4);
    if (paramActivity.getClass() == HomeActivity.class)
      setNoTransitionAnimation(localIntent);
    paramActivity.startActivity(localIntent);
  }

  // ERROR //
  public static String readStringFromHttpResponse(HttpResponse paramHttpResponse)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_1
    //   2: aload_0
    //   3: invokestatic 824	com/google/android/apps/translate/Util:getResponseInputStream	(Lorg/apache/http/HttpResponse;)Ljava/io/InputStream;
    //   6: astore_1
    //   7: aload_1
    //   8: ifnull +119 -> 127
    //   11: new 826	java/io/InputStreamReader
    //   14: dup
    //   15: aload_1
    //   16: ldc_w 702
    //   19: invokespecial 829	java/io/InputStreamReader:<init>	(Ljava/io/InputStream;Ljava/lang/String;)V
    //   22: astore 9
    //   24: sipush 512
    //   27: newarray char
    //   29: astore 10
    //   31: new 200	java/lang/StringBuilder
    //   34: dup
    //   35: invokespecial 201	java/lang/StringBuilder:<init>	()V
    //   38: astore 11
    //   40: aload 9
    //   42: aload 10
    //   44: invokevirtual 833	java/io/InputStreamReader:read	([C)I
    //   47: istore 12
    //   49: iload 12
    //   51: iconst_m1
    //   52: if_icmple +33 -> 85
    //   55: aload 11
    //   57: aload 10
    //   59: iconst_0
    //   60: iload 12
    //   62: invokevirtual 836	java/lang/StringBuilder:append	([CII)Ljava/lang/StringBuilder;
    //   65: pop
    //   66: goto -26 -> 40
    //   69: astore 7
    //   71: aconst_null
    //   72: astore 5
    //   74: aload_1
    //   75: ifnull +7 -> 82
    //   78: aload_1
    //   79: invokevirtual 841	java/io/InputStream:close	()V
    //   82: aload 5
    //   84: areturn
    //   85: aload 11
    //   87: invokevirtual 842	java/lang/StringBuilder:length	()I
    //   90: istore 14
    //   92: aconst_null
    //   93: astore 5
    //   95: iload 14
    //   97: ifle +14 -> 111
    //   100: aload 11
    //   102: invokevirtual 211	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   105: astore 15
    //   107: aload 15
    //   109: astore 5
    //   111: aload_1
    //   112: ifnull -30 -> 82
    //   115: aload_1
    //   116: invokevirtual 841	java/io/InputStream:close	()V
    //   119: aload 5
    //   121: areturn
    //   122: astore 16
    //   124: aload 5
    //   126: areturn
    //   127: aconst_null
    //   128: astore 5
    //   130: aload_1
    //   131: ifnull -49 -> 82
    //   134: aload_1
    //   135: invokevirtual 841	java/io/InputStream:close	()V
    //   138: aconst_null
    //   139: areturn
    //   140: astore 17
    //   142: aconst_null
    //   143: areturn
    //   144: astore 4
    //   146: aconst_null
    //   147: astore 5
    //   149: aload_1
    //   150: ifnull -68 -> 82
    //   153: aload_1
    //   154: invokevirtual 841	java/io/InputStream:close	()V
    //   157: aconst_null
    //   158: areturn
    //   159: astore 6
    //   161: aconst_null
    //   162: areturn
    //   163: astore_2
    //   164: aload_1
    //   165: ifnull +7 -> 172
    //   168: aload_1
    //   169: invokevirtual 841	java/io/InputStream:close	()V
    //   172: aload_2
    //   173: athrow
    //   174: astore 8
    //   176: aconst_null
    //   177: areturn
    //   178: astore_3
    //   179: goto -7 -> 172
    //
    // Exception table:
    //   from	to	target	type
    //   2	7	69	java/io/UnsupportedEncodingException
    //   11	40	69	java/io/UnsupportedEncodingException
    //   40	49	69	java/io/UnsupportedEncodingException
    //   55	66	69	java/io/UnsupportedEncodingException
    //   85	92	69	java/io/UnsupportedEncodingException
    //   100	107	69	java/io/UnsupportedEncodingException
    //   115	119	122	java/io/IOException
    //   134	138	140	java/io/IOException
    //   2	7	144	java/io/IOException
    //   11	40	144	java/io/IOException
    //   40	49	144	java/io/IOException
    //   55	66	144	java/io/IOException
    //   85	92	144	java/io/IOException
    //   100	107	144	java/io/IOException
    //   153	157	159	java/io/IOException
    //   2	7	163	finally
    //   11	40	163	finally
    //   40	49	163	finally
    //   55	66	163	finally
    //   85	92	163	finally
    //   100	107	163	finally
    //   78	82	174	java/io/IOException
    //   168	172	178	java/io/IOException
  }

  public static void searchTextOnWeb(Activity paramActivity, String paramString, ChipView.ChipPart paramChipPart)
  {
    Intent localIntent = new Intent();
    localIntent.setAction("android.intent.action.WEB_SEARCH");
    localIntent.putExtra("query", paramString);
    try
    {
      paramActivity.startActivity(localIntent);
      UserActivityMgr localUserActivityMgr = UserActivityMgr.get();
      if (paramChipPart == ChipView.ChipPart.INPUT_TEXT);
      for (UserActivityMgr.IntervalCountTag localIntervalCountTag = UserActivityMgr.IntervalCountTag.CHIPVIEW_SRC_SEARCH_CLICKS; ; localIntervalCountTag = UserActivityMgr.IntervalCountTag.CHIPVIEW_TRG_SEARCH_CLICKS)
      {
        localUserActivityMgr.incrementIntervalCount(localIntervalCountTag, 1);
        return;
      }
    }
    catch (ActivityNotFoundException localActivityNotFoundException)
    {
      Logger.e(R.string.msg_error_intent_web_search + " " + localActivityNotFoundException.getMessage());
      showLongToastMessage(paramActivity, R.string.msg_error_intent_web_search);
    }
  }

  private static void setNoTransitionAnimation(Intent paramIntent)
  {
    SdkVersionWrapper.getWrapper().setIntentNoAnimation(paramIntent);
    paramIntent.putExtra("key_no_transition_animation", true);
  }

  public static void setTextAndFont(TextView paramTextView, String paramString, Language[] paramArrayOfLanguage, Constants.AppearanceType paramAppearanceType, boolean paramBoolean)
  {
    if (paramBoolean);
    for (SpannableString localSpannableString = new SpannableString(Html.fromHtml(paramString)); ; localSpannableString = new SpannableString(paramString))
    {
      detectAndSetFonts(localSpannableString, paramArrayOfLanguage, paramAppearanceType);
      paramTextView.setText(localSpannableString);
      return;
    }
  }

  public static void setUserAgentToHttpClient(HttpClient paramHttpClient, String paramString)
  {
    HttpParams localHttpParams = paramHttpClient.getParams();
    if (paramString == null)
      paramString = HttpProtocolParams.getUserAgent(localHttpParams);
    if (paramString.indexOf("gzip") == -1)
      paramString = paramString + " (gzip)";
    HttpProtocolParams.setUserAgent(localHttpParams, paramString);
  }

  public static void showLongToastMessage(Activity paramActivity, int paramInt)
  {
    showLongToastMessage(paramActivity, paramActivity.getString(paramInt));
  }

  public static void showLongToastMessage(Activity paramActivity, CharSequence paramCharSequence)
  {
    showToastMessage(paramActivity, paramCharSequence, 1);
  }

  public static void showNetworkTtsError(Activity paramActivity, int paramInt)
  {
    switch (paramInt)
    {
    default:
      return;
    case 0:
      showLongToastMessage(paramActivity, paramActivity.getString(R.string.msg_network_tts_error));
      return;
    case 1:
    }
    showLongToastMessage(paramActivity, paramActivity.getString(R.string.msg_network_tts_too_long));
  }

  public static void showShortToastMessage(Activity paramActivity, int paramInt)
  {
    showShortToastMessage(paramActivity, paramActivity.getString(paramInt));
  }

  public static void showShortToastMessage(Activity paramActivity, CharSequence paramCharSequence)
  {
    showToastMessage(paramActivity, paramCharSequence, 0);
  }

  private static void showToastMessage(Activity paramActivity, final CharSequence paramCharSequence, final int paramInt)
  {
    if (TextUtils.isEmpty(paramCharSequence))
      return;
    paramActivity.runOnUiThread(new Runnable()
    {
      public void run()
      {
        Toast.makeText(this.val$activity.getApplicationContext(), paramCharSequence, paramInt).show();
      }
    });
  }

  public static void showTranslationErrorToastMessage(Activity paramActivity, int paramInt)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    switch (paramInt)
    {
    default:
      localStringBuilder.append(paramActivity.getString(R.string.msg_translation_error));
      localStringBuilder.append(" E");
      localStringBuilder.append(Math.abs(paramInt));
    case 0:
    case -2:
    case -201:
    }
    while (true)
    {
      showLongToastMessage(paramActivity, localStringBuilder);
      return;
      localStringBuilder.append(paramActivity.getString(R.string.msg_network_error));
      continue;
      localStringBuilder.append(paramActivity.getString(R.string.msg_network_error));
      localStringBuilder.append(" E");
      localStringBuilder.append(Math.abs(paramInt));
    }
  }

  public static String[] splitTranslateResult(Entry paramEntry)
  {
    if (paramEntry == null)
      return new String[] { "" };
    return splitTranslateResult(paramEntry.getOutputText());
  }

  public static String[] splitTranslateResult(String paramString)
  {
    if (TextUtils.isEmpty(paramString))
      return new String[] { "" };
    return paramString.split("\t");
  }

  public static void toggleStarredTranslation(Activity paramActivity, boolean paramBoolean, Entry paramEntry)
  {
    if (paramBoolean)
    {
      FavoriteDb.add(paramActivity, paramEntry);
      return;
    }
    FavoriteDb.remove(paramActivity, paramEntry);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.Util
 * JD-Core Version:    0.6.2
 */