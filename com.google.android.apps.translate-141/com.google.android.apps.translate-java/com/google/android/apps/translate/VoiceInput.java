package com.google.android.apps.translate;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.speech.RecognitionListener;
import android.speech.SpeechRecognizer;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import com.google.android.apps.translate.asreditor.AsrResultEditor;
import com.google.android.apps.translate.editor.InstantTranslateHandler;
import com.google.android.apps.translate.editor.TextSlot;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class VoiceInput
  implements CompoundButton.OnCheckedChangeListener
{
  public static final int DEFAULT = 0;
  public static final int ERROR = 3;
  private static final String EXTRA_SPEECH_INPUT_COMPLETE_SILENCE_LENGTH_MILLIS = "android.speech.extras.SPEECH_INPUT_COMPLETE_SILENCE_LENGTH_MILLIS";
  private static final String EXTRA_SPEECH_INPUT_POSSIBLY_COMPLETE_SILENCE_LENGTH_MILLIS = "android.speech.extras.SPEECH_INPUT_POSSIBLY_COMPLETE_SILENCE_LENGTH_MILLIS";
  private static final String EXTRA_SPEECH_MINIMUM_LENGTH_MILLIS = "android.speech.extras.SPEECH_INPUT_MINIMUM_LENGTH_MILLIS";
  private static final String INPUT_COMPLETE_SILENCE_LENGTH_DEFAULT_VALUE_MILLIS = "1000";
  private static final String INPUT_POSSIBLY_COMPLETE_SILENCE_LENGTH_DEFAULT_VALUE_MILLIS = null;
  public static final int LISTENING = 1;
  private static final int MAX_ASR_RESULTS = 50;
  private static final int MSG_CLOSE_ERROR_DIALOG = 1;
  private static final String TAG = "VoiceInput";
  private static final String UNSUPPORTED_EXTRA_DICTATION_MODE = "android.speech.extra.DICTATION_MODE";
  private static final String UNSUPPORTED_EXTRA_PARTIAL_RESULTS = "com.google.android.voicesearch.UNSUPPORTED_PARTIAL_RESULTS";
  public static final int WORKING = 2;
  private AsrResultEditor mAsrResultEditor;
  private Context mContext;
  private TextSlot mEditorField;
  private final Handler mHandler = new Handler()
  {
    public void handleMessage(Message paramAnonymousMessage)
    {
      if (paramAnonymousMessage.what == 1)
      {
        VoiceInput.access$002(VoiceInput.this, 0);
        VoiceInput.this.mRecognitionView.finish();
        VoiceInput.this.onCancelVoice();
      }
    }
  };
  private InstantTranslateHandler mInstantTranslateHandler;
  private String mLocale;
  private String mPrefixText = "";
  private String mPrevText = "";
  private RecognitionListener mRecognitionListener = new ImeRecognitionListener(null);
  private RecognitionView mRecognitionView;
  private SpeechRecognizer mSpeechRecognizer;
  private int mState = 0;
  private String mSuffixText = "";
  private List<UiListener> mUiListenerList = Lists.newArrayList();
  private boolean mVoiceCanceled;

  public VoiceInput(Context paramContext, RecognitionView paramRecognitionView)
  {
    this.mSpeechRecognizer = SpeechRecognizer.createSpeechRecognizer(paramContext);
    this.mSpeechRecognizer.setRecognitionListener(this.mRecognitionListener);
    this.mContext = paramContext;
    this.mRecognitionView = paramRecognitionView;
  }

  private int getErrorStringId(int paramInt, boolean paramBoolean)
  {
    switch (paramInt)
    {
    default:
      return R.string.voice_error;
    case 5:
      return R.string.voice_not_installed;
    case 2:
      return R.string.voice_network_error;
    case 1:
      if (paramBoolean)
        return R.string.voice_network_error;
      return R.string.voice_too_much_speech;
    case 3:
      return R.string.voice_audio_error;
    case 4:
      return R.string.voice_server_error;
    case 6:
      return R.string.voice_speech_timeout;
    case 7:
      return R.string.voice_no_match;
    case 8:
    }
    return R.string.voice_recognizer_busy;
  }

  private String getPrefixText()
  {
    if (this.mEditorField != null)
    {
      int i = this.mEditorField.getSelectionStart();
      if ((i >= 0) && (i <= this.mPrevText.length()))
        return this.mPrevText.subSequence(0, i).toString();
    }
    return "";
  }

  private String getSuffixText()
  {
    if (this.mEditorField != null)
    {
      int i = this.mEditorField.getSelectionEnd();
      if ((i >= 0) && (i <= this.mPrevText.length()))
        return this.mPrevText.subSequence(i, this.mPrevText.length()).toString();
    }
    return "";
  }

  private void onCancelVoice()
  {
    this.mVoiceCanceled = true;
    Iterator localIterator = this.mUiListenerList.iterator();
    while (localIterator.hasNext())
      ((UiListener)localIterator.next()).onCancelVoice();
  }

  private void onError()
  {
    Iterator localIterator = this.mUiListenerList.iterator();
    while (localIterator.hasNext())
      ((UiListener)localIterator.next()).onError();
  }

  private void onError(int paramInt, boolean paramBoolean)
  {
    Logger.e("VoiceInput", "error " + paramInt);
    onError(this.mContext.getString(getErrorStringId(paramInt, paramBoolean)));
  }

  private void onError(String paramString)
  {
    this.mState = 3;
    this.mRecognitionView.showError(paramString);
    this.mHandler.sendMessageDelayed(Message.obtain(this.mHandler, 1), 2000L);
  }

  private void onVoiceResults(List<CharSequence> paramList, boolean paramBoolean1, boolean paramBoolean2)
  {
    if (paramBoolean1);
    do
    {
      return;
      if ((paramList != null) && (paramList.size() > 0))
        setText(paramList, paramBoolean2);
      Iterator localIterator = this.mUiListenerList.iterator();
      while (localIterator.hasNext())
        ((UiListener)localIterator.next()).onVoiceResults(paramList, paramBoolean1, paramBoolean2);
    }
    while (!paramBoolean2);
    setCursorAtEnd();
    this.mInstantTranslateHandler.commitSourceText();
    cancel();
  }

  private void putEndpointerExtra(ContentResolver paramContentResolver, Intent paramIntent, String paramString1, String paramString2)
  {
    long l1 = -1L;
    if (paramString2 != null);
    try
    {
      long l2 = Long.valueOf(paramString2).longValue();
      l1 = l2;
      if (l1 != -1L)
        paramIntent.putExtra(paramString1, l1);
      return;
    }
    catch (NumberFormatException localNumberFormatException)
    {
      while (true)
        Logger.e("VoiceInput", "could not parse value for " + paramString2);
    }
  }

  private void setPreviousText()
  {
    try
    {
      if (this.mEditorField != null)
        this.mPrevText = this.mEditorField.getText().toString();
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  private void setPreviousTexts(String paramString1, String paramString2)
  {
    this.mPrefixText = paramString1;
    this.mSuffixText = paramString2;
  }

  private void setText(List<CharSequence> paramList, boolean paramBoolean)
  {
    ((CharSequence)paramList.get(0)).toString();
    ArrayList localArrayList = Lists.newArrayList();
    Iterator localIterator = paramList.iterator();
    while (localIterator.hasNext())
    {
      CharSequence localCharSequence = (CharSequence)localIterator.next();
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append(this.mPrefixText);
      localStringBuilder.append(localCharSequence);
      localStringBuilder.append(this.mSuffixText);
      localArrayList.add(localStringBuilder.toString());
    }
    this.mAsrResultEditor.setAsrResults(localArrayList);
    if (paramBoolean)
      this.mAsrResultEditor.setRecognizing(false);
  }

  private void startListeningAfterInitialization(Context paramContext, String paramString)
  {
    setPreviousText();
    setPreviousTexts(getPrefixText(), getSuffixText());
    Intent localIntent = new Intent("android.speech.action.RECOGNIZE_SPEECH");
    VoiceInputHelper.prepareAsrIntent(localIntent, paramString);
    localIntent.putExtra("android.speech.extra.PARTIAL_RESULTS", true);
    localIntent.putExtra("android.speech.extra.DICTATION_MODE", true);
    localIntent.putExtra("android.speech.extra.MAX_RESULTS", 50);
    ContentResolver localContentResolver = this.mContext.getContentResolver();
    putEndpointerExtra(localContentResolver, localIntent, "android.speech.extras.SPEECH_INPUT_MINIMUM_LENGTH_MILLIS", null);
    putEndpointerExtra(localContentResolver, localIntent, "android.speech.extras.SPEECH_INPUT_COMPLETE_SILENCE_LENGTH_MILLIS", "1000");
    putEndpointerExtra(localContentResolver, localIntent, "android.speech.extras.SPEECH_INPUT_POSSIBLY_COMPLETE_SILENCE_LENGTH_MILLIS", INPUT_POSSIBLY_COMPLETE_SILENCE_LENGTH_DEFAULT_VALUE_MILLIS);
    this.mSpeechRecognizer.startListening(localIntent);
  }

  public void addVoiceInputListener(UiListener paramUiListener)
  {
    this.mUiListenerList.add(paramUiListener);
  }

  public void cancel()
  {
    this.mState = 0;
    this.mHandler.removeMessages(1);
    setPreviousTexts("", "");
    this.mSpeechRecognizer.cancel();
    this.mRecognitionView.finish();
    onCancelVoice();
  }

  public void destroy()
  {
    this.mSpeechRecognizer.destroy();
  }

  public void onCheckedChanged(CompoundButton paramCompoundButton, boolean paramBoolean)
  {
    if (paramBoolean)
    {
      this.mVoiceCanceled = false;
      this.mState = 1;
      this.mRecognitionView.showInitializing();
      startListeningAfterInitialization(this.mContext, this.mLocale);
      return;
    }
    cancel();
  }

  public void setCursorAtEnd()
  {
    int i = this.mEditorField.getText().toString().length();
    this.mEditorField.setSelection(i, i);
  }

  public void setInstantTranslateHandler(InstantTranslateHandler paramInstantTranslateHandler)
  {
    this.mInstantTranslateHandler = paramInstantTranslateHandler;
  }

  public void startListening(Context paramContext, AsrResultEditor paramAsrResultEditor, String paramString)
  {
    this.mVoiceCanceled = false;
    this.mAsrResultEditor = paramAsrResultEditor;
    this.mEditorField = this.mAsrResultEditor.getEditorField();
    this.mState = 1;
    this.mContext = paramContext;
    this.mLocale = paramString;
    this.mRecognitionView.showInitializing();
    startListeningAfterInitialization(this.mContext, this.mLocale);
    this.mAsrResultEditor.setRecognizing(true);
  }

  private class ImeRecognitionListener
    implements RecognitionListener
  {
    private boolean mEndpointed = false;
    int mSpeechStart;
    final ByteArrayOutputStream mWaveBuffer = new ByteArrayOutputStream();

    private ImeRecognitionListener()
    {
    }

    public void onBeginningOfSpeech()
    {
      this.mEndpointed = false;
      this.mSpeechStart = this.mWaveBuffer.size();
      VoiceInput.this.onError();
    }

    public void onBufferReceived(byte[] paramArrayOfByte)
    {
      try
      {
        this.mWaveBuffer.write(paramArrayOfByte);
        return;
      }
      catch (IOException localIOException)
      {
      }
    }

    public void onEndOfSpeech()
    {
      this.mEndpointed = true;
      VoiceInput.access$002(VoiceInput.this, 2);
      VoiceInput.this.mRecognitionView.showWorking(this.mWaveBuffer, this.mSpeechStart, this.mWaveBuffer.size());
    }

    public void onError(int paramInt)
    {
      VoiceInput.access$002(VoiceInput.this, 3);
      VoiceInput.this.onError(paramInt, this.mEndpointed);
      VoiceInput.this.onError();
    }

    public void onEvent(int paramInt, Bundle paramBundle)
    {
    }

    public void onPartialResults(Bundle paramBundle)
    {
      String[] arrayOfString = paramBundle.getStringArray("com.google.android.voicesearch.UNSUPPORTED_PARTIAL_RESULTS");
      if ((arrayOfString != null) && (arrayOfString.length > 0))
      {
        VoiceInput.access$002(VoiceInput.this, 0);
        ArrayList localArrayList = new ArrayList();
        int i = arrayOfString.length;
        for (int j = 0; j < i; j++)
          localArrayList.add(arrayOfString[j]);
        VoiceInput.this.onVoiceResults(localArrayList, VoiceInput.this.mVoiceCanceled, false);
      }
      onResults(paramBundle);
    }

    public void onReadyForSpeech(Bundle paramBundle)
    {
      VoiceInput.this.mRecognitionView.showListening();
    }

    public void onResults(Bundle paramBundle)
    {
      ArrayList localArrayList1 = paramBundle.getStringArrayList("results_recognition");
      ArrayList localArrayList2 = new ArrayList();
      if ((localArrayList1 != null) && (localArrayList1.size() > 0))
      {
        VoiceInput.access$002(VoiceInput.this, 0);
        Iterator localIterator = localArrayList1.iterator();
        while (localIterator.hasNext())
        {
          String str = (String)localIterator.next();
          localArrayList2.add(str);
          Logger.d("VoiceInput", str);
        }
        VoiceInput.this.onVoiceResults(localArrayList2, VoiceInput.this.mVoiceCanceled, true);
        VoiceInput.this.mRecognitionView.finish();
      }
    }

    public void onRmsChanged(float paramFloat)
    {
      VoiceInput.this.mRecognitionView.updateVoiceMeter(paramFloat);
    }
  }

  public static abstract interface UiListener
  {
    public abstract void onCancelVoice();

    public abstract void onError();

    public abstract void onVoiceResults(List<CharSequence> paramList, boolean paramBoolean1, boolean paramBoolean2);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.VoiceInput
 * JD-Core Version:    0.6.2
 */