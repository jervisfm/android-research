package com.google.android.apps.translate;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Build.VERSION;
import android.os.Bundle;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

public final class VoiceInputHelper
{
  public static final boolean DEBUG = false;
  private static final String[] DEFAULT_CONVERSATION_LANGUAGES = { "en", "es" };
  private static final Set<String> DEFAULT_CONVERSATION_LANGUAGES_SET = new HashSet(Arrays.asList(DEFAULT_CONVERSATION_LANGUAGES));
  private static final String DOGFOOD_VOICE_LANGUAGE_TAG = "dogfood";
  private static final boolean EXCLUDE_DOGFOOD_VOICE_INPUT_LANGUAGES = false;
  private static final String FIELD_NAME = "fieldName";
  private static final String IME_OPTIONS = "imeOptions";
  private static final String INPUT_TYPE = "inputType";
  private static final int MAX_ASR_RESULTS = 50;
  private static final String PACKAGE_NAME = "packageName";
  private static final int SDK_INT = 0;
  private static final String SELECTED_LANGUAGE = "selectedLanguage";
  private static final String SINGLE_LINE = "singleLine";
  private static final String TAG = "VoiceInputHelper";
  private Context mContext;
  private Map<String, VoiceInputLanguages> mDefaultVoiceInputLanguages = Maps.newHashMap();
  private boolean mHasSpeechRecognizer;
  private Map<String, VoiceInputLanguages> mVoiceInputLanguages = Maps.newHashMap();

  public VoiceInputHelper(Context paramContext)
  {
    this.mContext = paramContext;
    reloadVoiceInputLanguageMap();
    this.mHasSpeechRecognizer = Util.hasSpeechRecognizer(paramContext);
    List localList = Arrays.asList(this.mContext.getResources().getStringArray(R.array.voiceInputLocales));
    loadVoiceInputLanguages(this.mDefaultVoiceInputLanguages, localList);
    logVoiceLanguages("DEFAULT", localList);
  }

  public static String getRecognitionResult(Intent paramIntent)
  {
    ArrayList localArrayList = paramIntent.getStringArrayListExtra("android.speech.extra.RESULTS");
    if ((localArrayList != null) && (!localArrayList.isEmpty()))
      return (String)localArrayList.get(0);
    return null;
  }

  public static List<String> getRecognitionResults(Intent paramIntent)
  {
    return paramIntent.getStringArrayListExtra("android.speech.extra.RESULTS");
  }

  public static String getShortLanguageNameFromVoiceInputLanguage(String paramString)
  {
    String str = paramString.toLowerCase();
    if (str.startsWith("he-"))
      return "iw";
    if (str.equals("latin"))
      return "la";
    if (str.equals("euskara"))
      return "eu";
    if (str.startsWith("fil-"))
      return "tl";
    if (str.startsWith("nb-"))
      return "no";
    if (str.contains("-hans-"))
      return "zh-CN";
    if (str.contains("-hant-"))
      return "zh-TW";
    int i = str.indexOf('-');
    if (i < 0)
      i = str.length();
    if ((i == 2) || (i == 3))
      return str.substring(0, i);
    Logger.d("VoiceInputHelper", "Unrecognized voice input language: [" + str + "]");
    return null;
  }

  public static void getSupportedLanguages(Context paramContext, BroadcastReceiver paramBroadcastReceiver)
  {
    if (SDK_INT >= 8)
      paramContext.sendOrderedBroadcast(new Intent("android.speech.action.GET_LANGUAGE_DETAILS"), null, paramBroadcastReceiver, null, -1, null, null);
  }

  public static final String getVoiceInputLanguageNameFromDogfoodName(String paramString)
  {
    String str = paramString.replaceAll("-x-dogfood.*$", "");
    Logger.d("VoiceInputHelper", "getVoiceInputLanguageNameFromDogfoodName " + paramString + " ==> " + str);
    return str;
  }

  public static final boolean isDogfoodVoiceInputLanguage(String paramString)
  {
    return paramString.contains("dogfood");
  }

  private boolean loadVoiceInputLanguages(Map<String, VoiceInputLanguages> paramMap, List<String> paramList)
  {
    if ((paramList != null) && (paramList.size() > 0))
    {
      Iterator localIterator = paramList.iterator();
      while (localIterator.hasNext())
      {
        String str1 = (String)localIterator.next();
        String str2 = getShortLanguageNameFromVoiceInputLanguage(str1);
        if (str2 != null)
        {
          VoiceInputLanguages localVoiceInputLanguages3 = (VoiceInputLanguages)paramMap.get(str2);
          if (localVoiceInputLanguages3 == null)
          {
            localVoiceInputLanguages3 = new VoiceInputLanguages(str2);
            paramMap.put(str2, localVoiceInputLanguages3);
          }
          localVoiceInputLanguages3.addVoiceInputLanguage(str1);
        }
      }
      VoiceInputLanguages localVoiceInputLanguages1 = (VoiceInputLanguages)paramMap.get("zh-CN");
      VoiceInputLanguages localVoiceInputLanguages2 = (VoiceInputLanguages)paramMap.get("zh-TW");
      if ((localVoiceInputLanguages1 != null) && (localVoiceInputLanguages2 != null))
      {
        localVoiceInputLanguages1.addVoiceInputLanguages(localVoiceInputLanguages2.getVoiceInputLanguageList());
        localVoiceInputLanguages2.clearVoiceInputLanguages();
        localVoiceInputLanguages2.addVoiceInputLanguages(localVoiceInputLanguages1.getVoiceInputLanguageList());
      }
      return true;
    }
    return false;
  }

  public static void logVoiceLanguages(String paramString, List<String> paramList)
  {
    Logger.e("VoiceInputHelper", "logVoiceLanguages START tag=" + paramString);
    if (paramList == null)
      return;
    Iterator localIterator = paramList.iterator();
    while (localIterator.hasNext())
    {
      String str = (String)localIterator.next();
      Logger.d("VoiceInputHelper", "logVoiceLanguages latest list: " + str);
    }
    Logger.e("VoiceInputHelper", "logVoiceLanguages DONE tag=" + paramString);
  }

  public static void prepareAsrIntent(Intent paramIntent, String paramString)
  {
    VoiceInputHelper.class.getPackage().getName();
    paramIntent.putExtra("calling_package", VoiceInputHelper.class.getPackage().getName());
    paramIntent.putExtra("android.speech.extra.LANGUAGE_MODEL", "free_form");
    paramIntent.putExtra("android.speech.extra.MAX_RESULTS", 1);
    paramIntent.putExtra("android.speech.extra.LANGUAGE", paramString);
    Logger.d("VoiceInputHelper: set EXTRA_LANGUAGE to " + paramString);
    Bundle localBundle = new Bundle();
    localBundle.putString("packageName", "com.google.android.apps.translate");
    localBundle.putString("fieldName", "AndroidTranslate");
    localBundle.putInt("inputType", 64);
    localBundle.putInt("imeOptions", 0);
    localBundle.putBoolean("singleLine", true);
    if (paramString != null)
      localBundle.putString("selectedLanguage", paramString);
    if (Util.getSdkVersion() < 8)
      paramIntent.putExtra("android.speech.extras.RECOGNITION_CONTEXT", localBundle);
  }

  public static void startVoiceInput(VoiceInputHelper paramVoiceInputHelper, Activity paramActivity, Language paramLanguage, int paramInt)
  {
    paramVoiceInputHelper.inputFromSpeech(paramActivity, paramLanguage, paramInt, null);
  }

  public String getAsrLocale(Context paramContext, Language paramLanguage)
  {
    VoiceInputLanguages localVoiceInputLanguages = (VoiceInputLanguages)this.mVoiceInputLanguages.get(paramLanguage.getShortName());
    String str = null;
    if (localVoiceInputLanguages != null)
    {
      str = Profile.getVoiceInputLanguage(paramContext, paramLanguage.getShortName());
      Logger.d("VoiceInputHelper", "getAsrLocale pref-locale=" + str);
      if ((str.equals("")) || (!localVoiceInputLanguages.containsVoiceInputLanguage(str)))
      {
        str = localVoiceInputLanguages.getDefaultVoiceInputLanguage();
        Logger.d("VoiceInputHelper", "getAsrLocale default-locale=" + str);
      }
    }
    return str;
  }

  public List<Language> getTranslateLanguageList(Languages paramLanguages)
  {
    ArrayList localArrayList = Lists.newArrayList();
    Iterator localIterator = this.mVoiceInputLanguages.keySet().iterator();
    while (localIterator.hasNext())
    {
      String str = (String)localIterator.next();
      Language localLanguage = paramLanguages.getToLanguageByShortName(str);
      if (localLanguage == null)
        Logger.d("VoiceInputHelper", "Unrecognized language: [" + str + "]");
      else
        localArrayList.add(localLanguage);
    }
    return localArrayList;
  }

  public List<String> getVoiceInputLanguageList(String paramString)
  {
    if (!this.mVoiceInputLanguages.containsKey(paramString))
      return new ArrayList();
    return ((VoiceInputLanguages)this.mVoiceInputLanguages.get(paramString)).getVoiceInputLanguageList();
  }

  public void inputFromSpeech(Activity paramActivity, Language paramLanguage, int paramInt, PendingIntent paramPendingIntent)
  {
    String str = getAsrLocale(paramActivity, paramLanguage);
    Intent localIntent = new Intent("android.speech.action.RECOGNIZE_SPEECH");
    prepareAsrIntent(localIntent, str);
    if (paramPendingIntent != null)
    {
      localIntent.putExtra("android.speech.extra.MAX_RESULTS", 50);
      localIntent.putExtra("android.speech.extra.RESULTS_PENDINGINTENT", paramPendingIntent);
      localIntent.putExtra("android.speech.extra.RESULTS_PENDINGINTENT_BUNDLE", new Bundle());
      localIntent.putExtra("android.speech.extra.PROMPT", paramActivity.getString(Util.getLocalizedStringId(paramActivity, R.string.label_speak_now, paramLanguage)));
    }
    try
    {
      paramActivity.startActivityForResult(localIntent, paramInt);
      return;
    }
    catch (ActivityNotFoundException localActivityNotFoundException)
    {
      Logger.e(R.string.msg_error_intent_voice_input + " " + localActivityNotFoundException.getMessage());
      Util.showLongToastMessage(paramActivity, R.string.msg_error_intent_voice_input);
    }
  }

  public boolean isConversationLanguage(Language paramLanguage)
  {
    return ((DEFAULT_CONVERSATION_LANGUAGES_SET.contains(paramLanguage.getShortName())) || (isVoiceInputAvailable(paramLanguage))) && (!"la".equals(paramLanguage.getShortName()));
  }

  public boolean isVoiceInputAvailable(Language paramLanguage)
  {
    if (paramLanguage == null)
      Logger.d("VoiceInputHelper", "isVoiceInputAvailable language=null");
    do
    {
      return false;
      Logger.d("VoiceInputHelper", "isVoiceInputAvailable language=" + paramLanguage.toString());
    }
    while ((!this.mHasSpeechRecognizer) || (this.mVoiceInputLanguages.get(paramLanguage.getShortName()) == null));
    return true;
  }

  public void reloadVoiceInputLanguageMap()
  {
    this.mVoiceInputLanguages.clear();
    if (!loadVoiceInputLanguages(this.mVoiceInputLanguages, Profile.getSupportedVoiceLanguages(this.mContext)))
    {
      if (Util.getSdkVersion() < 5)
        break label43;
      this.mVoiceInputLanguages = this.mDefaultVoiceInputLanguages;
    }
    label43: String str;
    do
    {
      do
        return;
      while (Util.getSdkVersion() != 4);
      str = Locale.getDefault().getLanguage();
      Logger.i("locale language: " + str);
    }
    while (!this.mDefaultVoiceInputLanguages.containsKey(str));
    this.mVoiceInputLanguages.put(str, this.mDefaultVoiceInputLanguages.get(str));
  }

  public void startVoiceInput(Activity paramActivity, Language paramLanguage)
  {
    startVoiceInput(this, paramActivity, paramLanguage, 100);
  }

  private static class VoiceInputLanguages
  {
    private boolean mPubliclyAvailableLangExists = false;
    private String mTranslateLanguage;
    private List<String> mVoiceInputLanguageList;

    public VoiceInputLanguages(String paramString)
    {
      this.mTranslateLanguage = paramString;
      this.mVoiceInputLanguageList = Lists.newArrayList();
    }

    public void addVoiceInputLanguage(String paramString)
    {
      if (!VoiceInputHelper.isDogfoodVoiceInputLanguage(paramString))
        this.mPubliclyAvailableLangExists = true;
      this.mVoiceInputLanguageList.add(paramString);
    }

    public void addVoiceInputLanguages(List<String> paramList)
    {
      Iterator localIterator = paramList.iterator();
      while (localIterator.hasNext())
        addVoiceInputLanguage((String)localIterator.next());
    }

    public void clearVoiceInputLanguages()
    {
      this.mPubliclyAvailableLangExists = false;
      this.mVoiceInputLanguageList.clear();
    }

    public boolean containsVoiceInputLanguage(String paramString)
    {
      return this.mVoiceInputLanguageList.contains(paramString);
    }

    public String getDefaultVoiceInputLanguage()
    {
      if (this.mVoiceInputLanguageList.size() == 1)
      {
        Logger.d("VoiceInputHelper", "getDefaultVoiceInputLanguage full-locale=" + (String)this.mVoiceInputLanguageList.get(0));
        return (String)this.mVoiceInputLanguageList.get(0);
      }
      if (!this.mPubliclyAvailableLangExists)
        return (String)this.mVoiceInputLanguageList.get(0);
      String str = this.mTranslateLanguage;
      if ((str != null) && (Util.getSdkVersion() >= 16))
      {
        if (!str.equals("zh-CN"))
          break label139;
        str = "cmn-Hans-CN";
      }
      while (true)
      {
        Logger.d("VoiceInputHelper", "getDefaultVoiceInputLanguage locale=" + str);
        return str;
        label139: if (str.equals("zh-TW"))
          str = "cmn-Hant-TW";
      }
    }

    public List<String> getVoiceInputLanguageList()
    {
      return this.mVoiceInputLanguageList;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.VoiceInputHelper
 * JD-Core Version:    0.6.2
 */