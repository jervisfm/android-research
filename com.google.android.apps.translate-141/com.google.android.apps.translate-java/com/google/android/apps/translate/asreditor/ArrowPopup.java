package com.google.android.apps.translate.asreditor;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.NinePatchDrawable;
import android.os.IBinder;
import android.text.Layout;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View.MeasureSpec;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.google.android.apps.translate.Logger;
import com.google.android.apps.translate.R.drawable;

public class ArrowPopup extends FrameLayout
{
  private static final String TAG = "ArrowPopup";
  private Point mArrowPoint;
  private NinePatchDrawable mCenterDrawable;
  private boolean mFilterDpad = false;
  private NinePatchDrawable mLeftDrawable;
  private OnOutsideEventListener mOutsideListener;
  private boolean mOutsideTouchable = true;
  private WindowManager.LayoutParams mParams;
  private Rect mPopupRect;
  private NinePatchDrawable mRightDrawable;
  private boolean mTouchStartedOutside = false;
  private boolean mTypingCancels = false;
  private boolean mVisible;
  private IBinder mWindowToken;

  public ArrowPopup(Context paramContext)
  {
    this(paramContext, null);
  }

  public ArrowPopup(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    Resources localResources = paramContext.getResources();
    this.mLeftDrawable = ((NinePatchDrawable)localResources.getDrawable(R.drawable.vs_dialog_correctbubble_left));
    this.mCenterDrawable = ((NinePatchDrawable)localResources.getDrawable(R.drawable.vs_dialog_correctbubble_center));
    this.mRightDrawable = ((NinePatchDrawable)localResources.getDrawable(R.drawable.vs_dialog_correctbubble_right));
    setBackgroundColor(0);
  }

  private Rect calculatePopupRect()
  {
    DisplayMetrics localDisplayMetrics = new DisplayMetrics();
    getWindowManager().getDefaultDisplay().getMetrics(localDisplayMetrics);
    measure(View.MeasureSpec.makeMeasureSpec(localDisplayMetrics.widthPixels, -2147483648), View.MeasureSpec.makeMeasureSpec(localDisplayMetrics.heightPixels - this.mArrowPoint.y, -2147483648));
    int i = getMeasuredWidth();
    int j = getMeasuredHeight();
    int k = this.mLeftDrawable.getMinimumWidth();
    int m = this.mCenterDrawable.getMinimumWidth();
    int n = i - (this.mRightDrawable.getMinimumWidth() + m / 2);
    int i1 = Math.max(k + m / 2, Math.min((int)(i * (this.mArrowPoint.x / localDisplayMetrics.widthPixels)), n));
    int i2 = Math.min(Math.max(this.mArrowPoint.x - i1, 0), localDisplayMetrics.widthPixels - i);
    int i3 = this.mArrowPoint.y;
    return new Rect(i2, i3, i2 + i, i3 + j);
  }

  public static Point getTextSelectionPopupPoint(TextView paramTextView, int paramInt1, int paramInt2)
  {
    Layout localLayout = paramTextView.getLayout();
    if (localLayout == null)
    {
      Logger.w("ArrowPopup", "Trying to calculate popup position with null layout");
      return new Point(0, 0);
    }
    int[] arrayOfInt = new int[2];
    paramTextView.getLocationOnScreen(arrayOfInt);
    int i = arrayOfInt[0] + paramTextView.getPaddingLeft();
    int j = arrayOfInt[1] + paramTextView.getExtendedPaddingTop();
    RectF localRectF = new RectF();
    Path localPath = new Path();
    int k = localLayout.getOffsetForHorizontal(localLayout.getLineForOffset(paramInt2), 0.0F);
    int m;
    if (k == paramInt2)
      m = j + localLayout.getLineBottom(localLayout.getLineForOffset(paramInt2));
    while (true)
    {
      return new Point(i, m);
      if (paramInt1 == paramInt2)
      {
        if (paramInt1 == paramTextView.length())
        {
          localLayout.getSelectionPath(paramInt1 - 1, paramInt2, localPath);
          localPath.computeBounds(localRectF, true);
          i += (int)localRectF.right;
        }
        while (true)
        {
          m = j + (int)localRectF.bottom;
          break;
          localLayout.getSelectionPath(paramInt1, paramInt2 + 1, localPath);
          localPath.computeBounds(localRectF, true);
          i += (int)localRectF.left;
        }
      }
      localLayout.getSelectionPath(Math.max(paramInt1, k), paramInt2, localPath);
      localPath.computeBounds(localRectF, true);
      i += (int)((localRectF.left + localRectF.right) / 2.0F);
      m = j + (int)localRectF.bottom;
    }
  }

  private WindowManager getWindowManager()
  {
    return (WindowManager)getContext().getSystemService("window");
  }

  private void initLayout(Point paramPoint)
  {
    this.mArrowPoint = paramPoint;
    this.mPopupRect = calculatePopupRect();
    this.mParams = createLayoutParams(this.mPopupRect);
  }

  public void cancel()
  {
    dismiss();
  }

  protected WindowManager.LayoutParams createLayoutParams(Rect paramRect)
  {
    DisplayMetrics localDisplayMetrics = new DisplayMetrics();
    getWindowManager().getDefaultDisplay().getMetrics(localDisplayMetrics);
    int i = Math.max(0, Math.min(localDisplayMetrics.widthPixels, paramRect.left));
    int j = Math.max(0, Math.min(localDisplayMetrics.heightPixels, paramRect.top));
    WindowManager.LayoutParams localLayoutParams = new WindowManager.LayoutParams();
    localLayoutParams.flags = 131328;
    localLayoutParams.gravity = 51;
    localLayoutParams.x = i;
    localLayoutParams.y = j;
    localLayoutParams.width = Math.min(localDisplayMetrics.widthPixels - i, paramRect.width());
    localLayoutParams.height = Math.min(localDisplayMetrics.heightPixels - j, paramRect.height());
    if (this.mWindowToken != null)
      localLayoutParams.token = this.mWindowToken;
    localLayoutParams.softInputMode = 1;
    localLayoutParams.type = 1002;
    localLayoutParams.format = -3;
    return localLayoutParams;
  }

  public void disableArrow()
  {
    this.mCenterDrawable = ((NinePatchDrawable)getContext().getResources().getDrawable(R.drawable.vs_dialog_correctbubble_center_noarrow));
  }

  public void dismiss()
  {
    if (this.mVisible)
    {
      this.mVisible = false;
      getWindowManager().removeView(this);
    }
  }

  public boolean dispatchKeyEvent(KeyEvent paramKeyEvent)
  {
    if (paramKeyEvent.getKeyCode() == 4)
    {
      cancel();
      return true;
    }
    int i = paramKeyEvent.getKeyCode();
    if ((this.mOutsideListener != null) && (i != 23) && (i != 19) && (i != 20) && (i != 21) && (i != 22))
    {
      if (this.mTypingCancels)
        cancel();
      if (this.mFilterDpad)
        this.mOutsideListener.onUnhandledKeyEvent(this, paramKeyEvent);
      return false;
    }
    return super.dispatchKeyEvent(paramKeyEvent);
  }

  public boolean dispatchTouchEvent(MotionEvent paramMotionEvent)
  {
    if (!this.mOutsideTouchable)
      return super.dispatchTouchEvent(paramMotionEvent);
    Rect localRect = new Rect();
    getGlobalVisibleRect(localRect);
    if (!localRect.contains((int)paramMotionEvent.getX(), (int)paramMotionEvent.getY()));
    for (boolean bool = true; ; bool = false)
    {
      if (paramMotionEvent.getAction() == 0)
        this.mTouchStartedOutside = bool;
      if ((this.mOutsideListener == null) || (!this.mTouchStartedOutside))
        break;
      if (paramMotionEvent.getAction() == 1)
        cancel();
      MotionEvent localMotionEvent = MotionEvent.obtain(paramMotionEvent);
      localMotionEvent.setLocation(paramMotionEvent.getRawX(), paramMotionEvent.getRawY());
      this.mOutsideListener.onOutsideTouch(this, localMotionEvent);
      return true;
    }
    return super.dispatchTouchEvent(paramMotionEvent);
  }

  public boolean isOutsideTouchable()
  {
    return this.mOutsideTouchable;
  }

  public boolean isVisible()
  {
    return this.mVisible;
  }

  public void move(Point paramPoint)
  {
    initLayout(paramPoint);
    if (isVisible())
    {
      getWindowManager().updateViewLayout(this, this.mParams);
      requestLayout();
      invalidate();
    }
  }

  protected void onDraw(Canvas paramCanvas)
  {
    super.onDraw(paramCanvas);
    int[] arrayOfInt = new int[2];
    getLocationOnScreen(arrayOfInt);
    int i = arrayOfInt[0];
    int j = getWidth();
    int k = getHeight();
    int m = this.mLeftDrawable.getMinimumWidth();
    int n = this.mCenterDrawable.getMinimumWidth();
    int i1 = this.mRightDrawable.getMinimumWidth();
    int i2 = this.mArrowPoint.x - n / 2;
    int i3 = j - i1 - n;
    int i4 = Math.max(m, Math.min(i2 - i, i3));
    this.mLeftDrawable.setBounds(new Rect(0, 0, i4, k));
    this.mCenterDrawable.setBounds(new Rect(i4, 0, i4 + n, k));
    this.mRightDrawable.setBounds(new Rect(i4 + n, 0, j, k));
    this.mLeftDrawable.draw(paramCanvas);
    this.mCenterDrawable.draw(paramCanvas);
    this.mRightDrawable.draw(paramCanvas);
  }

  public void refreshLayout()
  {
    if (isVisible())
    {
      initLayout(this.mArrowPoint);
      getWindowManager().updateViewLayout(this, this.mParams);
      requestLayout();
      invalidate();
    }
  }

  public void setFilterDpad(boolean paramBoolean)
  {
    this.mFilterDpad = paramBoolean;
  }

  public void setOnOutsideTouchListener(OnOutsideEventListener paramOnOutsideEventListener)
  {
    this.mOutsideListener = paramOnOutsideEventListener;
  }

  public void setOutsideTouchable(boolean paramBoolean)
  {
    this.mOutsideTouchable = paramBoolean;
  }

  public void setTypingCancels(boolean paramBoolean)
  {
    this.mTypingCancels = paramBoolean;
  }

  public void setWindowToken(IBinder paramIBinder)
  {
    this.mWindowToken = paramIBinder;
  }

  public void showAt(Point paramPoint)
  {
    initLayout(paramPoint);
    if (!isShown())
      getWindowManager().addView(this, this.mParams);
    this.mVisible = true;
  }

  static abstract interface OnOutsideEventListener
  {
    public abstract void onOutsideTouch(ArrowPopup paramArrowPopup, MotionEvent paramMotionEvent);

    public abstract void onUnhandledKeyEvent(ArrowPopup paramArrowPopup, KeyEvent paramKeyEvent);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.asreditor.ArrowPopup
 * JD-Core Version:    0.6.2
 */