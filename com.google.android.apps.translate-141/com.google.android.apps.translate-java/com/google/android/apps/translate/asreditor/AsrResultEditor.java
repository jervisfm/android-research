package com.google.android.apps.translate.asreditor;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import com.google.android.apps.translate.Logger;
import com.google.android.apps.translate.R.layout;
import com.google.android.apps.translate.editor.SlotContainer;
import com.google.android.apps.translate.editor.SlotView;
import com.google.android.apps.translate.editor.TextSlot;
import com.google.android.apps.translate.editor.TextValue;
import java.util.List;

public class AsrResultEditor extends DragFriendlyScrollView
  implements KeyboardStateTracker.KeyboardModeListener, SlotContainer
{
  private static final String TAG = "AsrResultEditor";
  private static final int VOICE_SEARCH_KEY_CODE = 92;
  protected RecognitionManager mController;
  private TextSlot mEditorField;
  private KeyboardStateTracker mKeyboardStateTracker;
  private PopupManager mPopupManager;
  private boolean mRecognizing;

  public AsrResultEditor(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    this.mKeyboardStateTracker = new KeyboardStateTracker(paramContext);
    setFillViewport(true);
    this.mEditorField = ((TextSlot)((LayoutInflater)paramContext.getSystemService("layout_inflater")).inflate(R.layout.text_slot, this, false));
    addView(this.mEditorField);
    this.mPopupManager = new PopupManager(this);
    this.mPopupManager.addFallthroughTouchView(this);
    this.mPopupManager.addFallthroughKeyView(this);
    initEditorField(paramContext);
    this.mKeyboardStateTracker.setKeyboardModeListener(this);
  }

  private static CorrectionTextValue correctionTextValueForSlot(List<? extends CharSequence> paramList)
  {
    Logger.d("AsrResultEditor", "correctionTextValueForSlot");
    return new CorrectionTextValue((String[])paramList.toArray(new String[paramList.size()]));
  }

  private void initEditorField(Context paramContext)
  {
    Logger.d("AsrResultEditor", "initEditorField this=" + toString());
    this.mEditorField.init(paramContext, this.mPopupManager, this.mKeyboardStateTracker, this);
  }

  public boolean dispatchKeyEventPreIme(KeyEvent paramKeyEvent)
  {
    Logger.d("AsrResultEditor", "dispatchKeyEventPreIme this=" + toString());
    if (paramKeyEvent.getKeyCode() == 92)
    {
      if ((paramKeyEvent.getAction() == 0) && (!this.mRecognizing))
        startRecognizing();
      return true;
    }
    return super.dispatchKeyEventPreIme(paramKeyEvent);
  }

  public TextSlot getEditorField()
  {
    return this.mEditorField;
  }

  public String getEditorFieldValue()
  {
    return this.mEditorField.getSlotValue().getText();
  }

  public KeyboardStateTracker getKeyboardStateTracker()
  {
    return this.mKeyboardStateTracker;
  }

  public PopupManager getPopupManager()
  {
    return this.mPopupManager;
  }

  public boolean inputAsrResults(List<? extends CharSequence> paramList)
  {
    Logger.d("AsrResultEditor", "inputAsrResults this=" + toString());
    return this.mEditorField.inputSlotValue(correctionTextValueForSlot(paramList));
  }

  public void notifyKeyboardChangedState(boolean paramBoolean)
  {
    Logger.d("AsrResultEditor", "notifyKeyboardChangedState this=" + toString());
    this.mKeyboardStateTracker.setSoftKeyboardVisible(paramBoolean);
  }

  public void onKeyboardTypeChange(KeyboardStateTracker paramKeyboardStateTracker)
  {
  }

  public void onKeyboardVisibilityChange(KeyboardStateTracker paramKeyboardStateTracker)
  {
    Logger.d("AsrResultEditor", "onKeyboardVisibilityChange this=" + toString());
  }

  protected void onScrollChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super.onScrollChanged(paramInt1, paramInt2, paramInt3, paramInt4);
    Logger.d("AsrResultEditor", "onScrollChanged this=" + toString());
    this.mEditorField.onParentScrolled();
  }

  public void setAsrResults(List<? extends CharSequence> paramList)
  {
    Logger.d("AsrResultEditor", "setAsrResults this=" + toString());
    CorrectionTextValue localCorrectionTextValue = correctionTextValueForSlot(paramList);
    this.mEditorField.setSelection(this.mEditorField.length());
    this.mEditorField.setSelected(true);
    this.mEditorField.requestFocus();
    this.mEditorField.setSlotValue(localCorrectionTextValue);
  }

  public void setRecognitionManager(RecognitionManager paramRecognitionManager)
  {
    this.mController = paramRecognitionManager;
  }

  public void setRecognizing(boolean paramBoolean)
  {
    Logger.d("AsrResultEditor", "setRecognizing this=" + toString());
    this.mRecognizing = paramBoolean;
  }

  public void startRecognizing()
  {
    Logger.d("AsrResultEditor", "startRecognizing this=" + toString());
    if (this.mController != null)
      this.mController.startRecognizing();
  }

  public void stopRecognizing()
  {
    Logger.d("AsrResultEditor", "stopRecognizing this=" + toString());
    if (this.mController != null)
      this.mController.stopRecognizing();
  }

  public void toggleRecognizing(SlotView paramSlotView)
  {
    Logger.d("AsrResultEditor", "toggleRecognizing this=" + toString());
    if (this.mController != null)
      this.mController.toggleRecognizing();
  }

  public static abstract interface RecognitionManager
  {
    public abstract void startRecognizing();

    public abstract void stopRecognizing();

    public abstract void toggleRecognizing();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.asreditor.AsrResultEditor
 * JD-Core Version:    0.6.2
 */