package com.google.android.apps.translate.asreditor;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.MeasureSpec;
import android.widget.Adapter;
import android.widget.ListAdapter;
import android.widget.ListView;

public class ColumnListView extends ListView
{
  public ColumnListView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }

  protected void onMeasure(int paramInt1, int paramInt2)
  {
    super.onMeasure(paramInt1, paramInt2);
    ListAdapter localListAdapter = getAdapter();
    if (localListAdapter == null);
    int i;
    do
    {
      return;
      i = localListAdapter.getCount();
    }
    while (i == 0);
    View localView = null;
    int j = getMeasuredWidth();
    int k = View.MeasureSpec.getSize(paramInt1);
    int m = View.MeasureSpec.getMode(paramInt1);
    int i5;
    if (m != -2147483648)
    {
      localView = null;
      if (m != 0);
    }
    else
    {
      j = 0;
      i5 = 0;
      if (i5 < i)
      {
        localView = localListAdapter.getView(i5, localView, this);
        localView.measure(paramInt1, paramInt2);
        int i6 = localView.getMeasuredWidth();
        if (i6 > j)
          j = i6;
        if ((m != -2147483648) || (j != k))
          break label240;
      }
    }
    int n = getMeasuredHeight();
    int i1 = View.MeasureSpec.getSize(paramInt2);
    int i2 = View.MeasureSpec.getMode(paramInt2);
    int i3;
    if ((i2 == -2147483648) || (i2 == 0))
    {
      n = 0;
      i3 = View.MeasureSpec.makeMeasureSpec(j, 1073741824);
    }
    for (int i4 = 0; ; i4++)
      if (i4 < i)
      {
        localView = localListAdapter.getView(i4, localView, this);
        localView.measure(i3, paramInt2);
        n += localView.getMeasuredHeight();
        if ((i2 == -2147483648) && (n >= i1))
          n = i1;
      }
      else
      {
        setMeasuredDimension(j, n);
        return;
        label240: i5++;
        break;
      }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.asreditor.ColumnListView
 * JD-Core Version:    0.6.2
 */