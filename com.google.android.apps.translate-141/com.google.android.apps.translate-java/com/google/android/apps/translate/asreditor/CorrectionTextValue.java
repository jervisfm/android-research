package com.google.android.apps.translate.asreditor;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import com.google.android.apps.translate.Constants;
import com.google.android.apps.translate.editor.TextValue;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CorrectionTextValue extends TextValue
  implements Parcelable
{
  public static final Parcelable.Creator<CorrectionTextValue> CREATOR = new Parcelable.Creator()
  {
    public CorrectionTextValue createFromParcel(Parcel paramAnonymousParcel)
    {
      return new CorrectionTextValue(paramAnonymousParcel);
    }

    public CorrectionTextValue[] newArray(int paramAnonymousInt)
    {
      return new CorrectionTextValue[paramAnonymousInt];
    }
  };
  private static final String WORD_BOUNDARY_PATTERN_STRING = "(\\s+|[\\p{InHangulJamo}\\p{InHangulCompatibilityJamo}\\p{InHangulSyllables}\\p{InCjkRadicalsSupplement}\\p{InKangxiRadicals}\\p{InIdeographicDescriptionCharacters}\\p{InCjkSymbolsAndPunctuation}\\p{InEnclosedCjkLettersAndMonths}\\p{InCjkUnifiedIdeographs}\\p{InCjkUnifiedIdeographsExtensionA}\\p{InCjkUnifiedIdeographsExtensionB}\\p{InHiragana}\\p{InKatakana}])";
  private static final String WORD_PATTERN_STRING = "([\\S&&[^\\p{InHangulJamo}\\p{InHangulCompatibilityJamo}\\p{InHangulSyllables}\\p{InCjkRadicalsSupplement}\\p{InKangxiRadicals}\\p{InIdeographicDescriptionCharacters}\\p{InCjkSymbolsAndPunctuation}\\p{InEnclosedCjkLettersAndMonths}\\p{InCjkUnifiedIdeographs}\\p{InCjkUnifiedIdeographsExtensionA}\\p{InCjkUnifiedIdeographsExtensionB}\\p{InHiragana}\\p{InKatakana}]]*)";
  private static final Pattern WS_SUFFIX_PATTERN = Pattern.compile("([\\S&&[^\\p{InHangulJamo}\\p{InHangulCompatibilityJamo}\\p{InHangulSyllables}\\p{InCjkRadicalsSupplement}\\p{InKangxiRadicals}\\p{InIdeographicDescriptionCharacters}\\p{InCjkSymbolsAndPunctuation}\\p{InEnclosedCjkLettersAndMonths}\\p{InCjkUnifiedIdeographs}\\p{InCjkUnifiedIdeographsExtensionA}\\p{InCjkUnifiedIdeographsExtensionB}\\p{InHiragana}\\p{InKatakana}]]*)(\\s+|[\\p{InHangulJamo}\\p{InHangulCompatibilityJamo}\\p{InHangulSyllables}\\p{InCjkRadicalsSupplement}\\p{InKangxiRadicals}\\p{InIdeographicDescriptionCharacters}\\p{InCjkSymbolsAndPunctuation}\\p{InEnclosedCjkLettersAndMonths}\\p{InCjkUnifiedIdeographs}\\p{InCjkUnifiedIdeographsExtensionA}\\p{InCjkUnifiedIdeographsExtensionB}\\p{InHiragana}\\p{InKatakana}])", 32);
  private String[] mPhrases;

  public CorrectionTextValue(Parcel paramParcel)
  {
    super(paramParcel);
    this.mPhrases = new String[paramParcel.readInt()];
    paramParcel.readStringArray(this.mPhrases);
  }

  public CorrectionTextValue(CorrectionTextValue paramCorrectionTextValue)
  {
    this(paramCorrectionTextValue.mPhrases);
  }

  public CorrectionTextValue(String paramString)
  {
    this.mPhrases = new String[1];
    this.mPhrases[0] = paramString;
  }

  public CorrectionTextValue(String[] paramArrayOfString)
  {
    this.mPhrases = paramArrayOfString;
  }

  public List<String> getAlternates(CharSequence paramCharSequence, int paramInt1, int paramInt2, boolean paramBoolean)
  {
    String str1 = paramCharSequence.subSequence(0, paramInt1).toString().toLowerCase();
    Object localObject = paramCharSequence.subSequence(paramInt2, paramCharSequence.length()).toString().toLowerCase();
    ArrayList localArrayList;
    int j;
    label95: String str2;
    String str3;
    int m;
    if (!TextUtils.isEmpty((CharSequence)localObject))
    {
      String str5 = ((String)localObject).substring(0, 1);
      if (Constants.CJK_PATTERN.matcher(str5).matches())
        localObject = str5;
    }
    else
    {
      localArrayList = new ArrayList();
      String[] arrayOfString = this.mPhrases;
      int i = arrayOfString.length;
      j = 0;
      if (j >= i)
        break label322;
      str2 = arrayOfString[j];
      str3 = str2.toLowerCase();
      int k = str3.indexOf(str1);
      if (k >= 0)
      {
        m = k + str1.length();
        if (((String)localObject).trim().length() != 0)
          break label306;
      }
    }
    label306: for (int n = str3.length(); ; n = str3.indexOf((String)localObject, m + 1))
    {
      if (n >= 0)
      {
        String str4 = str2.substring(m, n);
        if ((paramBoolean) && (str4.length() > 0))
          str4 = str4.substring(0, 1).toUpperCase() + str4.substring(1);
        if ((!localArrayList.contains(str4)) && (!TextUtils.isEmpty(str4.trim())))
          localArrayList.add(str4);
      }
      j++;
      break label95;
      Matcher localMatcher = WS_SUFFIX_PATTERN.matcher(((String)localObject).substring(1, ((String)localObject).length()));
      if (!localMatcher.find())
        break;
      localObject = ((String)localObject).substring(0, 1 + localMatcher.start(2));
      break;
    }
    label322: localArrayList.remove(paramCharSequence.subSequence(paramInt1, paramInt2).toString());
    return localArrayList;
  }

  public String getText()
  {
    return this.mPhrases[0];
  }

  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    super.writeToParcel(paramParcel, paramInt);
    paramParcel.writeInt(this.mPhrases.length);
    paramParcel.writeStringArray(this.mPhrases);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.asreditor.CorrectionTextValue
 * JD-Core Version:    0.6.2
 */