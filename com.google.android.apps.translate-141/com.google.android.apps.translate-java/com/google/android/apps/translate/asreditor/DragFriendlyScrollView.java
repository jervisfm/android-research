package com.google.android.apps.translate.asreditor;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ScrollView;
import com.google.android.apps.translate.Logger;

public class DragFriendlyScrollView extends ScrollView
{
  private static final float MAX_SCROLL_ANGLE = 0.3490658F;
  private static final float MAX_SELECT_ANGLE = 1.047198F;
  private static final float MIN_MOVE_DISTANCE = 4900.0F;
  private static final float MIN_SCROLL_SPEED = 21025.0F;
  private static final int SCROLL_MODE_NON_SCROLLING = 2;
  private static final int SCROLL_MODE_SCROLLING = 1;
  private static final int SCROLL_MODE_UNDETECTED = 0;
  private static final String TAG = "DragFriendlyScrollView";
  private float mDragStartX;
  private float mDragStartY;
  private long mEventStartTime;
  private int mScrollMode;

  public DragFriendlyScrollView(Context paramContext)
  {
    this(paramContext, null);
  }

  public DragFriendlyScrollView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }

  private void updateScrollMode(MotionEvent paramMotionEvent)
  {
    if (this.mScrollMode != 0);
    float f3;
    float f6;
    do
    {
      return;
      float f1 = Math.abs(this.mDragStartX - paramMotionEvent.getX());
      float f2 = Math.abs(this.mDragStartY - paramMotionEvent.getY());
      f3 = f1 * f1 + f2 * f2;
      float f4 = f3 / ((float)(paramMotionEvent.getEventTime() - this.mEventStartTime) / 1000.0F);
      float f5 = (float)Math.atan(f1 / f2);
      f6 = 1.570796F - f5;
      if ((f5 < 0.3490658F) && (f3 > 4900.0F) && (f4 > 21025.0F))
      {
        Logger.d("DragFriendlyScrollView", "touch move  - scrolling");
        this.mScrollMode = 1;
        return;
      }
    }
    while ((f6 >= 1.047198F) || (f3 <= 4900.0F));
    Logger.d("DragFriendlyScrollView", "touch move  - selecting");
    this.mScrollMode = 2;
  }

  public boolean onInterceptTouchEvent(MotionEvent paramMotionEvent)
  {
    boolean bool = super.onInterceptTouchEvent(paramMotionEvent);
    Logger.d("DragFriendlyScrollView", "onInterceptTouchEvent " + paramMotionEvent.getAction() + " super " + bool);
    if (paramMotionEvent.getAction() == 0)
    {
      this.mDragStartX = paramMotionEvent.getX();
      this.mDragStartY = paramMotionEvent.getY();
      this.mEventStartTime = paramMotionEvent.getEventTime();
      this.mScrollMode = 0;
      return false;
    }
    if (paramMotionEvent.getAction() == 2)
    {
      updateScrollMode(paramMotionEvent);
      if (this.mScrollMode != 0)
      {
        if (this.mScrollMode == 1)
        {
          Logger.d("DragFriendlyScrollView", "onInterceptTouchEvent scroll mode");
          return bool;
        }
        Logger.d("DragFriendlyScrollView", "onInterceptTouchEvent select mode");
        return false;
      }
    }
    return false;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.asreditor.DragFriendlyScrollView
 * JD-Core Version:    0.6.2
 */