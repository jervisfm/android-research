package com.google.android.apps.translate.asreditor;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.SpeechRecognizer;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.LinearLayout;
import com.google.android.apps.translate.Language;
import com.google.android.apps.translate.Logger;
import com.google.android.apps.translate.R.drawable;
import com.google.android.apps.translate.R.id;
import com.google.android.apps.translate.R.layout;
import com.google.android.apps.translate.R.string;
import com.google.android.apps.translate.R.style;
import com.google.android.apps.translate.TranslateApplication;
import com.google.android.apps.translate.Util;
import com.google.android.apps.translate.VoiceInputHelper;
import com.google.android.apps.translate.editor.SlotView;
import java.util.ArrayList;
import java.util.List;

public class EditorDialog extends Dialog
  implements AsrResultEditor.RecognitionManager, RecognitionListener
{
  private static final int MAX_ASR_RESULTS = 50;
  private static final int MODE_ERROR = 4;
  private static final int MODE_INITIALIZING = 1;
  private static final int MODE_NOT_RECOGNIZING = 0;
  private static final int MODE_RECOGNIZING = 2;
  private static final int MODE_WORKING = 3;
  private static final String TAG = "EditorDialog";
  private Button mCancelButton;
  private Context mContext;
  private AsrResultEditor mEditor;
  private Button mGoButton;
  private final InputMethodManager mInputMethodManager;
  private SlotView mInputSlot;
  private boolean mKeyboardShown;
  private Listener mListener;
  private LinearLayout mMainView;
  private int mRecognitionError;
  private int mRecognitionMode;
  private List<String> mRecognitionResults;
  private RecordingPopup mRecordingPopup;
  private Language mSourceLanguage;
  private SpeechRecognizer mSpeechRecognizer;
  private Vibrator mVibrator;
  private VoiceInputHelper mVoiceInputHelper;

  public EditorDialog(Activity paramActivity)
  {
    super(paramActivity, R.style.VoiceSearchDialogTheme);
    this.mInputMethodManager = ((InputMethodManager)paramActivity.getSystemService("input_method"));
    this.mVoiceInputHelper = ((TranslateApplication)paramActivity.getApplication()).getVoiceInputHelper();
    this.mContext = paramActivity;
  }

  private void changeRecognitionMode(int paramInt)
  {
    this.mRecognitionMode = paramInt;
    switch (paramInt)
    {
    default:
      return;
    case 0:
      if (this.mRecordingPopup != null)
      {
        this.mRecordingPopup.dismiss();
        this.mRecordingPopup = null;
      }
      this.mMainView.setBackgroundResource(R.drawable.vs_dialog_green);
      this.mEditor.setRecognizing(false);
      return;
    case 1:
      this.mRecordingPopup = new RecordingPopup(getContext(), this.mSourceLanguage);
      this.mRecordingPopup.setListener(new RecordingPopup.Listener()
      {
        public void onCancel(RecordingPopup paramAnonymousRecordingPopup)
        {
          EditorDialog.this.stopRecognizing();
        }
      });
      this.mRecordingPopup.showRecording();
      return;
    case 2:
      this.mMainView.setBackgroundResource(R.drawable.vs_dialog_red);
      this.mEditor.setRecognizing(true);
      if (this.mRecordingPopup != null)
        this.mRecordingPopup.showRecording();
      hideKeyboardAndShowPopup();
      return;
    case 3:
      this.mMainView.setBackgroundResource(R.drawable.vs_dialog_blue);
      if (this.mRecordingPopup != null)
        this.mRecordingPopup.showWorking();
      this.mEditor.setRecognizing(true);
      return;
    case 4:
    }
    this.mMainView.setBackgroundResource(R.drawable.vs_dialog_yellow);
    if (this.mRecordingPopup != null)
      this.mRecordingPopup.showError(getErrorMessageId(this.mRecognitionError));
    hideKeyboardAndShowPopup();
  }

  private static int getErrorMessageId(int paramInt)
  {
    switch (paramInt)
    {
    case 5:
    default:
      Logger.d("EditorDialog", "other errors:" + paramInt);
      return R.string.error;
    case 1:
    case 2:
      return R.string.network_error;
    case 3:
      return R.string.audio_error;
    case 4:
      return R.string.server_error;
    case 6:
      return R.string.speech_timeout;
    case 7:
      return R.string.no_match;
    case 8:
    }
    return R.string.recognizer_busy_error;
  }

  private Point getRecordingPopupPosition()
  {
    int i = this.mInputSlot.getSelectionStart();
    int j = this.mInputSlot.getSelectionEnd();
    if ((i < 0) || (j < 0))
    {
      i = this.mInputSlot.length();
      j = this.mInputSlot.length();
    }
    return ArrowPopup.getTextSelectionPopupPoint(this.mInputSlot, i, j);
  }

  private void hideKeyboard()
  {
    if (this.mMainView != null)
    {
      Logger.d("EditorDialog", "hideKeyboard");
      this.mInputMethodManager.hideSoftInputFromWindow(this.mMainView.getWindowToken(), 0);
    }
  }

  private void hideKeyboardAndShowPopup()
  {
    if (this.mKeyboardShown)
      hideKeyboard();
    while (this.mRecordingPopup == null)
      return;
    Point localPoint = getRecordingPopupPosition();
    this.mEditor.getPopupManager().showPopup(this.mRecordingPopup, localPoint);
  }

  private void layoutChanged()
  {
    Point localPoint;
    if (this.mRecordingPopup != null)
    {
      localPoint = getRecordingPopupPosition();
      if (!this.mRecordingPopup.isShown())
        break label56;
      this.mRecordingPopup.move(localPoint);
    }
    while (true)
    {
      if ((this.mInputSlot != null) && (!this.mInputSlot.isFocused()))
        this.mInputSlot.requestFocus();
      return;
      label56: this.mEditor.getPopupManager().showPopup(this.mRecordingPopup, localPoint);
    }
  }

  private void renderLocalizedUi(Context paramContext)
  {
    String str1 = paramContext.getString(Util.getLocalizedStringId(paramContext, R.string.label_ok, this.mSourceLanguage));
    this.mGoButton.setText(str1);
    String str2 = paramContext.getString(Util.getLocalizedStringId(paramContext, R.string.label_cancel, this.mSourceLanguage));
    this.mCancelButton.setText(str2);
  }

  public void dismiss()
  {
    hideKeyboard();
    super.dismiss();
  }

  public void onBeginningOfSpeech()
  {
  }

  public void onBufferReceived(byte[] paramArrayOfByte)
  {
  }

  protected void onCreate(Bundle paramBundle)
  {
    setContentView(R.layout.editor_dialog);
    this.mVibrator = new Vibrator(getContext());
    this.mMainView = ((LinearLayout)findViewById(R.id.editor_dialog));
    WindowManager.LayoutParams localLayoutParams = getWindow().getAttributes();
    localLayoutParams.softInputMode = 2;
    getWindow().setAttributes(localLayoutParams);
    this.mEditor = ((AsrResultEditor)findViewById(R.id.slot_editor));
    this.mEditor.setRecognitionManager(this);
    this.mInputSlot = ((SlotView)findViewById(R.id.editor_field));
    this.mGoButton = ((Button)findViewById(R.id.editor_go_button));
    this.mGoButton.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        EditorDialog.this.mListener.onGo(EditorDialog.this.mEditor.getEditorFieldValue());
      }
    });
    this.mCancelButton = ((Button)findViewById(R.id.editor_cancel_button));
    this.mCancelButton.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        if (EditorDialog.this.mListener != null)
          EditorDialog.this.mListener.onCancel();
      }
    });
    this.mEditor.setAsrResults(this.mRecognitionResults);
    renderLocalizedUi(this.mContext);
    changeRecognitionMode(0);
  }

  public void onEndOfSpeech()
  {
    changeRecognitionMode(3);
  }

  public void onError(int paramInt)
  {
    this.mRecognitionError = paramInt;
    this.mVibrator.vibrate(this.mMainView);
    changeRecognitionMode(4);
  }

  public void onEvent(int paramInt, Bundle paramBundle)
  {
  }

  public void onPartialResults(Bundle paramBundle)
  {
  }

  public void onPause()
  {
    hideKeyboard();
  }

  public void onReadyForSpeech(Bundle paramBundle)
  {
    changeRecognitionMode(2);
  }

  public void onResults(Bundle paramBundle)
  {
    ArrayList localArrayList = paramBundle.getStringArrayList("results_recognition");
    boolean bool = this.mEditor.inputAsrResults(localArrayList);
    this.mVibrator.vibrate(this.mMainView);
    if (!bool)
    {
      this.mRecognitionError = 7;
      changeRecognitionMode(4);
      return;
    }
    changeRecognitionMode(0);
  }

  public void onRmsChanged(float paramFloat)
  {
    if (this.mRecordingPopup != null)
      this.mRecordingPopup.setLevel(paramFloat);
  }

  protected void onStart()
  {
    this.mSpeechRecognizer = SpeechRecognizer.createSpeechRecognizer(getContext());
    this.mSpeechRecognizer.setRecognitionListener(this);
    super.onStart();
  }

  protected void onStop()
  {
    this.mSpeechRecognizer.destroy();
    super.onStop();
  }

  public void setAsrResults(List<String> paramList, Language paramLanguage)
  {
    this.mRecognitionResults = paramList;
    this.mSourceLanguage = paramLanguage;
  }

  public void setListener(Listener paramListener)
  {
    this.mListener = paramListener;
  }

  public void startRecognizing()
  {
    Intent localIntent = new Intent("android.speech.action.RECOGNIZE_SPEECH");
    VoiceInputHelper.prepareAsrIntent(localIntent, this.mVoiceInputHelper.getAsrLocale(this.mContext, this.mSourceLanguage));
    localIntent.putExtra("android.speech.extra.MAX_RESULTS", 50);
    this.mSpeechRecognizer.startListening(localIntent);
    changeRecognitionMode(1);
  }

  public void stopRecognizing()
  {
    if (this.mRecognitionMode != 0)
    {
      this.mSpeechRecognizer.cancel();
      changeRecognitionMode(0);
    }
  }

  public void toggleRecognizing()
  {
    if (this.mRecognitionMode == 0)
    {
      startRecognizing();
      return;
    }
    stopRecognizing();
    startRecognizing();
  }

  public static abstract interface Listener
  {
    public abstract void onCancel();

    public abstract void onGo(String paramString);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.asreditor.EditorDialog
 * JD-Core Version:    0.6.2
 */