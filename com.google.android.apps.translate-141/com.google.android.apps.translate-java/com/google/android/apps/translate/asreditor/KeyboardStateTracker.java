package com.google.android.apps.translate.asreditor;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;

public class KeyboardStateTracker
  implements Handler.Callback
{
  private static final int SOFT_KEYBOARD_HIDDEN = 2;
  private static final int SOFT_KEYBOARD_VISIBLE = 1;
  private Handler mHandler = new Handler(this);
  private boolean mHardKeyboardVisible = false;
  private KeyboardModeListener mListener;
  private boolean mSoftKeyboardTransition = false;
  private boolean mSoftKeyboardVisible = false;

  public KeyboardStateTracker(Context paramContext)
  {
    Configuration localConfiguration = paramContext.getResources().getConfiguration();
    int j;
    if (localConfiguration.hardKeyboardHidden == i)
    {
      j = i;
      this.mHardKeyboardVisible = j;
      if ((this.mHardKeyboardVisible) || (localConfiguration.keyboard != i))
        break label85;
    }
    while (true)
    {
      this.mSoftKeyboardVisible = i;
      return;
      j = 0;
      break;
      label85: i = 0;
    }
  }

  private void notifyTypeChange()
  {
    this.mSoftKeyboardTransition = false;
    if (this.mListener != null)
      this.mListener.onKeyboardTypeChange(this);
  }

  private void notifyVisibilityChange()
  {
    this.mSoftKeyboardTransition = false;
    if (this.mListener != null)
      this.mListener.onKeyboardVisibilityChange(this);
  }

  public boolean handleMessage(Message paramMessage)
  {
    boolean bool = true;
    switch (paramMessage.what)
    {
    default:
      bool = false;
    case 1:
    case 2:
    }
    do
    {
      do
        return bool;
      while ((!this.mSoftKeyboardTransition) || (!this.mSoftKeyboardVisible));
      notifyVisibilityChange();
      return bool;
    }
    while ((!this.mSoftKeyboardTransition) || (this.mSoftKeyboardVisible));
    notifyVisibilityChange();
    return bool;
  }

  public boolean isHardKeyboardVisible()
  {
    return this.mHardKeyboardVisible;
  }

  public boolean isInKeyboardMode()
  {
    return (isSoftKeyboardVisible()) || (isHardKeyboardVisible());
  }

  public boolean isSoftKeyboardVisible()
  {
    return this.mSoftKeyboardVisible;
  }

  public void setHardKeyboardVisible(boolean paramBoolean)
  {
    if (this.mHardKeyboardVisible != paramBoolean)
    {
      this.mHardKeyboardVisible = paramBoolean;
      if ((this.mSoftKeyboardTransition) && (this.mSoftKeyboardVisible != this.mHardKeyboardVisible))
        notifyTypeChange();
    }
    else
    {
      return;
    }
    notifyVisibilityChange();
  }

  public void setKeyboardModeListener(KeyboardModeListener paramKeyboardModeListener)
  {
    this.mListener = paramKeyboardModeListener;
  }

  public void setSoftKeyboardVisible(boolean paramBoolean)
  {
    int i = 1;
    Handler localHandler;
    if (this.mSoftKeyboardVisible != paramBoolean)
    {
      this.mSoftKeyboardVisible = paramBoolean;
      this.mSoftKeyboardTransition = i;
      localHandler = this.mHandler;
      if (!paramBoolean)
        break label36;
    }
    while (true)
    {
      localHandler.sendEmptyMessage(i);
      return;
      label36: i = 2;
    }
  }

  public static abstract interface KeyboardModeListener
  {
    public abstract void onKeyboardTypeChange(KeyboardStateTracker paramKeyboardStateTracker);

    public abstract void onKeyboardVisibilityChange(KeyboardStateTracker paramKeyboardStateTracker);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.asreditor.KeyboardStateTracker
 * JD-Core Version:    0.6.2
 */