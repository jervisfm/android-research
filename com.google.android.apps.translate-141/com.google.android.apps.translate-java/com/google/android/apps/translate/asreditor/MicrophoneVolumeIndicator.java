package com.google.android.apps.translate.asreditor;

public abstract interface MicrophoneVolumeIndicator
{
  public abstract void setRmsdB(float paramFloat);

  public abstract void start();

  public abstract void stop();
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.asreditor.MicrophoneVolumeIndicator
 * JD-Core Version:    0.6.2
 */