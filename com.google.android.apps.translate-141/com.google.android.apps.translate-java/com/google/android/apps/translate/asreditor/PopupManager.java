package com.google.android.apps.translate.asreditor;

import android.graphics.Point;
import android.graphics.Rect;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class PopupManager
  implements ArrowPopup.OnOutsideEventListener
{
  private ArrowPopup mLastShownPopup;
  private List<View> mOutsideKeyViews = new ArrayList();
  private List<View> mOutsideTouchViews = new ArrayList();
  private View mView;

  public PopupManager(View paramView)
  {
    this.mView = paramView;
  }

  private boolean forwardOutsideEvent(MotionEvent paramMotionEvent, View paramView)
  {
    MotionEvent localMotionEvent = MotionEvent.obtain(paramMotionEvent);
    int[] arrayOfInt = new int[2];
    paramView.getLocationOnScreen(arrayOfInt);
    localMotionEvent.offsetLocation(-arrayOfInt[0], -arrayOfInt[1]);
    boolean bool1 = new Rect(0, 0, paramView.getWidth(), paramView.getHeight()).contains((int)paramMotionEvent.getX(), (int)paramMotionEvent.getY());
    boolean bool2 = false;
    if (bool1)
      bool2 = paramView.dispatchTouchEvent(localMotionEvent);
    localMotionEvent.recycle();
    return bool2;
  }

  private void showPopupWithoutSetup(ArrowPopup paramArrowPopup, Point paramPoint)
  {
    if (this.mLastShownPopup != paramArrowPopup)
      dismissCurrentPopup();
    this.mLastShownPopup = paramArrowPopup;
    paramArrowPopup.showAt(paramPoint);
  }

  public void addFallthroughKeyView(View paramView)
  {
    this.mOutsideKeyViews.add(paramView);
  }

  public void addFallthroughTouchView(View paramView)
  {
    this.mOutsideTouchViews.add(paramView);
  }

  public void dismissCurrentPopup()
  {
    if (this.mLastShownPopup != null)
    {
      this.mLastShownPopup.dismiss();
      this.mLastShownPopup = null;
    }
  }

  public boolean isPopupShowing()
  {
    return (this.mLastShownPopup != null) && (this.mLastShownPopup.isShown());
  }

  public void onOutsideTouch(ArrowPopup paramArrowPopup, MotionEvent paramMotionEvent)
  {
    Iterator localIterator = this.mOutsideTouchViews.iterator();
    while ((localIterator.hasNext()) && (!forwardOutsideEvent(paramMotionEvent, (View)localIterator.next())));
  }

  public void onUnhandledKeyEvent(ArrowPopup paramArrowPopup, KeyEvent paramKeyEvent)
  {
    Iterator localIterator = this.mOutsideKeyViews.iterator();
    while ((localIterator.hasNext()) && (!((View)localIterator.next()).dispatchKeyEvent(paramKeyEvent)));
  }

  public void removeFallthroughKeyView(View paramView)
  {
    this.mOutsideKeyViews.remove(paramView);
  }

  public void removeFallthroughTouchView(View paramView)
  {
    this.mOutsideTouchViews.remove(paramView);
  }

  public void setupPopup(ArrowPopup paramArrowPopup)
  {
    paramArrowPopup.setOnOutsideTouchListener(this);
    paramArrowPopup.setWindowToken(this.mView.getWindowToken());
  }

  public void showPopup(ArrowPopup paramArrowPopup, Point paramPoint)
  {
    setupPopup(paramArrowPopup);
    showPopupWithoutSetup(paramArrowPopup, paramPoint);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.asreditor.PopupManager
 * JD-Core Version:    0.6.2
 */