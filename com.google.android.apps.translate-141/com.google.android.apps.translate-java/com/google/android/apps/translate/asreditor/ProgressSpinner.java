package com.google.android.apps.translate.asreditor;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.ColorFilter;
import android.graphics.drawable.AnimationDrawable;
import android.os.Handler;
import android.util.AttributeSet;
import android.widget.ImageView;
import com.google.android.apps.translate.R.drawable;

public class ProgressSpinner extends ImageView
{
  private static final int ANIMATION_START_DELAY = 100;
  private AnimationDrawable mBackground;
  private ColorFilter mColorFilter;
  private Handler mHandler = new Handler();
  private final Runnable mStartAnimation = new Runnable()
  {
    public void run()
    {
      ProgressSpinner.this.mBackground.start();
    }
  };
  private final Runnable mStopAnimation = new Runnable()
  {
    public void run()
    {
      ProgressSpinner.this.mBackground.stop();
    }
  };

  public ProgressSpinner(Context paramContext)
  {
    super(paramContext);
    init();
  }

  public ProgressSpinner(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    init();
  }

  public ProgressSpinner(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    init();
  }

  private void init()
  {
    this.mBackground = ((AnimationDrawable)getContext().getResources().getDrawable(R.drawable.spinner));
    setBackgroundDrawable(this.mBackground);
    if (this.mColorFilter != null)
      this.mBackground.setColorFilter(this.mColorFilter);
  }

  protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
    if (paramInt1 > 0)
    {
      this.mHandler.postDelayed(this.mStartAnimation, 100L);
      return;
    }
    this.mHandler.post(this.mStopAnimation);
  }

  public void setColorFilter(ColorFilter paramColorFilter)
  {
    super.setColorFilter(paramColorFilter);
    this.mColorFilter = paramColorFilter;
    if (this.mBackground != null)
      this.mBackground.setColorFilter(paramColorFilter);
  }

  public void setVisibility(int paramInt)
  {
    super.setVisibility(paramInt);
    if (paramInt == 0)
    {
      this.mHandler.postDelayed(this.mStartAnimation, 100L);
      return;
    }
    this.mHandler.post(this.mStopAnimation);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.asreditor.ProgressSpinner
 * JD-Core Version:    0.6.2
 */