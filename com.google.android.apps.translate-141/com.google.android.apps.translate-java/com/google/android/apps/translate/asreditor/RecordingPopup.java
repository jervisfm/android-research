package com.google.android.apps.translate.asreditor;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.google.android.apps.translate.Language;
import com.google.android.apps.translate.R.dimen;
import com.google.android.apps.translate.R.id;
import com.google.android.apps.translate.R.layout;
import com.google.android.apps.translate.R.string;
import com.google.android.apps.translate.Util;

public class RecordingPopup extends ArrowPopup
{
  private Button mCancel;
  private Context mContext;
  private ImageView mImage;
  private SoundIndicator mIndicator;
  private final Language mLanguage;
  private Listener mListener;
  private ProgressSpinner mSpinner;
  private TextView mText;

  public RecordingPopup(Context paramContext, AttributeSet paramAttributeSet, Language paramLanguage)
  {
    super(paramContext, paramAttributeSet);
    this.mContext = paramContext;
    this.mLanguage = paramLanguage;
    ((LayoutInflater)paramContext.getSystemService("layout_inflater")).inflate(R.layout.recording_popup, this, true);
    this.mText = ((TextView)findViewById(R.id.recording_popup_text));
    this.mSpinner = ((ProgressSpinner)findViewById(R.id.recording_popup_progress));
    this.mIndicator = ((SoundIndicator)findViewById(R.id.recording_popup_indicator));
    this.mImage = ((ImageView)findViewById(R.id.recording_popup_image));
    this.mCancel = ((Button)findViewById(R.id.recording_popup_cancel));
    this.mCancel.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        if (RecordingPopup.this.mListener != null)
        {
          RecordingPopup.this.mListener.onCancel(RecordingPopup.this);
          RecordingPopup.this.dismiss();
        }
      }
    });
    setOutsideTouchable(false);
  }

  public RecordingPopup(Context paramContext, Language paramLanguage)
  {
    this(paramContext, null, paramLanguage);
  }

  protected WindowManager.LayoutParams createLayoutParams(Rect paramRect)
  {
    WindowManager.LayoutParams localLayoutParams = super.createLayoutParams(paramRect);
    int i = (int)getContext().getResources().getDimension(R.dimen.recording_popup_size);
    if (localLayoutParams.height < i)
    {
      disableArrow();
      localLayoutParams.gravity = 17;
      localLayoutParams.width = i;
      localLayoutParams.height = i;
      localLayoutParams.x = 0;
      localLayoutParams.y = 0;
    }
    return localLayoutParams;
  }

  public boolean dispatchKeyEvent(KeyEvent paramKeyEvent)
  {
    if ((paramKeyEvent.getKeyCode() == 4) && (this.mListener != null))
      this.mListener.onCancel(this);
    return super.dispatchKeyEvent(paramKeyEvent);
  }

  public void setLevel(float paramFloat)
  {
    this.mIndicator.setRmsdB(paramFloat);
  }

  public void setListener(Listener paramListener)
  {
    this.mListener = paramListener;
  }

  public void showError(int paramInt)
  {
    this.mText.setText(Util.getLocalizedStringId(this.mContext, paramInt, this.mLanguage));
    this.mCancel.setText(Util.getLocalizedStringId(this.mContext, R.string.label_ok, this.mLanguage));
    this.mImage.setVisibility(0);
    this.mSpinner.setVisibility(8);
    this.mIndicator.setVisibility(8);
    this.mIndicator.stop();
  }

  public void showRecording()
  {
    this.mText.setText(Util.getLocalizedStringId(this.mContext, R.string.label_speak_now, this.mLanguage));
    this.mCancel.setText(Util.getLocalizedStringId(this.mContext, R.string.label_cancel, this.mLanguage));
    this.mImage.setVisibility(8);
    this.mSpinner.setVisibility(8);
    this.mIndicator.setVisibility(0);
    this.mIndicator.start();
  }

  public void showWorking()
  {
    this.mText.setText(Util.getLocalizedStringId(this.mContext, R.string.working, this.mLanguage));
    this.mCancel.setText(Util.getLocalizedStringId(this.mContext, R.string.label_cancel, this.mLanguage));
    this.mImage.setVisibility(8);
    this.mSpinner.setVisibility(0);
    this.mIndicator.setVisibility(8);
    this.mIndicator.stop();
  }

  public static abstract interface Listener
  {
    public abstract void onCancel(RecordingPopup paramRecordingPopup);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.asreditor.RecordingPopup
 * JD-Core Version:    0.6.2
 */