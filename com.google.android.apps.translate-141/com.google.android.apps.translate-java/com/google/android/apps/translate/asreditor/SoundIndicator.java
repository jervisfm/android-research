package com.google.android.apps.translate.asreditor;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.util.AttributeSet;
import android.widget.ImageView;
import com.google.android.apps.translate.R.drawable;

public class SoundIndicator extends ImageView
  implements MicrophoneVolumeIndicator
{
  private static final float AUDIO_METER_DB_RANGE = 20.0F;
  private static final float AUDIO_METER_MIN_DB = 7.0F;
  private static final float DOWN_SMOOTHING_FACTOR = 0.4F;
  private static final long FRAME_DELAY = 50L;
  private static final float UP_SMOOTHING_FACTOR = 0.9F;
  private Canvas mBufferCanvas;
  private Paint mClearPaint;
  private Runnable mDrawFrame = new Runnable()
  {
    public void run()
    {
      SoundIndicator.this.invalidate();
      SoundIndicator.this.mHandler.postDelayed(SoundIndicator.this.mDrawFrame, 50L);
    }
  };
  private Bitmap mDrawingBuffer;
  private Bitmap mEdgeBitmap;
  private int mEdgeBitmapOffset;
  private Drawable mFrontDrawable = getDrawable();
  private Handler mHandler;
  private float mLevel = 0.0F;
  private Paint mMultPaint;

  public SoundIndicator(Context paramContext)
  {
    this(paramContext, null);
  }

  public SoundIndicator(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    this.mEdgeBitmap = ((BitmapDrawable)paramContext.getResources().getDrawable(R.drawable.vs_popup_mic_edge)).getBitmap();
    this.mEdgeBitmapOffset = (this.mEdgeBitmap.getHeight() / 2);
    this.mDrawingBuffer = Bitmap.createBitmap(this.mFrontDrawable.getIntrinsicWidth(), this.mFrontDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
    this.mBufferCanvas = new Canvas(this.mDrawingBuffer);
    this.mClearPaint = new Paint();
    this.mClearPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
    this.mMultPaint = new Paint();
    this.mMultPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.MULTIPLY));
    this.mHandler = new Handler();
  }

  public void onDraw(Canvas paramCanvas)
  {
    float f1 = getWidth();
    float f2 = getHeight();
    this.mBufferCanvas.drawRect(0.0F, 0.0F, f1, f2, this.mClearPaint);
    Rect localRect = new Rect(0, (int)((1.0D - this.mLevel) * (f2 + this.mEdgeBitmapOffset)) - this.mEdgeBitmapOffset, (int)f1, (int)f2);
    this.mBufferCanvas.save();
    this.mBufferCanvas.clipRect(localRect);
    this.mFrontDrawable.setBounds(new Rect(0, 0, (int)f1, (int)f2));
    this.mFrontDrawable.draw(this.mBufferCanvas);
    this.mBufferCanvas.restore();
    this.mBufferCanvas.drawBitmap(this.mEdgeBitmap, 0.0F, localRect.top, this.mMultPaint);
    paramCanvas.drawBitmap(this.mDrawingBuffer, 0.0F, 0.0F, null);
  }

  public void setRmsdB(float paramFloat)
  {
    float f = Math.min(Math.max(0.0F, (paramFloat - 7.0F) / 20.0F), 1.0F);
    if (f > this.mLevel)
    {
      this.mLevel = (0.9F * (f - this.mLevel) + this.mLevel);
      return;
    }
    this.mLevel = (0.4F * (f - this.mLevel) + this.mLevel);
  }

  public void start()
  {
    this.mHandler.post(this.mDrawFrame);
  }

  public void stop()
  {
    this.mHandler.removeCallbacks(this.mDrawFrame);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.asreditor.SoundIndicator
 * JD-Core Version:    0.6.2
 */