package com.google.android.apps.translate.asreditor;

import android.content.Context;
import android.view.View;

public class Vibrator
{
  private android.os.Vibrator mVibrator;
  private Runnable mVibratorRunnable = new Runnable()
  {
    public void run()
    {
      Vibrator.this.mVibrator.vibrate(new long[] { 0L, 16L, 32L, 32L }, -1);
    }
  };

  public Vibrator(Context paramContext)
  {
    this.mVibrator = ((android.os.Vibrator)paramContext.getSystemService("vibrator"));
  }

  public void vibrate(View paramView)
  {
    if (this.mVibrator != null)
      paramView.post(this.mVibratorRunnable);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.asreditor.Vibrator
 * JD-Core Version:    0.6.2
 */