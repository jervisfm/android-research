package com.google.android.apps.translate.conversation;

import android.app.ListActivity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import com.google.android.apps.translate.Language;
import com.google.android.apps.translate.Logger;
import com.google.android.apps.translate.R.id;
import com.google.android.apps.translate.R.layout;
import com.google.android.apps.translate.Util;

public class ConversationActivity extends ListActivity
{
  private static final String TAG = "ConversationFragment";
  private ConversationHelper mConversationHelper = new ConversationHelper();
  private BroadcastReceiver mOnLanguagesChanged;

  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    this.mConversationHelper.onActivityResult(paramInt1, paramInt2, paramIntent);
  }

  public void onConfigurationChanged(Configuration paramConfiguration)
  {
    super.onConfigurationChanged(paramConfiguration);
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    this.mConversationHelper.onCreate(this);
    View localView = getLayoutInflater().inflate(R.layout.conversation_activity, null);
    this.mConversationHelper.init((ConversationPanel)localView.findViewById(R.id.conversation_panel), (Language)getIntent().getSerializableExtra("key_language_from"), (Language)getIntent().getSerializableExtra("key_language_to"), getIntent().getStringExtra("key_current_translation"));
    setContentView(localView);
    setListAdapter(this.mConversationHelper.getConversationAdapter());
    this.mConversationHelper.setListView(getListView());
    this.mOnLanguagesChanged = new BroadcastReceiver()
    {
      public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
      {
        ConversationActivity.this.mConversationHelper.refreshLanguageList();
      }
    };
    registerReceiver(this.mOnLanguagesChanged, new IntentFilter("com.google.android.apps.translate.broadcast.LANGUAGES_CHANGED"));
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    Logger.d("ConversationFragment", "onCreateOptionsMenu");
    this.mConversationHelper.onCreateOptionsMenu(paramMenu, getMenuInflater());
    return super.onCreateOptionsMenu(paramMenu);
  }

  public void onDestroy()
  {
    this.mConversationHelper.onDestroy();
    if (this.mOnLanguagesChanged != null)
      unregisterReceiver(this.mOnLanguagesChanged);
    super.onDestroy();
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    if (this.mConversationHelper.onOptionsItemSelected(paramMenuItem))
      return true;
    if (paramMenuItem.getItemId() == 16908332)
    {
      Util.openHomeActivity(this);
      return true;
    }
    return super.onOptionsItemSelected(paramMenuItem);
  }

  protected void onPause()
  {
    super.onPause();
    this.mConversationHelper.onPause();
  }

  public boolean onPrepareOptionsMenu(Menu paramMenu)
  {
    if (this.mConversationHelper.onPrepareOptionsMenu(paramMenu))
      return true;
    return super.onPrepareOptionsMenu(paramMenu);
  }

  protected void onResume()
  {
    super.onResume();
    this.mConversationHelper.onResume();
  }

  protected void onStart()
  {
    super.onStart();
    this.mConversationHelper.onStart();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.conversation.ConversationActivity
 * JD-Core Version:    0.6.2
 */