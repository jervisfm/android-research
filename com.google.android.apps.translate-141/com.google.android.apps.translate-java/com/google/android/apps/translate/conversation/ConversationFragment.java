package com.google.android.apps.translate.conversation;

import android.app.Activity;
import android.app.ListFragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.apps.translate.Language;
import com.google.android.apps.translate.Logger;
import com.google.android.apps.translate.R.layout;
import com.google.android.apps.translate.SdkVersionWrapper;
import com.google.android.apps.translate.SdkVersionWrapper.WrapperBase;
import com.google.android.apps.translate.editor.PreImeAutoCompleteTextView.OnKeyPreImeListener;
import com.google.android.apps.translate.translation.TranslateEntry;

public class ConversationFragment extends ListFragment
  implements PreImeAutoCompleteTextView.OnKeyPreImeListener
{
  private static final String TAG = "ConversationFragment";
  private Activity mActivity;
  private Language mFromLanguage;
  private ConversationHelper mHelper = new ConversationHelper();
  private String mInputText;
  private BroadcastReceiver mOnLanguagesChanged;
  private Language mToLanguage;

  public ConversationFragment()
  {
  }

  public ConversationFragment(TranslateEntry paramTranslateEntry)
  {
    this.mFromLanguage = paramTranslateEntry.fromLanguage;
    this.mToLanguage = paramTranslateEntry.toLanguage;
    this.mInputText = paramTranslateEntry.inputText;
  }

  public void onActivityCreated(Bundle paramBundle)
  {
    super.onActivityCreated(paramBundle);
    this.mActivity = getActivity();
    this.mHelper.onCreate(this.mActivity);
    this.mHelper.init((ConversationPanel)getView(), this.mFromLanguage, this.mToLanguage, this.mInputText);
    setListAdapter(this.mHelper.getConversationAdapter());
    this.mHelper.setListView(getListView());
    this.mOnLanguagesChanged = new BroadcastReceiver()
    {
      public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
      {
        ConversationFragment.this.mHelper.refreshLanguageList();
      }
    };
    this.mActivity.registerReceiver(this.mOnLanguagesChanged, new IntentFilter("com.google.android.apps.translate.broadcast.LANGUAGES_CHANGED"));
  }

  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    Logger.d("ConversationFragment", "onActivityResult");
    this.mHelper.onActivityResult(paramInt1, paramInt2, paramIntent);
  }

  public void onConfigurationChanged(Configuration paramConfiguration)
  {
    super.onConfigurationChanged(paramConfiguration);
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setHasOptionsMenu(true);
  }

  public void onCreateOptionsMenu(Menu paramMenu, MenuInflater paramMenuInflater)
  {
    Logger.d("ConversationFragment", "onCreateOptionsMenu");
    if (this.mHelper.onCreateOptionsMenu(paramMenu, paramMenuInflater))
      return;
    super.onCreateOptionsMenu(paramMenu, paramMenuInflater);
  }

  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
    return paramLayoutInflater.inflate(R.layout.conversation_panel, null);
  }

  public void onDestroy()
  {
    Logger.d("ConversationFragment", "onDestroy");
    this.mHelper.onDestroy();
    if (this.mOnLanguagesChanged != null)
      this.mActivity.unregisterReceiver(this.mOnLanguagesChanged);
    super.onDestroy();
  }

  public boolean onKeyPreIme(int paramInt, KeyEvent paramKeyEvent)
  {
    if (this.mHelper != null)
      return this.mHelper.onKeyPreIme(paramInt, paramKeyEvent);
    return false;
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    Logger.d("ConversationFragment", "onOptionsItemSelected");
    if ((this.mActivity != null) && (this.mHelper != null) && (this.mHelper.onOptionsItemSelected(paramMenuItem)))
    {
      this.mActivity.invalidateOptionsMenu();
      return true;
    }
    return super.onOptionsItemSelected(paramMenuItem);
  }

  public void onPause()
  {
    Logger.d("ConversationFragment", "onPause");
    super.onPause();
    this.mHelper.onPause();
  }

  public void onPrepareOptionsMenu(Menu paramMenu)
  {
    Logger.d("ConversationFragment", "onPrepareOptionsMenu");
    if (this.mHelper.onPrepareOptionsMenu(paramMenu))
      return;
    super.onPrepareOptionsMenu(paramMenu);
  }

  public void onResume()
  {
    Logger.d("ConversationFragment", "onResume");
    super.onResume();
    SdkVersionWrapper.getWrapper().setHomeButton(this.mActivity, true);
    if (this.mHelper != null)
      this.mHelper.onResume();
  }

  public void onStart()
  {
    Logger.d("ConversationFragment", "onStart");
    super.onStart();
    this.mHelper.onStart();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.conversation.ConversationFragment
 * JD-Core Version:    0.6.2
 */