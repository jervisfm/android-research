package com.google.android.apps.translate.conversation;

import android.app.Activity;
import android.content.Intent;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import com.google.android.apps.translate.Language;
import com.google.android.apps.translate.Languages;
import com.google.android.apps.translate.LanguagesFactory;
import com.google.android.apps.translate.Logger;
import com.google.android.apps.translate.Preconditions;
import com.google.android.apps.translate.R.id;
import com.google.android.apps.translate.R.layout;
import com.google.android.apps.translate.R.menu;
import com.google.android.apps.translate.SdkVersionWrapper;
import com.google.android.apps.translate.SdkVersionWrapper.WrapperBase;
import com.google.android.apps.translate.SettingsActivity;
import com.google.android.apps.translate.Translate;
import com.google.android.apps.translate.TranslateApplication;
import com.google.android.apps.translate.TranslateManager;
import com.google.android.apps.translate.Util;
import com.google.android.apps.translate.VoiceInputHelper;
import com.google.android.apps.translate.editor.EditPanelView;
import com.google.android.apps.translate.editor.InputMethodView.InputMethod;
import com.google.android.apps.translate.editor.PreImeAutoCompleteTextView.OnKeyPreImeListener;
import com.google.android.apps.translate.tts.MyTts;
import com.google.android.apps.translate.tts.NetworkTts.Callback;
import java.util.ArrayList;
import java.util.Locale;

public class ConversationHelper
  implements VoiceTranslateItemView.Callback, PreImeAutoCompleteTextView.OnKeyPreImeListener
{
  private static final boolean ALWAYS_SHOW_MENU_ITEMS = true;
  private static final boolean DEBUG = false;
  private static final String DUMMY_OUTPUT = "-----";
  private static final String TAG = "ConversationHelper";
  private Activity mActivity;
  private VoiceTranslationAdapter mAdapter = new VoiceTranslationAdapter();
  private EditPanelView mConfirmView;
  private ConversationPanel mConversationPanel;
  private int mEntries = 0;
  private ListView mListView;
  private NetworkTts.Callback mNetworkTtsCallback = new NetworkTts.Callback()
  {
    public void onError(int paramAnonymousInt)
    {
      Util.showNetworkTtsError(ConversationHelper.this.mActivity, paramAnonymousInt);
    }

    public void onPrepare()
    {
    }

    public void onReady()
    {
    }
  };
  private TranslateManager mTranslateManager;
  private MyTts mTts;

  private boolean canSpeak(String paramString)
  {
    if (this.mTts == null)
      return false;
    boolean bool = this.mTts.isLanguageAvailable(Util.languageShortNameToLocale(paramString));
    Logger.d(this, "is TTS available: " + paramString + ", return: " + bool);
    return bool;
  }

  private void debugPrintAdapter()
  {
    int i = this.mAdapter.getCount();
    Logger.d("ConversationHelper", "debugPrintAdapter count=" + i);
    for (int j = 0; j < i; j++)
    {
      VoiceTranslationEntry localVoiceTranslationEntry = (VoiceTranslationEntry)this.mAdapter.getItem(j);
      Logger.d("ConversationHelper", "==== item " + j + " ====");
      Logger.d("ConversationHelper", "inputLanguage=" + localVoiceTranslationEntry.inputLanguage);
      Logger.d("ConversationHelper", "outputLanguage=" + localVoiceTranslationEntry.outputLanguage);
      Logger.d("ConversationHelper", "inputText=" + localVoiceTranslationEntry.inputText);
      Logger.d("ConversationHelper", "outputText=" + localVoiceTranslationEntry.outputText);
      Logger.d("ConversationHelper", "hasTts=" + localVoiceTranslationEntry.hasTts);
      Logger.d("ConversationHelper", "sourceLeft=" + localVoiceTranslationEntry.sourceLeft);
    }
  }

  private void doTextTranslateAsync(final VoiceTranslationEntry paramVoiceTranslationEntry)
  {
    new Thread(new Runnable()
    {
      public void run()
      {
        ConversationHelper.this.doTextTranslateSync(paramVoiceTranslationEntry);
      }
    }).start();
  }

  private void doTextTranslateSync(VoiceTranslationEntry paramVoiceTranslationEntry)
  {
    Logger.d("ConversationHelper", "doTextTranslateSync entry.outputText=" + paramVoiceTranslationEntry.outputText);
    Language localLanguage1 = paramVoiceTranslationEntry.inputLanguage;
    Language localLanguage2 = paramVoiceTranslationEntry.outputLanguage;
    String str1 = paramVoiceTranslationEntry.inputText;
    while (true)
    {
      int i;
      synchronized (this.mTranslateManager)
      {
        Logger.d("ConversationHelper", "doTextTranslateSync from=" + localLanguage1 + " to=" + localLanguage2 + " text=" + str1);
        this.mTranslateManager.setLanguagePair(localLanguage1, localLanguage2);
        String str2 = this.mTranslateManager.doTranslate(str1);
        i = Translate.getResultCode(str2);
        if (i == 0)
        {
          String[] arrayOfString = str2.split("\t", 3);
          int j = arrayOfString.length;
          str3 = null;
          if (j > 1)
            str3 = arrayOfString[1];
          if (this.mConfirmView != null)
            this.mConfirmView.onTranslationDone();
          if (!TextUtils.isEmpty(str3))
            break label252;
          paramVoiceTranslationEntry.outputText = "-----";
          paramVoiceTranslationEntry.hasTts = false;
          str3 = "null";
          Logger.d("translated text: ", str3);
          this.mActivity.runOnUiThread(new Runnable()
          {
            public void run()
            {
              ConversationHelper.this.mAdapter.notifyDataSetChanged();
            }
          });
          return;
        }
      }
      Util.showTranslationErrorToastMessage(this.mActivity, i);
      String str3 = null;
      continue;
      label252: Language localLanguage3 = paramVoiceTranslationEntry.outputLanguage;
      paramVoiceTranslationEntry.outputText = str3;
      paramVoiceTranslationEntry.hasTts = canSpeak(localLanguage3.getShortName());
      doTtsAsync(str3, localLanguage3);
    }
  }

  private void doTtsAsync(final String paramString, final Language paramLanguage)
  {
    Runnable local4 = new Runnable()
    {
      public void run()
      {
        ConversationHelper.this.doTtsSync(paramString, paramLanguage);
      }
    };
    this.mConversationPanel.post(local4);
  }

  private void doTtsSync(String paramString, Language paramLanguage)
  {
    if ((paramString == null) || (paramLanguage == null));
    while (!canSpeak(paramLanguage.getShortName()))
      return;
    this.mTts.speak(this.mActivity, paramLanguage, paramString, this.mNetworkTtsCallback);
  }

  private final Languages getLanguages()
  {
    return LanguagesFactory.get().getLanguages(this.mActivity, Locale.getDefault());
  }

  void addNewTranslateEntry(String paramString, boolean paramBoolean1, Language paramLanguage1, Language paramLanguage2, boolean paramBoolean2)
  {
    Preconditions.checkNotNull(paramString);
    Logger.d("ConversationHelper", "addNewTranslateEntry recognitionResult=" + paramString);
    VoiceTranslationEntry localVoiceTranslationEntry = new VoiceTranslationEntry();
    localVoiceTranslationEntry.inputText = paramString;
    localVoiceTranslationEntry.inputLanguage = paramLanguage1;
    localVoiceTranslationEntry.outputLanguage = paramLanguage2;
    localVoiceTranslationEntry.sourceLeft = paramBoolean2;
    localVoiceTranslationEntry.position = this.mEntries;
    this.mEntries = (1 + this.mEntries);
    if (!TextUtils.isEmpty(localVoiceTranslationEntry.inputText))
      doTextTranslateAsync(localVoiceTranslationEntry);
    this.mAdapter.addTranslationEntry(localVoiceTranslationEntry);
  }

  VoiceTranslationAdapter getConversationAdapter()
  {
    return this.mAdapter;
  }

  ConversationPanel getConversationPanelView()
  {
    return this.mConversationPanel;
  }

  ListView getListView()
  {
    return this.mListView;
  }

  void init(ConversationPanel paramConversationPanel, Language paramLanguage1, Language paramLanguage2, String paramString)
  {
    Logger.d("ConversationHelper", "init from=" + paramLanguage1 + " to=" + paramLanguage2 + " inputText=" + paramString);
    this.mConversationPanel = paramConversationPanel;
    if (Util.isAutoDetectLanguage(paramLanguage1));
    for (Language localLanguage1 = null; ; localLanguage1 = paramLanguage1)
    {
      this.mConversationPanel.initParameters(this, this.mActivity, localLanguage1, paramLanguage2);
      this.mConversationPanel.setLanguageList(getLanguages());
      Language localLanguage2 = this.mConversationPanel.getLeftLanguage();
      Language localLanguage3 = this.mConversationPanel.getRightLanguage();
      if ((this.mEntries == 0) && (!TextUtils.isEmpty(paramString)) && (localLanguage2 != null) && (!localLanguage2.getShortName().equals("select")) && (localLanguage3 != null) && (!localLanguage3.getShortName().equals("select")))
        addNewTranslateEntry(paramString, false, localLanguage2, localLanguage3, true);
      return;
    }
  }

  void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    if (this.mConversationPanel == null);
    do
    {
      return;
      if (this.mConfirmView != null)
        this.mConfirmView.onActivityResult(paramInt1, paramInt2, paramIntent);
      switch (paramInt1)
      {
      default:
        return;
      case 100:
      case 181:
      }
    }
    while (paramInt2 == 101);
    if ((paramInt2 == -1) && (paramIntent != null));
    for (String str = VoiceInputHelper.getRecognitionResult(paramIntent); ; str = null)
    {
      this.mConversationPanel.onVoiceRecognitionResult(str);
      return;
    }
  }

  void onCreate(Activity paramActivity)
  {
    this.mActivity = paramActivity;
    this.mActivity.setVolumeControlStream(3);
    TranslateApplication localTranslateApplication = (TranslateApplication)this.mActivity.getApplication();
    this.mTranslateManager = localTranslateApplication.getTranslateManager();
    this.mTts = localTranslateApplication.getMyTts();
  }

  boolean onCreateOptionsMenu(Menu paramMenu, MenuInflater paramMenuInflater)
  {
    Logger.d("ConversationHelper", "onCreateOptionsMenu");
    paramMenuInflater.inflate(R.menu.conversation_activity_menu, paramMenu);
    return false;
  }

  void onDestroy()
  {
    if (this.mConversationPanel != null)
      this.mConversationPanel.onDestroy();
  }

  public boolean onKeyPreIme(int paramInt, KeyEvent paramKeyEvent)
  {
    if (this.mConversationPanel != null)
      return this.mConversationPanel.onKeyPreIme(paramInt, paramKeyEvent);
    return false;
  }

  boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    int i = paramMenuItem.getItemId();
    if (i == R.id.menu_feedback)
    {
      SdkVersionWrapper.getWrapper().sendFeedback(this.mActivity, true);
      return true;
    }
    if (i == R.id.menu_settings)
    {
      this.mActivity.startActivity(new Intent(this.mActivity, SettingsActivity.class));
      return true;
    }
    return false;
  }

  void onPause()
  {
    if (this.mConversationPanel != null)
      this.mConversationPanel.onPause();
  }

  boolean onPrepareOptionsMenu(Menu paramMenu)
  {
    Logger.d("ConversationHelper", "onPrepareOptionsMenu");
    return false;
  }

  void onResume()
  {
    if (this.mConversationPanel != null)
      this.mConversationPanel.onResume();
  }

  void onStart()
  {
    Translate.setAcceptLanguage(Locale.getDefault().toString());
  }

  public void onTts(VoiceTranslationEntry paramVoiceTranslationEntry)
  {
    doTtsAsync(paramVoiceTranslationEntry.outputText, paramVoiceTranslationEntry.outputLanguage);
  }

  void onVoiceInput(Language paramLanguage, int paramInt1, int paramInt2, InputMethodView.InputMethod paramInputMethod)
  {
    Logger.d("ConversationHelper", "onVoiceInput");
    if (paramLanguage != null)
    {
      Logger.d("ConversationHelper", "onVoiceInput => getVoiceInput");
      this.mConversationPanel.getVoiceInput(paramInt2, paramInputMethod);
      return;
    }
    Logger.d("ConversationHelper", "onVoiceInput => onActivityResult");
    onActivityResult(paramInt1, 101, null);
  }

  void refreshLanguageList()
  {
    Logger.d("ConversationHelper", "refreshLanguageList");
    this.mConversationPanel.refreshLanguageList();
  }

  void setCurrentConfirmView(EditPanelView paramEditPanelView)
  {
    this.mConfirmView = paramEditPanelView;
  }

  void setListView(ListView paramListView)
  {
    this.mListView = paramListView;
    this.mListView.setDividerHeight(0);
    this.mListView.setItemsCanFocus(true);
    View localView = this.mConversationPanel.getIntroView();
    if (localView != null)
      this.mListView.setEmptyView(localView);
  }

  private class VoiceTranslationAdapter extends BaseAdapter
  {
    private ArrayList<ConversationHelper.VoiceTranslationEntry> mVoiceTranslateEntries = new ArrayList();

    public VoiceTranslationAdapter()
    {
    }

    public void addTranslationEntry(ConversationHelper.VoiceTranslationEntry paramVoiceTranslationEntry)
    {
      this.mVoiceTranslateEntries.add(paramVoiceTranslationEntry);
      notifyDataSetChanged();
    }

    public int getCount()
    {
      return this.mVoiceTranslateEntries.size();
    }

    public Object getItem(int paramInt)
    {
      return this.mVoiceTranslateEntries.get(paramInt);
    }

    public long getItemId(int paramInt)
    {
      return paramInt;
    }

    public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
    {
      ConversationHelper.VoiceTranslationEntry localVoiceTranslationEntry = (ConversationHelper.VoiceTranslationEntry)getItem(paramInt);
      if (paramView == null)
      {
        paramView = ConversationHelper.this.mActivity.getLayoutInflater().inflate(R.layout.voice_translate_item_view, null);
        ((VoiceTranslateItemView)paramView).initParameters(ConversationHelper.this.mActivity, ConversationHelper.this.mListView);
        ((VoiceTranslateItemView)paramView).setCallback(ConversationHelper.this);
      }
      VoiceTranslateItemView localVoiceTranslateItemView = (VoiceTranslateItemView)paramView;
      localVoiceTranslateItemView.addInputText(localVoiceTranslationEntry);
      if (localVoiceTranslationEntry.outputText != null)
        localVoiceTranslateItemView.addTranslate(localVoiceTranslationEntry);
      localVoiceTranslateItemView.render(localVoiceTranslationEntry, paramInt, this.mVoiceTranslateEntries.size());
      return paramView;
    }
  }

  class VoiceTranslationEntry
  {
    public boolean hasTts;
    public Language inputLanguage;
    public String inputText;
    public Language outputLanguage;
    public String outputText;
    public int position;
    public boolean sourceLeft;

    VoiceTranslationEntry()
    {
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.conversation.ConversationHelper
 * JD-Core Version:    0.6.2
 */