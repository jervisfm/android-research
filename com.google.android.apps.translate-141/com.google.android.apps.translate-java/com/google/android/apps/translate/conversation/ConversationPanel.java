package com.google.android.apps.translate.conversation;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import com.google.android.apps.translate.Language;
import com.google.android.apps.translate.LanguagePicker;
import com.google.android.apps.translate.LanguagePicker.OnLanguagePairSelectedListener;
import com.google.android.apps.translate.Languages;
import com.google.android.apps.translate.LanguagesFactory;
import com.google.android.apps.translate.Logger;
import com.google.android.apps.translate.Profile;
import com.google.android.apps.translate.R.id;
import com.google.android.apps.translate.R.string;
import com.google.android.apps.translate.TranslateApplication;
import com.google.android.apps.translate.TranslateBaseActivity;
import com.google.android.apps.translate.UserActivityMgr;
import com.google.android.apps.translate.Util;
import com.google.android.apps.translate.VoiceInputHelper;
import com.google.android.apps.translate.asreditor.AsrResultEditor;
import com.google.android.apps.translate.editor.EditPanelView;
import com.google.android.apps.translate.editor.EditPanelView.Callback;
import com.google.android.apps.translate.editor.InputMethodView;
import com.google.android.apps.translate.editor.InputMethodView.InputMethod;
import com.google.android.apps.translate.editor.InputMethodView.InputMethodEvent;
import com.google.android.apps.translate.editor.TextSlot;
import com.google.android.apps.translate.home.TitleView;
import com.google.android.apps.translate.translation.TranslateHelper;
import com.google.android.apps.translate.translation.TranslateHelper.LocalizedLanguageCallback;
import java.util.Locale;

public class ConversationPanel extends RelativeLayout
  implements View.OnClickListener, EditPanelView.Callback, View.OnTouchListener, LanguagePicker.OnLanguagePairSelectedListener
{
  private static final boolean ALWAYS_SHOW_INTRO = true;
  private static final String TAG = "ConversationPanel";
  private static final boolean USE_VOICE_IS_POSSIBLE = true;
  private Activity mActivity;
  private InputMethodView mControlPanel;
  private View mControlPanelWrapper;
  private ConversationHelper mConversationHelper;
  private EditPanelView mCurrentEditView;
  private Language mFromLanguage;
  private View mIntroMessageView;
  private Languages mLanguageList;
  private View mLanguagePanel;
  private LanguagePicker mLanguagePicker;
  private AsrResultEditor mLeftAsrResultEditor;
  private TextView mLeftBtn;
  private EditPanelView mLeftEditView;
  private TextSlot mLeftInputEditText;
  private View mListWrapper;
  private AsrResultEditor mRightAsrResultEditor;
  private TextView mRightBtn;
  private EditPanelView mRightEditView;
  private TextSlot mRightInputEditText;
  private TitleView mTitleView;
  private Language mToLanguage;
  private VoiceInputHelper mVoiceInputHelper;

  public ConversationPanel(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }

  private void endEditMode(EditPanelView paramEditPanelView, TextSlot paramTextSlot, View paramView1, View paramView2, boolean paramBoolean, InputMethodView.InputMethodEvent paramInputMethodEvent)
  {
    Logger.d("ConversationPanel", "endEditMode");
    String str1 = paramTextSlot.getText().toString();
    ConversationHelper localConversationHelper;
    String str2;
    Language localLanguage1;
    Language localLanguage2;
    if ((paramBoolean) && (!TextUtils.isEmpty(str1)))
    {
      localConversationHelper = this.mConversationHelper;
      str2 = paramTextSlot.getText().toString();
      localLanguage1 = paramEditPanelView.getSourceLanguage();
      localLanguage2 = paramEditPanelView.getTargetLanguage();
      if (this.mLeftEditView != paramEditPanelView)
        break label156;
    }
    label156: for (boolean bool = true; ; bool = false)
    {
      localConversationHelper.addNewTranslateEntry(str2, true, localLanguage1, localLanguage2, bool);
      paramTextSlot.setText("");
      paramView2.setVisibility(0);
      paramEditPanelView.disableEditMode(true, paramInputMethodEvent);
      paramEditPanelView.setVisibility(8);
      paramView1.setVisibility(0);
      invalidate();
      if (this.mIntroMessageView != null)
        this.mIntroMessageView.setVisibility(0);
      this.mTitleView.fixEditingViewVisibility(paramEditPanelView, 0);
      postDelayed(new Runnable()
      {
        public void run()
        {
          if (ConversationPanel.this.mLanguagePanel.getVisibility() != 0)
            ConversationPanel.this.mLanguagePanel.setVisibility(0);
        }
      }
      , 1000L);
      return;
    }
  }

  private void languagePairSelected(Language paramLanguage1, Language paramLanguage2, boolean paramBoolean1, boolean paramBoolean2)
  {
    if ((!paramLanguage1.equals(this.mFromLanguage)) || (!paramLanguage2.equals(this.mToLanguage)));
    for (int i = 1; ; i = 0)
    {
      if (i != 0)
      {
        this.mFromLanguage = paramLanguage1;
        this.mToLanguage = paramLanguage2;
        this.mLanguagePicker.setLanguagePairToSpinners(paramLanguage1, paramLanguage2);
        onLanguagePairSelected(paramLanguage1, paramLanguage2, true, paramBoolean2);
        Util.showShortToastMessage(this.mActivity, Util.generateLongPairText(paramLanguage1, paramLanguage2));
      }
      return;
    }
  }

  private void onVoiceInputClick(int paramInt, Language paramLanguage)
  {
    if (this.mVoiceInputHelper.isVoiceInputAvailable(paramLanguage))
      this.mConversationHelper.onVoiceInput(paramLanguage, 181, paramInt, InputMethodView.InputMethod.VOICE);
    do
    {
      return;
      Util.showShortToastMessage(this.mActivity, R.string.msg_voice_input_not_supported);
    }
    while (!Profile.getConversationInputConfirm(this.mActivity));
    this.mConversationHelper.onVoiceInput(paramLanguage, 181, paramInt, InputMethodView.InputMethod.KEYBOARD);
  }

  private void startEditMode(EditPanelView paramEditPanelView, View paramView1, View paramView2, InputMethodView.InputMethod paramInputMethod)
  {
    Logger.d("ConversationPanel", "startEditMode");
    this.mTitleView.hideTitleBar();
    this.mCurrentEditView = paramEditPanelView;
    paramEditPanelView.init(this.mActivity, this.mLanguageList, paramEditPanelView.getSourceLanguage(), paramEditPanelView.getTargetLanguage(), this.mVoiceInputHelper.getAsrLocale(this.mActivity, paramEditPanelView.getSourceLanguage()), true);
    paramView2.setVisibility(8);
    this.mConversationHelper.setCurrentConfirmView(paramEditPanelView);
    paramView1.setVisibility(8);
    paramEditPanelView.setVisibility(0);
    if (paramInputMethod != null)
      paramEditPanelView.enableEditMode(paramInputMethod);
    if (this.mIntroMessageView != null)
      this.mIntroMessageView.setVisibility(8);
  }

  private void swapLanguagePair(boolean paramBoolean)
  {
    Language localLanguage1 = this.mLanguageList.getToLanguageByShortName(this.mToLanguage.getShortName());
    if (localLanguage1 == null)
      localLanguage1 = this.mLanguageList.getDefaultFromLanguage();
    Language localLanguage2 = this.mLanguageList.getToLanguageByShortName(this.mFromLanguage.getShortName());
    if (localLanguage2 == null)
      localLanguage2 = this.mLanguageList.getDefaultToLanguage();
    languagePairSelected(localLanguage1, localLanguage2, paramBoolean, paramBoolean);
  }

  View getIntroView()
  {
    return this.mIntroMessageView;
  }

  Language getLeftLanguage()
  {
    return this.mFromLanguage;
  }

  Language getRightLanguage()
  {
    return this.mToLanguage;
  }

  public void getVoiceInput(final int paramInt, final InputMethodView.InputMethod paramInputMethod)
  {
    this.mActivity.runOnUiThread(new Runnable()
    {
      public void run()
      {
        if (paramInt == R.id.left_speak_btn)
          ConversationPanel.this.startEditMode(ConversationPanel.this.mLeftEditView, ConversationPanel.this.mLeftBtn, ConversationPanel.this.mRightBtn, paramInputMethod);
        while (true)
        {
          ConversationPanel.this.invalidate();
          ConversationPanel.this.mConversationHelper.getListView().invalidate();
          return;
          if (paramInt == R.id.right_speak_btn)
            ConversationPanel.this.startEditMode(ConversationPanel.this.mRightEditView, ConversationPanel.this.mRightBtn, ConversationPanel.this.mLeftBtn, paramInputMethod);
          else
            Logger.e("ConversationPanel", "getVoiceInput invalid resourceId=" + paramInt);
        }
      }
    });
  }

  public boolean hasSomethingToClear()
  {
    return false;
  }

  public void initParameters(ConversationHelper paramConversationHelper, Activity paramActivity, Language paramLanguage1, Language paramLanguage2)
  {
    Logger.d("ConversationPanel", "initParameters");
    this.mConversationHelper = paramConversationHelper;
    this.mActivity = paramActivity;
    if (Util.isHoneycombCompatible())
    {
      this.mTitleView = ((TitleView)getRootView().findViewById(R.id.fragments_translate_title_bar));
      ((TitleView)findViewById(R.id.translate_title_bar)).setVisibility(8);
    }
    while (true)
    {
      ((TextView)this.mTitleView.findViewById(R.id.translate_title)).setText(TranslateBaseActivity.getTitle(this.mActivity));
      this.mLanguagePanel = findViewById(R.id.language_selection_panel);
      this.mTitleView.setLanguagePanel(this.mLanguagePanel);
      this.mVoiceInputHelper = ((TranslateApplication)this.mActivity.getApplication()).getVoiceInputHelper();
      this.mControlPanelWrapper = this.mLanguagePanel.getRootView().findViewById(R.id.input_method_view_wrapper);
      this.mControlPanel = ((InputMethodView)this.mControlPanelWrapper.findViewById(R.id.control_panel));
      this.mLeftEditView = ((EditPanelView)findViewById(R.id.left_conv_edit_view));
      this.mLeftEditView.setCallback(this);
      this.mLeftEditView.setInputMethodView(this.mControlPanel);
      this.mRightEditView = ((EditPanelView)findViewById(R.id.right_conv_edit_view));
      this.mRightEditView.setCallback(this);
      this.mRightEditView.setInputMethodView(this.mControlPanel);
      setLanguageList(LanguagesFactory.get().getLanguages(paramActivity, Locale.getDefault()));
      this.mLeftAsrResultEditor = ((AsrResultEditor)this.mLeftEditView.findViewById(R.id.msg_confirm_content));
      this.mLeftInputEditText = this.mLeftAsrResultEditor.getEditorField();
      this.mLeftInputEditText.setTouchEventCallback(this.mLeftEditView);
      this.mRightAsrResultEditor = ((AsrResultEditor)this.mRightEditView.findViewById(R.id.msg_confirm_content));
      this.mRightInputEditText = this.mRightAsrResultEditor.getEditorField();
      this.mRightInputEditText.setTouchEventCallback(this.mRightEditView);
      this.mLeftInputEditText.clearFocus();
      this.mRightInputEditText.clearFocus();
      this.mLeftInputEditText.addTextChangedListener(this.mLeftEditView);
      this.mRightInputEditText.addTextChangedListener(this.mRightEditView);
      this.mLeftBtn = ((TextView)findViewById(R.id.left_speak_btn));
      this.mRightBtn = ((TextView)findViewById(R.id.right_speak_btn));
      this.mLeftBtn.setOnClickListener(this);
      this.mRightBtn.setOnClickListener(this);
      this.mListWrapper = findViewById(R.id.conversation_list_wrapper);
      findViewById(R.id.btn_swap).setOnClickListener(this);
      Spinner localSpinner1 = (Spinner)findViewById(R.id.spinner_my_lang);
      Spinner localSpinner2 = (Spinner)findViewById(R.id.spinner_their_lang);
      this.mLanguagePicker = new LanguagePicker(this.mActivity, localSpinner1, localSpinner2, this);
      this.mLanguagePicker.setupLanguageSpinnersForConversationMode(this.mVoiceInputHelper, paramLanguage1, paramLanguage2);
      this.mFromLanguage = this.mLanguagePicker.getFromLanguage();
      this.mToLanguage = this.mLanguagePicker.getToLanguage();
      Logger.d("ConversationPanel", "setLanguagePair 4");
      setLanguagePair(this.mFromLanguage, this.mToLanguage, false);
      this.mLeftInputEditText.setOnTouchListener(this);
      this.mRightInputEditText.setOnTouchListener(this);
      this.mIntroMessageView = findViewById(R.id.conversation_list_empty_view);
      this.mTitleView.checkTitleShow();
      return;
      this.mTitleView = ((TitleView)findViewById(R.id.translate_title_bar));
    }
  }

  public void onAccept(final int paramInt, final boolean paramBoolean)
  {
    Logger.d("ConversationPanel", "onAccept resourceId=" + paramInt);
    this.mActivity.runOnUiThread(new Runnable()
    {
      public void run()
      {
        if (ConversationPanel.this.mCurrentEditView == ConversationPanel.this.mLeftEditView)
        {
          ConversationPanel.this.endEditMode(ConversationPanel.this.mLeftEditView, ConversationPanel.this.mLeftInputEditText, ConversationPanel.this.mLeftBtn, ConversationPanel.this.mRightBtn, paramBoolean, InputMethodView.InputMethodEvent.ACCEPT);
          return;
        }
        if (ConversationPanel.this.mCurrentEditView == ConversationPanel.this.mRightEditView)
        {
          ConversationPanel.this.endEditMode(ConversationPanel.this.mRightEditView, ConversationPanel.this.mRightInputEditText, ConversationPanel.this.mRightBtn, ConversationPanel.this.mLeftBtn, paramBoolean, InputMethodView.InputMethodEvent.ACCEPT);
          return;
        }
        Logger.e("ConversationPanel", "onAccept invalid resourceId=" + paramInt);
      }
    });
  }

  public void onClick(View paramView)
  {
    int i = paramView.getId();
    Logger.d("ConversationPanel", "onClick id=" + i);
    if (i == R.id.left_speak_btn)
    {
      this.mRightInputEditText.clearFocus();
      this.mLeftInputEditText.requestFocus();
      onVoiceInputClick(i, this.mLeftEditView.getSourceLanguage());
    }
    do
    {
      return;
      if (i == R.id.right_speak_btn)
      {
        this.mLeftInputEditText.clearFocus();
        this.mRightInputEditText.requestFocus();
        onVoiceInputClick(i, this.mRightEditView.getSourceLanguage());
        return;
      }
      if (i == R.id.btn_swap)
      {
        swapLanguagePair(true);
        return;
      }
    }
    while (i != R.id.conversation_list_empty_view);
  }

  public void onConfigurationChanged(Configuration paramConfiguration)
  {
    Logger.d("ConversationPanel", "onConfigurationChanged");
    if (((paramConfiguration.orientation != 2) && (paramConfiguration.orientation != 1)) || (this.mCurrentEditView == null))
      return;
    this.mCurrentEditView.onConfigurationChanged(paramConfiguration);
    this.mTitleView.fixEditingViewVisibility(this.mCurrentEditView, 500);
  }

  void onDestroy()
  {
    if (this.mLeftEditView != null)
      this.mLeftEditView.onDestroy();
    if (this.mRightEditView != null)
      this.mRightEditView.onDestroy();
  }

  public void onEditModeReady(InputMethodView.InputMethod paramInputMethod)
  {
    Logger.d("ConversationPanel", "onEditModeReady");
    if ((this.mTitleView != null) && (this.mCurrentEditView != null))
      this.mTitleView.fixEditingViewVisibility(this.mCurrentEditView, 0);
  }

  public void onEditModeStart(EditPanelView paramEditPanelView, InputMethodView.InputMethod paramInputMethod)
  {
    if (paramEditPanelView == this.mLeftEditView)
    {
      startEditMode(this.mLeftEditView, this.mLeftBtn, this.mRightBtn, paramInputMethod);
      return;
    }
    startEditMode(this.mRightEditView, this.mRightBtn, this.mLeftBtn, paramInputMethod);
  }

  public boolean onKeyPreIme(int paramInt, KeyEvent paramKeyEvent)
  {
    if (this.mCurrentEditView != null)
      return this.mCurrentEditView.onKeyPreIme(paramInt, paramKeyEvent);
    return false;
  }

  public void onLanguagePairSelected(Language paramLanguage1, Language paramLanguage2, boolean paramBoolean1, boolean paramBoolean2)
  {
    Logger.d("ConversationPanel", "onLanguagePairSelected left=" + paramLanguage1 + " right=" + paramLanguage2);
    UserActivityMgr.setLanguageChangesForConversation(this.mFromLanguage, this.mToLanguage, paramLanguage1, paramLanguage2);
    this.mFromLanguage = paramLanguage1;
    this.mToLanguage = paramLanguage2;
    Logger.d("ConversationPanel", "setLanguagePair 5");
    setLanguagePair(this.mFromLanguage, this.mToLanguage, paramBoolean2);
  }

  public void onPause()
  {
  }

  public void onResume()
  {
    this.mTitleView.checkTitleShow();
    if ((this.mCurrentEditView != null) && (this.mCurrentEditView.isEditMode()))
      onEditModeStart(this.mCurrentEditView, null);
  }

  public void onSourceLanguageChangeRequested(Language paramLanguage, String paramString)
  {
  }

  public boolean onTouch(View paramView, MotionEvent paramMotionEvent)
  {
    Logger.d("ConversationPanel", "onTouch");
    int i = paramView.getId();
    if ((paramMotionEvent != null) && (paramMotionEvent.getAction() == 0) && ((i != R.id.editor_field) || (this.mCurrentEditView == null)))
    {
      if (paramView == this.mLeftInputEditText)
      {
        Language localLanguage2 = this.mLeftEditView.getSourceLanguage();
        if (this.mVoiceInputHelper.isConversationLanguage(localLanguage2))
        {
          onVoiceInputClick(R.id.left_speak_btn, localLanguage2);
          return true;
        }
        this.mRightInputEditText.clearFocus();
        this.mLeftInputEditText.requestFocus();
        startEditMode(this.mLeftEditView, this.mLeftBtn, this.mRightBtn, InputMethodView.InputMethod.KEYBOARD);
        return true;
      }
      if (paramView == this.mRightInputEditText)
      {
        Language localLanguage1 = this.mRightEditView.getSourceLanguage();
        if (this.mVoiceInputHelper.isConversationLanguage(localLanguage1))
        {
          onVoiceInputClick(R.id.right_speak_btn, localLanguage1);
          return true;
        }
        this.mLeftInputEditText.clearFocus();
        this.mRightInputEditText.requestFocus();
        startEditMode(this.mRightEditView, this.mRightBtn, this.mLeftBtn, InputMethodView.InputMethod.KEYBOARD);
        return true;
      }
    }
    return false;
  }

  public void onVoiceRecognitionResult(String paramString)
  {
    Logger.d("ConversationPanel", "onVoiceRecognitionResult");
    this.mCurrentEditView.hideCurrentInputMethod(InputMethodView.InputMethodEvent.SWITCH);
    this.mCurrentEditView.onNonStreamingVoiceResult(paramString);
    if (!Profile.getConversationInputConfirm(this.mActivity))
      onAccept(this.mCurrentEditView.getId(), true);
  }

  void refreshLanguageList()
  {
    Logger.d("ConversationPanel", "refreshLanguageList");
    Languages localLanguages = LanguagesFactory.get().getLanguages(this.mActivity, Locale.getDefault());
    setLanguageList(localLanguages);
    Language localLanguage1 = localLanguages.getToLanguageByShortName(this.mFromLanguage.getShortName());
    Language localLanguage2 = localLanguages.getToLanguageByShortName(this.mToLanguage.getShortName());
    if ((localLanguage1 != null) && (localLanguage2 != null))
      setLanguagePair(localLanguage1, localLanguage2, false);
  }

  public void setLanguageList(Languages paramLanguages)
  {
    this.mLanguageList = paramLanguages;
  }

  public void setLanguagePair(Language paramLanguage1, Language paramLanguage2, boolean paramBoolean)
  {
    Logger.d("ConversationPanel", "setLanguagePair [" + paramLanguage1 + ", " + paramLanguage2 + "]");
    this.mLanguagePicker.setLanguagePairToSpinners(paramLanguage1, paramLanguage2);
    boolean bool = true;
    if ((paramLanguage1 == null) || (paramLanguage1.getShortName().equals("select")))
    {
      this.mLeftBtn.setText("");
      bool = false;
      paramLanguage1 = this.mLanguageList.getFromLanguageByShortName("en");
      if ((paramLanguage2 != null) && (!paramLanguage2.getShortName().equals("select")))
        break label353;
      this.mRightBtn.setText("");
      bool = false;
      paramLanguage2 = this.mLanguageList.getFromLanguageByShortName("en");
    }
    while (true)
    {
      this.mLeftBtn.setEnabled(bool);
      this.mRightBtn.setEnabled(bool);
      EditPanelView localEditPanelView1 = this.mLeftEditView;
      Activity localActivity1 = this.mActivity;
      Languages localLanguages1 = this.mLanguageList;
      String str1 = this.mVoiceInputHelper.getAsrLocale(this.mActivity, paramLanguage1);
      localEditPanelView1.init(localActivity1, localLanguages1, paramLanguage1, paramLanguage2, str1, true);
      EditPanelView localEditPanelView2 = this.mLeftEditView;
      TextSlot localTextSlot1 = this.mLeftInputEditText;
      TextView localTextView1 = this.mLeftBtn;
      TextView localTextView2 = this.mRightBtn;
      InputMethodView.InputMethodEvent localInputMethodEvent1 = InputMethodView.InputMethodEvent.INIT;
      endEditMode(localEditPanelView2, localTextSlot1, localTextView1, localTextView2, bool, localInputMethodEvent1);
      EditPanelView localEditPanelView3 = this.mRightEditView;
      Activity localActivity2 = this.mActivity;
      Languages localLanguages2 = this.mLanguageList;
      String str2 = this.mVoiceInputHelper.getAsrLocale(this.mActivity, paramLanguage2);
      localEditPanelView3.init(localActivity2, localLanguages2, paramLanguage2, paramLanguage1, str2, true);
      EditPanelView localEditPanelView4 = this.mRightEditView;
      TextSlot localTextSlot2 = this.mRightInputEditText;
      TextView localTextView3 = this.mRightBtn;
      TextView localTextView4 = this.mLeftBtn;
      InputMethodView.InputMethodEvent localInputMethodEvent2 = InputMethodView.InputMethodEvent.INIT;
      endEditMode(localEditPanelView4, localTextSlot2, localTextView3, localTextView4, bool, localInputMethodEvent2);
      return;
      TranslateHelper.getLocalizedLanguageName(this.mActivity, paramLanguage1, new TranslateHelper.LocalizedLanguageCallback()
      {
        public void onLanguageNameReceived(Language paramAnonymousLanguage, Locale paramAnonymousLocale, String paramAnonymousString)
        {
          ConversationPanel.this.mLeftBtn.setText(paramAnonymousString);
          ConversationPanel.this.mLeftInputEditText.setHint(paramAnonymousString);
        }
      });
      break;
      label353: TranslateHelper.getLocalizedLanguageName(this.mActivity, paramLanguage2, new TranslateHelper.LocalizedLanguageCallback()
      {
        public void onLanguageNameReceived(Language paramAnonymousLanguage, Locale paramAnonymousLocale, String paramAnonymousString)
        {
          ConversationPanel.this.mRightBtn.setText(paramAnonymousString);
          ConversationPanel.this.mRightInputEditText.setHint(paramAnonymousString);
        }
      });
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.conversation.ConversationPanel
 * JD-Core Version:    0.6.2
 */