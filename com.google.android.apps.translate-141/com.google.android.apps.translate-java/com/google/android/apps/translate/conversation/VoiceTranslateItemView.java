package com.google.android.apps.translate.conversation;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.TextView;
import com.google.android.apps.translate.Constants.AppearanceType;
import com.google.android.apps.translate.Language;
import com.google.android.apps.translate.R.dimen;
import com.google.android.apps.translate.R.drawable;
import com.google.android.apps.translate.R.id;
import com.google.android.apps.translate.Util;

public class VoiceTranslateItemView extends LinearLayout
  implements View.OnClickListener
{
  private static final String TAG = "VoiceTranslateItemView";
  private Activity mActivity;
  private int mBubblePaddingPx;
  private Callback mCallback;
  private TextView mInputText;
  private ConversationHelper.VoiceTranslationEntry mItemEntry;
  private ListView mListView;
  private LinearLayout mPaddingView;
  private TextView mTranslatedText;
  private View mTtsButton;
  private FrameLayout mWrapperItem;

  public VoiceTranslateItemView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }

  public void addInputText(ConversationHelper.VoiceTranslationEntry paramVoiceTranslationEntry)
  {
    if ((paramVoiceTranslationEntry.inputText == null) || (paramVoiceTranslationEntry.inputLanguage == null))
      return;
    TextView localTextView = this.mInputText;
    String str = paramVoiceTranslationEntry.inputText;
    Language[] arrayOfLanguage = new Language[1];
    arrayOfLanguage[0] = paramVoiceTranslationEntry.inputLanguage;
    Util.setTextAndFont(localTextView, str, arrayOfLanguage, Constants.AppearanceType.UNCHANGED, false);
    this.mItemEntry = null;
    this.mTranslatedText.setText("");
  }

  public void addTranslate(ConversationHelper.VoiceTranslationEntry paramVoiceTranslationEntry)
  {
    if ((paramVoiceTranslationEntry.outputText == null) || (paramVoiceTranslationEntry.outputLanguage == null));
    do
    {
      return;
      TextView localTextView = this.mTranslatedText;
      String str = paramVoiceTranslationEntry.outputText;
      Language[] arrayOfLanguage = new Language[2];
      arrayOfLanguage[0] = paramVoiceTranslationEntry.inputLanguage;
      arrayOfLanguage[1] = paramVoiceTranslationEntry.outputLanguage;
      Util.setTextAndFont(localTextView, str, arrayOfLanguage, Constants.AppearanceType.UNCHANGED, false);
    }
    while (!paramVoiceTranslationEntry.hasTts);
    this.mItemEntry = paramVoiceTranslationEntry;
  }

  public void initParameters(Activity paramActivity, ListView paramListView)
  {
    this.mActivity = paramActivity;
    this.mListView = paramListView;
    this.mBubblePaddingPx = this.mActivity.getResources().getDimensionPixelSize(R.dimen.conversation_bubble_padding);
    this.mWrapperItem = ((FrameLayout)findViewById(R.id.voice_translate_item));
    this.mTtsButton = findViewById(R.id.conv_tts_button);
    this.mInputText = ((TextView)findViewById(R.id.s2s_input_text));
    this.mTranslatedText = ((TextView)findViewById(R.id.s2s_translated_text));
    this.mPaddingView = ((LinearLayout)findViewById(R.id.voice_translate_item_padding));
    this.mTtsButton.setOnClickListener(this);
  }

  public void onClick(View paramView)
  {
    int i = paramView.getId();
    if (((i == R.id.conv_tts_button) || (i == R.id.voice_translate_item) || (i == R.id.s2s_input_text) || (i == R.id.s2s_translated_text)) && (this.mCallback != null) && (this.mItemEntry != null) && (this.mItemEntry.hasTts))
      this.mCallback.onTts(this.mItemEntry);
  }

  public void render(ConversationHelper.VoiceTranslationEntry paramVoiceTranslationEntry, int paramInt1, int paramInt2)
  {
    LinearLayout.LayoutParams localLayoutParams = (LinearLayout.LayoutParams)this.mWrapperItem.getLayoutParams();
    int i;
    label60: LinearLayout localLinearLayout;
    int j;
    if (paramVoiceTranslationEntry.sourceLeft)
    {
      this.mWrapperItem.setBackgroundResource(R.drawable.conversation_bubble_left);
      localLayoutParams.setMargins(0, 0, this.mBubblePaddingPx, 0);
      this.mWrapperItem.setLayoutParams(localLayoutParams);
      if (paramInt1 != paramInt2 - 1)
        break label147;
      i = 1;
      localLinearLayout = this.mPaddingView;
      j = 0;
      if (i == 0)
        break label153;
    }
    while (true)
    {
      localLinearLayout.setVisibility(j);
      setMinimumHeight(-1);
      if (i != 0)
      {
        float f = this.mActivity.getResources().getDimension(R.dimen.s2s_list_top_padding);
        setMinimumHeight(this.mListView.getHeight() - (int)f);
      }
      return;
      this.mWrapperItem.setBackgroundResource(R.drawable.conversation_bubble_right);
      localLayoutParams.setMargins(this.mBubblePaddingPx, 0, 0, 0);
      break;
      label147: i = 0;
      break label60;
      label153: j = 8;
    }
  }

  public void setCallback(Callback paramCallback)
  {
    this.mCallback = paramCallback;
  }

  public static abstract interface Callback
  {
    public abstract void onTts(ConversationHelper.VoiceTranslationEntry paramVoiceTranslationEntry);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.conversation.VoiceTranslateItemView
 * JD-Core Version:    0.6.2
 */