package com.google.android.apps.translate.editor;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Filter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import com.google.android.apps.translate.Language;
import com.google.android.apps.translate.Languages;
import com.google.android.apps.translate.Logger;
import com.google.android.apps.translate.Preconditions;
import com.google.android.apps.translate.Profile;
import com.google.android.apps.translate.R.dimen;
import com.google.android.apps.translate.R.id;
import com.google.android.apps.translate.R.integer;
import com.google.android.apps.translate.TranslateApplication;
import com.google.android.apps.translate.UserActivityMgr;
import com.google.android.apps.translate.UserActivityMgr.IntervalCountTag;
import com.google.android.apps.translate.UserActivityMgr.RequestSource;
import com.google.android.apps.translate.Util;
import com.google.android.apps.translate.asreditor.AsrResultEditor;
import com.google.android.apps.translate.handwriting.HandwritingInputView.HandwritingInputViewCallback;
import com.google.android.apps.translate.history.Entry;
import com.google.android.apps.translate.translation.TranslateEntry;
import java.util.List;
import java.util.Locale;

public class EditPanelView extends LinearLayout
  implements View.OnClickListener, TextView.OnEditorActionListener, InstantTranslateHandler.InstantTranslateListner, View.OnKeyListener, PreImeAutoCompleteTextView.OnKeyPreImeListener, InstantTranslateTextView.Callback, HandwritingInputView.HandwritingInputViewCallback, TextWatcher, SlotView.TouchEventListener
{
  public static final boolean ALWAYS_SHOW_CLEAR_BUTTON_IN_EDITMODE = true;
  private static final boolean SELECT_TEXT_WHEN_EDIT_MODE_STARTS = true;
  private static final String TAG = "EditPanelView";
  private static final boolean TRANSLATE_AFTER_VOICE_INPUT = true;
  private ImageView mAccept;
  private View mAcceptWrapper;
  private Activity mActivity;
  private AsrResultEditor mAsrResultEditor;
  private Callback mCallback;
  private ImageButton mClearButton;
  private InputMethodView mControlPanel;
  private View mControlPanelWrapper;
  private boolean mEditMode = false;
  private TextSlot mEditorField;
  private InstantTranslateHandler mInstantTranslateHandler;
  private View mInstantTranslationDivider;
  private boolean mInternalEditMode = false;
  private boolean mIsConversationMode = false;
  private Languages mLanguageList;
  private String mLatestTranslationSource;
  private String mLatestTranslationTarget;
  private ListView mListView;
  private boolean mLongPressed = false;
  private String mPostEditInitialText;
  private InputMethodView.InputMethod mPostEditInputMethod = InputMethodView.InputMethod.NONE;
  private Language mSourceLanguage;
  private SuggestAdapter mSuggestAdapter;
  private SuggestAdapter.SuggestionFilter mSuggestFilter;
  private SuggestionUpdateHandler mSuggestUpdateHandler = new SuggestionUpdateHandler(null);
  private Language mTargetLanguage;
  private InstantTranslateTextView mTranslate;
  private ProgressBar mTranslateProgressBar;
  private String mVoiceLocale;
  private final int sMaxLines;

  public EditPanelView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    Logger.d("EditPanelView", "EditPanelView");
    this.sMaxLines = getResources().getInteger(R.integer.InputMaxLines);
  }

  private void clearTexts()
  {
    try
    {
      Logger.d("EditPanelView", "clearTexts");
      this.mEditorField.setText("");
      this.mTranslate.setText("");
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  private void handleTapOnInputTextBox()
  {
    if (!isEditMode())
    {
      this.mCallback.onEditModeStart(this, InputMethodView.InputMethod.KEYBOARD);
      return;
    }
    if (!hasInputMethodShown())
    {
      this.mControlPanel.startInputMethod(InputMethodView.InputMethod.KEYBOARD);
      return;
    }
    switch (7.$SwitchMap$com$google$android$apps$translate$editor$InputMethodView$InputMethod[this.mControlPanel.getCurrentInputMethod().ordinal()])
    {
    default:
      return;
    case 1:
      this.mControlPanel.startInputMethod(InputMethodView.InputMethod.KEYBOARD);
      return;
    case 2:
    }
    this.mControlPanel.cancel();
  }

  private void onSuggestSelected(final SuggestAdapter.SuggestEntry paramSuggestEntry)
  {
    final String str = paramSuggestEntry.getEntry().getInputText();
    Logger.d("EditPanelView", "onSuggestSelected inputText=" + str);
    this.mActivity.runOnUiThread(new Runnable()
    {
      public void run()
      {
        EditPanelView.this.mControlPanel.cancel();
        EditPanelView.this.mSuggestUpdateHandler.update(str, null, null, true);
        if (paramSuggestEntry.getType() == SuggestAdapter.SuggestType.TWS_DETECTED_SRCLANG)
          EditPanelView.this.mCallback.onSourceLanguageChangeRequested(EditPanelView.this.mLanguageList.getFromLanguageByShortName(paramSuggestEntry.getEntry().getInputText()), paramSuggestEntry.getSourceText());
        while (true)
        {
          SuggestAdapter.logSuggestionClick(EditPanelView.this.mActivity, paramSuggestEntry, true);
          return;
          EditPanelView.this.mEditorField.replaceTextWithSelection(str);
        }
      }
    });
  }

  private void updateImeSuggestions()
  {
    Logger.d("EditPanelView", "updateImeSuggestions");
    if (Locale.getDefault().getLanguage().equals(this.mSourceLanguage.getShortName()))
    {
      this.mEditorField.setInputType(0xFFFEFFFF & this.mEditorField.getInputType());
      return;
    }
    this.mEditorField.setInputType(0x10000 | this.mEditorField.getInputType());
  }

  public void afterTextChanged(Editable paramEditable)
  {
    Logger.d("EditPanelView", "afterTextChanged text=" + paramEditable.toString());
    if (this.mEditorField.isInternalEditMode())
      return;
    if ((!TextUtils.isEmpty(paramEditable.toString())) && (!this.mInternalEditMode) && (!isEditMode()))
      enableEditMode(InputMethodView.InputMethod.NONE);
    String str = paramEditable.toString().trim();
    this.mSuggestUpdateHandler.update(str, null, null, false);
    updateButtons(str);
  }

  public void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
  {
  }

  public void disableEditMode(boolean paramBoolean, InputMethodView.InputMethodEvent paramInputMethodEvent)
  {
    Logger.d("EditPanelView", "disableEditMode");
    disableSuggestions();
    this.mEditorField.setMaxLines(this.sMaxLines);
    this.mEditorField.setIsTextEditor(true);
    this.mEditMode = false;
    this.mControlPanel.hideCurrentInputMethod(paramInputMethodEvent);
    InstantTranslateTextView localInstantTranslateTextView = this.mTranslate;
    if (paramBoolean);
    for (int i = 8; ; i = 0)
    {
      localInstantTranslateTextView.setVisibility(i);
      if (!this.mInstantTranslateHandler.isStopped())
        UserActivityMgr.get().setImpression(UserActivityMgr.IntervalCountTag.INSTANT_TRANSLATION_SHOWN, false);
      this.mInstantTranslateHandler.stop();
      if (this.mIsConversationMode)
      {
        this.mControlPanelWrapper.setVisibility(8);
        this.mAcceptWrapper.setVisibility(8);
        this.mClearButton.setVisibility(8);
      }
      this.mInstantTranslationDivider.setVisibility(8);
      return;
    }
  }

  public boolean disableSuggestions()
  {
    Logger.d("EditPanelView", "disableSuggestions");
    if (this.mListView != null)
    {
      this.mListView.setVisibility(8);
      this.mListView.setAdapter(null);
      return true;
    }
    return false;
  }

  public void enableEditMode(InputMethodView.InputMethod paramInputMethod)
  {
    Logger.d("EditPanelView", "enableEditMode");
    this.mEditMode = true;
    this.mEditorField.setMaxLines(2147483647);
    this.mEditorField.setIsTextEditor(true);
    initInstantTranslation(true);
    this.mInstantTranslationDivider.setVisibility(0);
    this.mControlPanelWrapper.setVisibility(0);
    this.mAcceptWrapper.setVisibility(0);
    this.mClearButton.setVisibility(0);
    this.mControlPanel.startInputMethod(paramInputMethod);
    InputMethodView.logInputMethod(paramInputMethod);
    this.mPostEditInputMethod = InputMethodView.InputMethod.NONE;
    if (this.mIsConversationMode)
      UserActivityMgr.get().setTranslationSource(UserActivityMgr.RequestSource.CONV);
    while (true)
    {
      hideTranslationLoading();
      updateButtons(this.mEditorField.getText().toString());
      if (!this.mInstantTranslateHandler.isStopped())
        UserActivityMgr.get().setImpression(UserActivityMgr.IntervalCountTag.INSTANT_TRANSLATION_SHOWN, true);
      return;
      UserActivityMgr.get().setTranslationSource(UserActivityMgr.RequestSource.EDIT);
    }
  }

  public boolean enableSuggestions()
  {
    Logger.d("EditPanelView", "enableSuggestions");
    ListView localListView = this.mListView;
    boolean bool = false;
    if (localListView != null)
    {
      if (this.mListView.getAdapter() == null)
        this.mListView.setAdapter(this.mSuggestAdapter);
      this.mListView.setVisibility(0);
      bool = true;
    }
    return bool;
  }

  public void endInternalEdit()
  {
    this.mInternalEditMode = false;
  }

  public Language getSourceLanguage()
  {
    return this.mSourceLanguage;
  }

  public int getSuggestTextViewCount()
  {
    int i = this.mControlPanel.getEditingAreaViewHeight();
    int j = (int)getResources().getDimension(R.dimen.suggest_text_box_height);
    if (j == 0)
      return 0;
    Logger.d("EditPanelView", "getSuggestTextViewCount editingAreaHeight=" + i);
    Logger.d("EditPanelView", "getSuggestTextViewCount suggestTextViewHeight=" + j);
    return i / j;
  }

  public Language getTargetLanguage()
  {
    return this.mTargetLanguage;
  }

  public String getTranslationText(String paramString)
  {
    if (TextUtils.equals(paramString, this.mLatestTranslationSource))
    {
      if (Logger.isDebug())
        Logger.d("EditPanelView", "getTranslationText: Use translation result in handwriting response. translation=[" + this.mLatestTranslationTarget + "]");
      return this.mLatestTranslationTarget;
    }
    if (Logger.isDebug())
      Logger.d("EditPanelView", "getTranslationText: Try to use instant translation result. translation=[" + this.mInstantTranslateHandler.getTranslationText(paramString) + "]");
    return this.mInstantTranslateHandler.getTranslationText(paramString);
  }

  public boolean hasInputMethodShown()
  {
    return this.mControlPanel.hasInputMethodShown();
  }

  public boolean hideCurrentInputMethod(InputMethodView.InputMethodEvent paramInputMethodEvent)
  {
    Logger.d("EditPanelView", "hideCurrentInputMethod");
    return this.mControlPanel.hideCurrentInputMethod(paramInputMethodEvent);
  }

  public void hideTranslationLoading()
  {
    Logger.d("EditPanelView", "hideTranslationLoading");
    this.mActivity.runOnUiThread(new Runnable()
    {
      public void run()
      {
        EditPanelView.this.mTranslateProgressBar.setVisibility(4);
        EditPanelView.this.mAccept.setVisibility(0);
      }
    });
  }

  public void init(Activity paramActivity, Languages paramLanguages, Language paramLanguage1, Language paramLanguage2, String paramString, boolean paramBoolean)
  {
    this.mActivity = paramActivity;
    SuggestionUpdateHandler localSuggestionUpdateHandler = this.mSuggestUpdateHandler;
    if ((this.mSourceLanguage != null) && (!this.mSourceLanguage.equals(paramLanguage1)) && (!this.mEditorField.isEmpty()));
    for (boolean bool = true; ; bool = false)
    {
      localSuggestionUpdateHandler.setUserExplicitlySetSourceLanguage(bool);
      this.mSourceLanguage = paramLanguage1;
      this.mSuggestUpdateHandler.setSourceLanguage(this.mSourceLanguage);
      this.mTargetLanguage = paramLanguage2;
      this.mVoiceLocale = paramString;
      this.mLanguageList = paramLanguages;
      Preconditions.checkNotNull(this.mLanguageList);
      this.mIsConversationMode = paramBoolean;
      this.mSuggestUpdateHandler.setIsConversationMode(this.mIsConversationMode);
      this.mInstantTranslateHandler = ((TranslateApplication)this.mActivity.getApplication()).getInstantTranslateHandler();
      this.mEditorField.setInstantTranslateHandler(this.mInstantTranslateHandler);
      InputMethodView.InputMethod localInputMethod = this.mControlPanel.getCurrentInputMethod();
      initInstantTranslation(true);
      this.mControlPanel.init(this.mActivity, this.mInstantTranslateHandler, this.mSourceLanguage, this.mTargetLanguage, this.mEditorField, this.mAsrResultEditor, this.mVoiceLocale, this);
      if (!this.mIsConversationMode)
        this.mControlPanel.restartInputMethod(localInputMethod);
      updateImeSuggestions();
      this.mSuggestFilter = new SuggestAdapter.SuggestionFilter()
      {
        public List<SuggestAdapter.SuggestEntry> filter(List<SuggestAdapter.SuggestEntry> paramAnonymousList)
        {
          Logger.d("EditPanelView", "filter entries.size()=" + paramAnonymousList.size());
          String str = TranslateEntry.normalizeInputText(EditPanelView.this.mEditorField.getText().toString());
          if (TextUtils.isEmpty(str))
            paramAnonymousList = null;
          SuggestAdapter.SuggestEntry localSuggestEntry;
          do
          {
            do
            {
              do
                return paramAnonymousList;
              while ((paramAnonymousList == null) || (paramAnonymousList.size() != 1));
              localSuggestEntry = (SuggestAdapter.SuggestEntry)paramAnonymousList.get(0);
              if (localSuggestEntry.getType() != SuggestAdapter.SuggestType.TWS_DETECTED_SRCLANG)
                break;
            }
            while (TextUtils.equals(str, localSuggestEntry.getSourceText()));
            return null;
          }
          while (!TextUtils.equals(str, localSuggestEntry.getEntry().getInputText()));
          return null;
        }
      };
      this.mSuggestAdapter = new SuggestAdapter(this.mActivity, this.mLanguageList, this.mSourceLanguage, this.mTargetLanguage, this.mSuggestFilter, this.mEditorField);
      this.mSuggestUpdateHandler.setSuggestAdapter(this.mSuggestAdapter);
      disableSuggestions();
      if (this.mListView != null)
      {
        this.mListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
          public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
          {
            Logger.d("EditPanelView", "mListView.setOnItemClickListener position=" + paramAnonymousInt);
            EditPanelView.this.onSuggestSelected((SuggestAdapter.SuggestEntry)paramAnonymousAdapterView.getItemAtPosition(paramAnonymousInt));
          }
        });
        this.mListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener()
        {
          public boolean onItemLongClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
          {
            Logger.d("EditPanelView", "mListView.setOnItemClickListener position=" + paramAnonymousInt);
            SuggestAdapter.SuggestEntry localSuggestEntry = (SuggestAdapter.SuggestEntry)paramAnonymousAdapterView.getItemAtPosition(paramAnonymousInt);
            if (localSuggestEntry.getHtmlInputText() != null)
              Util.showLongToastMessage(EditPanelView.this.mActivity, Html.fromHtml(localSuggestEntry.getHtmlInputText()));
            while (true)
            {
              return true;
              Util.showLongToastMessage(EditPanelView.this.mActivity, localSuggestEntry.getEntry().getInputText());
            }
          }
        });
      }
      updateButtons(this.mEditorField.getText().toString());
      return;
    }
  }

  public void initInstantTranslation(boolean paramBoolean)
  {
    if ((this.mEditMode) && (paramBoolean) && (Profile.getInstantTranslation(this.mActivity)))
    {
      this.mInstantTranslateHandler.init(this.mActivity, this, this.mSourceLanguage, this.mTargetLanguage);
      this.mEditorField.forceInstantTranslation();
      this.mTranslate.setVisibility(0);
    }
    while (true)
    {
      invalidate();
      this.mEditorField.invalidate();
      return;
      this.mTranslate.setVisibility(8);
      this.mInstantTranslateHandler.stop();
    }
  }

  public boolean isEditMode()
  {
    return this.mEditMode;
  }

  public void onAccept(boolean paramBoolean)
  {
    String str = this.mEditorField.getText().toString();
    this.mEditorField.setSelection(str.length());
    if ((this.mPostEditInitialText != null) && (!this.mPostEditInitialText.equals(str)))
      UserActivityMgr.get().setTranslationSource(UserActivityMgr.RequestSource.CONV_EDIT);
    if (str.trim().length() == 0)
      paramBoolean = false;
    if (paramBoolean)
      showTranslationLoading();
    this.mCallback.onAccept(getId(), paramBoolean);
  }

  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    Logger.d("EditPanelView", "onActivityResult");
    if (this.mControlPanel != null)
      this.mControlPanel.onActivityResult(paramInt1, paramInt2, paramIntent);
  }

  public void onClick(View paramView)
  {
    Logger.d("EditPanelView", "onClick");
    int i = paramView.getId();
    if (i == R.id.btn_confirm_view_accept)
    {
      this.mControlPanel.hideCurrentInputMethod(InputMethodView.InputMethodEvent.ACCEPT);
      onAccept(true);
    }
    boolean bool;
    do
    {
      do
        return;
      while (i != R.id.btn_confirm_view_clear);
      bool = this.mEditorField.isEmpty();
      clearTexts();
      if (this.mInstantTranslateHandler != null)
        this.mInstantTranslateHandler.reset();
      this.mControlPanel.onClearText();
      this.mControlPanel.cancel();
    }
    while ((!isEditMode()) || ((this.mControlPanel.hasInputMethodShown()) && (!bool)));
    onAccept(false);
  }

  public void onConfigurationChanged(Configuration paramConfiguration)
  {
    if (Util.getSdkVersion() >= 8)
      super.onConfigurationChanged(paramConfiguration);
    if (this.mControlPanel != null)
      this.mControlPanel.onConfigurationChanged(paramConfiguration);
  }

  public void onConfirm()
  {
    UserActivityMgr.get().incrementIntervalCount(UserActivityMgr.IntervalCountTag.INSTANT_TRANSLATION_CLICKED, 1);
    this.mControlPanel.hideCurrentInputMethod(InputMethodView.InputMethodEvent.ACCEPT);
    onAccept(true);
  }

  public void onDestroy()
  {
    this.mControlPanel.onDestroy();
  }

  public boolean onDoubleTap(MotionEvent paramMotionEvent)
  {
    return false;
  }

  public boolean onDoubleTapEvent(MotionEvent paramMotionEvent)
  {
    return false;
  }

  public boolean onDown(MotionEvent paramMotionEvent)
  {
    return false;
  }

  public void onEditCompleted(String paramString1, String paramString2)
  {
    this.mLatestTranslationSource = paramString1;
    this.mLatestTranslationTarget = paramString2;
  }

  public void onEditPanelHeightChanged(int paramInt)
  {
    this.mControlPanel.onEditPanelHeightChanged(paramInt);
  }

  public boolean onEditorAction(TextView paramTextView, int paramInt, KeyEvent paramKeyEvent)
  {
    Logger.d("EditPanelView", "onEditorAction actionId=" + paramInt);
    if (paramKeyEvent != null)
      Logger.d("EditPanelView", "onEditorAction event.getAction()=" + paramKeyEvent.getAction());
    if (((paramKeyEvent != null) && (paramKeyEvent.getAction() == 1)) || (paramInt == 6))
    {
      this.mControlPanel.hideCurrentInputMethod(InputMethodView.InputMethodEvent.ACCEPT);
      onAccept(true);
    }
    return false;
  }

  protected void onFinishInflate()
  {
    super.onFinishInflate();
    this.mAsrResultEditor = ((AsrResultEditor)findViewById(R.id.msg_confirm_content));
    this.mEditorField = this.mAsrResultEditor.getEditorField();
    this.mAccept = ((ImageView)findViewById(R.id.btn_confirm_view_accept));
    this.mAccept.setOnClickListener(this);
    this.mAcceptWrapper = findViewById(R.id.frame_accept);
    this.mClearButton = ((ImageButton)findViewById(R.id.btn_confirm_view_clear));
    this.mClearButton.setOnClickListener(this);
    this.mInstantTranslationDivider = findViewById(R.id.instant_translation_divider);
    this.mTranslate = ((InstantTranslateTextView)findViewById(R.id.edit_panel_instant_translate));
    this.mTranslate.setCallback(this);
    this.mTranslateProgressBar = ((ProgressBar)findViewById(R.id.translation_loading));
    this.mEditorField.setCursorVisible(true);
    this.mEditorField.setKeyPreImeListener(this);
    this.mEditorField.setOnEditorActionListener(this);
    this.mEditorField.setOnKeyListener(this);
  }

  public boolean onFling(MotionEvent paramMotionEvent1, MotionEvent paramMotionEvent2, float paramFloat1, float paramFloat2)
  {
    return false;
  }

  public void onInputMethodReady(InputMethodView.InputMethod paramInputMethod)
  {
    Logger.d("EditPanelView", "onInputMethodReady");
    disableSuggestions();
    invalidate();
    this.mCallback.onEditModeReady(paramInputMethod);
    if ((!this.mLongPressed) && (!this.mIsConversationMode) && ((paramInputMethod == InputMethodView.InputMethod.CAMERA) || (paramInputMethod == InputMethodView.InputMethod.HANDWRITING) || (paramInputMethod == InputMethodView.InputMethod.KEYBOARD)))
      this.mEditorField.selectAll();
    enableSuggestions();
    if (this.mPostEditInputMethod == InputMethodView.InputMethod.NONE)
    {
      InputMethodView.logInputMethod(paramInputMethod);
      this.mPostEditInputMethod = paramInputMethod;
    }
  }

  public boolean onInputMethodStart(InputMethodView.InputMethod paramInputMethod)
  {
    if (!isEditMode())
    {
      Logger.d("EditPanelView", "onInputMethodStart NOT-EDIT");
      this.mCallback.onEditModeStart(this, paramInputMethod);
      return true;
    }
    Logger.d("EditPanelView", "onInputMethodStart EDIT");
    disableSuggestions();
    return false;
  }

  public boolean onKey(View paramView, int paramInt, KeyEvent paramKeyEvent)
  {
    Logger.d("EditPanelView", "onKey KeyEvent=" + paramKeyEvent.toString());
    if ((paramKeyEvent != null) && (paramInt == 66))
    {
      Logger.d("EditPanelView", "onKey ENTER");
      if ((paramKeyEvent.getAction() == 1) && (isEditMode()))
      {
        this.mControlPanel.hideCurrentInputMethod(InputMethodView.InputMethodEvent.ACCEPT);
        onAccept(true);
      }
      return true;
    }
    return false;
  }

  public boolean onKeyPreIme(int paramInt, KeyEvent paramKeyEvent)
  {
    Logger.d("EditPanelView", "onKeyPreIme keyCode=" + paramInt);
    if ((paramKeyEvent != null) && (paramInt == 4) && (isEditMode()))
    {
      if (paramKeyEvent.getAction() == 0)
      {
        if (hasInputMethodShown())
        {
          this.mControlPanel.startInputMethod(InputMethodView.InputMethod.NONE);
          if (this.mIsConversationMode);
          do
          {
            return true;
            if (TextUtils.isEmpty(this.mEditorField.getText().toString()))
            {
              onAccept(false);
              return true;
            }
          }
          while (this.mSuggestAdapter.getCount() != 0);
          onAccept(false);
          return true;
        }
        onAccept(false);
        Logger.d("EditPanelView", "onKeyPreIme ACCEPT");
        return true;
      }
      Logger.d("EditPanelView", "onKeyPreIme FORWARD_BACK");
      return true;
    }
    return this.mControlPanel.onKeyPreIme(paramInt, paramKeyEvent);
  }

  public void onLongPause(SpannableStringBuilder paramSpannableStringBuilder1, SpannableStringBuilder paramSpannableStringBuilder2, SpannableStringBuilder paramSpannableStringBuilder3, String paramString)
  {
    Logger.d("EditPanelView", "onLongPause");
    this.mSuggestUpdateHandler.update(this.mEditorField.getText().toString(), paramSpannableStringBuilder3, paramString, false);
  }

  public void onLongPress(MotionEvent paramMotionEvent)
  {
    this.mLongPressed = true;
    handleTapOnInputTextBox();
  }

  public void onNonStreamingVoiceResult(String paramString)
  {
    Logger.d("EditPanelView", "onNonStreamingVoiceResult recognitionResult=" + paramString);
    String str;
    if (paramString != null)
    {
      str = this.mEditorField.replaceSelectedText(paramString, false);
      if (this.mIsConversationMode)
        break label63;
      this.mControlPanel.hideCurrentInputMethod(InputMethodView.InputMethodEvent.ACCEPT);
      onAccept(true);
    }
    label63: 
    while ((this.mPostEditInitialText != null) || (!this.mIsConversationMode))
      return;
    this.mPostEditInitialText = str;
  }

  public void onPause()
  {
    if (this.mControlPanel.onPause());
    while (!isEditMode())
      return;
    disableEditMode(true, InputMethodView.InputMethodEvent.PAUSE);
  }

  public boolean onScroll(MotionEvent paramMotionEvent1, MotionEvent paramMotionEvent2, float paramFloat1, float paramFloat2)
  {
    return false;
  }

  public void onShowPress(MotionEvent paramMotionEvent)
  {
  }

  public boolean onSingleTapConfirmed(MotionEvent paramMotionEvent)
  {
    this.mLongPressed = false;
    handleTapOnInputTextBox();
    return false;
  }

  public boolean onSingleTapUp(MotionEvent paramMotionEvent)
  {
    return false;
  }

  protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    if ((paramInt2 <= 0) && (isEditMode()) && (this.mControlPanel.getHeight() > 0) && (this.mControlPanel.getCurrentInputMethod() == InputMethodView.InputMethod.HANDWRITING))
    {
      Logger.e("EditPanelView", "Forcibly switch to keyboard. edit_panel_height=" + paramInt2 + " input_method_panel_height=" + this.mControlPanel.getHeight());
      this.mControlPanel.startInputMethod(InputMethodView.InputMethod.KEYBOARD);
    }
  }

  public void onSourceTextDone(SpannableStringBuilder paramSpannableStringBuilder)
  {
  }

  public void onSourceTextUpdate(SpannableStringBuilder paramSpannableStringBuilder)
  {
  }

  public void onTargetTextDone(SpannableStringBuilder paramSpannableStringBuilder)
  {
    this.mTranslate.setText(paramSpannableStringBuilder);
  }

  public void onTargetTextUpdate(SpannableStringBuilder paramSpannableStringBuilder1, SpannableStringBuilder paramSpannableStringBuilder2, SpannableStringBuilder paramSpannableStringBuilder3, String paramString)
  {
    this.mSuggestUpdateHandler.update(this.mEditorField.getText().toString(), paramSpannableStringBuilder3, paramString, false);
    String str = this.mTranslate.getText().toString();
    this.mTranslate.setText(paramSpannableStringBuilder2);
    this.mTranslate.scrollToEdge(str, this.mEditorField, false);
  }

  public void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
  {
    if (Logger.isDebug())
      Logger.d("EditPanelView", "onTextChanged s=" + paramCharSequence + " start=" + paramInt1 + " before=" + paramInt2 + " count=" + paramInt3);
  }

  public void onTranslationDone()
  {
    Logger.d("EditPanelView", "onTranslationDone");
    hideTranslationLoading();
  }

  public void resetSuggestions()
  {
    if (this.mListView != null)
    {
      this.mListView.setAdapter(this.mSuggestAdapter);
      this.mListView.setVisibility(0);
      this.mSuggestAdapter.getFilter().filter(this.mEditorField.getText().toString().trim());
    }
  }

  public void setCallback(Callback paramCallback)
  {
    Logger.d("EditPanelView", "setCallback");
    this.mCallback = paramCallback;
  }

  public void setInputMethodView(InputMethodView paramInputMethodView)
  {
    this.mControlPanel = paramInputMethodView;
    this.mControlPanelWrapper = this.mControlPanel.getRootView().findViewById(R.id.input_method_view_wrapper);
  }

  public void setLanguageList(Languages paramLanguages)
  {
    this.mLanguageList = paramLanguages;
  }

  public void setListView(ListView paramListView)
  {
    this.mListView = paramListView;
  }

  public void showTranslationLoading()
  {
    Logger.d("EditPanelView", "showTranslationLoading");
    this.mActivity.runOnUiThread(new Runnable()
    {
      public void run()
      {
        EditPanelView.this.mTranslateProgressBar.setVisibility(0);
        EditPanelView.this.mAccept.setVisibility(4);
      }
    });
  }

  public void startInternalEdit()
  {
    this.mInternalEditMode = true;
  }

  public void updateButtons(String paramString)
  {
    Logger.d("EditPanelView", "updateButtons text=" + paramString);
    if (!TextUtils.isEmpty(paramString.trim()));
    for (boolean bool1 = true; ; bool1 = false)
    {
      ImageButton localImageButton = this.mClearButton;
      boolean bool2;
      if ((!bool1) && (!isEditMode()))
      {
        boolean bool3 = this.mCallback.hasSomethingToClear();
        bool2 = false;
        if (!bool3);
      }
      else
      {
        bool2 = true;
      }
      localImageButton.setEnabled(bool2);
      this.mAccept.setEnabled(bool1);
      return;
    }
  }

  public static abstract interface Callback
  {
    public abstract boolean hasSomethingToClear();

    public abstract void onAccept(int paramInt, boolean paramBoolean);

    public abstract void onEditModeReady(InputMethodView.InputMethod paramInputMethod);

    public abstract void onEditModeStart(EditPanelView paramEditPanelView, InputMethodView.InputMethod paramInputMethod);

    public abstract void onSourceLanguageChangeRequested(Language paramLanguage, String paramString);
  }

  private static class SuggestionUpdateHandler extends Handler
  {
    private static final int MSG_UPDATE_SUGGESTIONS = 0;
    private static final long UPDATE_SUGGESTIONS_MILLIS = 500L;
    private boolean mIsConversationMode = false;
    private Language mSourceLanguage;
    private SuggestAdapter mSuggestAdapter;
    private boolean mUserExplicitlySetSourceLanguage = true;

    private void filterSuggestions(String paramString1, SpannableStringBuilder paramSpannableStringBuilder, String paramString2)
    {
      boolean bool;
      if (this.mSuggestAdapter != null)
      {
        if (paramSpannableStringBuilder == null)
          break label96;
        bool = this.mSuggestAdapter.setSpellCorrection(paramSpannableStringBuilder, paramString1);
        if ((bool) || (this.mIsConversationMode) || (this.mUserExplicitlySetSourceLanguage) || (this.mSourceLanguage.getShortName().equals(paramString2)))
          break label102;
        this.mSuggestAdapter.setDetectedSrcShortLangName(paramString2, paramString1);
      }
      while (true)
      {
        this.mSuggestAdapter.notifyDataSetChanged();
        this.mSuggestAdapter.getFilter().filter(paramString1);
        if (TextUtils.isEmpty(paramString1))
          this.mUserExplicitlySetSourceLanguage = false;
        return;
        label96: bool = false;
        break;
        label102: this.mSuggestAdapter.setDetectedSrcShortLangName(null, paramString1);
      }
    }

    public void handleMessage(Message paramMessage)
    {
      switch (paramMessage.what)
      {
      default:
        if (Logger.isDebug())
          throw new IllegalArgumentException("Invalid message=" + paramMessage.what);
        break;
      case 0:
        SuggestionUpdateData localSuggestionUpdateData = (SuggestionUpdateData)paramMessage.obj;
        filterSuggestions(localSuggestionUpdateData.mInputText, localSuggestionUpdateData.mSpellCorrection, localSuggestionUpdateData.mDetectedSrcShortLangName);
      }
    }

    public void setIsConversationMode(boolean paramBoolean)
    {
      this.mIsConversationMode = paramBoolean;
    }

    public void setSourceLanguage(Language paramLanguage)
    {
      this.mSourceLanguage = paramLanguage;
    }

    public void setSuggestAdapter(SuggestAdapter paramSuggestAdapter)
    {
      this.mSuggestAdapter = paramSuggestAdapter;
    }

    public void setUserExplicitlySetSourceLanguage(boolean paramBoolean)
    {
      this.mUserExplicitlySetSourceLanguage = paramBoolean;
    }

    public void update(String paramString1, SpannableStringBuilder paramSpannableStringBuilder, String paramString2, boolean paramBoolean)
    {
      Logger.d("EditPanelView", "update text=" + paramString1);
      String str = TranslateEntry.normalizeInputText(paramString1);
      removeMessages(0);
      if ((paramBoolean) || (TextUtils.isEmpty(str)))
      {
        filterSuggestions(str, paramSpannableStringBuilder, paramString2);
        return;
      }
      SuggestionUpdateData localSuggestionUpdateData = new SuggestionUpdateData(null);
      localSuggestionUpdateData.mInputText = str;
      localSuggestionUpdateData.mSpellCorrection = paramSpannableStringBuilder;
      localSuggestionUpdateData.mDetectedSrcShortLangName = paramString2;
      sendMessageDelayed(obtainMessage(0, localSuggestionUpdateData), 500L);
    }

    private static class SuggestionUpdateData
    {
      String mDetectedSrcShortLangName;
      String mInputText;
      SpannableStringBuilder mSpellCorrection;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.editor.EditPanelView
 * JD-Core Version:    0.6.2
 */