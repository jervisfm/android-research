package com.google.android.apps.translate.editor;

import android.content.Context;
import android.text.style.TextAppearanceSpan;
import com.google.android.apps.translate.R.style;

public class HighlightSpan extends TextAppearanceSpan
{
  public HighlightSpan(Context paramContext)
  {
    super(paramContext, R.style.correctable_highlight);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.editor.HighlightSpan
 * JD-Core Version:    0.6.2
 */