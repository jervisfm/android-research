package com.google.android.apps.translate.editor;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.Uri;
import android.provider.MediaStore.Images.Media;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.google.android.apps.translate.Language;
import com.google.android.apps.translate.Logger;
import com.google.android.apps.translate.Profile;
import com.google.android.apps.translate.R.drawable;
import com.google.android.apps.translate.R.id;
import com.google.android.apps.translate.R.layout;
import com.google.android.apps.translate.R.string;
import com.google.android.apps.translate.RecognitionView;
import com.google.android.apps.translate.TranslateApplication;
import com.google.android.apps.translate.UserActivityMgr;
import com.google.android.apps.translate.UserActivityMgr.InputMethod;
import com.google.android.apps.translate.Util;
import com.google.android.apps.translate.VoiceInput;
import com.google.android.apps.translate.VoiceInput.UiListener;
import com.google.android.apps.translate.VoiceInputHelper;
import com.google.android.apps.translate.asreditor.AsrResultEditor;
import com.google.android.apps.translate.handwriting.HandwritingInputView;
import com.google.android.apps.unveil.textinput.TextInput.ClientType;
import com.google.android.apps.unveil.textinput.TextInputView.Listener;
import com.google.android.apps.unveil.textinput.compatible.TextInputView;
import com.google.research.handwriting.gui.CandidateViewHandler;
import java.util.List;
import java.util.Locale;

public class InputMethodView extends LinearLayout
  implements VoiceInput.UiListener, TextInputView.Listener
{
  private static final boolean ENABLE_CAMERA_FOR_AUTO_DETECT = false;
  private static final boolean HIDE_INPUT_SELECTOR = true;
  private static final boolean SHOW_KEYBOARD = false;
  public static final int SLIDE_ANIMATION_MILLISECONDS = 150;
  private static final String TAG = "InputMethodView";
  private static final boolean USE_STREAMING_VOICE_INPUT;
  private InputMethod mActiveInputMethod = InputMethod.NONE;
  private Activity mActivity;
  private AsrResultEditor mAsrResultEditor;
  private ImageButton mCameraBtn;
  private int mCameraHeight;
  private TextInputView mCameraInputView = null;
  private TextInputView[] mCameraInputViews = new TextInputView[2];
  private boolean mCameraSupported = false;
  private EditPanelView mEditPanel;
  private TextSlot mEditorField;
  private ImageButton mHandwritingBtn;
  private HandwritingInputView mHandwritingInputView = null;
  private int mIconCount = 0;
  private LinearLayout mInputMethodPlaceholder;
  private View mInputSelector;
  private InstantTranslateHandler mInstantTranslateHandler;
  private ImageButton mKeyboardBtn;
  private boolean mLoadingImage = false;
  private ImageButton mMicBtn;
  private Language mPreviousSourceLanguage;
  private Language mPreviousTargetLanguage;
  private Language mSourceLanguage;
  private InputMethod mStartedInputMethod = InputMethod.NONE;
  private RecognitionView mStreamingView;
  private Language mTargetLanguage;
  private View mTranslateView;
  private VoiceInput mVoiceInput;
  private VoiceInputHelper mVoiceInputHelper;
  private String mVoiceLocale;

  public InputMethodView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }

  private void changeCameraLanguage()
  {
    if (Util.isAutoDetectLanguage(this.mSourceLanguage))
    {
      this.mCameraInputView.changeSourceLanguage(null);
      return;
    }
    this.mCameraInputView.changeSourceLanguage(this.mSourceLanguage.getShortName());
  }

  private void clearSelection()
  {
    this.mKeyboardBtn.setImageResource(R.drawable.input_keyboard);
    this.mMicBtn.setImageResource(R.drawable.input_voice);
    this.mHandwritingBtn.setImageResource(R.drawable.input_handwriting);
  }

  private String getCameraInputUserAgent()
  {
    String str = "";
    try
    {
      str = this.mActivity.getPackageManager().getPackageInfo(this.mActivity.getApplicationInfo().packageName, 0).versionName;
      return String.format((Locale)null, "GoogleGoggles-AndroidTranslateTextSearch/%s", new Object[] { str });
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
      while (true)
        Logger.e("InputMethodView", "Unable to retrieve version code from manifest.");
    }
  }

  private void hideInputSelector(boolean paramBoolean)
  {
    Logger.d("InputMethodView", "hideInputSelector");
    if (paramBoolean)
    {
      slideDown(this.mInputSelector);
      return;
    }
    this.mInputSelector.setVisibility(8);
  }

  private void hideSoftwareKeyboard()
  {
    Logger.d("InputMethodView", "hideSoftInputFromWindow");
    ((InputMethodManager)this.mActivity.getSystemService("input_method")).hideSoftInputFromWindow(this.mEditorField.getWindowToken(), 0);
  }

  private void initializeCameraInputView(LayoutInflater paramLayoutInflater)
  {
    Logger.d("InputMethodView", "initializeCameraInputView");
    FrameLayout localFrameLayout = (FrameLayout)getRootView();
    if (getContext().getResources().getConfiguration().orientation == 2);
    for (int i = 1; ; i = 0)
    {
      if (this.mCameraInputViews[i] == null)
        this.mCameraInputViews[i] = ((TextInputView)paramLayoutInflater.inflate(R.layout.camera_text_input_view, null));
      if (this.mCameraInputViews[i] != this.mCameraInputView)
      {
        if (this.mCameraInputView != null)
          ((ViewGroup)this.mCameraInputView.getParent()).removeView(this.mCameraInputView);
        this.mCameraInputView = this.mCameraInputViews[i];
        localFrameLayout.addView(this.mCameraInputView, 0);
      }
      this.mCameraInputView.setListener(this);
      this.mCameraInputView.setClientType(TextInput.ClientType.TRANSLATE);
      this.mCameraInputView.setUserAgent(getCameraInputUserAgent());
      return;
    }
  }

  private void initializeHandwritingInputView(LayoutInflater paramLayoutInflater)
  {
    if (this.mHandwritingInputView == null)
    {
      CandidateViewHandler localCandidateViewHandler = new CandidateViewHandler(paramLayoutInflater);
      this.mHandwritingInputView = ((HandwritingInputView)paramLayoutInflater.inflate(R.layout.handwriting_input_view, null));
      this.mHandwritingInputView.initialize(localCandidateViewHandler, this.mEditorField, this);
      this.mHandwritingInputView.setCallback(this.mEditPanel);
      return;
    }
    this.mHandwritingInputView.initializeEditText(this.mEditorField);
    this.mHandwritingInputView.setVisibility(0);
  }

  private void initializeVoiceInputView(LayoutInflater paramLayoutInflater)
  {
    this.mVoiceInputHelper = ((TranslateApplication)this.mActivity.getApplication()).getVoiceInputHelper();
  }

  static void logInputMethod(InputMethod paramInputMethod)
  {
    UserActivityMgr.InputMethod localInputMethod;
    switch (17.$SwitchMap$com$google$android$apps$translate$editor$InputMethodView$InputMethod[paramInputMethod.ordinal()])
    {
    default:
      localInputMethod = UserActivityMgr.InputMethod.UNKNOWN;
    case 2:
    case 3:
    case 1:
    case 4:
    }
    while (true)
    {
      if (localInputMethod != UserActivityMgr.InputMethod.UNKNOWN)
        UserActivityMgr.get().setTranslationInputMethod(localInputMethod);
      return;
      localInputMethod = UserActivityMgr.InputMethod.SPEECH;
      continue;
      localInputMethod = UserActivityMgr.InputMethod.HANDWRITING;
      continue;
      localInputMethod = UserActivityMgr.InputMethod.VIRTUAL_KEYBOARD;
      continue;
      localInputMethod = UserActivityMgr.InputMethod.CAMERA;
    }
  }

  private void render()
  {
    Logger.d("InputMethodView", "render");
    this.mInputMethodPlaceholder.removeAllViews();
    switch (17.$SwitchMap$com$google$android$apps$translate$editor$InputMethodView$InputMethod[this.mActiveInputMethod.ordinal()])
    {
    case 2:
    default:
    case 1:
    case 3:
      do
      {
        return;
        ViewGroup.LayoutParams localLayoutParams2 = this.mInputMethodPlaceholder.getLayoutParams();
        localLayoutParams2.height = -2;
        localLayoutParams2.width = -1;
        this.mInputMethodPlaceholder.setLayoutParams(localLayoutParams2);
        this.mInputMethodPlaceholder.invalidate();
        return;
      }
      while (!Profile.isHandwritingSupported(this.mActivity, this.mSourceLanguage));
      this.mInputMethodPlaceholder.addView(this.mHandwritingInputView);
      ViewGroup.LayoutParams localLayoutParams1 = this.mHandwritingInputView.getLayoutParams();
      localLayoutParams1.height = (this.mActivity.getApplicationContext().getResources().getDisplayMetrics().heightPixels / 2);
      localLayoutParams1.width = -1;
      this.mHandwritingInputView.setLayoutParams(localLayoutParams1);
      this.mHandwritingInputView.invalidate();
      slideUpInputMethodView();
      return;
    case 4:
    }
    if (this.mStartedInputMethod == InputMethod.CAMERA)
    {
      restartCameraView();
      return;
    }
    initializeCameraInputView((LayoutInflater)this.mActivity.getSystemService("layout_inflater"));
    setCameraViewHeight();
    this.mCameraInputView.setImageLogging(Profile.getCameraLogging(this.mActivity));
    if (!TranslateApplication.isReleaseBuild(this.mActivity))
    {
      String str = Profile.getCameraInputFrontendUrl(this.mActivity);
      Logger.d("InputMethodView", "camera input frontend url=" + str);
      this.mCameraInputView.setFrontendUrl(str);
    }
    startCameraInput();
  }

  private void restartCameraView()
  {
    Logger.d("InputMethodView", "restartCameraView");
    hideCurrentInputMethod(InputMethodEvent.PAUSE);
    postDelayed(new Runnable()
    {
      public void run()
      {
        InputMethodView.this.startInputMethod(InputMethodView.InputMethod.CAMERA);
      }
    }
    , 500L);
  }

  private void setCameraViewHeight()
  {
    this.mCameraHeight = (this.mActivity.getApplicationContext().getResources().getDisplayMetrics().heightPixels / 2);
    int i = this.mTranslateView.getBottom() - this.mTranslateView.getTop();
    Logger.d("InputMethodView", "translateViewHeight=" + i);
    ViewGroup.LayoutParams localLayoutParams = this.mTranslateView.getLayoutParams();
    localLayoutParams.height = (i - this.mCameraHeight);
    Logger.d("InputMethodView", "CAMERA mCameraHeight=" + this.mCameraHeight);
    Logger.d("InputMethodView", "CAMERA params.height=" + localLayoutParams.height);
    this.mTranslateView.setLayoutParams(localLayoutParams);
    this.mTranslateView.invalidate();
    this.mCameraInputView.setImeControlsHeight(this.mCameraHeight);
    this.mCameraInputView.invalidate();
    Logger.d("InputMethodView", "params.height" + localLayoutParams.height);
    Logger.d("InputMethodView", "params.width" + localLayoutParams.width);
    Logger.d("InputMethodView", "mCameraInputView.getHeight()" + this.mCameraInputView.getHeight());
    Logger.d("InputMethodView", "mCameraInputView.getWidth()" + this.mCameraInputView.getWidth());
  }

  public static void setSoftwareKeyboardAvailable(Activity paramActivity, boolean paramBoolean)
  {
    InputMethodManager localInputMethodManager = (InputMethodManager)paramActivity.getSystemService("input_method");
    if (localInputMethodManager == null)
      return;
    if (paramBoolean)
    {
      localInputMethodManager.toggleSoftInput(0, 1);
      return;
    }
    localInputMethodManager.toggleSoftInput(0, 1);
  }

  private void showCameraLoggingDialog(final Activity paramActivity, boolean paramBoolean)
  {
    if (paramBoolean);
    try
    {
      Profile.setShowCameraLoggingDialog(paramActivity, true);
      View localView = ((LayoutInflater)paramActivity.getSystemService("layout_inflater")).inflate(R.layout.camera_logging_dialog, null);
      CheckBox localCheckBox = (CheckBox)localView.findViewById(R.id.camera_logging_checkbox);
      localCheckBox.setChecked(Profile.getCameraLogging(paramActivity));
      localCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
      {
        public void onCheckedChanged(CompoundButton paramAnonymousCompoundButton, boolean paramAnonymousBoolean)
        {
          Profile.setCameraLogging(paramActivity, paramAnonymousBoolean);
        }
      });
      String str = paramActivity.getString(R.string.label_ok);
      StringBuilder localStringBuilder = new StringBuilder(paramActivity.getString(R.string.label_camera_input));
      AlertDialog.Builder localBuilder = new AlertDialog.Builder(paramActivity);
      localBuilder.setTitle(localStringBuilder.toString()).setView(localView);
      localBuilder.setInverseBackgroundForced(true);
      localBuilder.setOnCancelListener(new DialogInterface.OnCancelListener()
      {
        public void onCancel(DialogInterface paramAnonymousDialogInterface)
        {
          InputMethodView.this.hideCurrentInputMethod(InputMethodView.InputMethodEvent.SWITCH);
          InputMethodView.this.mEditPanel.onAccept(false);
        }
      });
      localBuilder.setPositiveButton(str, new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          Profile.setShowCameraLoggingDialog(paramActivity, false);
          paramAnonymousDialogInterface.dismiss();
        }
      });
      localBuilder.create().show();
      while (true)
      {
        return;
        boolean bool = Profile.getShowCameraLoggingDialog(paramActivity);
        if (bool)
          break;
      }
    }
    finally
    {
    }
  }

  private void showInputSelector(boolean paramBoolean)
  {
    Logger.d("InputMethodView", "showInputSelector");
    if (this.mIconCount == 0)
    {
      if (paramBoolean)
      {
        slideDown(this.mInputSelector);
        return;
      }
      this.mInputSelector.setVisibility(8);
      return;
    }
    if (paramBoolean)
    {
      slideUp(this.mInputSelector);
      return;
    }
    this.mInputSelector.setVisibility(0);
  }

  private void slideDown(final View paramView)
  {
    Logger.d("InputMethodView", "slideDown");
    if (paramView.getVisibility() == 8)
      return;
    TranslateAnimation localTranslateAnimation = new TranslateAnimation(0.0F, 0.0F, 0.0F, getHeight());
    localTranslateAnimation.setDuration(150L);
    localTranslateAnimation.setAnimationListener(new Animation.AnimationListener()
    {
      public void onAnimationEnd(Animation paramAnonymousAnimation)
      {
        paramView.setVisibility(8);
      }

      public void onAnimationRepeat(Animation paramAnonymousAnimation)
      {
      }

      public void onAnimationStart(Animation paramAnonymousAnimation)
      {
      }
    });
    paramView.startAnimation(localTranslateAnimation);
  }

  private void slideDownInputMethodView()
  {
    Logger.d("InputMethodView", "slideDownInputMethodView");
    TranslateAnimation localTranslateAnimation = new TranslateAnimation(0.0F, 0.0F, 0.0F, getHeight());
    localTranslateAnimation.setDuration(150L);
    localTranslateAnimation.setAnimationListener(new Animation.AnimationListener()
    {
      public void onAnimationEnd(Animation paramAnonymousAnimation)
      {
        InputMethodView.this.render();
        if (InputMethodView.this.mActiveInputMethod == InputMethodView.InputMethod.NONE)
          InputMethodView.this.showInputSelector(false);
      }

      public void onAnimationRepeat(Animation paramAnonymousAnimation)
      {
      }

      public void onAnimationStart(Animation paramAnonymousAnimation)
      {
      }
    });
    startAnimation(localTranslateAnimation);
  }

  private void slideUp(View paramView)
  {
    Logger.d("InputMethodView", "slideUp");
    if (paramView.getVisibility() == 0)
      return;
    paramView.setVisibility(0);
    TranslateAnimation localTranslateAnimation = new TranslateAnimation(0.0F, 0.0F, paramView.getHeight(), 0.0F);
    localTranslateAnimation.setDuration(150L);
    localTranslateAnimation.setAnimationListener(new Animation.AnimationListener()
    {
      public void onAnimationEnd(Animation paramAnonymousAnimation)
      {
      }

      public void onAnimationRepeat(Animation paramAnonymousAnimation)
      {
      }

      public void onAnimationStart(Animation paramAnonymousAnimation)
      {
      }
    });
    paramView.startAnimation(localTranslateAnimation);
  }

  private void slideUpInputMethodView()
  {
    Logger.d("InputMethodView", "slideUpInputMethodView");
    TranslateAnimation localTranslateAnimation = new TranslateAnimation(0.0F, 0.0F, getHeight(), 0.0F);
    localTranslateAnimation.setDuration(150L);
    localTranslateAnimation.setAnimationListener(new Animation.AnimationListener()
    {
      public void onAnimationEnd(Animation paramAnonymousAnimation)
      {
        switch (InputMethodView.17.$SwitchMap$com$google$android$apps$translate$editor$InputMethodView$InputMethod[InputMethodView.this.mActiveInputMethod.ordinal()])
        {
        default:
        case 3:
        }
        while (true)
        {
          InputMethodView.this.mEditPanel.onInputMethodReady(InputMethodView.this.mActiveInputMethod);
          return;
          InputMethodView.this.mHandwritingInputView.onEditStarted();
          InputMethodView.this.mHandwritingInputView.resetStrokes(false);
        }
      }

      public void onAnimationRepeat(Animation paramAnonymousAnimation)
      {
      }

      public void onAnimationStart(Animation paramAnonymousAnimation)
      {
      }
    });
    startAnimation(localTranslateAnimation);
  }

  private void startCameraInput()
  {
    if (Util.isAutoDetectLanguage(this.mSourceLanguage))
    {
      this.mCameraInputView.startInput(null, null);
      return;
    }
    this.mCameraInputView.startInput(null, this.mSourceLanguage.getShortName());
  }

  public void cancel()
  {
    Logger.d("InputMethodView", "cancel");
    clearSelection();
    switch (17.$SwitchMap$com$google$android$apps$translate$editor$InputMethodView$InputMethod[this.mActiveInputMethod.ordinal()])
    {
    case 1:
    case 4:
    default:
      return;
    case 3:
      this.mHandwritingInputView.clearLimboState();
      this.mHandwritingInputView.resetStrokes(false);
      return;
    case 2:
    }
    if (this.mVoiceInput != null)
      this.mVoiceInput.cancel();
    showInputSelector(false);
  }

  public InputMethod getCurrentInputMethod()
  {
    return this.mActiveInputMethod;
  }

  public int getEditingAreaViewHeight()
  {
    int i;
    switch (17.$SwitchMap$com$google$android$apps$translate$editor$InputMethodView$InputMethod[this.mActiveInputMethod.ordinal()])
    {
    case 2:
    default:
      i = this.mActivity.getApplicationContext().getResources().getDisplayMetrics().heightPixels;
    case 1:
      int j;
      do
      {
        return i;
        i = this.mActivity.getApplicationContext().getResources().getDisplayMetrics().heightPixels / 2;
        j = this.mTranslateView.getHeight();
      }
      while (i < j);
      return j;
    case 3:
    case 4:
    }
    return this.mActivity.getApplicationContext().getResources().getDisplayMetrics().heightPixels / 2;
  }

  public boolean hasInputMethodShown()
  {
    return this.mActiveInputMethod != InputMethod.NONE;
  }

  public boolean hideCurrentInputMethod(InputMethodEvent paramInputMethodEvent)
  {
    Logger.d("InputMethodView", "hideCurrentInputMethod");
    if (this.mEditorField == null)
      return false;
    this.mStartedInputMethod = InputMethod.NONE;
    this.mEditorField.setIsTextEditor(true);
    cancel();
    if (this.mInstantTranslateHandler != null)
      this.mInstantTranslateHandler.commitSourceText();
    Boolean localBoolean = Boolean.valueOf(true);
    switch (17.$SwitchMap$com$google$android$apps$translate$editor$InputMethodView$InputMethod[this.mActiveInputMethod.ordinal()])
    {
    default:
      showInputSelector(true);
      localBoolean = Boolean.valueOf(false);
    case 2:
    case 3:
    case 4:
    case 1:
    }
    while (true)
    {
      this.mActiveInputMethod = InputMethod.NONE;
      return localBoolean.booleanValue();
      this.mHandwritingInputView.onEditCompleted(paramInputMethodEvent);
      slideDownInputMethodView();
      continue;
      this.mCameraInputView.finishInput();
      ViewGroup.LayoutParams localLayoutParams = this.mTranslateView.getLayoutParams();
      localLayoutParams.height = -1;
      this.mTranslateView.setLayoutParams(localLayoutParams);
      this.mTranslateView.invalidate();
      showInputSelector(true);
      continue;
      setSoftwareKeyboardAvailable(this.mActivity, false);
      post(new Runnable()
      {
        public void run()
        {
          InputMethodView.this.hideSoftwareKeyboard();
        }
      });
      showInputSelector(true);
    }
  }

  public void init(Activity paramActivity, InstantTranslateHandler paramInstantTranslateHandler, Language paramLanguage1, Language paramLanguage2, TextSlot paramTextSlot, AsrResultEditor paramAsrResultEditor, String paramString, EditPanelView paramEditPanelView)
  {
    Logger.d("InputMethodView", "init from=" + paramLanguage1.getLongName() + " to=" + paramLanguage2.getLongName());
    this.mActivity = paramActivity;
    this.mInstantTranslateHandler = paramInstantTranslateHandler;
    this.mPreviousSourceLanguage = this.mSourceLanguage;
    this.mSourceLanguage = paramLanguage1;
    this.mPreviousTargetLanguage = this.mTargetLanguage;
    this.mTargetLanguage = paramLanguage2;
    this.mEditorField = paramTextSlot;
    this.mAsrResultEditor = paramAsrResultEditor;
    this.mVoiceLocale = paramString;
    this.mEditPanel = paramEditPanelView;
    this.mInputSelector = findViewById(R.id.input_method_selector);
    this.mInputMethodPlaceholder = ((LinearLayout)findViewById(R.id.input_method_placeholder));
    LayoutInflater localLayoutInflater = (LayoutInflater)this.mActivity.getSystemService("layout_inflater");
    this.mKeyboardBtn = ((ImageButton)findViewById(R.id.input_method_keyboard_button));
    this.mIconCount = 0;
    this.mKeyboardBtn.setVisibility(8);
    initializeVoiceInputView(localLayoutInflater);
    this.mMicBtn = ((ImageButton)findViewById(R.id.input_method_mic_button));
    label299: int j;
    if (this.mVoiceInputHelper.isVoiceInputAvailable(this.mSourceLanguage))
    {
      this.mMicBtn.setVisibility(0);
      this.mMicBtn.setOnClickListener(new View.OnClickListener()
      {
        public void onClick(View paramAnonymousView)
        {
          InputMethodView.this.startInputMethod(InputMethodView.InputMethod.VOICE);
        }
      });
      this.mIconCount = (1 + this.mIconCount);
      this.mHandwritingBtn = ((ImageButton)findViewById(R.id.input_method_handwriting_button));
      if (!Profile.isHandwritingSupported(this.mActivity, this.mSourceLanguage))
        break label541;
      this.mHandwritingBtn.setVisibility(0);
      this.mHandwritingBtn.setOnClickListener(new View.OnClickListener()
      {
        public void onClick(View paramAnonymousView)
        {
          InputMethodView.this.startInputMethod(InputMethodView.InputMethod.HANDWRITING);
        }
      });
      this.mIconCount = (1 + this.mIconCount);
      this.mCameraBtn = ((ImageButton)findViewById(R.id.input_method_camera_button));
      this.mCameraSupported = false;
      if (TextInputView.isSupported(this.mActivity))
      {
        initializeCameraInputView(localLayoutInflater);
        if (this.mCameraInputView != null)
        {
          String[] arrayOfString = this.mCameraInputView.getSupportedLanguages();
          int i = arrayOfString.length;
          j = 0;
          label358: if (j < i)
          {
            if (!arrayOfString[j].toLowerCase().equals(this.mSourceLanguage.getShortName()))
              break label553;
            this.mCameraSupported = true;
          }
        }
      }
      this.mTranslateView = getRootView().findViewById(R.id.translate_app_container);
      if (!this.mCameraSupported)
        break label559;
      this.mCameraBtn.setVisibility(0);
      this.mCameraBtn.setOnClickListener(new View.OnClickListener()
      {
        public void onClick(View paramAnonymousView)
        {
          InputMethodView.this.startInputMethod(InputMethodView.InputMethod.CAMERA);
        }
      });
      this.mIconCount = (1 + this.mIconCount);
      label445: if ((!this.mEditPanel.isEditMode()) || (getCurrentInputMethod() == InputMethod.NONE))
        break label571;
      hideInputSelector(true);
    }
    while (true)
    {
      findViewById(R.id.input_method_divider_1).setVisibility(8);
      findViewById(R.id.input_method_divider_2).setVisibility(8);
      findViewById(R.id.input_method_divider_3).setVisibility(8);
      if (this.mTranslateView != null)
        this.mTranslateView.setOnTouchListener(new View.OnTouchListener()
        {
          public boolean onTouch(View paramAnonymousView, MotionEvent paramAnonymousMotionEvent)
          {
            return InputMethodView.this.mActiveInputMethod == InputMethodView.InputMethod.CAMERA;
          }
        });
      return;
      this.mMicBtn.setVisibility(8);
      break;
      label541: this.mHandwritingBtn.setVisibility(8);
      break label299;
      label553: j++;
      break label358;
      label559: this.mCameraBtn.setVisibility(8);
      break label445;
      label571: showInputSelector(true);
    }
  }

  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    Logger.d("InputMethodView", "onActivityResult");
    switch (paramInt1)
    {
    default:
      return;
    case 182:
    }
    if ((getCurrentInputMethod() == InputMethod.CAMERA) && (paramInt2 == -1) && (this.mCameraInputView != null))
    {
      Uri localUri = paramIntent.getData();
      this.mCameraInputView.loadImage(localUri);
    }
    this.mLoadingImage = false;
  }

  public void onCameraError()
  {
    Logger.e("InputMethodView", "Failed to open camera!");
    Toast.makeText(this.mActivity, this.mActivity.getString(R.string.text_input_cannot_open_camera), 1).show();
  }

  public void onCancelVoice()
  {
  }

  public void onClearText()
  {
    switch (17.$SwitchMap$com$google$android$apps$translate$editor$InputMethodView$InputMethod[this.mActiveInputMethod.ordinal()])
    {
    default:
      return;
    case 3:
    }
    this.mHandwritingInputView.onClearText();
  }

  public void onConfigurationChanged(Configuration paramConfiguration)
  {
    if (Util.getSdkVersion() >= 8)
      super.onConfigurationChanged(paramConfiguration);
    render();
  }

  public void onDestroy()
  {
    Logger.d("InputMethodView", "onDestroy");
    hideCurrentInputMethod(InputMethodEvent.PAUSE);
  }

  public void onEditPanelHeightChanged(final int paramInt)
  {
    postDelayed(new Runnable()
    {
      public void run()
      {
        switch (InputMethodView.17.$SwitchMap$com$google$android$apps$translate$editor$InputMethodView$InputMethod[InputMethodView.this.mActiveInputMethod.ordinal()])
        {
        default:
          return;
        case 4:
        }
        int i = InputMethodView.this.mActivity.getApplicationContext().getResources().getDisplayMetrics().heightPixels;
        ViewGroup.LayoutParams localLayoutParams = InputMethodView.this.mTranslateView.getLayoutParams();
        localLayoutParams.height = (i - InputMethodView.this.mCameraHeight - paramInt / 2);
        InputMethodView.this.mTranslateView.setLayoutParams(localLayoutParams);
        InputMethodView.this.mTranslateView.invalidate();
      }
    }
    , 500L);
  }

  public void onError()
  {
  }

  public boolean onKeyPreIme(int paramInt, KeyEvent paramKeyEvent)
  {
    if (paramKeyEvent == null);
    do
    {
      do
      {
        return false;
        switch (paramInt)
        {
        default:
          return false;
        case 27:
        case 80:
        }
      }
      while (getCurrentInputMethod() != InputMethod.CAMERA);
      Logger.d("InputMethodView", "onKeyPreIme CAMERA!");
      if (paramKeyEvent.getAction() == 0)
        return this.mCameraInputView.onKeyDown(paramInt, paramKeyEvent);
      if (paramKeyEvent.getAction() == 1)
        return this.mCameraInputView.onKeyUp(paramInt, paramKeyEvent);
      return true;
    }
    while (getCurrentInputMethod() != InputMethod.CAMERA);
    Logger.d("InputMethodView", "onKeyPreIme FOCUS!");
    return true;
  }

  public void onKeyboardButton()
  {
    startInputMethod(InputMethod.KEYBOARD);
  }

  public void onLoadImage()
  {
    Logger.d("InputMethodView", "onLoadImage");
    Intent localIntent = new Intent("android.intent.action.PICK", MediaStore.Images.Media.INTERNAL_CONTENT_URI);
    this.mLoadingImage = true;
    this.mActivity.startActivityForResult(localIntent, 182);
  }

  public void onLoadImageError()
  {
    Logger.d("InputMethodView", "onLoadImageError");
    Util.showShortToastMessage(this.mActivity, R.string.text_input_cannot_load_image);
  }

  public void onNetworkError(int paramInt)
  {
    Logger.e("InputMethodView", "Camera input failed due to network error. status=" + paramInt);
    Toast.makeText(this.mActivity, this.mActivity.getString(R.string.msg_network_error), 1).show();
  }

  public boolean onPause()
  {
    Logger.d("InputMethodView", "onPause");
    return this.mLoadingImage;
  }

  public void onTextSelected(String paramString, String[] paramArrayOfString)
  {
    if (TextUtils.isEmpty(paramString))
      return;
    this.mEditorField.replaceSelectedText(paramString, true);
  }

  public void onVoiceResults(List<CharSequence> paramList, boolean paramBoolean1, boolean paramBoolean2)
  {
  }

  public void restartInputMethod(InputMethod paramInputMethod)
  {
    switch (17.$SwitchMap$com$google$android$apps$translate$editor$InputMethodView$InputMethod[this.mActiveInputMethod.ordinal()])
    {
    default:
      return;
    case 3:
    case 4:
    }
    startInputMethod(paramInputMethod);
  }

  public void startInputMethod(InputMethod paramInputMethod)
  {
    Logger.d("InputMethodView", "startInputMethod: " + paramInputMethod.toString());
    if (this.mEditPanel.onInputMethodStart(paramInputMethod))
      return;
    this.mEditorField.setIsTextEditor(true);
    if (this.mActiveInputMethod != paramInputMethod)
    {
      hideCurrentInputMethod(InputMethodEvent.SWITCH);
      this.mActiveInputMethod = paramInputMethod;
    }
    switch (17.$SwitchMap$com$google$android$apps$translate$editor$InputMethodView$InputMethod[paramInputMethod.ordinal()])
    {
    default:
      showInputSelector(true);
      this.mEditPanel.onInputMethodReady(this.mActiveInputMethod);
    case 3:
    case 2:
    case 4:
    case 1:
    }
    while (true)
    {
      if (this.mStartedInputMethod != paramInputMethod)
        this.mEditorField.beginEditing();
      this.mStartedInputMethod = this.mActiveInputMethod;
      return;
      hideInputSelector(false);
      if (Profile.isHandwritingSupported(this.mActivity, this.mSourceLanguage))
      {
        if ((this.mStartedInputMethod != InputMethod.HANDWRITING) || (this.mPreviousSourceLanguage == null) || ((this.mPreviousSourceLanguage == this.mSourceLanguage) && (this.mPreviousTargetLanguage == this.mTargetLanguage)))
        {
          initializeHandwritingInputView((LayoutInflater)this.mActivity.getSystemService("layout_inflater"));
          this.mHandwritingInputView.setSourceAndTargetLanguages(this.mSourceLanguage, this.mTargetLanguage);
          render();
        }
        else
        {
          this.mHandwritingInputView.setSourceAndTargetLanguages(this.mSourceLanguage, this.mTargetLanguage);
        }
      }
      else
      {
        Logger.d("InputMethodView", "Handwriting not supported for language=" + this.mSourceLanguage.toString());
        startInputMethod(InputMethod.KEYBOARD);
        continue;
        this.mEditPanel.onInputMethodReady(this.mActiveInputMethod);
        this.mVoiceInputHelper.startVoiceInput(this.mActivity, this.mSourceLanguage);
        render();
        continue;
        showCameraLoggingDialog(this.mActivity, false);
        hideInputSelector(false);
        if (this.mCameraSupported)
        {
          if (this.mStartedInputMethod == InputMethod.CAMERA)
          {
            if ((this.mPreviousSourceLanguage != null) && (this.mPreviousSourceLanguage != this.mSourceLanguage))
              changeCameraLanguage();
          }
          else
          {
            render();
            this.mEditPanel.onInputMethodReady(this.mActiveInputMethod);
          }
        }
        else
        {
          Logger.d("InputMethodView", "Camera not supported for language=" + this.mSourceLanguage.toString());
          startInputMethod(InputMethod.KEYBOARD);
          continue;
          hideInputSelector(false);
          this.mKeyboardBtn.setImageResource(R.drawable.input_keyboard_selected);
          render();
          setSoftwareKeyboardAvailable(this.mActivity, true);
          postDelayed(new Runnable()
          {
            public void run()
            {
              InputMethodView.this.mEditPanel.onInputMethodReady(InputMethodView.this.mActiveInputMethod);
            }
          }
          , 500L);
          invalidate();
        }
      }
    }
  }

  public static enum InputMethod
  {
    static
    {
      KEYBOARD = new InputMethod("KEYBOARD", 1);
      VOICE = new InputMethod("VOICE", 2);
      HANDWRITING = new InputMethod("HANDWRITING", 3);
      CAMERA = new InputMethod("CAMERA", 4);
      InputMethod[] arrayOfInputMethod = new InputMethod[5];
      arrayOfInputMethod[0] = NONE;
      arrayOfInputMethod[1] = KEYBOARD;
      arrayOfInputMethod[2] = VOICE;
      arrayOfInputMethod[3] = HANDWRITING;
      arrayOfInputMethod[4] = CAMERA;
    }
  }

  public static enum InputMethodEvent
  {
    static
    {
      InputMethodEvent[] arrayOfInputMethodEvent = new InputMethodEvent[4];
      arrayOfInputMethodEvent[0] = ACCEPT;
      arrayOfInputMethodEvent[1] = INIT;
      arrayOfInputMethodEvent[2] = PAUSE;
      arrayOfInputMethodEvent[3] = SWITCH;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.editor.InputMethodView
 * JD-Core Version:    0.6.2
 */