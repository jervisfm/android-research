package com.google.android.apps.translate.editor;

import android.app.Activity;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import com.google.android.apps.translate.Language;
import com.google.android.apps.translate.Logger;
import com.google.android.apps.translate.Translate;
import com.google.android.apps.translate.Translate.TranslateResult;
import com.google.android.apps.translate.TranslateApplication;
import com.google.android.apps.translate.TranslateManager;
import com.google.android.apps.translate.VoiceInput.UiListener;
import java.util.List;

public class InstantTranslateHandler
  implements VoiceInput.UiListener
{
  private static final String PLACEHOLDER = "…";
  private static final String TAG = "InstantTranslateHandler";
  private Activity mActivity;
  private InstantTranslateListner mCallback;
  private String mDetectedSrcShortLangName = "";
  private boolean mIsStopped = false;
  private Language mSourceLanguage;
  private SpannableStringBuilder mSpellCorrection = new SpannableStringBuilder();
  private SourceTextUpdateHandler mSrcHandler = new SourceTextUpdateHandler(null);
  private SpannableStringBuilder mSrcTextComposing = new SpannableStringBuilder();
  private Language mTargetLanguage;
  private SpannableStringBuilder mTranslateComposing = new SpannableStringBuilder();
  private TranslateManager mTranslateManager;
  private Thread mTranslateThread = new Thread()
  {
    public void run()
    {
      Looper.prepare();
      InstantTranslateHandler.access$102(InstantTranslateHandler.this, new InstantTranslateHandler.TargetTextUpdateHandler(InstantTranslateHandler.this, null));
      InstantTranslateHandler.this.mTrgHandler.startSendingTranslationRequests();
      Looper.loop();
    }
  };
  private TargetTextUpdateHandler mTrgHandler;

  private void doTextTranslateSync(Language paramLanguage1, Language paramLanguage2, String paramString)
  {
    if (this.mIsStopped)
      return;
    if (TextUtils.isEmpty(paramString.trim()))
    {
      this.mTrgHandler.handleTranslationResult(paramString, "", "", "");
      return;
    }
    String str;
    synchronized (this.mTranslateManager)
    {
      Logger.d("InstantTranslateHandler", "doTextTranslateSync from=" + paramLanguage1 + " to=" + paramLanguage2 + " text=" + paramString);
      this.mTranslateManager.setLanguagePair(paramLanguage1, paramLanguage2);
      this.mTranslateManager.setInstantTransalte(true);
      long l = System.currentTimeMillis();
      str = this.mTranslateManager.doTranslate(paramString);
      (System.currentTimeMillis() - l);
      int i = Translate.getResultCode(str);
      switch (i)
      {
      default:
        Logger.d("translation error happened error=" + i);
        return;
      case -4:
      case 0:
      }
    }
    Translate.TranslateResult localTranslateResult = new Translate.TranslateResult(str, true);
    this.mTrgHandler.handleTranslationResult(paramString, localTranslateResult.getTranslateText(), localTranslateResult.getSpellCorrection(), localTranslateResult.getDetectedSrcShortLangName(paramLanguage1.getShortName()));
    Logger.d("translated text: ", localTranslateResult.toString());
  }

  static boolean equalsExceptForDots(String paramString1, String paramString2)
  {
    int i = paramString1.length() - paramString2.length();
    if (i == 0)
      return paramString1.equals(paramString2);
    if (Math.abs(i) == 1)
    {
      String str;
      StringBuilder localStringBuilder;
      if (i > 0)
      {
        str = paramString2;
        localStringBuilder = new StringBuilder(str);
        localStringBuilder.append("…");
        if (i <= 0)
          break label68;
      }
      while (true)
      {
        return localStringBuilder.equals(paramString1);
        str = paramString1;
        break;
        label68: paramString1 = paramString2;
      }
    }
    return false;
  }

  private void setTranslateText(final boolean paramBoolean)
  {
    this.mActivity.runOnUiThread(new Runnable()
    {
      public void run()
      {
        if (paramBoolean)
        {
          SpannableStringBuilder localSpannableStringBuilder = new SpannableStringBuilder(InstantTranslateHandler.this.mTranslateComposing);
          localSpannableStringBuilder.append("…");
          InstantTranslateHandler.this.mCallback.onTargetTextUpdate(InstantTranslateHandler.this.mSrcTextComposing, localSpannableStringBuilder, InstantTranslateHandler.this.mSpellCorrection, InstantTranslateHandler.this.mDetectedSrcShortLangName);
          return;
        }
        InstantTranslateHandler.this.mCallback.onTargetTextUpdate(InstantTranslateHandler.this.mSrcTextComposing, InstantTranslateHandler.this.mTranslateComposing, InstantTranslateHandler.this.mSpellCorrection, InstantTranslateHandler.this.mDetectedSrcShortLangName);
      }
    });
  }

  public void commitSourceText()
  {
    if ((this.mIsStopped) || (this.mTrgHandler == null))
      return;
    this.mSrcHandler.commit();
    this.mTrgHandler.commit();
  }

  public String getTranslationText(String paramString)
  {
    if (TextUtils.equals(paramString, this.mSrcTextComposing.toString()))
      return this.mTranslateComposing.toString();
    return "";
  }

  public void init(Activity paramActivity, InstantTranslateListner paramInstantTranslateListner, Language paramLanguage1, Language paramLanguage2)
  {
    Logger.d("InstantTranslateHandler", "InstantTranslateHandler#init");
    this.mCallback = paramInstantTranslateListner;
    this.mActivity = paramActivity;
    this.mTranslateManager = ((TranslateApplication)this.mActivity.getApplication()).getTranslateManager();
    this.mSourceLanguage = paramLanguage1;
    this.mTargetLanguage = paramLanguage2;
    this.mIsStopped = false;
    reset();
    if (!this.mTranslateThread.isAlive())
    {
      Logger.d("InstantTranslateHandler", "InstantTranslateHandler#init => mTranslateThread.start()");
      this.mTranslateThread.start();
    }
    while (true)
    {
      this.mSrcHandler.updateSourceText();
      return;
      this.mTrgHandler.init();
    }
  }

  public boolean isStopped()
  {
    return this.mIsStopped;
  }

  public void onCancelVoice()
  {
    commitSourceText();
  }

  public void onError()
  {
  }

  public void onVoiceResults(List<CharSequence> paramList, boolean paramBoolean1, boolean paramBoolean2)
  {
  }

  public void reset()
  {
    try
    {
      this.mSrcTextComposing = new SpannableStringBuilder();
      this.mTranslateComposing = new SpannableStringBuilder();
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public void stop()
  {
    this.mIsStopped = true;
    this.mSrcHandler.stop();
    if (this.mTrgHandler != null)
      this.mTrgHandler.stop();
    reset();
  }

  public void updateSourceText(CharSequence paramCharSequence)
  {
    this.mSrcTextComposing = new SpannableStringBuilder(paramCharSequence);
    this.mSrcHandler.updateSourceText();
  }

  public static abstract interface InstantTranslateListner
  {
    public abstract void onLongPause(SpannableStringBuilder paramSpannableStringBuilder1, SpannableStringBuilder paramSpannableStringBuilder2, SpannableStringBuilder paramSpannableStringBuilder3, String paramString);

    public abstract void onSourceTextDone(SpannableStringBuilder paramSpannableStringBuilder);

    public abstract void onSourceTextUpdate(SpannableStringBuilder paramSpannableStringBuilder);

    public abstract void onTargetTextDone(SpannableStringBuilder paramSpannableStringBuilder);

    public abstract void onTargetTextUpdate(SpannableStringBuilder paramSpannableStringBuilder1, SpannableStringBuilder paramSpannableStringBuilder2, SpannableStringBuilder paramSpannableStringBuilder3, String paramString);
  }

  private class SourceTextUpdateHandler extends InstantTranslateHandler.TextUpdateHandler
  {
    private static final int FORCE_TRANSLATION_MILLIS = 500;
    private static final int MSG_FORCE_TRANSLATION = 2;
    private static final int MSG_SRCTEXT_DONE = 1;
    private static final int MSG_SRCTEXT_UPDATE;

    private SourceTextUpdateHandler()
    {
      super(null);
    }

    public void commit()
    {
      sendMessage(1);
    }

    public void handleMessage(Message paramMessage)
    {
      if ((InstantTranslateHandler.this.mCallback == null) || (!InstantTranslateHandler.this.mCallback.equals(paramMessage.obj)));
      do
      {
        return;
        switch (paramMessage.what)
        {
        default:
        case 0:
        case 1:
        case 2:
        }
      }
      while (!Logger.isDebug());
      throw new IllegalArgumentException("Invalid message=" + paramMessage.what);
      InstantTranslateHandler.this.mCallback.onSourceTextUpdate(InstantTranslateHandler.this.mSrcTextComposing);
      InstantTranslateHandler.this.setTranslateText(true);
      return;
      InstantTranslateHandler.this.mSrcTextComposing.clearSpans();
      InstantTranslateHandler.this.mCallback.onSourceTextDone(InstantTranslateHandler.this.mSrcTextComposing);
      removeMessages(0, InstantTranslateHandler.this.mCallback);
      return;
      if (InstantTranslateHandler.this.mTrgHandler != null)
      {
        InstantTranslateHandler.this.mTrgHandler.startSendingTranslationRequests();
        return;
      }
      sendMessageDelayed(2, 500L);
    }

    public void stop()
    {
      removeMessages(0, InstantTranslateHandler.this.mCallback);
    }

    public void updateSourceText()
    {
      sendMessage(0);
      sendMessage(2);
    }
  }

  private class TargetTextUpdateHandler extends InstantTranslateHandler.TextUpdateHandler
  {
    private static final int LONG_PAUSE_MILLIS = 1500;
    private static final int MSG_LONG_PAUSE = 2;
    private static final int MSG_REMOVE_ELLIPSE = 3;
    private static final int MSG_SEND_TRANSLATE_REQUEST = 1;
    private static final int MSG_TRGTEXT_DONE = 0;
    private static final int REMOVE_ELLIPSE_MILLIS = 500;
    private String mLastSrcText = "";

    private TargetTextUpdateHandler()
    {
      super(null);
    }

    private void doTranslationRequestPostProcessing()
    {
      removeMessages(2, InstantTranslateHandler.this.mCallback);
      sendMessageDelayed(2, 1500L);
      removeMessages(3, InstantTranslateHandler.this.mCallback);
      sendMessage(3);
      sendMessageDelayed(3, 500L);
    }

    public void commit()
    {
      sendMessage(0);
    }

    public void handleMessage(Message paramMessage)
    {
      if ((InstantTranslateHandler.this.mCallback == null) || (!InstantTranslateHandler.this.mCallback.equals(paramMessage.obj)));
      do
      {
        return;
        switch (paramMessage.what)
        {
        default:
        case 0:
        case 1:
        case 3:
        case 2:
        }
      }
      while (!Logger.isDebug());
      throw new IllegalArgumentException("Invalid message=" + paramMessage.what);
      InstantTranslateHandler.this.mTranslateComposing.clearSpans();
      InstantTranslateHandler.this.mActivity.runOnUiThread(new Runnable()
      {
        public void run()
        {
          InstantTranslateHandler.this.mCallback.onTargetTextDone(InstantTranslateHandler.this.mTranslateComposing);
        }
      });
      return;
      String str = InstantTranslateHandler.this.mSrcTextComposing.toString();
      if (!this.mLastSrcText.equals(str))
      {
        this.mLastSrcText = str;
        InstantTranslateHandler.this.setTranslateText(true);
        InstantTranslateHandler.this.doTextTranslateSync(InstantTranslateHandler.this.mSourceLanguage, InstantTranslateHandler.this.mTargetLanguage, str);
        return;
      }
      doTranslationRequestPostProcessing();
      return;
      InstantTranslateHandler.this.mActivity.runOnUiThread(new Runnable()
      {
        public void run()
        {
          if (!InstantTranslateHandler.TargetTextUpdateHandler.this.hasMessages(1))
            InstantTranslateHandler.this.setTranslateText(false);
        }
      });
      return;
      InstantTranslateHandler.this.mActivity.runOnUiThread(new Runnable()
      {
        public void run()
        {
          InstantTranslateHandler.this.mCallback.onLongPause(InstantTranslateHandler.this.mSrcTextComposing, InstantTranslateHandler.this.mTranslateComposing, InstantTranslateHandler.this.mSpellCorrection, InstantTranslateHandler.this.mDetectedSrcShortLangName);
        }
      });
    }

    public void handleTranslationResult(String paramString1, String paramString2, String paramString3, String paramString4)
    {
      if (paramString2 == null)
        paramString2 = "";
      SpannableStringBuilder localSpannableStringBuilder = new SpannableStringBuilder(paramString2);
      InstantTranslateHandler.access$702(InstantTranslateHandler.this, localSpannableStringBuilder);
      InstantTranslateHandler localInstantTranslateHandler = InstantTranslateHandler.this;
      if (paramString3 == null)
        paramString3 = "";
      InstantTranslateHandler.access$1202(localInstantTranslateHandler, new SpannableStringBuilder(paramString3));
      InstantTranslateHandler.access$1302(InstantTranslateHandler.this, paramString4);
      doTranslationRequestPostProcessing();
    }

    public void init()
    {
      this.mLastSrcText = "";
    }

    public void startSendingTranslationRequests()
    {
      removeMessages(3, InstantTranslateHandler.this.mCallback);
      removeMessages(1, InstantTranslateHandler.this.mCallback);
      sendMessage(1);
    }

    public void stop()
    {
      removeMessages(1, InstantTranslateHandler.this.mCallback);
    }
  }

  private class TextUpdateHandler extends Handler
  {
    private TextUpdateHandler()
    {
    }

    private Message obtainMessageForCallback(int paramInt)
    {
      return obtainMessage(paramInt, 0, 0, InstantTranslateHandler.this.mCallback);
    }

    public final boolean sendMessage(int paramInt)
    {
      return sendMessage(obtainMessageForCallback(paramInt));
    }

    public final boolean sendMessageDelayed(int paramInt, long paramLong)
    {
      return sendMessageDelayed(obtainMessageForCallback(paramInt), paramLong);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.editor.InstantTranslateHandler
 * JD-Core Version:    0.6.2
 */