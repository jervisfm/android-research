package com.google.android.apps.translate.editor;

import android.content.Context;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Message;
import android.text.TextPaint;
import android.text.TextUtils.TruncateAt;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;
import android.widget.EditText;
import android.widget.TextView;
import com.google.android.apps.translate.Logger;

public class InstantTranslateTextView extends TextView
  implements GestureDetector.OnGestureListener, GestureDetector.OnDoubleTapListener
{
  private static final int AUTO_SCROLL_DELAY_MILLIS = 1000;
  private static final float AUTO_SCROLL_PX_PER_MILLIS = 0.2F;
  private static final boolean DEBUG_SCROLL = false;
  private static final int MAX_PADDING_CHARS_TO_ELLIPSIZE = 10;
  private static final int PREFIX_COMP_CHARS = 10;
  private static final int SCROLL_INTERVAL_MILLIS = 10;
  private static final String TAG = "InstantTranslateTextView";
  private AutoScrollHandler mAutoScrollHandler = new AutoScrollHandler(null);
  private Callback mCallback;
  private GestureDetector mGestureDetector;
  private int mInitialScrollX = 0;
  private boolean mLongPressed = false;
  private int mMaxVisibleTextSizePx = 0;
  private int mTextBoundLeft = 0;
  private int mTextBoundRight = 0;
  private int mTextBoundWidth = 0;

  public InstantTranslateTextView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }

  private int getDestinationPosition(int paramInt)
  {
    if (isRtl(this.mInitialScrollX));
    for (int i = -1; i * paramInt > 0; i = 1)
      return getPositionWithEllipsesAtEnd();
    return getPositionWithEllipsesAtStart();
  }

  private int getPositionWithEllipsesAtEnd()
  {
    if (isRtl(this.mInitialScrollX))
      return this.mInitialScrollX;
    return 0;
  }

  private int getPositionWithEllipsesAtStart()
  {
    Logger.d("InstantTranslateTextView", "getPositionWithEllipsesAtStart START");
    int i = this.mTextBoundWidth;
    int j = this.mMaxVisibleTextSizePx;
    int k = 0;
    if (i >= j)
    {
      k = 0 + (this.mTextBoundRight - this.mMaxVisibleTextSizePx);
      Logger.d("InstantTranslateTextView", "getPositionWithEllipsesAtStart offset=" + k);
    }
    if (isRtl(this.mInitialScrollX))
    {
      Logger.d("InstantTranslateTextView", "getPositionWithEllipsesAtStart mInitialScrollX=" + this.mInitialScrollX);
      return this.mInitialScrollX - k;
    }
    Logger.d("InstantTranslateTextView", "getPositionWithEllipsesAtStart END offset=" + k);
    return k;
  }

  private void init()
  {
    this.mInitialScrollX = 0;
    this.mTextBoundLeft = 0;
    this.mTextBoundRight = 0;
    this.mTextBoundWidth = 0;
    this.mMaxVisibleTextSizePx = 0;
  }

  private boolean isLtl(int paramInt)
  {
    return (this.mMaxVisibleTextSizePx > 0) && (paramInt < this.mMaxVisibleTextSizePx);
  }

  private boolean isRtl(int paramInt)
  {
    return (this.mTextBoundWidth > this.mMaxVisibleTextSizePx) && (paramInt > 5 * this.mTextBoundWidth);
  }

  private boolean scrollToByUpToGivenDistance(int paramInt1, int paramInt2)
  {
    while (true)
    {
      boolean bool;
      int i;
      label90: int k;
      try
      {
        setBaseScrollX();
        if (this.mTextBoundWidth < this.mMaxVisibleTextSizePx)
        {
          setEllipsize(TextUtils.TruncateAt.END);
          setInstantTranslateToScrollable();
          Logger.d("InstantTranslateTextView", "scrollByCustom bye: short");
          bool = false;
          return bool;
        }
        setInstantTranslateToScrollable();
        this.mMaxVisibleTextSizePx = (getWidth() - getPaddingLeft() - getPaddingRight());
        i = getScrollX();
        if (i == paramInt1)
        {
          Logger.d("InstantTranslateTextView", "scrollByCustom bye!");
          bool = false;
          continue;
          k = i + j * paramInt2;
          if (Math.abs(k - paramInt1) < paramInt2)
          {
            scrollTo(paramInt1, 0);
            bool = false;
            continue;
          }
        }
      }
      finally
      {
      }
      do
      {
        j = -1;
        break label90;
        scrollTo(k, 0);
        bool = true;
        break;
      }
      while (i >= paramInt1);
      int j = 1;
    }
  }

  private void setBaseScrollX()
  {
    try
    {
      int i = getScrollX();
      this.mMaxVisibleTextSizePx = (getWidth() - getPaddingLeft() - getPaddingRight());
      TextPaint localTextPaint = getPaint();
      String str = getText().toString();
      Rect localRect = new Rect();
      localTextPaint.getTextBounds(str, 0, str.length(), localRect);
      this.mTextBoundLeft = localRect.left;
      this.mTextBoundRight = localRect.right;
      this.mTextBoundWidth = Math.abs(this.mTextBoundLeft - this.mTextBoundRight);
      if ((this.mTextBoundWidth > str.length()) && (this.mTextBoundWidth < this.mMaxVisibleTextSizePx))
      {
        Logger.d("InstantTranslateTextView", "setBaseScrollX INIT mInitialScrollX=" + i);
        this.mInitialScrollX = i;
      }
      if (isLtl(i))
        if (isRtl(this.mInitialScrollX))
          Logger.d("InstantTranslateTextView", "setBaseScrollX switch-to-RTL mInitialScrollX=" + i);
      for (this.mInitialScrollX = i; ; this.mInitialScrollX = i)
      {
        do
          return;
        while ((!isRtl(i)) || (!isLtl(this.mInitialScrollX)));
        Logger.d("InstantTranslateTextView", "setBaseScrollX switch-to-LTR mInitialScrollX=" + i);
      }
    }
    finally
    {
    }
  }

  private void setInstantTranslateToScrollable()
  {
    if (getEllipsize() != null)
      setEllipsize(null);
  }

  public boolean onDoubleTap(MotionEvent paramMotionEvent)
  {
    return false;
  }

  public boolean onDoubleTapEvent(MotionEvent paramMotionEvent)
  {
    return false;
  }

  public boolean onDown(MotionEvent paramMotionEvent)
  {
    return false;
  }

  protected void onFinishInflate()
  {
    super.onFinishInflate();
    this.mGestureDetector = new GestureDetector(getContext(), this);
    this.mGestureDetector.setOnDoubleTapListener(this);
    this.mGestureDetector.setIsLongpressEnabled(false);
  }

  public boolean onFling(MotionEvent paramMotionEvent1, MotionEvent paramMotionEvent2, float paramFloat1, float paramFloat2)
  {
    this.mAutoScrollHandler.startScroll(getDestinationPosition((int)paramFloat1), Math.abs(paramFloat1 / 1000.0F), 0);
    return true;
  }

  public void onLongPress(MotionEvent paramMotionEvent)
  {
    this.mLongPressed = true;
    if (this.mCallback != null)
      this.mAutoScrollHandler.stop();
  }

  public boolean onScroll(MotionEvent paramMotionEvent1, MotionEvent paramMotionEvent2, float paramFloat1, float paramFloat2)
  {
    Math.abs(paramMotionEvent2.getEventTime() - paramMotionEvent1.getEventTime());
    int i = getScrollX() + (int)paramFloat1;
    if (isRtl(this.mInitialScrollX))
      if (this.mInitialScrollX - this.mTextBoundLeft < i)
        i = this.mInitialScrollX - this.mTextBoundLeft;
    while (true)
    {
      this.mAutoScrollHandler.stop();
      scrollToByUpToGivenDistance(i, (int)Math.abs(paramFloat1));
      return true;
      if (i < this.mInitialScrollX - this.mTextBoundRight + this.mMaxVisibleTextSizePx)
      {
        i = this.mInitialScrollX - this.mTextBoundRight + this.mMaxVisibleTextSizePx;
        continue;
        if (i < this.mTextBoundLeft)
          i = this.mTextBoundLeft;
        else if (this.mTextBoundRight < i + this.mMaxVisibleTextSizePx)
          i = this.mTextBoundRight - this.mMaxVisibleTextSizePx;
      }
    }
  }

  public void onShowPress(MotionEvent paramMotionEvent)
  {
  }

  public boolean onSingleTapConfirmed(MotionEvent paramMotionEvent)
  {
    if ((this.mCallback != null) && (!this.mLongPressed))
    {
      this.mCallback.onConfirm();
      return true;
    }
    return false;
  }

  public boolean onSingleTapUp(MotionEvent paramMotionEvent)
  {
    return false;
  }

  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    Logger.d("InstantTranslateTextView", "onTouchEvent");
    this.mLongPressed = false;
    if ((this.mGestureDetector != null) && (this.mGestureDetector.onTouchEvent(paramMotionEvent)))
      return true;
    Logger.d("InstantTranslateTextView", "super.onTouchEvent getScrollX=" + getScrollX());
    return super.onTouchEvent(paramMotionEvent);
  }

  void scrollToEdge(String paramString, EditText paramEditText, final boolean paramBoolean)
  {
    try
    {
      Logger.d("InstantTranslateTextView", "scrollToEdge");
      final int i = paramEditText.getSelectionStart();
      final int j = paramEditText.getSelectionEnd();
      final int k = paramEditText.length();
      final boolean bool1 = false;
      if (i == 0)
      {
        bool1 = false;
        if (j == k)
          bool1 = true;
      }
      String str = getText().toString();
      if (bool1)
      {
        boolean bool2 = InstantTranslateHandler.equalsExceptForDots(paramString, str.toString());
        if (!bool2);
      }
      while (true)
      {
        return;
        if ((bool1) || (paramString.length() < 10) || (str.toString().length() < 10) || (!paramString.subSequence(0, 10).equals(str.toString().subSequence(0, 10))))
        {
          Logger.d("InstantTranslateTextView", " onTargetTextUpdate requestLayout text=" + str);
          init();
          requestLayout();
        }
        post(new Runnable()
        {
          public void run()
          {
            InstantTranslateTextView.this.setBaseScrollX();
            if ((bool1) && (!paramBoolean))
              if (InstantTranslateTextView.this.mAutoScrollHandler != null);
            do
            {
              return;
              InstantTranslateTextView.this.mAutoScrollHandler.startScroll(InstantTranslateTextView.this.getPositionWithEllipsesAtStart(), 0.2F, 1000);
              return;
              if ((i >= 0) && (i < 10))
              {
                InstantTranslateTextView.this.scrollToByUpToGivenDistance(InstantTranslateTextView.access$500(InstantTranslateTextView.this), InstantTranslateTextView.this.mTextBoundWidth);
                return;
              }
            }
            while ((j < 0) || (k - j >= 10));
            InstantTranslateTextView.this.scrollToByUpToGivenDistance(InstantTranslateTextView.access$400(InstantTranslateTextView.this), InstantTranslateTextView.this.mTextBoundWidth);
          }
        });
      }
    }
    finally
    {
    }
  }

  void setCallback(Callback paramCallback)
  {
    this.mCallback = paramCallback;
  }

  private class AutoScrollHandler extends Handler
  {
    private static final int MSG_START_SCROLL;

    private AutoScrollHandler()
    {
    }

    public void handleMessage(Message paramMessage)
    {
      switch (paramMessage.what)
      {
      default:
        if (Logger.isDebug())
          throw new IllegalArgumentException("Invalid message=" + paramMessage.what);
        break;
      case 0:
        int i = paramMessage.arg1;
        int j = paramMessage.arg2;
        if (InstantTranslateTextView.this.scrollToByUpToGivenDistance(i, j))
          sendMessageDelayed(obtainMessage(0, i, j), 10L);
        break;
      }
    }

    public void startScroll(int paramInt1, float paramFloat, int paramInt2)
    {
      Logger.d("InstantTranslateTextView", "startScroll toPositionX=" + paramInt1 + " pxPerMillis=" + paramFloat);
      removeMessages(0);
      sendMessageDelayed(obtainMessage(0, paramInt1, (int)(10.0F * paramFloat)), paramInt2);
    }

    public void stop()
    {
      removeMessages(0);
    }
  }

  public static abstract interface Callback
  {
    public abstract void onConfirm();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.editor.InstantTranslateTextView
 * JD-Core Version:    0.6.2
 */