package com.google.android.apps.translate.editor;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.widget.EditText;

public class PreImeAutoCompleteTextView extends EditText
{
  private OnKeyPreImeListener mKeyPreImeListener;

  public PreImeAutoCompleteTextView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }

  public boolean onKeyPreIme(int paramInt, KeyEvent paramKeyEvent)
  {
    if ((this.mKeyPreImeListener != null) && (this.mKeyPreImeListener.onKeyPreIme(paramInt, paramKeyEvent)))
      return true;
    return super.onKeyPreIme(paramInt, paramKeyEvent);
  }

  public void setKeyPreImeListener(OnKeyPreImeListener paramOnKeyPreImeListener)
  {
    this.mKeyPreImeListener = paramOnKeyPreImeListener;
  }

  public static abstract interface OnKeyPreImeListener
  {
    public abstract boolean onKeyPreIme(int paramInt, KeyEvent paramKeyEvent);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.editor.PreImeAutoCompleteTextView
 * JD-Core Version:    0.6.2
 */