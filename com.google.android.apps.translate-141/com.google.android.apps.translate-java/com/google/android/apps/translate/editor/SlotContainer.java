package com.google.android.apps.translate.editor;

public abstract interface SlotContainer
{
  public abstract void toggleRecognizing(SlotView paramSlotView);
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.editor.SlotContainer
 * JD-Core Version:    0.6.2
 */