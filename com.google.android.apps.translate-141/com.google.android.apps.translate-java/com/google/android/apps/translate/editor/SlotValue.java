package com.google.android.apps.translate.editor;

import android.os.Parcelable;

public abstract class SlotValue
  implements Parcelable
{
  public int describeContents()
  {
    return 0;
  }

  public abstract boolean isEmpty();
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.editor.SlotValue
 * JD-Core Version:    0.6.2
 */