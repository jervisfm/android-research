package com.google.android.apps.translate.editor;

import android.content.Context;
import android.graphics.Path;
import android.graphics.Region;
import android.text.Editable;
import android.text.Layout;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputMethodManager;
import com.google.android.apps.translate.Constants;
import com.google.android.apps.translate.Logger;
import com.google.android.apps.translate.SdkVersionWrapper;
import com.google.android.apps.translate.SdkVersionWrapper.WrapperBase;
import com.google.android.apps.translate.Util;
import com.google.android.apps.translate.asreditor.KeyboardStateTracker;
import com.google.android.apps.translate.asreditor.PopupManager;
import com.google.android.apps.translate.handwriting.EditTextCallback;
import com.google.android.apps.translate.handwriting.HandwritingTextEdit;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class SlotView extends PreImeAutoCompleteTextView
  implements GestureDetector.OnGestureListener, GestureDetector.OnDoubleTapListener, HandwritingTextEdit
{
  private static final String TAG = "SlotView";
  private boolean handledEvent = false;
  private List<SlotBehavior> mBehaviors = new ArrayList();
  private EditTextCallback mCallback;
  private boolean mCanDelete = false;
  protected boolean mCheckIsTextEditor = true;
  private GestureDetector mGestureDetector;
  private boolean mInternalEdit = false;
  private final boolean mIsAlwaysEditable = false;
  private KeyboardStateTracker mKeyboardStateTracker;
  private List<ValueListener> mListeners = new ArrayList();
  private TouchEventListener mOnTouchListener;
  private PopupManager mPopupManager;
  private boolean mShowKeyboard = false;
  private SlotContainer mSlotContainer;

  public SlotView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }

  private void initTitle()
  {
    setText("");
  }

  private void initWatcher()
  {
    addTextChangedListener(new TextWatcher()
    {
      public void afterTextChanged(Editable paramAnonymousEditable)
      {
        if (!SlotView.this.mInternalEdit)
          SlotView.this.onValueChanged(paramAnonymousEditable.toString());
      }

      public void beforeTextChanged(CharSequence paramAnonymousCharSequence, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3)
      {
      }

      public void onTextChanged(CharSequence paramAnonymousCharSequence, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3)
      {
      }
    });
  }

  private void makeCursorVisible()
  {
    post(new Runnable()
    {
      public void run()
      {
        int i = SlotView.this.getSelectionStart();
        if (i == SlotView.this.getSelectionEnd())
        {
          SlotView.access$002(SlotView.this, true);
          SlotView.this.getEditableText().insert(i, " ");
          SlotView.this.getEditableText().delete(i, i + 1);
          SlotView.this.postDelayed(new Runnable()
          {
            public void run()
            {
              SlotView.access$002(SlotView.this, false);
            }
          }
          , 500L);
        }
      }
    });
  }

  private char safeCharAt(CharSequence paramCharSequence, int paramInt)
  {
    if ((paramInt < 0) || (paramInt >= paramCharSequence.length()))
      return '\000';
    return paramCharSequence.charAt(paramInt);
  }

  private boolean tryShowKeyboard()
  {
    Logger.d("SlotView", "tryShowKeyboard this=" + toString());
    return ((InputMethodManager)getContext().getSystemService("input_method")).showSoftInput(this, 0);
  }

  protected void addSlotBehavior(SlotBehavior paramSlotBehavior)
  {
    this.mBehaviors.add(paramSlotBehavior);
  }

  public void addValueListener(ValueListener paramValueListener)
  {
    this.mListeners.add(paramValueListener);
  }

  public void beginEditing()
  {
    Logger.d("SlotView", "beginEditing");
    setSelected(true);
    if (getSelectionStart() < 0)
      setSelection(length());
  }

  public boolean canDelete()
  {
    return this.mCanDelete;
  }

  public void endEditing()
  {
    Logger.d("SlotView", "endEditing this=" + toString());
    hideKeyboard();
  }

  protected void enterKeyboardMode()
  {
    Logger.d("SlotView", "enterKeyboardMode");
    this.mPopupManager.dismissCurrentPopup();
  }

  protected void exitKeyboardMode()
  {
    Logger.d("SlotView", "exitKeyboardMode");
    this.mPopupManager.dismissCurrentPopup();
  }

  public PopupManager getPopupManager()
  {
    return this.mPopupManager;
  }

  public int getSelectionEnd()
  {
    int i = super.getSelectionStart();
    int j = super.getSelectionEnd();
    if (i > j)
      return i;
    return j;
  }

  public int getSelectionStart()
  {
    int i = super.getSelectionStart();
    int j = super.getSelectionEnd();
    if (i > j)
      return j;
    return i;
  }

  public SlotContainer getSlotContainer()
  {
    return this.mSlotContainer;
  }

  public abstract SlotValue getSlotValue();

  protected <T> List<T> getSpans(MotionEvent paramMotionEvent, Class<T> paramClass)
  {
    Layout localLayout = getLayout();
    Editable localEditable = getEditableText();
    Object[] arrayOfObject = localEditable.getSpans(0, length(), paramClass);
    ArrayList localArrayList = new ArrayList();
    Path localPath = new Path();
    Region localRegion1 = new Region(0, 0, getWidth(), getHeight());
    Region localRegion2 = new Region();
    int i = arrayOfObject.length;
    for (int j = 0; j < i; j++)
    {
      Object localObject = arrayOfObject[j];
      localLayout.getSelectionPath(localEditable.getSpanStart(localObject), localEditable.getSpanEnd(localObject), localPath);
      localRegion2.setPath(localPath, localRegion1);
      if (localRegion2.contains((int)paramMotionEvent.getX() - getCompoundPaddingLeft(), (int)paramMotionEvent.getY() - getCompoundPaddingTop()))
        localArrayList.add(localObject);
    }
    return localArrayList;
  }

  protected String getStringValue()
  {
    return getEditableText().subSequence(0, length()).toString();
  }

  public void hideKeyboard()
  {
    Logger.d("SlotView", "hideSoftInputFromWindow this=" + toString());
    ((InputMethodManager)getContext().getSystemService("input_method")).hideSoftInputFromWindow(getWindowToken(), 0);
  }

  public void init(Context paramContext, PopupManager paramPopupManager, KeyboardStateTracker paramKeyboardStateTracker, SlotContainer paramSlotContainer)
  {
    this.mPopupManager = paramPopupManager;
    this.mKeyboardStateTracker = paramKeyboardStateTracker;
    this.mSlotContainer = paramSlotContainer;
    this.mGestureDetector = new GestureDetector(paramContext, this);
    this.mGestureDetector.setOnDoubleTapListener(this);
    this.mGestureDetector.setIsLongpressEnabled(true);
    initTitle();
    initWatcher();
    if (Util.getSdkVersion() >= 14)
      SdkVersionWrapper.getWrapper().setTextIsSelectable(this, true);
  }

  public abstract boolean inputSlotValue(SlotValue paramSlotValue);

  protected void inputString(CharSequence paramCharSequence, int[] paramArrayOfInt)
  {
    Editable localEditable = getEditableText();
    if (getSelectionStart() < 0)
      setSelection(length());
    int i = getSelectionStart();
    int j = getSelectionEnd();
    char c1 = safeCharAt(localEditable, i - 1);
    char c2 = safeCharAt(localEditable, j);
    char c3 = safeCharAt(paramCharSequence, 0);
    safeCharAt(paramCharSequence, -1 + paramCharSequence.length());
    if (i == 0);
    for (int k = 1; ; k = 0)
    {
      SpannableStringBuilder localSpannableStringBuilder = new SpannableStringBuilder();
      if ((paramCharSequence.length() > 0) && (!Constants.CJK_PATTERN.matcher(String.valueOf(paramCharSequence.charAt(0))).matches()) && (k == 0) && (Character.isLetterOrDigit(c1)) && (Character.isLetterOrDigit(c3)))
        localSpannableStringBuilder.append(" ");
      localSpannableStringBuilder.append(paramCharSequence);
      if ((paramCharSequence.length() > 0) && (!Constants.CJK_PATTERN.matcher(String.valueOf(paramCharSequence.charAt(-1 + paramCharSequence.length()))).matches()) && (Character.isLetterOrDigit(c3)) && (Character.isLetterOrDigit(c2)))
        localSpannableStringBuilder.append(" ");
      localEditable.replace(i, j, "");
      if (paramArrayOfInt != null)
      {
        paramArrayOfInt[0] = i;
        paramArrayOfInt[1] = (i + localSpannableStringBuilder.length());
      }
      localEditable.insert(i, localSpannableStringBuilder);
      return;
    }
  }

  public boolean isEmpty()
  {
    return getStringValue().length() == 0;
  }

  public boolean isHardKeyboardVisible()
  {
    return this.mKeyboardStateTracker.isHardKeyboardVisible();
  }

  public boolean isInKeyboardMode()
  {
    return this.mKeyboardStateTracker.isInKeyboardMode();
  }

  boolean isInternalEditMode()
  {
    return this.mInternalEdit;
  }

  public boolean isSoftKeyboardVisible()
  {
    return this.mKeyboardStateTracker.isSoftKeyboardVisible();
  }

  public boolean isSuggestionsEnabled()
  {
    return false;
  }

  public boolean onCheckIsTextEditor()
  {
    if (this.mIsAlwaysEditable)
      return super.onCheckIsTextEditor();
    return this.mCheckIsTextEditor;
  }

  public InputConnection onCreateInputConnection(EditorInfo paramEditorInfo)
  {
    boolean bool = this.mCheckIsTextEditor;
    this.mCheckIsTextEditor = true;
    InputConnection localInputConnection = super.onCreateInputConnection(paramEditorInfo);
    this.mCheckIsTextEditor = bool;
    paramEditorInfo.imeOptions = 268435456;
    return localInputConnection;
  }

  public boolean onDoubleTap(MotionEvent paramMotionEvent)
  {
    Logger.d("SlotView", "onDoubleTap");
    if (this.mOnTouchListener != null)
      this.mOnTouchListener.onDoubleTap(paramMotionEvent);
    return false;
  }

  public boolean onDoubleTapEvent(MotionEvent paramMotionEvent)
  {
    Logger.d("SlotView", "onDoubleTapEvent");
    if (this.mOnTouchListener != null)
      this.mOnTouchListener.onDoubleTapEvent(paramMotionEvent);
    return false;
  }

  public boolean onDown(MotionEvent paramMotionEvent)
  {
    Logger.d("SlotView", "onDown");
    if (this.mOnTouchListener != null)
      this.mOnTouchListener.onDown(paramMotionEvent);
    return false;
  }

  public void onEditorAction(int paramInt)
  {
    Logger.d("SlotView", "onEditorAction actionCode=" + paramInt);
    super.onEditorAction(paramInt);
  }

  public boolean onFling(MotionEvent paramMotionEvent1, MotionEvent paramMotionEvent2, float paramFloat1, float paramFloat2)
  {
    Logger.d("SlotView", "onFling");
    if (this.mOnTouchListener != null)
      this.mOnTouchListener.onFling(paramMotionEvent1, paramMotionEvent2, paramFloat1, paramFloat2);
    return false;
  }

  public void onLongPress(MotionEvent paramMotionEvent)
  {
    Logger.d("SlotView", "onLongPress");
    if (this.mOnTouchListener != null)
      this.mOnTouchListener.onLongPress(paramMotionEvent);
    this.handledEvent = false;
  }

  public void onParentScrolled()
  {
    Logger.d("SlotView", "onParentScrolled");
  }

  public boolean onScroll(MotionEvent paramMotionEvent1, MotionEvent paramMotionEvent2, float paramFloat1, float paramFloat2)
  {
    Logger.d("SlotView", "onScroll");
    if (this.mOnTouchListener != null)
      this.mOnTouchListener.onScroll(paramMotionEvent1, paramMotionEvent2, paramFloat1, paramFloat2);
    return false;
  }

  protected void onSelectionChanged(int paramInt1, int paramInt2)
  {
    super.onSelectionChanged(paramInt1, paramInt2);
  }

  public void onShowPress(MotionEvent paramMotionEvent)
  {
    Logger.d("SlotView", "onShowPress");
    if (this.mOnTouchListener != null)
      this.mOnTouchListener.onShowPress(paramMotionEvent);
  }

  public boolean onSingleTapConfirmed(MotionEvent paramMotionEvent)
  {
    Logger.d("SlotView", "onSingleTapConfirmed");
    if (this.mOnTouchListener != null)
      this.mOnTouchListener.onSingleTapConfirmed(paramMotionEvent);
    makeCursorVisible();
    return false;
  }

  public boolean onSingleTapUp(MotionEvent paramMotionEvent)
  {
    Logger.d("SlotView", "onSingleTapUp");
    if (this.mOnTouchListener != null)
      this.mOnTouchListener.onSingleTapUp(paramMotionEvent);
    return false;
  }

  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    Logger.d("SlotView", "onTouchEvent");
    if (!isSelected())
      setSelected(true);
    boolean bool1 = this.mCheckIsTextEditor;
    this.mCheckIsTextEditor = false;
    this.handledEvent = false;
    if (this.mGestureDetector.onTouchEvent(paramMotionEvent))
    {
      this.mCheckIsTextEditor = bool1;
      return true;
    }
    if (!this.handledEvent)
    {
      boolean bool2 = super.onTouchEvent(paramMotionEvent);
      this.mCheckIsTextEditor = bool1;
      return bool2;
    }
    this.mCheckIsTextEditor = bool1;
    return true;
  }

  protected void onValueChanged(String paramString)
  {
  }

  public void onWindowFocusChanged(boolean paramBoolean)
  {
    Logger.d("SlotView", "onWindowFocusChanged this=" + toString());
    Logger.d("SlotView", "onWindowFocusChanged hasWindowFocus=" + paramBoolean);
    super.onWindowFocusChanged(paramBoolean);
    if ((paramBoolean) && (this.mShowKeyboard))
    {
      this.mShowKeyboard = false;
      tryShowKeyboard();
    }
  }

  protected void onWindowVisibilityChanged(int paramInt)
  {
    Logger.d("SlotView", "onWindowVisibilityChanged this=" + toString());
    Logger.d("SlotView", "onWindowVisibilityChanged visibility=" + paramInt);
    if (paramInt != 0)
      endEditing();
    super.onWindowVisibilityChanged(paramInt);
  }

  public String replaceSelectedText(String paramString, boolean paramBoolean)
  {
    Logger.d("SlotView", "replaceSelectedText newText=" + paramString);
    int i = getSelectionStart();
    if (TextUtils.isEmpty(paramString))
      return getStringValue();
    StringBuilder localStringBuilder = new StringBuilder(getText());
    int j = getSelectionEnd();
    if (i < 0)
    {
      i = localStringBuilder.length();
      localStringBuilder.append(paramString);
      setText(localStringBuilder);
      if (!paramBoolean)
        break label133;
      setSelection(i, i + paramString.length());
    }
    while (true)
    {
      return getStringValue();
      if (i > j)
      {
        int k = i;
        i = j;
        j = k;
      }
      localStringBuilder.replace(i, j, paramString);
      break;
      label133: setSelection(i + paramString.length());
    }
  }

  public void replaceTextWithSelection(String paramString)
  {
    if ((getSelectionStart() == 0) && (getSelectionEnd() == getText().length()));
    for (int i = 1; ; i = 0)
    {
      getSelectionStart();
      setText(paramString);
      if (i == 0)
        break;
      setSelection(0, paramString.length());
      return;
    }
    paramString.length();
    setSelection(getText().length());
  }

  protected void resetStringValue()
  {
    getEditableText().clear();
  }

  public void setCallback(EditTextCallback paramEditTextCallback)
  {
    this.mCallback = paramEditTextCallback;
  }

  public void setCanDelete(boolean paramBoolean)
  {
    this.mCanDelete = paramBoolean;
  }

  public void setIsTextEditor(boolean paramBoolean)
  {
    this.mCheckIsTextEditor = paramBoolean;
  }

  public abstract void setSlotValue(SlotValue paramSlotValue);

  protected void setStringValue(String paramString)
  {
    getEditableText().replace(0, length(), paramString);
  }

  public void setTouchEventCallback(TouchEventListener paramTouchEventListener)
  {
    this.mOnTouchListener = paramTouchEventListener;
  }

  public void showKeyboard(View paramView)
  {
    Logger.d("SlotView", "showKeyboard this=" + toString());
    if (!tryShowKeyboard())
    {
      this.mShowKeyboard = true;
      paramView.requestFocus();
    }
  }

  public static abstract interface SlotBehavior
  {
    public abstract boolean isEnabled();

    public abstract boolean onTouch(MotionEvent paramMotionEvent, int paramInt);
  }

  static abstract interface TouchEventListener extends GestureDetector.OnGestureListener, GestureDetector.OnDoubleTapListener
  {
  }

  public static abstract interface ValueListener
  {
    public abstract void onValueChange(SlotView paramSlotView);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.editor.SlotView
 * JD-Core Version:    0.6.2
 */