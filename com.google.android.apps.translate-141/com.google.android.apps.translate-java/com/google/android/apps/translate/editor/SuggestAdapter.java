package com.google.android.apps.translate.editor;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filter.FilterResults;
import android.widget.Filterable;
import android.widget.TextView;
import com.google.android.apps.translate.Constants.AppearanceType;
import com.google.android.apps.translate.Language;
import com.google.android.apps.translate.Languages;
import com.google.android.apps.translate.Lists;
import com.google.android.apps.translate.Logger;
import com.google.android.apps.translate.Preconditions;
import com.google.android.apps.translate.R.id;
import com.google.android.apps.translate.R.layout;
import com.google.android.apps.translate.R.string;
import com.google.android.apps.translate.TranslateApplication;
import com.google.android.apps.translate.UserActivityMgr;
import com.google.android.apps.translate.UserActivityMgr.IntervalCountTag;
import com.google.android.apps.translate.UserActivityMgr.RequestSource;
import com.google.android.apps.translate.Util;
import com.google.android.apps.translate.history.Entry;
import com.google.android.apps.translate.translation.TranslateEntry;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class SuggestAdapter extends BaseAdapter
  implements Filterable
{
  private static final String TAG = "SuggestAdapter";
  private Activity mActivity;
  private EditText mEditorField;
  private Languages mLanguageList;
  private Language mSourceLanguage;
  private SuggestEntry mSpellCorrectionEntry;
  private SuggestEntry mSrcLangDetectEntry;
  private SuggestionFilter mSuggestFilter;
  private List<SuggestEntry> mSuggestList;
  private Language mTargetLanguage;

  public SuggestAdapter(Activity paramActivity, Languages paramLanguages, Language paramLanguage1, Language paramLanguage2, SuggestionFilter paramSuggestionFilter, EditText paramEditText)
  {
    this.mActivity = ((Activity)Preconditions.checkNotNull(paramActivity));
    this.mSourceLanguage = ((Language)Preconditions.checkNotNull(paramLanguage1));
    this.mTargetLanguage = ((Language)Preconditions.checkNotNull(paramLanguage2));
    this.mLanguageList = ((Languages)Preconditions.checkNotNull(paramLanguages));
    this.mSuggestFilter = ((SuggestionFilter)Preconditions.checkNotNull(paramSuggestionFilter));
    this.mEditorField = ((EditText)Preconditions.checkNotNull(paramEditText));
  }

  public static SuggestEntry getDetectedLanguageSuggestEntry(Languages paramLanguages, Language paramLanguage1, Language paramLanguage2, String paramString1, String paramString2)
  {
    Logger.d("SuggestAdapter", "getDetectedLanguageSuggestEntry " + paramString1);
    if ((TextUtils.isEmpty(paramString1)) || (paramLanguage1.getShortName().equals(paramString1)));
    String str;
    do
    {
      return null;
      str = paramLanguages.getFromLanguageLongName(paramString1);
    }
    while (TextUtils.isEmpty(str));
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("<b><i>");
    localStringBuilder.append(str);
    localStringBuilder.append("</i></b>");
    return new SuggestEntry(new Entry(paramLanguage1, paramLanguage2, paramString1, ""), SuggestType.TWS_DETECTED_SRCLANG, localStringBuilder.toString(), paramString2);
  }

  private List<SuggestEntry> getHistorySuggestions(CharSequence paramCharSequence)
  {
    List localList = Util.getInputSuggestions(this.mActivity, this.mSourceLanguage.getShortName(), this.mTargetLanguage.getShortName(), paramCharSequence.toString().trim());
    Collections.sort(localList, new Comparator()
    {
      public int compare(Entry paramAnonymousEntry1, Entry paramAnonymousEntry2)
      {
        long l = paramAnonymousEntry1.getAccessedTime() - paramAnonymousEntry2.getAccessedTime();
        if (l == 0L)
          l = paramAnonymousEntry1.getCreatedTime() - paramAnonymousEntry2.getCreatedTime();
        if ((l == 0L) && (!TextUtils.isEmpty(paramAnonymousEntry1.getInputText())) && (!TextUtils.isEmpty(paramAnonymousEntry2.getInputText())))
          l = paramAnonymousEntry1.getInputText().compareTo(paramAnonymousEntry2.getInputText());
        if (l > 0L)
          return 1;
        return -1;
      }
    });
    ArrayList localArrayList = Lists.newArrayList();
    Iterator localIterator = localList.iterator();
    while (localIterator.hasNext())
    {
      Entry localEntry = (Entry)localIterator.next();
      localArrayList.add(new SuggestEntry(localEntry, SuggestType.HISTORY, null, localEntry.getInputText()));
    }
    return localArrayList;
  }

  public static SuggestEntry getSpellCorrectionSuggestEntry(Language paramLanguage1, Language paramLanguage2, SpannableStringBuilder paramSpannableStringBuilder, String paramString)
  {
    Logger.d("SuggestAdapter", "getSpellCorrectionSuggestEntry " + paramSpannableStringBuilder);
    if (TextUtils.isEmpty(paramSpannableStringBuilder))
      return null;
    return new SuggestEntry(new Entry(paramLanguage1, paramLanguage2, Html.fromHtml(paramSpannableStringBuilder.toString()).toString().trim(), ""), SuggestType.TWS_SPELL_CORRECTION, paramSpannableStringBuilder.toString(), paramString);
  }

  public static void logSuggestionClick(Context paramContext, SuggestEntry paramSuggestEntry, boolean paramBoolean)
  {
    UserActivityMgr.RequestSource localRequestSource = UserActivityMgr.RequestSource.SUGGEST;
    UserActivityMgr.IntervalCountTag localIntervalCountTag = UserActivityMgr.IntervalCountTag.HISTORY_CLICKED_IN_EDIT_MODE;
    switch (2.$SwitchMap$com$google$android$apps$translate$editor$SuggestAdapter$SuggestType[paramSuggestEntry.getType().ordinal()])
    {
    default:
      if ((paramSuggestEntry.getType() != null) && (!TranslateApplication.isReleaseBuild(paramContext)))
        throw new IllegalArgumentException("New suggestion type ignored.");
      break;
    case 2:
      if (paramBoolean)
      {
        localRequestSource = UserActivityMgr.RequestSource.TWS_SPELL_CORRECTION_IN_EDIT_MODE;
        localIntervalCountTag = UserActivityMgr.IntervalCountTag.SPELL_CORRECTION_CLICKED_IN_EDIT_MODE;
      }
      break;
    case 3:
    case 1:
    }
    while (true)
    {
      UserActivityMgr.get().setTranslationSource(localRequestSource);
      UserActivityMgr.get().incrementIntervalCount(localIntervalCountTag, 1);
      return;
      localRequestSource = UserActivityMgr.RequestSource.TWS_SPELL_CORRECTION_ON_CHIP_VIEW;
      localIntervalCountTag = UserActivityMgr.IntervalCountTag.SPELL_CORRECTION_CLICKED_ON_CHIP_VIEW;
      continue;
      if (paramBoolean)
      {
        localRequestSource = UserActivityMgr.RequestSource.TWS_LANGID_IN_EDIT_MODE;
        localIntervalCountTag = UserActivityMgr.IntervalCountTag.LANGID_CLICKED_IN_EDIT_MODE;
      }
      else
      {
        localRequestSource = UserActivityMgr.RequestSource.TWS_LANGID_ON_CHIP_VIEW;
        localIntervalCountTag = UserActivityMgr.IntervalCountTag.LANGID_CLICKED_ON_CHIP_VIEW;
      }
    }
  }

  public int getCount()
  {
    if (this.mSuggestList == null)
      return 0;
    return this.mSuggestList.size();
  }

  public Filter getFilter()
  {
    return new SuggestFilter(null);
  }

  public Object getItem(int paramInt)
  {
    if (this.mSuggestList == null)
      return null;
    return (SuggestEntry)this.mSuggestList.get(paramInt);
  }

  public long getItemId(int paramInt)
  {
    return paramInt;
  }

  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    Logger.d("SuggestAdapter", "adapter getview called at position: " + paramInt);
    View localView1;
    InstantTranslateTextView localInstantTranslateTextView;
    SuggestEntry localSuggestEntry;
    Entry localEntry;
    View localView2;
    TextView localTextView;
    label169: String str1;
    String str2;
    String str3;
    label201: Language[] arrayOfLanguage;
    Constants.AppearanceType localAppearanceType;
    if (paramView != null)
    {
      localView1 = paramView;
      localInstantTranslateTextView = (InstantTranslateTextView)localView1.findViewById(R.id.text);
      localSuggestEntry = (SuggestEntry)getItem(paramInt);
      localEntry = localSuggestEntry.getEntry();
      Language localLanguage1 = this.mLanguageList.getFromLanguageByShortName(localEntry.getFromLanguageShortName());
      Language localLanguage2 = this.mLanguageList.getToLanguageByShortName(localEntry.getToLanguageShortName());
      localView2 = localView1.findViewById(R.id.ic_suggest_type);
      localTextView = (TextView)localView1.findViewById(R.id.suggest_type);
      switch (2.$SwitchMap$com$google$android$apps$translate$editor$SuggestAdapter$SuggestType[localSuggestEntry.getType().ordinal()])
      {
      default:
        localTextView.setVisibility(8);
        localView2.setVisibility(0);
        localInstantTranslateTextView.setTypeface(Typeface.DEFAULT_BOLD);
        str1 = localInstantTranslateTextView.getText().toString();
        str2 = localSuggestEntry.getHtmlInputText();
        if (str2 == null)
        {
          str3 = localSuggestEntry.getEntry().getInputText();
          arrayOfLanguage = new Language[] { localLanguage1, localLanguage2 };
          localAppearanceType = Constants.AppearanceType.UNCHANGED;
          if (str2 == null)
            break label387;
        }
        break;
      case 1:
      case 2:
      }
    }
    label387: for (boolean bool = true; ; bool = false)
    {
      Util.setTextAndFont(localInstantTranslateTextView, str3, arrayOfLanguage, localAppearanceType, bool);
      if (localSuggestEntry.getType() == SuggestType.TWS_SPELL_CORRECTION)
        localInstantTranslateTextView.scrollToEdge(str1, this.mEditorField, true);
      localEntry.getTranslation();
      localView1.findViewById(R.id.translation).setVisibility(8);
      return localView1;
      localView1 = View.inflate(this.mActivity, R.layout.suggest_text, null);
      break;
      localTextView.setText(this.mActivity.getString(R.string.label_translate_from));
      localTextView.setVisibility(0);
      localView2.setVisibility(4);
      localInstantTranslateTextView.setTypeface(Typeface.DEFAULT_BOLD);
      break label169;
      localTextView.setText(this.mActivity.getString(R.string.msg_did_you_mean));
      localTextView.setVisibility(0);
      localView2.setVisibility(4);
      localInstantTranslateTextView.setTypeface(Typeface.DEFAULT);
      break label169;
      str3 = str2;
      break label201;
    }
  }

  public boolean setDetectedSrcShortLangName(String paramString1, String paramString2)
  {
    Logger.d("SuggestAdapter", "setDetectedSrcShortLangName " + paramString1);
    this.mSrcLangDetectEntry = getDetectedLanguageSuggestEntry(this.mLanguageList, this.mSourceLanguage, this.mTargetLanguage, paramString1, paramString2);
    return this.mSrcLangDetectEntry != null;
  }

  public boolean setSpellCorrection(SpannableStringBuilder paramSpannableStringBuilder, String paramString)
  {
    Logger.d("SuggestAdapter", "setSpellCorrection " + paramSpannableStringBuilder);
    this.mSpellCorrectionEntry = getSpellCorrectionSuggestEntry(this.mSourceLanguage, this.mTargetLanguage, paramSpannableStringBuilder, paramString);
    return this.mSpellCorrectionEntry != null;
  }

  public static class SuggestEntry
  {
    private Entry mEntry;
    private String mHtmlInputText;
    private String mSourceText;
    private SuggestAdapter.SuggestType mType;

    public SuggestEntry(Entry paramEntry, SuggestAdapter.SuggestType paramSuggestType, String paramString1, String paramString2)
    {
      this.mEntry = paramEntry;
      this.mType = paramSuggestType;
      this.mHtmlInputText = paramString1;
      this.mSourceText = TranslateEntry.normalizeInputText(paramString2);
    }

    public Entry getEntry()
    {
      return this.mEntry;
    }

    public String getHtmlInputText()
    {
      return this.mHtmlInputText;
    }

    public String getSourceText()
    {
      return this.mSourceText;
    }

    public SuggestAdapter.SuggestType getType()
    {
      return this.mType;
    }
  }

  private class SuggestFilter extends Filter
  {
    private SuggestFilter()
    {
    }

    public CharSequence convertResultToString(Object paramObject)
    {
      return ((SuggestAdapter.SuggestEntry)paramObject).getEntry().getInputText();
    }

    // ERROR //
    protected Filter.FilterResults performFiltering(CharSequence paramCharSequence)
    {
      // Byte code:
      //   0: ldc 36
      //   2: new 38	java/lang/StringBuilder
      //   5: dup
      //   6: invokespecial 39	java/lang/StringBuilder:<init>	()V
      //   9: ldc 41
      //   11: invokevirtual 45	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   14: aload_1
      //   15: invokevirtual 48	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
      //   18: invokevirtual 51	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   21: invokestatic 57	com/google/android/apps/translate/Logger:d	(Ljava/lang/String;Ljava/lang/String;)V
      //   24: iconst_0
      //   25: istore_2
      //   26: aconst_null
      //   27: astore_3
      //   28: iconst_0
      //   29: istore 4
      //   31: aload_1
      //   32: ifnull +200 -> 232
      //   35: aload_0
      //   36: getfield 10	com/google/android/apps/translate/editor/SuggestAdapter$SuggestFilter:this$0	Lcom/google/android/apps/translate/editor/SuggestAdapter;
      //   39: aload_1
      //   40: invokestatic 63	com/google/android/apps/translate/editor/SuggestAdapter:access$000	(Lcom/google/android/apps/translate/editor/SuggestAdapter;Ljava/lang/CharSequence;)Ljava/util/List;
      //   43: astore 12
      //   45: aload_0
      //   46: getfield 10	com/google/android/apps/translate/editor/SuggestAdapter$SuggestFilter:this$0	Lcom/google/android/apps/translate/editor/SuggestAdapter;
      //   49: invokestatic 67	com/google/android/apps/translate/editor/SuggestAdapter:access$100	(Lcom/google/android/apps/translate/editor/SuggestAdapter;)Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;
      //   52: ifnull +239 -> 291
      //   55: aload 12
      //   57: invokeinterface 73 1 0
      //   62: astore 17
      //   64: aload 17
      //   66: invokeinterface 79 1 0
      //   71: istore 18
      //   73: iconst_0
      //   74: istore 19
      //   76: iconst_0
      //   77: istore_2
      //   78: iconst_0
      //   79: istore 4
      //   81: iload 18
      //   83: ifeq +41 -> 124
      //   86: aload 17
      //   88: invokeinterface 83 1 0
      //   93: checkcast 20	com/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry
      //   96: invokevirtual 24	com/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry:getEntry	()Lcom/google/android/apps/translate/history/Entry;
      //   99: invokevirtual 30	com/google/android/apps/translate/history/Entry:getInputText	()Ljava/lang/String;
      //   102: aload_0
      //   103: getfield 10	com/google/android/apps/translate/editor/SuggestAdapter$SuggestFilter:this$0	Lcom/google/android/apps/translate/editor/SuggestAdapter;
      //   106: invokestatic 67	com/google/android/apps/translate/editor/SuggestAdapter:access$100	(Lcom/google/android/apps/translate/editor/SuggestAdapter;)Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;
      //   109: invokevirtual 24	com/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry:getEntry	()Lcom/google/android/apps/translate/history/Entry;
      //   112: invokevirtual 30	com/google/android/apps/translate/history/Entry:getInputText	()Ljava/lang/String;
      //   115: invokestatic 89	android/text/TextUtils:equals	(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z
      //   118: ifeq -54 -> 64
      //   121: iconst_1
      //   122: istore 19
      //   124: iconst_0
      //   125: istore_2
      //   126: iconst_0
      //   127: istore 4
      //   129: iload 19
      //   131: ifne +21 -> 152
      //   134: aload 12
      //   136: aload_0
      //   137: getfield 10	com/google/android/apps/translate/editor/SuggestAdapter$SuggestFilter:this$0	Lcom/google/android/apps/translate/editor/SuggestAdapter;
      //   140: invokestatic 67	com/google/android/apps/translate/editor/SuggestAdapter:access$100	(Lcom/google/android/apps/translate/editor/SuggestAdapter;)Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;
      //   143: invokeinterface 93 2 0
      //   148: pop
      //   149: iconst_1
      //   150: istore 4
      //   152: aload_0
      //   153: getfield 10	com/google/android/apps/translate/editor/SuggestAdapter$SuggestFilter:this$0	Lcom/google/android/apps/translate/editor/SuggestAdapter;
      //   156: invokestatic 97	com/google/android/apps/translate/editor/SuggestAdapter:access$300	(Lcom/google/android/apps/translate/editor/SuggestAdapter;)Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestionFilter;
      //   159: ifnull +19 -> 178
      //   162: aload_0
      //   163: getfield 10	com/google/android/apps/translate/editor/SuggestAdapter$SuggestFilter:this$0	Lcom/google/android/apps/translate/editor/SuggestAdapter;
      //   166: invokestatic 97	com/google/android/apps/translate/editor/SuggestAdapter:access$300	(Lcom/google/android/apps/translate/editor/SuggestAdapter;)Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestionFilter;
      //   169: aload 12
      //   171: invokeinterface 103 2 0
      //   176: astore 12
      //   178: aconst_null
      //   179: astore_3
      //   180: aload 12
      //   182: ifnull +50 -> 232
      //   185: aload 12
      //   187: invokeinterface 106 1 0
      //   192: istore 15
      //   194: aconst_null
      //   195: astore_3
      //   196: iload 15
      //   198: ifne +34 -> 232
      //   201: new 108	android/widget/Filter$FilterResults
      //   204: dup
      //   205: invokespecial 109	android/widget/Filter$FilterResults:<init>	()V
      //   208: astore 16
      //   210: aload 16
      //   212: aload 12
      //   214: invokeinterface 113 1 0
      //   219: putfield 117	android/widget/Filter$FilterResults:count	I
      //   222: aload 16
      //   224: aload 12
      //   226: putfield 121	android/widget/Filter$FilterResults:values	Ljava/lang/Object;
      //   229: aload 16
      //   231: astore_3
      //   232: invokestatic 127	com/google/android/apps/translate/UserActivityMgr:get	()Lcom/google/android/apps/translate/UserActivityMgr;
      //   235: getstatic 133	com/google/android/apps/translate/UserActivityMgr$IntervalCountTag:SPELL_CORRECTION_SHOWN_IN_EDIT_MODE	Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;
      //   238: iload 4
      //   240: invokevirtual 137	com/google/android/apps/translate/UserActivityMgr:setImpression	(Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;Z)Lcom/google/android/apps/translate/UserActivityMgr;
      //   243: pop
      //   244: invokestatic 127	com/google/android/apps/translate/UserActivityMgr:get	()Lcom/google/android/apps/translate/UserActivityMgr;
      //   247: getstatic 140	com/google/android/apps/translate/UserActivityMgr$IntervalCountTag:LANGID_SHOWN_IN_EDIT_MODE	Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;
      //   250: iload_2
      //   251: invokevirtual 137	com/google/android/apps/translate/UserActivityMgr:setImpression	(Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;Z)Lcom/google/android/apps/translate/UserActivityMgr;
      //   254: pop
      //   255: invokestatic 127	com/google/android/apps/translate/UserActivityMgr:get	()Lcom/google/android/apps/translate/UserActivityMgr;
      //   258: astore 7
      //   260: getstatic 143	com/google/android/apps/translate/UserActivityMgr$IntervalCountTag:HISTORY_SHOWN_IN_EDIT_MODE	Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;
      //   263: astore 8
      //   265: aload_3
      //   266: ifnull +81 -> 347
      //   269: aload_3
      //   270: getfield 117	android/widget/Filter$FilterResults:count	I
      //   273: ifle +74 -> 347
      //   276: iconst_1
      //   277: istore 9
      //   279: aload 7
      //   281: aload 8
      //   283: iload 9
      //   285: invokevirtual 137	com/google/android/apps/translate/UserActivityMgr:setImpression	(Lcom/google/android/apps/translate/UserActivityMgr$IntervalCountTag;Z)Lcom/google/android/apps/translate/UserActivityMgr;
      //   288: pop
      //   289: aload_3
      //   290: areturn
      //   291: aload_0
      //   292: getfield 10	com/google/android/apps/translate/editor/SuggestAdapter$SuggestFilter:this$0	Lcom/google/android/apps/translate/editor/SuggestAdapter;
      //   295: invokestatic 146	com/google/android/apps/translate/editor/SuggestAdapter:access$200	(Lcom/google/android/apps/translate/editor/SuggestAdapter;)Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;
      //   298: astore 13
      //   300: iconst_0
      //   301: istore_2
      //   302: iconst_0
      //   303: istore 4
      //   305: aload 13
      //   307: ifnull -155 -> 152
      //   310: aload 12
      //   312: aload_0
      //   313: getfield 10	com/google/android/apps/translate/editor/SuggestAdapter$SuggestFilter:this$0	Lcom/google/android/apps/translate/editor/SuggestAdapter;
      //   316: invokestatic 146	com/google/android/apps/translate/editor/SuggestAdapter:access$200	(Lcom/google/android/apps/translate/editor/SuggestAdapter;)Lcom/google/android/apps/translate/editor/SuggestAdapter$SuggestEntry;
      //   319: invokeinterface 93 2 0
      //   324: pop
      //   325: iconst_1
      //   326: istore_2
      //   327: iconst_0
      //   328: istore 4
      //   330: goto -178 -> 152
      //   333: astore 11
      //   335: ldc 36
      //   337: ldc 148
      //   339: aload 11
      //   341: invokestatic 151	com/google/android/apps/translate/Logger:d	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
      //   344: goto -112 -> 232
      //   347: iconst_0
      //   348: istore 9
      //   350: goto -71 -> 279
      //   353: astore 11
      //   355: aload 16
      //   357: astore_3
      //   358: goto -23 -> 335
      //
      // Exception table:
      //   from	to	target	type
      //   35	64	333	java/lang/Exception
      //   64	73	333	java/lang/Exception
      //   86	121	333	java/lang/Exception
      //   134	149	333	java/lang/Exception
      //   152	178	333	java/lang/Exception
      //   185	194	333	java/lang/Exception
      //   201	210	333	java/lang/Exception
      //   291	300	333	java/lang/Exception
      //   310	325	333	java/lang/Exception
      //   210	229	353	java/lang/Exception
    }

    protected void publishResults(CharSequence paramCharSequence, Filter.FilterResults paramFilterResults)
    {
      Logger.d("SuggestAdapter", "in publish result: " + paramCharSequence);
      SuggestAdapter localSuggestAdapter = SuggestAdapter.this;
      if ((paramFilterResults != null) && (paramFilterResults.count > 0));
      for (List localList = (List)paramFilterResults.values; ; localList = null)
      {
        SuggestAdapter.access$402(localSuggestAdapter, localList);
        SuggestAdapter.this.notifyDataSetChanged();
        return;
      }
    }
  }

  public static enum SuggestType
  {
    static
    {
      TWS_DETECTED_SRCLANG = new SuggestType("TWS_DETECTED_SRCLANG", 2);
      SuggestType[] arrayOfSuggestType = new SuggestType[3];
      arrayOfSuggestType[0] = HISTORY;
      arrayOfSuggestType[1] = TWS_SPELL_CORRECTION;
      arrayOfSuggestType[2] = TWS_DETECTED_SRCLANG;
    }
  }

  public static abstract interface SuggestionFilter
  {
    public abstract List<SuggestAdapter.SuggestEntry> filter(List<SuggestAdapter.SuggestEntry> paramList);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.editor.SuggestAdapter
 * JD-Core Version:    0.6.2
 */