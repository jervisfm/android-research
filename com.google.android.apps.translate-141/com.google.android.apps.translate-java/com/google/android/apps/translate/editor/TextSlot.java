package com.google.android.apps.translate.editor;

import android.content.Context;
import android.text.Editable;
import android.text.TextUtils;
import android.util.AttributeSet;
import com.google.android.apps.translate.Logger;
import com.google.android.apps.translate.asreditor.CorrectionTextValue;
import com.google.android.apps.translate.asreditor.KeyboardStateTracker;
import com.google.android.apps.translate.asreditor.PopupManager;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TextSlot extends SlotView
{
  private static final String TAG = "TextSlot";
  private static final Pattern TOKENIZE_PATTERN = Pattern.compile("([\\p{InHangulJamo}\\p{InHangulCompatibilityJamo}\\p{InHangulSyllables}\\p{InCjkRadicalsSupplement}\\p{InKangxiRadicals}\\p{InIdeographicDescriptionCharacters}\\p{InCjkSymbolsAndPunctuation}\\p{InEnclosedCjkLettersAndMonths}\\p{InCjkUnifiedIdeographs}\\p{InCjkUnifiedIdeographsExtensionA}\\p{InCjkUnifiedIdeographsExtensionB}\\p{InHiragana}\\p{InKatakana}])|(^|\\s+|\\b)([\\S&&[^\\p{InHangulJamo}\\p{InHangulCompatibilityJamo}\\p{InHangulSyllables}\\p{InCjkRadicalsSupplement}\\p{InKangxiRadicals}\\p{InIdeographicDescriptionCharacters}\\p{InCjkSymbolsAndPunctuation}\\p{InEnclosedCjkLettersAndMonths}\\p{InCjkUnifiedIdeographs}\\p{InCjkUnifiedIdeographsExtensionA}\\p{InCjkUnifiedIdeographsExtensionB}\\p{InHiragana}\\p{InKatakana}]]+)");
  private InstantTranslateHandler mInstantTranslateHandler;

  public TextSlot(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    setMinLines(1);
  }

  private void addHighlightsForSpan(CorrectionTextValue paramCorrectionTextValue)
  {
    Logger.d("TextSlot", "addHighlightsForSpan");
    Editable localEditable = getEditableText();
    int i = localEditable.getSpanStart(paramCorrectionTextValue);
    String str = localEditable.subSequence(i, localEditable.getSpanEnd(paramCorrectionTextValue)).toString();
    Matcher localMatcher = TOKENIZE_PATTERN.matcher(str);
    label155: 
    while (localMatcher.find())
    {
      int j;
      if (localMatcher.group(3) != null)
        j = localMatcher.start(3);
      for (int k = localMatcher.end(3); ; k = localMatcher.end(1))
      {
        if (paramCorrectionTextValue.getAlternates(str, j, k, isIndexAtStartOfSentence(i + j)).size() <= 0)
          break label155;
        localEditable.setSpan(new HighlightSpan(getContext()), i + j, i + k, 33);
        break;
        j = localMatcher.start(1);
      }
    }
  }

  private String capitalizeString(String paramString)
  {
    if (paramString.length() < 1)
      return paramString;
    return paramString.substring(0, 1).toUpperCase() + paramString.substring(1);
  }

  private CorrectionTextValue[] getPhraseSpans()
  {
    return (CorrectionTextValue[])getEditableText().getSpans(0, length(), CorrectionTextValue.class);
  }

  private boolean isIndexAtStartOfSentence(int paramInt)
  {
    return TextUtils.getCapsMode(getStringValue(), paramInt, 16384) != 0;
  }

  private void refreshHighlights()
  {
    Logger.d("TextSlot", "refreshHighlights this=" + toString());
    removeAllHighlights();
    CorrectionTextValue[] arrayOfCorrectionTextValue = getPhraseSpans();
    int i = arrayOfCorrectionTextValue.length;
    for (int j = 0; j < i; j++)
      addHighlightsForSpan(arrayOfCorrectionTextValue[j]);
  }

  private void removeAllHighlights()
  {
    Logger.d("TextSlot", "removeAllHighlights");
    Editable localEditable = getEditableText();
    HighlightSpan[] arrayOfHighlightSpan = (HighlightSpan[])localEditable.getSpans(0, length(), HighlightSpan.class);
    int i = arrayOfHighlightSpan.length;
    for (int j = 0; j < i; j++)
      localEditable.removeSpan(arrayOfHighlightSpan[j]);
  }

  public void deleteBetween(int paramInt1, int paramInt2)
  {
    if (paramInt2 < paramInt1)
    {
      int n = paramInt1;
      paramInt1 = paramInt2;
      paramInt2 = n;
    }
    Editable localEditable = getEditableText();
    localEditable.replace(paramInt1, paramInt2, "");
    int i;
    if (paramInt1 < length())
    {
      i = 1;
      if (i != 0)
        if (localEditable.charAt(paramInt1) != ' ')
          break label118;
    }
    label118: for (int j = 1; ; j = 0)
    {
      int k;
      if (paramInt1 != 0)
      {
        int m = localEditable.charAt(paramInt1 - 1);
        k = 0;
        if (m != 32);
      }
      else
      {
        k = 1;
      }
      if ((j != 0) && (k != 0))
        localEditable.replace(paramInt1, paramInt1 + 1, "");
      return;
      i = 0;
      break;
    }
  }

  protected void enterKeyboardMode()
  {
    Logger.d("TextSlot", "enterKeyboardMode this=" + toString());
    super.enterKeyboardMode();
    if (!isInCorrectionMode())
      removeAllHighlights();
  }

  protected void exitKeyboardMode()
  {
    Logger.d("TextSlot", "exitKeyboardMode this=" + toString());
    super.exitKeyboardMode();
    refreshHighlights();
  }

  public void forceInstantTranslation()
  {
    if (this.mInstantTranslateHandler != null)
      this.mInstantTranslateHandler.updateSourceText(getText());
  }

  public TextValue getSlotValue()
  {
    return new TextValue(getStringValue());
  }

  public void init(Context paramContext, PopupManager paramPopupManager, KeyboardStateTracker paramKeyboardStateTracker, SlotContainer paramSlotContainer)
  {
    super.init(paramContext, paramPopupManager, paramKeyboardStateTracker, paramSlotContainer);
  }

  public boolean inputSlotValue(SlotValue paramSlotValue)
  {
    Logger.d("TextSlot", "inputSlotValue this=" + toString());
    if (paramSlotValue == null)
    {
      Logger.w("TextSlot", "Tried to input null value");
      return false;
    }
    if ((paramSlotValue instanceof CorrectionTextValue))
    {
      CorrectionTextValue localCorrectionTextValue = (CorrectionTextValue)paramSlotValue;
      String str = localCorrectionTextValue.getText();
      if (isSelectionAtStartOfSentence())
        str = capitalizeString(str);
      int[] arrayOfInt = new int[2];
      inputString(str, arrayOfInt);
      getEditableText().setSpan(localCorrectionTextValue, arrayOfInt[0], arrayOfInt[1], 33);
      refreshHighlights();
      return true;
    }
    Logger.w("TextSlot", "Inputed SlotValue of unexpected type: " + paramSlotValue.getClass());
    return false;
  }

  public boolean isInCorrectionMode()
  {
    return (!isInKeyboardMode()) || (isHardKeyboardVisible());
  }

  public boolean isSelectionAtStartOfSentence()
  {
    return isIndexAtStartOfSentence(getSelectionStart());
  }

  protected void onValueChanged(String paramString)
  {
    if (this.mInstantTranslateHandler != null)
      this.mInstantTranslateHandler.updateSourceText(paramString);
    if (isInCorrectionMode())
      refreshHighlights();
    getPopupManager().dismissCurrentPopup();
  }

  public void setInstantTranslateHandler(InstantTranslateHandler paramInstantTranslateHandler)
  {
    this.mInstantTranslateHandler = paramInstantTranslateHandler;
  }

  public void setSlotValue(SlotValue paramSlotValue)
  {
    Logger.d("TextSlot", "setSlotValue this=" + toString());
    getEditableText().replace(0, length(), "");
    if (paramSlotValue != null)
      inputSlotValue(paramSlotValue);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.editor.TextSlot
 * JD-Core Version:    0.6.2
 */