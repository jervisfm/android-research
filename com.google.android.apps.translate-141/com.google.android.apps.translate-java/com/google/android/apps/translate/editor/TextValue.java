package com.google.android.apps.translate.editor;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class TextValue extends SlotValue
  implements Parcelable
{
  public static final Parcelable.Creator<TextValue> CREATOR = new Parcelable.Creator()
  {
    public TextValue createFromParcel(Parcel paramAnonymousParcel)
    {
      return new TextValue(paramAnonymousParcel);
    }

    public TextValue[] newArray(int paramAnonymousInt)
    {
      return new TextValue[paramAnonymousInt];
    }
  };
  private String mText;

  public TextValue()
  {
  }

  public TextValue(Parcel paramParcel)
  {
    this.mText = paramParcel.readString();
  }

  public TextValue(TextValue paramTextValue)
  {
    this(paramTextValue.mText);
  }

  public TextValue(String paramString)
  {
    this.mText = paramString;
  }

  public String getText()
  {
    return this.mText;
  }

  public boolean isEmpty()
  {
    String str = getText();
    if (str == null);
    while (str.trim().length() == 0)
      return true;
    return false;
  }

  public String toString()
  {
    return this.mText;
  }

  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramParcel.writeString(this.mText);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.editor.TextValue
 * JD-Core Version:    0.6.2
 */