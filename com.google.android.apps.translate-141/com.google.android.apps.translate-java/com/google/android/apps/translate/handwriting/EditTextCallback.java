package com.google.android.apps.translate.handwriting;

public abstract interface EditTextCallback
{
  public abstract void onClearText();

  public abstract void onSelectionChanged(int paramInt1, int paramInt2);
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.handwriting.EditTextCallback
 * JD-Core Version:    0.6.2
 */