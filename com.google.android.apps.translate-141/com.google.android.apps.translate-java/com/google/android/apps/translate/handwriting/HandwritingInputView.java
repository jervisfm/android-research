package com.google.android.apps.translate.handwriting;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.Keyboard.Key;
import android.inputmethodservice.KeyboardView;
import android.inputmethodservice.KeyboardView.OnKeyboardActionListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.inputmethod.BaseInputConnection;
import android.view.inputmethod.ExtractedText;
import android.view.inputmethod.ExtractedTextRequest;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.google.android.apps.translate.Language;
import com.google.android.apps.translate.Logger;
import com.google.android.apps.translate.Profile;
import com.google.android.apps.translate.R.drawable;
import com.google.android.apps.translate.R.id;
import com.google.android.apps.translate.R.integer;
import com.google.android.apps.translate.R.string;
import com.google.android.apps.translate.R.xml;
import com.google.android.apps.translate.TranslateApplication;
import com.google.android.apps.translate.Util;
import com.google.android.apps.translate.editor.InputMethodView;
import com.google.android.apps.translate.editor.InputMethodView.InputMethod;
import com.google.android.apps.translate.editor.InputMethodView.InputMethodEvent;
import com.google.research.handwriting.gui.CandidateViewHandler;
import com.google.research.handwriting.gui.CandidateViewHandler.OnPickSuggestionListener;
import com.google.research.handwriting.gui.ContinuousWritingHandler;
import com.google.research.handwriting.gui.ContinuousWritingManager;
import com.google.research.handwriting.gui.HandwritingOverlayView;
import com.google.research.handwriting.gui.HandwritingOverlayView.HandwritingOverlayListener;
import com.google.research.handwriting.gui.ImeHandwritingRecognizer;
import com.google.research.handwriting.gui.ImeHandwritingRecognizer.RecognizerUISettings;
import com.google.research.handwriting.gui.ImeUtils;
import com.google.research.handwriting.gui.RecognizerHandler;
import com.google.research.handwriting.gui.SuggestedWords;
import com.google.research.handwriting.gui.UIHandler;
import com.google.research.handwriting.gui.UIHandlerCallback;
import com.google.research.handwriting.networkrecognizer.CloudRecognizer;
import com.google.research.handwriting.networkrecognizer.CloudRecognizer.CloudRecognizerSettings;
import com.google.research.handwriting.networkrecognizer.HandwritingHttpClient;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

public class HandwritingInputView extends LinearLayout
  implements KeyboardView.OnKeyboardActionListener, CandidateViewHandler.OnPickSuggestionListener, HandwritingOverlayView.HandwritingOverlayListener, View.OnTouchListener, UIHandlerCallback, EditTextCallback
{
  private static final String BUNDLE_KEY_TEXTBOX = "textbox";
  private static final String CLIENT_NAME = "atrans";
  private static final String HANDWRITING_API_RECO_URL = "/request";
  private static final String HANDWRITING_API_SERVER_NAME = "http://inputtools.google.com";
  private static final float MAX_STROKE_WIDTH_CHAR_BY_CHAR = 4.0F;
  private static final float MAX_STROKE_WIDTH_WORD_BY_WORD = 4.0F;
  private static final float MIN_STROKE_WIDTH_CHAR_BY_CHAR = 4.0F;
  private static final float MIN_STROKE_WIDTH_WORD_BY_WORD = 4.0F;
  private static final int MSG_HINT = 1;
  private static final String SPACE_KEY_LABEL_FOR_PENDING_REQUESTS = "...";
  private static final boolean SUPPORT_BOOKKEEPER = false;
  private static final String TAG = HandwritingInputView.class.getSimpleName();
  private static final boolean TALK_TO_TRANSLATE_FRONTEND = true;
  private static final String TRANSLATE_API_RECO_URL = "/translate_a/hw";
  private static final String TRANSLATE_API_SERVER_NAME = "http://translate.google.com";
  private static final boolean USE_ZH_WORD_HACK;
  private HandwritingInputViewCallback mCallback;
  private View mCandidateContainerView;
  private CandidateViewHandler mCandidateViewHandler;
  private final int mCodeDelete;
  private final int mCodeKeyboard;
  private final int mCodeSpace;
  private Context mContext;
  private ContinuousWritingHandler mContinuousWritingHandler = new ContinuousWritingHandler();
  private ContinuousWritingManager mContinuousWritingManager = new ContinuousWritingManager();
  private int mCursorSelectionEnd = -1;
  private int mCursorSelectionStart = -1;
  private EditText mEditText;
  private boolean mFullScreenHorizontal = true;
  private HandwritingOverlayView mHandwritingOverlayView;
  private TextView mHintText;
  private Object mIcLock = new Object();
  private TranslateInputConnection mInputConnection;
  private InputMethodView mInputMethodView;
  private Keyboard mKeyboard;
  private KeyboardView mKeyboardView;
  private String mLocale;
  private Handler mMessageHandler;
  private final int mPostContextSize;
  private final int mPreContextSize;
  private ImeHandwritingRecognizer mRecognizer;
  private final RecognizerHandler mRecognizerHandler;
  private CloudRecognizer.CloudRecognizerSettings mRecognizerSettings = new CloudRecognizer.CloudRecognizerSettings();
  private boolean mShouldShowTopCandidateOnSpaceKey = true;
  private boolean mShowHint = true;
  private Language mSourceLanguage;
  private Keyboard.Key mSpaceKey;
  private Language mTargetLanguage;
  private UIHandler mUIHandler;
  private final ImeHandwritingRecognizer.RecognizerUISettings mUiSettings = new ImeHandwritingRecognizer.RecognizerUISettings();

  public HandwritingInputView(Context paramContext)
  {
    this(paramContext, null);
  }

  public HandwritingInputView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    Logger.d(TAG, "HandwritingInputView constructor");
    this.mContext = paramContext;
    this.mCodeKeyboard = getResources().getInteger(R.integer.button_input_method_keyboard);
    this.mCodeSpace = getResources().getInteger(R.integer.button_space);
    this.mCodeDelete = getResources().getInteger(R.integer.button_delete);
    this.mPreContextSize = getResources().getInteger(R.integer.handwriting_pre_context_size);
    this.mPostContextSize = getResources().getInteger(R.integer.handwriting_post_context_size);
    setWillNotDraw(false);
    this.mUIHandler = new UIHandler(this, this.mContinuousWritingHandler, this.mContinuousWritingManager, this.mIcLock);
    this.mUIHandler.setHandwritingImeView(this);
    this.mRecognizerHandler = new TranslateRecognizerHandler(paramContext);
    this.mRecognizerHandler.setHIHandler(this.mUIHandler);
    this.mRecognizerHandler.setAutoSelect(false);
    this.mRecognizerHandler.setAutoSelectMilli(1000);
    this.mRecognizerHandler.setApplicationContext(getContext().getApplicationContext());
    this.mRecognizerHandler.setShowDebugInformation(false);
    this.mUIHandler.setAutoSpace(true);
    this.mUIHandler.setUseBookkeeper(false);
    this.mUiSettings.useBackGesture = false;
    this.mUiSettings.useSpaceGesture = false;
    this.mUIHandler.setRecognizerSettings(this.mUiSettings);
    setRecognizer(true);
    if (this.mRecognizerHandler.isInitialized())
      findViewById(R.id.busyDisplay).setVisibility(8);
    this.mUIHandler.setRecognizerHandler(this.mRecognizerHandler);
    this.mUIHandler.setHandwritingRecognizer(this.mRecognizer);
    this.mMessageHandler = new Handler()
    {
      public void handleMessage(Message paramAnonymousMessage)
      {
        switch (paramAnonymousMessage.what)
        {
        default:
        case 1:
        }
        do
          return;
        while (HandwritingInputView.this.mHintText == null);
        String str = (String)paramAnonymousMessage.obj;
        int i;
        int j;
        label68: TextView localTextView2;
        if (!TextUtils.isEmpty(str))
        {
          i = 1;
          TextView localTextView1 = HandwritingInputView.this.mHintText;
          j = 0;
          if (i == 0)
            break label100;
          localTextView1.setVisibility(j);
          localTextView2 = HandwritingInputView.this.mHintText;
          if (i == 0)
            break label107;
        }
        while (true)
        {
          localTextView2.setText(str);
          return;
          i = 0;
          break;
          label100: j = 8;
          break label68;
          label107: str = "";
        }
      }
    };
  }

  private void clearLimboStateAndClearText()
  {
    Logger.d(TAG, "clearLimboStateAndClearText");
    if (this.mEditText != null)
      this.mEditText.clearComposingText();
  }

  private void initializeCursor()
  {
    Logger.d(TAG, "initializeCursor");
    synchronized (this.mIcLock)
    {
      InputConnection localInputConnection = getImeCurrentInputConnection();
      if (localInputConnection != null)
      {
        ExtractedText localExtractedText = localInputConnection.getExtractedText(new ExtractedTextRequest(), 0);
        if (localExtractedText == null)
          break label97;
        if (getCursorSelectionStart() == -1)
          setCursor(localExtractedText.text.length(), localInputConnection);
      }
      label97: 
      while (getCursorSelectionStart() != -1)
      {
        if (getCursorSelectionStart() == -1)
          setCursor(this.mEditText.getText().length(), null);
        return;
      }
      setCursor(this.mEditText.getText().length(), localInputConnection);
    }
  }

  private boolean isCharacterByCharacterRecognitionLanguage(String paramString)
  {
    return (paramString.equals("ja")) || (paramString.equals("zh-CN")) || (paramString.equals("zh-TW")) || (paramString.equals("ko"));
  }

  private void setCursor(int paramInt, InputConnection paramInputConnection)
  {
    Logger.d(TAG, "setCursor");
    this.mCursorSelectionStart = paramInt;
    this.mCursorSelectionEnd = paramInt;
    if (paramInputConnection != null)
      paramInputConnection.setSelection(paramInt, paramInt);
  }

  private void setHintAndCursor(boolean paramBoolean1, boolean paramBoolean2)
  {
    Logger.d(TAG, "setHintAndCursor initializeBookkeeper=" + paramBoolean1 + " showHint=" + paramBoolean2);
    int i;
    if (this.mEditText != null)
    {
      if (TextUtils.isEmpty(this.mEditText.getText()))
        break label115;
      i = 1;
      if (!paramBoolean2)
        break label120;
      String str = getContext().getString(R.string.hint_handwriting_input_text);
      Object[] arrayOfObject = new Object[1];
      arrayOfObject[0] = this.mSourceLanguage.getLongName();
      setHint(String.format(str, arrayOfObject));
    }
    while (true)
    {
      if ((paramBoolean1) && (i == 0))
        initializeCursor();
      return;
      label115: i = 0;
      break;
      label120: setHint("");
    }
  }

  private void setKeyboardView()
  {
    Logger.d(TAG, "setKeyboardView");
    this.mKeyboardView = ((KeyboardView)findViewById(R.id.handwriting_view_buttons));
    this.mKeyboard = new Keyboard(getContext(), R.xml.handwriting_view_buttons);
    this.mKeyboardView.setKeyboard(this.mKeyboard);
    this.mKeyboardView.setPreviewEnabled(false);
    this.mKeyboardView.setOnKeyboardActionListener(this);
    int i = 0;
    int j = getResources().getInteger(R.integer.button_space);
    Iterator localIterator = this.mKeyboard.getKeys().iterator();
    while (true)
    {
      if (localIterator.hasNext())
      {
        Keyboard.Key localKey = (Keyboard.Key)localIterator.next();
        int[] arrayOfInt = localKey.codes;
        if ((arrayOfInt.length > 0) && (arrayOfInt[0] == j))
          this.mSpaceKey = localKey;
      }
      else
      {
        return;
      }
      i++;
    }
  }

  private void setRecognizer(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      this.mRecognizerSettings.useTranslateApi = true;
      this.mRecognizerSettings.server = "http://translate.google.com";
    }
    for (this.mRecognizerSettings.recoPath = "/translate_a/hw"; ; this.mRecognizerSettings.recoPath = "/request")
    {
      this.mRecognizerSettings.verbosity = 0;
      this.mRecognizerSettings.clientName = "atrans";
      HandwritingHttpClient localHandwritingHttpClient = HandwritingHttpClient.getNewHttpClient();
      Util.setUserAgentToHttpClient(localHandwritingHttpClient, Util.generateUserAgentName(getContext()));
      this.mRecognizer = new ImeHandwritingRecognizer(this.mRecognizerHandler);
      this.mRecognizer.setRecognizer(new CloudRecognizer(localHandwritingHttpClient, this.mRecognizerSettings));
      return;
      this.mRecognizerSettings.useTranslateApi = false;
      this.mRecognizerSettings.server = "http://inputtools.google.com";
    }
  }

  private void updateSpaceKeyLabel(CharSequence paramCharSequence)
  {
    Logger.d(TAG, "updateSpaceKeyLabel");
    if (TextUtils.isEmpty(paramCharSequence))
      this.mSpaceKey.label = null;
    for (this.mSpaceKey.icon = getResources().getDrawable(R.drawable.sym_keyboard_space); ; this.mSpaceKey.icon = null)
    {
      this.mKeyboardView.invalidateAllKeys();
      return;
      this.mSpaceKey.label = paramCharSequence;
    }
  }

  public void cancelStroke()
  {
  }

  public void clearLimboState()
  {
    Logger.d(TAG, "clearLimboState");
    this.mEditText.clearComposingText();
    this.mEditText.invalidate();
  }

  public void deleteText()
  {
    Logger.d(TAG, "deleteText");
    Editable localEditable = this.mEditText.getEditableText();
    if (this.mEditText.getSelectionStart() < 0)
      if (localEditable.length() > 0)
        localEditable.delete(-1 + localEditable.length(), localEditable.length());
    while (true)
    {
      this.mUIHandler.clearTranslatedText();
      return;
      if (this.mEditText.getSelectionStart() < this.mEditText.getSelectionEnd())
        localEditable.delete(this.mEditText.getSelectionStart(), this.mEditText.getSelectionEnd());
      else if (this.mEditText.getSelectionStart() >= 1)
        localEditable.delete(-1 + this.mEditText.getSelectionStart(), this.mEditText.getSelectionStart());
    }
  }

  public int getCursorSelectionEnd()
  {
    return this.mCursorSelectionEnd;
  }

  public int getCursorSelectionStart()
  {
    return this.mCursorSelectionStart;
  }

  public InputConnection getImeCurrentInputConnection()
  {
    return this.mInputConnection;
  }

  public String getRecognizerLanguage()
  {
    return this.mLocale;
  }

  public String getSourceTextToTranslate()
  {
    return this.mEditText.getText().toString();
  }

  public void initText(String paramString)
  {
    Logger.d(TAG, "initText");
    this.mEditText.setText(paramString);
    this.mUIHandler.commitText("");
    synchronized (this.mIcLock)
    {
      setCursor(paramString.length(), getImeCurrentInputConnection());
      return;
    }
  }

  public void initialize(CandidateViewHandler paramCandidateViewHandler, EditText paramEditText, InputMethodView paramInputMethodView)
  {
    Logger.d(TAG, "initialize");
    this.mEditText = paramEditText;
    this.mInputMethodView = paramInputMethodView;
    ((HandwritingTextEdit)this.mEditText).setCallback(this);
    this.mInputConnection = new TranslateInputConnection(this.mEditText, true);
    this.mEditText.addTextChangedListener(new TextWatcher()
    {
      public void afterTextChanged(Editable paramAnonymousEditable)
      {
        HandwritingInputView.this.setHintAndCursor(false, false);
      }

      public void beforeTextChanged(CharSequence paramAnonymousCharSequence, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3)
      {
      }

      public void onTextChanged(CharSequence paramAnonymousCharSequence, int paramAnonymousInt1, int paramAnonymousInt2, int paramAnonymousInt3)
      {
      }
    });
    this.mCandidateViewHandler = paramCandidateViewHandler;
    this.mCandidateViewHandler.setListener(this);
    this.mCandidateViewHandler.setCompletionListLocationBelowCandidates(true);
    this.mUIHandler.setCandidateViewHandler(this.mCandidateViewHandler);
    this.mCandidateContainerView = this.mCandidateViewHandler.getContainerView();
    ((LinearLayout)findViewById(R.id.handwriting_candidate_view)).addView(this.mCandidateContainerView);
    this.mCandidateContainerView.setBackgroundResource(R.drawable.handwriting_extra_row_bg_tile);
    this.mContinuousWritingHandler.setIclock(this.mIcLock);
    this.mContinuousWritingHandler.setCurrentInputConnection(this.mInputConnection);
    this.mContinuousWritingHandler.setUIHandler(this.mUIHandler);
    this.mRecognizerHandler.onInitialized(true);
  }

  public void initializeEditText(EditText paramEditText)
  {
    this.mEditText = paramEditText;
  }

  public void onClearText()
  {
    Logger.d(TAG, "onClearText");
    this.mUIHandler.onKeyDelete();
    resetStrokes(true);
  }

  public void onCommitText(CharSequence paramCharSequence)
  {
    Logger.d(TAG, "onCommitText");
    this.mUIHandler.updateTranslatedText();
    this.mCallback.onEditCompleted(this.mEditText.getText().toString(), this.mUIHandler.getTranslatedText());
  }

  protected void onDraw(Canvas paramCanvas)
  {
    super.onDraw(paramCanvas);
  }

  public void onEditCompleted(InputMethodView.InputMethodEvent paramInputMethodEvent)
  {
    Logger.d(TAG, "onEditCompleted");
    this.mUIHandler.updateTranslatedText();
    UIHandler localUIHandler = this.mUIHandler;
    if (paramInputMethodEvent == InputMethodView.InputMethodEvent.ACCEPT);
    for (boolean bool = true; ; bool = false)
    {
      localUIHandler.finishInput(bool);
      this.mCallback.onEditCompleted(this.mEditText.getText().toString(), this.mUIHandler.getTranslatedText());
      Logger.d(TAG, "mTranslatedText is... " + this.mUIHandler.getTranslatedText());
      resetStrokes(false);
      this.mUIHandler.setHandwritingOverlayView(null);
      setVisibility(8);
      return;
    }
  }

  public void onEditStarted()
  {
    Logger.d(TAG, "onEditStarted");
    if (this.mUIHandler != null)
      this.mUIHandler.setHandwritingOverlayView(this.mHandwritingOverlayView);
    if ((this.mRecognizerHandler != null) && (this.mRecognizerHandler.isInitialized()))
      findViewById(R.id.busyDisplay).setVisibility(8);
    int i = getWidth();
    int j = getHeight();
    if ((i > 0) && (j > 0))
      onSizeChanged(i, j);
  }

  protected void onFinishInflate()
  {
    super.onFinishInflate();
    this.mHandwritingOverlayView = ((HandwritingOverlayView)findViewById(R.id.handwriting_overlay));
    this.mHandwritingOverlayView.setHandwritingOverlayListener(this);
    this.mRecognizerHandler.setHandwritingOverlayView(this.mHandwritingOverlayView);
    this.mRecognizerHandler.setBusyDisplay(findViewById(R.id.busyDisplay));
    this.mUIHandler.setHandwritingOverlayView(this.mHandwritingOverlayView);
    setKeyboardView();
    this.mContinuousWritingManager.setRecognizer(this.mRecognizer);
    this.mContinuousWritingManager.setCallback(this.mContinuousWritingHandler);
    this.mContinuousWritingHandler.setHandwritingOverlayView(this.mHandwritingOverlayView);
    this.mRecognizer.initialize();
    this.mHintText = ((TextView)findViewById(R.id.handwriting_hint_text));
  }

  public void onKey(int paramInt, int[] paramArrayOfInt)
  {
    Logger.d(TAG, "onKey primaryCode=" + paramInt);
    this.mEditText.getEditableText();
    if (paramInt == 4)
    {
      onEditCompleted(InputMethodView.InputMethodEvent.SWITCH);
      return;
    }
    this.mUIHandler.cancelAutoSelect();
    if (paramInt >= 0)
      this.mUIHandler.onKeyNormal(paramInt);
    while (true)
    {
      this.mRecognizer.setPreContext(this.mUIHandler.getPreContext());
      this.mRecognizer.setPostContext(this.mUIHandler.getPostContext());
      this.mUIHandler.clearResults();
      return;
      if (paramInt == this.mCodeDelete)
      {
        this.mUIHandler.onKeyDelete();
      }
      else if (paramInt == this.mCodeKeyboard)
      {
        resetStrokes(false);
        this.mUIHandler.setHandwritingOverlayView(null);
        setVisibility(8);
        this.mInputMethodView.startInputMethod(InputMethodView.InputMethod.KEYBOARD);
      }
      else
      {
        sendKeyChar((char)paramInt);
      }
    }
  }

  public void onPenDown(float paramFloat1, float paramFloat2, long paramLong, float paramFloat3)
  {
    if (this.mShowHint)
      this.mShowHint = false;
    setHint("");
    ((HandwritingTextEdit)this.mEditText).setIsTextEditor(false);
    if (this.mShouldShowTopCandidateOnSpaceKey)
      updateSpaceKeyLabel("...");
    this.mUIHandler.onPenDown(paramFloat1, paramFloat2, paramLong, paramFloat3);
  }

  public void onPenMove(float paramFloat1, float paramFloat2, long paramLong, float paramFloat3)
  {
    this.mUIHandler.onPenMove(paramFloat1, paramFloat2, paramLong, paramFloat3);
  }

  public void onPenUp(float paramFloat1, float paramFloat2, long paramLong, float paramFloat3)
  {
    this.mUIHandler.onPenUp(paramFloat1, paramFloat2, paramLong, paramFloat3);
  }

  public void onPickSuggestion(int paramInt, CharSequence paramCharSequence, boolean paramBoolean)
  {
    Logger.d(TAG, "onPickSuggestion");
    this.mUIHandler.onPickSuggestion(paramInt, paramCharSequence, paramBoolean);
  }

  public void onPress(int paramInt)
  {
  }

  public void onRelease(int paramInt)
  {
  }

  public void onRestoreInstanceState(Bundle paramBundle)
  {
    initText(paramBundle.getString("textbox"));
  }

  public void onSaveInstanceState(Bundle paramBundle)
  {
    paramBundle.putString("textbox", this.mEditText.getText().toString());
  }

  public void onSelectionChanged(int paramInt1, int paramInt2)
  {
    Logger.d(TAG, "onSelectionChanged");
    if (this.mUIHandler == null)
      return;
    this.mCursorSelectionStart = paramInt1;
    this.mCursorSelectionEnd = paramInt1;
    if (this.mUIHandler.getJustModifiedComposingRegion())
    {
      this.mUIHandler.setJustModifiedComposingRegion(false);
      return;
    }
    this.mUIHandler.onUpdateSelection(paramInt1, paramInt2);
  }

  public void onSizeChanged(int paramInt1, int paramInt2)
  {
    Logger.d(TAG, "onSizeChanged width=" + paramInt1);
    Logger.d(TAG, "onSizeChanged height=" + paramInt2);
    if (this.mUIHandler != null)
      this.mUIHandler.onSizeChanged(paramInt1, paramInt2);
    clearLimboStateAndClearText();
    setKeyboardView();
    if (this.mEditText != null)
      ((InputMethodManager)this.mContext.getSystemService("input_method")).hideSoftInputFromWindow(this.mEditText.getWindowToken(), 0);
  }

  public void onText(CharSequence paramCharSequence)
  {
  }

  public boolean onTouch(View paramView, MotionEvent paramMotionEvent)
  {
    Logger.d(TAG, "onTouch");
    if (this.mHandwritingOverlayView == null)
      return false;
    int i = (int)paramMotionEvent.getX();
    int j = (int)paramMotionEvent.getY();
    Logger.d(TAG, "onTouch: [" + i + ", " + j + "]");
    switch (paramMotionEvent.getAction())
    {
    case 0:
    case 1:
    case 2:
    }
    return true;
  }

  public void resetStrokes(boolean paramBoolean)
  {
    Logger.d(TAG, "resetStrokes");
    this.mUIHandler.clearRecognizer();
    if (paramBoolean)
      this.mEditText.setText("");
    ((HandwritingTextEdit)this.mEditText).setIsTextEditor(true);
    setHintAndCursor(true, this.mShowHint);
    this.mUIHandler.clearResults();
  }

  public void sendKeyChar(char paramChar)
  {
    Logger.d(TAG, "sendKeyChar");
    Editable localEditable = this.mEditText.getEditableText();
    if (paramChar == this.mCodeSpace)
    {
      Logger.d(TAG, "sendKeyChar codeSpace");
      if (this.mEditText.getSelectionStart() >= 0)
        localEditable.insert(this.mEditText.getSelectionStart(), " ");
      this.mUIHandler.clearTranslatedText();
      return;
    }
    Logger.d(TAG, "sendKeyChar ignoring primaryCode=" + paramChar);
  }

  public void setCallback(HandwritingInputViewCallback paramHandwritingInputViewCallback)
  {
    this.mCallback = paramHandwritingInputViewCallback;
  }

  public void setHint(String paramString)
  {
    this.mMessageHandler.sendMessage(Message.obtain(this.mMessageHandler, 1, paramString));
  }

  public void setImeCandidatesViewShown(boolean paramBoolean)
  {
    Logger.d(TAG, "setImeCandidatesViewShown");
    if (this.mFullScreenHorizontal)
      return;
    View localView = this.mCandidateContainerView;
    if (paramBoolean);
    for (int i = 0; ; i = 8)
    {
      localView.setVisibility(i);
      return;
    }
  }

  public void setSourceAndTargetLanguages(Language paramLanguage1, Language paramLanguage2)
  {
    boolean bool1 = true;
    Logger.d(TAG, "setSourceAndTargetLanguages");
    boolean bool2;
    boolean bool3;
    if ((this.mSourceLanguage != null) && (this.mSourceLanguage != paramLanguage1))
    {
      bool2 = bool1;
      if ((this.mTargetLanguage == null) || (this.mTargetLanguage == paramLanguage2))
        break label248;
      bool3 = bool1;
      label47: if ((bool2) || (bool3))
      {
        Logger.d(TAG, "setSourceAndTargetLanguages: language changed.");
        clearLimboStateAndClearText();
        resetStrokes(false);
        this.mUIHandler.clearTranslatedText();
      }
      this.mSourceLanguage = paramLanguage1;
      this.mTargetLanguage = paramLanguage2;
      String str = paramLanguage1.getShortName();
      if (this.mRecognizerSettings.useTranslateApi)
        this.mRecognizerSettings.targetLanguage = this.mTargetLanguage.getShortName();
      this.mLocale = Profile.getHandwritingLangShortNameFromFromShortLangName(str);
      if (this.mRecognizer != null)
      {
        if (!TranslateApplication.isReleaseBuild(this.mContext))
        {
          if (!this.mLocale.equals(Locale.CHINESE.getLanguage()))
            break label254;
          this.mLocale = "zh-words";
          setRecognizer(false);
        }
        label176: this.mRecognizer.setLanguage(this.mLocale);
      }
      if (!isCharacterByCharacterRecognitionLanguage(str))
        break label262;
      this.mHandwritingOverlayView.setMinStrokeWidth(4.0F);
      this.mHandwritingOverlayView.setMaxStrokeWidth(4.0F);
      label214: setHintAndCursor(bool1, this.mShowHint);
      if (ImeUtils.isLanguageWithSpaces(this.mSourceLanguage.getShortName()))
        break label283;
    }
    while (true)
    {
      this.mShouldShowTopCandidateOnSpaceKey = bool1;
      return;
      bool2 = false;
      break;
      label248: bool3 = false;
      break label47;
      label254: setRecognizer(bool1);
      break label176;
      label262: this.mHandwritingOverlayView.setMinStrokeWidth(4.0F);
      this.mHandwritingOverlayView.setMaxStrokeWidth(4.0F);
      break label214;
      label283: bool1 = false;
    }
  }

  public void setSuggestedWords(SuggestedWords paramSuggestedWords)
  {
    Logger.d(TAG, "setSuggestedWords");
    if (!this.mShouldShowTopCandidateOnSpaceKey)
      return;
    if ((paramSuggestedWords != null) && (paramSuggestedWords.size() > 0))
    {
      if (this.mRecognizer.hasPendingRequests())
      {
        updateSpaceKeyLabel("...");
        return;
      }
      updateSpaceKeyLabel(paramSuggestedWords.getWord(0));
      return;
    }
    updateSpaceKeyLabel("");
  }

  public void swipeDown()
  {
  }

  public void swipeLeft()
  {
  }

  public void swipeRight()
  {
  }

  public void swipeUp()
  {
  }

  public static abstract interface HandwritingInputViewCallback
  {
    public abstract void onEditCompleted(String paramString1, String paramString2);
  }

  private class TranslateInputConnection extends BaseInputConnection
  {
    public TranslateInputConnection(View paramBoolean, boolean arg3)
    {
      super(bool);
    }

    public boolean commitText(CharSequence paramCharSequence, int paramInt)
    {
      Logger.d(HandwritingInputView.TAG, "commitText text=" + paramCharSequence + ", newCursorPosition=" + paramInt);
      HandwritingInputView.this.mEditText.beginBatchEdit();
      int i = HandwritingInputView.this.mEditText.getSelectionStart();
      int j = HandwritingInputView.this.mEditText.getSelectionEnd();
      if (i < j)
      {
        HandwritingInputView.this.mEditText.getText().delete(i, j);
        HandwritingInputView.this.mUIHandler.clearTranslatedText();
      }
      boolean bool = super.commitText(paramCharSequence, paramInt);
      HandwritingInputView.this.mUIHandler.setJustModifiedComposingRegion(true);
      HandwritingInputView.access$302(HandwritingInputView.this, HandwritingInputView.this.mEditText.getSelectionStart());
      HandwritingInputView.access$402(HandwritingInputView.this, HandwritingInputView.this.mEditText.getSelectionEnd());
      ((HandwritingTextEdit)HandwritingInputView.this.mEditText).setIsTextEditor(true);
      HandwritingInputView.this.post(new Runnable()
      {
        public void run()
        {
          HandwritingInputView.this.setCursor(HandwritingInputView.this.getCursorSelectionStart(), HandwritingInputView.TranslateInputConnection.this);
          HandwritingInputView.this.mEditText.endBatchEdit();
        }
      });
      return bool;
    }

    public Editable getEditable()
    {
      return HandwritingInputView.this.mEditText.getEditableText();
    }

    public ExtractedText getExtractedText(ExtractedTextRequest paramExtractedTextRequest, int paramInt)
    {
      ExtractedText localExtractedText = new ExtractedText();
      localExtractedText.text = getEditable().toString();
      return localExtractedText;
    }
  }

  private class TranslateRecognizerHandler extends RecognizerHandler
  {
    public TranslateRecognizerHandler(Context arg2)
    {
      super();
    }

    public void errorMessage(int paramInt, Exception paramException, String paramString)
    {
      Logger.e(HandwritingInputView.TAG, "errorMessage: (" + paramInt + ") " + paramString, paramException);
      HandwritingInputView.this.setHint(HandwritingInputView.this.mContext.getString(R.string.msg_network_error));
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.handwriting.HandwritingInputView
 * JD-Core Version:    0.6.2
 */