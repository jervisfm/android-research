package com.google.android.apps.translate.handwriting;

public abstract interface HandwritingTextEdit
{
  public abstract void setCallback(EditTextCallback paramEditTextCallback);

  public abstract void setIsTextEditor(boolean paramBoolean);
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.handwriting.HandwritingTextEdit
 * JD-Core Version:    0.6.2
 */