package com.google.android.apps.translate.history;

import android.content.Context;
import android.text.TextUtils;
import com.google.android.apps.translate.Lists;
import com.google.android.apps.translate.Logger;
import com.google.android.apps.translate.Preconditions;
import com.google.android.apps.translate.Util;
import java.io.File;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicInteger;

public class BaseDb
{
  private static final int BUFFER_SIZE = 51200;
  static final Collator COLLATOR;
  private static final boolean DEBUG = false;
  private static final Entry EMPTY_ENTRY = new Entry("", "", "", "");
  private static final Comparator<Entry> ENTRY_ATIME_COMPARATOR = new Comparator()
  {
    public int compare(Entry paramAnonymousEntry1, Entry paramAnonymousEntry2)
    {
      if (paramAnonymousEntry1 == paramAnonymousEntry2);
      long l1;
      long l2;
      do
      {
        return 0;
        l1 = paramAnonymousEntry1.getAccessedTime();
        l2 = paramAnonymousEntry2.getAccessedTime();
        if (l1 < l2)
          return 1;
      }
      while (l1 <= l2);
      return -1;
    }
  };
  private static final Comparator<Entry> ENTRY_COMPARATOR;
  private static final int FLUSHER_DELAY_MS = 5000;
  private static final int FLUSHER_TIMEOUT_MS = 100000;
  private static final String TAG = "BaseDb";
  private static final int VERSION = 1;
  private final Context mContext;
  private final String mDbName;
  private final TreeMap<Entry, Entry> mEntries = new TreeMap(ENTRY_COMPARATOR);
  Thread mFlusherThread;
  private boolean mIsOnMemoryDb = false;
  private long mLastModifiedTimeMs;
  private boolean mLoadedInMemory;
  private final TreeMap<Entry, Entry> mLruEntries = new TreeMap(ENTRY_ATIME_COMPARATOR);
  private final int mMaxCount;
  boolean mMemoryDirty;
  private final AtomicInteger mOpenedCount = new AtomicInteger();

  static
  {
    COLLATOR = Collator.getInstance();
    COLLATOR.setStrength(2);
    ENTRY_COMPARATOR = new Comparator()
    {
      public int compare(Entry paramAnonymousEntry1, Entry paramAnonymousEntry2)
      {
        int i;
        if (paramAnonymousEntry1 == paramAnonymousEntry2)
          i = 0;
        do
        {
          return i;
          String str1 = paramAnonymousEntry1.getInputText();
          String str2 = paramAnonymousEntry2.getInputText();
          i = str1.compareTo(str2);
          if (i != 0)
          {
            int j = BaseDb.COLLATOR.compare(str1, str2);
            if (j != 0)
              i = j;
          }
          if (i == 0)
            i = paramAnonymousEntry1.getFromLanguageShortName().compareTo(paramAnonymousEntry2.getFromLanguageShortName());
        }
        while (i != 0);
        return paramAnonymousEntry1.getToLanguageShortName().compareTo(paramAnonymousEntry2.getToLanguageShortName());
      }
    };
  }

  public BaseDb(Context paramContext, String paramString, int paramInt, boolean paramBoolean)
  {
    this.mContext = paramContext;
    this.mDbName = ((String)Preconditions.checkNotNull(paramString));
    this.mMaxCount = paramInt;
    this.mIsOnMemoryDb = paramBoolean;
  }

  private void dumpEntry(String paramString, Entry paramEntry)
  {
    Logger.d("BaseDb", paramString + " DbName       =" + this.mDbName);
    Logger.d("BaseDb", paramString + " InputText    =" + paramEntry.getInputText());
    Logger.d("BaseDb", paramString + " OutputText   =" + paramEntry.getOutputText());
    Logger.d("BaseDb", paramString + " AccessedTime =" + Util.formatTimeStampString(this.mContext, paramEntry.getAccessedTime()));
    Logger.d("BaseDb", paramString + " CreatedTime  =" + Util.formatTimeStampString(this.mContext, paramEntry.getCreatedTime()));
  }

  private ArrayList<Entry> getFilteredEntries(int paramInt, String paramString, Collection<Entry> paramCollection)
  {
    ArrayList localArrayList = Lists.newArrayList();
    Iterator localIterator = paramCollection.iterator();
    while (true)
    {
      Entry localEntry;
      if (localIterator.hasNext())
      {
        localEntry = (Entry)localIterator.next();
        if (paramInt != 0);
      }
      else
      {
        return localArrayList;
      }
      if ((Util.filterMatches(paramString, localEntry.getInputText())) || (Util.filterMatches(paramString, localEntry.getTranslation())))
      {
        localArrayList.add(localEntry);
        paramInt--;
      }
    }
  }

  // ERROR //
  private void loadIntoMemory()
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield 204	com/google/android/apps/translate/history/BaseDb:mLoadedInMemory	Z
    //   6: ifne +12 -> 18
    //   9: aload_0
    //   10: getfield 98	com/google/android/apps/translate/history/BaseDb:mIsOnMemoryDb	Z
    //   13: istore_2
    //   14: iload_2
    //   15: ifeq +6 -> 21
    //   18: aload_0
    //   19: monitorexit
    //   20: return
    //   21: aconst_null
    //   22: astore_3
    //   23: aload_0
    //   24: iconst_0
    //   25: putfield 206	com/google/android/apps/translate/history/BaseDb:mMemoryDirty	Z
    //   28: aload_0
    //   29: getfield 89	com/google/android/apps/translate/history/BaseDb:mEntries	Ljava/util/TreeMap;
    //   32: invokevirtual 209	java/util/TreeMap:clear	()V
    //   35: aload_0
    //   36: getfield 100	com/google/android/apps/translate/history/BaseDb:mContext	Landroid/content/Context;
    //   39: aload_0
    //   40: getfield 110	com/google/android/apps/translate/history/BaseDb:mDbName	Ljava/lang/String;
    //   43: invokevirtual 215	android/content/Context:getFileStreamPath	(Ljava/lang/String;)Ljava/io/File;
    //   46: astore 10
    //   48: new 217	java/io/DataInputStream
    //   51: dup
    //   52: new 219	java/io/BufferedInputStream
    //   55: dup
    //   56: aload_0
    //   57: getfield 100	com/google/android/apps/translate/history/BaseDb:mContext	Landroid/content/Context;
    //   60: aload_0
    //   61: getfield 110	com/google/android/apps/translate/history/BaseDb:mDbName	Ljava/lang/String;
    //   64: invokevirtual 223	android/content/Context:openFileInput	(Ljava/lang/String;)Ljava/io/FileInputStream;
    //   67: ldc 7
    //   69: invokespecial 226	java/io/BufferedInputStream:<init>	(Ljava/io/InputStream;I)V
    //   72: invokespecial 229	java/io/DataInputStream:<init>	(Ljava/io/InputStream;)V
    //   75: astore 11
    //   77: aload_0
    //   78: aload 10
    //   80: invokevirtual 234	java/io/File:lastModified	()J
    //   83: putfield 236	com/google/android/apps/translate/history/BaseDb:mLastModifiedTimeMs	J
    //   86: aload 11
    //   88: invokevirtual 240	java/io/DataInputStream:readInt	()I
    //   91: istore 13
    //   93: iload 13
    //   95: iconst_1
    //   96: if_icmpne +90 -> 186
    //   99: iconst_0
    //   100: istore 15
    //   102: iload 15
    //   104: aload_0
    //   105: getfield 112	com/google/android/apps/translate/history/BaseDb:mMaxCount	I
    //   108: if_icmpge +25 -> 133
    //   111: aload 11
    //   113: invokestatic 244	com/google/android/apps/translate/history/Entry:readData	(Ljava/io/DataInputStream;)Lcom/google/android/apps/translate/history/Entry;
    //   116: astore 16
    //   118: aload 16
    //   120: invokevirtual 247	com/google/android/apps/translate/history/Entry:getFromLanguageShortName	()Ljava/lang/String;
    //   123: invokestatic 253	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   126: istore 17
    //   128: iload 17
    //   130: ifeq +26 -> 156
    //   133: aload 11
    //   135: ifnull +222 -> 357
    //   138: aload 11
    //   140: invokevirtual 256	java/io/DataInputStream:close	()V
    //   143: aload_0
    //   144: iconst_1
    //   145: putfield 204	com/google/android/apps/translate/history/BaseDb:mLoadedInMemory	Z
    //   148: goto -130 -> 18
    //   151: astore_1
    //   152: aload_0
    //   153: monitorexit
    //   154: aload_1
    //   155: athrow
    //   156: aload_0
    //   157: getfield 89	com/google/android/apps/translate/history/BaseDb:mEntries	Ljava/util/TreeMap;
    //   160: aload 16
    //   162: aload 16
    //   164: invokevirtual 260	java/util/TreeMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   167: pop
    //   168: aload_0
    //   169: getfield 91	com/google/android/apps/translate/history/BaseDb:mLruEntries	Ljava/util/TreeMap;
    //   172: aload 16
    //   174: aload 16
    //   176: invokevirtual 260	java/util/TreeMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   179: pop
    //   180: iinc 15 1
    //   183: goto -81 -> 102
    //   186: invokestatic 263	com/google/android/apps/translate/Logger:isWarning	()Z
    //   189: ifeq -56 -> 133
    //   192: aload_0
    //   193: new 116	java/lang/StringBuilder
    //   196: dup
    //   197: invokespecial 117	java/lang/StringBuilder:<init>	()V
    //   200: ldc_w 265
    //   203: invokevirtual 121	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   206: iload 13
    //   208: invokevirtual 268	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   211: ldc_w 270
    //   214: invokevirtual 121	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   217: iconst_1
    //   218: invokevirtual 268	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   221: ldc_w 272
    //   224: invokevirtual 121	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   227: invokevirtual 127	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   230: invokestatic 276	com/google/android/apps/translate/Logger:w	(Ljava/lang/Object;Ljava/lang/String;)V
    //   233: goto -100 -> 133
    //   236: astore 12
    //   238: aload 11
    //   240: astore_3
    //   241: aload_0
    //   242: invokestatic 281	java/lang/System:currentTimeMillis	()J
    //   245: putfield 236	com/google/android/apps/translate/history/BaseDb:mLastModifiedTimeMs	J
    //   248: aload_3
    //   249: ifnull -106 -> 143
    //   252: aload_3
    //   253: invokevirtual 256	java/io/DataInputStream:close	()V
    //   256: goto -113 -> 143
    //   259: astore 7
    //   261: goto -118 -> 143
    //   264: astore 14
    //   266: goto -123 -> 143
    //   269: astore 8
    //   271: aload_0
    //   272: new 116	java/lang/StringBuilder
    //   275: dup
    //   276: invokespecial 117	java/lang/StringBuilder:<init>	()V
    //   279: ldc_w 283
    //   282: invokevirtual 121	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   285: aload_0
    //   286: getfield 110	com/google/android/apps/translate/history/BaseDb:mDbName	Ljava/lang/String;
    //   289: invokevirtual 121	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   292: invokevirtual 127	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   295: aload 8
    //   297: invokestatic 287	com/google/android/apps/translate/Logger:e	(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V
    //   300: aload_3
    //   301: ifnull -158 -> 143
    //   304: aload_3
    //   305: invokevirtual 256	java/io/DataInputStream:close	()V
    //   308: goto -165 -> 143
    //   311: astore 9
    //   313: goto -170 -> 143
    //   316: astore 5
    //   318: aload_3
    //   319: ifnull +7 -> 326
    //   322: aload_3
    //   323: invokevirtual 256	java/io/DataInputStream:close	()V
    //   326: aload 5
    //   328: athrow
    //   329: astore 6
    //   331: goto -5 -> 326
    //   334: astore 5
    //   336: aload 11
    //   338: astore_3
    //   339: goto -21 -> 318
    //   342: astore 8
    //   344: aload 11
    //   346: astore_3
    //   347: goto -76 -> 271
    //   350: astore 4
    //   352: aconst_null
    //   353: astore_3
    //   354: goto -113 -> 241
    //   357: goto -214 -> 143
    //
    // Exception table:
    //   from	to	target	type
    //   2	14	151	finally
    //   138	143	151	finally
    //   143	148	151	finally
    //   252	256	151	finally
    //   304	308	151	finally
    //   322	326	151	finally
    //   326	329	151	finally
    //   77	93	236	java/io/FileNotFoundException
    //   102	128	236	java/io/FileNotFoundException
    //   156	180	236	java/io/FileNotFoundException
    //   186	233	236	java/io/FileNotFoundException
    //   252	256	259	java/io/IOException
    //   138	143	264	java/io/IOException
    //   23	77	269	java/io/IOException
    //   304	308	311	java/io/IOException
    //   23	77	316	finally
    //   241	248	316	finally
    //   271	300	316	finally
    //   322	326	329	java/io/IOException
    //   77	93	334	finally
    //   102	128	334	finally
    //   156	180	334	finally
    //   186	233	334	finally
    //   77	93	342	java/io/IOException
    //   102	128	342	java/io/IOException
    //   156	180	342	java/io/IOException
    //   186	233	342	java/io/IOException
    //   23	77	350	java/io/FileNotFoundException
  }

  private void onModified()
  {
    this.mLastModifiedTimeMs = System.currentTimeMillis();
    this.mMemoryDirty = true;
  }

  private void saveIntoDbAsync()
  {
    while (true)
    {
      try
      {
        if (this.mMemoryDirty)
        {
          boolean bool = this.mIsOnMemoryDb;
          if (!bool);
        }
        else
        {
          return;
        }
        if (this.mFlusherThread == null)
        {
          this.mFlusherThread = new Thread(this.mDbName + "-Flusher")
          {
            private boolean iterate()
            {
              try
              {
                sleep(5000L);
                BaseDb localBaseDb = BaseDb.this;
                int i = 0;
                try
                {
                  while ((!BaseDb.this.mMemoryDirty) && (i < 100000))
                  {
                    BaseDb.this.wait(5000L);
                    i += 5000;
                  }
                  if (!BaseDb.this.mMemoryDirty)
                  {
                    BaseDb.this.mFlusherThread = null;
                    return false;
                  }
                  BaseDb.this.saveIntoDb();
                  label81: return true;
                }
                finally
                {
                }
              }
              catch (InterruptedException localInterruptedException)
              {
                break label81;
              }
            }

            public void run()
            {
              while (iterate());
            }
          };
          this.mFlusherThread.start();
          continue;
        }
      }
      finally
      {
      }
      notify();
    }
  }

  public void add(Entry paramEntry)
  {
    try
    {
      Entry localEntry1 = (Entry)this.mEntries.put(paramEntry, paramEntry);
      if (localEntry1 == null)
        if (this.mEntries.size() > this.mMaxCount)
        {
          Entry localEntry2 = (Entry)this.mLruEntries.lastKey();
          this.mLruEntries.remove(localEntry2);
          this.mEntries.remove(localEntry2);
        }
      while (true)
      {
        this.mLruEntries.put(paramEntry, paramEntry);
        onModified();
        return;
        this.mLruEntries.remove(localEntry1);
      }
    }
    finally
    {
    }
  }

  public void close(boolean paramBoolean)
  {
    flush(paramBoolean);
    this.mOpenedCount.decrementAndGet();
  }

  public boolean exists(Entry paramEntry)
  {
    try
    {
      boolean bool = this.mEntries.containsKey(paramEntry);
      return bool;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public boolean exists(String paramString1, String paramString2, String paramString3)
  {
    try
    {
      boolean bool = this.mEntries.containsKey(new Entry(paramString1, paramString2, paramString3, ""));
      return bool;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public void flush(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      saveIntoDb();
      return;
    }
    saveIntoDbAsync();
  }

  public Entry get(Entry paramEntry)
  {
    try
    {
      Entry localEntry = (Entry)this.mEntries.get(paramEntry);
      return localEntry;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public Entry get(String paramString1, String paramString2, String paramString3)
  {
    try
    {
      Entry localEntry = (Entry)this.mEntries.get(new Entry(paramString1, paramString2, paramString3, ""));
      return localEntry;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public List<Entry> getAll()
  {
    try
    {
      ArrayList localArrayList = new ArrayList(this.mEntries.values());
      return localArrayList;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public List<Entry> getAll(int paramInt)
  {
    try
    {
      Object localObject2 = getAll();
      int i = ((List)localObject2).size();
      if (paramInt >= i);
      while (true)
      {
        return localObject2;
        List localList = ((List)localObject2).subList(0, paramInt);
        localObject2 = localList;
      }
    }
    finally
    {
    }
  }

  public List<Entry> getAll(int paramInt, String paramString)
  {
    try
    {
      List localList;
      if (TextUtils.isEmpty(paramString))
        localList = getAll(paramInt);
      ArrayList localArrayList;
      for (Object localObject2 = localList; ; localObject2 = localArrayList)
      {
        return localObject2;
        localArrayList = getFilteredEntries(paramInt, paramString, this.mEntries.values());
      }
    }
    finally
    {
    }
  }

  public List<Entry> getAllByATime()
  {
    try
    {
      List localList = getAllByATime(this.mLruEntries.size());
      return localList;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public List<Entry> getAllByATime(int paramInt)
  {
    try
    {
      Object localObject1 = new ArrayList(this.mLruEntries.values());
      if (paramInt >= ((ArrayList)localObject1).size());
      while (true)
      {
        ArrayList localArrayList = new ArrayList((Collection)localObject1);
        return localArrayList;
        List localList = ((ArrayList)localObject1).subList(0, paramInt);
        localObject1 = localList;
      }
    }
    finally
    {
    }
  }

  public List<Entry> getAllByATime(int paramInt, String paramString)
  {
    try
    {
      List localList;
      if (TextUtils.isEmpty(paramString))
        localList = getAllByATime(paramInt);
      ArrayList localArrayList;
      for (Object localObject2 = localList; ; localObject2 = localArrayList)
      {
        return localObject2;
        localArrayList = getFilteredEntries(paramInt, paramString, getAllByATime());
      }
    }
    finally
    {
    }
  }

  public long getLastModifiedTime()
  {
    try
    {
      long l2;
      if (this.mLastModifiedTimeMs == 0L)
        l2 = this.mContext.getFileStreamPath(this.mDbName).lastModified();
      for (long l1 = l2; ; l1 = this.mLastModifiedTimeMs)
        return l1;
    }
    finally
    {
    }
  }

  public int getRawCount()
  {
    try
    {
      int i = this.mEntries.size();
      return i;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public boolean isOpened()
  {
    return this.mOpenedCount.get() > 0;
  }

  public BaseDb open()
  {
    loadIntoMemory();
    this.mOpenedCount.incrementAndGet();
    return this;
  }

  public void remove(Entry paramEntry)
  {
    try
    {
      Entry localEntry = (Entry)this.mEntries.remove(paramEntry);
      if (localEntry != null)
      {
        this.mLruEntries.remove(localEntry);
        onModified();
      }
      return;
    }
    finally
    {
    }
  }

  public void removeAll()
  {
    try
    {
      this.mEntries.clear();
      this.mLruEntries.clear();
      onModified();
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  // ERROR //
  void saveIntoDb()
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield 206	com/google/android/apps/translate/history/BaseDb:mMemoryDirty	Z
    //   6: ifeq +10 -> 16
    //   9: aload_0
    //   10: getfield 98	com/google/android/apps/translate/history/BaseDb:mIsOnMemoryDb	Z
    //   13: ifeq +6 -> 19
    //   16: aload_0
    //   17: monitorexit
    //   18: return
    //   19: aload_0
    //   20: getfield 91	com/google/android/apps/translate/history/BaseDb:mLruEntries	Ljava/util/TreeMap;
    //   23: invokevirtual 310	java/util/TreeMap:size	()I
    //   26: istore_2
    //   27: aload_0
    //   28: getfield 91	com/google/android/apps/translate/history/BaseDb:mLruEntries	Ljava/util/TreeMap;
    //   31: invokevirtual 347	java/util/TreeMap:values	()Ljava/util/Collection;
    //   34: iload_2
    //   35: anewarray 49	com/google/android/apps/translate/history/Entry
    //   38: invokeinterface 389 2 0
    //   43: checkcast 391	[Lcom/google/android/apps/translate/history/Entry;
    //   46: astore_3
    //   47: aload_0
    //   48: iconst_0
    //   49: putfield 206	com/google/android/apps/translate/history/BaseDb:mMemoryDirty	Z
    //   52: aload_0
    //   53: monitorexit
    //   54: aload_0
    //   55: getfield 110	com/google/android/apps/translate/history/BaseDb:mDbName	Ljava/lang/String;
    //   58: astore 4
    //   60: aload 4
    //   62: monitorenter
    //   63: aconst_null
    //   64: astore 5
    //   66: new 393	java/io/DataOutputStream
    //   69: dup
    //   70: new 395	java/io/BufferedOutputStream
    //   73: dup
    //   74: aload_0
    //   75: getfield 100	com/google/android/apps/translate/history/BaseDb:mContext	Landroid/content/Context;
    //   78: aload_0
    //   79: getfield 110	com/google/android/apps/translate/history/BaseDb:mDbName	Ljava/lang/String;
    //   82: iconst_0
    //   83: invokevirtual 399	android/content/Context:openFileOutput	(Ljava/lang/String;I)Ljava/io/FileOutputStream;
    //   86: ldc 7
    //   88: invokespecial 402	java/io/BufferedOutputStream:<init>	(Ljava/io/OutputStream;I)V
    //   91: invokespecial 405	java/io/DataOutputStream:<init>	(Ljava/io/OutputStream;)V
    //   94: astore 6
    //   96: aload 6
    //   98: iconst_1
    //   99: invokevirtual 408	java/io/DataOutputStream:writeInt	(I)V
    //   102: iconst_0
    //   103: istore 11
    //   105: iload 11
    //   107: iload_2
    //   108: if_icmpge +23 -> 131
    //   111: aload_3
    //   112: iload 11
    //   114: aaload
    //   115: aload 6
    //   117: invokevirtual 412	com/google/android/apps/translate/history/Entry:writeData	(Ljava/io/DataOutputStream;)V
    //   120: iinc 11 1
    //   123: goto -18 -> 105
    //   126: astore_1
    //   127: aload_0
    //   128: monitorexit
    //   129: aload_1
    //   130: athrow
    //   131: getstatic 57	com/google/android/apps/translate/history/BaseDb:EMPTY_ENTRY	Lcom/google/android/apps/translate/history/Entry;
    //   134: aload 6
    //   136: invokevirtual 412	com/google/android/apps/translate/history/Entry:writeData	(Ljava/io/DataOutputStream;)V
    //   139: aload 6
    //   141: ifnull +8 -> 149
    //   144: aload 6
    //   146: invokevirtual 413	java/io/DataOutputStream:close	()V
    //   149: aload 4
    //   151: monitorexit
    //   152: return
    //   153: aload 4
    //   155: monitorexit
    //   156: aload 9
    //   158: athrow
    //   159: astore 12
    //   161: aload_0
    //   162: new 116	java/lang/StringBuilder
    //   165: dup
    //   166: invokespecial 117	java/lang/StringBuilder:<init>	()V
    //   169: ldc_w 415
    //   172: invokevirtual 121	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   175: aload_0
    //   176: getfield 110	com/google/android/apps/translate/history/BaseDb:mDbName	Ljava/lang/String;
    //   179: invokevirtual 121	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   182: invokevirtual 127	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   185: aload 12
    //   187: invokestatic 287	com/google/android/apps/translate/Logger:e	(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V
    //   190: aload_0
    //   191: iconst_1
    //   192: putfield 206	com/google/android/apps/translate/history/BaseDb:mMemoryDirty	Z
    //   195: goto -46 -> 149
    //   198: astore 8
    //   200: aload_0
    //   201: new 116	java/lang/StringBuilder
    //   204: dup
    //   205: invokespecial 117	java/lang/StringBuilder:<init>	()V
    //   208: ldc_w 415
    //   211: invokevirtual 121	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   214: aload_0
    //   215: getfield 110	com/google/android/apps/translate/history/BaseDb:mDbName	Ljava/lang/String;
    //   218: invokevirtual 121	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   221: invokevirtual 127	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   224: aload 8
    //   226: invokestatic 287	com/google/android/apps/translate/Logger:e	(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V
    //   229: aload_0
    //   230: iconst_1
    //   231: putfield 206	com/google/android/apps/translate/history/BaseDb:mMemoryDirty	Z
    //   234: goto -85 -> 149
    //   237: astore 7
    //   239: aload 5
    //   241: ifnull +8 -> 249
    //   244: aload 5
    //   246: invokevirtual 413	java/io/DataOutputStream:close	()V
    //   249: aload 7
    //   251: athrow
    //   252: astore 10
    //   254: aload_0
    //   255: new 116	java/lang/StringBuilder
    //   258: dup
    //   259: invokespecial 117	java/lang/StringBuilder:<init>	()V
    //   262: ldc_w 415
    //   265: invokevirtual 121	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   268: aload_0
    //   269: getfield 110	com/google/android/apps/translate/history/BaseDb:mDbName	Ljava/lang/String;
    //   272: invokevirtual 121	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   275: invokevirtual 127	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   278: aload 10
    //   280: invokestatic 287	com/google/android/apps/translate/Logger:e	(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Throwable;)V
    //   283: aload_0
    //   284: iconst_1
    //   285: putfield 206	com/google/android/apps/translate/history/BaseDb:mMemoryDirty	Z
    //   288: goto -39 -> 249
    //   291: astore 9
    //   293: goto -140 -> 153
    //   296: astore 7
    //   298: aload 6
    //   300: astore 5
    //   302: goto -63 -> 239
    //   305: astore 9
    //   307: goto -154 -> 153
    //   310: astore 8
    //   312: goto -112 -> 200
    //
    // Exception table:
    //   from	to	target	type
    //   2	16	126	finally
    //   16	18	126	finally
    //   19	54	126	finally
    //   127	129	126	finally
    //   144	149	159	java/io/IOException
    //   161	195	198	java/io/IOException
    //   66	96	237	finally
    //   244	249	252	java/io/IOException
    //   144	149	291	finally
    //   161	195	291	finally
    //   96	102	296	finally
    //   111	120	296	finally
    //   131	139	296	finally
    //   149	152	305	finally
    //   153	156	305	finally
    //   200	234	305	finally
    //   244	249	305	finally
    //   249	252	305	finally
    //   254	288	305	finally
    //   249	252	310	java/io/IOException
    //   254	288	310	java/io/IOException
  }

  public void updateLastAccessed(Entry paramEntry)
  {
    try
    {
      Entry localEntry = (Entry)this.mEntries.get(paramEntry);
      if (localEntry != null)
      {
        this.mLruEntries.remove(localEntry);
        localEntry.setAccessedTime(System.currentTimeMillis());
        this.mLruEntries.put(localEntry, localEntry);
        onModified();
      }
      return;
    }
    finally
    {
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.history.BaseDb
 * JD-Core Version:    0.6.2
 */