package com.google.android.apps.translate.history;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.res.Configuration;
import android.os.Handler;
import android.text.TextUtils;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.Filter.FilterResults;
import android.widget.Filterable;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.google.android.apps.translate.HomeActivity;
import com.google.android.apps.translate.Languages;
import com.google.android.apps.translate.Lists;
import com.google.android.apps.translate.Logger;
import com.google.android.apps.translate.Profile;
import com.google.android.apps.translate.R.id;
import com.google.android.apps.translate.R.menu;
import com.google.android.apps.translate.R.string;
import com.google.android.apps.translate.UserActivityMgr;
import com.google.android.apps.translate.UserActivityMgr.RequestSource;
import com.google.android.apps.translate.Util;
import com.google.android.apps.translate.editor.PreImeAutoCompleteTextView.OnKeyPreImeListener;
import com.google.android.apps.translate.translation.TranslateFragment;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public abstract class BaseHistoryListAdapter extends BaseAdapter
  implements Filterable, PreImeAutoCompleteTextView.OnKeyPreImeListener
{
  protected static final ArrayList<HistoryEntry> EMPTY_LIST = Lists.newArrayList();
  private static final String TAG = "BaseHistoryListAdapter";
  protected Activity mActivity;
  protected BaseDb mDb;
  private long mDbLastModifiedTime = 0L;
  private TextView mEmptyMessage;
  private long mFavoriteDbLastModifiedTime = 0L;
  private final Filter mFilter = new Filter()
  {
    protected Filter.FilterResults performFiltering(CharSequence paramAnonymousCharSequence)
    {
      Filter.FilterResults localFilterResults = new Filter.FilterResults();
      ArrayList localArrayList = BaseHistoryListAdapter.this.fetchEntriesFromDb(paramAnonymousCharSequence.toString());
      localFilterResults.values = localArrayList;
      localFilterResults.count = localArrayList.size();
      return localFilterResults;
    }

    protected void publishResults(CharSequence paramAnonymousCharSequence, Filter.FilterResults paramAnonymousFilterResults)
    {
      BaseHistoryListAdapter.this.refreshHistoryList((ArrayList)paramAnonymousFilterResults.values, true, false);
    }
  };
  private boolean mFlushOnPause = true;
  private Handler mHandler = new Handler();
  private final Runnable mHideProgressRunnable = new Runnable()
  {
    public void run()
    {
      if (BaseHistoryListAdapter.this.mProgressBar != null)
        BaseHistoryListAdapter.this.mProgressBar.setVisibility(8);
      if (BaseHistoryListAdapter.this.mEmptyMessage != null)
        BaseHistoryListAdapter.this.mEmptyMessage.setText(null);
    }
  };
  protected boolean mIsLoadingList;
  protected Languages mLanguages;
  protected List<HistoryEntry> mList = EMPTY_LIST;
  protected ListView mListView;
  private final Runnable mMayShowProgressRunnable = new Runnable()
  {
    public void run()
    {
      if (BaseHistoryListAdapter.this.mIsLoadingList)
      {
        BaseHistoryListAdapter.this.clear();
        if (BaseHistoryListAdapter.this.mProgressBar != null)
          BaseHistoryListAdapter.this.mProgressBar.setVisibility(0);
        if (BaseHistoryListAdapter.this.mEmptyMessage != null)
          BaseHistoryListAdapter.this.mEmptyMessage.setText(BaseHistoryListAdapter.this.mActivity.getString(R.string.msg_loading));
      }
    }
  };
  protected HistoryDisplayMode mMode;
  protected int mOrder;
  private ProgressBar mProgressBar;

  public BaseHistoryListAdapter(Activity paramActivity, HistoryDisplayMode paramHistoryDisplayMode)
  {
    Logger.d("BaseHistoryListAdapter", "loadDatabaseFile");
    this.mMode = paramHistoryDisplayMode;
    this.mActivity = paramActivity;
    if (this.mMode == HistoryDisplayMode.HISTORY)
      this.mDb = HistoryDb.open(this.mActivity);
    for (this.mOrder = Profile.getHistoryOrder(this.mActivity); ; this.mOrder = Profile.getFavoriteOrder(this.mActivity))
    {
      Logger.d("BaseHistoryListAdapter", "loadDatabaseFile mDb.getRawCount=" + this.mDb.getRawCount());
      this.mProgressBar = ((ProgressBar)this.mActivity.findViewById(R.id.progress));
      this.mEmptyMessage = ((TextView)this.mActivity.findViewById(R.id.msg_empty));
      this.mLanguages = Util.getLanguageListFromProfile(this.mActivity);
      openDb();
      return;
      this.mDb = FavoriteDb.open(this.mActivity);
    }
  }

  protected static ArrayList<HistoryEntry> getHistoryList(List<Entry> paramList, BaseDb paramBaseDb)
  {
    ArrayList localArrayList = new ArrayList();
    if (paramList != null)
    {
      Iterator localIterator = paramList.iterator();
      if (localIterator.hasNext())
      {
        Entry localEntry = (Entry)localIterator.next();
        if (paramBaseDb != null);
        for (boolean bool = paramBaseDb.exists(localEntry); ; bool = true)
        {
          localArrayList.add(new HistoryEntry(localEntry, bool));
          break;
        }
      }
    }
    return localArrayList;
  }

  private boolean hasStarred()
  {
    Iterator localIterator = this.mList.iterator();
    while (localIterator.hasNext())
      if (((HistoryEntry)localIterator.next()).isFavorite)
        return true;
    return false;
  }

  private boolean hasUnstarred()
  {
    Iterator localIterator = this.mList.iterator();
    while (localIterator.hasNext())
      if (!((HistoryEntry)localIterator.next()).isFavorite)
        return true;
    return false;
  }

  private void hideSoftwareKeyboard()
  {
    Logger.d("BaseHistoryListAdapter", "hideSoftInputFromWindow");
    if ((this.mActivity != null) && (this.mListView != null))
      ((InputMethodManager)this.mActivity.getSystemService("input_method")).hideSoftInputFromWindow(this.mListView.getWindowToken(), 0);
  }

  private void startFilter()
  {
    InputMethodManager localInputMethodManager = (InputMethodManager)this.mActivity.getSystemService("input_method");
    this.mListView.requestFocus();
    localInputMethodManager.showSoftInput(this.mListView, 0);
    Util.showLongToastMessage(this.mActivity, this.mActivity.getString(R.string.msg_filter));
  }

  public void changeOrder(int paramInt)
  {
    Logger.d("BaseHistoryListAdapter", "changeOrder order=" + paramInt);
    this.mOrder = paramInt;
    loadListInBackground(true, false);
    if (isHistoryMode())
    {
      Profile.setHistoryOrder(this.mActivity, paramInt);
      return;
    }
    Profile.setFavoriteOrder(this.mActivity, paramInt);
  }

  protected void clear()
  {
    this.mList = EMPTY_LIST;
    notifyDataSetInvalidated();
  }

  void confirmRemoval()
  {
    AlertDialog.Builder localBuilder = new AlertDialog.Builder(this.mActivity).setTitle(R.string.app_name);
    if (isFiltered());
    for (int i = R.string.msg_confirm_remove_displayed_entry; ; i = R.string.msg_confirm_remove_all_entry)
    {
      localBuilder.setMessage(i).setPositiveButton(17039370, new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          paramAnonymousDialogInterface.dismiss();
          BaseHistoryListAdapter.this.removeAllEntries();
        }
      }).setNegativeButton(17039360, new DialogInterface.OnClickListener()
      {
        public void onClick(DialogInterface paramAnonymousDialogInterface, int paramAnonymousInt)
        {
          paramAnonymousDialogInterface.dismiss();
        }
      }).show();
      return;
    }
  }

  ArrayList<HistoryEntry> fetchEntriesFromDb(String paramString)
  {
    Logger.d("BaseHistoryListAdapter", "fetchEntriesFromDb mOrder=" + this.mOrder);
    if (this.mDb == null)
      return Lists.newArrayList();
    List localList;
    ArrayList localArrayList;
    switch (this.mOrder)
    {
    default:
      localList = this.mDb.getAllByATime(2147483647, paramString);
      if (isHistoryMode())
      {
        BaseDb localBaseDb = FavoriteDb.open(this.mActivity);
        localArrayList = getHistoryList(localList, localBaseDb);
        localBaseDb.close(false);
      }
      break;
    case 0:
    }
    while (true)
    {
      return localArrayList;
      localList = this.mDb.getAll(2147483647, paramString);
      break;
      localArrayList = getHistoryList(localList, null);
    }
  }

  public int getCount()
  {
    return this.mList.size();
  }

  public Entry getEntry(int paramInt)
  {
    return ((HistoryEntry)this.mList.get(paramInt)).entry;
  }

  public Filter getFilter()
  {
    return this.mFilter;
  }

  public List<HistoryEntry> getHistoryEntries()
  {
    return new ArrayList(this.mList);
  }

  public HistoryEntry getHistoryEntryItem(int paramInt)
  {
    return (HistoryEntry)this.mList.get(paramInt);
  }

  public Object getItem(int paramInt)
  {
    if ((paramInt < 0) || (paramInt > this.mList.size()))
      return null;
    return this.mList.get(paramInt);
  }

  public long getItemId(int paramInt)
  {
    return paramInt;
  }

  boolean isAlphabeticalOrder()
  {
    return this.mOrder == 0;
  }

  boolean isDbChanged()
  {
    return (this.mDb != null) && (this.mDb.getLastModifiedTime() > this.mDbLastModifiedTime);
  }

  protected boolean isFavoriteDbChanged()
  {
    return FavoriteDb.getLastModifiedTime(this.mActivity) > this.mFavoriteDbLastModifiedTime;
  }

  boolean isFiltered()
  {
    return !TextUtils.isEmpty(this.mListView.getTextFilter());
  }

  public boolean isHistoryMode()
  {
    return this.mMode == HistoryDisplayMode.HISTORY;
  }

  void loadListInBackground(final boolean paramBoolean)
  {
    Logger.d("BaseHistoryListAdapter", "loadListInBackground");
    final boolean bool = isDbChanged();
    if ((!paramBoolean) && (!bool) && ((!isHistoryMode()) || (!isFavoriteDbChanged())) && (this.mList.isEmpty()))
    {
      Logger.d("BaseHistoryListAdapter", "loadListInBackground BYE!");
      return;
    }
    setLoadingMessage();
    this.mIsLoadingList = true;
    CharSequence localCharSequence = this.mListView.getTextFilter();
    if (localCharSequence == null);
    for (final String str = null; ; str = localCharSequence.toString())
    {
      new Thread()
      {
        public void run()
        {
          ArrayList localArrayList = BaseHistoryListAdapter.this.fetchEntriesFromDb(str);
          BaseHistoryListAdapter.this.mIsLoadingList = false;
          boolean bool1;
          if (!paramBoolean)
          {
            boolean bool2 = bool;
            bool1 = false;
            if (!bool2);
          }
          else
          {
            bool1 = true;
          }
          BaseHistoryListAdapter.this.refreshHistoryList(localArrayList, bool1);
        }
      }
      .start();
      return;
    }
  }

  void loadListInBackground(boolean paramBoolean1, boolean paramBoolean2)
  {
    loadListInBackground(paramBoolean1);
  }

  public boolean onConfigurationChanged(Configuration paramConfiguration)
  {
    return false;
  }

  public void onCreateContextMenu(ContextMenu paramContextMenu, View paramView, ContextMenu.ContextMenuInfo paramContextMenuInfo)
  {
    this.mActivity.getMenuInflater().inflate(R.menu.history_activity_context_menu, paramContextMenu);
    int i = ((AdapterView.AdapterContextMenuInfo)paramContextMenuInfo).position;
    paramContextMenu.removeItem(R.id.context_menu_share);
    paramContextMenu.setHeaderTitle(((HistoryEntry)this.mList.get(i)).entry.getInputText());
  }

  public boolean onCreateOptionsMenu(Menu paramMenu, MenuInflater paramMenuInflater)
  {
    paramMenu.clear();
    paramMenuInflater.inflate(R.menu.history_activity_menu, paramMenu);
    return true;
  }

  public void onDestroy()
  {
    if (this.mDb != null)
    {
      this.mDb.close(true);
      this.mDb = null;
    }
  }

  public boolean onKeyPreIme(int paramInt, KeyEvent paramKeyEvent)
  {
    return false;
  }

  boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    Logger.d("BaseHistoryListAdapter", "onOptionsItemSelected");
    int i = paramMenuItem.getItemId();
    if (i == R.id.menu_order_alphabetical)
    {
      changeOrder(0);
      return true;
    }
    if (i == R.id.menu_order_by_time)
    {
      changeOrder(1);
      return true;
    }
    if (i == R.id.menu_filter)
    {
      startFilter();
      return true;
    }
    if (i == R.id.menu_star_all)
    {
      starAll();
      return true;
    }
    if (i == R.id.menu_unstar_all)
    {
      unstarAll();
      return true;
    }
    if (i == R.id.menu_remove_all)
    {
      confirmRemoval();
      return true;
    }
    return false;
  }

  public void onPause()
  {
    if ((this.mDb != null) && (this.mFlushOnPause))
    {
      this.mDb.flush(true);
      if (isHistoryMode())
        FavoriteDb.flush(this.mActivity, true);
    }
    hideSoftwareKeyboard();
  }

  void onPrepareOptionsMenu(Menu paramMenu)
  {
    boolean bool1 = true;
    Logger.d("BaseHistoryListAdapter", "onPrepareOptionsMenu");
    boolean bool2;
    boolean bool3;
    label80: MenuItem localMenuItem2;
    if (isHistoryMode())
    {
      paramMenu.findItem(R.id.menu_star_all).setVisible(hasUnstarred());
      paramMenu.findItem(R.id.menu_unstar_all).setVisible(hasStarred());
      bool2 = isEmpty();
      MenuItem localMenuItem1 = paramMenu.findItem(R.id.menu_filter);
      if (bool2)
        break label192;
      bool3 = bool1;
      localMenuItem1.setVisible(bool3);
      localMenuItem2 = paramMenu.findItem(R.id.menu_remove_all);
      if (bool2)
        break label198;
    }
    label192: label198: for (boolean bool4 = bool1; ; bool4 = false)
    {
      localMenuItem2.setVisible(bool4);
      if (!bool2)
        break label204;
      paramMenu.findItem(R.id.menu_order_alphabetical).setVisible(false);
      paramMenu.findItem(R.id.menu_order_by_time).setVisible(false);
      return;
      paramMenu.findItem(R.id.menu_star_all).setVisible(false);
      paramMenu.findItem(R.id.menu_unstar_all).setVisible(false);
      break;
      bool3 = false;
      break label80;
    }
    label204: boolean bool5 = isAlphabeticalOrder();
    MenuItem localMenuItem3 = paramMenu.findItem(R.id.menu_order_alphabetical);
    if (!bool5);
    while (true)
    {
      localMenuItem3.setVisible(bool1);
      paramMenu.findItem(R.id.menu_order_by_time).setVisible(bool5);
      return;
      bool1 = false;
    }
  }

  public void onResume()
  {
    Logger.d("BaseHistoryListAdapter", "onResume");
    loadListInBackground(false);
  }

  public void openDb()
  {
    if (this.mDb != null)
      return;
    if (this.mMode == HistoryDisplayMode.HISTORY)
    {
      this.mDb = HistoryDb.open(this.mActivity);
      this.mOrder = Profile.getHistoryOrder(this.mActivity);
      return;
    }
    this.mDb = FavoriteDb.open(this.mActivity);
    this.mOrder = Profile.getFavoriteOrder(this.mActivity);
  }

  void openTranslateActivity(Entry paramEntry, boolean paramBoolean)
  {
    UserActivityMgr localUserActivityMgr = UserActivityMgr.get();
    if (paramBoolean);
    for (UserActivityMgr.RequestSource localRequestSource = UserActivityMgr.RequestSource.HISTORY_VIEW; ; localRequestSource = UserActivityMgr.RequestSource.FAVORITES_VIEW)
    {
      localUserActivityMgr.setTranslationSource(localRequestSource);
      if (!(this.mActivity instanceof HomeActivity))
        break;
      TranslateFragment.startTranslateFragment(this.mActivity, paramEntry.getInputText(), this.mLanguages.getFromLanguageByShortName(paramEntry.getFromLanguageShortName()), this.mLanguages.getToLanguageByShortName(paramEntry.getToLanguageShortName()), true);
      return;
    }
    Util.openTranslateActivity(this.mActivity, paramEntry.getInputText(), paramEntry.getOutputText(), paramEntry.getFromLanguageShortName(), paramEntry.getToLanguageShortName());
  }

  protected void refreshHistoryList(final List<HistoryEntry> paramList, final boolean paramBoolean)
  {
    Logger.d("BaseHistoryListAdapter", "refreshHistoryList");
    this.mActivity.runOnUiThread(new Runnable()
    {
      public void run()
      {
        if (paramList == null)
        {
          BaseHistoryListAdapter.this.clear();
          BaseHistoryListAdapter.this.notifyDataSetChanged();
          if (!BaseHistoryListAdapter.this.mList.isEmpty())
            break label100;
          BaseHistoryListAdapter.this.setEmptyMessage();
        }
        while (true)
        {
          if (Util.getSdkVersion() >= 11)
            BaseHistoryListAdapter.this.mActivity.invalidateOptionsMenu();
          BaseHistoryListAdapter.this.updateDbModifiedTime();
          if (BaseHistoryListAdapter.this.isHistoryMode())
            BaseHistoryListAdapter.this.updateFavoriteDbModifiedTime();
          return;
          BaseHistoryListAdapter.this.mList = paramList;
          break;
          label100: if (paramBoolean)
            BaseHistoryListAdapter.this.mListView.setSelection(0);
        }
      }
    });
  }

  protected void refreshHistoryList(List<HistoryEntry> paramList, boolean paramBoolean1, boolean paramBoolean2)
  {
    refreshHistoryList(paramList, paramBoolean1);
  }

  void removeAllEntries()
  {
    final ProgressDialog localProgressDialog = new ProgressDialog(this.mActivity);
    localProgressDialog.setCancelable(false);
    localProgressDialog.setMessage(this.mActivity.getString(R.string.msg_deleting));
    localProgressDialog.show();
    new Thread()
    {
      public void run()
      {
        if (BaseHistoryListAdapter.this.isFiltered())
        {
          Iterator localIterator = BaseHistoryListAdapter.this.mList.iterator();
          while (localIterator.hasNext())
          {
            HistoryEntry localHistoryEntry = (HistoryEntry)localIterator.next();
            BaseHistoryListAdapter.this.mDb.remove(localHistoryEntry.entry);
          }
        }
        BaseHistoryListAdapter.this.mDb.removeAll();
        BaseHistoryListAdapter.this.refreshHistoryList(BaseHistoryListAdapter.EMPTY_LIST, false, false);
        localProgressDialog.cancel();
      }
    }
    .start();
  }

  void removeHistoryEntry(final int paramInt)
  {
    final ProgressDialog localProgressDialog = new ProgressDialog(this.mActivity);
    localProgressDialog.setCancelable(false);
    localProgressDialog.setMessage(this.mActivity.getString(R.string.msg_deleting));
    localProgressDialog.show();
    new Thread()
    {
      public void run()
      {
        BaseHistoryListAdapter.this.mDb.remove(BaseHistoryListAdapter.this.getEntry(paramInt));
        List localList = BaseHistoryListAdapter.this.getHistoryEntries();
        localList.remove(paramInt);
        BaseHistoryListAdapter.this.refreshHistoryList(localList, false, false);
        localProgressDialog.cancel();
      }
    }
    .start();
  }

  public void setEmptyMessage()
  {
    this.mActivity.runOnUiThread(new Runnable()
    {
      public void run()
      {
        if (BaseHistoryListAdapter.this.mProgressBar != null)
          BaseHistoryListAdapter.this.mProgressBar.setVisibility(8);
        TextView localTextView;
        if (BaseHistoryListAdapter.this.mEmptyMessage != null)
        {
          localTextView = BaseHistoryListAdapter.this.mEmptyMessage;
          if (!BaseHistoryListAdapter.this.isHistoryMode())
            break label70;
        }
        label70: for (String str = BaseHistoryListAdapter.this.mActivity.getString(R.string.msg_no_history); ; str = BaseHistoryListAdapter.this.mActivity.getString(R.string.msg_no_favorite_translation))
        {
          localTextView.setText(str);
          return;
        }
      }
    });
  }

  public void setFlushOnPause(boolean paramBoolean)
  {
    this.mFlushOnPause = paramBoolean;
  }

  public void setListView(ListView paramListView)
  {
    Logger.d("BaseHistoryListAdapter", "setListView");
    this.mListView = paramListView;
    this.mListView.setItemsCanFocus(true);
    this.mListView.setOnKeyListener(new View.OnKeyListener()
    {
      public boolean onKey(View paramAnonymousView, int paramAnonymousInt, KeyEvent paramAnonymousKeyEvent)
      {
        return (paramAnonymousInt == 62) && (paramAnonymousKeyEvent.getAction() == 1) && (!BaseHistoryListAdapter.this.mListView.hasTextFilter());
      }
    });
  }

  protected void setLoadingMessage()
  {
    this.mActivity.runOnUiThread(this.mHideProgressRunnable);
    this.mHandler.postDelayed(this.mMayShowProgressRunnable, 100L);
  }

  void setStar(View paramView, boolean paramBoolean)
  {
    CheckBox localCheckBox = (CheckBox)paramView.findViewById(R.id.btn_star);
    localCheckBox.setVisibility(0);
    localCheckBox.setChecked(paramBoolean);
  }

  void starAll()
  {
    Iterator localIterator = this.mList.iterator();
    while (localIterator.hasNext())
    {
      HistoryEntry localHistoryEntry = (HistoryEntry)localIterator.next();
      if (!localHistoryEntry.isFavorite)
        starTranslateEntry(true, localHistoryEntry);
    }
    notifyDataSetChanged();
  }

  public void starTranslateEntry(boolean paramBoolean, int paramInt)
  {
    starTranslateEntry(paramBoolean, (HistoryEntry)this.mList.get(paramInt));
  }

  protected void starTranslateEntry(boolean paramBoolean, HistoryEntry paramHistoryEntry)
  {
    Util.toggleStarredTranslation(this.mActivity, paramBoolean, paramHistoryEntry.entry);
    if (isHistoryMode())
      updateFavoriteDbModifiedTime();
    paramHistoryEntry.isFavorite = paramBoolean;
  }

  void unstarAll()
  {
    Iterator localIterator = this.mList.iterator();
    while (localIterator.hasNext())
    {
      HistoryEntry localHistoryEntry = (HistoryEntry)localIterator.next();
      if (localHistoryEntry.isFavorite)
        starTranslateEntry(false, localHistoryEntry);
    }
    notifyDataSetChanged();
  }

  protected void updateDbModifiedTime()
  {
    if (this.mDb != null)
    {
      this.mDbLastModifiedTime = this.mDb.getLastModifiedTime();
      return;
    }
    if (this.mMode == HistoryDisplayMode.HISTORY)
    {
      this.mDbLastModifiedTime = HistoryDb.getLastModifiedTime(this.mActivity);
      return;
    }
    this.mDbLastModifiedTime = FavoriteDb.getLastModifiedTime(this.mActivity);
  }

  protected void updateFavoriteDbModifiedTime()
  {
    this.mFavoriteDbLastModifiedTime = FavoriteDb.getLastModifiedTime(this.mActivity);
  }

  public static enum HistoryDisplayMode
  {
    static
    {
      FAVORITE = new HistoryDisplayMode("FAVORITE", 1);
      HistoryDisplayMode[] arrayOfHistoryDisplayMode = new HistoryDisplayMode[2];
      arrayOfHistoryDisplayMode[0] = HISTORY;
      arrayOfHistoryDisplayMode[1] = FAVORITE;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.history.BaseHistoryListAdapter
 * JD-Core Version:    0.6.2
 */