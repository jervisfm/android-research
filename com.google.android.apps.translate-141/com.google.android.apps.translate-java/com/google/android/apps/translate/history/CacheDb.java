package com.google.android.apps.translate.history;

import android.content.Context;
import com.google.android.apps.translate.Language;
import com.google.android.apps.translate.Logger;
import java.util.List;

public class CacheDb
{
  private static final String DB_NAME = "cachedb";
  private static final long MAX_CACHE_LIFE_MILLIS = 600000L;
  private static final int MAX_COUNT = 50;
  private static final String TAG = "CacheDb";
  private static BaseDb sInstance;

  public static void add(Context paramContext, Entry paramEntry)
  {
    BaseDb localBaseDb = open(paramContext);
    localBaseDb.add(paramEntry);
    localBaseDb.close(false);
  }

  public static boolean contains(Context paramContext, Entry paramEntry)
  {
    BaseDb localBaseDb = open(paramContext);
    boolean bool = localBaseDb.exists(paramEntry);
    localBaseDb.close(false);
    return bool;
  }

  public static void flush(Context paramContext, boolean paramBoolean)
  {
    getInstance(paramContext).flush(paramBoolean);
  }

  public static List<Entry> getAll(Context paramContext, int paramInt)
  {
    BaseDb localBaseDb = open(paramContext);
    List localList = localBaseDb.getAll(paramInt);
    localBaseDb.close(false);
    return localList;
  }

  public static List<Entry> getAll(Context paramContext, int paramInt, String paramString)
  {
    BaseDb localBaseDb = open(paramContext);
    List localList = localBaseDb.getAll(paramInt, paramString);
    localBaseDb.close(false);
    return localList;
  }

  public static List<Entry> getAllByATime(Context paramContext)
  {
    BaseDb localBaseDb = open(paramContext);
    List localList = localBaseDb.getAllByATime();
    localBaseDb.close(false);
    return localList;
  }

  public static List<Entry> getAllByATime(Context paramContext, int paramInt, String paramString)
  {
    BaseDb localBaseDb = open(paramContext);
    List localList = localBaseDb.getAllByATime(paramInt, paramString);
    localBaseDb.close(false);
    return localList;
  }

  public static Entry getCachedEntry(BaseDb paramBaseDb, Language paramLanguage1, Language paramLanguage2, String paramString)
  {
    while (true)
    {
      try
      {
        Entry localEntry1 = new Entry(paramLanguage1, paramLanguage2, paramString, "");
        localEntry2 = paramBaseDb.get(localEntry1);
        if (localEntry2 != null)
        {
          long l = localEntry1.getAccessedTime() - localEntry2.getAccessedTime();
          if (l < 600000L)
          {
            Logger.d("CacheDb", "cache hit. ageMillis=" + l);
            paramBaseDb.updateLastAccessed(localEntry2);
            return localEntry2;
          }
          Logger.d("CacheDb", "cache too old. ageMillis=" + l);
        }
        else
        {
          Logger.d("CacheDb", "cache NOT hit.");
        }
      }
      finally
      {
      }
      Entry localEntry2 = null;
    }
  }

  public static BaseDb getInstance(Context paramContext)
  {
    try
    {
      if (sInstance == null)
        sInstance = new BaseDb(paramContext, "cachedb", 50, true);
      BaseDb localBaseDb = sInstance;
      return localBaseDb;
    }
    finally
    {
    }
  }

  public static long getLastModifiedTime(Context paramContext)
  {
    BaseDb localBaseDb = open(paramContext);
    long l = localBaseDb.getLastModifiedTime();
    localBaseDb.close(false);
    return l;
  }

  public static int getRawCount(Context paramContext)
  {
    BaseDb localBaseDb = open(paramContext);
    int i = localBaseDb.getRawCount();
    localBaseDb.close(false);
    return i;
  }

  public static BaseDb open(Context paramContext)
  {
    return getInstance(paramContext).open();
  }

  public static void remove(Context paramContext, Entry paramEntry)
  {
    BaseDb localBaseDb = open(paramContext);
    localBaseDb.remove(paramEntry);
    localBaseDb.close(false);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.history.CacheDb
 * JD-Core Version:    0.6.2
 */