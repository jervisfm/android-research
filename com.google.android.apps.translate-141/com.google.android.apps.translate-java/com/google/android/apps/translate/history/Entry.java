package com.google.android.apps.translate.history;

import com.google.android.apps.translate.Language;
import com.google.android.apps.translate.Languages;
import com.google.android.apps.translate.Preconditions;
import com.google.android.apps.translate.Translate.TranslateResult;
import com.google.android.apps.translate.Util;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.Serializable;

public class Entry
  implements Serializable
{
  private static final String KEY_DELIMITER = "\\t";
  private static final long serialVersionUID = 1L;
  private long mAccessedTime;
  private long mCreatedTime;
  private final String mInputText;
  private final String mLanguageFrom;
  private final String mLanguageTo;
  private final String mOutputText;

  public Entry(Language paramLanguage1, Language paramLanguage2, String paramString1, String paramString2)
  {
    this.mLanguageFrom = ((Language)Preconditions.checkNotNull(paramLanguage1)).getShortName();
    this.mLanguageTo = ((Language)Preconditions.checkNotNull(paramLanguage2)).getShortName();
    this.mInputText = ((String)Preconditions.checkNotNull(paramString1));
    this.mOutputText = ((String)Preconditions.checkNotNull(paramString2));
    long l = System.currentTimeMillis();
    this.mAccessedTime = l;
    this.mCreatedTime = l;
  }

  public Entry(Entry paramEntry)
  {
    this.mLanguageFrom = paramEntry.mLanguageFrom;
    this.mLanguageTo = paramEntry.mLanguageTo;
    this.mInputText = paramEntry.mInputText;
    this.mOutputText = paramEntry.mOutputText;
    this.mAccessedTime = paramEntry.mAccessedTime;
    this.mCreatedTime = paramEntry.mCreatedTime;
  }

  private Entry(Entry paramEntry, String paramString)
  {
    this.mLanguageFrom = paramEntry.mLanguageFrom;
    this.mLanguageTo = paramEntry.mLanguageTo;
    this.mInputText = paramEntry.mInputText;
    this.mOutputText = paramString;
    this.mAccessedTime = paramEntry.mAccessedTime;
    this.mCreatedTime = paramEntry.mCreatedTime;
  }

  public Entry(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    this.mLanguageFrom = ((String)Preconditions.checkNotNull(paramString1));
    this.mLanguageTo = ((String)Preconditions.checkNotNull(paramString2));
    this.mInputText = ((String)Preconditions.checkNotNull(paramString3));
    this.mOutputText = ((String)Preconditions.checkNotNull(paramString4));
    long l = System.currentTimeMillis();
    this.mAccessedTime = l;
    this.mCreatedTime = l;
  }

  public static String getDbKey(String paramString1, String paramString2, String paramString3)
  {
    return paramString3 + "\\t" + Util.generateLanguagePairText(paramString1, paramString2);
  }

  public static Entry readData(DataInputStream paramDataInputStream)
    throws IOException
  {
    Entry localEntry = new Entry(paramDataInputStream.readUTF(), paramDataInputStream.readUTF(), paramDataInputStream.readUTF(), paramDataInputStream.readUTF());
    try
    {
      localEntry.setCreatedTime(paramDataInputStream.readLong());
      localEntry.setAccessedTime(paramDataInputStream.readLong());
      return localEntry;
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
    }
    throw new IOException("Failed to read time from stream");
  }

  public static Entry readKey(DataInputStream paramDataInputStream)
    throws IOException
  {
    return new Entry(paramDataInputStream.readUTF(), paramDataInputStream.readUTF(), paramDataInputStream.readUTF(), "");
  }

  public long getAccessedTime()
  {
    return this.mAccessedTime;
  }

  public long getCreatedTime()
  {
    return this.mCreatedTime;
  }

  public String getDbKey()
  {
    return getDbKey(this.mLanguageFrom, this.mLanguageTo, this.mInputText);
  }

  public Entry getEntryWithoutOnMemoryAttributes()
  {
    Translate.TranslateResult localTranslateResult = new Translate.TranslateResult(this.mOutputText, false);
    if (localTranslateResult.stripOnMemoryAttributes())
      this = new Entry(this, localTranslateResult.toString());
    return this;
  }

  public Language getFromLanguage(Languages paramLanguages)
  {
    return paramLanguages.getFromLanguageByShortName(this.mLanguageFrom);
  }

  public String getFromLanguageShortName()
  {
    return this.mLanguageFrom;
  }

  public String getInputText()
  {
    return this.mInputText;
  }

  public String getOutputText()
  {
    return this.mOutputText;
  }

  public Language getToLanguage(Languages paramLanguages)
  {
    return paramLanguages.getToLanguageByShortName(this.mLanguageTo);
  }

  public String getToLanguageShortName()
  {
    return this.mLanguageTo;
  }

  public String getTranslation()
  {
    int i = this.mOutputText.indexOf("\t");
    if (i != -1)
      return this.mOutputText.substring(0, i).trim();
    return this.mOutputText;
  }

  public boolean isSameTranslation(Entry paramEntry)
  {
    return (paramEntry.mLanguageFrom.equals(this.mLanguageFrom)) && (paramEntry.mLanguageTo.equals(this.mLanguageTo)) && (paramEntry.mInputText.equals(this.mInputText)) && (paramEntry.mOutputText.equals(this.mOutputText));
  }

  public void setAccessedTime(long paramLong)
  {
    if (paramLong >= 0L);
    for (boolean bool = true; ; bool = false)
    {
      Preconditions.checkArguments(bool);
      this.mAccessedTime = paramLong;
      return;
    }
  }

  public void setCreatedTime(long paramLong)
  {
    if (paramLong >= 0L);
    for (boolean bool = true; ; bool = false)
    {
      Preconditions.checkArguments(bool);
      this.mCreatedTime = paramLong;
      return;
    }
  }

  public void writeData(DataOutputStream paramDataOutputStream)
    throws IOException
  {
    paramDataOutputStream.writeUTF(this.mLanguageFrom);
    paramDataOutputStream.writeUTF(this.mLanguageTo);
    paramDataOutputStream.writeUTF(this.mInputText);
    paramDataOutputStream.writeUTF(this.mOutputText);
    paramDataOutputStream.writeLong(this.mCreatedTime);
    paramDataOutputStream.writeLong(this.mAccessedTime);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.history.Entry
 * JD-Core Version:    0.6.2
 */