package com.google.android.apps.translate.history;

import android.content.Context;
import java.util.List;

public class FavoriteDb
{
  private static final String DB_NAME = "favoritedb";
  private static final int MAX_COUNT = 200;
  private static BaseDb sInstance;

  public static void add(Context paramContext, Entry paramEntry)
  {
    BaseDb localBaseDb = open(paramContext);
    localBaseDb.add(paramEntry.getEntryWithoutOnMemoryAttributes());
    localBaseDb.close(false);
  }

  public static boolean contains(Context paramContext, Entry paramEntry)
  {
    BaseDb localBaseDb = open(paramContext);
    boolean bool = localBaseDb.exists(paramEntry);
    localBaseDb.close(false);
    return bool;
  }

  public static void flush(Context paramContext, boolean paramBoolean)
  {
    getInstance(paramContext).flush(paramBoolean);
  }

  public static List<Entry> getAll(Context paramContext, int paramInt)
  {
    BaseDb localBaseDb = open(paramContext);
    List localList = localBaseDb.getAll(paramInt);
    localBaseDb.close(false);
    return localList;
  }

  public static List<Entry> getAll(Context paramContext, int paramInt, String paramString)
  {
    BaseDb localBaseDb = open(paramContext);
    List localList = localBaseDb.getAll(paramInt, paramString);
    localBaseDb.close(false);
    return localList;
  }

  public static List<Entry> getAllByATime(Context paramContext)
  {
    BaseDb localBaseDb = open(paramContext);
    List localList = localBaseDb.getAllByATime();
    localBaseDb.close(false);
    return localList;
  }

  public static List<Entry> getAllByATime(Context paramContext, int paramInt, String paramString)
  {
    BaseDb localBaseDb = open(paramContext);
    List localList = localBaseDb.getAllByATime(paramInt, paramString);
    localBaseDb.close(false);
    return localList;
  }

  public static BaseDb getInstance(Context paramContext)
  {
    try
    {
      if (sInstance == null)
        sInstance = new BaseDb(paramContext, "favoritedb", 200, false);
      BaseDb localBaseDb = sInstance;
      return localBaseDb;
    }
    finally
    {
    }
  }

  public static long getLastModifiedTime(Context paramContext)
  {
    BaseDb localBaseDb = open(paramContext);
    long l = localBaseDb.getLastModifiedTime();
    localBaseDb.close(false);
    return l;
  }

  public static int getRawCount(Context paramContext)
  {
    BaseDb localBaseDb = open(paramContext);
    int i = localBaseDb.getRawCount();
    localBaseDb.close(false);
    return i;
  }

  public static BaseDb open(Context paramContext)
  {
    return getInstance(paramContext).open();
  }

  public static void remove(Context paramContext, Entry paramEntry)
  {
    BaseDb localBaseDb = open(paramContext);
    localBaseDb.remove(paramEntry);
    localBaseDb.close(false);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.history.FavoriteDb
 * JD-Core Version:    0.6.2
 */