package com.google.android.apps.translate.history;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;

public class FocusRequestingListView extends ListView
{
  private boolean mFirstLayoutDone = false;

  public FocusRequestingListView(Context paramContext)
  {
    super(paramContext);
  }

  public FocusRequestingListView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }

  public FocusRequestingListView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
  }

  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
    if (!this.mFirstLayoutDone)
    {
      setFocusable(true);
      requestFocus();
    }
    this.mFirstLayoutDone = true;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.history.FocusRequestingListView
 * JD-Core Version:    0.6.2
 */