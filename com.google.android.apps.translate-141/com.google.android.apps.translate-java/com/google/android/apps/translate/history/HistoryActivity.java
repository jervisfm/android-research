package com.google.android.apps.translate.history;

import android.app.ListActivity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import com.google.android.apps.translate.Logger;
import com.google.android.apps.translate.R.layout;
import com.google.android.apps.translate.TranslateBaseActivity;
import com.google.android.apps.translate.Util;

public class HistoryActivity extends ListActivity
{
  private static final String TAG = "HistoryActivity";
  private boolean isHistoryMode = false;
  private HistoryHelper mHelper = new HistoryHelper();
  private BaseHistoryListAdapter mListAdapter;

  public boolean isHistoryMode()
  {
    return this.isHistoryMode;
  }

  public void onConfigurationChanged(Configuration paramConfiguration)
  {
    super.onConfigurationChanged(paramConfiguration);
    if (this.mHelper != null)
      this.mHelper.onConfigurationChanged(paramConfiguration);
  }

  public boolean onContextItemSelected(MenuItem paramMenuItem)
  {
    if (this.mHelper.onContextItemSelected(paramMenuItem))
      return true;
    return super.onContextItemSelected(paramMenuItem);
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    this.mHelper.onCreate(this);
    this.isHistoryMode = getIntent().getBooleanExtra("history", true);
    boolean bool = getIntent().getBooleanExtra("flush_on_pause", true);
    TranslateBaseActivity.beforeSetContentView(this);
    setContentView(R.layout.history_view);
    TranslateBaseActivity.afterSetContentView(this);
    this.mHelper.loadDatabaseFile(this.isHistoryMode);
    this.mHelper.setFlushOnPause(bool);
    this.mListAdapter = this.mHelper.getListAdapter();
    setListAdapter(this.mListAdapter);
    this.mHelper.setListView(getListView());
    registerForContextMenu(getListView());
  }

  public void onCreateContextMenu(ContextMenu paramContextMenu, View paramView, ContextMenu.ContextMenuInfo paramContextMenuInfo)
  {
    Logger.d("HistoryActivity", "onCreateContextMenu");
    this.mHelper.onCreateContextMenu(paramContextMenu, paramView, paramContextMenuInfo);
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    if (this.mHelper.onCreateOptionsMenu(paramMenu, getMenuInflater()))
      return true;
    return super.onCreateOptionsMenu(paramMenu);
  }

  public void onDestroy()
  {
    this.mHelper.onDestroy();
    super.onDestroy();
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    Logger.d("HistoryActivity", "onKeyDown");
    if ((this.mHelper != null) && (this.mHelper.onKeyPreIme(paramInt, paramKeyEvent)))
      return true;
    return super.onKeyDown(paramInt, paramKeyEvent);
  }

  public void onListItemClick(ListView paramListView, View paramView, int paramInt, long paramLong)
  {
    this.mHelper.onListItemClick(paramListView, paramView, paramInt, paramLong);
    super.onListItemClick(paramListView, paramView, paramInt, paramLong);
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    if (this.mHelper.onOptionsItemSelected(paramMenuItem))
      return true;
    if (paramMenuItem.getItemId() == 16908332)
    {
      Util.openHomeActivity(this);
      return true;
    }
    return super.onOptionsItemSelected(paramMenuItem);
  }

  protected void onPause()
  {
    super.onPause();
    this.mHelper.onPause();
  }

  public boolean onPrepareOptionsMenu(Menu paramMenu)
  {
    this.mHelper.onPrepareOptionsMenu(paramMenu);
    return super.onPrepareOptionsMenu(paramMenu);
  }

  protected void onResume()
  {
    super.onResume();
    this.mHelper.onResume();
  }

  protected void onStart()
  {
    super.onStart();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.history.HistoryActivity
 * JD-Core Version:    0.6.2
 */