package com.google.android.apps.translate.history;

public class HistoryEntry
{
  public Entry entry;
  public boolean isFavorite;

  public HistoryEntry(Entry paramEntry, boolean paramBoolean)
  {
    this.entry = paramEntry;
    this.isFavorite = paramBoolean;
  }

  public HistoryEntry(HistoryEntry paramHistoryEntry)
  {
    this.entry = paramHistoryEntry.entry;
    this.isFavorite = paramHistoryEntry.isFavorite;
  }

  public static HistoryEntry getNullHistoryEntry()
  {
    Entry localEntry = new Entry("null", "null", "", "");
    HistoryEntry localHistoryEntry = new HistoryEntry(localEntry, false);
    localHistoryEntry.entry = localEntry;
    localHistoryEntry.isFavorite = false;
    return localHistoryEntry;
  }

  public boolean isNullEntry()
  {
    return this.entry.getFromLanguageShortName().equals("null");
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.history.HistoryEntry
 * JD-Core Version:    0.6.2
 */