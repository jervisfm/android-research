package com.google.android.apps.translate.history;

import android.app.Activity;
import android.app.ListFragment;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ListView;
import com.google.android.apps.translate.Logger;
import com.google.android.apps.translate.R.id;
import com.google.android.apps.translate.R.layout;
import com.google.android.apps.translate.editor.PreImeAutoCompleteTextView.OnKeyPreImeListener;

public class HistoryFragment extends ListFragment
  implements PreImeAutoCompleteTextView.OnKeyPreImeListener
{
  private static final String TAG = "HistoryFragment";
  private Activity mActivity;
  private View mControlPanelWrapper;
  private final boolean mFlushOnPause;
  private HistoryHelper mHelper;
  private final boolean mIsHistoryMode;

  public HistoryFragment()
  {
    this(true, true);
  }

  public HistoryFragment(boolean paramBoolean1, boolean paramBoolean2)
  {
    Logger.d("HistoryFragment", "HistoryFragment");
    this.mIsHistoryMode = paramBoolean1;
    this.mFlushOnPause = paramBoolean2;
    this.mHelper = new HistoryHelper();
  }

  public boolean isHistoryMode()
  {
    return this.mIsHistoryMode;
  }

  public void onActivityCreated(Bundle paramBundle)
  {
    Logger.d("HistoryFragment", "onActivityCreated");
    super.onActivityCreated(paramBundle);
    this.mActivity = getActivity();
    this.mHelper.onCreate(this.mActivity);
    this.mHelper.loadDatabaseFile(this.mIsHistoryMode);
    this.mHelper.setFlushOnPause(this.mFlushOnPause);
    this.mControlPanelWrapper = this.mActivity.getWindow().getDecorView().findViewById(R.id.input_method_view_wrapper);
    setListAdapter(this.mHelper.getListAdapter());
    ListView localListView = getListView();
    this.mHelper.setListView(localListView);
    registerForContextMenu(localListView);
  }

  public void onConfigurationChanged(Configuration paramConfiguration)
  {
    super.onConfigurationChanged(paramConfiguration);
    if (this.mHelper != null)
      this.mHelper.onConfigurationChanged(paramConfiguration);
  }

  public boolean onContextItemSelected(MenuItem paramMenuItem)
  {
    if (this.mHelper.onContextItemSelected(paramMenuItem))
      return true;
    return super.onContextItemSelected(paramMenuItem);
  }

  public void onCreate(Bundle paramBundle)
  {
    Logger.d("HistoryFragment", "onCreate");
    super.onCreate(paramBundle);
    setHasOptionsMenu(true);
  }

  public void onCreateContextMenu(ContextMenu paramContextMenu, View paramView, ContextMenu.ContextMenuInfo paramContextMenuInfo)
  {
    Logger.d("HistoryFragment", "onCreateContextMenu");
    this.mHelper.onCreateContextMenu(paramContextMenu, paramView, paramContextMenuInfo);
  }

  public void onCreateOptionsMenu(Menu paramMenu, MenuInflater paramMenuInflater)
  {
    this.mHelper.onCreateOptionsMenu(paramMenu, paramMenuInflater);
    super.onCreateOptionsMenu(paramMenu, paramMenuInflater);
  }

  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    Logger.d("HistoryFragment", "onCreateView");
    super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
    return paramLayoutInflater.inflate(R.layout.history_view, paramViewGroup, false);
  }

  public void onDestroy()
  {
    super.onDestroy();
    this.mHelper.onDestroy();
  }

  public boolean onKeyPreIme(int paramInt, KeyEvent paramKeyEvent)
  {
    if (this.mHelper != null)
      return this.mHelper.onKeyPreIme(paramInt, paramKeyEvent);
    return false;
  }

  public void onListItemClick(ListView paramListView, View paramView, int paramInt, long paramLong)
  {
    this.mHelper.onListItemClick(paramListView, paramView, paramInt, paramLong);
    super.onListItemClick(paramListView, paramView, paramInt, paramLong);
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    Logger.d("HistoryFragment", "onOptionsItemSelected");
    if ((this.mActivity != null) && (this.mHelper != null) && (this.mHelper.onOptionsItemSelected(paramMenuItem)))
    {
      this.mActivity.invalidateOptionsMenu();
      return true;
    }
    return super.onOptionsItemSelected(paramMenuItem);
  }

  public void onPause()
  {
    super.onPause();
    this.mHelper.onPause();
  }

  public void onPrepareOptionsMenu(Menu paramMenu)
  {
    Logger.d("HistoryFragment", "onPrepareOptionsMenu");
    this.mHelper.onPrepareOptionsMenu(paramMenu);
    super.onPrepareOptionsMenu(paramMenu);
  }

  public void onResume()
  {
    Logger.d("HistoryFragment", "onResume");
    super.onResume();
    this.mHelper.onResume();
    if (this.mControlPanelWrapper != null)
      this.mControlPanelWrapper.setVisibility(8);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.history.HistoryFragment
 * JD-Core Version:    0.6.2
 */