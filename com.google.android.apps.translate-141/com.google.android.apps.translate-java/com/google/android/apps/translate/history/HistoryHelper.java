package com.google.android.apps.translate.history;

import android.app.Activity;
import android.content.res.Configuration;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ListView;
import com.google.android.apps.translate.LanguagesFactory;
import com.google.android.apps.translate.Logger;
import com.google.android.apps.translate.R.id;
import com.google.android.apps.translate.R.menu;
import com.google.android.apps.translate.R.string;
import com.google.android.apps.translate.SdkVersionWrapper;
import com.google.android.apps.translate.SdkVersionWrapper.WrapperBase;
import com.google.android.apps.translate.Translate.TranslateResult;
import com.google.android.apps.translate.UserActivityMgr;
import com.google.android.apps.translate.UserActivityMgr.IntervalCountTag;
import com.google.android.apps.translate.Util;
import com.google.android.apps.translate.editor.PreImeAutoCompleteTextView.OnKeyPreImeListener;
import com.google.android.apps.translate.translation.BaseTranslateHelper;
import com.google.android.apps.translate.translation.ChipView;
import com.google.android.apps.translate.translation.ChipView.ChipPart;
import com.google.android.apps.translate.translation.TranslateHelper;
import java.util.Locale;

public class HistoryHelper extends BaseTranslateHelper
  implements PreImeAutoCompleteTextView.OnKeyPreImeListener
{
  private static final String TAG = "HistoryHelper";
  private boolean mIsHistoryMode = false;
  private BaseHistoryListAdapter mListAdapter;

  public BaseHistoryListAdapter getListAdapter()
  {
    return this.mListAdapter;
  }

  public void loadDatabaseFile(boolean paramBoolean)
  {
    this.mIsHistoryMode = paramBoolean;
    if (paramBoolean)
    {
      UserActivityMgr.get().incrementIntervalCount(UserActivityMgr.IntervalCountTag.HISTORY_VIEWS, 1);
      this.mListAdapter = new HistoryListAdapter(this.mActivity, BaseHistoryListAdapter.HistoryDisplayMode.HISTORY);
      return;
    }
    UserActivityMgr.get().incrementIntervalCount(UserActivityMgr.IntervalCountTag.FAVORITES_VIEWS, 1);
    this.mListAdapter = new HistoryListAdapter(this.mActivity, BaseHistoryListAdapter.HistoryDisplayMode.FAVORITE);
  }

  public boolean onConfigurationChanged(Configuration paramConfiguration)
  {
    Logger.d("HistoryHelper", "onConfigurationChanged");
    if (this.mListAdapter != null)
      return this.mListAdapter.onConfigurationChanged(paramConfiguration);
    return false;
  }

  public boolean onContextItemSelected(MenuItem paramMenuItem)
  {
    AdapterView.AdapterContextMenuInfo localAdapterContextMenuInfo = (AdapterView.AdapterContextMenuInfo)paramMenuItem.getMenuInfo();
    int i = localAdapterContextMenuInfo.position;
    int j = paramMenuItem.getItemId();
    boolean bool;
    if (j == R.id.context_menu_translate_input_text)
    {
      Entry localEntry5 = this.mListAdapter.getEntry(i);
      this.mListAdapter.openTranslateActivity(localEntry5, this.mIsHistoryMode);
      UserActivityMgr.get().incrementIntervalCount(UserActivityMgr.IntervalCountTag.CHIPVIEW_SRC_TRANSLATE_CLICKS, 1);
      bool = true;
    }
    int k;
    do
    {
      return bool;
      if (j == R.id.context_menu_translate_translation)
      {
        Entry localEntry3 = this.mListAdapter.getEntry(i);
        Entry localEntry4 = new Entry(getFromLanguageGivenToLanguage(localEntry3.getToLanguage(this.mLanguageList), false), getToLanguageGivenFromLanguage(localEntry3.getFromLanguage(this.mLanguageList), false), localEntry3.getTranslation(), "");
        this.mListAdapter.openTranslateActivity(localEntry4, this.mIsHistoryMode);
        UserActivityMgr.get().incrementIntervalCount(UserActivityMgr.IntervalCountTag.CHIPVIEW_TRG_TRANSLATE_CLICKS, 1);
        return true;
      }
      if (j == R.id.context_menu_copy_input_text)
      {
        Util.copyToClipBoard(this.mActivity, this.mListAdapter.getEntry(i).getInputText(), ChipView.ChipPart.INPUT_TEXT);
        Util.showShortToastMessage(this.mActivity, R.string.toast_message_copy_input_text);
        this.mListAdapter.notifyDataSetChanged();
        return true;
      }
      if (j == R.id.context_menu_copy)
      {
        Translate.TranslateResult localTranslateResult1 = new Translate.TranslateResult(this.mListAdapter.getEntry(i));
        Util.copyToClipBoard(this.mActivity, localTranslateResult1.getTranslateText(), ChipView.ChipPart.TRANSLATION_TEXT);
        Util.showShortToastMessage(this.mActivity, R.string.toast_message_copy);
        this.mListAdapter.notifyDataSetChanged();
        return true;
      }
      if (j == R.id.context_menu_remove)
      {
        this.mListAdapter.removeHistoryEntry(i);
        return true;
      }
      if (j == R.id.context_menu_share)
      {
        Entry localEntry2 = this.mListAdapter.getEntry(i);
        if ((localAdapterContextMenuInfo.targetView instanceof ChipView))
          TranslateHelper.performChipActionShare(this.mActivity, this.mLanguageList, localEntry2, ((ChipView)localAdapterContextMenuInfo.targetView).getSelectedChipPart());
        while (true)
        {
          this.mListAdapter.notifyDataSetChanged();
          return true;
          TranslateHelper.performChipActionShare(this.mActivity, this.mLanguageList, localEntry2, ChipView.ChipPart.NONE);
        }
      }
      if (j == R.id.context_menu_search_input_text)
      {
        Entry localEntry1 = this.mListAdapter.getEntry(i);
        Util.searchTextOnWeb(this.mActivity, localEntry1.getInputText(), ChipView.ChipPart.INPUT_TEXT);
        this.mListAdapter.notifyDataSetChanged();
        return true;
      }
      k = R.id.context_menu_search_translation;
      bool = false;
    }
    while (j != k);
    this.mListAdapter.getEntry(i);
    Translate.TranslateResult localTranslateResult2 = new Translate.TranslateResult(this.mListAdapter.getEntry(i));
    Util.searchTextOnWeb(this.mActivity, localTranslateResult2.getTranslateText(), ChipView.ChipPart.TRANSLATION_TEXT);
    this.mListAdapter.notifyDataSetChanged();
    return true;
  }

  public void onCreate(Activity paramActivity)
  {
    Logger.d("HistoryHelper", "onCreate");
    super.onCreate(paramActivity, LanguagesFactory.get().getLanguages(paramActivity, Locale.getDefault()));
  }

  public void onCreateContextMenu(ContextMenu paramContextMenu, View paramView, ContextMenu.ContextMenuInfo paramContextMenuInfo)
  {
    this.mActivity.getMenuInflater().inflate(R.menu.history_activity_context_menu, paramContextMenu);
    AdapterView.AdapterContextMenuInfo localAdapterContextMenuInfo = (AdapterView.AdapterContextMenuInfo)paramContextMenuInfo;
    int i = localAdapterContextMenuInfo.position;
    Entry localEntry = this.mListAdapter.getHistoryEntryItem(i).entry;
    if ((localAdapterContextMenuInfo.targetView instanceof ChipView))
    {
      ChipView localChipView = (ChipView)localAdapterContextMenuInfo.targetView;
      switch (1.$SwitchMap$com$google$android$apps$translate$translation$ChipView$ChipPart[localChipView.getSelectedChipPart().ordinal()])
      {
      default:
        return;
      case 1:
        paramContextMenu.removeItem(R.id.context_menu_translate_translation);
        paramContextMenu.removeItem(R.id.context_menu_share);
        paramContextMenu.removeItem(R.id.context_menu_copy);
        paramContextMenu.removeItem(R.id.context_menu_search_translation);
        paramContextMenu.removeItem(R.id.context_menu_copy_input_text);
        paramContextMenu.removeItem(R.id.context_menu_search_input_text);
        paramContextMenu.setHeaderTitle(localEntry.getInputText());
        return;
      case 2:
        paramContextMenu.removeItem(R.id.context_menu_translate_translation);
        paramContextMenu.removeItem(R.id.context_menu_remove);
        paramContextMenu.removeItem(R.id.context_menu_copy);
        paramContextMenu.removeItem(R.id.context_menu_search_translation);
        paramContextMenu.setHeaderTitle(localEntry.getInputText());
        return;
      case 3:
      }
      paramContextMenu.removeItem(R.id.context_menu_translate_input_text);
      paramContextMenu.removeItem(R.id.context_menu_remove);
      paramContextMenu.removeItem(R.id.context_menu_copy_input_text);
      paramContextMenu.removeItem(R.id.context_menu_search_input_text);
      paramContextMenu.setHeaderTitle(localEntry.getTranslation());
      return;
    }
    paramContextMenu.removeItem(R.id.context_menu_share);
    paramContextMenu.setHeaderTitle(localEntry.getInputText());
  }

  public boolean onCreateOptionsMenu(Menu paramMenu, MenuInflater paramMenuInflater)
  {
    paramMenu.clear();
    paramMenuInflater.inflate(R.menu.history_activity_menu, paramMenu);
    return true;
  }

  public void onDestroy()
  {
    if (this.mListAdapter != null)
      this.mListAdapter.onDestroy();
  }

  public boolean onKeyPreIme(int paramInt, KeyEvent paramKeyEvent)
  {
    if (this.mListAdapter != null)
      return this.mListAdapter.onKeyPreIme(paramInt, paramKeyEvent);
    return false;
  }

  public void onListItemClick(ListView paramListView, View paramView, int paramInt, long paramLong)
  {
    Entry localEntry = this.mListAdapter.getHistoryEntryItem(paramInt).entry;
    this.mListAdapter.openTranslateActivity(localEntry, this.mIsHistoryMode);
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    if (paramMenuItem.getItemId() == R.id.menu_feedback)
    {
      SdkVersionWrapper.getWrapper().sendFeedback(this.mActivity, true);
      return true;
    }
    return this.mListAdapter.onOptionsItemSelected(paramMenuItem);
  }

  public void onPause()
  {
    super.onPause();
    if (this.mListAdapter != null)
      this.mListAdapter.onPause();
  }

  public void onPrepareOptionsMenu(Menu paramMenu)
  {
    if (this.mListAdapter != null)
      this.mListAdapter.onPrepareOptionsMenu(paramMenu);
  }

  public void onResume()
  {
    Logger.d("HistoryHelper", "onResume");
    super.onResume();
    SdkVersionWrapper.getWrapper().setHomeButton(this.mActivity, true);
    if (this.mListAdapter != null)
      this.mListAdapter.onResume();
  }

  public void setFlushOnPause(boolean paramBoolean)
  {
    this.mListAdapter.setFlushOnPause(paramBoolean);
  }

  public void setListView(ListView paramListView)
  {
    Logger.d("HistoryHelper", "setListView");
    this.mListAdapter.setListView(paramListView);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.history.HistoryHelper
 * JD-Core Version:    0.6.2
 */