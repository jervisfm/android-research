package com.google.android.apps.translate.history;

import android.app.Activity;
import android.content.res.Configuration;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import com.google.android.apps.translate.Language;
import com.google.android.apps.translate.Languages;
import com.google.android.apps.translate.Logger;
import com.google.android.apps.translate.R.layout;
import com.google.android.apps.translate.TranslateApplication;
import com.google.android.apps.translate.TranslateManager;
import com.google.android.apps.translate.Util;
import com.google.android.apps.translate.translation.ChipView;
import com.google.android.apps.translate.translation.ChipView.ChipViewTag;
import com.google.android.apps.translate.translation.InputPanel;
import com.google.android.apps.translate.translation.TranslateEntry;
import java.util.ArrayList;
import java.util.List;

public class HistoryListAdapter extends BaseHistoryListAdapter
{
  private static final int MINIMUM_INSTANT_SCROLLING_POSITIONS = 2;
  private static final int SCROLLING_OVERRUN_FOR_REFRESH_PX = 0;
  public static final int SELECTED_HISTORY_ITEM_POSITION_NULL = -1;
  private static final int SELECT_LAST_ITEM_ANIMATION_MILLIS = 1000;
  private static final String TAG = "HistoryListAdapter";
  private boolean mAnimateScroll = false;
  private InputPanel mInputPanel;
  private ChipView mPreviousSelectedItem;
  private int mSelectedHistoryItemPosition = -1;
  private int mSelectedItemPosition = -1;
  private TranslateManager mTranslateManager = ((TranslateApplication)this.mActivity.getApplication()).getTranslateManager();

  public HistoryListAdapter(Activity paramActivity, BaseHistoryListAdapter.HistoryDisplayMode paramHistoryDisplayMode)
  {
    super(paramActivity, paramHistoryDisplayMode);
  }

  private static TranslateEntry buildTranslateEntry(HistoryEntry paramHistoryEntry, Languages paramLanguages)
  {
    return TranslateEntry.build(paramHistoryEntry.entry.getFromLanguageShortName(), paramHistoryEntry.entry.getToLanguageShortName(), paramHistoryEntry.entry.getInputText(), paramHistoryEntry.entry.getTranslation(), false, paramLanguages);
  }

  private void scroll(final int paramInt, final boolean paramBoolean)
  {
    this.mListView.post(new Runnable()
    {
      public void run()
      {
        if (paramBoolean)
          HistoryListAdapter.this.setSelectedHistoryItemPosition(paramInt);
        if (HistoryListAdapter.this.mAnimateScroll)
        {
          HistoryListAdapter.this.notifyDataSetChanged();
          if ((2 + paramInt < HistoryListAdapter.this.mListView.getFirstVisiblePosition()) || (paramInt > 2 + HistoryListAdapter.this.mListView.getLastVisiblePosition()))
            HistoryListAdapter.this.mListView.setSelection(HistoryListAdapter.this.mSelectedHistoryItemPosition);
          HistoryListAdapter.this.mListView.smoothScrollToPositionFromTop(paramInt, 0, 1000);
          return;
        }
        HistoryListAdapter.this.notifyDataSetChanged();
        HistoryListAdapter.this.mListView.setSelection(paramInt);
      }
    });
  }

  public ChipView getSelectedHistoryItem()
  {
    return this.mPreviousSelectedItem;
  }

  public int getSelectedHistoryItemPosition()
  {
    return this.mSelectedHistoryItemPosition;
  }

  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    HistoryEntry localHistoryEntry = (HistoryEntry)this.mList.get(paramInt);
    Entry localEntry = localHistoryEntry.entry;
    ChipView localChipView;
    if ((paramView == null) || (!(paramView instanceof ChipView)) || ((paramView.getTag() != null) && (((ChipView.ChipViewTag)paramView.getTag()).position != paramInt)))
    {
      Logger.d("HistoryListAdapter", "getView NEW position=" + paramInt);
      localChipView = (ChipView)this.mActivity.getLayoutInflater().inflate(R.layout.chip_view, null);
      ChipView.ChipViewTag localChipViewTag = new ChipView.ChipViewTag();
      localChipViewTag.position = paramInt;
      localChipView.setTag(localChipViewTag);
      Language localLanguage1 = this.mLanguages.getFromLanguageByShortName(localEntry.getFromLanguageShortName());
      Language localLanguage2 = this.mLanguages.getToLanguageByShortName(localEntry.getToLanguageShortName());
      localChipView.initParameters(this.mActivity, this.mListView, this, this.mTranslateManager, localLanguage1, localLanguage2, this.mInputPanel, this.mAnimateScroll);
    }
    while (true)
    {
      localChipView.render(localHistoryEntry, paramInt, this.mList.size());
      return localChipView;
      Logger.d("HistoryListAdapter", "getView REUSE position=" + paramInt);
      localChipView = (ChipView)paramView;
    }
  }

  public boolean hasNonFakeSelectedItem()
  {
    return this.mSelectedHistoryItemPosition != -1;
  }

  public void loadListInBackground(final boolean paramBoolean1, final boolean paramBoolean2)
  {
    Logger.d("HistoryListAdapter", "loadListInBackground force=" + paramBoolean1 + " selectTop=" + paramBoolean2);
    final boolean bool = isDbChanged();
    if ((!paramBoolean1) && (!bool) && ((!isHistoryMode()) || (!isFavoriteDbChanged())))
      return;
    setLoadingMessage();
    this.mIsLoadingList = true;
    CharSequence localCharSequence = this.mListView.getTextFilter();
    if (localCharSequence == null);
    for (final String str = null; ; str = localCharSequence.toString())
    {
      new Thread()
      {
        public void run()
        {
          ArrayList localArrayList = HistoryListAdapter.this.fetchEntriesFromDb(str);
          HistoryListAdapter.this.mIsLoadingList = false;
          boolean bool1;
          if (!paramBoolean1)
          {
            boolean bool2 = bool;
            bool1 = false;
            if (!bool2);
          }
          else
          {
            bool1 = true;
          }
          HistoryListAdapter.this.refreshHistoryList(localArrayList, bool1, paramBoolean2);
        }
      }
      .start();
      return;
    }
  }

  public boolean onConfigurationChanged(Configuration paramConfiguration)
  {
    Logger.d("HistoryListAdapter", "onConfigurationChanged");
    return false;
  }

  public boolean onKeyPreIme(int paramInt, KeyEvent paramKeyEvent)
  {
    int i = this.mSelectedHistoryItemPosition;
    boolean bool = false;
    if (i != -1)
    {
      if ((this.mSelectedHistoryItemPosition < this.mListView.getFirstVisiblePosition()) || (this.mSelectedHistoryItemPosition > this.mListView.getLastVisiblePosition()))
        break label56;
      this.mSelectedHistoryItemPosition = -1;
      notifyDataSetChanged();
    }
    while (true)
    {
      bool = true;
      return bool;
      label56: scroll(this.mSelectedHistoryItemPosition, false);
    }
  }

  public void onPause()
  {
    super.onPause();
    this.mSelectedItemPosition = getSelectedHistoryItemPosition();
  }

  public void onResume()
  {
    Logger.d("HistoryListAdapter", "onResume");
    super.onPause();
    if (this.mListView != null)
    {
      if (this.mSelectedItemPosition >= 0)
        this.mListView.post(new Runnable()
        {
          public void run()
          {
            Logger.d("HistoryListAdapter", "onTranslationDone SELECT LAST");
            HistoryListAdapter.this.setSelectedHistoryItemPosition(HistoryListAdapter.this.mSelectedItemPosition);
            HistoryListAdapter.this.notifyDataSetChanged();
            HistoryListAdapter.this.mListView.setSelection(HistoryListAdapter.this.mSelectedItemPosition);
          }
        });
    }
    else
      return;
    loadListInBackground(true, false);
  }

  protected void refreshHistoryList(final List<HistoryEntry> paramList, final boolean paramBoolean1, final boolean paramBoolean2)
  {
    Logger.d("HistoryListAdapter", "refreshHistoryList scrollToTop=" + paramBoolean1);
    this.mActivity.runOnUiThread(new Runnable()
    {
      public void run()
      {
        if (paramList == null)
        {
          HistoryListAdapter.this.clear();
          if (!HistoryListAdapter.this.isEmpty())
            break label95;
          HistoryListAdapter.this.notifyDataSetChanged();
          HistoryListAdapter.this.setEmptyMessage();
        }
        while (true)
        {
          if (Util.getSdkVersion() >= 11)
            HistoryListAdapter.this.mActivity.invalidateOptionsMenu();
          HistoryListAdapter.this.updateDbModifiedTime();
          if (HistoryListAdapter.this.isHistoryMode())
            HistoryListAdapter.this.updateFavoriteDbModifiedTime();
          return;
          HistoryListAdapter.this.mList = paramList;
          break;
          label95: if (paramBoolean1)
          {
            HistoryListAdapter.this.scroll(0, paramBoolean2);
          }
          else
          {
            if (paramBoolean2)
              HistoryListAdapter.this.setSelectedHistoryItemPosition(0);
            HistoryListAdapter.this.notifyDataSetChanged();
          }
        }
      }
    });
  }

  public void setInputPanel(InputPanel paramInputPanel)
  {
    this.mInputPanel = paramInputPanel;
  }

  public void setListView(ListView paramListView)
  {
    Logger.d("HistoryListAdapter", "setListView");
    super.setListView(paramListView);
  }

  public void setSelectedHistoryItem(ChipView paramChipView)
  {
    this.mPreviousSelectedItem = paramChipView;
  }

  public void setSelectedHistoryItemPosition(int paramInt)
  {
    this.mSelectedHistoryItemPosition = paramInt;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.history.HistoryListAdapter
 * JD-Core Version:    0.6.2
 */