package com.google.android.apps.translate.history;

import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import com.google.android.apps.translate.R.color;

public class InputPanelListView extends FocusRequestingListView
{
  private Context mContext;

  public InputPanelListView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    this.mContext = paramContext;
  }

  protected float getBottomFadingEdgeStrength()
  {
    return 0.0F;
  }

  public int getSolidColor()
  {
    return this.mContext.getResources().getColor(R.color.list_view_edge_effect_color);
  }

  protected float getTopFadingEdgeStrength()
  {
    return 0.8F;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.history.InputPanelListView
 * JD-Core Version:    0.6.2
 */