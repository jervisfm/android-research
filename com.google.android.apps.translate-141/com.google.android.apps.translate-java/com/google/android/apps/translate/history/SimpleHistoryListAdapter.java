package com.google.android.apps.translate.history;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;
import com.google.android.apps.translate.Constants.AppearanceType;
import com.google.android.apps.translate.Language;
import com.google.android.apps.translate.Languages;
import com.google.android.apps.translate.R.id;
import com.google.android.apps.translate.R.layout;
import com.google.android.apps.translate.Translate.TranslateResult;
import com.google.android.apps.translate.Util;

public class SimpleHistoryListAdapter extends BaseHistoryListAdapter
{
  public SimpleHistoryListAdapter(Activity paramActivity, BaseHistoryListAdapter.HistoryDisplayMode paramHistoryDisplayMode)
  {
    super(paramActivity, paramHistoryDisplayMode);
  }

  private void setEntryView(View paramView, HistoryEntry paramHistoryEntry)
  {
    Language localLanguage1 = this.mLanguages.getFromLanguageByShortName(paramHistoryEntry.entry.getFromLanguageShortName());
    Language localLanguage2 = this.mLanguages.getToLanguageByShortName(paramHistoryEntry.entry.getToLanguageShortName());
    Util.setTextAndFont((TextView)paramView.findViewById(R.id.title), paramHistoryEntry.entry.getInputText(), new Language[] { localLanguage1, localLanguage2 }, Constants.AppearanceType.ENTRY_TITLE, false);
    Translate.TranslateResult localTranslateResult = new Translate.TranslateResult(paramHistoryEntry.entry);
    if (localTranslateResult.hasTranslateText())
      Util.setTextAndFont((TextView)paramView.findViewById(R.id.summary), localTranslateResult.getTranslateText(), new Language[] { localLanguage2, localLanguage1 }, Constants.AppearanceType.ENTRY_SUMMARY, false);
    setStar(paramView, paramHistoryEntry.isFavorite);
  }

  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    if ((paramView != null) && (paramView.getId() == R.id.translate_entry));
    for (View localView = paramView; ; localView = this.mActivity.getLayoutInflater().inflate(R.layout.entry, paramViewGroup, false))
    {
      final HistoryEntry localHistoryEntry = getHistoryEntryItem(paramInt);
      setEntryView(localView, localHistoryEntry);
      ((CheckBox)localView.findViewById(R.id.btn_star)).setOnClickListener(new View.OnClickListener()
      {
        public void onClick(View paramAnonymousView)
        {
          SimpleHistoryListAdapter.this.starTranslateEntry(((CheckBox)paramAnonymousView).isChecked(), localHistoryEntry);
        }
      });
      return localView;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.history.SimpleHistoryListAdapter
 * JD-Core Version:    0.6.2
 */