package com.google.android.apps.translate.home;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ListFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.ListView;
import com.google.android.apps.translate.ActionBarSpinnerAdapter;
import com.google.android.apps.translate.Logger;
import com.google.android.apps.translate.R.id;
import com.google.android.apps.translate.R.layout;
import com.google.android.apps.translate.SdkVersionWrapper;
import com.google.android.apps.translate.SdkVersionWrapper.WrapperBase;
import com.google.android.apps.translate.translation.TranslateFragmentWrapper;

public class TabMenuFragment extends ListFragment
{
  public static final String TAB_MENU_FRAGMENT_TAG = "tagmenu";
  private static final String TAG = "TabMenuFragment";
  private Activity mActivity;
  private ActionBarSpinnerAdapter mSpinnerAdapter;
  private TranslateFragmentWrapper mTranslateFragment;

  public static void setTabMenuFragment(Activity paramActivity, ActionBarSpinnerAdapter paramActionBarSpinnerAdapter)
  {
    try
    {
      Logger.d("TabMenuFragment", "setTabMenuFragment");
      ((LinearLayout)paramActivity.getWindow().getDecorView().findViewById(R.id.tab_menu_fragment_container)).setVisibility(0);
      TabMenuFragment localTabMenuFragment = new TabMenuFragment();
      localTabMenuFragment.setAdapter(paramActionBarSpinnerAdapter);
      FragmentTransaction localFragmentTransaction = paramActivity.getFragmentManager().beginTransaction();
      localFragmentTransaction.replace(R.id.tab_menu_fragment_container, localTabMenuFragment, "tagmenu");
      localFragmentTransaction.commit();
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public static void setTabMenuFragmentVisibility(Activity paramActivity, boolean paramBoolean)
  {
    try
    {
      Logger.d("TabMenuFragment", "setTabMenuFragmentVisibility");
      LinearLayout localLinearLayout = (LinearLayout)paramActivity.getWindow().getDecorView().findViewById(R.id.tab_menu_fragment_container);
      if (paramBoolean);
      for (int i = 0; ; i = 8)
      {
        localLinearLayout.setVisibility(i);
        return;
      }
    }
    finally
    {
    }
  }

  public void onActivityCreated(Bundle paramBundle)
  {
    Logger.d("TabMenuFragment", "onActivityCreated");
    super.onActivityCreated(paramBundle);
    this.mActivity = getActivity();
  }

  public void onCreate(Bundle paramBundle)
  {
    Logger.d("TabMenuFragment", "onCreate");
    super.onCreate(paramBundle);
  }

  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    Logger.d("TabMenuFragment", "onCreateView");
    super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
    return paramLayoutInflater.inflate(R.layout.tab_menu_panel, null);
  }

  public void onDestroy()
  {
    Logger.d("TabMenuFragment", "onDestroy");
    super.onDestroy();
  }

  public void onListItemClick(ListView paramListView, View paramView, int paramInt, long paramLong)
  {
    Logger.d("TabMenuFragment", "onListItemClick position=" + paramInt);
    if (this.mSpinnerAdapter != null)
    {
      this.mSpinnerAdapter.onNavigationItemSelected(paramInt, paramLong);
      SdkVersionWrapper.getWrapper().setActionBarTitle(this.mActivity, this.mSpinnerAdapter);
    }
    super.onListItemClick(paramListView, paramView, paramInt, paramLong);
  }

  public void onPause()
  {
    Logger.d("TabMenuFragment", "onPause");
    super.onPause();
  }

  public void onResume()
  {
    Logger.d("TabMenuFragment", "onResume");
    super.onResume();
  }

  public void onStart()
  {
    Logger.d("TabMenuFragment", "onStart");
    super.onStart();
  }

  public void setAdapter(ActionBarSpinnerAdapter paramActionBarSpinnerAdapter)
  {
    this.mSpinnerAdapter = paramActionBarSpinnerAdapter;
    setListAdapter(this.mSpinnerAdapter);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.home.TabMenuFragment
 * JD-Core Version:    0.6.2
 */