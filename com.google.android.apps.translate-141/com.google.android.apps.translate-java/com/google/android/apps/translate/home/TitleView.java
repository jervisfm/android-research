package com.google.android.apps.translate.home;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.TranslateAnimation;
import android.widget.LinearLayout;
import com.google.android.apps.translate.Logger;
import com.google.android.apps.translate.Profile;
import com.google.android.apps.translate.Util;
import com.google.android.apps.translate.editor.EditPanelView;

public class TitleView extends LinearLayout
{
  private static final String TAG = "TitleView";
  private Activity mActivity;
  private View mLanguagePanel;

  public TitleView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    this.mActivity = ((Activity)paramContext);
  }

  private void hideActionBar()
  {
    if (this.mActivity.getActionBar().isShowing())
      this.mActivity.getActionBar().hide();
  }

  private void showActionBar()
  {
    if (!this.mActivity.getActionBar().isShowing())
      this.mActivity.getActionBar().show();
  }

  private void slideUpInputMethodView(final View paramView)
  {
    try
    {
      int i = paramView.getVisibility();
      if (i == 8);
      while (true)
      {
        return;
        TranslateAnimation localTranslateAnimation = new TranslateAnimation(0.0F, 0.0F, 0.0F, -paramView.getHeight());
        localTranslateAnimation.setDuration(150L);
        localTranslateAnimation.setAnimationListener(new Animation.AnimationListener()
        {
          public void onAnimationEnd(Animation paramAnonymousAnimation)
          {
            paramView.setVisibility(8);
          }

          public void onAnimationRepeat(Animation paramAnonymousAnimation)
          {
          }

          public void onAnimationStart(Animation paramAnonymousAnimation)
          {
          }
        });
        paramView.startAnimation(localTranslateAnimation);
      }
    }
    finally
    {
    }
  }

  public void checkTitleShow()
  {
    if (Util.isHoneycombCompatible())
      post(new Runnable()
      {
        public void run()
        {
          ViewGroup.LayoutParams localLayoutParams = TitleView.this.getLayoutParams();
          localLayoutParams.height = TitleView.this.mActivity.getActionBar().getHeight();
          TitleView.this.setLayoutParams(localLayoutParams);
          TitleView.this.invalidate();
        }
      });
  }

  public void fixEditingViewVisibility(final EditPanelView paramEditPanelView, int paramInt)
  {
    Logger.d("TitleView", "fixEditingViewVisibility");
    postDelayed(new Runnable()
    {
      public void run()
      {
        int i = 1;
        TitleView.this.checkTitleShow();
        if (!paramEditPanelView.isEditMode())
        {
          TitleView.this.showTitleBar();
          if (TitleView.this.mLanguagePanel.getVisibility() != 0)
            TitleView.this.mLanguagePanel.setVisibility(0);
          return;
        }
        int j = TitleView.this.mLanguagePanel.getHeight();
        paramEditPanelView.disableSuggestions();
        int k = paramEditPanelView.getSuggestTextViewCount();
        Logger.d("TitleView", "fixEditingViewVisibility c=" + k);
        if (!Profile.getInstantTranslation(TitleView.this.mActivity))
        {
          Logger.d("TitleView", "fixEditingViewVisibility NO INSTANT c=" + k);
          k++;
        }
        label196: EditPanelView localEditPanelView2;
        if (k > 5)
        {
          TitleView.this.showTitleBar();
          if (k <= 3)
            break label257;
          if (TitleView.this.mLanguagePanel.getVisibility() != 0)
          {
            TitleView.this.mLanguagePanel.setVisibility(0);
            paramEditPanelView.onEditPanelHeightChanged(j);
          }
          if (k > 4)
            paramEditPanelView.enableSuggestions();
          if (Util.getSdkVersion() < 14)
            break label302;
          localEditPanelView2 = paramEditPanelView;
          if (k <= i)
            break label297;
        }
        while (true)
        {
          localEditPanelView2.initInstantTranslation(i);
          TitleView.this.getRootView().invalidate();
          return;
          TitleView.this.hideTitleBar();
          break;
          label257: if (TitleView.this.mLanguagePanel.getVisibility() == 8)
            break label196;
          TitleView.this.slideUpInputMethodView(TitleView.this.mLanguagePanel);
          paramEditPanelView.onEditPanelHeightChanged(j);
          break label196;
          label297: i = 0;
        }
        label302: EditPanelView localEditPanelView1 = paramEditPanelView;
        if (k > 3);
        while (true)
        {
          localEditPanelView1.initInstantTranslation(i);
          break;
          i = 0;
        }
      }
    }
    , paramInt);
  }

  public void hideTitleBar()
  {
    Logger.d("TitleView", "hideTitleBar");
    setVisibility(8);
    if (Util.isHoneycombCompatible())
      hideActionBar();
  }

  public void setLanguagePanel(View paramView)
  {
    this.mLanguagePanel = paramView;
  }

  public void showTitleBar()
  {
    checkTitleShow();
    if (Util.isHoneycombCompatible())
      showActionBar();
    setVisibility(0);
    if (this.mLanguagePanel != null)
      this.mLanguagePanel.setVisibility(0);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.home.TitleView
 * JD-Core Version:    0.6.2
 */