package com.google.android.apps.translate.offline;

import android.text.TextUtils;
import com.google.android.apps.translate.Languages;
import com.google.android.apps.translatedecoder.profiles.LanguagePairProfile;
import java.util.Comparator;

public class LanguagePairProfileComparator
  implements Comparator<LanguagePairProfile>
{
  private Languages mLanguageList;

  public LanguagePairProfileComparator(Languages paramLanguages)
  {
    this.mLanguageList = paramLanguages;
  }

  private String getLanguageNameWithEnglishMovedToEnd(String paramString)
  {
    if (paramString.equals("en"))
      return "zzz";
    return getLanguageName(paramString);
  }

  public int compare(LanguagePairProfile paramLanguagePairProfile1, LanguagePairProfile paramLanguagePairProfile2)
  {
    String str1 = getLanguageNameWithEnglishMovedToEnd(paramLanguagePairProfile1.getFromLangCode());
    String str2 = getLanguageNameWithEnglishMovedToEnd(paramLanguagePairProfile1.getToLangCode());
    String str3 = getLanguageNameWithEnglishMovedToEnd(paramLanguagePairProfile2.getFromLangCode());
    int i = str2.compareTo(getLanguageNameWithEnglishMovedToEnd(paramLanguagePairProfile2.getToLangCode()));
    int j = str1.compareTo(str3);
    if (j == 0)
      return i;
    return j;
  }

  public String getLanguageName(String paramString)
  {
    String str;
    if (this.mLanguageList == null)
      str = paramString;
    do
    {
      return str;
      if (paramString.equals("zh"))
        paramString = "zh-CN";
      str = this.mLanguageList.getFromLanguageLongName(paramString);
    }
    while (!TextUtils.isEmpty(str));
    return paramString;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.offline.LanguagePairProfileComparator
 * JD-Core Version:    0.6.2
 */