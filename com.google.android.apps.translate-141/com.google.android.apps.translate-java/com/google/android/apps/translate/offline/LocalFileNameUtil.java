package com.google.android.apps.translate.offline;

import android.os.Environment;
import com.google.android.apps.translate.Languages;
import java.io.File;

public class LocalFileNameUtil
{
  private static final String CONFIGS_DIR = "configs";
  private static final String LANGUAGE_MODELS_DIR = "lms";
  public static final String LOCAL_MODEL_PROFILES = "localModelProfiles";
  public static final String PROFILES_URL = "http://dl.google.com/translate/offline/profiles.txt";

  public static String getLocalConfigFile()
  {
    return getLocalTranslateRoot() + "/common/config.txt";
  }

  public static String getLocalConfigFile(String paramString1, String paramString2)
  {
    StringBuilder localStringBuilder = new StringBuilder(getLocalTranslateRoot());
    localStringBuilder.append("/configs/");
    localStringBuilder.append(paramString1);
    localStringBuilder.append(".");
    localStringBuilder.append(paramString2);
    localStringBuilder.append("_data.config.txt");
    return localStringBuilder.toString();
  }

  public static String getLocalLmFile(String paramString)
  {
    return getLocalLmsFileNamePrefix(paramString) + "lm.bit.trie";
  }

  public static String getLocalLmSymbolFile(String paramString)
  {
    return getLocalLmsFileNamePrefix(paramString) + "lm.symbol";
  }

  private static String getLocalLmsFileNamePrefix(String paramString)
  {
    return getLocalTranslateRoot() + "/lms/" + paramString + "_data-00000-of-00001.mobilelm/";
  }

  public static String getLocalPreprocFile()
  {
    return getLocalTranslateRoot() + "/common/preproc.data";
  }

  public static String getLocalProfilesFile()
  {
    return getLocalTranslateRoot() + "/profiles.txt";
  }

  private static String getLocalPtFileNamePrefix(String paramString1, String paramString2)
  {
    return getLocalPtsFileNamePrefix(paramString1, paramString2) + "_data-00000-of-00001.bitpt/";
  }

  public static String getLocalPtSymbolFile(String paramString1, String paramString2)
  {
    return getLocalPtFileNamePrefix(paramString1, paramString2) + "pt.symbol";
  }

  private static String getLocalPtsFileNamePrefix(String paramString1, String paramString2)
  {
    return getLocalTranslateRoot() + "/pts/" + paramString1 + "." + paramString2;
  }

  public static String getLocalTmFile(String paramString1, String paramString2)
  {
    return getLocalPtFileNamePrefix(paramString1, paramString2) + "pt.bit.trie";
  }

  private static String getLocalTranslateRoot()
  {
    File localFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/googletranslate/models/");
    localFile.mkdirs();
    return localFile.getAbsolutePath();
  }

  public static String getOfflineLanguageCode(String paramString)
  {
    if (Languages.isChinese(paramString))
      paramString = "zh";
    return paramString;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.offline.LocalFileNameUtil
 * JD-Core Version:    0.6.2
 */