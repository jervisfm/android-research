package com.google.android.apps.translate.offline;

public class ModelDownloader
{
  private static final int BUFFER_SIZE = 51200;
  private static final int CONNECTION_TIMEOUT = 50000;
  private static final int SOCKET_TIMEOUT = 300000;
  private static final String TAG = "ModelDownloader";

  // ERROR //
  public static boolean downloadFile(String paramString1, String paramString2)
  {
    // Byte code:
    //   0: new 27	java/net/URL
    //   3: dup
    //   4: aload_0
    //   5: invokespecial 30	java/net/URL:<init>	(Ljava/lang/String;)V
    //   8: astore_2
    //   9: invokestatic 36	java/lang/System:currentTimeMillis	()J
    //   12: lstore_3
    //   13: ldc 15
    //   15: new 38	java/lang/StringBuilder
    //   18: dup
    //   19: invokespecial 39	java/lang/StringBuilder:<init>	()V
    //   22: ldc 41
    //   24: invokevirtual 45	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   27: aload_0
    //   28: invokevirtual 45	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   31: invokevirtual 49	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   34: invokestatic 55	com/google/android/apps/translate/Logger:d	(Ljava/lang/String;Ljava/lang/String;)V
    //   37: aload_2
    //   38: invokevirtual 59	java/net/URL:openConnection	()Ljava/net/URLConnection;
    //   41: astore 6
    //   43: aload 6
    //   45: ldc 11
    //   47: invokevirtual 65	java/net/URLConnection:setReadTimeout	(I)V
    //   50: aload 6
    //   52: ldc 9
    //   54: invokevirtual 68	java/net/URLConnection:setConnectTimeout	(I)V
    //   57: new 70	java/io/File
    //   60: dup
    //   61: aload_1
    //   62: invokespecial 71	java/io/File:<init>	(Ljava/lang/String;)V
    //   65: invokevirtual 75	java/io/File:getParentFile	()Ljava/io/File;
    //   68: astore 7
    //   70: aload 7
    //   72: invokevirtual 79	java/io/File:exists	()Z
    //   75: istore 8
    //   77: iconst_0
    //   78: istore 9
    //   80: iload 8
    //   82: ifeq +14 -> 96
    //   85: aload 7
    //   87: invokevirtual 82	java/io/File:isDirectory	()Z
    //   90: ifeq +127 -> 217
    //   93: iconst_1
    //   94: istore 9
    //   96: iload 9
    //   98: ifne +164 -> 262
    //   101: aload 7
    //   103: invokevirtual 85	java/io/File:mkdirs	()Z
    //   106: ifne +156 -> 262
    //   109: ldc 15
    //   111: new 38	java/lang/StringBuilder
    //   114: dup
    //   115: invokespecial 39	java/lang/StringBuilder:<init>	()V
    //   118: ldc 87
    //   120: invokevirtual 45	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   123: aload 7
    //   125: invokevirtual 90	java/io/File:getPath	()Ljava/lang/String;
    //   128: invokevirtual 45	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   131: invokevirtual 49	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   134: invokestatic 93	com/google/android/apps/translate/Logger:e	(Ljava/lang/String;Ljava/lang/String;)V
    //   137: iconst_0
    //   138: ireturn
    //   139: astore 16
    //   141: ldc 15
    //   143: new 38	java/lang/StringBuilder
    //   146: dup
    //   147: invokespecial 39	java/lang/StringBuilder:<init>	()V
    //   150: ldc 95
    //   152: invokevirtual 45	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   155: aload_0
    //   156: invokevirtual 45	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   159: ldc 97
    //   161: invokevirtual 45	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   164: aload_1
    //   165: invokevirtual 45	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   168: invokevirtual 49	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   171: aload 16
    //   173: invokestatic 100	com/google/android/apps/translate/Logger:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    //   176: iconst_0
    //   177: ireturn
    //   178: astore 5
    //   180: ldc 15
    //   182: new 38	java/lang/StringBuilder
    //   185: dup
    //   186: invokespecial 39	java/lang/StringBuilder:<init>	()V
    //   189: ldc 102
    //   191: invokevirtual 45	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   194: aload_0
    //   195: invokevirtual 45	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   198: ldc 97
    //   200: invokevirtual 45	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   203: aload_1
    //   204: invokevirtual 45	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   207: invokevirtual 49	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   210: aload 5
    //   212: invokestatic 100	com/google/android/apps/translate/Logger:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    //   215: iconst_0
    //   216: ireturn
    //   217: aload 7
    //   219: invokevirtual 105	java/io/File:delete	()Z
    //   222: istore 15
    //   224: iconst_0
    //   225: istore 9
    //   227: iload 15
    //   229: ifne -133 -> 96
    //   232: ldc 15
    //   234: new 38	java/lang/StringBuilder
    //   237: dup
    //   238: invokespecial 39	java/lang/StringBuilder:<init>	()V
    //   241: ldc 107
    //   243: invokevirtual 45	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   246: aload 7
    //   248: invokevirtual 90	java/io/File:getPath	()Ljava/lang/String;
    //   251: invokevirtual 45	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   254: invokevirtual 49	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   257: invokestatic 93	com/google/android/apps/translate/Logger:e	(Ljava/lang/String;Ljava/lang/String;)V
    //   260: iconst_0
    //   261: ireturn
    //   262: new 109	java/io/BufferedInputStream
    //   265: dup
    //   266: aload 6
    //   268: invokevirtual 113	java/net/URLConnection:getInputStream	()Ljava/io/InputStream;
    //   271: ldc 7
    //   273: invokespecial 116	java/io/BufferedInputStream:<init>	(Ljava/io/InputStream;I)V
    //   276: astore 10
    //   278: new 118	java/io/FileOutputStream
    //   281: dup
    //   282: aload_1
    //   283: invokespecial 119	java/io/FileOutputStream:<init>	(Ljava/lang/String;)V
    //   286: astore 11
    //   288: ldc 7
    //   290: newarray byte
    //   292: astore 13
    //   294: aload 10
    //   296: aload 13
    //   298: invokevirtual 123	java/io/BufferedInputStream:read	([B)I
    //   301: istore 14
    //   303: iload 14
    //   305: iconst_m1
    //   306: if_icmpeq +29 -> 335
    //   309: aload 11
    //   311: aload 13
    //   313: iconst_0
    //   314: iload 14
    //   316: invokevirtual 127	java/io/FileOutputStream:write	([BII)V
    //   319: goto -25 -> 294
    //   322: astore 12
    //   324: ldc 15
    //   326: ldc 129
    //   328: aload 12
    //   330: invokestatic 100	com/google/android/apps/translate/Logger:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    //   333: iconst_0
    //   334: ireturn
    //   335: aload 11
    //   337: invokevirtual 132	java/io/FileOutputStream:flush	()V
    //   340: aload 11
    //   342: invokevirtual 135	java/io/FileOutputStream:close	()V
    //   345: aload 10
    //   347: invokevirtual 136	java/io/BufferedInputStream:close	()V
    //   350: ldc 15
    //   352: new 38	java/lang/StringBuilder
    //   355: dup
    //   356: invokespecial 39	java/lang/StringBuilder:<init>	()V
    //   359: ldc 138
    //   361: invokevirtual 45	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   364: invokestatic 36	java/lang/System:currentTimeMillis	()J
    //   367: lload_3
    //   368: lsub
    //   369: ldc2_w 139
    //   372: ldiv
    //   373: invokevirtual 143	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   376: ldc 145
    //   378: invokevirtual 45	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   381: invokevirtual 49	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   384: invokestatic 55	com/google/android/apps/translate/Logger:d	(Ljava/lang/String;Ljava/lang/String;)V
    //   387: iconst_1
    //   388: ireturn
    //
    // Exception table:
    //   from	to	target	type
    //   0	9	139	java/net/MalformedURLException
    //   37	43	178	java/io/IOException
    //   262	294	322	java/io/IOException
    //   294	303	322	java/io/IOException
    //   309	319	322	java/io/IOException
    //   335	350	322	java/io/IOException
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.offline.ModelDownloader
 * JD-Core Version:    0.6.2
 */