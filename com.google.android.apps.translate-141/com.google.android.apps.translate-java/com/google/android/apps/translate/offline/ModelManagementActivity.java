package com.google.android.apps.translate.offline;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import com.google.android.apps.translate.Languages;
import com.google.android.apps.translate.LanguagesFactory;
import com.google.android.apps.translate.Logger;
import com.google.android.apps.translate.R.layout;
import com.google.android.apps.translate.TranslateApplication;
import com.google.android.apps.translatedecoder.profiles.LanguagePairProfile;
import com.google.android.apps.translatedecoder.profiles.LanguageProfile;
import com.google.android.apps.translatedecoder.profiles.OfflineDataProfile;
import com.google.android.apps.translatedecoder.profiles.OfflineDataProfileException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;

public class ModelManagementActivity extends ListActivity
{
  private static final boolean OFFLINE_TRANSLATE_SUPPORTED;
  private OfflineDataProfile backendProfiles;
  private SharedPreferences localProfiles;
  private ProfilesAdapter profilesAdapter;
  private ProgressDialog progressDialog;
  private final boolean usbMode = false;

  private void fetchProfiles()
  {
    if (!ModelDownloader.downloadFile("http://dl.google.com/translate/offline/profiles.txt", LocalFileNameUtil.getLocalProfilesFile()))
    {
      Logger.e("profileFetcher", "Download profile error");
      return;
    }
    try
    {
      this.backendProfiles = OfflineDataProfile.readFromFile(LocalFileNameUtil.getLocalProfilesFile());
      this.localProfiles = getSharedPreferences("localModelProfiles", 0);
      return;
    }
    catch (OfflineDataProfileException localOfflineDataProfileException)
    {
      Logger.e(localOfflineDataProfileException.getMessage(), localOfflineDataProfileException);
    }
  }

  public static boolean isOfflineTranslateSupported(Context paramContext)
  {
    return !TranslateApplication.isReleaseBuild(paramContext);
  }

  public void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setContentView(R.layout.offline_model_management);
    this.profilesAdapter = new ProfilesAdapter(this, R.layout.offline_model_adaptor_row, new ArrayList(), null, false);
    setListAdapter(this.profilesAdapter);
    new ProfileFetcherTask(null).execute(new String[] { "" });
  }

  private class ProfileFetcherTask extends AsyncTask<String, Void, Void>
  {
    private ProfileFetcherTask()
    {
    }

    protected Void doInBackground(String[] paramArrayOfString)
    {
      ModelManagementActivity.this.fetchProfiles();
      return null;
    }

    protected void onPostExecute(Void paramVoid)
    {
      if ((ModelManagementActivity.this.backendProfiles != null) && (ModelManagementActivity.this.backendProfiles.getLanguageProfiles().size() > 0) && (ModelManagementActivity.this.backendProfiles.getLanguagePairProfiles().size() > 0))
      {
        ModelManagementActivity.this.profilesAdapter.notifyDataSetChanged();
        ModelManagementActivity.this.profilesAdapter.setLocalProfiles(ModelManagementActivity.this.localProfiles);
        ModelManagementActivity.this.profilesAdapter.setCommonProfile(ModelManagementActivity.this.backendProfiles.getCommonProfile());
        Iterator localIterator1 = ModelManagementActivity.this.backendProfiles.getLanguageProfiles().iterator();
        while (localIterator1.hasNext())
        {
          LanguageProfile localLanguageProfile = (LanguageProfile)localIterator1.next();
          ModelManagementActivity.this.profilesAdapter.addLanguageProfile(localLanguageProfile);
        }
        Languages localLanguages = LanguagesFactory.get().getLanguages(ModelManagementActivity.this, Locale.getDefault());
        Iterator localIterator2 = ModelManagementActivity.this.backendProfiles.getLanguagePairProfiles().iterator();
        while (localIterator2.hasNext())
        {
          LanguagePairProfile localLanguagePairProfile = (LanguagePairProfile)localIterator2.next();
          ModelManagementActivity.this.profilesAdapter.add(localLanguagePairProfile);
        }
        LanguagePairProfileComparator localLanguagePairProfileComparator = new LanguagePairProfileComparator(localLanguages);
        ModelManagementActivity.this.profilesAdapter.sort(localLanguagePairProfileComparator);
        ModelManagementActivity.this.profilesAdapter.setLanguagePairProfileComparator(localLanguagePairProfileComparator);
      }
      while (true)
      {
        ModelManagementActivity.this.progressDialog.dismiss();
        ModelManagementActivity.this.profilesAdapter.notifyDataSetChanged();
        return;
        Logger.e("profilesFetcher", "No profiles");
      }
    }

    protected void onPreExecute()
    {
      ModelManagementActivity.access$102(ModelManagementActivity.this, ProgressDialog.show(ModelManagementActivity.this, "Please wait...", "Retrieving profiles ...", true));
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.offline.ModelManagementActivity
 * JD-Core Version:    0.6.2
 */