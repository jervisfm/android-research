package com.google.android.apps.translate.offline;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.apps.translate.Logger;
import com.google.android.apps.translate.Maps;
import com.google.android.apps.translate.R.id;
import com.google.android.apps.translate.R.layout;
import com.google.android.apps.translatedecoder.profiles.CommonProfile;
import com.google.android.apps.translatedecoder.profiles.LanguagePairProfile;
import com.google.android.apps.translatedecoder.profiles.LanguageProfile;
import java.util.List;
import java.util.Map;

public class ProfilesAdapter extends ArrayAdapter<LanguagePairProfile>
  implements View.OnClickListener
{
  public static final String KEY_OFFLINE_COMMON_PREFIX = "key_offline_common_";
  public static final String KEY_OFFLINE_LANGPAIR_MODEL_PREFIX = "key_offline_langpair_";
  public static final String KEY_OFFLINE_LANG_MODEL_PREFIX = "key_offline_lang_";
  public static final String KEY_OFFLINE_SUFFIX_SEPARATOR = ".";
  public static final String KEY_OFFLINE_VERSION = "key_offline_version";
  private static final String TAG = "ProfilesAdapter";
  private CommonProfile commonProfile;
  private final List<LanguagePairProfile> languagePairs;
  private Map<String, LanguageProfile> languageProfileMap = Maps.newHashMap();
  private SharedPreferences localProfiles;
  private LanguagePairProfileComparator mComparator;
  private ProgressDialog progressDialog = null;
  private final boolean usbMode;

  public ProfilesAdapter(Context paramContext, int paramInt, List<LanguagePairProfile> paramList, SharedPreferences paramSharedPreferences, boolean paramBoolean)
  {
    super(paramContext, paramInt, paramList);
    this.languagePairs = paramList;
    this.localProfiles = paramSharedPreferences;
    this.usbMode = paramBoolean;
  }

  private void fetchCommonFiles()
  {
    if (hasLocalCommonFiles(this.localProfiles, this.commonProfile))
      return;
    ModelDownloader.downloadFile(this.commonProfile.getConfigFile(), LocalFileNameUtil.getLocalConfigFile());
    ModelDownloader.downloadFile(this.commonProfile.getPreprocFile(), LocalFileNameUtil.getLocalPreprocFile());
    setLocalCommonFiles(this.localProfiles, this.commonProfile);
    setLocalVersion(this.localProfiles, this.commonProfile.getVersion());
  }

  private void fetchLanguageModels(LanguageProfile paramLanguageProfile)
  {
    if (hasLocalLanguageModel(this.localProfiles, paramLanguageProfile))
      return;
    String str = paramLanguageProfile.getLangCode();
    ModelDownloader.downloadFile(paramLanguageProfile.getLmFile(), LocalFileNameUtil.getLocalLmFile(str));
    ModelDownloader.downloadFile(paramLanguageProfile.getLmSymbolFile(), LocalFileNameUtil.getLocalLmSymbolFile(paramLanguageProfile.getLangCode()));
    setLocalLanguageModel(this.localProfiles, paramLanguageProfile);
  }

  private void fetchLanguagePairModels(LanguagePairProfile paramLanguagePairProfile)
  {
    if (hasLocalLanguagePairModel(this.localProfiles, paramLanguagePairProfile))
      return;
    String str1 = paramLanguagePairProfile.getFromLangCode();
    String str2 = paramLanguagePairProfile.getToLangCode();
    ModelDownloader.downloadFile(paramLanguagePairProfile.getConfigFile(), LocalFileNameUtil.getLocalConfigFile(str1, str2));
    ModelDownloader.downloadFile(paramLanguagePairProfile.getTmFile(), LocalFileNameUtil.getLocalTmFile(str1, str2));
    ModelDownloader.downloadFile(paramLanguagePairProfile.getPtSymbolFile(), LocalFileNameUtil.getLocalPtSymbolFile(str1, str2));
    setLocalLanguagePairModel(this.localProfiles, paramLanguagePairProfile);
  }

  private void fetchModels(LanguagePairProfile paramLanguagePairProfile)
  {
    try
    {
      fetchCommonFiles();
      fetchLanguageModels((LanguageProfile)this.languageProfileMap.get(paramLanguagePairProfile.getToLangCode()));
      fetchLanguagePairModels(paramLanguagePairProfile);
      return;
    }
    catch (Exception localException)
    {
      Log.e("modelFetcher", localException.getMessage());
      Toast.makeText(getContext(), "Download model failed", 0).show();
    }
  }

  public static String getCommonFilesKey(int paramInt)
  {
    StringBuilder localStringBuilder = new StringBuilder("key_offline_common_");
    localStringBuilder.append(paramInt);
    return localStringBuilder.toString();
  }

  public static String getCommonFilesKey(CommonProfile paramCommonProfile)
  {
    StringBuilder localStringBuilder = new StringBuilder("key_offline_common_");
    localStringBuilder.append(paramCommonProfile.getVersion());
    return localStringBuilder.toString();
  }

  public static String getLanguageModelKey(LanguageProfile paramLanguageProfile)
  {
    return getLanguageModelKey(paramLanguageProfile.getLangCode(), paramLanguageProfile.getVersion());
  }

  public static String getLanguageModelKey(String paramString, int paramInt)
  {
    StringBuilder localStringBuilder = new StringBuilder("key_offline_lang_");
    localStringBuilder.append(paramInt);
    localStringBuilder.append(".");
    localStringBuilder.append(paramString);
    return localStringBuilder.toString();
  }

  public static String getLanguagePairModelKey(LanguagePairProfile paramLanguagePairProfile)
  {
    return getLanguagePairModelKey(paramLanguagePairProfile.getFromLangCode(), paramLanguagePairProfile.getToLangCode(), paramLanguagePairProfile.getVersion());
  }

  public static String getLanguagePairModelKey(String paramString1, String paramString2, int paramInt)
  {
    StringBuilder localStringBuilder = new StringBuilder("key_offline_lang_");
    localStringBuilder.append(paramInt);
    localStringBuilder.append(".");
    localStringBuilder.append(paramString1);
    localStringBuilder.append(".");
    localStringBuilder.append(paramString2);
    return localStringBuilder.toString();
  }

  public static int getLocalVersion(SharedPreferences paramSharedPreferences)
  {
    return paramSharedPreferences.getInt("key_offline_version", -1);
  }

  public static boolean hasLocalCommonFiles(SharedPreferences paramSharedPreferences, CommonProfile paramCommonProfile)
  {
    return paramSharedPreferences.getBoolean(getCommonFilesKey(paramCommonProfile), false);
  }

  public static boolean hasLocalLanguageModel(SharedPreferences paramSharedPreferences, LanguageProfile paramLanguageProfile)
  {
    return paramSharedPreferences.getBoolean(getLanguageModelKey(paramLanguageProfile), false);
  }

  public static boolean hasLocalLanguageModel(SharedPreferences paramSharedPreferences, String paramString, int paramInt)
  {
    return paramSharedPreferences.getBoolean(getLanguageModelKey(paramString, paramInt), false);
  }

  public static boolean hasLocalLanguagePairModel(SharedPreferences paramSharedPreferences, LanguagePairProfile paramLanguagePairProfile)
  {
    return paramSharedPreferences.getBoolean(getLanguagePairModelKey(paramLanguagePairProfile), false);
  }

  public static boolean hasLocalModelFiles(SharedPreferences paramSharedPreferences, String paramString1, String paramString2)
  {
    String str1 = LocalFileNameUtil.getOfflineLanguageCode(paramString1);
    String str2 = LocalFileNameUtil.getOfflineLanguageCode(paramString2);
    int i = getLocalVersion(paramSharedPreferences);
    if (i < 0)
    {
      Logger.e("ProfilesAdapter", "version negative version=" + i);
      return false;
    }
    if (!hasLocalLanguageModel(paramSharedPreferences, str2, i))
    {
      Logger.e("ProfilesAdapter", "hasLocalLanguageModel to=" + str2);
      Logger.e("ProfilesAdapter", "hasLocalLanguageModel version=" + i);
      return false;
    }
    if (!paramSharedPreferences.getBoolean(getLanguagePairModelKey(str1, str2, i), false))
    {
      Logger.e("ProfilesAdapter", "getLanguagePairModelKey to=" + str2);
      Logger.e("ProfilesAdapter", "getLanguagePairModelKey from=" + str1);
      Logger.e("ProfilesAdapter", "getLanguagePairModelKey version=" + i);
      return false;
    }
    return true;
  }

  private static void removeLocalModel(SharedPreferences paramSharedPreferences, LanguagePairProfile paramLanguagePairProfile)
  {
    SharedPreferences.Editor localEditor = paramSharedPreferences.edit();
    localEditor.remove(getLanguagePairModelKey(paramLanguagePairProfile));
    localEditor.remove(getLanguageModelKey(paramLanguagePairProfile.getToLangCode(), paramLanguagePairProfile.getVersion()));
    localEditor.remove(getCommonFilesKey(paramLanguagePairProfile.getVersion()));
    localEditor.commit();
  }

  private static void setLocalCommonFiles(SharedPreferences paramSharedPreferences, CommonProfile paramCommonProfile)
  {
    SharedPreferences.Editor localEditor = paramSharedPreferences.edit();
    localEditor.putBoolean(getCommonFilesKey(paramCommonProfile), true);
    localEditor.commit();
  }

  private static void setLocalLanguageModel(SharedPreferences paramSharedPreferences, LanguageProfile paramLanguageProfile)
  {
    SharedPreferences.Editor localEditor = paramSharedPreferences.edit();
    localEditor.putBoolean(getLanguageModelKey(paramLanguageProfile), true);
    localEditor.commit();
  }

  private static void setLocalLanguagePairModel(SharedPreferences paramSharedPreferences, LanguagePairProfile paramLanguagePairProfile)
  {
    SharedPreferences.Editor localEditor = paramSharedPreferences.edit();
    localEditor.putBoolean(getLanguagePairModelKey(paramLanguagePairProfile), true);
    localEditor.commit();
  }

  private static void setLocalVersion(SharedPreferences paramSharedPreferences, int paramInt)
  {
    SharedPreferences.Editor localEditor = paramSharedPreferences.edit();
    localEditor.putInt("key_offline_version", paramInt);
    localEditor.commit();
  }

  public void addLanguageProfile(LanguageProfile paramLanguageProfile)
  {
    this.languageProfileMap.put(paramLanguageProfile.getLangCode(), paramLanguageProfile);
  }

  public SharedPreferences getLocalProfiles()
  {
    return this.localProfiles;
  }

  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    View localView = paramView;
    if (localView == null)
      localView = ((LayoutInflater)getContext().getSystemService("layout_inflater")).inflate(R.layout.offline_model_adaptor_row, null);
    LanguagePairProfile localLanguagePairProfile = (LanguagePairProfile)this.languagePairs.get(paramInt);
    Button localButton1;
    if (localLanguagePairProfile != null)
    {
      TextView localTextView1 = (TextView)localView.findViewById(R.id.fromlang);
      if (localTextView1 != null)
        localTextView1.setText(this.mComparator.getLanguageName(localLanguagePairProfile.getFromLangCode()));
      TextView localTextView2 = (TextView)localView.findViewById(R.id.tolang);
      if (localTextView2 != null)
        localTextView2.setText(this.mComparator.getLanguageName(localLanguagePairProfile.getToLangCode()));
      localButton1 = (Button)localView.findViewById(R.id.download);
      if (localButton1 != null)
      {
        localButton1.setText("dowload");
        localButton1.setTag(Integer.valueOf(paramInt));
        if (!hasLocalLanguagePairModel(this.localProfiles, localLanguagePairProfile))
          break label241;
        localButton1.setClickable(false);
        localButton1.setEnabled(false);
      }
    }
    Button localButton2;
    while (true)
    {
      localButton2 = (Button)localView.findViewById(R.id.delete);
      if (localButton2 != null)
      {
        localButton2.setText("delete");
        localButton2.setTag(Integer.valueOf(paramInt));
        if (!hasLocalLanguagePairModel(this.localProfiles, localLanguagePairProfile))
          break;
        localButton2.setEnabled(true);
        localButton2.setOnClickListener(this);
      }
      return localView;
      label241: localButton1.setEnabled(true);
      localButton1.setOnClickListener(this);
    }
    localButton2.setClickable(false);
    localButton2.setEnabled(false);
    return localView;
  }

  public void onClick(View paramView)
  {
    if (paramView.getId() == R.id.download)
    {
      localDownloadTask = new DownloadTask(null);
      arrayOfString1 = new String[1];
      arrayOfString1[0] = ((Integer)paramView.getTag()).toString();
      localDownloadTask.execute(arrayOfString1);
    }
    while (paramView.getId() != R.id.delete)
    {
      DownloadTask localDownloadTask;
      String[] arrayOfString1;
      return;
    }
    DeleteTask localDeleteTask = new DeleteTask(null);
    String[] arrayOfString2 = new String[1];
    arrayOfString2[0] = ((Integer)paramView.getTag()).toString();
    localDeleteTask.execute(arrayOfString2);
  }

  public void setCommonProfile(CommonProfile paramCommonProfile)
  {
    this.commonProfile = paramCommonProfile;
  }

  public void setLanguagePairProfileComparator(LanguagePairProfileComparator paramLanguagePairProfileComparator)
  {
    this.mComparator = paramLanguagePairProfileComparator;
  }

  public void setLocalProfiles(SharedPreferences paramSharedPreferences)
  {
    this.localProfiles = paramSharedPreferences;
  }

  private class DeleteTask extends AsyncTask<String, Void, LanguagePairProfile>
  {
    private DeleteTask()
    {
    }

    protected LanguagePairProfile doInBackground(String[] paramArrayOfString)
    {
      LanguagePairProfile localLanguagePairProfile = (LanguagePairProfile)ProfilesAdapter.this.languagePairs.get(new Integer(paramArrayOfString[0]).intValue());
      try
      {
        if (ProfilesAdapter.this.usbMode)
          Thread.sleep(2000L);
        ProfilesAdapter.removeLocalModel(ProfilesAdapter.this.getLocalProfiles(), localLanguagePairProfile);
        return localLanguagePairProfile;
      }
      catch (InterruptedException localInterruptedException)
      {
        localInterruptedException.printStackTrace();
      }
      return localLanguagePairProfile;
    }

    protected void onPostExecute(LanguagePairProfile paramLanguagePairProfile)
    {
      ProfilesAdapter.this.progressDialog.dismiss();
      ProfilesAdapter.this.notifyDataSetChanged();
    }

    protected void onPreExecute()
    {
      ProfilesAdapter.access$202(ProfilesAdapter.this, ProgressDialog.show(ProfilesAdapter.this.getContext(), "Please wait...", "Deleting model, this may take a few minutes ...", true));
    }
  }

  private class DownloadTask extends AsyncTask<String, Void, LanguagePairProfile>
  {
    private DownloadTask()
    {
    }

    protected LanguagePairProfile doInBackground(String[] paramArrayOfString)
    {
      LanguagePairProfile localLanguagePairProfile = (LanguagePairProfile)ProfilesAdapter.this.languagePairs.get(new Integer(paramArrayOfString[0]).intValue());
      try
      {
        if (ProfilesAdapter.this.usbMode)
        {
          Thread.sleep(2000L);
          ProfilesAdapter.setLocalLanguagePairModel(ProfilesAdapter.this.getLocalProfiles(), localLanguagePairProfile);
          return localLanguagePairProfile;
        }
        ProfilesAdapter.this.fetchModels(localLanguagePairProfile);
        return localLanguagePairProfile;
      }
      catch (InterruptedException localInterruptedException)
      {
        localInterruptedException.printStackTrace();
      }
      return localLanguagePairProfile;
    }

    protected void onPostExecute(LanguagePairProfile paramLanguagePairProfile)
    {
      ProfilesAdapter.this.progressDialog.dismiss();
      ProfilesAdapter.this.notifyDataSetChanged();
    }

    protected void onPreExecute()
    {
      ProfilesAdapter.access$202(ProfilesAdapter.this, ProgressDialog.show(ProfilesAdapter.this.getContext(), "Please wait...", "Downloaing model, this may take a few minutes ...", true));
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.offline.ProfilesAdapter
 * JD-Core Version:    0.6.2
 */