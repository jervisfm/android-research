package com.google.android.apps.translate.offline;

import android.util.Log;
import com.google.android.apps.translate.Logger;
import com.google.android.apps.translatedecoder.decoder.Decoder;
import com.google.android.apps.translatedecoder.util.Config;
import com.google.android.apps.translatedecoder.util.HashMapBasedSymbol;
import com.google.android.apps.translatedecoder.util.SymbolTable;
import java.util.HashMap;
import java.util.Map;

public class TranslateOfflineUtil
{
  private static final String LOG_TAG = "TranslateOfflineUtil";
  private static final boolean PROFILING_MODE;
  private static Map<String, Decoder> mDecoders = new HashMap();

  private static Decoder getDecoder(String paramString1, String paramString2)
  {
    String str = paramString1 + " " + paramString2;
    Decoder localDecoder1 = (Decoder)mDecoders.get(str);
    if (localDecoder1 != null)
    {
      Log.i("TranslateOfflineUtil", "=====using old offline decoder=====");
      return localDecoder1;
    }
    Log.i("TranslateOfflineUtil", "=====create a new offline decoder=====");
    Config localConfig1 = new Config(LocalFileNameUtil.getLocalConfigFile());
    Config localConfig2 = new Config(LocalFileNameUtil.getLocalConfigFile(paramString1, paramString2));
    localConfig1.setRelativeLmWeight(localConfig2.relativeLmWeight());
    localConfig1.setOovTmCost(localConfig2.oovTmCost());
    localConfig1.setMaxPhraseLength(-1);
    localConfig1.setBeamSize(3);
    localConfig1.setSrcLang(paramString1);
    Logger.d("TranslateOfflineUtil", "config.relativeLmWeight=" + localConfig1.relativeLmWeight());
    Logger.d("TranslateOfflineUtil", "config.oovTmCost" + localConfig1.oovTmCost());
    Logger.d("TranslateOfflineUtil", "config.srcLang" + localConfig1.srcLang());
    Logger.d("TranslateOfflineUtil", LocalFileNameUtil.getLocalConfigFile());
    Logger.d("TranslateOfflineUtil", LocalFileNameUtil.getLocalConfigFile(paramString1, paramString2));
    Logger.d("TranslateOfflineUtil", LocalFileNameUtil.getLocalPtSymbolFile(paramString1, paramString2));
    Logger.d("TranslateOfflineUtil", LocalFileNameUtil.getLocalLmSymbolFile(paramString2));
    Logger.d("TranslateOfflineUtil", LocalFileNameUtil.getLocalTmFile(paramString1, paramString2));
    Logger.d("TranslateOfflineUtil", LocalFileNameUtil.getLocalLmFile(paramString2));
    Logger.d("TranslateOfflineUtil", LocalFileNameUtil.getLocalPreprocFile());
    localConfig1.setPtSymbolTblFile(LocalFileNameUtil.getLocalPtSymbolFile(paramString1, paramString2));
    localConfig1.setLmSymbolTblFile(LocalFileNameUtil.getLocalLmSymbolFile(paramString2));
    localConfig1.setTmFile(LocalFileNameUtil.getLocalTmFile(paramString1, paramString2));
    localConfig1.setLmFile(LocalFileNameUtil.getLocalLmFile(paramString2));
    localConfig1.setPreprocDataFile(LocalFileNameUtil.getLocalPreprocFile());
    Log.i("TranslateOfflineUtil", "ptFile=" + LocalFileNameUtil.getLocalTmFile(paramString1, paramString2));
    Object localObject1 = new HashMapBasedSymbol();
    Object localObject2 = new HashMapBasedSymbol();
    if (localConfig1.readSymbolFromFile())
    {
      localObject1 = SymbolTable.readFromFile(localConfig1.ptSymbolTblFile());
      localObject2 = SymbolTable.readFromFile(localConfig1.lmSymbolTblFile());
    }
    Decoder localDecoder2 = new Decoder(localConfig1, (SymbolTable)localObject1, (SymbolTable)localObject2);
    mDecoders.put(str, localDecoder2);
    return localDecoder2;
  }

  public static String translate(String paramString1, String paramString2, String paramString3)
  {
    String str = getDecoder(LocalFileNameUtil.getOfflineLanguageCode(paramString1), LocalFileNameUtil.getOfflineLanguageCode(paramString2)).decode(paramString3);
    return "Offline translate:\n" + str + "\n";
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.offline.TranslateOfflineUtil
 * JD-Core Version:    0.6.2
 */