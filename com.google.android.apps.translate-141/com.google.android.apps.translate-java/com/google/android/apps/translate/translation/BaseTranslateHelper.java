package com.google.android.apps.translate.translation;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.preference.PreferenceManager;
import com.google.android.apps.translate.Language;
import com.google.android.apps.translate.Languages;
import com.google.android.apps.translate.Logger;
import com.google.android.apps.translate.Profile;
import java.util.List;

public class BaseTranslateHelper
{
  private static final String TAG = "BaseTranslateHelper";
  protected Activity mActivity;
  protected Languages mLanguageList;
  private List<Language> mRecentToLanguages;
  private final SharedPreferences.OnSharedPreferenceChangeListener mSharedPreferenceChangeListener = new SharedPreferences.OnSharedPreferenceChangeListener()
  {
    public void onSharedPreferenceChanged(SharedPreferences paramAnonymousSharedPreferences, String paramAnonymousString)
    {
      if (paramAnonymousString.equals("key_recent_language_to"))
        BaseTranslateHelper.access$002(BaseTranslateHelper.this, Profile.getRecentToLanguages(BaseTranslateHelper.this.mActivity, BaseTranslateHelper.this.mLanguageList));
    }
  };

  public BaseTranslateHelper()
  {
    Logger.d("BaseTranslateHelper", "BaseTranslateHelper");
  }

  public static Language getFromLanguageGivenToLanguage(Languages paramLanguages, Language paramLanguage, boolean paramBoolean)
  {
    if (Languages.isChinese(paramLanguage));
    for (Language localLanguage = Languages.getDefaultChineseFromLanguage(paramLanguages); ; localLanguage = paramLanguages.getFromLanguageByShortName(paramLanguage.getShortName()))
    {
      if ((!paramBoolean) && (localLanguage == null))
        localLanguage = paramLanguages.getDefaultFromLanguage();
      return localLanguage;
    }
  }

  public static Language getToLanguageGivenFromLanguage(List<Language> paramList, Languages paramLanguages, Language paramLanguage, boolean paramBoolean)
  {
    if (Languages.isChinese(paramLanguage));
    for (Language localLanguage = Languages.getBestChineseToLanguage(paramList, paramLanguages); ; localLanguage = paramLanguages.getToLanguageByShortName(paramLanguage.getShortName()))
    {
      if ((!paramBoolean) && (localLanguage == null))
        localLanguage = paramLanguages.getDefaultToLanguage();
      return localLanguage;
    }
  }

  public Language getFromLanguageGivenToLanguage(Language paramLanguage, boolean paramBoolean)
  {
    return getFromLanguageGivenToLanguage(this.mLanguageList, paramLanguage, paramBoolean);
  }

  public Language getToLanguageGivenFromLanguage(Language paramLanguage, boolean paramBoolean)
  {
    return getToLanguageGivenFromLanguage(this.mRecentToLanguages, this.mLanguageList, paramLanguage, paramBoolean);
  }

  public void onCreate(Activity paramActivity, Languages paramLanguages)
  {
    this.mActivity = paramActivity;
    this.mLanguageList = paramLanguages;
  }

  public void onPause()
  {
    if (this.mActivity != null)
      PreferenceManager.getDefaultSharedPreferences(this.mActivity).unregisterOnSharedPreferenceChangeListener(this.mSharedPreferenceChangeListener);
  }

  public void onResume()
  {
    Logger.d("BaseTranslateHelper", "onResume");
    this.mRecentToLanguages = Profile.getRecentToLanguages(this.mActivity, this.mLanguageList);
    PreferenceManager.getDefaultSharedPreferences(this.mActivity).registerOnSharedPreferenceChangeListener(this.mSharedPreferenceChangeListener);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.translation.BaseTranslateHelper
 * JD-Core Version:    0.6.2
 */