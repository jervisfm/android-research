package com.google.android.apps.translate.translation;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.TextUtils.TruncateAt;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.TextView.BufferType;
import com.google.android.apps.translate.Constants.AppearanceType;
import com.google.android.apps.translate.ExtTypefaceSpan;
import com.google.android.apps.translate.ExternalFonts;
import com.google.android.apps.translate.Language;
import com.google.android.apps.translate.Languages;
import com.google.android.apps.translate.Logger;
import com.google.android.apps.translate.R.drawable;
import com.google.android.apps.translate.R.id;
import com.google.android.apps.translate.R.string;
import com.google.android.apps.translate.R.style;
import com.google.android.apps.translate.Translate.TranslateResult;
import com.google.android.apps.translate.TranslateApplication;
import com.google.android.apps.translate.TranslateManager;
import com.google.android.apps.translate.UserActivityMgr;
import com.google.android.apps.translate.UserActivityMgr.IntervalCountTag;
import com.google.android.apps.translate.UserActivityMgr.RequestSource;
import com.google.android.apps.translate.Util;
import com.google.android.apps.translate.editor.InstantTranslateHandler;
import com.google.android.apps.translate.editor.InstantTranslateHandler.InstantTranslateListner;
import com.google.android.apps.translate.editor.SuggestAdapter;
import com.google.android.apps.translate.editor.SuggestAdapter.SuggestEntry;
import com.google.android.apps.translate.editor.SuggestAdapter.SuggestType;
import com.google.android.apps.translate.history.Entry;
import com.google.android.apps.translate.history.HistoryEntry;
import com.google.android.apps.translate.history.HistoryListAdapter;
import com.google.android.apps.translate.tts.MyTts;
import com.google.android.apps.translate.tts.NetworkTts.Callback;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class ChipView extends LinearLayout
  implements OutputPanelView.OutputPanelViewCallback, View.OnLongClickListener, View.OnClickListener, InstantTranslateHandler.InstantTranslateListner
{
  private static final boolean ADD_LITTLE_STAR_TO_UNSELECTED_CHIP = true;
  private static final boolean DEBUG = false;
  private static final int EXPANDING_ANIMATION_DELAY_MILLIS = 50;
  private static final Set<Integer> LOWER_CHIP_RESOURCE_ID_LIST;
  private static final Set<Integer> PADDING_CHIP_RESOURCE_ID_LIST;
  private static final ExternalFonts PHONETIC_FONT = new ExternalFonts("ipa.ttf");
  private static final ExtTypefaceSpan PHONETIC_SPAN = new ExtTypefaceSpan(PHONETIC_FONT.getTypeface());
  private static final int SCROLLING_ANIMATION_DELAY_MILLIS = 50;
  private static final int SCROLLING_ANIMATION_MILLIS = 300;
  private static final int SCROLLING_OVERRUN_PX = 0;
  private static final Set<Integer> SUGGEST_CHIP_RESOURCE_ID_LIST;
  private static final String TAG = "ChipView";
  private static final boolean UNSELECT_ON_TOUCH = true;
  private static final Set<Integer> UPPER_CHIP_RESOURCE_ID_LIST = new HashSet();
  private TextView mAccessTimeStampView;
  private Activity mActivity;
  private boolean mAnimateScroll = false;
  private TranslateApplication mApp;
  private LinearLayout mChip;
  private View mChipDivider;
  private TextView mCreateTimeStampView;
  private TranslateEntry mCurrentTranslation;
  private View mDictDivider;
  private Language mFromLanguage;
  private TextView mFromLanguageText;
  private InputPanel mInputPanel;
  private TextView mInputText;
  private LinearLayout mInputTextChip;
  private InstantTranslateHandler mInstantTranslateHandler;
  private boolean mIsExpanded = false;
  private HistoryEntry mItemEntry;
  private Languages mLanguageList;
  private HistoryListAdapter mListAdapter;
  private ListView mListView;
  private ImageButton mLittleStarBtn1;
  private ImageButton mLittleStarBtn2;
  private OutputPanelView mOutputView;
  private SuggestAdapter.SuggestEntry mSpellCorrectionEntry;
  private SuggestAdapter.SuggestEntry mSrcLangDetectEntry;
  private TextView mSrcTransliteration;
  private View mSrcTts;
  private View mSrcTtsWrapper;
  private TextView mSuggestTypeView;
  private TextView mSuggestView;
  private Language mToLanguage;
  private TextView mToLanguageText;
  private int mTouchEventViewId;
  private TranslateManager mTranslateManager;
  private TextView mTranslatedText;
  private LinearLayout mTranslationChip;
  private TextView mTrgTransliteration;
  private View mTrgTts;
  private View mTrgTtsWrapper;
  private MyTts mTts;
  private int mTtsSpinningWheelHeight;
  private int mTtsSpinningWheelWidth;

  static
  {
    SUGGEST_CHIP_RESOURCE_ID_LIST = new HashSet();
    LOWER_CHIP_RESOURCE_ID_LIST = new HashSet();
    PADDING_CHIP_RESOURCE_ID_LIST = new HashSet();
    UPPER_CHIP_RESOURCE_ID_LIST.add(Integer.valueOf(R.id.mvh_input_text));
    SUGGEST_CHIP_RESOURCE_ID_LIST.add(Integer.valueOf(R.id.mvh_suggest_text));
    LOWER_CHIP_RESOURCE_ID_LIST.add(Integer.valueOf(R.id.mvh_translated_text));
    PADDING_CHIP_RESOURCE_ID_LIST.add(Integer.valueOf(R.id.chip_divider));
  }

  public ChipView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }

  private boolean canSpeak(String paramString)
  {
    if (this.mTts == null)
      return false;
    boolean bool = this.mTts.isLanguageAvailable(Util.languageShortNameToLocale(paramString));
    Logger.d("ChipView", "is TTS available: " + paramString + ", return: " + bool);
    return bool;
  }

  private int getPosition()
  {
    ChipViewTag localChipViewTag = (ChipViewTag)getTag();
    if (localChipViewTag == null)
      return 0;
    return localChipViewTag.position;
  }

  private CharSequence getTranslationText()
  {
    if (this.mCurrentTranslation == null)
      return "";
    return new Translate.TranslateResult(this.mCurrentTranslation.outputText).getTranslateText();
  }

  private void initializeOutputPanelView()
  {
    this.mOutputView.init(this, this, this.mTranslateManager, this.mLanguageList);
    this.mOutputView.postBackCurrentTranslation(this.mCurrentTranslation);
    this.mOutputView.setResult(this.mCurrentTranslation);
  }

  private void renderOutputView(final ViewState paramViewState)
  {
    this.mActivity.runOnUiThread(new Runnable()
    {
      public void run()
      {
        switch (ChipView.7.$SwitchMap$com$google$android$apps$translate$translation$ChipView$ViewState[paramViewState.ordinal()])
        {
        default:
        case 1:
          while (true)
          {
            ChipView.this.setMinimumHeight(-1);
            ChipView.this.invalidate();
            return;
            Logger.d("ChipView", "renderOutputView UNSELECTED");
            ChipView.this.mChip.setBackgroundResource(R.drawable.btn_chip_unselected);
            ChipView.this.mInputText.setBackgroundResource(0);
            ChipView.this.mTranslatedText.setBackgroundResource(0);
            ChipView.this.mChip.setAddStatesFromChildren(true);
            ChipView.this.mInputTextChip.setAddStatesFromChildren(true);
            ChipView.this.mTranslationChip.setAddStatesFromChildren(true);
            ChipView.this.mFromLanguageText.setVisibility(8);
            ChipView.this.mToLanguageText.setVisibility(8);
            ChipView.this.mChipDivider.setVisibility(8);
            ChipView.this.mSrcTtsWrapper.setVisibility(8);
            ChipView.this.mTrgTtsWrapper.setVisibility(8);
            ChipView.this.mLittleStarBtn1.setVisibility(8);
            ChipView.this.mLittleStarBtn2.setVisibility(0);
            ChipView.this.mInputText.setSingleLine(true);
            ChipView.this.mTranslatedText.setSingleLine(true);
            ChipView.this.mInputText.setEllipsize(TextUtils.TruncateAt.END);
            ChipView.this.mTranslatedText.setEllipsize(TextUtils.TruncateAt.END);
            ChipView.this.mTranslatedText.setTextAppearance(ChipView.this.mActivity, R.style.chip_unselected_translation);
            ChipView.this.mOutputView.hideTranslateResultView();
            ChipView.this.mSrcTransliteration.setVisibility(8);
            ChipView.this.mTrgTransliteration.setVisibility(8);
            ChipView.this.mChip.setVisibility(0);
            Logger.d("ChipView", "mIsExpanded " + ChipView.this.mIsExpanded + " ==> false");
            ChipView.access$2102(ChipView.this, false);
          }
        case 2:
        }
        Logger.d("ChipView", "renderOutputView SELECTED_EXPANDED");
        ChipView.this.initializeOutputPanelView();
        ChipView.this.setSpeakerIcons();
        ChipView.this.mChip.setBackgroundResource(R.drawable.chip_color_selected_normal);
        ChipView.this.mInputText.setBackgroundResource(R.drawable.btn_chip_selected);
        ChipView.this.mTranslatedText.setBackgroundResource(R.drawable.btn_chip_selected);
        ChipView.this.mChip.setAddStatesFromChildren(false);
        ChipView.this.mInputTextChip.setAddStatesFromChildren(false);
        ChipView.this.mTranslationChip.setAddStatesFromChildren(false);
        ChipView.this.mFromLanguageText.setVisibility(0);
        ChipView.this.mToLanguageText.setVisibility(0);
        ChipView.this.mChipDivider.setVisibility(0);
        ChipView.this.mSrcTtsWrapper.setVisibility(0);
        ChipView.this.mTrgTtsWrapper.setVisibility(0);
        ChipView.this.mLittleStarBtn2.setVisibility(8);
        ChipView.this.mInputText.setSingleLine(false);
        ChipView.this.mTranslatedText.setSingleLine(false);
        ChipView.this.mInputText.setEllipsize(null);
        ChipView.this.mTranslatedText.setEllipsize(null);
        ChipView.this.mTranslatedText.setTextAppearance(ChipView.this.mActivity, R.style.chip_selected_translation);
        TextView localTextView1 = ChipView.this.mSrcTransliteration;
        int i;
        label619: int j;
        if (ChipView.this.mSrcTransliteration.getText().length() == 0)
        {
          i = 8;
          localTextView1.setVisibility(i);
          TextView localTextView2 = ChipView.this.mTrgTransliteration;
          if (ChipView.this.mTrgTransliteration.getText().length() != 0)
            break label758;
          j = 8;
          label654: localTextView2.setVisibility(j);
          ChipView.this.mChip.setVisibility(0);
          if (!TextUtils.isEmpty(ChipView.this.getTranslationText()))
            break label764;
          ChipView.this.mLittleStarBtn1.setVisibility(8);
          ChipView.this.mOutputView.hideTranslateResultView();
        }
        while (true)
        {
          Logger.d("ChipView", "mIsExpanded " + ChipView.this.mIsExpanded + " ==> true");
          ChipView.access$2102(ChipView.this, true);
          break;
          i = 0;
          break label619;
          label758: j = 0;
          break label654;
          label764: ChipView.this.mLittleStarBtn1.setVisibility(0);
          ChipView.this.mOutputView.showTranslateResultView();
        }
      }
    });
  }

  private void selectAndOpenContextMenu()
  {
    if (this.mIsExpanded)
    {
      if (UPPER_CHIP_RESOURCE_ID_LIST.contains(Integer.valueOf(this.mTouchEventViewId)))
      {
        this.mInputTextChip.setSelected(true);
        this.mActivity.openContextMenu(this.mInputTextChip);
      }
      while ((!LOWER_CHIP_RESOURCE_ID_LIST.contains(Integer.valueOf(this.mTouchEventViewId))) || (TextUtils.isEmpty(getTranslationText())))
        return;
      this.mTranslationChip.setSelected(true);
      this.mActivity.openContextMenu(this.mTranslationChip);
      return;
    }
    setSelected(true);
    this.mActivity.openContextMenu(this);
  }

  private void selectChip(final int paramInt)
  {
    Logger.d("ChipView", "selectChip position=" + paramInt);
    if (this.mListAdapter == null)
    {
      if (SUGGEST_CHIP_RESOURCE_ID_LIST.contains(Integer.valueOf(this.mTouchEventViewId)))
        if (this.mSpellCorrectionEntry != null)
        {
          SuggestAdapter.logSuggestionClick(this.mActivity, this.mSpellCorrectionEntry, false);
          this.mInputPanel.copyToInputTextBox(this.mSpellCorrectionEntry.getEntry().getInputText());
        }
      label86: 
      do
      {
        do
        {
          do
          {
            break label86;
            do
              return;
            while (this.mSrcLangDetectEntry == null);
            this.mFromLanguage = this.mLanguageList.getFromLanguageByShortName(this.mSrcLangDetectEntry.getEntry().getInputText());
            SuggestAdapter.logSuggestionClick(this.mActivity, this.mSrcLangDetectEntry, false);
            this.mInputPanel.onSourceLanguageChangeRequested(this.mFromLanguage, this.mSrcLangDetectEntry.getSourceText());
            return;
            if (!UPPER_CHIP_RESOURCE_ID_LIST.contains(Integer.valueOf(this.mTouchEventViewId)))
              break;
            Logger.d("ChipView", "selectChip TOUCHTOUCH SRC");
          }
          while (this.mInputPanel == null);
          UserActivityMgr.get().setTranslationSource(UserActivityMgr.RequestSource.CHIPVIEW_SRCTEXT);
          this.mInputPanel.copyToInputTextBox(ChipPart.INPUT_TEXT);
          return;
        }
        while (!LOWER_CHIP_RESOURCE_ID_LIST.contains(Integer.valueOf(this.mTouchEventViewId)));
        Logger.d("ChipView", "selectChip TOUCHTOUCH TRG");
      }
      while (this.mInputPanel == null);
      UserActivityMgr.get().setTranslationSource(UserActivityMgr.RequestSource.CHIPVIEW_TRGTEXT);
      this.mInputPanel.copyToInputTextBox(ChipPart.TRANSLATION_TEXT);
      return;
    }
    if (this.mListAdapter.getSelectedHistoryItemPosition() == paramInt)
    {
      this.mListAdapter.setSelectedHistoryItemPosition(-1);
      this.mListAdapter.setSelectedHistoryItem(null);
      this.mListAdapter.notifyDataSetChanged();
      this.mInputTextChip.setSelected(false);
      this.mTranslationChip.setSelected(false);
      setSelected(false);
      return;
    }
    if (this.mListAdapter.isHistoryMode())
      UserActivityMgr.get().incrementIntervalCount(UserActivityMgr.IntervalCountTag.HISTORY_VIEW_ITEM_EXPANSIONS, 1);
    while (true)
    {
      if (this.mListAdapter.getSelectedHistoryItem() != null)
      {
        this.mListAdapter.setSelectedHistoryItemPosition(-1);
        this.mListAdapter.setSelectedHistoryItem(null);
        this.mListAdapter.notifyDataSetChanged();
      }
      if (!this.mAnimateScroll)
        break;
      postDelayed(new Runnable()
      {
        public void run()
        {
          ChipView.this.mListAdapter.setSelectedHistoryItem(ChipView.this);
          ChipView.this.renderOutputView(ChipView.ViewState.SELECTED_EXPANDED);
          ChipView.this.postDelayed(new Runnable()
          {
            public void run()
            {
              ChipView.this.mListAdapter.setSelectedHistoryItemPosition(ChipView.4.this.val$position);
              ChipView.this.mListView.smoothScrollBy(0 + ChipView.this.getTop(), 300);
              ChipView.this.postDelayed(new Runnable()
              {
                public void run()
                {
                  ChipView.this.mListView.setSelection(ChipView.4.this.val$position);
                  ChipView.this.mListAdapter.notifyDataSetChanged();
                }
              }
              , 350L);
            }
          }
          , 50L);
        }
      }
      , 100L);
      return;
      UserActivityMgr.get().incrementIntervalCount(UserActivityMgr.IntervalCountTag.FAVORITES_VIEW_ITEM_EXPANSIONS, 1);
    }
    this.mActivity.runOnUiThread(new Runnable()
    {
      public void run()
      {
        ChipView.this.mListAdapter.setSelectedHistoryItem(ChipView.this);
        ChipView.this.renderOutputView(ChipView.ViewState.SELECTED_EXPANDED);
        ChipView.this.mListAdapter.setSelectedHistoryItemPosition(paramInt);
        ChipView.this.mListAdapter.notifyDataSetChanged();
        ChipView.this.mListView.setSelection(paramInt);
      }
    });
  }

  private void setSpeakerIcons()
  {
    if (this.mCurrentTranslation == null)
    {
      Logger.d("no translation or no result, do not set speaker icon");
      return;
    }
    final boolean bool1 = canSpeak(this.mCurrentTranslation.fromLanguage.getShortName());
    if ((!TextUtils.isEmpty(getTranslationText())) && (canSpeak(this.mCurrentTranslation.toLanguage.getShortName())));
    for (final boolean bool2 = true; ; bool2 = false)
    {
      this.mActivity.runOnUiThread(new Runnable()
      {
        public void run()
        {
          View localView1 = ChipView.this.mSrcTts;
          int i;
          View localView2;
          int j;
          if (bool1)
          {
            i = 0;
            localView1.setVisibility(i);
            localView2 = ChipView.this.mTrgTts;
            boolean bool = bool2;
            j = 0;
            if (!bool)
              break label57;
          }
          while (true)
          {
            localView2.setVisibility(j);
            return;
            i = 8;
            break;
            label57: j = 8;
          }
        }
      });
      return;
    }
  }

  private void setStar(boolean paramBoolean)
  {
    Logger.d("ChipView", "setStar");
    ImageButton localImageButton1 = this.mLittleStarBtn1;
    int i;
    ImageButton localImageButton2;
    if (paramBoolean)
    {
      i = R.drawable.ic_star_active;
      localImageButton1.setImageResource(i);
      localImageButton2 = this.mLittleStarBtn2;
      if (!paramBoolean)
        break label56;
    }
    label56: for (int j = R.drawable.ic_star_active; ; j = R.drawable.ic_star_inactive)
    {
      localImageButton2.setImageResource(j);
      return;
      i = R.drawable.ic_star_inactive;
      break;
    }
  }

  private void setTransliteration(TextView paramTextView, String paramString)
  {
    if (TextUtils.isEmpty(paramString))
    {
      paramTextView.setText("");
      return;
    }
    paramTextView.setText(paramString, TextView.BufferType.SPANNABLE);
    ((Spannable)paramTextView.getText()).setSpan(PHONETIC_SPAN, 0, paramTextView.getText().length(), 33);
  }

  public OutputPanelView getOutputView()
  {
    return this.mOutputView;
  }

  public ChipPart getSelectedChipPart()
  {
    if (this.mIsExpanded)
    {
      if (UPPER_CHIP_RESOURCE_ID_LIST.contains(Integer.valueOf(this.mTouchEventViewId)))
        return ChipPart.INPUT_TEXT;
      if (LOWER_CHIP_RESOURCE_ID_LIST.contains(Integer.valueOf(this.mTouchEventViewId)))
        return ChipPart.TRANSLATION_TEXT;
      if (SUGGEST_CHIP_RESOURCE_ID_LIST.contains(Integer.valueOf(this.mTouchEventViewId)))
        return ChipPart.SUGGESTION;
    }
    return ChipPart.ENTIRE_CHIP;
  }

  public int getSelectedViewId()
  {
    return this.mTouchEventViewId;
  }

  public void initParameters(Activity paramActivity, ListView paramListView, HistoryListAdapter paramHistoryListAdapter, TranslateManager paramTranslateManager, Language paramLanguage1, Language paramLanguage2, InputPanel paramInputPanel, boolean paramBoolean)
  {
    this.mActivity = paramActivity;
    this.mListView = paramListView;
    this.mListAdapter = paramHistoryListAdapter;
    this.mTranslateManager = paramTranslateManager;
    this.mFromLanguage = paramLanguage1;
    this.mToLanguage = paramLanguage2;
    this.mApp = ((TranslateApplication)this.mActivity.getApplication());
    this.mOutputView = ((OutputPanelView)findViewById(R.id.panel_output));
    this.mOutputView.setVisibility(8);
    this.mChip = ((LinearLayout)findViewById(R.id.mvh_chip));
    this.mInputTextChip = ((LinearLayout)findViewById(R.id.mvh_input_text_wrapper));
    this.mTranslationChip = ((LinearLayout)findViewById(R.id.mvh_translation_text_wrapper));
    this.mInputPanel = paramInputPanel;
    this.mLanguageList = Util.getLanguageListFromProfile(this.mActivity);
    this.mChipDivider = findViewById(R.id.chip_divider);
    this.mDictDivider = findViewById(R.id.dictionary_divider);
    this.mInputText = ((TextView)findViewById(R.id.mvh_input_text));
    this.mTranslatedText = ((TextView)findViewById(R.id.mvh_translated_text));
    this.mFromLanguageText = ((TextView)findViewById(R.id.mvh_my_lang));
    this.mToLanguageText = ((TextView)findViewById(R.id.mvh_their_lang));
    this.mSrcTransliteration = ((TextView)findViewById(R.id.mvh_src_transliteration));
    this.mTrgTransliteration = ((TextView)findViewById(R.id.mvh_trg_transliteration));
    this.mSuggestTypeView = ((TextView)findViewById(R.id.mvh_suggest_type));
    this.mSuggestView = ((TextView)findViewById(R.id.mvh_suggest_text));
    this.mSuggestTypeView.setVisibility(8);
    this.mSuggestView.setVisibility(8);
    if (this.mInputPanel != null)
    {
      this.mInstantTranslateHandler = this.mApp.getInstantTranslateHandler();
      this.mInstantTranslateHandler.init(this.mActivity, this, this.mFromLanguage, this.mToLanguage);
    }
    Iterator localIterator1 = UPPER_CHIP_RESOURCE_ID_LIST.iterator();
    while (localIterator1.hasNext())
    {
      int m = ((Integer)localIterator1.next()).intValue();
      findViewById(m).setOnLongClickListener(this);
      findViewById(m).setOnClickListener(this);
    }
    Iterator localIterator2 = SUGGEST_CHIP_RESOURCE_ID_LIST.iterator();
    while (localIterator2.hasNext())
    {
      int k = ((Integer)localIterator2.next()).intValue();
      findViewById(k).setOnLongClickListener(this);
      findViewById(k).setOnClickListener(this);
    }
    Iterator localIterator3 = LOWER_CHIP_RESOURCE_ID_LIST.iterator();
    while (localIterator3.hasNext())
    {
      int j = ((Integer)localIterator3.next()).intValue();
      findViewById(j).setOnLongClickListener(this);
      findViewById(j).setOnClickListener(this);
    }
    Iterator localIterator4 = PADDING_CHIP_RESOURCE_ID_LIST.iterator();
    while (localIterator4.hasNext())
    {
      int i = ((Integer)localIterator4.next()).intValue();
      findViewById(i).setOnLongClickListener(this);
      findViewById(i).setOnClickListener(this);
    }
    this.mChip.setOnLongClickListener(this);
    this.mChip.setOnClickListener(this);
    this.mAnimateScroll = paramBoolean;
    this.mLittleStarBtn1 = ((ImageButton)findViewById(R.id.btn_little_star_button1));
    this.mLittleStarBtn2 = ((ImageButton)findViewById(R.id.btn_little_star_button2));
    this.mLittleStarBtn1.setOnClickListener(new StarOnClickListener(this.mLittleStarBtn1));
    this.mLittleStarBtn2.setOnClickListener(new StarOnClickListener(this.mLittleStarBtn2));
    setVisibility(8);
    this.mSrcTtsWrapper = findViewById(R.id.frame_src_tts_speak);
    this.mTrgTtsWrapper = findViewById(R.id.frame_trg_tts_speak);
    this.mSrcTts = this.mSrcTtsWrapper.findViewById(R.id.frame_tts_speak);
    this.mTrgTts = this.mTrgTtsWrapper.findViewById(R.id.frame_tts_speak);
    this.mSrcTts.findViewById(R.id.btn_translate_tts_speak).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        if ((ChipView.this.mTts != null) && (ChipView.this.mCurrentTranslation != null) && (ChipView.this.mSrcTts.getVisibility() == 0))
        {
          ChipView.this.mTts.speak(ChipView.this.mActivity, ChipView.this.mCurrentTranslation.fromLanguage, ChipView.this.mCurrentTranslation.inputText, new ChipView.NetworkTtsCallback(ChipView.this, ChipView.this.mSrcTts));
          UserActivityMgr.get().incrementIntervalCount(UserActivityMgr.IntervalCountTag.CHIPVIEW_SRC_TTS_CLICKS, 1);
        }
      }
    });
    this.mTrgTts.findViewById(R.id.btn_translate_tts_speak).setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        if ((ChipView.this.mTts != null) && (ChipView.this.mCurrentTranslation != null) && (ChipView.this.mTrgTts.getVisibility() == 0))
        {
          ChipView.this.mTts.speak(ChipView.this.mActivity, ChipView.this.mCurrentTranslation.toLanguage, ChipView.this.getTranslationText().toString(), new ChipView.NetworkTtsCallback(ChipView.this, ChipView.this.mTrgTts));
          UserActivityMgr.get().incrementIntervalCount(UserActivityMgr.IntervalCountTag.CHIPVIEW_TRG_TTS_CLICKS, 1);
        }
      }
    });
    this.mTts = this.mApp.getMyTts();
    Drawable localDrawable = getResources().getDrawable(R.drawable.tts_speak);
    this.mTtsSpinningWheelWidth = localDrawable.getIntrinsicWidth();
    this.mTtsSpinningWheelHeight = localDrawable.getIntrinsicHeight();
  }

  public void onClick(View paramView)
  {
    Logger.d("ChipView", "onClick");
    this.mTouchEventViewId = paramView.getId();
    selectChip(getPosition());
  }

  public void onDestroy()
  {
    Logger.d("ChipView", "onDestroy");
    if (this.mOutputView != null)
      this.mOutputView.onDestroy();
  }

  public void onFavoriteChanged(TranslateEntry paramTranslateEntry, boolean paramBoolean)
  {
    if (this.mInputPanel != null)
      this.mInputPanel.onFavoriteChanged(paramTranslateEntry, paramBoolean);
  }

  public boolean onLongClick(View paramView)
  {
    Logger.d("ChipView", "onLongClick");
    this.mTouchEventViewId = paramView.getId();
    selectAndOpenContextMenu();
    return true;
  }

  public void onLongPause(SpannableStringBuilder paramSpannableStringBuilder1, SpannableStringBuilder paramSpannableStringBuilder2, SpannableStringBuilder paramSpannableStringBuilder3, String paramString)
  {
  }

  public void onPause()
  {
    if (this.mOutputView != null)
      this.mOutputView.onPause();
  }

  public void onSourceTextDone(SpannableStringBuilder paramSpannableStringBuilder)
  {
  }

  public void onSourceTextUpdate(SpannableStringBuilder paramSpannableStringBuilder)
  {
  }

  public void onTargetTextDone(SpannableStringBuilder paramSpannableStringBuilder)
  {
  }

  public void onTargetTextUpdate(SpannableStringBuilder paramSpannableStringBuilder1, SpannableStringBuilder paramSpannableStringBuilder2, SpannableStringBuilder paramSpannableStringBuilder3, String paramString)
  {
    boolean bool1 = true;
    if ((!TextUtils.isEmpty(paramSpannableStringBuilder2)) && (!TextUtils.equals(paramSpannableStringBuilder2, this.mTranslatedText.getText())))
      return;
    boolean bool2 = false;
    this.mSpellCorrectionEntry = SuggestAdapter.getSpellCorrectionSuggestEntry(this.mFromLanguage, this.mToLanguage, paramSpannableStringBuilder3, paramSpannableStringBuilder1.toString());
    boolean bool3;
    SuggestAdapter.SuggestEntry localSuggestEntry1;
    if (this.mSpellCorrectionEntry == null)
    {
      this.mSrcLangDetectEntry = SuggestAdapter.getDetectedLanguageSuggestEntry(this.mLanguageList, this.mFromLanguage, this.mToLanguage, paramString, paramSpannableStringBuilder1.toString());
      SuggestAdapter.SuggestEntry localSuggestEntry2 = this.mSrcLangDetectEntry;
      bool3 = false;
      if (localSuggestEntry2 != null)
        bool3 = true;
      localSuggestEntry1 = this.mSrcLangDetectEntry;
    }
    while (true)
    {
      UserActivityMgr.get().setImpression(UserActivityMgr.IntervalCountTag.SPELL_CORRECTION_SHOWN_ON_CHIP_VIEW, bool2);
      UserActivityMgr.get().setImpression(UserActivityMgr.IntervalCountTag.LANGID_SHOWN_ON_CHIP_VIEW, bool3);
      if (localSuggestEntry1 != null)
        break;
      this.mSuggestTypeView.setVisibility(8);
      this.mSuggestView.setVisibility(8);
      return;
      bool2 = true;
      localSuggestEntry1 = this.mSpellCorrectionEntry;
      bool3 = false;
    }
    String str1;
    TextView localTextView;
    String str2;
    label255: Language[] arrayOfLanguage;
    Constants.AppearanceType localAppearanceType;
    switch (7.$SwitchMap$com$google$android$apps$translate$editor$SuggestAdapter$SuggestType[localSuggestEntry1.getType().ordinal()])
    {
    default:
      this.mSuggestTypeView.setVisibility(0);
      str1 = localSuggestEntry1.getHtmlInputText() + "   ";
      localTextView = this.mSuggestView;
      if (str1 == null)
      {
        str2 = localSuggestEntry1.getEntry().getInputText();
        arrayOfLanguage = new Language[2];
        arrayOfLanguage[0] = this.mFromLanguage;
        arrayOfLanguage[bool1] = this.mToLanguage;
        localAppearanceType = Constants.AppearanceType.UNCHANGED;
        if (str1 == null)
          break label357;
      }
      break;
    case 1:
    case 2:
    }
    while (true)
    {
      Util.setTextAndFont(localTextView, str2, arrayOfLanguage, localAppearanceType, bool1);
      this.mSuggestView.setVisibility(0);
      return;
      this.mSuggestTypeView.setText(this.mActivity.getString(R.string.label_translate_from));
      break;
      this.mSuggestTypeView.setText(this.mActivity.getString(R.string.msg_did_you_mean));
      break;
      str2 = str1;
      break label255;
      label357: bool1 = false;
    }
  }

  public void onTranslationDone(TranslateEntry paramTranslateEntry, boolean paramBoolean)
  {
    if (this.mInputPanel != null)
      this.mInputPanel.onTranslationDone(paramTranslateEntry, paramBoolean);
  }

  public void onTranslationFailed(Language paramLanguage1, Language paramLanguage2, String paramString)
  {
    if (this.mInputPanel != null)
      this.mInputPanel.onTranslationFailed(paramLanguage1, paramLanguage2, paramString);
  }

  public void render(HistoryEntry paramHistoryEntry)
  {
    if (paramHistoryEntry != null)
    {
      setVisibility(0);
      render(paramHistoryEntry, 0, 1);
      return;
    }
    setVisibility(8);
  }

  public void render(HistoryEntry paramHistoryEntry, int paramInt1, int paramInt2)
  {
    int i = 8;
    Logger.d("ChipView", "render");
    Entry localEntry = paramHistoryEntry.entry;
    if ((paramHistoryEntry.isNullEntry()) || (localEntry == null))
    {
      Logger.d("ChipView", "render NULL position=" + paramInt1);
      this.mFromLanguageText.setText("");
      this.mToLanguageText.setText("");
      this.mSrcTransliteration.setText("");
      this.mTrgTransliteration.setText("");
      setVisibility(i);
      return;
    }
    if (getVisibility() == i)
      setVisibility(0);
    if ((this.mItemEntry != null) && (paramHistoryEntry != null) && (this.mItemEntry.entry.getDbKey().equals(paramHistoryEntry.entry.getDbKey())) && (this.mItemEntry.isFavorite == paramHistoryEntry.isFavorite))
    {
      Logger.d("ChipView", "render REUSE position=" + paramInt1);
      this.mItemEntry = new HistoryEntry(paramHistoryEntry);
      if (this.mListAdapter == null)
        renderOutputView(ViewState.SELECTED_EXPANDED);
    }
    else
    {
      Logger.d("ChipView", "render NEW position=" + paramInt1);
      this.mItemEntry = new HistoryEntry(paramHistoryEntry);
      this.mFromLanguage = this.mLanguageList.getFromLanguageByShortName(localEntry.getFromLanguageShortName());
      this.mToLanguage = this.mLanguageList.getToLanguageByShortName(localEntry.getToLanguageShortName());
      if (this.mInstantTranslateHandler != null)
        this.mInstantTranslateHandler.init(this.mActivity, this, this.mFromLanguage, this.mToLanguage);
      TextView localTextView1 = this.mInputText;
      String str1 = localEntry.getInputText();
      Language[] arrayOfLanguage1 = new Language[1];
      arrayOfLanguage1[0] = this.mFromLanguage;
      Util.setTextAndFont(localTextView1, str1, arrayOfLanguage1, Constants.AppearanceType.UNCHANGED, false);
      this.mSuggestTypeView.setVisibility(i);
      this.mSuggestView.setVisibility(i);
      if (this.mInstantTranslateHandler != null)
      {
        this.mSpellCorrectionEntry = null;
        this.mSrcLangDetectEntry = null;
        this.mInstantTranslateHandler.updateSourceText(this.mInputText.getText());
      }
      Translate.TranslateResult localTranslateResult = new Translate.TranslateResult(localEntry);
      TextView localTextView2 = this.mTranslatedText;
      String str2 = localTranslateResult.getTranslateText();
      Language[] arrayOfLanguage2 = new Language[2];
      arrayOfLanguage2[0] = this.mFromLanguage;
      arrayOfLanguage2[1] = this.mToLanguage;
      Util.setTextAndFont(localTextView2, str2, arrayOfLanguage2, Constants.AppearanceType.UNCHANGED, false);
      this.mFromLanguageText.setText(this.mFromLanguage.getLongName().toUpperCase());
      this.mToLanguageText.setText(this.mToLanguage.getLongName().toUpperCase());
      setTransliteration(this.mSrcTransliteration, localTranslateResult.getSrcTransliteration());
      setTransliteration(this.mTrgTransliteration, localTranslateResult.getTrgTransliteration());
      View localView = this.mDictDivider;
      if (TextUtils.isEmpty(localTranslateResult.getDictionaryResult()));
      while (true)
      {
        localView.setVisibility(i);
        this.mCurrentTranslation = new TranslateEntry(this.mFromLanguage, this.mToLanguage, localEntry.getInputText(), localEntry.getOutputText(), false);
        setStar(this.mItemEntry.isFavorite);
        break;
        i = 0;
      }
    }
    if (this.mListAdapter.getSelectedHistoryItemPosition() == paramInt1);
    for (ViewState localViewState = ViewState.SELECTED_EXPANDED; ; localViewState = ViewState.UNSELECTED)
    {
      renderOutputView(localViewState);
      return;
    }
  }

  public static enum ChipPart
  {
    static
    {
      ENTIRE_CHIP = new ChipPart("ENTIRE_CHIP", 1);
      INPUT_TEXT = new ChipPart("INPUT_TEXT", 2);
      TRANSLATION_TEXT = new ChipPart("TRANSLATION_TEXT", 3);
      SUGGESTION = new ChipPart("SUGGESTION", 4);
      ChipPart[] arrayOfChipPart = new ChipPart[5];
      arrayOfChipPart[0] = NONE;
      arrayOfChipPart[1] = ENTIRE_CHIP;
      arrayOfChipPart[2] = INPUT_TEXT;
      arrayOfChipPart[3] = TRANSLATION_TEXT;
      arrayOfChipPart[4] = SUGGESTION;
    }
  }

  public static class ChipViewTag
  {
    public int position;
  }

  private class NetworkTtsCallback
    implements NetworkTts.Callback
  {
    private View mTriggeringSpeaker;
    private View mTtsIconView;

    public NetworkTtsCallback(View arg2)
    {
      Object localObject;
      this.mTtsIconView = localObject;
      this.mTriggeringSpeaker = this.mTtsIconView.findViewById(R.id.btn_translate_tts_speak);
    }

    private void hideTtsLoading(final int paramInt)
    {
      ChipView.this.mActivity.runOnUiThread(new Runnable()
      {
        public void run()
        {
          ChipView.NetworkTtsCallback.this.mTtsIconView.findViewById(paramInt).setVisibility(4);
          ChipView.NetworkTtsCallback.this.mTriggeringSpeaker.setVisibility(0);
        }
      });
    }

    private void showTtsLoading(final int paramInt)
    {
      ChipView.this.mActivity.runOnUiThread(new Runnable()
      {
        public void run()
        {
          ProgressBar localProgressBar = (ProgressBar)ChipView.NetworkTtsCallback.this.mTtsIconView.findViewById(paramInt);
          localProgressBar.getLayoutParams().width = ChipView.this.mTtsSpinningWheelWidth;
          localProgressBar.getLayoutParams().height = ChipView.this.mTtsSpinningWheelHeight;
          localProgressBar.setVisibility(0);
          ChipView.NetworkTtsCallback.this.mTriggeringSpeaker.setVisibility(4);
        }
      });
    }

    public void onError(int paramInt)
    {
      hideTtsLoading(R.id.translate_tts_loading);
      Util.showNetworkTtsError(ChipView.this.mActivity, paramInt);
    }

    public void onPrepare()
    {
      showTtsLoading(R.id.translate_tts_loading);
    }

    public void onReady()
    {
      hideTtsLoading(R.id.translate_tts_loading);
    }
  }

  private class StarOnClickListener
    implements View.OnClickListener
  {
    private View mStarBtn;

    StarOnClickListener(View arg2)
    {
      Object localObject;
      this.mStarBtn = localObject;
    }

    public void onClick(View paramView)
    {
      int i = 1;
      if ((paramView.getId() != this.mStarBtn.getId()) || (ChipView.this.mItemEntry == null))
        return;
      HistoryEntry localHistoryEntry = ChipView.this.mItemEntry;
      int j;
      label94: UserActivityMgr localUserActivityMgr;
      UserActivityMgr.IntervalCountTag localIntervalCountTag;
      if (!ChipView.this.mItemEntry.isFavorite)
      {
        j = i;
        localHistoryEntry.isFavorite = j;
        if (ChipView.this.mListAdapter == null)
          break label150;
        ChipView.this.mListAdapter.starTranslateEntry(ChipView.this.mItemEntry.isFavorite, ChipView.this.getPosition());
        localUserActivityMgr = UserActivityMgr.get();
        localIntervalCountTag = UserActivityMgr.IntervalCountTag.STARS_MINUS_UNSTARS;
        if (!ChipView.this.mItemEntry.isFavorite)
          break label183;
      }
      while (true)
      {
        localUserActivityMgr.incrementIntervalCount(localIntervalCountTag, i);
        ChipView.this.setStar(ChipView.this.mItemEntry.isFavorite);
        return;
        j = 0;
        break;
        label150: Util.toggleStarredTranslation(ChipView.this.mActivity, ChipView.this.mItemEntry.isFavorite, ChipView.this.mItemEntry.entry);
        break label94;
        label183: i = -1;
      }
    }
  }

  private static enum ViewState
  {
    static
    {
      SELECTED_EXPANDED = new ViewState("SELECTED_EXPANDED", 1);
      ViewState[] arrayOfViewState = new ViewState[2];
      arrayOfViewState[0] = UNSELECTED;
      arrayOfViewState[1] = SELECTED_EXPANDED;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.translation.ChipView
 * JD-Core Version:    0.6.2
 */