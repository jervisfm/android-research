package com.google.android.apps.translate.translation;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.text.Editable;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView.BufferType;
import com.google.android.apps.translate.Constants.AppearanceType;
import com.google.android.apps.translate.Language;
import com.google.android.apps.translate.LanguagePicker;
import com.google.android.apps.translate.LanguagePicker.OnLanguagePairSelectedListener;
import com.google.android.apps.translate.Languages;
import com.google.android.apps.translate.Logger;
import com.google.android.apps.translate.Preconditions;
import com.google.android.apps.translate.R.id;
import com.google.android.apps.translate.R.string;
import com.google.android.apps.translate.SdkVersionWrapper;
import com.google.android.apps.translate.SdkVersionWrapper.WrapperBase;
import com.google.android.apps.translate.TranslateApplication;
import com.google.android.apps.translate.TranslateManager;
import com.google.android.apps.translate.UserActivityMgr;
import com.google.android.apps.translate.UserActivityMgr.InputMethod;
import com.google.android.apps.translate.UserActivityMgr.RequestSource;
import com.google.android.apps.translate.Util;
import com.google.android.apps.translate.VoiceInputHelper;
import com.google.android.apps.translate.asreditor.AsrResultEditor;
import com.google.android.apps.translate.editor.EditPanelView;
import com.google.android.apps.translate.editor.EditPanelView.Callback;
import com.google.android.apps.translate.editor.InputMethodView;
import com.google.android.apps.translate.editor.InputMethodView.InputMethod;
import com.google.android.apps.translate.editor.InputMethodView.InputMethodEvent;
import com.google.android.apps.translate.editor.PreImeAutoCompleteTextView.OnKeyPreImeListener;
import com.google.android.apps.translate.editor.TextSlot;
import com.google.android.apps.translate.history.Entry;
import com.google.android.apps.translate.history.HistoryEntry;
import com.google.android.apps.translate.home.TitleView;

public class InputPanel
  implements View.OnClickListener, TextWatcher, View.OnTouchListener, LanguagePicker.OnLanguagePairSelectedListener, OutputPanelView.OutputPanelViewCallback, PreImeAutoCompleteTextView.OnKeyPreImeListener, EditPanelView.Callback
{
  private static final boolean ALWAYS_SHOW_INTRO = true;
  private static final boolean CLEAR_OR_APPEND_TEXT_AFTER_TRANSLATION = false;
  private static final boolean CLEAR_TEXT_AFTER_TRANSLATION = false;
  private static final boolean ENABLE_CHINESE_AUTO_SWAP = false;
  private static final boolean ENABLE_LOCAL_AUTO_SWAP = false;
  private static final boolean SELECT_TEXT_AFTER_TRANSLATION = true;
  private static final String TAG = "InputPanel";
  private Activity mActivity;
  private AsrResultEditor mAsrResultEditor;
  private ImageButton mBtnClearInput;
  private ChipView mChipView;
  private View mChipWrapper;
  private EditPanelView mConfirmView;
  private InputMethodView mControlPanel;
  private View mControlPanelWrapper;
  private boolean mEnableLanguageDetector = true;
  private Language mFromLanguage;
  private TextSlot mInputEditText;
  private View mIntroMessageView;
  private Languages mLanguageList;
  private View mLanguagePanel;
  private LanguagePicker mLanguagePicker;
  private ListView mListView;
  private OutputPanelView mOutputView;
  private View mPanelView;
  private int mPrevTextLength = 0;
  private int mSelectionEnd;
  private boolean mSelectionSaved = false;
  private int mSelectionStart;
  private String mSelectionText;
  private boolean mShowIntro = false;
  private TitleView mTitleView;
  private Language mToLanguage;
  private TranslateHelper mTranslateHelper;
  private TranslateManager mTranslateManager;
  private VoiceInputHelper mVoiceInputHelper;

  public InputPanel(Activity paramActivity, Languages paramLanguages, View paramView, TranslateHelper paramTranslateHelper)
  {
    this.mActivity = paramActivity;
    this.mLanguageList = paramLanguages;
    this.mPanelView = paramView.findViewById(R.id.panel_input);
    this.mTranslateHelper = paramTranslateHelper;
    Preconditions.checkNotNull(this.mLanguageList);
    if (Util.isHoneycombCompatible())
    {
      this.mTitleView = ((TitleView)paramView.getRootView().findViewById(R.id.fragments_translate_title_bar));
      ((TitleView)this.mPanelView.findViewById(R.id.translate_title_bar)).setVisibility(8);
      if ((this.mTitleView != null) || (!(this.mActivity instanceof TranslateActivity)));
    }
    for (this.mTitleView = ((TitleView)this.mPanelView.findViewById(R.id.translate_title_bar)); ; this.mTitleView = ((TitleView)this.mPanelView.findViewById(R.id.translate_title_bar)))
    {
      this.mLanguagePanel = this.mPanelView.findViewById(R.id.language_selection_panel);
      this.mTitleView.setLanguagePanel(this.mLanguagePanel);
      this.mIntroMessageView = this.mPanelView.findViewById(R.id.input_panel_intro);
      this.mChipWrapper = this.mPanelView.findViewById(R.id.history_list_wrapper);
      TranslateApplication localTranslateApplication = (TranslateApplication)this.mActivity.getApplication();
      this.mTranslateManager = localTranslateApplication.getTranslateManager();
      this.mChipView = ((ChipView)this.mPanelView.findViewById(R.id.input_panel_chip_view));
      this.mVoiceInputHelper = localTranslateApplication.getVoiceInputHelper();
      this.mListView = ((ListView)this.mPanelView.findViewById(R.id.suggest_list));
      this.mControlPanelWrapper = paramView.getRootView().findViewById(R.id.input_method_view_wrapper);
      this.mControlPanel = ((InputMethodView)this.mControlPanelWrapper.findViewById(R.id.control_panel));
      this.mConfirmView = ((EditPanelView)this.mPanelView.findViewById(R.id.input_panel_input_view));
      this.mConfirmView.startInternalEdit();
      this.mConfirmView.setCallback(this);
      this.mConfirmView.setInputMethodView(this.mControlPanel);
      this.mConfirmView.setListView(this.mListView);
      this.mAsrResultEditor = ((AsrResultEditor)this.mConfirmView.findViewById(R.id.msg_confirm_content));
      this.mInputEditText = this.mAsrResultEditor.getEditorField();
      this.mInputEditText.setTouchEventCallback(this.mConfirmView);
      this.mBtnClearInput = ((ImageButton)this.mConfirmView.findViewById(R.id.btn_confirm_view_clear));
      this.mPanelView.findViewById(R.id.btn_swap).setOnClickListener(this);
      this.mBtnClearInput.setOnClickListener(this);
      Spinner localSpinner1 = (Spinner)this.mPanelView.findViewById(R.id.spinner_my_lang);
      Spinner localSpinner2 = (Spinner)this.mPanelView.findViewById(R.id.spinner_their_lang);
      this.mLanguagePicker = new LanguagePicker(this.mActivity, localSpinner1, localSpinner2, this);
      this.mLanguagePicker.setupLanguageSpinners();
      this.mFromLanguage = this.mLanguagePicker.getFromLanguage();
      this.mToLanguage = this.mLanguagePicker.getToLanguage();
      this.mChipView.initParameters(this.mActivity, null, null, this.mTranslateManager, this.mFromLanguage, this.mToLanguage, this, false);
      this.mOutputView = this.mChipView.getOutputView();
      this.mOutputView.init(this, this.mChipView, this.mTranslateManager, this.mLanguageList);
      this.mConfirmView.init(this.mActivity, this.mLanguageList, this.mFromLanguage, this.mToLanguage, this.mVoiceInputHelper.getAsrLocale(this.mActivity, this.mFromLanguage), false);
      this.mInputEditText.requestFocus();
      this.mShowIntro = true;
      setResultView(false);
      this.mTitleView.checkTitleShow();
      return;
    }
  }

  private void doTranslate(String paramString1, String paramString2)
  {
    Logger.d("InputPanel", "doTranslate input_text=" + paramString1);
    Util.hideSoftKeyboard(this.mActivity, this.mInputEditText.getWindowToken());
    this.mInputEditText.selectAll();
    this.mOutputView.doTranslate(this.mFromLanguage, this.mToLanguage, paramString1.trim(), paramString2);
    String str = this.mVoiceInputHelper.getAsrLocale(this.mActivity, this.mFromLanguage);
    this.mConfirmView.init(this.mActivity, this.mLanguageList, this.mFromLanguage, this.mToLanguage, str, false);
    endEditMode(InputMethodView.InputMethodEvent.INIT);
  }

  private void endEditMode(final InputMethodView.InputMethodEvent paramInputMethodEvent)
  {
    this.mShowIntro = true;
    Logger.d("InputPanel", "endEditMode");
    this.mActivity.runOnUiThread(new Runnable()
    {
      public void run()
      {
        InputPanel.this.mConfirmView.disableEditMode(true, paramInputMethodEvent);
        View localView = InputPanel.this.mPanelView.findViewById(R.id.panel_input);
        InputPanel.this.setInputBoxIcons(true);
        localView.invalidate();
        SdkVersionWrapper.getWrapper().invalidateOptionsMenu(InputPanel.this.mActivity);
        InputPanel.this.mTitleView.fixEditingViewVisibility(InputPanel.this.mConfirmView, 0);
      }
    });
    this.mPanelView.postDelayed(new Runnable()
    {
      public void run()
      {
        if (InputPanel.this.mLanguagePanel.getVisibility() != 0)
          InputPanel.this.mLanguagePanel.setVisibility(0);
      }
    }
    , 1000L);
  }

  private void internalSetInputText(final String paramString)
  {
    Logger.d("InputPanel", "internalSetInputText text=" + paramString);
    this.mActivity.runOnUiThread(new Runnable()
    {
      public void run()
      {
        InputPanel.this.mConfirmView.startInternalEdit();
        InputPanel.this.mInputEditText.setText(paramString, TextView.BufferType.SPANNABLE);
        InputPanel.this.mInputEditText.requestFocus();
        InputPanel.this.mInputEditText.selectAll();
        InputPanel.this.setInputTextHint();
        InputPanel.this.setInputBoxIcons(false);
        InputPanel.this.mConfirmView.endInternalEdit();
      }
    });
  }

  private boolean isCompositionText(Spanned paramSpanned, int paramInt1, int paramInt2)
  {
    for (Object localObject : paramSpanned.getSpans(paramInt1, paramInt2, Object.class))
      if ((paramSpanned.getSpanStart(localObject) == paramInt1) && (paramSpanned.getSpanEnd(localObject) == paramInt2) && ((0x100 & paramSpanned.getSpanFlags(localObject)) != 0))
        return true;
    return false;
  }

  private void notifyLanguageChanges(boolean paramBoolean)
  {
    Logger.d("InputPanel", "languagePairChanged triggerTranslate=" + paramBoolean);
    TextSlot localTextSlot = this.mInputEditText;
    Language[] arrayOfLanguage = new Language[2];
    arrayOfLanguage[0] = this.mFromLanguage;
    arrayOfLanguage[1] = this.mToLanguage;
    Util.detectAndSetFonts(localTextSlot, arrayOfLanguage, Constants.AppearanceType.INPUT_PANEL);
    this.mTranslateManager.setLanguagePair(this.mFromLanguage, this.mToLanguage);
    if (this.mConfirmView.isEditMode())
    {
      this.mConfirmView.init(this.mActivity, this.mLanguageList, this.mFromLanguage, this.mToLanguage, this.mVoiceInputHelper.getAsrLocale(this.mActivity, this.mFromLanguage), false);
      setInputTextHint();
      this.mConfirmView.resetSuggestions();
      return;
    }
    if (paramBoolean)
    {
      TranslateEntry localTranslateEntry = this.mOutputView.getCurrentTranslation();
      if ((localTranslateEntry == null) || (!this.mFromLanguage.equals(localTranslateEntry.fromLanguage)) || (!this.mToLanguage.equals(localTranslateEntry.toLanguage)))
        this.mOutputView.doTranslate(this.mFromLanguage, this.mToLanguage, getInputText(), null);
    }
    this.mActivity.runOnUiThread(new Runnable()
    {
      public void run()
      {
        InputPanel.this.mConfirmView.init(InputPanel.this.mActivity, InputPanel.this.mLanguageList, InputPanel.this.mFromLanguage, InputPanel.this.mToLanguage, InputPanel.this.mVoiceInputHelper.getAsrLocale(InputPanel.this.mActivity, InputPanel.this.mFromLanguage), false);
        InputPanel.this.setInputBoxIcons(false);
        InputPanel.this.setInputTextHint();
        InputPanel.this.endEditMode(InputMethodView.InputMethodEvent.INIT);
        SdkVersionWrapper.getWrapper().invalidateOptionsMenu(InputPanel.this.mActivity);
      }
    });
  }

  private void restoreSelection()
  {
    try
    {
      if (this.mSelectionSaved)
      {
        this.mSelectionSaved = false;
        if ((this.mSelectionText != null) && (this.mSelectionText.equals(this.mInputEditText.getText().toString())))
        {
          if (this.mSelectionStart < this.mSelectionEnd)
            this.mInputEditText.setSelected(true);
          this.mInputEditText.setSelection(this.mSelectionStart, this.mSelectionEnd);
        }
      }
      return;
    }
    finally
    {
    }
  }

  private void setInputBoxIcons(boolean paramBoolean)
  {
    if (!TextUtils.isEmpty(this.mInputEditText.getText()));
    for (boolean bool = true; !this.mConfirmView.isEditMode(); bool = false)
    {
      this.mBtnClearInput.setVisibility(0);
      this.mBtnClearInput.setEnabled(bool);
      return;
    }
    this.mBtnClearInput.setVisibility(0);
  }

  private void setInputTextHint()
  {
    if (this.mFromLanguage == null)
      return;
    if (!TextUtils.isEmpty(this.mInputEditText.getText()))
    {
      this.mInputEditText.setHint(null);
      return;
    }
    TextSlot localTextSlot = this.mInputEditText;
    if (Util.isAutoDetectLanguage(this.mFromLanguage));
    for (String str = this.mActivity.getString(R.string.hint_input_text_auto); ; str = this.mFromLanguage.getLongName())
    {
      localTextSlot.setHint(str);
      return;
    }
  }

  private void setPreviousResult()
  {
    if (this.mTranslateHelper == null);
    for (TranslateEntry localTranslateEntry = null; (localTranslateEntry != null) && (!localTranslateEntry.isInvalid()); localTranslateEntry = this.mTranslateHelper.getCurrentTranslation())
    {
      setResultView(true);
      this.mTranslateHelper.setInputPanel(false, false);
      return;
    }
    setResultView(false);
  }

  private void setResultView(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      this.mIntroMessageView.setVisibility(8);
      this.mChipWrapper.setVisibility(0);
      return;
    }
    this.mChipWrapper.setVisibility(8);
    if (this.mShowIntro)
    {
      this.mIntroMessageView.setVisibility(0);
      return;
    }
    this.mIntroMessageView.setVisibility(8);
  }

  private void startEditMode(final InputMethodView.InputMethod paramInputMethod)
  {
    Logger.d("InputPanel", "startEditMode");
    this.mShowIntro = false;
    this.mPrevTextLength = this.mInputEditText.getText().toString().length();
    this.mActivity.runOnUiThread(new Runnable()
    {
      public void run()
      {
        InputPanel.this.setResultView(false);
        View localView = InputPanel.this.mPanelView.findViewById(R.id.panel_input);
        if (paramInputMethod != null)
          InputPanel.this.mConfirmView.enableEditMode(paramInputMethod);
        localView.invalidate();
        SdkVersionWrapper.getWrapper().invalidateOptionsMenu(InputPanel.this.mActivity);
      }
    });
  }

  private void swapLanguagePair(boolean paramBoolean)
  {
    Logger.d("InputPanel", "swapLanguagePair");
    if (Util.isAutoDetectLanguage(this.mFromLanguage))
    {
      Util.showShortToastMessage(this.mActivity, this.mActivity.getString(R.string.msg_error_swap_language));
      return;
    }
    languagePairSelected(this.mTranslateHelper.getFromLanguageGivenToLanguage(this.mToLanguage, false), this.mTranslateHelper.getToLanguageGivenFromLanguage(this.mFromLanguage, false), paramBoolean, paramBoolean);
  }

  public void afterTextChanged(Editable paramEditable)
  {
    if (this.mConfirmView != null)
      this.mConfirmView.afterTextChanged(paramEditable);
  }

  public void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
  {
    if (this.mConfirmView != null)
      this.mConfirmView.beforeTextChanged(paramCharSequence, paramInt1, paramInt2, paramInt3);
  }

  public void clearInputText()
  {
    Logger.d("InputPanel", "clearInputText");
    this.mActivity.runOnUiThread(new Runnable()
    {
      public void run()
      {
        InputPanel.this.setInputText("");
      }
    });
  }

  void copyToInputTextBox(ChipView.ChipPart paramChipPart)
  {
    while (true)
    {
      TranslateEntry localTranslateEntry;
      try
      {
        if (this.mTranslateHelper == null)
        {
          localTranslateEntry = null;
          if (localTranslateEntry != null)
          {
            boolean bool = localTranslateEntry.isInvalid();
            if (!bool)
              continue;
          }
        }
        else
        {
          localTranslateEntry = this.mTranslateHelper.getCurrentTranslation();
          continue;
        }
        if (paramChipPart == ChipView.ChipPart.TRANSLATION_TEXT)
        {
          str = localTranslateEntry.toNewEntry().getTranslation();
          if (TextUtils.isEmpty(str))
            continue;
          if (paramChipPart != ChipView.ChipPart.TRANSLATION_TEXT)
            break label124;
          languagePairSelected(this.mTranslateHelper.getFromLanguageGivenToLanguage(localTranslateEntry.toLanguage, false), this.mTranslateHelper.getToLanguageGivenFromLanguage(localTranslateEntry.fromLanguage, false), false, false);
          setTextAndDoTranslate(str);
          continue;
        }
      }
      finally
      {
      }
      String str = localTranslateEntry.inputText;
      continue;
      label124: languagePairSelected(localTranslateEntry.fromLanguage, localTranslateEntry.toLanguage, false, false);
    }
  }

  void copyToInputTextBox(String paramString)
  {
    try
    {
      TranslateEntry localTranslateEntry = this.mTranslateHelper.getCurrentTranslation();
      if ((localTranslateEntry != null) && (!TextUtils.isEmpty(paramString)))
      {
        languagePairSelected(localTranslateEntry.fromLanguage, localTranslateEntry.toLanguage, false, false);
        setTextAndDoTranslate(paramString);
      }
      return;
    }
    finally
    {
    }
  }

  public View getContextMenuTargetView()
  {
    return this.mChipView;
  }

  public Language getFromLanguage()
  {
    return this.mFromLanguage;
  }

  public String getInputText()
  {
    return this.mInputEditText.getText().toString();
  }

  public ChipView.ChipPart getSelectedChipView()
  {
    if (this.mChipView != null)
      return this.mChipView.getSelectedChipPart();
    return ChipView.ChipPart.NONE;
  }

  public Language getToLanguage()
  {
    return this.mToLanguage;
  }

  public boolean hasSomethingToClear()
  {
    if (this.mTranslateHelper == null);
    for (TranslateEntry localTranslateEntry = null; (localTranslateEntry != null) && (localTranslateEntry.hasInputText()); localTranslateEntry = this.mTranslateHelper.getCurrentTranslation())
      return true;
    return false;
  }

  public void invalidate()
  {
    this.mPanelView.invalidate();
  }

  public boolean isInEditingMode()
  {
    return (this.mConfirmView != null) && (this.mConfirmView.isEditMode());
  }

  void languagePairSelected(Language paramLanguage1, Language paramLanguage2, boolean paramBoolean1, boolean paramBoolean2)
  {
    if (paramLanguage1 != null);
    while (true)
    {
      try
      {
        int i;
        if (paramLanguage1.equals(this.mFromLanguage))
        {
          i = 0;
          if (paramLanguage2 != null)
          {
            boolean bool = paramLanguage2.equals(this.mToLanguage);
            i = 0;
            if (!bool);
          }
          else
          {
            if (i != 0)
            {
              Logger.d("InputPanel", "languagePairSelected CHANGED!");
              if ((paramLanguage1.equals(this.mTranslateHelper.getFromLanguageGivenToLanguage(paramLanguage2, false))) && (!Languages.isChinese(paramLanguage1)))
              {
                if (paramLanguage1.equals(this.mFromLanguage))
                  continue;
                paramLanguage2 = this.mTranslateHelper.getToLanguageGivenFromLanguage(this.mFromLanguage, false);
              }
              this.mFromLanguage = paramLanguage1;
              this.mToLanguage = paramLanguage2;
              notifyLanguageChanges(paramBoolean1);
              this.mLanguagePicker.setLanguagePairToSpinners(paramLanguage1, paramLanguage2);
              if (!paramBoolean2)
                Util.showShortToastMessage(this.mActivity, Util.generateLongPairText(paramLanguage1, paramLanguage2));
            }
            return;
            Language localLanguage = this.mTranslateHelper.getFromLanguageGivenToLanguage(this.mToLanguage, false);
            paramLanguage1 = localLanguage;
            continue;
          }
        }
      }
      finally
      {
      }
      i = 1;
    }
  }

  public void onAccept(int paramInt, boolean paramBoolean)
  {
    Logger.d("InputPanel", "onAccept");
    String str = this.mInputEditText.getText().toString();
    if ((paramBoolean) && (!TextUtils.isEmpty(str)))
    {
      setTextAndDoTranslate(str, this.mConfirmView.getTranslationText(str));
      return;
    }
    endEditMode(InputMethodView.InputMethodEvent.ACCEPT);
    setPreviousResult();
  }

  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    Logger.d("InputPanel", "onActivityResult");
    if (this.mConfirmView != null)
      this.mConfirmView.onActivityResult(paramInt1, paramInt2, paramIntent);
    switch (paramInt1)
    {
    default:
    case 170:
    case 100:
    }
    do
    {
      String str;
      do
      {
        do
          return;
        while ((paramInt2 != -1) || (paramIntent == null));
        str = paramIntent.getStringExtra("android.intent.extra.TEXT");
      }
      while (TextUtils.isEmpty(str));
      UserActivityMgr.get().setTranslationSource(UserActivityMgr.RequestSource.SMS).setTranslationInputMethod(UserActivityMgr.InputMethod.UNKNOWN);
      setTextAndDoTranslate(str);
      return;
    }
    while ((paramInt2 != -1) || (paramIntent == null));
    restoreSelection();
    if (!this.mConfirmView.isEditMode())
      this.mConfirmView.enableEditMode(InputMethodView.InputMethod.NONE);
    this.mConfirmView.hideCurrentInputMethod(InputMethodView.InputMethodEvent.SWITCH);
    this.mConfirmView.onNonStreamingVoiceResult(VoiceInputHelper.getRecognitionResult(paramIntent));
  }

  public void onClick(View paramView)
  {
    Logger.d("InputPanel", "onClick");
    int i = paramView.getId();
    if (i == R.id.btn_swap)
    {
      UserActivityMgr.get().setTranslationInputMethod(UserActivityMgr.InputMethod.UNKNOWN).setTranslationSource(UserActivityMgr.RequestSource.SWAP);
      swapLanguagePair(true);
    }
    while (i != R.id.btn_confirm_view_clear)
      return;
    if ((this.mConfirmView != null) && (this.mConfirmView.isEditMode()))
    {
      this.mConfirmView.onClick(paramView);
      setInputTextHint();
      return;
    }
    if (TextUtils.isEmpty(getInputText()))
      this.mTranslateHelper.setCurrentTranslation(null);
    while (true)
    {
      this.mConfirmView.updateButtons("");
      return;
      clearInputText();
    }
  }

  public boolean onConfigurationChanged(Configuration paramConfiguration)
  {
    Logger.d("InputPanel", "onConfigurationChanged");
    if ((paramConfiguration.orientation == 2) || (paramConfiguration.orientation == 1))
    {
      if (this.mConfirmView != null)
      {
        this.mConfirmView.onConfigurationChanged(paramConfiguration);
        if (this.mTitleView != null)
          this.mTitleView.fixEditingViewVisibility(this.mConfirmView, 500);
      }
      return true;
    }
    return false;
  }

  public void onDestroy()
  {
    Logger.d("InputPanel", "onDestroy");
    if (this.mChipView != null)
      this.mChipView.onDestroy();
    if (this.mInputEditText != null)
      this.mInputEditText.removeTextChangedListener(this);
  }

  public void onEditModeReady(InputMethodView.InputMethod paramInputMethod)
  {
    if ((this.mTitleView != null) && (this.mConfirmView != null))
      this.mTitleView.fixEditingViewVisibility(this.mConfirmView, 0);
  }

  public void onEditModeStart(EditPanelView paramEditPanelView, InputMethodView.InputMethod paramInputMethod)
  {
    Logger.d("InputPanel", "onEditModeStart");
    startEditMode(paramInputMethod);
  }

  public void onFavoriteChanged(TranslateEntry paramTranslateEntry, boolean paramBoolean)
  {
  }

  public boolean onKeyPreIme(int paramInt, KeyEvent paramKeyEvent)
  {
    if (this.mConfirmView != null)
      return this.mConfirmView.onKeyPreIme(paramInt, paramKeyEvent);
    return false;
  }

  public void onLanguagePairSelected(Language paramLanguage1, Language paramLanguage2, boolean paramBoolean1, boolean paramBoolean2)
  {
    Logger.d("InputPanel", "onLanguagePairSelected");
    if (this.mInputEditText.getText().length() > 0)
      UserActivityMgr.setLanguageChanges(this.mFromLanguage, this.mToLanguage, paramLanguage1, paramLanguage2);
    languagePairSelected(paramLanguage1, paramLanguage2, paramBoolean1, paramBoolean2);
  }

  public void onPause()
  {
    if (this.mConfirmView != null)
      this.mConfirmView.onPause();
    if (this.mChipView != null)
      this.mChipView.onPause();
    if (this.mInputEditText.getSelectionStart() < this.mInputEditText.getSelectionEnd())
    {
      this.mSelectionSaved = true;
      this.mSelectionStart = this.mInputEditText.getSelectionStart();
      this.mSelectionEnd = this.mInputEditText.getSelectionEnd();
      this.mSelectionText = this.mInputEditText.getText().toString();
    }
    this.mInputEditText.setSelection(this.mInputEditText.length());
    this.mInputEditText.setIsTextEditor(false);
    this.mTitleView.showTitleBar();
  }

  public void onResume(TranslateEntry paramTranslateEntry)
  {
    Logger.d("InputPanel", "onResume");
    if (this.mControlPanelWrapper != null)
      this.mControlPanelWrapper.setVisibility(0);
    this.mLanguagePicker.setupLanguageSpinners();
    if ((paramTranslateEntry != null) && (this.mLanguageList != null))
    {
      languagePairSelected(paramTranslateEntry.getFromLanguageForLanguagePicker(this.mLanguageList), paramTranslateEntry.toLanguage, false, false);
      this.mTitleView.showTitleBar();
      showSearchButtons();
      if ((this.mConfirmView == null) || (!this.mConfirmView.isEditMode()))
        break label157;
      startEditMode(null);
      Logger.d("InputPanel", "onResume ==> startEditMode");
    }
    while (true)
    {
      this.mInputEditText.setIsTextEditor(true);
      restoreSelection();
      this.mConfirmView.endInternalEdit();
      return;
      if ((this.mFromLanguage != null) && (this.mToLanguage != null))
        break;
      languagePairSelected(this.mLanguagePicker.getFromLanguage(), this.mLanguagePicker.getToLanguage(), false, false);
      break;
      label157: this.mConfirmView.init(this.mActivity, this.mLanguageList, this.mFromLanguage, this.mToLanguage, this.mVoiceInputHelper.getAsrLocale(this.mActivity, this.mFromLanguage), false);
      endEditMode(InputMethodView.InputMethodEvent.INIT);
      if ((paramTranslateEntry != null) && (!TextUtils.isEmpty(paramTranslateEntry.inputText)))
      {
        Logger.d("InputPanel", "onResume ==> setTranslationResult");
        if (TextUtils.isEmpty(paramTranslateEntry.outputText))
          setTextAndDoTranslate(paramTranslateEntry.inputText, paramTranslateEntry.outputText);
        else
          setTranslationResult(paramTranslateEntry, true);
      }
    }
  }

  public void onSourceLanguageChangeRequested(Language paramLanguage, String paramString)
  {
    Logger.d("InputPanel", "onSourceLanguageChangeRequested newSrcLang=" + paramLanguage + " inputText=" + paramString);
    int i;
    if (!this.mConfirmView.isEditMode())
      i = 1;
    while (i != 0)
      if ((!TextUtils.isEmpty(paramString)) && (!TextUtils.equals(paramString, TranslateEntry.normalizeInputText(getInputText()))))
      {
        languagePairSelected(paramLanguage, this.mToLanguage, false, false);
        setTextAndDoTranslate(paramString);
        return;
        i = 0;
      }
      else
      {
        languagePairSelected(paramLanguage, this.mToLanguage, true, false);
        return;
      }
    languagePairSelected(paramLanguage, this.mToLanguage, false, false);
  }

  public void onStart()
  {
    Logger.d("InputPanel", "onStart");
    if (this.mInputEditText != null)
      this.mInputEditText.addTextChangedListener(this);
    this.mTitleView.checkTitleShow();
  }

  public void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
  {
    if (Logger.isDebug())
      Logger.d("InputPanel", "onTextChanged s=" + paramCharSequence + " start=" + paramInt1 + " before=" + paramInt2 + " count=" + paramInt3);
    setInputBoxIcons(false);
    setInputTextHint();
    if (this.mConfirmView != null)
      this.mConfirmView.onTextChanged(paramCharSequence, paramInt1, paramInt2, paramInt3);
    this.mEnableLanguageDetector = true;
  }

  public boolean onTouch(View paramView, MotionEvent paramMotionEvent)
  {
    Logger.d("InputPanel", "onTouch");
    if (paramView.getId() == this.mIntroMessageView.getId());
    return false;
  }

  public void onTranslationDone()
  {
    this.mConfirmView.onTranslationDone();
  }

  public void onTranslationDone(TranslateEntry paramTranslateEntry, boolean paramBoolean)
  {
    Logger.d("InputPanel", "onTranslationDone mInputPanel.getInputText()=" + getInputText());
    this.mTranslateHelper.setCurrentTranslation(paramTranslateEntry);
    showSearchButtons();
    if (!TextUtils.isEmpty(getInputText()))
    {
      this.mActivity.runOnUiThread(new Runnable()
      {
        public void run()
        {
          Logger.d("InputPanel", "onTranslationDone SELECT LAST");
          InputPanel.this.setResultView(true);
          InputPanel.this.onTranslationDone();
        }
      });
      return;
    }
    this.mActivity.runOnUiThread(new Runnable()
    {
      public void run()
      {
        Logger.d("InputPanel", "onTranslationDone NOT SELECT LAST");
        InputPanel.this.invalidate();
      }
    });
  }

  public void onTranslationFailed(Language paramLanguage1, Language paramLanguage2, String paramString)
  {
    this.mConfirmView.hideTranslationLoading();
    this.mTranslateHelper.setCurrentTranslation(TranslateEntry.build(paramLanguage1.getShortName(), paramLanguage2.getShortName(), paramString, "", Util.isAutoDetectLanguage(paramLanguage1), this.mLanguageList));
  }

  public void setInputText(String paramString)
  {
    Logger.d("InputPanel", "setInputText text=" + paramString);
    this.mEnableLanguageDetector = false;
    internalSetInputText(paramString);
  }

  public void setLanguageList(Languages paramLanguages)
  {
    this.mLanguageList = paramLanguages;
    this.mLanguagePicker.updateLanguageList(this.mLanguageList);
    if (this.mConfirmView != null)
      this.mConfirmView.setLanguageList(paramLanguages);
  }

  public void setTextAndDoTranslate(String paramString)
  {
    Logger.d("InputPanel", "setTextAndDoTranslate text=" + paramString);
    setTextAndDoTranslate(paramString, null);
  }

  public void setTextAndDoTranslate(String paramString1, String paramString2)
  {
    Logger.d("InputPanel", "setTextAndDoTranslate text=" + paramString1 + " translatedText=" + paramString2);
    setInputText(paramString1);
    doTranslate(paramString1, paramString2);
  }

  void setTranslationResult(final TranslateEntry paramTranslateEntry, final boolean paramBoolean)
  {
    this.mActivity.runOnUiThread(new Runnable()
    {
      public void run()
      {
        InputPanel.this.mConfirmView.hideTranslationLoading();
        if (paramTranslateEntry != null)
        {
          if (paramTranslateEntry.isInvalid())
          {
            if (paramTranslateEntry.hasSourceAndTargetLanguages())
              InputPanel.this.languagePairSelected(paramTranslateEntry.fromLanguage, paramTranslateEntry.toLanguage, false, false);
            InputPanel localInputPanel = InputPanel.this;
            if (paramTranslateEntry.hasInputText());
            for (String str = paramTranslateEntry.inputText; ; str = "")
            {
              localInputPanel.setInputText(str);
              InputPanel.this.setResultView(false);
              return;
            }
          }
          InputPanel.this.setResultView(true);
          Entry localEntry = paramTranslateEntry.toNewEntry();
          HistoryEntry localHistoryEntry = new HistoryEntry(localEntry, Util.isStarredTranslation(InputPanel.this.mActivity, localEntry));
          if ((paramBoolean) && (InputPanel.this.mInputEditText.isEmpty()))
            InputPanel.this.setInputText(localHistoryEntry.entry.getInputText());
          InputPanel.this.mChipView.render(localHistoryEntry, 0, 1);
          InputPanel.this.mConfirmView.updateButtons(InputPanel.this.getInputText());
          return;
        }
        InputPanel.this.setResultView(false);
      }
    });
  }

  public void showSearchButtons()
  {
    this.mActivity.runOnUiThread(new Runnable()
    {
      public void run()
      {
        InputPanel.this.setInputBoxIcons(false);
      }
    });
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.translation.InputPanel
 * JD-Core Version:    0.6.2
 */