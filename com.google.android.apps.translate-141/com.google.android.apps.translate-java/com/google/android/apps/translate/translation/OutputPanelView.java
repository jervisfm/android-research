package com.google.android.apps.translate.translation;

import android.app.Activity;
import android.content.Context;
import android.os.ConditionVariable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.google.android.apps.translate.Constants.AppearanceType;
import com.google.android.apps.translate.Language;
import com.google.android.apps.translate.Languages;
import com.google.android.apps.translate.LanguagesFactory;
import com.google.android.apps.translate.Logger;
import com.google.android.apps.translate.Preconditions;
import com.google.android.apps.translate.R.id;
import com.google.android.apps.translate.R.string;
import com.google.android.apps.translate.SdkVersionWrapper;
import com.google.android.apps.translate.SdkVersionWrapper.WrapperBase;
import com.google.android.apps.translate.Translate;
import com.google.android.apps.translate.Translate.TranslateResult;
import com.google.android.apps.translate.TranslateManager;
import com.google.android.apps.translate.Util;
import com.google.android.apps.translate.history.BaseDb;
import com.google.android.apps.translate.history.Entry;
import com.google.android.apps.translate.history.FavoriteDb;
import com.google.android.apps.translate.history.HistoryDb;
import java.util.List;
import java.util.Locale;

public class OutputPanelView extends LinearLayout
  implements View.OnClickListener
{
  public static final boolean SHOW_TRANSLATED_TEXT = false;
  private static final String TAG = "OutputPanelView";
  private Activity mActivity;
  private OutputPanelViewCallback mCallback;
  private TranslateEntry mCurrentTranslation;
  private TextView mDictionaryTextView;
  private BaseDb mFavoriteDb;
  private BaseDb mHistoryDb;
  private ConditionVariable mInitLock = new ConditionVariable();
  private Languages mLanguageList;
  private TextView mMessageTextView;
  private LinearLayout mOutputTextViewPanelView;
  private TranslateManager mTranslateManager;

  public OutputPanelView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    this.mActivity = ((Activity)paramContext);
  }

  private boolean addToDb(BaseDb paramBaseDb, TranslateEntry paramTranslateEntry, boolean paramBoolean)
  {
    Logger.d("OutputPanelView", "addToDb");
    if ((paramBaseDb != null) && (paramBaseDb.isOpened()))
    {
      if (this.mCallback != null)
        this.mCallback.onTranslationDone(paramTranslateEntry, isStarred(paramTranslateEntry));
      if (!paramBoolean)
      {
        paramBaseDb.add(paramTranslateEntry.toNewEntry().getEntryWithoutOnMemoryAttributes());
        paramBaseDb.flush(true);
      }
      return true;
    }
    Logger.e("OutputPanelView", "database not opened!");
    return false;
  }

  private void closeFavoriteDb()
  {
    if (this.mFavoriteDb != null)
    {
      this.mFavoriteDb.close(true);
      this.mFavoriteDb = null;
    }
  }

  private void closeHistoryDb()
  {
    if (this.mHistoryDb != null)
    {
      this.mHistoryDb.close(true);
      this.mHistoryDb = null;
    }
  }

  private void flushFavoriteDb()
  {
    if (this.mFavoriteDb != null)
      this.mFavoriteDb.flush(true);
  }

  private void flushHistoryDb()
  {
    if (this.mHistoryDb != null)
      this.mHistoryDb.flush(true);
  }

  private Entry getFromDb(BaseDb paramBaseDb, String paramString1, String paramString2, String paramString3)
  {
    if ((paramBaseDb != null) && (paramBaseDb.isOpened()))
      return paramBaseDb.get(paramString1, paramString2, paramString3);
    return null;
  }

  private void hideTranslationAlpha()
  {
    this.mActivity.runOnUiThread(new Runnable()
    {
      public void run()
      {
        OutputPanelView.this.findViewById(R.id.text_translation_alpha).setVisibility(8);
      }
    });
  }

  private void initializeInBackground()
  {
    Logger.d("OutputPanelView", "initializeInBackground");
    showTranslatingMessage();
    this.mInitLock.close();
    new Thread(new Runnable()
    {
      public void run()
      {
        OutputPanelView.access$002(OutputPanelView.this, OutputPanelView.this.openFavoriteDb());
        OutputPanelView.access$202(OutputPanelView.this, OutputPanelView.this.openHistoryDb());
        OutputPanelView.this.mInitLock.open();
        TranslateEntry localTranslateEntry;
        if (OutputPanelView.this.mCurrentTranslation != null)
        {
          localTranslateEntry = OutputPanelView.this.mCurrentTranslation;
          Language localLanguage1 = localTranslateEntry.fromLanguage;
          Language localLanguage2 = localTranslateEntry.toLanguage;
          if (!TextUtils.isEmpty(localTranslateEntry.outputText))
            break label108;
          String str = localTranslateEntry.inputText;
          OutputPanelView.this.doTranslate(localLanguage1, localLanguage2, str, null);
        }
        while (true)
        {
          OutputPanelView.this.showTranslateResultView();
          return;
          label108: OutputPanelView.this.addToDb(OutputPanelView.this.mHistoryDb, OutputPanelView.this.mCurrentTranslation, false);
          OutputPanelView.this.showTranslatingMessage();
          OutputPanelView.this.setTranslateResult(localTranslateEntry);
        }
      }
    }).start();
  }

  private boolean isInDb(BaseDb paramBaseDb, TranslateEntry paramTranslateEntry)
  {
    if ((paramBaseDb != null) && (paramBaseDb.isOpened()))
      return paramBaseDb.exists(paramTranslateEntry.fromLanguage.getShortName(), paramTranslateEntry.toLanguage.getShortName(), paramTranslateEntry.inputText);
    return false;
  }

  private BaseDb openFavoriteDb()
  {
    return FavoriteDb.open(this.mActivity);
  }

  private BaseDb openHistoryDb()
  {
    return HistoryDb.open(this.mActivity);
  }

  private void setDictionaryResult(String paramString1, final String paramString2, final Language paramLanguage1, final Language paramLanguage2)
  {
    this.mActivity.runOnUiThread(new Runnable()
    {
      public void run()
      {
        if (TextUtils.isEmpty(paramString2))
        {
          OutputPanelView.this.findViewById(R.id.panel_dict).setVisibility(8);
          return;
        }
        OutputPanelView.this.findViewById(R.id.panel_dict).setVisibility(0);
        TextView localTextView = OutputPanelView.this.mDictionaryTextView;
        String str = paramString2.replaceAll("\\n+$", "");
        Language[] arrayOfLanguage = new Language[2];
        arrayOfLanguage[0] = paramLanguage2;
        arrayOfLanguage[1] = paramLanguage1;
        Util.setTextAndFont(localTextView, str, arrayOfLanguage, Constants.AppearanceType.DICTIONARY_RESULT, false);
      }
    });
  }

  private void setTranslateResult(final TranslateEntry paramTranslateEntry)
  {
    this.mActivity.runOnUiThread(new Runnable()
    {
      public void run()
      {
        if (paramTranslateEntry == null)
          return;
        Logger.d("OutputPanelView", "setTranslateResult result.outputText=" + paramTranslateEntry.outputText);
        Translate.TranslateResult localTranslateResult = new Translate.TranslateResult(paramTranslateEntry.outputText);
        OutputPanelView.this.showTranslationAlpha(paramTranslateEntry.fromLanguage, paramTranslateEntry.toLanguage);
        if (localTranslateResult.hasDictionaryResult())
          OutputPanelView.this.setDictionaryResult(paramTranslateEntry.inputText, localTranslateResult.getDictionaryResult(), paramTranslateEntry.fromLanguage, paramTranslateEntry.toLanguage);
        while (true)
        {
          OutputPanelView.this.showTranslateResultView();
          return;
          OutputPanelView.this.setDictionaryResult(paramTranslateEntry.inputText, "", paramTranslateEntry.fromLanguage, paramTranslateEntry.toLanguage);
        }
      }
    });
  }

  private void showTranslationAlpha(final Language paramLanguage1, final Language paramLanguage2)
  {
    this.mActivity.runOnUiThread(new Runnable()
    {
      public void run()
      {
        TextView localTextView = (TextView)OutputPanelView.this.findViewById(R.id.text_translation_alpha);
        if ((!Languages.isAlphaLanguage(paramLanguage1)) && (!Languages.isAlphaLanguage(paramLanguage2)))
        {
          localTextView.setVisibility(8);
          return;
        }
        localTextView.setVisibility(0);
      }
    });
  }

  private void translateInBackground(final Language paramLanguage1, final Language paramLanguage2, final String paramString1, final String paramString2)
  {
    Logger.d("OutputPanelView", "translateInBackground");
    if (TextUtils.isEmpty(paramString1))
    {
      Logger.d("no text, do not translate");
      return;
    }
    showTranslatingMessage();
    new Thread(new Runnable()
    {
      public void run()
      {
        OutputPanelView.this.mInitLock.block();
        Language localLanguage = paramLanguage1;
        boolean bool1 = Util.isAutoDetectLanguage(paramLanguage1);
        if (!TextUtils.isEmpty(paramString2))
        {
          TranslateEntry localTranslateEntry1 = new TranslateEntry(localLanguage, paramLanguage2, paramString1, paramString2, bool1);
          OutputPanelView.this.setTranslateResult(localTranslateEntry1);
        }
        OutputPanelView.this.mTranslateManager.setLanguagePair(paramLanguage1, paramLanguage2);
        String str1 = OutputPanelView.this.mTranslateManager.doTranslate(paramString1);
        int i = Translate.getResultCode(str1);
        boolean bool2;
        if ((i == 0) || (i == -4))
        {
          String str2 = str1;
          int j = str1.indexOf("\t");
          if (j >= 0)
          {
            localLanguage = OutputPanelView.this.mLanguageList.getFromLanguageByShortName(str1.substring(0, j));
            if (localLanguage == null)
              localLanguage = paramLanguage1;
            str2 = str1.substring(j + 1);
          }
          TranslateEntry localTranslateEntry2 = new TranslateEntry(localLanguage, paramLanguage2, paramString1, str2, bool1);
          OutputPanelView.this.postBackCurrentTranslation(localTranslateEntry2);
          OutputPanelView.this.setResult(localTranslateEntry2);
          Logger.d("OutputPanelView", "Saves to history...");
          OutputPanelView localOutputPanelView = OutputPanelView.this;
          BaseDb localBaseDb = OutputPanelView.this.mHistoryDb;
          if (i == -4)
          {
            bool2 = true;
            localOutputPanelView.addToDb(localBaseDb, localTranslateEntry2, bool2);
          }
        }
        while (true)
        {
          OutputPanelView.this.mActivity.runOnUiThread(new Runnable()
          {
            public void run()
            {
              SdkVersionWrapper.getWrapper().invalidateOptionsMenu(OutputPanelView.this.mActivity);
            }
          });
          return;
          bool2 = false;
          break;
          Logger.d("OutputPanelView", "Gets result from history if possible...");
          OutputPanelView.this.postBackCurrentTranslation(null);
          TranslateEntry localTranslateEntry3 = OutputPanelView.this.tryLoadingFromHistory(paramLanguage1, paramLanguage2, paramString1);
          if (localTranslateEntry3 == null)
          {
            Util.showTranslationErrorToastMessage(OutputPanelView.this.mActivity, i);
            if (OutputPanelView.this.mCallback != null)
              OutputPanelView.this.mCallback.onTranslationFailed(paramLanguage1, paramLanguage2, paramString1);
          }
          else if (OutputPanelView.this.mCallback != null)
          {
            OutputPanelView.this.mCallback.onTranslationDone(localTranslateEntry3, true);
          }
        }
      }
    }).start();
  }

  private TranslateEntry tryLoadingFromHistory(Language paramLanguage1, Language paramLanguage2, String paramString)
  {
    boolean bool1 = Util.isAutoDetectLanguage(paramLanguage1);
    List localList1;
    Entry localEntry3;
    label77: TranslateEntry localTranslateEntry1;
    if (bool1)
    {
      localList1 = FavoriteDb.getAllByATime(this.mActivity, 1, paramString);
      if (localList1.isEmpty())
      {
        localEntry3 = null;
        if ((localEntry3 == null) || (!localEntry3.getInputText().equals(paramString)) || (!localEntry3.getToLanguageShortName().equals(paramLanguage2)))
          break label187;
        paramLanguage1 = this.mLanguageList.getFromLanguageByShortName(localEntry3.getFromLanguageShortName());
      }
    }
    else
    {
      boolean bool2 = Util.isAutoDetectLanguage(paramLanguage1);
      localTranslateEntry1 = null;
      if (!bool2)
      {
        Entry localEntry1 = getFromDb(this.mFavoriteDb, paramLanguage1.getShortName(), paramLanguage2.getShortName(), paramString);
        if (localEntry1 == null)
          break label272;
        String str2 = localEntry1.getOutputText();
        localTranslateEntry1 = new TranslateEntry(paramLanguage1, paramLanguage2, paramString, str2, bool1);
        postBackCurrentTranslation(localTranslateEntry1);
        Logger.d("Loaded from favorites");
        setResult(localTranslateEntry1);
        addToDb(this.mHistoryDb, localTranslateEntry1, false);
      }
    }
    label187: Entry localEntry2;
    label270: label272: 
    do
    {
      return localTranslateEntry1;
      localEntry3 = (Entry)localList1.get(0);
      break;
      List localList2 = HistoryDb.getAllByATime(this.mActivity, 1, paramString);
      if (localList2.isEmpty());
      for (Entry localEntry4 = null; ; localEntry4 = (Entry)localList2.get(0))
      {
        if ((localEntry4 == null) || (!localEntry4.getInputText().equals(paramString)) || (!localEntry4.getToLanguageShortName().equals(paramLanguage2)))
          break label270;
        paramLanguage1 = this.mLanguageList.getFromLanguageByShortName(localEntry4.getFromLanguageShortName());
        break;
      }
      break label77;
      localEntry2 = getFromDb(this.mHistoryDb, paramLanguage1.getShortName(), paramLanguage2.getShortName(), paramString);
      localTranslateEntry1 = null;
    }
    while (localEntry2 == null);
    updateLastAccessed(this.mHistoryDb, localEntry2);
    String str1 = localEntry2.getOutputText();
    TranslateEntry localTranslateEntry2 = new TranslateEntry(paramLanguage1, paramLanguage2, paramString, str1, bool1);
    postBackCurrentTranslation(localTranslateEntry2);
    Logger.d("Loaded from history");
    setResult(localTranslateEntry2);
    return localTranslateEntry2;
  }

  private void updateLastAccessed(BaseDb paramBaseDb, Entry paramEntry)
  {
    if ((paramBaseDb != null) && (paramBaseDb.isOpened()))
      paramBaseDb.updateLastAccessed(paramEntry);
  }

  public void copyTranslationText()
  {
    if ((this.mCurrentTranslation == null) || (this.mCurrentTranslation.isInvalid()))
      return;
    Translate.TranslateResult localTranslateResult = new Translate.TranslateResult(this.mCurrentTranslation.toNewEntry());
    Util.copyToClipBoard(this.mActivity, localTranslateResult.getTranslateText(), ChipView.ChipPart.TRANSLATION_TEXT);
    Util.showShortToastMessage(this.mActivity, R.string.toast_message_copy);
  }

  public void doTranslate(final Language paramLanguage1, final Language paramLanguage2, final String paramString1, final String paramString2)
  {
    Logger.d("OutputPanelView", "doTranslate");
    if (TextUtils.isEmpty(paramString1))
      return;
    this.mActivity.runOnUiThread(new Runnable()
    {
      public void run()
      {
        OutputPanelView.access$502(OutputPanelView.this, null);
        OutputPanelView.this.translateInBackground(paramLanguage1, paramLanguage2, paramString1, paramString2);
      }
    });
  }

  public TranslateEntry getCurrentTranslation()
  {
    return this.mCurrentTranslation;
  }

  public void hideTranslateResultView()
  {
    Logger.d("OutputPanelView", "hideTranslateResultView");
    this.mActivity.runOnUiThread(new Runnable()
    {
      public void run()
      {
        OutputPanelView.this.setVisibility(8);
        OutputPanelView.this.invalidate();
      }
    });
  }

  public void init(OutputPanelViewCallback paramOutputPanelViewCallback, ChipView paramChipView, TranslateManager paramTranslateManager, Languages paramLanguages)
  {
    Logger.d("OutputPanelView", "init");
    if (this.mCallback != null);
    do
    {
      return;
      this.mCallback = paramOutputPanelViewCallback;
      this.mTranslateManager = paramTranslateManager;
      this.mLanguageList = paramLanguages;
      Preconditions.checkNotNull(this.mTranslateManager);
      Preconditions.checkNotNull(this.mLanguageList);
      this.mOutputTextViewPanelView = ((LinearLayout)findViewById(R.id.output_textview_panel));
      Preconditions.checkNotNull(this.mOutputTextViewPanelView);
      this.mMessageTextView = ((TextView)findViewById(R.id.text_translation_panel_message));
      this.mMessageTextView.setVisibility(8);
      this.mDictionaryTextView = ((TextView)findViewById(R.id.text_dict));
      findViewById(R.id.btn_supersize).setOnClickListener(this);
      findViewById(R.id.btn_share).setOnClickListener(this);
      findViewById(R.id.btn_copy).setOnClickListener(this);
      initializeInBackground();
    }
    while (this.mCurrentTranslation == null);
    showTranslateResultView();
  }

  public boolean isStarred(TranslateEntry paramTranslateEntry)
  {
    return isInDb(this.mFavoriteDb, paramTranslateEntry);
  }

  public boolean isTranslateResultViewHidden()
  {
    return getVisibility() == 8;
  }

  public void onClick(View paramView)
  {
    int i = paramView.getId();
    if (i == R.id.btn_copy)
      copyTranslationText();
    do
    {
      return;
      if (i == R.id.btn_share)
      {
        shareTranslatedText();
        return;
      }
    }
    while (i != R.id.btn_supersize);
    showSupersizeTranslatText();
  }

  public void onDestroy()
  {
    closeFavoriteDb();
    closeHistoryDb();
  }

  public void onPause()
  {
    flushHistoryDb();
    flushFavoriteDb();
  }

  public void postBackCurrentTranslation(final TranslateEntry paramTranslateEntry)
  {
    Logger.d("OutputPanelView", "postBackCurrentTranslation");
    this.mActivity.runOnUiThread(new Runnable()
    {
      public void run()
      {
        OutputPanelView.access$502(OutputPanelView.this, paramTranslateEntry);
      }
    });
  }

  public void refreshLanguageList(Language paramLanguage1, Language paramLanguage2)
  {
    this.mLanguageList = LanguagesFactory.get().getLanguages(this.mActivity, Locale.getDefault());
    if ((this.mCurrentTranslation != null) && (paramLanguage1 != null) && (paramLanguage2 != null))
      this.mCurrentTranslation = new TranslateEntry(paramLanguage1, paramLanguage2, this.mCurrentTranslation.inputText, this.mCurrentTranslation.outputText, this.mCurrentTranslation.isAutoLanguage);
  }

  public void setResult(TranslateEntry paramTranslateEntry)
  {
    Logger.d("OutputPanelView", "setResult result.outputText=" + paramTranslateEntry.outputText);
    setTranslateResult(paramTranslateEntry);
  }

  public void shareTranslatedText()
  {
    if ((this.mCurrentTranslation == null) || (this.mCurrentTranslation.isInvalid()))
      return;
    TranslateHelper.performChipActionShare(this.mActivity, this.mLanguageList, this.mCurrentTranslation.toNewEntry(), ChipView.ChipPart.TRANSLATION_TEXT);
  }

  public void showSupersizeTranslatText()
  {
    if (this.mCurrentTranslation == null)
      return;
    Util.openSupersizeTextActivity(this.mActivity, this.mCurrentTranslation, false);
  }

  public void showTranslateResultView()
  {
    Logger.d("OutputPanelView", "showTranslateResultView");
    this.mActivity.runOnUiThread(new Runnable()
    {
      public void run()
      {
        OutputPanelView.this.mOutputTextViewPanelView.setVisibility(0);
        OutputPanelView.this.setVisibility(0);
        OutputPanelView.this.mOutputTextViewPanelView.invalidate();
        OutputPanelView.this.invalidate();
      }
    });
  }

  public void showTranslatingMessage()
  {
  }

  public static abstract interface OutputPanelViewCallback
  {
    public abstract void onFavoriteChanged(TranslateEntry paramTranslateEntry, boolean paramBoolean);

    public abstract void onTranslationDone(TranslateEntry paramTranslateEntry, boolean paramBoolean);

    public abstract void onTranslationFailed(Language paramLanguage1, Language paramLanguage2, String paramString);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.translation.OutputPanelView
 * JD-Core Version:    0.6.2
 */