package com.google.android.apps.translate.translation;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import com.google.android.apps.translate.Languages;
import com.google.android.apps.translate.Logger;
import com.google.android.apps.translate.R.layout;
import com.google.android.apps.translate.Util;

public class TranslateActivity extends Activity
{
  private static final String TAG = "TranslateActivity";
  private Languages mLanguageList;
  private BroadcastReceiver mOnLanguagesChanged;
  private TranslateHelper mTranslateHelper = new TranslateHelper();

  public void finish()
  {
    super.finish();
    this.mTranslateHelper.finish();
  }

  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    this.mTranslateHelper.onActivityResult(paramInt1, paramInt2, paramIntent);
  }

  public void onConfigurationChanged(Configuration paramConfiguration)
  {
    Logger.d("TranslateActivity", "onConfigurationChanged");
    super.onConfigurationChanged(paramConfiguration);
    if ((this.mTranslateHelper != null) && (this.mTranslateHelper.onConfigurationChanged(paramConfiguration)));
  }

  public boolean onContextItemSelected(MenuItem paramMenuItem)
  {
    if (this.mTranslateHelper.onContextItemSelected(paramMenuItem));
    return true;
  }

  protected void onCreate(Bundle paramBundle)
  {
    Logger.d("TranslateActivity", "onCreate");
    super.onCreate(paramBundle);
    this.mLanguageList = Util.getLanguageListFromProfile(this);
    FrameLayout localFrameLayout = (FrameLayout)getLayoutInflater().inflate(R.layout.translate_activity, null);
    this.mTranslateHelper.onCreate(this, this.mLanguageList, localFrameLayout, null);
    setContentView(localFrameLayout);
    this.mOnLanguagesChanged = new BroadcastReceiver()
    {
      public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
      {
        TranslateActivity.this.mTranslateHelper.onLanguagesChanged();
      }
    };
    registerReceiver(this.mOnLanguagesChanged, new IntentFilter("com.google.android.apps.translate.broadcast.LANGUAGES_CHANGED"));
    registerForContextMenu(this.mTranslateHelper.getContextMenuTargetView());
  }

  public void onCreateContextMenu(ContextMenu paramContextMenu, View paramView, ContextMenu.ContextMenuInfo paramContextMenuInfo)
  {
    super.onCreateContextMenu(paramContextMenu, paramView, paramContextMenuInfo);
    this.mTranslateHelper.onCreateContextMenu(paramContextMenu, paramView, paramContextMenuInfo);
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    if (TranslateHelper.onCreateOptionsMenu(this, paramMenu, getMenuInflater()))
      return true;
    return super.onCreateOptionsMenu(paramMenu);
  }

  protected void onDestroy()
  {
    Logger.d("TranslateActivity", "onDestroy");
    super.onDestroy();
    this.mTranslateHelper.onDestroy();
    if (this.mOnLanguagesChanged != null)
      unregisterReceiver(this.mOnLanguagesChanged);
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    Logger.d("TranslateActivity", "onKeyDown");
    if ((this.mTranslateHelper != null) && (this.mTranslateHelper.onKeyPreIme(paramInt, paramKeyEvent)))
      return true;
    return super.onKeyDown(paramInt, paramKeyEvent);
  }

  protected void onNewIntent(Intent paramIntent)
  {
    Logger.d("TranslateActivity", "onNewIntent");
    this.mTranslateHelper.onNewIntent(paramIntent);
    super.onNewIntent(paramIntent);
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    if (this.mTranslateHelper.onOptionsItemSelected(paramMenuItem))
      return true;
    return super.onOptionsItemSelected(paramMenuItem);
  }

  protected void onPause()
  {
    super.onPause();
    this.mTranslateHelper.onPause();
  }

  public boolean onPrepareOptionsMenu(Menu paramMenu)
  {
    if (TranslateHelper.onPrepareOptionsMenu(paramMenu))
      return true;
    return super.onPrepareOptionsMenu(paramMenu);
  }

  protected void onResume()
  {
    super.onResume();
    this.mTranslateHelper.onResume();
  }

  protected void onStart()
  {
    Logger.d("TranslateActivity", "onStart");
    super.onStart();
    if (this.mTranslateHelper != null)
      this.mTranslateHelper.onStart();
  }

  protected void onStop()
  {
    Logger.d("TranslateActivity", "onStop");
    super.onStop();
    if (this.mTranslateHelper != null)
      this.mTranslateHelper.onStop();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.translation.TranslateActivity
 * JD-Core Version:    0.6.2
 */