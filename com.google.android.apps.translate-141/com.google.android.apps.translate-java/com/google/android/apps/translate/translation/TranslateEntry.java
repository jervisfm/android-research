package com.google.android.apps.translate.translation;

import android.text.TextUtils;
import com.google.android.apps.translate.Language;
import com.google.android.apps.translate.Languages;
import com.google.android.apps.translate.history.Entry;

public class TranslateEntry
{
  public final Language fromLanguage;
  public final String inputText;
  public final boolean isAutoLanguage;
  public final String outputText;
  public final Language toLanguage;

  public TranslateEntry(Language paramLanguage1, Language paramLanguage2, String paramString1, String paramString2, boolean paramBoolean)
  {
    this.fromLanguage = paramLanguage1;
    this.toLanguage = paramLanguage2;
    this.inputText = normalizeInputText(paramString1);
    this.outputText = paramString2;
    this.isAutoLanguage = paramBoolean;
  }

  public static TranslateEntry build(String paramString1, String paramString2, String paramString3, String paramString4, boolean paramBoolean, Languages paramLanguages)
  {
    if ((paramString1 != null) && (paramString2 != null) && (paramString3 != null))
    {
      Language localLanguage1 = paramLanguages.getFromLanguageByShortName(paramString1);
      Language localLanguage2 = paramLanguages.getToLanguageByShortName(paramString2);
      if ((localLanguage1 != null) && (localLanguage2 != null))
      {
        if (paramString4 == null);
        for (String str = ""; ; str = paramString4)
        {
          TranslateEntry localTranslateEntry = new TranslateEntry(localLanguage1, localLanguage2, paramString3, str, paramBoolean);
          if (!localTranslateEntry.hasInputText())
            break;
          return localTranslateEntry;
        }
        return null;
      }
    }
    return null;
  }

  public static String normalizeInputText(String paramString)
  {
    if (paramString == null)
      return "";
    return paramString.trim().replace('\t', ' ');
  }

  public Language getFromLanguageForLanguagePicker(Languages paramLanguages)
  {
    if ((this.isAutoLanguage) && (paramLanguages != null))
      return paramLanguages.getFromLanguageByShortName("auto");
    return this.fromLanguage;
  }

  public boolean hasInputText()
  {
    return !TextUtils.isEmpty(this.inputText);
  }

  public boolean hasSourceAndTargetLanguages()
  {
    return (this.fromLanguage != null) && (this.toLanguage != null);
  }

  public boolean isInvalid()
  {
    return (this.fromLanguage == null) || (this.toLanguage == null) || (TextUtils.isEmpty(this.inputText)) || (TextUtils.isEmpty(this.outputText));
  }

  public Entry toNewEntry()
  {
    return new Entry(this.fromLanguage.getShortName(), this.toLanguage.getShortName(), this.inputText, this.outputText);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.translation.TranslateEntry
 * JD-Core Version:    0.6.2
 */