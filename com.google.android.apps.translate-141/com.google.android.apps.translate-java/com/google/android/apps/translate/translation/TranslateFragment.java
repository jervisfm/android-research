package com.google.android.apps.translate.translation;

import android.app.Activity;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.apps.translate.ActionBarSpinnerAdapter;
import com.google.android.apps.translate.HomeActivity;
import com.google.android.apps.translate.Language;
import com.google.android.apps.translate.Languages;
import com.google.android.apps.translate.Logger;
import com.google.android.apps.translate.Preconditions;
import com.google.android.apps.translate.R.layout;
import com.google.android.apps.translate.SdkVersionWrapper;
import com.google.android.apps.translate.SdkVersionWrapper.WrapperBase;
import com.google.android.apps.translate.Util;
import com.google.android.apps.translate.editor.PreImeAutoCompleteTextView.OnKeyPreImeListener;

public class TranslateFragment extends Fragment
  implements PreImeAutoCompleteTextView.OnKeyPreImeListener
{
  private static final String TAG = "TranslateFragment";
  private Activity mActivity;
  private Languages mLanguageList;
  private BroadcastReceiver mOnLanguagesChanged;
  private ActionBarSpinnerAdapter mSpinnerAdapter;
  private TranslateFragmentWrapper mTranslateFragmentWrapper;
  private TranslateHelper mTranslateHelper = new TranslateHelper();

  public TranslateFragment()
  {
  }

  public TranslateFragment(TranslateFragmentWrapper paramTranslateFragmentWrapper, ActionBarSpinnerAdapter paramActionBarSpinnerAdapter)
  {
    Logger.d("TranslateFragment", "TranslateFragment");
    this.mSpinnerAdapter = paramActionBarSpinnerAdapter;
    this.mTranslateFragmentWrapper = paramTranslateFragmentWrapper;
  }

  public static void startTranslateFragment(Activity paramActivity, String paramString, Language paramLanguage1, Language paramLanguage2, boolean paramBoolean)
  {
    TranslateFragment localTranslateFragment = ((HomeActivity)paramActivity).getTranslateFragmentWrapper().getTranslateFragment();
    if (localTranslateFragment != null)
    {
      localTranslateFragment.setTranslateEntry(new TranslateEntry(paramLanguage1, paramLanguage2, paramString, "", Util.isAutoDetectLanguage(paramLanguage1)));
      localTranslateFragment.mSpinnerAdapter.onNavigationItemSelected(0, 0L);
      SdkVersionWrapper.getWrapper().setActionBarTitle(paramActivity, localTranslateFragment.mSpinnerAdapter);
    }
  }

  public TranslateHelper getHelper()
  {
    return this.mTranslateHelper;
  }

  public void onActivityCreated(Bundle paramBundle)
  {
    Logger.d("TranslateFragment", "onActivityCreated");
    super.onActivityCreated(paramBundle);
    this.mActivity = getActivity();
    this.mLanguageList = Util.getLanguageListFromProfile(this.mActivity);
    Preconditions.checkNotNull(this.mLanguageList);
    this.mTranslateHelper.onCreate(this.mActivity, this.mLanguageList, getView(), this.mTranslateFragmentWrapper);
    this.mOnLanguagesChanged = new BroadcastReceiver()
    {
      public void onReceive(Context paramAnonymousContext, Intent paramAnonymousIntent)
      {
        TranslateFragment.this.mTranslateHelper.onLanguagesChanged();
      }
    };
    this.mActivity.registerReceiver(this.mOnLanguagesChanged, new IntentFilter("com.google.android.apps.translate.broadcast.LANGUAGES_CHANGED"));
    registerForContextMenu(this.mTranslateHelper.getContextMenuTargetView());
  }

  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    if (this.mTranslateHelper != null)
      this.mTranslateHelper.onActivityResult(paramInt1, paramInt2, paramIntent);
  }

  public void onConfigurationChanged(Configuration paramConfiguration)
  {
    Logger.d("TranslateFragment", "onConfigurationChanged");
    super.onConfigurationChanged(paramConfiguration);
    if ((this.mTranslateHelper != null) && (this.mTranslateHelper.onConfigurationChanged(paramConfiguration)));
  }

  public boolean onContextItemSelected(MenuItem paramMenuItem)
  {
    if (this.mTranslateHelper != null)
      this.mTranslateHelper.onContextItemSelected(paramMenuItem);
    return super.onContextItemSelected(paramMenuItem);
  }

  public void onCreate(Bundle paramBundle)
  {
    Logger.d("TranslateFragment", "onCreate");
    super.onCreate(paramBundle);
    setHasOptionsMenu(true);
  }

  public void onCreateContextMenu(ContextMenu paramContextMenu, View paramView, ContextMenu.ContextMenuInfo paramContextMenuInfo)
  {
    if (this.mTranslateHelper != null)
      this.mTranslateHelper.onCreateContextMenu(paramContextMenu, paramView, paramContextMenuInfo);
  }

  public void onCreateOptionsMenu(Menu paramMenu, MenuInflater paramMenuInflater)
  {
    Logger.d("TranslateFragment", "onCreateOptionsMenu");
    if (TranslateHelper.onCreateOptionsMenu(getActivity(), paramMenu, paramMenuInflater))
      return;
    super.onCreateOptionsMenu(paramMenu, paramMenuInflater);
  }

  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    super.onCreateView(paramLayoutInflater, paramViewGroup, paramBundle);
    return paramLayoutInflater.inflate(R.layout.translate_panel, null);
  }

  public void onDestroy()
  {
    Logger.d("TranslateFragment", "onDestroy");
    super.onDestroy();
    this.mTranslateHelper.onDestroy();
    if (this.mOnLanguagesChanged != null)
      this.mActivity.unregisterReceiver(this.mOnLanguagesChanged);
  }

  public boolean onKeyPreIme(int paramInt, KeyEvent paramKeyEvent)
  {
    return (this.mTranslateHelper != null) && (this.mTranslateHelper.onKeyPreIme(paramInt, paramKeyEvent));
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    Logger.d("TranslateFragment", "onOptionsItemSelected");
    if ((this.mTranslateHelper != null) && (this.mTranslateHelper.onOptionsItemSelected(paramMenuItem)))
    {
      this.mActivity.invalidateOptionsMenu();
      return true;
    }
    return super.onOptionsItemSelected(paramMenuItem);
  }

  public void onPause()
  {
    Logger.d("TranslateFragment", "onPause");
    super.onPause();
    this.mTranslateHelper.onPause();
  }

  public void onPrepareOptionsMenu(Menu paramMenu)
  {
    Logger.d("TranslateFragment", "onPrepareOptionsMenu");
    if (TranslateHelper.onPrepareOptionsMenu(paramMenu))
      return;
    super.onPrepareOptionsMenu(paramMenu);
  }

  public void onResume()
  {
    Logger.d("TranslateFragment", "onResume");
    super.onResume();
    SdkVersionWrapper.getWrapper().setHomeButton(this.mActivity, false);
    this.mTranslateHelper.onResume();
  }

  public void onStart()
  {
    Logger.d("TranslateFragment", "onStart");
    super.onStart();
    if (this.mTranslateHelper != null)
      this.mTranslateHelper.onStart();
  }

  public void onStop()
  {
    Logger.d("TranslateFragment", "onStop");
    super.onStop();
    if (this.mTranslateHelper != null)
      this.mTranslateHelper.onStop();
  }

  public void setInputTextAndDoTranslate(String paramString)
  {
    this.mTranslateHelper.setTextAndDoTranslate(paramString);
  }

  public void setTranslateEntry(TranslateEntry paramTranslateEntry)
  {
    this.mTranslateHelper.setTranslateEntry(paramTranslateEntry, false, false, true);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.translation.TranslateFragment
 * JD-Core Version:    0.6.2
 */