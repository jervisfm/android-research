package com.google.android.apps.translate.translation;

import android.app.ActionBar;
import android.app.Activity;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Display;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import com.google.android.apps.translate.ActionBarSpinnerAdapter;
import com.google.android.apps.translate.Logger;
import com.google.android.apps.translate.R.id;
import com.google.android.apps.translate.R.layout;
import com.google.android.apps.translate.SdkVersionWrapper;
import com.google.android.apps.translate.SdkVersionWrapper.WrapperBase;
import com.google.android.apps.translate.home.TabMenuFragment;

public class TranslateFragmentWrapper
{
  private static final float MINIMUM_SCREEN_WIDTH_FOR_TAB_MENU = 600.0F;
  private static final String TAG = "TranslateFragmentWrapper";
  private Activity mActivity;
  private ActionBarSpinnerAdapter mSpinnerAdapter = null;
  private TranslateFragment mTranslateFragment;

  public TranslateFragmentWrapper(Activity paramActivity)
  {
    this.mActivity = paramActivity;
    boolean bool = shouldUseTabMenu(null);
    this.mSpinnerAdapter = new ActionBarSpinnerAdapter(paramActivity, R.layout.action_bar_spinner_item, R.id.spinner_item_text_view, bool, this);
    TabMenuFragment.setTabMenuFragment(this.mActivity, this.mSpinnerAdapter);
    SdkVersionWrapper.getWrapper().setActionBarTitle(this.mActivity, this.mSpinnerAdapter);
    this.mTranslateFragment = this.mSpinnerAdapter.getTranslateFragment();
  }

  private boolean shouldUseTabMenu(Configuration paramConfiguration)
  {
    if (paramConfiguration == null)
      paramConfiguration = this.mActivity.getResources().getConfiguration();
    if (paramConfiguration == null)
    {
      Logger.d("TranslateFragmentWrapper", "shouldUseTabMenu false: null");
      return false;
    }
    if (paramConfiguration.orientation != 2)
    {
      Logger.d("TranslateFragmentWrapper", "shouldUseTabMenu false: not landscape");
      return false;
    }
    if (this.mActivity.getWindowManager().getDefaultDisplay().getWidth() / this.mActivity.getResources().getDisplayMetrics().density >= 600.0F)
    {
      Logger.d("TranslateFragmentWrapper", "shouldUseTabMenu true");
      return true;
    }
    Logger.d("TranslateFragmentWrapper", "shouldUseTabMenu false: small screenl");
    return false;
  }

  public TranslateHelper getHelper()
  {
    return this.mTranslateFragment.getHelper();
  }

  public TranslateFragment getTranslateFragment()
  {
    return this.mTranslateFragment;
  }

  public void goBackHome()
  {
    this.mActivity.getActionBar().setSelectedNavigationItem(0);
  }

  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    if (this.mSpinnerAdapter != null)
      this.mSpinnerAdapter.onActivityResult(paramInt1, paramInt2, paramIntent);
  }

  public void onConfigurationChanged(Configuration paramConfiguration)
  {
    Logger.d("TranslateFragmentWrapper", "onConfigurationChanged");
    if ((this.mActivity != null) && (this.mSpinnerAdapter != null))
    {
      boolean bool = shouldUseTabMenu(paramConfiguration);
      this.mSpinnerAdapter.setTabMenuMode(bool);
      SdkVersionWrapper.getWrapper().setActionBarTitle(this.mActivity, this.mSpinnerAdapter);
    }
  }

  public void onCreateContextMenu(ContextMenu paramContextMenu, View paramView, ContextMenu.ContextMenuInfo paramContextMenuInfo)
  {
    this.mTranslateFragment.onCreateContextMenu(paramContextMenu, paramView, paramContextMenuInfo);
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if (this.mSpinnerAdapter != null)
      return this.mSpinnerAdapter.onKeyDown(paramInt, paramKeyEvent);
    return false;
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    Logger.d("TranslateFragmentWrapper", "onOptionsItemSelected");
    int i = paramMenuItem.getItemId();
    boolean bool = false;
    if (i == 16908332)
    {
      Activity localActivity = this.mActivity;
      bool = false;
      if (localActivity != null)
      {
        ActionBarSpinnerAdapter localActionBarSpinnerAdapter = this.mSpinnerAdapter;
        bool = false;
        if (localActionBarSpinnerAdapter != null)
        {
          this.mActivity.getFragmentManager().popBackStack();
          this.mSpinnerAdapter.onNavigationItemSelected(0, i);
          this.mActivity.invalidateOptionsMenu();
          bool = true;
        }
      }
    }
    return bool;
  }

  public void onResume()
  {
    if ((this.mActivity != null) && (this.mSpinnerAdapter != null))
      SdkVersionWrapper.getWrapper().setActionBarTitle(this.mActivity, this.mSpinnerAdapter);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.translation.TranslateFragmentWrapper
 * JD-Core Version:    0.6.2
 */