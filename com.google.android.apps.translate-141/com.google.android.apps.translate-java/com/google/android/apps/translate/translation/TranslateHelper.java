package com.google.android.apps.translate.translation;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.net.Uri;
import android.net.Uri.Builder;
import android.text.TextUtils;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.URLUtil;
import com.google.android.apps.translate.Language;
import com.google.android.apps.translate.Languages;
import com.google.android.apps.translate.LanguagesFactory;
import com.google.android.apps.translate.Logger;
import com.google.android.apps.translate.Profile;
import com.google.android.apps.translate.R.id;
import com.google.android.apps.translate.R.menu;
import com.google.android.apps.translate.R.string;
import com.google.android.apps.translate.SdkVersionWrapper;
import com.google.android.apps.translate.SdkVersionWrapper.WrapperBase;
import com.google.android.apps.translate.SettingsActivity;
import com.google.android.apps.translate.Translate;
import com.google.android.apps.translate.Translate.TranslateResult;
import com.google.android.apps.translate.TranslateApplication;
import com.google.android.apps.translate.UserActivityMgr;
import com.google.android.apps.translate.UserActivityMgr.IntervalCountTag;
import com.google.android.apps.translate.UserActivityMgr.RequestSource;
import com.google.android.apps.translate.Util;
import com.google.android.apps.translate.editor.PreImeAutoCompleteTextView.OnKeyPreImeListener;
import com.google.android.apps.translate.history.Entry;
import java.util.List;
import java.util.Locale;

public class TranslateHelper extends BaseTranslateHelper
  implements PreImeAutoCompleteTextView.OnKeyPreImeListener
{
  private static final String DEFAULT_BROWSER_ACTIVITY = "com.android.browser.BrowserActivity";
  private static final String DEFAULT_BROWSER_PACKAGE = "com.android.browser";
  private static final String TAG = "TranslateHelper";
  private TranslateEntry mCurrentTranslation;
  private InputPanel mInputPanel;
  private TranslateFragmentWrapper mTranslateFragmentWrapper;
  private View mTranslatePanel;

  public TranslateHelper()
  {
    Logger.d("TranslateHelper", "TranslateHelper");
  }

  public static String getAutoTranslationMessage(Context paramContext, Language paramLanguage, String paramString)
  {
    return paramContext.getString(Util.getLocalizedStringId(paramContext, R.string.label_automatic_translation, paramLanguage), new Object[] { paramString });
  }

  private static void getLocalizedLanguageName(Activity paramActivity, Language paramLanguage, Languages paramLanguages, Locale paramLocale, LocalizedLanguageCallback paramLocalizedLanguageCallback)
  {
    Logger.d("TranslateHelper", "sendTranslateResult");
    Object localObject;
    int i;
    if (paramLanguage != null)
    {
      String str1 = paramLanguage.getShortName();
      localObject = paramLanguage.getLongName();
      i = 0;
      if (paramLanguages != null)
      {
        String str2 = paramLanguages.getToLanguageLongName(str1);
        if (str2 == null)
          break label72;
        localObject = str2;
      }
    }
    while (true)
    {
      if (i != 0)
        getLocalizedLanguageNameAfterLoadLanguages(paramActivity, paramLanguage, paramLocale, paramLocalizedLanguageCallback);
      paramLocalizedLanguageCallback.onLanguageNameReceived(paramLanguage, paramLocale, (String)localObject);
      return;
      label72: i = 1;
    }
  }

  public static void getLocalizedLanguageName(Activity paramActivity, Language paramLanguage, LocalizedLanguageCallback paramLocalizedLanguageCallback)
  {
    Logger.d("TranslateHelper", "getLocalizedLanguageName");
    Locale localLocale;
    Languages localLanguages;
    if (paramLanguage != null)
    {
      localLocale = Util.languageShortNameToLocale(paramLanguage.getShortName());
      localLanguages = LanguagesFactory.get().getLanguages(paramActivity, localLocale, false);
      if (localLanguages == null)
        getLocalizedLanguageNameAfterLoadLanguages(paramActivity, paramLanguage, localLocale, paramLocalizedLanguageCallback);
    }
    else
    {
      return;
    }
    getLocalizedLanguageName(paramActivity, paramLanguage, localLanguages, localLocale, paramLocalizedLanguageCallback);
  }

  private static void getLocalizedLanguageNameAfterLoadLanguages(Activity paramActivity, final Language paramLanguage, final Locale paramLocale, final LocalizedLanguageCallback paramLocalizedLanguageCallback)
  {
    Logger.d("TranslateHelper", "getLocalizedLanguageNameAfterLoadLanguages");
    final boolean[] arrayOfBoolean = { false };
    new Thread()
    {
      public void run()
      {
        Util.getLanguagesFromServer(this.val$activity, paramLocale);
        this.val$activity.runOnUiThread(new Runnable()
        {
          public void run()
          {
            if (TranslateHelper.4.this.val$canceled[0] == 0)
            {
              TranslateHelper.4.this.val$dialog.dismiss();
              Languages localLanguages = LanguagesFactory.get().getLanguages(TranslateHelper.4.this.val$activity, TranslateHelper.4.this.val$locale);
              TranslateHelper.getLocalizedLanguageName(TranslateHelper.4.this.val$activity, TranslateHelper.4.this.val$lang, localLanguages, TranslateHelper.4.this.val$locale, TranslateHelper.4.this.val$callback);
            }
          }
        });
      }
    }
    .start();
  }

  public static String getOriginalTextMessage(Context paramContext, Language paramLanguage, String paramString)
  {
    return paramContext.getString(Util.getLocalizedStringId(paramContext, R.string.label_original_text, paramLanguage), new Object[] { paramString });
  }

  private static TranslateEntry getTranslateEntryByAction(Activity paramActivity, Intent paramIntent, Languages paramLanguages)
  {
    Logger.d("TranslateHelper", "getTranslateEntryByAction");
    String str = paramIntent.getAction();
    TranslateEntry localTranslateEntry1 = parseNativeIntent(paramIntent, paramLanguages);
    if ((str == null) || (localTranslateEntry1 != null))
      return localTranslateEntry1;
    if (str.equals("android.intent.action.VIEW"))
    {
      TranslateEntry localTranslateEntry4 = parseTranslateUrl(paramActivity, paramIntent, paramLanguages);
      if (localTranslateEntry4 != null)
        UserActivityMgr.get().setTranslationSource(UserActivityMgr.RequestSource.ACTION_VIEW);
      return localTranslateEntry4;
    }
    if (str.equals("android.intent.action.SEND"))
    {
      TranslateEntry localTranslateEntry3 = parseSentData(paramActivity, paramIntent, paramLanguages);
      if (localTranslateEntry3 != null)
        UserActivityMgr.get().setTranslationSource(UserActivityMgr.RequestSource.ACTION_SEND);
      return localTranslateEntry3;
    }
    if (str.equals("android.intent.action.SEARCH"))
    {
      TranslateEntry localTranslateEntry2 = parseSearchData(paramActivity, paramIntent, paramLanguages);
      if (localTranslateEntry2 != null)
        UserActivityMgr.get().setTranslationSource(UserActivityMgr.RequestSource.ACTION_SEARCH);
      return localTranslateEntry2;
    }
    return null;
  }

  public static Uri getWebTranslateUri(Languages paramLanguages, String paramString1, String paramString2)
  {
    if ((!URLUtil.isValidUrl(paramString2)) || ((!URLUtil.isHttpUrl(paramString2)) && (!URLUtil.isHttpsUrl(paramString2))))
      return null;
    Uri.Builder localBuilder = new Uri.Builder();
    localBuilder.scheme("http").authority("translate.google.com").path("translate").appendQueryParameter("sl", "auto").appendQueryParameter("u", paramString2).fragment("");
    if (!TextUtils.isEmpty(paramString1))
    {
      localBuilder.appendQueryParameter("hl", paramString1);
      Language localLanguage = paramLanguages.getToLanguageByShortName(paramString1);
      if (localLanguage != null)
        localBuilder.appendQueryParameter("tl", localLanguage.getShortName());
    }
    return localBuilder.build();
  }

  static boolean onCreateOptionsMenu(Activity paramActivity, Menu paramMenu, MenuInflater paramMenuInflater)
  {
    Logger.d("TranslateHelper", "onCreateOptionsMenu");
    paramMenuInflater.inflate(R.menu.translate_activity_menu, paramMenu);
    if (SdkVersionWrapper.getWrapper().useFragments())
    {
      paramMenu.findItem(R.id.menu_conversation_mode).setVisible(false);
      paramMenu.findItem(R.id.menu_history).setVisible(false);
      paramMenu.findItem(R.id.menu_favorite).setVisible(false);
    }
    paramMenu.findItem(R.id.menu_sms_translation).setVisible(Util.hasSmsSupport(paramActivity));
    return false;
  }

  static boolean onPrepareOptionsMenu(Menu paramMenu)
  {
    Logger.d("TranslateHelper", "onPrepareOptionsMenu");
    return false;
  }

  private static TranslateEntry parseCgiParams(Uri paramUri, Languages paramLanguages)
  {
    String str1 = paramUri.getQueryParameter("sl");
    if (TextUtils.isEmpty(str1))
      str1 = "auto";
    String str2 = paramUri.getQueryParameter("tl");
    if (TextUtils.isEmpty(str2))
      str2 = Util.getLanguageShortNameByLocale();
    return TranslateEntry.build(str1, str2, paramUri.getQueryParameter("q"), null, Util.isAutoDetectLanguage(str1), paramLanguages);
  }

  private static TranslateEntry parseFragmentParams(Uri paramUri, Languages paramLanguages)
  {
    Logger.d("TranslateHelper", "parseFragmentParams");
    String str = paramUri.getFragment();
    boolean bool = TextUtils.isEmpty(str);
    TranslateEntry localTranslateEntry = null;
    if (!bool)
    {
      String[] arrayOfString = str.split("\\|", 3);
      int i = arrayOfString.length;
      localTranslateEntry = null;
      if (i == 3)
        localTranslateEntry = TranslateEntry.build(arrayOfString[0], arrayOfString[1], arrayOfString[2], null, Util.isAutoDetectLanguage(arrayOfString[0]), paramLanguages);
    }
    return localTranslateEntry;
  }

  private static TranslateEntry parseNativeIntent(Intent paramIntent, Languages paramLanguages)
  {
    String str = paramIntent.getStringExtra("key_language_from");
    return TranslateEntry.build(str, paramIntent.getStringExtra("key_language_to"), paramIntent.getStringExtra("key_text_input"), paramIntent.getStringExtra("key_text_output"), Util.isAutoDetectLanguage(str), paramLanguages);
  }

  private static TranslateEntry parseSearchData(Activity paramActivity, Intent paramIntent, Languages paramLanguages)
  {
    Logger.d("TranslateHelper", "parseSearchData");
    if ((paramIntent.getAction() == null) || (!paramIntent.getAction().equals("android.intent.action.SEARCH")))
      return null;
    String str = paramIntent.getStringExtra("query");
    if (str == null)
      str = paramIntent.getStringExtra("user_query");
    return prepareForAutoTranslation(paramActivity, str, paramLanguages);
  }

  private static TranslateEntry parseSentData(Activity paramActivity, Intent paramIntent, Languages paramLanguages)
  {
    Logger.d("TranslateHelper", "parseSentData");
    if ((paramIntent.getAction() == null) || (!paramIntent.getAction().equals("android.intent.action.SEND")))
      return null;
    return prepareForAutoTranslation(paramActivity, paramIntent.getStringExtra("android.intent.extra.TEXT"), paramLanguages);
  }

  private static TranslateEntry parseTranslateUrl(Activity paramActivity, Intent paramIntent, Languages paramLanguages)
  {
    Logger.d("TranslateHelper", "parseTranslateUrl");
    TranslateEntry localTranslateEntry;
    if ((paramIntent.getAction() == null) || (!paramIntent.getAction().equals("android.intent.action.VIEW")) || (TextUtils.isEmpty(paramIntent.getDataString())))
      localTranslateEntry = null;
    do
    {
      return localTranslateEntry;
      Uri localUri = Uri.parse(paramIntent.getDataString());
      localTranslateEntry = parseCgiParams(localUri, paramLanguages);
      if (localTranslateEntry == null)
        localTranslateEntry = parseFragmentParams(localUri, paramLanguages);
    }
    while (localTranslateEntry != null);
    Logger.d("TranslateHelper", "parseTranslateUrl fall back to Translate web app.");
    startWebTranslate(paramActivity, Uri.parse(paramIntent.getDataString()));
    return null;
  }

  public static void performChipActionShare(Activity paramActivity, Languages paramLanguages, Entry paramEntry, ChipView.ChipPart paramChipPart)
  {
    Translate.TranslateResult localTranslateResult = new Translate.TranslateResult(paramEntry);
    TranslateEntry localTranslateEntry = new TranslateEntry(paramLanguages.getFromLanguageByShortName(paramEntry.getFromLanguageShortName()), paramLanguages.getToLanguageByShortName(paramEntry.getToLanguageShortName()), paramEntry.getInputText(), paramEntry.getOutputText(), Util.isAutoDetectLanguage(paramEntry.getFromLanguageShortName()));
    switch (6.$SwitchMap$com$google$android$apps$translate$translation$ChipView$ChipPart[paramChipPart.ordinal()])
    {
    default:
      sendTranslateResult(paramActivity, localTranslateEntry);
      UserActivityMgr.get().incrementIntervalCount(UserActivityMgr.IntervalCountTag.CHIPVIEW_TRG_SHARE_CLICKS, 1);
      return;
    case 1:
    case 2:
      sendText(paramActivity, localTranslateEntry.inputText);
      UserActivityMgr.get().incrementIntervalCount(UserActivityMgr.IntervalCountTag.CHIPVIEW_SRC_SHARE_CLICKS, 1);
      return;
    case 3:
    }
    sendText(paramActivity, localTranslateResult.getTranslateText());
    UserActivityMgr.get().incrementIntervalCount(UserActivityMgr.IntervalCountTag.CHIPVIEW_TRG_SHARE_CLICKS, 1);
  }

  private static TranslateEntry prepareForAutoTranslation(Activity paramActivity, String paramString, Languages paramLanguages)
  {
    Logger.d("TranslateHelper", "prepareForAutoTranslation inputText=" + paramString);
    String str = TranslateEntry.normalizeInputText(paramString);
    if (TextUtils.isEmpty(str))
      return null;
    Uri localUri = getWebTranslateUri(paramLanguages, Util.getLanguageShortNameByLocale(), str);
    if (localUri != null)
    {
      Logger.d("TranslateHelper", "prepareForAutoTranslation uri=" + localUri.toString());
      startWebTranslate(paramActivity, localUri);
      return null;
    }
    return new TranslateEntry(null, null, str, null, false);
  }

  public static TranslateEntry readIntentData(Activity paramActivity, Intent paramIntent, Languages paramLanguages)
  {
    Logger.d("TranslateHelper", "readIntentData");
    if (paramIntent == null)
    {
      Logger.e("no intent, finish activity");
      return null;
    }
    TranslateEntry localTranslateEntry = getTranslateEntryByAction(paramActivity, paramIntent, paramLanguages);
    if (localTranslateEntry == null)
    {
      Logger.e("no input text or from/to languages");
      return null;
    }
    Profile.setLanguagePair(paramActivity, localTranslateEntry.fromLanguage, localTranslateEntry.toLanguage);
    return localTranslateEntry;
  }

  public static void sendText(Activity paramActivity, String paramString)
  {
    Logger.d("TranslateHelper", "sendText");
    if (paramString != null)
    {
      Intent localIntent = new Intent("android.intent.action.SEND");
      localIntent.setType("text/plain");
      localIntent.putExtra("android.intent.extra.TEXT", paramString);
      paramActivity.startActivity(Intent.createChooser(localIntent, paramActivity.getString(R.string.title_send_chooser)));
    }
  }

  public static void sendTranslateResult(Activity paramActivity, TranslateEntry paramTranslateEntry)
  {
    Logger.d("TranslateHelper", "sendTranslateResult");
    Locale localLocale;
    Languages localLanguages;
    if (paramTranslateEntry != null)
    {
      localLocale = Util.languageShortNameToLocale(paramTranslateEntry.toLanguage.getShortName());
      localLanguages = LanguagesFactory.get().getLanguages(paramActivity, localLocale, false);
      if (localLanguages == null)
        sendTranslateResultAfterLoadLanguages(paramActivity, paramTranslateEntry, localLocale);
    }
    else
    {
      return;
    }
    sendTranslateResult(paramActivity, paramTranslateEntry, localLanguages, localLocale);
  }

  private static void sendTranslateResult(Activity paramActivity, TranslateEntry paramTranslateEntry, Languages paramLanguages, Locale paramLocale)
  {
    Logger.d("TranslateHelper", "sendTranslateResult");
    Object localObject1;
    Object localObject2;
    int i;
    if (paramTranslateEntry != null)
    {
      String str1 = paramTranslateEntry.fromLanguage.getShortName();
      String str2 = paramTranslateEntry.toLanguage.getShortName();
      localObject1 = paramTranslateEntry.fromLanguage.getLongName();
      localObject2 = paramTranslateEntry.toLanguage.getLongName();
      i = 0;
      if (paramLanguages != null)
      {
        String str4 = paramLanguages.getToLanguageLongName(str1);
        if (str4 == null)
          break label100;
        localObject1 = str4;
        String str5 = paramLanguages.getToLanguageLongName(str2);
        if (str5 == null)
          break label106;
        localObject2 = str5;
      }
    }
    while (true)
    {
      if (i == 0)
        break label112;
      sendTranslateResultAfterLoadLanguages(paramActivity, paramTranslateEntry, paramLocale);
      return;
      label100: i = 1;
      break;
      label106: i = 1;
    }
    label112: Translate.TranslateResult localTranslateResult = new Translate.TranslateResult(paramTranslateEntry.outputText);
    Language localLanguage = paramTranslateEntry.toLanguage;
    String str3 = getAutoTranslationMessage(paramActivity, localLanguage, (String)localObject2) + "\n" + localTranslateResult.getTranslateText() + "\n\n" + getOriginalTextMessage(paramActivity, localLanguage, (String)localObject1) + "\n" + paramTranslateEntry.inputText;
    Intent localIntent = new Intent("android.intent.action.SEND");
    localIntent.setType("text/plain");
    localIntent.putExtra("android.intent.extra.SUBJECT", paramActivity.getString(Util.getLocalizedStringId(paramActivity, R.string.msg_send_translation_subject, paramTranslateEntry.toLanguage)));
    localIntent.putExtra("android.intent.extra.TEXT", str3);
    paramActivity.startActivity(Intent.createChooser(localIntent, paramActivity.getString(R.string.title_send_chooser)));
  }

  private static void sendTranslateResultAfterLoadLanguages(Activity paramActivity, final TranslateEntry paramTranslateEntry, final Locale paramLocale)
  {
    Logger.d("TranslateHelper", "sendTranslateResultAfterLoadLanguages");
    final boolean[] arrayOfBoolean = { false };
    new Thread()
    {
      public void run()
      {
        Util.getLanguagesFromServer(this.val$activity, paramLocale);
        this.val$activity.runOnUiThread(new Runnable()
        {
          public void run()
          {
            if (TranslateHelper.2.this.val$canceled[0] == 0)
            {
              TranslateHelper.2.this.val$dialog.dismiss();
              Languages localLanguages = LanguagesFactory.get().getLanguages(TranslateHelper.2.this.val$activity, TranslateHelper.2.this.val$locale);
              TranslateHelper.sendTranslateResult(TranslateHelper.2.this.val$activity, TranslateHelper.2.this.val$translateEntry, localLanguages, TranslateHelper.2.this.val$locale);
            }
          }
        });
      }
    }
    .start();
  }

  private static void startWebTranslate(Activity paramActivity, Uri paramUri)
  {
    Logger.d("TranslateHelper", "startWebTranslate");
    Intent localIntent1 = new Intent("android.intent.action.VIEW");
    localIntent1.addCategory("android.intent.category.DEFAULT");
    localIntent1.addCategory("android.intent.category.BROWSABLE");
    localIntent1.setDataAndType(paramUri, "text/html");
    Intent localIntent2 = new Intent();
    localIntent2.setClassName("com.android.browser", "com.android.browser.BrowserActivity");
    List localList = paramActivity.getPackageManager().queryIntentActivityOptions(paramActivity.getComponentName(), new Intent[] { localIntent2 }, localIntent1, 65536);
    if (localList.size() > 0)
      localIntent1.setClassName(((ResolveInfo)localList.get(0)).activityInfo.packageName, ((ResolveInfo)localList.get(0)).activityInfo.name);
    try
    {
      paramActivity.startActivity(localIntent1);
      UserActivityMgr.get().incrementIntervalCount(UserActivityMgr.IntervalCountTag.WEB_TRANSLATION_REQUESTS, 1);
      return;
    }
    catch (ActivityNotFoundException localActivityNotFoundException)
    {
      while (true)
      {
        Logger.e(R.string.msg_error_intent_web_translate + " " + localActivityNotFoundException.getMessage());
        Util.showLongToastMessage(paramActivity, R.string.msg_error_intent_web_translate);
      }
    }
  }

  public void finish()
  {
    if (this.mActivity.getIntent().getBooleanExtra("key_no_transition_animation", false))
      SdkVersionWrapper.getWrapper().removeTransitionAnimation(this.mActivity);
  }

  public View getContextMenuTargetView()
  {
    return this.mInputPanel.getContextMenuTargetView();
  }

  TranslateEntry getCurrentTranslation()
  {
    return this.mCurrentTranslation;
  }

  public TranslateEntry getInitialTranslationEntry()
  {
    TranslateEntry localTranslateEntry = this.mCurrentTranslation;
    boolean bool1 = false;
    if (localTranslateEntry != null)
    {
      boolean bool2 = this.mCurrentTranslation.hasInputText();
      bool1 = false;
      if (bool2)
      {
        boolean bool3 = this.mCurrentTranslation.hasSourceAndTargetLanguages();
        bool1 = false;
        if (bool3)
          if (this.mInputPanel != null)
            break label121;
      }
    }
    Language localLanguage1;
    String str1;
    label121: for (String str2 = ""; ; str2 = TranslateEntry.normalizeInputText(this.mInputPanel.getInputText()))
    {
      bool1 = TextUtils.equals(this.mCurrentTranslation.inputText, str2);
      if (!bool1)
        break;
      localLanguage1 = getToLanguageGivenFromLanguage(this.mCurrentTranslation.fromLanguage, true);
      localLanguage2 = this.mCurrentTranslation.toLanguage;
      str1 = this.mCurrentTranslation.inputText;
      return new TranslateEntry(localLanguage1, localLanguage2, str1, "", Util.isAutoDetectLanguage(localLanguage1));
    }
    if (this.mInputPanel == null)
      localLanguage1 = null;
    for (Language localLanguage2 = null; ; localLanguage2 = this.mInputPanel.getToLanguage())
    {
      str1 = "";
      break;
      localLanguage1 = getToLanguageGivenFromLanguage(this.mInputPanel.getFromLanguage(), true);
    }
  }

  public String getInputText()
  {
    StringBuilder localStringBuilder = new StringBuilder().append("getInputText mInputPanel is null? ");
    if (this.mInputPanel == null);
    for (boolean bool = true; ; bool = false)
    {
      Logger.d("TranslateHelper", bool);
      if (this.mInputPanel == null)
        break;
      return this.mInputPanel.getInputText();
    }
    return "";
  }

  public boolean isInEditingMode()
  {
    if (this.mInputPanel != null)
      return this.mInputPanel.isInEditingMode();
    return false;
  }

  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    if ((paramInt2 != -1) || (paramIntent == null));
    while (this.mInputPanel == null)
      return;
    setCurrentTranslation(null);
    this.mInputPanel.onActivityResult(paramInt1, paramInt2, paramIntent);
  }

  public boolean onConfigurationChanged(Configuration paramConfiguration)
  {
    Logger.d("TranslateHelper", "onConfigurationChanged");
    if (this.mInputPanel == null);
    while ((paramConfiguration.orientation != 2) && (paramConfiguration.orientation != 1))
      return false;
    this.mInputPanel.onConfigurationChanged(paramConfiguration);
    return false;
  }

  public boolean onContextItemSelected(MenuItem paramMenuItem)
  {
    int i = paramMenuItem.getItemId();
    if (i == R.id.context_menu_translate_input_text)
    {
      this.mInputPanel.setLanguageList(this.mLanguageList);
      this.mInputPanel.languagePairSelected(this.mCurrentTranslation.fromLanguage, this.mCurrentTranslation.toLanguage, false, false);
      this.mInputPanel.setTextAndDoTranslate(this.mCurrentTranslation.inputText, "");
      UserActivityMgr.get().incrementIntervalCount(UserActivityMgr.IntervalCountTag.CHIPVIEW_SRC_TRANSLATE_CLICKS, 1);
      return true;
    }
    if (i == R.id.context_menu_translate_translation)
    {
      this.mInputPanel.setLanguageList(this.mLanguageList);
      String str = this.mCurrentTranslation.toNewEntry().getTranslation();
      this.mInputPanel.setInputText(str);
      this.mInputPanel.languagePairSelected(getFromLanguageGivenToLanguage(this.mCurrentTranslation.toLanguage, false), getToLanguageGivenFromLanguage(this.mCurrentTranslation.fromLanguage, false), false, false);
      this.mInputPanel.setTextAndDoTranslate(str, "");
      UserActivityMgr.get().incrementIntervalCount(UserActivityMgr.IntervalCountTag.CHIPVIEW_TRG_TRANSLATE_CLICKS, 1);
      return true;
    }
    if (i == R.id.context_menu_copy_input_text)
    {
      Util.copyToClipBoard(this.mActivity, this.mCurrentTranslation.toNewEntry().getInputText(), ChipView.ChipPart.INPUT_TEXT);
      Util.showShortToastMessage(this.mActivity, R.string.toast_message_copy_input_text);
      return true;
    }
    if (i == R.id.context_menu_copy)
    {
      Translate.TranslateResult localTranslateResult1 = new Translate.TranslateResult(this.mCurrentTranslation.toNewEntry());
      Util.copyToClipBoard(this.mActivity, localTranslateResult1.getTranslateText(), ChipView.ChipPart.TRANSLATION_TEXT);
      Util.showShortToastMessage(this.mActivity, R.string.toast_message_copy);
      return true;
    }
    if (i == R.id.context_menu_share)
    {
      Entry localEntry2 = this.mCurrentTranslation.toNewEntry();
      if (this.mInputPanel != null)
      {
        performChipActionShare(this.mActivity, this.mLanguageList, localEntry2, this.mInputPanel.getSelectedChipView());
        return true;
      }
      performChipActionShare(this.mActivity, this.mLanguageList, localEntry2, ChipView.ChipPart.NONE);
      return true;
    }
    if (i == R.id.context_menu_search_input_text)
    {
      Entry localEntry1 = this.mCurrentTranslation.toNewEntry();
      Util.searchTextOnWeb(this.mActivity, localEntry1.getInputText(), ChipView.ChipPart.INPUT_TEXT);
      return true;
    }
    if (i == R.id.context_menu_search_translation)
    {
      this.mCurrentTranslation.toNewEntry();
      Translate.TranslateResult localTranslateResult2 = new Translate.TranslateResult(this.mCurrentTranslation.toNewEntry());
      Util.searchTextOnWeb(this.mActivity, localTranslateResult2.getTranslateText(), ChipView.ChipPart.TRANSLATION_TEXT);
      return true;
    }
    return false;
  }

  public void onCreate(Activity paramActivity, Languages paramLanguages, View paramView, TranslateFragmentWrapper paramTranslateFragmentWrapper)
  {
    Logger.d("TranslateHelper", "onCreate");
    super.onCreate(paramActivity, paramLanguages);
    this.mActivity.setVolumeControlStream(3);
    this.mLanguageList = paramLanguages;
    this.mTranslatePanel = paramView;
    this.mTranslateFragmentWrapper = paramTranslateFragmentWrapper;
    TranslateEntry localTranslateEntry1 = this.mCurrentTranslation;
    int i = 0;
    if (localTranslateEntry1 == null)
    {
      this.mCurrentTranslation = readIntentData(this.mActivity, this.mActivity.getIntent(), this.mLanguageList);
      TranslateEntry localTranslateEntry2 = this.mCurrentTranslation;
      i = 0;
      if (localTranslateEntry2 != null)
        i = 1;
    }
    if (this.mCurrentTranslation == null)
      this.mCurrentTranslation = Profile.getLastTranslation(this.mActivity, this.mLanguageList);
    if (i != 0)
      setInputPanel(true, true);
    while (true)
    {
      ((TranslateApplication)this.mActivity.getApplication()).resetAppCsiTimer();
      return;
      this.mInputPanel = new InputPanel(this.mActivity, this.mLanguageList, this.mTranslatePanel, this);
    }
  }

  public void onCreateContextMenu(ContextMenu paramContextMenu, View paramView, ContextMenu.ContextMenuInfo paramContextMenuInfo)
  {
    this.mActivity.getMenuInflater().inflate(R.menu.history_activity_context_menu, paramContextMenu);
    if ((this.mInputPanel != null) && (this.mCurrentTranslation != null))
      switch (6.$SwitchMap$com$google$android$apps$translate$translation$ChipView$ChipPart[this.mInputPanel.getSelectedChipView().ordinal()])
      {
      default:
      case 2:
      case 3:
      }
    do
    {
      do
      {
        paramContextMenu.clear();
        return;
      }
      while (TextUtils.isEmpty(this.mCurrentTranslation.inputText));
      paramContextMenu.removeItem(R.id.context_menu_translate_translation);
      paramContextMenu.removeItem(R.id.context_menu_remove);
      paramContextMenu.removeItem(R.id.context_menu_copy);
      paramContextMenu.removeItem(R.id.context_menu_search_translation);
      paramContextMenu.setHeaderTitle(this.mCurrentTranslation.inputText);
      return;
    }
    while (this.mCurrentTranslation.isInvalid());
    paramContextMenu.removeItem(R.id.context_menu_translate_input_text);
    paramContextMenu.removeItem(R.id.context_menu_remove);
    paramContextMenu.removeItem(R.id.context_menu_copy_input_text);
    paramContextMenu.removeItem(R.id.context_menu_search_input_text);
    paramContextMenu.setHeaderTitle(this.mCurrentTranslation.toNewEntry().getTranslation());
  }

  public void onDestroy()
  {
    Logger.d("TranslateHelper", "onDestroy");
    if (this.mInputPanel != null)
      this.mInputPanel.onDestroy();
  }

  public boolean onKeyPreIme(int paramInt, KeyEvent paramKeyEvent)
  {
    boolean bool2;
    if ((this.mInputPanel != null) && (this.mInputPanel.onKeyPreIme(paramInt, paramKeyEvent)))
      bool2 = true;
    boolean bool1;
    do
    {
      return bool2;
      bool1 = Util.isFloatingActivity(this.mActivity);
      bool2 = false;
    }
    while (!bool1);
    return false;
  }

  public void onLanguagesChanged()
  {
    Logger.d("TranslateHelper", "onLanguagesChanged");
    if (this.mActivity == null)
      Logger.d("TranslateHelper", "onLanguagesChanged not changed!");
    while (true)
    {
      return;
      this.mLanguageList = LanguagesFactory.get().getLanguages(this.mActivity, Locale.getDefault());
      TranslateEntry localTranslateEntry = this.mCurrentTranslation;
      Language localLanguage1 = null;
      Language localLanguage2 = null;
      if (localTranslateEntry != null)
      {
        boolean bool = this.mCurrentTranslation.isInvalid();
        localLanguage1 = null;
        localLanguage2 = null;
        if (!bool)
        {
          localLanguage1 = this.mCurrentTranslation.getFromLanguageForLanguagePicker(this.mLanguageList);
          localLanguage2 = this.mCurrentTranslation.toLanguage;
        }
      }
      Language[] arrayOfLanguage;
      if ((localLanguage1 == null) || (localLanguage2 == null))
      {
        arrayOfLanguage = Profile.getLanguagePair(this.mActivity, this.mLanguageList);
        if ((arrayOfLanguage[0] == null) || (arrayOfLanguage[1] == null))
          break label167;
        localLanguage1 = arrayOfLanguage[0];
      }
      for (localLanguage2 = arrayOfLanguage[1]; this.mInputPanel != null; localLanguage2 = this.mLanguageList.getDefaultToLanguage())
      {
        this.mInputPanel.setLanguageList(this.mLanguageList);
        this.mInputPanel.languagePairSelected(localLanguage1, localLanguage2, false, false);
        return;
        label167: localLanguage1 = this.mLanguageList.getDefaultFromLanguage();
      }
    }
  }

  void onNewIntent(Intent paramIntent)
  {
    Logger.d("TranslateHelper", "onNewIntent");
    setTranslateEntry(readIntentData(this.mActivity, paramIntent, this.mLanguageList), true, true, true);
  }

  boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    int i = paramMenuItem.getItemId();
    if (i == R.id.menu_history)
    {
      Util.openHistoryActivity(this.mActivity);
      return true;
    }
    if (i == R.id.menu_favorite)
    {
      Util.openFavoriteActivity(this.mActivity);
      return true;
    }
    if (i == R.id.menu_conversation_mode)
    {
      Util.openConversationActivity(this.mActivity, getInitialTranslationEntry());
      return true;
    }
    if (i == R.id.menu_sms_translation)
      Util.openSmsTranslationActivity(this.mActivity);
    do
    {
      return false;
      if (i == R.id.menu_feedback)
      {
        SdkVersionWrapper.getWrapper().sendFeedback(this.mActivity, true);
        return true;
      }
      if (i == R.id.menu_settings)
      {
        this.mActivity.startActivity(new Intent(this.mActivity, SettingsActivity.class));
        return true;
      }
    }
    while (i != 16908332);
    Util.openHomeActivity(this.mActivity);
    return true;
  }

  public void onPause()
  {
    super.onPause();
    if (this.mInputPanel != null)
      this.mInputPanel.onPause();
  }

  public void onResume()
  {
    Logger.d("TranslateHelper", "onResume");
    super.onResume();
    if (this.mInputPanel != null)
      this.mInputPanel.onResume(this.mCurrentTranslation);
  }

  public void onStart()
  {
    Translate.setAcceptLanguage(Locale.getDefault().toString());
    if (this.mInputPanel != null)
      this.mInputPanel.onStart();
  }

  public void onStop()
  {
    if (this.mActivity != null)
      Profile.setLastTranslation(this.mActivity, this.mCurrentTranslation);
  }

  public void setCurrentTranslation(TranslateEntry paramTranslateEntry)
  {
    Logger.d("TranslateHelper", "setCurrentTranslation");
    setTranslateEntry(paramTranslateEntry, false, false, true);
    this.mInputPanel.setTranslationResult(this.mCurrentTranslation, true);
  }

  void setInputPanel(final boolean paramBoolean1, final boolean paramBoolean2)
  {
    Logger.d("TranslateHelper", "setInputPanel");
    this.mActivity.runOnUiThread(new Runnable()
    {
      public void run()
      {
        if (TranslateHelper.this.mInputPanel == null)
        {
          TranslateHelper.access$202(TranslateHelper.this, new InputPanel(TranslateHelper.this.mActivity, TranslateHelper.this.mLanguageList, TranslateHelper.this.mTranslatePanel, TranslateHelper.this));
          TranslateHelper.this.mInputPanel.setLanguageList(TranslateHelper.this.mLanguageList);
        }
        if (TranslateHelper.this.mCurrentTranslation != null)
        {
          if (paramBoolean2)
            TranslateHelper.this.mInputPanel.setInputText(TranslateHelper.this.mCurrentTranslation.inputText);
          if ((TranslateHelper.this.mCurrentTranslation.fromLanguage != null) && (TranslateHelper.this.mCurrentTranslation.toLanguage != null))
            TranslateHelper.this.mInputPanel.languagePairSelected(TranslateHelper.this.mCurrentTranslation.getFromLanguageForLanguagePicker(TranslateHelper.this.mLanguageList), TranslateHelper.this.mCurrentTranslation.toLanguage, false, false);
          if (paramBoolean1)
            TranslateHelper.this.mInputPanel.setTextAndDoTranslate(TranslateHelper.this.mCurrentTranslation.inputText, TranslateHelper.this.mCurrentTranslation.outputText);
        }
        else
        {
          return;
        }
        TranslateHelper.this.mInputPanel.setTranslationResult(TranslateHelper.this.mCurrentTranslation, paramBoolean2);
      }
    });
  }

  public void setLanguageList(Languages paramLanguages)
  {
    this.mInputPanel.setLanguageList(paramLanguages);
  }

  public void setTextAndDoTranslate(String paramString)
  {
    this.mInputPanel.setTextAndDoTranslate(paramString);
  }

  void setTranslateEntry(TranslateEntry paramTranslateEntry, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3)
  {
    try
    {
      this.mCurrentTranslation = paramTranslateEntry;
      if (paramBoolean1)
        setInputPanel(paramBoolean2, paramBoolean3);
      return;
    }
    finally
    {
    }
  }

  public static abstract interface LocalizedLanguageCallback
  {
    public abstract void onLanguageNameReceived(Language paramLanguage, Locale paramLocale, String paramString);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.translation.TranslateHelper
 * JD-Core Version:    0.6.2
 */