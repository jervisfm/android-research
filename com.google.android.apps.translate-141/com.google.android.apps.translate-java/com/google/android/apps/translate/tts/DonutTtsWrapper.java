package com.google.android.apps.translate.tts;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build.VERSION;
import com.google.android.apps.translate.Constants;
import com.google.android.apps.translate.Sets;
import com.google.tts.TextToSpeechBeta;
import com.google.tts.TextToSpeechBeta.OnInitListener;
import java.util.HashSet;
import java.util.Locale;

public class DonutTtsWrapper
{
  private static final String ESPEAK_ENGINE_PACKAGE = "com.marvin.espeak";
  private static final boolean NEW_PLATFORM = false;
  private static final String PICO_ENGINE_PACKAGE = "com.svox.pico";
  private static final HashSet<String> PICO_LANG_SET;
  private static final String TTS_EXTENDED_PACKAGE = "com.google.tts";
  private DonutTtsCallback mCallback;
  private final String mEspeakEngine;
  private final boolean mIsEspeakEngineInstalled;
  private final boolean mIsTtsExtendedInstalled;
  private TextToSpeechBeta mTts;

  static
  {
    boolean bool;
    if (Integer.parseInt(Build.VERSION.SDK) >= 8)
      bool = true;
    while (true)
    {
      NEW_PLATFORM = bool;
      PICO_LANG_SET = Sets.newHashSet();
      setPicoLanguagesMap();
      try
      {
        Class.forName("android.speech.tts.TextToSpeech");
        return;
        bool = false;
      }
      catch (Exception localException)
      {
        throw new RuntimeException(localException);
      }
    }
  }

  public DonutTtsWrapper(Context paramContext, DonutTtsCallback paramDonutTtsCallback)
  {
    this.mCallback = paramDonutTtsCallback;
    this.mTts = new TextToSpeechBeta(paramContext, new TextToSpeechBeta.OnInitListener()
    {
      public void onInit(int paramAnonymousInt1, int paramAnonymousInt2)
      {
        DonutTtsCallback localDonutTtsCallback;
        if (DonutTtsWrapper.this.mCallback != null)
        {
          localDonutTtsCallback = DonutTtsWrapper.this.mCallback;
          if (paramAnonymousInt1 != 0)
            break label34;
        }
        label34: for (boolean bool = true; ; bool = false)
        {
          localDonutTtsCallback.onInit(bool);
          return;
        }
      }
    });
    if (NEW_PLATFORM);
    for (String str = "com.marvin.espeak"; ; str = "com.google.tts")
    {
      this.mEspeakEngine = str;
      this.mIsTtsExtendedInstalled = isTtsExtendedInstalled(paramContext);
      this.mIsEspeakEngineInstalled = isEspeakEngineInstalled(paramContext);
      return;
    }
  }

  public static void checkAvailable()
  {
  }

  public static boolean isEspeakEngineInstalled(Context paramContext)
  {
    if (!NEW_PLATFORM)
      return false;
    PackageManager localPackageManager = paramContext.getPackageManager();
    try
    {
      localPackageManager.getPackageInfo("com.marvin.espeak", 0);
      return true;
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
    }
    return false;
  }

  private boolean isLanguageAvailable(Locale paramLocale, String paramString)
  {
    this.mTts.setEngineByPackageNameExtended(paramString);
    this.mTts.setLanguage(paramLocale);
    int i = this.mTts.isLanguageAvailable(paramLocale);
    return (i != -1) && (i != -2);
  }

  private static boolean isPicoSupportedLanguage(String paramString)
  {
    return PICO_LANG_SET.contains(paramString);
  }

  public static boolean isTtsExtendedInstalled(Context paramContext)
  {
    return (!NEW_PLATFORM) && (TextToSpeechBeta.isInstalled(paramContext));
  }

  private static void setPicoLanguagesMap()
  {
    for (String str : Constants.PICO_SUPPORTED_LANGUAGES)
      PICO_LANG_SET.add(str);
  }

  public boolean isLanguageAvailable(Locale paramLocale)
  {
    if (this.mTts == null);
    do
    {
      do
      {
        return false;
        if ((!this.mIsTtsExtendedInstalled) && (!NEW_PLATFORM))
          break;
        String str = this.mTts.getDefaultEngineExtended();
        if (str != null)
        {
          if (this.mTts.areDefaultsEnforcedExtended())
            return true;
          if ((!str.equals("com.svox.pico")) && (!str.equals(this.mEspeakEngine)) && (isLanguageAvailable(paramLocale, str)))
            return true;
        }
        if (isLanguageAvailable(paramLocale, "com.svox.pico"))
          return true;
      }
      while ((!this.mIsTtsExtendedInstalled) && (!this.mIsEspeakEngineInstalled));
      return isLanguageAvailable(paramLocale, this.mEspeakEngine);
    }
    while (!isPicoSupportedLanguage(paramLocale.getLanguage()));
    return true;
  }

  public void shutdown()
  {
    if (this.mTts != null)
    {
      this.mTts.shutdown();
      this.mTts = null;
    }
  }

  public void speak(Locale paramLocale, String paramString, int paramInt)
  {
    if ((this.mIsTtsExtendedInstalled) || (NEW_PLATFORM))
    {
      String str = this.mTts.getDefaultEngineExtended();
      if (str != null)
      {
        boolean bool = this.mTts.areDefaultsEnforcedExtended();
        if (((bool) || ((!str.equals("com.svox.pico")) && (!str.equals(this.mEspeakEngine)))) && ((bool) || (isLanguageAvailable(paramLocale, str))))
        {
          this.mTts.setEngineByPackageNameExtended(str);
          this.mTts.setLanguage(paramLocale);
          this.mTts.speak(paramString, paramInt, null);
          return;
        }
      }
      if (!isLanguageAvailable(paramLocale, "com.svox.pico"))
        break label150;
      this.mTts.setEngineByPackageNameExtended("com.svox.pico");
    }
    while (true)
    {
      this.mTts.setLanguage(paramLocale);
      this.mTts.speak(paramString, paramInt, null);
      return;
      label150: if ((!this.mIsTtsExtendedInstalled) && (!this.mIsEspeakEngineInstalled))
        break;
      this.mTts.setEngineByPackageNameExtended(this.mEspeakEngine);
    }
  }

  public void stop()
  {
    this.mTts.stop();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.tts.DonutTtsWrapper
 * JD-Core Version:    0.6.2
 */