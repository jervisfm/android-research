package com.google.android.apps.translate.tts;

import android.app.Activity;
import android.content.Context;
import android.media.AudioManager;
import com.google.android.apps.translate.Language;
import com.google.android.apps.translate.Logger;
import com.google.android.apps.translate.Profile;
import com.google.android.apps.translate.R.string;
import com.google.android.apps.translate.Util;
import java.util.Locale;

public class MyTts
  implements DonutTtsCallback
{
  private static final String TAG = "MyTts";
  private static boolean sHasDonutTts;
  private Context mContext;
  private DonutTtsCallback mDonutCallback;
  private DonutTtsWrapper mDonutTts;
  private NetworkTts mNetworkTts;

  static
  {
    try
    {
      DonutTtsWrapper.checkAvailable();
      sHasDonutTts = true;
      return;
    }
    catch (Throwable localThrowable)
    {
      sHasDonutTts = false;
    }
  }

  public MyTts(Context paramContext)
  {
    this.mContext = paramContext;
    init();
  }

  public static String getTtsSpeakingMessage(Context paramContext, String paramString)
  {
    return String.format(paramContext.getString(R.string.msg_speaking), new Object[] { paramString });
  }

  private boolean isNativeTtsAvailable(Locale paramLocale)
  {
    return (sHasDonutTts) && (this.mDonutTts.isLanguageAvailable(paramLocale));
  }

  public void init()
  {
    this.mNetworkTts = new NetworkTts();
    if (sHasDonutTts)
    {
      Logger.d("new Donut TTS");
      this.mDonutTts = new DonutTtsWrapper(this.mContext, this);
    }
  }

  public boolean isLanguageAvailable(Locale paramLocale)
  {
    if ((Profile.getPreferNetworkTts(this.mContext)) && (NetworkTts.isLanguageAvailable(paramLocale)))
      return true;
    Logger.d("is language available (Donut) for locale: " + paramLocale.toString());
    return isNativeTtsAvailable(paramLocale);
  }

  public void onInit(boolean paramBoolean)
  {
    if (this.mDonutCallback != null)
      this.mDonutCallback.onInit(paramBoolean);
  }

  public void prefetch(Locale paramLocale, String paramString)
  {
    if (Profile.getPreferNetworkTts(this.mContext))
      this.mNetworkTts.prefetch(this.mContext, paramLocale, paramString);
  }

  public void setCallback(DonutTtsCallback paramDonutTtsCallback)
  {
    this.mDonutCallback = paramDonutTtsCallback;
  }

  public void shutdown()
  {
    if ((sHasDonutTts) && (this.mDonutTts != null))
      this.mDonutTts.shutdown();
  }

  public void speak(Activity paramActivity, Language paramLanguage, String paramString, NetworkTts.Callback paramCallback)
  {
    AudioManager localAudioManager = (AudioManager)this.mContext.getSystemService("audio");
    Locale localLocale;
    if ((localAudioManager != null) && (localAudioManager.getStreamVolume(3) == 0) && (paramActivity != null))
    {
      Util.showShortToastMessage(paramActivity, this.mContext.getString(R.string.msg_tts_volume_off));
      localLocale = Util.languageShortNameToLocale(paramLanguage.getShortName());
      if ((!Profile.getPreferNetworkTts(this.mContext)) || (!NetworkTts.isLanguageAvailable(localLocale)))
        break label119;
      if (this.mDonutTts != null)
        this.mDonutTts.stop();
      this.mNetworkTts.speak(this.mContext, localLocale, paramString, paramCallback);
    }
    label119: 
    while (!isNativeTtsAvailable(localLocale))
    {
      return;
      Util.showShortToastMessage(paramActivity, getTtsSpeakingMessage(paramActivity, paramLanguage.getLongName()));
      break;
    }
    this.mNetworkTts.stop();
    this.mDonutTts.speak(localLocale, paramString, 0);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.tts.MyTts
 * JD-Core Version:    0.6.2
 */