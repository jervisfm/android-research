package com.google.android.apps.translate.tts;

import android.content.Context;
import android.net.Uri;
import android.net.Uri.Builder;
import com.google.android.apps.translate.Logger;
import com.google.android.apps.translate.Util;
import java.util.HashSet;
import java.util.Locale;

public class NetworkTts
{
  private static final String[] NETWORK_TTS_SUPPORTED_LANGUAGES = { "af", "ar", "bs", "ca", "cs", "cy", "el", "en", "eo", "es", "da", "de", "fi", "fr", "hi", "hr", "ht", "hu", "hy", "id", "is", "it", "ja", "ko", "ku", "lv", "mk", "ro", "sq", "sr", "sw", "vi", "la", "nl", "no", "pl", "pt", "ru", "sk", "sv", "ta", "th", "tr", "zh", "zh-CN", "zh-TW" };
  private static final String REQUEST_DEFAULT_QUERY_PARAMETERS = "ie=utf-8&client=android-translate";
  private static final String REQUEST_HOST = "translate.google.com";
  private static final String REQUEST_PATH = "/translate_tts";
  private static final String REQUEST_SCHEME = "http";
  private static final String TAG = "NetworkTts";
  private static final String TARGET_LANG = "tl";
  private static final String TARGET_TEXT = "text";
  private static HashSet<Locale> sNetworkTtsLocaleSet = new HashSet();
  private NetworkTtsPlayer mNetworkTtsPlayer = new NetworkTtsPlayer();

  static
  {
    setNetworkTtsLanguagesMap();
  }

  private Uri getNetworkTtsUri(Locale paramLocale, String paramString)
  {
    Uri.Builder localBuilder = new Uri.Builder();
    localBuilder.scheme("http").authority("translate.google.com").path("/translate_tts").encodedQuery("ie=utf-8&client=android-translate").appendQueryParameter("text", paramString).appendQueryParameter("tl", paramLocale.getLanguage());
    return localBuilder.build();
  }

  public static boolean isLanguageAvailable(Locale paramLocale)
  {
    return sNetworkTtsLocaleSet.contains(paramLocale);
  }

  private static void setNetworkTtsLanguagesMap()
  {
    for (String str : NETWORK_TTS_SUPPORTED_LANGUAGES)
      sNetworkTtsLocaleSet.add(Util.languageShortNameToLocale(str));
  }

  public void prefetch(Context paramContext, Locale paramLocale, String paramString)
  {
    if (!isLanguageAvailable(paramLocale));
    Uri localUri;
    do
    {
      return;
      Logger.d("NetworkTts", "Prefetching " + paramString + " in " + paramLocale);
      localUri = getNetworkTtsUri(paramLocale, paramString);
    }
    while (localUri == null);
    this.mNetworkTtsPlayer.prefetch(paramContext, localUri);
  }

  public void speak(Context paramContext, Locale paramLocale, String paramString, Callback paramCallback)
  {
    if (!isLanguageAvailable(paramLocale));
    Uri localUri;
    do
    {
      return;
      Logger.d("NetworkTts", "Speaking " + paramString + " in " + paramLocale);
      localUri = getNetworkTtsUri(paramLocale, paramString);
    }
    while (localUri == null);
    this.mNetworkTtsPlayer.play(paramContext, localUri, paramCallback);
  }

  public void stop()
  {
    Logger.d("NetworkTts", "Stopping NetworkTTS playback");
    this.mNetworkTtsPlayer.stop();
  }

  public static abstract interface Callback
  {
    public abstract void onError(int paramInt);

    public abstract void onPrepare();

    public abstract void onReady();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.tts.NetworkTts
 * JD-Core Version:    0.6.2
 */