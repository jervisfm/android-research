package com.google.android.apps.translate.tts;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import com.google.android.apps.translate.CsiTimer;
import com.google.android.apps.translate.Lists;
import com.google.android.apps.translate.Logger;
import com.google.android.apps.translate.Util;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;

public class NetworkTtsPlayer
{
  private static final byte[] NETWORK_BUFFER = new byte[512];
  private static final int PLAY = 1;
  private static final int PREFETCH = 3;
  private static final int STOP = 2;
  private static final String TAG = "NetworkTtsPlayer";
  private static final String TTS_AUDIO_FILE = "tts.dat";
  public static final int TTS_TOO_LONG = 1;
  public static final int TTS_UNAVAILABLE = 0;
  private static final String USER_AGENT = "AndroidTranslate";
  private LinkedList<Command> mCmdQueue = Lists.newLinkedList();
  private Uri mLastUri;
  private MediaPlayer mPlayer;
  private Uri mPrefetchedUri;
  private MediaPlayer mPrefetchingPlayer;
  private int mState = 2;
  private Thread mThread;
  private PowerManager.WakeLock mWakeLock;

  private void acquireWakeLock()
  {
    if (this.mWakeLock != null)
      this.mWakeLock.acquire();
  }

  private void enqueueLocked(Command paramCommand)
  {
    this.mCmdQueue.clear();
    this.mCmdQueue.add(paramCommand);
    if (this.mThread == null)
    {
      acquireWakeLock();
      this.mThread = new Thread();
      this.mThread.start();
    }
  }

  private void prefetchSound(Command paramCommand)
  {
    if (((this.mPlayer != null) && (this.mLastUri != null) && (this.mLastUri.equals(paramCommand.uri))) || ((this.mPrefetchingPlayer != null) && (this.mPrefetchedUri != null) && (this.mPrefetchedUri.equals(paramCommand.uri))))
      return;
    try
    {
      MediaPlayer localMediaPlayer = prepareNewStream(paramCommand.context, paramCommand.uri);
      if (this.mPrefetchingPlayer != null)
        this.mPrefetchingPlayer.release();
      this.mPrefetchingPlayer = localMediaPlayer;
      this.mPrefetchedUri = paramCommand.uri;
      return;
    }
    catch (Exception localException)
    {
      Logger.w("NetworkTtsPlayer", "error prefetching sound for " + paramCommand.uri, localException);
    }
  }

  private MediaPlayer prepareNewStream(Context paramContext, Uri paramUri)
    throws IOException
  {
    MediaPlayer localMediaPlayer = new MediaPlayer();
    localMediaPlayer.setAudioStreamType(3);
    Logger.t("streamingStart");
    streamFromServer(paramContext, localMediaPlayer, paramUri);
    Logger.t("streamingEnd");
    localMediaPlayer.prepare();
    return localMediaPlayer;
  }

  private void releaseWakeLock()
  {
    if (this.mWakeLock != null)
      this.mWakeLock.release();
  }

  private void startSound(Command paramCommand)
  {
    try
    {
      Logger.d("NetworkTtsPlayer", "Starting playback");
      if (this.mPlayer != null)
      {
        if (this.mLastUri != null)
        {
          boolean bool = this.mLastUri.equals(paramCommand.uri);
          if (bool)
            try
            {
              this.mPlayer.seekTo(0);
              this.mPlayer.start();
              Logger.d("NetworkTtsPlayer", "Cache hit");
              return;
            }
            catch (Exception localException2)
            {
              Logger.e("NetworkTtsPlayer", "Cache replay error");
            }
        }
        this.mPlayer.release();
        this.mPlayer = null;
      }
      if ((this.mPrefetchingPlayer != null) && (this.mPrefetchedUri != null) && (this.mPrefetchedUri.equals(paramCommand.uri)))
      {
        if (this.mPlayer != null)
        {
          this.mPlayer.release();
          this.mPlayer = null;
        }
        this.mPlayer = this.mPrefetchingPlayer;
        this.mLastUri = this.mPrefetchedUri;
        this.mPrefetchingPlayer = null;
        this.mPrefetchedUri = null;
        Logger.d("NetworkTtsPlayer", "Prefetch hit");
        startSound(paramCommand);
        return;
      }
    }
    catch (TtsNotFoundException localTtsNotFoundException)
    {
      do
        Logger.w("NetworkTtsPlayer", "TTS not found for " + paramCommand.uri, localTtsNotFoundException);
      while (paramCommand.callback == null);
      paramCommand.callback.onError(1);
      return;
      CsiTimer localCsiTimer = new CsiTimer("nts");
      localCsiTimer.begin(new String[] { "t" });
      if (paramCommand.callback != null)
        paramCommand.callback.onPrepare();
      MediaPlayer localMediaPlayer = prepareNewStream(paramCommand.context, paramCommand.uri);
      if (paramCommand.callback != null)
        paramCommand.callback.onReady();
      localCsiTimer.end(new String[] { "t" });
      localCsiTimer.report(paramCommand.context);
      Logger.d("NetworkTtsPlayer", "Playing " + paramCommand.uri);
      localMediaPlayer.start();
      this.mPlayer = localMediaPlayer;
      this.mLastUri = paramCommand.uri;
      return;
    }
    catch (Exception localException1)
    {
      do
        Logger.w("NetworkTtsPlayer", "Error loading sound for " + paramCommand.uri, localException1);
      while (paramCommand.callback == null);
      paramCommand.callback.onError(0);
    }
  }

  private void streamFromServer(Context paramContext, MediaPlayer paramMediaPlayer, Uri paramUri)
    throws IOException
  {
    File localFile = new File(paramContext.getCacheDir(), "tts.dat");
    localFile.createNewFile();
    FileOutputStream localFileOutputStream = new FileOutputStream(localFile, false);
    HttpResponse localHttpResponse = Util.newHttpClient("AndroidTranslate").execute(new HttpGet(paramUri.toString()));
    if (localHttpResponse.getStatusLine().getStatusCode() == 404)
      throw new TtsNotFoundException();
    InputStream localInputStream = Util.getResponseInputStream(localHttpResponse);
    if (localInputStream != null)
      while (true)
      {
        int i = localInputStream.read(NETWORK_BUFFER);
        if (i <= -1)
          break;
        localFileOutputStream.write(NETWORK_BUFFER, 0, i);
      }
    localFileOutputStream.close();
    paramMediaPlayer.setDataSource(new FileInputStream(localFile).getFD());
  }

  public void play(Context paramContext, Uri paramUri)
  {
    play(paramContext, paramUri, null);
  }

  public void play(Context paramContext, Uri paramUri, NetworkTts.Callback paramCallback)
  {
    Logger.t("playStart");
    Command localCommand = new Command(null);
    localCommand.code = 1;
    localCommand.context = paramContext;
    localCommand.uri = paramUri;
    localCommand.callback = paramCallback;
    synchronized (this.mCmdQueue)
    {
      enqueueLocked(localCommand);
      this.mState = 1;
      return;
    }
  }

  public void prefetch(Context paramContext, Uri paramUri)
  {
    Command localCommand = new Command(null);
    localCommand.code = 3;
    localCommand.context = paramContext;
    localCommand.uri = paramUri;
    synchronized (this.mCmdQueue)
    {
      enqueueLocked(localCommand);
      return;
    }
  }

  public void setUsesWakeLock(Context paramContext)
  {
    if ((this.mWakeLock != null) || (this.mThread != null))
      throw new RuntimeException("assertion failed mWakeLock=" + this.mWakeLock + " mThread=" + this.mThread);
    this.mWakeLock = ((PowerManager)paramContext.getSystemService("power")).newWakeLock(1, "NetworkTtsPlayer");
  }

  public void stop()
  {
    synchronized (this.mCmdQueue)
    {
      if (this.mState != 2)
      {
        Command localCommand = new Command(null);
        localCommand.code = 2;
        enqueueLocked(localCommand);
        this.mState = 2;
      }
      return;
    }
  }

  private static final class Command
  {
    NetworkTts.Callback callback;
    int code;
    Context context;
    Uri uri;

    public String toString()
    {
      return "{ code=" + this.code + " uri=" + this.uri + " }";
    }
  }

  private final class Thread extends Thread
  {
    Thread()
    {
      super();
    }

    public void run()
    {
      while (true)
      {
        NetworkTtsPlayer.Command localCommand;
        synchronized (NetworkTtsPlayer.this.mCmdQueue)
        {
          Logger.d("NetworkTtsPlayer", "RemoveFirst");
          localCommand = (NetworkTtsPlayer.Command)NetworkTtsPlayer.this.mCmdQueue.removeFirst();
          switch (localCommand.code)
          {
          default:
          case 1:
          case 2:
          case 3:
          }
        }
        synchronized (NetworkTtsPlayer.this.mCmdQueue)
        {
          while (NetworkTtsPlayer.this.mCmdQueue.size() == 0)
          {
            NetworkTtsPlayer.access$402(NetworkTtsPlayer.this, null);
            NetworkTtsPlayer.this.releaseWakeLock();
            return;
            localObject1 = finally;
            throw localObject1;
            Logger.d("NetworkTtsPlayer", "PLAY");
            Logger.t("internalPlayStart");
            NetworkTtsPlayer.this.startSound(localCommand);
            Logger.t("internalPlayEnd");
            Logger.t("playEnd");
            continue;
            Logger.d("NetworkTtsPlayer", "STOP");
            if ((NetworkTtsPlayer.this.mPlayer != null) && (NetworkTtsPlayer.this.mPlayer.isPlaying()))
            {
              NetworkTtsPlayer.this.mPlayer.pause();
              continue;
              Logger.d("NetworkTtsPlayer", "PREFETCH");
              NetworkTtsPlayer.this.prefetchSound(localCommand);
            }
          }
        }
      }
    }
  }

  public static class TtsNotFoundException extends IOException
  {
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translate.tts.NetworkTtsPlayer
 * JD-Core Version:    0.6.2
 */