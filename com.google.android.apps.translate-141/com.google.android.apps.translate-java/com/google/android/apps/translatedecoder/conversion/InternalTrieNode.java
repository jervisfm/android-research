package com.google.android.apps.translatedecoder.conversion;

import java.util.List;

public class InternalTrieNode
{
  private List<InternalTrieNode> children;
  private String value;
  private int wordId;

  public InternalTrieNode(int paramInt, List<InternalTrieNode> paramList, String paramString)
  {
    this.wordId = paramInt;
    this.value = paramString;
    this.children = paramList;
  }

  public List<InternalTrieNode> children()
  {
    return this.children;
  }

  public void setChildren(List<InternalTrieNode> paramList)
  {
    this.children = paramList;
  }

  public void setValue(String paramString)
  {
    this.value = paramString;
  }

  public void setWordId(int paramInt)
  {
    this.wordId = paramInt;
  }

  public String value()
  {
    return this.value;
  }

  public int wordId()
  {
    return this.wordId;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.conversion.InternalTrieNode
 * JD-Core Version:    0.6.2
 */