package com.google.android.apps.translatedecoder.conversion;

import com.google.android.apps.translatedecoder.lm.ImplictTrieBasedLm;
import com.google.android.apps.translatedecoder.lm.LanguageModel;
import com.google.android.apps.translatedecoder.succinct.FixedBitsTrieNodeList;
import com.google.android.apps.translatedecoder.succinct.ImplicitTrie;
import com.google.android.apps.translatedecoder.succinct.Quantizer;
import com.google.android.apps.translatedecoder.succinct.TrieNode;
import com.google.android.apps.translatedecoder.succinct.TrieNodeList;
import com.google.android.apps.translatedecoder.util.ConfigParser;
import com.google.android.apps.translatedecoder.util.SymbolTable;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

public class LmTrieConverter
{
  private static final Logger logger = Logger.getLogger(LmTrieConverter.class.getName());
  private final Quantizer costQuantizer;
  private final int lmOrder;
  private final boolean noBackoff;
  private final double oovCost;
  private final boolean prefixTrue;
  private final double stupidBackoffCost;
  private final SymbolTable symbol;

  public LmTrieConverter(SymbolTable paramSymbolTable, int paramInt, double paramDouble1, double paramDouble2, boolean paramBoolean1, Quantizer paramQuantizer, boolean paramBoolean2)
  {
    this.symbol = paramSymbolTable;
    this.lmOrder = paramInt;
    this.oovCost = paramDouble1;
    this.stupidBackoffCost = paramDouble2;
    this.noBackoff = paramBoolean1;
    this.costQuantizer = paramQuantizer;
    this.prefixTrue = paramBoolean2;
  }

  private ImplictTrieBasedLm convert(InternalTrieNode paramInternalTrieNode, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    ArrayList localArrayList1 = new ArrayList();
    Object localObject = paramInternalTrieNode.children();
    int i = 0;
    while (((List)localObject).size() > 0)
    {
      i++;
      if (i > paramInt4)
        throw new RuntimeException("Number of layers in trie is greater than actualMaxSrcLen, wherecurorder=" + i + "; and actualMaxSrcLen=" + paramInt4);
      ArrayList localArrayList2 = new ArrayList();
      int j = 0;
      BitSet localBitSet = new BitSet();
      FixedBitsTrieNodeList localFixedBitsTrieNodeList;
      Iterator localIterator;
      if (i == paramInt4)
      {
        localFixedBitsTrieNodeList = new FixedBitsTrieNodeList(localBitSet, 0, paramInt1, 0, paramInt3, this.costQuantizer);
        localArrayList1.add(localFixedBitsTrieNodeList);
        localIterator = ((List)localObject).iterator();
      }
      label143: label328: 
      while (true)
      {
        if (!localIterator.hasNext())
          break label330;
        InternalTrieNode localInternalTrieNode = (InternalTrieNode)localIterator.next();
        if (localInternalTrieNode.value() == null)
          if (this.prefixTrue)
          {
            logger.severe("Prefix node do not have value, must be wrong, try to set --prefixTrue=false if you know this is ok!");
            System.exit(1);
          }
        while (true)
        {
          if (localInternalTrieNode.children() == null)
            break label328;
          j += localInternalTrieNode.children().size();
          localArrayList2.addAll(localInternalTrieNode.children());
          break label143;
          localFixedBitsTrieNodeList = new FixedBitsTrieNodeList(localBitSet, 0, paramInt1, paramInt2, paramInt3, this.costQuantizer);
          break;
          localFixedBitsTrieNodeList.add(new TrieNode(localInternalTrieNode.wordId(), j, 0.0D));
          logger.warning("The trieNode has null value!");
          continue;
          int k = localInternalTrieNode.wordId();
          double d = new Double(localInternalTrieNode.value()).doubleValue();
          localFixedBitsTrieNodeList.add(new TrieNode(k, j, d));
        }
      }
      label330: localObject = localArrayList2;
    }
    logger.info("actualMaxSrcLen is " + paramInt4);
    return new ImplictTrieBasedLm(paramInt4, this.oovCost, this.stupidBackoffCost, this.noBackoff, new ImplicitTrie(localArrayList1.size(), localArrayList1));
  }

  public static void main(String[] paramArrayOfString)
  {
    if (paramArrayOfString.length <= 0)
    {
      System.out.println("Usage: java LmTrieConverter --symbolFile=file --quantizerFile=file --lmFile=file --lmOrder=order --oovCost=oovCost --stupidBackoffCost=cost --wordIdLen=len --offsetLen=len --outFile=file --prefixTrue=flag --mmapFormat=flag");
      System.exit(1);
    }
    ConfigParser localConfigParser = new ConfigParser(paramArrayOfString);
    localConfigParser.checkRequiredProperties(new String[] { "symbolFile", "quantizerFile", "lmFile", "lmOrder", "oovCost", "stupidBackoffCost", "noBackoff", "wordIdLen", "offsetLen", "outFile" });
    SymbolTable localSymbolTable = SymbolTable.readFromFile(localConfigParser.getProperty("symbolFile"));
    Quantizer localQuantizer = Quantizer.readFromFile(localConfigParser.getProperty("quantizerFile"));
    LmTrieConverter localLmTrieConverter = new LmTrieConverter(localSymbolTable, Integer.valueOf(localConfigParser.getProperty("lmOrder")).intValue(), new Double(localConfigParser.getProperty("oovCost")).doubleValue(), new Double(localConfigParser.getProperty("stupidBackoffCost")).doubleValue(), new Boolean(localConfigParser.getProperty("noBackoff", "false")).booleanValue(), localQuantizer, new Boolean(localConfigParser.getProperty("prefixTrue", "true")).booleanValue());
    int i = localQuantizer.numBitsRequired();
    localLmTrieConverter.convertToImplicitTrie(localConfigParser.getProperty("lmFile"), Integer.valueOf(localConfigParser.getProperty("wordIdLen")).intValue(), Integer.valueOf(localConfigParser.getProperty("offsetLen")).intValue(), i).writeToFile(localConfigParser.getProperty("outFile"), new Boolean(localConfigParser.getProperty("mmapFormat", "true")).booleanValue());
  }

  public ImplictTrieBasedLm convertToImplicitTrie(String paramString, int paramInt1, int paramInt2, int paramInt3)
  {
    int[] arrayOfInt = new int[1];
    return convert(TrieModelReader.readIntoInternalTrie(paramString, this.symbol, this.lmOrder, true, arrayOfInt), paramInt1, paramInt2, paramInt3, arrayOfInt[0]);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.conversion.LmTrieConverter
 * JD-Core Version:    0.6.2
 */