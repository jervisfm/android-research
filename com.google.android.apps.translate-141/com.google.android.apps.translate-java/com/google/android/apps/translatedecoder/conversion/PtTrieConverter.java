package com.google.android.apps.translatedecoder.conversion;

import com.google.android.apps.translatedecoder.pt.ImplicitTrieBasedPt;
import com.google.android.apps.translatedecoder.pt.PhrasePair;
import com.google.android.apps.translatedecoder.pt.PhraseTable;
import com.google.android.apps.translatedecoder.pt.TargetPhrasesContainer;
import com.google.android.apps.translatedecoder.succinct.FixedBitsTrieNodeList;
import com.google.android.apps.translatedecoder.succinct.ImplicitTrie;
import com.google.android.apps.translatedecoder.succinct.Quantizer;
import com.google.android.apps.translatedecoder.succinct.TrieNode;
import com.google.android.apps.translatedecoder.succinct.TrieNodeList;
import com.google.android.apps.translatedecoder.util.ConfigParser;
import com.google.android.apps.translatedecoder.util.SymbolTable;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

public class PtTrieConverter
{
  private static final String DICT_TAG_WORD = "_dict_";
  private static final Logger logger = Logger.getLogger(PtTrieConverter.class.getName());
  private final Quantizer costQuantizer;
  private final int maxPhraseLen;
  private final double oovCost;
  private final SymbolTable symbol;

  public PtTrieConverter(SymbolTable paramSymbolTable, int paramInt, double paramDouble, Quantizer paramQuantizer)
  {
    this.symbol = paramSymbolTable;
    this.maxPhraseLen = paramInt;
    this.oovCost = paramDouble;
    this.costQuantizer = paramQuantizer;
  }

  private ImplicitTrieBasedPt convert(InternalTrieNode paramInternalTrieNode, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
  {
    TargetPhrasesContainer localTargetPhrasesContainer = new TargetPhrasesContainer(new BitSet(), paramInt1, paramInt4, this.costQuantizer, this.symbol.addWord("_dict_"));
    int i = 0;
    ArrayList localArrayList1 = new ArrayList();
    ArrayList localArrayList2 = new ArrayList();
    Object localObject = paramInternalTrieNode.children();
    int j = 0;
    while (((List)localObject).size() > 0)
    {
      j++;
      if (j > paramInt5)
        throw new RuntimeException("Number of layers in trie is greater than actualMaxSrcLen, wherecurorder=" + j + "; and actualMaxSrcLen=" + paramInt5);
      ArrayList localArrayList3 = new ArrayList();
      int k = 0;
      BitSet localBitSet = new BitSet();
      Quantizer localQuantizer = new Quantizer(-1.0D, 1.0D, -1 + (int)Math.pow(2.0D, paramInt3));
      FixedBitsTrieNodeList localFixedBitsTrieNodeList;
      Iterator localIterator;
      if (j == paramInt5)
      {
        localFixedBitsTrieNodeList = new FixedBitsTrieNodeList(localBitSet, 0, paramInt1, 0, paramInt3, localQuantizer);
        localArrayList2.add(localFixedBitsTrieNodeList);
        localIterator = ((List)localObject).iterator();
      }
      label210: label494: 
      while (true)
      {
        if (!localIterator.hasNext())
          break label496;
        InternalTrieNode localInternalTrieNode = (InternalTrieNode)localIterator.next();
        if (localInternalTrieNode.value() == null)
          localFixedBitsTrieNodeList.add(new TrieNode(localInternalTrieNode.wordId(), k, -1.0D));
        while (true)
        {
          if (localInternalTrieNode.children() == null)
            break label494;
          k += localInternalTrieNode.children().size();
          localArrayList3.addAll(localInternalTrieNode.children());
          break label210;
          localFixedBitsTrieNodeList = new FixedBitsTrieNodeList(localBitSet, 0, paramInt1, paramInt2, paramInt3, localQuantizer);
          break;
          int m = localInternalTrieNode.wordId();
          double d = i;
          localFixedBitsTrieNodeList.add(new TrieNode(m, k, d));
          localArrayList1.clear();
          String[] arrayOfString1 = localInternalTrieNode.value().split("\\s+\\|{2}\\s+");
          if (arrayOfString1.length <= 0)
          {
            logger.severe("no target phrases!");
            System.exit(1);
          }
          int n = arrayOfString1.length;
          for (int i1 = 0; i1 < n; i1++)
          {
            String[] arrayOfString2 = arrayOfString1[i1].split("\\s+\\|{3}\\s+");
            if (arrayOfString2.length != 2)
            {
              logger.severe("target phrase format error!");
              System.exit(1);
            }
            localArrayList1.add(new PhrasePair(null, this.symbol.addWords(arrayOfString2[0]), new Double(arrayOfString2[1]).doubleValue()));
          }
          i = localTargetPhrasesContainer.addTargetPhrases(localArrayList1, i);
        }
      }
      label496: localObject = localArrayList3;
    }
    ImplicitTrie localImplicitTrie = new ImplicitTrie(localArrayList2.size(), localArrayList2);
    logger.info("actualMaxSrcLen is " + paramInt5);
    return new ImplicitTrieBasedPt(paramInt5, this.oovCost, localImplicitTrie, localTargetPhrasesContainer);
  }

  public static void main(String[] paramArrayOfString)
  {
    if (paramArrayOfString.length <= 0)
    {
      System.out.println("Usage: java PtTrieConverter --symbolFile=file --quantizerFile=file --ptFile=file --ptOrder=order --oovCost=oovCost --wordIdLen=len --offsetLen=len --targetBytesLen=len --outFile=file --mmapFormat=flag");
      System.exit(1);
    }
    ConfigParser localConfigParser = new ConfigParser(paramArrayOfString);
    localConfigParser.checkRequiredProperties(new String[] { "symbolFile", "quantizerFile", "ptFile", "ptOrder", "oovCost", "wordIdLen", "offsetLen", "targetBytesLen", "outFile" });
    SymbolTable localSymbolTable = SymbolTable.readFromFile(localConfigParser.getProperty("symbolFile"));
    int i = Integer.valueOf(localConfigParser.getProperty("ptOrder")).intValue();
    double d = new Double(localConfigParser.getProperty("oovCost")).doubleValue();
    Quantizer localQuantizer = Quantizer.readFromFile(localConfigParser.getProperty("quantizerFile"));
    PtTrieConverter localPtTrieConverter = new PtTrieConverter(localSymbolTable, i, d, localQuantizer);
    int j = localQuantizer.numBitsRequired();
    localPtTrieConverter.convertToImplicitTrie(localConfigParser.getProperty("ptFile"), Integer.valueOf(localConfigParser.getProperty("wordIdLen")).intValue(), Integer.valueOf(localConfigParser.getProperty("offsetLen")).intValue(), Integer.valueOf(localConfigParser.getProperty("targetBytesLen")).intValue(), j).writeToFile(localConfigParser.getProperty("outFile"), new Boolean(localConfigParser.getProperty("mmapFormat", "true")).booleanValue());
  }

  public ImplicitTrieBasedPt convertToImplicitTrie(String paramString, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    int[] arrayOfInt = new int[1];
    return convert(TrieModelReader.readIntoInternalTrie(paramString, this.symbol, this.maxPhraseLen, false, arrayOfInt), paramInt1, paramInt2, paramInt3, paramInt4, arrayOfInt[0]);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.conversion.PtTrieConverter
 * JD-Core Version:    0.6.2
 */