package com.google.android.apps.translatedecoder.conversion;

import com.google.android.apps.translatedecoder.util.SymbolTable;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class TrieModelReader
{
  private static final Logger logger = Logger.getLogger(TrieModelReader.class.getName());

  private static InternalTrieNode getTrieNode(List<InternalTrieNode> paramList, int paramInt)
  {
    int i = 0;
    int j = -1 + paramList.size();
    int k = 0;
    while (i <= j)
    {
      int m = (i + j) / 2;
      InternalTrieNode localInternalTrieNode2 = (InternalTrieNode)paramList.get(m);
      int n = localInternalTrieNode2.wordId();
      if (paramInt == n)
        return localInternalTrieNode2;
      if (paramInt > n)
      {
        i = m + 1;
        k = i;
      }
      else
      {
        j = m - 1;
        k = m;
      }
    }
    InternalTrieNode localInternalTrieNode1 = new InternalTrieNode(paramInt, null, null);
    paramList.add(k, localInternalTrieNode1);
    return localInternalTrieNode1;
  }

  public static InternalTrieNode readIntoInternalTrie(String paramString, SymbolTable paramSymbolTable, int paramInt, boolean paramBoolean)
  {
    return readIntoInternalTrie(paramString, paramSymbolTable, paramInt, paramBoolean, null);
  }

  public static InternalTrieNode readIntoInternalTrie(String paramString, SymbolTable paramSymbolTable, int paramInt, boolean paramBoolean, int[] paramArrayOfInt)
  {
    while (true)
    {
      InternalTrieNode localInternalTrieNode1;
      int k;
      try
      {
        FileInputStream localFileInputStream = new FileInputStream(paramString);
        localDataInputStream = new DataInputStream(localFileInputStream);
        InputStreamReader localInputStreamReader = new InputStreamReader(localDataInputStream);
        BufferedReader localBufferedReader = new BufferedReader(localInputStreamReader);
        localInternalTrieNode1 = new InternalTrieNode(-1, null, null);
        i = 0;
        j = 0;
        String str1 = localBufferedReader.readLine();
        if (str1 != null)
        {
          k = j + 1;
          if ((j % 1000000 == 0) && (k != 1))
            logger.info("# of ngrams read: " + k);
          String[] arrayOfString = str1.trim().split("\\s+\\|{3}\\s+");
          if (arrayOfString.length <= 1)
          {
            logger.warning("Has one or zero field, line=" + str1);
            j = k;
            continue;
          }
          int[] arrayOfInt = paramSymbolTable.addWords(arrayOfString[0].trim());
          if ((paramInt > 0) && (arrayOfInt.length > paramInt))
          {
            logger.warning("wordIds.length > maxSrcLen, where wordIds.length=" + arrayOfInt.length + "; maxSrcLen=" + paramInt + "\nline=" + str1);
            j = k;
            continue;
          }
          if (arrayOfInt.length <= 0)
          {
            logger.severe("no words");
            System.exit(1);
          }
          if (arrayOfInt.length <= i)
            break label562;
          i = arrayOfInt.length;
          break label562;
          if (m >= arrayOfInt.length)
            break label575;
          if (((InternalTrieNode)localObject).children() == null)
            ((InternalTrieNode)localObject).setChildren(new ArrayList());
          localInternalTrieNode2 = getTrieNode(((InternalTrieNode)localObject).children(), arrayOfInt[m]);
          localObject = localInternalTrieNode2;
          m++;
          continue;
          if (n < arrayOfString.length)
          {
            str2 = str2 + arrayOfString[n];
            if (n >= -1 + arrayOfString.length)
              break label585;
            str2 = str2 + " ||| ";
            break label585;
          }
          if (localInternalTrieNode2.value() == null)
            localInternalTrieNode2.setValue(str2);
          else if (paramBoolean)
            logger.warning("Duplicate value, line=" + str1);
        }
      }
      catch (FileNotFoundException localFileNotFoundException)
      {
        localFileNotFoundException.printStackTrace();
        logger.info("Finished reading the model.");
        return null;
        localInternalTrieNode2.setValue(localInternalTrieNode2.value() + " || " + str2);
      }
      catch (IOException localIOException)
      {
        DataInputStream localDataInputStream;
        int i;
        localIOException.printStackTrace();
        continue;
        logger.info("actualMaxSrcLen0=" + i);
        if (paramArrayOfInt != null)
          paramArrayOfInt[0] = i;
        localDataInputStream.close();
        return localInternalTrieNode1;
      }
      label562: Object localObject = localInternalTrieNode1;
      InternalTrieNode localInternalTrieNode2 = null;
      int m = 0;
      continue;
      label575: String str2 = "";
      int n = 1;
      continue;
      label585: n++;
      continue;
      int j = k;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.conversion.TrieModelReader
 * JD-Core Version:    0.6.2
 */