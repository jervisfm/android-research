package com.google.android.apps.translatedecoder.decoder;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

public class BeamPruner
{
  private final PriorityQueue<LatticeNode> queue;
  private final int topN;

  public BeamPruner(int paramInt)
  {
    this.topN = paramInt;
    this.queue = new PriorityQueue();
  }

  public List<LatticeNode> sortAndPruneList(List<LatticeNode> paramList)
  {
    return sortAndPruneList(paramList, this.topN);
  }

  public List<LatticeNode> sortAndPruneList(List<LatticeNode> paramList, int paramInt)
  {
    this.queue.addAll(paramList);
    ArrayList localArrayList = new ArrayList();
    for (int i = 0; (i < paramInt) && (i < paramList.size()); i++)
      localArrayList.add(this.queue.poll());
    this.queue.clear();
    return localArrayList;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.decoder.BeamPruner
 * JD-Core Version:    0.6.2
 */