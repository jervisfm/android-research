package com.google.android.apps.translatedecoder.decoder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Chart
{
  private static final Logger logger = Logger.getLogger(Chart.class.getCanonicalName());
  private final Map<Integer, List<LatticeNode>> latticeNodes = new HashMap();
  private final Map<String, LatticeNode> nodesForEndPosition = new HashMap();
  private final BeamPruner pruner;

  public Chart()
  {
    this(null);
  }

  public Chart(BeamPruner paramBeamPruner)
  {
    this.pruner = paramBeamPruner;
  }

  private void printLatticeNodes(List<LatticeNode> paramList)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    Iterator localIterator = paramList.iterator();
    while (localIterator.hasNext())
    {
      LatticeNode localLatticeNode = (LatticeNode)localIterator.next();
      if (localLatticeNode.viterbiEdge() != null)
        localStringBuilder.append(localLatticeNode.viterbiEdge().insideViterbiCost());
      localStringBuilder.append(" ");
    }
    logger.finer(localStringBuilder.toString());
  }

  public void finishProcess(int paramInt)
  {
    logger.fine("processed endPos= " + paramInt);
    this.nodesForEndPosition.clear();
    if (!hasNode(paramInt));
    do
    {
      return;
      if (this.pruner != null)
        this.latticeNodes.put(Integer.valueOf(paramInt), this.pruner.sortAndPruneList((List)this.latticeNodes.get(Integer.valueOf(paramInt))));
    }
    while (logger.getLevel() != Level.FINER);
    printLatticeNodes((List)this.latticeNodes.get(Integer.valueOf(paramInt)));
  }

  public LatticeNode getNode(int paramInt, int[] paramArrayOfInt)
  {
    LatticeNode localLatticeNode = (LatticeNode)this.nodesForEndPosition.get(LatticeNode.getSignature(paramArrayOfInt, paramInt));
    if (localLatticeNode == null)
    {
      localLatticeNode = new LatticeNode(paramInt, paramArrayOfInt);
      this.nodesForEndPosition.put(localLatticeNode.getSignature(), localLatticeNode);
      Object localObject = (List)this.latticeNodes.get(Integer.valueOf(paramInt));
      if (localObject == null)
      {
        localObject = new ArrayList();
        this.latticeNodes.put(Integer.valueOf(paramInt), localObject);
      }
      ((List)localObject).add(localLatticeNode);
    }
    return localLatticeNode;
  }

  public boolean hasNode(int paramInt)
  {
    List localList = (List)this.latticeNodes.get(Integer.valueOf(paramInt));
    return (localList != null) && (localList.size() > 0);
  }

  public List<LatticeNode> nodes(int paramInt)
  {
    return (List)this.latticeNodes.get(Integer.valueOf(paramInt));
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.decoder.Chart
 * JD-Core Version:    0.6.2
 */