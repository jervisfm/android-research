package com.google.android.apps.translatedecoder.decoder;

import java.util.List;

public abstract interface Combiner<T extends Transition>
{
  public abstract void combine(List<LatticeNode> paramList, T paramT, int paramInt);

  public abstract void combine(List<LatticeNode> paramList, List<T> paramList1, int paramInt);

  public abstract LatticeNode createGoalNode(List<LatticeNode> paramList, Transition paramTransition, int paramInt);
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.decoder.Combiner
 * JD-Core Version:    0.6.2
 */