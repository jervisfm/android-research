package com.google.android.apps.translatedecoder.decoder;

import com.google.android.apps.translatedecoder.dictionary.DictionaryDecoder;
import com.google.android.apps.translatedecoder.dictionary.DictionaryEntry;
import com.google.android.apps.translatedecoder.lm.LanguageModel;
import com.google.android.apps.translatedecoder.lm.LanguageModelFeature;
import com.google.android.apps.translatedecoder.preprocess.Tokenizer;
import com.google.android.apps.translatedecoder.pt.HashMapBasedPt;
import com.google.android.apps.translatedecoder.pt.PhraseTable;
import com.google.android.apps.translatedecoder.rapidresp.RapidResponseTbl;
import com.google.android.apps.translatedecoder.util.Config;
import com.google.android.apps.translatedecoder.util.HashMapBasedSymbol;
import com.google.android.apps.translatedecoder.util.LmSymbolConverter;
import com.google.android.apps.translatedecoder.util.SymbolTable;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.logging.Logger;

public class Decoder
{
  private static final String LM_UNK_STRING = "<UNK>";
  private static final int kNumDefaultThreadsInExecutor = 4;
  private static final Logger logger = Logger.getLogger(Decoder.class.getCanonicalName());
  private static final ExecutorService mExecutor = Executors.newFixedThreadPool(4);
  private final Config config;
  private DictionaryDecoder dictDecoder = null;
  private final LanguageModelFeature lmFeature;
  private final LmSymbolConverter lmSymbolConverter;
  private final SymbolTable ptSymbol;
  private RapidResponseTbl rapidRespTbl;
  private final PhraseTable tm;
  private final Tokenizer tokenizer;

  public Decoder(Config paramConfig, SymbolTable paramSymbolTable1, SymbolTable paramSymbolTable2)
  {
    this.config = paramConfig;
    this.ptSymbol = paramSymbolTable1;
    int i = -1;
    boolean bool1;
    if (paramConfig.convertSymbol())
    {
      if (paramSymbolTable2 == null)
        throw new RuntimeException("convertSymbol is set true, but lmSymbol is null");
      boolean bool2 = paramConfig.simulateProdlm();
      bool1 = false;
      if (bool2)
      {
        if (paramSymbolTable2.hasWord("<UNK>"))
        {
          bool1 = true;
          i = paramSymbolTable2.getId("<UNK>");
        }
      }
      else
        this.lmSymbolConverter = new LmSymbolConverter(paramSymbolTable1, paramSymbolTable2, bool1, i);
    }
    while (true)
    {
      this.tm = PhraseTable.readFromFile(paramConfig.tmFile());
      if (paramConfig.oovTmCost() > 0.0D)
        this.tm.setOovCost(paramConfig.oovTmCost());
      if ((paramConfig.maxPhraseLength() > 0) && (paramConfig.maxPhraseLength() < this.tm.maxPhraseLen()))
        this.tm.setMaxPhraseLen(paramConfig.maxPhraseLength());
      logger.info("Finished reading pt!");
      if (paramConfig.rapidRespFile() != null)
      {
        HashMapBasedPt localHashMapBasedPt = new HashMapBasedPt(paramConfig.maxPhraseLength(), paramConfig.oovTmCost(), paramConfig.rapidRespFile(), paramSymbolTable1);
        this.rapidRespTbl = new RapidResponseTbl(localHashMapBasedPt, paramConfig.dominateCost());
        if ((paramConfig.maxPhraseLength() > 0) && (paramConfig.maxPhraseLength() < localHashMapBasedPt.maxPhraseLen()))
          localHashMapBasedPt.setMaxPhraseLen(paramConfig.maxPhraseLength());
        logger.info("actuall maxPhraseLength=" + this.tm.maxPhraseLen());
        logger.info("Finished reading rapid response phrase table!");
      }
      LanguageModel localLanguageModel = LanguageModel.readFromFile(paramConfig.lmFile());
      localLanguageModel.setMaxNumCachedNgrams(paramConfig.maxNumCachedNgrams());
      if (bool1)
      {
        localLanguageModel.setUnkId(i);
        localLanguageModel.setSimulateProdlm(true);
      }
      if (paramConfig.oovLmCost() > 0.0D)
        localLanguageModel.setOovCost(paramConfig.oovLmCost());
      int j = localLanguageModel.lmOrder();
      if ((paramConfig.lmOrder() > 0) && (paramConfig.lmOrder() < localLanguageModel.lmOrder()))
      {
        j = paramConfig.lmOrder();
        localLanguageModel.setLmOrder(j);
      }
      this.lmFeature = new LanguageModelFeature(j, localLanguageModel, this.lmSymbolConverter);
      this.lmFeature.setRelativeLmWeight(paramConfig.relativeLmWeight());
      if (paramConfig.ngramQueriedFile() != null)
        this.lmFeature.setRememberNgrams(paramSymbolTable1);
      logger.info("Finished reading lm!");
      this.tokenizer = Tokenizer.readFromFile(paramConfig.preprocDataFile());
      logger.info("Finished reading tokenizer data!");
      this.dictDecoder = new DictionaryDecoder(paramConfig, paramSymbolTable1);
      return;
      logger.warning("lm symbol tbl does not have <UNK>");
      bool1 = false;
      break;
      this.lmSymbolConverter = null;
      bool1 = false;
    }
  }

  public static void main(String[] paramArrayOfString)
  {
    if (paramArrayOfString.length < 1)
    {
      System.out.println("Usage: java Decoder configFile [--option_key=option_value]+");
      System.exit(1);
    }
    Config localConfig = new Config(paramArrayOfString[0]);
    String[] arrayOfString = new String[-1 + paramArrayOfString.length];
    for (int i = 0; i < arrayOfString.length; i++)
      arrayOfString[i] = paramArrayOfString[(i + 1)];
    localConfig.setParametersFromArgs(arrayOfString);
    Object localObject1 = new HashMapBasedSymbol();
    Object localObject2 = new HashMapBasedSymbol();
    if (localConfig.readSymbolFromFile())
    {
      localObject1 = SymbolTable.readFromFile(localConfig.ptSymbolTblFile());
      localObject2 = SymbolTable.readFromFile(localConfig.lmSymbolTblFile());
    }
    Decoder localDecoder = new Decoder(localConfig, (SymbolTable)localObject1, (SymbolTable)localObject2);
    localDecoder.batchDecoding(localConfig.inputFile(), localConfig.outputFile());
    localDecoder.shutDown();
  }

  public void batchDecoding(String paramString1, String paramString2)
  {
    try
    {
      localDataInputStream = new DataInputStream(new FileInputStream(paramString1));
      BufferedReader localBufferedReader = new BufferedReader(new InputStreamReader(localDataInputStream));
      localDataOutputStream = new DataOutputStream(new FileOutputStream(paramString2));
      localBufferedWriter = new BufferedWriter(new OutputStreamWriter(localDataOutputStream));
      while (true)
      {
        String str = localBufferedReader.readLine();
        if (str == null)
          break;
        logger.info("Decoding: " + str);
        localBufferedWriter.write(decoding(str) + "\n");
      }
    }
    catch (FileNotFoundException localFileNotFoundException)
    {
      DataInputStream localDataInputStream;
      DataOutputStream localDataOutputStream;
      BufferedWriter localBufferedWriter;
      localFileNotFoundException.printStackTrace();
      while (true)
      {
        if (this.config.ngramQueriedFile() != null)
          this.lmFeature.saveNgramsQueried(this.config.ngramQueriedFile());
        return;
        localDataInputStream.close();
        localBufferedWriter.flush();
        localDataOutputStream.close();
      }
    }
    catch (IOException localIOException)
    {
      while (true)
        localIOException.printStackTrace();
    }
  }

  public String decode(String paramString)
  {
    Future localFuture = mExecutor.submit(new DictionaryDecodingTask(this.dictDecoder, paramString));
    String str = decoding(paramString);
    StringBuilder localStringBuilder;
    try
    {
      localList = (List)localFuture.get(500L, TimeUnit.MILLISECONDS);
      localStringBuilder = new StringBuilder();
      if ((localList != null) && (localList.size() > 0))
      {
        localStringBuilder.append(((DictionaryEntry)localList.get(0)).tgtWords());
        localStringBuilder.append("\t");
        int i = 1;
        Iterator localIterator = localList.iterator();
        while (localIterator.hasNext())
        {
          DictionaryEntry localDictionaryEntry = (DictionaryEntry)localIterator.next();
          localStringBuilder.append(" ").append(i).append(". ").append(localDictionaryEntry.tgtWords()).append("\n");
          i++;
          localStringBuilder.append("\n");
        }
      }
    }
    catch (InterruptedException localInterruptedException)
    {
      while (true)
      {
        logger.info("Dictionary Decoding was exceptional! " + localInterruptedException.toString());
        localList = null;
      }
    }
    catch (ExecutionException localExecutionException)
    {
      while (true)
      {
        logger.info("Dictionary Decoding was exceptional! " + localExecutionException.toString());
        localList = null;
      }
    }
    catch (TimeoutException localTimeoutException)
    {
      while (true)
      {
        logger.info("Dictionary Decoding timed out! " + localTimeoutException.toString());
        List localList = null;
      }
      localStringBuilder.append(str);
    }
    return localStringBuilder.toString();
  }

  public String decoding(String paramString)
  {
    if (this.config.runPreprocess())
    {
      String str = this.tokenizer.tokenizeWithJoin(this.config.srcLang(), paramString.trim());
      return decoding(this.ptSymbol.addWords(str));
    }
    return decoding(this.ptSymbol.addWords(paramString.trim()));
  }

  public String decoding(int[] paramArrayOfInt)
  {
    Lattice localLattice = new Search(this.tm, this.rapidRespTbl, this.lmFeature, this.config, this.ptSymbol, paramArrayOfInt).search();
    if (this.config.runPostprocess())
      return this.tokenizer.deTokenize(this.config.tgtLang(), localLattice.extractViterbi(this.config.printBracket(), this.ptSymbol));
    return localLattice.extractViterbi(this.config.printBracket(), this.ptSymbol);
  }

  public DecoderOutput decodingWithStatInfo(String paramString)
  {
    DecoderOutput localDecoderOutput = new DecoderOutput();
    localDecoderOutput.setRawInput(paramString);
    String str1 = paramString.trim();
    if (this.config.runPreprocess())
      str1 = this.tokenizer.tokenizeWithJoin(this.config.srcLang(), paramString.trim());
    localDecoderOutput.setTokenizedInput(str1);
    int[] arrayOfInt = this.ptSymbol.addWords(str1);
    Lattice localLattice = new Search(this.tm, this.rapidRespTbl, this.lmFeature, this.config, this.ptSymbol, arrayOfInt).search();
    localDecoderOutput.setCost(localLattice.goalNode().insideViterbiCost());
    String str2 = localLattice.extractViterbi(this.config.printBracket(), this.ptSymbol);
    localDecoderOutput.setTokenizedOutput(str2);
    String str3 = str2;
    if (this.config.runPostprocess())
      str3 = this.tokenizer.deTokenize(this.config.tgtLang(), str2);
    localDecoderOutput.setDetokenizedOutput(str3);
    return localDecoderOutput;
  }

  public void shutDown()
  {
    mExecutor.shutdownNow();
  }

  private class DictionaryDecodingTask
    implements Callable<List<DictionaryEntry>>
  {
    private DictionaryDecoder decoder;
    private String inputSent;

    public DictionaryDecodingTask(DictionaryDecoder paramString, String arg3)
    {
      this.decoder = paramString;
      Object localObject;
      this.inputSent = localObject;
    }

    public List<DictionaryEntry> call()
    {
      return this.decoder.decoding(this.inputSent);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.decoder.Decoder
 * JD-Core Version:    0.6.2
 */