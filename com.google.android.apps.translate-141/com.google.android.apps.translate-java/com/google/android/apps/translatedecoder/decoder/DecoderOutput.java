package com.google.android.apps.translatedecoder.decoder;

public class DecoderOutput
{
  private double cost = 0.0D;
  private String deTokenizedOutput = "";
  private String rawInput = "";
  private String tokenizedInput = "";
  private String tokenizedOutput = "";

  public DecoderOutput()
  {
  }

  public DecoderOutput(String paramString1, String paramString2, String paramString3, String paramString4, double paramDouble)
  {
    this.rawInput = paramString1;
    this.tokenizedInput = paramString2;
    this.tokenizedOutput = paramString3;
    this.deTokenizedOutput = paramString4;
    this.cost = paramDouble;
  }

  public double getCost()
  {
    return this.cost;
  }

  public String getDetokenizedOutput()
  {
    return this.deTokenizedOutput;
  }

  public String getRawInput()
  {
    return this.rawInput;
  }

  public String getTokenizedInput()
  {
    return this.tokenizedInput;
  }

  public String getTokenizedOutput()
  {
    return this.tokenizedOutput;
  }

  public void setCost(double paramDouble)
  {
    this.cost = paramDouble;
  }

  public void setDetokenizedOutput(String paramString)
  {
    this.deTokenizedOutput = paramString;
  }

  public void setRawInput(String paramString)
  {
    this.rawInput = paramString;
  }

  public void setTokenizedInput(String paramString)
  {
    this.tokenizedInput = paramString;
  }

  public void setTokenizedOutput(String paramString)
  {
    this.tokenizedOutput = paramString;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.decoder.DecoderOutput
 * JD-Core Version:    0.6.2
 */