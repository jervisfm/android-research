package com.google.android.apps.translatedecoder.decoder;

import com.google.android.apps.translatedecoder.lm.LanguageModelFeature;
import java.util.Iterator;
import java.util.List;

public class ExhaustiveCombiner<T extends Transition>
  implements Combiner<T>
{
  private final Chart chart;
  private final LanguageModelFeature lm;

  public ExhaustiveCombiner(LanguageModelFeature paramLanguageModelFeature, Chart paramChart)
  {
    this.lm = paramLanguageModelFeature;
    this.chart = paramChart;
  }

  private void createEdge(LatticeNode paramLatticeNode, Transition paramTransition, int paramInt)
  {
    int[] arrayOfInt = this.lm.state(paramLatticeNode.stateWords(), paramTransition.tgtWords());
    createEdge(paramLatticeNode, paramTransition, this.chart.getNode(paramInt, arrayOfInt));
  }

  private void createEdge(LatticeNode paramLatticeNode1, Transition paramTransition, LatticeNode paramLatticeNode2)
  {
    double d = paramLatticeNode1.insideViterbiCost() + paramTransition.cost() + this.lm.cost(paramLatticeNode1.stateWords(), paramTransition.tgtWords()) * this.lm.relativeLmWeight();
    if (paramLatticeNode2.shouldUpdateVitberbi(d))
      paramLatticeNode2.setVitberbiEdge(new LatticeEdge(paramLatticeNode1, d, paramTransition));
  }

  public void combine(List<LatticeNode> paramList, T paramT, int paramInt)
  {
    Iterator localIterator = paramList.iterator();
    while (localIterator.hasNext())
      createEdge((LatticeNode)localIterator.next(), paramT, paramInt);
  }

  public void combine(List<LatticeNode> paramList, List<T> paramList1, int paramInt)
  {
    Iterator localIterator1 = paramList.iterator();
    while (localIterator1.hasNext())
    {
      LatticeNode localLatticeNode = (LatticeNode)localIterator1.next();
      Iterator localIterator2 = paramList1.iterator();
      while (localIterator2.hasNext())
        createEdge(localLatticeNode, (Transition)localIterator2.next(), paramInt);
    }
  }

  public LatticeNode createGoalNode(List<LatticeNode> paramList, Transition paramTransition, int paramInt)
  {
    LatticeNode localLatticeNode = new LatticeNode(paramInt, null);
    Iterator localIterator = paramList.iterator();
    while (localIterator.hasNext())
      createEdge((LatticeNode)localIterator.next(), paramTransition, localLatticeNode);
    return localLatticeNode;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.decoder.ExhaustiveCombiner
 * JD-Core Version:    0.6.2
 */