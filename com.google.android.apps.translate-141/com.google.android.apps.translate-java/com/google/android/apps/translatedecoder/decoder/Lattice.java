package com.google.android.apps.translatedecoder.decoder;

import com.google.android.apps.translatedecoder.util.SymbolTable;
import java.util.logging.Logger;

public class Lattice
{
  private static final Logger logger = Logger.getLogger(Lattice.class.getCanonicalName());
  private final LatticeNode goalNode;

  public Lattice(LatticeNode paramLatticeNode)
  {
    this.goalNode = paramLatticeNode;
  }

  private void extractViterbi(LatticeNode paramLatticeNode, boolean paramBoolean, SymbolTable paramSymbolTable, StringBuilder paramStringBuilder)
  {
    if (paramLatticeNode.viterbiEdge() != null)
    {
      extractViterbi(paramLatticeNode.viterbiEdge().src(), paramBoolean, paramSymbolTable, paramStringBuilder);
      if (paramBoolean)
        paramStringBuilder.append("[ ");
      paramStringBuilder.append(paramSymbolTable.getWords(paramLatticeNode.viterbiEdge().transition().tgtWords()));
      paramStringBuilder.append(" ");
      if (paramBoolean)
        paramStringBuilder.append("] ");
    }
  }

  public String extractViterbi(boolean paramBoolean, SymbolTable paramSymbolTable)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    extractViterbi(this.goalNode.viterbiEdge().src(), paramBoolean, paramSymbolTable, localStringBuilder);
    logger.info("res:" + localStringBuilder);
    return localStringBuilder.toString().trim();
  }

  public LatticeNode goalNode()
  {
    return this.goalNode;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.decoder.Lattice
 * JD-Core Version:    0.6.2
 */