package com.google.android.apps.translatedecoder.decoder;

public class LatticeEdge
{
  private static final double EPSILON = 0.01D;
  private final double insideViterbiCost;
  private final LatticeNode src;
  private final Transition transition;

  public LatticeEdge(LatticeNode paramLatticeNode, double paramDouble, Transition paramTransition)
  {
    this.src = paramLatticeNode;
    this.insideViterbiCost = paramDouble;
    this.transition = paramTransition;
  }

  public boolean equals(Object paramObject)
  {
    LatticeEdge localLatticeEdge;
    if ((paramObject instanceof LatticeEdge))
    {
      localLatticeEdge = (LatticeEdge)paramObject;
      if (Math.abs(this.insideViterbiCost - localLatticeEdge.insideViterbiCost) <= 0.01D)
        break label33;
    }
    label33: 
    do
    {
      return false;
      if (this.src != null)
        break;
    }
    while (localLatticeEdge.src != null);
    while (true)
      if (this.transition == null)
      {
        if (localLatticeEdge.transition != null)
          break;
        return true;
        if (!this.src.equals(localLatticeEdge.src))
          return false;
      }
    return this.transition.equals(localLatticeEdge.transition);
  }

  public double insideViterbiCost()
  {
    return this.insideViterbiCost;
  }

  public LatticeNode src()
  {
    return this.src;
  }

  public Transition transition()
  {
    return this.transition;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.decoder.LatticeEdge
 * JD-Core Version:    0.6.2
 */