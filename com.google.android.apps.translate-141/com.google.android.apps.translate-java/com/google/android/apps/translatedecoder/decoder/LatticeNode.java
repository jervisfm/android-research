package com.google.android.apps.translatedecoder.decoder;

import java.util.Arrays;

public class LatticeNode
  implements Comparable<LatticeNode>
{
  private static final double INIT_COST;
  private final int endPos;
  private final int[] stateWords;
  private LatticeEdge viterbiEdge;

  public LatticeNode(int paramInt, int[] paramArrayOfInt)
  {
    this.endPos = paramInt;
    this.stateWords = paramArrayOfInt;
    this.viterbiEdge = null;
  }

  public static String getSignature(int[] paramArrayOfInt, int paramInt)
  {
    StringBuilder localStringBuilder = new StringBuilder(12 * paramArrayOfInt.length);
    for (int i = 0; i < paramArrayOfInt.length; i++)
    {
      localStringBuilder.append(paramArrayOfInt[i]);
      if (i < -1 + paramArrayOfInt.length)
        localStringBuilder.append(" ");
    }
    return localStringBuilder.toString();
  }

  public int compareTo(LatticeNode paramLatticeNode)
  {
    if (insideViterbiCost() == paramLatticeNode.insideViterbiCost())
      return 0;
    if (insideViterbiCost() < paramLatticeNode.insideViterbiCost())
      return -1;
    return 1;
  }

  public int endPos()
  {
    return this.endPos;
  }

  public boolean equals(Object paramObject)
  {
    LatticeNode localLatticeNode;
    if ((paramObject instanceof LatticeNode))
    {
      localLatticeNode = (LatticeNode)paramObject;
      if ((this.endPos == localLatticeNode.endPos) && (Arrays.equals(this.stateWords, localLatticeNode.stateWords)))
        break label39;
    }
    label39: 
    do
    {
      return false;
      if (this.viterbiEdge != null)
        break;
    }
    while (localLatticeNode.viterbiEdge != null);
    return true;
    return this.viterbiEdge.equals(localLatticeNode.viterbiEdge);
  }

  public String getSignature()
  {
    return getSignature(this.stateWords, this.endPos);
  }

  public double insideViterbiCost()
  {
    if (this.viterbiEdge != null)
      return this.viterbiEdge.insideViterbiCost();
    return 0.0D;
  }

  public void setVitberbiEdge(LatticeEdge paramLatticeEdge)
  {
    this.viterbiEdge = paramLatticeEdge;
  }

  public boolean shouldUpdateVitberbi(double paramDouble)
  {
    return (this.viterbiEdge == null) || (paramDouble < this.viterbiEdge.insideViterbiCost());
  }

  public int[] stateWords()
  {
    return this.stateWords;
  }

  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("========= Node Info ============\n");
    localStringBuilder.append("endPos=" + this.endPos + "\n");
    localStringBuilder.append("stateWords=" + this.stateWords + "\n");
    localStringBuilder.append("ViterbiCost=" + insideViterbiCost() + "\n");
    return localStringBuilder.toString();
  }

  public LatticeEdge viterbiEdge()
  {
    return this.viterbiEdge;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.decoder.LatticeNode
 * JD-Core Version:    0.6.2
 */