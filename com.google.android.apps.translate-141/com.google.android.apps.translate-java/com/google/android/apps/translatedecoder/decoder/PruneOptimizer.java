package com.google.android.apps.translatedecoder.decoder;

import com.google.android.apps.translatedecoder.util.Config;
import com.google.android.apps.translatedecoder.util.HashMapBasedSymbol;
import com.google.android.apps.translatedecoder.util.SymbolTable;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

public class PruneOptimizer
{
  private static final Logger logger = Logger.getLogger(PruneOptimizer.class.getCanonicalName());
  private final Config config;
  private final SymbolTable lmSymbol;
  private final SymbolTable ptSymbol;

  public PruneOptimizer(Config paramConfig)
  {
    this.config = paramConfig;
    if (paramConfig.readSymbolFromFile())
    {
      this.ptSymbol = SymbolTable.readFromFile(paramConfig.ptSymbolTblFile());
      this.lmSymbol = SymbolTable.readFromFile(paramConfig.lmSymbolTblFile());
      return;
    }
    this.ptSymbol = new HashMapBasedSymbol();
    this.lmSymbol = new HashMapBasedSymbol();
  }

  private int binarySearch(String paramString)
  {
    DecoderOutput localDecoderOutput1 = decoding(paramString, -1);
    int i = 1;
    int j = 100;
    while (i <= j)
    {
      int k = (i + j) / 2;
      DecoderOutput localDecoderOutput2 = decoding(paramString, k);
      logger.info("middle=" + k + "; start=" + i + "; end=" + j + "; exactCost=" + localDecoderOutput1.getCost() + "; Cost=" + localDecoderOutput2.getCost());
      if (hasSearchError(localDecoderOutput1, localDecoderOutput2))
        i = k + 1;
      else
        j = k - 1;
    }
    return j + 1;
  }

  private DecoderOutput decoding(String paramString, int paramInt)
  {
    this.config.setBeamSize(paramInt);
    return new Decoder(this.config, this.ptSymbol, this.lmSymbol).decodingWithStatInfo(paramString);
  }

  private int getOptimalBeam(List<Integer> paramList, double paramDouble)
  {
    int i = (int)(paramDouble * paramList.size());
    Integer[] arrayOfInteger = (Integer[])paramList.toArray(new Integer[0]);
    Arrays.sort(arrayOfInteger);
    System.out.println("sorted array: " + Arrays.toString(arrayOfInteger));
    return arrayOfInteger[(-1 + (paramList.size() - i))].intValue();
  }

  private boolean hasSearchError(DecoderOutput paramDecoderOutput1, DecoderOutput paramDecoderOutput2)
  {
    if (paramDecoderOutput2.getCost() > paramDecoderOutput1.getCost());
    do
    {
      return true;
      if (paramDecoderOutput2.getCost() != paramDecoderOutput1.getCost())
        break;
    }
    while (paramDecoderOutput1.getDetokenizedOutput().compareTo(paramDecoderOutput2.getDetokenizedOutput()) != 0);
    return false;
    throw new RuntimeException("Cost is better, should never happen");
  }

  public static void main(String[] paramArrayOfString)
  {
    if (paramArrayOfString.length < 1)
    {
      System.out.println("Usage: java PruneOptimizer configFile [--option_key=option_value]+");
      System.exit(1);
    }
    Config localConfig = new Config(paramArrayOfString[0]);
    String[] arrayOfString = new String[-1 + paramArrayOfString.length];
    for (int i = 0; i < arrayOfString.length; i++)
      arrayOfString[i] = paramArrayOfString[(i + 1)];
    localConfig.setParametersFromArgs(arrayOfString);
    new PruneOptimizer(localConfig).batchSearch(localConfig.inputFile(), localConfig.outputFile());
  }

  public void batchSearch(String paramString1, String paramString2)
  {
    try
    {
      localDataInputStream = new DataInputStream(new FileInputStream(paramString1));
      BufferedReader localBufferedReader = new BufferedReader(new InputStreamReader(localDataInputStream));
      localDataOutputStream = new DataOutputStream(new FileOutputStream(paramString2));
      localBufferedWriter = new BufferedWriter(new OutputStreamWriter(localDataOutputStream));
      localArrayList = new ArrayList();
      while (true)
      {
        String str = localBufferedReader.readLine();
        if (str == null)
          break;
        logger.info("Decoding: " + str);
        int i = binarySearch(str);
        localArrayList.add(Integer.valueOf(i));
        localBufferedWriter.write(i + "\n");
        localBufferedWriter.flush();
      }
    }
    catch (FileNotFoundException localFileNotFoundException)
    {
      DataInputStream localDataInputStream;
      DataOutputStream localDataOutputStream;
      BufferedWriter localBufferedWriter;
      ArrayList localArrayList;
      localFileNotFoundException.printStackTrace();
      return;
      double d = this.config.maxSearchError();
      int j = getOptimalBeam(localArrayList, d);
      localBufferedWriter.write("Optimal beam size allows " + d + " error is " + j + "\n");
      localBufferedWriter.flush();
      localDataInputStream.close();
      localBufferedWriter.flush();
      localDataOutputStream.close();
      return;
    }
    catch (IOException localIOException)
    {
      localIOException.printStackTrace();
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.decoder.PruneOptimizer
 * JD-Core Version:    0.6.2
 */