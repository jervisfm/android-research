package com.google.android.apps.translatedecoder.decoder;

import com.google.android.apps.translatedecoder.lm.LanguageModelFeature;
import com.google.android.apps.translatedecoder.pt.PhrasePair;
import com.google.android.apps.translatedecoder.pt.PhraseTable;
import com.google.android.apps.translatedecoder.rapidresp.RapidResponseTbl;
import com.google.android.apps.translatedecoder.util.Config;
import com.google.android.apps.translatedecoder.util.SymbolTable;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

public class Search
{
  private static final String BOS = "<S>";
  private static final String EOS = "</S>";
  private static final Logger logger = Logger.getLogger(Search.class.getCanonicalName());
  private final Chart chart;
  private final Combiner<PhrasePair> combiner;
  private int[] inputWords;
  private final LanguageModelFeature lm;
  private final PhraseTable phraseTable;
  private final RapidResponseTbl rapidRespTbl;
  private final int wordIdForBOS;
  private final int wordIdForEOS;

  public Search(PhraseTable paramPhraseTable, LanguageModelFeature paramLanguageModelFeature, Config paramConfig, SymbolTable paramSymbolTable, int[] paramArrayOfInt)
  {
    this(paramPhraseTable, null, paramLanguageModelFeature, paramConfig, paramSymbolTable, paramArrayOfInt);
  }

  public Search(PhraseTable paramPhraseTable, RapidResponseTbl paramRapidResponseTbl, LanguageModelFeature paramLanguageModelFeature, Config paramConfig, SymbolTable paramSymbolTable, int[] paramArrayOfInt)
  {
    this.phraseTable = paramPhraseTable;
    this.rapidRespTbl = paramRapidResponseTbl;
    this.lm = paramLanguageModelFeature;
    this.inputWords = paramArrayOfInt;
    this.wordIdForBOS = paramSymbolTable.addWord("<S>");
    this.wordIdForEOS = paramSymbolTable.addWord("</S>");
    if (paramConfig.beamSize() <= 0);
    for (this.chart = new Chart(); ; this.chart = new Chart(new BeamPruner(paramConfig.beamSize())))
    {
      this.combiner = new ExhaustiveCombiner(paramLanguageModelFeature, this.chart);
      return;
    }
  }

  private void processEndPos(int paramInt)
  {
    if (paramInt == 0)
    {
      Chart localChart = this.chart;
      LanguageModelFeature localLanguageModelFeature = this.lm;
      int[] arrayOfInt2 = new int[1];
      arrayOfInt2[0] = this.wordIdForBOS;
      localChart.getNode(0, localLanguageModelFeature.state(null, arrayOfInt2));
    }
    while (true)
    {
      this.chart.finishProcess(paramInt);
      return;
      if (paramInt > this.inputWords.length)
        break label251;
      int i = 0;
      if (i < paramInt)
      {
        List localList2 = this.chart.nodes(i);
        if ((localList2 == null) || (localList2.size() == 0));
        while (true)
        {
          i++;
          break;
          int[] arrayOfInt1 = Arrays.copyOfRange(this.inputWords, i, paramInt);
          List localList3 = this.phraseTable.getPhrases(arrayOfInt1);
          if (this.rapidRespTbl != null)
            localList3 = this.rapidRespTbl.filterPhrases(arrayOfInt1, localList3);
          if ((localList3 != null) && (localList3.size() != 0))
            this.combiner.combine(localList2, localList3, paramInt);
        }
      }
      if (!this.chart.hasNode(paramInt))
      {
        int j = paramInt - 1;
        List localList1 = this.chart.nodes(j);
        PhrasePair localPhrasePair = this.phraseTable.createOOVPhrase(this.inputWords[j]);
        if ((localPhrasePair == null) || (localList1 == null))
          break;
        this.combiner.combine(localList1, localPhrasePair, paramInt);
      }
    }
    throw new RuntimeException("Either phrase or nodes are null.");
    label251: throw new RuntimeException("Wrong endPos " + paramInt);
  }

  public Chart chart()
  {
    return this.chart;
  }

  public Lattice createLattice(int paramInt)
  {
    List localList = this.chart.nodes(paramInt - 1);
    if ((localList == null) || (localList.size() == 0))
      throw new RuntimeException("No src nodes for end postion " + (paramInt - 1));
    PhrasePair localPhrasePair = this.phraseTable.createIdentityPhrase(this.wordIdForEOS, 0.0D);
    LatticeNode localLatticeNode = this.combiner.createGoalNode(localList, localPhrasePair, paramInt);
    if ((localLatticeNode == null) || (localLatticeNode.viterbiEdge() == null))
      throw new RuntimeException("No goal node, must be wrong!");
    return new Lattice(localLatticeNode);
  }

  public Lattice decodeAdditionalWords(int[] paramArrayOfInt)
  {
    if (this.inputWords == null);
    for (int i = 0; ; i = this.inputWords.length)
    {
      int[] arrayOfInt = this.inputWords;
      this.inputWords = new int[i + paramArrayOfInt.length];
      for (int j = 0; j < i; j++)
        this.inputWords[j] = arrayOfInt[j];
    }
    for (int k = i; k < this.inputWords.length; k++)
      this.inputWords[k] = paramArrayOfInt[(k - i)];
    System.out.println("prevEndPos=" + i);
    return search(i);
  }

  public Lattice search()
  {
    return search(0);
  }

  public Lattice search(int paramInt)
  {
    if ((this.inputWords == null) || (this.inputWords.length <= paramInt))
      return null;
    int i;
    if (paramInt == 0)
      i = 0;
    while (i < 1 + this.inputWords.length)
    {
      processEndPos(i);
      i++;
      continue;
      i = paramInt + 1;
    }
    return createLattice(1 + this.inputWords.length);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.decoder.Search
 * JD-Core Version:    0.6.2
 */