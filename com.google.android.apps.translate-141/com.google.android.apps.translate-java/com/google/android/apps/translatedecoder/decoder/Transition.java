package com.google.android.apps.translatedecoder.decoder;

public abstract interface Transition
{
  public abstract double cost();

  public abstract int[] tgtWords();
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.decoder.Transition
 * JD-Core Version:    0.6.2
 */