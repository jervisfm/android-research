package com.google.android.apps.translatedecoder.dictionary;

import com.google.android.apps.translatedecoder.preprocess.Tokenizer;
import com.google.android.apps.translatedecoder.pt.PhrasePair;
import com.google.android.apps.translatedecoder.pt.PhraseTable;
import com.google.android.apps.translatedecoder.util.Config;
import com.google.android.apps.translatedecoder.util.SymbolTable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

public class DictionaryDecoder
{
  private static final Logger logger = Logger.getLogger(DictionaryDecoder.class.getCanonicalName());
  private final Config config;
  private final PhraseTable phraseTbl;
  private final SymbolTable ptSymbol;
  private final Tokenizer tokenizer;

  public DictionaryDecoder(Config paramConfig, SymbolTable paramSymbolTable)
  {
    this.config = paramConfig;
    this.ptSymbol = paramSymbolTable;
    this.phraseTbl = PhraseTable.readFromFile(paramConfig.tmFile());
    logger.info("Finished reading phraseTbl!");
    this.tokenizer = Tokenizer.readFromFile(paramConfig.preprocDataFile());
    logger.info("Finished reading tokenizer data!");
  }

  public List<DictionaryEntry> decoding(String paramString)
  {
    if (this.config.runPreprocess());
    List localList;
    for (int[] arrayOfInt = this.ptSymbol.addWords(this.tokenizer.tokenizeWithJoin(this.config.srcLang(), paramString.trim())); ; arrayOfInt = this.ptSymbol.addWords(paramString.trim()))
    {
      localList = this.phraseTbl.getPhrases(arrayOfInt);
      if ((localList != null) && (localList.size() > 0))
        break;
      localObject = null;
      return localObject;
    }
    Object localObject = null;
    Iterator localIterator = localList.iterator();
    label90: PhrasePair localPhrasePair;
    while (localIterator.hasNext())
    {
      localPhrasePair = (PhrasePair)localIterator.next();
      if (localPhrasePair.dictInfo() != null)
      {
        if (localObject == null)
          localObject = new ArrayList();
        if (!this.config.runPostprocess())
          break label221;
      }
    }
    label221: for (DictionaryEntry localDictionaryEntry = new DictionaryEntry(this.ptSymbol.getWords(localPhrasePair.srcWords()), this.tokenizer.deTokenize(this.config.tgtLang(), this.ptSymbol.getWords(localPhrasePair.tgtWords())), this.ptSymbol.getWords(localPhrasePair.dictInfo()), localPhrasePair.cost()); ; localDictionaryEntry = new DictionaryEntry(this.ptSymbol.getWords(localPhrasePair.srcWords()), this.ptSymbol.getWords(localPhrasePair.tgtWords()), this.ptSymbol.getWords(localPhrasePair.dictInfo()), localPhrasePair.cost()))
    {
      ((List)localObject).add(localDictionaryEntry);
      break label90;
      break;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.dictionary.DictionaryDecoder
 * JD-Core Version:    0.6.2
 */