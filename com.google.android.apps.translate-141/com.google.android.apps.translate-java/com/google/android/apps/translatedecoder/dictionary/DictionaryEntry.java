package com.google.android.apps.translatedecoder.dictionary;

public class DictionaryEntry
{
  private final double cost;
  private final String srcWords;
  private final String tag;
  private final String tgtWords;

  public DictionaryEntry(String paramString1, String paramString2, String paramString3, double paramDouble)
  {
    this.srcWords = paramString1;
    this.tgtWords = paramString2;
    this.tag = paramString3;
    this.cost = paramDouble;
  }

  public double cost()
  {
    return this.cost;
  }

  public String srcWords()
  {
    return this.srcWords;
  }

  public String tag()
  {
    return this.tag;
  }

  public String tgtWords()
  {
    return this.tgtWords;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.dictionary.DictionaryEntry
 * JD-Core Version:    0.6.2
 */