package com.google.android.apps.translatedecoder.lm;

import com.google.android.apps.translatedecoder.util.SymbolTable;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

public class HashMapBasedLm extends LanguageModel
{
  private static final Logger logger = Logger.getLogger(HashMapBasedLm.class.getCanonicalName());
  private static final long serialVersionUID = -6948027990348658173L;
  private final Map<String, Double> lmModel;
  private final transient SymbolTable symbol;

  public HashMapBasedLm(int paramInt, double paramDouble, String paramString, SymbolTable paramSymbolTable)
  {
    this.lmOrder = paramInt;
    this.oovCost = paramDouble;
    this.symbol = paramSymbolTable;
    this.lmModel = new HashMap();
    if (paramString == null)
      logger.warning("lm file is empty!");
    while (true)
    {
      this.noBackoff = true;
      return;
      loadFromFile(paramString);
    }
  }

  private void loadFromFile(String paramString)
  {
    this.noBackoff = true;
    int i = 0;
    try
    {
      DataInputStream localDataInputStream = new DataInputStream(new FileInputStream(paramString));
      BufferedReader localBufferedReader = new BufferedReader(new InputStreamReader(localDataInputStream));
      int k;
      for (int j = 0; ; j = k)
      {
        String str = localBufferedReader.readLine();
        if (str == null)
          break;
        k = j + 1;
        if ((j % 1000 == 0) && (k != 1))
          logger.info("# of ngrams read: " + k);
        String[] arrayOfString = str.trim().split("\\s+\\|{3}\\s+");
        int[] arrayOfInt = this.symbol.addWords(arrayOfString[0].trim());
        if (arrayOfInt.length > i)
          i = arrayOfInt.length;
        StringBuilder localStringBuilder = new StringBuilder();
        m = 0;
        if (m < arrayOfInt.length)
        {
          localStringBuilder.append(arrayOfInt[m]);
          if (m >= -1 + arrayOfInt.length)
            break label309;
          localStringBuilder.append(" ");
          break label309;
        }
        this.lmModel.put(localStringBuilder.toString(), new Double(arrayOfString[1]));
      }
      localDataInputStream.close();
      logger.info("Finished reading lm table.");
      if (i != this.lmOrder)
      {
        logger.warning("maxOrder != lmOrder, that is: " + i + " != " + this.lmOrder);
        this.lmOrder = i;
      }
      return;
    }
    catch (FileNotFoundException localFileNotFoundException)
    {
      while (true)
        localFileNotFoundException.printStackTrace();
    }
    catch (IOException localIOException)
    {
      while (true)
      {
        int m;
        localIOException.printStackTrace();
        continue;
        label309: m++;
      }
    }
  }

  public double ngramCost(List<Integer> paramList)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    for (int i = 0; i < paramList.size(); i++)
    {
      localStringBuilder.append(paramList.get(i));
      if (i < -1 + paramList.size())
        localStringBuilder.append(" ");
    }
    Double localDouble = (Double)this.lmModel.get(localStringBuilder.toString());
    if (localDouble == null)
      localDouble = Double.valueOf(this.oovCost);
    return localDouble.doubleValue();
  }

  public void writeToByteBuffer(ByteBuffer paramByteBuffer)
  {
    logger.severe("calling unimplement function");
    System.exit(1);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.lm.HashMapBasedLm
 * JD-Core Version:    0.6.2
 */