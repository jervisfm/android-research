package com.google.android.apps.translatedecoder.lm;

import com.google.android.apps.translatedecoder.succinct.ImplicitTrie;
import com.google.android.apps.translatedecoder.succinct.TrieNode;
import com.google.android.apps.translatedecoder.util.Utils;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ImplictTrieBasedLm extends LanguageModel
{
  public static final int CLASS_ID = 1;
  private static final long serialVersionUID = -5229708598567246449L;
  private transient Map<String, Double> costCache;
  private double stupidBackoffCost;
  private final ImplicitTrie trie;

  public ImplictTrieBasedLm(int paramInt, double paramDouble1, double paramDouble2, boolean paramBoolean, ImplicitTrie paramImplicitTrie)
  {
    this(paramInt, paramDouble1, paramDouble2, paramBoolean, paramImplicitTrie, 0);
  }

  public ImplictTrieBasedLm(int paramInt1, double paramDouble1, double paramDouble2, boolean paramBoolean, ImplicitTrie paramImplicitTrie, int paramInt2)
  {
    this.lmOrder = paramInt1;
    this.oovCost = paramDouble1;
    this.stupidBackoffCost = paramDouble2;
    this.noBackoff = paramBoolean;
    this.trie = paramImplicitTrie;
    this.maxNumCachedNgrams = paramInt2;
  }

  private double noBackoffCost(List<Integer> paramList)
  {
    TrieNode localTrieNode = this.trie.lookup(paramList);
    if (localTrieNode != null)
      return localTrieNode.getValue();
    return this.oovCost;
  }

  public static LanguageModel readFromByteBufferHelper(ByteBuffer paramByteBuffer)
  {
    int i = 1;
    int k = paramByteBuffer.getInt();
    double d = paramByteBuffer.getDouble();
    if (paramByteBuffer.getInt() == i);
    while (true)
    {
      return new ImplictTrieBasedLm(k, d, paramByteBuffer.getDouble(), i, ImplicitTrie.readFromByteBuffer(paramByteBuffer));
      int j = 0;
    }
  }

  public double ngramCost(List<Integer> paramList)
  {
    if (this.maxNumCachedNgrams > 0)
    {
      if (this.costCache == null)
        this.costCache = new HashMap();
      String str = Utils.listToString(paramList);
      Double localDouble = (Double)this.costCache.get(str);
      if (localDouble != null)
        return localDouble.doubleValue();
      double d = ngramCostHelper(paramList);
      if (this.costCache.size() >= this.maxNumCachedNgrams)
        this.costCache.clear();
      this.costCache.put(str, Double.valueOf(d));
      return d;
    }
    return ngramCostHelper(paramList);
  }

  public double ngramCostHelper(List<Integer> paramList)
  {
    if (this.noBackoff)
    {
      d = noBackoffCost(paramList);
      return d;
    }
    double d = 0.0D;
    int i = 0;
    label20: if (i < paramList.size())
    {
      TrieNode localTrieNode = this.trie.lookup(paramList.subList(i, paramList.size()));
      if (localTrieNode != null)
        return d + localTrieNode.getValue();
      if (i != -1 + paramList.size())
        break label91;
      d = this.oovCost;
    }
    while (true)
    {
      i++;
      break label20;
      break;
      label91: if (!this.simulateProdlm)
      {
        d += this.stupidBackoffCost;
      }
      else
      {
        if (this.lmOrder > 2)
          throw new RuntimeException("simulateProdlm does not work for order" + this.lmOrder);
        if (((Integer)paramList.get(0)).intValue() != this.unkId)
          d += this.stupidBackoffCost;
      }
    }
  }

  public void setStupidBackoffCost(double paramDouble)
  {
    this.stupidBackoffCost = paramDouble;
  }

  public double stupidBackoffCost()
  {
    return this.stupidBackoffCost;
  }

  public void writeToByteBuffer(ByteBuffer paramByteBuffer)
  {
    int i = 1;
    paramByteBuffer.putInt(i);
    paramByteBuffer.putInt(this.lmOrder);
    paramByteBuffer.putDouble(this.oovCost);
    if (this.noBackoff);
    while (true)
    {
      paramByteBuffer.putInt(i);
      paramByteBuffer.putDouble(this.stupidBackoffCost);
      this.trie.writeToByteBuffer(paramByteBuffer);
      return;
      i = 0;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.lm.ImplictTrieBasedLm
 * JD-Core Version:    0.6.2
 */