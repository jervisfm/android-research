package com.google.android.apps.translatedecoder.lm;

import com.google.android.apps.translatedecoder.util.MemMapUtil;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.RandomAccessFile;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileChannel.MapMode;
import java.util.List;
import java.util.logging.Logger;

public abstract class LanguageModel
  implements Serializable
{
  private static final Logger logger = Logger.getLogger(LanguageModel.class.getName());
  private static final long serialVersionUID = -5371845021620059255L;
  int lmOrder;
  transient int maxNumCachedNgrams = 0;
  boolean noBackoff;
  double oovCost;
  transient boolean simulateProdlm = false;
  transient int unkId = -1;

  public static LanguageModel readFromByteBuffer(ByteBuffer paramByteBuffer)
  {
    int i = paramByteBuffer.getInt();
    if (i == 1)
      return ImplictTrieBasedLm.readFromByteBufferHelper(paramByteBuffer);
    logger.severe("unknown class id" + i);
    System.exit(1);
    return null;
  }

  public static LanguageModel readFromFile(String paramString)
  {
    try
    {
      FileChannel localFileChannel = new RandomAccessFile(paramString, "r").getChannel();
      MappedByteBuffer localMappedByteBuffer = localFileChannel.map(FileChannel.MapMode.READ_ONLY, 0L, (int)localFileChannel.size());
      if (localMappedByteBuffer.getInt() == MemMapUtil.mmapFileSignature())
      {
        LanguageModel localLanguageModel2 = readFromByteBuffer(localMappedByteBuffer);
        localFileChannel.close();
        return localLanguageModel2;
      }
      ObjectInputStream localObjectInputStream = new ObjectInputStream(new FileInputStream(paramString));
      LanguageModel localLanguageModel1 = (LanguageModel)localObjectInputStream.readObject();
      localObjectInputStream.close();
      localFileChannel.close();
      return localLanguageModel1;
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
    }
    return null;
  }

  public int lmOrder()
  {
    return this.lmOrder;
  }

  public int maxNumCachedNgrams()
  {
    return this.maxNumCachedNgrams;
  }

  public abstract double ngramCost(List<Integer> paramList);

  public boolean noBackoff()
  {
    return this.noBackoff;
  }

  public double oovCost()
  {
    return this.oovCost;
  }

  public double sentenceCost(List<Integer> paramList, int paramInt)
  {
    double d1 = 0.0D;
    if (paramList == null);
    while (true)
    {
      return d1;
      int i = paramList.size();
      if (i > 0)
      {
        d1 = 0.0D;
        for (int j = paramInt; (j < this.lmOrder) && (j <= i); j++)
        {
          double d3 = ngramCost(paramList.subList(0, j));
          d1 += d3;
          logger.info("partial ngram at " + j + "; cost=" + d3);
        }
        for (int k = 0; k <= i - this.lmOrder; k++)
        {
          double d2 = ngramCost(paramList.subList(k, k + this.lmOrder));
          d1 += d2;
          logger.info("reguar ngram at " + k + "; cost=" + d2);
        }
      }
    }
  }

  public void setLmOrder(int paramInt)
  {
    this.lmOrder = paramInt;
  }

  public void setMaxNumCachedNgrams(int paramInt)
  {
    this.maxNumCachedNgrams = paramInt;
  }

  public void setNoBackoff(boolean paramBoolean)
  {
    this.noBackoff = paramBoolean;
  }

  public void setOovCost(double paramDouble)
  {
    this.oovCost = paramDouble;
  }

  public void setSimulateProdlm(boolean paramBoolean)
  {
    this.simulateProdlm = paramBoolean;
  }

  public void setUnkId(int paramInt)
  {
    this.unkId = paramInt;
  }

  public boolean simulateProdlm()
  {
    return this.simulateProdlm;
  }

  public int unkId()
  {
    return this.unkId;
  }

  public abstract void writeToByteBuffer(ByteBuffer paramByteBuffer);

  public void writeToFile(String paramString)
  {
    writeToFile(paramString, false);
  }

  public void writeToFile(String paramString, boolean paramBoolean)
  {
    if (paramBoolean);
    try
    {
      RandomAccessFile localRandomAccessFile = new RandomAccessFile(paramString, "rw");
      FileChannel localFileChannel = localRandomAccessFile.getChannel();
      MappedByteBuffer localMappedByteBuffer = localFileChannel.map(FileChannel.MapMode.READ_WRITE, 0L, 500000000L);
      localMappedByteBuffer.putInt(MemMapUtil.mmapFileSignature());
      writeToByteBuffer(localMappedByteBuffer);
      logger.info("Final buffer size is " + localMappedByteBuffer.position());
      localFileChannel.truncate(localMappedByteBuffer.position());
      localRandomAccessFile.close();
      return;
      ObjectOutputStream localObjectOutputStream = new ObjectOutputStream(new FileOutputStream(paramString));
      localObjectOutputStream.writeObject(this);
      localObjectOutputStream.close();
      return;
    }
    catch (IOException localIOException)
    {
      localIOException.printStackTrace();
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.lm.LanguageModel
 * JD-Core Version:    0.6.2
 */