package com.google.android.apps.translatedecoder.lm;

import com.google.android.apps.translatedecoder.util.LmSymbolConverter;
import com.google.android.apps.translatedecoder.util.SymbolTable;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

public class LanguageModelFeature
{
  private static final Logger logger = Logger.getLogger(LanguageModelFeature.class.getCanonicalName());
  private final LanguageModel lmModel;
  private int lmOrder;
  private final LmSymbolConverter lmSymbolConverter;
  private Set<String> ngramQueried;
  private double relativeLmWeight = 1.0D;
  private SymbolTable symbol;

  public LanguageModelFeature(int paramInt, LanguageModel paramLanguageModel)
  {
    this(paramInt, paramLanguageModel, null, 1.0D);
  }

  public LanguageModelFeature(int paramInt, LanguageModel paramLanguageModel, LmSymbolConverter paramLmSymbolConverter)
  {
    this(paramInt, paramLanguageModel, paramLmSymbolConverter, 1.0D);
  }

  public LanguageModelFeature(int paramInt, LanguageModel paramLanguageModel, LmSymbolConverter paramLmSymbolConverter, double paramDouble)
  {
    this.lmOrder = paramInt;
    this.lmModel = paramLanguageModel;
    this.lmSymbolConverter = paramLmSymbolConverter;
    this.relativeLmWeight = paramDouble;
    if (this.lmOrder > this.lmModel.lmOrder())
    {
      logger.severe("LanguageModelFeature's lmOrder is greater than the underlying lmModel's lmOrder");
      System.exit(1);
    }
  }

  private int[] convertSymbols(int[] paramArrayOfInt)
  {
    if (this.lmSymbolConverter == null)
      return paramArrayOfInt;
    return this.lmSymbolConverter.toLmSymbolIds(paramArrayOfInt);
  }

  public double cost(int[] paramArrayOfInt1, int[] paramArrayOfInt2)
  {
    if (paramArrayOfInt1.length > -1 + this.lmOrder)
    {
      logger.severe("State has too many words!");
      System.exit(1);
    }
    int[] arrayOfInt = convertSymbols(paramArrayOfInt2);
    ArrayList localArrayList = new ArrayList();
    for (int i = 0; i < paramArrayOfInt1.length; i++)
      localArrayList.add(Integer.valueOf(paramArrayOfInt1[i]));
    double d = 0.0D;
    for (int j = 0; j < arrayOfInt.length; j++)
    {
      localArrayList.add(Integer.valueOf(arrayOfInt[j]));
      d += this.lmModel.ngramCost(localArrayList);
      if (this.ngramQueried != null)
        this.ngramQueried.add(this.symbol.getWords(localArrayList));
      if (localArrayList.size() >= this.lmOrder)
        localArrayList.remove(0);
    }
    return d;
  }

  public LanguageModel lmModel()
  {
    return this.lmModel;
  }

  public int lmOrder()
  {
    return this.lmOrder;
  }

  public double relativeLmWeight()
  {
    return this.relativeLmWeight;
  }

  public void saveNgramsQueried(String paramString)
  {
    if (this.ngramQueried == null)
    {
      logger.severe("ngramQueried == null, be sure to call function setRememberNgrams");
      System.exit(1);
    }
    try
    {
      localDataOutputStream = new DataOutputStream(new FileOutputStream(paramString));
      localBufferedWriter = new BufferedWriter(new OutputStreamWriter(localDataOutputStream));
      Iterator localIterator = this.ngramQueried.iterator();
      while (localIterator.hasNext())
        localBufferedWriter.write((String)localIterator.next() + "\n");
    }
    catch (FileNotFoundException localFileNotFoundException)
    {
      DataOutputStream localDataOutputStream;
      BufferedWriter localBufferedWriter;
      localFileNotFoundException.printStackTrace();
      return;
      localBufferedWriter.flush();
      localDataOutputStream.close();
      return;
    }
    catch (IOException localIOException)
    {
      localIOException.printStackTrace();
    }
  }

  public void setLmOrder(int paramInt)
  {
    this.lmOrder = paramInt;
  }

  public void setRelativeLmWeight(double paramDouble)
  {
    this.relativeLmWeight = paramDouble;
  }

  public void setRememberNgrams(SymbolTable paramSymbolTable)
  {
    if (this.ngramQueried == null)
      this.ngramQueried = new HashSet();
    this.symbol = paramSymbolTable;
  }

  public int[] state(int[] paramArrayOfInt1, int[] paramArrayOfInt2)
  {
    int[] arrayOfInt1 = convertSymbols(paramArrayOfInt2);
    if (paramArrayOfInt1 == null)
      return arrayOfInt1;
    if (paramArrayOfInt1.length > -1 + this.lmOrder)
    {
      logger.severe("State has too many words!");
      System.exit(1);
    }
    if (paramArrayOfInt1.length + arrayOfInt1.length >= -1 + this.lmOrder);
    int[] arrayOfInt2;
    int j;
    for (int i = -1 + this.lmOrder; ; i = paramArrayOfInt1.length + arrayOfInt1.length)
    {
      arrayOfInt2 = new int[i];
      j = 0;
      int k = paramArrayOfInt1.length - (arrayOfInt2.length - arrayOfInt1.length);
      while (k < paramArrayOfInt1.length)
      {
        int i1 = j + 1;
        arrayOfInt2[j] = paramArrayOfInt1[k];
        k++;
        j = i1;
      }
    }
    int m;
    if (arrayOfInt1.length - arrayOfInt2.length >= 0)
      m = arrayOfInt1.length - arrayOfInt2.length;
    while (m < arrayOfInt1.length)
    {
      int n = j + 1;
      arrayOfInt2[j] = arrayOfInt1[m];
      m++;
      j = n;
      continue;
      m = 0;
    }
    return arrayOfInt2;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.lm.LanguageModelFeature
 * JD-Core Version:    0.6.2
 */