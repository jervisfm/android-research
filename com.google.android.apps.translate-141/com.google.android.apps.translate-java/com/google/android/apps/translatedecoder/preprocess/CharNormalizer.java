package com.google.android.apps.translatedecoder.preprocess;

import com.google.android.apps.translatedecoder.util.MemMapUtil;
import com.google.android.apps.translatedecoder.util.StringMap;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.RandomAccessFile;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileChannel.MapMode;
import java.text.Normalizer;
import java.text.Normalizer.Form;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CharNormalizer
  implements Serializable
{
  private static final int MAX_MMAP_SIZE = 50000000;
  private static final Logger logger = Logger.getLogger(CharNormalizer.class.getCanonicalName());
  private static final long serialVersionUID = 5262218632493529932L;
  private final StringMap charMap;
  private boolean removeDiacritics;

  public CharNormalizer(boolean paramBoolean, StringMap paramStringMap)
  {
    this.removeDiacritics = paramBoolean;
    this.charMap = paramStringMap;
  }

  public static String normalize(String paramString)
  {
    return Normalizer.normalize(paramString, Normalizer.Form.NFC);
  }

  public static CharNormalizer readFromByteBuffer(ByteBuffer paramByteBuffer)
  {
    int i = 1;
    if (paramByteBuffer.getInt() == i);
    while (true)
    {
      return new CharNormalizer(i, StringMap.readFromByteBuffer(paramByteBuffer));
      int j = 0;
    }
  }

  public static CharNormalizer readFromFile(String paramString)
  {
    try
    {
      FileChannel localFileChannel = new RandomAccessFile(paramString, "r").getChannel();
      MappedByteBuffer localMappedByteBuffer = localFileChannel.map(FileChannel.MapMode.READ_ONLY, 0L, (int)localFileChannel.size());
      if (localMappedByteBuffer.getInt() == MemMapUtil.mmapFileSignature())
        return readFromByteBuffer(localMappedByteBuffer);
      ObjectInputStream localObjectInputStream = new ObjectInputStream(new FileInputStream(paramString));
      CharNormalizer localCharNormalizer = (CharNormalizer)localObjectInputStream.readObject();
      localObjectInputStream.close();
      return localCharNormalizer;
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
    }
    return null;
  }

  public static String removeDiacriticalMarks(String paramString)
  {
    String str = Normalizer.normalize(paramString, Normalizer.Form.NFD);
    return Pattern.compile("\\p{InCombiningDiacriticalMarks}+").matcher(str).replaceAll("");
  }

  public String mapChars(String paramString)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    int i = 0;
    if (i < paramString.length())
    {
      int j = paramString.codePointAt(i);
      String str1 = new String(Character.toChars(j));
      String str2 = this.charMap.get(str1);
      if (str2 != null)
        localStringBuilder.append(str2);
      while (true)
      {
        i += Character.charCount(j);
        break;
        localStringBuilder.append(str1);
      }
    }
    return localStringBuilder.toString();
  }

  public String normalizeChars(String paramString)
  {
    if (this.removeDiacritics)
      return removeDiacriticalMarks(mapChars(normalize(paramString)));
    return mapChars(normalize(paramString));
  }

  public boolean removeDiacritics()
  {
    return this.removeDiacritics;
  }

  public void setRemoveDiacritics(boolean paramBoolean)
  {
    this.removeDiacritics = paramBoolean;
  }

  public void writeToByteBuffer(ByteBuffer paramByteBuffer)
  {
    if (this.removeDiacritics);
    for (int i = 1; ; i = 0)
    {
      paramByteBuffer.putInt(i);
      this.charMap.writeToByteBuffer(paramByteBuffer);
      return;
    }
  }

  public void writeToFile(String paramString)
  {
    writeToFile(paramString, false);
  }

  public void writeToFile(String paramString, boolean paramBoolean)
  {
    if (paramBoolean);
    try
    {
      RandomAccessFile localRandomAccessFile = new RandomAccessFile(paramString, "rw");
      FileChannel localFileChannel = localRandomAccessFile.getChannel();
      MappedByteBuffer localMappedByteBuffer = localFileChannel.map(FileChannel.MapMode.READ_WRITE, 0L, 50000000L);
      localMappedByteBuffer.putInt(MemMapUtil.mmapFileSignature());
      writeToByteBuffer(localMappedByteBuffer);
      logger.info("Final buffer size is " + localMappedByteBuffer.position());
      localFileChannel.truncate(localMappedByteBuffer.position());
      localRandomAccessFile.close();
      return;
      ObjectOutputStream localObjectOutputStream = new ObjectOutputStream(new FileOutputStream(paramString));
      localObjectOutputStream.writeObject(this);
      localObjectOutputStream.close();
      return;
    }
    catch (IOException localIOException)
    {
      localIOException.printStackTrace();
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.preprocess.CharNormalizer
 * JD-Core Version:    0.6.2
 */