package com.google.android.apps.translatedecoder.preprocess;

import com.google.android.apps.translatedecoder.util.ArrayBasedStringMap;
import com.google.android.apps.translatedecoder.util.ArrayBasedStringSet;
import com.google.android.apps.translatedecoder.util.ConfigParser;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PreprocDataConverter
{
  private static final Logger logger = Logger.getLogger(PreprocDataConverter.class.getCanonicalName());
  Set<String> preprocAbbrData;
  Map<String, String> preprocMapData;

  private static String handleSpace(String paramString)
  {
    if (paramString.matches("^\\s+$"))
      return " ";
    return paramString.trim();
  }

  public static void main(String[] paramArrayOfString)
  {
    if (paramArrayOfString.length <= 0)
    {
      System.out.println("Usage: blaze run PreprocDataConverter -- --charMapFile=file --charMapOutFile=file--preprocDataFile=file --preprocDataOutFile=file --mmapFormat=flag--removeDiacritics=flag");
      System.exit(1);
    }
    ConfigParser localConfigParser = new ConfigParser(paramArrayOfString);
    localConfigParser.checkRequiredProperties(new String[] { "charMapFile", "charMapOutFile", "preprocDataFile", "preprocDataOutFile" });
    Map localMap = readCharMapTbl(localConfigParser.getProperty("charMapFile"));
    CharNormalizer localCharNormalizer = new CharNormalizer(new Boolean(localConfigParser.getProperty("removeDiacritics", "false")).booleanValue(), new ArrayBasedStringMap(localMap));
    localCharNormalizer.writeToFile(localConfigParser.getProperty("charMapOutFile"), new Boolean(localConfigParser.getProperty("mmapFormat", "true")).booleanValue());
    PreprocDataConverter localPreprocDataConverter = new PreprocDataConverter();
    localPreprocDataConverter.readPreprocData(localConfigParser.getProperty("preprocDataFile"), localCharNormalizer);
    new Tokenizer(new ArrayBasedStringSet(localPreprocDataConverter.preprocAbbrData), new ArrayBasedStringMap(localPreprocDataConverter.preprocMapData), localCharNormalizer).writeToFile(localConfigParser.getProperty("preprocDataOutFile"), new Boolean(localConfigParser.getProperty("mmapFormat", "true")).booleanValue());
  }

  public static Map<String, String> readCharMapTbl(String paramString)
  {
    HashMap localHashMap;
    DataInputStream localDataInputStream;
    while (true)
    {
      String str2;
      try
      {
        localHashMap = new HashMap();
        localDataInputStream = new DataInputStream(new FileInputStream(paramString));
        BufferedReader localBufferedReader = new BufferedReader(new InputStreamReader(localDataInputStream, "utf8"));
        Pattern localPattern = Pattern.compile("\\|\\s+(.{1,4})\\s+\\|\\s+(.{1,8})\\s+\\|\\s+(.{1,4})\\s+\\|\\s+(.{1,8})\\s+\\|\\s+\\|");
        String str1 = localBufferedReader.readLine();
        if (str1 == null)
          break;
        str2 = CharNormalizer.normalize(str1);
        Matcher localMatcher = localPattern.matcher(str2);
        if ((localMatcher.find()) && (localMatcher.groupCount() == 4))
        {
          String str3 = handleSpace(localMatcher.group(1));
          String str4 = handleSpace(localMatcher.group(3));
          if ((str4.matches("^\\s+$")) && (localMatcher.group(4).matches("^\\s*0\\s*$")))
            str4 = str3;
          localHashMap.put(str3, str4);
          if ((str3.length() <= 4) && (str4.length() <= 4))
            continue;
          logger.severe("A human-perceivable char has more than four machine chars (utf16)! Theline is " + str2);
          System.exit(1);
          continue;
        }
      }
      catch (Exception localException)
      {
        localException.printStackTrace();
        System.exit(1);
        return null;
      }
      logger.severe("No match with the regular expression! The line is " + str2 + ". The file" + " is " + paramString);
      System.exit(1);
    }
    localDataInputStream.close();
    return localHashMap;
  }

  public void readPreprocData(String paramString, CharNormalizer paramCharNormalizer)
  {
    DataInputStream localDataInputStream;
    try
    {
      logger.info("Read preproc data from " + paramString);
      this.preprocAbbrData = new HashSet();
      this.preprocMapData = new HashMap();
      localDataInputStream = new DataInputStream(new FileInputStream(paramString));
      BufferedReader localBufferedReader = new BufferedReader(new InputStreamReader(localDataInputStream));
      Pattern localPattern1 = Pattern.compile("^([a-zA-Z]+)\\s+[aA][bB][bB][rR][eE][vV]\\s+(.+)$");
      localPattern2 = Pattern.compile("^([a-zA-Z]+)\\s+word_map\\s+([^\\s]+)\\s+(.+)$");
      while (true)
      {
        String str1 = localBufferedReader.readLine();
        if (str1 == null)
          break label244;
        str2 = paramCharNormalizer.normalizeChars(str1);
        Matcher localMatcher1 = localPattern1.matcher(str2);
        if ((!localMatcher1.find()) || (localMatcher1.groupCount() != 2))
          break;
        this.preprocAbbrData.add(Tokenizer.getAbbrSignature(localMatcher1.group(1), localMatcher1.group(2)));
      }
    }
    catch (FileNotFoundException localFileNotFoundException)
    {
      while (true)
      {
        Pattern localPattern2;
        String str2;
        localFileNotFoundException.printStackTrace();
        return;
        Matcher localMatcher2 = localPattern2.matcher(str2);
        if ((localMatcher2.find()) && (localMatcher2.groupCount() == 3))
          this.preprocMapData.put(Tokenizer.getMapSignature(localMatcher2.group(1), localMatcher2.group(2)), localMatcher2.group(3));
      }
    }
    catch (IOException localIOException)
    {
      localIOException.printStackTrace();
      return;
    }
    label244: localDataInputStream.close();
    logger.info("abbrSize=" + this.preprocAbbrData.size());
    logger.info("mapSize=" + this.preprocMapData.size());
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.preprocess.PreprocDataConverter
 * JD-Core Version:    0.6.2
 */