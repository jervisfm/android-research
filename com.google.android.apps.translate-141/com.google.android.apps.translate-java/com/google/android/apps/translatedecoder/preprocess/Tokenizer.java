package com.google.android.apps.translatedecoder.preprocess;

import com.google.android.apps.translatedecoder.util.ConfigParser;
import com.google.android.apps.translatedecoder.util.MemMapUtil;
import com.google.android.apps.translatedecoder.util.StringMap;
import com.google.android.apps.translatedecoder.util.StringSet;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.RandomAccessFile;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileChannel.MapMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

public class Tokenizer
  implements Serializable
{
  private static final Logger logger = Logger.getLogger(Tokenizer.class.getCanonicalName());
  private static final long serialVersionUID = -3438434462125278275L;
  private final CharNormalizer charNormalizer;
  private final boolean handleSpecialPuncts = true;
  private final StringSet preprocAbbrData;
  private final StringMap preprocMapData;

  public Tokenizer(StringSet paramStringSet, StringMap paramStringMap, CharNormalizer paramCharNormalizer)
  {
    this.preprocAbbrData = paramStringSet;
    this.preprocMapData = paramStringMap;
    this.charNormalizer = paramCharNormalizer;
  }

  public static String getAbbrSignature(String paramString1, String paramString2)
  {
    return paramString1 + paramString2;
  }

  public static String getMapSignature(String paramString1, String paramString2)
  {
    return paramString1 + paramString2;
  }

  public static boolean isCJKT(String paramString)
  {
    return (paramString.equalsIgnoreCase("chinese")) || (paramString.toLowerCase().startsWith("zh")) || (paramString.equalsIgnoreCase("japanese")) || (paramString.equalsIgnoreCase("ja")) || (paramString.equalsIgnoreCase("korean")) || (paramString.equalsIgnoreCase("ko")) || (paramString.equalsIgnoreCase("thai")) || (paramString.equalsIgnoreCase("th"));
  }

  public static void main(String[] paramArrayOfString)
  {
    if (paramArrayOfString.length <= 0)
    {
      System.out.println("Usage: java Tokenizer --inputFile=file --outputFile=file --language=language --preprocDataFile=file");
      System.exit(1);
    }
    ConfigParser localConfigParser = new ConfigParser(paramArrayOfString);
    localConfigParser.checkRequiredProperties(new String[] { "inputFile", "outputFile", "language", "preprocDataFile" });
    Tokenizer localTokenizer = readFromFile(localConfigParser.getProperty("preprocDataFile"));
    try
    {
      localDataInputStream = new DataInputStream(new FileInputStream(localConfigParser.getProperty("inputFile")));
      BufferedReader localBufferedReader = new BufferedReader(new InputStreamReader(localDataInputStream));
      localDataOutputStream = new DataOutputStream(new FileOutputStream(localConfigParser.getProperty("outputFile")));
      localBufferedWriter = new BufferedWriter(new OutputStreamWriter(localDataOutputStream));
      while (true)
      {
        String str1 = localBufferedReader.readLine();
        if (str1 == null)
          break;
        logger.info("Process: " + str1);
        String str2 = localTokenizer.tokenizeWithJoin(localConfigParser.getProperty("language"), str1.trim());
        localBufferedWriter.write(str2 + "\n");
      }
    }
    catch (FileNotFoundException localFileNotFoundException)
    {
      DataInputStream localDataInputStream;
      DataOutputStream localDataOutputStream;
      BufferedWriter localBufferedWriter;
      localFileNotFoundException.printStackTrace();
      return;
      localDataInputStream.close();
      localBufferedWriter.flush();
      localDataOutputStream.close();
      return;
    }
    catch (IOException localIOException)
    {
      localIOException.printStackTrace();
    }
  }

  public static Tokenizer readFromByteBuffer(ByteBuffer paramByteBuffer)
  {
    return new Tokenizer(StringSet.readFromByteBuffer(paramByteBuffer), StringMap.readFromByteBuffer(paramByteBuffer), CharNormalizer.readFromByteBuffer(paramByteBuffer));
  }

  public static Tokenizer readFromFile(String paramString)
  {
    try
    {
      FileChannel localFileChannel = new RandomAccessFile(paramString, "r").getChannel();
      MappedByteBuffer localMappedByteBuffer = localFileChannel.map(FileChannel.MapMode.READ_ONLY, 0L, (int)localFileChannel.size());
      if (localMappedByteBuffer.getInt() == MemMapUtil.mmapFileSignature())
      {
        Tokenizer localTokenizer2 = readFromByteBuffer(localMappedByteBuffer);
        localFileChannel.close();
        return localTokenizer2;
      }
      ObjectInputStream localObjectInputStream = new ObjectInputStream(new FileInputStream(paramString));
      Tokenizer localTokenizer1 = (Tokenizer)localObjectInputStream.readObject();
      localObjectInputStream.close();
      localFileChannel.close();
      return localTokenizer1;
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
    }
    return null;
  }

  public String deTokenize(String paramString1, String paramString2)
  {
    if (isCJKT(paramString1))
      return paramString2.replaceAll("\\s+", "");
    StringBuilder localStringBuilder = new StringBuilder();
    int i = 0;
    int j = 0;
    if (j < paramString2.length())
    {
      int k = paramString2.codePointAt(j);
      String str = new String(Character.toChars(k));
      if ((i != 0) && (!str.matches("^\\p{Punct}$")))
        localStringBuilder.append(" ");
      if (str.matches("^\\p{Space}$"));
      for (i = 1; ; i = 0)
      {
        j += Character.charCount(k);
        break;
        localStringBuilder.append(str);
      }
    }
    return localStringBuilder.toString();
  }

  public boolean getWordMap(String paramString1, String paramString2, List<String> paramList)
  {
    String str = this.preprocMapData.get(getMapSignature(paramString1, paramString2));
    if (str != null)
    {
      paramList.clear();
      paramList.addAll(Arrays.asList(str.split("\\s+")));
      return true;
    }
    return false;
  }

  public boolean isAbbrev(String paramString1, String paramString2)
  {
    return this.preprocAbbrData.contains(getAbbrSignature(paramString1, paramString2));
  }

  public boolean isNumber(String paramString)
  {
    return paramString.matches("-*[\\d]+\\.[\\d]+");
  }

  public boolean isSpecialPunct(String paramString)
  {
    return (paramString.matches("&apos;")) || (paramString.matches("&quot;"));
  }

  public String replaceSpecialPuncts(String paramString)
  {
    return paramString.replace("&apos;", " &apos; ").replace("&quot;", " &quot; ");
  }

  public List<String> tokenize(String paramString1, String paramString2)
  {
    if (isCJKT(paramString1))
    {
      localObject = tokenizeForCJK(paramString1, paramString2);
      return localObject;
    }
    String[] arrayOfString = this.charNormalizer.normalizeChars(paramString2).split("\\s+");
    Object localObject = new ArrayList();
    ArrayList localArrayList = new ArrayList();
    int i = arrayOfString.length;
    int j = 0;
    label58: String str1;
    if (j < i)
    {
      str1 = arrayOfString[j];
      if ((!isAbbrev(paramString1, str1)) && (!isNumber(str1)) && (!isSpecialPunct(str1)))
        break label115;
      ((List)localObject).add(str1);
    }
    label220: 
    while (true)
    {
      j++;
      break label58;
      break;
      label115: if (getWordMap(paramString1, str1, localArrayList))
      {
        ((List)localObject).addAll(localArrayList);
      }
      else
      {
        StringBuilder localStringBuilder = null;
        int k = 0;
        if (k < str1.length())
        {
          int m = str1.codePointAt(k);
          String str2 = new String(Character.toChars(m));
          int n;
          int i1;
          if (str2.matches("^\\p{Punct}$"))
            if (str2.compareTo("'") == 0)
              if (localStringBuilder != null)
              {
                n = localStringBuilder.charAt(-1 + localStringBuilder.length());
                if (k + Character.charCount(m) >= str1.length())
                  break label322;
                i1 = str1.codePointAt(k + Character.charCount(m));
                label251: if ((n != 108) && (n != 76) && (n != 100) && (n != 68))
                  break label328;
                localStringBuilder.append(str2);
                ((List)localObject).add(localStringBuilder.toString());
                localStringBuilder = null;
              }
          while (true)
          {
            k += Character.charCount(m);
            break;
            n = 0;
            break label220;
            i1 = 0;
            break label251;
            if ((i1 == 115) && (localStringBuilder != null))
            {
              ((List)localObject).add(localStringBuilder.toString());
              localStringBuilder = null;
            }
            if (localStringBuilder == null)
              localStringBuilder = new StringBuilder();
            localStringBuilder.append(str2);
            continue;
            if (localStringBuilder != null)
              ((List)localObject).add(localStringBuilder.toString());
            ((List)localObject).add(str2);
            localStringBuilder = null;
            continue;
            if (localStringBuilder == null)
              localStringBuilder = new StringBuilder();
            localStringBuilder.append(str2);
          }
        }
        if (localStringBuilder != null)
          ((List)localObject).add(localStringBuilder.toString());
      }
    }
  }

  public List<String> tokenizeForCJK(String paramString1, String paramString2)
  {
    String[] arrayOfString = this.charNormalizer.normalizeChars(paramString2).split("\\s+");
    ArrayList localArrayList = new ArrayList();
    int i = arrayOfString.length;
    for (int j = 0; j < i; j++)
    {
      String str1 = arrayOfString[j];
      StringBuilder localStringBuilder = null;
      int k = 0;
      if (k < str1.length())
      {
        int m = str1.codePointAt(k);
        String str2 = String.valueOf(Character.toChars(m));
        if (m > 127)
        {
          if (localStringBuilder != null)
            localArrayList.add(localStringBuilder.toString());
          localArrayList.add(str2);
          localStringBuilder = null;
        }
        while (true)
        {
          k += Character.charCount(m);
          break;
          if (localStringBuilder == null)
            localStringBuilder = new StringBuilder();
          localStringBuilder.append(str2);
        }
      }
      if (localStringBuilder != null)
        localArrayList.add(localStringBuilder.toString());
    }
    return localArrayList;
  }

  public String tokenizeWithJoin(String paramString1, String paramString2)
  {
    List localList = tokenize(paramString1, replaceSpecialPuncts(paramString2));
    StringBuilder localStringBuilder = new StringBuilder();
    for (int i = 0; i < localList.size(); i++)
    {
      localStringBuilder.append((String)localList.get(i));
      if (i < -1 + localList.size())
        localStringBuilder.append(" ");
    }
    return localStringBuilder.toString();
  }

  public void writeToByteBuffer(ByteBuffer paramByteBuffer)
  {
    this.preprocAbbrData.writeToByteBuffer(paramByteBuffer);
    this.preprocMapData.writeToByteBuffer(paramByteBuffer);
    this.charNormalizer.writeToByteBuffer(paramByteBuffer);
  }

  public void writeToFile(String paramString)
  {
    writeToFile(paramString, false);
  }

  public void writeToFile(String paramString, boolean paramBoolean)
  {
    if (paramBoolean);
    try
    {
      RandomAccessFile localRandomAccessFile = new RandomAccessFile(paramString, "rw");
      FileChannel localFileChannel = localRandomAccessFile.getChannel();
      MappedByteBuffer localMappedByteBuffer = localFileChannel.map(FileChannel.MapMode.READ_WRITE, 0L, 100000000L);
      localMappedByteBuffer.putInt(MemMapUtil.mmapFileSignature());
      writeToByteBuffer(localMappedByteBuffer);
      logger.info("Final buffer size is " + localMappedByteBuffer.position());
      localFileChannel.truncate(localMappedByteBuffer.position());
      localRandomAccessFile.close();
      return;
      ObjectOutputStream localObjectOutputStream = new ObjectOutputStream(new FileOutputStream(paramString));
      localObjectOutputStream.writeObject(this);
      localObjectOutputStream.close();
      return;
    }
    catch (IOException localIOException)
    {
      localIOException.printStackTrace();
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.preprocess.Tokenizer
 * JD-Core Version:    0.6.2
 */