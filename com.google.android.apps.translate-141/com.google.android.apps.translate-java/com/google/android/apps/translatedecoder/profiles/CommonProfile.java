package com.google.android.apps.translatedecoder.profiles;

import org.json.JSONException;
import org.json.JSONObject;

public class CommonProfile
{
  private String configFile;
  private String preprocFile;
  private int version;

  public static Builder newBuilder()
  {
    return new Builder();
  }

  public static CommonProfile readFrom(JSONObject paramJSONObject)
    throws OfflineDataProfileException
  {
    try
    {
      CommonProfile localCommonProfile = newBuilder().setConfigFile(paramJSONObject.getString(Key.CONFIG.name())).setPreprocFile(paramJSONObject.getString(Key.PREPROC.name())).setVersion(paramJSONObject.getInt(Key.VERSION.name())).build();
      return localCommonProfile;
    }
    catch (JSONException localJSONException)
    {
      throw new OfflineDataProfileException("Invalid format found when reading profile.", localJSONException);
    }
  }

  public String getConfigFile()
  {
    return this.configFile;
  }

  public JSONObject getJsonObject()
    throws JSONException
  {
    JSONObject localJSONObject = new JSONObject();
    localJSONObject.put(Key.CONFIG.name(), this.configFile);
    localJSONObject.put(Key.PREPROC.name(), this.preprocFile);
    localJSONObject.put(Key.VERSION.name(), this.version);
    return localJSONObject;
  }

  public String getPreprocFile()
  {
    return this.preprocFile;
  }

  public int getVersion()
  {
    return this.version;
  }

  public String toString()
  {
    try
    {
      String str = getJsonObject().toString(2);
      return str;
    }
    catch (JSONException localJSONException)
    {
      return "configFile=" + this.configFile + " error=" + localJSONException.getMessage();
    }
  }

  public static class Builder
  {
    private CommonProfile commonProfile = new CommonProfile();

    public CommonProfile build()
    {
      return this.commonProfile;
    }

    public Builder setConfigFile(String paramString)
    {
      CommonProfile.access$002(this.commonProfile, paramString);
      return this;
    }

    public Builder setPreprocFile(String paramString)
    {
      CommonProfile.access$102(this.commonProfile, paramString);
      return this;
    }

    public Builder setVersion(int paramInt)
    {
      CommonProfile.access$202(this.commonProfile, paramInt);
      return this;
    }
  }

  private static enum Key
  {
    static
    {
      Key[] arrayOfKey = new Key[3];
      arrayOfKey[0] = CONFIG;
      arrayOfKey[1] = PREPROC;
      arrayOfKey[2] = VERSION;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.profiles.CommonProfile
 * JD-Core Version:    0.6.2
 */