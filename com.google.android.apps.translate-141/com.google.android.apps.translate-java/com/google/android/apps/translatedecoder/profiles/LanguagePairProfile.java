package com.google.android.apps.translatedecoder.profiles;

import org.json.JSONException;
import org.json.JSONObject;

public class LanguagePairProfile
{
  private String configFile;
  private String fromLangCode;
  private String ptSymbolFile;
  private String tmFile;
  private String toLangCode;
  private int version;

  public static Builder newBuilder()
  {
    return new Builder();
  }

  public static LanguagePairProfile readFrom(JSONObject paramJSONObject)
    throws OfflineDataProfileException
  {
    try
    {
      LanguagePairProfile localLanguagePairProfile = newBuilder().setFromLangCode(paramJSONObject.getString(Key.FROMLANG.name())).setToLangCode(paramJSONObject.getString(Key.TOLANG.name())).setConfigFile(paramJSONObject.getString(Key.CONFIG.name())).setPtSymbolFile(paramJSONObject.getString(Key.PTSYMBOL.name())).setTmFile(paramJSONObject.getString(Key.TM.name())).setVersion(paramJSONObject.getInt(Key.VERSION.name())).build();
      return localLanguagePairProfile;
    }
    catch (JSONException localJSONException)
    {
      throw new OfflineDataProfileException("Invalid format found when reading profile.", localJSONException);
    }
  }

  public String getConfigFile()
  {
    return this.configFile;
  }

  public String getFromLangCode()
  {
    return this.fromLangCode;
  }

  public JSONObject getJsonObject()
    throws JSONException
  {
    JSONObject localJSONObject = new JSONObject();
    localJSONObject.put(Key.FROMLANG.name(), this.fromLangCode);
    localJSONObject.put(Key.TOLANG.name(), this.toLangCode);
    localJSONObject.put(Key.CONFIG.name(), this.configFile);
    localJSONObject.put(Key.PTSYMBOL.name(), this.ptSymbolFile);
    localJSONObject.put(Key.TM.name(), this.tmFile);
    localJSONObject.put(Key.VERSION.name(), this.version);
    return localJSONObject;
  }

  public String getPtSymbolFile()
  {
    return this.ptSymbolFile;
  }

  public String getTmFile()
  {
    return this.tmFile;
  }

  public String getToLangCode()
  {
    return this.toLangCode;
  }

  public int getVersion()
  {
    return this.version;
  }

  public String toString()
  {
    try
    {
      String str = getJsonObject().toString(2);
      return str;
    }
    catch (JSONException localJSONException)
    {
      return "fromLangCode=" + this.fromLangCode + " toLangCode=" + this.toLangCode + " error=" + localJSONException.getMessage();
    }
  }

  public static class Builder
  {
    private LanguagePairProfile langPairProfile = new LanguagePairProfile();

    public LanguagePairProfile build()
    {
      return this.langPairProfile;
    }

    public Builder setConfigFile(String paramString)
    {
      LanguagePairProfile.access$202(this.langPairProfile, paramString);
      return this;
    }

    public Builder setFromLangCode(String paramString)
    {
      LanguagePairProfile.access$002(this.langPairProfile, paramString);
      return this;
    }

    public Builder setPtSymbolFile(String paramString)
    {
      LanguagePairProfile.access$302(this.langPairProfile, paramString);
      return this;
    }

    public Builder setTmFile(String paramString)
    {
      LanguagePairProfile.access$402(this.langPairProfile, paramString);
      return this;
    }

    public Builder setToLangCode(String paramString)
    {
      LanguagePairProfile.access$102(this.langPairProfile, paramString);
      return this;
    }

    public Builder setVersion(int paramInt)
    {
      LanguagePairProfile.access$502(this.langPairProfile, paramInt);
      return this;
    }
  }

  private static enum Key
  {
    static
    {
      CONFIG = new Key("CONFIG", 2);
      PTSYMBOL = new Key("PTSYMBOL", 3);
      TM = new Key("TM", 4);
      VERSION = new Key("VERSION", 5);
      Key[] arrayOfKey = new Key[6];
      arrayOfKey[0] = FROMLANG;
      arrayOfKey[1] = TOLANG;
      arrayOfKey[2] = CONFIG;
      arrayOfKey[3] = PTSYMBOL;
      arrayOfKey[4] = TM;
      arrayOfKey[5] = VERSION;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.profiles.LanguagePairProfile
 * JD-Core Version:    0.6.2
 */