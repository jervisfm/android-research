package com.google.android.apps.translatedecoder.profiles;

import org.json.JSONException;
import org.json.JSONObject;

public class LanguageProfile
{
  private String langCode;
  private String lmFile;
  private String lmSymbolFile;
  private int version;

  public static Builder newBuilder()
  {
    return new Builder();
  }

  public static LanguageProfile readFrom(JSONObject paramJSONObject)
    throws OfflineDataProfileException
  {
    try
    {
      LanguageProfile localLanguageProfile = newBuilder().setLangCode(paramJSONObject.getString(Key.LANG.name())).setLmSymbolFile(paramJSONObject.getString(Key.LMSYMBOL.name())).setLmFile(paramJSONObject.getString(Key.LM.name())).setVersion(paramJSONObject.getInt(Key.VERSION.name())).build();
      return localLanguageProfile;
    }
    catch (JSONException localJSONException)
    {
      throw new OfflineDataProfileException("Invalid format found when reading profile.", localJSONException);
    }
  }

  public JSONObject getJsonObject()
    throws JSONException
  {
    JSONObject localJSONObject = new JSONObject();
    localJSONObject.put(Key.LANG.name(), this.langCode);
    localJSONObject.put(Key.LMSYMBOL.name(), this.lmSymbolFile);
    localJSONObject.put(Key.LM.name(), this.lmFile);
    localJSONObject.put(Key.VERSION.name(), this.version);
    return localJSONObject;
  }

  public String getLangCode()
  {
    return this.langCode;
  }

  public String getLmFile()
  {
    return this.lmFile;
  }

  public String getLmSymbolFile()
  {
    return this.lmSymbolFile;
  }

  public int getVersion()
  {
    return this.version;
  }

  public String toString()
  {
    try
    {
      String str = getJsonObject().toString(2);
      return str;
    }
    catch (JSONException localJSONException)
    {
      return "langCode=" + this.langCode + " error=" + localJSONException.getMessage();
    }
  }

  public static class Builder
  {
    private LanguageProfile langProfile = new LanguageProfile();

    public LanguageProfile build()
    {
      return this.langProfile;
    }

    public Builder setLangCode(String paramString)
    {
      LanguageProfile.access$002(this.langProfile, paramString);
      return this;
    }

    public Builder setLmFile(String paramString)
    {
      LanguageProfile.access$202(this.langProfile, paramString);
      return this;
    }

    public Builder setLmSymbolFile(String paramString)
    {
      LanguageProfile.access$102(this.langProfile, paramString);
      return this;
    }

    public Builder setVersion(int paramInt)
    {
      LanguageProfile.access$302(this.langProfile, paramInt);
      return this;
    }
  }

  private static enum Key
  {
    static
    {
      LM = new Key("LM", 2);
      VERSION = new Key("VERSION", 3);
      Key[] arrayOfKey = new Key[4];
      arrayOfKey[0] = LANG;
      arrayOfKey[1] = LMSYMBOL;
      arrayOfKey[2] = LM;
      arrayOfKey[3] = VERSION;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.profiles.LanguageProfile
 * JD-Core Version:    0.6.2
 */