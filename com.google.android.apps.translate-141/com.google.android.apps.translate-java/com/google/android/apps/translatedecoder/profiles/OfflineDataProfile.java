package com.google.android.apps.translatedecoder.profiles;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class OfflineDataProfile
{
  private static final int BUFFSIZE = 1024;
  public static final int JSON_INDENT = 2;
  private CommonProfile commonProfile;
  private Set<LanguagePairProfile> languagePairProfiles = new HashSet();
  private Set<LanguageProfile> languageProfiles = new HashSet();

  public static String getProfileString(OfflineDataProfile paramOfflineDataProfile)
    throws OfflineDataProfileException
  {
    try
    {
      String str = paramOfflineDataProfile.getJsonObject().toString(2);
      return str;
    }
    catch (JSONException localJSONException)
    {
      throw new OfflineDataProfileException("Invalid format found when writing profile.", localJSONException);
    }
  }

  public static Builder newBuilder()
  {
    return new Builder();
  }

  // ERROR //
  public static OfflineDataProfile readFrom(JSONObject paramJSONObject)
    throws OfflineDataProfileException
  {
    // Byte code:
    //   0: aload_0
    //   1: getstatic 68	com/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key:COMMON	Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key;
    //   4: invokevirtual 72	com/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key:name	()Ljava/lang/String;
    //   7: invokevirtual 76	org/json/JSONObject:getJSONObject	(Ljava/lang/String;)Lorg/json/JSONObject;
    //   10: invokestatic 81	com/google/android/apps/translatedecoder/profiles/CommonProfile:readFrom	(Lorg/json/JSONObject;)Lcom/google/android/apps/translatedecoder/profiles/CommonProfile;
    //   13: astore_2
    //   14: invokestatic 83	com/google/android/apps/translatedecoder/profiles/OfflineDataProfile:newBuilder	()Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Builder;
    //   17: aload_2
    //   18: invokevirtual 87	com/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Builder:setCommonProfile	(Lcom/google/android/apps/translatedecoder/profiles/CommonProfile;)Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Builder;
    //   21: astore_3
    //   22: aload_0
    //   23: getstatic 90	com/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key:LANG	Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key;
    //   26: invokevirtual 72	com/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key:name	()Ljava/lang/String;
    //   29: invokevirtual 94	org/json/JSONObject:getJSONArray	(Ljava/lang/String;)Lorg/json/JSONArray;
    //   32: astore 5
    //   34: iconst_0
    //   35: istore 6
    //   37: iload 6
    //   39: aload 5
    //   41: invokevirtual 100	org/json/JSONArray:length	()I
    //   44: if_icmpge +50 -> 94
    //   47: aload_3
    //   48: aload 5
    //   50: iload 6
    //   52: invokevirtual 103	org/json/JSONArray:getJSONObject	(I)Lorg/json/JSONObject;
    //   55: invokestatic 108	com/google/android/apps/translatedecoder/profiles/LanguageProfile:readFrom	(Lorg/json/JSONObject;)Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile;
    //   58: invokevirtual 112	com/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Builder:addLanguageProfiles	(Lcom/google/android/apps/translatedecoder/profiles/LanguageProfile;)Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Builder;
    //   61: pop
    //   62: iinc 6 1
    //   65: goto -28 -> 37
    //   68: astore_1
    //   69: new 38	com/google/android/apps/translatedecoder/profiles/OfflineDataProfileException
    //   72: dup
    //   73: ldc 114
    //   75: aload_1
    //   76: invokespecial 55	com/google/android/apps/translatedecoder/profiles/OfflineDataProfileException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   79: athrow
    //   80: astore 4
    //   82: new 38	com/google/android/apps/translatedecoder/profiles/OfflineDataProfileException
    //   85: dup
    //   86: ldc 116
    //   88: aload 4
    //   90: invokespecial 55	com/google/android/apps/translatedecoder/profiles/OfflineDataProfileException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   93: athrow
    //   94: aload_0
    //   95: getstatic 119	com/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key:LANGPAIR	Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key;
    //   98: invokevirtual 72	com/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Key:name	()Ljava/lang/String;
    //   101: invokevirtual 94	org/json/JSONObject:getJSONArray	(Ljava/lang/String;)Lorg/json/JSONArray;
    //   104: astore 8
    //   106: iconst_0
    //   107: istore 9
    //   109: iload 9
    //   111: aload 8
    //   113: invokevirtual 100	org/json/JSONArray:length	()I
    //   116: if_icmpge +38 -> 154
    //   119: aload_3
    //   120: aload 8
    //   122: iload 9
    //   124: invokevirtual 103	org/json/JSONArray:getJSONObject	(I)Lorg/json/JSONObject;
    //   127: invokestatic 124	com/google/android/apps/translatedecoder/profiles/LanguagePairProfile:readFrom	(Lorg/json/JSONObject;)Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;
    //   130: invokevirtual 128	com/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Builder:addLanguagePairProfiles	(Lcom/google/android/apps/translatedecoder/profiles/LanguagePairProfile;)Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Builder;
    //   133: pop
    //   134: iinc 9 1
    //   137: goto -28 -> 109
    //   140: astore 7
    //   142: new 38	com/google/android/apps/translatedecoder/profiles/OfflineDataProfileException
    //   145: dup
    //   146: ldc 130
    //   148: aload 7
    //   150: invokespecial 55	com/google/android/apps/translatedecoder/profiles/OfflineDataProfileException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   153: athrow
    //   154: aload_3
    //   155: invokevirtual 134	com/google/android/apps/translatedecoder/profiles/OfflineDataProfile$Builder:build	()Lcom/google/android/apps/translatedecoder/profiles/OfflineDataProfile;
    //   158: areturn
    //
    // Exception table:
    //   from	to	target	type
    //   0	14	68	org/json/JSONException
    //   22	34	80	org/json/JSONException
    //   37	62	80	org/json/JSONException
    //   94	106	140	org/json/JSONException
    //   109	134	140	org/json/JSONException
  }

  public static OfflineDataProfile readFromFile(String paramString)
    throws OfflineDataProfileException
  {
    String str = readTextFromFile(paramString);
    try
    {
      OfflineDataProfile localOfflineDataProfile = readFrom(new JSONObject(str));
      return localOfflineDataProfile;
    }
    catch (JSONException localJSONException)
    {
      throw new OfflineDataProfileException("Invalid format found when reading profile. jsonString=" + str, localJSONException);
    }
  }

  private static String readTextFrom(DataInputStream paramDataInputStream)
    throws OfflineDataProfileException
  {
    byte[] arrayOfByte = new byte[1024];
    StringBuilder localStringBuilder = new StringBuilder();
    try
    {
      int i = paramDataInputStream.read(arrayOfByte, 0, 1024);
      if (i != -1)
        for (int j = 0; j < i; j++)
          localStringBuilder.append((char)arrayOfByte[j]);
    }
    catch (IOException localIOException)
    {
      throw new OfflineDataProfileException("I/O error happened when reading profile. offset=" + 0, localIOException);
    }
    return localStringBuilder.toString();
  }

  // ERROR //
  public static String readTextFromFile(String paramString)
    throws OfflineDataProfileException
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_1
    //   2: new 162	java/io/DataInputStream
    //   5: dup
    //   6: new 178	java/io/FileInputStream
    //   9: dup
    //   10: aload_0
    //   11: invokespecial 179	java/io/FileInputStream:<init>	(Ljava/lang/String;)V
    //   14: invokespecial 182	java/io/DataInputStream:<init>	(Ljava/io/InputStream;)V
    //   17: astore_2
    //   18: aload_2
    //   19: invokestatic 184	com/google/android/apps/translatedecoder/profiles/OfflineDataProfile:readTextFrom	(Ljava/io/DataInputStream;)Ljava/lang/String;
    //   22: astore 6
    //   24: aload_2
    //   25: ifnull +7 -> 32
    //   28: aload_2
    //   29: invokevirtual 187	java/io/DataInputStream:close	()V
    //   32: aload 6
    //   34: areturn
    //   35: astore 7
    //   37: new 38	com/google/android/apps/translatedecoder/profiles/OfflineDataProfileException
    //   40: dup
    //   41: new 147	java/lang/StringBuilder
    //   44: dup
    //   45: invokespecial 148	java/lang/StringBuilder:<init>	()V
    //   48: ldc 189
    //   50: invokevirtual 154	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   53: aload_0
    //   54: invokevirtual 154	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   57: invokevirtual 156	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   60: aload 7
    //   62: invokespecial 55	com/google/android/apps/translatedecoder/profiles/OfflineDataProfileException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   65: athrow
    //   66: astore_3
    //   67: new 38	com/google/android/apps/translatedecoder/profiles/OfflineDataProfileException
    //   70: dup
    //   71: new 147	java/lang/StringBuilder
    //   74: dup
    //   75: invokespecial 148	java/lang/StringBuilder:<init>	()V
    //   78: ldc 191
    //   80: invokevirtual 154	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   83: aload_0
    //   84: invokevirtual 154	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   87: invokevirtual 156	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   90: aload_3
    //   91: invokespecial 55	com/google/android/apps/translatedecoder/profiles/OfflineDataProfileException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   94: athrow
    //   95: astore 4
    //   97: aload_1
    //   98: ifnull +7 -> 105
    //   101: aload_1
    //   102: invokevirtual 187	java/io/DataInputStream:close	()V
    //   105: aload 4
    //   107: athrow
    //   108: astore 5
    //   110: new 38	com/google/android/apps/translatedecoder/profiles/OfflineDataProfileException
    //   113: dup
    //   114: new 147	java/lang/StringBuilder
    //   117: dup
    //   118: invokespecial 148	java/lang/StringBuilder:<init>	()V
    //   121: ldc 189
    //   123: invokevirtual 154	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   126: aload_0
    //   127: invokevirtual 154	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   130: invokevirtual 156	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   133: aload 5
    //   135: invokespecial 55	com/google/android/apps/translatedecoder/profiles/OfflineDataProfileException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   138: athrow
    //   139: astore 4
    //   141: aload_2
    //   142: astore_1
    //   143: goto -46 -> 97
    //   146: astore_3
    //   147: aload_2
    //   148: astore_1
    //   149: goto -82 -> 67
    //
    // Exception table:
    //   from	to	target	type
    //   28	32	35	java/io/IOException
    //   2	18	66	java/io/FileNotFoundException
    //   2	18	95	finally
    //   67	95	95	finally
    //   101	105	108	java/io/IOException
    //   18	24	139	finally
    //   18	24	146	java/io/FileNotFoundException
  }

  public static void writeTo(OfflineDataProfile paramOfflineDataProfile, DataOutputStream paramDataOutputStream)
    throws OfflineDataProfileException
  {
    try
    {
      paramDataOutputStream.writeBytes(getProfileString(paramOfflineDataProfile));
      return;
    }
    catch (IOException localIOException)
    {
    }
    throw new OfflineDataProfileException("I/O error happened when writing profile");
  }

  public CommonProfile getCommonProfile()
  {
    return this.commonProfile;
  }

  public JSONObject getJsonObject()
    throws JSONException
  {
    JSONObject localJSONObject = new JSONObject();
    localJSONObject.put(Key.COMMON.name(), this.commonProfile.getJsonObject());
    JSONArray localJSONArray1 = new JSONArray();
    Iterator localIterator1 = this.languageProfiles.iterator();
    while (localIterator1.hasNext())
      localJSONArray1.put(((LanguageProfile)localIterator1.next()).getJsonObject());
    localJSONObject.put(Key.LANG.name(), localJSONArray1);
    JSONArray localJSONArray2 = new JSONArray();
    Iterator localIterator2 = this.languagePairProfiles.iterator();
    while (localIterator2.hasNext())
      localJSONArray2.put(((LanguagePairProfile)localIterator2.next()).getJsonObject());
    localJSONObject.put(Key.LANGPAIR.name(), localJSONArray2);
    return localJSONObject;
  }

  public Set<LanguagePairProfile> getLanguagePairProfiles()
  {
    return this.languagePairProfiles;
  }

  public Set<LanguageProfile> getLanguageProfiles()
  {
    return this.languageProfiles;
  }

  public static class Builder
  {
    private OfflineDataProfile profile = new OfflineDataProfile();

    public Builder addLanguagePairProfiles(LanguagePairProfile paramLanguagePairProfile)
    {
      this.profile.languagePairProfiles.add(paramLanguagePairProfile);
      return this;
    }

    public Builder addLanguageProfiles(LanguageProfile paramLanguageProfile)
    {
      this.profile.languageProfiles.add(paramLanguageProfile);
      return this;
    }

    public OfflineDataProfile build()
    {
      return this.profile;
    }

    public Builder setCommonProfile(CommonProfile paramCommonProfile)
    {
      OfflineDataProfile.access$002(this.profile, paramCommonProfile);
      return this;
    }
  }

  private static enum Key
  {
    static
    {
      Key[] arrayOfKey = new Key[4];
      arrayOfKey[0] = COMMON;
      arrayOfKey[1] = LANG;
      arrayOfKey[2] = LANGPAIR;
      arrayOfKey[3] = VERSION;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.profiles.OfflineDataProfile
 * JD-Core Version:    0.6.2
 */