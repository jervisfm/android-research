package com.google.android.apps.translatedecoder.profiles;

public class OfflineDataProfileException extends Exception
{
  public OfflineDataProfileException(String paramString)
  {
    super(paramString);
  }

  public OfflineDataProfileException(String paramString, Throwable paramThrowable)
  {
    super(paramString, paramThrowable);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.profiles.OfflineDataProfileException
 * JD-Core Version:    0.6.2
 */