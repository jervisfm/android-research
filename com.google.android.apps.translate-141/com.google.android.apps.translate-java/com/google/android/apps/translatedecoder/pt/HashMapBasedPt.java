package com.google.android.apps.translatedecoder.pt;

import com.google.android.apps.translatedecoder.util.SymbolTable;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

public class HashMapBasedPt extends PhraseTable
{
  private static final Logger logger = Logger.getLogger(HashMapBasedPt.class.getCanonicalName());
  private static final long serialVersionUID = 3189535196804145190L;
  private final transient SymbolTable symbol;
  private final Map<String, List<PhrasePair>> table;

  public HashMapBasedPt(int paramInt, double paramDouble, String paramString, SymbolTable paramSymbolTable)
  {
    this.maxPhraseLen = paramInt;
    this.oovCost = paramDouble;
    this.symbol = paramSymbolTable;
    this.table = new HashMap();
    loadFromFile(paramString);
  }

  private String joinWords(int[] paramArrayOfInt)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    for (int i = 0; i < paramArrayOfInt.length; i++)
    {
      localStringBuilder.append(paramArrayOfInt[i]);
      if (i < -1 + paramArrayOfInt.length)
        localStringBuilder.append(" ");
    }
    return localStringBuilder.toString();
  }

  private void loadFromFile(String paramString)
  {
    int i = -1;
    try
    {
      DataInputStream localDataInputStream = new DataInputStream(new FileInputStream(paramString));
      BufferedReader localBufferedReader = new BufferedReader(new InputStreamReader(localDataInputStream));
      int k;
      for (int j = 0; ; j = k)
      {
        String str1 = localBufferedReader.readLine();
        if (str1 == null)
          break;
        k = j + 1;
        if (j % 1000000 == 0)
          logger.info("# of phrases readed: " + k);
        String[] arrayOfString = str1.trim().split("\\s+\\|{3}\\s+");
        int[] arrayOfInt = this.symbol.addWords(arrayOfString[0]);
        if (arrayOfInt.length > i)
          i = arrayOfInt.length;
        String str2 = joinWords(arrayOfInt);
        Object localObject = (List)this.table.get(str2);
        if (localObject == null)
        {
          localObject = new ArrayList();
          this.table.put(str2, localObject);
        }
        ((List)localObject).add(new PhrasePair(arrayOfInt, this.symbol.addWords(arrayOfString[1]), new Double(arrayOfString[2]).doubleValue()));
      }
      localDataInputStream.close();
      logger.info("Finished reading phrase table.");
      if (i != this.maxPhraseLen)
      {
        logger.warning("maxNumSrcWords != maxPhraseLen, that is: " + i + " != " + this.maxPhraseLen);
        this.maxPhraseLen = i;
      }
      return;
    }
    catch (FileNotFoundException localFileNotFoundException)
    {
      while (true)
        localFileNotFoundException.printStackTrace();
    }
    catch (IOException localIOException)
    {
      while (true)
        localIOException.printStackTrace();
    }
  }

  public List<PhrasePair> getPhrases(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt.length > this.maxPhraseLen)
      return null;
    return (List)this.table.get(joinWords(paramArrayOfInt));
  }

  public void writeToByteBuffer(ByteBuffer paramByteBuffer)
  {
    logger.severe("calling unimplement function");
    System.exit(1);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.pt.HashMapBasedPt
 * JD-Core Version:    0.6.2
 */