package com.google.android.apps.translatedecoder.pt;

import com.google.android.apps.translatedecoder.succinct.ImplicitTrie;
import com.google.android.apps.translatedecoder.succinct.TrieNode;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.logging.Logger;

public class ImplicitTrieBasedPt extends PhraseTable
  implements Serializable
{
  public static final int CLASS_ID = 1;
  private static final Logger logger = Logger.getLogger(ImplicitTrieBasedPt.class.getName());
  private static final long serialVersionUID = 4356163820925873063L;
  private final ImplicitTrie srcTrie;
  private final TargetPhrasesContainer tgtPhrases;

  public ImplicitTrieBasedPt(int paramInt, double paramDouble, ImplicitTrie paramImplicitTrie, TargetPhrasesContainer paramTargetPhrasesContainer)
  {
    this.maxPhraseLen = paramInt;
    this.oovCost = paramDouble;
    this.srcTrie = paramImplicitTrie;
    this.tgtPhrases = paramTargetPhrasesContainer;
  }

  public static PhraseTable readFromByteBufferHelper(ByteBuffer paramByteBuffer)
  {
    return new ImplicitTrieBasedPt(paramByteBuffer.getInt(), paramByteBuffer.getDouble(), ImplicitTrie.readFromByteBuffer(paramByteBuffer), TargetPhrasesContainer.readFromByteBuffer(paramByteBuffer));
  }

  public List<PhrasePair> getPhrases(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt.length > this.maxPhraseLen);
    TrieNode localTrieNode;
    do
    {
      return null;
      localTrieNode = this.srcTrie.lookup(paramArrayOfInt);
    }
    while ((localTrieNode == null) || (localTrieNode.getValue() < 0.0D));
    return this.tgtPhrases.convertToPhrases(paramArrayOfInt, (int)localTrieNode.getValue());
  }

  public void writeToByteBuffer(ByteBuffer paramByteBuffer)
  {
    paramByteBuffer.putInt(1);
    paramByteBuffer.putInt(this.maxPhraseLen);
    paramByteBuffer.putDouble(this.oovCost);
    this.srcTrie.writeToByteBuffer(paramByteBuffer);
    this.tgtPhrases.writeToByteBuffer(paramByteBuffer);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.pt.ImplicitTrieBasedPt
 * JD-Core Version:    0.6.2
 */