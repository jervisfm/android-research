package com.google.android.apps.translatedecoder.pt;

import com.google.android.apps.translatedecoder.decoder.Transition;
import java.io.Serializable;
import java.util.Arrays;

public class PhrasePair
  implements Serializable, Transition
{
  private static final double EPSILON = 0.01D;
  private static final long serialVersionUID = 5115124741589651099L;
  private double cost;
  private final int[] dictInfo;
  private final int[] srcWords;
  private final int[] tgtWords;

  public PhrasePair(int[] paramArrayOfInt1, int[] paramArrayOfInt2, double paramDouble)
  {
    this(paramArrayOfInt1, paramArrayOfInt2, null, paramDouble);
  }

  public PhrasePair(int[] paramArrayOfInt1, int[] paramArrayOfInt2, int[] paramArrayOfInt3, double paramDouble)
  {
    this.srcWords = paramArrayOfInt1;
    this.tgtWords = paramArrayOfInt2;
    this.dictInfo = paramArrayOfInt3;
    this.cost = paramDouble;
  }

  public double cost()
  {
    return this.cost;
  }

  public int[] dictInfo()
  {
    return this.dictInfo;
  }

  public boolean equals(Object paramObject)
  {
    boolean bool1 = paramObject instanceof PhrasePair;
    boolean bool2 = false;
    if (bool1)
    {
      PhrasePair localPhrasePair = (PhrasePair)paramObject;
      boolean bool3 = Arrays.equals(this.srcWords, localPhrasePair.srcWords);
      bool2 = false;
      if (bool3)
      {
        boolean bool4 = Arrays.equals(this.tgtWords, localPhrasePair.tgtWords);
        bool2 = false;
        if (bool4)
        {
          boolean bool5 = Arrays.equals(this.dictInfo, localPhrasePair.dictInfo);
          bool2 = false;
          if (bool5)
          {
            boolean bool6 = Math.abs(this.cost - localPhrasePair.cost) < 0.01D;
            bool2 = false;
            if (bool6)
              bool2 = true;
          }
        }
      }
    }
    return bool2;
  }

  public void setCost(double paramDouble)
  {
    this.cost = paramDouble;
  }

  public int[] srcWords()
  {
    return this.srcWords;
  }

  public int[] tgtWords()
  {
    return this.tgtWords;
  }

  public String toString()
  {
    return "src=" + Arrays.toString(this.srcWords) + "; trg=" + Arrays.toString(this.tgtWords) + "; cost=" + this.cost;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.pt.PhrasePair
 * JD-Core Version:    0.6.2
 */