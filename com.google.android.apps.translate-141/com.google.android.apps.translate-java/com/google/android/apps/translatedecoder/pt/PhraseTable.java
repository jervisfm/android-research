package com.google.android.apps.translatedecoder.pt;

import com.google.android.apps.translatedecoder.util.MemMapUtil;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.RandomAccessFile;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileChannel.MapMode;
import java.util.List;
import java.util.logging.Logger;

public abstract class PhraseTable
  implements Serializable
{
  private static final Logger logger = Logger.getLogger(PhraseTable.class.getName());
  private static final long serialVersionUID = -4316712519165410699L;
  int maxPhraseLen = 5;
  double oovCost = 100.0D;

  public static PhraseTable readFromByteBuffer(ByteBuffer paramByteBuffer)
  {
    int i = paramByteBuffer.getInt();
    if (i == 1)
      return ImplicitTrieBasedPt.readFromByteBufferHelper(paramByteBuffer);
    logger.severe("unknown class id" + i);
    System.exit(1);
    return null;
  }

  public static PhraseTable readFromFile(String paramString)
  {
    try
    {
      FileChannel localFileChannel = new RandomAccessFile(paramString, "r").getChannel();
      MappedByteBuffer localMappedByteBuffer = localFileChannel.map(FileChannel.MapMode.READ_ONLY, 0L, (int)localFileChannel.size());
      if (localMappedByteBuffer.getInt() == MemMapUtil.mmapFileSignature())
      {
        PhraseTable localPhraseTable2 = readFromByteBuffer(localMappedByteBuffer);
        localFileChannel.close();
        return localPhraseTable2;
      }
      ObjectInputStream localObjectInputStream = new ObjectInputStream(new FileInputStream(paramString));
      PhraseTable localPhraseTable1 = (PhraseTable)localObjectInputStream.readObject();
      localObjectInputStream.close();
      localFileChannel.close();
      return localPhraseTable1;
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
    }
    return null;
  }

  public PhrasePair createIdentityPhrase(int paramInt, double paramDouble)
  {
    return new PhrasePair(new int[] { paramInt }, new int[] { paramInt }, paramDouble);
  }

  public PhrasePair createOOVPhrase(int paramInt)
  {
    return createIdentityPhrase(paramInt, this.oovCost);
  }

  public abstract List<PhrasePair> getPhrases(int[] paramArrayOfInt);

  public int maxPhraseLen()
  {
    return this.maxPhraseLen;
  }

  public double oovCost()
  {
    return this.oovCost;
  }

  public void setMaxPhraseLen(int paramInt)
  {
    this.maxPhraseLen = paramInt;
  }

  public void setOovCost(double paramDouble)
  {
    this.oovCost = paramDouble;
  }

  public abstract void writeToByteBuffer(ByteBuffer paramByteBuffer);

  public void writeToFile(String paramString)
  {
    writeToFile(paramString, false);
  }

  public void writeToFile(String paramString, boolean paramBoolean)
  {
    if (paramBoolean);
    try
    {
      RandomAccessFile localRandomAccessFile = new RandomAccessFile(paramString, "rw");
      FileChannel localFileChannel = localRandomAccessFile.getChannel();
      MappedByteBuffer localMappedByteBuffer = localFileChannel.map(FileChannel.MapMode.READ_WRITE, 0L, 500000000L);
      localMappedByteBuffer.putInt(MemMapUtil.mmapFileSignature());
      writeToByteBuffer(localMappedByteBuffer);
      logger.info("Final buffer size is " + localMappedByteBuffer.position());
      localFileChannel.truncate(localMappedByteBuffer.position());
      localRandomAccessFile.close();
      return;
      ObjectOutputStream localObjectOutputStream = new ObjectOutputStream(new FileOutputStream(paramString));
      localObjectOutputStream.writeObject(this);
      localObjectOutputStream.close();
      return;
    }
    catch (IOException localIOException)
    {
      localIOException.printStackTrace();
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.pt.PhraseTable
 * JD-Core Version:    0.6.2
 */