package com.google.android.apps.translatedecoder.pt;

import com.google.android.apps.translatedecoder.succinct.Quantizer;
import com.google.android.apps.translatedecoder.util.BitData;
import com.google.android.apps.translatedecoder.util.BitSetBasedBitData;
import com.google.android.apps.translatedecoder.util.BitsUtil;
import com.google.android.apps.translatedecoder.util.Utils;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.List;

public class TargetPhrasesContainer
  implements Serializable
{
  private static final long serialVersionUID = -394991420114210370L;
  private final BitData data;
  private final int dictTagWordId;
  private final Quantizer quantizer;
  private final int valLen;
  private final int wordIdLen;

  public TargetPhrasesContainer(BitData paramBitData, int paramInt1, int paramInt2, Quantizer paramQuantizer, int paramInt3)
  {
    this.data = paramBitData;
    this.wordIdLen = paramInt1;
    this.valLen = paramInt2;
    this.quantizer = paramQuantizer;
    this.dictTagWordId = paramInt3;
  }

  public TargetPhrasesContainer(BitSet paramBitSet, int paramInt1, int paramInt2, Quantizer paramQuantizer)
  {
    this(new BitSetBasedBitData(paramBitSet), paramInt1, paramInt2, paramQuantizer, -1);
  }

  public TargetPhrasesContainer(BitSet paramBitSet, int paramInt1, int paramInt2, Quantizer paramQuantizer, int paramInt3)
  {
    this(new BitSetBasedBitData(paramBitSet), paramInt1, paramInt2, paramQuantizer, paramInt3);
  }

  private int addTargetPhrase(BitData paramBitData, int paramInt1, int[] paramArrayOfInt, int paramInt2, boolean paramBoolean)
  {
    boolean bool1 = true;
    int i = paramInt1;
    int j = 0;
    if (j < paramArrayOfInt.length)
    {
      BitsUtil.intToBitSet(paramArrayOfInt[j], paramBitData, i, this.wordIdLen);
      int n = i + this.wordIdLen;
      int i1 = n + 1;
      if (j < -1 + paramArrayOfInt.length);
      for (boolean bool2 = bool1; ; bool2 = false)
      {
        paramBitData.set(n, bool2);
        j++;
        i = i1;
        break;
      }
    }
    BitsUtil.intToBitSet(paramInt2, paramBitData, i, this.valLen);
    int k = i + this.valLen;
    int m = k + 1;
    if (!paramBoolean);
    while (true)
    {
      paramBitData.set(k, bool1);
      return m;
      bool1 = false;
    }
  }

  private static int findDictInfoStartPos(int paramInt, int[] paramArrayOfInt)
  {
    for (int i = 0; i < paramArrayOfInt.length; i++)
      if (paramArrayOfInt[i] == paramInt)
        return i;
    return -1;
  }

  public static TargetPhrasesContainer readFromByteBuffer(ByteBuffer paramByteBuffer)
  {
    int i = paramByteBuffer.getInt();
    int j = paramByteBuffer.getInt();
    int k = paramByteBuffer.getInt();
    Quantizer localQuantizer = Quantizer.readFromByteBuffer(paramByteBuffer);
    return new TargetPhrasesContainer(BitData.readFromByteBuffer(paramByteBuffer), i, j, localQuantizer, k);
  }

  public int addTargetPhrases(List<PhrasePair> paramList, int paramInt)
  {
    int i = paramInt * 8;
    int j = 0;
    if (j < paramList.size())
    {
      BitData localBitData = this.data;
      int[] arrayOfInt = ((PhrasePair)paramList.get(j)).tgtWords();
      int k = this.quantizer.getUnit(((PhrasePair)paramList.get(j)).cost());
      if (j >= -1 + paramList.size());
      for (boolean bool = true; ; bool = false)
      {
        i = addTargetPhrase(localBitData, i, arrayOfInt, k, bool);
        j++;
        break;
      }
    }
    while (i % 8 != 0)
    {
      this.data.set(i);
      i++;
    }
    return i / 8;
  }

  public List<PhrasePair> convertToPhrases(int[] paramArrayOfInt, int paramInt)
  {
    ArrayList localArrayList1 = new ArrayList();
    ArrayList localArrayList2 = new ArrayList();
    int i = paramInt * 8;
    int j = 1;
    int k;
    if (j != 0)
    {
      localArrayList2.clear();
      k = 1;
    }
    while (true)
    {
      int i3;
      if (k != 0)
      {
        localArrayList2.add(Integer.valueOf(BitsUtil.bitSetToInt(this.data, i, this.wordIdLen)));
        int i2 = i + this.wordIdLen;
        BitData localBitData2 = this.data;
        i3 = i2 + 1;
        if (!localBitData2.get(i2))
        {
          i = i3;
          k = 0;
        }
      }
      else
      {
        double d = this.quantizer.getValue(BitsUtil.bitSetToInt(this.data, i, this.valLen));
        int m = i + this.valLen;
        int[] arrayOfInt;
        int i1;
        if (this.dictTagWordId >= 0)
        {
          arrayOfInt = Utils.listToArray(localArrayList2);
          i1 = findDictInfoStartPos(this.dictTagWordId, arrayOfInt);
          if (i1 == -1)
            localArrayList1.add(new PhrasePair(paramArrayOfInt, arrayOfInt, d));
        }
        while (true)
        {
          BitData localBitData1 = this.data;
          int n = m + 1;
          if (!localBitData1.get(m))
            j = 0;
          i = n;
          break;
          localArrayList1.add(new PhrasePair(paramArrayOfInt, Arrays.copyOfRange(arrayOfInt, 0, i1), Arrays.copyOfRange(arrayOfInt, i1 + 1, arrayOfInt.length), d));
          continue;
          localArrayList1.add(new PhrasePair(paramArrayOfInt, Utils.listToArray(localArrayList2), d));
        }
        return localArrayList1;
        i = i3;
      }
    }
  }

  public void writeToByteBuffer(ByteBuffer paramByteBuffer)
  {
    paramByteBuffer.putInt(this.wordIdLen);
    paramByteBuffer.putInt(this.valLen);
    paramByteBuffer.putInt(this.dictTagWordId);
    this.quantizer.writeToByteBuffer(paramByteBuffer);
    this.data.writeToByteBuffer(paramByteBuffer);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.pt.TargetPhrasesContainer
 * JD-Core Version:    0.6.2
 */