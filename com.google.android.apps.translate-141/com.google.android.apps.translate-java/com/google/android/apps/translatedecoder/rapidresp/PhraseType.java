package com.google.android.apps.translatedecoder.rapidresp;

public enum PhraseType
{
  static
  {
    ANTI_DOMINATING_PHRASE = new PhraseType("ANTI_DOMINATING_PHRASE", 1);
    PhraseType[] arrayOfPhraseType = new PhraseType[2];
    arrayOfPhraseType[0] = DOMINATING_PHRASE;
    arrayOfPhraseType[1] = ANTI_DOMINATING_PHRASE;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.rapidresp.PhraseType
 * JD-Core Version:    0.6.2
 */