package com.google.android.apps.translatedecoder.rapidresp;

import com.google.android.apps.translatedecoder.pt.PhrasePair;
import com.google.android.apps.translatedecoder.pt.PhraseTable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class RapidResponseTbl
{
  private final double dominateCost;
  private final PhraseTable rapidRespPT;

  public RapidResponseTbl(PhraseTable paramPhraseTable, double paramDouble)
  {
    this.rapidRespPT = paramPhraseTable;
    this.dominateCost = paramDouble;
  }

  private PhraseType getPhraseType(PhrasePair paramPhrasePair)
  {
    if (paramPhrasePair.cost() <= 0.0D)
      return PhraseType.DOMINATING_PHRASE;
    return PhraseType.ANTI_DOMINATING_PHRASE;
  }

  public List<PhrasePair> filterPhrases(int[] paramArrayOfInt, List<PhrasePair> paramList)
  {
    List localList = this.rapidRespPT.getPhrases(paramArrayOfInt);
    if ((localList == null) || (localList.size() <= 0))
    {
      localObject = paramList;
      return localObject;
    }
    Object localObject = new ArrayList();
    if (paramList != null)
      ((List)localObject).addAll(paramList);
    Iterator localIterator = localList.iterator();
    label58: label191: 
    while (true)
    {
      PhrasePair localPhrasePair;
      if (localIterator.hasNext())
      {
        localPhrasePair = (PhrasePair)localIterator.next();
        if (getPhraseType(localPhrasePair) == PhraseType.DOMINATING_PHRASE)
        {
          localPhrasePair.setCost(this.dominateCost);
          ((List)localObject).clear();
          ((List)localObject).add(localPhrasePair);
          return localObject;
        }
        if (getPhraseType(localPhrasePair) != PhraseType.ANTI_DOMINATING_PHRASE);
      }
      else
      {
        for (int i = 0; ; i++)
        {
          if (i >= ((List)localObject).size())
            break label191;
          if (Arrays.equals(((PhrasePair)((List)localObject).get(i)).tgtWords(), localPhrasePair.tgtWords()))
          {
            ((List)localObject).remove(i);
            break label58;
            break;
          }
        }
      }
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.rapidresp.RapidResponseTbl
 * JD-Core Version:    0.6.2
 */