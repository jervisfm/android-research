package com.google.android.apps.translatedecoder.succinct;

public class BinarySearch
{
  @Deprecated
  public static int search(TrieNodeList paramTrieNodeList, int paramInt)
  {
    return search(paramTrieNodeList, paramInt, false);
  }

  public static int search(TrieNodeList paramTrieNodeList, int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean)
  {
    int i = paramInt1;
    int j = paramInt2 - 1;
    int k = -1;
    TrieNode localTrieNode = new TrieNode(-1, -1, -1.0D);
    while (true)
    {
      int m;
      int n;
      if (i <= j)
      {
        m = (i + j) / 2;
        paramTrieNodeList.fill(m, localTrieNode);
        n = localTrieNode.valueForSort();
        if (paramInt3 == n)
          k = m;
      }
      else
      {
        return k;
      }
      if (paramInt3 > n)
      {
        i = m + 1;
        if (paramBoolean)
          k = i;
      }
      else
      {
        j = m - 1;
        if (paramBoolean)
          k = m;
      }
    }
  }

  @Deprecated
  public static int search(TrieNodeList paramTrieNodeList, int paramInt, boolean paramBoolean)
  {
    return search(paramTrieNodeList, 0, paramTrieNodeList.size(), paramInt, paramBoolean);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.succinct.BinarySearch
 * JD-Core Version:    0.6.2
 */