package com.google.android.apps.translatedecoder.succinct;

import com.google.android.apps.translatedecoder.util.BitData;
import com.google.android.apps.translatedecoder.util.BitSetBasedBitData;
import com.google.android.apps.translatedecoder.util.BitsUtil;
import java.nio.ByteBuffer;
import java.util.BitSet;
import java.util.logging.Logger;

public class FixedBitsTrieNodeList extends TrieNodeList
{
  public static final int CLASS_ID = 1;
  private static final Logger logger = Logger.getLogger(FixedBitsTrieNodeList.class.getName());
  private static final long serialVersionUID = -8904496639492050960L;
  private final BitData data;
  private final int offsetLen;
  private final Quantizer quantizer;
  private int size;
  private final int trieNodeLen;
  private final int valueLen;
  private final int wordIdLen;

  public FixedBitsTrieNodeList(BitData paramBitData, int paramInt1, int paramInt2, int paramInt3, int paramInt4, Quantizer paramQuantizer)
  {
    this.size = paramInt1;
    this.wordIdLen = paramInt2;
    this.offsetLen = paramInt3;
    this.valueLen = paramInt4;
    this.trieNodeLen = (paramInt4 + (paramInt2 + paramInt3));
    this.quantizer = paramQuantizer;
    if (this.quantizer == null)
      throw new IllegalArgumentException("quantizer is null!");
    this.data = paramBitData;
    if (paramBitData.size() < paramInt1 * this.trieNodeLen)
      logger.warning("data size is too small! data.size=" + paramBitData.size() + "; expected=" + paramInt1 * this.trieNodeLen);
  }

  public FixedBitsTrieNodeList(BitSet paramBitSet, int paramInt1, int paramInt2, int paramInt3, int paramInt4, Quantizer paramQuantizer)
  {
    this(new BitSetBasedBitData(paramBitSet), paramInt1, paramInt2, paramInt3, paramInt4, paramQuantizer);
  }

  public static TrieNodeList readFromByteBufferHelper(ByteBuffer paramByteBuffer)
  {
    int i = paramByteBuffer.getInt();
    int j = paramByteBuffer.getInt();
    int k = paramByteBuffer.getInt();
    int m = paramByteBuffer.getInt();
    Quantizer localQuantizer = Quantizer.readFromByteBuffer(paramByteBuffer);
    return new FixedBitsTrieNodeList(BitData.readFromByteBuffer(paramByteBuffer), i, j, k, m, localQuantizer);
  }

  public void add(TrieNode paramTrieNode)
  {
    BitsUtil.intToBitSet(paramTrieNode.getWordId(), this.data, this.size * this.trieNodeLen, this.wordIdLen);
    BitsUtil.intToBitSet(paramTrieNode.getOffset(), this.data, this.size * this.trieNodeLen + this.wordIdLen, this.offsetLen);
    BitsUtil.intToBitSet(this.quantizer.getUnit(paramTrieNode.getValue()), this.data, this.size * this.trieNodeLen + this.wordIdLen + this.offsetLen, this.valueLen);
    this.size = (1 + this.size);
  }

  public void clear()
  {
    this.data.clear();
    this.size = 0;
  }

  public void fill(int paramInt, TrieNode paramTrieNode)
  {
    if (this.wordIdLen > 0)
    {
      paramTrieNode.setWordId(BitsUtil.bitSetToInt(this.data, paramInt * this.trieNodeLen, this.wordIdLen));
      if (this.offsetLen <= 0)
        break label115;
      paramTrieNode.setOffset(BitsUtil.bitSetToInt(this.data, paramInt * this.trieNodeLen + this.wordIdLen, this.offsetLen));
    }
    while (true)
    {
      if (this.valueLen <= 0)
        break label123;
      paramTrieNode.setValue(this.quantizer.getValue(BitsUtil.bitSetToInt(this.data, paramInt * this.trieNodeLen + this.wordIdLen + this.offsetLen, this.valueLen)));
      return;
      paramTrieNode.setWordId(-1);
      break;
      label115: paramTrieNode.setOffset(-1);
    }
    label123: paramTrieNode.setValue(0.0D);
  }

  public int size()
  {
    return this.size;
  }

  public void writeToByteBuffer(ByteBuffer paramByteBuffer)
  {
    logger.info("write to buffer, postion=" + paramByteBuffer.position());
    paramByteBuffer.putInt(1);
    paramByteBuffer.putInt(this.size);
    paramByteBuffer.putInt(this.wordIdLen);
    paramByteBuffer.putInt(this.offsetLen);
    paramByteBuffer.putInt(this.valueLen);
    this.quantizer.writeToByteBuffer(paramByteBuffer);
    this.data.writeToByteBuffer(paramByteBuffer);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.succinct.FixedBitsTrieNodeList
 * JD-Core Version:    0.6.2
 */