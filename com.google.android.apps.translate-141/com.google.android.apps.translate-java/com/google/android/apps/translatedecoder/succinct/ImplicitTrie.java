package com.google.android.apps.translatedecoder.succinct;

import java.io.Serializable;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

public class ImplicitTrie
  implements Serializable
{
  private static final Logger logger = Logger.getLogger(ImplicitTrie.class.getName());
  private static final long serialVersionUID = 5765215695143316197L;
  private final List<TrieNodeList> nodes;
  private final int order;

  public ImplicitTrie(int paramInt, List<TrieNodeList> paramList)
  {
    this.order = paramInt;
    this.nodes = paramList;
    if (paramInt != paramList.size())
      throw new RuntimeException("order is not correct!");
  }

  public static ImplicitTrie readFromByteBuffer(ByteBuffer paramByteBuffer)
  {
    int i = paramByteBuffer.getInt();
    logger.info("Read from buffer, order=" + i);
    ArrayList localArrayList = new ArrayList();
    for (int j = 0; j < i; j++)
      localArrayList.add(TrieNodeList.readFromByteBuffer(paramByteBuffer));
    return new ImplicitTrie(i, localArrayList);
  }

  // ERROR //
  public static ImplicitTrie readFromFile(java.lang.String paramString)
    throws java.io.IOException
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_1
    //   2: new 101	java/io/ObjectInputStream
    //   5: dup
    //   6: new 103	java/io/FileInputStream
    //   9: dup
    //   10: aload_0
    //   11: invokespecial 104	java/io/FileInputStream:<init>	(Ljava/lang/String;)V
    //   14: invokespecial 107	java/io/ObjectInputStream:<init>	(Ljava/io/InputStream;)V
    //   17: astore_2
    //   18: aload_2
    //   19: invokevirtual 111	java/io/ObjectInputStream:readObject	()Ljava/lang/Object;
    //   22: checkcast 2	com/google/android/apps/translatedecoder/succinct/ImplicitTrie
    //   25: astore 5
    //   27: aload_2
    //   28: ifnull +7 -> 35
    //   31: aload_2
    //   32: invokevirtual 114	java/io/ObjectInputStream:close	()V
    //   35: aload 5
    //   37: areturn
    //   38: astore_3
    //   39: aload_3
    //   40: invokevirtual 117	java/lang/ClassNotFoundException:printStackTrace	()V
    //   43: new 49	java/lang/RuntimeException
    //   46: dup
    //   47: ldc 119
    //   49: invokespecial 54	java/lang/RuntimeException:<init>	(Ljava/lang/String;)V
    //   52: athrow
    //   53: astore 4
    //   55: aload_1
    //   56: ifnull +7 -> 63
    //   59: aload_1
    //   60: invokevirtual 114	java/io/ObjectInputStream:close	()V
    //   63: aload 4
    //   65: athrow
    //   66: astore 4
    //   68: aload_2
    //   69: astore_1
    //   70: goto -15 -> 55
    //   73: astore_3
    //   74: aload_2
    //   75: astore_1
    //   76: goto -37 -> 39
    //
    // Exception table:
    //   from	to	target	type
    //   2	18	38	java/lang/ClassNotFoundException
    //   2	18	53	finally
    //   39	53	53	finally
    //   18	27	66	finally
    //   18	27	73	java/lang/ClassNotFoundException
  }

  public TrieNode lookup(List<Integer> paramList)
  {
    int[] arrayOfInt = new int[paramList.size()];
    for (int i = 0; i < paramList.size(); i++)
      arrayOfInt[i] = ((Integer)paramList.get(i)).intValue();
    return lookup(arrayOfInt);
  }

  public TrieNode lookup(int[] paramArrayOfInt)
  {
    TrieNode localTrieNode;
    if ((paramArrayOfInt == null) || (paramArrayOfInt.length > this.order))
    {
      logger.warning("words is null or too long!");
      localTrieNode = null;
    }
    int i;
    int k;
    TrieNodeList localTrieNodeList;
    int m;
    do
    {
      return localTrieNode;
      localTrieNode = new TrieNode();
      i = 0;
      j = ((TrieNodeList)this.nodes.get(0)).size();
      k = 0;
      if (k < paramArrayOfInt.length)
      {
        localTrieNodeList = (TrieNodeList)this.nodes.get(k);
        m = BinarySearch.search(localTrieNodeList, i, j, paramArrayOfInt[k], false);
        if (m != -1)
          break;
      }
    }
    while (k >= paramArrayOfInt.length);
    return null;
    localTrieNodeList.fill(m, localTrieNode);
    if (k < -1 + paramArrayOfInt.length)
    {
      i = localTrieNode.getOffset();
      if (m != -1 + localTrieNodeList.size())
        break label170;
    }
    for (int j = ((TrieNodeList)this.nodes.get(k + 1)).size(); ; j = localTrieNode.getOffset())
    {
      k++;
      break;
      label170: if (m >= -1 + localTrieNodeList.size())
        break label201;
      localTrieNodeList.fill(m + 1, localTrieNode);
    }
    label201: throw new RuntimeException("Returned pos must be wrong!");
  }

  public int order()
  {
    return this.order;
  }

  public void writeToByteBuffer(ByteBuffer paramByteBuffer)
  {
    logger.info("write to buffer, order=" + this.order);
    paramByteBuffer.putInt(this.order);
    Iterator localIterator = this.nodes.iterator();
    while (localIterator.hasNext())
      ((TrieNodeList)localIterator.next()).writeToByteBuffer(paramByteBuffer);
  }

  // ERROR //
  public void writeToFile(java.lang.String paramString)
    throws java.io.IOException
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_2
    //   2: new 183	java/io/ObjectOutputStream
    //   5: dup
    //   6: new 185	java/io/FileOutputStream
    //   9: dup
    //   10: aload_1
    //   11: invokespecial 186	java/io/FileOutputStream:<init>	(Ljava/lang/String;)V
    //   14: invokespecial 189	java/io/ObjectOutputStream:<init>	(Ljava/io/OutputStream;)V
    //   17: astore_3
    //   18: aload_3
    //   19: aload_0
    //   20: invokevirtual 193	java/io/ObjectOutputStream:writeObject	(Ljava/lang/Object;)V
    //   23: aload_3
    //   24: ifnull +7 -> 31
    //   27: aload_3
    //   28: invokevirtual 194	java/io/ObjectOutputStream:close	()V
    //   31: return
    //   32: astore 4
    //   34: aload_2
    //   35: ifnull +7 -> 42
    //   38: aload_2
    //   39: invokevirtual 194	java/io/ObjectOutputStream:close	()V
    //   42: aload 4
    //   44: athrow
    //   45: astore 4
    //   47: aload_3
    //   48: astore_2
    //   49: goto -15 -> 34
    //
    // Exception table:
    //   from	to	target	type
    //   2	18	32	finally
    //   18	23	45	finally
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.succinct.ImplicitTrie
 * JD-Core Version:    0.6.2
 */