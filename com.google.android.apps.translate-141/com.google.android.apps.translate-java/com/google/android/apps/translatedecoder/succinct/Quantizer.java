package com.google.android.apps.translatedecoder.succinct;

import com.google.android.apps.translatedecoder.util.BitsUtil;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.util.logging.Logger;

public class Quantizer
  implements Serializable
{
  private static final Logger logger = Logger.getLogger(Quantizer.class.getName());
  private static final long serialVersionUID = -1292463938444642930L;
  private final int maxUnitValue;
  private final double minValue;
  private final double valPerUnit;

  public Quantizer(double paramDouble1, double paramDouble2, int paramInt)
  {
    this.minValue = paramDouble1;
    this.valPerUnit = paramDouble2;
    this.maxUnitValue = paramInt;
  }

  public static Quantizer readFromByteBuffer(ByteBuffer paramByteBuffer)
  {
    return new Quantizer(paramByteBuffer.getDouble(), paramByteBuffer.getDouble(), paramByteBuffer.getInt());
  }

  public static Quantizer readFromFile(String paramString)
  {
    try
    {
      ObjectInputStream localObjectInputStream = new ObjectInputStream(new FileInputStream(paramString));
      Quantizer localQuantizer = (Quantizer)localObjectInputStream.readObject();
      localObjectInputStream.close();
      return localQuantizer;
    }
    catch (IOException localIOException)
    {
      localIOException.printStackTrace();
      return null;
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      localClassNotFoundException.printStackTrace();
    }
    return null;
  }

  public int getUnit(double paramDouble)
  {
    int i = (int)Math.round((paramDouble - this.minValue) / this.valPerUnit);
    if (i > this.maxUnitValue)
    {
      logger.warning("unit is large than max. The val is " + paramDouble);
      i = this.maxUnitValue;
    }
    if (i < 0)
    {
      logger.warning("unit is smaller than min. The val is " + paramDouble);
      i = 0;
    }
    return i;
  }

  public double getValue(int paramInt)
  {
    if (paramInt > this.maxUnitValue)
    {
      logger.severe("unit is too large!");
      System.exit(1);
    }
    return this.minValue + paramInt * this.valPerUnit;
  }

  public int numBitsRequired()
  {
    return BitsUtil.numBitsRequired(this.maxUnitValue);
  }

  public void writeToByteBuffer(ByteBuffer paramByteBuffer)
  {
    paramByteBuffer.putDouble(this.minValue);
    paramByteBuffer.putDouble(this.valPerUnit);
    paramByteBuffer.putInt(this.maxUnitValue);
  }

  public void writeToFile(String paramString)
  {
    try
    {
      ObjectOutputStream localObjectOutputStream = new ObjectOutputStream(new FileOutputStream(paramString));
      localObjectOutputStream.writeObject(this);
      localObjectOutputStream.close();
      return;
    }
    catch (IOException localIOException)
    {
      localIOException.printStackTrace();
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.succinct.Quantizer
 * JD-Core Version:    0.6.2
 */