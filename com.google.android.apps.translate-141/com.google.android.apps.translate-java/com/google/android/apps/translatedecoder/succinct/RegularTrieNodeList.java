package com.google.android.apps.translatedecoder.succinct;

import java.nio.ByteBuffer;
import java.util.List;

public class RegularTrieNodeList extends TrieNodeList
{
  private static final long serialVersionUID = 2659592752622964155L;
  private final List<TrieNode> nodes;

  public RegularTrieNodeList(List<TrieNode> paramList)
  {
    this.nodes = paramList;
  }

  public void add(TrieNode paramTrieNode)
  {
    this.nodes.add(new TrieNode(paramTrieNode.getWordId(), paramTrieNode.getOffset(), paramTrieNode.getValue()));
  }

  public void clear()
  {
    this.nodes.clear();
  }

  public void fill(int paramInt, TrieNode paramTrieNode)
  {
    TrieNode localTrieNode = (TrieNode)this.nodes.get(paramInt);
    paramTrieNode.setWordId(localTrieNode.getWordId());
    paramTrieNode.setOffset(localTrieNode.getOffset());
    paramTrieNode.setValue(localTrieNode.getValue());
  }

  public int size()
  {
    return this.nodes.size();
  }

  public void writeToByteBuffer(ByteBuffer paramByteBuffer)
  {
    throw new UnsupportedOperationException("calling unimplement function");
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.succinct.RegularTrieNodeList
 * JD-Core Version:    0.6.2
 */