package com.google.android.apps.translatedecoder.succinct;

public class TrieNode
{
  private int offset;
  private double value;
  private int wordId;

  public TrieNode()
  {
    this(-1, -1, -1.0D);
  }

  public TrieNode(int paramInt1, int paramInt2, double paramDouble)
  {
    this.wordId = paramInt1;
    this.offset = paramInt2;
    this.value = paramDouble;
  }

  public boolean equals(Object paramObject)
  {
    boolean bool1 = paramObject instanceof TrieNode;
    boolean bool2 = false;
    if (bool1)
    {
      TrieNode localTrieNode = (TrieNode)paramObject;
      int i = this.wordId;
      int j = localTrieNode.wordId;
      bool2 = false;
      if (i == j)
      {
        int k = this.offset;
        int m = localTrieNode.offset;
        bool2 = false;
        if (k == m)
        {
          boolean bool3 = this.value < localTrieNode.value;
          bool2 = false;
          if (!bool3)
            bool2 = true;
        }
      }
    }
    return bool2;
  }

  public int getOffset()
  {
    return this.offset;
  }

  public double getValue()
  {
    return this.value;
  }

  public int getWordId()
  {
    return this.wordId;
  }

  public int hashCode()
  {
    return super.hashCode();
  }

  public void setOffset(int paramInt)
  {
    this.offset = paramInt;
  }

  public void setValue(double paramDouble)
  {
    this.value = paramDouble;
  }

  public void setWordId(int paramInt)
  {
    this.wordId = paramInt;
  }

  public String toString()
  {
    return "wordId=" + this.wordId + "; offSet=" + this.offset + "; value=" + this.value;
  }

  public int valueForSort()
  {
    return this.wordId;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.succinct.TrieNode
 * JD-Core Version:    0.6.2
 */