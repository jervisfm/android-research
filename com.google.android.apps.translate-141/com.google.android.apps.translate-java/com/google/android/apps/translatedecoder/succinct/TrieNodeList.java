package com.google.android.apps.translatedecoder.succinct;

import java.io.Serializable;
import java.nio.ByteBuffer;
import java.util.logging.Logger;

public abstract class TrieNodeList
  implements Serializable
{
  private static final Logger logger = Logger.getLogger(TrieNodeList.class.getName());
  private static final long serialVersionUID = 7064789864054029558L;

  public static TrieNodeList readFromByteBuffer(ByteBuffer paramByteBuffer)
  {
    logger.info("read from buffer, postion=" + paramByteBuffer.position());
    int i = paramByteBuffer.getInt();
    if (i == 1)
      return FixedBitsTrieNodeList.readFromByteBufferHelper(paramByteBuffer);
    throw new RuntimeException("unknown class id " + i);
  }

  public abstract void add(TrieNode paramTrieNode);

  public abstract void clear();

  public abstract void fill(int paramInt, TrieNode paramTrieNode);

  public abstract int size();

  public abstract void writeToByteBuffer(ByteBuffer paramByteBuffer);
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.succinct.TrieNodeList
 * JD-Core Version:    0.6.2
 */