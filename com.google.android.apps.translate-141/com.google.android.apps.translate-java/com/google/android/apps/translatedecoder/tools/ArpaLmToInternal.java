package com.google.android.apps.translatedecoder.tools;

import com.google.android.apps.translatedecoder.util.ConfigParser;
import com.google.android.apps.translatedecoder.util.Utils;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.util.logging.Logger;

public class ArpaLmToInternal
{
  private static final Logger logger = Logger.getLogger(ArpaLmToInternal.class.getCanonicalName());

  public static void convert(String paramString1, int paramInt, String paramString2)
  {
    while (true)
    {
      DataInputStream localDataInputStream;
      DataOutputStream localDataOutputStream;
      BufferedWriter localBufferedWriter;
      int j;
      try
      {
        localDataInputStream = new DataInputStream(Utils.getInputStream(paramString1));
        InputStreamReader localInputStreamReader = new InputStreamReader(localDataInputStream);
        BufferedReader localBufferedReader = new BufferedReader(localInputStreamReader);
        FileOutputStream localFileOutputStream = new FileOutputStream(paramString2);
        localDataOutputStream = new DataOutputStream(localFileOutputStream);
        OutputStreamWriter localOutputStreamWriter = new OutputStreamWriter(localDataOutputStream);
        localBufferedWriter = new BufferedWriter(localOutputStreamWriter);
        i = 0;
        j = 0;
        String str1 = localBufferedReader.readLine();
        if (str1 != null)
        {
          str2 = str1.trim();
          if ((str2.matches("^\\s*$")) || (str2.matches("^\\\\\\s*end\\\\\\s*$")))
            continue;
          if (str2.matches("^\\\\\\d-grams:\\s*$"))
          {
            i = 1;
            j = Integer.valueOf(str2.substring(1, 2)).intValue();
            logger.info("line=" + str2 + "; order=" + j);
            continue;
          }
        }
      }
      catch (FileNotFoundException localFileNotFoundException)
      {
        int i;
        String str2;
        localFileNotFoundException.printStackTrace();
        return;
        if (i == 0)
          continue;
        String[] arrayOfString = str2.split("\\s+");
        if (j >= paramInt)
          break label455;
        k = j + 2;
        if (arrayOfString.length != k)
        {
          logger.severe("wrong line " + str2);
          System.exit(1);
        }
        StringBuilder localStringBuilder = new StringBuilder();
        int m;
        if (j < paramInt)
        {
          m = -1 + arrayOfString.length;
          break label443;
          if (n < m)
          {
            localStringBuilder.append(arrayOfString[n]);
            if (n >= m - 1)
              break label449;
            localStringBuilder.append(" ");
            break label449;
          }
        }
        else
        {
          m = arrayOfString.length;
          break label443;
        }
        double d = -new Double(arrayOfString[0]).doubleValue();
        localBufferedWriter.write(localStringBuilder + " ||| " + d + "\n");
        continue;
      }
      catch (IOException localIOException)
      {
        localIOException.printStackTrace();
        return;
      }
      localDataInputStream.close();
      localBufferedWriter.flush();
      localDataOutputStream.close();
      logger.info("finish reading file " + paramString1 + ", and wrote to file " + paramString2);
      return;
      label443: int n = 1;
      continue;
      label449: n++;
      continue;
      label455: int k = j + 1;
    }
  }

  public static void main(String[] paramArrayOfString)
  {
    if (paramArrayOfString.length <= 0)
    {
      System.out.println("Usage: java ArpaLmToInternal --arpaFile=file --order=order --outFile=file");
      System.exit(1);
    }
    ConfigParser localConfigParser = new ConfigParser(paramArrayOfString);
    localConfigParser.checkRequiredProperties(new String[] { "arpaFile", "order", "outFile" });
    localConfigParser.printProperties();
    convert(localConfigParser.getProperty("arpaFile"), Integer.valueOf(localConfigParser.getProperty("order")).intValue(), localConfigParser.getProperty("outFile"));
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.tools.ArpaLmToInternal
 * JD-Core Version:    0.6.2
 */