package com.google.android.apps.translatedecoder.tools;

import com.google.android.apps.translatedecoder.lm.HashMapBasedLm;
import com.google.android.apps.translatedecoder.lm.LanguageModel;
import com.google.android.apps.translatedecoder.pt.HashMapBasedPt;
import com.google.android.apps.translatedecoder.pt.PhraseTable;
import com.google.android.apps.translatedecoder.util.ConfigParser;
import com.google.android.apps.translatedecoder.util.SymbolTable;
import java.io.PrintStream;

public class HashMapBasedModelGenerator
{
  public static void main(String[] paramArrayOfString)
  {
    if (paramArrayOfString.length <= 0)
    {
      System.out.println("Usage: java SymbolTblGenerator --symbolFile=file --lmFile=file --lmOrder=order --lmOovCost=cost --lmOutFile=file--ptFile=file --ptOrder=order --ptOovCost=cost --ptOutFile=file");
      System.exit(1);
    }
    ConfigParser localConfigParser = new ConfigParser(paramArrayOfString);
    localConfigParser.checkRequiredProperties(new String[] { "ptFile", "lmFile", "symbolFile", "ptOutFile", "lmOutFile" });
    SymbolTable localSymbolTable = SymbolTable.readFromFile(localConfigParser.getProperty("symbolFile"));
    new HashMapBasedLm(Integer.valueOf(localConfigParser.getProperty("lmOrder", "2")).intValue(), new Double(localConfigParser.getProperty("lmOovCost", "100")).doubleValue(), localConfigParser.getProperty("lmFile"), localSymbolTable).writeToFile(localConfigParser.getProperty("lmOutFile"));
    new HashMapBasedPt(Integer.valueOf(localConfigParser.getProperty("ptOrder", "2")).intValue(), new Double(localConfigParser.getProperty("ptOovCost", "100")).doubleValue(), localConfigParser.getProperty("ptFile"), localSymbolTable).writeToFile(localConfigParser.getProperty("ptOutFile"));
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.tools.HashMapBasedModelGenerator
 * JD-Core Version:    0.6.2
 */