package com.google.android.apps.translatedecoder.tools;

import com.google.android.apps.translatedecoder.conversion.LmTrieConverter;
import com.google.android.apps.translatedecoder.lm.LanguageModel;
import com.google.android.apps.translatedecoder.succinct.Quantizer;
import com.google.android.apps.translatedecoder.util.ConfigParser;
import com.google.android.apps.translatedecoder.util.SymbolTable;
import java.util.logging.Logger;

public class LMGenerator
{
  private static final Logger logger = Logger.getLogger(LMGenerator.class.getCanonicalName());

  public static void main(String[] paramArrayOfString)
  {
    if (paramArrayOfString.length <= 0)
    {
      logger.info("Usage: java LMGenerator --inFile=file --outDir=file --lmOrder=order --oovCost=oovCost --stupidBackoffCost=cost --wordIdLen=len --offsetLen=len --valueLen=len --mmapFormat=flag --arpaInput=flag");
      System.exit(1);
    }
    ConfigParser localConfigParser = new ConfigParser(paramArrayOfString);
    localConfigParser.checkRequiredProperties(new String[] { "inFile", "outDir", "lmOrder", "oovCost", "stupidBackoffCost", "wordIdLen", "offsetLen", "valueLen" });
    Object localObject = localConfigParser.getProperty("inFile");
    String str1 = localConfigParser.getProperty("outDir") + "/lm";
    boolean bool = new Boolean(localConfigParser.getProperty("mmapFormat", "true")).booleanValue();
    if (new Boolean(localConfigParser.getProperty("arpaInput", "false")).booleanValue())
    {
      logger.info("Convert arpa-format lm to text-format.");
      String str4 = str1 + ".text";
      ArpaLmToInternal.convert((String)localObject, new Integer(localConfigParser.getProperty("lmOrder")).intValue(), str4);
      localObject = str4;
    }
    logger.info("Generate a symbol table.");
    String str2 = str1 + ".symbol";
    SymbolTblGenerator.generateTbl((String)localObject, str2, new Boolean(localConfigParser.getProperty("compactFormat", "true")).booleanValue(), localConfigParser.getProperty("plainSymbolFile"), bool);
    logger.info("Generate a quantizer.");
    int i = new Integer(localConfigParser.getProperty("valueLen")).intValue();
    Quantizer localQuantizer1 = QuantizerGenerator.generateQuantizer(-1 + (int)Math.pow(2.0D, i), (String)localObject);
    logger.info("input numBits=" + i + "; out numBits=" + localQuantizer1.numBitsRequired());
    String str3 = str1 + ".quantizer";
    localQuantizer1.writeToFile(str3);
    logger.info("Generate a bit-based trie file.");
    SymbolTable localSymbolTable = SymbolTable.readFromFile(str2);
    Quantizer localQuantizer2 = Quantizer.readFromFile(str3);
    new LmTrieConverter(localSymbolTable, new Integer(localConfigParser.getProperty("lmOrder")).intValue(), new Double(localConfigParser.getProperty("oovCost")).doubleValue(), new Double(localConfigParser.getProperty("stupidBackoffCost")).doubleValue(), new Boolean(localConfigParser.getProperty("noBackoff", "false")).booleanValue(), localQuantizer2, new Boolean(localConfigParser.getProperty("prefixTrue", "true")).booleanValue()).convertToImplicitTrie((String)localObject, new Integer(localConfigParser.getProperty("wordIdLen")).intValue(), new Integer(localConfigParser.getProperty("offsetLen")).intValue(), localQuantizer2.numBitsRequired()).writeToFile(str1 + ".bit.trie", bool);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.tools.LMGenerator
 * JD-Core Version:    0.6.2
 */