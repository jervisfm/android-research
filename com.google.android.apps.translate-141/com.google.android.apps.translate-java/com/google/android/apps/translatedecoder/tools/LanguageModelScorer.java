package com.google.android.apps.translatedecoder.tools;

import com.google.android.apps.translatedecoder.lm.LanguageModel;
import com.google.android.apps.translatedecoder.util.ConfigParser;
import com.google.android.apps.translatedecoder.util.SymbolTable;
import com.google.android.apps.translatedecoder.util.Utils;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintStream;

public class LanguageModelScorer
{
  public static final int[] getIds(SymbolTable paramSymbolTable, String paramString, int paramInt)
  {
    String[] arrayOfString = paramString.trim().split("\\s+");
    int[] arrayOfInt = new int[arrayOfString.length];
    int i = 0;
    if (i < arrayOfString.length)
    {
      if (paramSymbolTable.hasWord(arrayOfString[i]))
        arrayOfInt[i] = paramSymbolTable.getId(arrayOfString[i]);
      while (true)
      {
        i++;
        break;
        arrayOfInt[i] = paramInt;
      }
    }
    return arrayOfInt;
  }

  public static void main(String[] paramArrayOfString)
  {
    if (paramArrayOfString.length <= 0)
    {
      System.out.println("Usage: java LanguageModelScorer --symbolFile=file --lmFile=file --inputFile=file --outputFile=file --startIndex=number");
      System.exit(1);
    }
    ConfigParser localConfigParser = new ConfigParser(paramArrayOfString);
    localConfigParser.checkRequiredProperties(new String[] { "symbolFile", "lmFile", "inputFile", "outputFile", "startIndex" });
    SymbolTable localSymbolTable = SymbolTable.readFromFile(localConfigParser.getProperty("symbolFile"));
    LanguageModel localLanguageModel = LanguageModel.readFromFile(localConfigParser.getProperty("lmFile"));
    int i = localSymbolTable.getId("<UNK>");
    localLanguageModel.setSimulateProdlm(true);
    localLanguageModel.setUnkId(i);
    int j = Integer.valueOf(localConfigParser.getProperty("startIndex")).intValue();
    try
    {
      localDataInputStream = new DataInputStream(new FileInputStream(localConfigParser.getProperty("inputFile")));
      BufferedReader localBufferedReader = new BufferedReader(new InputStreamReader(localDataInputStream));
      localDataOutputStream = new DataOutputStream(new FileOutputStream(localConfigParser.getProperty("outputFile")));
      localBufferedWriter = new BufferedWriter(new OutputStreamWriter(localDataOutputStream));
      while (true)
      {
        String str = localBufferedReader.readLine();
        if (str == null)
          break;
        double d = localLanguageModel.sentenceCost(Utils.intArrayToList(getIds(localSymbolTable, str.trim(), i)), j);
        localBufferedWriter.write(str + " ||| " + d + "\n");
      }
    }
    catch (FileNotFoundException localFileNotFoundException)
    {
      DataInputStream localDataInputStream;
      DataOutputStream localDataOutputStream;
      BufferedWriter localBufferedWriter;
      localFileNotFoundException.printStackTrace();
      return;
      localDataInputStream.close();
      localBufferedWriter.flush();
      localDataOutputStream.close();
      return;
    }
    catch (IOException localIOException)
    {
      localIOException.printStackTrace();
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.tools.LanguageModelScorer
 * JD-Core Version:    0.6.2
 */