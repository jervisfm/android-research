package com.google.android.apps.translatedecoder.tools;

import com.google.android.apps.translatedecoder.lm.LanguageModel;
import com.google.android.apps.translatedecoder.preprocess.Tokenizer;
import com.google.android.apps.translatedecoder.pt.PhraseTable;
import com.google.android.apps.translatedecoder.util.ConfigParser;
import com.google.android.apps.translatedecoder.util.SymbolTable;
import java.io.PrintStream;

public class ModelLoader
{
  public static void main(String[] paramArrayOfString)
  {
    if (paramArrayOfString.length <= 0)
    {
      System.out.println("Usage: java ModelLoader --ptFile=file --lmFile=file --symbolFile=file --preprocDataFile=file");
      System.exit(1);
    }
    ConfigParser localConfigParser = new ConfigParser(paramArrayOfString);
    long l1 = System.currentTimeMillis();
    boolean bool1 = localConfigParser.hasProperty("ptFile");
    PhraseTable localPhraseTable = null;
    if (bool1)
    {
      long l5 = System.currentTimeMillis();
      localPhraseTable = PhraseTable.readFromFile(localConfigParser.getProperty("ptFile"));
      System.out.println("Finish reading pt file");
      System.out.println("# of seconds taken: " + (System.currentTimeMillis() - l5) / 1000L);
    }
    boolean bool2 = localConfigParser.hasProperty("lmFile");
    LanguageModel localLanguageModel = null;
    if (bool2)
    {
      long l4 = System.currentTimeMillis();
      localLanguageModel = LanguageModel.readFromFile(localConfigParser.getProperty("lmFile"));
      System.out.println("Finish reading lm file");
      System.out.println("# of seconds taken: " + (System.currentTimeMillis() - l4) / 1000L);
    }
    boolean bool3 = localConfigParser.hasProperty("symbolFile");
    SymbolTable localSymbolTable = null;
    if (bool3)
    {
      long l3 = System.currentTimeMillis();
      localSymbolTable = SymbolTable.readFromFile(localConfigParser.getProperty("symbolFile"));
      System.out.println("Finish reading symbol table.");
      System.out.println("# of seconds taken: " + (System.currentTimeMillis() - l3) / 1000L);
    }
    boolean bool4 = localConfigParser.hasProperty("charMapFile");
    Tokenizer localTokenizer = null;
    if (bool4)
    {
      boolean bool5 = localConfigParser.hasProperty("preprocDataFile");
      localTokenizer = null;
      if (bool5)
      {
        long l2 = System.currentTimeMillis();
        localTokenizer = Tokenizer.readFromFile(localConfigParser.getProperty("preprocDataFile"));
        System.out.println("Finish reading tokenizer files");
        System.out.println("# of seconds taken: " + (System.currentTimeMillis() - l2) / 1000L);
      }
    }
    System.out.println("Total # of seconds taken: " + (System.currentTimeMillis() - l1) / 1000L);
    if (localPhraseTable != null)
      System.out.println("pt maxPhraseLen " + localPhraseTable.maxPhraseLen());
    if (localLanguageModel != null)
      System.out.println("lm order maxPhraseLen " + localLanguageModel.lmOrder());
    if (localSymbolTable != null)
      System.out.println("Word for id 1 is " + localSymbolTable.getWord(1));
    if (localTokenizer != null)
      System.out.println("Is U.K an abbreviation?" + localTokenizer.isAbbrev("ENGLISH", "U.K"));
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.tools.ModelLoader
 * JD-Core Version:    0.6.2
 */