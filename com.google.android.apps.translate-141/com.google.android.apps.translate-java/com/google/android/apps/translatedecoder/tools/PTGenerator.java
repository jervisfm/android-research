package com.google.android.apps.translatedecoder.tools;

import com.google.android.apps.translatedecoder.conversion.PtTrieConverter;
import com.google.android.apps.translatedecoder.pt.PhraseTable;
import com.google.android.apps.translatedecoder.succinct.Quantizer;
import com.google.android.apps.translatedecoder.util.ConfigParser;
import com.google.android.apps.translatedecoder.util.SymbolTable;
import java.util.logging.Logger;

public class PTGenerator
{
  private static final Logger logger = Logger.getLogger(PTGenerator.class.getCanonicalName());

  public static void main(String[] paramArrayOfString)
  {
    if (paramArrayOfString.length <= 0)
    {
      logger.info("Usage: java PTGenerator --inFile=file --outDir=file --ptOrder=order --oovCost=oovCost --wordIdLen=len --offsetLen=len --valueLen=len --targetBytesLen=len--mmapFormat=flag --lmSymbol=file");
      System.exit(1);
    }
    ConfigParser localConfigParser = new ConfigParser(paramArrayOfString);
    localConfigParser.checkRequiredProperties(new String[] { "inFile", "outDir", "ptOrder", "oovCost", "wordIdLen", "offsetLen", "valueLen", "targetBytesLen" });
    String str1 = localConfigParser.getProperty("inFile");
    String str2 = localConfigParser.getProperty("outDir") + "/pt";
    boolean bool = new Boolean(localConfigParser.getProperty("mmapFormat", "true")).booleanValue();
    logger.info("Generate a symbol table.");
    String str3 = str2 + ".symbol";
    if (localConfigParser.getProperty("lmSymbol") != null)
      SymbolTblGenerator.generatePTWrapperTbl(str1, SymbolTable.readFromFile(localConfigParser.getProperty("lmSymbol")), str3, new Boolean(localConfigParser.getProperty("compactFormat", "true")).booleanValue(), localConfigParser.getProperty("plainSymbolFile"), bool);
    while (true)
    {
      logger.info("Generate a quantizer.");
      int i = new Integer(localConfigParser.getProperty("valueLen")).intValue();
      Quantizer localQuantizer1 = QuantizerGenerator.generateQuantizer(-1 + (int)Math.pow(2.0D, i), str1);
      logger.info("input numBits=" + i + "; out numBits=" + localQuantizer1.numBitsRequired());
      String str4 = str2 + ".quantizer";
      localQuantizer1.writeToFile(str4);
      logger.info("Generate a bit-based trie file.");
      SymbolTable localSymbolTable = SymbolTable.readFromFile(str3);
      Quantizer localQuantizer2 = Quantizer.readFromFile(str4);
      new PtTrieConverter(localSymbolTable, new Integer(localConfigParser.getProperty("ptOrder")).intValue(), new Double(localConfigParser.getProperty("oovCost")).doubleValue(), localQuantizer2).convertToImplicitTrie(str1, new Integer(localConfigParser.getProperty("wordIdLen")).intValue(), new Integer(localConfigParser.getProperty("offsetLen")).intValue(), new Integer(localConfigParser.getProperty("targetBytesLen")).intValue(), localQuantizer2.numBitsRequired()).writeToFile(str2 + ".bit.trie", bool);
      return;
      SymbolTblGenerator.generateTbl(str1, str3, new Boolean(localConfigParser.getProperty("compactFormat", "true")).booleanValue(), localConfigParser.getProperty("plainSymbolFile"), bool);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.tools.PTGenerator
 * JD-Core Version:    0.6.2
 */