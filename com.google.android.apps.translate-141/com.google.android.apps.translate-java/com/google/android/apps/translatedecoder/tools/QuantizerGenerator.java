package com.google.android.apps.translatedecoder.tools;

import com.google.android.apps.translatedecoder.succinct.Quantizer;
import com.google.android.apps.translatedecoder.util.ConfigParser;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

public class QuantizerGenerator
{
  private static final Logger logger = Logger.getLogger(QuantizerGenerator.class.getName());

  public static Quantizer generateQuantizer(int paramInt, String paramString)
  {
    String str = null;
    try
    {
      DataInputStream localDataInputStream = new DataInputStream(new FileInputStream(paramString));
      BufferedReader localBufferedReader = new BufferedReader(new InputStreamReader(localDataInputStream));
      ArrayList localArrayList = new ArrayList();
      int j;
      for (int i = 0; ; i = j)
      {
        str = localBufferedReader.readLine();
        if (str == null)
          break;
        j = i + 1;
        if ((i % 1000000 == 0) && (j != 1))
          logger.info("# of ngrams read: " + j);
        String[] arrayOfString = str.trim().split("\\s+\\|{3}\\s+");
        localArrayList.add(new Double(arrayOfString[(-1 + arrayOfString.length)]));
      }
      localDataInputStream.close();
      logger.info("Finished reading the model.");
      Quantizer localQuantizer = generateQuantizer(paramInt, localArrayList);
      return localQuantizer;
    }
    catch (FileNotFoundException localFileNotFoundException)
    {
      localFileNotFoundException.printStackTrace();
      return null;
    }
    catch (IOException localIOException)
    {
      while (true)
        localIOException.printStackTrace();
    }
    catch (NumberFormatException localNumberFormatException)
    {
      while (true)
      {
        logger.info("line=" + str);
        localNumberFormatException.printStackTrace();
      }
    }
  }

  public static Quantizer generateQuantizer(int paramInt, List<Double> paramList)
  {
    double d1 = 1.7976931348623157E+308D;
    double d2 = 4.9E-324D;
    Iterator localIterator = paramList.iterator();
    while (localIterator.hasNext())
    {
      double d4 = ((Double)localIterator.next()).doubleValue();
      if (d4 < d1)
        d1 = d4;
      if (d4 > d2)
        d2 = d4;
    }
    double d3 = (d2 - d1) / paramInt;
    logger.info("minValue=" + d1 + "; maxValue=" + d2 + "; valPerUnit=" + d3 + "; maxUnitValue=" + paramInt);
    return new Quantizer(d1, d3, paramInt);
  }

  public static void main(String[] paramArrayOfString)
  {
    if (paramArrayOfString.length <= 0)
    {
      System.out.println("Usage: java QuantizerGenerator --modelFile=file --numBits=num --outFile=file");
      System.exit(1);
    }
    ConfigParser localConfigParser = new ConfigParser(paramArrayOfString);
    localConfigParser.checkRequiredProperties(new String[] { "modelFile", "numBits", "outFile" });
    int i = Integer.valueOf(localConfigParser.getProperty("numBits")).intValue();
    Quantizer localQuantizer = generateQuantizer(-1 + (int)Math.pow(2.0D, i), localConfigParser.getProperty("modelFile"));
    System.out.println("input numBits=" + i + "; out numBits=" + localQuantizer.numBitsRequired());
    localQuantizer.writeToFile(localConfigParser.getProperty("outFile"));
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.tools.QuantizerGenerator
 * JD-Core Version:    0.6.2
 */