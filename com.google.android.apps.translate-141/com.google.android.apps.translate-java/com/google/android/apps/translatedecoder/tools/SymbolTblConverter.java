package com.google.android.apps.translatedecoder.tools;

import com.google.android.apps.translatedecoder.util.ConfigParser;
import com.google.android.apps.translatedecoder.util.HashMapBasedSymbol;
import com.google.android.apps.translatedecoder.util.SymbolTable;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;

public class SymbolTblConverter
{
  public static void main(String[] paramArrayOfString)
  {
    if (paramArrayOfString.length <= 0)
    {
      System.out.println("Usage: java SymbolTblConverter --inFile=file --outFile=file");
      System.exit(1);
    }
    ConfigParser localConfigParser = new ConfigParser(paramArrayOfString);
    localConfigParser.checkRequiredProperties(new String[] { "inFile", "outFile" });
    try
    {
      localHashMapBasedSymbol = new HashMapBasedSymbol();
      localDataInputStream = new DataInputStream(new FileInputStream(localConfigParser.getProperty("inFile")));
      BufferedReader localBufferedReader = new BufferedReader(new InputStreamReader(localDataInputStream));
      while (true)
      {
        String str = localBufferedReader.readLine();
        if (str == null)
          break;
        localHashMapBasedSymbol.addWord(str.trim().split("\\s+")[0]);
      }
    }
    catch (FileNotFoundException localFileNotFoundException)
    {
      HashMapBasedSymbol localHashMapBasedSymbol;
      DataInputStream localDataInputStream;
      localFileNotFoundException.printStackTrace();
      return;
      localDataInputStream.close();
      localHashMapBasedSymbol.writeToFile(localConfigParser.getProperty("outFile"));
      return;
    }
    catch (IOException localIOException)
    {
      localIOException.printStackTrace();
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.tools.SymbolTblConverter
 * JD-Core Version:    0.6.2
 */