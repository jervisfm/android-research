package com.google.android.apps.translatedecoder.tools;

import com.google.android.apps.translatedecoder.util.ArrayBasedSymbol;
import com.google.android.apps.translatedecoder.util.ConfigParser;
import com.google.android.apps.translatedecoder.util.HashMapBasedSymbol;
import com.google.android.apps.translatedecoder.util.PTSymbolWrapper;
import com.google.android.apps.translatedecoder.util.SortedStringArray;
import com.google.android.apps.translatedecoder.util.SymbolTable;
import com.google.android.apps.translatedecoder.util.Utils;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.logging.Logger;

public class SymbolTblGenerator
{
  private static final Logger logger = Logger.getLogger(SymbolTblGenerator.class.getCanonicalName());

  public static HashMapBasedSymbol convertToHashMapBasedSymbol(String paramString)
  {
    SymbolTable localSymbolTable = SymbolTable.readFromFile(paramString);
    HashMapBasedSymbol localHashMapBasedSymbol = new HashMapBasedSymbol(-1);
    for (String str : localSymbolTable.allWords())
    {
      localHashMapBasedSymbol.addWord(str);
      if (localHashMapBasedSymbol.getId(str) != localSymbolTable.getId(str))
        throw new RuntimeException("Different id for word " + str + "; " + localHashMapBasedSymbol.getId(str) + " vs " + localSymbolTable.getId(str));
    }
    return localHashMapBasedSymbol;
  }

  public static void generatePTWrapperTbl(String paramString1, SymbolTable paramSymbolTable, String paramString2, boolean paramBoolean1, String paramString3, boolean paramBoolean2)
  {
    if (paramBoolean1 != true)
      throw new UnsupportedOperationException("compactFormat has to be true");
    try
    {
      HashMapBasedSymbol localHashMapBasedSymbol = new HashMapBasedSymbol();
      readModel(paramString1, paramSymbolTable, localHashMapBasedSymbol);
      String[] arrayOfString1 = localHashMapBasedSymbol.allWords();
      Arrays.sort(arrayOfString1);
      PTSymbolWrapper localPTSymbolWrapper = new PTSymbolWrapper(paramSymbolTable, new ArrayBasedSymbol(new SortedStringArray(arrayOfString1, true, 1 + paramSymbolTable.maximumId())));
      logger.info("write file to " + paramString2);
      localPTSymbolWrapper.writeToFile(paramString2, paramBoolean2);
      if (paramString3 != null)
      {
        String[] arrayOfString2 = localPTSymbolWrapper.allWords();
        DataOutputStream localDataOutputStream = new DataOutputStream(new FileOutputStream(paramString3));
        BufferedWriter localBufferedWriter = new BufferedWriter(new OutputStreamWriter(localDataOutputStream));
        int i = arrayOfString2.length;
        for (int j = 0; j < i; j++)
        {
          String str1 = arrayOfString2[j];
          localBufferedWriter.write(str1 + " " + localPTSymbolWrapper.getId(str1) + "\n");
        }
        localBufferedWriter.flush();
        localDataOutputStream.close();
      }
      String[] arrayOfString3 = paramSymbolTable.allWords();
      int k = arrayOfString3.length;
      m = 0;
      if (m < k)
      {
        String str2 = arrayOfString3[m];
        if (localPTSymbolWrapper.getId(str2) == paramSymbolTable.getId(str2))
          break label344;
        throw new RuntimeException("Different id for word " + str2 + "; " + localPTSymbolWrapper.getId(str2) + " vs " + paramSymbolTable.getId(str2));
      }
    }
    catch (FileNotFoundException localFileNotFoundException)
    {
      while (true)
      {
        int m;
        localFileNotFoundException.printStackTrace();
        return;
        m++;
      }
    }
    catch (IOException localIOException)
    {
      label344: localIOException.printStackTrace();
    }
  }

  public static void generateTbl(String paramString1, HashMapBasedSymbol paramHashMapBasedSymbol, String paramString2, boolean paramBoolean1, String paramString3, boolean paramBoolean2)
  {
    try
    {
      readModel(paramString1, paramHashMapBasedSymbol);
      String[] arrayOfString = paramHashMapBasedSymbol.allWords();
      Arrays.sort(arrayOfString);
      logger.info("write file to " + paramString2);
      Object localObject = paramHashMapBasedSymbol;
      if (paramBoolean1)
        localObject = new ArrayBasedSymbol(new SortedStringArray(arrayOfString, true));
      ((SymbolTable)localObject).writeToFile(paramString2, paramBoolean2);
      if (paramString3 != null)
      {
        DataOutputStream localDataOutputStream = new DataOutputStream(new FileOutputStream(paramString3));
        BufferedWriter localBufferedWriter = new BufferedWriter(new OutputStreamWriter(localDataOutputStream));
        int i = arrayOfString.length;
        for (int j = 0; j < i; j++)
        {
          String str = arrayOfString[j];
          localBufferedWriter.write(str + " " + ((SymbolTable)localObject).getId(str) + "\n");
        }
        localBufferedWriter.flush();
        localDataOutputStream.close();
      }
      return;
    }
    catch (FileNotFoundException localFileNotFoundException)
    {
      localFileNotFoundException.printStackTrace();
      return;
    }
    catch (IOException localIOException)
    {
      localIOException.printStackTrace();
    }
  }

  public static void generateTbl(String paramString1, String paramString2, boolean paramBoolean1, String paramString3, boolean paramBoolean2)
  {
    generateTbl(paramString1, new HashMapBasedSymbol(), paramString2, paramBoolean1, paramString3, paramBoolean2);
  }

  public static void main(String[] paramArrayOfString)
  {
    if (paramArrayOfString.length <= 0)
    {
      System.out.println("Usage: java SymbolTblGenerator --inFile=file --outFile=file --compactFormat=true/false --plainSymbolFile=file --mmapFormat=flag");
      System.exit(1);
    }
    ConfigParser localConfigParser = new ConfigParser(paramArrayOfString);
    localConfigParser.checkRequiredProperties(new String[] { "inFile", "outFile" });
    generateTbl(localConfigParser.getProperty("inFile"), localConfigParser.getProperty("outFile"), new Boolean(localConfigParser.getProperty("compactFormat", "true")).booleanValue(), localConfigParser.getProperty("plainSymbolFile"), new Boolean(localConfigParser.getProperty("mmapFormat", "true")).booleanValue());
  }

  private static void readModel(String paramString, SymbolTable paramSymbolTable)
    throws FileNotFoundException, IOException
  {
    readModel(paramString, paramSymbolTable, null);
  }

  private static void readModel(String paramString, SymbolTable paramSymbolTable1, SymbolTable paramSymbolTable2)
    throws FileNotFoundException, IOException
  {
    logger.info("read file from " + paramString);
    DataInputStream localDataInputStream = new DataInputStream(Utils.getInputStream(paramString));
    BufferedReader localBufferedReader = new BufferedReader(new InputStreamReader(localDataInputStream));
    while (true)
    {
      String str1 = localBufferedReader.readLine();
      if (str1 == null)
        break;
      String[] arrayOfString1 = str1.trim().split("\\s+\\|{3}\\s+");
      for (int i = 0; i < -1 + arrayOfString1.length; i++)
      {
        String[] arrayOfString2 = arrayOfString1[i].split("\\s+");
        int j = arrayOfString2.length;
        int k = 0;
        if (k < j)
        {
          String str2 = arrayOfString2[k];
          if ((paramSymbolTable2 != null) && (!paramSymbolTable1.hasWord(str2)))
            paramSymbolTable2.addWord(str2);
          while (true)
          {
            k++;
            break;
            paramSymbolTable1.addWord(str2);
          }
        }
      }
    }
    localDataInputStream.close();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.tools.SymbolTblGenerator
 * JD-Core Version:    0.6.2
 */