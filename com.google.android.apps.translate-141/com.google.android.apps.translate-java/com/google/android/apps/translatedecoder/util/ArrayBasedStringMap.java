package com.google.android.apps.translatedecoder.util;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

public class ArrayBasedStringMap extends StringMap
{
  public static final int CLASS_ID = 1;
  private static final Logger logger = Logger.getLogger(ArrayBasedStringMap.class.getCanonicalName());
  private static final long serialVersionUID = 9050234241037724791L;
  private final SortedStringArray keys;
  private final SortedStringArray values;

  public ArrayBasedStringMap(SortedStringArray paramSortedStringArray1, SortedStringArray paramSortedStringArray2)
  {
    this.keys = paramSortedStringArray1;
    this.values = paramSortedStringArray2;
  }

  public ArrayBasedStringMap(Map<String, String> paramMap)
  {
    String[] arrayOfString1 = new String[paramMap.size()];
    int i = 0;
    Iterator localIterator = paramMap.keySet().iterator();
    while (localIterator.hasNext())
    {
      String str = (String)localIterator.next();
      int k = i + 1;
      arrayOfString1[i] = str;
      i = k;
    }
    Arrays.sort(arrayOfString1);
    String[] arrayOfString2 = new String[paramMap.size()];
    for (int j = 0; j < arrayOfString1.length; j++)
      arrayOfString2[j] = ((String)paramMap.get(arrayOfString1[j]));
    this.keys = new SortedStringArray(arrayOfString1, true);
    this.values = new SortedStringArray(arrayOfString2, true);
  }

  public static StringMap readFromByteBufferHelper(ByteBuffer paramByteBuffer)
  {
    logger.info("Read from buffer");
    return new ArrayBasedStringMap(SortedStringArray.readFromByteBuffer(paramByteBuffer), SortedStringArray.readFromByteBuffer(paramByteBuffer));
  }

  public String get(String paramString)
  {
    int i = this.keys.getPos(paramString);
    if (i < 0)
      return null;
    return this.values.getWord(i);
  }

  public void writeToByteBuffer(ByteBuffer paramByteBuffer)
  {
    logger.info("write to buffer, size=" + this.keys.size());
    paramByteBuffer.putInt(1);
    this.keys.writeToByteBuffer(paramByteBuffer);
    this.values.writeToByteBuffer(paramByteBuffer);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.util.ArrayBasedStringMap
 * JD-Core Version:    0.6.2
 */