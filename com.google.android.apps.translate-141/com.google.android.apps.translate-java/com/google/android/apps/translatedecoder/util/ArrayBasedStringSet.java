package com.google.android.apps.translatedecoder.util;

import java.nio.ByteBuffer;
import java.util.Iterator;
import java.util.Set;
import java.util.logging.Logger;

public class ArrayBasedStringSet extends StringSet
{
  public static final int CLASS_ID = 1;
  private static final Logger logger = Logger.getLogger(ArrayBasedStringSet.class.getCanonicalName());
  private static final long serialVersionUID = -1980208895725712034L;
  private final SortedStringArray keys;

  public ArrayBasedStringSet(SortedStringArray paramSortedStringArray)
  {
    this.keys = paramSortedStringArray;
  }

  public ArrayBasedStringSet(Set<String> paramSet)
  {
    String[] arrayOfString = new String[paramSet.size()];
    int i = 0;
    Iterator localIterator = paramSet.iterator();
    while (localIterator.hasNext())
    {
      String str = (String)localIterator.next();
      int j = i + 1;
      arrayOfString[i] = str;
      i = j;
    }
    this.keys = new SortedStringArray(arrayOfString, false);
  }

  public static StringSet readFromByteBufferHelper(ByteBuffer paramByteBuffer)
  {
    logger.info("Read from buffer");
    return new ArrayBasedStringSet(SortedStringArray.readFromByteBuffer(paramByteBuffer));
  }

  public boolean contains(String paramString)
  {
    return this.keys.contains(paramString);
  }

  public void writeToByteBuffer(ByteBuffer paramByteBuffer)
  {
    logger.info("write to buffer, size=" + this.keys.size());
    paramByteBuffer.putInt(1);
    this.keys.writeToByteBuffer(paramByteBuffer);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.util.ArrayBasedStringSet
 * JD-Core Version:    0.6.2
 */