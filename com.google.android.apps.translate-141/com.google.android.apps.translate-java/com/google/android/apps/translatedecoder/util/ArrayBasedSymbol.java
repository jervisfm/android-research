package com.google.android.apps.translatedecoder.util;

import java.nio.ByteBuffer;
import java.util.logging.Logger;

public class ArrayBasedSymbol extends SymbolTable
{
  public static final int CLASS_ID = 1;
  private static final Logger logger = Logger.getLogger(ArrayBasedSymbol.class.getCanonicalName());
  private static final long serialVersionUID = 1016624039539829432L;
  private transient HashMapBasedSymbol dynamicTbl;
  private final SortedStringArray sortedArray;

  public ArrayBasedSymbol(SortedStringArray paramSortedStringArray)
  {
    this.sortedArray = paramSortedStringArray;
  }

  public static SymbolTable readFromByteBufferHelper(ByteBuffer paramByteBuffer)
  {
    return new ArrayBasedSymbol(SortedStringArray.readFromByteBuffer(paramByteBuffer));
  }

  public int addWord(String paramString)
  {
    int i = this.sortedArray.getPos(paramString);
    if (i != -1)
      return i;
    if (this.dynamicTbl == null)
    {
      this.dynamicTbl = new HashMapBasedSymbol(this.sortedArray.maxPos());
      logger.info("First time add, the max id of static table is " + this.sortedArray.maxPos());
    }
    return this.dynamicTbl.addWord(paramString);
  }

  public String[] allWords()
  {
    return this.sortedArray.getAllWords();
  }

  public int getId(String paramString)
  {
    int i = this.sortedArray.getPos(paramString);
    if (i != -1)
      return i;
    if (this.dynamicTbl != null)
      return this.dynamicTbl.getId(paramString);
    logger.severe("The word " + paramString + " does not exist!");
    System.exit(1);
    return -1;
  }

  public String getWord(int paramInt)
  {
    String str = this.sortedArray.getWord(paramInt);
    if (str != null)
      return str;
    if (this.dynamicTbl != null)
      return this.dynamicTbl.getWord(paramInt);
    logger.severe("The id " + paramInt + " does not exist!");
    System.exit(1);
    return null;
  }

  public boolean hasWord(String paramString)
  {
    return this.sortedArray.getPos(paramString) != -1;
  }

  public int maximumId()
  {
    return this.sortedArray.maxPos();
  }

  public void writeToByteBuffer(ByteBuffer paramByteBuffer)
  {
    paramByteBuffer.putInt(1);
    this.sortedArray.writeToByteBuffer(paramByteBuffer);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.util.ArrayBasedSymbol
 * JD-Core Version:    0.6.2
 */