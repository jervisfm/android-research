package com.google.android.apps.translatedecoder.util;

import java.io.Serializable;
import java.nio.ByteBuffer;
import java.util.logging.Logger;

public abstract class BitData
  implements Serializable
{
  private static final Logger logger = Logger.getLogger(BitData.class.getName());
  private static final long serialVersionUID = 4242802809648338759L;

  public static BitData readFromByteBuffer(ByteBuffer paramByteBuffer)
  {
    int i = paramByteBuffer.getInt();
    if (i == 1)
      return ByteBufferBasedBitData.readFromByteBufferHelper(paramByteBuffer);
    throw new RuntimeException("unknown class id " + i);
  }

  public abstract void clear();

  public abstract boolean get(int paramInt);

  public abstract int length();

  public abstract int nextSetBit(int paramInt);

  public void set(int paramInt)
  {
    set(paramInt, true);
  }

  public abstract void set(int paramInt, boolean paramBoolean);

  public abstract int size();

  public abstract void writeToByteBuffer(ByteBuffer paramByteBuffer);
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.util.BitData
 * JD-Core Version:    0.6.2
 */