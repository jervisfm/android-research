package com.google.android.apps.translatedecoder.util;

import java.nio.ByteBuffer;
import java.util.BitSet;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

public class BitSetBasedBitData extends BitData
{
  private static final Logger logger = Logger.getLogger(BitSetBasedBitData.class.getName());
  private static final long serialVersionUID = -3266810767285013403L;
  private final BitSet bitSet;

  public BitSetBasedBitData(BitSet paramBitSet)
  {
    this.bitSet = paramBitSet;
  }

  public void clear()
  {
    this.bitSet.clear();
  }

  public boolean equals(Object paramObject)
  {
    if ((paramObject instanceof BitSetBasedBitData))
      return ((BitSetBasedBitData)paramObject).bitSet.equals(this.bitSet);
    return false;
  }

  public boolean get(int paramInt)
  {
    return this.bitSet.get(paramInt);
  }

  public int length()
  {
    return this.bitSet.length();
  }

  public int nextSetBit(int paramInt)
  {
    return this.bitSet.nextSetBit(paramInt);
  }

  public void set(int paramInt, boolean paramBoolean)
  {
    this.bitSet.set(paramInt, paramBoolean);
  }

  public int size()
  {
    return this.bitSet.size();
  }

  public void writeToByteBuffer(ByteBuffer paramByteBuffer)
  {
    logger.info("write to memory mapped file!");
    paramByteBuffer.putInt(1);
    List localList = BitsUtil.toLongArray(this);
    paramByteBuffer.putInt(64 * localList.size() / 8);
    Iterator localIterator = localList.iterator();
    while (localIterator.hasNext())
      paramByteBuffer.putLong(((Long)localIterator.next()).longValue());
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.util.BitSetBasedBitData
 * JD-Core Version:    0.6.2
 */