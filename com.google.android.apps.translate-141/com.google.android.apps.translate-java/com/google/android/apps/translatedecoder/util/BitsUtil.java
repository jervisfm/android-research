package com.google.android.apps.translatedecoder.util;

import java.util.ArrayList;
import java.util.List;

public class BitsUtil
{
  public static final int LM_MAX_MMAP_SIZE = 500000000;
  public static final int PT_MAX_MMAP_SIZE = 500000000;
  public static final int TOKENIZER_MAX_MMAP_SIZE = 100000000;

  public static int bitSetToInt(BitData paramBitData)
  {
    if (paramBitData.length() <= 0)
      return 0;
    return bitSetToInt(paramBitData, 0, paramBitData.length());
  }

  public static int bitSetToInt(BitData paramBitData, int paramInt1, int paramInt2)
  {
    if (paramInt2 <= 0)
      throw new IllegalArgumentException("len is zero or negative!");
    if (paramInt2 > 32)
      throw new IllegalArgumentException("Bitset is too big for an integer, len = " + paramInt2 + " max_size = " + 32);
    int i = 0;
    for (int j = paramBitData.nextSetBit(paramInt1); (j >= 0) && (j < paramInt1 + paramInt2); j = paramBitData.nextSetBit(j + 1))
      i |= 1 << j - paramInt1;
    return i;
  }

  public static long bitSetToLong(BitData paramBitData, int paramInt1, int paramInt2)
  {
    if (paramInt2 <= 0)
      throw new IllegalArgumentException("len is zero or negative!");
    if (paramInt2 > 64)
      throw new IllegalArgumentException("Bitset is too big for a Long, len = " + paramInt2 + " max_size = " + 64);
    long l = 0L;
    for (int i = paramBitData.nextSetBit(paramInt1); (i >= 0) && (i < paramInt1 + paramInt2); i = paramBitData.nextSetBit(i + 1))
      l |= 1L << i - paramInt1;
    return l;
  }

  public static void intToBitSet(int paramInt, BitData paramBitData)
  {
    intToBitSet(paramInt, paramBitData, 0, paramBitData.size());
  }

  public static void intToBitSet(int paramInt1, BitData paramBitData, int paramInt2, int paramInt3)
  {
    if (paramInt3 <= 0);
    int i;
    do
    {
      return;
      int j;
      while (j == 0)
      {
        i = 0;
        j = paramInt1;
      }
      if (j % 2 != 0)
        paramBitData.set(i + paramInt2);
      j >>>= 1;
      i++;
    }
    while (i <= paramInt3);
    throw new IllegalArgumentException("The integer is longer than specified length, n=" + paramInt1 + "; index=" + i + "; len=" + paramInt3);
  }

  public static int numBitsRequired(int paramInt)
  {
    if (paramInt <= 0)
      throw new IllegalArgumentException("input n is not greater than 0; n=" + paramInt);
    for (int i = 1; ; i++)
      if (Math.pow(2.0D, i) - 1.0D >= paramInt)
        return i;
  }

  public static List<Long> toLongArray(BitData paramBitData)
  {
    ArrayList localArrayList = new ArrayList();
    int i = 0;
    if (i < paramBitData.length())
    {
      if (paramBitData.length() - i < 64)
        localArrayList.add(Long.valueOf(bitSetToLong(paramBitData, i, paramBitData.length() - i)));
      while (true)
      {
        i += 64;
        break;
        localArrayList.add(Long.valueOf(bitSetToLong(paramBitData, i, 64)));
      }
    }
    return localArrayList;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.util.BitsUtil
 * JD-Core Version:    0.6.2
 */