package com.google.android.apps.translatedecoder.util;

import java.nio.ByteBuffer;
import java.nio.LongBuffer;
import java.util.logging.Logger;

public class ByteBufferBasedBitData extends BitData
{
  public static final int CLASS_ID = 1;
  private static final Logger logger = Logger.getLogger(ByteBufferBasedBitData.class.getName());
  private static final long serialVersionUID = -8628852561243908443L;
  private final LongBuffer lb;
  private final int size;

  public ByteBufferBasedBitData(LongBuffer paramLongBuffer)
  {
    this.lb = paramLongBuffer;
    this.size = (64 * paramLongBuffer.limit());
  }

  public static BitData readFromByteBufferHelper(ByteBuffer paramByteBuffer)
  {
    int i = paramByteBuffer.getInt();
    LongBuffer localLongBuffer = MemMapUtil.subBuffer(paramByteBuffer, i).asLongBuffer();
    paramByteBuffer.position(i + paramByteBuffer.position());
    return new ByteBufferBasedBitData(localLongBuffer);
  }

  public void clear()
  {
    logger.severe("calling unimplement function");
    System.exit(1);
  }

  public boolean get(int paramInt)
  {
    if (paramInt < 0)
      throw new IndexOutOfBoundsException("bitIndex < 0: " + paramInt);
    return (this.lb.get(paramInt / 64) & 1L << paramInt % 64) != 0L;
  }

  public int length()
  {
    return this.size;
  }

  public int nextSetBit(int paramInt)
  {
    if (paramInt < 0)
      throw new IndexOutOfBoundsException("fromIndex < 0: " + paramInt);
    int i = 1;
    int j = paramInt;
    int k = j / 64;
    int m = j % 64;
    long l = 0L;
    while (j < this.size)
    {
      if (i != 0)
      {
        l = this.lb.get(k);
        i = 0;
      }
      if ((l & 1L << m) != 0L)
        return j;
      j++;
      m++;
      if (m == 64)
      {
        k++;
        i = 1;
        m = 0;
      }
    }
    return -1;
  }

  public void set(int paramInt, boolean paramBoolean)
  {
    logger.severe("calling unimplement function");
    System.exit(1);
  }

  public int size()
  {
    return this.size;
  }

  public void writeToByteBuffer(ByteBuffer paramByteBuffer)
  {
    logger.info("write to memory mapped file!");
    paramByteBuffer.putInt(1);
    paramByteBuffer.putInt(64 * this.lb.limit() / 8);
    for (int i = 0; i < this.lb.limit(); i++)
      paramByteBuffer.putLong(this.lb.get());
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.util.ByteBufferBasedBitData
 * JD-Core Version:    0.6.2
 */