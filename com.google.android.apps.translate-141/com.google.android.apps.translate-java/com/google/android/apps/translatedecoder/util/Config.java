package com.google.android.apps.translatedecoder.util;

import java.io.FileInputStream;
import java.io.PrintStream;
import java.util.Enumeration;
import java.util.Properties;
import java.util.logging.Logger;

public class Config
{
  private static final Logger logger = Logger.getLogger(Config.class.getCanonicalName());
  private int beamSize = 3;
  private boolean convertSymbol = false;
  private double dominateCost = -10.0D;
  private String inputFile;
  private String lmFile;
  private int lmOrder = 0;
  private String lmSymbolTblFile;
  private int maxNumCachedNgrams = 10000;
  private int maxPhraseLength = 0;
  private double maxSearchError = 0.05D;
  private String ngramQueriedFile;
  private double oovLmCost = 0.0D;
  private double oovTmCost = 0.0D;
  private String outputFile;
  private String preprocDataFile;
  private boolean printBracket = false;
  private String ptSymbolTblFile;
  private String rapidRespFile = null;
  private boolean readSymbolFromFile = false;
  private double relativeLmWeight = 1.0D;
  private boolean removeDiacritics = false;
  private boolean runPostprocess = true;
  private boolean runPreprocess = true;
  private boolean simulateProdlm = false;
  private String srcLang = "ENGLISH";
  private String tgtLang = "FRENCH";
  private String tmFile;

  public Config()
  {
  }

  public Config(String paramString)
  {
    Properties localProperties = new Properties();
    try
    {
      localProperties.load(new FileInputStream(paramString));
      Enumeration localEnumeration = localProperties.propertyNames();
      while (localEnumeration.hasMoreElements())
      {
        String str = (String)localEnumeration.nextElement();
        setField(str, localProperties.getProperty(str));
        System.out.println("key=" + str + "; val=" + localProperties.getProperty(str));
      }
    }
    catch (Exception localException)
    {
      while (true)
        localException.printStackTrace();
    }
  }

  public int beamSize()
  {
    return this.beamSize;
  }

  public boolean convertSymbol()
  {
    return this.convertSymbol;
  }

  public double dominateCost()
  {
    return this.dominateCost;
  }

  public String inputFile()
  {
    return this.inputFile;
  }

  public String lmFile()
  {
    return this.lmFile;
  }

  public int lmOrder()
  {
    return this.lmOrder;
  }

  public String lmSymbolTblFile()
  {
    return this.lmSymbolTblFile;
  }

  public int maxNumCachedNgrams()
  {
    return this.maxNumCachedNgrams;
  }

  public int maxPhraseLength()
  {
    return this.maxPhraseLength;
  }

  public double maxSearchError()
  {
    return this.maxSearchError;
  }

  public String ngramQueriedFile()
  {
    return this.ngramQueriedFile;
  }

  public double oovLmCost()
  {
    return this.oovLmCost;
  }

  public double oovTmCost()
  {
    return this.oovTmCost;
  }

  public String outputFile()
  {
    return this.outputFile;
  }

  public String preprocDataFile()
  {
    return this.preprocDataFile;
  }

  public boolean printBracket()
  {
    return this.printBracket;
  }

  public String ptSymbolTblFile()
  {
    return this.ptSymbolTblFile;
  }

  public String rapidRespFile()
  {
    return this.rapidRespFile;
  }

  public boolean readSymbolFromFile()
  {
    return this.readSymbolFromFile;
  }

  public double relativeLmWeight()
  {
    return this.relativeLmWeight;
  }

  public boolean removeDiacritics()
  {
    return this.removeDiacritics;
  }

  public boolean runPostprocess()
  {
    return this.runPostprocess;
  }

  public boolean runPreprocess()
  {
    return this.runPreprocess;
  }

  public void setBeamSize(int paramInt)
  {
    this.beamSize = paramInt;
  }

  public void setConvertSymbol(boolean paramBoolean)
  {
    this.convertSymbol = paramBoolean;
  }

  public void setDominateCost(double paramDouble)
  {
    this.dominateCost = paramDouble;
  }

  public void setField(String paramString1, String paramString2)
  {
    if (paramString1.equals("inputFile"))
    {
      this.inputFile = paramString2;
      return;
    }
    if (paramString1.equals("runPreprocess"))
    {
      this.runPreprocess = new Boolean(paramString2).booleanValue();
      return;
    }
    if (paramString1.equals("runPostprocess"))
    {
      this.runPostprocess = new Boolean(paramString2).booleanValue();
      return;
    }
    if (paramString1.equals("outputFile"))
    {
      this.outputFile = paramString2;
      return;
    }
    if (paramString1.equals("preprocDataFile"))
    {
      this.preprocDataFile = paramString2;
      return;
    }
    if (paramString1.equals("srcLang"))
    {
      this.srcLang = paramString2;
      return;
    }
    if (paramString1.equals("tgtLang"))
    {
      this.tgtLang = paramString2;
      return;
    }
    if (paramString1.equals("removeDiacritics"))
    {
      this.removeDiacritics = new Boolean(paramString2).booleanValue();
      return;
    }
    if (paramString1.equals("readSymbolFromFile"))
    {
      this.readSymbolFromFile = new Boolean(paramString2).booleanValue();
      return;
    }
    if (paramString1.equals("ptSymbolTblFile"))
    {
      this.ptSymbolTblFile = paramString2;
      return;
    }
    if (paramString1.equals("lmSymbolTblFile"))
    {
      this.lmSymbolTblFile = paramString2;
      return;
    }
    if (paramString1.equals("convertSymbol"))
    {
      this.convertSymbol = new Boolean(paramString2).booleanValue();
      return;
    }
    if (paramString1.equals("tmFile"))
    {
      this.tmFile = paramString2;
      return;
    }
    if (paramString1.equals("maxPhraseLength"))
    {
      this.maxPhraseLength = Integer.valueOf(paramString2).intValue();
      return;
    }
    if (paramString1.equals("lmFile"))
    {
      this.lmFile = paramString2;
      return;
    }
    if (paramString1.equals("relativeLmWeight"))
    {
      this.relativeLmWeight = new Double(paramString2).doubleValue();
      return;
    }
    if (paramString1.equals("lmOrder"))
    {
      this.lmOrder = Integer.valueOf(paramString2).intValue();
      return;
    }
    if (paramString1.equals("ngramQueriedFile"))
    {
      this.ngramQueriedFile = paramString2;
      return;
    }
    if (paramString1.equals("simulateProdlm"))
    {
      this.simulateProdlm = new Boolean(paramString2).booleanValue();
      return;
    }
    if (paramString1.equals("maxNumCachedNgrams"))
    {
      this.maxNumCachedNgrams = Integer.valueOf(paramString2).intValue();
      return;
    }
    if (paramString1.equals("printBracket"))
    {
      this.printBracket = new Boolean(paramString2).booleanValue();
      return;
    }
    if (paramString1.equals("oovTmCost"))
    {
      this.oovTmCost = new Double(paramString2).doubleValue();
      return;
    }
    if (paramString1.equals("oovLmCost"))
    {
      this.oovLmCost = new Double(paramString2).doubleValue();
      return;
    }
    if (paramString1.equals("rapidRespFile"))
    {
      this.rapidRespFile = paramString2.trim();
      return;
    }
    if (paramString1.equals("dominateCost"))
    {
      this.dominateCost = new Double(paramString2).doubleValue();
      return;
    }
    if (paramString1.equals("beamSize"))
    {
      this.beamSize = new Integer(paramString2).intValue();
      return;
    }
    if (paramString1.equals("maxSearchError"))
    {
      this.maxSearchError = new Double(paramString2).doubleValue();
      return;
    }
    logger.severe("wrong configuration line! key=" + paramString1 + "; val=" + paramString2);
    System.exit(1);
  }

  public void setInputFile(String paramString)
  {
    this.inputFile = paramString;
  }

  public void setLmFile(String paramString)
  {
    this.lmFile = paramString;
  }

  public void setLmOrder(int paramInt)
  {
    this.lmOrder = paramInt;
  }

  public void setLmSymbolTblFile(String paramString)
  {
    this.lmSymbolTblFile = paramString;
  }

  public void setMaxNumCachedNgrams(int paramInt)
  {
    this.maxNumCachedNgrams = paramInt;
  }

  public void setMaxPhraseLength(int paramInt)
  {
    this.maxPhraseLength = paramInt;
  }

  public void setMaxSearchError(double paramDouble)
  {
    this.maxSearchError = paramDouble;
  }

  public void setNgramQueriedFile(String paramString)
  {
    this.ngramQueriedFile = paramString;
  }

  public void setOovLmCost(double paramDouble)
  {
    this.oovLmCost = paramDouble;
  }

  public void setOovTmCost(double paramDouble)
  {
    this.oovTmCost = paramDouble;
  }

  public void setOutputFile(String paramString)
  {
    this.outputFile = paramString;
  }

  public void setParametersFromArgs(String[] paramArrayOfString)
  {
    if (paramArrayOfString == null)
      return;
    int i = 0;
    label7: if (i < paramArrayOfString.length)
    {
      if (!paramArrayOfString[i].matches("--\\w+=[^\\s]+"))
        break label101;
      String[] arrayOfString = paramArrayOfString[i].trim().split("=");
      if (arrayOfString.length != 2)
      {
        logger.severe("config option does not have two fields! " + paramArrayOfString[i]);
        System.exit(1);
      }
      setField(arrayOfString[0].replace("--", ""), arrayOfString[1]);
    }
    while (true)
    {
      i++;
      break label7;
      break;
      label101: logger.severe("wrong config option!" + paramArrayOfString[i]);
      System.exit(1);
    }
  }

  public void setPreprocDataFile(String paramString)
  {
    this.preprocDataFile = paramString;
  }

  public void setPrintBracket(boolean paramBoolean)
  {
    this.printBracket = paramBoolean;
  }

  public void setPtSymbolTblFile(String paramString)
  {
    this.ptSymbolTblFile = paramString;
  }

  public void setRapidRespFile(String paramString)
  {
    this.rapidRespFile = paramString;
  }

  public void setReadSymbolFile(boolean paramBoolean)
  {
    this.readSymbolFromFile = paramBoolean;
  }

  public void setRelativeLmWeight(double paramDouble)
  {
    this.relativeLmWeight = paramDouble;
  }

  public void setRemoveDiacritics(boolean paramBoolean)
  {
    this.removeDiacritics = paramBoolean;
  }

  public void setRunPostprocess(boolean paramBoolean)
  {
    this.runPostprocess = paramBoolean;
  }

  public void setRunPreprocess(boolean paramBoolean)
  {
    this.runPreprocess = paramBoolean;
  }

  public void setSimulateProdlm(boolean paramBoolean)
  {
    this.simulateProdlm = paramBoolean;
  }

  public void setSrcLang(String paramString)
  {
    this.srcLang = paramString;
  }

  public void setTgtLang(String paramString)
  {
    this.tgtLang = paramString;
  }

  public void setTmFile(String paramString)
  {
    this.tmFile = paramString;
  }

  public boolean simulateProdlm()
  {
    return this.simulateProdlm;
  }

  public String srcLang()
  {
    return this.srcLang;
  }

  public String tgtLang()
  {
    return this.tgtLang;
  }

  public String tmFile()
  {
    return this.tmFile;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.util.Config
 * JD-Core Version:    0.6.2
 */