package com.google.android.apps.translatedecoder.util;

import java.io.PrintStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.logging.Logger;

public class ConfigParser
{
  private static final Logger logger = Logger.getLogger(ConfigParser.class.getCanonicalName());
  Map<String, String> properties = new HashMap();

  public ConfigParser(String[] paramArrayOfString)
  {
    int i = 0;
    if (i < paramArrayOfString.length)
    {
      if (paramArrayOfString[i].matches("--\\w+=[^\\s=]+"))
      {
        String[] arrayOfString = paramArrayOfString[i].trim().split("=");
        if (arrayOfString.length != 2)
        {
          logger.severe("config option does not have two fields! " + paramArrayOfString[i]);
          System.exit(1);
        }
        arrayOfString[0] = arrayOfString[0].replace("--", "");
        this.properties.put(arrayOfString[0].trim(), arrayOfString[1].trim());
      }
      while (true)
      {
        i++;
        break;
        logger.severe("wrong config option!" + paramArrayOfString[i]);
        System.exit(1);
      }
    }
  }

  public void checkRequiredProperties(String[] paramArrayOfString)
  {
    int i = paramArrayOfString.length;
    int j = 0;
    if (j < i)
    {
      String str = paramArrayOfString[j];
      if (!hasProperty(str))
      {
        logger.severe("Miss the required property " + str + "; try to add --" + str + "=somevalue");
        System.exit(1);
      }
      while (true)
      {
        j++;
        break;
        logger.info(str + "=" + getProperty(str));
      }
    }
  }

  public String getProperty(String paramString)
  {
    return (String)this.properties.get(paramString);
  }

  public String getProperty(String paramString1, String paramString2)
  {
    if (hasProperty(paramString1))
      return (String)this.properties.get(paramString1);
    return paramString2;
  }

  public boolean hasProperty(String paramString)
  {
    return this.properties.containsKey(paramString);
  }

  public void printProperties()
  {
    System.out.println("======== List of Configuration Properties ========");
    Iterator localIterator = this.properties.entrySet().iterator();
    while (localIterator.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      System.out.println("--" + (String)localEntry.getKey() + "=" + (String)localEntry.getValue());
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.util.ConfigParser
 * JD-Core Version:    0.6.2
 */