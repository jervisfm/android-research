package com.google.android.apps.translatedecoder.util;

import java.nio.ByteBuffer;
import java.util.Map;
import java.util.logging.Logger;

public class HashMapBasedStringMap extends StringMap
{
  private static final Logger logger = Logger.getLogger(HashMapBasedStringMap.class.getName());
  private static final long serialVersionUID = -7100675744920843709L;
  private final Map<String, String> map;

  public HashMapBasedStringMap(Map<String, String> paramMap)
  {
    this.map = paramMap;
  }

  public String get(String paramString)
  {
    return (String)this.map.get(paramString);
  }

  public void writeToByteBuffer(ByteBuffer paramByteBuffer)
  {
    logger.severe("calling unimplement function");
    System.exit(1);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.util.HashMapBasedStringMap
 * JD-Core Version:    0.6.2
 */