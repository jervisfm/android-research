package com.google.android.apps.translatedecoder.util;

import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

public class HashMapBasedSymbol extends SymbolTable
{
  private static final Logger logger = Logger.getLogger(HashMapBasedSymbol.class.getCanonicalName());
  private static final long serialVersionUID = 1471978034784649406L;
  private int curId;
  private final Map<Integer, String> intToStrTbl = new HashMap();
  private final Map<String, Integer> strToIntTbl = new HashMap();

  public HashMapBasedSymbol()
  {
    this(0);
  }

  public HashMapBasedSymbol(int paramInt)
  {
    this.curId = paramInt;
  }

  public int addWord(String paramString)
  {
    Integer localInteger = (Integer)this.strToIntTbl.get(paramString);
    if (localInteger != null)
      return localInteger.intValue();
    this.curId = (1 + this.curId);
    this.strToIntTbl.put(paramString, Integer.valueOf(this.curId));
    this.intToStrTbl.put(Integer.valueOf(this.curId), paramString);
    return this.curId;
  }

  public String[] allWords()
  {
    String[] arrayOfString = new String[this.strToIntTbl.size()];
    int i = 0;
    Iterator localIterator = this.strToIntTbl.keySet().iterator();
    while (localIterator.hasNext())
    {
      String str = (String)localIterator.next();
      int j = i + 1;
      arrayOfString[i] = str;
      i = j;
    }
    return arrayOfString;
  }

  public int getId(String paramString)
  {
    Integer localInteger = (Integer)this.strToIntTbl.get(paramString);
    if (localInteger != null)
      return localInteger.intValue();
    logger.severe("The word " + paramString + " does not exist!");
    System.exit(1);
    return -1;
  }

  public String getWord(int paramInt)
  {
    String str = (String)this.intToStrTbl.get(Integer.valueOf(paramInt));
    if (str != null)
      return str;
    logger.severe("The id " + paramInt + " does not exist!");
    System.exit(1);
    return null;
  }

  public boolean hasWord(String paramString)
  {
    return this.strToIntTbl.containsKey(paramString);
  }

  public int maximumId()
  {
    throw new UnsupportedOperationException("calling unimplement(ed) function");
  }

  public void writeToByteBuffer(ByteBuffer paramByteBuffer)
  {
    logger.severe("calling unimplement(ed) function");
    System.exit(1);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.util.HashMapBasedSymbol
 * JD-Core Version:    0.6.2
 */