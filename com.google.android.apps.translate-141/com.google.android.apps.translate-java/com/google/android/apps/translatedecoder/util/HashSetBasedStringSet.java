package com.google.android.apps.translatedecoder.util;

import java.nio.ByteBuffer;
import java.util.Set;
import java.util.logging.Logger;

public class HashSetBasedStringSet extends StringSet
{
  private static final Logger logger = Logger.getLogger(HashSetBasedStringSet.class.getName());
  private static final long serialVersionUID = -3796078103315976035L;
  private final Set<String> set;

  public HashSetBasedStringSet(Set<String> paramSet)
  {
    this.set = paramSet;
  }

  public boolean contains(String paramString)
  {
    return this.set.contains(paramString);
  }

  public void writeToByteBuffer(ByteBuffer paramByteBuffer)
  {
    logger.severe("calling unimplement function");
    System.exit(1);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.util.HashSetBasedStringSet
 * JD-Core Version:    0.6.2
 */