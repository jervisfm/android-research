package com.google.android.apps.translatedecoder.util;

import com.google.android.apps.translatedecoder.lm.ImplictTrieBasedLm;
import com.google.android.apps.translatedecoder.lm.LanguageModel;
import com.google.android.apps.translatedecoder.succinct.FixedBitsTrieNodeList;
import com.google.android.apps.translatedecoder.succinct.ImplicitTrie;
import com.google.android.apps.translatedecoder.succinct.Quantizer;
import com.google.android.apps.translatedecoder.succinct.TrieNode;
import com.google.android.apps.translatedecoder.succinct.TrieNodeList;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;

public class ImplicitTrieLmGenerator
{
  public static ImplicitTrie constructImplicitTrie(SymbolTable paramSymbolTable, double paramDouble)
  {
    String[] arrayOfString = { "<S>", "A", "AA", "B", "C", "D", "E", "</S>" };
    for (int i = 1; i <= arrayOfString.length; i++)
      paramSymbolTable.addWord(arrayOfString[(i - 1)]);
    ArrayList localArrayList = new ArrayList();
    BitSet localBitSet = new BitSet();
    Quantizer localQuantizer = new Quantizer(-1.0D, 0.01D, 10000);
    FixedBitsTrieNodeList localFixedBitsTrieNodeList1 = new FixedBitsTrieNodeList(localBitSet, 0, 32, 32, 32, localQuantizer);
    localFixedBitsTrieNodeList1.add(new TrieNode(paramSymbolTable.getId("<S>"), 0, paramDouble));
    localFixedBitsTrieNodeList1.add(new TrieNode(paramSymbolTable.getId("A"), 2, paramDouble));
    localFixedBitsTrieNodeList1.add(new TrieNode(paramSymbolTable.getId("B"), 4, paramDouble));
    localFixedBitsTrieNodeList1.add(new TrieNode(paramSymbolTable.getId("C"), 6, paramDouble));
    localFixedBitsTrieNodeList1.add(new TrieNode(paramSymbolTable.getId("D"), 8, paramDouble));
    localFixedBitsTrieNodeList1.add(new TrieNode(paramSymbolTable.getId("E"), 9, paramDouble));
    localArrayList.add(localFixedBitsTrieNodeList1);
    FixedBitsTrieNodeList localFixedBitsTrieNodeList2 = new FixedBitsTrieNodeList(new BitSet(), 0, 32, 0, 32, localQuantizer);
    localFixedBitsTrieNodeList2.add(new TrieNode(paramSymbolTable.getId("A"), 0, 0.0D));
    localFixedBitsTrieNodeList2.add(new TrieNode(paramSymbolTable.getId("AA"), 0, 10.0D));
    localFixedBitsTrieNodeList2.add(new TrieNode(paramSymbolTable.getId("B"), 0, 0.13D));
    localFixedBitsTrieNodeList2.add(new TrieNode(paramSymbolTable.getId("E"), 0, 0.4D));
    localFixedBitsTrieNodeList2.add(new TrieNode(paramSymbolTable.getId("C"), 0, 0.2D));
    localFixedBitsTrieNodeList2.add(new TrieNode(paramSymbolTable.getId("E"), 0, 0.5D));
    localFixedBitsTrieNodeList2.add(new TrieNode(paramSymbolTable.getId("D"), 0, 0.3D));
    localFixedBitsTrieNodeList2.add(new TrieNode(paramSymbolTable.getId("E"), 0, -0.19D));
    localFixedBitsTrieNodeList2.add(new TrieNode(paramSymbolTable.getId("</S>"), 0, 4.0D));
    localFixedBitsTrieNodeList2.add(new TrieNode(paramSymbolTable.getId("</S>"), 0, 1.0D));
    localArrayList.add(localFixedBitsTrieNodeList2);
    return new ImplicitTrie(2, localArrayList);
  }

  public static void main(String[] paramArrayOfString)
  {
    new ImplictTrieBasedLm(2, 0.0D, 0.0D, false, constructImplicitTrie(SymbolTable.readFromFile("/home/zfli/git_android/google3/java/com/google/android/apps/translatedecoder/tests/src/testdata/symbol.txt"), 0.0D)).writeToFile("/home/zfli/git_android/google3/java/com/google/android/apps/translatedecoder/tests/src/testdata/implicit_trie_lm_oovcost_0.mmap", true);
    LanguageModel localLanguageModel = LanguageModel.readFromFile("/home/zfli/git_android/google3/java/com/google/android/apps/translatedecoder/tests/src/testdata/implicit_trie_lm_oovcost_0.mmap");
    System.out.println("lm2 " + localLanguageModel.lmOrder());
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.util.ImplicitTrieLmGenerator
 * JD-Core Version:    0.6.2
 */