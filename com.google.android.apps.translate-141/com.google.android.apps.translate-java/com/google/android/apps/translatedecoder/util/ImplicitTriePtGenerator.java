package com.google.android.apps.translatedecoder.util;

import com.google.android.apps.translatedecoder.pt.ImplicitTrieBasedPt;
import com.google.android.apps.translatedecoder.pt.PhrasePair;
import com.google.android.apps.translatedecoder.pt.PhraseTable;
import com.google.android.apps.translatedecoder.pt.TargetPhrasesContainer;
import com.google.android.apps.translatedecoder.succinct.FixedBitsTrieNodeList;
import com.google.android.apps.translatedecoder.succinct.ImplicitTrie;
import com.google.android.apps.translatedecoder.succinct.Quantizer;
import com.google.android.apps.translatedecoder.succinct.TrieNode;
import com.google.android.apps.translatedecoder.succinct.TrieNodeList;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;

public class ImplicitTriePtGenerator
{
  public static ImplicitTrieBasedPt constructImplicitTriePt(SymbolTable paramSymbolTable)
  {
    Quantizer localQuantizer1 = new Quantizer(0.0D, 0.05D, 10000);
    TargetPhrasesContainer localTargetPhrasesContainer = new TargetPhrasesContainer(new BitSet(), 32, 32, localQuantizer1);
    int[] arrayOfInt = new int[10];
    ArrayList localArrayList1 = new ArrayList();
    int i = 0 + 1;
    arrayOfInt[0] = 0;
    localArrayList1.add(new PhrasePair(paramSymbolTable.addWords("a"), paramSymbolTable.addWords("A"), 1.0D));
    localArrayList1.add(new PhrasePair(paramSymbolTable.addWords("a"), paramSymbolTable.addWords("AA"), 2.0D));
    int j = localTargetPhrasesContainer.addTargetPhrases(localArrayList1, 0);
    int k = i + 1;
    arrayOfInt[i] = j;
    localArrayList1.clear();
    localArrayList1.add(new PhrasePair(paramSymbolTable.addWords("b"), paramSymbolTable.addWords("B"), 1.0D));
    int m = localTargetPhrasesContainer.addTargetPhrases(localArrayList1, j);
    int n = k + 1;
    arrayOfInt[k] = m;
    localArrayList1.clear();
    localArrayList1.add(new PhrasePair(paramSymbolTable.addWords("c"), paramSymbolTable.addWords("C"), 1.0D));
    int i1 = localTargetPhrasesContainer.addTargetPhrases(localArrayList1, m);
    int i2 = n + 1;
    arrayOfInt[n] = i1;
    localArrayList1.clear();
    localArrayList1.add(new PhrasePair(paramSymbolTable.addWords("d"), paramSymbolTable.addWords("D"), 1.0D));
    int i3 = localTargetPhrasesContainer.addTargetPhrases(localArrayList1, i1);
    int i4 = i2 + 1;
    arrayOfInt[i2] = i3;
    localArrayList1.clear();
    localArrayList1.add(new PhrasePair(paramSymbolTable.addWords("a b"), paramSymbolTable.addWords("A B"), 1.1D));
    localArrayList1.add(new PhrasePair(paramSymbolTable.addWords("a b"), paramSymbolTable.addWords("AA BB"), 2.0D));
    int i5 = localTargetPhrasesContainer.addTargetPhrases(localArrayList1, i3);
    int i6 = i4 + 1;
    arrayOfInt[i4] = i5;
    localArrayList1.clear();
    localArrayList1.add(new PhrasePair(paramSymbolTable.addWords("b c"), paramSymbolTable.addWords("B C"), 1.0D));
    int i7 = localTargetPhrasesContainer.addTargetPhrases(localArrayList1, i5);
    int i8 = i6 + 1;
    arrayOfInt[i6] = i7;
    localArrayList1.clear();
    localArrayList1.add(new PhrasePair(paramSymbolTable.addWords("c d"), paramSymbolTable.addWords("C E"), 3.0D));
    int i9 = localTargetPhrasesContainer.addTargetPhrases(localArrayList1, i7);
    int i10 = i8 + 1;
    arrayOfInt[i8] = i9;
    localArrayList1.clear();
    localArrayList1.add(new PhrasePair(paramSymbolTable.addWords("a b c"), paramSymbolTable.addWords("A BB C"), 10.0D));
    localArrayList1.add(new PhrasePair(paramSymbolTable.addWords("a b c"), paramSymbolTable.addWords("A BB CC"), 4.0D));
    int i11 = localTargetPhrasesContainer.addTargetPhrases(localArrayList1, i9);
    int i12 = i10 + 1;
    arrayOfInt[i10] = i11;
    localArrayList1.clear();
    localArrayList1.add(new PhrasePair(paramSymbolTable.addWords("b c d"), paramSymbolTable.addWords("B C D"), 0.9D));
    int i13 = localTargetPhrasesContainer.addTargetPhrases(localArrayList1, i11);
    (i12 + 1);
    arrayOfInt[i12] = i13;
    localArrayList1.clear();
    localArrayList1.add(new PhrasePair(paramSymbolTable.addWords("a b c d"), paramSymbolTable.addWords("A B C D"), 2.9D));
    localArrayList1.add(new PhrasePair(paramSymbolTable.addWords("a b c d"), paramSymbolTable.addWords("A B C DD"), 5.0D));
    localTargetPhrasesContainer.addTargetPhrases(localArrayList1, i13);
    ArrayList localArrayList2 = new ArrayList();
    Quantizer localQuantizer2 = new Quantizer(-1.0D, 1.0D, 10000);
    FixedBitsTrieNodeList localFixedBitsTrieNodeList1 = new FixedBitsTrieNodeList(new BitSet(), 0, 32, 32, 32, localQuantizer2);
    int i14 = paramSymbolTable.addWord("a");
    int i15 = 0 + 1;
    localFixedBitsTrieNodeList1.add(new TrieNode(i14, 0, arrayOfInt[0]));
    int i16 = paramSymbolTable.addWord("b");
    int i17 = i15 + 1;
    localFixedBitsTrieNodeList1.add(new TrieNode(i16, 1, arrayOfInt[i15]));
    int i18 = paramSymbolTable.addWord("c");
    int i19 = i17 + 1;
    localFixedBitsTrieNodeList1.add(new TrieNode(i18, 2, arrayOfInt[i17]));
    int i20 = paramSymbolTable.addWord("d");
    int i21 = i19 + 1;
    localFixedBitsTrieNodeList1.add(new TrieNode(i20, 3, arrayOfInt[i19]));
    localArrayList2.add(localFixedBitsTrieNodeList1);
    FixedBitsTrieNodeList localFixedBitsTrieNodeList2 = new FixedBitsTrieNodeList(new BitSet(), 0, 32, 32, 32, localQuantizer2);
    int i22 = paramSymbolTable.addWord("b");
    int i23 = i21 + 1;
    localFixedBitsTrieNodeList2.add(new TrieNode(i22, 0, arrayOfInt[i21]));
    int i24 = paramSymbolTable.addWord("c");
    int i25 = i23 + 1;
    localFixedBitsTrieNodeList2.add(new TrieNode(i24, 1, arrayOfInt[i23]));
    int i26 = paramSymbolTable.addWord("d");
    int i27 = i25 + 1;
    localFixedBitsTrieNodeList2.add(new TrieNode(i26, 2, arrayOfInt[i25]));
    localArrayList2.add(localFixedBitsTrieNodeList2);
    FixedBitsTrieNodeList localFixedBitsTrieNodeList3 = new FixedBitsTrieNodeList(new BitSet(), 0, 32, 32, 32, localQuantizer2);
    int i28 = paramSymbolTable.addWord("c");
    int i29 = i27 + 1;
    localFixedBitsTrieNodeList3.add(new TrieNode(i28, 0, arrayOfInt[i27]));
    int i30 = paramSymbolTable.addWord("d");
    int i31 = i29 + 1;
    localFixedBitsTrieNodeList3.add(new TrieNode(i30, 1, arrayOfInt[i29]));
    localArrayList2.add(localFixedBitsTrieNodeList3);
    FixedBitsTrieNodeList localFixedBitsTrieNodeList4 = new FixedBitsTrieNodeList(new BitSet(), 0, 32, 32, 32, localQuantizer2);
    int i32 = paramSymbolTable.addWord("d");
    (i31 + 1);
    localFixedBitsTrieNodeList4.add(new TrieNode(i32, -1, arrayOfInt[i31]));
    localArrayList2.add(localFixedBitsTrieNodeList4);
    ImplicitTrie localImplicitTrie = new ImplicitTrie(4, localArrayList2);
    TrieNode localTrieNode = localImplicitTrie.lookup(paramSymbolTable.addWords("a b c d"));
    System.out.println("trieNode=" + localTrieNode);
    return new ImplicitTrieBasedPt(4, 100.0D, localImplicitTrie, localTargetPhrasesContainer);
  }

  public static void main(String[] paramArrayOfString)
  {
    constructImplicitTriePt(SymbolTable.readFromFile("/home/zfli/git_android/google3/java/com/google/android/apps/translatedecoder/tests/src/testdata/symbol.txt")).writeToFile("/home/zfli/git_android/google3/java/com/google/android/apps/translatedecoder/tests/src/testdata/implicit_trie_pt.mmap", true);
    PhraseTable localPhraseTable = PhraseTable.readFromFile("/home/zfli/git_android/google3/java/com/google/android/apps/translatedecoder/tests/src/testdata/implicit_trie_pt.mmap");
    System.out.println("pt2 " + localPhraseTable.maxPhraseLen());
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.util.ImplicitTriePtGenerator
 * JD-Core Version:    0.6.2
 */