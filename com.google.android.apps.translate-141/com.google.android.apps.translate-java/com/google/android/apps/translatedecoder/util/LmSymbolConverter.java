package com.google.android.apps.translatedecoder.util;

public class LmSymbolConverter
{
  private final SymbolTable lmSymbolTbl;
  private int lmUnkId = -1;
  private final SymbolTable ptSymbolTbl;
  private boolean simulateProdlm = false;

  public LmSymbolConverter(SymbolTable paramSymbolTable1, SymbolTable paramSymbolTable2, boolean paramBoolean, int paramInt)
  {
    this.ptSymbolTbl = paramSymbolTable1;
    this.lmSymbolTbl = paramSymbolTable2;
    this.simulateProdlm = paramBoolean;
    this.lmUnkId = paramInt;
  }

  public int lmUnkId()
  {
    return this.lmUnkId;
  }

  public int toLmSymbolId(int paramInt)
  {
    if (!this.simulateProdlm)
      return this.lmSymbolTbl.addWord(this.ptSymbolTbl.getWord(paramInt));
    String str = this.ptSymbolTbl.getWord(paramInt);
    if (this.lmSymbolTbl.hasWord(str))
      return this.lmSymbolTbl.getId(str);
    return this.lmUnkId;
  }

  public int[] toLmSymbolIds(int[] paramArrayOfInt)
  {
    int[] arrayOfInt = new int[paramArrayOfInt.length];
    for (int i = 0; i < arrayOfInt.length; i++)
      arrayOfInt[i] = toLmSymbolId(paramArrayOfInt[i]);
    return arrayOfInt;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.util.LmSymbolConverter
 * JD-Core Version:    0.6.2
 */