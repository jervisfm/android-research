package com.google.android.apps.translatedecoder.util;

import java.nio.ByteBuffer;
import java.util.logging.Logger;

public class MemMapUtil
{
  private static final Logger logger = Logger.getLogger(MemMapUtil.class.getName());

  public static int mmapFileSignature()
  {
    return 1234504321;
  }

  public static ByteBuffer subBuffer(ByteBuffer paramByteBuffer, int paramInt)
  {
    if (paramInt > paramByteBuffer.remaining())
    {
      logger.severe("not enought remaining bits");
      System.exit(1);
    }
    ByteBuffer localByteBuffer = paramByteBuffer.slice();
    localByteBuffer.limit(paramInt);
    return localByteBuffer;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.util.MemMapUtil
 * JD-Core Version:    0.6.2
 */