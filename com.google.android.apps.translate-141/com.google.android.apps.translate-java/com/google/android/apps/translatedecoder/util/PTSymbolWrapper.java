package com.google.android.apps.translatedecoder.util;

import java.nio.ByteBuffer;

public class PTSymbolWrapper extends SymbolTable
{
  public static final int CLASS_ID = 2;
  private static final long serialVersionUID = 1L;
  private final SymbolTable lmSymbol;
  private final SymbolTable ptSymbol;

  public PTSymbolWrapper(SymbolTable paramSymbolTable1, SymbolTable paramSymbolTable2)
  {
    this.lmSymbol = paramSymbolTable1;
    this.ptSymbol = paramSymbolTable2;
  }

  public static SymbolTable readFromByteBufferHelper(ByteBuffer paramByteBuffer)
  {
    return new PTSymbolWrapper(readSymbolHelper(paramByteBuffer), readSymbolHelper(paramByteBuffer));
  }

  private static SymbolTable readSymbolHelper(ByteBuffer paramByteBuffer)
  {
    int i = paramByteBuffer.getInt();
    int j = paramByteBuffer.position();
    SymbolTable localSymbolTable = SymbolTable.readFromByteBuffer(MemMapUtil.subBuffer(paramByteBuffer, i));
    paramByteBuffer.position(j + i);
    return localSymbolTable;
  }

  private void writeSymbolHelper(SymbolTable paramSymbolTable, ByteBuffer paramByteBuffer)
  {
    ByteBuffer localByteBuffer = ByteBuffer.allocateDirect(100000000);
    paramSymbolTable.writeToByteBuffer(localByteBuffer);
    int i = localByteBuffer.position();
    localByteBuffer.limit(i);
    localByteBuffer.position(0);
    paramByteBuffer.putInt(i);
    paramByteBuffer.put(localByteBuffer);
  }

  public int addWord(String paramString)
  {
    if (this.lmSymbol.hasWord(paramString))
      return this.lmSymbol.getId(paramString);
    return this.ptSymbol.addWord(paramString);
  }

  public String[] allWords()
  {
    throw new UnsupportedOperationException("calling unimplement(ed) function");
  }

  public int getId(String paramString)
  {
    if (this.lmSymbol.hasWord(paramString))
      return this.lmSymbol.getId(paramString);
    return this.ptSymbol.getId(paramString);
  }

  public String getWord(int paramInt)
  {
    if (paramInt <= this.lmSymbol.maximumId())
      return this.lmSymbol.getWord(paramInt);
    return this.ptSymbol.getWord(paramInt);
  }

  public boolean hasWord(String paramString)
  {
    return (this.lmSymbol.hasWord(paramString)) || (this.ptSymbol.hasWord(paramString));
  }

  public int maximumId()
  {
    throw new UnsupportedOperationException("calling unimplement(ed) function");
  }

  public void writeToByteBuffer(ByteBuffer paramByteBuffer)
  {
    paramByteBuffer.putInt(2);
    writeSymbolHelper(this.lmSymbol, paramByteBuffer);
    writeSymbolHelper(this.ptSymbol, paramByteBuffer);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.util.PTSymbolWrapper
 * JD-Core Version:    0.6.2
 */