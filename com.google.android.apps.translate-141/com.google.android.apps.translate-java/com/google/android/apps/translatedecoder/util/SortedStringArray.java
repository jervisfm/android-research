package com.google.android.apps.translatedecoder.util;

import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.IntBuffer;
import java.util.Arrays;
import java.util.logging.Logger;

public class SortedStringArray
  implements Serializable
{
  private static final Logger logger = Logger.getLogger(SortedStringArray.class.getCanonicalName());
  private static final long serialVersionUID = 6315231234187901918L;
  private final CharBuffer chars;
  private final int offSet;
  private final int size;
  private final IntBuffer startPoss;

  public SortedStringArray(int paramInt1, IntBuffer paramIntBuffer, CharBuffer paramCharBuffer, int paramInt2)
  {
    this.size = paramInt1;
    this.startPoss = paramIntBuffer;
    this.chars = paramCharBuffer;
    this.offSet = paramInt2;
  }

  public SortedStringArray(int paramInt1, int[] paramArrayOfInt, char[] paramArrayOfChar, int paramInt2)
  {
    this(paramInt1, IntBuffer.wrap(paramArrayOfInt), CharBuffer.wrap(paramArrayOfChar), paramInt2);
  }

  public SortedStringArray(String[] paramArrayOfString, boolean paramBoolean)
  {
    this(paramArrayOfString, paramBoolean, 0);
  }

  public SortedStringArray(String[] paramArrayOfString, boolean paramBoolean, int paramInt)
  {
    if (!paramBoolean)
      Arrays.sort(paramArrayOfString);
    this.offSet = paramInt;
    int i = 0;
    for (int j = 0; j < paramArrayOfString.length; j++)
      i += paramArrayOfString[j].length();
    this.size = paramArrayOfString.length;
    this.startPoss = IntBuffer.wrap(new int[this.size]);
    this.chars = CharBuffer.wrap(new char[i]);
    int k = 0;
    for (int m = 0; m < this.size; m++)
    {
      this.startPoss.put(m, k);
      String str = paramArrayOfString[m];
      int n = 0;
      while (n < str.length())
      {
        CharBuffer localCharBuffer = this.chars;
        int i1 = k + 1;
        localCharBuffer.put(k, str.charAt(n));
        n++;
        k = i1;
      }
    }
  }

  private int binarySearch(String paramString)
  {
    int i = 0;
    int j = this.size;
    while (i < j)
    {
      int k = i + (j - i) / 2;
      if (paramString.compareTo(getWord(k + this.offSet)) > 0)
        i = k + 1;
      else
        j = k;
    }
    if ((i < this.size) && (getWord(i + this.offSet).compareTo(paramString) == 0))
      return i;
    return -1;
  }

  public static SortedStringArray readFromByteBuffer(ByteBuffer paramByteBuffer)
  {
    int i = paramByteBuffer.getInt();
    logger.info("Read from buffer, size=" + i);
    int j = paramByteBuffer.getInt();
    logger.info("Position offSet is " + j);
    int k = paramByteBuffer.getInt();
    IntBuffer localIntBuffer = MemMapUtil.subBuffer(paramByteBuffer, k).asIntBuffer();
    paramByteBuffer.position(k + paramByteBuffer.position());
    int m = paramByteBuffer.getInt();
    CharBuffer localCharBuffer = MemMapUtil.subBuffer(paramByteBuffer, m).asCharBuffer();
    paramByteBuffer.position(m + paramByteBuffer.position());
    return new SortedStringArray(i, localIntBuffer, localCharBuffer, j);
  }

  public boolean contains(String paramString)
  {
    return getPos(paramString) != -1;
  }

  public String[] getAllWords()
  {
    String[] arrayOfString = new String[this.size];
    for (int i = 0; i < this.size; i++)
      arrayOfString[i] = getWord(i + this.offSet);
    return arrayOfString;
  }

  public int getPos(String paramString)
  {
    int i = binarySearch(paramString);
    if (i < 0)
      return i;
    return i + this.offSet;
  }

  public String getWord(int paramInt)
  {
    int i = paramInt - this.offSet;
    if ((i < 0) || (i >= this.size))
      return null;
    int j = this.startPoss.get(i);
    int k = this.chars.limit();
    if (i < -1 + this.size)
      k = this.startPoss.get(i + 1);
    int m = k - j;
    char[] arrayOfChar = new char[m];
    for (int n = 0; n < m; n++)
      arrayOfChar[n] = this.chars.get(j + n);
    return new String(arrayOfChar);
  }

  public int maxPos()
  {
    return -1 + (this.size + this.offSet);
  }

  public int size()
  {
    return this.size;
  }

  public void writeToByteBuffer(ByteBuffer paramByteBuffer)
  {
    logger.info("write to buffer, size=" + this.size);
    paramByteBuffer.putInt(this.size);
    logger.info("Position offSet is " + this.offSet);
    paramByteBuffer.putInt(this.offSet);
    paramByteBuffer.putInt(32 * this.startPoss.limit() / 8);
    for (int i = 0; i < this.startPoss.limit(); i++)
      paramByteBuffer.putInt(this.startPoss.get(i));
    paramByteBuffer.putInt(16 * this.chars.limit() / 8);
    for (int j = 0; j < this.chars.limit(); j++)
      paramByteBuffer.putChar(this.chars.get(j));
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.util.SortedStringArray
 * JD-Core Version:    0.6.2
 */