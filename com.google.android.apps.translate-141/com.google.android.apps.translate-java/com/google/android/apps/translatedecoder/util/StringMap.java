package com.google.android.apps.translatedecoder.util;

import java.io.Serializable;
import java.nio.ByteBuffer;
import java.util.logging.Logger;

public abstract class StringMap
  implements Serializable
{
  private static final Logger logger = Logger.getLogger(StringMap.class.getCanonicalName());
  private static final long serialVersionUID = 743774365586857781L;

  public static StringMap readFromByteBuffer(ByteBuffer paramByteBuffer)
  {
    int i = paramByteBuffer.getInt();
    if (i == 1)
      return ArrayBasedStringMap.readFromByteBufferHelper(paramByteBuffer);
    logger.severe("unknown class id" + i);
    System.exit(1);
    return null;
  }

  public abstract String get(String paramString);

  public abstract void writeToByteBuffer(ByteBuffer paramByteBuffer);
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.util.StringMap
 * JD-Core Version:    0.6.2
 */