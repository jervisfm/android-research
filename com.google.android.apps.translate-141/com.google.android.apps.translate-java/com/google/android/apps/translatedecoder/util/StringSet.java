package com.google.android.apps.translatedecoder.util;

import java.io.Serializable;
import java.nio.ByteBuffer;
import java.util.logging.Logger;

public abstract class StringSet
  implements Serializable
{
  private static final Logger logger = Logger.getLogger(StringSet.class.getCanonicalName());
  private static final long serialVersionUID = 3871945104960723530L;

  public static StringSet readFromByteBuffer(ByteBuffer paramByteBuffer)
  {
    int i = paramByteBuffer.getInt();
    if (i == 1)
      return ArrayBasedStringSet.readFromByteBufferHelper(paramByteBuffer);
    logger.severe("unknown class id" + i);
    System.exit(1);
    return null;
  }

  public abstract boolean contains(String paramString);

  public abstract void writeToByteBuffer(ByteBuffer paramByteBuffer);
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.util.StringSet
 * JD-Core Version:    0.6.2
 */