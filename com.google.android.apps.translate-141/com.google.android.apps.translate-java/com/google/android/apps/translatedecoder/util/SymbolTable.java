package com.google.android.apps.translatedecoder.util;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.RandomAccessFile;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileChannel.MapMode;
import java.util.List;
import java.util.logging.Logger;

public abstract class SymbolTable
  implements Serializable
{
  public static final int MAX_MMAP_SIZE = 100000000;
  private static final Logger logger = Logger.getLogger(SymbolTable.class.getName());
  private static final long serialVersionUID = 3479233830563222053L;

  public static SymbolTable readFromByteBuffer(ByteBuffer paramByteBuffer)
  {
    int i = paramByteBuffer.getInt();
    if (i == 1)
      return ArrayBasedSymbol.readFromByteBufferHelper(paramByteBuffer);
    if (i == 2)
      return PTSymbolWrapper.readFromByteBufferHelper(paramByteBuffer);
    throw new RuntimeException("unknown class id " + i);
  }

  public static SymbolTable readFromFile(String paramString)
  {
    try
    {
      FileChannel localFileChannel = new RandomAccessFile(paramString, "r").getChannel();
      MappedByteBuffer localMappedByteBuffer = localFileChannel.map(FileChannel.MapMode.READ_ONLY, 0L, (int)localFileChannel.size());
      if (localMappedByteBuffer.getInt() == MemMapUtil.mmapFileSignature())
      {
        SymbolTable localSymbolTable2 = readFromByteBuffer(localMappedByteBuffer);
        localFileChannel.close();
        return localSymbolTable2;
      }
      ObjectInputStream localObjectInputStream = new ObjectInputStream(new FileInputStream(paramString));
      SymbolTable localSymbolTable1 = (SymbolTable)localObjectInputStream.readObject();
      localObjectInputStream.close();
      localFileChannel.close();
      return localSymbolTable1;
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
    }
    return null;
  }

  public abstract int addWord(String paramString);

  public final int[] addWords(String paramString)
  {
    return addWords(paramString.trim().split("\\s+"));
  }

  public final int[] addWords(String[] paramArrayOfString)
  {
    int[] arrayOfInt = new int[paramArrayOfString.length];
    for (int i = 0; i < paramArrayOfString.length; i++)
      arrayOfInt[i] = addWord(paramArrayOfString[i]);
    return arrayOfInt;
  }

  public abstract String[] allWords();

  public abstract int getId(String paramString);

  public final int[] getIds(String paramString)
  {
    String[] arrayOfString = paramString.trim().split("\\s+");
    int[] arrayOfInt = new int[arrayOfString.length];
    for (int i = 0; i < arrayOfString.length; i++)
      arrayOfInt[i] = getId(arrayOfString[i]);
    return arrayOfInt;
  }

  public abstract String getWord(int paramInt);

  public final String getWords(List<Integer> paramList)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    for (int i = 0; i < paramList.size(); i++)
    {
      localStringBuilder.append(getWord(((Integer)paramList.get(i)).intValue()));
      if (i < -1 + paramList.size())
        localStringBuilder.append(" ");
    }
    return localStringBuilder.toString();
  }

  public final String getWords(int[] paramArrayOfInt)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    for (int i = 0; i < paramArrayOfInt.length; i++)
    {
      localStringBuilder.append(getWord(paramArrayOfInt[i]));
      if (i < -1 + paramArrayOfInt.length)
        localStringBuilder.append(" ");
    }
    return localStringBuilder.toString();
  }

  public abstract boolean hasWord(String paramString);

  public abstract int maximumId();

  public abstract void writeToByteBuffer(ByteBuffer paramByteBuffer);

  public void writeToFile(String paramString)
  {
    writeToFile(paramString, false);
  }

  public void writeToFile(String paramString, boolean paramBoolean)
  {
    if (paramBoolean);
    try
    {
      RandomAccessFile localRandomAccessFile = new RandomAccessFile(paramString, "rw");
      FileChannel localFileChannel = localRandomAccessFile.getChannel();
      MappedByteBuffer localMappedByteBuffer = localFileChannel.map(FileChannel.MapMode.READ_WRITE, 0L, 100000000L);
      localMappedByteBuffer.putInt(MemMapUtil.mmapFileSignature());
      writeToByteBuffer(localMappedByteBuffer);
      logger.info("Final buffer size is " + localMappedByteBuffer.position());
      localFileChannel.truncate(localMappedByteBuffer.position());
      localRandomAccessFile.close();
      return;
      ObjectOutputStream localObjectOutputStream = new ObjectOutputStream(new FileOutputStream(paramString));
      localObjectOutputStream.writeObject(this);
      localObjectOutputStream.close();
      return;
    }
    catch (IOException localIOException)
    {
      localIOException.printStackTrace();
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.util.SymbolTable
 * JD-Core Version:    0.6.2
 */