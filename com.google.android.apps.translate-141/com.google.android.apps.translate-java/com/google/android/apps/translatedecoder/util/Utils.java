package com.google.android.apps.translatedecoder.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.GZIPInputStream;

public class Utils
{
  public static final InputStream getInputStream(String paramString)
    throws IOException
  {
    Object localObject = new FileInputStream(paramString);
    if (paramString.endsWith(".gz"))
      localObject = new GZIPInputStream((InputStream)localObject);
    return localObject;
  }

  public static List<Integer> intArrayToList(int[] paramArrayOfInt)
  {
    ArrayList localArrayList = new ArrayList();
    for (int i = 0; i < paramArrayOfInt.length; i++)
      localArrayList.add(Integer.valueOf(paramArrayOfInt[i]));
    return localArrayList;
  }

  public static int[] listToArray(List<Integer> paramList)
  {
    int[] arrayOfInt = new int[paramList.size()];
    for (int i = 0; i < paramList.size(); i++)
      arrayOfInt[i] = ((Integer)paramList.get(i)).intValue();
    return arrayOfInt;
  }

  public static String listToString(List<Integer> paramList)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    for (int i = 0; i < paramList.size(); i++)
    {
      localStringBuilder.append(paramList.get(i));
      if (i < -1 + paramList.size())
        localStringBuilder.append(" ");
    }
    return localStringBuilder.toString();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.translatedecoder.util.Utils
 * JD-Core Version:    0.6.2
 */