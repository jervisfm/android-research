package com.google.android.apps.unveil.env;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Rect;

public class BorderedText
{
  private final Paint exteriorPaint;
  private final Paint interiorPaint = new Paint();
  private float[] positions;
  private final float textSize;
  private float[] widths;

  public BorderedText(float paramFloat)
  {
    this(-1, -16777216, paramFloat);
  }

  public BorderedText(int paramInt1, int paramInt2, float paramFloat)
  {
    this.interiorPaint.setTextSize(paramFloat);
    this.interiorPaint.setColor(paramInt1);
    this.interiorPaint.setStyle(Paint.Style.FILL);
    this.interiorPaint.setAntiAlias(false);
    this.interiorPaint.setAlpha(255);
    this.exteriorPaint = new Paint();
    this.exteriorPaint.setTextSize(paramFloat);
    this.exteriorPaint.setColor(paramInt2);
    this.exteriorPaint.setStyle(Paint.Style.FILL);
    this.exteriorPaint.setAntiAlias(false);
    this.exteriorPaint.setAlpha(255);
    this.exteriorPaint.setFakeBoldText(true);
    this.textSize = paramFloat;
  }

  public void drawText(Canvas paramCanvas, float paramFloat1, float paramFloat2, String paramString)
  {
    if ((this.widths == null) || (this.widths.length < paramString.length()))
    {
      this.widths = new float[paramString.length()];
      this.positions = new float[2 * paramString.length()];
    }
    this.exteriorPaint.getTextWidths(paramString, this.widths);
    float f = paramFloat1;
    for (int i = 0; i < this.widths.length; i++)
    {
      this.positions[(i * 2)] = f;
      this.positions[(1 + i * 2)] = paramFloat2;
      f += this.widths[i];
    }
    paramCanvas.drawPosText(paramString, this.positions, this.exteriorPaint);
    paramCanvas.drawPosText(paramString, this.positions, this.interiorPaint);
  }

  public void getTextBounds(String paramString, int paramInt1, int paramInt2, Rect paramRect)
  {
    this.interiorPaint.getTextBounds(paramString, paramInt1, paramInt2, paramRect);
  }

  public float getTextSize()
  {
    return this.textSize;
  }

  public void setAlpha(int paramInt)
  {
    this.interiorPaint.setAlpha(paramInt);
    this.exteriorPaint.setAlpha(paramInt);
  }

  public void setExteriorColor(int paramInt)
  {
    this.exteriorPaint.setColor(paramInt);
  }

  public void setInteriorColor(int paramInt)
  {
    this.interiorPaint.setColor(paramInt);
  }

  public void setTextAlign(Paint.Align paramAlign)
  {
    this.interiorPaint.setTextAlign(paramAlign);
    this.exteriorPaint.setTextAlign(paramAlign);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.unveil.env.BorderedText
 * JD-Core Version:    0.6.2
 */