package com.google.android.apps.unveil.env;

import android.os.SystemClock;

public abstract class Clock
{
  public static final Clock SYSTEM_CLOCK = new SystemClock(null);

  public abstract long now();

  private static class SystemClock extends Clock
  {
    public long now()
    {
      return SystemClock.uptimeMillis();
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.unveil.env.Clock
 * JD-Core Version:    0.6.2
 */