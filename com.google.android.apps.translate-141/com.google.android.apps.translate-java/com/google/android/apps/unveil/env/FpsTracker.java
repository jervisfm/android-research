package com.google.android.apps.unveil.env;

import android.os.SystemClock;
import android.text.TextUtils;

public class FpsTracker
{
  public static final int DEFAULT_CIRCULAR_QUEUE_SIZE = 40;
  private static final int MAX_SUPPORTED_FPS = 40;
  private final int[] circularQueue;
  private int currIndex = 0;
  final int[] fpsBuckets = new int[41];
  private String fpsString = null;
  private long lastFrameTimestamp = 0L;
  private int numFrames = 0;
  private int totalTime = 0;

  public FpsTracker()
  {
    this(40);
  }

  public FpsTracker(int paramInt)
  {
    this.circularQueue = new int[paramInt];
  }

  private void handleDelta(int paramInt)
  {
    if (paramInt <= 0)
      return;
    if (this.numFrames > this.circularQueue.length)
    {
      int j = this.circularQueue[this.currIndex];
      int k = 1000 / j;
      if (k <= 40)
      {
        int[] arrayOfInt4 = this.fpsBuckets;
        arrayOfInt4[k] = (-1 + arrayOfInt4[k]);
        this.totalTime -= j;
      }
    }
    else
    {
      this.circularQueue[this.currIndex] = paramInt;
      int i = 1000 / paramInt;
      if (i > 40)
        break label147;
      int[] arrayOfInt2 = this.fpsBuckets;
      arrayOfInt2[i] = (1 + arrayOfInt2[i]);
    }
    while (true)
    {
      this.totalTime = (paramInt + this.totalTime);
      this.fpsString = null;
      return;
      int[] arrayOfInt3 = this.fpsBuckets;
      arrayOfInt3[40] = (-1 + arrayOfInt3[40]);
      break;
      label147: int[] arrayOfInt1 = this.fpsBuckets;
      arrayOfInt1[40] = (1 + arrayOfInt1[40]);
    }
  }

  public String getFpsString()
  {
    if (this.fpsString != null)
      return this.fpsString;
    int i = Math.min(this.numFrames, this.circularQueue.length);
    if (i == 0)
      return "";
    Integer[] arrayOfInteger = new Integer[5];
    for (int j = 0; j < arrayOfInteger.length; j++)
      arrayOfInteger[j] = Integer.valueOf(-1);
    int k = 0;
    int m = 0;
    int n = 0;
    if (n <= 40)
    {
      if (this.fpsBuckets[n] == 0);
      while (true)
      {
        n++;
        break;
        k += this.fpsBuckets[n];
        if (k >= m * i / 4)
        {
          arrayOfInteger[m] = Integer.valueOf(n);
          m++;
        }
      }
    }
    float f = 1000.0F / (this.totalTime / i);
    this.fpsString = (NumberUtils.format(f, 2) + "  [" + TextUtils.join(",", arrayOfInteger) + "] " + this.currIndex + "/" + this.circularQueue.length);
    return this.fpsString;
  }

  public boolean tick()
  {
    return tick(SystemClock.uptimeMillis());
  }

  public boolean tick(long paramLong)
  {
    this.currIndex = ((-1 + this.numFrames) % this.circularQueue.length);
    if (this.numFrames > 0)
      handleDelta((int)(paramLong - this.lastFrameTimestamp));
    this.lastFrameTimestamp = paramLong;
    this.numFrames = (1 + this.numFrames);
    return this.currIndex == 0;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.unveil.env.FpsTracker
 * JD-Core Version:    0.6.2
 */