package com.google.android.apps.unveil.env;

public final class PictureLoadingException extends Exception
{
  public PictureLoadingException(String paramString)
  {
    super(paramString);
  }

  public PictureLoadingException(String paramString, Exception paramException)
  {
    super(paramString, paramException);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.unveil.env.PictureLoadingException
 * JD-Core Version:    0.6.2
 */