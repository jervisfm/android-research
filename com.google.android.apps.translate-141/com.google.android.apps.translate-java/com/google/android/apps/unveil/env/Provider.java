package com.google.android.apps.unveil.env;

public abstract interface Provider<T>
{
  public abstract T get();
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.unveil.env.Provider
 * JD-Core Version:    0.6.2
 */