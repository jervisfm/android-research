package com.google.android.apps.unveil.env;

public class Providers
{
  public static <T> Provider<T> staticProvider(T paramT)
  {
    return new Provider()
    {
      public T get()
      {
        return this.val$value;
      }
    };
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.unveil.env.Providers
 * JD-Core Version:    0.6.2
 */