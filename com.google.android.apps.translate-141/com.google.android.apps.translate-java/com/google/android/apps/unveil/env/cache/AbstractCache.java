package com.google.android.apps.unveil.env.cache;

public abstract class AbstractCache<Key, Value>
{
  public abstract Value get(Key paramKey);

  public abstract void put(Key paramKey, Value paramValue);
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.unveil.env.cache.AbstractCache
 * JD-Core Version:    0.6.2
 */