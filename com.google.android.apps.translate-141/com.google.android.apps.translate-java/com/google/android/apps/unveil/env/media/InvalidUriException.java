package com.google.android.apps.unveil.env.media;

public class InvalidUriException extends Exception
{
  public InvalidUriException(Exception paramException)
  {
    super(paramException);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.unveil.env.media.InvalidUriException
 * JD-Core Version:    0.6.2
 */