package com.google.android.apps.unveil.hardware;

import android.content.Context;
import android.media.AudioManager;

public class AudioManagerProxy
{
  private AudioManager audioManager;

  public AudioManagerProxy(Context paramContext)
  {
    if (paramContext == null)
      return;
    this.audioManager = ((AudioManager)paramContext.getSystemService("audio"));
  }

  public void playSoundEffect(int paramInt)
  {
    this.audioManager.playSoundEffect(paramInt);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.unveil.hardware.AudioManagerProxy
 * JD-Core Version:    0.6.2
 */