package com.google.android.apps.unveil.history;

public final class NoResultsItem extends ItemModel
{
  final int messageId;

  NoResultsItem(int paramInt)
  {
    this.messageId = paramInt;
  }

  public int getMessageId()
  {
    return this.messageId;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.unveil.history.NoResultsItem
 * JD-Core Version:    0.6.2
 */