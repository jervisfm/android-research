package com.google.android.apps.unveil.history;

public abstract interface SearchListener
{
  public abstract void onSearchCompleted(SearchHistoryQuery.QuerySpec paramQuerySpec, int paramInt);

  public abstract void onSearchResultsNoResults(int paramInt);
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.unveil.history.SearchListener
 * JD-Core Version:    0.6.2
 */