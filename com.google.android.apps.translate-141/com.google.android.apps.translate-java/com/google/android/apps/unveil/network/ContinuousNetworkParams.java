package com.google.android.apps.unveil.network;

import com.google.goggles.GogglesProtos.GogglesRequest.Source;

public class ContinuousNetworkParams
{
  public long interPushDelayMs;
  public GogglesProtos.GogglesRequest.Source source;
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.unveil.network.ContinuousNetworkParams
 * JD-Core Version:    0.6.2
 */