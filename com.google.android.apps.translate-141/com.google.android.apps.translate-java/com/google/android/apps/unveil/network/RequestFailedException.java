package com.google.android.apps.unveil.network;

public class RequestFailedException extends Exception
{
  public RequestFailedException(Exception paramException)
  {
    super(paramException);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.unveil.network.RequestFailedException
 * JD-Core Version:    0.6.2
 */