package com.google.android.apps.unveil.nonstop;

import com.google.android.apps.unveil.env.FpsTracker;
import com.google.android.apps.unveil.env.UnveilLogger;
import com.google.android.apps.unveil.sensors.CameraManager;
import com.google.android.apps.unveil.sensors.UnveilSensor;
import com.google.android.apps.unveil.sensors.UnveilSensor.Listener;
import com.google.android.apps.unveil.sensors.UnveilSensorProvider;
import com.google.android.apps.unveil.ui.GlOverlay;
import java.util.Vector;

public class StatusFrameProcessor extends FrameProcessor
{
  private static final UnveilLogger logger = new UnveilLogger();
  private final CameraManager cameraManager;
  private final Vector<String> debugText = new Vector();
  private final DebugView debugView;
  private FpsTracker fpsTracker = new FpsTracker();
  private final GlOverlay glDebugView;
  private UnveilSensor.Listener listener;
  private final UnveilSensorProvider sensorProvider;
  private float[] sensorValues;

  public StatusFrameProcessor(CameraManager paramCameraManager, UnveilSensorProvider paramUnveilSensorProvider, DebugView paramDebugView, GlOverlay paramGlOverlay)
  {
    this.cameraManager = paramCameraManager;
    this.sensorProvider = paramUnveilSensorProvider;
    this.debugView = paramDebugView;
    this.glDebugView = paramGlOverlay;
  }

  public Vector<String> getDebugText()
  {
    try
    {
      this.debugText.clear();
      this.debugText.add(this.fpsTracker.getFpsString());
      this.debugText.add("Screen: " + getViewSize() + ", Preview: " + getPreviewFrameSize());
      this.debugText.add("Camera state: " + this.cameraManager.getStateName());
      if (this.sensorValues != null)
        this.debugText.add("azimuth: " + this.sensorValues[0] + ", pitch: " + this.sensorValues[1] + ", roll: " + this.sensorValues[2]);
      Vector localVector = this.debugText;
      return localVector;
    }
    finally
    {
    }
  }

  protected void onPause()
  {
    if (this.sensorProvider != null)
      this.sensorProvider.getOrientationSensor().unregisterListener(this.listener);
  }

  protected void onProcessFrame(TimestampedFrame paramTimestampedFrame)
  {
    try
    {
      if ((this.fpsTracker.tick(paramTimestampedFrame.getTimestamp())) && (logger.shouldShowVerbose()))
        logger.v(this.fpsTracker.getFpsString(), new Object[0]);
      if (this.debugView.getVisibility() == 0)
      {
        this.debugView.invalidate();
        if (this.glDebugView != null)
          this.glDebugView.requestRender();
      }
      return;
    }
    finally
    {
    }
  }

  protected void onStart()
  {
    if (this.sensorProvider != null)
    {
      this.listener = new UnveilSensor.Listener()
      {
        public void onSet(UnveilSensor paramAnonymousUnveilSensor)
        {
          StatusFrameProcessor.access$002(StatusFrameProcessor.this, paramAnonymousUnveilSensor.getValues());
        }
      };
      this.sensorProvider.getOrientationSensor().registerListener(this.listener);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.unveil.nonstop.StatusFrameProcessor
 * JD-Core Version:    0.6.2
 */