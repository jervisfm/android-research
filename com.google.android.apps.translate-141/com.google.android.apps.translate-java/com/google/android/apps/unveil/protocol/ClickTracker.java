package com.google.android.apps.unveil.protocol;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.content.res.Resources;
import android.text.TextUtils;
import android.view.View;
import com.google.android.apps.unveil.UnveilContext;
import com.google.android.apps.unveil.env.UnveilLogger;
import com.google.android.apps.unveil.network.AbstractConnector;
import com.google.android.apps.unveil.network.AbstractConnector.ResponseHandler;
import com.google.android.apps.unveil.network.UnveilResponse;
import com.google.android.apps.unveil.results.ResultItem;
import com.google.goggles.AnnotationResultProtos.AnnotationResult;
import com.google.goggles.NativeClientLoggingProtos.NativeClientClick;
import com.google.goggles.NativeClientLoggingProtos.NativeClientClick.Builder;
import com.google.goggles.NativeClientLoggingProtos.NativeClientClick.CLICK_TARGET;
import com.google.goggles.NativeClientLoggingProtos.NativeClientInstall;
import com.google.goggles.NativeClientLoggingProtos.NativeClientInstall.Builder;
import com.google.goggles.NativeClientLoggingProtos.NativeClientLogEventRequest;
import com.google.goggles.NativeClientLoggingProtos.NativeClientLogEventRequest.Builder;
import com.google.goggles.NativeClientLoggingProtos.NativeClientLogEventResponse;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class ClickTracker
{
  public static final int DEFAULT_MAX_PARALLEL_REQUESTS = 2;
  private static final Set<NativeClientLoggingProtos.NativeClientClick.CLICK_TARGET> TRACKING_ID_REQUIRED_CLICKS = EnumSet.of(localCLICK_TARGET, arrayOfCLICK_TARGET);
  private static final UnveilLogger logger = new UnveilLogger();
  private final SharedPreferences.OnSharedPreferenceChangeListener clickTrackingPreferenceChangeListener;
  private final ClickLogConnector connector;
  private final ArrayList<NativeClientLoggingProtos.NativeClientLogEventRequest.Builder> logRequestsWithoutTrackingIds;
  private final AbstractConnector.ResponseHandler<NativeClientLoggingProtos.NativeClientLogEventResponse> responseHandler = new AbstractConnector.ResponseHandler()
  {
    public void onNetworkError()
    {
      ClickTracker.logger.e("Failed to send logs.", new Object[0]);
    }

    public void onResponse(UnveilResponse<NativeClientLoggingProtos.NativeClientLogEventResponse> paramAnonymousUnveilResponse)
    {
      ClickTracker.logger.i("Succesffully sent click log.", new Object[0]);
    }

    public void onServerErrorCode(int paramAnonymousInt)
    {
      UnveilLogger localUnveilLogger = ClickTracker.logger;
      Object[] arrayOfObject = new Object[1];
      arrayOfObject[0] = Integer.valueOf(paramAnonymousInt);
      localUnveilLogger.e("Server responded %d to log request.", arrayOfObject);
    }
  };
  private String sessionId;
  private String trackingId;

  static
  {
    NativeClientLoggingProtos.NativeClientClick.CLICK_TARGET localCLICK_TARGET = NativeClientLoggingProtos.NativeClientClick.CLICK_TARGET.RESULT_CLICK_IN_RESULTS_LIST;
    NativeClientLoggingProtos.NativeClientClick.CLICK_TARGET[] arrayOfCLICK_TARGET = new NativeClientLoggingProtos.NativeClientClick.CLICK_TARGET[38];
    arrayOfCLICK_TARGET[0] = NativeClientLoggingProtos.NativeClientClick.CLICK_TARGET.BOUNDING_BOX_CLICK_ON_RESULTS_PAGE;
    arrayOfCLICK_TARGET[1] = NativeClientLoggingProtos.NativeClientClick.CLICK_TARGET.RESULT_CLICK_VIA_SCROLL_BALL_ON_RESULTS_PAGE;
    arrayOfCLICK_TARGET[2] = NativeClientLoggingProtos.NativeClientClick.CLICK_TARGET.BACK_BUTTON_FROM_RESULTS_LIST_PAGE;
    arrayOfCLICK_TARGET[3] = NativeClientLoggingProtos.NativeClientClick.CLICK_TARGET.BACK_BUTTON_FROM_RESULT_PAGE;
    arrayOfCLICK_TARGET[4] = NativeClientLoggingProtos.NativeClientClick.CLICK_TARGET.NEW_SEARCH_BUTTON_FROM_RESULT_PAGE;
    arrayOfCLICK_TARGET[5] = NativeClientLoggingProtos.NativeClientClick.CLICK_TARGET.EYECANDY_RESULT_CLICK;
    arrayOfCLICK_TARGET[6] = NativeClientLoggingProtos.NativeClientClick.CLICK_TARGET.RATINGS_OPENED_ON_RESULTS_LIST_PAGE;
    arrayOfCLICK_TARGET[7] = NativeClientLoggingProtos.NativeClientClick.CLICK_TARGET.RATINGS_CLOSED_ON_RESULTS_LIST_PAGE;
    arrayOfCLICK_TARGET[8] = NativeClientLoggingProtos.NativeClientClick.CLICK_TARGET.TAP_OUTSIDE_BOUNDING_BOX_HIDE_RESULTS_LIST;
    arrayOfCLICK_TARGET[9] = NativeClientLoggingProtos.NativeClientClick.CLICK_TARGET.TAP_OUTSIDE_BOUNDING_BOX_SHOW_RESULTS_LIST;
    arrayOfCLICK_TARGET[10] = NativeClientLoggingProtos.NativeClientClick.CLICK_TARGET.RESULT_HTML_CACHED;
    arrayOfCLICK_TARGET[11] = NativeClientLoggingProtos.NativeClientClick.CLICK_TARGET.DRAWER_OPENED;
    arrayOfCLICK_TARGET[12] = NativeClientLoggingProtos.NativeClientClick.CLICK_TARGET.DRAWER_SEMI;
    arrayOfCLICK_TARGET[13] = NativeClientLoggingProtos.NativeClientClick.CLICK_TARGET.DRAWER_CLOSED;
    arrayOfCLICK_TARGET[14] = NativeClientLoggingProtos.NativeClientClick.CLICK_TARGET.SHOW_FULL_SIMILAR_IMAGE;
    arrayOfCLICK_TARGET[15] = NativeClientLoggingProtos.NativeClientClick.CLICK_TARGET.PUGGLE_CLICK_LIST_VIEW;
    arrayOfCLICK_TARGET[16] = NativeClientLoggingProtos.NativeClientClick.CLICK_TARGET.PUGGLE_CLICK_GRID_VIEW;
    arrayOfCLICK_TARGET[17] = NativeClientLoggingProtos.NativeClientClick.CLICK_TARGET.PUGGLE_REFINE_QUERY_SPEECH;
    arrayOfCLICK_TARGET[18] = NativeClientLoggingProtos.NativeClientClick.CLICK_TARGET.PUGGLE_REFINE_QUERY_TYPE;
    arrayOfCLICK_TARGET[19] = NativeClientLoggingProtos.NativeClientClick.CLICK_TARGET.PUGGLE_RESULT_CLICK_IN_RESULT_LIST;
    arrayOfCLICK_TARGET[20] = NativeClientLoggingProtos.NativeClientClick.CLICK_TARGET.PUGGLE_EXTERNAL_CLICK_IN_RESULT_VIEW;
    arrayOfCLICK_TARGET[21] = NativeClientLoggingProtos.NativeClientClick.CLICK_TARGET.BACK_BUTTON_FROM_PUGGLE_RESULT;
    arrayOfCLICK_TARGET[22] = NativeClientLoggingProtos.NativeClientClick.CLICK_TARGET.SUGGEST_A_RESULT_BUTTON_CLICK;
    arrayOfCLICK_TARGET[23] = NativeClientLoggingProtos.NativeClientClick.CLICK_TARGET.TAG_IMAGE_CONTINUE;
    arrayOfCLICK_TARGET[24] = NativeClientLoggingProtos.NativeClientClick.CLICK_TARGET.TAG_IMAGE_CANCEL;
    arrayOfCLICK_TARGET[25] = NativeClientLoggingProtos.NativeClientClick.CLICK_TARGET.TAG_IMAGE_BACK;
    arrayOfCLICK_TARGET[26] = NativeClientLoggingProtos.NativeClientClick.CLICK_TARGET.DESCRIBE_SUBMIT;
    arrayOfCLICK_TARGET[27] = NativeClientLoggingProtos.NativeClientClick.CLICK_TARGET.DESCRIBE_CANCEL;
    arrayOfCLICK_TARGET[28] = NativeClientLoggingProtos.NativeClientClick.CLICK_TARGET.DESCRIBE_BACK;
    arrayOfCLICK_TARGET[29] = NativeClientLoggingProtos.NativeClientClick.CLICK_TARGET.CONTINUOUS_TIMELINE_THUMBNAIL_TAP;
    arrayOfCLICK_TARGET[30] = NativeClientLoggingProtos.NativeClientClick.CLICK_TARGET.CONTINUOUS_TIMELINE_SWIPE;
    arrayOfCLICK_TARGET[31] = NativeClientLoggingProtos.NativeClientClick.CLICK_TARGET.CONTINUOUS_VIEW_ALL_RESULTS;
    arrayOfCLICK_TARGET[32] = NativeClientLoggingProtos.NativeClientClick.CLICK_TARGET.CONTINUOUS_ALL_RESULTS_RESULT_ITEM_TAP;
    arrayOfCLICK_TARGET[33] = NativeClientLoggingProtos.NativeClientClick.CLICK_TARGET.RESULT_SHOW_EXPANDED_RESULT;
    arrayOfCLICK_TARGET[34] = NativeClientLoggingProtos.NativeClientClick.CLICK_TARGET.RESULT_DISMBIGUATION_ACTION;
    arrayOfCLICK_TARGET[35] = NativeClientLoggingProtos.NativeClientClick.CLICK_TARGET.RESULT_EXPANDED_RESULT_ACTION;
    arrayOfCLICK_TARGET[36] = NativeClientLoggingProtos.NativeClientClick.CLICK_TARGET.CONTINUOUS_CLICK_TO_PAUSE;
    arrayOfCLICK_TARGET[37] = NativeClientLoggingProtos.NativeClientClick.CLICK_TARGET.CONTINUOUS_AUTOMATED_PAUSE;
  }

  public ClickTracker(SharedPreferences paramSharedPreferences, Resources paramResources, ClickLogConnector paramClickLogConnector)
  {
    this.connector = paramClickLogConnector;
    this.logRequestsWithoutTrackingIds = new ArrayList();
    this.clickTrackingPreferenceChangeListener = new ClickTrackingPreferenceChangeListener(this, paramResources);
    paramSharedPreferences.registerOnSharedPreferenceChangeListener(this.clickTrackingPreferenceChangeListener);
  }

  private void addToRequests(NativeClientLoggingProtos.NativeClientLogEventRequest.Builder paramBuilder)
  {
    if ((paramBuilder.hasClientClick()) && (TRACKING_ID_REQUIRED_CLICKS.contains(paramBuilder.getClientClick().getClickTarget())))
    {
      maybeSendRequestWithTrackingId(paramBuilder);
      return;
    }
    sendRequest(paramBuilder);
  }

  private static ClickTracker get(Context paramContext)
  {
    return ((UnveilContext)paramContext.getApplicationContext()).getClickTracker();
  }

  public static void logActionClick(View paramView, NativeClientLoggingProtos.NativeClientClick.CLICK_TARGET paramCLICK_TARGET, ResultItem paramResultItem, int paramInt)
  {
    get(paramView.getContext()).logActionClick(paramCLICK_TARGET, paramResultItem, paramInt);
  }

  private void logActionClick(NativeClientLoggingProtos.NativeClientClick.CLICK_TARGET paramCLICK_TARGET, ResultItem paramResultItem, int paramInt)
  {
    addToRequests(newRequestBuilder().setClientClick(NativeClientLoggingProtos.NativeClientClick.newBuilder().setClickTarget(paramCLICK_TARGET).setResultPosition(paramResultItem.getResultPosition()).setResultId(paramResultItem.getAnnotationResult().getResultId()).setActionPosition(paramInt)));
  }

  public static void logClick(Context paramContext, NativeClientLoggingProtos.NativeClientClick.CLICK_TARGET paramCLICK_TARGET)
  {
    get(paramContext).logClick(paramCLICK_TARGET);
  }

  public static void logClick(View paramView, NativeClientLoggingProtos.NativeClientClick.CLICK_TARGET paramCLICK_TARGET)
  {
    logClick(paramView.getContext(), paramCLICK_TARGET);
  }

  public static void logResultClick(Context paramContext, NativeClientLoggingProtos.NativeClientClick.CLICK_TARGET paramCLICK_TARGET, ResultItem paramResultItem, int paramInt)
  {
    get(paramContext).logResultClick(paramCLICK_TARGET, paramResultItem, paramInt);
  }

  private void logResultClick(NativeClientLoggingProtos.NativeClientClick.CLICK_TARGET paramCLICK_TARGET, ResultItem paramResultItem, int paramInt)
  {
    NativeClientLoggingProtos.NativeClientClick.Builder localBuilder = NativeClientLoggingProtos.NativeClientClick.newBuilder().setClickTarget(paramCLICK_TARGET).setResultPosition(paramResultItem.getResultPosition()).setResultId(paramResultItem.getAnnotationResult().getResultId()).setDisplayPosition(paramInt);
    NativeClientLoggingProtos.NativeClientLogEventRequest.Builder localBuilder1 = newRequestBuilder();
    localBuilder1.setClientClick(localBuilder);
    addToRequests(localBuilder1);
  }

  private void maybeSendRequestWithTrackingId(NativeClientLoggingProtos.NativeClientLogEventRequest.Builder paramBuilder)
  {
    if (TextUtils.isEmpty(this.trackingId))
    {
      this.logRequestsWithoutTrackingIds.add(paramBuilder);
      return;
    }
    paramBuilder.setTrackingId(this.trackingId);
    sendRequest(paramBuilder);
  }

  public static ClickLogConnector newDefaultLogConnector(AbstractConnector paramAbstractConnector)
  {
    return new DefaultClickLogConnector(paramAbstractConnector, Executors.newFixedThreadPool(2));
  }

  private static NativeClientLoggingProtos.NativeClientLogEventRequest.Builder newRequestBuilder()
  {
    NativeClientLoggingProtos.NativeClientLogEventRequest.Builder localBuilder = NativeClientLoggingProtos.NativeClientLogEventRequest.newBuilder();
    localBuilder.setMsSinceEpoch(System.currentTimeMillis());
    return localBuilder;
  }

  private void sendRequest(NativeClientLoggingProtos.NativeClientLogEventRequest.Builder paramBuilder)
  {
    if (!TextUtils.isEmpty(this.sessionId))
      paramBuilder.setSessionId(this.sessionId);
    this.connector.sendRequest(paramBuilder.build(), this.responseHandler);
  }

  public void clearTrackingId()
  {
    this.trackingId = null;
  }

  public String getTrackingId()
  {
    return this.trackingId;
  }

  public void logClick(NativeClientLoggingProtos.NativeClientClick.CLICK_TARGET paramCLICK_TARGET)
  {
    logger.i("Logging simple click: %s", new Object[] { paramCLICK_TARGET });
    NativeClientLoggingProtos.NativeClientLogEventRequest.Builder localBuilder = newRequestBuilder();
    NativeClientLoggingProtos.NativeClientClick.Builder localBuilder1 = NativeClientLoggingProtos.NativeClientClick.newBuilder();
    localBuilder1.setClickTarget(paramCLICK_TARGET);
    localBuilder.setClientClick(localBuilder1);
    addToRequests(localBuilder);
  }

  public void logClick(NativeClientLoggingProtos.NativeClientClick.CLICK_TARGET paramCLICK_TARGET, int paramInt)
  {
    NativeClientLoggingProtos.NativeClientLogEventRequest.Builder localBuilder = newRequestBuilder();
    NativeClientLoggingProtos.NativeClientClick.Builder localBuilder1 = NativeClientLoggingProtos.NativeClientClick.newBuilder();
    localBuilder1.setClickTarget(paramCLICK_TARGET);
    localBuilder1.setResultPosition(paramInt);
    localBuilder.setClientClick(localBuilder1);
    addToRequests(localBuilder);
  }

  public void logClick(NativeClientLoggingProtos.NativeClientClick.CLICK_TARGET paramCLICK_TARGET, int paramInt1, int paramInt2)
  {
    NativeClientLoggingProtos.NativeClientLogEventRequest.Builder localBuilder = newRequestBuilder();
    NativeClientLoggingProtos.NativeClientClick.Builder localBuilder1 = NativeClientLoggingProtos.NativeClientClick.newBuilder();
    localBuilder1.setClickTarget(paramCLICK_TARGET);
    localBuilder1.setResultPosition(paramInt1);
    localBuilder1.setResultsShownAboveFold(paramInt2);
    localBuilder.setClientClick(localBuilder1);
    addToRequests(localBuilder);
  }

  public void logInstall(String paramString1, String paramString2)
  {
    NativeClientLoggingProtos.NativeClientLogEventRequest.Builder localBuilder = NativeClientLoggingProtos.NativeClientLogEventRequest.newBuilder();
    localBuilder.setMsSinceEpoch(System.currentTimeMillis());
    NativeClientLoggingProtos.NativeClientInstall.Builder localBuilder1 = NativeClientLoggingProtos.NativeClientInstall.newBuilder();
    if (!paramString1.equals("0"))
      localBuilder1.setUpgradeFrom(paramString1);
    localBuilder1.setInstalledVersion(paramString2);
    localBuilder.setClientInstall(localBuilder1);
    addToRequests(localBuilder);
  }

  public void logNotificationClick(int paramInt)
  {
    NativeClientLoggingProtos.NativeClientLogEventRequest.Builder localBuilder = newRequestBuilder();
    NativeClientLoggingProtos.NativeClientClick.Builder localBuilder1 = NativeClientLoggingProtos.NativeClientClick.newBuilder();
    localBuilder1.setClickTarget(NativeClientLoggingProtos.NativeClientClick.CLICK_TARGET.SEARCH_FROM_CAMERA_GOGGLES_NOTIFICATION);
    localBuilder1.setNotificationResultsCount(paramInt);
    localBuilder.setClientClick(localBuilder1);
    addToRequests(localBuilder);
  }

  public void setSessionId(String paramString)
  {
    this.sessionId = paramString;
  }

  public void setTrackingId(String paramString)
  {
    if (paramString != null)
    {
      try
      {
        this.trackingId = paramString;
        Iterator localIterator = this.logRequestsWithoutTrackingIds.iterator();
        while (localIterator.hasNext())
        {
          NativeClientLoggingProtos.NativeClientLogEventRequest.Builder localBuilder = (NativeClientLoggingProtos.NativeClientLogEventRequest.Builder)localIterator.next();
          localBuilder.setTrackingId(paramString);
          sendRequest(localBuilder);
        }
      }
      finally
      {
      }
      this.logRequestsWithoutTrackingIds.clear();
    }
  }

  public static abstract interface ClickLogConnector
  {
    public abstract void sendRequest(NativeClientLoggingProtos.NativeClientLogEventRequest paramNativeClientLogEventRequest, AbstractConnector.ResponseHandler<NativeClientLoggingProtos.NativeClientLogEventResponse> paramResponseHandler);
  }

  static class DefaultClickLogConnector
    implements ClickTracker.ClickLogConnector
  {
    private static final Class<NativeClientLoggingProtos.NativeClientLogEventResponse> RESPONSE_CLASS = NativeClientLoggingProtos.NativeClientLogEventResponse.class;
    private final AbstractConnector connector;
    private final Executor executor;

    public DefaultClickLogConnector(AbstractConnector paramAbstractConnector, Executor paramExecutor)
    {
      this.connector = paramAbstractConnector;
      this.executor = paramExecutor;
    }

    public void sendRequest(NativeClientLoggingProtos.NativeClientLogEventRequest paramNativeClientLogEventRequest, AbstractConnector.ResponseHandler<NativeClientLoggingProtos.NativeClientLogEventResponse> paramResponseHandler)
    {
      this.executor.execute(this.connector.requestRunnable(paramNativeClientLogEventRequest, RESPONSE_CLASS, "", paramResponseHandler));
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.unveil.protocol.ClickTracker
 * JD-Core Version:    0.6.2
 */