package com.google.android.apps.unveil.protocol;

public class LocalTracingCookieFetcher extends TracingCookieFetcher
{
  public static final LocalTracingCookieFetcher NO_OP = new LocalTracingCookieFetcher("");
  private final String cookie;

  public LocalTracingCookieFetcher(String paramString)
  {
    super(null);
    this.cookie = paramString;
  }

  public String getFreshCookie()
  {
    return this.cookie;
  }

  public void replenish()
  {
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.unveil.protocol.LocalTracingCookieFetcher
 * JD-Core Version:    0.6.2
 */