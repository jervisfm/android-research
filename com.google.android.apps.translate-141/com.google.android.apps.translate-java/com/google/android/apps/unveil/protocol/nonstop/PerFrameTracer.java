package com.google.android.apps.unveil.protocol.nonstop;

import com.google.android.apps.unveil.protocol.TraceTracker;

public abstract interface PerFrameTracer
{
  public abstract void onProcessFrame(int paramInt, TraceTracker paramTraceTracker);
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.unveil.protocol.nonstop.PerFrameTracer
 * JD-Core Version:    0.6.2
 */