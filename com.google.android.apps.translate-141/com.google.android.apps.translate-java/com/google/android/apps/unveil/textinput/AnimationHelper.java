package com.google.android.apps.unveil.textinput;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.os.Build.VERSION;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.Interpolator;
import android.view.animation.TranslateAnimation;

public class AnimationHelper
{
  public static void alpha(View paramView, float paramFloat1, float paramFloat2, long paramLong, Interpolator paramInterpolator, Runnable paramRunnable)
  {
    if (Build.VERSION.SDK_INT >= 12)
      animatePostHC(paramView.animate().alpha(paramFloat2), paramLong, paramInterpolator, paramRunnable);
    while ((paramView.getVisibility() != 0) || (paramView.getWidth() <= 0) || (paramView.getHeight() <= 0))
      return;
    animatePreHCMR1(paramView, new AlphaAnimation(paramFloat1, paramFloat2), paramLong, paramInterpolator, paramRunnable);
  }

  private static void animatePostHC(ViewPropertyAnimator paramViewPropertyAnimator, long paramLong, Interpolator paramInterpolator, Runnable paramRunnable)
  {
    paramViewPropertyAnimator.setDuration(paramLong);
    if (paramInterpolator != null)
      paramViewPropertyAnimator.setInterpolator(paramInterpolator);
    if (paramRunnable != null)
    {
      paramViewPropertyAnimator.setListener(new AnimatorListenerAdapter()
      {
        public void onAnimationEnd(Animator paramAnonymousAnimator)
        {
          this.val$onEnd.run();
        }
      });
      return;
    }
    paramViewPropertyAnimator.setListener(null);
  }

  private static void animatePreHCMR1(View paramView, Animation paramAnimation, long paramLong, Interpolator paramInterpolator, Runnable paramRunnable)
  {
    paramView.clearAnimation();
    paramAnimation.setDuration(paramLong);
    paramAnimation.setFillAfter(true);
    if (paramInterpolator != null)
      paramAnimation.setInterpolator(paramInterpolator);
    if (paramRunnable != null)
      paramAnimation.setAnimationListener(new Animation.AnimationListener()
      {
        public void onAnimationEnd(Animation paramAnonymousAnimation)
        {
          this.val$onEnd.run();
        }

        public void onAnimationRepeat(Animation paramAnonymousAnimation)
        {
        }

        public void onAnimationStart(Animation paramAnonymousAnimation)
        {
        }
      });
    while (true)
    {
      paramView.startAnimation(paramAnimation);
      return;
      paramAnimation.setAnimationListener(null);
    }
  }

  public static void translateY(View paramView, float paramFloat1, float paramFloat2, long paramLong, Interpolator paramInterpolator, Runnable paramRunnable)
  {
    if (Build.VERSION.SDK_INT >= 12)
    {
      paramView.setTranslationY(paramFloat1);
      animatePostHC(paramView.animate().translationY(paramFloat2), paramLong, paramInterpolator, paramRunnable);
    }
    while ((paramView.getVisibility() != 0) || (paramView.getWidth() <= 0) || (paramView.getHeight() <= 0))
      return;
    animatePreHCMR1(paramView, new TranslateAnimation(0.0F, 0.0F, paramFloat1, paramFloat2), paramLong, paramInterpolator, paramRunnable);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.unveil.textinput.AnimationHelper
 * JD-Core Version:    0.6.2
 */