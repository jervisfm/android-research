package com.google.android.apps.unveil.textinput;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import com.google.android.apps.unveil.env.Check;
import com.google.android.apps.unveil.env.PixelUtils;
import com.google.android.apps.unveil.env.Size;
import com.google.android.apps.unveil.env.UnveilLogger;
import com.google.goggles.BoundingBoxProtos.BoundingBox;
import java.util.Iterator;
import java.util.List;

public class BoundingBoxView extends View
{
  private static final float HIGHLIGHT_RADIUS_DIP = 5.0F;
  private static final int SELECTED_COLOR = Color.argb(75, 0, 160, 210);
  private static final int UNSELECTED_COLOR = Color.argb(40, 255, 255, 255);
  private static UnveilLogger logger = new UnveilLogger();
  private final float highlightRadius;
  private final Paint paint = new Paint();
  private Size queryPictureSize;
  private TextMasker textMasker;
  private RectF wordRect = new RectF();

  public BoundingBoxView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    this.paint.setStyle(Paint.Style.FILL);
    this.highlightRadius = PixelUtils.dipToPix(5.0F, paramContext);
  }

  private void drawWords(Canvas paramCanvas, List<UnderlinePositioner.WordUnderline> paramList, boolean paramBoolean)
  {
    Paint localPaint = this.paint;
    if (paramBoolean);
    for (int i = SELECTED_COLOR; ; i = UNSELECTED_COLOR)
    {
      localPaint.setColor(i);
      Iterator localIterator = paramList.iterator();
      while (localIterator.hasNext())
      {
        BoundingBoxProtos.BoundingBox localBoundingBox = ((UnderlinePositioner.WordUnderline)localIterator.next()).getBox();
        this.wordRect.set(localBoundingBox.getX(), localBoundingBox.getY(), localBoundingBox.getX() + localBoundingBox.getWidth(), localBoundingBox.getY() + localBoundingBox.getHeight());
        paramCanvas.drawRoundRect(this.wordRect, this.highlightRadius, this.highlightRadius, this.paint);
      }
    }
  }

  public void onDraw(Canvas paramCanvas)
  {
    try
    {
      TextMasker localTextMasker = this.textMasker;
      if (localTextMasker == null);
      while (true)
      {
        return;
        paramCanvas.scale(getWidth() / this.queryPictureSize.width, getHeight() / this.queryPictureSize.height);
        drawWords(paramCanvas, this.textMasker.getSelectedWords(), true);
        drawWords(paramCanvas, this.textMasker.getUnselectedWords(), false);
      }
    }
    finally
    {
    }
  }

  public void setQueryPictureSize(Size paramSize)
  {
    Check.checkNotNull(paramSize);
    this.queryPictureSize = paramSize;
    invalidate();
  }

  public void setTextMasker(TextMasker paramTextMasker)
  {
    Check.checkNotNull(paramTextMasker);
    this.textMasker = paramTextMasker;
    invalidate();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.unveil.textinput.BoundingBoxView
 * JD-Core Version:    0.6.2
 */