package com.google.android.apps.unveil.textinput;

import com.google.android.apps.unveil.results.ResultItem;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class ConfigurationConstants
{
  public static final boolean IS_RELEASE_BUILD = false;
  public static final int LOOPER_FPS = 30;
  public static final int SMUDGE_ENTER_FADE_DURATION_MS = 600;
  public static final int SMUDGE_EXIT_FADE_DURATION_MS = 400;
  public static final Set<String> TEXT_RESULT_TYPES = new HashSet(Arrays.asList(new String[] { "Text", "Contact" }));

  public static boolean isTextResult(ResultItem paramResultItem)
  {
    return TEXT_RESULT_TYPES.contains(paramResultItem.getType());
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.unveil.textinput.ConfigurationConstants
 * JD-Core Version:    0.6.2
 */