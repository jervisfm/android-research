package com.google.android.apps.unveil.textinput;

import android.content.Context;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.Display;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.WindowManager;
import com.google.android.apps.unveil.env.Size;
import com.google.android.apps.unveil.env.UnveilLogger;
import com.google.android.apps.unveil.sensors.CameraManager;

class Container extends ViewGroup
{
  private static final UnveilLogger logger = new UnveilLogger();
  private Listener listener;

  public Container(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }

  public int getDisplayRotation()
  {
    return ((WindowManager)getContext().getSystemService("window")).getDefaultDisplay().getRotation();
  }

  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    UnveilLogger localUnveilLogger = logger;
    Object[] arrayOfObject = new Object[4];
    arrayOfObject[0] = Integer.valueOf(paramInt1);
    arrayOfObject[1] = Integer.valueOf(paramInt2);
    arrayOfObject[2] = Integer.valueOf(paramInt3);
    arrayOfObject[3] = Integer.valueOf(paramInt4);
    localUnveilLogger.d("layout at %d, %d, %d, %d", arrayOfObject);
    int i = 0;
    if (i < getChildCount())
    {
      View localView = getChildAt(i);
      if (localView.getId() == R.id.non_stretchable)
        localView.layout(0, 0, paramInt3 - paramInt1, localView.getMeasuredHeight());
      while (true)
      {
        i++;
        break;
        if (localView.getId() != R.id.stretchable)
          break label137;
        localView.layout(0, 0, paramInt3 - paramInt1, localView.getMeasuredHeight());
      }
      label137: throw new RuntimeException("Illegal child in Container.");
    }
  }

  public void onMeasure(int paramInt1, int paramInt2)
  {
    super.onMeasure(paramInt1, paramInt2);
    int i = getMeasuredWidth();
    UnveilLogger localUnveilLogger1 = logger;
    Object[] arrayOfObject1 = new Object[1];
    arrayOfObject1[0] = Integer.valueOf(i);
    localUnveilLogger1.d("measured width is %d", arrayOfObject1);
    int j = getMeasuredHeight();
    int k = paramInt2;
    CameraManager localCameraManager = (CameraManager)findViewById(R.id.camera_manager);
    WindowManager localWindowManager = (WindowManager)getContext().getSystemService("window");
    Point localPoint = new Point(localWindowManager.getDefaultDisplay().getWidth(), localWindowManager.getDefaultDisplay().getHeight());
    int m = (int)(i * localPoint.y / localPoint.x);
    Size localSize1 = new Size(i, m);
    localCameraManager.setFullScreenDisplaySize(localSize1);
    Size localSize2 = localCameraManager.getOptimalPreviewSize(i, m);
    if ((getDisplayRotation() == 0) || (getDisplayRotation() == 2))
      localSize2 = new Size(localSize2.height, localSize2.width);
    if (localSize2 != null)
    {
      int i4 = (int)(i / localSize2.width * localSize2.height);
      k = View.MeasureSpec.makeMeasureSpec(i4, 1073741824);
      UnveilLogger localUnveilLogger4 = logger;
      Object[] arrayOfObject4 = new Object[4];
      arrayOfObject4[0] = Integer.valueOf(localSize2.width);
      arrayOfObject4[1] = Integer.valueOf(localSize2.height);
      arrayOfObject4[2] = Integer.valueOf(i);
      arrayOfObject4[3] = Integer.valueOf(i4);
      localUnveilLogger4.d("preview size is %dx%d, non-stretchable size is %dx%d", arrayOfObject4);
    }
    int n = View.MeasureSpec.getMode(paramInt2);
    int i1 = View.MeasureSpec.getSize(paramInt2);
    UnveilLogger localUnveilLogger2 = logger;
    Object[] arrayOfObject2 = new Object[2];
    String str;
    int i2;
    int i3;
    label387: View localView;
    if (n == -2147483648)
    {
      str = "atmost";
      arrayOfObject2[0] = str;
      arrayOfObject2[1] = Integer.valueOf(i1);
      localUnveilLogger2.d("suggested height mode is %s, size is %d", arrayOfObject2);
      UnveilLogger localUnveilLogger3 = logger;
      Object[] arrayOfObject3 = new Object[1];
      arrayOfObject3[0] = Integer.valueOf(i1);
      localUnveilLogger3.d("stretchable height is %d", arrayOfObject3);
      if (n == 0)
        i1 = j;
      i2 = View.MeasureSpec.makeMeasureSpec(i1, 1073741824);
      i3 = 0;
      if (i3 >= getChildCount())
        break label482;
      localView = getChildAt(i3);
      if (localView.getId() != R.id.non_stretchable)
        break label450;
      localView.measure(paramInt1, k);
    }
    while (true)
    {
      i3++;
      break label387;
      if (n == 1073741824)
      {
        str = "exactly";
        break;
      }
      str = "undefined";
      break;
      label450: if (localView.getId() != R.id.stretchable)
        break label472;
      localView.measure(paramInt1, i2);
    }
    label472: throw new RuntimeException("Illegal child in Container.");
    label482: setMeasuredDimension(i, i1);
    if (this.listener != null)
      this.listener.onReady();
  }

  protected void onVisibilityChanged(View paramView, int paramInt)
  {
    UnveilLogger localUnveilLogger = logger;
    Object[] arrayOfObject = new Object[2];
    arrayOfObject[0] = paramView.toString();
    arrayOfObject[1] = Integer.valueOf(paramInt);
    localUnveilLogger.d("onVisibilityChanged(%s, %d)", arrayOfObject);
  }

  public void setListener(Listener paramListener)
  {
    this.listener = paramListener;
  }

  public static abstract interface Listener
  {
    public abstract void onReady();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.unveil.textinput.Container
 * JD-Core Version:    0.6.2
 */