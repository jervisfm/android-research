package com.google.android.apps.unveil.textinput;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import com.google.android.apps.unveil.env.PixelUtils;

class GridOverlayView extends View
{
  private static final int ALPHA = 65;
  private static final int MIN_HEIGHT_DIP = 60;
  private int minHeightPx;
  private final Paint paint = new Paint();

  public GridOverlayView(Context paramContext)
  {
    super(paramContext);
    init(paramContext);
  }

  public GridOverlayView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    init(paramContext);
  }

  private void init(Context paramContext)
  {
    this.paint.setAlpha(65);
    this.minHeightPx = PixelUtils.dipToPix(60.0F, paramContext);
  }

  protected void onDraw(Canvas paramCanvas)
  {
    super.onDraw(paramCanvas);
    int i = getHeight() / this.minHeightPx;
    int j = getHeight() / i;
    for (int k = 1; k < i; k++)
      paramCanvas.drawLine(0.0F, k * j, getWidth(), k * j, this.paint);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.unveil.textinput.GridOverlayView
 * JD-Core Version:    0.6.2
 */