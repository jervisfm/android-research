package com.google.android.apps.unveil.textinput;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.preference.PreferenceManager;
import com.google.android.apps.unveil.env.UnveilLogger;
import java.util.UUID;

public class InstallationIdHelper
{
  private static final int NO_PREVIOUS_VERSION = -1;
  private static final UnveilLogger logger = new UnveilLogger();

  public static String getInstallationId(Context paramContext)
  {
    SharedPreferences localSharedPreferences = PreferenceManager.getDefaultSharedPreferences(paramContext);
    int i = getPreviousVersion(paramContext, localSharedPreferences);
    try
    {
      j = paramContext.getPackageManager().getPackageInfo(paramContext.getApplicationInfo().packageName, 0).versionCode;
      if (j != i)
        resetInstallationId(paramContext, localSharedPreferences, j);
      return getInstallationId(paramContext, localSharedPreferences);
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
      while (true)
      {
        logger.e("Unable to retrieve version code from manifest", new Object[0]);
        int j = 0;
      }
    }
  }

  private static String getInstallationId(Context paramContext, SharedPreferences paramSharedPreferences)
  {
    String str = paramSharedPreferences.getString(paramContext.getString(R.string.text_input_installation_id_key), "");
    logger.d("Installation id: %s", new Object[] { str });
    return str;
  }

  private static int getPreviousVersion(Context paramContext, SharedPreferences paramSharedPreferences)
  {
    return paramSharedPreferences.getInt(paramContext.getString(R.string.text_input_previous_version_key), -1);
  }

  private static void resetInstallationId(Context paramContext, SharedPreferences paramSharedPreferences, int paramInt)
  {
    String str = UUID.randomUUID().toString();
    logger.d("Setting installationId to %s", new Object[] { str });
    paramSharedPreferences.edit().putString(paramContext.getString(R.string.text_input_installation_id_key), str).putInt(paramContext.getString(R.string.text_input_previous_version_key), paramInt).commit();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.unveil.textinput.InstallationIdHelper
 * JD-Core Version:    0.6.2
 */