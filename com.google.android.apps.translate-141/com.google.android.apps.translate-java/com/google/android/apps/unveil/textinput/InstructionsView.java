package com.google.android.apps.unveil.textinput;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.FontMetricsInt;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import com.google.android.apps.unveil.env.UnveilLogger;

public class InstructionsView extends View
{
  private static final UnveilLogger logger = new UnveilLogger();
  final Rect bounds = new Rect();
  private String currentText;
  private String longestText = "";
  private final int maxTextSize;
  private final Paint paint = new Paint();

  public InstructionsView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    this.paint.setAntiAlias(true);
    this.paint.setColor(paramContext.getResources().getColor(R.color.instructions_text_color));
    for (Instruction localInstruction : Instruction.values())
    {
      String str = paramContext.getResources().getString(localInstruction.textId);
      if (str.length() > this.longestText.length())
        this.longestText = str;
    }
    this.maxTextSize = paramContext.getResources().getDimensionPixelSize(R.dimen.max_instruction_text_size);
  }

  public void onDraw(Canvas paramCanvas)
  {
    super.onDraw(paramCanvas);
    Drawable localDrawable = getBackground();
    if (localDrawable != null)
    {
      localDrawable.setBounds(0, 0, getWidth(), getHeight());
      localDrawable.draw(paramCanvas);
    }
    if (this.currentText != null)
    {
      Rect localRect = new Rect();
      this.paint.getTextBounds(this.currentText, 0, this.currentText.length(), localRect);
      int i = getPaddingLeft() + (getWidth() - localRect.width() - getPaddingLeft() - getPaddingRight()) / 2;
      int j = getPaddingTop() + (getHeight() - localRect.height() - getPaddingTop() - getPaddingBottom()) / 2;
      paramCanvas.drawText(this.currentText, i, j - localRect.top, this.paint);
    }
  }

  public void onMeasure(int paramInt1, int paramInt2)
  {
    super.onMeasure(paramInt1, paramInt2);
    int i = getMeasuredWidth() - getPaddingLeft() - getPaddingRight();
    (getMeasuredHeight() - getPaddingTop() - getPaddingBottom());
    int j = 1;
    do
    {
      this.paint.setTextSize(j);
      this.paint.getTextBounds(this.longestText, 0, this.longestText.length(), this.bounds);
      j++;
    }
    while ((this.bounds.width() < i) && (j <= this.maxTextSize));
    UnveilLogger localUnveilLogger = logger;
    Object[] arrayOfObject = new Object[2];
    arrayOfObject[0] = Integer.valueOf(j - 1);
    arrayOfObject[1] = Integer.valueOf(this.maxTextSize);
    localUnveilLogger.d("Decide instruction text size as %d, max is %d", arrayOfObject);
    this.paint.setTextSize(j - 1);
    Paint.FontMetricsInt localFontMetricsInt = this.paint.getFontMetricsInt();
    setMeasuredDimension(getMeasuredWidth(), localFontMetricsInt.descent - localFontMetricsInt.ascent + getPaddingTop() + getPaddingBottom());
  }

  public void show(Instruction paramInstruction)
  {
    if (paramInstruction == null);
    for (this.currentText = null; ; this.currentText = getContext().getResources().getString(paramInstruction.textId))
    {
      postInvalidate();
      return;
    }
  }

  public static enum Instruction
  {
    public int textId;

    static
    {
      HOLD_STEADY = new Instruction("HOLD_STEADY", 2, R.string.text_input_hold_steady);
      NO_TEXT = new Instruction("NO_TEXT", 3, R.string.text_input_no_text);
      USE_TWO_FINGERS_TO_PAN = new Instruction("USE_TWO_FINGERS_TO_PAN", 4, R.string.text_input_use_two_fingers_to_pan);
      Instruction[] arrayOfInstruction = new Instruction[5];
      arrayOfInstruction[0] = TAP_TO_READ;
      arrayOfInstruction[1] = USE_FINGER_TO_HIGHLIGHT;
      arrayOfInstruction[2] = HOLD_STEADY;
      arrayOfInstruction[3] = NO_TEXT;
      arrayOfInstruction[4] = USE_TWO_FINGERS_TO_PAN;
    }

    private Instruction(int paramInt)
    {
      this.textId = paramInt;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.unveil.textinput.InstructionsView
 * JD-Core Version:    0.6.2
 */