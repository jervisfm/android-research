package com.google.android.apps.unveil.textinput;

import android.content.Context;
import android.graphics.Point;
import com.google.android.apps.unveil.env.PixelUtils;
import com.google.goggles.AnnotationResultProtos.AnnotationResult.TextInformation.Word;
import com.google.goggles.BoundingBoxProtos.BoundingBox;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class SlantedUnderlinePositioner
  implements UnderlinePositioner
{
  private static final int VERTICAL_SHIFT_DIP = 3;
  private final Map<AnnotationResultProtos.AnnotationResult.TextInformation.Word, UnderlinePositioner.WordUnderline> underlines = new HashMap();
  private final int verticalShiftDip;

  public SlantedUnderlinePositioner(Context paramContext)
  {
    this.verticalShiftDip = PixelUtils.dipToPix(3.0F, paramContext);
  }

  private Point boxCenter(BoundingBoxProtos.BoundingBox paramBoundingBox)
  {
    return new Point(paramBoundingBox.getX() + paramBoundingBox.getWidth() / 2, paramBoundingBox.getY() + paramBoundingBox.getHeight() / 2);
  }

  private boolean inSameLine(AnnotationResultProtos.AnnotationResult.TextInformation.Word paramWord1, AnnotationResultProtos.AnnotationResult.TextInformation.Word paramWord2)
  {
    if (paramWord2.getBox().getX() <= paramWord1.getBox().getX() + paramWord1.getBox().getWidth());
    while (Math.abs(paramWord2.getBox().getY() - paramWord1.getBox().getY()) > paramWord1.getBox().getHeight() / 2)
      return false;
    return true;
  }

  private void processLine(ArrayList<AnnotationResultProtos.AnnotationResult.TextInformation.Word> paramArrayList)
  {
    BoundingBoxProtos.BoundingBox localBoundingBox1 = ((AnnotationResultProtos.AnnotationResult.TextInformation.Word)paramArrayList.get(0)).getBox();
    BoundingBoxProtos.BoundingBox localBoundingBox2 = ((AnnotationResultProtos.AnnotationResult.TextInformation.Word)paramArrayList.get(-1 + paramArrayList.size())).getBox();
    Point localPoint1 = boxCenter(localBoundingBox1);
    Point localPoint2 = boxCenter(localBoundingBox2);
    int i = paramArrayList.size();
    float f = 0.0F;
    if (i > 1)
      f = slope(localPoint1, localPoint2);
    Iterator localIterator = paramArrayList.iterator();
    while (localIterator.hasNext())
    {
      AnnotationResultProtos.AnnotationResult.TextInformation.Word localWord = (AnnotationResultProtos.AnnotationResult.TextInformation.Word)localIterator.next();
      BoundingBoxProtos.BoundingBox localBoundingBox3 = localWord.getBox();
      this.underlines.put(localWord, new UnderlinePositioner.WordUnderline(new Point(localBoundingBox3.getX(), (int)(f * (localBoundingBox3.getX() - localBoundingBox1.getX()) + localBoundingBox1.getY() + localBoundingBox1.getHeight() + this.verticalShiftDip)), new Point(localBoundingBox3.getX() + localBoundingBox3.getWidth(), (int)(f * (localBoundingBox3.getX() + localBoundingBox3.getWidth() - localBoundingBox1.getX()) + localBoundingBox1.getY() + localBoundingBox1.getHeight() + this.verticalShiftDip)), localBoundingBox3));
    }
  }

  private float slope(Point paramPoint1, Point paramPoint2)
  {
    if (paramPoint1.x == paramPoint2.x)
      return 2.147484E+009F;
    return (paramPoint2.y - paramPoint1.y) / (paramPoint2.x - paramPoint1.x);
  }

  public UnderlinePositioner.WordUnderline getUnderline(AnnotationResultProtos.AnnotationResult.TextInformation.Word paramWord)
  {
    if (this.underlines.containsKey(paramWord))
      return (UnderlinePositioner.WordUnderline)this.underlines.get(paramWord);
    return null;
  }

  public void setWords(List<AnnotationResultProtos.AnnotationResult.TextInformation.Word> paramList)
  {
    this.underlines.clear();
    if ((paramList == null) || (paramList.size() == 0));
    ArrayList localArrayList;
    do
    {
      return;
      localArrayList = new ArrayList();
      localArrayList.add(paramList.get(0));
      for (int i = 1; i < paramList.size(); i++)
      {
        if (!inSameLine((AnnotationResultProtos.AnnotationResult.TextInformation.Word)localArrayList.get(-1 + localArrayList.size()), (AnnotationResultProtos.AnnotationResult.TextInformation.Word)paramList.get(i)))
        {
          processLine(localArrayList);
          localArrayList.clear();
        }
        localArrayList.add(paramList.get(i));
      }
    }
    while (localArrayList.isEmpty());
    processLine(localArrayList);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.unveil.textinput.SlantedUnderlinePositioner
 * JD-Core Version:    0.6.2
 */