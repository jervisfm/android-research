package com.google.android.apps.unveil.textinput;

import android.content.Context;
import android.graphics.AvoidXfermode;
import android.graphics.AvoidXfermode.Mode;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Cap;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Build.VERSION;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import com.google.android.apps.unveil.env.GeometryUtils;
import com.google.android.apps.unveil.env.Picture;
import com.google.android.apps.unveil.env.PictureFactory;
import com.google.android.apps.unveil.env.PixelUtils;
import com.google.android.apps.unveil.env.Size;
import com.google.android.apps.unveil.env.UnveilLogger;
import java.util.ArrayList;
import java.util.List;

public class SmudgeView extends View
  implements ZoomableContainer.EventListener
{
  private static final float[] BASE_COLORMATRIX_DATA = { 0.3F, 0.15F, 0.15F, 0.0F, 0.0F, 0.15F, 0.3F, 0.15F, 0.0F, 0.0F, 0.15F, 0.15F, 0.3F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 1.0F, 0.0F };
  private static final float FAKE_HIGHLIGHT_ALPHA = 0.6F;
  private static final float HORIZONTAL_MAXIMUM_ABSOLUTE_SLOPE = 0.25F;
  private static final float[] INVERTING_COLORMATRIX_DATA = { 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, -1.0F, 100.0F };
  private static final long MAX_FULL_REDRAW_TIME_MS = 500L;
  private static final int MIN_TOUCH_RADIUS_DIP = 50;
  private static final int MIN_X_DELTA_FOR_SLOPE_COMPUTATION_DIP = 100;
  private static final float MULTIPASS_SMUDGE_LIMIT_MS = 1000.0F;
  private static final int SINGLE_LINE_DETECTION_ERROR_DIP = 5;
  private static final float TOUCH_RADIUS_DOWNSCALE_RATIO = 0.75F;
  private static final UnveilLogger logger = new UnveilLogger();
  private boolean acceptSmudges;
  private final Paint alphaPaint;
  private final Paint baseImagePaint;
  private boolean currentSmudgeHorizontal = true;
  private boolean currentSmudgeLineLeft = true;
  private boolean currentSmudgeLineRight = true;
  private List<Float> currentSmudgeMajorDiameters = new ArrayList();
  private Path currentSmudgePath;
  private List<Point> currentSmudgePoints = new ArrayList();
  private Float currentSmudgeStartMajorDiamater;
  private final Rect dirtyRect = new Rect();
  private final Paint highlightCompositor;
  private final Paint highlightImagePaint;
  private final Paint highlightPaint;
  private Bitmap imageToDisplay;
  private volatile boolean imageToDisplayIsValid;
  private Picture imageToDisplayPicture;
  private final Paint joinPaint;
  final Path joinPath = new Path();
  private long lastFullRedrawTime;
  private long lastSmudgeCompletedTime;
  private Listener listener;
  private final float minTouchRadius;
  private final float minXDeltaForSlopeComputation;
  private Canvas offScreenCanvas;
  private Picture offScreenPicture;
  private final float singleLineDetectionError;
  private final float[] tmpFloats = new float[3];
  private final float[] tmpPoint = new float[2];
  private final RectF tmpRectF = new RectF();
  private final Rect tmpRectI = new Rect();
  private final Matrix unzoomMatrix = new Matrix();
  private boolean useManualInvalidation;
  private final Matrix zoomMatrix = new Matrix();

  public SmudgeView(Context paramContext)
  {
    this(paramContext, null);
  }

  public SmudgeView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }

  public SmudgeView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    setFocusable(true);
    setBackgroundColor(0);
    this.highlightPaint = new Paint();
    this.highlightPaint.setAntiAlias(true);
    this.highlightPaint.setColor(-16777216);
    this.joinPaint = new Paint(this.highlightPaint);
    this.joinPaint.setStyle(Paint.Style.STROKE);
    this.joinPaint.setStrokeCap(Paint.Cap.ROUND);
    this.baseImagePaint = new Paint();
    this.baseImagePaint.setColorFilter(new ColorMatrixColorFilter(BASE_COLORMATRIX_DATA));
    this.highlightImagePaint = new Paint();
    this.highlightImagePaint.setXfermode(new AvoidXfermode(-16777216, 0, AvoidXfermode.Mode.TARGET));
    this.highlightImagePaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_ATOP));
    this.highlightCompositor = new Paint();
    this.highlightCompositor.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_OVER));
    this.alphaPaint = new Paint();
    this.alphaPaint.setColorFilter(new ColorMatrixColorFilter(INVERTING_COLORMATRIX_DATA));
    this.minXDeltaForSlopeComputation = PixelUtils.dipToPix(100.0F, paramContext);
    this.minTouchRadius = PixelUtils.dipToPix(50.0F, paramContext);
    this.singleLineDetectionError = PixelUtils.dipToPix(5.0F, paramContext);
    setAlphaValue(0.6F);
  }

  private void addPathPoint(int paramInt1, int paramInt2, float paramFloat)
  {
    boolean bool1 = true;
    while (true)
    {
      try
      {
        float[] arrayOfFloat = mapPoint(paramInt1, paramInt2, Math.max(this.minTouchRadius, paramFloat));
        int i = (int)arrayOfFloat[0];
        int j = (int)arrayOfFloat[1];
        float f = arrayOfFloat[2];
        this.currentSmudgePoints.add(new Point(i, j));
        this.currentSmudgeMajorDiameters.add(Float.valueOf(f));
        if (this.currentSmudgePath == null)
        {
          this.currentSmudgePath = new Path();
          this.currentSmudgePath.moveTo(i, j);
          this.currentSmudgeStartMajorDiamater = Float.valueOf(f);
          return;
        }
        Point localPoint1 = (Point)this.currentSmudgePoints.get(-2 + this.currentSmudgePoints.size());
        Point localPoint2 = (Point)this.currentSmudgePoints.get(0);
        boolean bool2 = this.currentSmudgeLineLeft;
        if (localPoint1.x >= i - this.singleLineDetectionError)
        {
          bool3 = bool1;
          this.currentSmudgeLineLeft = (bool3 & bool2);
          boolean bool4 = this.currentSmudgeLineRight;
          if (localPoint1.x > i + this.singleLineDetectionError)
            break label326;
          bool5 = bool1;
          this.currentSmudgeLineRight = (bool5 & bool4);
          if (Math.abs(localPoint2.x - i) >= this.minXDeltaForSlopeComputation)
          {
            boolean bool6 = this.currentSmudgeHorizontal;
            if (Math.abs((j - localPoint2.y) / (i - localPoint2.x)) >= 0.25F)
              break label332;
            this.currentSmudgeHorizontal = (bool1 & bool6);
          }
          this.currentSmudgePath.lineTo(i, j);
          continue;
        }
      }
      finally
      {
      }
      boolean bool3 = false;
      continue;
      label326: boolean bool5 = false;
      continue;
      label332: bool1 = false;
    }
  }

  private Path completePath(int paramInt1, int paramInt2, float paramFloat)
  {
    try
    {
      float[] arrayOfFloat = mapPoint(paramInt1, paramInt2, Math.max(this.minTouchRadius, paramFloat));
      int i = (int)arrayOfFloat[0];
      int j = (int)arrayOfFloat[1];
      float f = arrayOfFloat[2];
      Path localPath = this.currentSmudgePath;
      if (GeometryUtils.intersect((Point)this.currentSmudgePoints.get(0), 0.75F * this.currentSmudgeStartMajorDiamater.floatValue() / 2.0F, new Point(i, j), 0.75F * f / 2.0F))
      {
        localPath.close();
        return localPath;
      }
      localPath = null;
    }
    finally
    {
    }
  }

  private void determineInvalidRect(Rect paramRect1, Rect paramRect2)
  {
    long l = SystemClock.uptimeMillis();
    if (l - this.lastFullRedrawTime < 500L)
    {
      this.tmpRectF.set(paramRect1);
      this.unzoomMatrix.mapRect(this.tmpRectF);
      this.tmpRectF.roundOut(paramRect2);
      return;
    }
    paramRect2.set(0, 0, getWidth(), getHeight());
    this.lastFullRedrawTime = l;
  }

  private boolean isCurrentSmudgeOneLine()
  {
    return (this.currentSmudgeHorizontal) && ((this.currentSmudgeLineLeft) || (this.currentSmudgeLineRight));
  }

  private float[] mapPoint(int paramInt1, int paramInt2, float paramFloat)
  {
    this.tmpPoint[0] = paramInt1;
    this.tmpPoint[1] = paramInt2;
    this.zoomMatrix.mapPoints(this.tmpPoint);
    float f = this.zoomMatrix.mapRadius(paramFloat);
    this.tmpFloats[0] = this.tmpPoint[0];
    this.tmpFloats[1] = this.tmpPoint[1];
    this.tmpFloats[2] = f;
    return this.tmpFloats;
  }

  private void paint(Path paramPath)
  {
    if (this.offScreenPicture != null)
    {
      this.offScreenCanvas.drawPath(paramPath, this.highlightPaint);
      if (this.imageToDisplayIsValid)
        this.offScreenCanvas.drawBitmap(this.imageToDisplay, 0.0F, 0.0F, this.highlightImagePaint);
    }
    invalidate();
  }

  private void paintJoin()
  {
    int i = this.currentSmudgePoints.size();
    if (i == 0)
      return;
    int j = i - 1;
    int k = Math.max(j - 1, 0);
    Point localPoint1 = (Point)this.currentSmudgePoints.get(j);
    Point localPoint2 = new Point((Point)this.currentSmudgePoints.get(k));
    if (GeometryUtils.distance(localPoint2, localPoint1) < 1.0F)
      localPoint2.offset(1, 1);
    this.joinPath.reset();
    this.joinPath.moveTo(localPoint2.x, localPoint2.y);
    this.joinPath.lineTo(localPoint1.x, localPoint1.y);
    float f1 = 0.75F * ((((Float)this.currentSmudgeMajorDiameters.get(j)).floatValue() + ((Float)this.currentSmudgeMajorDiameters.get(k)).floatValue()) / 2.0F);
    this.joinPaint.setStrokeWidth(f1);
    this.offScreenCanvas.drawPath(this.joinPath, this.joinPaint);
    float f2 = f1 / 2.0F;
    this.tmpRectF.set(Math.min(localPoint2.x, localPoint1.x) - f2, Math.min(localPoint2.y, localPoint1.y) - f2, f2 + Math.max(localPoint2.x, localPoint1.x), f2 + Math.max(localPoint2.y, localPoint1.y));
    this.tmpRectF.roundOut(this.tmpRectI);
    this.dirtyRect.union(this.tmpRectI);
  }

  private void resetPath()
  {
    this.currentSmudgePath = null;
    this.currentSmudgeStartMajorDiamater = null;
    this.currentSmudgePoints = new ArrayList();
    this.currentSmudgeMajorDiameters = new ArrayList();
    this.currentSmudgeLineLeft = true;
    this.currentSmudgeLineRight = true;
    this.currentSmudgeHorizontal = true;
  }

  private void setAlphaValue(float paramFloat)
  {
    if (255.0F * getAlphaValue() == 255.0F * paramFloat)
      return;
    int i = (int)(255.0F * paramFloat);
    this.highlightPaint.setAlpha(i);
    this.baseImagePaint.setAlpha(i);
    this.highlightImagePaint.setAlpha(i);
    this.highlightCompositor.setAlpha(i);
    this.joinPaint.setAlpha(i);
  }

  public void clearSmudges()
  {
    this.offScreenCanvas.drawColor(0, PorterDuff.Mode.CLEAR);
    postInvalidate();
  }

  public float getAlphaValue()
  {
    return this.joinPaint.getAlpha() / 255.0F;
  }

  public void invalidate()
  {
    this.lastFullRedrawTime = SystemClock.uptimeMillis();
    if (this.useManualInvalidation)
      this.dirtyRect.set(0, 0, getWidth(), getHeight());
    super.invalidate();
  }

  public void invalidate(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    if (this.useManualInvalidation)
    {
      this.dirtyRect.union(paramInt1, paramInt2, paramInt3, paramInt4);
      super.invalidate(this.dirtyRect.left, this.dirtyRect.top, this.dirtyRect.right, this.dirtyRect.bottom);
      return;
    }
    super.invalidate(paramInt1, paramInt2, paramInt3, paramInt4);
  }

  public void invalidate(Rect paramRect)
  {
    if (this.useManualInvalidation)
    {
      this.dirtyRect.union(paramRect);
      paramRect.set(this.dirtyRect);
    }
    super.invalidate(paramRect);
  }

  protected void onDraw(Canvas paramCanvas)
  {
    if (this.offScreenPicture == null)
      return;
    if (this.imageToDisplayIsValid)
    {
      setAlphaValue(1.0F);
      if (this.useManualInvalidation)
        this.tmpRectI.set(this.dirtyRect);
      while (true)
      {
        Rect localRect1 = this.tmpRectI;
        localRect1.left = (-1 + localRect1.left);
        Rect localRect2 = this.tmpRectI;
        localRect2.top = (-1 + localRect2.top);
        Rect localRect3 = this.tmpRectI;
        localRect3.right = (1 + localRect3.right);
        Rect localRect4 = this.tmpRectI;
        localRect4.bottom = (1 + localRect4.bottom);
        this.tmpRectI.intersect(0, 0, getWidth(), getHeight());
        this.offScreenCanvas.drawBitmap(this.imageToDisplay, this.tmpRectI, this.tmpRectI, this.highlightImagePaint);
        paramCanvas.drawBitmap(this.imageToDisplay, this.tmpRectI, this.tmpRectI, this.baseImagePaint);
        paramCanvas.drawBitmap(this.offScreenPicture.peekBitmap(), this.tmpRectI, this.tmpRectI, this.highlightCompositor);
        this.dirtyRect.setEmpty();
        return;
        paramCanvas.getClipBounds(this.tmpRectI);
      }
    }
    paramCanvas.drawBitmap(this.offScreenPicture.peekBitmap(), 0.0F, 0.0F, this.alphaPaint);
  }

  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    UnveilLogger localUnveilLogger = logger;
    Object[] arrayOfObject = new Object[4];
    arrayOfObject[0] = Integer.valueOf(paramInt1);
    arrayOfObject[1] = Integer.valueOf(paramInt2);
    arrayOfObject[2] = Integer.valueOf(paramInt3);
    arrayOfObject[3] = Integer.valueOf(paramInt4);
    localUnveilLogger.v("onLayout(%d, %d, %d, %d)", arrayOfObject);
    super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
  }

  protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    if (this.offScreenPicture != null)
      this.offScreenPicture = null;
    if (this.imageToDisplayPicture != null)
      this.imageToDisplayPicture = null;
    if ((paramInt1 <= 0) || (paramInt2 <= 0));
    do
    {
      return;
      Bitmap localBitmap = Bitmap.createBitmap(paramInt1, paramInt2, Bitmap.Config.ARGB_8888);
      this.offScreenCanvas = new Canvas(localBitmap);
      this.offScreenCanvas.drawColor(-16777216);
      this.imageToDisplay = localBitmap.copy(localBitmap.getConfig(), true);
      this.imageToDisplayPicture = PictureFactory.createBitmap(this.imageToDisplay, 0);
      this.offScreenPicture = PictureFactory.createBitmap(localBitmap, 0);
    }
    while ((Build.VERSION.SDK_INT < 11) || (!isHardwareAccelerated()));
    this.useManualInvalidation = true;
    setLayerType(2, null);
  }

  public boolean onTouchEvent(MotionEvent paramMotionEvent)
  {
    if (!this.acceptSmudges)
      return false;
    long l = System.currentTimeMillis();
    boolean bool;
    int i;
    int j;
    int k;
    if ((float)(l - this.lastSmudgeCompletedTime) < 1000.0F)
    {
      bool = true;
      this.lastSmudgeCompletedTime = l;
      i = paramMotionEvent.getActionMasked();
      if ((i == 0) || (i == 2) || (i == 7))
      {
        if ((i == 0) && (this.listener != null) && (!bool))
          this.listener.onSmudgeStarted();
        j = paramMotionEvent.getHistorySize();
        k = paramMotionEvent.getPointerCount();
      }
    }
    else
    {
      for (int m = 0; ; m++)
      {
        if (m >= j)
          break label168;
        int i1 = 0;
        while (true)
          if (i1 < k)
          {
            addPathPoint((int)paramMotionEvent.getHistoricalX(i1, m), (int)paramMotionEvent.getHistoricalY(i1, m), paramMotionEvent.getHistoricalTouchMajor(i1, m));
            paintJoin();
            i1++;
            continue;
            bool = false;
            break;
          }
      }
      label168: for (int n = 0; n < k; n++)
      {
        addPathPoint((int)paramMotionEvent.getX(n), (int)paramMotionEvent.getY(n), paramMotionEvent.getTouchMajor(n));
        paintJoin();
      }
      determineInvalidRect(this.dirtyRect, this.tmpRectI);
      invalidate(this.tmpRectI);
      if (this.listener != null)
        this.listener.onSmudgeProgress(new Smudge(this.offScreenPicture, isCurrentSmudgeOneLine(), bool, this.currentSmudgePoints), this.tmpRectI);
      return true;
    }
    if ((i == 1) || (i == 3))
    {
      for (int i2 = 0; i2 < paramMotionEvent.getPointerCount(); i2++)
      {
        addPathPoint((int)paramMotionEvent.getX(i2), (int)paramMotionEvent.getY(i2), paramMotionEvent.getTouchMajor(i2));
        paintJoin();
      }
      Path localPath = completePath((int)paramMotionEvent.getX(), (int)paramMotionEvent.getY(), paramMotionEvent.getTouchMajor());
      if (localPath != null)
        paint(localPath);
      invalidate();
      if ((this.listener != null) && (i != 3))
        this.listener.onSmudgeComplete(new Smudge(this.offScreenPicture, isCurrentSmudgeOneLine(), bool, this.currentSmudgePoints));
      resetPath();
      if (i == 3)
        clearSmudges();
      return true;
    }
    return false;
  }

  public void onZoom(Matrix paramMatrix)
  {
    paramMatrix.invert(this.zoomMatrix);
    this.unzoomMatrix.set(paramMatrix);
    if (this.listener != null)
      this.listener.onZoom();
  }

  public void setAcceptSmudges(boolean paramBoolean)
  {
    this.acceptSmudges = paramBoolean;
  }

  public void setImageToDisplay(Bitmap paramBitmap, Matrix paramMatrix)
  {
    if (paramBitmap == null)
    {
      this.imageToDisplayIsValid = false;
      postInvalidate();
      return;
    }
    Canvas localCanvas = new Canvas(this.imageToDisplay);
    if (paramMatrix != null)
      localCanvas.drawBitmap(paramBitmap, paramMatrix, null);
    while (true)
    {
      this.imageToDisplayIsValid = true;
      break;
      localCanvas.drawBitmap(paramBitmap, null, new Rect(0, 0, this.imageToDisplay.getWidth(), this.imageToDisplay.getHeight()), null);
    }
  }

  public void setListener(Listener paramListener)
  {
    this.listener = paramListener;
  }

  public static abstract interface Listener
  {
    public abstract void onSmudgeComplete(SmudgeView.Smudge paramSmudge);

    public abstract void onSmudgeProgress(SmudgeView.Smudge paramSmudge, Rect paramRect);

    public abstract void onSmudgeStarted();

    public abstract void onZoom();
  }

  public static class Smudge
  {
    public static final Smudge ALL = new Smudge(null, false, false, null);
    public static final Smudge NONE = new Smudge(null, false, false, null);
    public final boolean isMultipass;
    public final Picture mask;
    public final boolean oneLineSelection;
    public final List<Point> points;

    public Smudge(Picture paramPicture, boolean paramBoolean1, boolean paramBoolean2, List<Point> paramList)
    {
      this.mask = paramPicture;
      this.oneLineSelection = paramBoolean1;
      this.isMultipass = paramBoolean2;
      this.points = paramList;
    }

    public boolean isHighlighted(int paramInt1, int paramInt2)
    {
      Size localSize = this.mask.getSize();
      return (paramInt1 > 0) && (paramInt1 < localSize.width) && (paramInt2 > 0) && (paramInt2 < localSize.height) && (Color.alpha(this.mask.peekBitmap().getPixel(paramInt1, paramInt2)) > 0);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.unveil.textinput.SmudgeView
 * JD-Core Version:    0.6.2
 */