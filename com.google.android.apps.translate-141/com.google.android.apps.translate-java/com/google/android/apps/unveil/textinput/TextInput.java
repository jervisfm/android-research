package com.google.android.apps.unveil.textinput;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.media.AudioManager;
import android.os.Handler;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;
import com.google.android.apps.unveil.UnveilContext;
import com.google.android.apps.unveil.env.HoneycombAsyncTask;
import com.google.android.apps.unveil.env.ImageUtils;
import com.google.android.apps.unveil.env.Picture;
import com.google.android.apps.unveil.env.PictureFactory;
import com.google.android.apps.unveil.env.Provider;
import com.google.android.apps.unveil.env.Providers;
import com.google.android.apps.unveil.env.Size;
import com.google.android.apps.unveil.env.UnveilLogger;
import com.google.android.apps.unveil.env.Viewport;
import com.google.android.apps.unveil.network.AbstractConnector;
import com.google.android.apps.unveil.network.AbstractConnector.ResponseHandler;
import com.google.android.apps.unveil.network.HttpClientConnector;
import com.google.android.apps.unveil.network.NetworkInfoProvider;
import com.google.android.apps.unveil.network.UnveilResponse;
import com.google.android.apps.unveil.nonstop.DebugView;
import com.google.android.apps.unveil.nonstop.FrameProcessor;
import com.google.android.apps.unveil.nonstop.ImageBlurProcessor;
import com.google.android.apps.unveil.nonstop.PreviewLooper;
import com.google.android.apps.unveil.nonstop.StatusFrameProcessor;
import com.google.android.apps.unveil.nonstop.TimestampedFrame;
import com.google.android.apps.unveil.protocol.QueryBuilder;
import com.google.android.apps.unveil.protocol.QueryManager;
import com.google.android.apps.unveil.protocol.QueryManagerParams;
import com.google.android.apps.unveil.protocol.QueryResponseFactory.QueryType;
import com.google.android.apps.unveil.protocol.TraceTracker;
import com.google.android.apps.unveil.protocol.TracingCookieFetcher;
import com.google.android.apps.unveil.protocol.nonstop.FrameEncoder;
import com.google.android.apps.unveil.protocol.nonstop.FrameEncoder.EncodedFrame;
import com.google.android.apps.unveil.protocol.nonstop.TimestampedFrameDebugDrawer;
import com.google.android.apps.unveil.sensors.CameraManager;
import com.google.android.apps.unveil.sensors.CameraManager.FocusCallback;
import com.google.android.apps.unveil.sensors.CameraManager.Listener;
import com.google.android.apps.unveil.sensors.CameraManager.PictureCallback;
import com.google.android.apps.unveil.sensors.CameraManager.PictureQuality;
import com.google.android.apps.unveil.sensors.UnveilSensorProvider;
import com.google.android.apps.unveil.sensors.proxies.camera.RealCamera;
import com.google.goggles.GogglesProtos.GogglesRequest.Source;
import com.google.goggles.TracingProtos.PointVariable.Type;
import com.google.goggles.TracingProtos.SpanVariable.Type;
import com.google.goggles.TracingProtos.TraceRequest;
import com.google.goggles.TracingProtos.TraceRequest.Builder;
import com.google.goggles.TracingProtos.TraceResponse;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicReference;

public class TextInput extends FrameLayout
  implements SmudgeView.Listener, TextQueryListener.Listener, CameraManager.Listener
{
  private static final UnveilLogger logger = new UnveilLogger();
  private AudioManager audioManager;
  private boolean autoFocus = true;
  private AutoFocusProcessor autoFocusProcessor;
  private CountDownLatch blockingCaptureLatch = new CountDownLatch(0);
  private BoundingBoxView boundingBoxView;
  private boolean cameraLayoutComplete = false;
  private CameraManager cameraManager;
  private Matrix capturedToCroppedMatrix;
  private ClientType clientType = ClientType.FELIX_TEXT;
  private AbstractConnector connector;
  private DebugView debugView;
  private View gridView;
  private boolean imageLogging = false;
  private Picture imageToSend;
  private volatile boolean isSnapping;
  private Listener listener;
  private PreviewLooper previewLooper;
  private long queryId;
  private TextQueryListener queryListener;
  private QueryManager queryManager;
  private RecentFrame recentFrame;
  private SmudgeView smudgeView;
  private String sourceLanguage;
  private final String[] supportedLanguages;
  private TextMasker textMasker;
  private QueryBuilder textQuery;
  private TraceTracker traceTracker;
  private Handler uiHandler;
  private String userAgent;
  private ZoomableContainer zoomableContainer;

  public TextInput(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    this.supportedLanguages = paramContext.getResources().getStringArray(R.array.text_input_languages);
    this.userAgent = paramContext.getResources().getString(R.string.text_input_default_user_agent);
    String str = getResources().getString(R.string.text_input_frontend_url);
    logger.d("Frontend is " + str, new Object[0]);
    setConnector(getContext(), str);
  }

  private void asyncCaptureAndSendFrame(final TextQueryListener paramTextQueryListener)
  {
    this.blockingCaptureLatch = new CountDownLatch(1);
    new HoneycombAsyncTask()
    {
      protected Void doInBackground()
      {
        TextInput.this.blockingCaptureAndSendFrame(paramTextQueryListener);
        return null;
      }

      protected void onPostExecute(Void paramAnonymousVoid)
      {
      }
    }
    .execute(this.cameraManager.getExecutor(), this.uiHandler);
  }

  private void blockingCaptureAndSendFrame(TextQueryListener paramTextQueryListener)
  {
    final AtomicReference localAtomicReference = new AtomicReference();
    this.audioManager.setStreamMute(1, true);
    this.cameraManager.takePicture(null, new CameraManager.PictureCallback()
    {
      public void onPictureTaken(Picture paramAnonymousPicture)
      {
        TextInput.this.traceTracker.beginInterval(TracingProtos.SpanVariable.Type.IMAGE_REENCODE);
        localAtomicReference.set(paramAnonymousPicture);
        TextInput.this.listener.onPictureTaken();
        TextInput.this.blockingCaptureLatch.countDown();
      }
    });
    try
    {
      this.blockingCaptureLatch.await();
      localBitmap = allocateBitmapAndMatriciesForCapture();
      if ((localBitmap == null) || (localAtomicReference.get() == null))
        return;
    }
    catch (InterruptedException localInterruptedException)
    {
      Bitmap localBitmap;
      while (true)
        localInterruptedException.printStackTrace();
      new Canvas(localBitmap).drawBitmap(((Picture)localAtomicReference.get()).peekBitmap(), this.capturedToCroppedMatrix, null);
      this.imageToSend = PictureFactory.createBitmap(localBitmap, 90);
      this.traceTracker.endInterval(TracingProtos.SpanVariable.Type.IMAGE_REENCODE);
      this.traceTracker.endInterval(TracingProtos.SpanVariable.Type.ACQUIRE_TO_IMAGE);
      paramTextQueryListener.setPictureSize(this.imageToSend.getSize());
      this.textQuery = new QueryBuilder().addMsSinceEpoch(Long.valueOf(System.currentTimeMillis())).addImageData(this.imageToSend.getJpegData(), this.imageToSend.getSize()).addSourceLanguage(this.sourceLanguage).setCanLogImage(this.imageLogging);
      this.textQuery.setQueryType(QueryResponseFactory.QueryType.FELIX_TEXT_IMAGE);
      this.textQuery.setSource(this.clientType.source);
      this.queryManager.sendQuery(this.textQuery, this.textQuery.buildGogglesRequestBuilder(), paramTextQueryListener, false);
      this.smudgeView.setImageToDisplay(this.imageToSend.peekBitmap(), null);
      this.cameraManager.forceReleaseCamera();
    }
  }

  private int calculatePreviewTranslation()
  {
    int[] arrayOfInt = new int[2];
    this.cameraManager.getLocationInWindow(arrayOfInt);
    int i = arrayOfInt[1];
    this.smudgeView.getLocationInWindow(arrayOfInt);
    return arrayOfInt[1] - i;
  }

  public static <T> View findViewByType(View paramView, Class<T> paramClass)
  {
    View localView = findViewByTypeInner(paramView, paramClass);
    if (localView == null)
      throw new RuntimeException("Can not find expected view of " + paramClass.getName() + ". Did you forget to define it in the xml layout?");
    return localView;
  }

  private static <T> View findViewByTypeInner(View paramView, Class<T> paramClass)
  {
    if (!(paramView instanceof ViewGroup))
    {
      if (paramView.getClass().equals(paramClass))
        return paramView;
      return null;
    }
    int i = ((ViewGroup)paramView).getChildCount();
    for (int j = 0; j < i; j++)
    {
      View localView1 = ((ViewGroup)paramView).getChildAt(j);
      if (localView1.getClass().equals(paramClass))
        return localView1;
      if ((localView1 instanceof ViewGroup))
      {
        View localView2 = findViewByTypeInner(localView1, paramClass);
        if (localView2 != null)
          return localView2;
      }
    }
    return null;
  }

  private void load(Picture paramPicture)
  {
    try
    {
      this.imageToSend = paramPicture;
      AnimationHelper.alpha(this.gridView, 1.0F, 0.0F, 600L, null, null);
      this.smudgeView.clearSmudges();
      this.smudgeView.setImageToDisplay(paramPicture.peekBitmap(), null);
      AnimationHelper.alpha(this.smudgeView, 1.0F, 1.0F, 600L, new DecelerateInterpolator(), null);
      this.queryId = SystemClock.uptimeMillis();
      TextQueryListener localTextQueryListener = new TextQueryListener(this.boundingBoxView, this.textMasker, this.queryId, new TextQueryListener.Listener[] { this });
      localTextQueryListener.setPictureSize(this.imageToSend.getSize());
      setQueryListener(localTextQueryListener);
      this.previewLooper.pauseLoop();
      this.cameraManager.forceReleaseCamera();
      this.smudgeView.setAcceptSmudges(true);
      this.textMasker.clearWords();
      this.textMasker.setSmudge(SmudgeView.Smudge.NONE);
      this.boundingBoxView.setVisibility(8);
      this.zoomableContainer.reset();
      this.textQuery = new QueryBuilder().addMsSinceEpoch(Long.valueOf(System.currentTimeMillis())).addImageData(this.imageToSend.getJpegData(), this.imageToSend.getSize()).addSourceLanguage(this.sourceLanguage).setCanLogImage(this.imageLogging);
      this.textQuery.setQueryType(QueryResponseFactory.QueryType.FELIX_TEXT_IMAGE);
      this.textQuery.setSource(this.clientType.source);
      this.queryManager.sendQuery(this.textQuery, this.textQuery.buildGogglesRequestBuilder(), localTextQueryListener, false);
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  private void maybeSendTraces()
  {
    if (!this.traceTracker.hasPendingActions())
      return;
    TracingProtos.TraceRequest.Builder localBuilder = TracingProtos.TraceRequest.newBuilder();
    this.traceTracker.populateRequest(localBuilder);
    this.connector.sendRequest(localBuilder.build(), TracingProtos.TraceResponse.class, new AbstractConnector.ResponseHandler()
    {
      public void onNetworkError()
      {
        TextInput.logger.e("Failed to send traces.", new Object[0]);
      }

      public void onResponse(UnveilResponse<TracingProtos.TraceResponse> paramAnonymousUnveilResponse)
      {
        TextInput.logger.d("Successfully sent traces.", new Object[0]);
      }

      public void onServerErrorCode(int paramAnonymousInt)
      {
        UnveilLogger localUnveilLogger = TextInput.logger;
        Object[] arrayOfObject = new Object[1];
        arrayOfObject[0] = Integer.valueOf(paramAnonymousInt);
        localUnveilLogger.e("failed to send traces with status code %d", arrayOfObject);
      }
    }
    , "");
  }

  private void maybeTriggerFocus(CameraManager.FocusCallback paramFocusCallback)
  {
    if ((this.autoFocusProcessor != null) && (this.autoFocusProcessor.shouldFocus()))
    {
      logger.d("Trigger focus before taking picture.", new Object[0]);
      this.cameraManager.focus(paramFocusCallback);
      return;
    }
    logger.d("Already in focus, no focus necessary.", new Object[0]);
    paramFocusCallback.onFocus(true);
  }

  private QueryManager newQueryManager()
  {
    return new QueryManager(this.traceTracker, new QueryManagerParams(new Provider()
    {
      public Configuration get()
      {
        return TextInput.this.getResources().getConfiguration();
      }
    }), Collections.emptyList(), this.connector, Providers.staticProvider(new Viewport(Viewport.computeNaturalOrientation(getContext()))), Providers.staticProvider(Boolean.valueOf(false)));
  }

  private boolean restart()
  {
    try
    {
      int i;
      if ((this.previewLooper != null) && (this.previewLooper.isRunning()))
      {
        i = 1;
        if ((this.previewLooper != null) && (!this.previewLooper.isRunning()))
          this.uiHandler.post(new Runnable()
          {
            public void run()
            {
              TextInput.this.startCameraPreviewAndLooper();
            }
          });
        setQueryListener(null);
        this.textQuery = null;
        this.imageToSend = null;
        this.smudgeView.setAcceptSmudges(false);
        AnimationHelper.alpha(this.smudgeView, this.smudgeView.getAlphaValue(), 0.0F, 400L, null, new Runnable()
        {
          public void run()
          {
            TextInput.this.smudgeView.setImageToDisplay(null, null);
          }
        });
        AnimationHelper.alpha(this.gridView, 0.0F, 1.0F, 400L, null, null);
        this.textMasker.clearWords();
        this.textMasker.setSmudge(SmudgeView.Smudge.NONE);
        this.boundingBoxView.setVisibility(8);
        this.zoomableContainer.reset();
        this.isSnapping = false;
        this.listener.onRestart();
        if (i != 0)
          break label180;
      }
      label180: for (boolean bool = true; ; bool = false)
      {
        return bool;
        i = 0;
        break;
      }
    }
    finally
    {
    }
  }

  private void setConnector(Context paramContext, String paramString)
  {
    this.connector = HttpClientConnector.newHttpClientConnector(paramContext, paramString, new Provider()
    {
      public String get()
      {
        return TextInput.this.userAgent;
      }
    }
    , InstallationIdHelper.getInstallationId(paramContext));
    this.traceTracker = new TraceTracker(new NetworkInfoProvider(paramContext)
    {
      public int getSignalStrength()
      {
        return -1;
      }
    }
    , new TracingCookieFetcher(this.connector));
    this.queryManager = newQueryManager();
  }

  private void setQueryListener(TextQueryListener paramTextQueryListener)
  {
    if (this.queryListener != null)
      this.queryListener.cancel(true);
    this.queryListener = paramTextQueryListener;
  }

  private void showRecentFrame()
  {
    Matrix localMatrix = new Matrix();
    Picture localPicture = this.recentFrame.get();
    int i = CameraManager.getCameraToDisplayRotation(getContext());
    if (localPicture != null)
      if ((i == 90) || (i == 270))
      {
        float f1 = Math.min(localPicture.getSize().width / 2, localPicture.getSize().height / 2);
        localMatrix.postRotate(i, f1, f1);
        if (i == 270)
          localMatrix.postTranslate(0.0F, localPicture.getSize().width - localPicture.getSize().height);
        float f2 = this.cameraManager.getWidth() / localPicture.getSize().height;
        localMatrix.postScale(f2, f2);
        localMatrix.postTranslate(0.0F, -calculatePreviewTranslation());
        this.smudgeView.setImageToDisplay(localPicture.peekBitmap(), localMatrix);
        localPicture.recycle();
      }
    while (true)
    {
      AnimationHelper.alpha(this.smudgeView, 1.0F, 1.0F, 600L, new DecelerateInterpolator(), null);
      return;
      localMatrix.postRotate(i, localPicture.getSize().width / 2, localPicture.getSize().height / 2);
      float f3 = this.cameraManager.getWidth() / localPicture.getSize().width;
      localMatrix.postScale(f3, f3);
      break;
      this.smudgeView.setImageToDisplay(null, null);
    }
  }

  private void startCameraPreviewAndLooper()
  {
    while (true)
    {
      try
      {
        if (this.cameraLayoutComplete)
        {
          boolean bool = this.cameraManager.isCameraConnected();
          if (bool);
        }
        else
        {
          return;
        }
        this.cameraManager.startPreview();
        if (this.previewLooper == null)
        {
          this.previewLooper = new PreviewLooper(this.cameraManager, CameraManager.getCameraToDisplayRotation(getContext()), 30.0F, 3);
          this.previewLooper.addPreviewProcessor(this.recentFrame, 0);
          this.previewLooper.addPreviewProcessor(this.autoFocusProcessor, 1);
          this.previewLooper.addPreviewProcessor(new StatusFrameProcessor(this.cameraManager, new UnveilSensorProvider(getContext()), this.debugView, null), 0);
          this.debugView.setCallback(this.previewLooper);
          Size localSize = this.cameraManager.getPreviewSize();
          int i = CameraManager.getCameraToDisplayRotation(getContext());
          if ((i == 90) || (i == 270))
            localSize = new Size(localSize.height, localSize.width);
          logger.d("preview size is " + localSize, new Object[0]);
          this.previewLooper.startLoop(localSize);
          continue;
        }
      }
      finally
      {
      }
      this.previewLooper.pauseLoop();
    }
  }

  protected Bitmap allocateBitmapAndMatriciesForCapture()
  {
    Size localSize1 = new Size(this.smudgeView.getWidth(), this.smudgeView.getHeight());
    Object localObject = this.cameraManager.getPictureSize();
    Size localSize2 = this.cameraManager.getPreviewSize();
    UnveilLogger localUnveilLogger = logger;
    Object[] arrayOfObject = new Object[5];
    arrayOfObject[0] = localSize1;
    arrayOfObject[1] = localObject;
    arrayOfObject[2] = localSize2;
    arrayOfObject[3] = Integer.valueOf(this.cameraManager.getWidth());
    arrayOfObject[4] = Integer.valueOf(this.cameraManager.getHeight());
    localUnveilLogger.d("allocateBitmapsForCapture:displayedSize(%s), capturedImageSize(%s), cameraPreviewSize(%s), cameraManager(%d x %d)", arrayOfObject);
    if ((localObject == null) || (localSize2 == null))
    {
      logger.e("Failed to get imageSize or previewSize", new Object[0]);
      return null;
    }
    int i = CameraManager.getCameraToDisplayRotation(getContext());
    if ((i == 90) || (i == 270))
    {
      Size localSize3 = new Size(((Size)localObject).height, ((Size)localObject).width);
      localSize2 = new Size(localSize2.height, localSize2.width);
      localObject = localSize3;
    }
    float f1 = Math.min(((Size)localObject).width / localSize2.width, ((Size)localObject).height / localSize2.height);
    Size localSize4 = new Size((int)(f1 * localSize2.width), (int)(f1 * localSize2.height));
    Matrix localMatrix = ImageUtils.generateUndistortTransformer(localSize4, localSize1);
    float[] arrayOfFloat = new float[9];
    localMatrix.getValues(arrayOfFloat);
    float f2 = arrayOfFloat[0];
    float f3 = arrayOfFloat[4];
    Bitmap localBitmap = Bitmap.createBitmap((int)(localSize4.width / f2), (int)(localSize4.height / f3), Bitmap.Config.RGB_565);
    this.capturedToCroppedMatrix = new Matrix();
    if ((i == 90) || (i == 270))
    {
      float f4 = Math.min(((Size)localObject).height / 2, ((Size)localObject).width / 2);
      this.capturedToCroppedMatrix.postRotate(i, f4, f4);
      if (i == 270)
        this.capturedToCroppedMatrix.postTranslate(0.0F, ((Size)localObject).height - ((Size)localObject).width);
    }
    while (true)
    {
      int j = (int)(Math.min(((Size)localObject).width / this.cameraManager.getWidth(), ((Size)localObject).height / this.cameraManager.getHeight()) * calculatePreviewTranslation());
      this.capturedToCroppedMatrix.postTranslate(-(((Size)localObject).width - localSize4.width) / 2, -(((Size)localObject).height - localSize4.height) / 2 - j);
      return localBitmap;
      if (i == 180)
        this.capturedToCroppedMatrix.postRotate(180.0F, ((Size)localObject).width / 2, ((Size)localObject).height / 2);
    }
  }

  public boolean changeSourceLanguage(String paramString)
  {
    try
    {
      String str = this.sourceLanguage;
      boolean bool1 = false;
      if (paramString == str);
      while (true)
      {
        return bool1;
        if (!TextUtils.isEmpty(paramString))
        {
          boolean bool2 = paramString.equals(this.sourceLanguage);
          bool1 = false;
          if (bool2);
        }
        else
        {
          this.sourceLanguage = paramString;
          QueryBuilder localQueryBuilder = this.textQuery;
          bool1 = false;
          if (localQueryBuilder != null)
          {
            load(this.imageToSend);
            bool1 = true;
          }
        }
      }
    }
    finally
    {
    }
  }

  public void cycleDebugMode(boolean paramBoolean)
  {
    this.debugView.cycleDebugMode(paramBoolean);
  }

  public void finishInput()
  {
    logger.d("finish input explicitly.", new Object[0]);
    this.imageToSend = null;
    if (this.previewLooper != null)
      this.previewLooper.pauseLoop();
    this.blockingCaptureLatch.countDown();
    this.cameraManager.stopPreview();
    this.cameraManager.forceReleaseCamera();
    this.traceTracker.tryToEndTraceAction(this.traceTracker.getCurrentActionNumber());
    maybeSendTraces();
    setVisibility(8);
    this.cameraManager.setVisibility(8);
    this.cameraLayoutComplete = false;
    this.previewLooper = null;
  }

  public String[] getSupportedLanguages()
  {
    return this.supportedLanguages;
  }

  public void gotResult(String paramString)
  {
    logger.d("got result: %s", new Object[] { paramString });
  }

  public boolean isFrozen()
  {
    return (this.previewLooper != null) && (!this.previewLooper.isRunning());
  }

  public void loadImage(Picture paramPicture)
  {
    try
    {
      Size localSize1 = new Size(this.smudgeView.getWidth(), this.smudgeView.getHeight());
      Size localSize2 = paramPicture.getSize();
      float f = 1.0F / Math.max(localSize2.width / localSize1.width, localSize2.height / localSize1.height);
      int i = (int)(f * localSize2.width);
      int j = (int)(f * localSize2.height);
      Bitmap localBitmap1 = Bitmap.createBitmap(localSize1.width, localSize1.height, Bitmap.Config.ARGB_8888);
      Canvas localCanvas = new Canvas(localBitmap1);
      int k = (localSize1.width - i) / 2;
      int m = (localSize1.width - j) / 2;
      Bitmap localBitmap2 = paramPicture.peekBitmap();
      Rect localRect = new Rect(k, m, k + i, m + j);
      localCanvas.drawBitmap(localBitmap2, null, localRect, null);
      load(PictureFactory.createBitmap(localBitmap1, 0));
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public void noResults()
  {
    logger.d("no results.", new Object[0]);
    this.traceTracker.endInterval(TracingProtos.SpanVariable.Type.START_TO_RENDERED);
    this.listener.noResults();
  }

  public void onCameraAcquisitionError()
  {
    logger.e("Failed to acquire camera.", new Object[0]);
    this.listener.onCameraError();
  }

  public void onCameraConnected()
  {
    logger.i("Camera connected", new Object[0]);
    setVisibility(0);
    startCameraPreviewAndLooper();
  }

  public void onCameraFlashControlError()
  {
    logger.e("Failed to apply camera flash setting.", new Object[0]);
  }

  public void onCameraLayoutComplete()
  {
    logger.i("Camera layout completed.", new Object[0]);
    this.cameraLayoutComplete = true;
    startCameraPreviewAndLooper();
    ViewGroup.LayoutParams localLayoutParams = this.zoomableContainer.getLayoutParams();
    localLayoutParams.width = this.cameraManager.getWidth();
    this.zoomableContainer.setLayoutParams(localLayoutParams);
    this.zoomableContainer.post(new Runnable()
    {
      public void run()
      {
        TextInput.this.zoomableContainer.requestLayout();
      }
    });
  }

  public void onCameraPreviewSizeChanged()
  {
    try
    {
      logger.i("Camera preview size changed.", new Object[0]);
      setAutoFocus(this.autoFocus);
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public void onCameraQualityError()
  {
    logger.e("Failed to apply camera quality settings.", new Object[0]);
  }

  protected void onFinishInflate()
  {
    super.onFinishInflate();
    Context localContext = getContext();
    this.cameraManager = ((CameraManager)findViewByType(this, CameraManager.class));
    this.cameraManager.setAcquireCameraOnVisibilityChange(false);
    this.cameraManager.registerListener(this);
    this.cameraManager.requestPictureQuality(CameraManager.PictureQuality.HIGH_QUALITY);
    this.zoomableContainer = ((ZoomableContainer)findViewByType(this, ZoomableContainer.class));
    this.smudgeView = ((SmudgeView)findViewByType(this, SmudgeView.class));
    this.boundingBoxView = ((BoundingBoxView)findViewByType(this, BoundingBoxView.class));
    this.gridView = findViewByType(this, GridOverlayView.class);
    this.debugView = ((DebugView)findViewByType(this, DebugView.class));
    this.textMasker = new TextMasker(localContext);
    this.boundingBoxView.setTextMasker(this.textMasker);
    this.smudgeView.setListener(this);
    this.smudgeView.setAcceptSmudges(false);
    this.uiHandler = new Handler();
    this.audioManager = ((AudioManager)localContext.getSystemService("audio"));
    this.recentFrame = new RecentFrame(null);
    this.autoFocusProcessor = new AutoFocusProcessor(this.cameraManager);
    if (!(localContext.getApplicationContext() instanceof UnveilContext))
      this.cameraManager.init(Providers.staticProvider(new UnveilSensorProvider(localContext)), Providers.staticProvider(Boolean.valueOf(false)), Providers.staticProvider(RealCamera.class.getSimpleName()), new Provider()
      {
        public Map<String, String> get()
        {
          return new HashMap();
        }
      });
    setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        TextInput.this.snap();
      }
    });
    setVisibility(8);
  }

  public void onNetworkError(int paramInt)
  {
    UnveilLogger localUnveilLogger = logger;
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = Integer.valueOf(paramInt);
    localUnveilLogger.d("on network error: %d", arrayOfObject);
    this.listener.onNetworkError(paramInt);
  }

  public void onResult()
  {
    logger.d("on result.", new Object[0]);
    this.traceTracker.endInterval(TracingProtos.SpanVariable.Type.START_TO_RENDERED);
    this.listener.onResult(this.textMasker.getSelectedWordsAsString(), null);
  }

  public void onSmudgeComplete(SmudgeView.Smudge paramSmudge)
  {
    logger.d("on smudge complete.", new Object[0]);
    if ((this.queryListener.queryId != this.queryId) || (this.queryListener.isCanceled()))
      return;
    this.textMasker.setSmudge(paramSmudge);
    if (this.queryListener.textResponseRecieved())
    {
      this.boundingBoxView.setVisibility(0);
      this.boundingBoxView.invalidate();
      this.listener.onResult(this.textMasker.getSelectedWordsAsString(), null);
      return;
    }
    this.listener.onSearching(this.queryId);
  }

  public void onSmudgeProgress(SmudgeView.Smudge paramSmudge, Rect paramRect)
  {
    logger.d("on smudge progress.", new Object[0]);
    if ((this.queryListener.queryId != this.queryId) || (!this.queryListener.textResponseRecieved()))
      return;
    this.textMasker.setSmudge(paramSmudge);
    this.boundingBoxView.setVisibility(0);
    this.boundingBoxView.invalidate(paramRect);
  }

  public void onSmudgeStarted()
  {
    logger.d("on smudge started.", new Object[0]);
    this.smudgeView.clearSmudges();
  }

  public void onZoom()
  {
    this.listener.onZoom();
  }

  public void setAutoFocus(boolean paramBoolean)
  {
    this.autoFocus = paramBoolean;
    this.cameraManager.setFocusable(true);
    if (!this.cameraManager.isFocusSupported())
    {
      logger.d("camera does not support focus.", new Object[0]);
      return;
    }
    if (this.cameraManager.isContinuousFocusSupported())
    {
      this.cameraManager.enableContinuousFocus(paramBoolean);
      logger.d("camera supports continuous focus.", new Object[0]);
      return;
    }
    logger.d("use image blurriness based auto focus.", new Object[0]);
  }

  public void setClientType(ClientType paramClientType)
  {
    this.clientType = paramClientType;
  }

  public void setFrontendUrl(String paramString)
  {
    setConnector(getContext(), paramString);
  }

  public void setImageLogging(boolean paramBoolean)
  {
    this.imageLogging = paramBoolean;
  }

  public void setListener(Listener paramListener)
  {
    this.listener = paramListener;
  }

  public void setUserAgent(String paramString)
  {
    this.userAgent = paramString;
  }

  public void snap()
  {
    try
    {
      if (this.isSnapping)
        logger.d("You already snapped, ignoring duplicate snap request.", new Object[0]);
      while (true)
      {
        return;
        this.isSnapping = true;
        this.traceTracker.addPoint(TracingProtos.PointVariable.Type.USER);
        this.traceTracker.beginInterval(TracingProtos.SpanVariable.Type.START_TO_RENDERED);
        this.traceTracker.beginInterval(TracingProtos.SpanVariable.Type.ACQUIRE_TO_IMAGE);
        AnimationHelper.alpha(this.gridView, 1.0F, 0.0F, 600L, null, null);
        this.smudgeView.clearSmudges();
        this.queryId = SystemClock.uptimeMillis();
        setQueryListener(new TextQueryListener(this.boundingBoxView, this.textMasker, this.queryId, new TextQueryListener.Listener[] { this }));
        maybeTriggerFocus(new CameraManager.FocusCallback()
        {
          public void onFocus(boolean paramAnonymousBoolean)
          {
            UnveilLogger localUnveilLogger = TextInput.logger;
            Object[] arrayOfObject = new Object[1];
            if (paramAnonymousBoolean);
            for (String str = "Succeed"; ; str = "Failed")
            {
              arrayOfObject[0] = str;
              localUnveilLogger.d("Camera focus completed. %s", arrayOfObject);
              TextInput.this.showRecentFrame();
              TextInput.this.previewLooper.pauseLoop();
              TextInput.this.listener.onPreviewFrozen();
              TextInput.this.asyncCaptureAndSendFrame(TextInput.this.queryListener);
              return;
            }
          }
        });
        this.smudgeView.setAcceptSmudges(true);
      }
    }
    finally
    {
    }
  }

  public boolean startInput(String paramString)
  {
    UnveilLogger localUnveilLogger = logger;
    Object[] arrayOfObject = new Object[1];
    if (TextUtils.isEmpty(paramString));
    for (String str = "auto-detect"; ; str = paramString)
    {
      arrayOfObject[0] = str;
      localUnveilLogger.d("start input explicitly, language %s", arrayOfObject);
      this.cameraManager.setVisibility(0);
      this.cameraManager.acquireCamera();
      this.sourceLanguage = paramString;
      this.traceTracker.startNewTraceAction();
      this.traceTracker.addPoint(TracingProtos.PointVariable.Type.CAMERA_PREVIEW_START);
      return restart();
    }
  }

  private class AutoFocusProcessor extends ImageBlurProcessor
  {
    public AutoFocusProcessor(CameraManager arg2)
    {
      super();
    }

    protected void onProcessFrame(TimestampedFrame paramTimestampedFrame)
    {
      if (!TextInput.this.cameraManager.isFocusSupported());
      while ((TextInput.this.cameraManager.isContinuousFocusSupported()) || (!TextInput.this.autoFocus))
        return;
      super.onProcessFrame(paramTimestampedFrame);
    }

    public boolean shouldFocus()
    {
      if (!TextInput.this.cameraManager.isFocusSupported());
      while (TextInput.this.cameraManager.isContinuousFocusSupported())
        return false;
      if (!TextInput.this.autoFocus)
        return true;
      return super.isLastFrameBlurred();
    }
  }

  public static enum ClientType
  {
    public final GogglesProtos.GogglesRequest.Source source;

    static
    {
      ClientType[] arrayOfClientType = new ClientType[2];
      arrayOfClientType[0] = FELIX_TEXT;
      arrayOfClientType[1] = TRANSLATE;
    }

    private ClientType(GogglesProtos.GogglesRequest.Source paramSource)
    {
      this.source = paramSource;
    }
  }

  public static abstract interface Listener
  {
    public abstract void noResults();

    public abstract void onCameraError();

    public abstract void onNetworkError(int paramInt);

    public abstract void onPictureTaken();

    public abstract void onPreviewFrozen();

    public abstract void onRestart();

    public abstract void onResult(String paramString, String[] paramArrayOfString);

    public abstract void onSearching(long paramLong);

    public abstract void onZoom();
  }

  private class RecentFrame extends FrameProcessor
  {
    private TimestampedFrame frame;

    private RecentFrame()
    {
    }

    public void finalize()
    {
      if (this.frame != null)
      {
        this.frame.removeReference();
        this.frame = null;
      }
    }

    public Picture get()
    {
      if (this.frame == null)
        return null;
      UnveilLogger localUnveilLogger = TextInput.logger;
      Object[] arrayOfObject = new Object[2];
      arrayOfObject[0] = Integer.valueOf(this.frame.getWidth());
      arrayOfObject[1] = Integer.valueOf(this.frame.getHeight());
      localUnveilLogger.d("get recent frame: %dx%d", arrayOfObject);
      Picture localPicture = PictureFactory.createBitmap(this.frame.getRawData(), this.frame.getWidth(), this.frame.getHeight(), 0);
      this.frame.removeReference();
      this.frame = null;
      return localPicture;
    }

    protected void onDrawDebug(Canvas paramCanvas)
    {
      if (this.frame != null)
      {
        FrameEncoder.EncodedFrame localEncodedFrame = new FrameEncoder().encode(this.frame);
        new TimestampedFrameDebugDrawer(this.frame, localEncodedFrame.jpegData).draw(paramCanvas, 0, 100.0F);
      }
    }

    protected void onPause()
    {
      finalize();
    }

    protected void onProcessFrame(TimestampedFrame paramTimestampedFrame)
    {
      if (this.frame != null)
        this.frame.removeReference();
      this.frame = paramTimestampedFrame;
      this.frame.addReference();
    }

    protected void onShutdown()
    {
      finalize();
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.unveil.textinput.TextInput
 * JD-Core Version:    0.6.2
 */