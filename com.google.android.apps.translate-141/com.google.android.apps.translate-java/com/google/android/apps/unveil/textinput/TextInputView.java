package com.google.android.apps.unveil.textinput;

import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.net.Uri;
import android.os.Build.VERSION;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.FrameLayout;
import com.google.android.apps.unveil.env.Picture;
import com.google.android.apps.unveil.env.PictureFactory;
import com.google.android.apps.unveil.env.PictureLoadingException;
import com.google.android.apps.unveil.env.PixelUtils;
import com.google.android.apps.unveil.env.UnveilLogger;
import com.google.android.apps.unveil.env.media.ImageLoader.Image;
import com.google.android.apps.unveil.env.media.InvalidUriException;
import com.google.android.apps.unveil.env.media.MediaStoreMergerAdapter;

public class TextInputView extends FrameLayout
  implements TextInput.Listener
{
  private static final UnveilLogger logger = new UnveilLogger();
  private String customHeight;
  private InstructionsView instructions;
  private View keyboardButton;
  private Listener listener;
  private View loadButton;
  private View progressBar;
  private View retryButton;
  private View snapButton;
  private String sourceLanguage;
  private TextInput textInput;

  public TextInputView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    addView(createView(paramContext));
  }

  public static final boolean isSupported(Context paramContext)
  {
    if (Build.VERSION.SDK_INT < 9);
    PackageManager localPackageManager;
    do
    {
      return false;
      localPackageManager = paramContext.getPackageManager();
    }
    while ((localPackageManager == null) || (!localPackageManager.hasSystemFeature("android.hardware.camera")) || (Camera.getNumberOfCameras() == 0));
    try
    {
      Camera.getCameraInfo(0, new Camera.CameraInfo());
      return true;
    }
    catch (RuntimeException localRuntimeException)
    {
      logger.e(localRuntimeException, "This device claims it has a camera, but attempted getCameraInfo failed", new Object[0]);
    }
    return false;
  }

  private int makeSizeSpec(String paramString, int paramInt)
  {
    int i = View.MeasureSpec.getMode(paramInt);
    if (i == 1073741824);
    while (true)
    {
      return paramInt;
      int j = View.MeasureSpec.getSize(paramInt);
      int k = j;
      if (paramString.endsWith("px"))
        k = Integer.valueOf(paramString.substring(0, -2 + paramString.length())).intValue();
      while ((i == 0) || ((i == -2147483648) && (j > k)))
      {
        return View.MeasureSpec.makeMeasureSpec(1073741824, k);
        if (paramString.endsWith("dp"))
          k = PixelUtils.dipToPix(Float.valueOf(paramString.substring(0, -2 + paramString.length())).floatValue(), getContext());
        else if (paramString.endsWith("in"))
          k = PixelUtils.inchToPix(Float.valueOf(paramString.substring(0, -2 + paramString.length())).floatValue(), getContext());
        else if ((paramString.endsWith("%")) && (i == -2147483648))
          k = (int)(j * Float.valueOf(paramString.substring(0, -1 + paramString.length())).floatValue() / 100.0F);
      }
    }
  }

  public boolean changeSourceLanguage(String paramString)
  {
    return this.textInput.changeSourceLanguage(paramString);
  }

  protected View createView(Context paramContext)
  {
    return View.inflate(paramContext, R.layout.text_input_view, null);
  }

  public void cycleDebugMode(boolean paramBoolean)
  {
    this.textInput.cycleDebugMode(paramBoolean);
  }

  public void finishInput()
  {
    this.textInput.finishInput();
  }

  public String[] getSupportedLanguages()
  {
    return this.textInput.getSupportedLanguages();
  }

  public boolean isFrozen()
  {
    return this.textInput.isFrozen();
  }

  public void loadImage(Uri paramUri)
  {
    try
    {
      Context localContext = getContext();
      ImageLoader.Image localImage = MediaStoreMergerAdapter.getImage(localContext.getContentResolver(), paramUri);
      if (localImage == null)
      {
        logger.e("Loaded null image for %s", new Object[] { paramUri });
        this.listener.onLoadImageError();
        return;
      }
      Picture localPicture = PictureFactory.loadAndDownsample(localContext, paramUri, localImage.orientation);
      this.textInput.loadImage(localPicture);
      return;
    }
    catch (InvalidUriException localInvalidUriException)
    {
      logger.e(localInvalidUriException, "Failed to load image, invalid uri: %s", new Object[] { paramUri });
      this.listener.onLoadImageError();
      return;
    }
    catch (PictureLoadingException localPictureLoadingException)
    {
      logger.e(localPictureLoadingException, "Failed to load image!", new Object[0]);
      this.listener.onLoadImageError();
    }
  }

  public void noResults()
  {
    logger.d("no results", new Object[0]);
    this.instructions.setVisibility(0);
    this.instructions.show(InstructionsView.Instruction.NO_TEXT);
    this.instructions.sendAccessibilityEvent(8);
    this.retryButton.setVisibility(0);
    this.snapButton.setVisibility(8);
    this.progressBar.setVisibility(4);
  }

  public void onCameraError()
  {
    this.listener.onCameraError();
  }

  protected void onFinishInflate()
  {
    super.onFinishInflate();
    getContext();
    this.textInput = ((TextInput)TextInput.findViewByType(this, TextInput.class));
    this.textInput.setListener(this);
    this.instructions = ((InstructionsView)findViewById(R.id.instructions));
    this.progressBar = findViewById(R.id.progress_bar);
    this.snapButton = findViewById(R.id.snapshot);
    this.snapButton.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        TextInputView.this.textInput.snap();
      }
    });
    this.retryButton = findViewById(R.id.retry);
    this.retryButton.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        TextInputView.this.textInput.startInput(TextInputView.this.sourceLanguage);
      }
    });
    this.keyboardButton = findViewById(R.id.keyboard);
    this.keyboardButton.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        TextInputView.this.listener.onKeyboardButton();
      }
    });
    this.loadButton = findViewById(R.id.load);
    this.loadButton.setOnClickListener(new View.OnClickListener()
    {
      public void onClick(View paramAnonymousView)
      {
        TextInputView.this.listener.onLoadImage();
      }
    });
    logger.d("text input view is created.", new Object[0]);
  }

  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    if (paramInt == 27)
    {
      this.textInput.snap();
      return true;
    }
    return super.onKeyDown(paramInt, paramKeyEvent);
  }

  public boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent)
  {
    if (paramInt == 27)
      return true;
    return super.onKeyDown(paramInt, paramKeyEvent);
  }

  public void onMeasure(int paramInt1, int paramInt2)
  {
    if (!TextUtils.isEmpty(this.customHeight))
    {
      paramInt2 = makeSizeSpec(this.customHeight, paramInt2);
      UnveilLogger localUnveilLogger = logger;
      Object[] arrayOfObject = new Object[1];
      arrayOfObject[0] = this.customHeight;
      localUnveilLogger.d("param.height is %s", arrayOfObject);
    }
    super.onMeasure(paramInt1, paramInt2);
  }

  public void onNetworkError(int paramInt)
  {
    UnveilLogger localUnveilLogger = logger;
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = Integer.valueOf(paramInt);
    localUnveilLogger.d("network error: %d", arrayOfObject);
    this.listener.onNetworkError(paramInt);
  }

  public void onPictureTaken()
  {
    this.instructions.setVisibility(0);
    this.instructions.show(InstructionsView.Instruction.USE_FINGER_TO_HIGHLIGHT);
    this.instructions.sendAccessibilityEvent(8);
  }

  public void onPreviewFrozen()
  {
    logger.d("on preview frozen", new Object[0]);
    this.instructions.setVisibility(0);
    this.instructions.show(InstructionsView.Instruction.HOLD_STEADY);
    this.instructions.sendAccessibilityEvent(8);
    this.retryButton.setVisibility(0);
    this.snapButton.setVisibility(8);
  }

  public void onRestart()
  {
    logger.d("on restart", new Object[0]);
    this.instructions.setVisibility(0);
    this.instructions.show(InstructionsView.Instruction.TAP_TO_READ);
    this.instructions.sendAccessibilityEvent(8);
    this.retryButton.setVisibility(8);
    this.snapButton.setVisibility(0);
    this.progressBar.setVisibility(4);
  }

  public void onResult(String paramString, String[] paramArrayOfString)
  {
    logger.d("on result '%s'", new Object[] { paramString });
    this.listener.onTextSelected(paramString, paramArrayOfString);
    this.retryButton.setVisibility(0);
    this.snapButton.setVisibility(8);
    this.progressBar.setVisibility(4);
    this.instructions.setVisibility(0);
  }

  public void onSearching(long paramLong)
  {
    UnveilLogger localUnveilLogger = logger;
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = Long.valueOf(paramLong);
    localUnveilLogger.d("on searching with query id %d", arrayOfObject);
    this.instructions.setVisibility(0);
    this.progressBar.setVisibility(0);
  }

  public void onZoom()
  {
    this.instructions.setVisibility(0);
    this.instructions.show(InstructionsView.Instruction.USE_TWO_FINGERS_TO_PAN);
  }

  public void setAutoFocus(boolean paramBoolean)
  {
    this.textInput.setAutoFocus(paramBoolean);
  }

  public void setClientType(TextInput.ClientType paramClientType)
  {
    this.textInput.setClientType(paramClientType);
  }

  public void setFrontendUrl(String paramString)
  {
    this.textInput.setFrontendUrl(paramString);
  }

  public void setImageLogging(boolean paramBoolean)
  {
    this.textInput.setImageLogging(paramBoolean);
  }

  public void setListener(Listener paramListener)
  {
    logger.d("set listener", new Object[0]);
    this.listener = paramListener;
  }

  public void setUserAgent(String paramString)
  {
    this.textInput.setUserAgent(paramString);
  }

  public void setViewHeight(String paramString)
  {
    this.customHeight = paramString;
  }

  public boolean startInput(EditorInfo paramEditorInfo, String paramString)
  {
    logger.d("start input with editor info", new Object[0]);
    this.sourceLanguage = paramString;
    return this.textInput.startInput(paramString);
  }

  public static abstract interface Listener
  {
    public abstract void onCameraError();

    public abstract void onKeyboardButton();

    public abstract void onLoadImage();

    public abstract void onLoadImageError();

    public abstract void onNetworkError(int paramInt);

    public abstract void onTextSelected(String paramString, String[] paramArrayOfString);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.unveil.textinput.TextInputView
 * JD-Core Version:    0.6.2
 */