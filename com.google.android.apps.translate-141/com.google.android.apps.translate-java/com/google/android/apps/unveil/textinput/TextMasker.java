package com.google.android.apps.unveil.textinput;

import android.content.Context;
import android.graphics.Point;
import android.text.TextUtils;
import com.google.android.apps.unveil.env.GeometryUtils;
import com.google.android.apps.unveil.env.Picture;
import com.google.android.apps.unveil.env.PixelUtils;
import com.google.android.apps.unveil.env.Size;
import com.google.android.apps.unveil.env.UnveilLogger;
import com.google.goggles.AnnotationResultProtos.AnnotationResult.TextInformation.Word;
import com.google.goggles.BoundingBoxProtos.BoundingBox;
import com.google.goggles.UrlGroupProtos.Url;
import com.google.goggles.UrlGroupProtos.UrlGroup;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class TextMasker
{
  private static final int MAX_DISTANCE_FOR_SINGLE_WORD_SELECTION_DIP = 5;
  private static final int MAX_WORD_DISTANCE_DIP = 40;
  private static final UnveilLogger logger = new UnveilLogger();
  private List<UrlGroupProtos.UrlGroup> actions;
  boolean allowSmartSelection;
  private final int maxDistanceForSingleWordSelectionPx;
  private final int maxWordDistancePx;
  private Size queryPictureSize;
  private final List<UnderlinePositioner.WordUnderline> selectedWordUnderlines = new ArrayList();
  private final List<AnnotationResultProtos.AnnotationResult.TextInformation.Word> selectedWords = new ArrayList();
  Set<Line> smartSelectedLines = new HashSet();
  Set<AnnotationResultProtos.AnnotationResult.TextInformation.Word> smartSelectedWords = new HashSet();
  private SmudgeView.Smudge smudge = SmudgeView.Smudge.NONE;
  private final UnderlinePositioner underlinePositioner;
  private final List<UnderlinePositioner.WordUnderline> unselectedWordUnderlines = new ArrayList();
  private final List<AnnotationResultProtos.AnnotationResult.TextInformation.Word> unselectedWords = new ArrayList();
  private List<AnnotationResultProtos.AnnotationResult.TextInformation.Word> words;

  public TextMasker(Context paramContext)
  {
    this.maxWordDistancePx = PixelUtils.dipToPix(40.0F, paramContext);
    this.maxDistanceForSingleWordSelectionPx = PixelUtils.dipToPix(5.0F, paramContext);
    this.underlinePositioner = new TrivialUnderlinePositioner();
  }

  private void computeSelectedWords()
  {
    if (!this.smudge.isMultipass)
    {
      this.smartSelectedWords.clear();
      this.smartSelectedLines.clear();
    }
    this.selectedWords.clear();
    this.unselectedWords.clear();
    this.selectedWordUnderlines.clear();
    this.unselectedWordUnderlines.clear();
    this.underlinePositioner.setWords(this.words);
    if (this.smudge == SmudgeView.Smudge.ALL)
    {
      if (this.words != null)
      {
        this.selectedWords.addAll(this.words);
        Iterator localIterator7 = this.words.iterator();
        while (localIterator7.hasNext())
        {
          AnnotationResultProtos.AnnotationResult.TextInformation.Word localWord4 = (AnnotationResultProtos.AnnotationResult.TextInformation.Word)localIterator7.next();
          this.selectedWordUnderlines.add(this.underlinePositioner.getUnderline(localWord4));
        }
      }
    }
    else if (this.smudge == SmudgeView.Smudge.NONE)
    {
      if (this.words != null)
      {
        this.unselectedWords.addAll(this.words);
        Iterator localIterator6 = this.words.iterator();
        while (localIterator6.hasNext())
        {
          AnnotationResultProtos.AnnotationResult.TextInformation.Word localWord3 = (AnnotationResultProtos.AnnotationResult.TextInformation.Word)localIterator6.next();
          this.unselectedWordUnderlines.add(this.underlinePositioner.getUnderline(localWord3));
        }
      }
    }
    else
      if ((this.smudge != null) && (this.words != null) && (this.queryPictureSize != null) && (this.smudge.points.size() != 0))
        break label290;
    return;
    break label852;
    label289: label290: ArrayList localArrayList;
    do
    {
      Size localSize = this.smudge.mask.getSize();
      float f1 = localSize.width / this.queryPictureSize.width;
      float f2 = localSize.height / this.queryPictureSize.height;
      Point localPoint1 = (Point)this.smudge.points.get(0);
      float f3 = 0.0F;
      Iterator localIterator1 = this.smudge.points.iterator();
      while (localIterator1.hasNext())
      {
        Point localPoint2 = (Point)localIterator1.next();
        f3 += (localPoint1.x - localPoint2.x) * (localPoint1.x - localPoint2.x) + (localPoint1.y - localPoint2.y) * (localPoint1.y - localPoint2.y);
      }
      if ((this.allowSmartSelection) && (f3 / this.smudge.points.size() <= this.maxDistanceForSingleWordSelectionPx * this.maxDistanceForSingleWordSelectionPx))
      {
        performSingleWordSelection(f1, f2);
        return;
      }
      localArrayList = new ArrayList();
      int i = 2147483647;
      Iterator localIterator2 = this.words.iterator();
      while (localIterator2.hasNext())
      {
        AnnotationResultProtos.AnnotationResult.TextInformation.Word localWord2 = (AnnotationResultProtos.AnnotationResult.TextInformation.Word)localIterator2.next();
        int j = (int)(f1 * (localWord2.getBox().getX() + localWord2.getBox().getWidth() / 2));
        int k = (int)(f1 * (localWord2.getBox().getX() + localWord2.getBox().getWidth()));
        int m = (int)(f2 * (localWord2.getBox().getY() + localWord2.getBox().getHeight() / 2));
        if (this.smudge.isHighlighted(j, m))
        {
          this.selectedWords.add(localWord2);
          this.selectedWordUnderlines.add(this.underlinePositioner.getUnderline(localWord2));
          if (j < i)
            localArrayList.add(new Line(null));
          i = k;
          ((Line)localArrayList.get(-1 + localArrayList.size())).addWord(localWord2, j, m, this.smudge.points);
        }
        else
        {
          this.unselectedWords.add(localWord2);
          this.unselectedWordUnderlines.add(this.underlinePositioner.getUnderline(localWord2));
        }
      }
    }
    while ((!this.allowSmartSelection) || (!this.smudge.oneLineSelection));
    float f4 = 3.4028235E+38F;
    Object localObject = null;
    Iterator localIterator3 = localArrayList.iterator();
    while (localIterator3.hasNext())
    {
      Line localLine2 = (Line)localIterator3.next();
      float f5 = localLine2.getCost();
      if (f5 < f4)
      {
        localObject = localLine2;
        f4 = f5;
      }
    }
    if (localObject != null)
      this.smartSelectedLines.add(localObject);
    Iterator localIterator4 = localArrayList.iterator();
    while (true)
    {
      label852: if (!localIterator4.hasNext())
        break label289;
      Line localLine1 = (Line)localIterator4.next();
      if ((localObject == localLine1) || (this.smartSelectedLines.contains(localLine1)))
        break;
      this.smartSelectedLines.remove(localLine1);
      Iterator localIterator5 = localLine1.getWords().iterator();
      while (localIterator5.hasNext())
      {
        AnnotationResultProtos.AnnotationResult.TextInformation.Word localWord1 = (AnnotationResultProtos.AnnotationResult.TextInformation.Word)localIterator5.next();
        if ((!this.smudge.isMultipass) || (!this.smartSelectedWords.contains(localWord1)))
        {
          this.selectedWords.remove(localWord1);
          this.selectedWordUnderlines.remove(this.underlinePositioner.getUnderline(localWord1));
          this.unselectedWords.add(localWord1);
          this.unselectedWordUnderlines.add(this.underlinePositioner.getUnderline(localWord1));
        }
      }
    }
  }

  private AnnotationResultProtos.AnnotationResult.TextInformation.Word nearestWord(int paramInt1, int paramInt2)
  {
    Object localObject = null;
    int i = 2147483647;
    Iterator localIterator = this.words.iterator();
    if (localIterator.hasNext())
    {
      AnnotationResultProtos.AnnotationResult.TextInformation.Word localWord = (AnnotationResultProtos.AnnotationResult.TextInformation.Word)localIterator.next();
      BoundingBoxProtos.BoundingBox localBoundingBox = localWord.getBox();
      int j;
      if (paramInt1 <= localBoundingBox.getX())
      {
        j = localBoundingBox.getX();
        label62: if (paramInt2 > localBoundingBox.getY())
          break label154;
      }
      label154: for (int k = localBoundingBox.getY(); ; k = Math.min(paramInt2, localBoundingBox.getY() + localBoundingBox.getHeight()))
      {
        int m = j - paramInt1;
        int n = k - paramInt2;
        int i1 = m * m + n * n;
        if ((i1 >= i) || (i1 >= this.maxWordDistancePx * this.maxWordDistancePx))
          break;
        i = i1;
        localObject = localWord;
        break;
        j = Math.min(paramInt1, localBoundingBox.getX() + localBoundingBox.getWidth());
        break label62;
      }
    }
    return localObject;
  }

  private void performSingleWordSelection(float paramFloat1, float paramFloat2)
  {
    AnnotationResultProtos.AnnotationResult.TextInformation.Word localWord1 = nearestWord((int)(((Point)this.smudge.points.get(0)).x / paramFloat1), (int)(((Point)this.smudge.points.get(0)).y / paramFloat2));
    Iterator localIterator1 = this.words.iterator();
    while (localIterator1.hasNext())
    {
      AnnotationResultProtos.AnnotationResult.TextInformation.Word localWord2 = (AnnotationResultProtos.AnnotationResult.TextInformation.Word)localIterator1.next();
      if ((localWord1 == localWord2) || (this.smartSelectedWords.contains(localWord2)));
      for (int i = 1; ; i = 0)
      {
        Iterator localIterator2 = this.smartSelectedLines.iterator();
        while (true)
        {
          if (!localIterator2.hasNext())
            break label185;
          Iterator localIterator3 = ((Line)localIterator2.next()).getWords().iterator();
          if (localIterator3.hasNext())
          {
            if (localWord2 != (AnnotationResultProtos.AnnotationResult.TextInformation.Word)localIterator3.next())
              break;
            i = 1;
          }
        }
      }
      label185: if (i != 0)
      {
        this.selectedWords.add(localWord2);
        this.selectedWordUnderlines.add(this.underlinePositioner.getUnderline(localWord2));
        this.smartSelectedWords.add(localWord2);
      }
      else
      {
        this.unselectedWords.add(localWord2);
        this.unselectedWordUnderlines.add(this.underlinePositioner.getUnderline(localWord2));
      }
    }
  }

  public void clearWords()
  {
    this.words = Collections.emptyList();
    this.actions = Collections.emptyList();
    computeSelectedWords();
  }

  public List<UrlGroupProtos.Url> getSelectedActions()
  {
    ArrayList localArrayList1 = new ArrayList();
    if (this.actions == null);
    while (true)
    {
      return localArrayList1;
      String str1 = getSelectedWordsAsString();
      ArrayList localArrayList2 = new ArrayList();
      Iterator localIterator1 = this.actions.iterator();
      while (localIterator1.hasNext())
      {
        Iterator localIterator3 = ((UrlGroupProtos.UrlGroup)localIterator1.next()).getUrlList().iterator();
        while (localIterator3.hasNext())
        {
          UrlGroupProtos.Url localUrl = (UrlGroupProtos.Url)localIterator3.next();
          String str2 = localUrl.getDescription();
          if ((!TextUtils.isEmpty(str2)) && (!str2.equals("Translate")) && (!str2.equals("Copy")))
            if (str2.equals("Add To Contacts"))
            {
              localArrayList2.add(localUrl);
            }
            else
            {
              if (str2.startsWith("WORK: "))
                str2 = str2.substring("WORK: ".length());
              if (str1.indexOf(str2) != -1)
              {
                logger.d("Matched on '%s'", new Object[] { str2 });
                localArrayList1.add(localUrl);
              }
              else
              {
                logger.d("No match for '%s'", new Object[] { str2 });
              }
            }
        }
      }
      if (!localArrayList1.isEmpty())
      {
        Iterator localIterator2 = localArrayList2.iterator();
        while (localIterator2.hasNext())
          localArrayList1.add(0, (UrlGroupProtos.Url)localIterator2.next());
      }
    }
  }

  public List<UnderlinePositioner.WordUnderline> getSelectedWords()
  {
    return this.selectedWordUnderlines;
  }

  public String getSelectedWordsAsString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    Iterator localIterator = this.selectedWords.iterator();
    while (localIterator.hasNext())
    {
      AnnotationResultProtos.AnnotationResult.TextInformation.Word localWord = (AnnotationResultProtos.AnnotationResult.TextInformation.Word)localIterator.next();
      localStringBuilder.append(localWord.getText() + " ");
    }
    if (localStringBuilder.length() > 0)
      localStringBuilder.deleteCharAt(-1 + localStringBuilder.length());
    return localStringBuilder.toString();
  }

  public List<UnderlinePositioner.WordUnderline> getUnselectedWords()
  {
    return this.unselectedWordUnderlines;
  }

  public void setQueryPictureSize(Size paramSize)
  {
    this.queryPictureSize = paramSize;
    computeSelectedWords();
  }

  public void setSmudge(SmudgeView.Smudge paramSmudge)
  {
    this.smudge = paramSmudge;
    boolean bool1 = this.allowSmartSelection;
    if (!paramSmudge.isMultipass);
    for (boolean bool2 = true; ; bool2 = false)
    {
      this.allowSmartSelection = (bool2 | bool1);
      computeSelectedWords();
      return;
    }
  }

  public void setWords(List<AnnotationResultProtos.AnnotationResult.TextInformation.Word> paramList, List<UrlGroupProtos.UrlGroup> paramList1)
  {
    this.words = paramList;
    this.actions = paramList1;
    this.allowSmartSelection = false;
    computeSelectedWords();
  }

  private static class Line
  {
    private float costSum = 0.0F;
    private final List<AnnotationResultProtos.AnnotationResult.TextInformation.Word> words = new ArrayList();

    public void addWord(AnnotationResultProtos.AnnotationResult.TextInformation.Word paramWord, int paramInt1, int paramInt2, List<Point> paramList)
    {
      this.words.add(paramWord);
      float f1 = 3.4028235E+38F;
      Iterator localIterator = paramList.iterator();
      while (localIterator.hasNext())
      {
        Point localPoint = (Point)localIterator.next();
        float f2 = GeometryUtils.squareDistance(paramInt1, paramInt2, localPoint.x, localPoint.y);
        if (f2 < f1)
          f1 = f2;
      }
      this.costSum = (f1 + this.costSum);
    }

    public float getCost()
    {
      return this.costSum / this.words.size();
    }

    public Iterable<AnnotationResultProtos.AnnotationResult.TextInformation.Word> getWords()
    {
      return this.words;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.unveil.textinput.TextMasker
 * JD-Core Version:    0.6.2
 */