package com.google.android.apps.unveil.textinput;

import android.text.TextUtils;
import android.view.animation.AnimationUtils;
import com.google.android.apps.unveil.env.Size;
import com.google.android.apps.unveil.env.UnveilLogger;
import com.google.android.apps.unveil.protocol.QueryListener;
import com.google.android.apps.unveil.protocol.QueryResponse;
import com.google.android.apps.unveil.results.ResultItem;
import com.google.goggles.AnnotationResultProtos.AnnotationResult;
import com.google.goggles.AnnotationResultProtos.AnnotationResult.TextInformation;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class TextQueryListener extends QueryListener
{
  private static final UnveilLogger logger = new UnveilLogger();
  private final BoundingBoxView boundingBoxView;
  private volatile boolean canceled = false;
  private final List<Listener> listeners;
  private Size pictureSize;
  public final long queryId;
  private final TextMasker textMasker;
  boolean textResponseReceived;

  public TextQueryListener(BoundingBoxView paramBoundingBoxView, TextMasker paramTextMasker, long paramLong, Listener[] paramArrayOfListener)
  {
    this.boundingBoxView = paramBoundingBoxView;
    this.textMasker = paramTextMasker;
    this.listeners = new ArrayList(Arrays.asList(paramArrayOfListener));
    this.queryId = paramLong;
  }

  private ResultItem getTextResult(Iterable<ResultItem> paramIterable)
  {
    Iterator localIterator = paramIterable.iterator();
    while (localIterator.hasNext())
    {
      ResultItem localResultItem = (ResultItem)localIterator.next();
      if (ConfigurationConstants.isTextResult(localResultItem))
        return localResultItem;
    }
    return null;
  }

  private void handleTextResponse(QueryResponse paramQueryResponse)
  {
    this.textResponseReceived = true;
    int i = 1;
    this.textMasker.setQueryPictureSize(this.pictureSize);
    this.boundingBoxView.setQueryPictureSize(this.pictureSize);
    if (!hasTextResult(paramQueryResponse))
    {
      this.textMasker.setWords(null, null);
      if (!isCanceled())
      {
        Iterator localIterator2 = this.listeners.iterator();
        while (localIterator2.hasNext())
          ((Listener)localIterator2.next()).noResults();
      }
      i = 0;
    }
    while (true)
    {
      this.boundingBoxView.setAnimation(AnimationUtils.loadAnimation(this.boundingBoxView.getContext(), 17432576));
      this.boundingBoxView.setVisibility(0);
      if ((isCanceled()) || (i == 0))
        break;
      Iterator localIterator1 = this.listeners.iterator();
      while (localIterator1.hasNext())
        ((Listener)localIterator1.next()).onResult();
      ResultItem localResultItem = getTextResult(paramQueryResponse.getResults());
      this.textMasker.setWords(localResultItem.getAnnotationResult().getTextInfo().getWordsList(), localResultItem.getAnnotationResult().getUrlGroupsList());
    }
  }

  private boolean hasTextResult(QueryResponse paramQueryResponse)
  {
    ResultItem localResultItem = getTextResult(paramQueryResponse.getResults());
    return (localResultItem != null) && (!TextUtils.isEmpty(localResultItem.getTitle()));
  }

  public void addListener(Listener paramListener)
  {
    this.listeners.add(paramListener);
  }

  public void cancel(boolean paramBoolean)
  {
    this.canceled = paramBoolean;
  }

  public boolean isCanceled()
  {
    return this.canceled;
  }

  public void onAuthenticationError()
  {
    logger.e("Authentication error.", new Object[0]);
    Iterator localIterator = this.listeners.iterator();
    while (localIterator.hasNext())
      ((Listener)localIterator.next()).onNetworkError(403);
  }

  public void onNetworkError(int paramInt)
  {
    UnveilLogger localUnveilLogger = logger;
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = Integer.valueOf(paramInt);
    localUnveilLogger.e("Network error, status code is %d", arrayOfObject);
    Iterator localIterator = this.listeners.iterator();
    while (localIterator.hasNext())
      ((Listener)localIterator.next()).onNetworkError(paramInt);
  }

  public void onQueryResponse(QueryResponse paramQueryResponse)
  {
    if (isCanceled())
      return;
    handleTextResponse(paramQueryResponse);
  }

  public void setPictureSize(Size paramSize)
  {
    this.pictureSize = paramSize;
  }

  public boolean textResponseRecieved()
  {
    return this.textResponseReceived;
  }

  public static abstract interface Listener
  {
    public abstract void gotResult(String paramString);

    public abstract void noResults();

    public abstract void onNetworkError(int paramInt);

    public abstract void onResult();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.unveil.textinput.TextQueryListener
 * JD-Core Version:    0.6.2
 */