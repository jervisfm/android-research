package com.google.android.apps.unveil.textinput;

import android.graphics.Point;
import com.google.goggles.AnnotationResultProtos.AnnotationResult.TextInformation.Word;
import com.google.goggles.BoundingBoxProtos.BoundingBox;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class TrivialUnderlinePositioner
  implements UnderlinePositioner
{
  private final Map<AnnotationResultProtos.AnnotationResult.TextInformation.Word, UnderlinePositioner.WordUnderline> underlines = new HashMap();

  public UnderlinePositioner.WordUnderline getUnderline(AnnotationResultProtos.AnnotationResult.TextInformation.Word paramWord)
  {
    if (this.underlines.containsKey(paramWord))
      return (UnderlinePositioner.WordUnderline)this.underlines.get(paramWord);
    return null;
  }

  public void setWords(List<AnnotationResultProtos.AnnotationResult.TextInformation.Word> paramList)
  {
    this.underlines.clear();
    if ((paramList == null) || (paramList.size() == 0));
    while (true)
    {
      return;
      Iterator localIterator = paramList.iterator();
      while (localIterator.hasNext())
      {
        AnnotationResultProtos.AnnotationResult.TextInformation.Word localWord = (AnnotationResultProtos.AnnotationResult.TextInformation.Word)localIterator.next();
        BoundingBoxProtos.BoundingBox localBoundingBox = localWord.getBox();
        this.underlines.put(localWord, new UnderlinePositioner.WordUnderline(new Point(localBoundingBox.getX(), localBoundingBox.getY() + localBoundingBox.getHeight()), new Point(localBoundingBox.getX() + localBoundingBox.getWidth(), localBoundingBox.getY() + localBoundingBox.getHeight()), localBoundingBox));
      }
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.unveil.textinput.TrivialUnderlinePositioner
 * JD-Core Version:    0.6.2
 */