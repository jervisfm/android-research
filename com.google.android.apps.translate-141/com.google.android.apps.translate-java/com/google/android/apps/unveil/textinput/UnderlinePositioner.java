package com.google.android.apps.unveil.textinput;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import com.google.goggles.AnnotationResultProtos.AnnotationResult.TextInformation.Word;
import com.google.goggles.BoundingBoxProtos.BoundingBox;
import java.util.List;

public abstract interface UnderlinePositioner
{
  public abstract WordUnderline getUnderline(AnnotationResultProtos.AnnotationResult.TextInformation.Word paramWord);

  public abstract void setWords(List<AnnotationResultProtos.AnnotationResult.TextInformation.Word> paramList);

  public static class WordUnderline
  {
    private final Point left;
    private final Point right;
    private final BoundingBoxProtos.BoundingBox wordBox;

    WordUnderline(Point paramPoint1, Point paramPoint2, BoundingBoxProtos.BoundingBox paramBoundingBox)
    {
      this.left = paramPoint1;
      this.right = paramPoint2;
      this.wordBox = paramBoundingBox;
    }

    void draw(Canvas paramCanvas, Paint paramPaint)
    {
      float f = paramPaint.getStrokeWidth();
      paramCanvas.drawLine(this.left.x + f / 2.0F, this.left.y - f, this.left.x + f / 2.0F, this.left.y, paramPaint);
      paramCanvas.drawLine(this.left.x, this.left.y, this.right.x, this.right.y, paramPaint);
      paramCanvas.drawLine(this.right.x - f / 2.0F, this.right.y, this.right.x - f / 2.0F, this.right.y - f, paramPaint);
    }

    public BoundingBoxProtos.BoundingBox getBox()
    {
      return this.wordBox;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.unveil.textinput.UnderlinePositioner
 * JD-Core Version:    0.6.2
 */