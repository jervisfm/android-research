package com.google.android.apps.unveil.textinput;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.ScaleGestureDetector.SimpleOnScaleGestureListener;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.apps.unveil.env.UnveilLogger;

public class ZoomableContainer extends ViewGroup
{
  private static float MAX_ZOOM = 5.0F;
  private static float MIN_ZOOM;
  private static final UnveilLogger logger = new UnveilLogger();
  private final Matrix matrix = new Matrix();
  private final PointF point = new PointF();
  private final ScaleGestureDetector scaleDetector = new ScaleGestureDetector(paramContext, new ScaleGestureDetector.SimpleOnScaleGestureListener()
  {
    final float[] values = new float[9];

    private void constrainMatrix(Matrix paramAnonymousMatrix)
    {
      paramAnonymousMatrix.getValues(this.values);
      float f1 = this.values[0];
      float f2 = this.values[2];
      float f3 = this.values[5];
      int i = ZoomableContainer.this.getWidth();
      int j = ZoomableContainer.this.getHeight();
      if (f2 > 0.0F)
      {
        f2 = 0.0F;
        if (f3 <= 0.0F)
          break label118;
        f3 = 0.0F;
      }
      while (true)
      {
        this.values[2] = f2;
        this.values[5] = f3;
        paramAnonymousMatrix.setValues(this.values);
        return;
        if (-f2 + i <= f1 * i)
          break;
        f2 = i - f1 * i;
        break;
        label118: if (-f3 + j > f1 * j)
          f3 = j - f1 * j;
      }
    }

    public boolean onScale(ScaleGestureDetector paramAnonymousScaleGestureDetector)
    {
      float f1 = paramAnonymousScaleGestureDetector.getFocusX();
      float f2 = paramAnonymousScaleGestureDetector.getFocusY();
      float f3 = f1 - ZoomableContainer.this.point.x;
      float f4 = f2 - ZoomableContainer.this.point.y;
      ZoomableContainer.this.point.set(f1, f2);
      float f5 = ZoomableContainer.this.scaleFactor;
      ZoomableContainer.access$232(ZoomableContainer.this, paramAnonymousScaleGestureDetector.getScaleFactor());
      ZoomableContainer.access$202(ZoomableContainer.this, Math.max(ZoomableContainer.MIN_ZOOM, Math.min(ZoomableContainer.this.scaleFactor, ZoomableContainer.MAX_ZOOM)));
      ZoomableContainer.this.matrix.postScale(ZoomableContainer.this.scaleFactor / f5, ZoomableContainer.this.scaleFactor / f5, ZoomableContainer.this.getWidth() / 2, ZoomableContainer.this.getHeight() / 2);
      ZoomableContainer.this.matrix.postTranslate(f3, f4);
      constrainMatrix(ZoomableContainer.this.matrix);
      ZoomableContainer.this.notifyChildren();
      ZoomableContainer.this.invalidate();
      return true;
    }

    public boolean onScaleBegin(ScaleGestureDetector paramAnonymousScaleGestureDetector)
    {
      ZoomableContainer.this.point.set(paramAnonymousScaleGestureDetector.getFocusX(), paramAnonymousScaleGestureDetector.getFocusY());
      if (ZoomableContainer.this.matrix.isIdentity())
        ZoomableContainer.access$202(ZoomableContainer.this, 1.0F);
      return true;
    }
  });
  private float scaleFactor = 1.0F;

  static
  {
    MIN_ZOOM = 1.0F;
  }

  public ZoomableContainer(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }

  private void notifyChildren()
  {
    int i = getChildCount();
    for (int j = 0; j < i; j++)
    {
      View localView = getChildAt(j);
      if ((localView instanceof EventListener))
        ((EventListener)localView).onZoom(this.matrix);
    }
  }

  protected void dispatchDraw(Canvas paramCanvas)
  {
    paramCanvas.save();
    paramCanvas.concat(this.matrix);
    super.dispatchDraw(paramCanvas);
    paramCanvas.restore();
  }

  public boolean dispatchTouchEvent(MotionEvent paramMotionEvent)
  {
    int i = 1;
    this.scaleDetector.onTouchEvent(paramMotionEvent);
    int j;
    if (paramMotionEvent.getPointerCount() == i)
      j = super.dispatchTouchEvent(paramMotionEvent);
    while ((paramMotionEvent.getPointerCount() <= j) || (paramMotionEvent.getActionMasked() != 5))
      return j;
    return super.dispatchTouchEvent(MotionEvent.obtain(paramMotionEvent.getDownTime(), paramMotionEvent.getEventTime(), 3, paramMotionEvent.getX(0), paramMotionEvent.getY(0), paramMotionEvent.getPressure(0), paramMotionEvent.getSize(0), paramMotionEvent.getMetaState(), paramMotionEvent.getXPrecision(), paramMotionEvent.getYPrecision(), paramMotionEvent.getDeviceId(), paramMotionEvent.getEdgeFlags()));
  }

  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    UnveilLogger localUnveilLogger = logger;
    Object[] arrayOfObject = new Object[4];
    arrayOfObject[0] = Integer.valueOf(paramInt1);
    arrayOfObject[1] = Integer.valueOf(paramInt2);
    arrayOfObject[2] = Integer.valueOf(paramInt3);
    arrayOfObject[3] = Integer.valueOf(paramInt4);
    localUnveilLogger.d("zoomable container onLayout: %d,%d,%d,%d", arrayOfObject);
    int i = getChildCount();
    for (int j = 0; j < i; j++)
      getChildAt(j).layout(0, 0, paramInt3 - paramInt1, paramInt4 - paramInt2);
  }

  public void reset()
  {
    this.matrix.reset();
    notifyChildren();
  }

  public static abstract interface EventListener
  {
    public abstract void onZoom(Matrix paramMatrix);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.unveil.textinput.ZoomableContainer
 * JD-Core Version:    0.6.2
 */