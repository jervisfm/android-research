package com.google.android.apps.unveil.textinput.compatible;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import com.google.android.apps.unveil.sensors.CameraManager;
import com.google.android.apps.unveil.textinput.R.id;
import com.google.android.apps.unveil.textinput.R.layout;
import com.google.android.apps.unveil.ui.CameraWrappingLayout;
import com.google.android.apps.unveil.ui.CameraWrappingLayout.Alignment;

public class TextInputView extends com.google.android.apps.unveil.textinput.TextInputView
{
  CameraWrappingLayout cameraWrappingLayout;

  public TextInputView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
  }

  protected View createView(Context paramContext)
  {
    return View.inflate(paramContext, R.layout.compatible_text_input_view, null);
  }

  protected void onFinishInflate()
  {
    super.onFinishInflate();
    this.cameraWrappingLayout = ((CameraWrappingLayout)findViewById(R.id.camera_wrapping_layout));
    this.cameraWrappingLayout.setCameraManager((CameraManager)findViewById(R.id.camera_manager));
    this.cameraWrappingLayout.setRotation(CameraManager.getCameraToDisplayRotation(getContext()));
    this.cameraWrappingLayout.setAlignment(CameraWrappingLayout.Alignment.BOTTOM);
  }

  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    this.cameraWrappingLayout.setRotation(CameraManager.getCameraToDisplayRotation(getContext()));
    super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
  }

  public void setImeControlsHeight(int paramInt)
  {
    findViewById(R.id.ime_controls).getLayoutParams().height = paramInt;
    requestLayout();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.unveil.textinput.compatible.TextInputView
 * JD-Core Version:    0.6.2
 */