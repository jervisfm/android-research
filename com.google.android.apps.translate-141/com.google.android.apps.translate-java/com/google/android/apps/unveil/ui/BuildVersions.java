package com.google.android.apps.unveil.ui;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.text.format.DateFormat;
import android.widget.TextView;
import com.google.android.apps.unveil.env.UnveilLogger;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class BuildVersions
{
  private static final UnveilLogger logger = new UnveilLogger();

  public static void populate(TextView paramTextView)
  {
    int i = -1;
    long l1 = 0L;
    try
    {
      Context localContext = paramTextView.getContext();
      i = localContext.getPackageManager().getPackageInfo(localContext.getPackageName(), 0).versionCode;
      long l2 = new ZipFile(localContext.getApplicationInfo().sourceDir).getEntry("classes.dex").getTime();
      l1 = l2;
      Object[] arrayOfObject = new Object[2];
      arrayOfObject[0] = Integer.valueOf(i);
      arrayOfObject[1] = DateFormat.format("M/d h:mm a", l1);
      paramTextView.setText(String.format("Build Version: %d (%s)", arrayOfObject));
      return;
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
      while (true)
        logger.e("Unable to load version code: %s", new Object[] { localNameNotFoundException });
    }
    catch (IOException localIOException)
    {
      while (true)
        logger.e("Unable to calculate timestamp: %s", new Object[] { localIOException });
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.apps.unveil.ui.BuildVersions
 * JD-Core Version:    0.6.2
 */