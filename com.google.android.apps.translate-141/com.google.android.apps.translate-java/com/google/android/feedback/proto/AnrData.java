package com.google.android.feedback.proto;

public abstract interface AnrData
{
  public static final int ACTIVITY = 1;
  public static final int CAUSE = 2;
  public static final int EXTRA = 3;
  public static final int STACK_TRACES = 4;
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.feedback.proto.AnrData
 * JD-Core Version:    0.6.2
 */