package com.google.android.feedback.proto;

public abstract interface BatteryData
{
  public static final int BATTERY_LEVEL = 5;
  public static final int BATTERY_STATE = 6;
  public static final int BATTERY_STATE_CHARGING = 2;
  public static final int BATTERY_STATE_FULL = 3;
  public static final int BATTERY_STATE_UNKNOWN = 0;
  public static final int BATTERY_STATE_UNPLUGGED = 1;
  public static final int CHECKIN_DETAILS = 4;
  public static final int DURATION_MICROS = 2;
  public static final int USAGE_DETAILS = 3;
  public static final int USAGE_PERCENT = 1;
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.feedback.proto.BatteryData
 * JD-Core Version:    0.6.2
 */