package com.google.android.feedback.proto;

public abstract interface Dimensions
{
  public static final int HEIGHT = 2;
  public static final int WIDTH = 1;
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.feedback.proto.Dimensions
 * JD-Core Version:    0.6.2
 */