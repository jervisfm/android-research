package com.google.android.feedback.proto;

public abstract interface Image
{
  public static final int CONTENT = 2;
  public static final int IMAGE_DIMENSION = 3;
  public static final int MIME_TYPE = 1;
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.feedback.proto.Image
 * JD-Core Version:    0.6.2
 */