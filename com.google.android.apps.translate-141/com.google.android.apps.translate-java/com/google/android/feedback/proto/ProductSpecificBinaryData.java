package com.google.android.feedback.proto;

public abstract interface ProductSpecificBinaryData
{
  public static final int DATA = 3;
  public static final int MIME_TYPE = 2;
  public static final int NAME = 1;
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.feedback.proto.ProductSpecificBinaryData
 * JD-Core Version:    0.6.2
 */