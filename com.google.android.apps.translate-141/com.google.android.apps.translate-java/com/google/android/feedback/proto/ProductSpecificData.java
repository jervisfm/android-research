package com.google.android.feedback.proto;

public abstract interface ProductSpecificData
{
  public static final int KEY = 1;
  public static final int VALUE = 2;
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.feedback.proto.ProductSpecificData
 * JD-Core Version:    0.6.2
 */