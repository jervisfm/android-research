package com.google.android.feedback.proto;

public abstract interface Rectangle
{
  public static final int HEIGHT = 4;
  public static final int LEFT = 1;
  public static final int TOP = 2;
  public static final int WIDTH = 3;
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.feedback.proto.Rectangle
 * JD-Core Version:    0.6.2
 */