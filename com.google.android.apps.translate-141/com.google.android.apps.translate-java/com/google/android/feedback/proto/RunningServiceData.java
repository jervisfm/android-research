package com.google.android.feedback.proto;

public abstract interface RunningServiceData
{
  public static final int DURATION_MILLIS = 1;
  public static final int SERVICE_DETAILS = 2;
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.feedback.proto.RunningServiceData
 * JD-Core Version:    0.6.2
 */