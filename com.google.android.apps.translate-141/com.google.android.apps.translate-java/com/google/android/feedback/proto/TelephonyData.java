package com.google.android.feedback.proto;

public abstract interface TelephonyData
{
  public static final int NETWORK_NAME = 2;
  public static final int NETWORK_TYPE = 3;
  public static final int PHONE_TYPE = 1;
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.android.feedback.proto.TelephonyData
 * JD-Core Version:    0.6.2
 */