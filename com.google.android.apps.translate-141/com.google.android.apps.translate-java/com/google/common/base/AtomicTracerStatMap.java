package com.google.common.base;

import com.google.common.annotations.GoogleInternal;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@GoogleInternal
public final class AtomicTracerStatMap
{
  private final ConcurrentMap<String, Long> map = new ConcurrentHashMap();

  public Map<String, Long> getMap()
  {
    return this.map;
  }

  public void incrementBy(String paramString, long paramLong)
  {
    Long localLong = (Long)this.map.get(paramString);
    if (localLong == null)
    {
      localLong = (Long)this.map.putIfAbsent(paramString, Long.valueOf(paramLong));
      if (localLong == null)
        return;
    }
    while (!this.map.replace(paramString, localLong, Long.valueOf(paramLong + localLong.longValue())))
      localLong = (Long)this.map.get(paramString);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.common.base.AtomicTracerStatMap
 * JD-Core Version:    0.6.2
 */