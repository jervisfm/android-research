package com.google.common.base;

import com.google.common.annotations.GoogleInternal;
import com.google.common.annotations.GwtCompatible;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Iterator;
import javax.annotation.Nullable;

@GoogleInternal
@GwtCompatible
public final class BinaryPredicates
{
  @GwtCompatible(serializable=true)
  public static <X, Y> BinaryPredicate<X, Y> alwaysFalse()
  {
    return restrict(AlwaysFalse.AlwaysFalse);
  }

  @GwtCompatible(serializable=true)
  public static <X, Y> BinaryPredicate<X, Y> alwaysTrue()
  {
    return restrict(AlwaysTrue.AlwaysTrue);
  }

  public static <X, Y> BinaryPredicate<X, Y> and(BinaryPredicate<? super X, ? super Y> paramBinaryPredicate1, BinaryPredicate<? super X, ? super Y> paramBinaryPredicate2)
  {
    return new And(Arrays.asList(new BinaryPredicate[] { restrict(paramBinaryPredicate1), restrict(paramBinaryPredicate2) }));
  }

  public static <X, Y> BinaryPredicate<X, Y> and(Iterable<? extends BinaryPredicate<? super X, ? super Y>> paramIterable)
  {
    return new And(paramIterable);
  }

  public static <X, Y> BinaryPredicate<X, Y> and(BinaryPredicate<? super X, ? super Y>[] paramArrayOfBinaryPredicate)
  {
    return new And(Arrays.asList(paramArrayOfBinaryPredicate));
  }

  @GwtCompatible(serializable=true)
  public static <X, Y> BinaryPredicate<X, Y> equality()
  {
    return restrict(Equality.Equality);
  }

  public static <X, Y> BinaryPredicate<X, Y> first(Predicate<? super X> paramPredicate)
  {
    return new First(paramPredicate);
  }

  @GwtCompatible(serializable=true)
  public static <X, Y> BinaryPredicate<X, Y> identity()
  {
    return restrict(Identity.Identity);
  }

  private static int iterableAsListHashCode(Iterable<?> paramIterable)
  {
    Iterator localIterator = paramIterable.iterator();
    Object localObject;
    for (int i = 1; localIterator.hasNext(); i = i * 31 + localObject.hashCode())
      localObject = localIterator.next();
    return i;
  }

  private static boolean iterableElementsEqual(Iterable<?> paramIterable1, Iterable<?> paramIterable2)
  {
    Iterator localIterator1 = paramIterable1.iterator();
    Iterator localIterator2 = paramIterable2.iterator();
    if (localIterator1.hasNext())
      if (localIterator2.hasNext());
    while (localIterator2.hasNext())
    {
      return false;
      if (localIterator1.next().equals(localIterator2.next()))
        break;
      return false;
    }
    return true;
  }

  public static <X, Y> BinaryPredicate<X, Y> not(BinaryPredicate<? super X, ? super Y> paramBinaryPredicate)
  {
    return new Not(paramBinaryPredicate);
  }

  public static <X, Y> BinaryPredicate<X, Y> or(BinaryPredicate<? super X, ? super Y> paramBinaryPredicate1, BinaryPredicate<? super X, ? super Y> paramBinaryPredicate2)
  {
    return new Or(Arrays.asList(new BinaryPredicate[] { restrict(paramBinaryPredicate1), restrict(paramBinaryPredicate2) }));
  }

  public static <X, Y> BinaryPredicate<X, Y> or(Iterable<? extends BinaryPredicate<? super X, ? super Y>> paramIterable)
  {
    return new Or(paramIterable);
  }

  public static <X, Y> BinaryPredicate<X, Y> or(BinaryPredicate<? super X, ? super Y>[] paramArrayOfBinaryPredicate)
  {
    return new Or(Arrays.asList(paramArrayOfBinaryPredicate));
  }

  private static <X, Y> BinaryPredicate<X, Y> restrict(@Nullable BinaryPredicate<? super X, ? super Y> paramBinaryPredicate)
  {
    return paramBinaryPredicate;
  }

  public static <X, Y> BinaryPredicate<X, Y> second(Predicate<? super Y> paramPredicate)
  {
    return new Second(paramPredicate);
  }

  static enum AlwaysFalse
    implements BinaryPredicate<Object, Object>
  {
    static
    {
      AlwaysFalse[] arrayOfAlwaysFalse = new AlwaysFalse[1];
      arrayOfAlwaysFalse[0] = AlwaysFalse;
    }

    public boolean apply(@Nullable Object paramObject1, @Nullable Object paramObject2)
    {
      return false;
    }
  }

  static enum AlwaysTrue
    implements BinaryPredicate<Object, Object>
  {
    static
    {
      AlwaysTrue[] arrayOfAlwaysTrue = new AlwaysTrue[1];
      arrayOfAlwaysTrue[0] = AlwaysTrue;
    }

    public boolean apply(@Nullable Object paramObject1, @Nullable Object paramObject2)
    {
      return true;
    }
  }

  private static final class And<X, Y>
    implements BinaryPredicate<X, Y>, Serializable
  {
    private static final long serialVersionUID = 4814831122225615776L;
    final Iterable<? extends BinaryPredicate<? super X, ? super Y>> predicates;

    And(Iterable<? extends BinaryPredicate<? super X, ? super Y>> paramIterable)
    {
      this.predicates = Preconditions.checkContentsNotNull(paramIterable);
    }

    public boolean apply(@Nullable X paramX, @Nullable Y paramY)
    {
      Iterator localIterator = this.predicates.iterator();
      while (localIterator.hasNext())
        if (!((BinaryPredicate)localIterator.next()).apply(paramX, paramY))
          return false;
      return true;
    }

    public boolean equals(@Nullable Object paramObject)
    {
      if ((paramObject instanceof And))
        return BinaryPredicates.iterableElementsEqual(this.predicates, ((And)paramObject).predicates);
      return false;
    }

    public int hashCode()
    {
      return BinaryPredicates.iterableAsListHashCode(this.predicates);
    }
  }

  static enum Equality
    implements BinaryPredicate<Object, Object>
  {
    static
    {
      Equality[] arrayOfEquality = new Equality[1];
      arrayOfEquality[0] = Equality;
    }

    public boolean apply(@Nullable Object paramObject1, @Nullable Object paramObject2)
    {
      return Objects.equal(paramObject1, paramObject2);
    }
  }

  private static final class First<X, Y>
    implements BinaryPredicate<X, Y>, Serializable
  {
    private static final long serialVersionUID = 5389902773091803723L;
    final Predicate<? super X> predicate;

    First(Predicate<? super X> paramPredicate)
    {
      this.predicate = ((Predicate)Preconditions.checkNotNull(paramPredicate));
    }

    public boolean apply(@Nullable X paramX, @Nullable Y paramY)
    {
      return this.predicate.apply(paramX);
    }

    public boolean equals(@Nullable Object paramObject)
    {
      if ((paramObject instanceof First))
      {
        First localFirst = (First)paramObject;
        return this.predicate.equals(localFirst.predicate);
      }
      return false;
    }

    public int hashCode()
    {
      return this.predicate.hashCode();
    }
  }

  static enum Identity
    implements BinaryPredicate<Object, Object>
  {
    static
    {
      Identity[] arrayOfIdentity = new Identity[1];
      arrayOfIdentity[0] = Identity;
    }

    public boolean apply(@Nullable Object paramObject1, @Nullable Object paramObject2)
    {
      return paramObject1 == paramObject2;
    }
  }

  private static final class Not<X, Y>
    implements BinaryPredicate<X, Y>, Serializable
  {
    private static final long serialVersionUID = 7318841078083112007L;
    final BinaryPredicate<? super X, ? super Y> predicate;

    Not(BinaryPredicate<? super X, ? super Y> paramBinaryPredicate)
    {
      this.predicate = ((BinaryPredicate)Preconditions.checkNotNull(paramBinaryPredicate));
    }

    public boolean apply(@Nullable X paramX, @Nullable Y paramY)
    {
      return !this.predicate.apply(paramX, paramY);
    }

    public boolean equals(@Nullable Object paramObject)
    {
      if ((paramObject instanceof Not))
      {
        Not localNot = (Not)paramObject;
        return this.predicate.equals(localNot.predicate);
      }
      return false;
    }

    public int hashCode()
    {
      return this.predicate.hashCode();
    }
  }

  private static final class Or<X, Y>
    implements BinaryPredicate<X, Y>, Serializable
  {
    private static final long serialVersionUID = -1352468805830701672L;
    final Iterable<? extends BinaryPredicate<? super X, ? super Y>> predicates;

    Or(Iterable<? extends BinaryPredicate<? super X, ? super Y>> paramIterable)
    {
      this.predicates = Preconditions.checkContentsNotNull(paramIterable);
    }

    public boolean apply(@Nullable X paramX, @Nullable Y paramY)
    {
      Iterator localIterator = this.predicates.iterator();
      while (localIterator.hasNext())
        if (((BinaryPredicate)localIterator.next()).apply(paramX, paramY))
          return true;
      return false;
    }

    public boolean equals(@Nullable Object paramObject)
    {
      if ((paramObject instanceof Or))
        return BinaryPredicates.iterableElementsEqual(this.predicates, ((Or)paramObject).predicates);
      return false;
    }

    public int hashCode()
    {
      return BinaryPredicates.iterableAsListHashCode(this.predicates);
    }
  }

  private static final class Second<X, Y>
    implements BinaryPredicate<X, Y>, Serializable
  {
    private static final long serialVersionUID = -7134579481937611424L;
    final Predicate<? super Y> predicate;

    Second(Predicate<? super Y> paramPredicate)
    {
      this.predicate = ((Predicate)Preconditions.checkNotNull(paramPredicate));
    }

    public boolean apply(@Nullable X paramX, @Nullable Y paramY)
    {
      return this.predicate.apply(paramY);
    }

    public boolean equals(@Nullable Object paramObject)
    {
      if ((paramObject instanceof Second))
      {
        Second localSecond = (Second)paramObject;
        return this.predicate.equals(localSecond.predicate);
      }
      return false;
    }

    public int hashCode()
    {
      return this.predicate.hashCode();
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.common.base.BinaryPredicates
 * JD-Core Version:    0.6.2
 */