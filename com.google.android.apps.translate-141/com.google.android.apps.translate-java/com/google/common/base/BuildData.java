package com.google.common.base;

import com.google.common.annotations.GoogleInternal;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Logger;

@GoogleInternal
public final class BuildData
{
  public static final String BUILD_CHANGELIST = "Build changelist";
  public static final String BUILD_CHANGELIST_AS_INT = "Build changelist as int";
  public static final String BUILD_CLIENT = "Build client";
  private static final String BUILD_DATA_PROPERTIES = "/build-data.properties";
  public static final String BUILD_DEPOT_PATH = "Build depot path";
  public static final String BUILD_GPLATFORM = "Build gplatform";
  public static final String BUILD_ID = "Build ID";
  public static final String BUILD_LABEL = "Build label";
  public static final String BUILD_TARGET = "Build target";
  public static final String BUILD_TIMESTAMP = "Build timestamp";
  public static final String BUILD_TIMESTAMP_AS_INT = "Build timestamp as int";
  public static final String BUILD_TOOL = "Build tool";
  public static final String BUILD_VERSION_MAP = "Build version map";
  public static final String BUILT_AT = "Built at";
  public static final String BUILT_ON = "Built on";
  public static final String DEPENDENCIES = "Dependencies";
  public static final String MPM_VERSION = "Mpm version";
  public static final int UNKNOWN_BUILD_CHANGELIST = -1;
  public static final String UNKNOWN_BUILD_ID = "<unknown>";
  public static final String UNKNOWN_BUILD_VERSION_MAP = "map 0 { // }";
  private static Properties properties;
  private static Map<String, String> testData = null;
  private static volatile boolean warned;

  public static int getBuildChangelist()
  {
    try
    {
      int i = Integer.parseInt((String)getData().get("Build changelist"));
      return i;
    }
    catch (NumberFormatException localNumberFormatException)
    {
    }
    return -1;
  }

  public static String getBuildDepotPath()
  {
    String str = (String)getData().get("Build depot path");
    if (str != null)
      return str;
    return "//depot/google3";
  }

  public static String getBuildId()
  {
    String str = (String)getData().get("Build ID");
    if (str != null)
      return str;
    return "<unknown>";
  }

  public static String getBuildVersionMap()
  {
    String str = (String)getData().get("Build version map");
    if (str != null)
      return str;
    return "map 0 { // }";
  }

  public static Map<String, String> getData()
  {
    return getData("/build-data.properties");
  }

  static Map<String, String> getData(String paramString)
  {
    Map localMap = tryGetData(paramString);
    if (localMap != null)
      return localMap;
    if (!warned)
    {
      warned = true;
      Logger.getLogger(BuildData.class.getName()).info("No build data available; consider building this application as a deploy jar");
    }
    return Collections.emptyMap();
  }

  public static Class<?> getEntryClass()
  {
    return getEntryClass("/build-data.properties");
  }

  static Class<?> getEntryClass(String paramString)
  {
    try
    {
      String str = getProperties(paramString).getProperty("main.class");
      if (str == null)
        return null;
      Class localClass = Class.forName(str);
      return localClass;
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
    }
    return null;
  }

  // ERROR //
  private static Properties getProperties(String paramString)
  {
    // Byte code:
    //   0: ldc 2
    //   2: monitorenter
    //   3: getstatic 164	com/google/common/base/BuildData:properties	Ljava/util/Properties;
    //   6: ifnonnull +34 -> 40
    //   9: new 153	java/util/Properties
    //   12: dup
    //   13: invokespecial 165	java/util/Properties:<init>	()V
    //   16: putstatic 164	com/google/common/base/BuildData:properties	Ljava/util/Properties;
    //   19: ldc 2
    //   21: aload_0
    //   22: invokevirtual 169	java/lang/Class:getResourceAsStream	(Ljava/lang/String;)Ljava/io/InputStream;
    //   25: astore 4
    //   27: aload 4
    //   29: ifnull +11 -> 40
    //   32: getstatic 164	com/google/common/base/BuildData:properties	Ljava/util/Properties;
    //   35: aload 4
    //   37: invokevirtual 173	java/util/Properties:load	(Ljava/io/InputStream;)V
    //   40: getstatic 164	com/google/common/base/BuildData:properties	Ljava/util/Properties;
    //   43: astore_2
    //   44: ldc 2
    //   46: monitorexit
    //   47: aload_2
    //   48: areturn
    //   49: astore_1
    //   50: ldc 2
    //   52: monitorexit
    //   53: aload_1
    //   54: athrow
    //   55: astore_3
    //   56: goto -16 -> 40
    //
    // Exception table:
    //   from	to	target	type
    //   3	19	49	finally
    //   19	27	49	finally
    //   32	40	49	finally
    //   40	44	49	finally
    //   19	27	55	java/io/IOException
    //   32	40	55	java/io/IOException
  }

  public static void main(String[] paramArrayOfString)
  {
    write(System.out);
  }

  private static Map<String, String> readDataFromProperties(String paramString)
  {
    Properties localProperties = getProperties(paramString);
    if (localProperties.isEmpty())
      return null;
    LinkedHashMap localLinkedHashMap = new LinkedHashMap();
    localLinkedHashMap.put("Built on", localProperties.getProperty("build.time", ""));
    localLinkedHashMap.put("Build timestamp", localProperties.getProperty("build.timestamp", ""));
    localLinkedHashMap.put("Build timestamp as int", localProperties.getProperty("build.timestamp.as.int", ""));
    localLinkedHashMap.put("Built at", localProperties.getProperty("build.location", ""));
    localLinkedHashMap.put("Build target", localProperties.getProperty("build.target", ""));
    localLinkedHashMap.put("Build ID", localProperties.getProperty("build.build_id", "<unknown>"));
    localLinkedHashMap.put("Build changelist", localProperties.getProperty("build.changelist", ""));
    localLinkedHashMap.put("Build changelist as int", localProperties.getProperty("build.changelist.as.int", ""));
    localLinkedHashMap.put("Build version map", localProperties.getProperty("build.versionmap", ""));
    localLinkedHashMap.put("Build client", localProperties.getProperty("build.client", ""));
    localLinkedHashMap.put("Build depot path", localProperties.getProperty("build.depot.path", ""));
    localLinkedHashMap.put("Build label", localProperties.getProperty("build.label", ""));
    localLinkedHashMap.put("Build tool", localProperties.getProperty("build.tool", ""));
    localLinkedHashMap.put("Build gplatform", localProperties.getProperty("build.gplatform", ""));
    localLinkedHashMap.put("Mpm version", localProperties.getProperty("build.mpm.version", ""));
    localLinkedHashMap.put("Dependencies", localProperties.getProperty("build.dependencies", ""));
    return localLinkedHashMap;
  }

  public static void setTestData(Map<String, String> paramMap)
  {
    testData = new HashMap(paramMap);
  }

  public static Map<String, String> tryGetData()
  {
    return tryGetData("/build-data.properties");
  }

  static Map<String, String> tryGetData(String paramString)
  {
    if (testData != null)
      return testData;
    return readDataFromProperties(paramString);
  }

  static void unsetProperties()
  {
    properties = null;
  }

  public static void unsetTestData()
  {
    testData = null;
  }

  public static void write(OutputStream paramOutputStream)
  {
    Map localMap = tryGetData();
    if (localMap != null)
    {
      PrintWriter localPrintWriter = new PrintWriter(paramOutputStream);
      Iterator localIterator = localMap.entrySet().iterator();
      while (localIterator.hasNext())
      {
        Map.Entry localEntry = (Map.Entry)localIterator.next();
        String str = ((String)localEntry.getKey()).toLowerCase().replace(' ', '-');
        localPrintWriter.write(str + ": " + (String)localEntry.getValue() + "\n");
      }
      localPrintWriter.flush();
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.common.base.BuildData
 * JD-Core Version:    0.6.2
 */