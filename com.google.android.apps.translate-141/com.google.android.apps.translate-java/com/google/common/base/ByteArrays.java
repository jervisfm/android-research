package com.google.common.base;

import com.google.common.annotations.GoogleInternal;
import com.google.common.annotations.GwtCompatible;

@GoogleInternal
@GwtCompatible
public final class ByteArrays
{
  private static final char[] hexDigits = "0123456789abcdef".toCharArray();

  public static String toHexString(byte[] paramArrayOfByte)
  {
    StringBuilder localStringBuilder = new StringBuilder(2 * paramArrayOfByte.length);
    int i = paramArrayOfByte.length;
    for (int j = 0; j < i; j++)
    {
      int k = paramArrayOfByte[j];
      localStringBuilder.append(hexDigits[(0xF & k >> 4)]).append(hexDigits[(k & 0xF)]);
    }
    return localStringBuilder.toString();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.common.base.ByteArrays
 * JD-Core Version:    0.6.2
 */