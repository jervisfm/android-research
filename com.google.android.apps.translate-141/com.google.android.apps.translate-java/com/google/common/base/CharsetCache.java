package com.google.common.base;

import com.google.common.annotations.GoogleInternal;
import java.lang.ref.SoftReference;
import java.nio.charset.Charset;
import java.nio.charset.UnsupportedCharsetException;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@GoogleInternal
public final class CharsetCache
{
  private static final CharsetCache INSTANCE = new CharsetCache(new DefaultLookup(null), 100);
  private final ConcurrentMap<String, SoftReference<Charset>> hitCache;
  private final Function<String, Charset> lookupFunction;
  private final Map<String, Boolean> missCache;

  CharsetCache(Function<String, Charset> paramFunction, final int paramInt)
  {
    Preconditions.checkNotNull(paramFunction);
    if (paramInt > 1);
    for (boolean bool = true; ; bool = false)
    {
      Preconditions.checkArgument(bool);
      this.lookupFunction = paramFunction;
      this.hitCache = new ConcurrentHashMap();
      this.missCache = Collections.synchronizedMap(new LinkedHashMap(paramInt, 0.75F, true)
      {
        protected boolean removeEldestEntry(Map.Entry<String, Boolean> paramAnonymousEntry)
        {
          return size() > paramInt;
        }
      });
      return;
    }
  }

  public static Charset forName(String paramString)
  {
    return INSTANCE.lookup(paramString);
  }

  private Charset lookupAndCache(String paramString)
  {
    Charset localCharset;
    try
    {
      localCharset = (Charset)this.lookupFunction.apply(paramString);
      SoftReference localSoftReference = new SoftReference(localCharset);
      this.hitCache.put(paramString, localSoftReference);
      Iterator localIterator = localCharset.aliases().iterator();
      while (localIterator.hasNext())
      {
        String str = (String)localIterator.next();
        this.hitCache.put(str.toLowerCase(), localSoftReference);
      }
    }
    catch (UnsupportedCharsetException localUnsupportedCharsetException)
    {
      this.missCache.put(paramString, Boolean.TRUE);
      throw localUnsupportedCharsetException;
    }
    return localCharset;
  }

  Charset lookup(String paramString)
  {
    if (paramString != null);
    String str;
    for (boolean bool = true; ; bool = false)
    {
      Preconditions.checkArgument(bool, "Charset name may not be null");
      str = paramString.toLowerCase();
      SoftReference localSoftReference = (SoftReference)this.hitCache.get(str);
      if (localSoftReference == null)
        break;
      Charset localCharset = (Charset)localSoftReference.get();
      if (localCharset == null)
        break label82;
      return localCharset;
    }
    if (this.missCache.get(str) != null)
      throw new UnsupportedCharsetException(str);
    label82: return lookupAndCache(str);
  }

  private static class DefaultLookup
    implements Function<String, Charset>
  {
    public Charset apply(String paramString)
    {
      return Charset.forName(paramString);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.common.base.CharsetCache
 * JD-Core Version:    0.6.2
 */