package com.google.common.base;

import com.google.common.annotations.GoogleInternal;
import com.google.common.annotations.GwtCompatible;
import java.util.Iterator;
import javax.annotation.Nullable;

@GoogleInternal
@GwtCompatible
public abstract class Converter<A, B>
  implements Function<A, B>
{
  private static final Converter<?, ?> IDENTITY_CONVERTER = new IdentityConverter(null);

  public static <T> Converter<T, T> identity()
  {
    return IDENTITY_CONVERTER;
  }

  @Nullable
  public final B apply(@Nullable A paramA)
  {
    return doForward(paramA);
  }

  public <C> Converter<A, C> compose(Converter<B, C> paramConverter)
  {
    return ConverterComposition.of(this, (Converter)Preconditions.checkNotNull(paramConverter, "secondConverter"));
  }

  @Nullable
  public final B convert(@Nullable A paramA)
  {
    return doForward(paramA);
  }

  public Iterable<B> convertAll(final Iterable<? extends A> paramIterable)
  {
    Preconditions.checkNotNull(paramIterable, "fromIterable");
    return new Iterable()
    {
      public Iterator<B> iterator()
      {
        return new Iterator()
        {
          private final Iterator<? extends A> fromIterator = Converter.1.this.val$fromIterable.iterator();

          public boolean hasNext()
          {
            return this.fromIterator.hasNext();
          }

          public B next()
          {
            return Converter.this.doForward(this.fromIterator.next());
          }

          public void remove()
          {
            this.fromIterator.remove();
          }
        };
      }
    };
  }

  @Nullable
  protected abstract A doBackward(@Nullable B paramB);

  @Nullable
  protected abstract B doForward(@Nullable A paramA);

  public boolean equals(@Nullable Object paramObject)
  {
    return super.equals(paramObject);
  }

  public Converter<B, A> inverse()
  {
    return reverse();
  }

  public Converter<B, A> reverse()
  {
    return new ReverseConverter(this);
  }

  @Deprecated
  @Nullable
  public final A reverse(@Nullable B paramB)
  {
    return doBackward(paramB);
  }

  private static final class ConverterComposition<A, B, C> extends Converter<A, C>
  {
    final Converter<A, B> first;
    final Converter<B, C> second;

    ConverterComposition(Converter<A, B> paramConverter, Converter<B, C> paramConverter1)
    {
      this.first = paramConverter;
      this.second = paramConverter1;
    }

    static <A, B, C> Converter<A, C> of(Converter<A, B> paramConverter, Converter<B, C> paramConverter1)
    {
      if (paramConverter1 == Converter.IDENTITY_CONVERTER)
        return paramConverter;
      return new ConverterComposition(paramConverter, paramConverter1);
    }

    public A doBackward(@Nullable C paramC)
    {
      return this.first.doBackward(this.second.doBackward(paramC));
    }

    public C doForward(@Nullable A paramA)
    {
      return this.second.doForward(this.first.doForward(paramA));
    }

    public boolean equals(@Nullable Object paramObject)
    {
      if (this == paramObject)
        return true;
      if (((paramObject instanceof ConverterComposition)) && (this.first.equals(((ConverterComposition)paramObject).first)) && (this.second.equals(((ConverterComposition)paramObject).second)));
      for (boolean bool = true; ; bool = false)
        return bool;
    }

    public int hashCode()
    {
      return 31 * this.first.hashCode() + this.second.hashCode();
    }

    public String toString()
    {
      return this.first + ".compose(" + this.second + ")";
    }
  }

  private static class IdentityConverter extends Converter<Object, Object>
  {
    public <C> Converter<Object, C> compose(Converter<Object, C> paramConverter)
    {
      return (Converter)Preconditions.checkNotNull(paramConverter, "otherConverter");
    }

    public Iterable<Object> convertAll(Iterable<?> paramIterable)
    {
      return (Iterable)Preconditions.checkNotNull(paramIterable, "fromIterable");
    }

    public Object doBackward(@Nullable Object paramObject)
    {
      return paramObject;
    }

    public Object doForward(@Nullable Object paramObject)
    {
      return paramObject;
    }

    public Converter<Object, Object> reverse()
    {
      return this;
    }

    public String toString()
    {
      return "IdentityConverter";
    }
  }

  private static final class ReverseConverter<A, B> extends Converter<B, A>
  {
    final Converter<A, B> original;

    ReverseConverter(Converter<A, B> paramConverter)
    {
      this.original = paramConverter;
    }

    public B doBackward(@Nullable A paramA)
    {
      return this.original.doForward(paramA);
    }

    public A doForward(@Nullable B paramB)
    {
      return this.original.doBackward(paramB);
    }

    public boolean equals(@Nullable Object paramObject)
    {
      if (this == paramObject);
      while (((paramObject instanceof ReverseConverter)) && (this.original.equals(((ReverseConverter)paramObject).original)))
        return true;
      return false;
    }

    public int hashCode()
    {
      return 0xFFFFFFFF ^ this.original.hashCode();
    }

    public Converter<A, B> reverse()
    {
      return this.original;
    }

    public String toString()
    {
      return "ReverseConverter(" + this.original + ")";
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.common.base.Converter
 * JD-Core Version:    0.6.2
 */