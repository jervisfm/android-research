package com.google.common.base;

import com.google.common.annotations.GoogleInternal;
import com.google.common.annotations.Strongly;
import com.google.common.annotations.VisibleForTesting;

@GoogleInternal
public final class FP
{
  @Strongly(contact="java-core-libraries-team", date="2012-09-01")
  @Deprecated
  public static long fingerprint(String paramString)
  {
    return fingerprint(paramString.getBytes(Charsets.UTF_8));
  }

  @VisibleForTesting
  static long fingerprint(byte[] paramArrayOfByte)
  {
    return Hash.combineFingerprints(hash32(paramArrayOfByte, 0), hash32(paramArrayOfByte, 102072));
  }

  @VisibleForTesting
  static int hash32(byte[] paramArrayOfByte, int paramInt)
  {
    int i = 0;
    int j = paramArrayOfByte.length;
    int k = -1640531527;
    int m = -1640531527;
    int n = paramInt;
    int i1 = j;
    while (i1 >= 12)
    {
      int i3 = k + word32At(paramArrayOfByte, i);
      int i4 = m + word32At(paramArrayOfByte, i + 4);
      int i5 = n + word32At(paramArrayOfByte, i + 8);
      int i6 = i3 - i4 - i5 ^ i5 >>> 13;
      int i7 = i4 - i5 - i6 ^ i6 << 8;
      int i8 = i5 - i6 - i7 ^ i7 >>> 13;
      int i9 = i6 - i7 - i8 ^ i8 >>> 12;
      int i10 = i7 - i8 - i9 ^ i9 << 16;
      int i11 = i8 - i9 - i10 ^ i10 >>> 5;
      k = i9 - i10 - i11 ^ i11 >>> 3;
      m = i10 - i11 - k ^ k << 10;
      n = i11 - k - m ^ m >>> 15;
      i1 -= 12;
      i += 12;
    }
    int i2 = n + j;
    switch (i1)
    {
    default:
    case 11:
    case 10:
    case 9:
    case 8:
    case 7:
    case 6:
    case 5:
    case 4:
    case 3:
    case 2:
    case 1:
    }
    while (true)
    {
      return Hash.mix32(k, m, i2);
      i2 += (paramArrayOfByte[(i + 10)] << 24);
      i2 += ((0xFF & paramArrayOfByte[(i + 9)]) << 16);
      i2 += ((0xFF & paramArrayOfByte[(i + 8)]) << 8);
      m += word32At(paramArrayOfByte, i + 4);
      k += word32At(paramArrayOfByte, i);
      continue;
      m += ((0xFF & paramArrayOfByte[(i + 6)]) << 16);
      m += ((0xFF & paramArrayOfByte[(i + 5)]) << 8);
      m += (0xFF & paramArrayOfByte[(i + 4)]);
      k += word32At(paramArrayOfByte, i);
      continue;
      k += ((0xFF & paramArrayOfByte[(i + 2)]) << 16);
      k += ((0xFF & paramArrayOfByte[(i + 1)]) << 8);
      k += (0xFF & paramArrayOfByte[(i + 0)]);
    }
  }

  private static int word32At(byte[] paramArrayOfByte, int paramInt)
  {
    return 0xFF & paramArrayOfByte[(paramInt + 0)] | (0xFF & paramArrayOfByte[(paramInt + 1)]) << 8 | (0xFF & paramArrayOfByte[(paramInt + 2)]) << 16 | (0xFF & paramArrayOfByte[(paramInt + 3)]) << 24;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.common.base.FP
 * JD-Core Version:    0.6.2
 */