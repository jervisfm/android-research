package com.google.common.base;

import com.google.common.annotations.GoogleInternal;
import com.google.common.annotations.Strongly;

@GoogleInternal
public final class Fingerprinting
{
  @Strongly(contact="java-core-libraries-team", date="2012-09-01")
  @Deprecated
  public static long addToOrdered(long paramLong1, long paramLong2)
  {
    return paramLong2 + ((paramLong1 >>> 63) + (paramLong1 << 1));
  }

  @Strongly(contact="java-core-libraries-team", date="2012-09-01")
  @Deprecated
  public static long orderedFingerprint(long[] paramArrayOfLong)
  {
    long l = 3141592653589793238L;
    int i = paramArrayOfLong.length;
    for (int j = 0; j < i; j++)
      l = addToOrdered(l, paramArrayOfLong[j]);
    return l;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.common.base.Fingerprinting
 * JD-Core Version:    0.6.2
 */