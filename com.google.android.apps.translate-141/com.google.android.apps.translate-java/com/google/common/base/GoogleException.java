package com.google.common.base;

import com.google.common.annotations.GoogleInternal;

@Deprecated
@GoogleInternal
public class GoogleException extends Exception
{
  private static final long serialVersionUID = 1L;
  private String externalMessage = "A system error has occurred";
  private String internalMessage;

  public GoogleException()
  {
  }

  public GoogleException(String paramString)
  {
    super(paramString);
    setInternalMessage(paramString);
  }

  public GoogleException(String paramString1, String paramString2)
  {
    super(paramString1);
    setInternalMessage(paramString1);
    setExternalMessage(paramString2);
  }

  public GoogleException(Throwable paramThrowable)
  {
    super(paramThrowable);
    setInternalMessage(Throwables.getStackTraceAsString(paramThrowable));
  }

  public GoogleException(Throwable paramThrowable, String paramString)
  {
    super(paramString, paramThrowable);
    setInternalMessage(Throwables.getStackTraceAsString(paramThrowable));
    setExternalMessage(paramString);
  }

  public int getErrorCode()
  {
    return -999;
  }

  public String getExternalMessage()
  {
    return this.externalMessage;
  }

  public String getInternalMessage()
  {
    return this.internalMessage;
  }

  public String getMessage()
  {
    return getInternalMessage();
  }

  public void setExternalMessage(String paramString)
  {
    this.externalMessage = paramString;
  }

  public void setInternalMessage(String paramString)
  {
    this.internalMessage = paramString;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.common.base.GoogleException
 * JD-Core Version:    0.6.2
 */