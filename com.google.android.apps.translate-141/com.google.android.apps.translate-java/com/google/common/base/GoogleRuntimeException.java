package com.google.common.base;

import com.google.common.annotations.GoogleInternal;

@Deprecated
@GoogleInternal
public class GoogleRuntimeException extends RuntimeException
{
  private static final long serialVersionUID = 1L;
  private String externalMessage = "A system error has occurred";
  private String internalMessage;

  public GoogleRuntimeException()
  {
  }

  public GoogleRuntimeException(GoogleException paramGoogleException)
  {
    super(paramGoogleException);
    setInternalMessage(paramGoogleException.getInternalMessage());
    setExternalMessage(paramGoogleException.getExternalMessage());
  }

  public GoogleRuntimeException(String paramString)
  {
    super(paramString);
    setInternalMessage(paramString);
  }

  public GoogleRuntimeException(String paramString, Throwable paramThrowable)
  {
    super(paramString, paramThrowable);
    setInternalMessage(Throwables.getStackTraceAsString(paramThrowable));
    setExternalMessage(paramString);
  }

  public String getExternalMessage()
  {
    return this.externalMessage;
  }

  public String getInternalMessage()
  {
    return this.internalMessage;
  }

  public String getMessage()
  {
    return getInternalMessage();
  }

  public void setExternalMessage(String paramString)
  {
    this.externalMessage = paramString;
  }

  public void setInternalMessage(String paramString)
  {
    this.internalMessage = paramString;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.common.base.GoogleRuntimeException
 * JD-Core Version:    0.6.2
 */