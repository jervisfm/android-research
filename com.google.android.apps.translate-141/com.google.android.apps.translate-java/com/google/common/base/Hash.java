package com.google.common.base;

import com.google.common.annotations.GoogleInternal;
import com.google.common.annotations.Strongly;
import com.google.common.annotations.VisibleForTesting;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import javax.annotation.Nullable;

@GoogleInternal
public final class Hash
{
  private static final int CONSTANT32 = -1640531527;
  static final long CONSTANT64 = -2266404186210603134L;
  private static final int SEED32 = 314159265;
  static final long SEED64 = 3141592653589793238L;

  @VisibleForTesting
  static long combineFingerprints(int paramInt1, int paramInt2)
  {
    if ((paramInt1 == 0) && ((paramInt2 == 0) || (paramInt2 == 1)))
    {
      paramInt1 ^= 319790063;
      paramInt2 ^= -1801410264;
    }
    return paramInt1 << 32 | 0xFFFFFFFF & paramInt2;
  }

  @Strongly(contact="java-core-libraries-team", date="2012-09-01")
  @Deprecated
  public static long fingerprint(String paramString)
  {
    byte[] arrayOfByte = paramString.getBytes(Charsets.UTF_8);
    return fingerprint(arrayOfByte, 0, arrayOfByte.length);
  }

  @Strongly(contact="java-core-libraries-team", date="2012-09-01")
  @Deprecated
  public static long fingerprint(byte[] paramArrayOfByte)
  {
    if (paramArrayOfByte == null);
    for (int i = 0; ; i = paramArrayOfByte.length)
      return fingerprint(paramArrayOfByte, 0, i);
  }

  @Strongly(contact="java-core-libraries-team", date="2012-09-01")
  @Deprecated
  static long fingerprint(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    return combineFingerprints(hash32(paramArrayOfByte, paramInt1, paramInt2, 0), hash32(paramArrayOfByte, paramInt1, paramInt2, 102072));
  }

  @Strongly(contact="java-core-libraries-team", date="2012-09-01")
  @Deprecated
  public static byte[] fprint96(String paramString)
  {
    return fprint96(paramString.getBytes(Charsets.UTF_8));
  }

  @Strongly(contact="java-core-libraries-team", date="2012-09-01")
  @Deprecated
  public static byte[] fprint96(byte[] paramArrayOfByte)
  {
    try
    {
      MessageDigest localMessageDigest = MessageDigest.getInstance("SHA-1");
      localMessageDigest.update(paramArrayOfByte);
      return Arrays.copyOfRange(localMessageDigest.digest(), 0, 12);
    }
    catch (NoSuchAlgorithmException localNoSuchAlgorithmException)
    {
    }
    throw new IllegalStateException("SHA-1 should be available in the JVM.");
  }

  @Strongly(contact="java-core-libraries-team", date="2012-09-01")
  @Deprecated
  public static String fprint96AsKey(String paramString)
  {
    byte[] arrayOfByte = fprint96(paramString.getBytes(Charsets.UTF_8));
    char[] arrayOfChar = new char[12];
    for (int i = 0; i < 12; i++)
      arrayOfChar[i] = ((char)(0xFF & arrayOfByte[i]));
    return new String(arrayOfChar);
  }

  @Strongly(contact="java-core-libraries-team", date="2012-09-01")
  @Deprecated
  public static String fprint96AsString(String paramString)
  {
    IntBuffer localIntBuffer = ByteBuffer.wrap(fprint96(paramString)).asIntBuffer();
    long l1 = 0xFFFFFFFF & localIntBuffer.get();
    long l2 = 0xFFFFFFFF & localIntBuffer.get();
    long l3 = 0xFFFFFFFF & localIntBuffer.get();
    Object[] arrayOfObject = new Object[3];
    arrayOfObject[0] = Long.valueOf(l1);
    arrayOfObject[1] = Long.valueOf(l2);
    arrayOfObject[2] = Long.valueOf(l3);
    return String.format("%08X_%08X_%08X", arrayOfObject);
  }

  public static int fprint96KeyModShardFromKey(String paramString, int paramInt)
  {
    if (paramInt > 0);
    for (boolean bool = true; ; bool = false)
    {
      Preconditions.checkArgument(bool);
      IntBuffer localIntBuffer = ByteBuffer.wrap(keyToFprint96(paramString)).asIntBuffer();
      BigInteger localBigInteger1 = BigInteger.valueOf(paramInt);
      BigInteger localBigInteger2 = BigInteger.valueOf(0xFFFFFFFF & localIntBuffer.get());
      BigInteger localBigInteger3 = BigInteger.valueOf(0xFFFFFFFF & localIntBuffer.get());
      BigInteger localBigInteger4 = BigInteger.valueOf(0xFFFFFFFF & localIntBuffer.get());
      return localBigInteger2.shiftLeft(32).or(localBigInteger3).mod(localBigInteger1).shiftLeft(32).or(localBigInteger4).mod(localBigInteger1).intValue();
    }
  }

  @Strongly(contact="java-core-libraries-team", date="2012-09-01")
  @Deprecated
  public static int hash32(int paramInt)
  {
    return mix32(paramInt, -1640531527, 314159265);
  }

  @Strongly(contact="java-core-libraries-team", date="2012-09-01")
  @Deprecated
  public static int hash32(int paramInt1, int paramInt2)
  {
    return mix32(paramInt1, -1640531527, paramInt2);
  }

  @Strongly(contact="java-core-libraries-team", date="2012-09-01")
  @Deprecated
  public static int hash32(long paramLong)
  {
    return (int)mix64(paramLong, -2266404186210603134L, 3141592653589793238L);
  }

  @Strongly(contact="java-core-libraries-team", date="2012-09-01")
  @Deprecated
  public static int hash32(long paramLong1, long paramLong2)
  {
    return (int)mix64(paramLong1, -2266404186210603134L, paramLong2);
  }

  @Strongly(contact="java-core-libraries-team", date="2012-09-01")
  @Deprecated
  public static int hash32(@Nullable String paramString)
  {
    return hash32(paramString, 314159265);
  }

  @Strongly(contact="java-core-libraries-team", date="2012-09-01")
  @Deprecated
  public static int hash32(@Nullable String paramString, int paramInt)
  {
    if (paramString == null)
      return hash32(null, 0, 0, paramInt);
    return hash32(paramString.getBytes(), paramInt);
  }

  @Strongly(contact="java-core-libraries-team", date="2012-09-01")
  @Deprecated
  public static int hash32(@Nullable String paramString, Charset paramCharset)
  {
    return hash32(paramString, paramCharset, 314159265);
  }

  @VisibleForTesting
  static int hash32(@Nullable String paramString, Charset paramCharset, int paramInt)
  {
    Preconditions.checkNotNull(paramCharset);
    if (paramString == null)
      return hash32(null, 0, 0, paramInt);
    return hash32(paramString.getBytes(paramCharset), paramInt);
  }

  @Strongly(contact="java-core-libraries-team", date="2012-09-01")
  @Deprecated
  public static int hash32(byte[] paramArrayOfByte)
  {
    if (paramArrayOfByte == null);
    for (int i = 0; ; i = paramArrayOfByte.length)
      return hash32(paramArrayOfByte, 0, i);
  }

  @VisibleForTesting
  static int hash32(@Nullable byte[] paramArrayOfByte, int paramInt)
  {
    if (paramArrayOfByte == null);
    for (int i = 0; ; i = paramArrayOfByte.length)
      return hash32(paramArrayOfByte, 0, i, paramInt);
  }

  @VisibleForTesting
  static int hash32(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    return hash32(paramArrayOfByte, paramInt1, paramInt2, 314159265);
  }

  private static int hash32(byte[] paramArrayOfByte, int paramInt1, int paramInt2, int paramInt3)
  {
    int i = -1640531527;
    int j = i;
    int k = paramInt3;
    int m = paramInt2;
    while (m >= 12)
    {
      int i1 = i + word32At(paramArrayOfByte, paramInt1);
      int i2 = j + word32At(paramArrayOfByte, paramInt1 + 4);
      int i3 = k + word32At(paramArrayOfByte, paramInt1 + 8);
      int i4 = i1 - i2 - i3 ^ i3 >>> 13;
      int i5 = i2 - i3 - i4 ^ i4 << 8;
      int i6 = i3 - i4 - i5 ^ i5 >>> 13;
      int i7 = i4 - i5 - i6 ^ i6 >>> 12;
      int i8 = i5 - i6 - i7 ^ i7 << 16;
      int i9 = i6 - i7 - i8 ^ i8 >>> 5;
      i = i7 - i8 - i9 ^ i9 >>> 3;
      j = i8 - i9 - i ^ i << 10;
      k = i9 - i - j ^ j >>> 15;
      m -= 12;
      paramInt1 += 12;
    }
    int n = k + paramInt2;
    switch (m)
    {
    default:
    case 11:
    case 10:
    case 9:
    case 8:
    case 7:
    case 6:
    case 5:
    case 4:
    case 3:
    case 2:
    case 1:
    }
    while (true)
    {
      return mix32(i, j, n);
      n += (paramArrayOfByte[(paramInt1 + 10)] << 24);
      n += ((0xFF & paramArrayOfByte[(paramInt1 + 9)]) << 16);
      n += ((0xFF & paramArrayOfByte[(paramInt1 + 8)]) << 8);
      j += word32At(paramArrayOfByte, paramInt1 + 4);
      i += word32At(paramArrayOfByte, paramInt1);
      continue;
      j += ((0xFF & paramArrayOfByte[(paramInt1 + 6)]) << 16);
      j += ((0xFF & paramArrayOfByte[(paramInt1 + 5)]) << 8);
      j += (0xFF & paramArrayOfByte[(paramInt1 + 4)]);
      i += word32At(paramArrayOfByte, paramInt1);
      continue;
      i += ((0xFF & paramArrayOfByte[(paramInt1 + 2)]) << 16);
      i += ((0xFF & paramArrayOfByte[(paramInt1 + 1)]) << 8);
      i += (0xFF & paramArrayOfByte[(paramInt1 + 0)]);
    }
  }

  @Strongly(contact="java-core-libraries-team", date="2012-09-01")
  @Deprecated
  public static long hash64(long paramLong)
  {
    return mix64(paramLong, -2266404186210603134L, 3141592653589793238L);
  }

  @Strongly(contact="java-core-libraries-team", date="2012-09-01")
  @Deprecated
  public static long hash64(long paramLong1, long paramLong2)
  {
    return mix64(paramLong1, -2266404186210603134L, paramLong2);
  }

  @Strongly(contact="java-core-libraries-team", date="2012-09-01")
  @Deprecated
  public static long hash64(@Nullable String paramString)
  {
    return hash64(paramString, 3141592653589793238L);
  }

  @Strongly(contact="java-core-libraries-team", date="2012-09-01")
  @Deprecated
  public static long hash64(@Nullable String paramString, long paramLong)
  {
    if (paramString == null)
      return hash64(null, 0, 0, paramLong);
    return hash64(paramString.getBytes(), paramLong);
  }

  @Strongly(contact="java-core-libraries-team", date="2012-09-01")
  @Deprecated
  public static long hash64(@Nullable String paramString, Charset paramCharset)
  {
    return hash64(paramString, paramCharset, 3141592653589793238L);
  }

  @VisibleForTesting
  static long hash64(@Nullable String paramString, Charset paramCharset, long paramLong)
  {
    Preconditions.checkNotNull(paramCharset);
    if (paramString == null)
      return hash64(null, 0, 0, paramLong);
    return hash64(paramString.getBytes(paramCharset), paramLong);
  }

  @Strongly(contact="java-core-libraries-team", date="2012-09-01")
  @Deprecated
  public static long hash64(@Nullable byte[] paramArrayOfByte)
  {
    if (paramArrayOfByte == null);
    for (int i = 0; ; i = paramArrayOfByte.length)
      return hash64(paramArrayOfByte, 0, i);
  }

  @VisibleForTesting
  static long hash64(@Nullable byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    return hash64(paramArrayOfByte, paramInt1, paramInt2, 3141592653589793238L);
  }

  @VisibleForTesting
  static long hash64(byte[] paramArrayOfByte, int paramInt1, int paramInt2, long paramLong)
  {
    long l1 = -2266404186210603134L;
    long l2 = l1;
    long l3 = paramLong;
    int i = paramInt2;
    while (i >= 24)
    {
      long l5 = l1 + word64At(paramArrayOfByte, paramInt1);
      long l6 = l2 + word64At(paramArrayOfByte, paramInt1 + 8);
      long l7 = l3 + word64At(paramArrayOfByte, paramInt1 + 16);
      long l8 = l5 - l6 - l7 ^ l7 >>> 43;
      long l9 = l6 - l7 - l8 ^ l8 << 9;
      long l10 = l7 - l8 - l9 ^ l9 >>> 8;
      long l11 = l8 - l9 - l10 ^ l10 >>> 38;
      long l12 = l9 - l10 - l11 ^ l11 << 23;
      long l13 = l10 - l11 - l12 ^ l12 >>> 5;
      long l14 = l11 - l12 - l13 ^ l13 >>> 35;
      long l15 = l12 - l13 - l14 ^ l14 << 49;
      long l16 = l13 - l14 - l15 ^ l15 >>> 11;
      l1 = l14 - l15 - l16 ^ l16 >>> 12;
      l2 = l15 - l16 - l1 ^ l1 << 18;
      l3 = l16 - l1 - l2 ^ l2 >>> 22;
      i -= 24;
      paramInt1 += 24;
    }
    long l4 = l3 + paramInt2;
    switch (i)
    {
    default:
    case 23:
    case 22:
    case 21:
    case 20:
    case 19:
    case 18:
    case 17:
    case 16:
    case 15:
    case 14:
    case 13:
    case 12:
    case 11:
    case 10:
    case 9:
    case 8:
    case 7:
    case 6:
    case 5:
    case 4:
    case 3:
    case 2:
    case 1:
    }
    while (true)
    {
      return mix64(l1, l2, l4);
      l4 += (paramArrayOfByte[(paramInt1 + 22)] << 56);
      l4 += ((0xFF & paramArrayOfByte[(paramInt1 + 21)]) << 48);
      l4 += ((0xFF & paramArrayOfByte[(paramInt1 + 20)]) << 40);
      l4 += ((0xFF & paramArrayOfByte[(paramInt1 + 19)]) << 32);
      l4 += ((0xFF & paramArrayOfByte[(paramInt1 + 18)]) << 24);
      l4 += ((0xFF & paramArrayOfByte[(paramInt1 + 17)]) << 16);
      l4 += ((0xFF & paramArrayOfByte[(paramInt1 + 16)]) << 8);
      l2 += word64At(paramArrayOfByte, paramInt1 + 8);
      l1 += word64At(paramArrayOfByte, paramInt1);
      continue;
      l2 += ((0xFF & paramArrayOfByte[(paramInt1 + 14)]) << 48);
      l2 += ((0xFF & paramArrayOfByte[(paramInt1 + 13)]) << 40);
      l2 += ((0xFF & paramArrayOfByte[(paramInt1 + 12)]) << 32);
      l2 += ((0xFF & paramArrayOfByte[(paramInt1 + 11)]) << 24);
      l2 += ((0xFF & paramArrayOfByte[(paramInt1 + 10)]) << 16);
      l2 += ((0xFF & paramArrayOfByte[(paramInt1 + 9)]) << 8);
      l2 += (0xFF & paramArrayOfByte[(paramInt1 + 8)]);
      l1 += word64At(paramArrayOfByte, paramInt1);
      continue;
      l1 += ((0xFF & paramArrayOfByte[(paramInt1 + 6)]) << 48);
      l1 += ((0xFF & paramArrayOfByte[(paramInt1 + 5)]) << 40);
      l1 += ((0xFF & paramArrayOfByte[(paramInt1 + 4)]) << 32);
      l1 += ((0xFF & paramArrayOfByte[(paramInt1 + 3)]) << 24);
      l1 += ((0xFF & paramArrayOfByte[(paramInt1 + 2)]) << 16);
      l1 += ((0xFF & paramArrayOfByte[(paramInt1 + 1)]) << 8);
      l1 += (0xFF & paramArrayOfByte[(paramInt1 + 0)]);
    }
  }

  @VisibleForTesting
  static long hash64(@Nullable byte[] paramArrayOfByte, long paramLong)
  {
    if (paramArrayOfByte == null);
    for (int i = 0; ; i = paramArrayOfByte.length)
      return hash64(paramArrayOfByte, 0, i, paramLong);
  }

  @VisibleForTesting
  static byte[] keyToFprint96(String paramString)
  {
    byte[] arrayOfByte = new byte[12];
    for (int i = 0; i < 12; i++)
      arrayOfByte[i] = ((byte)(0xFF & paramString.charAt(i)));
    return arrayOfByte;
  }

  @VisibleForTesting
  static int mix32(int paramInt1, int paramInt2, int paramInt3)
  {
    int i = paramInt1 - paramInt2 - paramInt3 ^ paramInt3 >>> 13;
    int j = paramInt2 - paramInt3 - i ^ i << 8;
    int k = paramInt3 - i - j ^ j >>> 13;
    int m = i - j - k ^ k >>> 12;
    int n = j - k - m ^ m << 16;
    int i1 = k - m - n ^ n >>> 5;
    int i2 = m - n - i1 ^ i1 >>> 3;
    int i3 = n - i1 - i2 ^ i2 << 10;
    return i1 - i2 - i3 ^ i3 >>> 15;
  }

  @VisibleForTesting
  static long mix64(long paramLong1, long paramLong2, long paramLong3)
  {
    long l1 = paramLong1 - paramLong2 - paramLong3 ^ paramLong3 >>> 43;
    long l2 = paramLong2 - paramLong3 - l1 ^ l1 << 9;
    long l3 = paramLong3 - l1 - l2 ^ l2 >>> 8;
    long l4 = l1 - l2 - l3 ^ l3 >>> 38;
    long l5 = l2 - l3 - l4 ^ l4 << 23;
    long l6 = l3 - l4 - l5 ^ l5 >>> 5;
    long l7 = l4 - l5 - l6 ^ l6 >>> 35;
    long l8 = l5 - l6 - l7 ^ l7 << 49;
    long l9 = l6 - l7 - l8 ^ l8 >>> 11;
    long l10 = l7 - l8 - l9 ^ l9 >>> 12;
    long l11 = l8 - l9 - l10 ^ l10 << 18;
    return l9 - l10 - l11 ^ l11 >>> 22;
  }

  private static int word32At(byte[] paramArrayOfByte, int paramInt)
  {
    return paramArrayOfByte[(paramInt + 0)] + (paramArrayOfByte[(paramInt + 1)] << 8) + (paramArrayOfByte[(paramInt + 2)] << 16) + (paramArrayOfByte[(paramInt + 3)] << 24);
  }

  private static long word64At(byte[] paramArrayOfByte, int paramInt)
  {
    return (0xFF & paramArrayOfByte[(paramInt + 0)]) + ((0xFF & paramArrayOfByte[(paramInt + 1)]) << 8) + ((0xFF & paramArrayOfByte[(paramInt + 2)]) << 16) + ((0xFF & paramArrayOfByte[(paramInt + 3)]) << 24) + ((0xFF & paramArrayOfByte[(paramInt + 4)]) << 32) + ((0xFF & paramArrayOfByte[(paramInt + 5)]) << 40) + ((0xFF & paramArrayOfByte[(paramInt + 6)]) << 48) + ((0xFF & paramArrayOfByte[(paramInt + 7)]) << 56);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.common.base.Hash
 * JD-Core Version:    0.6.2
 */