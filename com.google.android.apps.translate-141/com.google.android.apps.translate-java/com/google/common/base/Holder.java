package com.google.common.base;

import com.google.common.annotations.GoogleInternal;
import com.google.common.annotations.GwtCompatible;
import com.google.common.annotations.Strongly;
import javax.annotation.Nullable;

@Strongly(contact="java-libraries-discuss")
@Deprecated
@GoogleInternal
@GwtCompatible
public final class Holder<T>
{

  @Nullable
  private T instance;

  private Holder(@Nullable T paramT)
  {
    this.instance = paramT;
  }

  public static <T> Holder<T> absent()
  {
    return new Holder(null);
  }

  public static <T> Holder<T> of(T paramT)
  {
    return new Holder(Preconditions.checkNotNull(paramT));
  }

  public static <T> Holder<T> unset()
  {
    return absent();
  }

  public T get()
  {
    Preconditions.checkState(isPresent());
    return this.instance;
  }

  public boolean isPresent()
  {
    return this.instance != null;
  }

  public boolean isSet()
  {
    return isPresent();
  }

  public T or(T paramT)
  {
    Preconditions.checkNotNull(paramT, "use orNull() instead of or(null)");
    if (isPresent())
      paramT = this.instance;
    return paramT;
  }

  @Nullable
  public T orNull()
  {
    return this.instance;
  }

  public void set(T paramT)
  {
    setNullable(Preconditions.checkNotNull(paramT));
  }

  public void setNullable(@Nullable T paramT)
  {
    this.instance = paramT;
  }

  public String toString()
  {
    if (isPresent())
      return "Holder.of(" + this.instance + ")";
    return "Holder.absent()";
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.common.base.Holder
 * JD-Core Version:    0.6.2
 */