package com.google.common.base;

import com.google.common.annotations.GoogleInternal;
import com.google.common.annotations.Strongly;
import java.io.InterruptedIOException;

@Strongly(contact="java-libraries-discuss")
@Deprecated
@GoogleInternal
public class InterruptedRuntimeException extends RuntimeException
{
  private static final long serialVersionUID = 1L;

  public InterruptedRuntimeException()
  {
    Thread.currentThread().interrupt();
  }

  public InterruptedRuntimeException(InterruptedIOException paramInterruptedIOException)
  {
    super(paramInterruptedIOException);
    Thread.currentThread().interrupt();
  }

  public InterruptedRuntimeException(InterruptedException paramInterruptedException)
  {
    super(paramInterruptedException);
    Thread.currentThread().interrupt();
  }

  public InterruptedRuntimeException(String paramString)
  {
    super(paramString);
    Thread.currentThread().interrupt();
  }

  public InterruptedRuntimeException(String paramString, InterruptedIOException paramInterruptedIOException)
  {
    super(paramString, paramInterruptedIOException);
    Thread.currentThread().interrupt();
  }

  public InterruptedRuntimeException(String paramString, InterruptedException paramInterruptedException)
  {
    super(paramString, paramInterruptedException);
    Thread.currentThread().interrupt();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.common.base.InterruptedRuntimeException
 * JD-Core Version:    0.6.2
 */