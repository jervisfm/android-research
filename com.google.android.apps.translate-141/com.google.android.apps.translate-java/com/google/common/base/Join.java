package com.google.common.base;

import com.google.common.annotations.GoogleInternal;
import com.google.common.annotations.GwtCompatible;
import java.io.IOException;
import java.util.AbstractList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;
import javax.annotation.Nullable;

@GoogleInternal
@GwtCompatible
public final class Join
{
  private static Iterable<Object> iterable(final Object paramObject, Object[] paramArrayOfObject)
  {
    Preconditions.checkNotNull(paramArrayOfObject);
    return new AbstractList()
    {
      public Object get(int paramAnonymousInt)
      {
        if (paramAnonymousInt == 0)
          return paramObject;
        return this.val$rest[(paramAnonymousInt - 1)];
      }

      public int size()
      {
        return 1 + this.val$rest.length;
      }
    };
  }

  @Deprecated
  public static <T extends Appendable> T join(T paramT, String paramString, Iterable<?> paramIterable)
  {
    try
    {
      Appendable localAppendable = Joiner.on(paramString).useForNull("null").appendTo(paramT, paramIterable);
      return localAppendable;
    }
    catch (IOException localIOException)
    {
      throw new RuntimeException(localIOException);
    }
  }

  @Deprecated
  public static <T extends Appendable> T join(T paramT, String paramString, Object[] paramArrayOfObject)
  {
    try
    {
      Appendable localAppendable = Joiner.on(paramString).useForNull("null").appendTo(paramT, Arrays.asList(paramArrayOfObject));
      return localAppendable;
    }
    catch (IOException localIOException)
    {
      throw new RuntimeException(localIOException);
    }
  }

  @Deprecated
  public static String join(String paramString, Iterable<?> paramIterable)
  {
    return Joiner.on(paramString).useForNull("null").join(paramIterable);
  }

  @Deprecated
  public static String join(String paramString, @Nullable Object paramObject, Object[] paramArrayOfObject)
  {
    return Joiner.on(paramString).useForNull("null").join(iterable(paramObject, paramArrayOfObject));
  }

  @Deprecated
  public static String join(String paramString1, String paramString2, Map<?, ?> paramMap)
  {
    return Joiner.on(paramString2).useForNull("null").withKeyValueSeparator(paramString1).join(paramMap);
  }

  @Deprecated
  public static String join(String paramString, Iterator<?> paramIterator)
  {
    return Joiner.on(paramString).useForNull("null").join(paramIterator);
  }

  @Deprecated
  public static String join(String paramString, Object[] paramArrayOfObject)
  {
    return Joiner.on(paramString).useForNull("null").join(paramArrayOfObject);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.common.base.Join
 * JD-Core Version:    0.6.2
 */