package com.google.common.base;

import com.google.common.annotations.GoogleInternal;
import com.google.common.logging.Logger;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.SQLException;

@Deprecated
@GoogleInternal
public final class Log2
{
  public static final String LOG2_USE_JAVA_LOG_PROP = "google.log2UseJavaLog";
  private static Logger defaultLog = null;
  private static Logger systemErrLog = null;
  public static boolean useJavaLogging = false;

  static
  {
    useJavaLogging = Boolean.getBoolean("google.log2UseJavaLog");
    if (useJavaLogging)
    {
      systemErrLog = new Log2Logger();
      defaultLog = systemErrLog;
    }
    if (defaultLog == null);
    try
    {
      systemErrLog = new LogWriter(new BufferedWriter(new OutputStreamWriter(System.err)));
      defaultLog = systemErrLog;
      return;
    }
    catch (Throwable localThrowable)
    {
    }
    throw new RuntimeException("Log2: could not initialize  LogWriter object!");
  }

  @Deprecated
  public static Logger getDefaultLog()
  {
    return defaultLog;
  }

  public static String getExceptionTrace(Throwable paramThrowable)
  {
    StringWriter localStringWriter = new StringWriter();
    PrintWriter localPrintWriter = new PrintWriter(localStringWriter);
    if ((paramThrowable instanceof SQLException))
    {
      SQLException localSQLException = (SQLException)paramThrowable;
      localPrintWriter.println("SQLException: errorCode=" + localSQLException.getErrorCode() + " sqlState=" + localSQLException.getSQLState());
      localSQLException.printStackTrace(localPrintWriter);
      localSQLException = localSQLException.getNextException();
      if (localSQLException != null);
    }
    while (true)
    {
      return localStringWriter.toString();
      localPrintWriter.println("chained to:");
      break;
      paramThrowable.printStackTrace(localPrintWriter);
    }
  }

  public static int getThreshold()
  {
    return defaultLog.getThreshold();
  }

  public static void logDebug(String paramString)
  {
    defaultLog.logDebug(paramString);
  }

  public static void logError(String paramString)
  {
    defaultLog.logError(paramString);
  }

  public static void logEvent(String paramString)
  {
    defaultLog.logEvent(paramString);
  }

  public static void logException(Throwable paramThrowable)
  {
    defaultLog.logException(paramThrowable);
  }

  public static void logException(Throwable paramThrowable, String paramString)
  {
    defaultLog.logException(paramThrowable, paramString);
  }

  public static void logSevereException(Throwable paramThrowable, String paramString)
  {
    defaultLog.logSevereException(paramThrowable, paramString);
  }

  @Deprecated
  public static void setDefaultLog(Logger paramLogger)
  {
    try
    {
      if ((defaultLog != null) && (defaultLog != systemErrLog))
        defaultLog.close();
      if (paramLogger != null);
      for (defaultLog = paramLogger; ; defaultLog = systemErrLog)
        return;
    }
    finally
    {
    }
  }

  public static void setThreadTag(String paramString)
  {
    defaultLog.setThreadTag(paramString);
  }

  public static void setThreshold(int paramInt)
  {
    defaultLog.setThreshold(paramInt);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.common.base.Log2
 * JD-Core Version:    0.6.2
 */