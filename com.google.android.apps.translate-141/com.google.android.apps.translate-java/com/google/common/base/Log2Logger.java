package com.google.common.base;

import com.google.common.annotations.GoogleInternal;
import com.google.common.logging.Log2FileHandler;
import com.google.common.logging.Log2Formatter;
import com.google.common.logging.LogContext;
import java.text.DateFormat;
import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.LogRecord;
import javax.annotation.Nullable;

@Deprecated
@GoogleInternal
public final class Log2Logger
  implements com.google.common.logging.Logger
{
  private static final java.util.logging.Logger log = java.util.logging.Logger.getLogger("com.google.common.base.Log2");
  private static boolean selfInstalledHandler = false;
  private static boolean useRootHandler = false;
  private int threshold;

  Log2Logger()
  {
    if ((log.getHandlers().length == 0) && (!useRootHandler))
    {
      ConsoleHandler localConsoleHandler = new ConsoleHandler();
      localConsoleHandler.setFormatter(new Log2Formatter());
      addHandler(localConsoleHandler);
      setThreshold(0);
    }
  }

  Log2Logger(String paramString1, String paramString2, @Nullable String paramString3, DateFormat paramDateFormat1, DateFormat paramDateFormat2, long paramLong)
  {
    Handler[] arrayOfHandler = log.getHandlers();
    if (((arrayOfHandler.length == 0) || (selfInstalledHandler)) && (!useRootHandler))
    {
      if (selfInstalledHandler)
        log.removeHandler(arrayOfHandler[0]);
      addHandler(new Log2FileHandler(paramString1, paramString2, paramString3, paramDateFormat1, paramDateFormat2, paramLong));
      setThreshold(0);
    }
  }

  private void addHandler(Handler paramHandler)
  {
    log.addHandler(paramHandler);
    String str1 = LogManager.getLogManager().getProperty(log.getName() + ".useParentHandlers");
    if (str1 == null);
    for (String str2 = ""; ; str2 = str1.toLowerCase())
    {
      boolean bool1;
      if (!str2.equals("true"))
      {
        boolean bool2 = str2.equals("1");
        bool1 = false;
        if (!bool2);
      }
      else
      {
        bool1 = true;
      }
      log.setUseParentHandlers(bool1);
      selfInstalledHandler = true;
      return;
    }
  }

  private void doCallerInference(LogRecord paramLogRecord)
  {
    String str = Log2.class.getName();
    StackTraceElement[] arrayOfStackTraceElement = new Throwable().getStackTrace();
    int i = 2147483647;
    int j = 0;
    if (j < arrayOfStackTraceElement.length)
    {
      if (arrayOfStackTraceElement[j].getClassName().equals(str))
        i = j;
      while (i == 2147483647)
      {
        j++;
        break;
      }
      paramLogRecord.setSourceClassName(arrayOfStackTraceElement[j].getClassName());
      paramLogRecord.setSourceMethodName(arrayOfStackTraceElement[j].getMethodName());
    }
  }

  private void logAfterSettingCaller(Level paramLevel, String paramString)
  {
    logAfterSettingCaller(paramLevel, paramString, null);
  }

  private void logAfterSettingCaller(Level paramLevel, String paramString, Throwable paramThrowable)
  {
    LogRecord localLogRecord = new LogRecord(paramLevel, paramString);
    if (paramThrowable != null)
      localLogRecord.setThrown(paramThrowable);
    doCallerInference(localLogRecord);
    log.log(localLogRecord);
  }

  public static void useRootHandler()
  {
    if (!useRootHandler)
    {
      for (Handler localHandler : log.getHandlers())
        log.removeHandler(localHandler);
      log.setUseParentHandlers(true);
      useRootHandler = true;
    }
  }

  public void close()
  {
    Handler[] arrayOfHandler = log.getHandlers();
    int i = arrayOfHandler.length;
    for (int j = 0; j < i; j++)
      arrayOfHandler[j].close();
  }

  public String getThreadTag()
  {
    return LogContext.getThreadTag();
  }

  public int getThreshold()
  {
    return this.threshold;
  }

  public void logDebug(String paramString)
  {
    if (log.isLoggable(Level.FINE))
      logAfterSettingCaller(Level.FINE, paramString);
  }

  public void logError(String paramString)
  {
    logAfterSettingCaller(Level.WARNING, paramString + "\n" + Log2.getExceptionTrace(new LoggedError(null)));
  }

  public void logEvent(String paramString)
  {
    if (log.isLoggable(Level.INFO))
    {
      LogRecord localLogRecord = new LogRecord(Level.INFO, paramString);
      doCallerInference(localLogRecord);
      log.log(localLogRecord);
    }
  }

  public void logException(Throwable paramThrowable)
  {
    logException(paramThrowable, "");
  }

  public void logException(Throwable paramThrowable, String paramString)
  {
    if (log.isLoggable(Level.WARNING))
      logAfterSettingCaller(Level.WARNING, paramString, paramThrowable);
  }

  public void logSevereException(Throwable paramThrowable)
  {
    logSevereException(paramThrowable, "");
  }

  public void logSevereException(Throwable paramThrowable, String paramString)
  {
    if (log.isLoggable(Level.SEVERE))
      logAfterSettingCaller(Level.SEVERE, paramString, paramThrowable);
  }

  public void logTimedEvent(String paramString, long paramLong1, long paramLong2)
  {
    log.info(paramLong2 - paramLong1 + " ms.: " + paramString);
  }

  public void setErrorEmail(String paramString)
  {
  }

  public void setThreadTag(String paramString)
  {
    LogContext.setThreadTag(paramString);
  }

  public void setThreshold(int paramInt)
  {
    Level localLevel;
    if (paramInt == 2)
      localLevel = Level.WARNING;
    while (true)
    {
      log.setLevel(localLevel);
      if (selfInstalledHandler)
        log.getHandlers()[0].setLevel(localLevel);
      this.threshold = paramInt;
      return;
      if (paramInt == 1)
      {
        localLevel = Level.INFO;
      }
      else
      {
        if (paramInt != 0)
          break;
        localLevel = Level.FINEST;
      }
    }
    throw new RuntimeException("Invalid level passed to Log2Logger.setThreshold ");
  }

  private static class LoggedError extends Throwable
  {
    private static final long serialVersionUID = 1L;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.common.base.Log2Logger
 * JD-Core Version:    0.6.2
 */