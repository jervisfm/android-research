package com.google.common.base;

import com.google.common.annotations.GoogleInternal;
import com.google.common.logging.Logger;
import java.io.IOException;
import java.io.PrintStream;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

@Deprecated
@GoogleInternal
class LogWriter
  implements Logger
{
  static final int DEFAULT_THRESHOLD = 0;
  private static final String LEVEL_IDS = "DIX";
  SimpleDateFormat dateFormatter = null;
  ThreadLocal<String> threadTagMap = new ThreadLocal();
  private int threshold;
  Writer writer = null;

  LogWriter()
  {
  }

  public LogWriter(Writer paramWriter)
  {
    this.writer = paramWriter;
    this.dateFormatter = new SimpleDateFormat("yyMMdd HH:mm:ss.SSS");
    this.threshold = 0;
  }

  public void close()
  {
    try
    {
      this.writer.flush();
      this.writer.close();
      return;
    }
    catch (Exception localException)
    {
    }
  }

  public String getThreadTag()
  {
    return (String)this.threadTagMap.get();
  }

  public int getThreshold()
  {
    return this.threshold;
  }

  public void logDebug(String paramString)
  {
    write(0, paramString);
  }

  public void logError(String paramString)
  {
    write(2, paramString + "\n" + Log2.getExceptionTrace(new LoggedError()));
  }

  public void logEvent(String paramString)
  {
    write(1, paramString);
  }

  public void logException(Throwable paramThrowable)
  {
    logException(paramThrowable, "");
  }

  public void logException(Throwable paramThrowable, String paramString)
  {
    String str = paramThrowable.getMessage();
    if (str != null)
      paramString = paramString + ": " + str;
    write(2, paramString + "\n" + Log2.getExceptionTrace(paramThrowable));
  }

  public void logSevereException(Throwable paramThrowable)
  {
    logSevereException(paramThrowable, "");
  }

  public void logSevereException(Throwable paramThrowable, String paramString)
  {
    String str = paramThrowable.getMessage();
    if (str != null)
      paramString = paramString + ": " + str;
    write(2, paramString + "\n" + Log2.getExceptionTrace(paramThrowable));
  }

  public void logTimedEvent(String paramString, long paramLong1, long paramLong2)
  {
    write(1, paramLong2 - paramLong1 + " ms.: " + paramString);
  }

  public void setErrorEmail(String paramString)
  {
  }

  public void setThreadTag(String paramString)
  {
    this.threadTagMap.set(paramString);
  }

  public void setThreshold(int paramInt)
  {
    if ((paramInt < 0) || (paramInt > 3))
      throw new RuntimeException("RotatingLog#setThreshold(int) : invalid threshold value: " + paramInt);
    this.threshold = paramInt;
  }

  protected void write(int paramInt, String paramString)
  {
    try
    {
      int i = this.threshold;
      if (paramInt < i);
      while (true)
      {
        return;
        String str1 = getThreadTag();
        if (str1 == null);
        StringBuffer localStringBuffer;
        String str2;
        for (Object localObject2 = ""; ; localObject2 = str2)
        {
          char c = "DIX".charAt(paramInt);
          localStringBuffer = new StringBuffer(1000);
          Iterator localIterator = Splitter.on('\n').omitEmptyStrings().split(paramString).iterator();
          while (localIterator.hasNext())
          {
            String str3 = (String)localIterator.next();
            if (this.dateFormatter != null)
            {
              localStringBuffer.append(this.dateFormatter.format(new Date()));
              localStringBuffer.append(':');
            }
            localStringBuffer.append(c);
            localStringBuffer.append(' ');
            localStringBuffer.append((String)localObject2);
            localStringBuffer.append(str3);
            localStringBuffer.append("\n");
            c = ' ';
          }
          str2 = str1 + " ";
        }
        try
        {
          this.writer.write(localStringBuffer.toString());
          this.writer.flush();
        }
        catch (IOException localIOException)
        {
          System.err.println("LogWriter#write(int, String) : error in writing to log!\n Exception thrown: " + localIOException.getMessage() + "\nlog entry: " + paramString);
        }
      }
    }
    finally
    {
    }
  }

  static class LoggedError extends Throwable
  {
    private static final long serialVersionUID = 1L;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.common.base.LogWriter
 * JD-Core Version:    0.6.2
 */