package com.google.common.base;

import com.google.common.annotations.Beta;
import com.google.common.annotations.GoogleInternal;
import com.google.common.annotations.GwtCompatible;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import javax.annotation.Nullable;

@GwtCompatible
public final class Objects
{
  @GoogleInternal
  public static boolean deepEquals(@Nullable Object paramObject1, @Nullable Object paramObject2)
  {
    return Arrays.deepEquals(new Object[] { paramObject1 }, new Object[] { paramObject2 });
  }

  @GoogleInternal
  public static int deepHashCode(@Nullable Object paramObject)
  {
    return -31 + Arrays.deepHashCode(new Object[] { paramObject });
  }

  @GoogleInternal
  public static String deepToString(@Nullable Object paramObject)
  {
    String str = Arrays.deepToString(new Object[] { paramObject });
    return str.substring(1, -1 + str.length());
  }

  public static boolean equal(@Nullable Object paramObject1, @Nullable Object paramObject2)
  {
    return (paramObject1 == paramObject2) || ((paramObject1 != null) && (paramObject1.equals(paramObject2)));
  }

  public static <T> T firstNonNull(@Nullable T paramT1, @Nullable T paramT2)
  {
    if (paramT1 != null)
      return paramT1;
    return Preconditions.checkNotNull(paramT2);
  }

  public static int hashCode(@Nullable Object[] paramArrayOfObject)
  {
    return Arrays.hashCode(paramArrayOfObject);
  }

  @GoogleInternal
  public static <T> T nonNull(T paramT)
  {
    if (paramT == null)
      throw new NullPointerException();
    return paramT;
  }

  @GoogleInternal
  public static <T> T nonNull(T paramT, @Nullable String paramString)
  {
    if (paramT == null)
      throw new NullPointerException(paramString);
    return paramT;
  }

  private static String simpleName(Class<?> paramClass)
  {
    String str = paramClass.getName().replaceAll("\\$[0-9]+", "\\$");
    int i = str.lastIndexOf('$');
    if (i == -1)
      i = str.lastIndexOf('.');
    return str.substring(i + 1);
  }

  public static ToStringHelper toStringHelper(Class<?> paramClass)
  {
    return new ToStringHelper(simpleName(paramClass), null);
  }

  public static ToStringHelper toStringHelper(Object paramObject)
  {
    return new ToStringHelper(simpleName(paramObject.getClass()), null);
  }

  public static ToStringHelper toStringHelper(String paramString)
  {
    return new ToStringHelper(paramString, null);
  }

  public static final class ToStringHelper
  {
    private final String className;
    private boolean omitNullValues = false;
    private final List<ValueHolder> valueHolders = new LinkedList();

    private ToStringHelper(String paramString)
    {
      this.className = ((String)Preconditions.checkNotNull(paramString));
    }

    private ValueHolder addHolder()
    {
      ValueHolder localValueHolder = new ValueHolder(null);
      this.valueHolders.add(localValueHolder);
      return localValueHolder;
    }

    private ValueHolder addHolder(@Nullable Object paramObject)
    {
      ValueHolder localValueHolder = addHolder();
      if (paramObject == null);
      for (boolean bool = true; ; bool = false)
      {
        localValueHolder.isNull = bool;
        return localValueHolder;
      }
    }

    private StringBuilder checkNameAndAppend(String paramString)
    {
      Preconditions.checkNotNull(paramString);
      return addHolder().builder.append(paramString).append('=');
    }

    public ToStringHelper add(String paramString, char paramChar)
    {
      checkNameAndAppend(paramString).append(paramChar);
      return this;
    }

    public ToStringHelper add(String paramString, double paramDouble)
    {
      checkNameAndAppend(paramString).append(paramDouble);
      return this;
    }

    public ToStringHelper add(String paramString, float paramFloat)
    {
      checkNameAndAppend(paramString).append(paramFloat);
      return this;
    }

    public ToStringHelper add(String paramString, int paramInt)
    {
      checkNameAndAppend(paramString).append(paramInt);
      return this;
    }

    public ToStringHelper add(String paramString, long paramLong)
    {
      checkNameAndAppend(paramString).append(paramLong);
      return this;
    }

    public ToStringHelper add(String paramString, @Nullable Object paramObject)
    {
      Preconditions.checkNotNull(paramString);
      addHolder(paramObject).builder.append(paramString).append('=').append(paramObject);
      return this;
    }

    public ToStringHelper add(String paramString, boolean paramBoolean)
    {
      checkNameAndAppend(paramString).append(paramBoolean);
      return this;
    }

    public ToStringHelper addValue(char paramChar)
    {
      addHolder().builder.append(paramChar);
      return this;
    }

    public ToStringHelper addValue(double paramDouble)
    {
      addHolder().builder.append(paramDouble);
      return this;
    }

    public ToStringHelper addValue(float paramFloat)
    {
      addHolder().builder.append(paramFloat);
      return this;
    }

    public ToStringHelper addValue(int paramInt)
    {
      addHolder().builder.append(paramInt);
      return this;
    }

    public ToStringHelper addValue(long paramLong)
    {
      addHolder().builder.append(paramLong);
      return this;
    }

    public ToStringHelper addValue(@Nullable Object paramObject)
    {
      addHolder(paramObject).builder.append(paramObject);
      return this;
    }

    public ToStringHelper addValue(boolean paramBoolean)
    {
      addHolder().builder.append(paramBoolean);
      return this;
    }

    @Beta
    public ToStringHelper omitNullValues()
    {
      this.omitNullValues = true;
      return this;
    }

    public String toString()
    {
      boolean bool = this.omitNullValues;
      int i = 0;
      StringBuilder localStringBuilder = new StringBuilder(32).append(this.className).append('{');
      Iterator localIterator = this.valueHolders.iterator();
      while (localIterator.hasNext())
      {
        ValueHolder localValueHolder = (ValueHolder)localIterator.next();
        if ((!bool) || (!localValueHolder.isNull))
        {
          if (i != 0)
            localStringBuilder.append(", ");
          while (true)
          {
            localStringBuilder.append(localValueHolder.builder);
            break;
            i = 1;
          }
        }
      }
      return '}';
    }

    private static final class ValueHolder
    {
      final StringBuilder builder = new StringBuilder();
      boolean isNull;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.common.base.Objects
 * JD-Core Version:    0.6.2
 */