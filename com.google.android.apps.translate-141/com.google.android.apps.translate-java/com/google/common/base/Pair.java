package com.google.common.base;

import com.google.common.annotations.GoogleInternal;
import com.google.common.annotations.GwtCompatible;
import java.io.Serializable;
import java.util.Comparator;
import javax.annotation.Nullable;

@GoogleInternal
@GwtCompatible(serializable=true)
public class Pair<A, B>
  implements Serializable
{
  private static final long serialVersionUID = 747826592375603043L;
  public final A first;
  public final B second;

  public Pair(@Nullable A paramA, @Nullable B paramB)
  {
    this.first = paramA;
    this.second = paramB;
  }

  public static <A extends Comparable, B> Comparator<Pair<A, B>> compareByFirst()
  {
    return FirstComparator.FIRST_COMPARATOR;
  }

  public static <A, B extends Comparable> Comparator<Pair<A, B>> compareBySecond()
  {
    return SecondComparator.SECOND_COMPARATOR;
  }

  public static <A, B> Function<Pair<A, B>, A> firstFunction()
  {
    return PairFirstFunction.INSTANCE;
  }

  public static <A, B> Pair<A, B> of(@Nullable A paramA, @Nullable B paramB)
  {
    return new Pair(paramA, paramB);
  }

  public static <A, B> Function<Pair<A, B>, B> secondFunction()
  {
    return PairSecondFunction.INSTANCE;
  }

  public boolean equals(@Nullable Object paramObject)
  {
    boolean bool1 = paramObject instanceof Pair;
    boolean bool2 = false;
    if (bool1)
    {
      Pair localPair = (Pair)paramObject;
      boolean bool3 = Objects.equal(this.first, localPair.first);
      bool2 = false;
      if (bool3)
      {
        boolean bool4 = Objects.equal(this.second, localPair.second);
        bool2 = false;
        if (bool4)
          bool2 = true;
      }
    }
    return bool2;
  }

  public A getFirst()
  {
    return this.first;
  }

  public B getSecond()
  {
    return this.second;
  }

  public int hashCode()
  {
    int i;
    if (this.first == null)
    {
      i = 0;
      if (this.second != null)
        break label36;
    }
    label36: for (int j = 0; ; j = this.second.hashCode())
    {
      return j + i * 31;
      i = this.first.hashCode();
      break;
    }
  }

  public String toString()
  {
    return "(" + this.first + ", " + this.second + ")";
  }

  private static enum FirstComparator
    implements Comparator<Pair<Comparable, Object>>
  {
    static
    {
      FirstComparator[] arrayOfFirstComparator = new FirstComparator[1];
      arrayOfFirstComparator[0] = FIRST_COMPARATOR;
    }

    public int compare(Pair<Comparable, Object> paramPair1, Pair<Comparable, Object> paramPair2)
    {
      return ((Comparable)paramPair1.getFirst()).compareTo(paramPair2.getFirst());
    }
  }

  private static final class PairFirstFunction<A, B>
    implements Function<Pair<A, B>, A>, Serializable
  {
    static final PairFirstFunction<Object, Object> INSTANCE = new PairFirstFunction();

    private Object readResolve()
    {
      return INSTANCE;
    }

    public A apply(Pair<A, B> paramPair)
    {
      return paramPair.getFirst();
    }
  }

  private static final class PairSecondFunction<A, B>
    implements Function<Pair<A, B>, B>, Serializable
  {
    static final PairSecondFunction<Object, Object> INSTANCE = new PairSecondFunction();

    private Object readResolve()
    {
      return INSTANCE;
    }

    public B apply(Pair<A, B> paramPair)
    {
      return paramPair.getSecond();
    }
  }

  private static enum SecondComparator
    implements Comparator<Pair<Object, Comparable>>
  {
    static
    {
      SecondComparator[] arrayOfSecondComparator = new SecondComparator[1];
      arrayOfSecondComparator[0] = SECOND_COMPARATOR;
    }

    public int compare(Pair<Object, Comparable> paramPair1, Pair<Object, Comparable> paramPair2)
    {
      return ((Comparable)paramPair1.getSecond()).compareTo(paramPair2.getSecond());
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.common.base.Pair
 * JD-Core Version:    0.6.2
 */