package com.google.common.base;

import com.google.common.annotations.GoogleInternal;
import java.security.SecureRandom;
import java.util.Random;

@GoogleInternal
public final class Randoms
{
  private static final Random RANDOM = new ReadOnlyRandom(null);
  private static final SecureRandom SECURE_RANDOM = newDefaultSecureRandom();

  public static Random insecureRandom()
  {
    return RANDOM;
  }

  public static Random insecureRandom(long paramLong)
  {
    return new Random(paramLong);
  }

  private static SecureRandom newDefaultSecureRandom()
  {
    SecureRandom localSecureRandom = new SecureRandom();
    localSecureRandom.nextLong();
    return localSecureRandom;
  }

  public static SecureRandom secureRandom()
  {
    return SECURE_RANDOM;
  }

  public static SecureRandom secureRandom(byte[] paramArrayOfByte)
  {
    SecureRandom localSecureRandom = new SecureRandom(paramArrayOfByte);
    localSecureRandom.nextLong();
    return localSecureRandom;
  }

  private static class ReadOnlyRandom extends Random
  {
    private static final long serialVersionUID = 898001275432099254L;
    private boolean initializationComplete = false;

    public void setSeed(long paramLong)
    {
      if (this.initializationComplete)
        throw new UnsupportedOperationException("Setting the seed on the shared Random object is not permitted");
      super.setSeed(paramLong);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.common.base.Randoms
 * JD-Core Version:    0.6.2
 */