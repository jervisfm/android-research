package com.google.common.base;

import com.google.common.annotations.GoogleInternal;
import com.google.common.annotations.GwtCompatible;
import javax.annotation.Nullable;

@Deprecated
@GoogleInternal
@GwtCompatible
public abstract interface Range<V extends Comparable<? super V>>
{
  @Deprecated
  public abstract boolean contains(V paramV);

  @Deprecated
  public abstract Range<V> enclose(V paramV);

  @Deprecated
  public abstract Range<V> enclosure(Range<V> paramRange);

  @Deprecated
  public abstract boolean equals(@Nullable Object paramObject);

  public abstract int hashCode();

  @Deprecated
  public abstract Range<V> intersection(Range<V> paramRange);

  @Deprecated
  public abstract boolean intersects(Range<V> paramRange);

  @Deprecated
  public abstract boolean isEmpty();

  @Deprecated
  public abstract V max();

  @Deprecated
  public abstract V min();
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.common.base.Range
 * JD-Core Version:    0.6.2
 */