package com.google.common.base;

import com.google.common.annotations.GoogleInternal;
import com.google.common.annotations.GwtCompatible;
import java.io.Serializable;
import javax.annotation.Nullable;

@GoogleInternal
@GwtCompatible(serializable=true)
final class RangeClosed<V extends Comparable<? super V>>
  implements Range<V>, Serializable
{
  private static final long serialVersionUID;
  private final V max;
  private final V min;

  RangeClosed(V paramV1, V paramV2)
  {
    if (paramV1 == null)
      throw new NullPointerException("Parameter 'min' is null.");
    if (paramV2 == null)
      throw new NullPointerException("Parameter 'max' is null.");
    if (paramV1.compareTo(paramV2) > 0)
      throw new IllegalArgumentException("Parameter 'min' cannot be greater than Parameter 'max'.");
    this.min = paramV1;
    this.max = paramV2;
  }

  public boolean contains(V paramV)
  {
    return (paramV != null) && (this.min.compareTo(paramV) < 1) && (this.max.compareTo(paramV) > -1);
  }

  public Range<V> enclose(V paramV)
  {
    if (paramV == null);
    do
    {
      return this;
      if (isEmpty())
        return new RangeClosed(paramV, paramV);
      if (this.min.compareTo(paramV) > 0)
        return new RangeClosed(paramV, this.max);
    }
    while (this.max.compareTo(paramV) >= 0);
    return new RangeClosed(this.min, paramV);
  }

  public Range<V> enclosure(Range<V> paramRange)
  {
    if (paramRange.isEmpty())
      return this;
    if (isEmpty())
      return paramRange;
    Comparable localComparable1 = paramRange.min();
    Comparable localComparable2 = paramRange.max();
    if (localComparable1.compareTo(this.min) < 0)
      if (localComparable2.compareTo(this.max) <= 0)
        break label78;
    while (true)
    {
      return new RangeClosed(localComparable1, localComparable2);
      localComparable1 = this.min;
      break;
      label78: localComparable2 = this.max;
    }
  }

  public boolean equals(@Nullable Object paramObject)
  {
    if ((paramObject instanceof Range))
    {
      Range localRange = (Range)paramObject;
      try
      {
        if (localRange.isEmpty())
          return false;
        if (localRange.min().equals(this.min))
        {
          boolean bool = localRange.max().equals(this.max);
          if (bool)
            return true;
        }
      }
      catch (ClassCastException localClassCastException)
      {
      }
    }
    return false;
  }

  public int hashCode()
  {
    return 555557 * this.min.hashCode() + this.max.hashCode();
  }

  public Range<V> intersection(Range<V> paramRange)
  {
    if (!intersects(paramRange))
      return Ranges.emptyRange();
    Comparable localComparable1 = paramRange.min();
    Comparable localComparable2 = paramRange.max();
    if (localComparable1.compareTo(this.min) > 0)
      if (localComparable2.compareTo(this.max) >= 0)
        break label70;
    while (true)
    {
      return new RangeClosed(localComparable1, localComparable2);
      localComparable1 = this.min;
      break;
      label70: localComparable2 = this.max;
    }
  }

  public boolean intersects(Range<V> paramRange)
  {
    int i = 1;
    if (paramRange.isEmpty())
      return false;
    if ((this.max.compareTo(paramRange.min()) > -1) && (this.min.compareTo(paramRange.max()) < i));
    while (true)
    {
      return i;
      int j = 0;
    }
  }

  public boolean isEmpty()
  {
    return false;
  }

  public V max()
  {
    return this.max;
  }

  public V min()
  {
    return this.min;
  }

  public String toString()
  {
    return "[Range:" + this.min + ", " + this.max + "]";
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.common.base.RangeClosed
 * JD-Core Version:    0.6.2
 */