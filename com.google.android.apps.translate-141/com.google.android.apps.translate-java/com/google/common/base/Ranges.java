package com.google.common.base;

import com.google.common.annotations.GoogleInternal;
import com.google.common.annotations.GwtCompatible;
import java.io.Serializable;
import java.util.NoSuchElementException;

@Deprecated
@GoogleInternal
@GwtCompatible
public final class Ranges
{
  @Deprecated
  @GwtCompatible(serializable=true)
  public static <V extends Comparable<? super V>> Range<V> emptyRange()
  {
    return EmptyRange.INSTANCE;
  }

  @Deprecated
  public static <V extends Comparable<? super V>> Range<V> enclose(Range<V> paramRange, Range<V>[] paramArrayOfRange)
  {
    Object localObject = paramRange;
    int i = paramArrayOfRange.length;
    for (int j = 0; j < i; j++)
      localObject = ((Range)localObject).enclosure(paramArrayOfRange[j]);
    return localObject;
  }

  @Deprecated
  @GwtCompatible(serializable=true)
  public static Range<Double> encloseDoubles(double[] paramArrayOfDouble)
  {
    if (paramArrayOfDouble.length == 0)
      return emptyRange();
    double d1 = paramArrayOfDouble[0];
    double d2 = paramArrayOfDouble[0];
    int i = paramArrayOfDouble.length;
    for (int j = 0; j < i; j++)
    {
      double d3 = paramArrayOfDouble[j];
      if (d1 > d3)
        d1 = d3;
      if (d2 < d3)
        d2 = d3;
    }
    return newRange(Double.valueOf(d1), Double.valueOf(d2));
  }

  @Deprecated
  @GwtCompatible(serializable=true)
  public static Range<Integer> encloseInts(int[] paramArrayOfInt)
  {
    if (paramArrayOfInt.length == 0)
      return emptyRange();
    int i = paramArrayOfInt[0];
    int j = paramArrayOfInt[0];
    int k = paramArrayOfInt.length;
    for (int m = 0; m < k; m++)
    {
      int n = paramArrayOfInt[m];
      if (i > n)
        i = n;
      if (j < n)
        j = n;
    }
    return newRange(Integer.valueOf(i), Integer.valueOf(j));
  }

  @Deprecated
  @GwtCompatible(serializable=true)
  public static Range<Long> encloseLongs(long[] paramArrayOfLong)
  {
    if (paramArrayOfLong.length == 0)
      return emptyRange();
    long l1 = paramArrayOfLong[0];
    long l2 = paramArrayOfLong[0];
    int i = paramArrayOfLong.length;
    for (int j = 0; j < i; j++)
    {
      long l3 = paramArrayOfLong[j];
      if (l1 > l3)
        l1 = l3;
      if (l2 < l3)
        l2 = l3;
    }
    return newRange(Long.valueOf(l1), Long.valueOf(l2));
  }

  @Deprecated
  public static <V extends Comparable<? super V>> Range<V> intersect(Range<V> paramRange, Range<V>[] paramArrayOfRange)
  {
    Object localObject = paramRange;
    int i = paramArrayOfRange.length;
    for (int j = 0; j < i; j++)
      localObject = ((Range)localObject).intersection(paramArrayOfRange[j]);
    return localObject;
  }

  @Deprecated
  @GwtCompatible(serializable=true)
  public static <V extends Comparable<? super V>> Range<V> newRange(V paramV1, V paramV2)
  {
    if (paramV2.compareTo(paramV1) > -1)
      return new RangeClosed(paramV1, paramV2);
    return emptyRange();
  }

  @Deprecated
  @GwtCompatible(serializable=true)
  public static <V extends Comparable<? super V>> Range<V> rangeOf(V[] paramArrayOfV)
  {
    Object localObject1 = null;
    Object localObject2 = null;
    int i = 1;
    int j = paramArrayOfV.length;
    int k = 0;
    if (k < j)
    {
      V ? = paramArrayOfV[k];
      if (? != null)
      {
        if (i == 0)
          break label49;
        i = 0;
        localObject1 = ?;
        localObject2 = ?;
      }
      while (true)
      {
        k++;
        break;
        label49: if (?.compareTo(localObject1) < 0)
          localObject1 = ?;
        if (?.compareTo(localObject2) > 0)
          localObject2 = ?;
      }
    }
    if (i != 0)
      return emptyRange();
    return newRange(localObject1, localObject2);
  }

  static class EmptyRange<V extends Comparable<? super V>>
    implements Range<V>, Serializable
  {
    private static final EmptyRange<Comparable<Object>> INSTANCE = new EmptyRange();
    private static final long serialVersionUID;

    public boolean contains(V paramV)
    {
      return false;
    }

    public Range<V> enclose(V paramV)
    {
      return Ranges.newRange(paramV, paramV);
    }

    public Range<V> enclosure(Range<V> paramRange)
    {
      return paramRange;
    }

    public boolean equals(Object paramObject)
    {
      return ((paramObject instanceof Range)) && (((Range)paramObject).isEmpty());
    }

    public int hashCode()
    {
      return 0;
    }

    public Range<V> intersection(Range<V> paramRange)
    {
      return this;
    }

    public boolean intersects(Range<V> paramRange)
    {
      return false;
    }

    public boolean isEmpty()
    {
      return true;
    }

    public V max()
    {
      throw new NoSuchElementException();
    }

    public V min()
    {
      throw new NoSuchElementException();
    }

    public String toString()
    {
      return "[Empty Range]";
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.common.base.Ranges
 * JD-Core Version:    0.6.2
 */