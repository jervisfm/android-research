package com.google.common.base;

import com.google.common.annotations.GoogleInternal;
import com.google.common.annotations.GwtCompatible;
import javax.annotation.Nullable;

@GoogleInternal
@GwtCompatible
public abstract interface Receiver<T>
{
  public abstract void accept(@Nullable T paramT);
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.common.base.Receiver
 * JD-Core Version:    0.6.2
 */