package com.google.common.base;

import com.google.common.annotations.GoogleInternal;
import com.google.common.annotations.GwtCompatible;
import java.util.Collection;
import java.util.Iterator;

@GoogleInternal
@GwtCompatible
public final class Receivers
{
  private static final Receiver<Object> IGNORING_RECEIVER = new Receiver()
  {
    public void accept(Object paramAnonymousObject)
    {
    }
  };

  public static <T> Receiver<T> collect(Collection<? super T> paramCollection)
  {
    return new Receiver()
    {
      public void accept(T paramAnonymousT)
      {
        this.val$collection.add(paramAnonymousT);
      }
    };
  }

  public static <T> Receiver<T> compose(Iterable<Receiver<T>> paramIterable)
  {
    return new Receiver()
    {
      public void accept(T paramAnonymousT)
      {
        Iterator localIterator = this.val$receivers.iterator();
        while (localIterator.hasNext())
          ((Receiver)localIterator.next()).accept(paramAnonymousT);
      }
    };
  }

  public static <T> Receiver<T> compose(Receiver<? super T>[] paramArrayOfReceiver)
  {
    return new Receiver()
    {
      public void accept(T paramAnonymousT)
      {
        Receiver[] arrayOfReceiver = this.val$receivers;
        int i = arrayOfReceiver.length;
        for (int j = 0; j < i; j++)
          arrayOfReceiver[j].accept(paramAnonymousT);
      }
    };
  }

  public static <T> Receiver<T> ignore()
  {
    return IGNORING_RECEIVER;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.common.base.Receivers
 * JD-Core Version:    0.6.2
 */