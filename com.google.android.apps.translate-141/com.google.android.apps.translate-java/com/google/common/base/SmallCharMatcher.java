package com.google.common.base;

import com.google.common.annotations.GwtCompatible;
import com.google.common.annotations.GwtIncompatible;
import com.google.common.annotations.VisibleForTesting;
import java.util.BitSet;

@GwtCompatible(emulated=true)
final class SmallCharMatcher extends CharMatcher.FastMatcher
{
  static final int MAX_SIZE = 63;
  static final int MAX_TABLE_SIZE = 128;
  private final boolean containsZero;
  final long filter;
  private final boolean reprobe;
  private final char[] table;

  private SmallCharMatcher(char[] paramArrayOfChar, long paramLong, boolean paramBoolean1, boolean paramBoolean2, String paramString)
  {
    super(paramString);
    this.table = paramArrayOfChar;
    this.filter = paramLong;
    this.containsZero = paramBoolean1;
    this.reprobe = paramBoolean2;
  }

  @VisibleForTesting
  static char[] buildTable(int paramInt, char[] paramArrayOfChar, boolean paramBoolean)
  {
    char[] arrayOfChar = new char[paramInt];
    int i = paramArrayOfChar.length;
    for (int j = 0; ; j++)
    {
      int k;
      int m;
      if (j < i)
      {
        k = paramArrayOfChar[j];
        m = k % paramInt;
        if (m < 0)
          m += paramInt;
        if ((arrayOfChar[m] != 0) && (!paramBoolean))
          arrayOfChar = null;
      }
      else
      {
        return arrayOfChar;
      }
      if (paramBoolean)
        while (arrayOfChar[m] != 0)
          m = (m + 1) % paramInt;
      arrayOfChar[m] = k;
    }
  }

  private boolean checkFilter(int paramInt)
  {
    return 1L == (1L & this.filter >> paramInt);
  }

  @GwtIncompatible("java.util.BitSet")
  static CharMatcher from(BitSet paramBitSet, String paramString)
  {
    char[] arrayOfChar = new char[paramBitSet.cardinality()];
    int i = paramBitSet.nextSetBit(0);
    int k;
    for (int j = 0; i != -1; j = k)
    {
      k = j + 1;
      arrayOfChar[j] = ((char)i);
      i = paramBitSet.nextSetBit(i + 1);
    }
    return from(arrayOfChar, paramString);
  }

  static CharMatcher from(char[] paramArrayOfChar, String paramString)
  {
    int i = paramArrayOfChar.length;
    if (paramArrayOfChar[0] == 0);
    long l;
    for (boolean bool1 = true; ; bool1 = false)
    {
      l = 0L;
      int j = paramArrayOfChar.length;
      for (int k = 0; k < j; k++)
        l |= 1L << paramArrayOfChar[k];
    }
    char[] arrayOfChar = null;
    for (int m = i; (arrayOfChar == null) && (m < 128); m++)
      arrayOfChar = buildTable(m, paramArrayOfChar, false);
    boolean bool2 = false;
    if (arrayOfChar == null)
    {
      arrayOfChar = buildTable(128, paramArrayOfChar, true);
      bool2 = true;
    }
    return new SmallCharMatcher(arrayOfChar, l, bool1, bool2, paramString);
  }

  public boolean matches(char paramChar)
  {
    boolean bool2;
    if (paramChar == 0)
      bool2 = this.containsZero;
    boolean bool1;
    do
    {
      return bool2;
      bool1 = checkFilter(paramChar);
      bool2 = false;
    }
    while (!bool1);
    int i = paramChar % this.table.length;
    if (i < 0)
      i += this.table.length;
    while (true)
    {
      int j = this.table[i];
      bool2 = false;
      if (j == 0)
        break;
      if (this.table[i] == paramChar)
        return true;
      boolean bool3 = this.reprobe;
      bool2 = false;
      if (!bool3)
        break;
      i = (i + 1) % this.table.length;
    }
  }

  @GwtIncompatible("java.util.BitSet")
  void setBits(BitSet paramBitSet)
  {
    if (this.containsZero)
      paramBitSet.set(0);
    for (int k : this.table)
      if (k != 0)
        paramBitSet.set(k);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.common.base.SmallCharMatcher
 * JD-Core Version:    0.6.2
 */