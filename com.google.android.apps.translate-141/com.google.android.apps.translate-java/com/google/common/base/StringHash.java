package com.google.common.base;

import com.google.common.annotations.GoogleInternal;
import com.google.common.annotations.Strongly;

@GoogleInternal
public final class StringHash
{
  @Strongly(contact="java-core-libraries-team", date="2012-09-01")
  @Deprecated
  public static long hash64(CharSequence paramCharSequence)
  {
    long l1 = -2266404186210603134L;
    long l2 = l1;
    long l3 = 3141592653589793238L;
    int i = 0;
    int j = paramCharSequence.length();
    while (j >= 12)
    {
      long l5 = l1 + word64At(paramCharSequence, i);
      long l6 = l2 + word64At(paramCharSequence, i + 4);
      long l7 = l3 + word64At(paramCharSequence, i + 8);
      long l8 = l5 - l6 - l7 ^ l7 >>> 43;
      long l9 = l6 - l7 - l8 ^ l8 << 9;
      long l10 = l7 - l8 - l9 ^ l9 >>> 8;
      long l11 = l8 - l9 - l10 ^ l10 >>> 38;
      long l12 = l9 - l10 - l11 ^ l11 << 23;
      long l13 = l10 - l11 - l12 ^ l12 >>> 5;
      long l14 = l11 - l12 - l13 ^ l13 >>> 35;
      long l15 = l12 - l13 - l14 ^ l14 << 49;
      long l16 = l13 - l14 - l15 ^ l15 >>> 11;
      l1 = l14 - l15 - l16 ^ l16 >>> 12;
      l2 = l15 - l16 - l1 ^ l1 << 18;
      l3 = l16 - l1 - l2 ^ l2 >>> 22;
      j -= 12;
      i += 12;
    }
    long l4 = l3 + paramCharSequence.length();
    switch (j)
    {
    default:
    case 11:
    case 10:
    case 9:
    case 8:
    case 7:
    case 6:
    case 5:
    case 4:
    case 3:
    case 2:
    case 1:
    }
    while (true)
    {
      return Hash.mix64(l1, l2, l4);
      l4 += ((0xFFFF & paramCharSequence.charAt(i + 10)) << 40);
      l4 += ((0xFFFF & paramCharSequence.charAt(i + 9)) << 24);
      l4 += ((0xFFFF & paramCharSequence.charAt(i + 8)) << 8);
      l2 += word64At(paramCharSequence, i + 4);
      l1 += word64At(paramCharSequence, i);
      continue;
      l2 += ((0xFFFF & paramCharSequence.charAt(i + 6)) << 32);
      l2 += ((0xFFFF & paramCharSequence.charAt(i + 5)) << 16);
      l2 += (0xFFFF & paramCharSequence.charAt(i + 4));
      l1 += word64At(paramCharSequence, i);
      continue;
      l1 += ((0xFFFF & paramCharSequence.charAt(i + 2)) << 32);
      l1 += ((0xFFFF & paramCharSequence.charAt(i + 1)) << 16);
      l1 += (0xFFFF & paramCharSequence.charAt(i));
    }
  }

  private static long word64At(CharSequence paramCharSequence, int paramInt)
  {
    return (0xFFFF & paramCharSequence.charAt(paramInt)) + ((0xFFFF & paramCharSequence.charAt(paramInt + 1)) << 16) + ((0xFFFF & paramCharSequence.charAt(paramInt + 2)) << 32) + ((0xFFFF & paramCharSequence.charAt(paramInt + 3)) << 48);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.common.base.StringHash
 * JD-Core Version:    0.6.2
 */