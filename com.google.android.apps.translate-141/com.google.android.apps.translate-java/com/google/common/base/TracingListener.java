package com.google.common.base;

import com.google.common.annotations.GoogleInternal;
import javax.annotation.Nullable;

@GoogleInternal
public abstract class TracingListener
{
  public void handleStartTracer(@Nullable String paramString1, @Nullable String paramString2, long paramLong)
  {
  }

  public void handleStopTracer(@Nullable String paramString1, @Nullable String paramString2, long paramLong1, long paramLong2)
  {
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.common.base.TracingListener
 * JD-Core Version:    0.6.2
 */