package com.google.common.base;

import com.google.common.annotations.GoogleInternal;

@GoogleInternal
public abstract interface TracingStatistic
{
  public abstract boolean enable();

  public abstract AtomicTracerStatMap getTracingStat();

  public abstract String getUnits();

  public abstract long start(Thread paramThread);

  public abstract long stop(Thread paramThread);
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.common.base.TracingStatistic
 * JD-Core Version:    0.6.2
 */