package com.google.common.base;

import com.google.common.annotations.GoogleInternal;

@GoogleInternal
public final class X
{
  public static void assertTrue(boolean paramBoolean)
  {
    if (!paramBoolean)
      throw new RuntimeException("Assertion failed");
  }

  public static void assertTrue(boolean paramBoolean, String paramString)
  {
    if (!paramBoolean)
      throw new RuntimeException("Assertion failed: " + paramString);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.common.base.X
 * JD-Core Version:    0.6.2
 */