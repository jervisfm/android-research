package com.google.common.base.genfiles;

import com.google.common.annotations.GoogleInternal;
import com.google.common.base.Preconditions;
import com.google.common.base.X;
import java.io.Serializable;

@GoogleInternal
public final class BooleanArray
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  private int length;
  private boolean[] list;

  @Deprecated
  public BooleanArray()
  {
    this.list = new boolean[4];
    this.length = 0;
  }

  @Deprecated
  public BooleanArray(int paramInt)
  {
    this.list = new boolean[paramInt];
    this.length = 0;
  }

  @Deprecated
  private BooleanArray(boolean[] paramArrayOfBoolean, int paramInt)
  {
    this.list = paramArrayOfBoolean;
    this.length = paramInt;
  }

  public BooleanArray(boolean[] paramArrayOfBoolean, int paramInt1, int paramInt2)
  {
    if (paramInt2 >= 0);
    for (boolean bool = true; ; bool = false)
    {
      X.assertTrue(bool);
      this.list = new boolean[paramInt2];
      this.length = paramInt2;
      System.arraycopy(paramArrayOfBoolean, paramInt1, this.list, 0, paramInt2);
      return;
    }
  }

  @Deprecated
  public static BooleanArray newInstance(boolean[] paramArrayOfBoolean)
  {
    Preconditions.checkNotNull(paramArrayOfBoolean);
    return new BooleanArray(paramArrayOfBoolean, paramArrayOfBoolean.length);
  }

  public void add(boolean paramBoolean)
  {
    if (this.length >= this.list.length)
      ensureCapacity(1 + this.length);
    boolean[] arrayOfBoolean = this.list;
    int i = this.length;
    this.length = (i + 1);
    arrayOfBoolean[i] = paramBoolean;
  }

  public void add(boolean[] paramArrayOfBoolean, int paramInt1, int paramInt2)
  {
    if (paramInt2 + this.length > this.list.length)
      ensureCapacity(paramInt2 + this.length);
    System.arraycopy(paramArrayOfBoolean, paramInt1, this.list, this.length, paramInt2);
    this.length = (paramInt2 + this.length);
  }

  public void addArray(BooleanArray paramBooleanArray)
  {
    add(paramBooleanArray.rep(), 0, paramBooleanArray.size());
  }

  public void clear()
  {
    this.length = 0;
  }

  public void ensureCapacity(int paramInt)
  {
    if (this.list.length < paramInt)
    {
      int i = 2 * this.list.length;
      if (i < paramInt)
        i = paramInt;
      boolean[] arrayOfBoolean = new boolean[i];
      System.arraycopy(this.list, 0, arrayOfBoolean, 0, this.length);
      this.list = arrayOfBoolean;
    }
  }

  public boolean get(int paramInt)
  {
    if ((paramInt >= 0) && (paramInt < this.length));
    for (boolean bool = true; ; bool = false)
    {
      X.assertTrue(bool);
      return this.list[paramInt];
    }
  }

  public boolean[] rep()
  {
    return this.list;
  }

  public void set(int paramInt, boolean paramBoolean)
  {
    if ((paramInt >= 0) && (paramInt < this.length));
    for (boolean bool = true; ; bool = false)
    {
      X.assertTrue(bool);
      this.list[paramInt] = paramBoolean;
      return;
    }
  }

  public int size()
  {
    return this.length;
  }

  public boolean[] toArray()
  {
    boolean[] arrayOfBoolean = new boolean[this.length];
    System.arraycopy(this.list, 0, arrayOfBoolean, 0, this.length);
    return arrayOfBoolean;
  }

  public void trimToSize()
  {
    if (this.list.length != this.length)
      this.list = toArray();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.common.base.genfiles.BooleanArray
 * JD-Core Version:    0.6.2
 */