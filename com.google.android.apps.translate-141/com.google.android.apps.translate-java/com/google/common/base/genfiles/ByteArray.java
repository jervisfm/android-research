package com.google.common.base.genfiles;

import com.google.common.annotations.GoogleInternal;
import com.google.common.base.Charsets;
import com.google.common.base.X;
import java.io.Serializable;

@GoogleInternal
public final class ByteArray
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  private int length;
  private byte[] list;

  @Deprecated
  public ByteArray()
  {
    this.list = new byte[4];
    this.length = 0;
  }

  @Deprecated
  public ByteArray(int paramInt)
  {
    this.list = new byte[paramInt];
    this.length = 0;
  }

  @Deprecated
  private ByteArray(byte[] paramArrayOfByte, int paramInt)
  {
    this.list = paramArrayOfByte;
    this.length = paramInt;
  }

  public ByteArray(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    if (paramInt2 >= 0);
    for (boolean bool = true; ; bool = false)
    {
      X.assertTrue(bool);
      this.list = new byte[paramInt2];
      this.length = paramInt2;
      System.arraycopy(paramArrayOfByte, paramInt1, this.list, 0, paramInt2);
      return;
    }
  }

  public void add(byte paramByte)
  {
    if (this.length >= this.list.length)
      ensureCapacity(1 + this.length);
    byte[] arrayOfByte = this.list;
    int i = this.length;
    this.length = (i + 1);
    arrayOfByte[i] = paramByte;
  }

  public void add(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    if (paramInt2 + this.length > this.list.length)
      ensureCapacity(paramInt2 + this.length);
    System.arraycopy(paramArrayOfByte, paramInt1, this.list, this.length, paramInt2);
    this.length = (paramInt2 + this.length);
  }

  public final void addUTF(String paramString)
  {
    int i = this.length;
    byte[] arrayOfByte = paramString.getBytes(Charsets.UTF_8);
    resize(this.length + arrayOfByte.length);
    for (int j = 0; j < arrayOfByte.length; j++)
      this.list[(i + j)] = arrayOfByte[j];
  }

  public void clear()
  {
    this.length = 0;
  }

  public void copy(byte[] paramArrayOfByte, int paramInt1, int paramInt2, int paramInt3)
  {
    boolean bool1 = true;
    boolean bool2;
    boolean bool3;
    label25: boolean bool4;
    label38: boolean bool5;
    if (paramInt2 >= 0)
    {
      bool2 = bool1;
      X.assertTrue(bool2);
      if (paramInt3 < 0)
        break label95;
      bool3 = bool1;
      X.assertTrue(bool3);
      if (paramInt1 < 0)
        break label101;
      bool4 = bool1;
      X.assertTrue(bool4);
      if (paramInt2 + paramInt1 > this.length)
        break label107;
      bool5 = bool1;
      label57: X.assertTrue(bool5);
      if (paramInt3 + paramInt1 > paramArrayOfByte.length)
        break label113;
    }
    while (true)
    {
      X.assertTrue(bool1);
      System.arraycopy(this.list, paramInt2, paramArrayOfByte, paramInt3, paramInt1);
      return;
      bool2 = false;
      break;
      label95: bool3 = false;
      break label25;
      label101: bool4 = false;
      break label38;
      label107: bool5 = false;
      break label57;
      label113: bool1 = false;
    }
  }

  public void ensureCapacity(int paramInt)
  {
    if (this.list.length < paramInt)
    {
      int i = 2 * this.list.length;
      if (i < paramInt)
        i = paramInt;
      byte[] arrayOfByte = new byte[i];
      System.arraycopy(this.list, 0, arrayOfByte, 0, this.length);
      this.list = arrayOfByte;
    }
  }

  public byte[] rep()
  {
    return this.list;
  }

  public void resize(int paramInt)
  {
    if (paramInt >= 0);
    for (boolean bool = true; ; bool = false)
    {
      X.assertTrue(bool);
      ensureCapacity(paramInt);
      this.length = paramInt;
      return;
    }
  }

  public int size()
  {
    return this.length;
  }

  public byte[] toArray()
  {
    byte[] arrayOfByte = new byte[this.length];
    System.arraycopy(this.list, 0, arrayOfByte, 0, this.length);
    return arrayOfByte;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.common.base.genfiles.ByteArray
 * JD-Core Version:    0.6.2
 */