package com.google.common.base.genfiles;

import com.google.common.annotations.GoogleInternal;
import com.google.common.base.Preconditions;
import com.google.common.base.X;
import java.io.Serializable;

@GoogleInternal
public final class DoubleArray
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  private int length;
  private double[] list;

  @Deprecated
  public DoubleArray()
  {
    this.list = new double[4];
    this.length = 0;
  }

  @Deprecated
  public DoubleArray(int paramInt)
  {
    this.list = new double[paramInt];
    this.length = 0;
  }

  @Deprecated
  private DoubleArray(double[] paramArrayOfDouble, int paramInt)
  {
    this.list = paramArrayOfDouble;
    this.length = paramInt;
  }

  public DoubleArray(double[] paramArrayOfDouble, int paramInt1, int paramInt2)
  {
    if (paramInt2 >= 0);
    for (boolean bool = true; ; bool = false)
    {
      X.assertTrue(bool);
      this.list = new double[paramInt2];
      this.length = paramInt2;
      System.arraycopy(paramArrayOfDouble, paramInt1, this.list, 0, paramInt2);
      return;
    }
  }

  @Deprecated
  public static DoubleArray newInstance(double[] paramArrayOfDouble)
  {
    Preconditions.checkNotNull(paramArrayOfDouble);
    return new DoubleArray(paramArrayOfDouble, paramArrayOfDouble.length);
  }

  public void add(double paramDouble)
  {
    if (this.length >= this.list.length)
      ensureCapacity(1 + this.length);
    double[] arrayOfDouble = this.list;
    int i = this.length;
    this.length = (i + 1);
    arrayOfDouble[i] = paramDouble;
  }

  public void add(double[] paramArrayOfDouble, int paramInt1, int paramInt2)
  {
    if (paramInt2 + this.length > this.list.length)
      ensureCapacity(paramInt2 + this.length);
    System.arraycopy(paramArrayOfDouble, paramInt1, this.list, this.length, paramInt2);
    this.length = (paramInt2 + this.length);
  }

  public void addArray(DoubleArray paramDoubleArray)
  {
    add(paramDoubleArray.rep(), 0, paramDoubleArray.size());
  }

  public void clear()
  {
    this.length = 0;
  }

  public void copy(double[] paramArrayOfDouble, int paramInt1, int paramInt2, int paramInt3)
  {
    boolean bool1 = true;
    boolean bool2;
    boolean bool3;
    label25: boolean bool4;
    label38: boolean bool5;
    if (paramInt2 >= 0)
    {
      bool2 = bool1;
      X.assertTrue(bool2);
      if (paramInt3 < 0)
        break label95;
      bool3 = bool1;
      X.assertTrue(bool3);
      if (paramInt1 < 0)
        break label101;
      bool4 = bool1;
      X.assertTrue(bool4);
      if (paramInt2 + paramInt1 > this.length)
        break label107;
      bool5 = bool1;
      label57: X.assertTrue(bool5);
      if (paramInt3 + paramInt1 > paramArrayOfDouble.length)
        break label113;
    }
    while (true)
    {
      X.assertTrue(bool1);
      System.arraycopy(this.list, paramInt2, paramArrayOfDouble, paramInt3, paramInt1);
      return;
      bool2 = false;
      break;
      label95: bool3 = false;
      break label25;
      label101: bool4 = false;
      break label38;
      label107: bool5 = false;
      break label57;
      label113: bool1 = false;
    }
  }

  public void ensureCapacity(int paramInt)
  {
    if (this.list.length < paramInt)
    {
      int i = 2 * this.list.length;
      if (i < paramInt)
        i = paramInt;
      double[] arrayOfDouble = new double[i];
      System.arraycopy(this.list, 0, arrayOfDouble, 0, this.length);
      this.list = arrayOfDouble;
    }
  }

  public double get(int paramInt)
  {
    if ((paramInt >= 0) && (paramInt < this.length));
    for (boolean bool = true; ; bool = false)
    {
      X.assertTrue(bool);
      return this.list[paramInt];
    }
  }

  public double[] rep()
  {
    return this.list;
  }

  public void set(int paramInt, double paramDouble)
  {
    if ((paramInt >= 0) && (paramInt < this.length));
    for (boolean bool = true; ; bool = false)
    {
      X.assertTrue(bool);
      this.list[paramInt] = paramDouble;
      return;
    }
  }

  public int size()
  {
    return this.length;
  }

  public double[] subArray(int paramInt1, int paramInt2)
  {
    boolean bool1 = true;
    boolean bool2;
    if (paramInt1 >= 0)
    {
      bool2 = bool1;
      X.assertTrue(bool2);
      if (paramInt1 + paramInt2 > this.length)
        break label54;
    }
    while (true)
    {
      X.assertTrue(bool1);
      double[] arrayOfDouble = new double[paramInt2];
      System.arraycopy(this.list, paramInt1, arrayOfDouble, 0, paramInt2);
      return arrayOfDouble;
      bool2 = false;
      break;
      label54: bool1 = false;
    }
  }

  public double[] toArray()
  {
    double[] arrayOfDouble = new double[this.length];
    System.arraycopy(this.list, 0, arrayOfDouble, 0, this.length);
    return arrayOfDouble;
  }

  public void trimToSize()
  {
    if (this.list.length != this.length)
      this.list = toArray();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.common.base.genfiles.DoubleArray
 * JD-Core Version:    0.6.2
 */