package com.google.common.base.genfiles;

import com.google.common.annotations.GoogleInternal;
import com.google.common.base.Preconditions;
import com.google.common.base.X;
import java.io.Serializable;

@GoogleInternal
public final class FloatArray
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  private int length;
  private float[] list;

  @Deprecated
  public FloatArray()
  {
    this.list = new float[4];
    this.length = 0;
  }

  @Deprecated
  public FloatArray(int paramInt)
  {
    this.list = new float[paramInt];
    this.length = 0;
  }

  @Deprecated
  private FloatArray(float[] paramArrayOfFloat, int paramInt)
  {
    this.list = paramArrayOfFloat;
    this.length = paramInt;
  }

  public FloatArray(float[] paramArrayOfFloat, int paramInt1, int paramInt2)
  {
    if (paramInt2 >= 0);
    for (boolean bool = true; ; bool = false)
    {
      X.assertTrue(bool);
      this.list = new float[paramInt2];
      this.length = paramInt2;
      System.arraycopy(paramArrayOfFloat, paramInt1, this.list, 0, paramInt2);
      return;
    }
  }

  @Deprecated
  public static FloatArray newInstance(float[] paramArrayOfFloat)
  {
    Preconditions.checkNotNull(paramArrayOfFloat);
    return new FloatArray(paramArrayOfFloat, paramArrayOfFloat.length);
  }

  public void add(float paramFloat)
  {
    if (this.length >= this.list.length)
      ensureCapacity(1 + this.length);
    float[] arrayOfFloat = this.list;
    int i = this.length;
    this.length = (i + 1);
    arrayOfFloat[i] = paramFloat;
  }

  public void add(float[] paramArrayOfFloat, int paramInt1, int paramInt2)
  {
    if (paramInt2 + this.length > this.list.length)
      ensureCapacity(paramInt2 + this.length);
    System.arraycopy(paramArrayOfFloat, paramInt1, this.list, this.length, paramInt2);
    this.length = (paramInt2 + this.length);
  }

  public void addArray(FloatArray paramFloatArray)
  {
    add(paramFloatArray.rep(), 0, paramFloatArray.size());
  }

  public void clear()
  {
    this.length = 0;
  }

  public void ensureCapacity(int paramInt)
  {
    if (this.list.length < paramInt)
    {
      int i = 2 * this.list.length;
      if (i < paramInt)
        i = paramInt;
      float[] arrayOfFloat = new float[i];
      System.arraycopy(this.list, 0, arrayOfFloat, 0, this.length);
      this.list = arrayOfFloat;
    }
  }

  public float get(int paramInt)
  {
    if ((paramInt >= 0) && (paramInt < this.length));
    for (boolean bool = true; ; bool = false)
    {
      X.assertTrue(bool);
      return this.list[paramInt];
    }
  }

  public float[] rep()
  {
    return this.list;
  }

  public int size()
  {
    return this.length;
  }

  public float[] subArray(int paramInt1, int paramInt2)
  {
    boolean bool1 = true;
    boolean bool2;
    if (paramInt1 >= 0)
    {
      bool2 = bool1;
      X.assertTrue(bool2);
      if (paramInt1 + paramInt2 > this.length)
        break label54;
    }
    while (true)
    {
      X.assertTrue(bool1);
      float[] arrayOfFloat = new float[paramInt2];
      System.arraycopy(this.list, paramInt1, arrayOfFloat, 0, paramInt2);
      return arrayOfFloat;
      bool2 = false;
      break;
      label54: bool1 = false;
    }
  }

  public float[] toArray()
  {
    float[] arrayOfFloat = new float[this.length];
    System.arraycopy(this.list, 0, arrayOfFloat, 0, this.length);
    return arrayOfFloat;
  }

  public void trimToSize()
  {
    if (this.list.length != this.length)
      this.list = toArray();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.common.base.genfiles.FloatArray
 * JD-Core Version:    0.6.2
 */