package com.google.common.base.genfiles;

import com.google.common.annotations.GoogleInternal;
import com.google.common.base.Preconditions;
import com.google.common.base.X;
import java.io.Serializable;

@GoogleInternal
public final class IntArray
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  private int length;
  private int[] list;

  @Deprecated
  public IntArray()
  {
    this.list = new int[4];
    this.length = 0;
  }

  @Deprecated
  public IntArray(int paramInt)
  {
    this.list = new int[paramInt];
    this.length = 0;
  }

  @Deprecated
  private IntArray(int[] paramArrayOfInt, int paramInt)
  {
    this.list = paramArrayOfInt;
    this.length = paramInt;
  }

  public IntArray(int[] paramArrayOfInt, int paramInt1, int paramInt2)
  {
    if (paramInt2 >= 0);
    for (boolean bool = true; ; bool = false)
    {
      X.assertTrue(bool);
      this.list = new int[paramInt2];
      this.length = paramInt2;
      System.arraycopy(paramArrayOfInt, paramInt1, this.list, 0, paramInt2);
      return;
    }
  }

  @Deprecated
  public static IntArray newInstance(int[] paramArrayOfInt)
  {
    Preconditions.checkNotNull(paramArrayOfInt);
    return new IntArray(paramArrayOfInt, paramArrayOfInt.length);
  }

  public void add(int paramInt)
  {
    if (this.length >= this.list.length)
      ensureCapacity(1 + this.length);
    int[] arrayOfInt = this.list;
    int i = this.length;
    this.length = (i + 1);
    arrayOfInt[i] = paramInt;
  }

  public void add(int[] paramArrayOfInt, int paramInt1, int paramInt2)
  {
    if (paramInt2 + this.length > this.list.length)
      ensureCapacity(paramInt2 + this.length);
    System.arraycopy(paramArrayOfInt, paramInt1, this.list, this.length, paramInt2);
    this.length = (paramInt2 + this.length);
  }

  public void addArray(IntArray paramIntArray)
  {
    add(paramIntArray.rep(), 0, paramIntArray.size());
  }

  public void clear()
  {
    this.length = 0;
  }

  public void ensureCapacity(int paramInt)
  {
    if (this.list.length < paramInt)
    {
      int i = 2 * this.list.length;
      if (i < paramInt)
        i = paramInt;
      int[] arrayOfInt = new int[i];
      System.arraycopy(this.list, 0, arrayOfInt, 0, this.length);
      this.list = arrayOfInt;
    }
  }

  public int get(int paramInt)
  {
    if ((paramInt >= 0) && (paramInt < this.length));
    for (boolean bool = true; ; bool = false)
    {
      X.assertTrue(bool);
      return this.list[paramInt];
    }
  }

  public int indexOf(int paramInt)
  {
    for (int i = 0; i < this.length; i++)
      if (this.list[i] == paramInt)
        return i;
    return -1;
  }

  public void removeFast(int paramInt)
  {
    if ((paramInt >= 0) && (paramInt < this.length));
    for (boolean bool = true; ; bool = false)
    {
      X.assertTrue(bool);
      this.list[paramInt] = this.list[(-1 + this.length)];
      removeLast();
      return;
    }
  }

  public void removeLast()
  {
    if (this.length > 0);
    for (boolean bool = true; ; bool = false)
    {
      X.assertTrue(bool);
      this.length = (-1 + this.length);
      return;
    }
  }

  public int[] rep()
  {
    return this.list;
  }

  public void set(int paramInt1, int paramInt2)
  {
    if ((paramInt1 >= 0) && (paramInt1 < this.length));
    for (boolean bool = true; ; bool = false)
    {
      X.assertTrue(bool);
      this.list[paramInt1] = paramInt2;
      return;
    }
  }

  public int size()
  {
    return this.length;
  }

  public int[] subArray(int paramInt1, int paramInt2)
  {
    boolean bool1 = true;
    boolean bool2;
    if (paramInt1 >= 0)
    {
      bool2 = bool1;
      X.assertTrue(bool2);
      if (paramInt1 + paramInt2 > this.length)
        break label54;
    }
    while (true)
    {
      X.assertTrue(bool1);
      int[] arrayOfInt = new int[paramInt2];
      System.arraycopy(this.list, paramInt1, arrayOfInt, 0, paramInt2);
      return arrayOfInt;
      bool2 = false;
      break;
      label54: bool1 = false;
    }
  }

  public int[] toArray()
  {
    int[] arrayOfInt = new int[this.length];
    System.arraycopy(this.list, 0, arrayOfInt, 0, this.length);
    return arrayOfInt;
  }

  public void trimToSize()
  {
    if (this.list.length != this.length)
      this.list = toArray();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.common.base.genfiles.IntArray
 * JD-Core Version:    0.6.2
 */