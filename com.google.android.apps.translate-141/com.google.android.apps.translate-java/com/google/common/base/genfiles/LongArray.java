package com.google.common.base.genfiles;

import com.google.common.annotations.GoogleInternal;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Preconditions;
import com.google.common.base.X;
import java.io.Serializable;

@GoogleInternal
public final class LongArray
  implements Serializable
{
  private static final long serialVersionUID = 1L;
  private int length;
  private long[] list;

  @Deprecated
  public LongArray()
  {
    this.list = new long[4];
    this.length = 0;
  }

  @Deprecated
  public LongArray(int paramInt)
  {
    this.list = new long[paramInt];
    this.length = 0;
  }

  @Deprecated
  private LongArray(long[] paramArrayOfLong, int paramInt)
  {
    this.list = paramArrayOfLong;
    this.length = paramInt;
  }

  public LongArray(long[] paramArrayOfLong, int paramInt1, int paramInt2)
  {
    if (paramInt2 >= 0);
    for (boolean bool = true; ; bool = false)
    {
      X.assertTrue(bool);
      this.list = new long[paramInt2];
      this.length = paramInt2;
      System.arraycopy(paramArrayOfLong, paramInt1, this.list, 0, paramInt2);
      return;
    }
  }

  private void ensureCapacity(int paramInt)
  {
    if (this.list.length < paramInt)
    {
      int i = 2 * this.list.length;
      if (i < paramInt)
        i = paramInt;
      long[] arrayOfLong = new long[i];
      System.arraycopy(this.list, 0, arrayOfLong, 0, this.length);
      this.list = arrayOfLong;
    }
  }

  @Deprecated
  public static LongArray newInstance(long[] paramArrayOfLong)
  {
    Preconditions.checkNotNull(paramArrayOfLong);
    return new LongArray(paramArrayOfLong, paramArrayOfLong.length);
  }

  @Deprecated
  @VisibleForTesting
  static LongArray newInstance(long[] paramArrayOfLong, int paramInt)
  {
    Preconditions.checkNotNull(paramArrayOfLong);
    if ((paramInt >= 0) && (paramInt <= paramArrayOfLong.length));
    for (boolean bool = true; ; bool = false)
    {
      Preconditions.checkArgument(bool);
      return new LongArray(paramArrayOfLong, paramInt);
    }
  }

  public void add(long paramLong)
  {
    if (this.length >= this.list.length)
      ensureCapacity(1 + this.length);
    long[] arrayOfLong = this.list;
    int i = this.length;
    this.length = (i + 1);
    arrayOfLong[i] = paramLong;
  }

  @VisibleForTesting
  void add(long[] paramArrayOfLong, int paramInt1, int paramInt2)
  {
    if (paramInt2 + this.length > this.list.length)
      ensureCapacity(paramInt2 + this.length);
    System.arraycopy(paramArrayOfLong, paramInt1, this.list, this.length, paramInt2);
    this.length = (paramInt2 + this.length);
  }

  public void addArray(LongArray paramLongArray)
  {
    add(paramLongArray.rep(), 0, paramLongArray.size());
  }

  public void clear()
  {
    this.length = 0;
  }

  @VisibleForTesting
  void copy(long[] paramArrayOfLong, int paramInt1, int paramInt2, int paramInt3)
  {
    boolean bool1 = true;
    boolean bool2;
    boolean bool3;
    label25: boolean bool4;
    label38: boolean bool5;
    if (paramInt2 >= 0)
    {
      bool2 = bool1;
      X.assertTrue(bool2);
      if (paramInt3 < 0)
        break label95;
      bool3 = bool1;
      X.assertTrue(bool3);
      if (paramInt1 < 0)
        break label101;
      bool4 = bool1;
      X.assertTrue(bool4);
      if (paramInt2 + paramInt1 > this.length)
        break label107;
      bool5 = bool1;
      label57: X.assertTrue(bool5);
      if (paramInt3 + paramInt1 > paramArrayOfLong.length)
        break label113;
    }
    while (true)
    {
      X.assertTrue(bool1);
      System.arraycopy(this.list, paramInt2, paramArrayOfLong, paramInt3, paramInt1);
      return;
      bool2 = false;
      break;
      label95: bool3 = false;
      break label25;
      label101: bool4 = false;
      break label38;
      label107: bool5 = false;
      break label57;
      label113: bool1 = false;
    }
  }

  public long get(int paramInt)
  {
    if ((paramInt >= 0) && (paramInt < this.length));
    for (boolean bool = true; ; bool = false)
    {
      X.assertTrue(bool);
      return this.list[paramInt];
    }
  }

  public int indexOf(long paramLong)
  {
    for (int i = 0; i < this.length; i++)
      if (this.list[i] == paramLong)
        return i;
    return -1;
  }

  public void removeFast(int paramInt)
  {
    if ((paramInt >= 0) && (paramInt < this.length));
    for (boolean bool = true; ; bool = false)
    {
      X.assertTrue(bool);
      this.list[paramInt] = this.list[(-1 + this.length)];
      removeLast();
      return;
    }
  }

  @VisibleForTesting
  void removeLast()
  {
    if (this.length > 0);
    for (boolean bool = true; ; bool = false)
    {
      X.assertTrue(bool);
      this.length = (-1 + this.length);
      return;
    }
  }

  public long[] rep()
  {
    return this.list;
  }

  @VisibleForTesting
  void resize(int paramInt)
  {
    if (paramInt >= 0);
    for (boolean bool = true; ; bool = false)
    {
      X.assertTrue(bool);
      ensureCapacity(paramInt);
      this.length = paramInt;
      return;
    }
  }

  public void set(int paramInt, long paramLong)
  {
    if ((paramInt >= 0) && (paramInt < this.length));
    for (boolean bool = true; ; bool = false)
    {
      X.assertTrue(bool);
      this.list[paramInt] = paramLong;
      return;
    }
  }

  public int size()
  {
    return this.length;
  }

  @VisibleForTesting
  long[] subArray(int paramInt1, int paramInt2)
  {
    boolean bool1 = true;
    boolean bool2;
    if (paramInt1 >= 0)
    {
      bool2 = bool1;
      X.assertTrue(bool2);
      if (paramInt1 + paramInt2 > this.length)
        break label54;
    }
    while (true)
    {
      X.assertTrue(bool1);
      long[] arrayOfLong = new long[paramInt2];
      System.arraycopy(this.list, paramInt1, arrayOfLong, 0, paramInt2);
      return arrayOfLong;
      bool2 = false;
      break;
      label54: bool1 = false;
    }
  }

  @VisibleForTesting
  void swap(LongArray paramLongArray)
  {
    int i = this.length;
    this.length = paramLongArray.length;
    paramLongArray.length = i;
    long[] arrayOfLong = this.list;
    this.list = paramLongArray.list;
    paramLongArray.list = arrayOfLong;
  }

  public long[] toArray()
  {
    long[] arrayOfLong = new long[this.length];
    System.arraycopy(this.list, 0, arrayOfLong, 0, this.length);
    return arrayOfLong;
  }

  public void trimToSize()
  {
    if (this.list.length != this.length)
      this.list = toArray();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.common.base.genfiles.LongArray
 * JD-Core Version:    0.6.2
 */