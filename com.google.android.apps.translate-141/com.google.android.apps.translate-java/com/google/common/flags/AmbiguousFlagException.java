package com.google.common.flags;

import java.util.Set;

public class AmbiguousFlagException extends FlagException
{
  private static final long serialVersionUID = 89034589803L;

  AmbiguousFlagException(String paramString, Set<FlagDescription> paramSet)
  {
    super("Ambiguous flag '" + paramString + "' matches " + paramSet);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.common.flags.AmbiguousFlagException
 * JD-Core Version:    0.6.2
 */