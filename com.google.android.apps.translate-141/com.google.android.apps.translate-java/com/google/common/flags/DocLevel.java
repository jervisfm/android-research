package com.google.common.flags;

public enum DocLevel
{
  static
  {
    DocLevel[] arrayOfDocLevel = new DocLevel[2];
    arrayOfDocLevel[0] = PUBLIC;
    arrayOfDocLevel[1] = SECRET;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.common.flags.DocLevel
 * JD-Core Version:    0.6.2
 */