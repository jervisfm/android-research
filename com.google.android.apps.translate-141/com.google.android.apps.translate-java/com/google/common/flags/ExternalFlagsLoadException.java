package com.google.common.flags;

public class ExternalFlagsLoadException extends FlagException
{
  private static final long serialVersionUID = 93487431290533L;

  ExternalFlagsLoadException(String paramString)
  {
    super("failed to open/read external flags specified in " + paramString);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.common.flags.ExternalFlagsLoadException
 * JD-Core Version:    0.6.2
 */