package com.google.common.flags;

import com.google.common.annotations.VisibleForTesting;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class Flag<T>
{
  private static final Pattern FALSE_PATTERN = Pattern.compile("^(false|f|no|n|0)$", 2);

  @VisibleForTesting
  static final String MUTABLE_SYSTEM_PROPERTY = "google3.flags.mutablecollections";
  private static final Pattern TRUE_PATTERN = Pattern.compile("^(true|t|yes|y|1)$", 2);
  boolean accessed = false;
  final T defaultValue;
  boolean setFromString = false;
  volatile T value;

  protected Flag(T paramT)
  {
    this.defaultValue = paramT;
    this.value = paramT;
  }

  private void checkWriteState()
    throws IllegalFlagStateException
  {
    if ((this.accessed) && (!Boolean.getBoolean("com.google.common.flags.disableStateChecking")))
      throw new IllegalFlagStateException();
  }

  private static Class<?> classForName(String paramString)
    throws InvalidFlagValueException
  {
    try
    {
      Class localClass = Class.forName(paramString.toString());
      return localClass;
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      InvalidFlagValueException localInvalidFlagValueException = new InvalidFlagValueException("You must provide a fully qualified class name");
      localInvalidFlagValueException.initCause(localClassNotFoundException);
      throw localInvalidFlagValueException;
    }
  }

  private static boolean collectionsAreMutable()
  {
    return true;
  }

  public static Flag<List<Double>> doubleList(double[] paramArrayOfDouble)
  {
    ArrayList localArrayList = new ArrayList();
    int i = paramArrayOfDouble.length;
    for (int j = 0; j < i; j++)
      localArrayList.add(Double.valueOf(paramArrayOfDouble[j]));
    return new Flag(maybeUnmodifiable(localArrayList))
    {
      public String parsableStringValue(List<Double> paramAnonymousList)
      {
        return Flag.joinToStrings(paramAnonymousList);
      }

      protected List<Double> parse(String paramAnonymousString)
        throws InvalidFlagValueException
      {
        ArrayList localArrayList = new ArrayList();
        String[] arrayOfString = paramAnonymousString.split(",");
        int i = arrayOfString.length;
        int j = 0;
        while (j < i)
        {
          String str = arrayOfString[j];
          try
          {
            localArrayList.add(Flag.parseDoubleOrMax(str.trim()));
            j++;
          }
          catch (NumberFormatException localNumberFormatException)
          {
            while (paramAnonymousString.trim().equals(""));
            throw new InvalidFlagValueException("Invalid double syntax " + str);
          }
        }
        return Flag.maybeUnmodifiable(localArrayList);
      }
    };
  }

  public static <T extends Enum<T>> Flag<List<T>> enumList(final Class<T> paramClass, final boolean paramBoolean, T[] paramArrayOfT)
  {
    if (paramClass == null)
      throw new NullPointerException("enumType must not be null");
    ArrayList localArrayList = new ArrayList();
    int i = paramArrayOfT.length;
    for (int j = 0; j < i; j++)
    {
      T ? = paramArrayOfT[j];
      if (? == null)
        throw new NullPointerException("Elements of defaultValues must not be null");
      localArrayList.add(?);
    }
    return new Flag(maybeUnmodifiable(localArrayList))
    {
      public String parsableStringValue(List<T> paramAnonymousList)
      {
        ArrayList localArrayList = new ArrayList();
        Iterator localIterator = paramAnonymousList.iterator();
        while (localIterator.hasNext())
          localArrayList.add(((Enum)localIterator.next()).name());
        return Flag.joinToStrings(localArrayList);
      }

      protected List<T> parse(String paramAnonymousString)
        throws InvalidFlagValueException
      {
        ArrayList localArrayList = new ArrayList();
        if (!paramAnonymousString.trim().equals(""))
        {
          if (paramBoolean)
            paramAnonymousString = paramAnonymousString.toUpperCase();
          String[] arrayOfString = paramAnonymousString.split(",");
          int i = arrayOfString.length;
          int j = 0;
          while (j < i)
          {
            String str = arrayOfString[j];
            try
            {
              localArrayList.add(Enum.valueOf(paramClass, str.trim()));
              j++;
            }
            catch (IllegalArgumentException localIllegalArgumentException)
            {
              throw new InvalidFlagValueException("Not a valid enum " + paramClass.getName() + " constant: " + str.trim());
            }
          }
        }
        return Flag.maybeUnmodifiable(localArrayList);
      }
    };
  }

  public static <T extends Enum<T>> Flag<List<T>> enumList(Class<T> paramClass, T[] paramArrayOfT)
  {
    return enumList(paramClass, false, paramArrayOfT);
  }

  public static Flag<List<Integer>> integerList(int[] paramArrayOfInt)
  {
    ArrayList localArrayList = new ArrayList();
    int i = paramArrayOfInt.length;
    for (int j = 0; j < i; j++)
      localArrayList.add(Integer.valueOf(paramArrayOfInt[j]));
    return new Flag(maybeUnmodifiable(localArrayList))
    {
      public String parsableStringValue(List<Integer> paramAnonymousList)
      {
        return Flag.joinToStrings(paramAnonymousList);
      }

      protected List<Integer> parse(String paramAnonymousString)
        throws InvalidFlagValueException
      {
        ArrayList localArrayList = new ArrayList();
        String[] arrayOfString = paramAnonymousString.split(",");
        int i = arrayOfString.length;
        int j = 0;
        while (j < i)
        {
          String str = arrayOfString[j];
          try
          {
            localArrayList.add(Flag.parseIntegerOrMax(str.trim()));
            j++;
          }
          catch (NumberFormatException localNumberFormatException)
          {
            while (paramAnonymousString.trim().equals(""));
            throw new InvalidFlagValueException("Invalid integer syntax " + str);
          }
        }
        return Flag.maybeUnmodifiable(localArrayList);
      }
    };
  }

  private static String joinToStrings(Iterable<?> paramIterable)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    Iterator localIterator = paramIterable.iterator();
    while (localIterator.hasNext())
    {
      Object localObject = localIterator.next();
      if (localStringBuilder.length() != 0)
        localStringBuilder.append(',');
      localStringBuilder.append(localObject);
    }
    return localStringBuilder.toString();
  }

  public static Flag<List<Long>> longList(long[] paramArrayOfLong)
  {
    ArrayList localArrayList = new ArrayList();
    int i = paramArrayOfLong.length;
    for (int j = 0; j < i; j++)
      localArrayList.add(Long.valueOf(paramArrayOfLong[j]));
    return new Flag(maybeUnmodifiable(localArrayList))
    {
      public String parsableStringValue(List<Long> paramAnonymousList)
      {
        return Flag.joinToStrings(paramAnonymousList);
      }

      protected List<Long> parse(String paramAnonymousString)
        throws InvalidFlagValueException
      {
        ArrayList localArrayList = new ArrayList();
        String[] arrayOfString = paramAnonymousString.split(",");
        int i = arrayOfString.length;
        int j = 0;
        while (j < i)
        {
          String str = arrayOfString[j];
          try
          {
            localArrayList.add(Flag.parseLongOrMax(str.trim()));
            j++;
          }
          catch (NumberFormatException localNumberFormatException)
          {
            while (paramAnonymousString.trim().equals(""));
            throw new InvalidFlagValueException("Invalid long syntax " + str);
          }
        }
        return Flag.maybeUnmodifiable(localArrayList);
      }
    };
  }

  private static <T> List<T> maybeUnmodifiable(List<T> paramList)
  {
    if (collectionsAreMutable())
      return paramList;
    return Collections.unmodifiableList(paramList);
  }

  private static <T> Set<T> maybeUnmodifiable(Set<T> paramSet)
  {
    if (collectionsAreMutable())
      return paramSet;
    return Collections.unmodifiableSet(paramSet);
  }

  public static Flag<Integer> nonnegativeValue(int paramInt)
  {
    return new Flag(Integer.valueOf(paramInt))
    {
      protected Integer parse(String paramAnonymousString)
        throws InvalidFlagValueException
      {
        Integer localInteger;
        try
        {
          localInteger = Flag.parseIntegerOrMax(paramAnonymousString);
          if (localInteger.intValue() < 0)
            throw new InvalidFlagValueException("Must not be negative");
        }
        catch (NumberFormatException localNumberFormatException)
        {
          InvalidFlagValueException localInvalidFlagValueException = new InvalidFlagValueException("Invalid integer syntax");
          localInvalidFlagValueException.initCause(localNumberFormatException);
          throw localInvalidFlagValueException;
        }
        return localInteger;
      }
    };
  }

  public static Flag<Long> nonnegativeValue(long paramLong)
  {
    return new Flag(Long.valueOf(paramLong))
    {
      protected Long parse(String paramAnonymousString)
        throws InvalidFlagValueException
      {
        Long localLong;
        try
        {
          localLong = Flag.parseLongOrMax(paramAnonymousString);
          if (localLong.longValue() < 0L)
            throw new InvalidFlagValueException("Must not be negative");
        }
        catch (NumberFormatException localNumberFormatException)
        {
          InvalidFlagValueException localInvalidFlagValueException = new InvalidFlagValueException("Invalid integer syntax");
          localInvalidFlagValueException.initCause(localNumberFormatException);
          throw localInvalidFlagValueException;
        }
        return localLong;
      }
    };
  }

  private static Double parseDoubleOrMax(String paramString)
  {
    if ("MAX".equals(paramString));
    for (double d = (1.0D / 0.0D); ; d = Double.parseDouble(paramString))
      return Double.valueOf(d);
  }

  private static Float parseFloatOrMax(String paramString)
  {
    if ("MAX".equals(paramString));
    for (float f = (1.0F / 1.0F); ; f = Float.parseFloat(paramString))
      return Float.valueOf(f);
  }

  private static Integer parseIntegerOrMax(String paramString)
  {
    if ("MAX".equals(paramString));
    for (int i = 2147483647; ; i = Integer.decode(paramString).intValue())
      return Integer.valueOf(i);
  }

  private static Long parseLongOrMax(String paramString)
  {
    if ("MAX".equals(paramString));
    for (long l = 9223372036854775807L; ; l = Long.decode(paramString).longValue())
      return Long.valueOf(l);
  }

  public static Flag<Integer> positiveValue(int paramInt)
  {
    return new Flag(Integer.valueOf(paramInt))
    {
      protected Integer parse(String paramAnonymousString)
        throws InvalidFlagValueException
      {
        Integer localInteger;
        try
        {
          localInteger = Flag.parseIntegerOrMax(paramAnonymousString);
          if (localInteger.intValue() <= 0)
            throw new InvalidFlagValueException("Must be positive");
        }
        catch (NumberFormatException localNumberFormatException)
        {
          InvalidFlagValueException localInvalidFlagValueException = new InvalidFlagValueException("Invalid integer syntax");
          localInvalidFlagValueException.initCause(localNumberFormatException);
          throw localInvalidFlagValueException;
        }
        return localInteger;
      }
    };
  }

  public static Flag<Long> positiveValue(long paramLong)
  {
    return new Flag(Long.valueOf(paramLong))
    {
      protected Long parse(String paramAnonymousString)
        throws InvalidFlagValueException
      {
        Long localLong;
        try
        {
          localLong = Flag.parseLongOrMax(paramAnonymousString);
          if (localLong.longValue() <= 0L)
            throw new InvalidFlagValueException("Must be positive");
        }
        catch (NumberFormatException localNumberFormatException)
        {
          InvalidFlagValueException localInvalidFlagValueException = new InvalidFlagValueException("Invalid integer syntax");
          localInvalidFlagValueException.initCause(localNumberFormatException);
          throw localInvalidFlagValueException;
        }
        return localLong;
      }
    };
  }

  private static List<String> splitCsvString(String paramString)
  {
    ArrayList localArrayList = new ArrayList();
    if (!paramString.trim().equals(""))
    {
      String[] arrayOfString = paramString.split(",");
      int i = arrayOfString.length;
      for (int j = 0; j < i; j++)
        localArrayList.add(arrayOfString[j].trim());
    }
    return localArrayList;
  }

  public static Flag<List<String>> stringCollector(String[] paramArrayOfString)
  {
    final ArrayList localArrayList1 = new ArrayList(paramArrayOfString.length);
    int i = paramArrayOfString.length;
    for (int j = 0; j < i; j++)
      localArrayList1.add(paramArrayOfString[j]);
    final ArrayList localArrayList2 = new ArrayList(localArrayList1);
    return new Flag(maybeUnmodifiable(localArrayList2))
    {
      private boolean resultCleared = false;

      public String parsableStringValue(List<String> paramAnonymousList)
      {
        throw new UnsupportedOperationException("List can not be represented in a single String: " + paramAnonymousList);
      }

      protected List<String> parse(String paramAnonymousString)
      {
        if (!this.resultCleared)
        {
          this.resultCleared = true;
          localArrayList2.clear();
        }
        localArrayList2.add(paramAnonymousString);
        return Flag.maybeUnmodifiable(localArrayList2);
      }

      public void resetForTest()
      {
        super.resetForTest();
        localArrayList2.clear();
        localArrayList2.addAll(localArrayList1);
        this.resultCleared = false;
      }
    };
  }

  public static Flag<List<String>> stringList(String[] paramArrayOfString)
  {
    ArrayList localArrayList = new ArrayList();
    int i = paramArrayOfString.length;
    for (int j = 0; j < i; j++)
      localArrayList.add(paramArrayOfString[j].toString());
    return new Flag(maybeUnmodifiable(localArrayList))
    {
      public String parsableStringValue(List<String> paramAnonymousList)
      {
        return Flag.joinToStrings(paramAnonymousList);
      }

      protected List<String> parse(String paramAnonymousString)
      {
        return Flag.maybeUnmodifiable(Flag.access$1000(paramAnonymousString));
      }
    };
  }

  public static Flag<Map<String, String>> stringMap(Map<String, String> paramMap)
  {
    return new Flag(paramMap)
    {
      public String parsableStringValue(Map<String, String> paramAnonymousMap)
      {
        ArrayList localArrayList = new ArrayList();
        Iterator localIterator = paramAnonymousMap.entrySet().iterator();
        while (localIterator.hasNext())
        {
          Map.Entry localEntry = (Map.Entry)localIterator.next();
          localArrayList.add((String)localEntry.getKey() + "=" + (String)localEntry.getValue());
        }
        return Flag.joinToStrings(localArrayList);
      }

      protected Map<String, String> parse(String paramAnonymousString)
        throws InvalidFlagValueException
      {
        LinkedHashMap localLinkedHashMap = new LinkedHashMap();
        if (!paramAnonymousString.trim().equals(""))
          for (String str : paramAnonymousString.split(","))
          {
            int k = str.indexOf('=');
            if (k == -1)
              throw new InvalidFlagValueException("Invalid map entry syntax " + str);
            localLinkedHashMap.put(str.substring(0, k).trim(), str.substring(k + 1).trim());
          }
        return localLinkedHashMap;
      }
    };
  }

  public static Flag<Set<String>> stringSet(String[] paramArrayOfString)
  {
    LinkedHashSet localLinkedHashSet = new LinkedHashSet();
    int i = paramArrayOfString.length;
    for (int j = 0; j < i; j++)
      localLinkedHashSet.add(paramArrayOfString[j].toString());
    return new Flag(maybeUnmodifiable(localLinkedHashSet))
    {
      public String parsableStringValue(Set<String> paramAnonymousSet)
      {
        return Flag.joinToStrings(paramAnonymousSet);
      }

      protected Set<String> parse(String paramAnonymousString)
      {
        return Flag.maybeUnmodifiable(new LinkedHashSet(Flag.splitCsvString(paramAnonymousString)));
      }
    };
  }

  public static Flag<Double> value(double paramDouble)
  {
    return new Flag(Double.valueOf(paramDouble))
    {
      protected Double parse(String paramAnonymousString)
        throws InvalidFlagValueException
      {
        try
        {
          Double localDouble = Flag.parseDoubleOrMax(paramAnonymousString);
          return localDouble;
        }
        catch (NumberFormatException localNumberFormatException)
        {
        }
        throw new InvalidFlagValueException("Invalid double syntax");
      }
    };
  }

  public static Flag<Float> value(float paramFloat)
  {
    return new Flag(Float.valueOf(paramFloat))
    {
      protected Float parse(String paramAnonymousString)
        throws InvalidFlagValueException
      {
        try
        {
          Float localFloat = Flag.parseFloatOrMax(paramAnonymousString);
          return localFloat;
        }
        catch (NumberFormatException localNumberFormatException)
        {
        }
        throw new InvalidFlagValueException("Invalid float syntax");
      }
    };
  }

  public static Flag<Integer> value(int paramInt)
  {
    return new Flag(Integer.valueOf(paramInt))
    {
      protected Integer parse(String paramAnonymousString)
        throws InvalidFlagValueException
      {
        try
        {
          Integer localInteger = Flag.parseIntegerOrMax(paramAnonymousString);
          return localInteger;
        }
        catch (NumberFormatException localNumberFormatException)
        {
        }
        throw new InvalidFlagValueException("Invalid integer syntax");
      }
    };
  }

  public static Flag<Long> value(long paramLong)
  {
    return new Flag(Long.valueOf(paramLong))
    {
      protected Long parse(String paramAnonymousString)
        throws InvalidFlagValueException
      {
        try
        {
          Long localLong = Flag.parseLongOrMax(paramAnonymousString);
          return localLong;
        }
        catch (NumberFormatException localNumberFormatException)
        {
        }
        throw new InvalidFlagValueException("Invalid long syntax");
      }
    };
  }

  public static Flag<Class<?>> value(Class<?> paramClass)
  {
    return new Flag(paramClass)
    {
      public String parsableStringValue(Class<?> paramAnonymousClass)
      {
        return paramAnonymousClass.getName();
      }

      protected Class<?> parse(String paramAnonymousString)
        throws InvalidFlagValueException
      {
        return Flag.classForName(paramAnonymousString);
      }
    };
  }

  public static <T> Flag<Class<? extends T>> value(Class<? extends T> paramClass, final Class<T> paramClass1)
  {
    return new Flag(paramClass)
    {
      public String parsableStringValue(Class<? extends T> paramAnonymousClass)
      {
        return paramAnonymousClass.getName();
      }

      protected Class<? extends T> parse(String paramAnonymousString)
        throws InvalidFlagValueException
      {
        try
        {
          Class localClass = Flag.classForName(paramAnonymousString).asSubclass(paramClass1);
          return localClass;
        }
        catch (ClassCastException localClassCastException)
        {
        }
        throw new InvalidFlagValueException("Not a subtype of " + paramClass1);
      }
    };
  }

  public static <T extends Enum<T>> Flag<T> value(Class<T> paramClass, boolean paramBoolean)
  {
    return new EnumFlag(null, paramClass, paramBoolean, null);
  }

  public static <T extends Enum<T>> Flag<T> value(T paramT)
  {
    return value(paramT, false);
  }

  public static <T extends Enum<T>> Flag<T> value(T paramT, boolean paramBoolean)
  {
    return new EnumFlag(paramT, paramT.getDeclaringClass(), paramBoolean, null);
  }

  public static Flag<String> value(String paramString)
  {
    return new Flag(paramString)
    {
      protected String parse(String paramAnonymousString)
      {
        if (paramAnonymousString == null)
          throw new NullPointerException();
        return paramAnonymousString;
      }
    };
  }

  public static Flag<BigDecimal> value(BigDecimal paramBigDecimal)
  {
    return new Flag(paramBigDecimal)
    {
      protected BigDecimal parse(String paramAnonymousString)
        throws InvalidFlagValueException
      {
        try
        {
          BigDecimal localBigDecimal = new BigDecimal(paramAnonymousString);
          return localBigDecimal;
        }
        catch (NumberFormatException localNumberFormatException)
        {
        }
        throw new InvalidFlagValueException("Invalid BigDecimal syntax");
      }
    };
  }

  public static Flag<Date> value(Date paramDate, final DateFormat paramDateFormat)
  {
    if (paramDateFormat == null)
      throw new NullPointerException();
    return new Flag(paramDate)
    {
      public String parsableStringValue(Date paramAnonymousDate)
      {
        return paramDateFormat.format(paramAnonymousDate);
      }

      protected Date parse(String paramAnonymousString)
        throws InvalidFlagValueException
      {
        try
        {
          Date localDate = paramDateFormat.parse(paramAnonymousString);
          return localDate;
        }
        catch (ParseException localParseException)
        {
        }
        throw new InvalidFlagValueException("Invalid date syntax");
      }
    };
  }

  public static Flag<Boolean> value(boolean paramBoolean)
  {
    return new Flag(Boolean.valueOf(paramBoolean))
    {
      protected Boolean parse(String paramAnonymousString)
        throws InvalidFlagValueException
      {
        if (Flag.TRUE_PATTERN.matcher(paramAnonymousString).matches())
          return Boolean.valueOf(true);
        if (Flag.FALSE_PATTERN.matcher(paramAnonymousString).matches())
          return Boolean.valueOf(false);
        throw new InvalidFlagValueException("Invalid boolean syntax");
      }
    };
  }

  public final T get()
  {
    this.accessed = true;
    return this.value;
  }

  public final T getDefault()
  {
    return this.defaultValue;
  }

  public final String parsableStringValue()
  {
    Object localObject = get();
    if (localObject != null)
      return parsableStringValue(localObject);
    return null;
  }

  protected String parsableStringValue(T paramT)
  {
    return paramT.toString();
  }

  protected abstract T parse(String paramString)
    throws InvalidFlagValueException;

  public void resetForTest()
  {
    checkWriteState();
    this.accessed = false;
    this.setFromString = false;
    this.value = this.defaultValue;
  }

  public void setForTest(T paramT)
  {
    checkWriteState();
    this.value = paramT;
  }

  public final void setFromString(String paramString)
    throws InvalidFlagValueException, IllegalFlagStateException
  {
    checkWriteState();
    this.setFromString = true;
    if (paramString == null)
    {
      if (Boolean.getBoolean("com.google.common.flags.disableStateChecking"))
      {
        this.value = null;
        return;
      }
      throw new NullPointerException();
    }
    this.value = parse(paramString);
  }

  public final String toString()
  {
    return String.valueOf(get());
  }

  public boolean wasSetFromString()
  {
    return this.setFromString;
  }

  private static class EnumFlag<T extends Enum<T>> extends Flag<T>
  {
    private final boolean autoUpperCase;
    private final Class<T> enumType;

    private EnumFlag(T paramT, Class<T> paramClass, boolean paramBoolean)
    {
      super();
      this.enumType = paramClass;
      this.autoUpperCase = paramBoolean;
    }

    public String parsableStringValue(T paramT)
    {
      return paramT.name();
    }

    protected T parse(String paramString)
      throws InvalidFlagValueException
    {
      try
      {
        Class localClass = this.enumType;
        if (this.autoUpperCase)
          paramString = paramString.toUpperCase();
        Enum localEnum = Enum.valueOf(localClass, paramString);
        return localEnum;
      }
      catch (IllegalArgumentException localIllegalArgumentException)
      {
      }
      throw new InvalidFlagValueException("Not a valid enum " + this.enumType.getName() + " constant");
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.common.flags.Flag
 * JD-Core Version:    0.6.2
 */