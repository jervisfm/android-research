package com.google.common.flags;

class FlagDescription
  implements Comparable<FlagDescription>
{
  private final String altName;
  private final String containerClassName;
  private final String doc;
  private final DocLevel docLevel;
  private final String shortFlagName;
  private final String simpleFieldName;
  private final String type;

  private FlagDescription(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, DocLevel paramDocLevel, String paramString6)
  {
    if (paramString2 == null)
      throw new NullPointerException();
    if (paramString4 == null)
      throw new NullPointerException();
    if (paramString5 == null)
      throw new NullPointerException();
    if (paramDocLevel == null)
      throw new NullPointerException();
    this.shortFlagName = emptyStringToNull(paramString1);
    this.containerClassName = paramString2;
    this.simpleFieldName = paramString3;
    this.type = paramString4;
    this.doc = paramString5;
    this.docLevel = paramDocLevel;
    this.altName = emptyStringToNull(paramString6);
    if ((!isField()) && (!isShortFlagNameSpecified()))
      throw new NullPointerException();
    checkNotReserved(getShortFlagName());
    checkNotReserved(getAltName());
  }

  private static void checkNotReserved(String paramString)
  {
    if ((paramString != null) && ((paramString.equals("help")) || (paramString.equals("helpxml")) || (paramString.equals("flagfile")) || (paramString.equals("flagresource"))))
      throw new IllegalArgumentException("The " + paramString + " flag is built-in and may not be registered");
  }

  static Builder createFlagFromField(String paramString)
  {
    return new Builder(parseContainerClassName(paramString), parseSimpleFieldName(paramString), null);
  }

  static Builder createManuallyRegisteredFlag(String paramString1, String paramString2)
  {
    return new Builder(paramString2, null, null).shortFlagName(stripFlagPrefix(paramString1));
  }

  private static String emptyStringToNull(String paramString)
  {
    if ((paramString == null) || (paramString.isEmpty()))
      paramString = null;
    return paramString;
  }

  private static int findLastDotIndex(String paramString)
  {
    int i = paramString.lastIndexOf(".");
    if (i <= 0)
      throw new IllegalArgumentException("Package name required in name: " + paramString);
    return i;
  }

  private static boolean nullSafeEquals(Object paramObject1, Object paramObject2)
  {
    if (paramObject1 == null)
      return paramObject1 == paramObject2;
    return paramObject1.equals(paramObject2);
  }

  private static String parseContainerClassName(String paramString)
  {
    return paramString.substring(0, findLastDotIndex(paramString));
  }

  private static String parseSimpleFieldName(String paramString)
  {
    return paramString.substring(1 + findLastDotIndex(paramString));
  }

  private static String stripFlagPrefix(String paramString)
  {
    if (paramString.startsWith("FLAG_"))
      paramString = paramString.substring(5);
    return paramString;
  }

  public int compareTo(FlagDescription paramFlagDescription)
  {
    return getLongFlagName().compareTo(paramFlagDescription.getLongFlagName());
  }

  public boolean equals(Object paramObject)
  {
    if (!(paramObject instanceof FlagDescription));
    FlagDescription localFlagDescription;
    do
    {
      return false;
      localFlagDescription = (FlagDescription)paramObject;
    }
    while ((localFlagDescription != this) && ((!localFlagDescription.getShortFlagName().equals(getShortFlagName())) || (!localFlagDescription.containerClassName.equals(this.containerClassName)) || (!nullSafeEquals(localFlagDescription.simpleFieldName, this.simpleFieldName)) || (!localFlagDescription.type.equals(this.type)) || (!localFlagDescription.doc.equals(this.doc)) || (!localFlagDescription.docLevel.equals(this.docLevel)) || (!nullSafeEquals(localFlagDescription.altName, this.altName))));
    return true;
  }

  String getAltName()
  {
    return this.altName;
  }

  String getContainerClassName()
  {
    return this.containerClassName;
  }

  String getDoc()
  {
    return this.doc;
  }

  DocLevel getDocLevel()
  {
    return this.docLevel;
  }

  String getFullyQualifiedFieldName()
  {
    return getContainerClassName() + "." + getSimpleFieldName();
  }

  String getLongFlagName()
  {
    return getContainerClassName() + "." + getShortFlagName();
  }

  String getShortFlagName()
  {
    if (isShortFlagNameSpecified())
      return this.shortFlagName;
    return stripFlagPrefix(getSimpleFieldName());
  }

  String getSimpleFieldName()
  {
    if (!isField())
      throw new UnsupportedOperationException("Flag " + getLongFlagName() + " is not associated with a field");
    return this.simpleFieldName;
  }

  String getType()
  {
    return this.type;
  }

  public int hashCode()
  {
    return getLongFlagName().hashCode();
  }

  boolean isField()
  {
    return this.simpleFieldName != null;
  }

  boolean isPositiveFormOfName(String paramString)
  {
    if (paramString == null);
    while ((!paramString.equals(getShortFlagName())) && (!paramString.equals(getAltName())) && (!paramString.equals(getLongFlagName())) && ((!isField()) || (!paramString.equals(getFullyQualifiedFieldName()))))
      return false;
    return true;
  }

  boolean isShortFlagNameSpecified()
  {
    return this.shortFlagName != null;
  }

  public String toString()
  {
    return getLongFlagName();
  }

  static class Builder
  {
    private String altName;
    private final String containerClassName;
    private String doc;
    private DocLevel docLevel;
    private String shortFlagName;
    private final String simpleFieldName;
    private String type;

    private Builder(String paramString1, String paramString2)
    {
      this.containerClassName = paramString1;
      this.simpleFieldName = paramString2;
    }

    Builder altName(String paramString)
    {
      this.altName = paramString;
      return this;
    }

    FlagDescription build()
    {
      return new FlagDescription(this.shortFlagName, this.containerClassName, this.simpleFieldName, this.type, this.doc, this.docLevel, this.altName, null);
    }

    Builder doc(String paramString)
    {
      this.doc = paramString;
      return this;
    }

    Builder docLevel(DocLevel paramDocLevel)
    {
      this.docLevel = paramDocLevel;
      return this;
    }

    Builder shortFlagName(String paramString)
    {
      this.shortFlagName = paramString;
      return this;
    }

    Builder type(String paramString)
    {
      this.type = paramString;
      return this;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.common.flags.FlagDescription
 * JD-Core Version:    0.6.2
 */