package com.google.common.flags;

public abstract class FlagException extends Exception
{
  private static final long serialVersionUID = 98239333L;

  FlagException(String paramString)
  {
    super(paramString);
  }

  FlagException(String paramString, Throwable paramThrowable)
  {
    super(paramString, paramThrowable);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.common.flags.FlagException
 * JD-Core Version:    0.6.2
 */