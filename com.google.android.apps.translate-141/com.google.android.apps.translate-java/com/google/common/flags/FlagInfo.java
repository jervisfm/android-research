package com.google.common.flags;

import java.util.List;

public abstract interface FlagInfo
{
  public abstract boolean accessed();

  public abstract String containerClass();

  public abstract Object defaultValue();

  public abstract String doc();

  public abstract List<String> names();

  public abstract String parsableStringValue();

  public abstract String type();

  public abstract Object value();
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.common.flags.FlagInfo
 * JD-Core Version:    0.6.2
 */