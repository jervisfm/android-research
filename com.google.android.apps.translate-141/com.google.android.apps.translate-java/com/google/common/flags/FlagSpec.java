package com.google.common.flags;

import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({java.lang.annotation.ElementType.FIELD})
public @interface FlagSpec
{
  public abstract String altName();

  public abstract DocLevel docLevel();

  public abstract String help();

  public abstract String name();
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.common.flags.FlagSpec
 * JD-Core Version:    0.6.2
 */