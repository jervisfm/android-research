package com.google.common.flags;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public final class FlagValues
{
  private final Map<String, String> values = new HashMap();

  public void addFlag(String paramString1, String paramString2)
  {
    this.values.put(paramString1, paramString2);
  }

  public String getAllFlagsAsString()
  {
    StringBuilder localStringBuilder = new StringBuilder(32);
    String[] arrayOfString = getAllFlagsAsStringArray();
    for (int i = 0; i < arrayOfString.length; i++)
    {
      localStringBuilder.append(arrayOfString[i]);
      if (i < -1 + arrayOfString.length)
        localStringBuilder.append(' ');
    }
    return localStringBuilder.toString();
  }

  public String[] getAllFlagsAsStringArray()
  {
    String[] arrayOfString = new String[this.values.size()];
    int i = 0;
    Iterator localIterator = this.values.keySet().iterator();
    if (localIterator.hasNext())
    {
      String str1 = (String)localIterator.next();
      String str2 = (String)this.values.get(str1);
      StringBuilder localStringBuilder = new StringBuilder().append("--").append(str1);
      if ("".equals(str2));
      for (String str3 = ""; ; str3 = "=" + str2)
      {
        arrayOfString[i] = str3;
        i++;
        break;
      }
    }
    return arrayOfString;
  }

  public String getFlagValue(String paramString)
  {
    return (String)this.values.get(paramString);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.common.flags.FlagValues
 * JD-Core Version:    0.6.2
 */