package com.google.common.flags;

import com.google.common.annotations.VisibleForTesting;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.security.AccessControlException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Flags
{
  private static final Pattern BLANK_LINE_PATTERN;
  private static final String CLASS_LOADER = "com.google.common.flags.classLoader";
  private static final Pattern COMMENT_LINE_PATTERN;
  public static final String DISABLE_CHECKING = "com.google.common.flags.disableStateChecking";
  private static final String DISABLE_EXIT = "com.google.common.flags.noExit";
  private static final String[] EMPTY_STRING_ARRAY;
  private static final Pattern FLAG_FILE_PATTERN;
  private static final Pattern FLAG_NAME_PATTERN;
  private static final Pattern FLAG_PATTERN;
  private static final Pattern FLAG_RESOURCE_PATTERN;
  private static final Pattern HELPSHORT_PATTERN;
  private static final Pattern HELP_PATTERN;
  private static final String UNDEF_OK_FLAG_NAME = "undefok";
  private static final Pattern XML_HELP_PATTERN;
  private static final String cachedMainClassName;
  private static final Collection<Runnable> completionHooks = new ArrayList();
  private static ClassLoader flagClassLoader;
  private static final Logger logger = Logger.getLogger(Flags.class.getName());
  private static PrintStream outputStream = System.out;
  private static Throwable parseStackTrace;
  private static ParseState parseState;
  private static final Set<String> preferredClasses;
  private static boolean undefOkSupported;
  private static String usagePrefix;

  static
  {
    EMPTY_STRING_ARRAY = new String[0];
    parseState = ParseState.NOT_STARTED;
    usagePrefix = null;
    preferredClasses = new HashSet();
    flagClassLoader = null;
    undefOkSupported = false;
    Object localObject = getMainClassNameFromStackTrace(Thread.currentThread().getStackTrace());
    if (localObject == null);
    try
    {
      Iterator localIterator = Thread.getAllStackTraces().values().iterator();
      do
      {
        if (!localIterator.hasNext())
          break;
        String str = getMainClassNameFromStackTrace((StackTraceElement[])localIterator.next());
        localObject = str;
      }
      while (localObject == null);
      cachedMainClassName = (String)localObject;
      HELP_PATTERN = Pattern.compile("-+help(=true)?");
      HELPSHORT_PATTERN = Pattern.compile("-+helpshort(=true)?");
      XML_HELP_PATTERN = Pattern.compile("-+helpxml(=true)?");
      FLAG_PATTERN = Pattern.compile("-+([^=]*)(?:=(.*))?", 40);
      FLAG_FILE_PATTERN = Pattern.compile("-+flagfile=(.+)");
      FLAG_RESOURCE_PATTERN = Pattern.compile("-+flagresource=(.+)");
      BLANK_LINE_PATTERN = Pattern.compile("\\s*");
      COMMENT_LINE_PATTERN = Pattern.compile("\\s*#.*");
      FLAG_NAME_PATTERN = Pattern.compile("\\w[_\\w]*");
      return;
    }
    catch (AccessControlException localAccessControlException)
    {
      while (true)
        logger.log(Level.FINE, "Unable to calculate main class name", localAccessControlException);
    }
  }

  public static void addPreferredClass(Class<?> paramClass)
  {
    addPreferredClass(paramClass.getName());
  }

  public static void addPreferredClass(String paramString)
  {
    preferredClasses.add(paramString);
  }

  public static Collection<FlagInfo> allFlags()
  {
    return Collections.unmodifiableCollection(allFlagsInternal());
  }

  private static Collection<FlagInfoImpl> allFlagsInternal()
  {
    HashMap localHashMap = new HashMap();
    Iterator localIterator1 = expandedFlagMap().entrySet().iterator();
    while (localIterator1.hasNext())
    {
      Map.Entry localEntry2 = (Map.Entry)localIterator1.next();
      String str = (String)localEntry2.getKey();
      Set localSet = (Set)localEntry2.getValue();
      if (localSet.size() == 1)
      {
        FlagDescription localFlagDescription2 = (FlagDescription)localSet.iterator().next();
        Object localObject = (Set)localHashMap.get(localFlagDescription2);
        if (localObject == null)
        {
          localObject = new TreeSet();
          localHashMap.put(localFlagDescription2, localObject);
        }
        ((Set)localObject).add(str);
      }
    }
    TreeSet localTreeSet = new TreeSet();
    Iterator localIterator2 = localHashMap.entrySet().iterator();
    while (localIterator2.hasNext())
    {
      Map.Entry localEntry1 = (Map.Entry)localIterator2.next();
      FlagDescription localFlagDescription1 = (FlagDescription)localEntry1.getKey();
      localTreeSet.add(new FlagInfoImpl(new ArrayList((Collection)localEntry1.getValue()), localFlagDescription1));
    }
    return localTreeSet;
  }

  private static void appendExternalFlags(BufferedReader paramBufferedReader, List<String> paramList)
    throws InvalidFlagSyntaxException, ExternalFlagsLoadException, IOException
  {
    String str = paramBufferedReader.readLine();
    if (str != null)
    {
      if (FLAG_PATTERN.matcher(str).matches())
        if ((FLAG_FILE_PATTERN.matcher(str).matches()) || (FLAG_RESOURCE_PATTERN.matcher(str).matches()))
          paramList.addAll(Arrays.asList(loadExternalFlags(new String[] { str })));
      while ((BLANK_LINE_PATTERN.matcher(str).matches()) || (COMMENT_LINE_PATTERN.matcher(str).matches()))
        while (true)
        {
          str = paramBufferedReader.readLine();
          break;
          paramList.add(str);
        }
      throw new InvalidFlagSyntaxException(str);
    }
  }

  // ERROR //
  private static void appendExternalFlagsFromFile(List<String> paramList, String paramString1, String paramString2)
    throws InvalidFlagSyntaxException, IOException, ExternalFlagsLoadException
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_3
    //   2: new 284	java/io/BufferedReader
    //   5: dup
    //   6: new 321	java/io/FileReader
    //   9: dup
    //   10: aload_1
    //   11: invokespecial 322	java/io/FileReader:<init>	(Ljava/lang/String;)V
    //   14: invokespecial 325	java/io/BufferedReader:<init>	(Ljava/io/Reader;)V
    //   17: astore 4
    //   19: aload 4
    //   21: aload_0
    //   22: invokestatic 327	com/google/common/flags/Flags:appendExternalFlags	(Ljava/io/BufferedReader;Ljava/util/List;)V
    //   25: aload 4
    //   27: ifnull +8 -> 35
    //   30: aload 4
    //   32: invokevirtual 330	java/io/BufferedReader:close	()V
    //   35: return
    //   36: astore 7
    //   38: new 280	com/google/common/flags/ExternalFlagsLoadException
    //   41: dup
    //   42: aload_2
    //   43: invokespecial 331	com/google/common/flags/ExternalFlagsLoadException:<init>	(Ljava/lang/String;)V
    //   46: athrow
    //   47: astore 6
    //   49: aload_3
    //   50: ifnull +7 -> 57
    //   53: aload_3
    //   54: invokevirtual 330	java/io/BufferedReader:close	()V
    //   57: aload 6
    //   59: athrow
    //   60: astore 6
    //   62: aload 4
    //   64: astore_3
    //   65: goto -16 -> 49
    //   68: astore 5
    //   70: aload 4
    //   72: astore_3
    //   73: goto -35 -> 38
    //
    // Exception table:
    //   from	to	target	type
    //   2	19	36	java/io/FileNotFoundException
    //   2	19	47	finally
    //   38	47	47	finally
    //   19	25	60	finally
    //   19	25	68	java/io/FileNotFoundException
  }

  private static void appendExternalFlagsFromResource(List<String> paramList, String paramString1, String paramString2)
    throws ExternalFlagsLoadException, InvalidFlagSyntaxException, IOException
  {
    InputStream localInputStream = Flags.class.getResourceAsStream(paramString1);
    if (localInputStream == null)
    {
      ClassLoader localClassLoader = Thread.currentThread().getContextClassLoader();
      if (localClassLoader != null)
        localInputStream = localClassLoader.getResourceAsStream(paramString1);
    }
    if (localInputStream == null)
      throw new ExternalFlagsLoadException(paramString2);
    try
    {
      appendExternalFlags(new BufferedReader(new InputStreamReader(localInputStream)), paramList);
      return;
    }
    finally
    {
      localInputStream.close();
    }
  }

  static Map<String, FlagDescription> canonicalFlagMap()
  {
    if (FlagMapHolder.canonicalFlagMap == null)
      initMaps();
    return FlagMapHolder.canonicalFlagMap;
  }

  public static void clearFlagData()
  {
    if (parseState != ParseState.DONE)
      throw new IllegalStateException("Flag parsing must be completed");
    FlagMapHolder.manuallyRegisteredFlags.clear();
    FlagMapHolder.canonicalFlagMap = Collections.emptyMap();
    FlagMapHolder.expandedFlagMap = Collections.emptyMap();
    FlagMapHolder.whitelistedPrefixes = null;
    FlagMapHolder.longNameMap = Collections.emptyMap();
  }

  static void clearFlagMapsForTesting()
  {
    reallyClearFlagMapsForTesting();
    FlagMapHolder.longNameMap = loadFlagManifests();
  }

  static Map<String, FlagDescription> createCanonicalFlagMap(Collection<FlagDescription> paramCollection, Map<String, Set<FlagDescription>> paramMap)
  {
    HashMap localHashMap = new HashMap();
    Iterator localIterator = paramCollection.iterator();
    while (localIterator.hasNext())
    {
      FlagDescription localFlagDescription = (FlagDescription)localIterator.next();
      localHashMap.put(getBestFlagName(localFlagDescription, paramMap), localFlagDescription);
    }
    return localHashMap;
  }

  public static void disableStateCheckingForTest()
  {
    System.getProperties().setProperty("com.google.common.flags.disableStateChecking", "true");
  }

  public static void disableUndefOkSupport()
  {
    undefOkSupported = false;
  }

  public static void enableStateCheckingForTest()
  {
    System.getProperties().setProperty("com.google.common.flags.disableStateChecking", "false");
  }

  public static void enableUndefOkSupport()
  {
    undefOkSupported = true;
  }

  private static void exitUnlessDisabled(int paramInt)
  {
    if (!Boolean.getBoolean("com.google.common.flags.noExit"))
      System.exit(paramInt);
  }

  static Map<String, Set<FlagDescription>> expandFlagMap(Collection<FlagDescription> paramCollection)
  {
    HashMap localHashMap = new HashMap();
    Iterator localIterator1 = paramCollection.iterator();
    while (localIterator1.hasNext())
    {
      FlagDescription localFlagDescription = (FlagDescription)localIterator1.next();
      Iterator localIterator4 = getAllNamesForFlag(localFlagDescription).iterator();
      while (localIterator4.hasNext())
      {
        String str2 = (String)localIterator4.next();
        Object localObject = (Set)localHashMap.get(str2);
        if (localObject == null)
        {
          localObject = new HashSet();
          localHashMap.put(str2, localObject);
        }
        ((Set)localObject).add(localFlagDescription);
      }
    }
    Iterator localIterator2 = localHashMap.keySet().iterator();
    while (localIterator2.hasNext())
    {
      String str1 = (String)localIterator2.next();
      Set localSet = (Set)localHashMap.get(str1);
      if (localSet.size() > 1)
      {
        StringBuilder localStringBuilder = new StringBuilder();
        Iterator localIterator3 = localSet.iterator();
        while (localIterator3.hasNext())
        {
          localStringBuilder.append(((FlagDescription)localIterator3.next()).getLongFlagName());
          localStringBuilder.append(',');
        }
        Logger localLogger = logger;
        Level localLevel = Level.FINE;
        Object[] arrayOfObject = new Object[3];
        arrayOfObject[0] = str1;
        arrayOfObject[1] = Integer.valueOf(localSet.size());
        arrayOfObject[2] = localStringBuilder;
        localLogger.log(localLevel, "Flag {0} is not a unique short form because {1} flags use it: {2}", arrayOfObject);
      }
    }
    return Collections.unmodifiableMap(localHashMap);
  }

  private static Map<String, Set<FlagDescription>> expandedFlagMap()
  {
    if (FlagMapHolder.expandedFlagMap == null)
      initMaps();
    return FlagMapHolder.expandedFlagMap;
  }

  public static Collection<FlagInfo> exposedFlags()
  {
    Collection localCollection = allFlagsInternal();
    Iterator localIterator = localCollection.iterator();
    while (localIterator.hasNext())
    {
      FlagInfoImpl localFlagInfoImpl = (FlagInfoImpl)localIterator.next();
      if ((localFlagInfoImpl.desc.getDocLevel() != DocLevel.PUBLIC) || (!whitelistAllowsAliasing(localFlagInfoImpl.desc)))
        localIterator.remove();
    }
    return Collections.unmodifiableCollection(localCollection);
  }

  static Flag<?> flag(FlagDescription paramFlagDescription)
  {
    Flag localFlag1 = (Flag)manuallyRegisteredFlags().get(paramFlagDescription.getLongFlagName());
    if (localFlag1 != null)
      return localFlag1;
    try
    {
      Field localField = loadClass(paramFlagDescription.getContainerClassName()).getDeclaredField(paramFlagDescription.getSimpleFieldName());
      localField.setAccessible(true);
      Flag localFlag2 = (Flag)localField.get(null);
      return localFlag2;
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      throw new LinkageError("Class for flag field " + paramFlagDescription.getFullyQualifiedFieldName() + " present in manifest, absent at runtime: " + localClassNotFoundException.toString());
    }
    catch (NoSuchFieldException localNoSuchFieldException)
    {
      throw new LinkageError("Flag field " + paramFlagDescription.getFullyQualifiedFieldName() + " present in manifest, absent at runtime: " + localNoSuchFieldException.toString());
    }
    catch (IllegalAccessException localIllegalAccessException)
    {
      throw new LinkageError("Unable to get flag field " + paramFlagDescription.getFullyQualifiedFieldName() + ": " + localIllegalAccessException.toString());
    }
    catch (NullPointerException localNullPointerException)
    {
      throw new LinkageError("Forgot to make the flag static? " + paramFlagDescription.getFullyQualifiedFieldName() + ": " + localNullPointerException.toString());
    }
    catch (ClassCastException localClassCastException)
    {
      throw new LinkageError("Cannot convert field " + paramFlagDescription.getSimpleFieldName() + " to a Flag in order to resolve " + paramFlagDescription.getFullyQualifiedFieldName() + ": " + localClassCastException.toString());
    }
  }

  private static void formatFlag(PrintStream paramPrintStream, String paramString1, String paramString2, String paramString3)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("   --").append(paramString1).append(" ").append(paramString2);
    if (paramString3 != null)
      localStringBuilder.append("; default: ").append(paramString3);
    while (true)
    {
      int i;
      if (localStringBuilder.length() > 70)
      {
        i = localStringBuilder.lastIndexOf(" ", 70);
        if (i == -1)
          i = localStringBuilder.indexOf(" ", 70);
        if (i >= 10);
      }
      else
      {
        if (localStringBuilder.length() != 0)
          paramPrintStream.println(localStringBuilder);
        return;
      }
      paramPrintStream.println(localStringBuilder.substring(0, i));
      localStringBuilder.replace(0, i + 1, "      ");
    }
  }

  @Deprecated
  public static Map<String, Object> getAllFlags()
  {
    TreeMap localTreeMap = new TreeMap();
    Iterator localIterator = canonicalFlagMap().entrySet().iterator();
    while (localIterator.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      String str = (String)localEntry.getKey();
      FlagDescription localFlagDescription = (FlagDescription)localEntry.getValue();
      if (str.indexOf('.') < 0)
        localTreeMap.put(str, flag(localFlagDescription).value);
    }
    return localTreeMap;
  }

  static List<String> getAllNamesForFlag(FlagDescription paramFlagDescription)
  {
    LinkedHashSet localLinkedHashSet = new LinkedHashSet();
    if (whitelistAllowsAliasing(paramFlagDescription))
    {
      if (paramFlagDescription.getAltName() != null)
        localLinkedHashSet.add(paramFlagDescription.getAltName());
      localLinkedHashSet.add(paramFlagDescription.getShortFlagName());
    }
    localLinkedHashSet.add(paramFlagDescription.getLongFlagName());
    if (!paramFlagDescription.isShortFlagNameSpecified())
      localLinkedHashSet.add(paramFlagDescription.getFullyQualifiedFieldName());
    if (paramFlagDescription.getType().equals("java.lang.Boolean"))
      localLinkedHashSet.addAll(getNoPrefixedAliases(localLinkedHashSet));
    return new ArrayList(localLinkedHashSet);
  }

  public static Collection<String> getAllowedFlags()
  {
    if (FlagMapHolder.whitelistedPrefixes == null)
      return null;
    return new LinkedHashSet(FlagMapHolder.whitelistedPrefixes);
  }

  private static String getAsString(Flag<?> paramFlag)
  {
    try
    {
      String str = paramFlag.parsableStringValue();
      return str;
    }
    catch (UnsupportedOperationException localUnsupportedOperationException)
    {
    }
    return valueToString(paramFlag.get());
  }

  private static FlagDescription getBestFlag(String paramString)
  {
    Set localSet = (Set)expandedFlagMap().get(paramString);
    if (localSet == null)
      return null;
    if (localSet.size() == 1)
      return (FlagDescription)localSet.iterator().next();
    Object localObject = null;
    Iterator localIterator = localSet.iterator();
    while (true)
    {
      if (!localIterator.hasNext())
        break label99;
      FlagDescription localFlagDescription = (FlagDescription)localIterator.next();
      if (preferredClasses.contains(localFlagDescription.getContainerClassName()))
      {
        if (localObject != null)
          break;
        localObject = localFlagDescription;
      }
    }
    label99: return localObject;
  }

  private static String getBestFlagName(FlagDescription paramFlagDescription, Map<String, Set<FlagDescription>> paramMap)
  {
    Iterator localIterator = getAllNamesForFlag(paramFlagDescription).iterator();
    while (localIterator.hasNext())
    {
      String str = (String)localIterator.next();
      Set localSet = (Set)paramMap.get(str);
      if ((localSet != null) && (localSet.size() == 1))
        return str;
    }
    throw new AssertionError("Must be one unique name for flag?!");
  }

  private static <T> String getDefaultAsString(Flag<T> paramFlag)
  {
    Object localObject = paramFlag.getDefault();
    if (localObject == null)
      return null;
    try
    {
      String str = paramFlag.parsableStringValue(localObject);
      return str;
    }
    catch (UnsupportedOperationException localUnsupportedOperationException)
    {
    }
    return valueToString(localObject);
  }

  private static ClassLoader getFlagManifestClassLoader()
  {
    String str = System.getProperty("com.google.common.flags.classLoader");
    if ((str != null) && (str.length() > 0));
    try
    {
      ClassLoader localClassLoader = (ClassLoader)Class.forName(str).newInstance();
      logger.info("User specified classloader: " + str);
      return localClassLoader;
    }
    catch (InstantiationException localInstantiationException)
    {
      logger.log(Level.WARNING, String.format("Couldn't instantiate ClassLoader '%s'", new Object[] { str }), localInstantiationException);
      return Flags.class.getClassLoader();
    }
    catch (IllegalAccessException localIllegalAccessException)
    {
      while (true)
        logger.log(Level.WARNING, String.format("Couldn't instantiate ClassLoader '%s'", new Object[] { str }), localIllegalAccessException);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      while (true)
        logger.log(Level.WARNING, String.format("Couldn't instantiate ClassLoader '%s'", new Object[] { str }), localClassNotFoundException);
    }
  }

  public static String getMainClassName()
  {
    return cachedMainClassName;
  }

  private static String getMainClassNameFromStackTrace(StackTraceElement[] paramArrayOfStackTraceElement)
  {
    if (paramArrayOfStackTraceElement.length > 0)
    {
      StackTraceElement localStackTraceElement = paramArrayOfStackTraceElement[(-1 + paramArrayOfStackTraceElement.length)];
      String str = localStackTraceElement.getMethodName();
      if (("main".equals(str)) || ("<clinit>".equals(str)))
        return localStackTraceElement.getClassName();
    }
    return null;
  }

  private static List<String> getNoPrefixedAliases(Iterable<String> paramIterable)
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = paramIterable.iterator();
    while (localIterator.hasNext())
    {
      String str = (String)localIterator.next();
      localArrayList.add("no" + str);
    }
    return localArrayList;
  }

  private static String getProgramName()
  {
    String str = "unknown";
    StackTraceElement[] arrayOfStackTraceElement = new Throwable().getStackTrace();
    if ((arrayOfStackTraceElement != null) && (arrayOfStackTraceElement.length > 0))
      str = arrayOfStackTraceElement[(-1 + arrayOfStackTraceElement.length)].getClassName();
    return str;
  }

  private static String getStackTraceAsString(Throwable paramThrowable)
  {
    StringWriter localStringWriter = new StringWriter();
    paramThrowable.printStackTrace(new PrintWriter(localStringWriter));
    return localStringWriter.toString();
  }

  private static List<String> getUnknownFlags(List<String> paramList, Set<String> paramSet)
  {
    ArrayList localArrayList = new ArrayList();
    Iterator localIterator = paramList.iterator();
    while (localIterator.hasNext())
    {
      String str = (String)localIterator.next();
      if ((!paramSet.contains(str)) && ((!str.startsWith("no")) || (!paramSet.contains(str.substring(2)))))
        localArrayList.add(str);
    }
    return localArrayList;
  }

  public static String getUsagePrefix()
  {
    return usagePrefix;
  }

  private static void initMaps()
  {
    Collection localCollection = longNameMap().values();
    FlagMapHolder.expandedFlagMap = expandFlagMap(localCollection);
    FlagMapHolder.canonicalFlagMap = createCanonicalFlagMap(localCollection, FlagMapHolder.expandedFlagMap);
  }

  private static boolean isAllowedUndefOkFlag(String paramString)
  {
    return (isUndefOkSupported()) && ("undefok".equals(paramString));
  }

  private static boolean isBooleanFlag(FlagDescription paramFlagDescription)
  {
    return (paramFlagDescription != null) && (paramFlagDescription.getType().equals("java.lang.Boolean"));
  }

  private static boolean isLegitimateFlag(FlagDescription paramFlagDescription)
  {
    return paramFlagDescription != null;
  }

  public static boolean isUndefOkSupported()
  {
    return undefOkSupported;
  }

  private static Class<?> loadClass(String paramString)
    throws ClassNotFoundException
  {
    while (true)
      try
      {
        if (flagClassLoader != null)
          return Class.forName(paramString, true, flagClassLoader);
        Class localClass = Class.forName(paramString);
        return localClass;
      }
      catch (ClassNotFoundException localClassNotFoundException)
      {
        int i = paramString.lastIndexOf('.');
        if (i < 0)
          throw localClassNotFoundException;
        paramString = paramString.substring(0, i) + "$" + paramString.substring(i + 1, paramString.length());
      }
  }

  private static String[] loadExternalFlags(String[] paramArrayOfString)
    throws InvalidFlagSyntaxException, ExternalFlagsLoadException
  {
    ArrayList localArrayList = new ArrayList();
    int i = paramArrayOfString.length;
    for (int j = 0; ; j++)
      if (j < i)
      {
        String str = paramArrayOfString[j];
        try
        {
          Matcher localMatcher1 = FLAG_FILE_PATTERN.matcher(str);
          Matcher localMatcher2 = FLAG_RESOURCE_PATTERN.matcher(str);
          if (localMatcher1.matches())
            appendExternalFlagsFromFile(localArrayList, localMatcher1.group(1), str);
          else if (localMatcher2.matches())
            appendExternalFlagsFromResource(localArrayList, localMatcher2.group(1), str);
        }
        catch (IOException localIOException)
        {
          throw new ExternalFlagsLoadException(str);
        }
        localArrayList.add(str);
      }
      else
      {
        return (String[])localArrayList.toArray(EMPTY_STRING_ARRAY);
      }
  }

  // ERROR //
  private static Map<String, FlagDescription> loadFlagManifests()
  {
    // Byte code:
    //   0: new 232	java/util/HashMap
    //   3: dup
    //   4: invokespecial 233	java/util/HashMap:<init>	()V
    //   7: astore_0
    //   8: invokestatic 777	com/google/common/flags/Flags:getFlagManifestClassLoader	()Ljava/lang/ClassLoader;
    //   11: astore_1
    //   12: aload_1
    //   13: ldc_w 779
    //   16: invokevirtual 783	java/lang/ClassLoader:getResources	(Ljava/lang/String;)Ljava/util/Enumeration;
    //   19: astore_3
    //   20: aload_3
    //   21: invokeinterface 788 1 0
    //   26: ifeq +95 -> 121
    //   29: aload_3
    //   30: invokeinterface 791 1 0
    //   35: checkcast 793	java/net/URL
    //   38: astore 4
    //   40: new 795	java/io/BufferedInputStream
    //   43: dup
    //   44: aload 4
    //   46: invokevirtual 799	java/net/URL:openStream	()Ljava/io/InputStream;
    //   49: invokespecial 800	java/io/BufferedInputStream:<init>	(Ljava/io/InputStream;)V
    //   52: astore 5
    //   54: aload 5
    //   56: aload_0
    //   57: invokestatic 806	com/google/common/flags/XmlSupport:fromXml	(Ljava/io/InputStream;Ljava/util/Map;)V
    //   60: aload 5
    //   62: invokevirtual 351	java/io/InputStream:close	()V
    //   65: goto -45 -> 20
    //   68: astore_2
    //   69: new 652	java/lang/AssertionError
    //   72: dup
    //   73: aload_2
    //   74: invokespecial 656	java/lang/AssertionError:<init>	(Ljava/lang/Object;)V
    //   77: athrow
    //   78: astore 7
    //   80: new 775	com/google/common/flags/MalformedFlagDescriptionException
    //   83: dup
    //   84: new 437	java/lang/StringBuilder
    //   87: dup
    //   88: invokespecial 438	java/lang/StringBuilder:<init>	()V
    //   91: ldc_w 808
    //   94: invokevirtual 445	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   97: aload 4
    //   99: invokevirtual 811	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   102: invokevirtual 535	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   105: aload 7
    //   107: invokespecial 814	com/google/common/flags/MalformedFlagDescriptionException:<init>	(Ljava/lang/String;Ljava/lang/Throwable;)V
    //   110: athrow
    //   111: astore 6
    //   113: aload 5
    //   115: invokevirtual 351	java/io/InputStream:close	()V
    //   118: aload 6
    //   120: athrow
    //   121: aload_0
    //   122: areturn
    //
    // Exception table:
    //   from	to	target	type
    //   12	20	68	java/io/IOException
    //   20	54	68	java/io/IOException
    //   60	65	68	java/io/IOException
    //   113	121	68	java/io/IOException
    //   54	60	78	com/google/common/flags/MalformedFlagDescriptionException
    //   54	60	111	finally
    //   80	111	111	finally
  }

  private static Map<String, FlagDescription> longNameMap()
  {
    return FlagMapHolder.longNameMap;
  }

  private static Map<String, Flag<?>> manuallyRegisteredFlags()
  {
    return FlagMapHolder.manuallyRegisteredFlags;
  }

  private static void maybeSyslogOnStart(String[] paramArrayOfString)
  {
    try
    {
      String str1 = getMainClassName();
      if (str1 == null)
        return;
      String str2 = InetAddress.getLocalHost().getHostName();
      if (str2.contains(".corp.google.com"))
      {
        Class localClass = Class.forName("com.sun.security.auth.module.UnixSystem");
        Object localObject = localClass.newInstance();
        Long localLong = (Long)localClass.getMethod("getUid", new Class[0]).invoke(localObject, new Object[0]);
        String str3 = " log_path:\"" + str1 + "\"" + " language:\"java\"" + " tool_type:\"cmdline\"" + " uid:" + localLong + " host_name:\"" + str2 + "\"" + " log_timestamp:" + System.currentTimeMillis() / 1000L + " logger:\"logger_java\"";
        StringBuilder localStringBuilder = new StringBuilder();
        int i = paramArrayOfString.length;
        for (int j = 0; j < i; j++)
        {
          String str4 = paramArrayOfString[j];
          localStringBuilder.append(" argv:\"" + str4 + "\"");
        }
        String[] arrayOfString2;
        if (new File("/usr/lib/crudd/log_usage").exists())
        {
          arrayOfString2 = new String[3];
          arrayOfString2[0] = "/usr/lib/crudd/log_usage";
          arrayOfString2[1] = "--tool_log_proto";
          arrayOfString2[2] = (str3 + localStringBuilder);
        }
        String[] arrayOfString1;
        for (ProcessBuilder localProcessBuilder = new ProcessBuilder(arrayOfString2); ; localProcessBuilder = new ProcessBuilder(arrayOfString1))
        {
          Process localProcess = localProcessBuilder.start();
          localProcess.getInputStream().close();
          localProcess.getOutputStream().close();
          localProcess.getErrorStream().close();
          return;
          arrayOfString1 = new String[2];
          arrayOfString1[0] = "/usr/bin/logger";
          arrayOfString1[1] = ("ToolLogProto: logjam_tag=tattler_initgoogle " + str3);
        }
      }
      return;
    }
    catch (Throwable localThrowable)
    {
    }
  }

  public static String[] parse(String[] paramArrayOfString)
  {
    maybeSyslogOnStart(paramArrayOfString);
    String[] arrayOfString;
    try
    {
      setParseState(ParseState.IN_PROGRESS);
      arrayOfString = parseInternal(paramArrayOfString);
      try
      {
        setParseState(ParseState.DONE);
        Runnable[] arrayOfRunnable = (Runnable[])completionHooks.toArray(new Runnable[0]);
        int i = arrayOfRunnable.length;
        for (int j = 0; j < i; j++)
          arrayOfRunnable[j].run();
      }
      finally
      {
      }
    }
    catch (FlagException localFlagException)
    {
      throw new InvalidFlagsException(localFlagException.getMessage(), localFlagException);
    }
    return arrayOfString;
  }

  private static String[] parseInternal(String[] paramArrayOfString)
    throws AmbiguousFlagException, ExternalFlagsLoadException, InvalidFlagSyntaxException, InvalidFlagValueException, UnrecognizedFlagException
  {
    initMaps();
    String[] arrayOfString1 = loadExternalFlags(paramArrayOfString);
    ArrayList localArrayList1 = new ArrayList();
    ArrayList localArrayList2 = new ArrayList();
    TreeSet localTreeSet = new TreeSet();
    int i = 0;
    if (i < arrayOfString1.length)
    {
      String str1 = arrayOfString1[i];
      if (!str1.startsWith("-"))
        localArrayList1.add(str1);
      while (true)
      {
        i++;
        break;
        if (str1.equals("--"))
          while (true)
          {
            i++;
            if (i >= arrayOfString1.length)
              break;
            localArrayList1.add(arrayOfString1[i]);
          }
        if (printRequestedHelp(str1))
        {
          List localList2 = getUnknownFlags(localArrayList2, localTreeSet);
          if (!localList2.isEmpty())
            outputStream.println("Unrecognized flags: " + localList2);
          if (localList2.isEmpty());
          for (int n = 0; ; n = 1)
          {
            exitUnlessDisabled(n);
            return EMPTY_STRING_ARRAY;
          }
        }
        Matcher localMatcher = FLAG_PATTERN.matcher(str1);
        if (!localMatcher.matches())
        {
          InvalidFlagSyntaxException localInvalidFlagSyntaxException1 = new InvalidFlagSyntaxException(str1);
          throw localInvalidFlagSyntaxException1;
        }
        String str2 = localMatcher.group(1);
        if (str2.trim().equals(""))
        {
          InvalidFlagSyntaxException localInvalidFlagSyntaxException2 = new InvalidFlagSyntaxException(str1);
          throw localInvalidFlagSyntaxException2;
        }
        String str3 = localMatcher.group(2);
        FlagDescription localFlagDescription = getBestFlag(str2);
        int j = 0;
        if (str3 == null)
          if (isBooleanFlag(localFlagDescription))
          {
            str3 = String.valueOf(localFlagDescription.isPositiveFormOfName(str2));
            str2 = localFlagDescription.getLongFlagName();
          }
        while (true)
          if (isAllowedUndefOkFlag(str2))
          {
            String[] arrayOfString2 = str3.split(",");
            int k = arrayOfString2.length;
            for (int m = 0; m < k; m++)
              localTreeSet.add(arrayOfString2[m]);
            break;
            if ((isLegitimateFlag(localFlagDescription)) || (isAllowedUndefOkFlag(str2)))
            {
              if (i == -1 + arrayOfString1.length)
                throw new InvalidFlagSyntaxException(str1 + " is missing a value");
              i++;
              str3 = arrayOfString1[i];
              j = 1;
            }
            else
            {
              localArrayList2.add(str2);
              break;
              boolean bool1 = isBooleanFlag(localFlagDescription);
              j = 0;
              if (bool1)
              {
                boolean bool2 = localFlagDescription.isPositiveFormOfName(str2);
                j = 0;
                if (!bool2)
                {
                  InvalidFlagSyntaxException localInvalidFlagSyntaxException3 = new InvalidFlagSyntaxException(str1);
                  throw localInvalidFlagSyntaxException3;
                }
              }
            }
          }
        try
        {
          processFlag(str2, str3);
        }
        catch (UnrecognizedFlagException localUnrecognizedFlagException2)
        {
          localArrayList2.add(str2);
        }
        if (j != 0)
          i--;
      }
    }
    List localList1 = getUnknownFlags(localArrayList2, localTreeSet);
    if (localList1.size() > 0)
    {
      UnrecognizedFlagException localUnrecognizedFlagException1 = new UnrecognizedFlagException(localList1);
      throw localUnrecognizedFlagException1;
    }
    return (String[])localArrayList1.toArray(EMPTY_STRING_ARRAY);
  }

  private static boolean printRequestedHelp(String paramString)
  {
    if (HELP_PATTERN.matcher(paramString).matches())
    {
      usage(outputStream, false, usagePrefix);
      return true;
    }
    if (HELPSHORT_PATTERN.matcher(paramString).matches())
    {
      usage(outputStream, true, usagePrefix);
      return true;
    }
    if (XML_HELP_PATTERN.matcher(paramString).matches())
    {
      xmlUsage(new PrintWriter(outputStream));
      return true;
    }
    return false;
  }

  public static void processFlag(String paramString1, String paramString2)
    throws UnrecognizedFlagException, InvalidFlagValueException, AmbiguousFlagException
  {
    FlagDescription localFlagDescription = getBestFlag(paramString1);
    if (localFlagDescription == null)
    {
      Set localSet = (Set)expandedFlagMap().get(paramString1);
      if ((localSet == null) || (localSet.size() == 0))
        throw new UnrecognizedFlagException(paramString1);
      throw new AmbiguousFlagException(paramString1, localSet);
    }
    try
    {
      flag(localFlagDescription).setFromString(paramString2);
      return;
    }
    catch (NullPointerException localNullPointerException)
    {
      InvalidFlagValueException localInvalidFlagValueException2 = new InvalidFlagValueException("NullPointerException");
      localInvalidFlagValueException2.flagName = localFlagDescription.getLongFlagName();
      localInvalidFlagValueException2.flagValue = "null";
      throw localInvalidFlagValueException2;
    }
    catch (InvalidFlagValueException localInvalidFlagValueException1)
    {
      localInvalidFlagValueException1.flagName = localFlagDescription.getLongFlagName();
      localInvalidFlagValueException1.flagValue = paramString2;
      throw localInvalidFlagValueException1;
    }
    catch (IllegalFlagStateException localIllegalFlagStateException)
    {
      localIllegalFlagStateException.flagName = localFlagDescription.getLongFlagName();
      throw localIllegalFlagStateException;
    }
  }

  static void reallyClearFlagMapsForTesting()
  {
    parseState = ParseState.NOT_STARTED;
    FlagMapHolder.longNameMap.clear();
    FlagMapHolder.canonicalFlagMap = null;
    FlagMapHolder.expandedFlagMap = null;
    FlagMapHolder.manuallyRegisteredFlags.clear();
    FlagMapHolder.whitelistedPrefixes = null;
    disableUndefOkSupport();
  }

  public static void registerCompletionHook(Runnable paramRunnable)
  {
    if (paramRunnable == null)
      throw new NullPointerException();
    try
    {
      if (parseState == ParseState.DONE);
      for (int i = 1; ; i = 0)
      {
        completionHooks.add(paramRunnable);
        if (i != 0)
          paramRunnable.run();
        return;
      }
    }
    finally
    {
    }
  }

  public static void registerFlag(String paramString1, String paramString2, String paramString3, String paramString4, DocLevel paramDocLevel, String paramString5, Flag<?> paramFlag)
  {
    FlagDescription localFlagDescription;
    try
    {
      localFlagDescription = FlagDescription.createManuallyRegisteredFlag(paramString2, paramString1).doc(paramString3).altName(paramString4).docLevel(paramDocLevel).type(paramString5).build();
      if (paramFlag == null)
        throw new NullPointerException();
    }
    finally
    {
    }
    if ((paramString4 != null) && (!FLAG_NAME_PATTERN.matcher(paramString4).matches()))
      throw new IllegalArgumentException("Alternate flag name must contain only alphanumeric characters, hyphens, and underscores.");
    Map localMap = longNameMap();
    if ((localMap.containsKey(localFlagDescription.getLongFlagName())) && (!stateCheckingDisabled()))
      throw new IllegalStateException("Duplicate flag " + localFlagDescription.getLongFlagName());
    localMap.put(localFlagDescription.getLongFlagName(), localFlagDescription);
    manuallyRegisteredFlags().put(localFlagDescription.getLongFlagName(), paramFlag);
    FlagMapHolder.canonicalFlagMap = null;
    FlagMapHolder.expandedFlagMap = null;
  }

  public static void resetAllFlagsForTest()
  {
    Iterator localIterator = longNameMap().values().iterator();
    while (localIterator.hasNext())
    {
      FlagDescription localFlagDescription = (FlagDescription)localIterator.next();
      try
      {
        flag(localFlagDescription).resetForTest();
      }
      catch (LinkageError localLinkageError)
      {
      }
    }
    setParseState(ParseState.NOT_STARTED);
  }

  public static void resetFlagForTest(Class<?> paramClass, String paramString)
  {
    verifyAndResetFlag(paramClass.getName() + "." + paramString.replaceFirst("^FLAG_", ""));
    setParseState(ParseState.NOT_STARTED);
  }

  static void resetOutputStreamForTesting()
  {
    outputStream = System.out;
  }

  @VisibleForTesting
  public static void resetPreferredClasses()
  {
    preferredClasses.clear();
  }

  public static void resetSomeFlagsForTest(String[] paramArrayOfString)
  {
    int i = paramArrayOfString.length;
    for (int j = 0; j < i; j++)
      verifyAndResetFlag(paramArrayOfString[j]);
    setParseState(ParseState.NOT_STARTED);
  }

  public static void setAllowedFlags(Collection<String> paramCollection)
  {
    if (paramCollection == null);
    for (FlagMapHolder.whitelistedPrefixes = null; ; FlagMapHolder.whitelistedPrefixes = new LinkedHashSet(paramCollection))
    {
      FlagMapHolder.expandedFlagMap = null;
      FlagMapHolder.canonicalFlagMap = null;
      return;
    }
  }

  public static void setFlagClassLoader(ClassLoader paramClassLoader)
  {
    flagClassLoader = paramClassLoader;
  }

  static void setOutputStreamForTesting(PrintStream paramPrintStream)
  {
    outputStream = paramPrintStream;
  }

  private static void setParseState(ParseState paramParseState)
  {
    try
    {
      if ((paramParseState == ParseState.IN_PROGRESS) && (parseState != ParseState.NOT_STARTED) && (!stateCheckingDisabled()))
        throw new IllegalStateException("Cannot call parse more than once.  Here is the stacktrace of the previous call:\n" + getStackTraceAsString(parseStackTrace));
    }
    finally
    {
    }
    parseState = paramParseState;
    if (paramParseState == ParseState.DONE);
    for (parseStackTrace = new Throwable("The stack trace associated with the PREVIOUS call to parse"); ; parseStackTrace = null)
      return;
  }

  public static void setUsagePrefix(String paramString)
  {
    usagePrefix = paramString;
  }

  private static String simpleType(String paramString)
  {
    if ("java.lang.Integer".equals(paramString))
      paramString = "int";
    do
    {
      return paramString;
      if ("java.lang.Long".equals(paramString))
        return "long";
      if ("java.lang.Float".equals(paramString))
        return "float";
      if ("java.lang.Double".equals(paramString))
        return "double";
      if ("java.lang.String".equals(paramString))
        return "string";
    }
    while (!"java.lang.Boolean".equals(paramString));
    return "boolean";
  }

  static Set<Map.Entry<String, FlagDescription>> sortFlags(boolean paramBoolean)
  {
    String str = getMainClassName();
    TreeSet localTreeSet = new TreeSet(new Comparator()
    {
      public int compare(Map.Entry<String, FlagDescription> paramAnonymousEntry1, Map.Entry<String, FlagDescription> paramAnonymousEntry2)
      {
        FlagDescription localFlagDescription1 = (FlagDescription)paramAnonymousEntry1.getValue();
        FlagDescription localFlagDescription2 = (FlagDescription)paramAnonymousEntry2.getValue();
        int i = localFlagDescription1.getContainerClassName().compareTo(localFlagDescription2.getContainerClassName());
        if (i == 0)
          i = localFlagDescription1.getShortFlagName().compareTo(localFlagDescription2.getShortFlagName());
        return i;
      }
    });
    Iterator localIterator = canonicalFlagMap().entrySet().iterator();
    while (localIterator.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      FlagDescription localFlagDescription = (FlagDescription)localEntry.getValue();
      if ((whitelistAllowsAliasing(localFlagDescription)) && ((!paramBoolean) || (str == null) || (localFlagDescription.getContainerClassName().equals(str))))
        localTreeSet.add(localEntry);
    }
    return localTreeSet;
  }

  public static boolean stateCheckingDisabled()
  {
    return Boolean.getBoolean("com.google.common.flags.disableStateChecking");
  }

  public static void usage(PrintStream paramPrintStream)
  {
    usage(paramPrintStream, false, usagePrefix);
  }

  public static void usage(PrintStream paramPrintStream, String paramString)
  {
    usage(paramPrintStream, false, paramString);
  }

  public static void usage(PrintStream paramPrintStream, boolean paramBoolean, String paramString)
  {
    if (paramString == null)
    {
      Object[] arrayOfObject = new Object[1];
      arrayOfObject[0] = getProgramName();
      paramString = String.format("Usage: java [jvm-flags...] %s [flags...] [args...]", arrayOfObject);
    }
    initMaps();
    paramPrintStream.println(paramString);
    paramPrintStream.println("where flags are");
    paramPrintStream.println(" Standard flags:");
    formatFlag(paramPrintStream, "help", "describes all flags", null);
    formatFlag(paramPrintStream, "helpshort", "describes the main class' flags", null);
    formatFlag(paramPrintStream, "helpxml", "emits XML description of all flags", null);
    String str1 = "";
    Iterator localIterator = sortFlags(paramBoolean).iterator();
    while (localIterator.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      String str2 = (String)localEntry.getKey();
      FlagDescription localFlagDescription = (FlagDescription)localEntry.getValue();
      if (localFlagDescription.getDocLevel() == DocLevel.PUBLIC)
      {
        if (!localFlagDescription.getContainerClassName().equals(str1))
        {
          paramPrintStream.println();
          paramPrintStream.println("  Flags for " + localFlagDescription.getContainerClassName() + ":");
          str1 = localFlagDescription.getContainerClassName();
        }
        formatFlag(paramPrintStream, str2, localFlagDescription.getDoc(), getDefaultAsString(flag(localFlagDescription)));
      }
    }
  }

  private static String valueToString(Object paramObject)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    if ((paramObject instanceof List))
    {
      List localList = (List)paramObject;
      int i = 1;
      Iterator localIterator = localList.iterator();
      while (localIterator.hasNext())
      {
        Object localObject = localIterator.next();
        if (i == 0)
          localStringBuilder.append(",");
        localStringBuilder.append(localObject);
        i = 0;
      }
    }
    if ((paramObject instanceof Class))
      localStringBuilder.append(((Class)paramObject).getName());
    while (true)
    {
      return localStringBuilder.toString();
      localStringBuilder.append(paramObject);
    }
  }

  private static void verifyAndResetFlag(String paramString)
  {
    FlagDescription localFlagDescription = (FlagDescription)longNameMap().get(paramString);
    if (localFlagDescription == null)
      throw new IllegalArgumentException("Invalid flag name: " + paramString);
    try
    {
      flag(localFlagDescription).resetForTest();
      return;
    }
    catch (LinkageError localLinkageError)
    {
    }
  }

  private static boolean whitelistAllowsAliasing(FlagDescription paramFlagDescription)
  {
    if (paramFlagDescription == null)
      throw new NullPointerException();
    if (FlagMapHolder.whitelistedPrefixes == null)
      return true;
    Iterator localIterator = FlagMapHolder.whitelistedPrefixes.iterator();
    while (true)
      if (localIterator.hasNext())
      {
        String str = (String)localIterator.next();
        if ((str.equals(paramFlagDescription.getLongFlagName())) || ((paramFlagDescription.isField()) && (str.equals(paramFlagDescription.getFullyQualifiedFieldName()))))
          break;
        if ((str.endsWith(".")) && (paramFlagDescription.getLongFlagName().startsWith(str)))
          return true;
      }
    return false;
  }

  public static void xmlUsage(PrintWriter paramPrintWriter)
  {
    paramPrintWriter.print("<?xml version=\"1.0\" encoding=\"UTF-8\"?><AllFlags>");
    paramPrintWriter.print(XmlSupport.toXmlElement("program", getProgramName()));
    paramPrintWriter.print(XmlSupport.toXmlElement("usage", getProgramName()));
    Iterator localIterator = new TreeMap(canonicalFlagMap()).entrySet().iterator();
    while (localIterator.hasNext())
    {
      Map.Entry localEntry = (Map.Entry)localIterator.next();
      String str1 = (String)localEntry.getKey();
      FlagDescription localFlagDescription = (FlagDescription)localEntry.getValue();
      if ((whitelistAllowsAliasing(localFlagDescription)) && (localFlagDescription.getDocLevel() == DocLevel.PUBLIC))
      {
        Flag localFlag = flag(localFlagDescription);
        paramPrintWriter.print("<flag>");
        paramPrintWriter.print(XmlSupport.toXmlElement("file", localFlagDescription.getContainerClassName()));
        paramPrintWriter.print(XmlSupport.toXmlElement("name", "--" + str1));
        if (localFlagDescription.getAltName() != null)
          paramPrintWriter.print(XmlSupport.toXmlElement("shortname", "--" + localFlagDescription.getShortFlagName()));
        paramPrintWriter.print(XmlSupport.toXmlElement("meaning", localFlagDescription.getDoc()));
        String str2;
        if (localFlag.getDefault() == null)
        {
          str2 = "null";
          label242: paramPrintWriter.print(XmlSupport.toXmlElement("default", str2));
          if (localFlag.get() != null)
            break label317;
        }
        label317: for (String str3 = "null"; ; str3 = getAsString(localFlag))
        {
          paramPrintWriter.print(XmlSupport.toXmlElement("current", str3));
          paramPrintWriter.print(XmlSupport.toXmlElement("type", simpleType(localFlagDescription.getType())));
          paramPrintWriter.print("</flag>");
          break;
          str2 = getDefaultAsString(localFlag);
          break label242;
        }
      }
    }
    paramPrintWriter.print("</AllFlags>");
    paramPrintWriter.flush();
  }

  @VisibleForTesting
  static class FlagInfoImpl
    implements FlagInfo, Comparable<FlagInfoImpl>
  {
    final FlagDescription desc;
    final List<String> names;

    FlagInfoImpl(List<String> paramList, FlagDescription paramFlagDescription)
    {
      this.names = paramList;
      this.desc = paramFlagDescription;
    }

    public boolean accessed()
    {
      return Flags.flag(this.desc).accessed;
    }

    public int compareTo(FlagInfoImpl paramFlagInfoImpl)
    {
      return this.desc.compareTo(paramFlagInfoImpl.desc);
    }

    public String containerClass()
    {
      return this.desc.getContainerClassName();
    }

    public Object defaultValue()
    {
      return Flags.flag(this.desc).defaultValue;
    }

    public String doc()
    {
      return this.desc.getDoc();
    }

    public boolean equals(Object paramObject)
    {
      if (this == paramObject)
        return true;
      if (!(paramObject instanceof FlagInfoImpl))
        return false;
      FlagInfoImpl localFlagInfoImpl = (FlagInfoImpl)paramObject;
      return this.desc.equals(localFlagInfoImpl.desc);
    }

    public int hashCode()
    {
      return this.desc.hashCode();
    }

    public List<String> names()
    {
      return Collections.unmodifiableList(this.names);
    }

    public String parsableStringValue()
    {
      return Flags.flag(this.desc).parsableStringValue();
    }

    public String toString()
    {
      return "[FlagInfo names=" + this.names + " desc=" + this.desc + "]";
    }

    public String type()
    {
      return this.desc.getType();
    }

    public Object value()
    {
      return Flags.flag(this.desc).value;
    }
  }

  private static class FlagMapHolder
  {
    static Map<String, FlagDescription> canonicalFlagMap;
    static Map<String, Set<FlagDescription>> expandedFlagMap;
    static Map<String, FlagDescription> longNameMap = Flags.access$000();
    static final Map<String, Flag<?>> manuallyRegisteredFlags = new HashMap();
    static Collection<String> whitelistedPrefixes = null;
  }

  private static enum ParseState
  {
    static
    {
      IN_PROGRESS = new ParseState("IN_PROGRESS", 1);
      DONE = new ParseState("DONE", 2);
      ParseState[] arrayOfParseState = new ParseState[3];
      arrayOfParseState[0] = NOT_STARTED;
      arrayOfParseState[1] = IN_PROGRESS;
      arrayOfParseState[2] = DONE;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.common.flags.Flags
 * JD-Core Version:    0.6.2
 */