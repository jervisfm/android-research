package com.google.common.flags;

public class IllegalFlagStateException extends IllegalStateException
{
  private static final long serialVersionUID = 98459854098L;
  String flagName = "";

  public String getMessage()
  {
    return "Attempt to set flag " + this.flagName + " after it was read";
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.common.flags.IllegalFlagStateException
 * JD-Core Version:    0.6.2
 */