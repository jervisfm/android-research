package com.google.common.flags;

class InvalidFlagSyntaxException extends FlagException
{
  private static final long serialVersionUID = 93487431290532L;

  InvalidFlagSyntaxException(String paramString)
  {
    super("Invalid flag syntax: " + paramString);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.common.flags.InvalidFlagSyntaxException
 * JD-Core Version:    0.6.2
 */