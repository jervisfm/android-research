package com.google.common.flags;

public class InvalidFlagValueException extends FlagException
{
  private static final long serialVersionUID = 2398478978L;
  String flagName;
  String flagValue;

  public InvalidFlagValueException(String paramString)
  {
    super(paramString);
  }

  public InvalidFlagValueException(String paramString1, String paramString2, Flag<?> paramFlag)
  {
    super(paramString1);
    this.flagName = paramString2;
    this.flagValue = paramFlag.parsableStringValue();
  }

  public InvalidFlagValueException(String paramString, Throwable paramThrowable)
  {
    super(paramString, paramThrowable);
  }

  public String getMessage()
  {
    String str = super.getMessage();
    if (this.flagName != null)
      str = "Invalid value for flag " + this.flagName + ": " + this.flagValue + " (" + str + ")";
    return str;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.common.flags.InvalidFlagValueException
 * JD-Core Version:    0.6.2
 */