package com.google.common.flags;

public class InvalidFlagsException extends IllegalArgumentException
{
  private static final long serialVersionUID = 98239333L;

  public InvalidFlagsException(String paramString)
  {
    super(paramString);
  }

  public InvalidFlagsException(String paramString, Throwable paramThrowable)
  {
    super(paramString, paramThrowable);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.common.flags.InvalidFlagsException
 * JD-Core Version:    0.6.2
 */