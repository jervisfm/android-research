package com.google.common.flags;

class MalformedFlagDescriptionException extends RuntimeException
{
  private static final long serialVersionUID = 189458902386L;

  public MalformedFlagDescriptionException(String paramString)
  {
    super(paramString);
  }

  public MalformedFlagDescriptionException(String paramString, Throwable paramThrowable)
  {
    super(paramString, paramThrowable);
  }

  public MalformedFlagDescriptionException(Throwable paramThrowable)
  {
    super(paramThrowable);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.common.flags.MalformedFlagDescriptionException
 * JD-Core Version:    0.6.2
 */