package com.google.common.flags;

import java.util.List;

public class UnrecognizedFlagException extends FlagException
{
  private static final long serialVersionUID = 43980234589L;

  UnrecognizedFlagException(String paramString)
  {
    super("Unrecognized flag: " + paramString);
  }

  UnrecognizedFlagException(List<String> paramList)
  {
    super("Unrecognized flags: " + paramList.toString());
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.common.flags.UnrecognizedFlagException
 * JD-Core Version:    0.6.2
 */