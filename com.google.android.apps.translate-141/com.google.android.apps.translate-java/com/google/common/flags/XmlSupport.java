package com.google.common.flags;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.xml.sax.Attributes;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

class XmlSupport
{
  static final Pattern TYPENAME_RE = Pattern.compile("com.google.common.flags.Flag<(.*)>");
  private static XMLReader xmlParser;

  static Set<FlagDescription> discoverFlags(String paramString)
  {
    HashSet localHashSet = new HashSet();
    try
    {
      Class localClass = Class.forName(paramString);
      for (Field localField : localClass.getDeclaredFields())
        if (localField.isAnnotationPresent(FlagSpec.class))
        {
          FlagSpec localFlagSpec = (FlagSpec)localField.getAnnotation(FlagSpec.class);
          String str = localField.getGenericType().toString();
          Matcher localMatcher = TYPENAME_RE.matcher(str);
          if (localMatcher.matches())
            str = localMatcher.group(1);
          localHashSet.add(FlagDescription.createFlagFromField(localField.getDeclaringClass().getName() + "." + localField.getName()).shortFlagName(localFlagSpec.name()).doc(localFlagSpec.help()).altName(localFlagSpec.altName()).docLevel(localFlagSpec.docLevel()).type(str).build());
        }
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
    }
    return localHashSet;
  }

  // ERROR //
  static void fromXml(java.io.InputStream paramInputStream, Map<String, FlagDescription> paramMap)
  {
    // Byte code:
    //   0: aload_1
    //   1: ifnonnull +11 -> 12
    //   4: new 150	java/lang/NullPointerException
    //   7: dup
    //   8: invokespecial 151	java/lang/NullPointerException:<init>	()V
    //   11: athrow
    //   12: new 153	com/google/common/flags/XmlSupport$FlagDescriptionHandler
    //   15: dup
    //   16: aload_1
    //   17: invokespecial 156	com/google/common/flags/XmlSupport$FlagDescriptionHandler:<init>	(Ljava/util/Map;)V
    //   20: astore_2
    //   21: ldc 2
    //   23: monitorenter
    //   24: getstatic 158	com/google/common/flags/XmlSupport:xmlParser	Lorg/xml/sax/XMLReader;
    //   27: astore 4
    //   29: aload 4
    //   31: ifnonnull +9 -> 40
    //   34: invokestatic 164	org/xml/sax/helpers/XMLReaderFactory:createXMLReader	()Lorg/xml/sax/XMLReader;
    //   37: putstatic 158	com/google/common/flags/XmlSupport:xmlParser	Lorg/xml/sax/XMLReader;
    //   40: getstatic 158	com/google/common/flags/XmlSupport:xmlParser	Lorg/xml/sax/XMLReader;
    //   43: aload_2
    //   44: invokeinterface 170 2 0
    //   49: getstatic 158	com/google/common/flags/XmlSupport:xmlParser	Lorg/xml/sax/XMLReader;
    //   52: aload_2
    //   53: invokeinterface 174 2 0
    //   58: getstatic 158	com/google/common/flags/XmlSupport:xmlParser	Lorg/xml/sax/XMLReader;
    //   61: new 176	org/xml/sax/InputSource
    //   64: dup
    //   65: aload_0
    //   66: invokespecial 179	org/xml/sax/InputSource:<init>	(Ljava/io/InputStream;)V
    //   69: invokeinterface 183 2 0
    //   74: getstatic 158	com/google/common/flags/XmlSupport:xmlParser	Lorg/xml/sax/XMLReader;
    //   77: aconst_null
    //   78: invokeinterface 170 2 0
    //   83: getstatic 158	com/google/common/flags/XmlSupport:xmlParser	Lorg/xml/sax/XMLReader;
    //   86: aconst_null
    //   87: invokeinterface 174 2 0
    //   92: ldc 2
    //   94: monitorexit
    //   95: return
    //   96: astore 8
    //   98: new 185	java/lang/AssertionError
    //   101: dup
    //   102: aload 8
    //   104: invokespecial 188	java/lang/AssertionError:<init>	(Ljava/lang/Object;)V
    //   107: athrow
    //   108: astore_3
    //   109: ldc 2
    //   111: monitorexit
    //   112: aload_3
    //   113: athrow
    //   114: astore 7
    //   116: new 190	com/google/common/flags/MalformedFlagDescriptionException
    //   119: dup
    //   120: aload 7
    //   122: invokespecial 193	com/google/common/flags/MalformedFlagDescriptionException:<init>	(Ljava/lang/Throwable;)V
    //   125: athrow
    //   126: astore 6
    //   128: getstatic 158	com/google/common/flags/XmlSupport:xmlParser	Lorg/xml/sax/XMLReader;
    //   131: aconst_null
    //   132: invokeinterface 170 2 0
    //   137: getstatic 158	com/google/common/flags/XmlSupport:xmlParser	Lorg/xml/sax/XMLReader;
    //   140: aconst_null
    //   141: invokeinterface 174 2 0
    //   146: aload 6
    //   148: athrow
    //   149: astore 5
    //   151: new 185	java/lang/AssertionError
    //   154: dup
    //   155: aload 5
    //   157: invokespecial 188	java/lang/AssertionError:<init>	(Ljava/lang/Object;)V
    //   160: athrow
    //
    // Exception table:
    //   from	to	target	type
    //   34	40	96	org/xml/sax/SAXException
    //   24	29	108	finally
    //   34	40	108	finally
    //   40	58	108	finally
    //   74	95	108	finally
    //   98	108	108	finally
    //   109	112	108	finally
    //   128	149	108	finally
    //   58	74	114	org/xml/sax/SAXException
    //   58	74	126	finally
    //   116	126	126	finally
    //   151	161	126	finally
    //   58	74	149	java/io/IOException
  }

  static String serializeOneFlag(FlagDescription paramFlagDescription)
  {
    if (paramFlagDescription == null)
      throw new NullPointerException();
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("<flag>");
    localStringBuilder.append(toXmlElement("name", paramFlagDescription.getFullyQualifiedFieldName()));
    if (paramFlagDescription.isShortFlagNameSpecified())
      localStringBuilder.append(toXmlElement("shortname", paramFlagDescription.getShortFlagName()));
    localStringBuilder.append(toXmlElement("doc", paramFlagDescription.getDoc()));
    localStringBuilder.append(toXmlElement("doclevel", paramFlagDescription.getDocLevel().toString()));
    if (paramFlagDescription.getAltName() != null)
      localStringBuilder.append(toXmlElement("altname", paramFlagDescription.getAltName()));
    localStringBuilder.append(toXmlElement("type", paramFlagDescription.getType()));
    localStringBuilder.append("</flag>\n");
    return localStringBuilder.toString();
  }

  static String toXml(Collection<FlagDescription> paramCollection)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("<flags>");
    Iterator localIterator = paramCollection.iterator();
    while (localIterator.hasNext())
      localStringBuilder.append(serializeOneFlag((FlagDescription)localIterator.next()));
    localStringBuilder.append("</flags>");
    return localStringBuilder.toString();
  }

  static String toXmlElement(String paramString1, String paramString2)
  {
    return "<" + paramString1 + ">" + xmlEscape(paramString2) + "</" + paramString1 + ">";
  }

  static String xmlEscape(String paramString)
  {
    return paramString.replaceAll("&", "&amp;").replaceAll("<", "&lt;").replaceAll(">", "&gt;").replaceAll("\"", "&quot;");
  }

  private static String xmlUnescape(String paramString)
  {
    return paramString.replaceAll("&quot;", "\"").replaceAll("&gt;", ">").replaceAll("&lt;", "<").replaceAll("&amp;", "&");
  }

  private static class FlagDescriptionHandler extends DefaultHandler
  {
    private List<String> flagContainers = new ArrayList();
    private Map<String, FlagDescription> flagDescriptions;
    private Map<String, String> flagFields = new HashMap();
    private final String[] flagNodeChildren = { "name", "shortname", "doc", "doclevel", "altname", "type" };
    private StringBuilder nodeData = new StringBuilder();
    private XmlSupport.ParseState parseState = XmlSupport.ParseState.FLAGS;

    public FlagDescriptionHandler(Map<String, FlagDescription> paramMap)
    {
      this.flagDescriptions = paramMap;
      clearFlagFields();
    }

    private void clearFlagFields()
    {
      for (String str : this.flagNodeChildren)
        this.flagFields.put(str, null);
    }

    private String currentFlagName()
    {
      String str = (String)this.flagFields.get("name");
      if (str == null)
        str = "<unknown>";
      return str;
    }

    private FlagDescription makeFlagDescription()
    {
      String str1 = (String)this.flagFields.get("name");
      if (str1 == null)
        throw new MalformedFlagDescriptionException("name missing");
      String str2 = (String)this.flagFields.get("doclevel");
      if (str2 == null)
        throw new MalformedFlagDescriptionException("doclevel missing for flag " + str1);
      DocLevel localDocLevel;
      String str3;
      try
      {
        localDocLevel = (DocLevel)Enum.valueOf(DocLevel.class, str2);
        str3 = (String)this.flagFields.get("doc");
        if (str3 == null)
          throw new MalformedFlagDescriptionException("doc missing for flag " + str1);
      }
      catch (IllegalArgumentException localIllegalArgumentException)
      {
        throw new MalformedFlagDescriptionException("Illegal doclevel for flag " + str1 + ": " + str2);
      }
      String str4 = (String)this.flagFields.get("type");
      if (str4 == null)
        throw new MalformedFlagDescriptionException("type missing for flag " + str1);
      return FlagDescription.createFlagFromField(str1).shortFlagName((String)this.flagFields.get("shortname")).doc(str3).altName((String)this.flagFields.get("altname")).docLevel(localDocLevel).type(str4).build();
    }

    public void characters(char[] paramArrayOfChar, int paramInt1, int paramInt2)
    {
      if (this.parseState == XmlSupport.ParseState.FLAGS)
        return;
      this.nodeData.append(paramArrayOfChar, paramInt1, paramInt2);
    }

    public void endDocument()
    {
      Iterator localIterator1 = this.flagContainers.iterator();
      while (localIterator1.hasNext())
      {
        Iterator localIterator2 = XmlSupport.discoverFlags((String)localIterator1.next()).iterator();
        while (localIterator2.hasNext())
        {
          FlagDescription localFlagDescription = (FlagDescription)localIterator2.next();
          if (!this.flagDescriptions.containsKey(localFlagDescription.getLongFlagName()))
            this.flagDescriptions.put(localFlagDescription.getLongFlagName(), localFlagDescription);
        }
      }
    }

    public void endElement(String paramString1, String paramString2, String paramString3)
    {
      switch (XmlSupport.1.$SwitchMap$com$google$common$flags$XmlSupport$ParseState[this.parseState.ordinal()])
      {
      default:
        throw new MalformedFlagDescriptionException("Unhandled element: " + paramString3);
      case 2:
        FlagDescription localFlagDescription = makeFlagDescription();
        if (!this.flagDescriptions.containsKey(localFlagDescription.getLongFlagName()))
          this.flagDescriptions.put(localFlagDescription.getLongFlagName(), localFlagDescription);
        clearFlagFields();
        this.parseState = XmlSupport.ParseState.FLAGS;
      case 1:
      case 4:
      case 3:
      }
      while (true)
      {
        this.nodeData.setLength(0);
        return;
        if (!this.flagFields.containsKey(paramString3))
          throw new MalformedFlagDescriptionException("Unknown element encountered while parsing flag named " + currentFlagName() + ": " + paramString3);
        this.flagFields.put(paramString3, XmlSupport.xmlUnescape(this.nodeData.toString().trim()));
        this.parseState = XmlSupport.ParseState.FLAG;
        continue;
        this.flagContainers.add(XmlSupport.xmlUnescape(this.nodeData.toString().trim()));
        this.parseState = XmlSupport.ParseState.FLAGS;
      }
    }

    public void startElement(String paramString1, String paramString2, String paramString3, Attributes paramAttributes)
    {
      switch (XmlSupport.1.$SwitchMap$com$google$common$flags$XmlSupport$ParseState[this.parseState.ordinal()])
      {
      default:
        throw new MalformedFlagDescriptionException("Unexpected parse state");
      case 1:
        if ("flag".equals(paramString3))
          this.parseState = XmlSupport.ParseState.FLAG;
        while (!"flagcontainer".equals(paramString3))
          return;
        this.parseState = XmlSupport.ParseState.FLAG_CONTAINER;
        return;
      case 2:
      }
      if (this.flagFields.get(paramString3) != null)
        throw new MalformedFlagDescriptionException("Duplicate element encountered while parsing flag " + currentFlagName() + ": " + paramString3);
      this.parseState = XmlSupport.ParseState.FLAG_CHILD;
    }
  }

  private static class FlagElementName
  {
    private static final String ALTNAME = "altname";
    private static final String DOC = "doc";
    private static final String DOCLEVEL = "doclevel";
    private static final String NAME = "name";
    private static final String SHORTNAME = "shortname";
    private static final String TYPE = "type";
  }

  private static enum ParseState
  {
    static
    {
      FLAG = new ParseState("FLAG", 1);
      FLAG_CHILD = new ParseState("FLAG_CHILD", 2);
      FLAG_CONTAINER = new ParseState("FLAG_CONTAINER", 3);
      ParseState[] arrayOfParseState = new ParseState[4];
      arrayOfParseState[0] = FLAGS;
      arrayOfParseState[1] = FLAG;
      arrayOfParseState[2] = FLAG_CHILD;
      arrayOfParseState[3] = FLAG_CONTAINER;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.common.flags.XmlSupport
 * JD-Core Version:    0.6.2
 */