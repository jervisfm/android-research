package com.google.common.logging;

import java.util.Properties;
import java.util.logging.Handler;

public abstract interface ConfigurableHandler
{
  public abstract Handler configure(String paramString, Properties paramProperties);
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.common.logging.ConfigurableHandler
 * JD-Core Version:    0.6.2
 */