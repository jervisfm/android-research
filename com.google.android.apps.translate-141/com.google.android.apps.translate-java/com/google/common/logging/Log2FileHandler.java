package com.google.common.logging;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Properties;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

@Deprecated
public final class Log2FileHandler extends Handler
  implements ConfigurableHandler
{
  private static String BACKUP_LOG_DIR = "/export/hda3/tmp";
  static final String BASENAME_PROP = "baseName";
  private static final String DEFAULT_BASE_FILE_NAME = "/export/hda3/tmp/Log2Trace";
  private static final String DEFAULT_FILENAME_TS_FORMAT_STR = "-yyyy_MM_dd_HH_mm_ss";
  private static final String DEFAULT_RECORD_TS_FORMAT_STR = "yyMMdd HH:mm:ss ";
  private static final long DEFAULT_ROTATION_UNIT_SIZE_MB = 1800L;
  static final String EXTENSION_PROP = "extension";
  static final String FILENAME_TS_FORMAT_PROP = "fileNameTsFormat";
  static final String FORMATTER_PROP = "formatter";
  static final String LIMIT_PROP = "limit";
  static final String LINKNAME_PROP = "linkName";
  private static String LOG_DIR_ENV = "GOOGLE_LOG_DIR";
  private static String LOG_DIR_PROP = "google.logDir";
  private static final long MB = 1048576L;
  static final String QUALIFY_BASENAME_PROP = "qualifyBaseName";
  static final String RECORD_TS_FORMAT_PROP = "recordTsFormat";
  private String baseFileName;
  private String extension;
  private DateFormat fileNameTsFormat;
  private String formatterClass;
  private String linkName;
  private Writer logFileWriter;
  private boolean qualifyBaseFileName;
  private DateFormat recordTsFormat;
  private long rotateSize;

  public Log2FileHandler()
  {
    this("/export/hda3/tmp/Log2Trace", null, null, new SimpleDateFormat("yyMMdd HH:mm:ss "), new SimpleDateFormat("-yyyy_MM_dd_HH_mm_ss"), getRotationUnitSize());
  }

  public Log2FileHandler(String paramString)
  {
    this(paramString, paramString, null, new SimpleDateFormat("yyMMdd HH:mm:ss "), new SimpleDateFormat("-yyyy_MM_dd_HH_mm_ss"), getRotationUnitSize());
  }

  public Log2FileHandler(String paramString1, String paramString2, String paramString3, DateFormat paramDateFormat1, DateFormat paramDateFormat2, long paramLong)
  {
    this.baseFileName = paramString1;
    this.qualifyBaseFileName = true;
    this.linkName = paramString2;
    this.extension = paramString3;
    this.recordTsFormat = paramDateFormat1;
    this.fileNameTsFormat = paramDateFormat2;
    this.rotateSize = paramLong;
    this.logFileWriter = null;
    this.formatterClass = "com.google.common.logging.Log2Formatter";
    setFormatter(new Log2Formatter(paramDateFormat1));
  }

  private static boolean directoryExists(String paramString)
  {
    File localFile = new File(paramString);
    return (localFile.exists()) && (localFile.isDirectory());
  }

  private static String getLoggingDirectory()
  {
    String str = System.getenv(LOG_DIR_ENV);
    if ((str != null) && (directoryExists(str)));
    do
    {
      return str;
      str = System.getProperty(LOG_DIR_PROP);
    }
    while (((str != null) && (directoryExists(str))) || (directoryExists(BACKUP_LOG_DIR)));
    return getTempDirectory();
  }

  private static long getRotationUnitSize()
  {
    String str = System.getenv("GOOGLE_MAX_LOG_MB");
    if (str != null)
      try
      {
        long l = Long.parseLong(str);
        if (l > 0L)
          return 1048576L * l;
      }
      catch (NumberFormatException localNumberFormatException)
      {
      }
    return 1887436800L;
  }

  private static String getTempDirectory()
  {
    String str = System.getProperty("java.io.tmpdir");
    if (str == null)
      str = System.getProperty("user.home");
    return str;
  }

  private void openWriter()
  {
    try
    {
      String str1 = this.baseFileName;
      String str2 = this.linkName;
      if ((this.qualifyBaseFileName) && (!new File(this.baseFileName).isAbsolute()))
        str1 = getLoggingDirectory() + File.separator + this.baseFileName;
      if ((this.qualifyBaseFileName) && (!new File(this.linkName).isAbsolute()))
        str2 = getLoggingDirectory() + File.separator + this.linkName;
      RotatingLogStream localRotatingLogStream = new RotatingLogStream(str1, str2, this.extension, this.fileNameTsFormat);
      localRotatingLogStream.setRotateSize(this.rotateSize);
      this.logFileWriter = new BufferedWriter(new OutputStreamWriter(localRotatingLogStream));
      return;
    }
    catch (IOException localIOException)
    {
      localIOException.printStackTrace();
    }
  }

  private String qualifyPropName(String paramString1, String paramString2)
  {
    return paramString1 + "." + paramString2;
  }

  public void close()
  {
    if (this.logFileWriter != null);
    try
    {
      this.logFileWriter.close();
      this.logFileWriter = null;
      return;
    }
    catch (IOException localIOException)
    {
      System.err.println("Log2FileHandler#flush : error in closing log!\n Exception thrown: " + localIOException.getMessage());
      this.logFileWriter = null;
    }
  }

  public Handler configure(String paramString, Properties paramProperties)
  {
    close();
    this.baseFileName = paramProperties.getProperty(qualifyPropName(paramString, "baseName"), "/export/hda3/tmp/Log2Trace");
    this.qualifyBaseFileName = Boolean.valueOf(paramProperties.getProperty(qualifyPropName(paramString, "qualifyBaseName"), "true")).booleanValue();
    this.rotateSize = Long.parseLong(paramProperties.getProperty(qualifyPropName(paramString, "limit"), "0"));
    if (this.rotateSize <= 0L)
      this.rotateSize = getRotationUnitSize();
    this.recordTsFormat = new SimpleDateFormat(paramProperties.getProperty(qualifyPropName(paramString, "recordTsFormat"), "yyMMdd HH:mm:ss "));
    this.fileNameTsFormat = new SimpleDateFormat(paramProperties.getProperty(qualifyPropName(paramString, "fileNameTsFormat"), "-yyyy_MM_dd_HH_mm_ss"));
    this.linkName = paramProperties.getProperty(qualifyPropName(paramString, "linkName"), null);
    this.extension = paramProperties.getProperty(qualifyPropName(paramString, "extension"), null);
    this.formatterClass = paramProperties.getProperty(qualifyPropName(paramString, "formatter"), "");
    if (!this.formatterClass.equals(""))
      try
      {
        Log2Formatter localLog2Formatter;
        if (this.formatterClass.equals(Log2Formatter.class.getName()))
          localLog2Formatter = new Log2Formatter(this.recordTsFormat);
        for (localObject = localLog2Formatter; ; localObject = (Formatter)Class.forName(this.formatterClass).newInstance())
        {
          if (localObject != null)
            setFormatter((Formatter)localObject);
          return this;
        }
      }
      catch (ClassNotFoundException localClassNotFoundException)
      {
        while (true)
        {
          System.err.println("Error loading formatter class specified by: " + qualifyPropName(paramString, "formatter") + "value: " + this.formatterClass);
          localObject = null;
        }
      }
      catch (InstantiationException localInstantiationException)
      {
        while (true)
        {
          System.err.println("Error instantiating formatter class specified by: " + qualifyPropName(paramString, "formatter") + "value: " + this.formatterClass);
          localObject = null;
        }
      }
      catch (IllegalAccessException localIllegalAccessException)
      {
        while (true)
        {
          System.err.println("Error instantiating formatter class specified by: " + qualifyPropName(paramString, "formatter") + "value: " + this.formatterClass);
          Object localObject = null;
        }
      }
    setFormatter(new Log2Formatter(this.recordTsFormat));
    return this;
  }

  public void flush()
  {
    if (this.logFileWriter != null);
    try
    {
      this.logFileWriter.flush();
      return;
    }
    catch (IOException localIOException)
    {
      System.err.println("Log2FileHandler#flush : error in flushing log!\n Exception thrown: " + localIOException.getMessage());
    }
  }

  public void publish(LogRecord paramLogRecord)
  {
    String str = getFormatter().format(paramLogRecord);
    if (this.logFileWriter == null)
      openWriter();
    try
    {
      this.logFileWriter.write(str);
      this.logFileWriter.flush();
      return;
    }
    catch (IOException localIOException)
    {
      System.err.println("Log2FileHandler#publish : error in writing to log!\n Exception thrown: " + localIOException.getMessage() + "\nlog entry: " + str);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.common.logging.Log2FileHandler
 * JD-Core Version:    0.6.2
 */