package com.google.common.logging;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.StringTokenizer;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.LogRecord;

public final class Log2Formatter extends Formatter
{
  private DateFormat dateFormatter;

  public Log2Formatter()
  {
    this.dateFormatter = new SimpleDateFormat("yyMMdd HH:mm:ss.SSS");
  }

  public Log2Formatter(DateFormat paramDateFormat)
  {
    this.dateFormatter = paramDateFormat;
  }

  private char getLevelPrefix(int paramInt)
  {
    if (paramInt >= Level.WARNING.intValue())
      return 'X';
    if (paramInt <= Level.FINE.intValue())
      return 'D';
    return 'I';
  }

  public String format(LogRecord paramLogRecord)
  {
    String str1 = LogContext.getThreadTag();
    if (str1 == null);
    StringBuilder localStringBuilder;
    for (String str2 = ""; ; str2 = str1 + " ")
    {
      localStringBuilder = new StringBuilder();
      String str3 = this.dateFormatter.format(new Date(paramLogRecord.getMillis()));
      char c = getLevelPrefix(paramLogRecord.getLevel().intValue());
      StringTokenizer localStringTokenizer = new StringTokenizer(formatMessage(paramLogRecord), "\n");
      while (localStringTokenizer.hasMoreTokens())
      {
        localStringBuilder.append(str3);
        localStringBuilder.append(':');
        localStringBuilder.append(c);
        localStringBuilder.append(' ');
        localStringBuilder.append(str2);
        localStringBuilder.append(localStringTokenizer.nextToken());
        localStringBuilder.append("\n");
        c = ' ';
      }
    }
    Throwable localThrowable = paramLogRecord.getThrown();
    if (localThrowable != null)
    {
      StringWriter localStringWriter = new StringWriter();
      PrintWriter localPrintWriter = new PrintWriter(localStringWriter);
      localThrowable.printStackTrace(localPrintWriter);
      localPrintWriter.flush();
      localStringBuilder.append(localStringWriter.toString());
    }
    return localStringBuilder.toString();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.common.logging.Log2Formatter
 * JD-Core Version:    0.6.2
 */