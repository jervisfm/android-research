package com.google.common.logging;

import javax.annotation.Nullable;

public final class LogContext
{
  private static ThreadLocal<String> threadTagMap = new ThreadLocal();

  @Nullable
  public static String getThreadTag()
  {
    return (String)threadTagMap.get();
  }

  public static void setThreadTag(@Nullable String paramString)
  {
    threadTagMap.set(paramString);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.common.logging.LogContext
 * JD-Core Version:    0.6.2
 */