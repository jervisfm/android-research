package com.google.common.logging;

@Deprecated
public abstract interface Logger
{
  public static final int DEBUG_LVL = 0;
  public static final int ERROR_LVL = 2;
  public static final int EVENT_LVL = 1;
  public static final int SILENT_LVL = 3;

  public abstract void close();

  public abstract String getThreadTag();

  public abstract int getThreshold();

  public abstract void logDebug(String paramString);

  public abstract void logError(String paramString);

  public abstract void logEvent(String paramString);

  public abstract void logException(Throwable paramThrowable);

  public abstract void logException(Throwable paramThrowable, String paramString);

  public abstract void logSevereException(Throwable paramThrowable);

  public abstract void logSevereException(Throwable paramThrowable, String paramString);

  public abstract void logTimedEvent(String paramString, long paramLong1, long paramLong2);

  public abstract void setErrorEmail(String paramString);

  public abstract void setThreadTag(String paramString);

  public abstract void setThreshold(int paramInt);
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.common.logging.Logger
 * JD-Core Version:    0.6.2
 */