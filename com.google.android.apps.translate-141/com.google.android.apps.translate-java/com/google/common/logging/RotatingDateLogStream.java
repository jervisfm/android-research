package com.google.common.logging;

import java.io.IOException;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;

public class RotatingDateLogStream extends RotatingLogStream
{
  private Calendar currentDate_;
  private Calendar lastDate_;

  public RotatingDateLogStream(String paramString1, String paramString2, String paramString3, DateFormat paramDateFormat)
    throws IOException
  {
    super(paramString1, paramString2, paramString3, paramDateFormat);
  }

  public RotatingDateLogStream(String paramString1, String paramString2, DateFormat paramDateFormat)
    throws IOException
  {
    super(paramString1, paramString2, paramDateFormat);
  }

  private void setCurrentDate(Date paramDate)
  {
    if (this.currentDate_ == null)
    {
      this.currentDate_ = Calendar.getInstance();
      this.currentDate_.setTimeZone(this.dateFormat_.getTimeZone());
    }
    this.currentDate_.setTime(paramDate);
  }

  protected void checkRotate(int paramInt)
    throws IOException
  {
    setCurrentDate(new Date());
    if (this.currentDate_.get(6) != this.lastDate_.get(6))
      rotate();
    super.checkRotate(paramInt);
  }

  protected String getNextFilename()
  {
    setLastDate(new Date());
    return super.getNextFilename();
  }

  protected void setLastDate(Date paramDate)
  {
    if (this.lastDate_ == null)
    {
      this.lastDate_ = Calendar.getInstance();
      this.lastDate_.setTimeZone(this.dateFormat_.getTimeZone());
    }
    this.lastDate_.setTime(paramDate);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.common.logging.RotatingDateLogStream
 * JD-Core Version:    0.6.2
 */