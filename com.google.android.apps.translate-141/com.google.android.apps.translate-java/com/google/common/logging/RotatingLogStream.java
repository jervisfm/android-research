package com.google.common.logging;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.flags.Flag;
import com.google.common.flags.FlagSpec;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.annotation.Nullable;

public class RotatingLogStream extends OutputStream
{
  public static final SimpleDateFormat kDefaultDateFormat = new SimpleDateFormat("-yyyy_MM_dd_HH_mm_ss");
  public static final long kDefaultRotateSize = 1887436800L;

  @FlagSpec(help="If enabled, then rotating logs will not create symbolic links from the base filename to the current log.  This is provided to avoid the Runtime.exec() call in RotatingLogStream that can cause OutOfMemoryError due to fork() temporarily doubling memory footprint.")
  private static final Flag<Boolean> skipLogSymLinkCreation = Flag.value(false);
  private static volatile SymbolicLinkProvider symlinker = new SymbolicLinkProvider()
  {
    public void createSymbolicLink(String paramAnonymousString1, String paramAnonymousString2)
      throws IOException
    {
      Runtime.getRuntime().exec(new String[] { "/bin/ln", "-sf", paramAnonymousString1, paramAnonymousString2 });
    }
  };
  private final String basename_;
  private long currentCombinedLogSize_;
  private FileDescriptor currentFileDescriptor_;
  private File currentFile_;
  private long currentSize_;
  protected final DateFormat dateFormat_;
  private final String extension_;
  private ArrayList<File> files_;
  private final String linkname_;
  private final List<Listener> listeners_ = Collections.synchronizedList(new ArrayList());
  private long maxCombinedLogSize_;
  private OutputStream output_;
  private long rotateSize_;

  public RotatingLogStream(String paramString)
    throws IOException
  {
    this(paramString, paramString, kDefaultDateFormat);
  }

  public RotatingLogStream(String paramString1, @Nullable String paramString2, @Nullable String paramString3, @Nullable DateFormat paramDateFormat)
    throws IOException
  {
  }

  RotatingLogStream(String paramString1, @Nullable String paramString2, @Nullable String paramString3, @Nullable DateFormat paramDateFormat, boolean paramBoolean)
    throws IOException
  {
    this.dateFormat_ = paramDateFormat;
    this.basename_ = paramString1;
    if (paramBoolean);
    while (true)
    {
      this.linkname_ = paramString2;
      this.extension_ = paramString3;
      this.rotateSize_ = 1887436800L;
      openNewFile();
      return;
      paramString2 = null;
    }
  }

  public RotatingLogStream(String paramString1, @Nullable String paramString2, @Nullable DateFormat paramDateFormat)
    throws IOException
  {
    this(paramString1, paramString2, null, paramDateFormat);
  }

  private void deleteOldestLogFiles(long paramLong)
  {
    while (true)
    {
      if ((paramLong < this.maxCombinedLogSize_) || (this.files_.size() <= 1))
        return;
      int i = 0;
      long l1 = ((File)this.files_.get(0)).lastModified();
      for (int j = 0; j < this.files_.size(); j++)
      {
        long l3 = ((File)this.files_.get(j)).lastModified();
        if (l3 < l1)
        {
          l1 = l3;
          i = j;
        }
      }
      File localFile = (File)this.files_.remove(i);
      long l2 = localFile.length();
      this.currentCombinedLogSize_ -= l2;
      paramLong -= l2;
      localFile.delete();
    }
  }

  private ArrayList<File> findAllFilesWithBasenameAndExtension(String paramString1, @Nullable final String paramString2)
  {
    String str1 = ".";
    int i = paramString1.lastIndexOf(File.separatorChar);
    if (i != -1)
    {
      str1 = paramString1.substring(0, i);
      paramString1 = paramString1.substring(i + 1);
    }
    final String str2 = paramString1;
    if (this.linkname_ != null);
    ArrayList localArrayList;
    for (final String str3 = new File(this.linkname_).getName(); ; str3 = null)
    {
      File[] arrayOfFile = new File(str1).listFiles(new FilenameFilter()
      {
        public boolean accept(File paramAnonymousFile, String paramAnonymousString)
        {
          if (paramAnonymousString.equals(str3));
          do
          {
            do
              return false;
            while (!paramAnonymousString.startsWith(str2));
            if (paramString2 == null)
              break;
          }
          while (!paramAnonymousString.endsWith(paramString2));
          return true;
          return true;
        }
      });
      localArrayList = new ArrayList();
      int j = arrayOfFile.length;
      for (int k = 0; k < j; k++)
        localArrayList.add(arrayOfFile[k]);
    }
    return localArrayList;
  }

  private void openNewFile()
    throws IOException
  {
    String str1 = getNextFilename();
    File localFile1 = new File(str1);
    if (localFile1.equals(this.currentFile_));
    while (true)
    {
      return;
      this.currentFile_ = localFile1;
      if (this.files_ != null)
        this.files_.add(this.currentFile_);
      String str2 = this.currentFile_.getAbsolutePath();
      this.currentSize_ = this.currentFile_.length();
      this.currentCombinedLogSize_ += this.currentSize_;
      if ((this.rotateSize_ != 0L) && (this.currentSize_ >= this.rotateSize_))
        System.err.println("Current log files size >= rotate size for " + str2);
      File localFile2 = this.currentFile_.getParentFile();
      if (localFile2 != null)
        localFile2.mkdirs();
      FileOutputStream localFileOutputStream = new FileOutputStream(str1, true);
      this.currentFileDescriptor_ = localFileOutputStream.getFD();
      if (this.output_ != null)
        this.output_.close();
      this.output_ = wrapFileOutputStream(localFileOutputStream);
      if (this.linkname_ != null)
      {
        String str3 = str1;
        int i = this.linkname_.lastIndexOf('/');
        if ((i != -1) && (str3.startsWith(this.linkname_.substring(0, i + 1))))
          str3 = str3.substring(i + 1);
        symlinker.createSymbolicLink(str3, this.linkname_);
      }
      Iterator localIterator = this.listeners_.iterator();
      while (localIterator.hasNext())
        ((Listener)localIterator.next()).logFileCreated(this.output_);
    }
  }

  public static void setSymbolicLinkProvider(SymbolicLinkProvider paramSymbolicLinkProvider)
  {
    symlinker = paramSymbolicLinkProvider;
  }

  public void addListener(Listener paramListener)
  {
    this.listeners_.add(paramListener);
  }

  void checkRotate(int paramInt)
    throws IOException
  {
    if (this.files_ == null)
      if ((this.currentSize_ + paramInt >= this.rotateSize_) && (new File(getNextFilename()).length() + paramInt < this.rotateSize_))
        rotate();
    while (true)
    {
      this.currentSize_ += paramInt;
      this.currentCombinedLogSize_ += paramInt;
      return;
      if (this.currentSize_ + paramInt >= this.rotateSize_)
        rotate();
      deleteOldestLogFiles(this.currentCombinedLogSize_ + paramInt);
    }
  }

  public void close()
    throws IOException
  {
    try
    {
      this.output_.close();
      this.output_ = null;
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public void flush()
    throws IOException
  {
    try
    {
      this.output_.flush();
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  @VisibleForTesting
  long getCurrentCombinedLogSize()
  {
    return this.currentCombinedLogSize_;
  }

  public File getCurrentFile()
  {
    return this.currentFile_;
  }

  public FileDescriptor getCurrentFileDescriptor()
  {
    return this.currentFileDescriptor_;
  }

  @VisibleForTesting
  long getCurrentLogSize()
  {
    return this.currentSize_;
  }

  String getNextFilename()
  {
    if (this.extension_ == null)
      return this.basename_ + this.dateFormat_.format(new Date());
    return this.basename_ + this.dateFormat_.format(new Date()) + this.extension_;
  }

  public void rotate()
    throws IOException
  {
    try
    {
      flush();
      openNewFile();
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public void setMaxCombinedLogSize(long paramLong)
  {
    try
    {
      if (paramLong < this.rotateSize_)
        throw new IllegalArgumentException("Max combined log size should be >= single log rotate size");
    }
    finally
    {
    }
    this.maxCombinedLogSize_ = paramLong;
    if (this.files_ == null)
    {
      this.files_ = findAllFilesWithBasenameAndExtension(this.basename_, this.extension_);
      long l = 0L;
      Iterator localIterator = this.files_.iterator();
      while (localIterator.hasNext())
        l += ((File)localIterator.next()).length();
      this.currentCombinedLogSize_ = l;
    }
  }

  public void setRotateSize(long paramLong)
  {
    try
    {
      this.rotateSize_ = paramLong;
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  protected OutputStream wrapFileOutputStream(FileOutputStream paramFileOutputStream)
    throws IOException
  {
    return new BufferedOutputStream(paramFileOutputStream);
  }

  public void write(int paramInt)
    throws IOException
  {
    try
    {
      checkRotate(1);
      this.output_.write(paramInt);
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public void write(byte[] paramArrayOfByte)
    throws IOException
  {
    try
    {
      checkRotate(paramArrayOfByte.length);
      this.output_.write(paramArrayOfByte);
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public void write(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
    throws IOException
  {
    try
    {
      checkRotate(paramInt2);
      this.output_.write(paramArrayOfByte, paramInt1, paramInt2);
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public static abstract interface Listener
  {
    public abstract void logFileCreated(OutputStream paramOutputStream);
  }

  public static abstract interface SymbolicLinkProvider
  {
    public abstract void createSymbolicLink(String paramString1, String paramString2)
      throws IOException;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.common.logging.RotatingLogStream
 * JD-Core Version:    0.6.2
 */