package com.google.protobuf;

public abstract interface MessageLiteOrBuilder
{
  public abstract MessageLite getDefaultInstanceForType();

  public abstract boolean isInitialized();
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.protobuf.MessageLiteOrBuilder
 * JD-Core Version:    0.6.2
 */