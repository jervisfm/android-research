package com.google.protos.aksara;

import com.google.protobuf.AbstractParser;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.GeneratedMessageLite.Builder;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectStreamException;

public final class BoundingBoxP
{
  public static void registerAllExtensions(ExtensionRegistryLite paramExtensionRegistryLite)
  {
  }

  public static final class BoundingBox extends GeneratedMessageLite
    implements BoundingBoxP.BoundingBoxOrBuilder
  {
    public static final int HEIGHT_FIELD_NUMBER = 4;
    public static final int LEFT_FIELD_NUMBER = 1;
    public static Parser<BoundingBox> PARSER = new AbstractParser()
    {
      public BoundingBoxP.BoundingBox parsePartialFrom(CodedInputStream paramAnonymousCodedInputStream, ExtensionRegistryLite paramAnonymousExtensionRegistryLite)
        throws InvalidProtocolBufferException
      {
        return new BoundingBoxP.BoundingBox(paramAnonymousCodedInputStream, paramAnonymousExtensionRegistryLite, null);
      }
    };
    public static final int TOP_FIELD_NUMBER = 2;
    public static final int WIDTH_FIELD_NUMBER = 3;
    private static final BoundingBox defaultInstance = new BoundingBox(true);
    private static final long serialVersionUID;
    private int bitField0_;
    private int height_;
    private int left_;
    private byte memoizedIsInitialized = -1;
    private int memoizedSerializedSize = -1;
    private int top_;
    private int width_;

    static
    {
      defaultInstance.initFields();
    }

    // ERROR //
    private BoundingBox(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      // Byte code:
      //   0: aload_0
      //   1: invokespecial 53	com/google/protobuf/GeneratedMessageLite:<init>	()V
      //   4: aload_0
      //   5: iconst_m1
      //   6: putfield 55	com/google/protos/aksara/BoundingBoxP$BoundingBox:memoizedIsInitialized	B
      //   9: aload_0
      //   10: iconst_m1
      //   11: putfield 57	com/google/protos/aksara/BoundingBoxP$BoundingBox:memoizedSerializedSize	I
      //   14: aload_0
      //   15: invokespecial 47	com/google/protos/aksara/BoundingBoxP$BoundingBox:initFields	()V
      //   18: iconst_0
      //   19: istore_3
      //   20: iload_3
      //   21: ifne +201 -> 222
      //   24: aload_1
      //   25: invokevirtual 63	com/google/protobuf/CodedInputStream:readTag	()I
      //   28: istore 7
      //   30: iload 7
      //   32: lookupswitch	default:+52->84, 0:+195->227, 8:+68->100, 16:+107->139, 24:+147->179, 32:+168->200
      //   85: aload_1
      //   86: aload_2
      //   87: iload 7
      //   89: invokevirtual 67	com/google/protos/aksara/BoundingBoxP$BoundingBox:parseUnknownField	(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z
      //   92: ifne -72 -> 20
      //   95: iconst_1
      //   96: istore_3
      //   97: goto -77 -> 20
      //   100: aload_0
      //   101: iconst_1
      //   102: aload_0
      //   103: getfield 69	com/google/protos/aksara/BoundingBoxP$BoundingBox:bitField0_	I
      //   106: ior
      //   107: putfield 69	com/google/protos/aksara/BoundingBoxP$BoundingBox:bitField0_	I
      //   110: aload_0
      //   111: aload_1
      //   112: invokevirtual 72	com/google/protobuf/CodedInputStream:readInt32	()I
      //   115: putfield 74	com/google/protos/aksara/BoundingBoxP$BoundingBox:left_	I
      //   118: goto -98 -> 20
      //   121: astore 6
      //   123: aload 6
      //   125: aload_0
      //   126: invokevirtual 78	com/google/protobuf/InvalidProtocolBufferException:setUnfinishedMessage	(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/InvalidProtocolBufferException;
      //   129: athrow
      //   130: astore 5
      //   132: aload_0
      //   133: invokevirtual 81	com/google/protos/aksara/BoundingBoxP$BoundingBox:makeExtensionsImmutable	()V
      //   136: aload 5
      //   138: athrow
      //   139: aload_0
      //   140: iconst_2
      //   141: aload_0
      //   142: getfield 69	com/google/protos/aksara/BoundingBoxP$BoundingBox:bitField0_	I
      //   145: ior
      //   146: putfield 69	com/google/protos/aksara/BoundingBoxP$BoundingBox:bitField0_	I
      //   149: aload_0
      //   150: aload_1
      //   151: invokevirtual 72	com/google/protobuf/CodedInputStream:readInt32	()I
      //   154: putfield 83	com/google/protos/aksara/BoundingBoxP$BoundingBox:top_	I
      //   157: goto -137 -> 20
      //   160: astore 4
      //   162: new 50	com/google/protobuf/InvalidProtocolBufferException
      //   165: dup
      //   166: aload 4
      //   168: invokevirtual 87	java/io/IOException:getMessage	()Ljava/lang/String;
      //   171: invokespecial 90	com/google/protobuf/InvalidProtocolBufferException:<init>	(Ljava/lang/String;)V
      //   174: aload_0
      //   175: invokevirtual 78	com/google/protobuf/InvalidProtocolBufferException:setUnfinishedMessage	(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/InvalidProtocolBufferException;
      //   178: athrow
      //   179: aload_0
      //   180: iconst_4
      //   181: aload_0
      //   182: getfield 69	com/google/protos/aksara/BoundingBoxP$BoundingBox:bitField0_	I
      //   185: ior
      //   186: putfield 69	com/google/protos/aksara/BoundingBoxP$BoundingBox:bitField0_	I
      //   189: aload_0
      //   190: aload_1
      //   191: invokevirtual 72	com/google/protobuf/CodedInputStream:readInt32	()I
      //   194: putfield 92	com/google/protos/aksara/BoundingBoxP$BoundingBox:width_	I
      //   197: goto -177 -> 20
      //   200: aload_0
      //   201: bipush 8
      //   203: aload_0
      //   204: getfield 69	com/google/protos/aksara/BoundingBoxP$BoundingBox:bitField0_	I
      //   207: ior
      //   208: putfield 69	com/google/protos/aksara/BoundingBoxP$BoundingBox:bitField0_	I
      //   211: aload_0
      //   212: aload_1
      //   213: invokevirtual 72	com/google/protobuf/CodedInputStream:readInt32	()I
      //   216: putfield 94	com/google/protos/aksara/BoundingBoxP$BoundingBox:height_	I
      //   219: goto -199 -> 20
      //   222: aload_0
      //   223: invokevirtual 81	com/google/protos/aksara/BoundingBoxP$BoundingBox:makeExtensionsImmutable	()V
      //   226: return
      //   227: iconst_1
      //   228: istore_3
      //   229: goto -209 -> 20
      //
      // Exception table:
      //   from	to	target	type
      //   24	30	121	com/google/protobuf/InvalidProtocolBufferException
      //   84	95	121	com/google/protobuf/InvalidProtocolBufferException
      //   100	118	121	com/google/protobuf/InvalidProtocolBufferException
      //   139	157	121	com/google/protobuf/InvalidProtocolBufferException
      //   179	197	121	com/google/protobuf/InvalidProtocolBufferException
      //   200	219	121	com/google/protobuf/InvalidProtocolBufferException
      //   24	30	130	finally
      //   84	95	130	finally
      //   100	118	130	finally
      //   123	130	130	finally
      //   139	157	130	finally
      //   162	179	130	finally
      //   179	197	130	finally
      //   200	219	130	finally
      //   24	30	160	java/io/IOException
      //   84	95	160	java/io/IOException
      //   100	118	160	java/io/IOException
      //   139	157	160	java/io/IOException
      //   179	197	160	java/io/IOException
      //   200	219	160	java/io/IOException
    }

    private BoundingBox(GeneratedMessageLite.Builder paramBuilder)
    {
      super();
    }

    private BoundingBox(boolean paramBoolean)
    {
    }

    public static BoundingBox getDefaultInstance()
    {
      return defaultInstance;
    }

    private void initFields()
    {
      this.left_ = -1;
      this.top_ = -1;
      this.width_ = -1;
      this.height_ = -1;
    }

    public static Builder newBuilder()
    {
      return Builder.access$100();
    }

    public static Builder newBuilder(BoundingBox paramBoundingBox)
    {
      return newBuilder().mergeFrom(paramBoundingBox);
    }

    public static BoundingBox parseDelimitedFrom(InputStream paramInputStream)
      throws IOException
    {
      return (BoundingBox)PARSER.parseDelimitedFrom(paramInputStream);
    }

    public static BoundingBox parseDelimitedFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return (BoundingBox)PARSER.parseDelimitedFrom(paramInputStream, paramExtensionRegistryLite);
    }

    public static BoundingBox parseFrom(ByteString paramByteString)
      throws InvalidProtocolBufferException
    {
      return (BoundingBox)PARSER.parseFrom(paramByteString);
    }

    public static BoundingBox parseFrom(ByteString paramByteString, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return (BoundingBox)PARSER.parseFrom(paramByteString, paramExtensionRegistryLite);
    }

    public static BoundingBox parseFrom(CodedInputStream paramCodedInputStream)
      throws IOException
    {
      return (BoundingBox)PARSER.parseFrom(paramCodedInputStream);
    }

    public static BoundingBox parseFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return (BoundingBox)PARSER.parseFrom(paramCodedInputStream, paramExtensionRegistryLite);
    }

    public static BoundingBox parseFrom(InputStream paramInputStream)
      throws IOException
    {
      return (BoundingBox)PARSER.parseFrom(paramInputStream);
    }

    public static BoundingBox parseFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return (BoundingBox)PARSER.parseFrom(paramInputStream, paramExtensionRegistryLite);
    }

    public static BoundingBox parseFrom(byte[] paramArrayOfByte)
      throws InvalidProtocolBufferException
    {
      return (BoundingBox)PARSER.parseFrom(paramArrayOfByte);
    }

    public static BoundingBox parseFrom(byte[] paramArrayOfByte, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return (BoundingBox)PARSER.parseFrom(paramArrayOfByte, paramExtensionRegistryLite);
    }

    public BoundingBox getDefaultInstanceForType()
    {
      return defaultInstance;
    }

    public int getHeight()
    {
      return this.height_;
    }

    public int getLeft()
    {
      return this.left_;
    }

    public Parser<BoundingBox> getParserForType()
    {
      return PARSER;
    }

    public int getSerializedSize()
    {
      int i = this.memoizedSerializedSize;
      if (i != -1)
        return i;
      int j = 0x1 & this.bitField0_;
      int k = 0;
      if (j == 1)
        k = 0 + CodedOutputStream.computeInt32Size(1, this.left_);
      if ((0x2 & this.bitField0_) == 2)
        k += CodedOutputStream.computeInt32Size(2, this.top_);
      if ((0x4 & this.bitField0_) == 4)
        k += CodedOutputStream.computeInt32Size(3, this.width_);
      if ((0x8 & this.bitField0_) == 8)
        k += CodedOutputStream.computeInt32Size(4, this.height_);
      this.memoizedSerializedSize = k;
      return k;
    }

    public int getTop()
    {
      return this.top_;
    }

    public int getWidth()
    {
      return this.width_;
    }

    public boolean hasHeight()
    {
      return (0x8 & this.bitField0_) == 8;
    }

    public boolean hasLeft()
    {
      return (0x1 & this.bitField0_) == 1;
    }

    public boolean hasTop()
    {
      return (0x2 & this.bitField0_) == 2;
    }

    public boolean hasWidth()
    {
      return (0x4 & this.bitField0_) == 4;
    }

    public final boolean isInitialized()
    {
      int i = this.memoizedIsInitialized;
      if (i != -1)
        return i == 1;
      this.memoizedIsInitialized = 1;
      return true;
    }

    public Builder newBuilderForType()
    {
      return newBuilder();
    }

    public Builder toBuilder()
    {
      return newBuilder(this);
    }

    protected Object writeReplace()
      throws ObjectStreamException
    {
      return super.writeReplace();
    }

    public void writeTo(CodedOutputStream paramCodedOutputStream)
      throws IOException
    {
      getSerializedSize();
      if ((0x1 & this.bitField0_) == 1)
        paramCodedOutputStream.writeInt32(1, this.left_);
      if ((0x2 & this.bitField0_) == 2)
        paramCodedOutputStream.writeInt32(2, this.top_);
      if ((0x4 & this.bitField0_) == 4)
        paramCodedOutputStream.writeInt32(3, this.width_);
      if ((0x8 & this.bitField0_) == 8)
        paramCodedOutputStream.writeInt32(4, this.height_);
    }

    public static final class Builder extends GeneratedMessageLite.Builder<BoundingBoxP.BoundingBox, Builder>
      implements BoundingBoxP.BoundingBoxOrBuilder
    {
      private int bitField0_;
      private int height_ = -1;
      private int left_ = -1;
      private int top_ = -1;
      private int width_ = -1;

      private Builder()
      {
        maybeForceBuilderInitialization();
      }

      private static Builder create()
      {
        return new Builder();
      }

      private void maybeForceBuilderInitialization()
      {
      }

      public BoundingBoxP.BoundingBox build()
      {
        BoundingBoxP.BoundingBox localBoundingBox = buildPartial();
        if (!localBoundingBox.isInitialized())
          throw newUninitializedMessageException(localBoundingBox);
        return localBoundingBox;
      }

      public BoundingBoxP.BoundingBox buildPartial()
      {
        BoundingBoxP.BoundingBox localBoundingBox = new BoundingBoxP.BoundingBox(this, null);
        int i = this.bitField0_;
        int j = i & 0x1;
        int k = 0;
        if (j == 1)
          k = 0x0 | 0x1;
        BoundingBoxP.BoundingBox.access$302(localBoundingBox, this.left_);
        if ((i & 0x2) == 2)
          k |= 2;
        BoundingBoxP.BoundingBox.access$402(localBoundingBox, this.top_);
        if ((i & 0x4) == 4)
          k |= 4;
        BoundingBoxP.BoundingBox.access$502(localBoundingBox, this.width_);
        if ((i & 0x8) == 8)
          k |= 8;
        BoundingBoxP.BoundingBox.access$602(localBoundingBox, this.height_);
        BoundingBoxP.BoundingBox.access$702(localBoundingBox, k);
        return localBoundingBox;
      }

      public Builder clear()
      {
        super.clear();
        this.left_ = -1;
        this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
        this.top_ = -1;
        this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
        this.width_ = -1;
        this.bitField0_ = (0xFFFFFFFB & this.bitField0_);
        this.height_ = -1;
        this.bitField0_ = (0xFFFFFFF7 & this.bitField0_);
        return this;
      }

      public Builder clearHeight()
      {
        this.bitField0_ = (0xFFFFFFF7 & this.bitField0_);
        this.height_ = -1;
        return this;
      }

      public Builder clearLeft()
      {
        this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
        this.left_ = -1;
        return this;
      }

      public Builder clearTop()
      {
        this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
        this.top_ = -1;
        return this;
      }

      public Builder clearWidth()
      {
        this.bitField0_ = (0xFFFFFFFB & this.bitField0_);
        this.width_ = -1;
        return this;
      }

      public Builder clone()
      {
        return create().mergeFrom(buildPartial());
      }

      public BoundingBoxP.BoundingBox getDefaultInstanceForType()
      {
        return BoundingBoxP.BoundingBox.getDefaultInstance();
      }

      public int getHeight()
      {
        return this.height_;
      }

      public int getLeft()
      {
        return this.left_;
      }

      public int getTop()
      {
        return this.top_;
      }

      public int getWidth()
      {
        return this.width_;
      }

      public boolean hasHeight()
      {
        return (0x8 & this.bitField0_) == 8;
      }

      public boolean hasLeft()
      {
        return (0x1 & this.bitField0_) == 1;
      }

      public boolean hasTop()
      {
        return (0x2 & this.bitField0_) == 2;
      }

      public boolean hasWidth()
      {
        return (0x4 & this.bitField0_) == 4;
      }

      public final boolean isInitialized()
      {
        return true;
      }

      public Builder mergeFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
        throws IOException
      {
        BoundingBoxP.BoundingBox localBoundingBox1 = null;
        try
        {
          BoundingBoxP.BoundingBox localBoundingBox2 = (BoundingBoxP.BoundingBox)BoundingBoxP.BoundingBox.PARSER.parsePartialFrom(paramCodedInputStream, paramExtensionRegistryLite);
          return this;
        }
        catch (InvalidProtocolBufferException localInvalidProtocolBufferException)
        {
          localBoundingBox1 = (BoundingBoxP.BoundingBox)localInvalidProtocolBufferException.getUnfinishedMessage();
          throw localInvalidProtocolBufferException;
        }
        finally
        {
          if (localBoundingBox1 != null)
            mergeFrom(localBoundingBox1);
        }
      }

      public Builder mergeFrom(BoundingBoxP.BoundingBox paramBoundingBox)
      {
        if (paramBoundingBox == BoundingBoxP.BoundingBox.getDefaultInstance());
        do
        {
          return this;
          if (paramBoundingBox.hasLeft())
            setLeft(paramBoundingBox.getLeft());
          if (paramBoundingBox.hasTop())
            setTop(paramBoundingBox.getTop());
          if (paramBoundingBox.hasWidth())
            setWidth(paramBoundingBox.getWidth());
        }
        while (!paramBoundingBox.hasHeight());
        setHeight(paramBoundingBox.getHeight());
        return this;
      }

      public Builder setHeight(int paramInt)
      {
        this.bitField0_ = (0x8 | this.bitField0_);
        this.height_ = paramInt;
        return this;
      }

      public Builder setLeft(int paramInt)
      {
        this.bitField0_ = (0x1 | this.bitField0_);
        this.left_ = paramInt;
        return this;
      }

      public Builder setTop(int paramInt)
      {
        this.bitField0_ = (0x2 | this.bitField0_);
        this.top_ = paramInt;
        return this;
      }

      public Builder setWidth(int paramInt)
      {
        this.bitField0_ = (0x4 | this.bitField0_);
        this.width_ = paramInt;
        return this;
      }
    }
  }

  public static abstract interface BoundingBoxOrBuilder extends MessageLiteOrBuilder
  {
    public abstract int getHeight();

    public abstract int getLeft();

    public abstract int getTop();

    public abstract int getWidth();

    public abstract boolean hasHeight();

    public abstract boolean hasLeft();

    public abstract boolean hasTop();

    public abstract boolean hasWidth();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.protos.aksara.BoundingBoxP
 * JD-Core Version:    0.6.2
 */