package com.google.protos.aksara.lattice;

import com.google.protobuf.AbstractParser;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.GeneratedMessageLite.Builder;
import com.google.protobuf.GeneratedMessageLite.ExtendableBuilder;
import com.google.protobuf.GeneratedMessageLite.ExtendableMessage;
import com.google.protobuf.GeneratedMessageLite.ExtendableMessage.ExtensionWriter;
import com.google.protobuf.GeneratedMessageLite.ExtendableMessageOrBuilder;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.LazyStringArrayList;
import com.google.protobuf.LazyStringList;
import com.google.protobuf.MessageLite;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import com.google.protobuf.UnmodifiableLazyStringList;
import com.google.protos.aksara.BoundingBoxP.BoundingBox;
import com.google.protos.aksara.BoundingBoxP.BoundingBox.Builder;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectStreamException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class LatticeP
{
  public static void registerAllExtensions(ExtensionRegistryLite paramExtensionRegistryLite)
  {
  }

  public static final class Cost extends GeneratedMessageLite.ExtendableMessage<Cost>
    implements LatticeP.CostOrBuilder
  {
    public static final int COST_FIELD_NUMBER = 2;
    public static final int ID_FIELD_NUMBER = 1;
    public static Parser<Cost> PARSER = new AbstractParser()
    {
      public LatticeP.Cost parsePartialFrom(CodedInputStream paramAnonymousCodedInputStream, ExtensionRegistryLite paramAnonymousExtensionRegistryLite)
        throws InvalidProtocolBufferException
      {
        return new LatticeP.Cost(paramAnonymousCodedInputStream, paramAnonymousExtensionRegistryLite, null);
      }
    };
    private static final Cost defaultInstance = new Cost(true);
    private static final long serialVersionUID;
    private int bitField0_;
    private double cost_;
    private int id_;
    private byte memoizedIsInitialized = -1;
    private int memoizedSerializedSize = -1;

    static
    {
      defaultInstance.initFields();
    }

    // ERROR //
    private Cost(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      // Byte code:
      //   0: aload_0
      //   1: invokespecial 49	com/google/protobuf/GeneratedMessageLite$ExtendableMessage:<init>	()V
      //   4: aload_0
      //   5: iconst_m1
      //   6: putfield 51	com/google/protos/aksara/lattice/LatticeP$Cost:memoizedIsInitialized	B
      //   9: aload_0
      //   10: iconst_m1
      //   11: putfield 53	com/google/protos/aksara/lattice/LatticeP$Cost:memoizedSerializedSize	I
      //   14: aload_0
      //   15: invokespecial 43	com/google/protos/aksara/lattice/LatticeP$Cost:initFields	()V
      //   18: iconst_0
      //   19: istore_3
      //   20: iload_3
      //   21: ifne +142 -> 163
      //   24: aload_1
      //   25: invokevirtual 59	com/google/protobuf/CodedInputStream:readTag	()I
      //   28: istore 7
      //   30: iload 7
      //   32: lookupswitch	default:+36->68, 0:+136->168, 8:+52->84, 17:+91->123
      //   69: aload_1
      //   70: aload_2
      //   71: iload 7
      //   73: invokevirtual 63	com/google/protos/aksara/lattice/LatticeP$Cost:parseUnknownField	(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z
      //   76: ifne -56 -> 20
      //   79: iconst_1
      //   80: istore_3
      //   81: goto -61 -> 20
      //   84: aload_0
      //   85: iconst_1
      //   86: aload_0
      //   87: getfield 65	com/google/protos/aksara/lattice/LatticeP$Cost:bitField0_	I
      //   90: ior
      //   91: putfield 65	com/google/protos/aksara/lattice/LatticeP$Cost:bitField0_	I
      //   94: aload_0
      //   95: aload_1
      //   96: invokevirtual 68	com/google/protobuf/CodedInputStream:readInt32	()I
      //   99: putfield 70	com/google/protos/aksara/lattice/LatticeP$Cost:id_	I
      //   102: goto -82 -> 20
      //   105: astore 6
      //   107: aload 6
      //   109: aload_0
      //   110: invokevirtual 74	com/google/protobuf/InvalidProtocolBufferException:setUnfinishedMessage	(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/InvalidProtocolBufferException;
      //   113: athrow
      //   114: astore 5
      //   116: aload_0
      //   117: invokevirtual 77	com/google/protos/aksara/lattice/LatticeP$Cost:makeExtensionsImmutable	()V
      //   120: aload 5
      //   122: athrow
      //   123: aload_0
      //   124: iconst_2
      //   125: aload_0
      //   126: getfield 65	com/google/protos/aksara/lattice/LatticeP$Cost:bitField0_	I
      //   129: ior
      //   130: putfield 65	com/google/protos/aksara/lattice/LatticeP$Cost:bitField0_	I
      //   133: aload_0
      //   134: aload_1
      //   135: invokevirtual 81	com/google/protobuf/CodedInputStream:readDouble	()D
      //   138: putfield 83	com/google/protos/aksara/lattice/LatticeP$Cost:cost_	D
      //   141: goto -121 -> 20
      //   144: astore 4
      //   146: new 46	com/google/protobuf/InvalidProtocolBufferException
      //   149: dup
      //   150: aload 4
      //   152: invokevirtual 87	java/io/IOException:getMessage	()Ljava/lang/String;
      //   155: invokespecial 90	com/google/protobuf/InvalidProtocolBufferException:<init>	(Ljava/lang/String;)V
      //   158: aload_0
      //   159: invokevirtual 74	com/google/protobuf/InvalidProtocolBufferException:setUnfinishedMessage	(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/InvalidProtocolBufferException;
      //   162: athrow
      //   163: aload_0
      //   164: invokevirtual 77	com/google/protos/aksara/lattice/LatticeP$Cost:makeExtensionsImmutable	()V
      //   167: return
      //   168: iconst_1
      //   169: istore_3
      //   170: goto -150 -> 20
      //
      // Exception table:
      //   from	to	target	type
      //   24	30	105	com/google/protobuf/InvalidProtocolBufferException
      //   68	79	105	com/google/protobuf/InvalidProtocolBufferException
      //   84	102	105	com/google/protobuf/InvalidProtocolBufferException
      //   123	141	105	com/google/protobuf/InvalidProtocolBufferException
      //   24	30	114	finally
      //   68	79	114	finally
      //   84	102	114	finally
      //   107	114	114	finally
      //   123	141	114	finally
      //   146	163	114	finally
      //   24	30	144	java/io/IOException
      //   68	79	144	java/io/IOException
      //   84	102	144	java/io/IOException
      //   123	141	144	java/io/IOException
    }

    private Cost(GeneratedMessageLite.ExtendableBuilder<Cost, ?> paramExtendableBuilder)
    {
      super();
    }

    private Cost(boolean paramBoolean)
    {
    }

    public static Cost getDefaultInstance()
    {
      return defaultInstance;
    }

    private void initFields()
    {
      this.id_ = 0;
      this.cost_ = 0.0D;
    }

    public static Builder newBuilder()
    {
      return Builder.access$100();
    }

    public static Builder newBuilder(Cost paramCost)
    {
      return newBuilder().mergeFrom(paramCost);
    }

    public static Cost parseDelimitedFrom(InputStream paramInputStream)
      throws IOException
    {
      return (Cost)PARSER.parseDelimitedFrom(paramInputStream);
    }

    public static Cost parseDelimitedFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return (Cost)PARSER.parseDelimitedFrom(paramInputStream, paramExtensionRegistryLite);
    }

    public static Cost parseFrom(ByteString paramByteString)
      throws InvalidProtocolBufferException
    {
      return (Cost)PARSER.parseFrom(paramByteString);
    }

    public static Cost parseFrom(ByteString paramByteString, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return (Cost)PARSER.parseFrom(paramByteString, paramExtensionRegistryLite);
    }

    public static Cost parseFrom(CodedInputStream paramCodedInputStream)
      throws IOException
    {
      return (Cost)PARSER.parseFrom(paramCodedInputStream);
    }

    public static Cost parseFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return (Cost)PARSER.parseFrom(paramCodedInputStream, paramExtensionRegistryLite);
    }

    public static Cost parseFrom(InputStream paramInputStream)
      throws IOException
    {
      return (Cost)PARSER.parseFrom(paramInputStream);
    }

    public static Cost parseFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return (Cost)PARSER.parseFrom(paramInputStream, paramExtensionRegistryLite);
    }

    public static Cost parseFrom(byte[] paramArrayOfByte)
      throws InvalidProtocolBufferException
    {
      return (Cost)PARSER.parseFrom(paramArrayOfByte);
    }

    public static Cost parseFrom(byte[] paramArrayOfByte, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return (Cost)PARSER.parseFrom(paramArrayOfByte, paramExtensionRegistryLite);
    }

    public double getCost()
    {
      return this.cost_;
    }

    public Cost getDefaultInstanceForType()
    {
      return defaultInstance;
    }

    public int getId()
    {
      return this.id_;
    }

    public Parser<Cost> getParserForType()
    {
      return PARSER;
    }

    public int getSerializedSize()
    {
      int i = this.memoizedSerializedSize;
      if (i != -1)
        return i;
      int j = 0x1 & this.bitField0_;
      int k = 0;
      if (j == 1)
        k = 0 + CodedOutputStream.computeInt32Size(1, this.id_);
      if ((0x2 & this.bitField0_) == 2)
        k += CodedOutputStream.computeDoubleSize(2, this.cost_);
      int m = k + extensionsSerializedSize();
      this.memoizedSerializedSize = m;
      return m;
    }

    public boolean hasCost()
    {
      return (0x2 & this.bitField0_) == 2;
    }

    public boolean hasId()
    {
      return (0x1 & this.bitField0_) == 1;
    }

    public final boolean isInitialized()
    {
      int i = this.memoizedIsInitialized;
      if (i != -1)
        return i == 1;
      if (!extensionsAreInitialized())
      {
        this.memoizedIsInitialized = 0;
        return false;
      }
      this.memoizedIsInitialized = 1;
      return true;
    }

    public Builder newBuilderForType()
    {
      return newBuilder();
    }

    public Builder toBuilder()
    {
      return newBuilder(this);
    }

    protected Object writeReplace()
      throws ObjectStreamException
    {
      return super.writeReplace();
    }

    public void writeTo(CodedOutputStream paramCodedOutputStream)
      throws IOException
    {
      getSerializedSize();
      GeneratedMessageLite.ExtendableMessage.ExtensionWriter localExtensionWriter = newExtensionWriter();
      if ((0x1 & this.bitField0_) == 1)
        paramCodedOutputStream.writeInt32(1, this.id_);
      if ((0x2 & this.bitField0_) == 2)
        paramCodedOutputStream.writeDouble(2, this.cost_);
      localExtensionWriter.writeUntil(536870912, paramCodedOutputStream);
    }

    public static final class Builder extends GeneratedMessageLite.ExtendableBuilder<LatticeP.Cost, Builder>
      implements LatticeP.CostOrBuilder
    {
      private int bitField0_;
      private double cost_;
      private int id_;

      private Builder()
      {
        maybeForceBuilderInitialization();
      }

      private static Builder create()
      {
        return new Builder();
      }

      private void maybeForceBuilderInitialization()
      {
      }

      public LatticeP.Cost build()
      {
        LatticeP.Cost localCost = buildPartial();
        if (!localCost.isInitialized())
          throw newUninitializedMessageException(localCost);
        return localCost;
      }

      public LatticeP.Cost buildPartial()
      {
        LatticeP.Cost localCost = new LatticeP.Cost(this, null);
        int i = this.bitField0_;
        int j = i & 0x1;
        int k = 0;
        if (j == 1)
          k = 0x0 | 0x1;
        LatticeP.Cost.access$302(localCost, this.id_);
        if ((i & 0x2) == 2)
          k |= 2;
        LatticeP.Cost.access$402(localCost, this.cost_);
        LatticeP.Cost.access$502(localCost, k);
        return localCost;
      }

      public Builder clear()
      {
        super.clear();
        this.id_ = 0;
        this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
        this.cost_ = 0.0D;
        this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
        return this;
      }

      public Builder clearCost()
      {
        this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
        this.cost_ = 0.0D;
        return this;
      }

      public Builder clearId()
      {
        this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
        this.id_ = 0;
        return this;
      }

      public Builder clone()
      {
        return create().mergeFrom(buildPartial());
      }

      public double getCost()
      {
        return this.cost_;
      }

      public LatticeP.Cost getDefaultInstanceForType()
      {
        return LatticeP.Cost.getDefaultInstance();
      }

      public int getId()
      {
        return this.id_;
      }

      public boolean hasCost()
      {
        return (0x2 & this.bitField0_) == 2;
      }

      public boolean hasId()
      {
        return (0x1 & this.bitField0_) == 1;
      }

      public final boolean isInitialized()
      {
        return extensionsAreInitialized();
      }

      public Builder mergeFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
        throws IOException
      {
        LatticeP.Cost localCost1 = null;
        try
        {
          LatticeP.Cost localCost2 = (LatticeP.Cost)LatticeP.Cost.PARSER.parsePartialFrom(paramCodedInputStream, paramExtensionRegistryLite);
          return this;
        }
        catch (InvalidProtocolBufferException localInvalidProtocolBufferException)
        {
          localCost1 = (LatticeP.Cost)localInvalidProtocolBufferException.getUnfinishedMessage();
          throw localInvalidProtocolBufferException;
        }
        finally
        {
          if (localCost1 != null)
            mergeFrom(localCost1);
        }
      }

      public Builder mergeFrom(LatticeP.Cost paramCost)
      {
        if (paramCost == LatticeP.Cost.getDefaultInstance())
          return this;
        if (paramCost.hasId())
          setId(paramCost.getId());
        if (paramCost.hasCost())
          setCost(paramCost.getCost());
        mergeExtensionFields(paramCost);
        return this;
      }

      public Builder setCost(double paramDouble)
      {
        this.bitField0_ = (0x2 | this.bitField0_);
        this.cost_ = paramDouble;
        return this;
      }

      public Builder setId(int paramInt)
      {
        this.bitField0_ = (0x1 | this.bitField0_);
        this.id_ = paramInt;
        return this;
      }
    }
  }

  public static abstract interface CostOrBuilder extends GeneratedMessageLite.ExtendableMessageOrBuilder<LatticeP.Cost>
  {
    public abstract double getCost();

    public abstract int getId();

    public abstract boolean hasCost();

    public abstract boolean hasId();
  }

  public static final class CostType extends GeneratedMessageLite
    implements LatticeP.CostTypeOrBuilder
  {
    public static final int DESCRIPTION_FIELD_NUMBER = 15;
    public static final int NAME_FIELD_NUMBER = 1;
    public static Parser<CostType> PARSER = new AbstractParser()
    {
      public LatticeP.CostType parsePartialFrom(CodedInputStream paramAnonymousCodedInputStream, ExtensionRegistryLite paramAnonymousExtensionRegistryLite)
        throws InvalidProtocolBufferException
      {
        return new LatticeP.CostType(paramAnonymousCodedInputStream, paramAnonymousExtensionRegistryLite, null);
      }
    };
    public static final int WEIGHT_FIELD_NUMBER = 2;
    private static final CostType defaultInstance = new CostType(true);
    private static final long serialVersionUID;
    private int bitField0_;
    private LazyStringList description_;
    private byte memoizedIsInitialized = -1;
    private int memoizedSerializedSize = -1;
    private Object name_;
    private double weight_;

    static
    {
      defaultInstance.initFields();
    }

    // ERROR //
    private CostType(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      // Byte code:
      //   0: aload_0
      //   1: invokespecial 53	com/google/protobuf/GeneratedMessageLite:<init>	()V
      //   4: aload_0
      //   5: iconst_m1
      //   6: putfield 55	com/google/protos/aksara/lattice/LatticeP$CostType:memoizedIsInitialized	B
      //   9: aload_0
      //   10: iconst_m1
      //   11: putfield 57	com/google/protos/aksara/lattice/LatticeP$CostType:memoizedSerializedSize	I
      //   14: aload_0
      //   15: invokespecial 47	com/google/protos/aksara/lattice/LatticeP$CostType:initFields	()V
      //   18: iconst_0
      //   19: istore_3
      //   20: iconst_0
      //   21: istore 4
      //   23: iload 4
      //   25: ifne +211 -> 236
      //   28: aload_1
      //   29: invokevirtual 63	com/google/protobuf/CodedInputStream:readTag	()I
      //   32: istore 8
      //   34: iload 8
      //   36: lookupswitch	default:+44->80, 0:+227->263, 10:+61->97, 17:+122->158, 122:+162->198
      //   81: aload_1
      //   82: aload_2
      //   83: iload 8
      //   85: invokevirtual 67	com/google/protos/aksara/lattice/LatticeP$CostType:parseUnknownField	(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z
      //   88: ifne -65 -> 23
      //   91: iconst_1
      //   92: istore 4
      //   94: goto -71 -> 23
      //   97: aload_0
      //   98: iconst_1
      //   99: aload_0
      //   100: getfield 69	com/google/protos/aksara/lattice/LatticeP$CostType:bitField0_	I
      //   103: ior
      //   104: putfield 69	com/google/protos/aksara/lattice/LatticeP$CostType:bitField0_	I
      //   107: aload_0
      //   108: aload_1
      //   109: invokevirtual 73	com/google/protobuf/CodedInputStream:readBytes	()Lcom/google/protobuf/ByteString;
      //   112: putfield 75	com/google/protos/aksara/lattice/LatticeP$CostType:name_	Ljava/lang/Object;
      //   115: goto -92 -> 23
      //   118: astore 7
      //   120: aload 7
      //   122: aload_0
      //   123: invokevirtual 79	com/google/protobuf/InvalidProtocolBufferException:setUnfinishedMessage	(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/InvalidProtocolBufferException;
      //   126: athrow
      //   127: astore 6
      //   129: iload_3
      //   130: iconst_4
      //   131: iand
      //   132: iconst_4
      //   133: if_icmpne +18 -> 151
      //   136: aload_0
      //   137: new 81	com/google/protobuf/UnmodifiableLazyStringList
      //   140: dup
      //   141: aload_0
      //   142: getfield 83	com/google/protos/aksara/lattice/LatticeP$CostType:description_	Lcom/google/protobuf/LazyStringList;
      //   145: invokespecial 86	com/google/protobuf/UnmodifiableLazyStringList:<init>	(Lcom/google/protobuf/LazyStringList;)V
      //   148: putfield 83	com/google/protos/aksara/lattice/LatticeP$CostType:description_	Lcom/google/protobuf/LazyStringList;
      //   151: aload_0
      //   152: invokevirtual 89	com/google/protos/aksara/lattice/LatticeP$CostType:makeExtensionsImmutable	()V
      //   155: aload 6
      //   157: athrow
      //   158: aload_0
      //   159: iconst_2
      //   160: aload_0
      //   161: getfield 69	com/google/protos/aksara/lattice/LatticeP$CostType:bitField0_	I
      //   164: ior
      //   165: putfield 69	com/google/protos/aksara/lattice/LatticeP$CostType:bitField0_	I
      //   168: aload_0
      //   169: aload_1
      //   170: invokevirtual 93	com/google/protobuf/CodedInputStream:readDouble	()D
      //   173: putfield 95	com/google/protos/aksara/lattice/LatticeP$CostType:weight_	D
      //   176: goto -153 -> 23
      //   179: astore 5
      //   181: new 50	com/google/protobuf/InvalidProtocolBufferException
      //   184: dup
      //   185: aload 5
      //   187: invokevirtual 99	java/io/IOException:getMessage	()Ljava/lang/String;
      //   190: invokespecial 102	com/google/protobuf/InvalidProtocolBufferException:<init>	(Ljava/lang/String;)V
      //   193: aload_0
      //   194: invokevirtual 79	com/google/protobuf/InvalidProtocolBufferException:setUnfinishedMessage	(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/InvalidProtocolBufferException;
      //   197: athrow
      //   198: iload_3
      //   199: iconst_4
      //   200: iand
      //   201: iconst_4
      //   202: if_icmpeq +18 -> 220
      //   205: aload_0
      //   206: new 104	com/google/protobuf/LazyStringArrayList
      //   209: dup
      //   210: invokespecial 105	com/google/protobuf/LazyStringArrayList:<init>	()V
      //   213: putfield 83	com/google/protos/aksara/lattice/LatticeP$CostType:description_	Lcom/google/protobuf/LazyStringList;
      //   216: iload_3
      //   217: iconst_4
      //   218: ior
      //   219: istore_3
      //   220: aload_0
      //   221: getfield 83	com/google/protos/aksara/lattice/LatticeP$CostType:description_	Lcom/google/protobuf/LazyStringList;
      //   224: aload_1
      //   225: invokevirtual 73	com/google/protobuf/CodedInputStream:readBytes	()Lcom/google/protobuf/ByteString;
      //   228: invokeinterface 111 2 0
      //   233: goto -210 -> 23
      //   236: iload_3
      //   237: iconst_4
      //   238: iand
      //   239: iconst_4
      //   240: if_icmpne +18 -> 258
      //   243: aload_0
      //   244: new 81	com/google/protobuf/UnmodifiableLazyStringList
      //   247: dup
      //   248: aload_0
      //   249: getfield 83	com/google/protos/aksara/lattice/LatticeP$CostType:description_	Lcom/google/protobuf/LazyStringList;
      //   252: invokespecial 86	com/google/protobuf/UnmodifiableLazyStringList:<init>	(Lcom/google/protobuf/LazyStringList;)V
      //   255: putfield 83	com/google/protos/aksara/lattice/LatticeP$CostType:description_	Lcom/google/protobuf/LazyStringList;
      //   258: aload_0
      //   259: invokevirtual 89	com/google/protos/aksara/lattice/LatticeP$CostType:makeExtensionsImmutable	()V
      //   262: return
      //   263: iconst_1
      //   264: istore 4
      //   266: goto -243 -> 23
      //
      // Exception table:
      //   from	to	target	type
      //   28	34	118	com/google/protobuf/InvalidProtocolBufferException
      //   80	91	118	com/google/protobuf/InvalidProtocolBufferException
      //   97	115	118	com/google/protobuf/InvalidProtocolBufferException
      //   158	176	118	com/google/protobuf/InvalidProtocolBufferException
      //   205	216	118	com/google/protobuf/InvalidProtocolBufferException
      //   220	233	118	com/google/protobuf/InvalidProtocolBufferException
      //   28	34	127	finally
      //   80	91	127	finally
      //   97	115	127	finally
      //   120	127	127	finally
      //   158	176	127	finally
      //   181	198	127	finally
      //   205	216	127	finally
      //   220	233	127	finally
      //   28	34	179	java/io/IOException
      //   80	91	179	java/io/IOException
      //   97	115	179	java/io/IOException
      //   158	176	179	java/io/IOException
      //   205	216	179	java/io/IOException
      //   220	233	179	java/io/IOException
    }

    private CostType(GeneratedMessageLite.Builder paramBuilder)
    {
      super();
    }

    private CostType(boolean paramBoolean)
    {
    }

    public static CostType getDefaultInstance()
    {
      return defaultInstance;
    }

    private void initFields()
    {
      this.name_ = "";
      this.weight_ = 1.0D;
      this.description_ = LazyStringArrayList.EMPTY;
    }

    public static Builder newBuilder()
    {
      return Builder.access$4800();
    }

    public static Builder newBuilder(CostType paramCostType)
    {
      return newBuilder().mergeFrom(paramCostType);
    }

    public static CostType parseDelimitedFrom(InputStream paramInputStream)
      throws IOException
    {
      return (CostType)PARSER.parseDelimitedFrom(paramInputStream);
    }

    public static CostType parseDelimitedFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return (CostType)PARSER.parseDelimitedFrom(paramInputStream, paramExtensionRegistryLite);
    }

    public static CostType parseFrom(ByteString paramByteString)
      throws InvalidProtocolBufferException
    {
      return (CostType)PARSER.parseFrom(paramByteString);
    }

    public static CostType parseFrom(ByteString paramByteString, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return (CostType)PARSER.parseFrom(paramByteString, paramExtensionRegistryLite);
    }

    public static CostType parseFrom(CodedInputStream paramCodedInputStream)
      throws IOException
    {
      return (CostType)PARSER.parseFrom(paramCodedInputStream);
    }

    public static CostType parseFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return (CostType)PARSER.parseFrom(paramCodedInputStream, paramExtensionRegistryLite);
    }

    public static CostType parseFrom(InputStream paramInputStream)
      throws IOException
    {
      return (CostType)PARSER.parseFrom(paramInputStream);
    }

    public static CostType parseFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return (CostType)PARSER.parseFrom(paramInputStream, paramExtensionRegistryLite);
    }

    public static CostType parseFrom(byte[] paramArrayOfByte)
      throws InvalidProtocolBufferException
    {
      return (CostType)PARSER.parseFrom(paramArrayOfByte);
    }

    public static CostType parseFrom(byte[] paramArrayOfByte, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return (CostType)PARSER.parseFrom(paramArrayOfByte, paramExtensionRegistryLite);
    }

    public CostType getDefaultInstanceForType()
    {
      return defaultInstance;
    }

    public String getDescription(int paramInt)
    {
      return (String)this.description_.get(paramInt);
    }

    public ByteString getDescriptionBytes(int paramInt)
    {
      return this.description_.getByteString(paramInt);
    }

    public int getDescriptionCount()
    {
      return this.description_.size();
    }

    public List<String> getDescriptionList()
    {
      return this.description_;
    }

    public String getName()
    {
      Object localObject = this.name_;
      if ((localObject instanceof String))
        return (String)localObject;
      ByteString localByteString = (ByteString)localObject;
      String str = localByteString.toStringUtf8();
      if (localByteString.isValidUtf8())
        this.name_ = str;
      return str;
    }

    public ByteString getNameBytes()
    {
      Object localObject = this.name_;
      if ((localObject instanceof String))
      {
        ByteString localByteString = ByteString.copyFromUtf8((String)localObject);
        this.name_ = localByteString;
        return localByteString;
      }
      return (ByteString)localObject;
    }

    public Parser<CostType> getParserForType()
    {
      return PARSER;
    }

    public int getSerializedSize()
    {
      int i = this.memoizedSerializedSize;
      if (i != -1)
        return i;
      int j = 0x1 & this.bitField0_;
      int k = 0;
      if (j == 1)
        k = 0 + CodedOutputStream.computeBytesSize(1, getNameBytes());
      if ((0x2 & this.bitField0_) == 2)
        k += CodedOutputStream.computeDoubleSize(2, this.weight_);
      int m = 0;
      for (int n = 0; n < this.description_.size(); n++)
        m += CodedOutputStream.computeBytesSizeNoTag(this.description_.getByteString(n));
      int i1 = k + m + 1 * getDescriptionList().size();
      this.memoizedSerializedSize = i1;
      return i1;
    }

    public double getWeight()
    {
      return this.weight_;
    }

    public boolean hasName()
    {
      return (0x1 & this.bitField0_) == 1;
    }

    public boolean hasWeight()
    {
      return (0x2 & this.bitField0_) == 2;
    }

    public final boolean isInitialized()
    {
      int i = this.memoizedIsInitialized;
      if (i != -1)
        return i == 1;
      this.memoizedIsInitialized = 1;
      return true;
    }

    public Builder newBuilderForType()
    {
      return newBuilder();
    }

    public Builder toBuilder()
    {
      return newBuilder(this);
    }

    protected Object writeReplace()
      throws ObjectStreamException
    {
      return super.writeReplace();
    }

    public void writeTo(CodedOutputStream paramCodedOutputStream)
      throws IOException
    {
      getSerializedSize();
      if ((0x1 & this.bitField0_) == 1)
        paramCodedOutputStream.writeBytes(1, getNameBytes());
      if ((0x2 & this.bitField0_) == 2)
        paramCodedOutputStream.writeDouble(2, this.weight_);
      for (int i = 0; i < this.description_.size(); i++)
        paramCodedOutputStream.writeBytes(15, this.description_.getByteString(i));
    }

    public static final class Builder extends GeneratedMessageLite.Builder<LatticeP.CostType, Builder>
      implements LatticeP.CostTypeOrBuilder
    {
      private int bitField0_;
      private LazyStringList description_ = LazyStringArrayList.EMPTY;
      private Object name_ = "";
      private double weight_ = 1.0D;

      private Builder()
      {
        maybeForceBuilderInitialization();
      }

      private static Builder create()
      {
        return new Builder();
      }

      private void ensureDescriptionIsMutable()
      {
        if ((0x4 & this.bitField0_) != 4)
        {
          this.description_ = new LazyStringArrayList(this.description_);
          this.bitField0_ = (0x4 | this.bitField0_);
        }
      }

      private void maybeForceBuilderInitialization()
      {
      }

      public Builder addAllDescription(Iterable<String> paramIterable)
      {
        ensureDescriptionIsMutable();
        GeneratedMessageLite.Builder.addAll(paramIterable, this.description_);
        return this;
      }

      public Builder addDescription(String paramString)
      {
        if (paramString == null)
          throw new NullPointerException();
        ensureDescriptionIsMutable();
        this.description_.add(paramString);
        return this;
      }

      public Builder addDescriptionBytes(ByteString paramByteString)
      {
        if (paramByteString == null)
          throw new NullPointerException();
        ensureDescriptionIsMutable();
        this.description_.add(paramByteString);
        return this;
      }

      public LatticeP.CostType build()
      {
        LatticeP.CostType localCostType = buildPartial();
        if (!localCostType.isInitialized())
          throw newUninitializedMessageException(localCostType);
        return localCostType;
      }

      public LatticeP.CostType buildPartial()
      {
        LatticeP.CostType localCostType = new LatticeP.CostType(this, null);
        int i = this.bitField0_;
        int j = i & 0x1;
        int k = 0;
        if (j == 1)
          k = 0x0 | 0x1;
        LatticeP.CostType.access$5002(localCostType, this.name_);
        if ((i & 0x2) == 2)
          k |= 2;
        LatticeP.CostType.access$5102(localCostType, this.weight_);
        if ((0x4 & this.bitField0_) == 4)
        {
          this.description_ = new UnmodifiableLazyStringList(this.description_);
          this.bitField0_ = (0xFFFFFFFB & this.bitField0_);
        }
        LatticeP.CostType.access$5202(localCostType, this.description_);
        LatticeP.CostType.access$5302(localCostType, k);
        return localCostType;
      }

      public Builder clear()
      {
        super.clear();
        this.name_ = "";
        this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
        this.weight_ = 1.0D;
        this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
        this.description_ = LazyStringArrayList.EMPTY;
        this.bitField0_ = (0xFFFFFFFB & this.bitField0_);
        return this;
      }

      public Builder clearDescription()
      {
        this.description_ = LazyStringArrayList.EMPTY;
        this.bitField0_ = (0xFFFFFFFB & this.bitField0_);
        return this;
      }

      public Builder clearName()
      {
        this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
        this.name_ = LatticeP.CostType.getDefaultInstance().getName();
        return this;
      }

      public Builder clearWeight()
      {
        this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
        this.weight_ = 1.0D;
        return this;
      }

      public Builder clone()
      {
        return create().mergeFrom(buildPartial());
      }

      public LatticeP.CostType getDefaultInstanceForType()
      {
        return LatticeP.CostType.getDefaultInstance();
      }

      public String getDescription(int paramInt)
      {
        return (String)this.description_.get(paramInt);
      }

      public ByteString getDescriptionBytes(int paramInt)
      {
        return this.description_.getByteString(paramInt);
      }

      public int getDescriptionCount()
      {
        return this.description_.size();
      }

      public List<String> getDescriptionList()
      {
        return Collections.unmodifiableList(this.description_);
      }

      public String getName()
      {
        Object localObject = this.name_;
        if (!(localObject instanceof String))
        {
          String str = ((ByteString)localObject).toStringUtf8();
          this.name_ = str;
          return str;
        }
        return (String)localObject;
      }

      public ByteString getNameBytes()
      {
        Object localObject = this.name_;
        if ((localObject instanceof String))
        {
          ByteString localByteString = ByteString.copyFromUtf8((String)localObject);
          this.name_ = localByteString;
          return localByteString;
        }
        return (ByteString)localObject;
      }

      public double getWeight()
      {
        return this.weight_;
      }

      public boolean hasName()
      {
        return (0x1 & this.bitField0_) == 1;
      }

      public boolean hasWeight()
      {
        return (0x2 & this.bitField0_) == 2;
      }

      public final boolean isInitialized()
      {
        return true;
      }

      public Builder mergeFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
        throws IOException
      {
        LatticeP.CostType localCostType1 = null;
        try
        {
          LatticeP.CostType localCostType2 = (LatticeP.CostType)LatticeP.CostType.PARSER.parsePartialFrom(paramCodedInputStream, paramExtensionRegistryLite);
          return this;
        }
        catch (InvalidProtocolBufferException localInvalidProtocolBufferException)
        {
          localCostType1 = (LatticeP.CostType)localInvalidProtocolBufferException.getUnfinishedMessage();
          throw localInvalidProtocolBufferException;
        }
        finally
        {
          if (localCostType1 != null)
            mergeFrom(localCostType1);
        }
      }

      public Builder mergeFrom(LatticeP.CostType paramCostType)
      {
        if (paramCostType == LatticeP.CostType.getDefaultInstance());
        do
        {
          return this;
          if (paramCostType.hasName())
          {
            this.bitField0_ = (0x1 | this.bitField0_);
            this.name_ = paramCostType.name_;
          }
          if (paramCostType.hasWeight())
            setWeight(paramCostType.getWeight());
        }
        while (paramCostType.description_.isEmpty());
        if (this.description_.isEmpty())
        {
          this.description_ = paramCostType.description_;
          this.bitField0_ = (0xFFFFFFFB & this.bitField0_);
          return this;
        }
        ensureDescriptionIsMutable();
        this.description_.addAll(paramCostType.description_);
        return this;
      }

      public Builder setDescription(int paramInt, String paramString)
      {
        if (paramString == null)
          throw new NullPointerException();
        ensureDescriptionIsMutable();
        this.description_.set(paramInt, paramString);
        return this;
      }

      public Builder setName(String paramString)
      {
        if (paramString == null)
          throw new NullPointerException();
        this.bitField0_ = (0x1 | this.bitField0_);
        this.name_ = paramString;
        return this;
      }

      public Builder setNameBytes(ByteString paramByteString)
      {
        if (paramByteString == null)
          throw new NullPointerException();
        this.bitField0_ = (0x1 | this.bitField0_);
        this.name_ = paramByteString;
        return this;
      }

      public Builder setWeight(double paramDouble)
      {
        this.bitField0_ = (0x2 | this.bitField0_);
        this.weight_ = paramDouble;
        return this;
      }
    }
  }

  public static abstract interface CostTypeOrBuilder extends MessageLiteOrBuilder
  {
    public abstract String getDescription(int paramInt);

    public abstract ByteString getDescriptionBytes(int paramInt);

    public abstract int getDescriptionCount();

    public abstract List<String> getDescriptionList();

    public abstract String getName();

    public abstract ByteString getNameBytes();

    public abstract double getWeight();

    public abstract boolean hasName();

    public abstract boolean hasWeight();
  }

  public static final class Edge extends GeneratedMessageLite.ExtendableMessage<Edge>
    implements LatticeP.EdgeOrBuilder
  {
    public static final int BOUNDS_FIELD_NUMBER = 4;
    public static final int COST_FIELD_NUMBER = 3;
    public static final int DESCRIPTION_FIELD_NUMBER = 15;
    public static final int LABEL_FIELD_NUMBER = 2;
    public static Parser<Edge> PARSER = new AbstractParser()
    {
      public LatticeP.Edge parsePartialFrom(CodedInputStream paramAnonymousCodedInputStream, ExtensionRegistryLite paramAnonymousExtensionRegistryLite)
        throws InvalidProtocolBufferException
      {
        return new LatticeP.Edge(paramAnonymousCodedInputStream, paramAnonymousExtensionRegistryLite, null);
      }
    };
    public static final int TARGET_FIELD_NUMBER = 1;
    private static final Edge defaultInstance = new Edge(true);
    private static final long serialVersionUID;
    private int bitField0_;
    private BoundingBoxP.BoundingBox bounds_;
    private List<LatticeP.Cost> cost_;
    private LazyStringList description_;
    private Object label_;
    private byte memoizedIsInitialized = -1;
    private int memoizedSerializedSize = -1;
    private int target_;

    static
    {
      defaultInstance.initFields();
    }

    // ERROR //
    private Edge(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      // Byte code:
      //   0: aload_0
      //   1: invokespecial 62	com/google/protobuf/GeneratedMessageLite$ExtendableMessage:<init>	()V
      //   4: aload_0
      //   5: iconst_m1
      //   6: putfield 64	com/google/protos/aksara/lattice/LatticeP$Edge:memoizedIsInitialized	B
      //   9: aload_0
      //   10: iconst_m1
      //   11: putfield 66	com/google/protos/aksara/lattice/LatticeP$Edge:memoizedSerializedSize	I
      //   14: aload_0
      //   15: invokespecial 56	com/google/protos/aksara/lattice/LatticeP$Edge:initFields	()V
      //   18: iconst_0
      //   19: istore_3
      //   20: iconst_0
      //   21: istore 4
      //   23: iload 4
      //   25: ifne +371 -> 396
      //   28: aload_1
      //   29: invokevirtual 72	com/google/protobuf/CodedInputStream:readTag	()I
      //   32: istore 8
      //   34: iload 8
      //   36: lookupswitch	default:+60->96, 0:+407->443, 8:+77->113, 18:+158->194, 26:+198->234, 34:+241->277, 122:+319->355
      //   97: aload_1
      //   98: aload_2
      //   99: iload 8
      //   101: invokevirtual 76	com/google/protos/aksara/lattice/LatticeP$Edge:parseUnknownField	(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z
      //   104: ifne -81 -> 23
      //   107: iconst_1
      //   108: istore 4
      //   110: goto -87 -> 23
      //   113: aload_0
      //   114: iconst_1
      //   115: aload_0
      //   116: getfield 78	com/google/protos/aksara/lattice/LatticeP$Edge:bitField0_	I
      //   119: ior
      //   120: putfield 78	com/google/protos/aksara/lattice/LatticeP$Edge:bitField0_	I
      //   123: aload_0
      //   124: aload_1
      //   125: invokevirtual 81	com/google/protobuf/CodedInputStream:readInt32	()I
      //   128: putfield 83	com/google/protos/aksara/lattice/LatticeP$Edge:target_	I
      //   131: goto -108 -> 23
      //   134: astore 7
      //   136: aload 7
      //   138: aload_0
      //   139: invokevirtual 87	com/google/protobuf/InvalidProtocolBufferException:setUnfinishedMessage	(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/InvalidProtocolBufferException;
      //   142: athrow
      //   143: astore 6
      //   145: iload_3
      //   146: iconst_4
      //   147: iand
      //   148: iconst_4
      //   149: if_icmpne +14 -> 163
      //   152: aload_0
      //   153: aload_0
      //   154: getfield 89	com/google/protos/aksara/lattice/LatticeP$Edge:cost_	Ljava/util/List;
      //   157: invokestatic 95	java/util/Collections:unmodifiableList	(Ljava/util/List;)Ljava/util/List;
      //   160: putfield 89	com/google/protos/aksara/lattice/LatticeP$Edge:cost_	Ljava/util/List;
      //   163: iload_3
      //   164: bipush 16
      //   166: iand
      //   167: bipush 16
      //   169: if_icmpne +18 -> 187
      //   172: aload_0
      //   173: new 97	com/google/protobuf/UnmodifiableLazyStringList
      //   176: dup
      //   177: aload_0
      //   178: getfield 99	com/google/protos/aksara/lattice/LatticeP$Edge:description_	Lcom/google/protobuf/LazyStringList;
      //   181: invokespecial 102	com/google/protobuf/UnmodifiableLazyStringList:<init>	(Lcom/google/protobuf/LazyStringList;)V
      //   184: putfield 99	com/google/protos/aksara/lattice/LatticeP$Edge:description_	Lcom/google/protobuf/LazyStringList;
      //   187: aload_0
      //   188: invokevirtual 105	com/google/protos/aksara/lattice/LatticeP$Edge:makeExtensionsImmutable	()V
      //   191: aload 6
      //   193: athrow
      //   194: aload_0
      //   195: iconst_2
      //   196: aload_0
      //   197: getfield 78	com/google/protos/aksara/lattice/LatticeP$Edge:bitField0_	I
      //   200: ior
      //   201: putfield 78	com/google/protos/aksara/lattice/LatticeP$Edge:bitField0_	I
      //   204: aload_0
      //   205: aload_1
      //   206: invokevirtual 109	com/google/protobuf/CodedInputStream:readBytes	()Lcom/google/protobuf/ByteString;
      //   209: putfield 111	com/google/protos/aksara/lattice/LatticeP$Edge:label_	Ljava/lang/Object;
      //   212: goto -189 -> 23
      //   215: astore 5
      //   217: new 59	com/google/protobuf/InvalidProtocolBufferException
      //   220: dup
      //   221: aload 5
      //   223: invokevirtual 115	java/io/IOException:getMessage	()Ljava/lang/String;
      //   226: invokespecial 118	com/google/protobuf/InvalidProtocolBufferException:<init>	(Ljava/lang/String;)V
      //   229: aload_0
      //   230: invokevirtual 87	com/google/protobuf/InvalidProtocolBufferException:setUnfinishedMessage	(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/InvalidProtocolBufferException;
      //   233: athrow
      //   234: iload_3
      //   235: iconst_4
      //   236: iand
      //   237: iconst_4
      //   238: if_icmpeq +18 -> 256
      //   241: aload_0
      //   242: new 120	java/util/ArrayList
      //   245: dup
      //   246: invokespecial 121	java/util/ArrayList:<init>	()V
      //   249: putfield 89	com/google/protos/aksara/lattice/LatticeP$Edge:cost_	Ljava/util/List;
      //   252: iload_3
      //   253: iconst_4
      //   254: ior
      //   255: istore_3
      //   256: aload_0
      //   257: getfield 89	com/google/protos/aksara/lattice/LatticeP$Edge:cost_	Ljava/util/List;
      //   260: aload_1
      //   261: getstatic 124	com/google/protos/aksara/lattice/LatticeP$Cost:PARSER	Lcom/google/protobuf/Parser;
      //   264: aload_2
      //   265: invokevirtual 128	com/google/protobuf/CodedInputStream:readMessage	(Lcom/google/protobuf/Parser;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite;
      //   268: invokeinterface 134 2 0
      //   273: pop
      //   274: goto -251 -> 23
      //   277: iconst_4
      //   278: aload_0
      //   279: getfield 78	com/google/protos/aksara/lattice/LatticeP$Edge:bitField0_	I
      //   282: iand
      //   283: istore 9
      //   285: aconst_null
      //   286: astore 10
      //   288: iload 9
      //   290: iconst_4
      //   291: if_icmpne +12 -> 303
      //   294: aload_0
      //   295: getfield 136	com/google/protos/aksara/lattice/LatticeP$Edge:bounds_	Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;
      //   298: invokevirtual 142	com/google/protos/aksara/BoundingBoxP$BoundingBox:toBuilder	()Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;
      //   301: astore 10
      //   303: aload_0
      //   304: aload_1
      //   305: getstatic 143	com/google/protos/aksara/BoundingBoxP$BoundingBox:PARSER	Lcom/google/protobuf/Parser;
      //   308: aload_2
      //   309: invokevirtual 128	com/google/protobuf/CodedInputStream:readMessage	(Lcom/google/protobuf/Parser;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite;
      //   312: checkcast 138	com/google/protos/aksara/BoundingBoxP$BoundingBox
      //   315: putfield 136	com/google/protos/aksara/lattice/LatticeP$Edge:bounds_	Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;
      //   318: aload 10
      //   320: ifnull +22 -> 342
      //   323: aload 10
      //   325: aload_0
      //   326: getfield 136	com/google/protos/aksara/lattice/LatticeP$Edge:bounds_	Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;
      //   329: invokevirtual 149	com/google/protos/aksara/BoundingBoxP$BoundingBox$Builder:mergeFrom	(Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;)Lcom/google/protos/aksara/BoundingBoxP$BoundingBox$Builder;
      //   332: pop
      //   333: aload_0
      //   334: aload 10
      //   336: invokevirtual 153	com/google/protos/aksara/BoundingBoxP$BoundingBox$Builder:buildPartial	()Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;
      //   339: putfield 136	com/google/protos/aksara/lattice/LatticeP$Edge:bounds_	Lcom/google/protos/aksara/BoundingBoxP$BoundingBox;
      //   342: aload_0
      //   343: iconst_4
      //   344: aload_0
      //   345: getfield 78	com/google/protos/aksara/lattice/LatticeP$Edge:bitField0_	I
      //   348: ior
      //   349: putfield 78	com/google/protos/aksara/lattice/LatticeP$Edge:bitField0_	I
      //   352: goto -329 -> 23
      //   355: iload_3
      //   356: bipush 16
      //   358: iand
      //   359: bipush 16
      //   361: if_icmpeq +19 -> 380
      //   364: aload_0
      //   365: new 155	com/google/protobuf/LazyStringArrayList
      //   368: dup
      //   369: invokespecial 156	com/google/protobuf/LazyStringArrayList:<init>	()V
      //   372: putfield 99	com/google/protos/aksara/lattice/LatticeP$Edge:description_	Lcom/google/protobuf/LazyStringList;
      //   375: iload_3
      //   376: bipush 16
      //   378: ior
      //   379: istore_3
      //   380: aload_0
      //   381: getfield 99	com/google/protos/aksara/lattice/LatticeP$Edge:description_	Lcom/google/protobuf/LazyStringList;
      //   384: aload_1
      //   385: invokevirtual 109	com/google/protobuf/CodedInputStream:readBytes	()Lcom/google/protobuf/ByteString;
      //   388: invokeinterface 161 2 0
      //   393: goto -370 -> 23
      //   396: iload_3
      //   397: iconst_4
      //   398: iand
      //   399: iconst_4
      //   400: if_icmpne +14 -> 414
      //   403: aload_0
      //   404: aload_0
      //   405: getfield 89	com/google/protos/aksara/lattice/LatticeP$Edge:cost_	Ljava/util/List;
      //   408: invokestatic 95	java/util/Collections:unmodifiableList	(Ljava/util/List;)Ljava/util/List;
      //   411: putfield 89	com/google/protos/aksara/lattice/LatticeP$Edge:cost_	Ljava/util/List;
      //   414: iload_3
      //   415: bipush 16
      //   417: iand
      //   418: bipush 16
      //   420: if_icmpne +18 -> 438
      //   423: aload_0
      //   424: new 97	com/google/protobuf/UnmodifiableLazyStringList
      //   427: dup
      //   428: aload_0
      //   429: getfield 99	com/google/protos/aksara/lattice/LatticeP$Edge:description_	Lcom/google/protobuf/LazyStringList;
      //   432: invokespecial 102	com/google/protobuf/UnmodifiableLazyStringList:<init>	(Lcom/google/protobuf/LazyStringList;)V
      //   435: putfield 99	com/google/protos/aksara/lattice/LatticeP$Edge:description_	Lcom/google/protobuf/LazyStringList;
      //   438: aload_0
      //   439: invokevirtual 105	com/google/protos/aksara/lattice/LatticeP$Edge:makeExtensionsImmutable	()V
      //   442: return
      //   443: iconst_1
      //   444: istore 4
      //   446: goto -423 -> 23
      //
      // Exception table:
      //   from	to	target	type
      //   28	34	134	com/google/protobuf/InvalidProtocolBufferException
      //   96	107	134	com/google/protobuf/InvalidProtocolBufferException
      //   113	131	134	com/google/protobuf/InvalidProtocolBufferException
      //   194	212	134	com/google/protobuf/InvalidProtocolBufferException
      //   241	252	134	com/google/protobuf/InvalidProtocolBufferException
      //   256	274	134	com/google/protobuf/InvalidProtocolBufferException
      //   277	285	134	com/google/protobuf/InvalidProtocolBufferException
      //   294	303	134	com/google/protobuf/InvalidProtocolBufferException
      //   303	318	134	com/google/protobuf/InvalidProtocolBufferException
      //   323	342	134	com/google/protobuf/InvalidProtocolBufferException
      //   342	352	134	com/google/protobuf/InvalidProtocolBufferException
      //   364	375	134	com/google/protobuf/InvalidProtocolBufferException
      //   380	393	134	com/google/protobuf/InvalidProtocolBufferException
      //   28	34	143	finally
      //   96	107	143	finally
      //   113	131	143	finally
      //   136	143	143	finally
      //   194	212	143	finally
      //   217	234	143	finally
      //   241	252	143	finally
      //   256	274	143	finally
      //   277	285	143	finally
      //   294	303	143	finally
      //   303	318	143	finally
      //   323	342	143	finally
      //   342	352	143	finally
      //   364	375	143	finally
      //   380	393	143	finally
      //   28	34	215	java/io/IOException
      //   96	107	215	java/io/IOException
      //   113	131	215	java/io/IOException
      //   194	212	215	java/io/IOException
      //   241	252	215	java/io/IOException
      //   256	274	215	java/io/IOException
      //   277	285	215	java/io/IOException
      //   294	303	215	java/io/IOException
      //   303	318	215	java/io/IOException
      //   323	342	215	java/io/IOException
      //   342	352	215	java/io/IOException
      //   364	375	215	java/io/IOException
      //   380	393	215	java/io/IOException
    }

    private Edge(GeneratedMessageLite.ExtendableBuilder<Edge, ?> paramExtendableBuilder)
    {
      super();
    }

    private Edge(boolean paramBoolean)
    {
    }

    public static Edge getDefaultInstance()
    {
      return defaultInstance;
    }

    private void initFields()
    {
      this.target_ = 0;
      this.label_ = "";
      this.cost_ = Collections.emptyList();
      this.bounds_ = BoundingBoxP.BoundingBox.getDefaultInstance();
      this.description_ = LazyStringArrayList.EMPTY;
    }

    public static Builder newBuilder()
    {
      return Builder.access$700();
    }

    public static Builder newBuilder(Edge paramEdge)
    {
      return newBuilder().mergeFrom(paramEdge);
    }

    public static Edge parseDelimitedFrom(InputStream paramInputStream)
      throws IOException
    {
      return (Edge)PARSER.parseDelimitedFrom(paramInputStream);
    }

    public static Edge parseDelimitedFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return (Edge)PARSER.parseDelimitedFrom(paramInputStream, paramExtensionRegistryLite);
    }

    public static Edge parseFrom(ByteString paramByteString)
      throws InvalidProtocolBufferException
    {
      return (Edge)PARSER.parseFrom(paramByteString);
    }

    public static Edge parseFrom(ByteString paramByteString, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return (Edge)PARSER.parseFrom(paramByteString, paramExtensionRegistryLite);
    }

    public static Edge parseFrom(CodedInputStream paramCodedInputStream)
      throws IOException
    {
      return (Edge)PARSER.parseFrom(paramCodedInputStream);
    }

    public static Edge parseFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return (Edge)PARSER.parseFrom(paramCodedInputStream, paramExtensionRegistryLite);
    }

    public static Edge parseFrom(InputStream paramInputStream)
      throws IOException
    {
      return (Edge)PARSER.parseFrom(paramInputStream);
    }

    public static Edge parseFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return (Edge)PARSER.parseFrom(paramInputStream, paramExtensionRegistryLite);
    }

    public static Edge parseFrom(byte[] paramArrayOfByte)
      throws InvalidProtocolBufferException
    {
      return (Edge)PARSER.parseFrom(paramArrayOfByte);
    }

    public static Edge parseFrom(byte[] paramArrayOfByte, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return (Edge)PARSER.parseFrom(paramArrayOfByte, paramExtensionRegistryLite);
    }

    public BoundingBoxP.BoundingBox getBounds()
    {
      return this.bounds_;
    }

    public LatticeP.Cost getCost(int paramInt)
    {
      return (LatticeP.Cost)this.cost_.get(paramInt);
    }

    public int getCostCount()
    {
      return this.cost_.size();
    }

    public List<LatticeP.Cost> getCostList()
    {
      return this.cost_;
    }

    public LatticeP.CostOrBuilder getCostOrBuilder(int paramInt)
    {
      return (LatticeP.CostOrBuilder)this.cost_.get(paramInt);
    }

    public List<? extends LatticeP.CostOrBuilder> getCostOrBuilderList()
    {
      return this.cost_;
    }

    public Edge getDefaultInstanceForType()
    {
      return defaultInstance;
    }

    public String getDescription(int paramInt)
    {
      return (String)this.description_.get(paramInt);
    }

    public ByteString getDescriptionBytes(int paramInt)
    {
      return this.description_.getByteString(paramInt);
    }

    public int getDescriptionCount()
    {
      return this.description_.size();
    }

    public List<String> getDescriptionList()
    {
      return this.description_;
    }

    public String getLabel()
    {
      Object localObject = this.label_;
      if ((localObject instanceof String))
        return (String)localObject;
      ByteString localByteString = (ByteString)localObject;
      String str = localByteString.toStringUtf8();
      if (localByteString.isValidUtf8())
        this.label_ = str;
      return str;
    }

    public ByteString getLabelBytes()
    {
      Object localObject = this.label_;
      if ((localObject instanceof String))
      {
        ByteString localByteString = ByteString.copyFromUtf8((String)localObject);
        this.label_ = localByteString;
        return localByteString;
      }
      return (ByteString)localObject;
    }

    public Parser<Edge> getParserForType()
    {
      return PARSER;
    }

    public int getSerializedSize()
    {
      int i = this.memoizedSerializedSize;
      if (i != -1)
        return i;
      int j = 0x1 & this.bitField0_;
      int k = 0;
      if (j == 1)
        k = 0 + CodedOutputStream.computeInt32Size(1, this.target_);
      if ((0x2 & this.bitField0_) == 2)
        k += CodedOutputStream.computeBytesSize(2, getLabelBytes());
      for (int m = 0; m < this.cost_.size(); m++)
        k += CodedOutputStream.computeMessageSize(3, (MessageLite)this.cost_.get(m));
      if ((0x4 & this.bitField0_) == 4)
        k += CodedOutputStream.computeMessageSize(4, this.bounds_);
      int n = 0;
      for (int i1 = 0; i1 < this.description_.size(); i1++)
        n += CodedOutputStream.computeBytesSizeNoTag(this.description_.getByteString(i1));
      int i2 = k + n + 1 * getDescriptionList().size() + extensionsSerializedSize();
      this.memoizedSerializedSize = i2;
      return i2;
    }

    public int getTarget()
    {
      return this.target_;
    }

    public boolean hasBounds()
    {
      return (0x4 & this.bitField0_) == 4;
    }

    public boolean hasLabel()
    {
      return (0x2 & this.bitField0_) == 2;
    }

    public boolean hasTarget()
    {
      return (0x1 & this.bitField0_) == 1;
    }

    public final boolean isInitialized()
    {
      int i = this.memoizedIsInitialized;
      if (i != -1)
        return i == 1;
      for (int j = 0; j < getCostCount(); j++)
        if (!getCost(j).isInitialized())
        {
          this.memoizedIsInitialized = 0;
          return false;
        }
      if (!extensionsAreInitialized())
      {
        this.memoizedIsInitialized = 0;
        return false;
      }
      this.memoizedIsInitialized = 1;
      return true;
    }

    public Builder newBuilderForType()
    {
      return newBuilder();
    }

    public Builder toBuilder()
    {
      return newBuilder(this);
    }

    protected Object writeReplace()
      throws ObjectStreamException
    {
      return super.writeReplace();
    }

    public void writeTo(CodedOutputStream paramCodedOutputStream)
      throws IOException
    {
      getSerializedSize();
      GeneratedMessageLite.ExtendableMessage.ExtensionWriter localExtensionWriter = newExtensionWriter();
      if ((0x1 & this.bitField0_) == 1)
        paramCodedOutputStream.writeInt32(1, this.target_);
      if ((0x2 & this.bitField0_) == 2)
        paramCodedOutputStream.writeBytes(2, getLabelBytes());
      for (int i = 0; i < this.cost_.size(); i++)
        paramCodedOutputStream.writeMessage(3, (MessageLite)this.cost_.get(i));
      if ((0x4 & this.bitField0_) == 4)
        paramCodedOutputStream.writeMessage(4, this.bounds_);
      for (int j = 0; j < this.description_.size(); j++)
        paramCodedOutputStream.writeBytes(15, this.description_.getByteString(j));
      localExtensionWriter.writeUntil(536870912, paramCodedOutputStream);
    }

    public static final class Builder extends GeneratedMessageLite.ExtendableBuilder<LatticeP.Edge, Builder>
      implements LatticeP.EdgeOrBuilder
    {
      private int bitField0_;
      private BoundingBoxP.BoundingBox bounds_ = BoundingBoxP.BoundingBox.getDefaultInstance();
      private List<LatticeP.Cost> cost_ = Collections.emptyList();
      private LazyStringList description_ = LazyStringArrayList.EMPTY;
      private Object label_ = "";
      private int target_;

      private Builder()
      {
        maybeForceBuilderInitialization();
      }

      private static Builder create()
      {
        return new Builder();
      }

      private void ensureCostIsMutable()
      {
        if ((0x4 & this.bitField0_) != 4)
        {
          this.cost_ = new ArrayList(this.cost_);
          this.bitField0_ = (0x4 | this.bitField0_);
        }
      }

      private void ensureDescriptionIsMutable()
      {
        if ((0x10 & this.bitField0_) != 16)
        {
          this.description_ = new LazyStringArrayList(this.description_);
          this.bitField0_ = (0x10 | this.bitField0_);
        }
      }

      private void maybeForceBuilderInitialization()
      {
      }

      public Builder addAllCost(Iterable<? extends LatticeP.Cost> paramIterable)
      {
        ensureCostIsMutable();
        GeneratedMessageLite.ExtendableBuilder.addAll(paramIterable, this.cost_);
        return this;
      }

      public Builder addAllDescription(Iterable<String> paramIterable)
      {
        ensureDescriptionIsMutable();
        GeneratedMessageLite.ExtendableBuilder.addAll(paramIterable, this.description_);
        return this;
      }

      public Builder addCost(int paramInt, LatticeP.Cost.Builder paramBuilder)
      {
        ensureCostIsMutable();
        this.cost_.add(paramInt, paramBuilder.build());
        return this;
      }

      public Builder addCost(int paramInt, LatticeP.Cost paramCost)
      {
        if (paramCost == null)
          throw new NullPointerException();
        ensureCostIsMutable();
        this.cost_.add(paramInt, paramCost);
        return this;
      }

      public Builder addCost(LatticeP.Cost.Builder paramBuilder)
      {
        ensureCostIsMutable();
        this.cost_.add(paramBuilder.build());
        return this;
      }

      public Builder addCost(LatticeP.Cost paramCost)
      {
        if (paramCost == null)
          throw new NullPointerException();
        ensureCostIsMutable();
        this.cost_.add(paramCost);
        return this;
      }

      public Builder addDescription(String paramString)
      {
        if (paramString == null)
          throw new NullPointerException();
        ensureDescriptionIsMutable();
        this.description_.add(paramString);
        return this;
      }

      public Builder addDescriptionBytes(ByteString paramByteString)
      {
        if (paramByteString == null)
          throw new NullPointerException();
        ensureDescriptionIsMutable();
        this.description_.add(paramByteString);
        return this;
      }

      public LatticeP.Edge build()
      {
        LatticeP.Edge localEdge = buildPartial();
        if (!localEdge.isInitialized())
          throw newUninitializedMessageException(localEdge);
        return localEdge;
      }

      public LatticeP.Edge buildPartial()
      {
        LatticeP.Edge localEdge = new LatticeP.Edge(this, null);
        int i = this.bitField0_;
        int j = i & 0x1;
        int k = 0;
        if (j == 1)
          k = 0x0 | 0x1;
        LatticeP.Edge.access$902(localEdge, this.target_);
        if ((i & 0x2) == 2)
          k |= 2;
        LatticeP.Edge.access$1002(localEdge, this.label_);
        if ((0x4 & this.bitField0_) == 4)
        {
          this.cost_ = Collections.unmodifiableList(this.cost_);
          this.bitField0_ = (0xFFFFFFFB & this.bitField0_);
        }
        LatticeP.Edge.access$1102(localEdge, this.cost_);
        if ((i & 0x8) == 8)
          k |= 4;
        LatticeP.Edge.access$1202(localEdge, this.bounds_);
        if ((0x10 & this.bitField0_) == 16)
        {
          this.description_ = new UnmodifiableLazyStringList(this.description_);
          this.bitField0_ = (0xFFFFFFEF & this.bitField0_);
        }
        LatticeP.Edge.access$1302(localEdge, this.description_);
        LatticeP.Edge.access$1402(localEdge, k);
        return localEdge;
      }

      public Builder clear()
      {
        super.clear();
        this.target_ = 0;
        this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
        this.label_ = "";
        this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
        this.cost_ = Collections.emptyList();
        this.bitField0_ = (0xFFFFFFFB & this.bitField0_);
        this.bounds_ = BoundingBoxP.BoundingBox.getDefaultInstance();
        this.bitField0_ = (0xFFFFFFF7 & this.bitField0_);
        this.description_ = LazyStringArrayList.EMPTY;
        this.bitField0_ = (0xFFFFFFEF & this.bitField0_);
        return this;
      }

      public Builder clearBounds()
      {
        this.bounds_ = BoundingBoxP.BoundingBox.getDefaultInstance();
        this.bitField0_ = (0xFFFFFFF7 & this.bitField0_);
        return this;
      }

      public Builder clearCost()
      {
        this.cost_ = Collections.emptyList();
        this.bitField0_ = (0xFFFFFFFB & this.bitField0_);
        return this;
      }

      public Builder clearDescription()
      {
        this.description_ = LazyStringArrayList.EMPTY;
        this.bitField0_ = (0xFFFFFFEF & this.bitField0_);
        return this;
      }

      public Builder clearLabel()
      {
        this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
        this.label_ = LatticeP.Edge.getDefaultInstance().getLabel();
        return this;
      }

      public Builder clearTarget()
      {
        this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
        this.target_ = 0;
        return this;
      }

      public Builder clone()
      {
        return create().mergeFrom(buildPartial());
      }

      public BoundingBoxP.BoundingBox getBounds()
      {
        return this.bounds_;
      }

      public LatticeP.Cost getCost(int paramInt)
      {
        return (LatticeP.Cost)this.cost_.get(paramInt);
      }

      public int getCostCount()
      {
        return this.cost_.size();
      }

      public List<LatticeP.Cost> getCostList()
      {
        return Collections.unmodifiableList(this.cost_);
      }

      public LatticeP.Edge getDefaultInstanceForType()
      {
        return LatticeP.Edge.getDefaultInstance();
      }

      public String getDescription(int paramInt)
      {
        return (String)this.description_.get(paramInt);
      }

      public ByteString getDescriptionBytes(int paramInt)
      {
        return this.description_.getByteString(paramInt);
      }

      public int getDescriptionCount()
      {
        return this.description_.size();
      }

      public List<String> getDescriptionList()
      {
        return Collections.unmodifiableList(this.description_);
      }

      public String getLabel()
      {
        Object localObject = this.label_;
        if (!(localObject instanceof String))
        {
          String str = ((ByteString)localObject).toStringUtf8();
          this.label_ = str;
          return str;
        }
        return (String)localObject;
      }

      public ByteString getLabelBytes()
      {
        Object localObject = this.label_;
        if ((localObject instanceof String))
        {
          ByteString localByteString = ByteString.copyFromUtf8((String)localObject);
          this.label_ = localByteString;
          return localByteString;
        }
        return (ByteString)localObject;
      }

      public int getTarget()
      {
        return this.target_;
      }

      public boolean hasBounds()
      {
        return (0x8 & this.bitField0_) == 8;
      }

      public boolean hasLabel()
      {
        return (0x2 & this.bitField0_) == 2;
      }

      public boolean hasTarget()
      {
        return (0x1 & this.bitField0_) == 1;
      }

      public final boolean isInitialized()
      {
        int i = 0;
        if (i < getCostCount())
          if (getCost(i).isInitialized());
        while (!extensionsAreInitialized())
        {
          return false;
          i++;
          break;
        }
        return true;
      }

      public Builder mergeBounds(BoundingBoxP.BoundingBox paramBoundingBox)
      {
        if (((0x8 & this.bitField0_) == 8) && (this.bounds_ != BoundingBoxP.BoundingBox.getDefaultInstance()));
        for (this.bounds_ = BoundingBoxP.BoundingBox.newBuilder(this.bounds_).mergeFrom(paramBoundingBox).buildPartial(); ; this.bounds_ = paramBoundingBox)
        {
          this.bitField0_ = (0x8 | this.bitField0_);
          return this;
        }
      }

      public Builder mergeFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
        throws IOException
      {
        LatticeP.Edge localEdge1 = null;
        try
        {
          LatticeP.Edge localEdge2 = (LatticeP.Edge)LatticeP.Edge.PARSER.parsePartialFrom(paramCodedInputStream, paramExtensionRegistryLite);
          return this;
        }
        catch (InvalidProtocolBufferException localInvalidProtocolBufferException)
        {
          localEdge1 = (LatticeP.Edge)localInvalidProtocolBufferException.getUnfinishedMessage();
          throw localInvalidProtocolBufferException;
        }
        finally
        {
          if (localEdge1 != null)
            mergeFrom(localEdge1);
        }
      }

      public Builder mergeFrom(LatticeP.Edge paramEdge)
      {
        if (paramEdge == LatticeP.Edge.getDefaultInstance())
          return this;
        if (paramEdge.hasTarget())
          setTarget(paramEdge.getTarget());
        if (paramEdge.hasLabel())
        {
          this.bitField0_ = (0x2 | this.bitField0_);
          this.label_ = paramEdge.label_;
        }
        if (!paramEdge.cost_.isEmpty())
        {
          if (this.cost_.isEmpty())
          {
            this.cost_ = paramEdge.cost_;
            this.bitField0_ = (0xFFFFFFFB & this.bitField0_);
          }
        }
        else
        {
          if (paramEdge.hasBounds())
            mergeBounds(paramEdge.getBounds());
          if (!paramEdge.description_.isEmpty())
          {
            if (!this.description_.isEmpty())
              break label180;
            this.description_ = paramEdge.description_;
            this.bitField0_ = (0xFFFFFFEF & this.bitField0_);
          }
        }
        while (true)
        {
          mergeExtensionFields(paramEdge);
          return this;
          ensureCostIsMutable();
          this.cost_.addAll(paramEdge.cost_);
          break;
          label180: ensureDescriptionIsMutable();
          this.description_.addAll(paramEdge.description_);
        }
      }

      public Builder removeCost(int paramInt)
      {
        ensureCostIsMutable();
        this.cost_.remove(paramInt);
        return this;
      }

      public Builder setBounds(BoundingBoxP.BoundingBox.Builder paramBuilder)
      {
        this.bounds_ = paramBuilder.build();
        this.bitField0_ = (0x8 | this.bitField0_);
        return this;
      }

      public Builder setBounds(BoundingBoxP.BoundingBox paramBoundingBox)
      {
        if (paramBoundingBox == null)
          throw new NullPointerException();
        this.bounds_ = paramBoundingBox;
        this.bitField0_ = (0x8 | this.bitField0_);
        return this;
      }

      public Builder setCost(int paramInt, LatticeP.Cost.Builder paramBuilder)
      {
        ensureCostIsMutable();
        this.cost_.set(paramInt, paramBuilder.build());
        return this;
      }

      public Builder setCost(int paramInt, LatticeP.Cost paramCost)
      {
        if (paramCost == null)
          throw new NullPointerException();
        ensureCostIsMutable();
        this.cost_.set(paramInt, paramCost);
        return this;
      }

      public Builder setDescription(int paramInt, String paramString)
      {
        if (paramString == null)
          throw new NullPointerException();
        ensureDescriptionIsMutable();
        this.description_.set(paramInt, paramString);
        return this;
      }

      public Builder setLabel(String paramString)
      {
        if (paramString == null)
          throw new NullPointerException();
        this.bitField0_ = (0x2 | this.bitField0_);
        this.label_ = paramString;
        return this;
      }

      public Builder setLabelBytes(ByteString paramByteString)
      {
        if (paramByteString == null)
          throw new NullPointerException();
        this.bitField0_ = (0x2 | this.bitField0_);
        this.label_ = paramByteString;
        return this;
      }

      public Builder setTarget(int paramInt)
      {
        this.bitField0_ = (0x1 | this.bitField0_);
        this.target_ = paramInt;
        return this;
      }
    }
  }

  public static abstract interface EdgeOrBuilder extends GeneratedMessageLite.ExtendableMessageOrBuilder<LatticeP.Edge>
  {
    public abstract BoundingBoxP.BoundingBox getBounds();

    public abstract LatticeP.Cost getCost(int paramInt);

    public abstract int getCostCount();

    public abstract List<LatticeP.Cost> getCostList();

    public abstract String getDescription(int paramInt);

    public abstract ByteString getDescriptionBytes(int paramInt);

    public abstract int getDescriptionCount();

    public abstract List<String> getDescriptionList();

    public abstract String getLabel();

    public abstract ByteString getLabelBytes();

    public abstract int getTarget();

    public abstract boolean hasBounds();

    public abstract boolean hasLabel();

    public abstract boolean hasTarget();
  }

  public static final class Lattice extends GeneratedMessageLite.ExtendableMessage<Lattice>
    implements LatticeP.LatticeOrBuilder
  {
    public static final int COST_TYPE_FIELD_NUMBER = 2;
    public static final int MAX_EDGE_FACTOR_FIELD_NUMBER = 5;
    public static final int NODE_FIELD_NUMBER = 1;
    public static Parser<Lattice> PARSER = new AbstractParser()
    {
      public LatticeP.Lattice parsePartialFrom(CodedInputStream paramAnonymousCodedInputStream, ExtensionRegistryLite paramAnonymousExtensionRegistryLite)
        throws InvalidProtocolBufferException
      {
        return new LatticeP.Lattice(paramAnonymousCodedInputStream, paramAnonymousExtensionRegistryLite, null);
      }
    };
    public static final int PATH_FIELD_NUMBER = 3;
    public static final int STATE_TYPE_FIELD_NUMBER = 4;
    private static final Lattice defaultInstance = new Lattice(true);
    private static final long serialVersionUID;
    private int bitField0_;
    private List<LatticeP.CostType> costType_;
    private double maxEdgeFactor_;
    private byte memoizedIsInitialized = -1;
    private int memoizedSerializedSize = -1;
    private List<LatticeP.Node> node_;
    private List<LatticeP.Path> path_;
    private List<LatticeP.StateType> stateType_;

    static
    {
      defaultInstance.initFields();
    }

    // ERROR //
    private Lattice(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      // Byte code:
      //   0: aload_0
      //   1: invokespecial 63	com/google/protobuf/GeneratedMessageLite$ExtendableMessage:<init>	()V
      //   4: aload_0
      //   5: iconst_m1
      //   6: putfield 65	com/google/protos/aksara/lattice/LatticeP$Lattice:memoizedIsInitialized	B
      //   9: aload_0
      //   10: iconst_m1
      //   11: putfield 67	com/google/protos/aksara/lattice/LatticeP$Lattice:memoizedSerializedSize	I
      //   14: aload_0
      //   15: invokespecial 57	com/google/protos/aksara/lattice/LatticeP$Lattice:initFields	()V
      //   18: iconst_0
      //   19: istore_3
      //   20: iconst_0
      //   21: istore 4
      //   23: iload 4
      //   25: ifne +395 -> 420
      //   28: aload_1
      //   29: invokevirtual 73	com/google/protobuf/CodedInputStream:readTag	()I
      //   32: istore 8
      //   34: iload 8
      //   36: lookupswitch	default:+60->96, 0:+463->499, 10:+77->113, 18:+212->248, 26:+274->310, 34:+317->353, 41:+363->399
      //   97: aload_1
      //   98: aload_2
      //   99: iload 8
      //   101: invokevirtual 77	com/google/protos/aksara/lattice/LatticeP$Lattice:parseUnknownField	(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z
      //   104: ifne -81 -> 23
      //   107: iconst_1
      //   108: istore 4
      //   110: goto -87 -> 23
      //   113: iload_3
      //   114: iconst_1
      //   115: iand
      //   116: iconst_1
      //   117: if_icmpeq +18 -> 135
      //   120: aload_0
      //   121: new 79	java/util/ArrayList
      //   124: dup
      //   125: invokespecial 80	java/util/ArrayList:<init>	()V
      //   128: putfield 82	com/google/protos/aksara/lattice/LatticeP$Lattice:node_	Ljava/util/List;
      //   131: iload_3
      //   132: iconst_1
      //   133: ior
      //   134: istore_3
      //   135: aload_0
      //   136: getfield 82	com/google/protos/aksara/lattice/LatticeP$Lattice:node_	Ljava/util/List;
      //   139: aload_1
      //   140: getstatic 85	com/google/protos/aksara/lattice/LatticeP$Node:PARSER	Lcom/google/protobuf/Parser;
      //   143: aload_2
      //   144: invokevirtual 89	com/google/protobuf/CodedInputStream:readMessage	(Lcom/google/protobuf/Parser;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite;
      //   147: invokeinterface 95 2 0
      //   152: pop
      //   153: goto -130 -> 23
      //   156: astore 7
      //   158: aload 7
      //   160: aload_0
      //   161: invokevirtual 99	com/google/protobuf/InvalidProtocolBufferException:setUnfinishedMessage	(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/InvalidProtocolBufferException;
      //   164: athrow
      //   165: astore 6
      //   167: iload_3
      //   168: iconst_1
      //   169: iand
      //   170: iconst_1
      //   171: if_icmpne +14 -> 185
      //   174: aload_0
      //   175: aload_0
      //   176: getfield 82	com/google/protos/aksara/lattice/LatticeP$Lattice:node_	Ljava/util/List;
      //   179: invokestatic 105	java/util/Collections:unmodifiableList	(Ljava/util/List;)Ljava/util/List;
      //   182: putfield 82	com/google/protos/aksara/lattice/LatticeP$Lattice:node_	Ljava/util/List;
      //   185: iload_3
      //   186: iconst_2
      //   187: iand
      //   188: iconst_2
      //   189: if_icmpne +14 -> 203
      //   192: aload_0
      //   193: aload_0
      //   194: getfield 107	com/google/protos/aksara/lattice/LatticeP$Lattice:costType_	Ljava/util/List;
      //   197: invokestatic 105	java/util/Collections:unmodifiableList	(Ljava/util/List;)Ljava/util/List;
      //   200: putfield 107	com/google/protos/aksara/lattice/LatticeP$Lattice:costType_	Ljava/util/List;
      //   203: iload_3
      //   204: iconst_4
      //   205: iand
      //   206: iconst_4
      //   207: if_icmpne +14 -> 221
      //   210: aload_0
      //   211: aload_0
      //   212: getfield 109	com/google/protos/aksara/lattice/LatticeP$Lattice:path_	Ljava/util/List;
      //   215: invokestatic 105	java/util/Collections:unmodifiableList	(Ljava/util/List;)Ljava/util/List;
      //   218: putfield 109	com/google/protos/aksara/lattice/LatticeP$Lattice:path_	Ljava/util/List;
      //   221: iload_3
      //   222: bipush 8
      //   224: iand
      //   225: bipush 8
      //   227: if_icmpne +14 -> 241
      //   230: aload_0
      //   231: aload_0
      //   232: getfield 111	com/google/protos/aksara/lattice/LatticeP$Lattice:stateType_	Ljava/util/List;
      //   235: invokestatic 105	java/util/Collections:unmodifiableList	(Ljava/util/List;)Ljava/util/List;
      //   238: putfield 111	com/google/protos/aksara/lattice/LatticeP$Lattice:stateType_	Ljava/util/List;
      //   241: aload_0
      //   242: invokevirtual 114	com/google/protos/aksara/lattice/LatticeP$Lattice:makeExtensionsImmutable	()V
      //   245: aload 6
      //   247: athrow
      //   248: iload_3
      //   249: iconst_2
      //   250: iand
      //   251: iconst_2
      //   252: if_icmpeq +18 -> 270
      //   255: aload_0
      //   256: new 79	java/util/ArrayList
      //   259: dup
      //   260: invokespecial 80	java/util/ArrayList:<init>	()V
      //   263: putfield 107	com/google/protos/aksara/lattice/LatticeP$Lattice:costType_	Ljava/util/List;
      //   266: iload_3
      //   267: iconst_2
      //   268: ior
      //   269: istore_3
      //   270: aload_0
      //   271: getfield 107	com/google/protos/aksara/lattice/LatticeP$Lattice:costType_	Ljava/util/List;
      //   274: aload_1
      //   275: getstatic 117	com/google/protos/aksara/lattice/LatticeP$CostType:PARSER	Lcom/google/protobuf/Parser;
      //   278: aload_2
      //   279: invokevirtual 89	com/google/protobuf/CodedInputStream:readMessage	(Lcom/google/protobuf/Parser;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite;
      //   282: invokeinterface 95 2 0
      //   287: pop
      //   288: goto -265 -> 23
      //   291: astore 5
      //   293: new 60	com/google/protobuf/InvalidProtocolBufferException
      //   296: dup
      //   297: aload 5
      //   299: invokevirtual 121	java/io/IOException:getMessage	()Ljava/lang/String;
      //   302: invokespecial 124	com/google/protobuf/InvalidProtocolBufferException:<init>	(Ljava/lang/String;)V
      //   305: aload_0
      //   306: invokevirtual 99	com/google/protobuf/InvalidProtocolBufferException:setUnfinishedMessage	(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/InvalidProtocolBufferException;
      //   309: athrow
      //   310: iload_3
      //   311: iconst_4
      //   312: iand
      //   313: iconst_4
      //   314: if_icmpeq +18 -> 332
      //   317: aload_0
      //   318: new 79	java/util/ArrayList
      //   321: dup
      //   322: invokespecial 80	java/util/ArrayList:<init>	()V
      //   325: putfield 109	com/google/protos/aksara/lattice/LatticeP$Lattice:path_	Ljava/util/List;
      //   328: iload_3
      //   329: iconst_4
      //   330: ior
      //   331: istore_3
      //   332: aload_0
      //   333: getfield 109	com/google/protos/aksara/lattice/LatticeP$Lattice:path_	Ljava/util/List;
      //   336: aload_1
      //   337: getstatic 127	com/google/protos/aksara/lattice/LatticeP$Path:PARSER	Lcom/google/protobuf/Parser;
      //   340: aload_2
      //   341: invokevirtual 89	com/google/protobuf/CodedInputStream:readMessage	(Lcom/google/protobuf/Parser;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite;
      //   344: invokeinterface 95 2 0
      //   349: pop
      //   350: goto -327 -> 23
      //   353: iload_3
      //   354: bipush 8
      //   356: iand
      //   357: bipush 8
      //   359: if_icmpeq +19 -> 378
      //   362: aload_0
      //   363: new 79	java/util/ArrayList
      //   366: dup
      //   367: invokespecial 80	java/util/ArrayList:<init>	()V
      //   370: putfield 111	com/google/protos/aksara/lattice/LatticeP$Lattice:stateType_	Ljava/util/List;
      //   373: iload_3
      //   374: bipush 8
      //   376: ior
      //   377: istore_3
      //   378: aload_0
      //   379: getfield 111	com/google/protos/aksara/lattice/LatticeP$Lattice:stateType_	Ljava/util/List;
      //   382: aload_1
      //   383: getstatic 130	com/google/protos/aksara/lattice/LatticeP$StateType:PARSER	Lcom/google/protobuf/Parser;
      //   386: aload_2
      //   387: invokevirtual 89	com/google/protobuf/CodedInputStream:readMessage	(Lcom/google/protobuf/Parser;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite;
      //   390: invokeinterface 95 2 0
      //   395: pop
      //   396: goto -373 -> 23
      //   399: aload_0
      //   400: iconst_1
      //   401: aload_0
      //   402: getfield 132	com/google/protos/aksara/lattice/LatticeP$Lattice:bitField0_	I
      //   405: ior
      //   406: putfield 132	com/google/protos/aksara/lattice/LatticeP$Lattice:bitField0_	I
      //   409: aload_0
      //   410: aload_1
      //   411: invokevirtual 136	com/google/protobuf/CodedInputStream:readDouble	()D
      //   414: putfield 138	com/google/protos/aksara/lattice/LatticeP$Lattice:maxEdgeFactor_	D
      //   417: goto -394 -> 23
      //   420: iload_3
      //   421: iconst_1
      //   422: iand
      //   423: iconst_1
      //   424: if_icmpne +14 -> 438
      //   427: aload_0
      //   428: aload_0
      //   429: getfield 82	com/google/protos/aksara/lattice/LatticeP$Lattice:node_	Ljava/util/List;
      //   432: invokestatic 105	java/util/Collections:unmodifiableList	(Ljava/util/List;)Ljava/util/List;
      //   435: putfield 82	com/google/protos/aksara/lattice/LatticeP$Lattice:node_	Ljava/util/List;
      //   438: iload_3
      //   439: iconst_2
      //   440: iand
      //   441: iconst_2
      //   442: if_icmpne +14 -> 456
      //   445: aload_0
      //   446: aload_0
      //   447: getfield 107	com/google/protos/aksara/lattice/LatticeP$Lattice:costType_	Ljava/util/List;
      //   450: invokestatic 105	java/util/Collections:unmodifiableList	(Ljava/util/List;)Ljava/util/List;
      //   453: putfield 107	com/google/protos/aksara/lattice/LatticeP$Lattice:costType_	Ljava/util/List;
      //   456: iload_3
      //   457: iconst_4
      //   458: iand
      //   459: iconst_4
      //   460: if_icmpne +14 -> 474
      //   463: aload_0
      //   464: aload_0
      //   465: getfield 109	com/google/protos/aksara/lattice/LatticeP$Lattice:path_	Ljava/util/List;
      //   468: invokestatic 105	java/util/Collections:unmodifiableList	(Ljava/util/List;)Ljava/util/List;
      //   471: putfield 109	com/google/protos/aksara/lattice/LatticeP$Lattice:path_	Ljava/util/List;
      //   474: iload_3
      //   475: bipush 8
      //   477: iand
      //   478: bipush 8
      //   480: if_icmpne +14 -> 494
      //   483: aload_0
      //   484: aload_0
      //   485: getfield 111	com/google/protos/aksara/lattice/LatticeP$Lattice:stateType_	Ljava/util/List;
      //   488: invokestatic 105	java/util/Collections:unmodifiableList	(Ljava/util/List;)Ljava/util/List;
      //   491: putfield 111	com/google/protos/aksara/lattice/LatticeP$Lattice:stateType_	Ljava/util/List;
      //   494: aload_0
      //   495: invokevirtual 114	com/google/protos/aksara/lattice/LatticeP$Lattice:makeExtensionsImmutable	()V
      //   498: return
      //   499: iconst_1
      //   500: istore 4
      //   502: goto -479 -> 23
      //
      // Exception table:
      //   from	to	target	type
      //   28	34	156	com/google/protobuf/InvalidProtocolBufferException
      //   96	107	156	com/google/protobuf/InvalidProtocolBufferException
      //   120	131	156	com/google/protobuf/InvalidProtocolBufferException
      //   135	153	156	com/google/protobuf/InvalidProtocolBufferException
      //   255	266	156	com/google/protobuf/InvalidProtocolBufferException
      //   270	288	156	com/google/protobuf/InvalidProtocolBufferException
      //   317	328	156	com/google/protobuf/InvalidProtocolBufferException
      //   332	350	156	com/google/protobuf/InvalidProtocolBufferException
      //   362	373	156	com/google/protobuf/InvalidProtocolBufferException
      //   378	396	156	com/google/protobuf/InvalidProtocolBufferException
      //   399	417	156	com/google/protobuf/InvalidProtocolBufferException
      //   28	34	165	finally
      //   96	107	165	finally
      //   120	131	165	finally
      //   135	153	165	finally
      //   158	165	165	finally
      //   255	266	165	finally
      //   270	288	165	finally
      //   293	310	165	finally
      //   317	328	165	finally
      //   332	350	165	finally
      //   362	373	165	finally
      //   378	396	165	finally
      //   399	417	165	finally
      //   28	34	291	java/io/IOException
      //   96	107	291	java/io/IOException
      //   120	131	291	java/io/IOException
      //   135	153	291	java/io/IOException
      //   255	266	291	java/io/IOException
      //   270	288	291	java/io/IOException
      //   317	328	291	java/io/IOException
      //   332	350	291	java/io/IOException
      //   362	373	291	java/io/IOException
      //   378	396	291	java/io/IOException
      //   399	417	291	java/io/IOException
    }

    private Lattice(GeneratedMessageLite.ExtendableBuilder<Lattice, ?> paramExtendableBuilder)
    {
      super();
    }

    private Lattice(boolean paramBoolean)
    {
    }

    public static Lattice getDefaultInstance()
    {
      return defaultInstance;
    }

    private void initFields()
    {
      this.node_ = Collections.emptyList();
      this.costType_ = Collections.emptyList();
      this.path_ = Collections.emptyList();
      this.stateType_ = Collections.emptyList();
      this.maxEdgeFactor_ = 0.0D;
    }

    public static Builder newBuilder()
    {
      return Builder.access$5500();
    }

    public static Builder newBuilder(Lattice paramLattice)
    {
      return newBuilder().mergeFrom(paramLattice);
    }

    public static Lattice parseDelimitedFrom(InputStream paramInputStream)
      throws IOException
    {
      return (Lattice)PARSER.parseDelimitedFrom(paramInputStream);
    }

    public static Lattice parseDelimitedFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return (Lattice)PARSER.parseDelimitedFrom(paramInputStream, paramExtensionRegistryLite);
    }

    public static Lattice parseFrom(ByteString paramByteString)
      throws InvalidProtocolBufferException
    {
      return (Lattice)PARSER.parseFrom(paramByteString);
    }

    public static Lattice parseFrom(ByteString paramByteString, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return (Lattice)PARSER.parseFrom(paramByteString, paramExtensionRegistryLite);
    }

    public static Lattice parseFrom(CodedInputStream paramCodedInputStream)
      throws IOException
    {
      return (Lattice)PARSER.parseFrom(paramCodedInputStream);
    }

    public static Lattice parseFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return (Lattice)PARSER.parseFrom(paramCodedInputStream, paramExtensionRegistryLite);
    }

    public static Lattice parseFrom(InputStream paramInputStream)
      throws IOException
    {
      return (Lattice)PARSER.parseFrom(paramInputStream);
    }

    public static Lattice parseFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return (Lattice)PARSER.parseFrom(paramInputStream, paramExtensionRegistryLite);
    }

    public static Lattice parseFrom(byte[] paramArrayOfByte)
      throws InvalidProtocolBufferException
    {
      return (Lattice)PARSER.parseFrom(paramArrayOfByte);
    }

    public static Lattice parseFrom(byte[] paramArrayOfByte, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return (Lattice)PARSER.parseFrom(paramArrayOfByte, paramExtensionRegistryLite);
    }

    public LatticeP.CostType getCostType(int paramInt)
    {
      return (LatticeP.CostType)this.costType_.get(paramInt);
    }

    public int getCostTypeCount()
    {
      return this.costType_.size();
    }

    public List<LatticeP.CostType> getCostTypeList()
    {
      return this.costType_;
    }

    public LatticeP.CostTypeOrBuilder getCostTypeOrBuilder(int paramInt)
    {
      return (LatticeP.CostTypeOrBuilder)this.costType_.get(paramInt);
    }

    public List<? extends LatticeP.CostTypeOrBuilder> getCostTypeOrBuilderList()
    {
      return this.costType_;
    }

    public Lattice getDefaultInstanceForType()
    {
      return defaultInstance;
    }

    public double getMaxEdgeFactor()
    {
      return this.maxEdgeFactor_;
    }

    public LatticeP.Node getNode(int paramInt)
    {
      return (LatticeP.Node)this.node_.get(paramInt);
    }

    public int getNodeCount()
    {
      return this.node_.size();
    }

    public List<LatticeP.Node> getNodeList()
    {
      return this.node_;
    }

    public LatticeP.NodeOrBuilder getNodeOrBuilder(int paramInt)
    {
      return (LatticeP.NodeOrBuilder)this.node_.get(paramInt);
    }

    public List<? extends LatticeP.NodeOrBuilder> getNodeOrBuilderList()
    {
      return this.node_;
    }

    public Parser<Lattice> getParserForType()
    {
      return PARSER;
    }

    public LatticeP.Path getPath(int paramInt)
    {
      return (LatticeP.Path)this.path_.get(paramInt);
    }

    public int getPathCount()
    {
      return this.path_.size();
    }

    public List<LatticeP.Path> getPathList()
    {
      return this.path_;
    }

    public LatticeP.PathOrBuilder getPathOrBuilder(int paramInt)
    {
      return (LatticeP.PathOrBuilder)this.path_.get(paramInt);
    }

    public List<? extends LatticeP.PathOrBuilder> getPathOrBuilderList()
    {
      return this.path_;
    }

    public int getSerializedSize()
    {
      int i = this.memoizedSerializedSize;
      if (i != -1)
        return i;
      int j = 0;
      for (int k = 0; k < this.node_.size(); k++)
        j += CodedOutputStream.computeMessageSize(1, (MessageLite)this.node_.get(k));
      for (int m = 0; m < this.costType_.size(); m++)
        j += CodedOutputStream.computeMessageSize(2, (MessageLite)this.costType_.get(m));
      for (int n = 0; n < this.path_.size(); n++)
        j += CodedOutputStream.computeMessageSize(3, (MessageLite)this.path_.get(n));
      for (int i1 = 0; i1 < this.stateType_.size(); i1++)
        j += CodedOutputStream.computeMessageSize(4, (MessageLite)this.stateType_.get(i1));
      if ((0x1 & this.bitField0_) == 1)
        j += CodedOutputStream.computeDoubleSize(5, this.maxEdgeFactor_);
      int i2 = j + extensionsSerializedSize();
      this.memoizedSerializedSize = i2;
      return i2;
    }

    public LatticeP.StateType getStateType(int paramInt)
    {
      return (LatticeP.StateType)this.stateType_.get(paramInt);
    }

    public int getStateTypeCount()
    {
      return this.stateType_.size();
    }

    public List<LatticeP.StateType> getStateTypeList()
    {
      return this.stateType_;
    }

    public LatticeP.StateTypeOrBuilder getStateTypeOrBuilder(int paramInt)
    {
      return (LatticeP.StateTypeOrBuilder)this.stateType_.get(paramInt);
    }

    public List<? extends LatticeP.StateTypeOrBuilder> getStateTypeOrBuilderList()
    {
      return this.stateType_;
    }

    public boolean hasMaxEdgeFactor()
    {
      return (0x1 & this.bitField0_) == 1;
    }

    public final boolean isInitialized()
    {
      int i = 1;
      int j = this.memoizedIsInitialized;
      if (j != -1)
      {
        if (j == i);
        while (true)
        {
          return i;
          i = 0;
        }
      }
      for (int k = 0; k < getNodeCount(); k++)
        if (!getNode(k).isInitialized())
        {
          this.memoizedIsInitialized = 0;
          return false;
        }
      for (int m = 0; m < getPathCount(); m++)
        if (!getPath(m).isInitialized())
        {
          this.memoizedIsInitialized = 0;
          return false;
        }
      if (!extensionsAreInitialized())
      {
        this.memoizedIsInitialized = 0;
        return false;
      }
      this.memoizedIsInitialized = i;
      return i;
    }

    public Builder newBuilderForType()
    {
      return newBuilder();
    }

    public Builder toBuilder()
    {
      return newBuilder(this);
    }

    protected Object writeReplace()
      throws ObjectStreamException
    {
      return super.writeReplace();
    }

    public void writeTo(CodedOutputStream paramCodedOutputStream)
      throws IOException
    {
      getSerializedSize();
      GeneratedMessageLite.ExtendableMessage.ExtensionWriter localExtensionWriter = newExtensionWriter();
      for (int i = 0; i < this.node_.size(); i++)
        paramCodedOutputStream.writeMessage(1, (MessageLite)this.node_.get(i));
      for (int j = 0; j < this.costType_.size(); j++)
        paramCodedOutputStream.writeMessage(2, (MessageLite)this.costType_.get(j));
      for (int k = 0; k < this.path_.size(); k++)
        paramCodedOutputStream.writeMessage(3, (MessageLite)this.path_.get(k));
      for (int m = 0; m < this.stateType_.size(); m++)
        paramCodedOutputStream.writeMessage(4, (MessageLite)this.stateType_.get(m));
      if ((0x1 & this.bitField0_) == 1)
        paramCodedOutputStream.writeDouble(5, this.maxEdgeFactor_);
      localExtensionWriter.writeUntil(536870912, paramCodedOutputStream);
    }

    public static final class Builder extends GeneratedMessageLite.ExtendableBuilder<LatticeP.Lattice, Builder>
      implements LatticeP.LatticeOrBuilder
    {
      private int bitField0_;
      private List<LatticeP.CostType> costType_ = Collections.emptyList();
      private double maxEdgeFactor_;
      private List<LatticeP.Node> node_ = Collections.emptyList();
      private List<LatticeP.Path> path_ = Collections.emptyList();
      private List<LatticeP.StateType> stateType_ = Collections.emptyList();

      private Builder()
      {
        maybeForceBuilderInitialization();
      }

      private static Builder create()
      {
        return new Builder();
      }

      private void ensureCostTypeIsMutable()
      {
        if ((0x2 & this.bitField0_) != 2)
        {
          this.costType_ = new ArrayList(this.costType_);
          this.bitField0_ = (0x2 | this.bitField0_);
        }
      }

      private void ensureNodeIsMutable()
      {
        if ((0x1 & this.bitField0_) != 1)
        {
          this.node_ = new ArrayList(this.node_);
          this.bitField0_ = (0x1 | this.bitField0_);
        }
      }

      private void ensurePathIsMutable()
      {
        if ((0x4 & this.bitField0_) != 4)
        {
          this.path_ = new ArrayList(this.path_);
          this.bitField0_ = (0x4 | this.bitField0_);
        }
      }

      private void ensureStateTypeIsMutable()
      {
        if ((0x8 & this.bitField0_) != 8)
        {
          this.stateType_ = new ArrayList(this.stateType_);
          this.bitField0_ = (0x8 | this.bitField0_);
        }
      }

      private void maybeForceBuilderInitialization()
      {
      }

      public Builder addAllCostType(Iterable<? extends LatticeP.CostType> paramIterable)
      {
        ensureCostTypeIsMutable();
        GeneratedMessageLite.ExtendableBuilder.addAll(paramIterable, this.costType_);
        return this;
      }

      public Builder addAllNode(Iterable<? extends LatticeP.Node> paramIterable)
      {
        ensureNodeIsMutable();
        GeneratedMessageLite.ExtendableBuilder.addAll(paramIterable, this.node_);
        return this;
      }

      public Builder addAllPath(Iterable<? extends LatticeP.Path> paramIterable)
      {
        ensurePathIsMutable();
        GeneratedMessageLite.ExtendableBuilder.addAll(paramIterable, this.path_);
        return this;
      }

      public Builder addAllStateType(Iterable<? extends LatticeP.StateType> paramIterable)
      {
        ensureStateTypeIsMutable();
        GeneratedMessageLite.ExtendableBuilder.addAll(paramIterable, this.stateType_);
        return this;
      }

      public Builder addCostType(int paramInt, LatticeP.CostType.Builder paramBuilder)
      {
        ensureCostTypeIsMutable();
        this.costType_.add(paramInt, paramBuilder.build());
        return this;
      }

      public Builder addCostType(int paramInt, LatticeP.CostType paramCostType)
      {
        if (paramCostType == null)
          throw new NullPointerException();
        ensureCostTypeIsMutable();
        this.costType_.add(paramInt, paramCostType);
        return this;
      }

      public Builder addCostType(LatticeP.CostType.Builder paramBuilder)
      {
        ensureCostTypeIsMutable();
        this.costType_.add(paramBuilder.build());
        return this;
      }

      public Builder addCostType(LatticeP.CostType paramCostType)
      {
        if (paramCostType == null)
          throw new NullPointerException();
        ensureCostTypeIsMutable();
        this.costType_.add(paramCostType);
        return this;
      }

      public Builder addNode(int paramInt, LatticeP.Node.Builder paramBuilder)
      {
        ensureNodeIsMutable();
        this.node_.add(paramInt, paramBuilder.build());
        return this;
      }

      public Builder addNode(int paramInt, LatticeP.Node paramNode)
      {
        if (paramNode == null)
          throw new NullPointerException();
        ensureNodeIsMutable();
        this.node_.add(paramInt, paramNode);
        return this;
      }

      public Builder addNode(LatticeP.Node.Builder paramBuilder)
      {
        ensureNodeIsMutable();
        this.node_.add(paramBuilder.build());
        return this;
      }

      public Builder addNode(LatticeP.Node paramNode)
      {
        if (paramNode == null)
          throw new NullPointerException();
        ensureNodeIsMutable();
        this.node_.add(paramNode);
        return this;
      }

      public Builder addPath(int paramInt, LatticeP.Path.Builder paramBuilder)
      {
        ensurePathIsMutable();
        this.path_.add(paramInt, paramBuilder.build());
        return this;
      }

      public Builder addPath(int paramInt, LatticeP.Path paramPath)
      {
        if (paramPath == null)
          throw new NullPointerException();
        ensurePathIsMutable();
        this.path_.add(paramInt, paramPath);
        return this;
      }

      public Builder addPath(LatticeP.Path.Builder paramBuilder)
      {
        ensurePathIsMutable();
        this.path_.add(paramBuilder.build());
        return this;
      }

      public Builder addPath(LatticeP.Path paramPath)
      {
        if (paramPath == null)
          throw new NullPointerException();
        ensurePathIsMutable();
        this.path_.add(paramPath);
        return this;
      }

      public Builder addStateType(int paramInt, LatticeP.StateType.Builder paramBuilder)
      {
        ensureStateTypeIsMutable();
        this.stateType_.add(paramInt, paramBuilder.build());
        return this;
      }

      public Builder addStateType(int paramInt, LatticeP.StateType paramStateType)
      {
        if (paramStateType == null)
          throw new NullPointerException();
        ensureStateTypeIsMutable();
        this.stateType_.add(paramInt, paramStateType);
        return this;
      }

      public Builder addStateType(LatticeP.StateType.Builder paramBuilder)
      {
        ensureStateTypeIsMutable();
        this.stateType_.add(paramBuilder.build());
        return this;
      }

      public Builder addStateType(LatticeP.StateType paramStateType)
      {
        if (paramStateType == null)
          throw new NullPointerException();
        ensureStateTypeIsMutable();
        this.stateType_.add(paramStateType);
        return this;
      }

      public LatticeP.Lattice build()
      {
        LatticeP.Lattice localLattice = buildPartial();
        if (!localLattice.isInitialized())
          throw newUninitializedMessageException(localLattice);
        return localLattice;
      }

      public LatticeP.Lattice buildPartial()
      {
        LatticeP.Lattice localLattice = new LatticeP.Lattice(this, null);
        int i = this.bitField0_;
        if ((0x1 & this.bitField0_) == 1)
        {
          this.node_ = Collections.unmodifiableList(this.node_);
          this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
        }
        LatticeP.Lattice.access$5702(localLattice, this.node_);
        if ((0x2 & this.bitField0_) == 2)
        {
          this.costType_ = Collections.unmodifiableList(this.costType_);
          this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
        }
        LatticeP.Lattice.access$5802(localLattice, this.costType_);
        if ((0x4 & this.bitField0_) == 4)
        {
          this.path_ = Collections.unmodifiableList(this.path_);
          this.bitField0_ = (0xFFFFFFFB & this.bitField0_);
        }
        LatticeP.Lattice.access$5902(localLattice, this.path_);
        if ((0x8 & this.bitField0_) == 8)
        {
          this.stateType_ = Collections.unmodifiableList(this.stateType_);
          this.bitField0_ = (0xFFFFFFF7 & this.bitField0_);
        }
        LatticeP.Lattice.access$6002(localLattice, this.stateType_);
        int j = i & 0x10;
        int k = 0;
        if (j == 16)
          k = 0x0 | 0x1;
        LatticeP.Lattice.access$6102(localLattice, this.maxEdgeFactor_);
        LatticeP.Lattice.access$6202(localLattice, k);
        return localLattice;
      }

      public Builder clear()
      {
        super.clear();
        this.node_ = Collections.emptyList();
        this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
        this.costType_ = Collections.emptyList();
        this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
        this.path_ = Collections.emptyList();
        this.bitField0_ = (0xFFFFFFFB & this.bitField0_);
        this.stateType_ = Collections.emptyList();
        this.bitField0_ = (0xFFFFFFF7 & this.bitField0_);
        this.maxEdgeFactor_ = 0.0D;
        this.bitField0_ = (0xFFFFFFEF & this.bitField0_);
        return this;
      }

      public Builder clearCostType()
      {
        this.costType_ = Collections.emptyList();
        this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
        return this;
      }

      public Builder clearMaxEdgeFactor()
      {
        this.bitField0_ = (0xFFFFFFEF & this.bitField0_);
        this.maxEdgeFactor_ = 0.0D;
        return this;
      }

      public Builder clearNode()
      {
        this.node_ = Collections.emptyList();
        this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
        return this;
      }

      public Builder clearPath()
      {
        this.path_ = Collections.emptyList();
        this.bitField0_ = (0xFFFFFFFB & this.bitField0_);
        return this;
      }

      public Builder clearStateType()
      {
        this.stateType_ = Collections.emptyList();
        this.bitField0_ = (0xFFFFFFF7 & this.bitField0_);
        return this;
      }

      public Builder clone()
      {
        return create().mergeFrom(buildPartial());
      }

      public LatticeP.CostType getCostType(int paramInt)
      {
        return (LatticeP.CostType)this.costType_.get(paramInt);
      }

      public int getCostTypeCount()
      {
        return this.costType_.size();
      }

      public List<LatticeP.CostType> getCostTypeList()
      {
        return Collections.unmodifiableList(this.costType_);
      }

      public LatticeP.Lattice getDefaultInstanceForType()
      {
        return LatticeP.Lattice.getDefaultInstance();
      }

      public double getMaxEdgeFactor()
      {
        return this.maxEdgeFactor_;
      }

      public LatticeP.Node getNode(int paramInt)
      {
        return (LatticeP.Node)this.node_.get(paramInt);
      }

      public int getNodeCount()
      {
        return this.node_.size();
      }

      public List<LatticeP.Node> getNodeList()
      {
        return Collections.unmodifiableList(this.node_);
      }

      public LatticeP.Path getPath(int paramInt)
      {
        return (LatticeP.Path)this.path_.get(paramInt);
      }

      public int getPathCount()
      {
        return this.path_.size();
      }

      public List<LatticeP.Path> getPathList()
      {
        return Collections.unmodifiableList(this.path_);
      }

      public LatticeP.StateType getStateType(int paramInt)
      {
        return (LatticeP.StateType)this.stateType_.get(paramInt);
      }

      public int getStateTypeCount()
      {
        return this.stateType_.size();
      }

      public List<LatticeP.StateType> getStateTypeList()
      {
        return Collections.unmodifiableList(this.stateType_);
      }

      public boolean hasMaxEdgeFactor()
      {
        return (0x10 & this.bitField0_) == 16;
      }

      public final boolean isInitialized()
      {
        int i = 0;
        if (i < getNodeCount())
          if (getNode(i).isInitialized());
        label56: 
        do
        {
          return false;
          i++;
          break;
          for (int j = 0; ; j++)
          {
            if (j >= getPathCount())
              break label56;
            if (!getPath(j).isInitialized())
              break;
          }
        }
        while (!extensionsAreInitialized());
        return true;
      }

      public Builder mergeFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
        throws IOException
      {
        LatticeP.Lattice localLattice1 = null;
        try
        {
          LatticeP.Lattice localLattice2 = (LatticeP.Lattice)LatticeP.Lattice.PARSER.parsePartialFrom(paramCodedInputStream, paramExtensionRegistryLite);
          return this;
        }
        catch (InvalidProtocolBufferException localInvalidProtocolBufferException)
        {
          localLattice1 = (LatticeP.Lattice)localInvalidProtocolBufferException.getUnfinishedMessage();
          throw localInvalidProtocolBufferException;
        }
        finally
        {
          if (localLattice1 != null)
            mergeFrom(localLattice1);
        }
      }

      public Builder mergeFrom(LatticeP.Lattice paramLattice)
      {
        if (paramLattice == LatticeP.Lattice.getDefaultInstance())
          return this;
        if (!paramLattice.node_.isEmpty())
        {
          if (this.node_.isEmpty())
          {
            this.node_ = paramLattice.node_;
            this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
          }
        }
        else
        {
          if (!paramLattice.costType_.isEmpty())
          {
            if (!this.costType_.isEmpty())
              break label225;
            this.costType_ = paramLattice.costType_;
            this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
          }
          label95: if (!paramLattice.path_.isEmpty())
          {
            if (!this.path_.isEmpty())
              break label246;
            this.path_ = paramLattice.path_;
            this.bitField0_ = (0xFFFFFFFB & this.bitField0_);
          }
          label138: if (!paramLattice.stateType_.isEmpty())
          {
            if (!this.stateType_.isEmpty())
              break label267;
            this.stateType_ = paramLattice.stateType_;
            this.bitField0_ = (0xFFFFFFF7 & this.bitField0_);
          }
        }
        while (true)
        {
          if (paramLattice.hasMaxEdgeFactor())
            setMaxEdgeFactor(paramLattice.getMaxEdgeFactor());
          mergeExtensionFields(paramLattice);
          return this;
          ensureNodeIsMutable();
          this.node_.addAll(paramLattice.node_);
          break;
          label225: ensureCostTypeIsMutable();
          this.costType_.addAll(paramLattice.costType_);
          break label95;
          label246: ensurePathIsMutable();
          this.path_.addAll(paramLattice.path_);
          break label138;
          label267: ensureStateTypeIsMutable();
          this.stateType_.addAll(paramLattice.stateType_);
        }
      }

      public Builder removeCostType(int paramInt)
      {
        ensureCostTypeIsMutable();
        this.costType_.remove(paramInt);
        return this;
      }

      public Builder removeNode(int paramInt)
      {
        ensureNodeIsMutable();
        this.node_.remove(paramInt);
        return this;
      }

      public Builder removePath(int paramInt)
      {
        ensurePathIsMutable();
        this.path_.remove(paramInt);
        return this;
      }

      public Builder removeStateType(int paramInt)
      {
        ensureStateTypeIsMutable();
        this.stateType_.remove(paramInt);
        return this;
      }

      public Builder setCostType(int paramInt, LatticeP.CostType.Builder paramBuilder)
      {
        ensureCostTypeIsMutable();
        this.costType_.set(paramInt, paramBuilder.build());
        return this;
      }

      public Builder setCostType(int paramInt, LatticeP.CostType paramCostType)
      {
        if (paramCostType == null)
          throw new NullPointerException();
        ensureCostTypeIsMutable();
        this.costType_.set(paramInt, paramCostType);
        return this;
      }

      public Builder setMaxEdgeFactor(double paramDouble)
      {
        this.bitField0_ = (0x10 | this.bitField0_);
        this.maxEdgeFactor_ = paramDouble;
        return this;
      }

      public Builder setNode(int paramInt, LatticeP.Node.Builder paramBuilder)
      {
        ensureNodeIsMutable();
        this.node_.set(paramInt, paramBuilder.build());
        return this;
      }

      public Builder setNode(int paramInt, LatticeP.Node paramNode)
      {
        if (paramNode == null)
          throw new NullPointerException();
        ensureNodeIsMutable();
        this.node_.set(paramInt, paramNode);
        return this;
      }

      public Builder setPath(int paramInt, LatticeP.Path.Builder paramBuilder)
      {
        ensurePathIsMutable();
        this.path_.set(paramInt, paramBuilder.build());
        return this;
      }

      public Builder setPath(int paramInt, LatticeP.Path paramPath)
      {
        if (paramPath == null)
          throw new NullPointerException();
        ensurePathIsMutable();
        this.path_.set(paramInt, paramPath);
        return this;
      }

      public Builder setStateType(int paramInt, LatticeP.StateType.Builder paramBuilder)
      {
        ensureStateTypeIsMutable();
        this.stateType_.set(paramInt, paramBuilder.build());
        return this;
      }

      public Builder setStateType(int paramInt, LatticeP.StateType paramStateType)
      {
        if (paramStateType == null)
          throw new NullPointerException();
        ensureStateTypeIsMutable();
        this.stateType_.set(paramInt, paramStateType);
        return this;
      }
    }
  }

  public static final class LatticeOptions extends GeneratedMessageLite
    implements LatticeP.LatticeOptionsOrBuilder
  {
    public static final int COMPUTE_NODE_POSTERIORS_FIELD_NUMBER = 3;
    public static final int INCLUDE_EDGE_BOUNDS_FIELD_NUMBER = 16;
    public static final int INCLUDE_NODE_STATE_FIELD_NUMBER = 2;
    public static final int LOG_COST_MULTIPLIER_FIELD_NUMBER = 4;
    public static final int MAX_ABSOLUTE_COST_FIELD_NUMBER = 17;
    public static final int MAX_EDGE_FACTOR_FIELD_NUMBER = 1;
    public static final int MAX_RELATIVE_COST_FIELD_NUMBER = 18;
    public static final int NBEST_FIELD_NUMBER = 15;
    public static Parser<LatticeOptions> PARSER = new AbstractParser()
    {
      public LatticeP.LatticeOptions parsePartialFrom(CodedInputStream paramAnonymousCodedInputStream, ExtensionRegistryLite paramAnonymousExtensionRegistryLite)
        throws InvalidProtocolBufferException
      {
        return new LatticeP.LatticeOptions(paramAnonymousCodedInputStream, paramAnonymousExtensionRegistryLite, null);
      }
    };
    private static final LatticeOptions defaultInstance = new LatticeOptions(true);
    private static final long serialVersionUID;
    private int bitField0_;
    private boolean computeNodePosteriors_;
    private boolean includeEdgeBounds_;
    private boolean includeNodeState_;
    private float logCostMultiplier_;
    private double maxAbsoluteCost_;
    private double maxEdgeFactor_;
    private double maxRelativeCost_;
    private byte memoizedIsInitialized = -1;
    private int memoizedSerializedSize = -1;
    private LatticeP.NBestOptions nbest_;

    static
    {
      defaultInstance.initFields();
    }

    // ERROR //
    private LatticeOptions(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      // Byte code:
      //   0: aload_0
      //   1: invokespecial 69	com/google/protobuf/GeneratedMessageLite:<init>	()V
      //   4: aload_0
      //   5: iconst_m1
      //   6: putfield 71	com/google/protos/aksara/lattice/LatticeP$LatticeOptions:memoizedIsInitialized	B
      //   9: aload_0
      //   10: iconst_m1
      //   11: putfield 73	com/google/protos/aksara/lattice/LatticeP$LatticeOptions:memoizedSerializedSize	I
      //   14: aload_0
      //   15: invokespecial 63	com/google/protos/aksara/lattice/LatticeP$LatticeOptions:initFields	()V
      //   18: iconst_0
      //   19: istore_3
      //   20: iload_3
      //   21: ifne +381 -> 402
      //   24: aload_1
      //   25: invokevirtual 79	com/google/protobuf/CodedInputStream:readTag	()I
      //   28: istore 7
      //   30: iload 7
      //   32: lookupswitch	default:+84->116, 0:+375->407, 9:+100->132, 16:+139->171, 24:+179->211, 37:+200->232, 122:+222->254, 128:+303->335, 137:+325->357, 145:+347->379
      //   117: aload_1
      //   118: aload_2
      //   119: iload 7
      //   121: invokevirtual 83	com/google/protos/aksara/lattice/LatticeP$LatticeOptions:parseUnknownField	(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z
      //   124: ifne -104 -> 20
      //   127: iconst_1
      //   128: istore_3
      //   129: goto -109 -> 20
      //   132: aload_0
      //   133: iconst_1
      //   134: aload_0
      //   135: getfield 85	com/google/protos/aksara/lattice/LatticeP$LatticeOptions:bitField0_	I
      //   138: ior
      //   139: putfield 85	com/google/protos/aksara/lattice/LatticeP$LatticeOptions:bitField0_	I
      //   142: aload_0
      //   143: aload_1
      //   144: invokevirtual 89	com/google/protobuf/CodedInputStream:readDouble	()D
      //   147: putfield 91	com/google/protos/aksara/lattice/LatticeP$LatticeOptions:maxEdgeFactor_	D
      //   150: goto -130 -> 20
      //   153: astore 6
      //   155: aload 6
      //   157: aload_0
      //   158: invokevirtual 95	com/google/protobuf/InvalidProtocolBufferException:setUnfinishedMessage	(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/InvalidProtocolBufferException;
      //   161: athrow
      //   162: astore 5
      //   164: aload_0
      //   165: invokevirtual 98	com/google/protos/aksara/lattice/LatticeP$LatticeOptions:makeExtensionsImmutable	()V
      //   168: aload 5
      //   170: athrow
      //   171: aload_0
      //   172: iconst_2
      //   173: aload_0
      //   174: getfield 85	com/google/protos/aksara/lattice/LatticeP$LatticeOptions:bitField0_	I
      //   177: ior
      //   178: putfield 85	com/google/protos/aksara/lattice/LatticeP$LatticeOptions:bitField0_	I
      //   181: aload_0
      //   182: aload_1
      //   183: invokevirtual 102	com/google/protobuf/CodedInputStream:readBool	()Z
      //   186: putfield 104	com/google/protos/aksara/lattice/LatticeP$LatticeOptions:includeNodeState_	Z
      //   189: goto -169 -> 20
      //   192: astore 4
      //   194: new 66	com/google/protobuf/InvalidProtocolBufferException
      //   197: dup
      //   198: aload 4
      //   200: invokevirtual 108	java/io/IOException:getMessage	()Ljava/lang/String;
      //   203: invokespecial 111	com/google/protobuf/InvalidProtocolBufferException:<init>	(Ljava/lang/String;)V
      //   206: aload_0
      //   207: invokevirtual 95	com/google/protobuf/InvalidProtocolBufferException:setUnfinishedMessage	(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/InvalidProtocolBufferException;
      //   210: athrow
      //   211: aload_0
      //   212: iconst_4
      //   213: aload_0
      //   214: getfield 85	com/google/protos/aksara/lattice/LatticeP$LatticeOptions:bitField0_	I
      //   217: ior
      //   218: putfield 85	com/google/protos/aksara/lattice/LatticeP$LatticeOptions:bitField0_	I
      //   221: aload_0
      //   222: aload_1
      //   223: invokevirtual 102	com/google/protobuf/CodedInputStream:readBool	()Z
      //   226: putfield 113	com/google/protos/aksara/lattice/LatticeP$LatticeOptions:computeNodePosteriors_	Z
      //   229: goto -209 -> 20
      //   232: aload_0
      //   233: bipush 8
      //   235: aload_0
      //   236: getfield 85	com/google/protos/aksara/lattice/LatticeP$LatticeOptions:bitField0_	I
      //   239: ior
      //   240: putfield 85	com/google/protos/aksara/lattice/LatticeP$LatticeOptions:bitField0_	I
      //   243: aload_0
      //   244: aload_1
      //   245: invokevirtual 117	com/google/protobuf/CodedInputStream:readFloat	()F
      //   248: putfield 119	com/google/protos/aksara/lattice/LatticeP$LatticeOptions:logCostMultiplier_	F
      //   251: goto -231 -> 20
      //   254: bipush 16
      //   256: aload_0
      //   257: getfield 85	com/google/protos/aksara/lattice/LatticeP$LatticeOptions:bitField0_	I
      //   260: iand
      //   261: istore 8
      //   263: aconst_null
      //   264: astore 9
      //   266: iload 8
      //   268: bipush 16
      //   270: if_icmpne +12 -> 282
      //   273: aload_0
      //   274: getfield 121	com/google/protos/aksara/lattice/LatticeP$LatticeOptions:nbest_	Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;
      //   277: invokevirtual 127	com/google/protos/aksara/lattice/LatticeP$NBestOptions:toBuilder	()Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;
      //   280: astore 9
      //   282: aload_0
      //   283: aload_1
      //   284: getstatic 128	com/google/protos/aksara/lattice/LatticeP$NBestOptions:PARSER	Lcom/google/protobuf/Parser;
      //   287: aload_2
      //   288: invokevirtual 132	com/google/protobuf/CodedInputStream:readMessage	(Lcom/google/protobuf/Parser;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite;
      //   291: checkcast 123	com/google/protos/aksara/lattice/LatticeP$NBestOptions
      //   294: putfield 121	com/google/protos/aksara/lattice/LatticeP$LatticeOptions:nbest_	Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;
      //   297: aload 9
      //   299: ifnull +22 -> 321
      //   302: aload 9
      //   304: aload_0
      //   305: getfield 121	com/google/protos/aksara/lattice/LatticeP$LatticeOptions:nbest_	Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;
      //   308: invokevirtual 138	com/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder:mergeFrom	(Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;)Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder;
      //   311: pop
      //   312: aload_0
      //   313: aload 9
      //   315: invokevirtual 142	com/google/protos/aksara/lattice/LatticeP$NBestOptions$Builder:buildPartial	()Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;
      //   318: putfield 121	com/google/protos/aksara/lattice/LatticeP$LatticeOptions:nbest_	Lcom/google/protos/aksara/lattice/LatticeP$NBestOptions;
      //   321: aload_0
      //   322: bipush 16
      //   324: aload_0
      //   325: getfield 85	com/google/protos/aksara/lattice/LatticeP$LatticeOptions:bitField0_	I
      //   328: ior
      //   329: putfield 85	com/google/protos/aksara/lattice/LatticeP$LatticeOptions:bitField0_	I
      //   332: goto -312 -> 20
      //   335: aload_0
      //   336: bipush 32
      //   338: aload_0
      //   339: getfield 85	com/google/protos/aksara/lattice/LatticeP$LatticeOptions:bitField0_	I
      //   342: ior
      //   343: putfield 85	com/google/protos/aksara/lattice/LatticeP$LatticeOptions:bitField0_	I
      //   346: aload_0
      //   347: aload_1
      //   348: invokevirtual 102	com/google/protobuf/CodedInputStream:readBool	()Z
      //   351: putfield 144	com/google/protos/aksara/lattice/LatticeP$LatticeOptions:includeEdgeBounds_	Z
      //   354: goto -334 -> 20
      //   357: aload_0
      //   358: bipush 64
      //   360: aload_0
      //   361: getfield 85	com/google/protos/aksara/lattice/LatticeP$LatticeOptions:bitField0_	I
      //   364: ior
      //   365: putfield 85	com/google/protos/aksara/lattice/LatticeP$LatticeOptions:bitField0_	I
      //   368: aload_0
      //   369: aload_1
      //   370: invokevirtual 89	com/google/protobuf/CodedInputStream:readDouble	()D
      //   373: putfield 146	com/google/protos/aksara/lattice/LatticeP$LatticeOptions:maxAbsoluteCost_	D
      //   376: goto -356 -> 20
      //   379: aload_0
      //   380: sipush 128
      //   383: aload_0
      //   384: getfield 85	com/google/protos/aksara/lattice/LatticeP$LatticeOptions:bitField0_	I
      //   387: ior
      //   388: putfield 85	com/google/protos/aksara/lattice/LatticeP$LatticeOptions:bitField0_	I
      //   391: aload_0
      //   392: aload_1
      //   393: invokevirtual 89	com/google/protobuf/CodedInputStream:readDouble	()D
      //   396: putfield 148	com/google/protos/aksara/lattice/LatticeP$LatticeOptions:maxRelativeCost_	D
      //   399: goto -379 -> 20
      //   402: aload_0
      //   403: invokevirtual 98	com/google/protos/aksara/lattice/LatticeP$LatticeOptions:makeExtensionsImmutable	()V
      //   406: return
      //   407: iconst_1
      //   408: istore_3
      //   409: goto -389 -> 20
      //
      // Exception table:
      //   from	to	target	type
      //   24	30	153	com/google/protobuf/InvalidProtocolBufferException
      //   116	127	153	com/google/protobuf/InvalidProtocolBufferException
      //   132	150	153	com/google/protobuf/InvalidProtocolBufferException
      //   171	189	153	com/google/protobuf/InvalidProtocolBufferException
      //   211	229	153	com/google/protobuf/InvalidProtocolBufferException
      //   232	251	153	com/google/protobuf/InvalidProtocolBufferException
      //   254	263	153	com/google/protobuf/InvalidProtocolBufferException
      //   273	282	153	com/google/protobuf/InvalidProtocolBufferException
      //   282	297	153	com/google/protobuf/InvalidProtocolBufferException
      //   302	321	153	com/google/protobuf/InvalidProtocolBufferException
      //   321	332	153	com/google/protobuf/InvalidProtocolBufferException
      //   335	354	153	com/google/protobuf/InvalidProtocolBufferException
      //   357	376	153	com/google/protobuf/InvalidProtocolBufferException
      //   379	399	153	com/google/protobuf/InvalidProtocolBufferException
      //   24	30	162	finally
      //   116	127	162	finally
      //   132	150	162	finally
      //   155	162	162	finally
      //   171	189	162	finally
      //   194	211	162	finally
      //   211	229	162	finally
      //   232	251	162	finally
      //   254	263	162	finally
      //   273	282	162	finally
      //   282	297	162	finally
      //   302	321	162	finally
      //   321	332	162	finally
      //   335	354	162	finally
      //   357	376	162	finally
      //   379	399	162	finally
      //   24	30	192	java/io/IOException
      //   116	127	192	java/io/IOException
      //   132	150	192	java/io/IOException
      //   171	189	192	java/io/IOException
      //   211	229	192	java/io/IOException
      //   232	251	192	java/io/IOException
      //   254	263	192	java/io/IOException
      //   273	282	192	java/io/IOException
      //   282	297	192	java/io/IOException
      //   302	321	192	java/io/IOException
      //   321	332	192	java/io/IOException
      //   335	354	192	java/io/IOException
      //   357	376	192	java/io/IOException
      //   379	399	192	java/io/IOException
    }

    private LatticeOptions(GeneratedMessageLite.Builder paramBuilder)
    {
      super();
    }

    private LatticeOptions(boolean paramBoolean)
    {
    }

    public static LatticeOptions getDefaultInstance()
    {
      return defaultInstance;
    }

    private void initFields()
    {
      this.maxEdgeFactor_ = (1.0D / 0.0D);
      this.includeNodeState_ = false;
      this.computeNodePosteriors_ = false;
      this.logCostMultiplier_ = 1.0F;
      this.nbest_ = LatticeP.NBestOptions.getDefaultInstance();
      this.includeEdgeBounds_ = false;
      this.maxAbsoluteCost_ = (1.0D / 0.0D);
      this.maxRelativeCost_ = (1.0D / 0.0D);
    }

    public static Builder newBuilder()
    {
      return Builder.access$7300();
    }

    public static Builder newBuilder(LatticeOptions paramLatticeOptions)
    {
      return newBuilder().mergeFrom(paramLatticeOptions);
    }

    public static LatticeOptions parseDelimitedFrom(InputStream paramInputStream)
      throws IOException
    {
      return (LatticeOptions)PARSER.parseDelimitedFrom(paramInputStream);
    }

    public static LatticeOptions parseDelimitedFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return (LatticeOptions)PARSER.parseDelimitedFrom(paramInputStream, paramExtensionRegistryLite);
    }

    public static LatticeOptions parseFrom(ByteString paramByteString)
      throws InvalidProtocolBufferException
    {
      return (LatticeOptions)PARSER.parseFrom(paramByteString);
    }

    public static LatticeOptions parseFrom(ByteString paramByteString, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return (LatticeOptions)PARSER.parseFrom(paramByteString, paramExtensionRegistryLite);
    }

    public static LatticeOptions parseFrom(CodedInputStream paramCodedInputStream)
      throws IOException
    {
      return (LatticeOptions)PARSER.parseFrom(paramCodedInputStream);
    }

    public static LatticeOptions parseFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return (LatticeOptions)PARSER.parseFrom(paramCodedInputStream, paramExtensionRegistryLite);
    }

    public static LatticeOptions parseFrom(InputStream paramInputStream)
      throws IOException
    {
      return (LatticeOptions)PARSER.parseFrom(paramInputStream);
    }

    public static LatticeOptions parseFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return (LatticeOptions)PARSER.parseFrom(paramInputStream, paramExtensionRegistryLite);
    }

    public static LatticeOptions parseFrom(byte[] paramArrayOfByte)
      throws InvalidProtocolBufferException
    {
      return (LatticeOptions)PARSER.parseFrom(paramArrayOfByte);
    }

    public static LatticeOptions parseFrom(byte[] paramArrayOfByte, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return (LatticeOptions)PARSER.parseFrom(paramArrayOfByte, paramExtensionRegistryLite);
    }

    public boolean getComputeNodePosteriors()
    {
      return this.computeNodePosteriors_;
    }

    public LatticeOptions getDefaultInstanceForType()
    {
      return defaultInstance;
    }

    public boolean getIncludeEdgeBounds()
    {
      return this.includeEdgeBounds_;
    }

    public boolean getIncludeNodeState()
    {
      return this.includeNodeState_;
    }

    public float getLogCostMultiplier()
    {
      return this.logCostMultiplier_;
    }

    public double getMaxAbsoluteCost()
    {
      return this.maxAbsoluteCost_;
    }

    public double getMaxEdgeFactor()
    {
      return this.maxEdgeFactor_;
    }

    public double getMaxRelativeCost()
    {
      return this.maxRelativeCost_;
    }

    public LatticeP.NBestOptions getNbest()
    {
      return this.nbest_;
    }

    public Parser<LatticeOptions> getParserForType()
    {
      return PARSER;
    }

    public int getSerializedSize()
    {
      int i = this.memoizedSerializedSize;
      if (i != -1)
        return i;
      int j = 0x1 & this.bitField0_;
      int k = 0;
      if (j == 1)
        k = 0 + CodedOutputStream.computeDoubleSize(1, this.maxEdgeFactor_);
      if ((0x2 & this.bitField0_) == 2)
        k += CodedOutputStream.computeBoolSize(2, this.includeNodeState_);
      if ((0x4 & this.bitField0_) == 4)
        k += CodedOutputStream.computeBoolSize(3, this.computeNodePosteriors_);
      if ((0x8 & this.bitField0_) == 8)
        k += CodedOutputStream.computeFloatSize(4, this.logCostMultiplier_);
      if ((0x10 & this.bitField0_) == 16)
        k += CodedOutputStream.computeMessageSize(15, this.nbest_);
      if ((0x20 & this.bitField0_) == 32)
        k += CodedOutputStream.computeBoolSize(16, this.includeEdgeBounds_);
      if ((0x40 & this.bitField0_) == 64)
        k += CodedOutputStream.computeDoubleSize(17, this.maxAbsoluteCost_);
      if ((0x80 & this.bitField0_) == 128)
        k += CodedOutputStream.computeDoubleSize(18, this.maxRelativeCost_);
      this.memoizedSerializedSize = k;
      return k;
    }

    public boolean hasComputeNodePosteriors()
    {
      return (0x4 & this.bitField0_) == 4;
    }

    public boolean hasIncludeEdgeBounds()
    {
      return (0x20 & this.bitField0_) == 32;
    }

    public boolean hasIncludeNodeState()
    {
      return (0x2 & this.bitField0_) == 2;
    }

    public boolean hasLogCostMultiplier()
    {
      return (0x8 & this.bitField0_) == 8;
    }

    public boolean hasMaxAbsoluteCost()
    {
      return (0x40 & this.bitField0_) == 64;
    }

    public boolean hasMaxEdgeFactor()
    {
      return (0x1 & this.bitField0_) == 1;
    }

    public boolean hasMaxRelativeCost()
    {
      return (0x80 & this.bitField0_) == 128;
    }

    public boolean hasNbest()
    {
      return (0x10 & this.bitField0_) == 16;
    }

    public final boolean isInitialized()
    {
      int i = this.memoizedIsInitialized;
      if (i != -1)
        return i == 1;
      this.memoizedIsInitialized = 1;
      return true;
    }

    public Builder newBuilderForType()
    {
      return newBuilder();
    }

    public Builder toBuilder()
    {
      return newBuilder(this);
    }

    protected Object writeReplace()
      throws ObjectStreamException
    {
      return super.writeReplace();
    }

    public void writeTo(CodedOutputStream paramCodedOutputStream)
      throws IOException
    {
      getSerializedSize();
      if ((0x1 & this.bitField0_) == 1)
        paramCodedOutputStream.writeDouble(1, this.maxEdgeFactor_);
      if ((0x2 & this.bitField0_) == 2)
        paramCodedOutputStream.writeBool(2, this.includeNodeState_);
      if ((0x4 & this.bitField0_) == 4)
        paramCodedOutputStream.writeBool(3, this.computeNodePosteriors_);
      if ((0x8 & this.bitField0_) == 8)
        paramCodedOutputStream.writeFloat(4, this.logCostMultiplier_);
      if ((0x10 & this.bitField0_) == 16)
        paramCodedOutputStream.writeMessage(15, this.nbest_);
      if ((0x20 & this.bitField0_) == 32)
        paramCodedOutputStream.writeBool(16, this.includeEdgeBounds_);
      if ((0x40 & this.bitField0_) == 64)
        paramCodedOutputStream.writeDouble(17, this.maxAbsoluteCost_);
      if ((0x80 & this.bitField0_) == 128)
        paramCodedOutputStream.writeDouble(18, this.maxRelativeCost_);
    }

    public static final class Builder extends GeneratedMessageLite.Builder<LatticeP.LatticeOptions, Builder>
      implements LatticeP.LatticeOptionsOrBuilder
    {
      private int bitField0_;
      private boolean computeNodePosteriors_;
      private boolean includeEdgeBounds_;
      private boolean includeNodeState_;
      private float logCostMultiplier_ = 1.0F;
      private double maxAbsoluteCost_ = (1.0D / 0.0D);
      private double maxEdgeFactor_ = (1.0D / 0.0D);
      private double maxRelativeCost_ = (1.0D / 0.0D);
      private LatticeP.NBestOptions nbest_ = LatticeP.NBestOptions.getDefaultInstance();

      private Builder()
      {
        maybeForceBuilderInitialization();
      }

      private static Builder create()
      {
        return new Builder();
      }

      private void maybeForceBuilderInitialization()
      {
      }

      public LatticeP.LatticeOptions build()
      {
        LatticeP.LatticeOptions localLatticeOptions = buildPartial();
        if (!localLatticeOptions.isInitialized())
          throw newUninitializedMessageException(localLatticeOptions);
        return localLatticeOptions;
      }

      public LatticeP.LatticeOptions buildPartial()
      {
        LatticeP.LatticeOptions localLatticeOptions = new LatticeP.LatticeOptions(this, null);
        int i = this.bitField0_;
        int j = i & 0x1;
        int k = 0;
        if (j == 1)
          k = 0x0 | 0x1;
        LatticeP.LatticeOptions.access$7502(localLatticeOptions, this.maxEdgeFactor_);
        if ((i & 0x2) == 2)
          k |= 2;
        LatticeP.LatticeOptions.access$7602(localLatticeOptions, this.includeNodeState_);
        if ((i & 0x4) == 4)
          k |= 4;
        LatticeP.LatticeOptions.access$7702(localLatticeOptions, this.computeNodePosteriors_);
        if ((i & 0x8) == 8)
          k |= 8;
        LatticeP.LatticeOptions.access$7802(localLatticeOptions, this.logCostMultiplier_);
        if ((i & 0x10) == 16)
          k |= 16;
        LatticeP.LatticeOptions.access$7902(localLatticeOptions, this.nbest_);
        if ((i & 0x20) == 32)
          k |= 32;
        LatticeP.LatticeOptions.access$8002(localLatticeOptions, this.includeEdgeBounds_);
        if ((i & 0x40) == 64)
          k |= 64;
        LatticeP.LatticeOptions.access$8102(localLatticeOptions, this.maxAbsoluteCost_);
        if ((i & 0x80) == 128)
          k |= 128;
        LatticeP.LatticeOptions.access$8202(localLatticeOptions, this.maxRelativeCost_);
        LatticeP.LatticeOptions.access$8302(localLatticeOptions, k);
        return localLatticeOptions;
      }

      public Builder clear()
      {
        super.clear();
        this.maxEdgeFactor_ = (1.0D / 0.0D);
        this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
        this.includeNodeState_ = false;
        this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
        this.computeNodePosteriors_ = false;
        this.bitField0_ = (0xFFFFFFFB & this.bitField0_);
        this.logCostMultiplier_ = 1.0F;
        this.bitField0_ = (0xFFFFFFF7 & this.bitField0_);
        this.nbest_ = LatticeP.NBestOptions.getDefaultInstance();
        this.bitField0_ = (0xFFFFFFEF & this.bitField0_);
        this.includeEdgeBounds_ = false;
        this.bitField0_ = (0xFFFFFFDF & this.bitField0_);
        this.maxAbsoluteCost_ = (1.0D / 0.0D);
        this.bitField0_ = (0xFFFFFFBF & this.bitField0_);
        this.maxRelativeCost_ = (1.0D / 0.0D);
        this.bitField0_ = (0xFFFFFF7F & this.bitField0_);
        return this;
      }

      public Builder clearComputeNodePosteriors()
      {
        this.bitField0_ = (0xFFFFFFFB & this.bitField0_);
        this.computeNodePosteriors_ = false;
        return this;
      }

      public Builder clearIncludeEdgeBounds()
      {
        this.bitField0_ = (0xFFFFFFDF & this.bitField0_);
        this.includeEdgeBounds_ = false;
        return this;
      }

      public Builder clearIncludeNodeState()
      {
        this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
        this.includeNodeState_ = false;
        return this;
      }

      public Builder clearLogCostMultiplier()
      {
        this.bitField0_ = (0xFFFFFFF7 & this.bitField0_);
        this.logCostMultiplier_ = 1.0F;
        return this;
      }

      public Builder clearMaxAbsoluteCost()
      {
        this.bitField0_ = (0xFFFFFFBF & this.bitField0_);
        this.maxAbsoluteCost_ = (1.0D / 0.0D);
        return this;
      }

      public Builder clearMaxEdgeFactor()
      {
        this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
        this.maxEdgeFactor_ = (1.0D / 0.0D);
        return this;
      }

      public Builder clearMaxRelativeCost()
      {
        this.bitField0_ = (0xFFFFFF7F & this.bitField0_);
        this.maxRelativeCost_ = (1.0D / 0.0D);
        return this;
      }

      public Builder clearNbest()
      {
        this.nbest_ = LatticeP.NBestOptions.getDefaultInstance();
        this.bitField0_ = (0xFFFFFFEF & this.bitField0_);
        return this;
      }

      public Builder clone()
      {
        return create().mergeFrom(buildPartial());
      }

      public boolean getComputeNodePosteriors()
      {
        return this.computeNodePosteriors_;
      }

      public LatticeP.LatticeOptions getDefaultInstanceForType()
      {
        return LatticeP.LatticeOptions.getDefaultInstance();
      }

      public boolean getIncludeEdgeBounds()
      {
        return this.includeEdgeBounds_;
      }

      public boolean getIncludeNodeState()
      {
        return this.includeNodeState_;
      }

      public float getLogCostMultiplier()
      {
        return this.logCostMultiplier_;
      }

      public double getMaxAbsoluteCost()
      {
        return this.maxAbsoluteCost_;
      }

      public double getMaxEdgeFactor()
      {
        return this.maxEdgeFactor_;
      }

      public double getMaxRelativeCost()
      {
        return this.maxRelativeCost_;
      }

      public LatticeP.NBestOptions getNbest()
      {
        return this.nbest_;
      }

      public boolean hasComputeNodePosteriors()
      {
        return (0x4 & this.bitField0_) == 4;
      }

      public boolean hasIncludeEdgeBounds()
      {
        return (0x20 & this.bitField0_) == 32;
      }

      public boolean hasIncludeNodeState()
      {
        return (0x2 & this.bitField0_) == 2;
      }

      public boolean hasLogCostMultiplier()
      {
        return (0x8 & this.bitField0_) == 8;
      }

      public boolean hasMaxAbsoluteCost()
      {
        return (0x40 & this.bitField0_) == 64;
      }

      public boolean hasMaxEdgeFactor()
      {
        return (0x1 & this.bitField0_) == 1;
      }

      public boolean hasMaxRelativeCost()
      {
        return (0x80 & this.bitField0_) == 128;
      }

      public boolean hasNbest()
      {
        return (0x10 & this.bitField0_) == 16;
      }

      public final boolean isInitialized()
      {
        return true;
      }

      public Builder mergeFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
        throws IOException
      {
        LatticeP.LatticeOptions localLatticeOptions1 = null;
        try
        {
          LatticeP.LatticeOptions localLatticeOptions2 = (LatticeP.LatticeOptions)LatticeP.LatticeOptions.PARSER.parsePartialFrom(paramCodedInputStream, paramExtensionRegistryLite);
          return this;
        }
        catch (InvalidProtocolBufferException localInvalidProtocolBufferException)
        {
          localLatticeOptions1 = (LatticeP.LatticeOptions)localInvalidProtocolBufferException.getUnfinishedMessage();
          throw localInvalidProtocolBufferException;
        }
        finally
        {
          if (localLatticeOptions1 != null)
            mergeFrom(localLatticeOptions1);
        }
      }

      public Builder mergeFrom(LatticeP.LatticeOptions paramLatticeOptions)
      {
        if (paramLatticeOptions == LatticeP.LatticeOptions.getDefaultInstance());
        do
        {
          return this;
          if (paramLatticeOptions.hasMaxEdgeFactor())
            setMaxEdgeFactor(paramLatticeOptions.getMaxEdgeFactor());
          if (paramLatticeOptions.hasIncludeNodeState())
            setIncludeNodeState(paramLatticeOptions.getIncludeNodeState());
          if (paramLatticeOptions.hasComputeNodePosteriors())
            setComputeNodePosteriors(paramLatticeOptions.getComputeNodePosteriors());
          if (paramLatticeOptions.hasLogCostMultiplier())
            setLogCostMultiplier(paramLatticeOptions.getLogCostMultiplier());
          if (paramLatticeOptions.hasNbest())
            mergeNbest(paramLatticeOptions.getNbest());
          if (paramLatticeOptions.hasIncludeEdgeBounds())
            setIncludeEdgeBounds(paramLatticeOptions.getIncludeEdgeBounds());
          if (paramLatticeOptions.hasMaxAbsoluteCost())
            setMaxAbsoluteCost(paramLatticeOptions.getMaxAbsoluteCost());
        }
        while (!paramLatticeOptions.hasMaxRelativeCost());
        setMaxRelativeCost(paramLatticeOptions.getMaxRelativeCost());
        return this;
      }

      public Builder mergeNbest(LatticeP.NBestOptions paramNBestOptions)
      {
        if (((0x10 & this.bitField0_) == 16) && (this.nbest_ != LatticeP.NBestOptions.getDefaultInstance()));
        for (this.nbest_ = LatticeP.NBestOptions.newBuilder(this.nbest_).mergeFrom(paramNBestOptions).buildPartial(); ; this.nbest_ = paramNBestOptions)
        {
          this.bitField0_ = (0x10 | this.bitField0_);
          return this;
        }
      }

      public Builder setComputeNodePosteriors(boolean paramBoolean)
      {
        this.bitField0_ = (0x4 | this.bitField0_);
        this.computeNodePosteriors_ = paramBoolean;
        return this;
      }

      public Builder setIncludeEdgeBounds(boolean paramBoolean)
      {
        this.bitField0_ = (0x20 | this.bitField0_);
        this.includeEdgeBounds_ = paramBoolean;
        return this;
      }

      public Builder setIncludeNodeState(boolean paramBoolean)
      {
        this.bitField0_ = (0x2 | this.bitField0_);
        this.includeNodeState_ = paramBoolean;
        return this;
      }

      public Builder setLogCostMultiplier(float paramFloat)
      {
        this.bitField0_ = (0x8 | this.bitField0_);
        this.logCostMultiplier_ = paramFloat;
        return this;
      }

      public Builder setMaxAbsoluteCost(double paramDouble)
      {
        this.bitField0_ = (0x40 | this.bitField0_);
        this.maxAbsoluteCost_ = paramDouble;
        return this;
      }

      public Builder setMaxEdgeFactor(double paramDouble)
      {
        this.bitField0_ = (0x1 | this.bitField0_);
        this.maxEdgeFactor_ = paramDouble;
        return this;
      }

      public Builder setMaxRelativeCost(double paramDouble)
      {
        this.bitField0_ = (0x80 | this.bitField0_);
        this.maxRelativeCost_ = paramDouble;
        return this;
      }

      public Builder setNbest(LatticeP.NBestOptions.Builder paramBuilder)
      {
        this.nbest_ = paramBuilder.build();
        this.bitField0_ = (0x10 | this.bitField0_);
        return this;
      }

      public Builder setNbest(LatticeP.NBestOptions paramNBestOptions)
      {
        if (paramNBestOptions == null)
          throw new NullPointerException();
        this.nbest_ = paramNBestOptions;
        this.bitField0_ = (0x10 | this.bitField0_);
        return this;
      }
    }
  }

  public static abstract interface LatticeOptionsOrBuilder extends MessageLiteOrBuilder
  {
    public abstract boolean getComputeNodePosteriors();

    public abstract boolean getIncludeEdgeBounds();

    public abstract boolean getIncludeNodeState();

    public abstract float getLogCostMultiplier();

    public abstract double getMaxAbsoluteCost();

    public abstract double getMaxEdgeFactor();

    public abstract double getMaxRelativeCost();

    public abstract LatticeP.NBestOptions getNbest();

    public abstract boolean hasComputeNodePosteriors();

    public abstract boolean hasIncludeEdgeBounds();

    public abstract boolean hasIncludeNodeState();

    public abstract boolean hasLogCostMultiplier();

    public abstract boolean hasMaxAbsoluteCost();

    public abstract boolean hasMaxEdgeFactor();

    public abstract boolean hasMaxRelativeCost();

    public abstract boolean hasNbest();
  }

  public static abstract interface LatticeOrBuilder extends GeneratedMessageLite.ExtendableMessageOrBuilder<LatticeP.Lattice>
  {
    public abstract LatticeP.CostType getCostType(int paramInt);

    public abstract int getCostTypeCount();

    public abstract List<LatticeP.CostType> getCostTypeList();

    public abstract double getMaxEdgeFactor();

    public abstract LatticeP.Node getNode(int paramInt);

    public abstract int getNodeCount();

    public abstract List<LatticeP.Node> getNodeList();

    public abstract LatticeP.Path getPath(int paramInt);

    public abstract int getPathCount();

    public abstract List<LatticeP.Path> getPathList();

    public abstract LatticeP.StateType getStateType(int paramInt);

    public abstract int getStateTypeCount();

    public abstract List<LatticeP.StateType> getStateTypeList();

    public abstract boolean hasMaxEdgeFactor();
  }

  public static final class NBestOptions extends GeneratedMessageLite
    implements LatticeP.NBestOptionsOrBuilder
  {
    public static final int INCLUDE_COSTS_FIELD_NUMBER = 5;
    public static final int INCLUDE_EDGES_FIELD_NUMBER = 3;
    public static final int INCLUDE_LABELS_FIELD_NUMBER = 4;
    public static final int NUM_PATHS_FIELD_NUMBER = 1;
    public static Parser<NBestOptions> PARSER = new AbstractParser()
    {
      public LatticeP.NBestOptions parsePartialFrom(CodedInputStream paramAnonymousCodedInputStream, ExtensionRegistryLite paramAnonymousExtensionRegistryLite)
        throws InvalidProtocolBufferException
      {
        return new LatticeP.NBestOptions(paramAnonymousCodedInputStream, paramAnonymousExtensionRegistryLite, null);
      }
    };
    public static final int UNIQUE_TEXT_FIELD_NUMBER = 2;
    private static final NBestOptions defaultInstance = new NBestOptions(true);
    private static final long serialVersionUID;
    private int bitField0_;
    private boolean includeCosts_;
    private boolean includeEdges_;
    private boolean includeLabels_;
    private byte memoizedIsInitialized = -1;
    private int memoizedSerializedSize = -1;
    private int numPaths_;
    private boolean uniqueText_;

    static
    {
      defaultInstance.initFields();
    }

    // ERROR //
    private NBestOptions(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      // Byte code:
      //   0: aload_0
      //   1: invokespecial 57	com/google/protobuf/GeneratedMessageLite:<init>	()V
      //   4: aload_0
      //   5: iconst_m1
      //   6: putfield 59	com/google/protos/aksara/lattice/LatticeP$NBestOptions:memoizedIsInitialized	B
      //   9: aload_0
      //   10: iconst_m1
      //   11: putfield 61	com/google/protos/aksara/lattice/LatticeP$NBestOptions:memoizedSerializedSize	I
      //   14: aload_0
      //   15: invokespecial 51	com/google/protos/aksara/lattice/LatticeP$NBestOptions:initFields	()V
      //   18: iconst_0
      //   19: istore_3
      //   20: iload_3
      //   21: ifne +231 -> 252
      //   24: aload_1
      //   25: invokevirtual 67	com/google/protobuf/CodedInputStream:readTag	()I
      //   28: istore 7
      //   30: iload 7
      //   32: lookupswitch	default:+60->92, 0:+225->257, 8:+76->108, 16:+115->147, 24:+155->187, 32:+176->208, 40:+198->230
      //   93: aload_1
      //   94: aload_2
      //   95: iload 7
      //   97: invokevirtual 71	com/google/protos/aksara/lattice/LatticeP$NBestOptions:parseUnknownField	(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z
      //   100: ifne -80 -> 20
      //   103: iconst_1
      //   104: istore_3
      //   105: goto -85 -> 20
      //   108: aload_0
      //   109: iconst_1
      //   110: aload_0
      //   111: getfield 73	com/google/protos/aksara/lattice/LatticeP$NBestOptions:bitField0_	I
      //   114: ior
      //   115: putfield 73	com/google/protos/aksara/lattice/LatticeP$NBestOptions:bitField0_	I
      //   118: aload_0
      //   119: aload_1
      //   120: invokevirtual 76	com/google/protobuf/CodedInputStream:readInt32	()I
      //   123: putfield 78	com/google/protos/aksara/lattice/LatticeP$NBestOptions:numPaths_	I
      //   126: goto -106 -> 20
      //   129: astore 6
      //   131: aload 6
      //   133: aload_0
      //   134: invokevirtual 82	com/google/protobuf/InvalidProtocolBufferException:setUnfinishedMessage	(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/InvalidProtocolBufferException;
      //   137: athrow
      //   138: astore 5
      //   140: aload_0
      //   141: invokevirtual 85	com/google/protos/aksara/lattice/LatticeP$NBestOptions:makeExtensionsImmutable	()V
      //   144: aload 5
      //   146: athrow
      //   147: aload_0
      //   148: iconst_2
      //   149: aload_0
      //   150: getfield 73	com/google/protos/aksara/lattice/LatticeP$NBestOptions:bitField0_	I
      //   153: ior
      //   154: putfield 73	com/google/protos/aksara/lattice/LatticeP$NBestOptions:bitField0_	I
      //   157: aload_0
      //   158: aload_1
      //   159: invokevirtual 89	com/google/protobuf/CodedInputStream:readBool	()Z
      //   162: putfield 91	com/google/protos/aksara/lattice/LatticeP$NBestOptions:uniqueText_	Z
      //   165: goto -145 -> 20
      //   168: astore 4
      //   170: new 54	com/google/protobuf/InvalidProtocolBufferException
      //   173: dup
      //   174: aload 4
      //   176: invokevirtual 95	java/io/IOException:getMessage	()Ljava/lang/String;
      //   179: invokespecial 98	com/google/protobuf/InvalidProtocolBufferException:<init>	(Ljava/lang/String;)V
      //   182: aload_0
      //   183: invokevirtual 82	com/google/protobuf/InvalidProtocolBufferException:setUnfinishedMessage	(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/InvalidProtocolBufferException;
      //   186: athrow
      //   187: aload_0
      //   188: iconst_4
      //   189: aload_0
      //   190: getfield 73	com/google/protos/aksara/lattice/LatticeP$NBestOptions:bitField0_	I
      //   193: ior
      //   194: putfield 73	com/google/protos/aksara/lattice/LatticeP$NBestOptions:bitField0_	I
      //   197: aload_0
      //   198: aload_1
      //   199: invokevirtual 89	com/google/protobuf/CodedInputStream:readBool	()Z
      //   202: putfield 100	com/google/protos/aksara/lattice/LatticeP$NBestOptions:includeEdges_	Z
      //   205: goto -185 -> 20
      //   208: aload_0
      //   209: bipush 8
      //   211: aload_0
      //   212: getfield 73	com/google/protos/aksara/lattice/LatticeP$NBestOptions:bitField0_	I
      //   215: ior
      //   216: putfield 73	com/google/protos/aksara/lattice/LatticeP$NBestOptions:bitField0_	I
      //   219: aload_0
      //   220: aload_1
      //   221: invokevirtual 89	com/google/protobuf/CodedInputStream:readBool	()Z
      //   224: putfield 102	com/google/protos/aksara/lattice/LatticeP$NBestOptions:includeLabels_	Z
      //   227: goto -207 -> 20
      //   230: aload_0
      //   231: bipush 16
      //   233: aload_0
      //   234: getfield 73	com/google/protos/aksara/lattice/LatticeP$NBestOptions:bitField0_	I
      //   237: ior
      //   238: putfield 73	com/google/protos/aksara/lattice/LatticeP$NBestOptions:bitField0_	I
      //   241: aload_0
      //   242: aload_1
      //   243: invokevirtual 89	com/google/protobuf/CodedInputStream:readBool	()Z
      //   246: putfield 104	com/google/protos/aksara/lattice/LatticeP$NBestOptions:includeCosts_	Z
      //   249: goto -229 -> 20
      //   252: aload_0
      //   253: invokevirtual 85	com/google/protos/aksara/lattice/LatticeP$NBestOptions:makeExtensionsImmutable	()V
      //   256: return
      //   257: iconst_1
      //   258: istore_3
      //   259: goto -239 -> 20
      //
      // Exception table:
      //   from	to	target	type
      //   24	30	129	com/google/protobuf/InvalidProtocolBufferException
      //   92	103	129	com/google/protobuf/InvalidProtocolBufferException
      //   108	126	129	com/google/protobuf/InvalidProtocolBufferException
      //   147	165	129	com/google/protobuf/InvalidProtocolBufferException
      //   187	205	129	com/google/protobuf/InvalidProtocolBufferException
      //   208	227	129	com/google/protobuf/InvalidProtocolBufferException
      //   230	249	129	com/google/protobuf/InvalidProtocolBufferException
      //   24	30	138	finally
      //   92	103	138	finally
      //   108	126	138	finally
      //   131	138	138	finally
      //   147	165	138	finally
      //   170	187	138	finally
      //   187	205	138	finally
      //   208	227	138	finally
      //   230	249	138	finally
      //   24	30	168	java/io/IOException
      //   92	103	168	java/io/IOException
      //   108	126	168	java/io/IOException
      //   147	165	168	java/io/IOException
      //   187	205	168	java/io/IOException
      //   208	227	168	java/io/IOException
      //   230	249	168	java/io/IOException
    }

    private NBestOptions(GeneratedMessageLite.Builder paramBuilder)
    {
      super();
    }

    private NBestOptions(boolean paramBoolean)
    {
    }

    public static NBestOptions getDefaultInstance()
    {
      return defaultInstance;
    }

    private void initFields()
    {
      this.numPaths_ = 0;
      this.uniqueText_ = true;
      this.includeEdges_ = false;
      this.includeLabels_ = false;
      this.includeCosts_ = false;
    }

    public static Builder newBuilder()
    {
      return Builder.access$6400();
    }

    public static Builder newBuilder(NBestOptions paramNBestOptions)
    {
      return newBuilder().mergeFrom(paramNBestOptions);
    }

    public static NBestOptions parseDelimitedFrom(InputStream paramInputStream)
      throws IOException
    {
      return (NBestOptions)PARSER.parseDelimitedFrom(paramInputStream);
    }

    public static NBestOptions parseDelimitedFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return (NBestOptions)PARSER.parseDelimitedFrom(paramInputStream, paramExtensionRegistryLite);
    }

    public static NBestOptions parseFrom(ByteString paramByteString)
      throws InvalidProtocolBufferException
    {
      return (NBestOptions)PARSER.parseFrom(paramByteString);
    }

    public static NBestOptions parseFrom(ByteString paramByteString, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return (NBestOptions)PARSER.parseFrom(paramByteString, paramExtensionRegistryLite);
    }

    public static NBestOptions parseFrom(CodedInputStream paramCodedInputStream)
      throws IOException
    {
      return (NBestOptions)PARSER.parseFrom(paramCodedInputStream);
    }

    public static NBestOptions parseFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return (NBestOptions)PARSER.parseFrom(paramCodedInputStream, paramExtensionRegistryLite);
    }

    public static NBestOptions parseFrom(InputStream paramInputStream)
      throws IOException
    {
      return (NBestOptions)PARSER.parseFrom(paramInputStream);
    }

    public static NBestOptions parseFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return (NBestOptions)PARSER.parseFrom(paramInputStream, paramExtensionRegistryLite);
    }

    public static NBestOptions parseFrom(byte[] paramArrayOfByte)
      throws InvalidProtocolBufferException
    {
      return (NBestOptions)PARSER.parseFrom(paramArrayOfByte);
    }

    public static NBestOptions parseFrom(byte[] paramArrayOfByte, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return (NBestOptions)PARSER.parseFrom(paramArrayOfByte, paramExtensionRegistryLite);
    }

    public NBestOptions getDefaultInstanceForType()
    {
      return defaultInstance;
    }

    public boolean getIncludeCosts()
    {
      return this.includeCosts_;
    }

    public boolean getIncludeEdges()
    {
      return this.includeEdges_;
    }

    public boolean getIncludeLabels()
    {
      return this.includeLabels_;
    }

    public int getNumPaths()
    {
      return this.numPaths_;
    }

    public Parser<NBestOptions> getParserForType()
    {
      return PARSER;
    }

    public int getSerializedSize()
    {
      int i = this.memoizedSerializedSize;
      if (i != -1)
        return i;
      int j = 0x1 & this.bitField0_;
      int k = 0;
      if (j == 1)
        k = 0 + CodedOutputStream.computeInt32Size(1, this.numPaths_);
      if ((0x2 & this.bitField0_) == 2)
        k += CodedOutputStream.computeBoolSize(2, this.uniqueText_);
      if ((0x4 & this.bitField0_) == 4)
        k += CodedOutputStream.computeBoolSize(3, this.includeEdges_);
      if ((0x8 & this.bitField0_) == 8)
        k += CodedOutputStream.computeBoolSize(4, this.includeLabels_);
      if ((0x10 & this.bitField0_) == 16)
        k += CodedOutputStream.computeBoolSize(5, this.includeCosts_);
      this.memoizedSerializedSize = k;
      return k;
    }

    public boolean getUniqueText()
    {
      return this.uniqueText_;
    }

    public boolean hasIncludeCosts()
    {
      return (0x10 & this.bitField0_) == 16;
    }

    public boolean hasIncludeEdges()
    {
      return (0x4 & this.bitField0_) == 4;
    }

    public boolean hasIncludeLabels()
    {
      return (0x8 & this.bitField0_) == 8;
    }

    public boolean hasNumPaths()
    {
      return (0x1 & this.bitField0_) == 1;
    }

    public boolean hasUniqueText()
    {
      return (0x2 & this.bitField0_) == 2;
    }

    public final boolean isInitialized()
    {
      int i = this.memoizedIsInitialized;
      if (i != -1)
        return i == 1;
      this.memoizedIsInitialized = 1;
      return true;
    }

    public Builder newBuilderForType()
    {
      return newBuilder();
    }

    public Builder toBuilder()
    {
      return newBuilder(this);
    }

    protected Object writeReplace()
      throws ObjectStreamException
    {
      return super.writeReplace();
    }

    public void writeTo(CodedOutputStream paramCodedOutputStream)
      throws IOException
    {
      getSerializedSize();
      if ((0x1 & this.bitField0_) == 1)
        paramCodedOutputStream.writeInt32(1, this.numPaths_);
      if ((0x2 & this.bitField0_) == 2)
        paramCodedOutputStream.writeBool(2, this.uniqueText_);
      if ((0x4 & this.bitField0_) == 4)
        paramCodedOutputStream.writeBool(3, this.includeEdges_);
      if ((0x8 & this.bitField0_) == 8)
        paramCodedOutputStream.writeBool(4, this.includeLabels_);
      if ((0x10 & this.bitField0_) == 16)
        paramCodedOutputStream.writeBool(5, this.includeCosts_);
    }

    public static final class Builder extends GeneratedMessageLite.Builder<LatticeP.NBestOptions, Builder>
      implements LatticeP.NBestOptionsOrBuilder
    {
      private int bitField0_;
      private boolean includeCosts_;
      private boolean includeEdges_;
      private boolean includeLabels_;
      private int numPaths_;
      private boolean uniqueText_ = true;

      private Builder()
      {
        maybeForceBuilderInitialization();
      }

      private static Builder create()
      {
        return new Builder();
      }

      private void maybeForceBuilderInitialization()
      {
      }

      public LatticeP.NBestOptions build()
      {
        LatticeP.NBestOptions localNBestOptions = buildPartial();
        if (!localNBestOptions.isInitialized())
          throw newUninitializedMessageException(localNBestOptions);
        return localNBestOptions;
      }

      public LatticeP.NBestOptions buildPartial()
      {
        LatticeP.NBestOptions localNBestOptions = new LatticeP.NBestOptions(this, null);
        int i = this.bitField0_;
        int j = i & 0x1;
        int k = 0;
        if (j == 1)
          k = 0x0 | 0x1;
        LatticeP.NBestOptions.access$6602(localNBestOptions, this.numPaths_);
        if ((i & 0x2) == 2)
          k |= 2;
        LatticeP.NBestOptions.access$6702(localNBestOptions, this.uniqueText_);
        if ((i & 0x4) == 4)
          k |= 4;
        LatticeP.NBestOptions.access$6802(localNBestOptions, this.includeEdges_);
        if ((i & 0x8) == 8)
          k |= 8;
        LatticeP.NBestOptions.access$6902(localNBestOptions, this.includeLabels_);
        if ((i & 0x10) == 16)
          k |= 16;
        LatticeP.NBestOptions.access$7002(localNBestOptions, this.includeCosts_);
        LatticeP.NBestOptions.access$7102(localNBestOptions, k);
        return localNBestOptions;
      }

      public Builder clear()
      {
        super.clear();
        this.numPaths_ = 0;
        this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
        this.uniqueText_ = true;
        this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
        this.includeEdges_ = false;
        this.bitField0_ = (0xFFFFFFFB & this.bitField0_);
        this.includeLabels_ = false;
        this.bitField0_ = (0xFFFFFFF7 & this.bitField0_);
        this.includeCosts_ = false;
        this.bitField0_ = (0xFFFFFFEF & this.bitField0_);
        return this;
      }

      public Builder clearIncludeCosts()
      {
        this.bitField0_ = (0xFFFFFFEF & this.bitField0_);
        this.includeCosts_ = false;
        return this;
      }

      public Builder clearIncludeEdges()
      {
        this.bitField0_ = (0xFFFFFFFB & this.bitField0_);
        this.includeEdges_ = false;
        return this;
      }

      public Builder clearIncludeLabels()
      {
        this.bitField0_ = (0xFFFFFFF7 & this.bitField0_);
        this.includeLabels_ = false;
        return this;
      }

      public Builder clearNumPaths()
      {
        this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
        this.numPaths_ = 0;
        return this;
      }

      public Builder clearUniqueText()
      {
        this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
        this.uniqueText_ = true;
        return this;
      }

      public Builder clone()
      {
        return create().mergeFrom(buildPartial());
      }

      public LatticeP.NBestOptions getDefaultInstanceForType()
      {
        return LatticeP.NBestOptions.getDefaultInstance();
      }

      public boolean getIncludeCosts()
      {
        return this.includeCosts_;
      }

      public boolean getIncludeEdges()
      {
        return this.includeEdges_;
      }

      public boolean getIncludeLabels()
      {
        return this.includeLabels_;
      }

      public int getNumPaths()
      {
        return this.numPaths_;
      }

      public boolean getUniqueText()
      {
        return this.uniqueText_;
      }

      public boolean hasIncludeCosts()
      {
        return (0x10 & this.bitField0_) == 16;
      }

      public boolean hasIncludeEdges()
      {
        return (0x4 & this.bitField0_) == 4;
      }

      public boolean hasIncludeLabels()
      {
        return (0x8 & this.bitField0_) == 8;
      }

      public boolean hasNumPaths()
      {
        return (0x1 & this.bitField0_) == 1;
      }

      public boolean hasUniqueText()
      {
        return (0x2 & this.bitField0_) == 2;
      }

      public final boolean isInitialized()
      {
        return true;
      }

      public Builder mergeFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
        throws IOException
      {
        LatticeP.NBestOptions localNBestOptions1 = null;
        try
        {
          LatticeP.NBestOptions localNBestOptions2 = (LatticeP.NBestOptions)LatticeP.NBestOptions.PARSER.parsePartialFrom(paramCodedInputStream, paramExtensionRegistryLite);
          return this;
        }
        catch (InvalidProtocolBufferException localInvalidProtocolBufferException)
        {
          localNBestOptions1 = (LatticeP.NBestOptions)localInvalidProtocolBufferException.getUnfinishedMessage();
          throw localInvalidProtocolBufferException;
        }
        finally
        {
          if (localNBestOptions1 != null)
            mergeFrom(localNBestOptions1);
        }
      }

      public Builder mergeFrom(LatticeP.NBestOptions paramNBestOptions)
      {
        if (paramNBestOptions == LatticeP.NBestOptions.getDefaultInstance());
        do
        {
          return this;
          if (paramNBestOptions.hasNumPaths())
            setNumPaths(paramNBestOptions.getNumPaths());
          if (paramNBestOptions.hasUniqueText())
            setUniqueText(paramNBestOptions.getUniqueText());
          if (paramNBestOptions.hasIncludeEdges())
            setIncludeEdges(paramNBestOptions.getIncludeEdges());
          if (paramNBestOptions.hasIncludeLabels())
            setIncludeLabels(paramNBestOptions.getIncludeLabels());
        }
        while (!paramNBestOptions.hasIncludeCosts());
        setIncludeCosts(paramNBestOptions.getIncludeCosts());
        return this;
      }

      public Builder setIncludeCosts(boolean paramBoolean)
      {
        this.bitField0_ = (0x10 | this.bitField0_);
        this.includeCosts_ = paramBoolean;
        return this;
      }

      public Builder setIncludeEdges(boolean paramBoolean)
      {
        this.bitField0_ = (0x4 | this.bitField0_);
        this.includeEdges_ = paramBoolean;
        return this;
      }

      public Builder setIncludeLabels(boolean paramBoolean)
      {
        this.bitField0_ = (0x8 | this.bitField0_);
        this.includeLabels_ = paramBoolean;
        return this;
      }

      public Builder setNumPaths(int paramInt)
      {
        this.bitField0_ = (0x1 | this.bitField0_);
        this.numPaths_ = paramInt;
        return this;
      }

      public Builder setUniqueText(boolean paramBoolean)
      {
        this.bitField0_ = (0x2 | this.bitField0_);
        this.uniqueText_ = paramBoolean;
        return this;
      }
    }
  }

  public static abstract interface NBestOptionsOrBuilder extends MessageLiteOrBuilder
  {
    public abstract boolean getIncludeCosts();

    public abstract boolean getIncludeEdges();

    public abstract boolean getIncludeLabels();

    public abstract int getNumPaths();

    public abstract boolean getUniqueText();

    public abstract boolean hasIncludeCosts();

    public abstract boolean hasIncludeEdges();

    public abstract boolean hasIncludeLabels();

    public abstract boolean hasNumPaths();

    public abstract boolean hasUniqueText();
  }

  public static final class Node extends GeneratedMessageLite.ExtendableMessage<Node>
    implements LatticeP.NodeOrBuilder
  {
    public static final int DESCRIPTION_FIELD_NUMBER = 15;
    public static final int EDGE_FIELD_NUMBER = 1;
    public static final int IS_TRANSIENT_FIELD_NUMBER = 16;
    public static Parser<Node> PARSER = new AbstractParser()
    {
      public LatticeP.Node parsePartialFrom(CodedInputStream paramAnonymousCodedInputStream, ExtensionRegistryLite paramAnonymousExtensionRegistryLite)
        throws InvalidProtocolBufferException
      {
        return new LatticeP.Node(paramAnonymousCodedInputStream, paramAnonymousExtensionRegistryLite, null);
      }
    };
    public static final int PIXEL_FIELD_NUMBER = 2;
    public static final int POSTERIOR_FIELD_NUMBER = 4;
    public static final int STATE_FIELD_NUMBER = 3;
    private static final Node defaultInstance = new Node(true);
    private static final long serialVersionUID;
    private int bitField0_;
    private LazyStringList description_;
    private List<LatticeP.Edge> edge_;
    private boolean isTransient_;
    private byte memoizedIsInitialized = -1;
    private int memoizedSerializedSize = -1;
    private int pixel_;
    private float posterior_;
    private List<LatticeP.State> state_;

    static
    {
      defaultInstance.initFields();
    }

    // ERROR //
    private Node(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      // Byte code:
      //   0: aload_0
      //   1: invokespecial 66	com/google/protobuf/GeneratedMessageLite$ExtendableMessage:<init>	()V
      //   4: aload_0
      //   5: iconst_m1
      //   6: putfield 68	com/google/protos/aksara/lattice/LatticeP$Node:memoizedIsInitialized	B
      //   9: aload_0
      //   10: iconst_m1
      //   11: putfield 70	com/google/protos/aksara/lattice/LatticeP$Node:memoizedSerializedSize	I
      //   14: aload_0
      //   15: invokespecial 60	com/google/protos/aksara/lattice/LatticeP$Node:initFields	()V
      //   18: iconst_0
      //   19: istore_3
      //   20: iconst_0
      //   21: istore 4
      //   23: iload 4
      //   25: ifne +388 -> 413
      //   28: aload_1
      //   29: invokevirtual 76	com/google/protobuf/CodedInputStream:readTag	()I
      //   32: istore 8
      //   34: iload 8
      //   36: lookupswitch	default:+68->104, 0:+444->480, 10:+85->121, 16:+208->244, 26:+248->284, 37:+294->330, 122:+315->351, 128:+356->392
      //   105: aload_1
      //   106: aload_2
      //   107: iload 8
      //   109: invokevirtual 80	com/google/protos/aksara/lattice/LatticeP$Node:parseUnknownField	(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z
      //   112: ifne -89 -> 23
      //   115: iconst_1
      //   116: istore 4
      //   118: goto -95 -> 23
      //   121: iload_3
      //   122: iconst_1
      //   123: iand
      //   124: iconst_1
      //   125: if_icmpeq +18 -> 143
      //   128: aload_0
      //   129: new 82	java/util/ArrayList
      //   132: dup
      //   133: invokespecial 83	java/util/ArrayList:<init>	()V
      //   136: putfield 85	com/google/protos/aksara/lattice/LatticeP$Node:edge_	Ljava/util/List;
      //   139: iload_3
      //   140: iconst_1
      //   141: ior
      //   142: istore_3
      //   143: aload_0
      //   144: getfield 85	com/google/protos/aksara/lattice/LatticeP$Node:edge_	Ljava/util/List;
      //   147: aload_1
      //   148: getstatic 88	com/google/protos/aksara/lattice/LatticeP$Edge:PARSER	Lcom/google/protobuf/Parser;
      //   151: aload_2
      //   152: invokevirtual 92	com/google/protobuf/CodedInputStream:readMessage	(Lcom/google/protobuf/Parser;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite;
      //   155: invokeinterface 98 2 0
      //   160: pop
      //   161: goto -138 -> 23
      //   164: astore 7
      //   166: aload 7
      //   168: aload_0
      //   169: invokevirtual 102	com/google/protobuf/InvalidProtocolBufferException:setUnfinishedMessage	(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/InvalidProtocolBufferException;
      //   172: athrow
      //   173: astore 6
      //   175: iload_3
      //   176: iconst_1
      //   177: iand
      //   178: iconst_1
      //   179: if_icmpne +14 -> 193
      //   182: aload_0
      //   183: aload_0
      //   184: getfield 85	com/google/protos/aksara/lattice/LatticeP$Node:edge_	Ljava/util/List;
      //   187: invokestatic 108	java/util/Collections:unmodifiableList	(Ljava/util/List;)Ljava/util/List;
      //   190: putfield 85	com/google/protos/aksara/lattice/LatticeP$Node:edge_	Ljava/util/List;
      //   193: iload_3
      //   194: bipush 8
      //   196: iand
      //   197: bipush 8
      //   199: if_icmpne +14 -> 213
      //   202: aload_0
      //   203: aload_0
      //   204: getfield 110	com/google/protos/aksara/lattice/LatticeP$Node:state_	Ljava/util/List;
      //   207: invokestatic 108	java/util/Collections:unmodifiableList	(Ljava/util/List;)Ljava/util/List;
      //   210: putfield 110	com/google/protos/aksara/lattice/LatticeP$Node:state_	Ljava/util/List;
      //   213: iload_3
      //   214: bipush 32
      //   216: iand
      //   217: bipush 32
      //   219: if_icmpne +18 -> 237
      //   222: aload_0
      //   223: new 112	com/google/protobuf/UnmodifiableLazyStringList
      //   226: dup
      //   227: aload_0
      //   228: getfield 114	com/google/protos/aksara/lattice/LatticeP$Node:description_	Lcom/google/protobuf/LazyStringList;
      //   231: invokespecial 117	com/google/protobuf/UnmodifiableLazyStringList:<init>	(Lcom/google/protobuf/LazyStringList;)V
      //   234: putfield 114	com/google/protos/aksara/lattice/LatticeP$Node:description_	Lcom/google/protobuf/LazyStringList;
      //   237: aload_0
      //   238: invokevirtual 120	com/google/protos/aksara/lattice/LatticeP$Node:makeExtensionsImmutable	()V
      //   241: aload 6
      //   243: athrow
      //   244: aload_0
      //   245: iconst_1
      //   246: aload_0
      //   247: getfield 122	com/google/protos/aksara/lattice/LatticeP$Node:bitField0_	I
      //   250: ior
      //   251: putfield 122	com/google/protos/aksara/lattice/LatticeP$Node:bitField0_	I
      //   254: aload_0
      //   255: aload_1
      //   256: invokevirtual 125	com/google/protobuf/CodedInputStream:readInt32	()I
      //   259: putfield 127	com/google/protos/aksara/lattice/LatticeP$Node:pixel_	I
      //   262: goto -239 -> 23
      //   265: astore 5
      //   267: new 63	com/google/protobuf/InvalidProtocolBufferException
      //   270: dup
      //   271: aload 5
      //   273: invokevirtual 131	java/io/IOException:getMessage	()Ljava/lang/String;
      //   276: invokespecial 134	com/google/protobuf/InvalidProtocolBufferException:<init>	(Ljava/lang/String;)V
      //   279: aload_0
      //   280: invokevirtual 102	com/google/protobuf/InvalidProtocolBufferException:setUnfinishedMessage	(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/InvalidProtocolBufferException;
      //   283: athrow
      //   284: iload_3
      //   285: bipush 8
      //   287: iand
      //   288: bipush 8
      //   290: if_icmpeq +19 -> 309
      //   293: aload_0
      //   294: new 82	java/util/ArrayList
      //   297: dup
      //   298: invokespecial 83	java/util/ArrayList:<init>	()V
      //   301: putfield 110	com/google/protos/aksara/lattice/LatticeP$Node:state_	Ljava/util/List;
      //   304: iload_3
      //   305: bipush 8
      //   307: ior
      //   308: istore_3
      //   309: aload_0
      //   310: getfield 110	com/google/protos/aksara/lattice/LatticeP$Node:state_	Ljava/util/List;
      //   313: aload_1
      //   314: getstatic 137	com/google/protos/aksara/lattice/LatticeP$State:PARSER	Lcom/google/protobuf/Parser;
      //   317: aload_2
      //   318: invokevirtual 92	com/google/protobuf/CodedInputStream:readMessage	(Lcom/google/protobuf/Parser;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite;
      //   321: invokeinterface 98 2 0
      //   326: pop
      //   327: goto -304 -> 23
      //   330: aload_0
      //   331: iconst_4
      //   332: aload_0
      //   333: getfield 122	com/google/protos/aksara/lattice/LatticeP$Node:bitField0_	I
      //   336: ior
      //   337: putfield 122	com/google/protos/aksara/lattice/LatticeP$Node:bitField0_	I
      //   340: aload_0
      //   341: aload_1
      //   342: invokevirtual 141	com/google/protobuf/CodedInputStream:readFloat	()F
      //   345: putfield 143	com/google/protos/aksara/lattice/LatticeP$Node:posterior_	F
      //   348: goto -325 -> 23
      //   351: iload_3
      //   352: bipush 32
      //   354: iand
      //   355: bipush 32
      //   357: if_icmpeq +19 -> 376
      //   360: aload_0
      //   361: new 145	com/google/protobuf/LazyStringArrayList
      //   364: dup
      //   365: invokespecial 146	com/google/protobuf/LazyStringArrayList:<init>	()V
      //   368: putfield 114	com/google/protos/aksara/lattice/LatticeP$Node:description_	Lcom/google/protobuf/LazyStringList;
      //   371: iload_3
      //   372: bipush 32
      //   374: ior
      //   375: istore_3
      //   376: aload_0
      //   377: getfield 114	com/google/protos/aksara/lattice/LatticeP$Node:description_	Lcom/google/protobuf/LazyStringList;
      //   380: aload_1
      //   381: invokevirtual 150	com/google/protobuf/CodedInputStream:readBytes	()Lcom/google/protobuf/ByteString;
      //   384: invokeinterface 155 2 0
      //   389: goto -366 -> 23
      //   392: aload_0
      //   393: iconst_2
      //   394: aload_0
      //   395: getfield 122	com/google/protos/aksara/lattice/LatticeP$Node:bitField0_	I
      //   398: ior
      //   399: putfield 122	com/google/protos/aksara/lattice/LatticeP$Node:bitField0_	I
      //   402: aload_0
      //   403: aload_1
      //   404: invokevirtual 159	com/google/protobuf/CodedInputStream:readBool	()Z
      //   407: putfield 161	com/google/protos/aksara/lattice/LatticeP$Node:isTransient_	Z
      //   410: goto -387 -> 23
      //   413: iload_3
      //   414: iconst_1
      //   415: iand
      //   416: iconst_1
      //   417: if_icmpne +14 -> 431
      //   420: aload_0
      //   421: aload_0
      //   422: getfield 85	com/google/protos/aksara/lattice/LatticeP$Node:edge_	Ljava/util/List;
      //   425: invokestatic 108	java/util/Collections:unmodifiableList	(Ljava/util/List;)Ljava/util/List;
      //   428: putfield 85	com/google/protos/aksara/lattice/LatticeP$Node:edge_	Ljava/util/List;
      //   431: iload_3
      //   432: bipush 8
      //   434: iand
      //   435: bipush 8
      //   437: if_icmpne +14 -> 451
      //   440: aload_0
      //   441: aload_0
      //   442: getfield 110	com/google/protos/aksara/lattice/LatticeP$Node:state_	Ljava/util/List;
      //   445: invokestatic 108	java/util/Collections:unmodifiableList	(Ljava/util/List;)Ljava/util/List;
      //   448: putfield 110	com/google/protos/aksara/lattice/LatticeP$Node:state_	Ljava/util/List;
      //   451: iload_3
      //   452: bipush 32
      //   454: iand
      //   455: bipush 32
      //   457: if_icmpne +18 -> 475
      //   460: aload_0
      //   461: new 112	com/google/protobuf/UnmodifiableLazyStringList
      //   464: dup
      //   465: aload_0
      //   466: getfield 114	com/google/protos/aksara/lattice/LatticeP$Node:description_	Lcom/google/protobuf/LazyStringList;
      //   469: invokespecial 117	com/google/protobuf/UnmodifiableLazyStringList:<init>	(Lcom/google/protobuf/LazyStringList;)V
      //   472: putfield 114	com/google/protos/aksara/lattice/LatticeP$Node:description_	Lcom/google/protobuf/LazyStringList;
      //   475: aload_0
      //   476: invokevirtual 120	com/google/protos/aksara/lattice/LatticeP$Node:makeExtensionsImmutable	()V
      //   479: return
      //   480: iconst_1
      //   481: istore 4
      //   483: goto -460 -> 23
      //
      // Exception table:
      //   from	to	target	type
      //   28	34	164	com/google/protobuf/InvalidProtocolBufferException
      //   104	115	164	com/google/protobuf/InvalidProtocolBufferException
      //   128	139	164	com/google/protobuf/InvalidProtocolBufferException
      //   143	161	164	com/google/protobuf/InvalidProtocolBufferException
      //   244	262	164	com/google/protobuf/InvalidProtocolBufferException
      //   293	304	164	com/google/protobuf/InvalidProtocolBufferException
      //   309	327	164	com/google/protobuf/InvalidProtocolBufferException
      //   330	348	164	com/google/protobuf/InvalidProtocolBufferException
      //   360	371	164	com/google/protobuf/InvalidProtocolBufferException
      //   376	389	164	com/google/protobuf/InvalidProtocolBufferException
      //   392	410	164	com/google/protobuf/InvalidProtocolBufferException
      //   28	34	173	finally
      //   104	115	173	finally
      //   128	139	173	finally
      //   143	161	173	finally
      //   166	173	173	finally
      //   244	262	173	finally
      //   267	284	173	finally
      //   293	304	173	finally
      //   309	327	173	finally
      //   330	348	173	finally
      //   360	371	173	finally
      //   376	389	173	finally
      //   392	410	173	finally
      //   28	34	265	java/io/IOException
      //   104	115	265	java/io/IOException
      //   128	139	265	java/io/IOException
      //   143	161	265	java/io/IOException
      //   244	262	265	java/io/IOException
      //   293	304	265	java/io/IOException
      //   309	327	265	java/io/IOException
      //   330	348	265	java/io/IOException
      //   360	371	265	java/io/IOException
      //   376	389	265	java/io/IOException
      //   392	410	265	java/io/IOException
    }

    private Node(GeneratedMessageLite.ExtendableBuilder<Node, ?> paramExtendableBuilder)
    {
      super();
    }

    private Node(boolean paramBoolean)
    {
    }

    public static Node getDefaultInstance()
    {
      return defaultInstance;
    }

    private void initFields()
    {
      this.edge_ = Collections.emptyList();
      this.pixel_ = 0;
      this.isTransient_ = false;
      this.state_ = Collections.emptyList();
      this.posterior_ = 0.0F;
      this.description_ = LazyStringArrayList.EMPTY;
    }

    public static Builder newBuilder()
    {
      return Builder.access$2200();
    }

    public static Builder newBuilder(Node paramNode)
    {
      return newBuilder().mergeFrom(paramNode);
    }

    public static Node parseDelimitedFrom(InputStream paramInputStream)
      throws IOException
    {
      return (Node)PARSER.parseDelimitedFrom(paramInputStream);
    }

    public static Node parseDelimitedFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return (Node)PARSER.parseDelimitedFrom(paramInputStream, paramExtensionRegistryLite);
    }

    public static Node parseFrom(ByteString paramByteString)
      throws InvalidProtocolBufferException
    {
      return (Node)PARSER.parseFrom(paramByteString);
    }

    public static Node parseFrom(ByteString paramByteString, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return (Node)PARSER.parseFrom(paramByteString, paramExtensionRegistryLite);
    }

    public static Node parseFrom(CodedInputStream paramCodedInputStream)
      throws IOException
    {
      return (Node)PARSER.parseFrom(paramCodedInputStream);
    }

    public static Node parseFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return (Node)PARSER.parseFrom(paramCodedInputStream, paramExtensionRegistryLite);
    }

    public static Node parseFrom(InputStream paramInputStream)
      throws IOException
    {
      return (Node)PARSER.parseFrom(paramInputStream);
    }

    public static Node parseFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return (Node)PARSER.parseFrom(paramInputStream, paramExtensionRegistryLite);
    }

    public static Node parseFrom(byte[] paramArrayOfByte)
      throws InvalidProtocolBufferException
    {
      return (Node)PARSER.parseFrom(paramArrayOfByte);
    }

    public static Node parseFrom(byte[] paramArrayOfByte, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return (Node)PARSER.parseFrom(paramArrayOfByte, paramExtensionRegistryLite);
    }

    public Node getDefaultInstanceForType()
    {
      return defaultInstance;
    }

    public String getDescription(int paramInt)
    {
      return (String)this.description_.get(paramInt);
    }

    public ByteString getDescriptionBytes(int paramInt)
    {
      return this.description_.getByteString(paramInt);
    }

    public int getDescriptionCount()
    {
      return this.description_.size();
    }

    public List<String> getDescriptionList()
    {
      return this.description_;
    }

    public LatticeP.Edge getEdge(int paramInt)
    {
      return (LatticeP.Edge)this.edge_.get(paramInt);
    }

    public int getEdgeCount()
    {
      return this.edge_.size();
    }

    public List<LatticeP.Edge> getEdgeList()
    {
      return this.edge_;
    }

    public LatticeP.EdgeOrBuilder getEdgeOrBuilder(int paramInt)
    {
      return (LatticeP.EdgeOrBuilder)this.edge_.get(paramInt);
    }

    public List<? extends LatticeP.EdgeOrBuilder> getEdgeOrBuilderList()
    {
      return this.edge_;
    }

    public boolean getIsTransient()
    {
      return this.isTransient_;
    }

    public Parser<Node> getParserForType()
    {
      return PARSER;
    }

    public int getPixel()
    {
      return this.pixel_;
    }

    public float getPosterior()
    {
      return this.posterior_;
    }

    public int getSerializedSize()
    {
      int i = this.memoizedSerializedSize;
      if (i != -1)
        return i;
      int j = 0;
      for (int k = 0; k < this.edge_.size(); k++)
        j += CodedOutputStream.computeMessageSize(1, (MessageLite)this.edge_.get(k));
      if ((0x1 & this.bitField0_) == 1)
        j += CodedOutputStream.computeInt32Size(2, this.pixel_);
      for (int m = 0; m < this.state_.size(); m++)
        j += CodedOutputStream.computeMessageSize(3, (MessageLite)this.state_.get(m));
      if ((0x4 & this.bitField0_) == 4)
        j += CodedOutputStream.computeFloatSize(4, this.posterior_);
      int n = 0;
      for (int i1 = 0; i1 < this.description_.size(); i1++)
        n += CodedOutputStream.computeBytesSizeNoTag(this.description_.getByteString(i1));
      int i2 = j + n + 1 * getDescriptionList().size();
      if ((0x2 & this.bitField0_) == 2)
        i2 += CodedOutputStream.computeBoolSize(16, this.isTransient_);
      int i3 = i2 + extensionsSerializedSize();
      this.memoizedSerializedSize = i3;
      return i3;
    }

    public LatticeP.State getState(int paramInt)
    {
      return (LatticeP.State)this.state_.get(paramInt);
    }

    public int getStateCount()
    {
      return this.state_.size();
    }

    public List<LatticeP.State> getStateList()
    {
      return this.state_;
    }

    public LatticeP.StateOrBuilder getStateOrBuilder(int paramInt)
    {
      return (LatticeP.StateOrBuilder)this.state_.get(paramInt);
    }

    public List<? extends LatticeP.StateOrBuilder> getStateOrBuilderList()
    {
      return this.state_;
    }

    public boolean hasIsTransient()
    {
      return (0x2 & this.bitField0_) == 2;
    }

    public boolean hasPixel()
    {
      return (0x1 & this.bitField0_) == 1;
    }

    public boolean hasPosterior()
    {
      return (0x4 & this.bitField0_) == 4;
    }

    public final boolean isInitialized()
    {
      int i = 1;
      int j = this.memoizedIsInitialized;
      if (j != -1)
      {
        if (j == i);
        while (true)
        {
          return i;
          i = 0;
        }
      }
      for (int k = 0; k < getEdgeCount(); k++)
        if (!getEdge(k).isInitialized())
        {
          this.memoizedIsInitialized = 0;
          return false;
        }
      for (int m = 0; m < getStateCount(); m++)
        if (!getState(m).isInitialized())
        {
          this.memoizedIsInitialized = 0;
          return false;
        }
      if (!extensionsAreInitialized())
      {
        this.memoizedIsInitialized = 0;
        return false;
      }
      this.memoizedIsInitialized = i;
      return i;
    }

    public Builder newBuilderForType()
    {
      return newBuilder();
    }

    public Builder toBuilder()
    {
      return newBuilder(this);
    }

    protected Object writeReplace()
      throws ObjectStreamException
    {
      return super.writeReplace();
    }

    public void writeTo(CodedOutputStream paramCodedOutputStream)
      throws IOException
    {
      getSerializedSize();
      GeneratedMessageLite.ExtendableMessage.ExtensionWriter localExtensionWriter = newExtensionWriter();
      for (int i = 0; i < this.edge_.size(); i++)
        paramCodedOutputStream.writeMessage(1, (MessageLite)this.edge_.get(i));
      if ((0x1 & this.bitField0_) == 1)
        paramCodedOutputStream.writeInt32(2, this.pixel_);
      for (int j = 0; j < this.state_.size(); j++)
        paramCodedOutputStream.writeMessage(3, (MessageLite)this.state_.get(j));
      if ((0x4 & this.bitField0_) == 4)
        paramCodedOutputStream.writeFloat(4, this.posterior_);
      for (int k = 0; k < this.description_.size(); k++)
        paramCodedOutputStream.writeBytes(15, this.description_.getByteString(k));
      if ((0x2 & this.bitField0_) == 2)
        paramCodedOutputStream.writeBool(16, this.isTransient_);
      localExtensionWriter.writeUntil(536870912, paramCodedOutputStream);
    }

    public static final class Builder extends GeneratedMessageLite.ExtendableBuilder<LatticeP.Node, Builder>
      implements LatticeP.NodeOrBuilder
    {
      private int bitField0_;
      private LazyStringList description_ = LazyStringArrayList.EMPTY;
      private List<LatticeP.Edge> edge_ = Collections.emptyList();
      private boolean isTransient_;
      private int pixel_;
      private float posterior_;
      private List<LatticeP.State> state_ = Collections.emptyList();

      private Builder()
      {
        maybeForceBuilderInitialization();
      }

      private static Builder create()
      {
        return new Builder();
      }

      private void ensureDescriptionIsMutable()
      {
        if ((0x20 & this.bitField0_) != 32)
        {
          this.description_ = new LazyStringArrayList(this.description_);
          this.bitField0_ = (0x20 | this.bitField0_);
        }
      }

      private void ensureEdgeIsMutable()
      {
        if ((0x1 & this.bitField0_) != 1)
        {
          this.edge_ = new ArrayList(this.edge_);
          this.bitField0_ = (0x1 | this.bitField0_);
        }
      }

      private void ensureStateIsMutable()
      {
        if ((0x8 & this.bitField0_) != 8)
        {
          this.state_ = new ArrayList(this.state_);
          this.bitField0_ = (0x8 | this.bitField0_);
        }
      }

      private void maybeForceBuilderInitialization()
      {
      }

      public Builder addAllDescription(Iterable<String> paramIterable)
      {
        ensureDescriptionIsMutable();
        GeneratedMessageLite.ExtendableBuilder.addAll(paramIterable, this.description_);
        return this;
      }

      public Builder addAllEdge(Iterable<? extends LatticeP.Edge> paramIterable)
      {
        ensureEdgeIsMutable();
        GeneratedMessageLite.ExtendableBuilder.addAll(paramIterable, this.edge_);
        return this;
      }

      public Builder addAllState(Iterable<? extends LatticeP.State> paramIterable)
      {
        ensureStateIsMutable();
        GeneratedMessageLite.ExtendableBuilder.addAll(paramIterable, this.state_);
        return this;
      }

      public Builder addDescription(String paramString)
      {
        if (paramString == null)
          throw new NullPointerException();
        ensureDescriptionIsMutable();
        this.description_.add(paramString);
        return this;
      }

      public Builder addDescriptionBytes(ByteString paramByteString)
      {
        if (paramByteString == null)
          throw new NullPointerException();
        ensureDescriptionIsMutable();
        this.description_.add(paramByteString);
        return this;
      }

      public Builder addEdge(int paramInt, LatticeP.Edge.Builder paramBuilder)
      {
        ensureEdgeIsMutable();
        this.edge_.add(paramInt, paramBuilder.build());
        return this;
      }

      public Builder addEdge(int paramInt, LatticeP.Edge paramEdge)
      {
        if (paramEdge == null)
          throw new NullPointerException();
        ensureEdgeIsMutable();
        this.edge_.add(paramInt, paramEdge);
        return this;
      }

      public Builder addEdge(LatticeP.Edge.Builder paramBuilder)
      {
        ensureEdgeIsMutable();
        this.edge_.add(paramBuilder.build());
        return this;
      }

      public Builder addEdge(LatticeP.Edge paramEdge)
      {
        if (paramEdge == null)
          throw new NullPointerException();
        ensureEdgeIsMutable();
        this.edge_.add(paramEdge);
        return this;
      }

      public Builder addState(int paramInt, LatticeP.State.Builder paramBuilder)
      {
        ensureStateIsMutable();
        this.state_.add(paramInt, paramBuilder.build());
        return this;
      }

      public Builder addState(int paramInt, LatticeP.State paramState)
      {
        if (paramState == null)
          throw new NullPointerException();
        ensureStateIsMutable();
        this.state_.add(paramInt, paramState);
        return this;
      }

      public Builder addState(LatticeP.State.Builder paramBuilder)
      {
        ensureStateIsMutable();
        this.state_.add(paramBuilder.build());
        return this;
      }

      public Builder addState(LatticeP.State paramState)
      {
        if (paramState == null)
          throw new NullPointerException();
        ensureStateIsMutable();
        this.state_.add(paramState);
        return this;
      }

      public LatticeP.Node build()
      {
        LatticeP.Node localNode = buildPartial();
        if (!localNode.isInitialized())
          throw newUninitializedMessageException(localNode);
        return localNode;
      }

      public LatticeP.Node buildPartial()
      {
        LatticeP.Node localNode = new LatticeP.Node(this, null);
        int i = this.bitField0_;
        if ((0x1 & this.bitField0_) == 1)
        {
          this.edge_ = Collections.unmodifiableList(this.edge_);
          this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
        }
        LatticeP.Node.access$2402(localNode, this.edge_);
        int j = i & 0x2;
        int k = 0;
        if (j == 2)
          k = 0x0 | 0x1;
        LatticeP.Node.access$2502(localNode, this.pixel_);
        if ((i & 0x4) == 4)
          k |= 2;
        LatticeP.Node.access$2602(localNode, this.isTransient_);
        if ((0x8 & this.bitField0_) == 8)
        {
          this.state_ = Collections.unmodifiableList(this.state_);
          this.bitField0_ = (0xFFFFFFF7 & this.bitField0_);
        }
        LatticeP.Node.access$2702(localNode, this.state_);
        if ((i & 0x10) == 16)
          k |= 4;
        LatticeP.Node.access$2802(localNode, this.posterior_);
        if ((0x20 & this.bitField0_) == 32)
        {
          this.description_ = new UnmodifiableLazyStringList(this.description_);
          this.bitField0_ = (0xFFFFFFDF & this.bitField0_);
        }
        LatticeP.Node.access$2902(localNode, this.description_);
        LatticeP.Node.access$3002(localNode, k);
        return localNode;
      }

      public Builder clear()
      {
        super.clear();
        this.edge_ = Collections.emptyList();
        this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
        this.pixel_ = 0;
        this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
        this.isTransient_ = false;
        this.bitField0_ = (0xFFFFFFFB & this.bitField0_);
        this.state_ = Collections.emptyList();
        this.bitField0_ = (0xFFFFFFF7 & this.bitField0_);
        this.posterior_ = 0.0F;
        this.bitField0_ = (0xFFFFFFEF & this.bitField0_);
        this.description_ = LazyStringArrayList.EMPTY;
        this.bitField0_ = (0xFFFFFFDF & this.bitField0_);
        return this;
      }

      public Builder clearDescription()
      {
        this.description_ = LazyStringArrayList.EMPTY;
        this.bitField0_ = (0xFFFFFFDF & this.bitField0_);
        return this;
      }

      public Builder clearEdge()
      {
        this.edge_ = Collections.emptyList();
        this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
        return this;
      }

      public Builder clearIsTransient()
      {
        this.bitField0_ = (0xFFFFFFFB & this.bitField0_);
        this.isTransient_ = false;
        return this;
      }

      public Builder clearPixel()
      {
        this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
        this.pixel_ = 0;
        return this;
      }

      public Builder clearPosterior()
      {
        this.bitField0_ = (0xFFFFFFEF & this.bitField0_);
        this.posterior_ = 0.0F;
        return this;
      }

      public Builder clearState()
      {
        this.state_ = Collections.emptyList();
        this.bitField0_ = (0xFFFFFFF7 & this.bitField0_);
        return this;
      }

      public Builder clone()
      {
        return create().mergeFrom(buildPartial());
      }

      public LatticeP.Node getDefaultInstanceForType()
      {
        return LatticeP.Node.getDefaultInstance();
      }

      public String getDescription(int paramInt)
      {
        return (String)this.description_.get(paramInt);
      }

      public ByteString getDescriptionBytes(int paramInt)
      {
        return this.description_.getByteString(paramInt);
      }

      public int getDescriptionCount()
      {
        return this.description_.size();
      }

      public List<String> getDescriptionList()
      {
        return Collections.unmodifiableList(this.description_);
      }

      public LatticeP.Edge getEdge(int paramInt)
      {
        return (LatticeP.Edge)this.edge_.get(paramInt);
      }

      public int getEdgeCount()
      {
        return this.edge_.size();
      }

      public List<LatticeP.Edge> getEdgeList()
      {
        return Collections.unmodifiableList(this.edge_);
      }

      public boolean getIsTransient()
      {
        return this.isTransient_;
      }

      public int getPixel()
      {
        return this.pixel_;
      }

      public float getPosterior()
      {
        return this.posterior_;
      }

      public LatticeP.State getState(int paramInt)
      {
        return (LatticeP.State)this.state_.get(paramInt);
      }

      public int getStateCount()
      {
        return this.state_.size();
      }

      public List<LatticeP.State> getStateList()
      {
        return Collections.unmodifiableList(this.state_);
      }

      public boolean hasIsTransient()
      {
        return (0x4 & this.bitField0_) == 4;
      }

      public boolean hasPixel()
      {
        return (0x2 & this.bitField0_) == 2;
      }

      public boolean hasPosterior()
      {
        return (0x10 & this.bitField0_) == 16;
      }

      public final boolean isInitialized()
      {
        int i = 0;
        if (i < getEdgeCount())
          if (getEdge(i).isInitialized());
        label56: 
        do
        {
          return false;
          i++;
          break;
          for (int j = 0; ; j++)
          {
            if (j >= getStateCount())
              break label56;
            if (!getState(j).isInitialized())
              break;
          }
        }
        while (!extensionsAreInitialized());
        return true;
      }

      public Builder mergeFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
        throws IOException
      {
        LatticeP.Node localNode1 = null;
        try
        {
          LatticeP.Node localNode2 = (LatticeP.Node)LatticeP.Node.PARSER.parsePartialFrom(paramCodedInputStream, paramExtensionRegistryLite);
          return this;
        }
        catch (InvalidProtocolBufferException localInvalidProtocolBufferException)
        {
          localNode1 = (LatticeP.Node)localInvalidProtocolBufferException.getUnfinishedMessage();
          throw localInvalidProtocolBufferException;
        }
        finally
        {
          if (localNode1 != null)
            mergeFrom(localNode1);
        }
      }

      public Builder mergeFrom(LatticeP.Node paramNode)
      {
        if (paramNode == LatticeP.Node.getDefaultInstance())
          return this;
        if (!paramNode.edge_.isEmpty())
        {
          if (this.edge_.isEmpty())
          {
            this.edge_ = paramNode.edge_;
            this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
          }
        }
        else
        {
          if (paramNode.hasPixel())
            setPixel(paramNode.getPixel());
          if (paramNode.hasIsTransient())
            setIsTransient(paramNode.getIsTransient());
          if (!paramNode.state_.isEmpty())
          {
            if (!this.state_.isEmpty())
              break label214;
            this.state_ = paramNode.state_;
            this.bitField0_ = (0xFFFFFFF7 & this.bitField0_);
          }
          label127: if (paramNode.hasPosterior())
            setPosterior(paramNode.getPosterior());
          if (!paramNode.description_.isEmpty())
          {
            if (!this.description_.isEmpty())
              break label235;
            this.description_ = paramNode.description_;
            this.bitField0_ = (0xFFFFFFDF & this.bitField0_);
          }
        }
        while (true)
        {
          mergeExtensionFields(paramNode);
          return this;
          ensureEdgeIsMutable();
          this.edge_.addAll(paramNode.edge_);
          break;
          label214: ensureStateIsMutable();
          this.state_.addAll(paramNode.state_);
          break label127;
          label235: ensureDescriptionIsMutable();
          this.description_.addAll(paramNode.description_);
        }
      }

      public Builder removeEdge(int paramInt)
      {
        ensureEdgeIsMutable();
        this.edge_.remove(paramInt);
        return this;
      }

      public Builder removeState(int paramInt)
      {
        ensureStateIsMutable();
        this.state_.remove(paramInt);
        return this;
      }

      public Builder setDescription(int paramInt, String paramString)
      {
        if (paramString == null)
          throw new NullPointerException();
        ensureDescriptionIsMutable();
        this.description_.set(paramInt, paramString);
        return this;
      }

      public Builder setEdge(int paramInt, LatticeP.Edge.Builder paramBuilder)
      {
        ensureEdgeIsMutable();
        this.edge_.set(paramInt, paramBuilder.build());
        return this;
      }

      public Builder setEdge(int paramInt, LatticeP.Edge paramEdge)
      {
        if (paramEdge == null)
          throw new NullPointerException();
        ensureEdgeIsMutable();
        this.edge_.set(paramInt, paramEdge);
        return this;
      }

      public Builder setIsTransient(boolean paramBoolean)
      {
        this.bitField0_ = (0x4 | this.bitField0_);
        this.isTransient_ = paramBoolean;
        return this;
      }

      public Builder setPixel(int paramInt)
      {
        this.bitField0_ = (0x2 | this.bitField0_);
        this.pixel_ = paramInt;
        return this;
      }

      public Builder setPosterior(float paramFloat)
      {
        this.bitField0_ = (0x10 | this.bitField0_);
        this.posterior_ = paramFloat;
        return this;
      }

      public Builder setState(int paramInt, LatticeP.State.Builder paramBuilder)
      {
        ensureStateIsMutable();
        this.state_.set(paramInt, paramBuilder.build());
        return this;
      }

      public Builder setState(int paramInt, LatticeP.State paramState)
      {
        if (paramState == null)
          throw new NullPointerException();
        ensureStateIsMutable();
        this.state_.set(paramInt, paramState);
        return this;
      }
    }
  }

  public static abstract interface NodeOrBuilder extends GeneratedMessageLite.ExtendableMessageOrBuilder<LatticeP.Node>
  {
    public abstract String getDescription(int paramInt);

    public abstract ByteString getDescriptionBytes(int paramInt);

    public abstract int getDescriptionCount();

    public abstract List<String> getDescriptionList();

    public abstract LatticeP.Edge getEdge(int paramInt);

    public abstract int getEdgeCount();

    public abstract List<LatticeP.Edge> getEdgeList();

    public abstract boolean getIsTransient();

    public abstract int getPixel();

    public abstract float getPosterior();

    public abstract LatticeP.State getState(int paramInt);

    public abstract int getStateCount();

    public abstract List<LatticeP.State> getStateList();

    public abstract boolean hasIsTransient();

    public abstract boolean hasPixel();

    public abstract boolean hasPosterior();
  }

  public static final class Path extends GeneratedMessageLite
    implements LatticeP.PathOrBuilder
  {
    public static final int COST_FIELD_NUMBER = 4;
    public static final int DESCRIPTION_FIELD_NUMBER = 6;
    public static final int EDGE_INDEX_FIELD_NUMBER = 3;
    public static final int LABEL_FIELD_NUMBER = 2;
    public static Parser<Path> PARSER = new AbstractParser()
    {
      public LatticeP.Path parsePartialFrom(CodedInputStream paramAnonymousCodedInputStream, ExtensionRegistryLite paramAnonymousExtensionRegistryLite)
        throws InvalidProtocolBufferException
      {
        return new LatticeP.Path(paramAnonymousCodedInputStream, paramAnonymousExtensionRegistryLite, null);
      }
    };
    public static final int TEXT_FIELD_NUMBER = 1;
    public static final int WEIGHTED_COST_FIELD_NUMBER = 5;
    private static final Path defaultInstance = new Path(true);
    private static final long serialVersionUID;
    private int bitField0_;
    private List<LatticeP.Cost> cost_;
    private Object description_;
    private List<Integer> edgeIndex_;
    private LazyStringList label_;
    private byte memoizedIsInitialized = -1;
    private int memoizedSerializedSize = -1;
    private Object text_;
    private double weightedCost_;

    static
    {
      defaultInstance.initFields();
    }

    // ERROR //
    private Path(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      // Byte code:
      //   0: aload_0
      //   1: invokespecial 65	com/google/protobuf/GeneratedMessageLite:<init>	()V
      //   4: aload_0
      //   5: iconst_m1
      //   6: putfield 67	com/google/protos/aksara/lattice/LatticeP$Path:memoizedIsInitialized	B
      //   9: aload_0
      //   10: iconst_m1
      //   11: putfield 69	com/google/protos/aksara/lattice/LatticeP$Path:memoizedSerializedSize	I
      //   14: aload_0
      //   15: invokespecial 59	com/google/protos/aksara/lattice/LatticeP$Path:initFields	()V
      //   18: iconst_0
      //   19: istore_3
      //   20: iconst_0
      //   21: istore 4
      //   23: iload 4
      //   25: ifne +465 -> 490
      //   28: aload_1
      //   29: invokevirtual 75	com/google/protobuf/CodedInputStream:readTag	()I
      //   32: istore 8
      //   34: iload 8
      //   36: lookupswitch	default:+76->112, 0:+519->555, 10:+93->129, 18:+192->228, 24:+249->285, 26:+291->327, 34:+366->402, 41:+412->448, 50:+433->469
      //   113: aload_1
      //   114: aload_2
      //   115: iload 8
      //   117: invokevirtual 79	com/google/protos/aksara/lattice/LatticeP$Path:parseUnknownField	(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z
      //   120: ifne -97 -> 23
      //   123: iconst_1
      //   124: istore 4
      //   126: goto -103 -> 23
      //   129: aload_0
      //   130: iconst_1
      //   131: aload_0
      //   132: getfield 81	com/google/protos/aksara/lattice/LatticeP$Path:bitField0_	I
      //   135: ior
      //   136: putfield 81	com/google/protos/aksara/lattice/LatticeP$Path:bitField0_	I
      //   139: aload_0
      //   140: aload_1
      //   141: invokevirtual 85	com/google/protobuf/CodedInputStream:readBytes	()Lcom/google/protobuf/ByteString;
      //   144: putfield 87	com/google/protos/aksara/lattice/LatticeP$Path:text_	Ljava/lang/Object;
      //   147: goto -124 -> 23
      //   150: astore 7
      //   152: aload 7
      //   154: aload_0
      //   155: invokevirtual 91	com/google/protobuf/InvalidProtocolBufferException:setUnfinishedMessage	(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/InvalidProtocolBufferException;
      //   158: athrow
      //   159: astore 6
      //   161: iload_3
      //   162: iconst_2
      //   163: iand
      //   164: iconst_2
      //   165: if_icmpne +18 -> 183
      //   168: aload_0
      //   169: new 93	com/google/protobuf/UnmodifiableLazyStringList
      //   172: dup
      //   173: aload_0
      //   174: getfield 95	com/google/protos/aksara/lattice/LatticeP$Path:label_	Lcom/google/protobuf/LazyStringList;
      //   177: invokespecial 98	com/google/protobuf/UnmodifiableLazyStringList:<init>	(Lcom/google/protobuf/LazyStringList;)V
      //   180: putfield 95	com/google/protos/aksara/lattice/LatticeP$Path:label_	Lcom/google/protobuf/LazyStringList;
      //   183: iload_3
      //   184: iconst_4
      //   185: iand
      //   186: iconst_4
      //   187: if_icmpne +14 -> 201
      //   190: aload_0
      //   191: aload_0
      //   192: getfield 100	com/google/protos/aksara/lattice/LatticeP$Path:edgeIndex_	Ljava/util/List;
      //   195: invokestatic 106	java/util/Collections:unmodifiableList	(Ljava/util/List;)Ljava/util/List;
      //   198: putfield 100	com/google/protos/aksara/lattice/LatticeP$Path:edgeIndex_	Ljava/util/List;
      //   201: iload_3
      //   202: bipush 8
      //   204: iand
      //   205: bipush 8
      //   207: if_icmpne +14 -> 221
      //   210: aload_0
      //   211: aload_0
      //   212: getfield 108	com/google/protos/aksara/lattice/LatticeP$Path:cost_	Ljava/util/List;
      //   215: invokestatic 106	java/util/Collections:unmodifiableList	(Ljava/util/List;)Ljava/util/List;
      //   218: putfield 108	com/google/protos/aksara/lattice/LatticeP$Path:cost_	Ljava/util/List;
      //   221: aload_0
      //   222: invokevirtual 111	com/google/protos/aksara/lattice/LatticeP$Path:makeExtensionsImmutable	()V
      //   225: aload 6
      //   227: athrow
      //   228: iload_3
      //   229: iconst_2
      //   230: iand
      //   231: iconst_2
      //   232: if_icmpeq +18 -> 250
      //   235: aload_0
      //   236: new 113	com/google/protobuf/LazyStringArrayList
      //   239: dup
      //   240: invokespecial 114	com/google/protobuf/LazyStringArrayList:<init>	()V
      //   243: putfield 95	com/google/protos/aksara/lattice/LatticeP$Path:label_	Lcom/google/protobuf/LazyStringList;
      //   246: iload_3
      //   247: iconst_2
      //   248: ior
      //   249: istore_3
      //   250: aload_0
      //   251: getfield 95	com/google/protos/aksara/lattice/LatticeP$Path:label_	Lcom/google/protobuf/LazyStringList;
      //   254: aload_1
      //   255: invokevirtual 85	com/google/protobuf/CodedInputStream:readBytes	()Lcom/google/protobuf/ByteString;
      //   258: invokeinterface 120 2 0
      //   263: goto -240 -> 23
      //   266: astore 5
      //   268: new 62	com/google/protobuf/InvalidProtocolBufferException
      //   271: dup
      //   272: aload 5
      //   274: invokevirtual 124	java/io/IOException:getMessage	()Ljava/lang/String;
      //   277: invokespecial 127	com/google/protobuf/InvalidProtocolBufferException:<init>	(Ljava/lang/String;)V
      //   280: aload_0
      //   281: invokevirtual 91	com/google/protobuf/InvalidProtocolBufferException:setUnfinishedMessage	(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/InvalidProtocolBufferException;
      //   284: athrow
      //   285: iload_3
      //   286: iconst_4
      //   287: iand
      //   288: iconst_4
      //   289: if_icmpeq +18 -> 307
      //   292: aload_0
      //   293: new 129	java/util/ArrayList
      //   296: dup
      //   297: invokespecial 130	java/util/ArrayList:<init>	()V
      //   300: putfield 100	com/google/protos/aksara/lattice/LatticeP$Path:edgeIndex_	Ljava/util/List;
      //   303: iload_3
      //   304: iconst_4
      //   305: ior
      //   306: istore_3
      //   307: aload_0
      //   308: getfield 100	com/google/protos/aksara/lattice/LatticeP$Path:edgeIndex_	Ljava/util/List;
      //   311: aload_1
      //   312: invokevirtual 133	com/google/protobuf/CodedInputStream:readInt32	()I
      //   315: invokestatic 139	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
      //   318: invokeinterface 144 2 0
      //   323: pop
      //   324: goto -301 -> 23
      //   327: aload_1
      //   328: aload_1
      //   329: invokevirtual 147	com/google/protobuf/CodedInputStream:readRawVarint32	()I
      //   332: invokevirtual 151	com/google/protobuf/CodedInputStream:pushLimit	(I)I
      //   335: istore 10
      //   337: iload_3
      //   338: iconst_4
      //   339: iand
      //   340: iconst_4
      //   341: if_icmpeq +25 -> 366
      //   344: aload_1
      //   345: invokevirtual 154	com/google/protobuf/CodedInputStream:getBytesUntilLimit	()I
      //   348: ifle +18 -> 366
      //   351: aload_0
      //   352: new 129	java/util/ArrayList
      //   355: dup
      //   356: invokespecial 130	java/util/ArrayList:<init>	()V
      //   359: putfield 100	com/google/protos/aksara/lattice/LatticeP$Path:edgeIndex_	Ljava/util/List;
      //   362: iload_3
      //   363: iconst_4
      //   364: ior
      //   365: istore_3
      //   366: aload_1
      //   367: invokevirtual 154	com/google/protobuf/CodedInputStream:getBytesUntilLimit	()I
      //   370: ifle +23 -> 393
      //   373: aload_0
      //   374: getfield 100	com/google/protos/aksara/lattice/LatticeP$Path:edgeIndex_	Ljava/util/List;
      //   377: aload_1
      //   378: invokevirtual 133	com/google/protobuf/CodedInputStream:readInt32	()I
      //   381: invokestatic 139	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
      //   384: invokeinterface 144 2 0
      //   389: pop
      //   390: goto -24 -> 366
      //   393: aload_1
      //   394: iload 10
      //   396: invokevirtual 158	com/google/protobuf/CodedInputStream:popLimit	(I)V
      //   399: goto -376 -> 23
      //   402: iload_3
      //   403: bipush 8
      //   405: iand
      //   406: bipush 8
      //   408: if_icmpeq +19 -> 427
      //   411: aload_0
      //   412: new 129	java/util/ArrayList
      //   415: dup
      //   416: invokespecial 130	java/util/ArrayList:<init>	()V
      //   419: putfield 108	com/google/protos/aksara/lattice/LatticeP$Path:cost_	Ljava/util/List;
      //   422: iload_3
      //   423: bipush 8
      //   425: ior
      //   426: istore_3
      //   427: aload_0
      //   428: getfield 108	com/google/protos/aksara/lattice/LatticeP$Path:cost_	Ljava/util/List;
      //   431: aload_1
      //   432: getstatic 161	com/google/protos/aksara/lattice/LatticeP$Cost:PARSER	Lcom/google/protobuf/Parser;
      //   435: aload_2
      //   436: invokevirtual 165	com/google/protobuf/CodedInputStream:readMessage	(Lcom/google/protobuf/Parser;Lcom/google/protobuf/ExtensionRegistryLite;)Lcom/google/protobuf/MessageLite;
      //   439: invokeinterface 144 2 0
      //   444: pop
      //   445: goto -422 -> 23
      //   448: aload_0
      //   449: iconst_2
      //   450: aload_0
      //   451: getfield 81	com/google/protos/aksara/lattice/LatticeP$Path:bitField0_	I
      //   454: ior
      //   455: putfield 81	com/google/protos/aksara/lattice/LatticeP$Path:bitField0_	I
      //   458: aload_0
      //   459: aload_1
      //   460: invokevirtual 169	com/google/protobuf/CodedInputStream:readDouble	()D
      //   463: putfield 171	com/google/protos/aksara/lattice/LatticeP$Path:weightedCost_	D
      //   466: goto -443 -> 23
      //   469: aload_0
      //   470: iconst_4
      //   471: aload_0
      //   472: getfield 81	com/google/protos/aksara/lattice/LatticeP$Path:bitField0_	I
      //   475: ior
      //   476: putfield 81	com/google/protos/aksara/lattice/LatticeP$Path:bitField0_	I
      //   479: aload_0
      //   480: aload_1
      //   481: invokevirtual 85	com/google/protobuf/CodedInputStream:readBytes	()Lcom/google/protobuf/ByteString;
      //   484: putfield 173	com/google/protos/aksara/lattice/LatticeP$Path:description_	Ljava/lang/Object;
      //   487: goto -464 -> 23
      //   490: iload_3
      //   491: iconst_2
      //   492: iand
      //   493: iconst_2
      //   494: if_icmpne +18 -> 512
      //   497: aload_0
      //   498: new 93	com/google/protobuf/UnmodifiableLazyStringList
      //   501: dup
      //   502: aload_0
      //   503: getfield 95	com/google/protos/aksara/lattice/LatticeP$Path:label_	Lcom/google/protobuf/LazyStringList;
      //   506: invokespecial 98	com/google/protobuf/UnmodifiableLazyStringList:<init>	(Lcom/google/protobuf/LazyStringList;)V
      //   509: putfield 95	com/google/protos/aksara/lattice/LatticeP$Path:label_	Lcom/google/protobuf/LazyStringList;
      //   512: iload_3
      //   513: iconst_4
      //   514: iand
      //   515: iconst_4
      //   516: if_icmpne +14 -> 530
      //   519: aload_0
      //   520: aload_0
      //   521: getfield 100	com/google/protos/aksara/lattice/LatticeP$Path:edgeIndex_	Ljava/util/List;
      //   524: invokestatic 106	java/util/Collections:unmodifiableList	(Ljava/util/List;)Ljava/util/List;
      //   527: putfield 100	com/google/protos/aksara/lattice/LatticeP$Path:edgeIndex_	Ljava/util/List;
      //   530: iload_3
      //   531: bipush 8
      //   533: iand
      //   534: bipush 8
      //   536: if_icmpne +14 -> 550
      //   539: aload_0
      //   540: aload_0
      //   541: getfield 108	com/google/protos/aksara/lattice/LatticeP$Path:cost_	Ljava/util/List;
      //   544: invokestatic 106	java/util/Collections:unmodifiableList	(Ljava/util/List;)Ljava/util/List;
      //   547: putfield 108	com/google/protos/aksara/lattice/LatticeP$Path:cost_	Ljava/util/List;
      //   550: aload_0
      //   551: invokevirtual 111	com/google/protos/aksara/lattice/LatticeP$Path:makeExtensionsImmutable	()V
      //   554: return
      //   555: iconst_1
      //   556: istore 4
      //   558: goto -535 -> 23
      //
      // Exception table:
      //   from	to	target	type
      //   28	34	150	com/google/protobuf/InvalidProtocolBufferException
      //   112	123	150	com/google/protobuf/InvalidProtocolBufferException
      //   129	147	150	com/google/protobuf/InvalidProtocolBufferException
      //   235	246	150	com/google/protobuf/InvalidProtocolBufferException
      //   250	263	150	com/google/protobuf/InvalidProtocolBufferException
      //   292	303	150	com/google/protobuf/InvalidProtocolBufferException
      //   307	324	150	com/google/protobuf/InvalidProtocolBufferException
      //   327	337	150	com/google/protobuf/InvalidProtocolBufferException
      //   344	362	150	com/google/protobuf/InvalidProtocolBufferException
      //   366	390	150	com/google/protobuf/InvalidProtocolBufferException
      //   393	399	150	com/google/protobuf/InvalidProtocolBufferException
      //   411	422	150	com/google/protobuf/InvalidProtocolBufferException
      //   427	445	150	com/google/protobuf/InvalidProtocolBufferException
      //   448	466	150	com/google/protobuf/InvalidProtocolBufferException
      //   469	487	150	com/google/protobuf/InvalidProtocolBufferException
      //   28	34	159	finally
      //   112	123	159	finally
      //   129	147	159	finally
      //   152	159	159	finally
      //   235	246	159	finally
      //   250	263	159	finally
      //   268	285	159	finally
      //   292	303	159	finally
      //   307	324	159	finally
      //   327	337	159	finally
      //   344	362	159	finally
      //   366	390	159	finally
      //   393	399	159	finally
      //   411	422	159	finally
      //   427	445	159	finally
      //   448	466	159	finally
      //   469	487	159	finally
      //   28	34	266	java/io/IOException
      //   112	123	266	java/io/IOException
      //   129	147	266	java/io/IOException
      //   235	246	266	java/io/IOException
      //   250	263	266	java/io/IOException
      //   292	303	266	java/io/IOException
      //   307	324	266	java/io/IOException
      //   327	337	266	java/io/IOException
      //   344	362	266	java/io/IOException
      //   366	390	266	java/io/IOException
      //   393	399	266	java/io/IOException
      //   411	422	266	java/io/IOException
      //   427	445	266	java/io/IOException
      //   448	466	266	java/io/IOException
      //   469	487	266	java/io/IOException
    }

    private Path(GeneratedMessageLite.Builder paramBuilder)
    {
      super();
    }

    private Path(boolean paramBoolean)
    {
    }

    public static Path getDefaultInstance()
    {
      return defaultInstance;
    }

    private void initFields()
    {
      this.text_ = "";
      this.label_ = LazyStringArrayList.EMPTY;
      this.edgeIndex_ = Collections.emptyList();
      this.cost_ = Collections.emptyList();
      this.weightedCost_ = 0.0D;
      this.description_ = "";
    }

    public static Builder newBuilder()
    {
      return Builder.access$3200();
    }

    public static Builder newBuilder(Path paramPath)
    {
      return newBuilder().mergeFrom(paramPath);
    }

    public static Path parseDelimitedFrom(InputStream paramInputStream)
      throws IOException
    {
      return (Path)PARSER.parseDelimitedFrom(paramInputStream);
    }

    public static Path parseDelimitedFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return (Path)PARSER.parseDelimitedFrom(paramInputStream, paramExtensionRegistryLite);
    }

    public static Path parseFrom(ByteString paramByteString)
      throws InvalidProtocolBufferException
    {
      return (Path)PARSER.parseFrom(paramByteString);
    }

    public static Path parseFrom(ByteString paramByteString, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return (Path)PARSER.parseFrom(paramByteString, paramExtensionRegistryLite);
    }

    public static Path parseFrom(CodedInputStream paramCodedInputStream)
      throws IOException
    {
      return (Path)PARSER.parseFrom(paramCodedInputStream);
    }

    public static Path parseFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return (Path)PARSER.parseFrom(paramCodedInputStream, paramExtensionRegistryLite);
    }

    public static Path parseFrom(InputStream paramInputStream)
      throws IOException
    {
      return (Path)PARSER.parseFrom(paramInputStream);
    }

    public static Path parseFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return (Path)PARSER.parseFrom(paramInputStream, paramExtensionRegistryLite);
    }

    public static Path parseFrom(byte[] paramArrayOfByte)
      throws InvalidProtocolBufferException
    {
      return (Path)PARSER.parseFrom(paramArrayOfByte);
    }

    public static Path parseFrom(byte[] paramArrayOfByte, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return (Path)PARSER.parseFrom(paramArrayOfByte, paramExtensionRegistryLite);
    }

    public LatticeP.Cost getCost(int paramInt)
    {
      return (LatticeP.Cost)this.cost_.get(paramInt);
    }

    public int getCostCount()
    {
      return this.cost_.size();
    }

    public List<LatticeP.Cost> getCostList()
    {
      return this.cost_;
    }

    public LatticeP.CostOrBuilder getCostOrBuilder(int paramInt)
    {
      return (LatticeP.CostOrBuilder)this.cost_.get(paramInt);
    }

    public List<? extends LatticeP.CostOrBuilder> getCostOrBuilderList()
    {
      return this.cost_;
    }

    public Path getDefaultInstanceForType()
    {
      return defaultInstance;
    }

    public String getDescription()
    {
      Object localObject = this.description_;
      if ((localObject instanceof String))
        return (String)localObject;
      ByteString localByteString = (ByteString)localObject;
      String str = localByteString.toStringUtf8();
      if (localByteString.isValidUtf8())
        this.description_ = str;
      return str;
    }

    public ByteString getDescriptionBytes()
    {
      Object localObject = this.description_;
      if ((localObject instanceof String))
      {
        ByteString localByteString = ByteString.copyFromUtf8((String)localObject);
        this.description_ = localByteString;
        return localByteString;
      }
      return (ByteString)localObject;
    }

    public int getEdgeIndex(int paramInt)
    {
      return ((Integer)this.edgeIndex_.get(paramInt)).intValue();
    }

    public int getEdgeIndexCount()
    {
      return this.edgeIndex_.size();
    }

    public List<Integer> getEdgeIndexList()
    {
      return this.edgeIndex_;
    }

    public String getLabel(int paramInt)
    {
      return (String)this.label_.get(paramInt);
    }

    public ByteString getLabelBytes(int paramInt)
    {
      return this.label_.getByteString(paramInt);
    }

    public int getLabelCount()
    {
      return this.label_.size();
    }

    public List<String> getLabelList()
    {
      return this.label_;
    }

    public Parser<Path> getParserForType()
    {
      return PARSER;
    }

    public int getSerializedSize()
    {
      int i = this.memoizedSerializedSize;
      if (i != -1)
        return i;
      int j = 0x1 & this.bitField0_;
      int k = 0;
      if (j == 1)
        k = 0 + CodedOutputStream.computeBytesSize(1, getTextBytes());
      int m = 0;
      for (int n = 0; n < this.label_.size(); n++)
        m += CodedOutputStream.computeBytesSizeNoTag(this.label_.getByteString(n));
      int i1 = k + m + 1 * getLabelList().size();
      int i2 = 0;
      for (int i3 = 0; i3 < this.edgeIndex_.size(); i3++)
        i2 += CodedOutputStream.computeInt32SizeNoTag(((Integer)this.edgeIndex_.get(i3)).intValue());
      int i4 = i1 + i2 + 1 * getEdgeIndexList().size();
      for (int i5 = 0; i5 < this.cost_.size(); i5++)
        i4 += CodedOutputStream.computeMessageSize(4, (MessageLite)this.cost_.get(i5));
      if ((0x2 & this.bitField0_) == 2)
        i4 += CodedOutputStream.computeDoubleSize(5, this.weightedCost_);
      if ((0x4 & this.bitField0_) == 4)
        i4 += CodedOutputStream.computeBytesSize(6, getDescriptionBytes());
      this.memoizedSerializedSize = i4;
      return i4;
    }

    public String getText()
    {
      Object localObject = this.text_;
      if ((localObject instanceof String))
        return (String)localObject;
      ByteString localByteString = (ByteString)localObject;
      String str = localByteString.toStringUtf8();
      if (localByteString.isValidUtf8())
        this.text_ = str;
      return str;
    }

    public ByteString getTextBytes()
    {
      Object localObject = this.text_;
      if ((localObject instanceof String))
      {
        ByteString localByteString = ByteString.copyFromUtf8((String)localObject);
        this.text_ = localByteString;
        return localByteString;
      }
      return (ByteString)localObject;
    }

    public double getWeightedCost()
    {
      return this.weightedCost_;
    }

    public boolean hasDescription()
    {
      return (0x4 & this.bitField0_) == 4;
    }

    public boolean hasText()
    {
      return (0x1 & this.bitField0_) == 1;
    }

    public boolean hasWeightedCost()
    {
      return (0x2 & this.bitField0_) == 2;
    }

    public final boolean isInitialized()
    {
      int i = this.memoizedIsInitialized;
      if (i != -1)
        return i == 1;
      for (int j = 0; j < getCostCount(); j++)
        if (!getCost(j).isInitialized())
        {
          this.memoizedIsInitialized = 0;
          return false;
        }
      this.memoizedIsInitialized = 1;
      return true;
    }

    public Builder newBuilderForType()
    {
      return newBuilder();
    }

    public Builder toBuilder()
    {
      return newBuilder(this);
    }

    protected Object writeReplace()
      throws ObjectStreamException
    {
      return super.writeReplace();
    }

    public void writeTo(CodedOutputStream paramCodedOutputStream)
      throws IOException
    {
      getSerializedSize();
      if ((0x1 & this.bitField0_) == 1)
        paramCodedOutputStream.writeBytes(1, getTextBytes());
      for (int i = 0; i < this.label_.size(); i++)
        paramCodedOutputStream.writeBytes(2, this.label_.getByteString(i));
      for (int j = 0; j < this.edgeIndex_.size(); j++)
        paramCodedOutputStream.writeInt32(3, ((Integer)this.edgeIndex_.get(j)).intValue());
      for (int k = 0; k < this.cost_.size(); k++)
        paramCodedOutputStream.writeMessage(4, (MessageLite)this.cost_.get(k));
      if ((0x2 & this.bitField0_) == 2)
        paramCodedOutputStream.writeDouble(5, this.weightedCost_);
      if ((0x4 & this.bitField0_) == 4)
        paramCodedOutputStream.writeBytes(6, getDescriptionBytes());
    }

    public static final class Builder extends GeneratedMessageLite.Builder<LatticeP.Path, Builder>
      implements LatticeP.PathOrBuilder
    {
      private int bitField0_;
      private List<LatticeP.Cost> cost_ = Collections.emptyList();
      private Object description_ = "";
      private List<Integer> edgeIndex_ = Collections.emptyList();
      private LazyStringList label_ = LazyStringArrayList.EMPTY;
      private Object text_ = "";
      private double weightedCost_;

      private Builder()
      {
        maybeForceBuilderInitialization();
      }

      private static Builder create()
      {
        return new Builder();
      }

      private void ensureCostIsMutable()
      {
        if ((0x8 & this.bitField0_) != 8)
        {
          this.cost_ = new ArrayList(this.cost_);
          this.bitField0_ = (0x8 | this.bitField0_);
        }
      }

      private void ensureEdgeIndexIsMutable()
      {
        if ((0x4 & this.bitField0_) != 4)
        {
          this.edgeIndex_ = new ArrayList(this.edgeIndex_);
          this.bitField0_ = (0x4 | this.bitField0_);
        }
      }

      private void ensureLabelIsMutable()
      {
        if ((0x2 & this.bitField0_) != 2)
        {
          this.label_ = new LazyStringArrayList(this.label_);
          this.bitField0_ = (0x2 | this.bitField0_);
        }
      }

      private void maybeForceBuilderInitialization()
      {
      }

      public Builder addAllCost(Iterable<? extends LatticeP.Cost> paramIterable)
      {
        ensureCostIsMutable();
        GeneratedMessageLite.Builder.addAll(paramIterable, this.cost_);
        return this;
      }

      public Builder addAllEdgeIndex(Iterable<? extends Integer> paramIterable)
      {
        ensureEdgeIndexIsMutable();
        GeneratedMessageLite.Builder.addAll(paramIterable, this.edgeIndex_);
        return this;
      }

      public Builder addAllLabel(Iterable<String> paramIterable)
      {
        ensureLabelIsMutable();
        GeneratedMessageLite.Builder.addAll(paramIterable, this.label_);
        return this;
      }

      public Builder addCost(int paramInt, LatticeP.Cost.Builder paramBuilder)
      {
        ensureCostIsMutable();
        this.cost_.add(paramInt, paramBuilder.build());
        return this;
      }

      public Builder addCost(int paramInt, LatticeP.Cost paramCost)
      {
        if (paramCost == null)
          throw new NullPointerException();
        ensureCostIsMutable();
        this.cost_.add(paramInt, paramCost);
        return this;
      }

      public Builder addCost(LatticeP.Cost.Builder paramBuilder)
      {
        ensureCostIsMutable();
        this.cost_.add(paramBuilder.build());
        return this;
      }

      public Builder addCost(LatticeP.Cost paramCost)
      {
        if (paramCost == null)
          throw new NullPointerException();
        ensureCostIsMutable();
        this.cost_.add(paramCost);
        return this;
      }

      public Builder addEdgeIndex(int paramInt)
      {
        ensureEdgeIndexIsMutable();
        this.edgeIndex_.add(Integer.valueOf(paramInt));
        return this;
      }

      public Builder addLabel(String paramString)
      {
        if (paramString == null)
          throw new NullPointerException();
        ensureLabelIsMutable();
        this.label_.add(paramString);
        return this;
      }

      public Builder addLabelBytes(ByteString paramByteString)
      {
        if (paramByteString == null)
          throw new NullPointerException();
        ensureLabelIsMutable();
        this.label_.add(paramByteString);
        return this;
      }

      public LatticeP.Path build()
      {
        LatticeP.Path localPath = buildPartial();
        if (!localPath.isInitialized())
          throw newUninitializedMessageException(localPath);
        return localPath;
      }

      public LatticeP.Path buildPartial()
      {
        LatticeP.Path localPath = new LatticeP.Path(this, null);
        int i = this.bitField0_;
        int j = i & 0x1;
        int k = 0;
        if (j == 1)
          k = 0x0 | 0x1;
        LatticeP.Path.access$3402(localPath, this.text_);
        if ((0x2 & this.bitField0_) == 2)
        {
          this.label_ = new UnmodifiableLazyStringList(this.label_);
          this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
        }
        LatticeP.Path.access$3502(localPath, this.label_);
        if ((0x4 & this.bitField0_) == 4)
        {
          this.edgeIndex_ = Collections.unmodifiableList(this.edgeIndex_);
          this.bitField0_ = (0xFFFFFFFB & this.bitField0_);
        }
        LatticeP.Path.access$3602(localPath, this.edgeIndex_);
        if ((0x8 & this.bitField0_) == 8)
        {
          this.cost_ = Collections.unmodifiableList(this.cost_);
          this.bitField0_ = (0xFFFFFFF7 & this.bitField0_);
        }
        LatticeP.Path.access$3702(localPath, this.cost_);
        if ((i & 0x10) == 16)
          k |= 2;
        LatticeP.Path.access$3802(localPath, this.weightedCost_);
        if ((i & 0x20) == 32)
          k |= 4;
        LatticeP.Path.access$3902(localPath, this.description_);
        LatticeP.Path.access$4002(localPath, k);
        return localPath;
      }

      public Builder clear()
      {
        super.clear();
        this.text_ = "";
        this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
        this.label_ = LazyStringArrayList.EMPTY;
        this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
        this.edgeIndex_ = Collections.emptyList();
        this.bitField0_ = (0xFFFFFFFB & this.bitField0_);
        this.cost_ = Collections.emptyList();
        this.bitField0_ = (0xFFFFFFF7 & this.bitField0_);
        this.weightedCost_ = 0.0D;
        this.bitField0_ = (0xFFFFFFEF & this.bitField0_);
        this.description_ = "";
        this.bitField0_ = (0xFFFFFFDF & this.bitField0_);
        return this;
      }

      public Builder clearCost()
      {
        this.cost_ = Collections.emptyList();
        this.bitField0_ = (0xFFFFFFF7 & this.bitField0_);
        return this;
      }

      public Builder clearDescription()
      {
        this.bitField0_ = (0xFFFFFFDF & this.bitField0_);
        this.description_ = LatticeP.Path.getDefaultInstance().getDescription();
        return this;
      }

      public Builder clearEdgeIndex()
      {
        this.edgeIndex_ = Collections.emptyList();
        this.bitField0_ = (0xFFFFFFFB & this.bitField0_);
        return this;
      }

      public Builder clearLabel()
      {
        this.label_ = LazyStringArrayList.EMPTY;
        this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
        return this;
      }

      public Builder clearText()
      {
        this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
        this.text_ = LatticeP.Path.getDefaultInstance().getText();
        return this;
      }

      public Builder clearWeightedCost()
      {
        this.bitField0_ = (0xFFFFFFEF & this.bitField0_);
        this.weightedCost_ = 0.0D;
        return this;
      }

      public Builder clone()
      {
        return create().mergeFrom(buildPartial());
      }

      public LatticeP.Cost getCost(int paramInt)
      {
        return (LatticeP.Cost)this.cost_.get(paramInt);
      }

      public int getCostCount()
      {
        return this.cost_.size();
      }

      public List<LatticeP.Cost> getCostList()
      {
        return Collections.unmodifiableList(this.cost_);
      }

      public LatticeP.Path getDefaultInstanceForType()
      {
        return LatticeP.Path.getDefaultInstance();
      }

      public String getDescription()
      {
        Object localObject = this.description_;
        if (!(localObject instanceof String))
        {
          String str = ((ByteString)localObject).toStringUtf8();
          this.description_ = str;
          return str;
        }
        return (String)localObject;
      }

      public ByteString getDescriptionBytes()
      {
        Object localObject = this.description_;
        if ((localObject instanceof String))
        {
          ByteString localByteString = ByteString.copyFromUtf8((String)localObject);
          this.description_ = localByteString;
          return localByteString;
        }
        return (ByteString)localObject;
      }

      public int getEdgeIndex(int paramInt)
      {
        return ((Integer)this.edgeIndex_.get(paramInt)).intValue();
      }

      public int getEdgeIndexCount()
      {
        return this.edgeIndex_.size();
      }

      public List<Integer> getEdgeIndexList()
      {
        return Collections.unmodifiableList(this.edgeIndex_);
      }

      public String getLabel(int paramInt)
      {
        return (String)this.label_.get(paramInt);
      }

      public ByteString getLabelBytes(int paramInt)
      {
        return this.label_.getByteString(paramInt);
      }

      public int getLabelCount()
      {
        return this.label_.size();
      }

      public List<String> getLabelList()
      {
        return Collections.unmodifiableList(this.label_);
      }

      public String getText()
      {
        Object localObject = this.text_;
        if (!(localObject instanceof String))
        {
          String str = ((ByteString)localObject).toStringUtf8();
          this.text_ = str;
          return str;
        }
        return (String)localObject;
      }

      public ByteString getTextBytes()
      {
        Object localObject = this.text_;
        if ((localObject instanceof String))
        {
          ByteString localByteString = ByteString.copyFromUtf8((String)localObject);
          this.text_ = localByteString;
          return localByteString;
        }
        return (ByteString)localObject;
      }

      public double getWeightedCost()
      {
        return this.weightedCost_;
      }

      public boolean hasDescription()
      {
        return (0x20 & this.bitField0_) == 32;
      }

      public boolean hasText()
      {
        return (0x1 & this.bitField0_) == 1;
      }

      public boolean hasWeightedCost()
      {
        return (0x10 & this.bitField0_) == 16;
      }

      public final boolean isInitialized()
      {
        for (int i = 0; i < getCostCount(); i++)
          if (!getCost(i).isInitialized())
            return false;
        return true;
      }

      public Builder mergeFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
        throws IOException
      {
        LatticeP.Path localPath1 = null;
        try
        {
          LatticeP.Path localPath2 = (LatticeP.Path)LatticeP.Path.PARSER.parsePartialFrom(paramCodedInputStream, paramExtensionRegistryLite);
          return this;
        }
        catch (InvalidProtocolBufferException localInvalidProtocolBufferException)
        {
          localPath1 = (LatticeP.Path)localInvalidProtocolBufferException.getUnfinishedMessage();
          throw localInvalidProtocolBufferException;
        }
        finally
        {
          if (localPath1 != null)
            mergeFrom(localPath1);
        }
      }

      public Builder mergeFrom(LatticeP.Path paramPath)
      {
        if (paramPath == LatticeP.Path.getDefaultInstance())
          return this;
        if (paramPath.hasText())
        {
          this.bitField0_ = (0x1 | this.bitField0_);
          this.text_ = paramPath.text_;
        }
        if (!paramPath.label_.isEmpty())
        {
          if (this.label_.isEmpty())
          {
            this.label_ = paramPath.label_;
            this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
          }
        }
        else
        {
          label77: if (!paramPath.edgeIndex_.isEmpty())
          {
            if (!this.edgeIndex_.isEmpty())
              break label228;
            this.edgeIndex_ = paramPath.edgeIndex_;
            this.bitField0_ = (0xFFFFFFFB & this.bitField0_);
          }
          label120: if (!paramPath.cost_.isEmpty())
          {
            if (!this.cost_.isEmpty())
              break label249;
            this.cost_ = paramPath.cost_;
            this.bitField0_ = (0xFFFFFFF7 & this.bitField0_);
          }
        }
        while (true)
        {
          if (paramPath.hasWeightedCost())
            setWeightedCost(paramPath.getWeightedCost());
          if (!paramPath.hasDescription())
            break;
          this.bitField0_ = (0x20 | this.bitField0_);
          this.description_ = paramPath.description_;
          return this;
          ensureLabelIsMutable();
          this.label_.addAll(paramPath.label_);
          break label77;
          label228: ensureEdgeIndexIsMutable();
          this.edgeIndex_.addAll(paramPath.edgeIndex_);
          break label120;
          label249: ensureCostIsMutable();
          this.cost_.addAll(paramPath.cost_);
        }
      }

      public Builder removeCost(int paramInt)
      {
        ensureCostIsMutable();
        this.cost_.remove(paramInt);
        return this;
      }

      public Builder setCost(int paramInt, LatticeP.Cost.Builder paramBuilder)
      {
        ensureCostIsMutable();
        this.cost_.set(paramInt, paramBuilder.build());
        return this;
      }

      public Builder setCost(int paramInt, LatticeP.Cost paramCost)
      {
        if (paramCost == null)
          throw new NullPointerException();
        ensureCostIsMutable();
        this.cost_.set(paramInt, paramCost);
        return this;
      }

      public Builder setDescription(String paramString)
      {
        if (paramString == null)
          throw new NullPointerException();
        this.bitField0_ = (0x20 | this.bitField0_);
        this.description_ = paramString;
        return this;
      }

      public Builder setDescriptionBytes(ByteString paramByteString)
      {
        if (paramByteString == null)
          throw new NullPointerException();
        this.bitField0_ = (0x20 | this.bitField0_);
        this.description_ = paramByteString;
        return this;
      }

      public Builder setEdgeIndex(int paramInt1, int paramInt2)
      {
        ensureEdgeIndexIsMutable();
        this.edgeIndex_.set(paramInt1, Integer.valueOf(paramInt2));
        return this;
      }

      public Builder setLabel(int paramInt, String paramString)
      {
        if (paramString == null)
          throw new NullPointerException();
        ensureLabelIsMutable();
        this.label_.set(paramInt, paramString);
        return this;
      }

      public Builder setText(String paramString)
      {
        if (paramString == null)
          throw new NullPointerException();
        this.bitField0_ = (0x1 | this.bitField0_);
        this.text_ = paramString;
        return this;
      }

      public Builder setTextBytes(ByteString paramByteString)
      {
        if (paramByteString == null)
          throw new NullPointerException();
        this.bitField0_ = (0x1 | this.bitField0_);
        this.text_ = paramByteString;
        return this;
      }

      public Builder setWeightedCost(double paramDouble)
      {
        this.bitField0_ = (0x10 | this.bitField0_);
        this.weightedCost_ = paramDouble;
        return this;
      }
    }
  }

  public static abstract interface PathOrBuilder extends MessageLiteOrBuilder
  {
    public abstract LatticeP.Cost getCost(int paramInt);

    public abstract int getCostCount();

    public abstract List<LatticeP.Cost> getCostList();

    public abstract String getDescription();

    public abstract ByteString getDescriptionBytes();

    public abstract int getEdgeIndex(int paramInt);

    public abstract int getEdgeIndexCount();

    public abstract List<Integer> getEdgeIndexList();

    public abstract String getLabel(int paramInt);

    public abstract ByteString getLabelBytes(int paramInt);

    public abstract int getLabelCount();

    public abstract List<String> getLabelList();

    public abstract String getText();

    public abstract ByteString getTextBytes();

    public abstract double getWeightedCost();

    public abstract boolean hasDescription();

    public abstract boolean hasText();

    public abstract boolean hasWeightedCost();
  }

  public static final class State extends GeneratedMessageLite.ExtendableMessage<State>
    implements LatticeP.StateOrBuilder
  {
    public static final int DESCRIPTION_FIELD_NUMBER = 15;
    public static Parser<State> PARSER = new AbstractParser()
    {
      public LatticeP.State parsePartialFrom(CodedInputStream paramAnonymousCodedInputStream, ExtensionRegistryLite paramAnonymousExtensionRegistryLite)
        throws InvalidProtocolBufferException
      {
        return new LatticeP.State(paramAnonymousCodedInputStream, paramAnonymousExtensionRegistryLite, null);
      }
    };
    public static final int STATE_FIELD_NUMBER = 1;
    private static final State defaultInstance = new State(true);
    private static final long serialVersionUID;
    private int bitField0_;
    private LazyStringList description_;
    private byte memoizedIsInitialized = -1;
    private int memoizedSerializedSize = -1;
    private ByteString state_;

    static
    {
      defaultInstance.initFields();
    }

    // ERROR //
    private State(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      // Byte code:
      //   0: aload_0
      //   1: invokespecial 50	com/google/protobuf/GeneratedMessageLite$ExtendableMessage:<init>	()V
      //   4: aload_0
      //   5: iconst_m1
      //   6: putfield 52	com/google/protos/aksara/lattice/LatticeP$State:memoizedIsInitialized	B
      //   9: aload_0
      //   10: iconst_m1
      //   11: putfield 54	com/google/protos/aksara/lattice/LatticeP$State:memoizedSerializedSize	I
      //   14: aload_0
      //   15: invokespecial 44	com/google/protos/aksara/lattice/LatticeP$State:initFields	()V
      //   18: iconst_0
      //   19: istore_3
      //   20: iconst_0
      //   21: istore 4
      //   23: iload 4
      //   25: ifne +182 -> 207
      //   28: aload_1
      //   29: invokevirtual 60	com/google/protobuf/CodedInputStream:readTag	()I
      //   32: istore 8
      //   34: iload 8
      //   36: lookupswitch	default:+36->72, 0:+198->234, 10:+53->89, 122:+114->150
      //   73: aload_1
      //   74: aload_2
      //   75: iload 8
      //   77: invokevirtual 64	com/google/protos/aksara/lattice/LatticeP$State:parseUnknownField	(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z
      //   80: ifne -57 -> 23
      //   83: iconst_1
      //   84: istore 4
      //   86: goto -63 -> 23
      //   89: aload_0
      //   90: iconst_1
      //   91: aload_0
      //   92: getfield 66	com/google/protos/aksara/lattice/LatticeP$State:bitField0_	I
      //   95: ior
      //   96: putfield 66	com/google/protos/aksara/lattice/LatticeP$State:bitField0_	I
      //   99: aload_0
      //   100: aload_1
      //   101: invokevirtual 70	com/google/protobuf/CodedInputStream:readBytes	()Lcom/google/protobuf/ByteString;
      //   104: putfield 72	com/google/protos/aksara/lattice/LatticeP$State:state_	Lcom/google/protobuf/ByteString;
      //   107: goto -84 -> 23
      //   110: astore 7
      //   112: aload 7
      //   114: aload_0
      //   115: invokevirtual 76	com/google/protobuf/InvalidProtocolBufferException:setUnfinishedMessage	(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/InvalidProtocolBufferException;
      //   118: athrow
      //   119: astore 6
      //   121: iload_3
      //   122: iconst_2
      //   123: iand
      //   124: iconst_2
      //   125: if_icmpne +18 -> 143
      //   128: aload_0
      //   129: new 78	com/google/protobuf/UnmodifiableLazyStringList
      //   132: dup
      //   133: aload_0
      //   134: getfield 80	com/google/protos/aksara/lattice/LatticeP$State:description_	Lcom/google/protobuf/LazyStringList;
      //   137: invokespecial 83	com/google/protobuf/UnmodifiableLazyStringList:<init>	(Lcom/google/protobuf/LazyStringList;)V
      //   140: putfield 80	com/google/protos/aksara/lattice/LatticeP$State:description_	Lcom/google/protobuf/LazyStringList;
      //   143: aload_0
      //   144: invokevirtual 86	com/google/protos/aksara/lattice/LatticeP$State:makeExtensionsImmutable	()V
      //   147: aload 6
      //   149: athrow
      //   150: iload_3
      //   151: iconst_2
      //   152: iand
      //   153: iconst_2
      //   154: if_icmpeq +18 -> 172
      //   157: aload_0
      //   158: new 88	com/google/protobuf/LazyStringArrayList
      //   161: dup
      //   162: invokespecial 89	com/google/protobuf/LazyStringArrayList:<init>	()V
      //   165: putfield 80	com/google/protos/aksara/lattice/LatticeP$State:description_	Lcom/google/protobuf/LazyStringList;
      //   168: iload_3
      //   169: iconst_2
      //   170: ior
      //   171: istore_3
      //   172: aload_0
      //   173: getfield 80	com/google/protos/aksara/lattice/LatticeP$State:description_	Lcom/google/protobuf/LazyStringList;
      //   176: aload_1
      //   177: invokevirtual 70	com/google/protobuf/CodedInputStream:readBytes	()Lcom/google/protobuf/ByteString;
      //   180: invokeinterface 95 2 0
      //   185: goto -162 -> 23
      //   188: astore 5
      //   190: new 47	com/google/protobuf/InvalidProtocolBufferException
      //   193: dup
      //   194: aload 5
      //   196: invokevirtual 99	java/io/IOException:getMessage	()Ljava/lang/String;
      //   199: invokespecial 102	com/google/protobuf/InvalidProtocolBufferException:<init>	(Ljava/lang/String;)V
      //   202: aload_0
      //   203: invokevirtual 76	com/google/protobuf/InvalidProtocolBufferException:setUnfinishedMessage	(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/InvalidProtocolBufferException;
      //   206: athrow
      //   207: iload_3
      //   208: iconst_2
      //   209: iand
      //   210: iconst_2
      //   211: if_icmpne +18 -> 229
      //   214: aload_0
      //   215: new 78	com/google/protobuf/UnmodifiableLazyStringList
      //   218: dup
      //   219: aload_0
      //   220: getfield 80	com/google/protos/aksara/lattice/LatticeP$State:description_	Lcom/google/protobuf/LazyStringList;
      //   223: invokespecial 83	com/google/protobuf/UnmodifiableLazyStringList:<init>	(Lcom/google/protobuf/LazyStringList;)V
      //   226: putfield 80	com/google/protos/aksara/lattice/LatticeP$State:description_	Lcom/google/protobuf/LazyStringList;
      //   229: aload_0
      //   230: invokevirtual 86	com/google/protos/aksara/lattice/LatticeP$State:makeExtensionsImmutable	()V
      //   233: return
      //   234: iconst_1
      //   235: istore 4
      //   237: goto -214 -> 23
      //
      // Exception table:
      //   from	to	target	type
      //   28	34	110	com/google/protobuf/InvalidProtocolBufferException
      //   72	83	110	com/google/protobuf/InvalidProtocolBufferException
      //   89	107	110	com/google/protobuf/InvalidProtocolBufferException
      //   157	168	110	com/google/protobuf/InvalidProtocolBufferException
      //   172	185	110	com/google/protobuf/InvalidProtocolBufferException
      //   28	34	119	finally
      //   72	83	119	finally
      //   89	107	119	finally
      //   112	119	119	finally
      //   157	168	119	finally
      //   172	185	119	finally
      //   190	207	119	finally
      //   28	34	188	java/io/IOException
      //   72	83	188	java/io/IOException
      //   89	107	188	java/io/IOException
      //   157	168	188	java/io/IOException
      //   172	185	188	java/io/IOException
    }

    private State(GeneratedMessageLite.ExtendableBuilder<State, ?> paramExtendableBuilder)
    {
      super();
    }

    private State(boolean paramBoolean)
    {
    }

    public static State getDefaultInstance()
    {
      return defaultInstance;
    }

    private void initFields()
    {
      this.state_ = ByteString.EMPTY;
      this.description_ = LazyStringArrayList.EMPTY;
    }

    public static Builder newBuilder()
    {
      return Builder.access$1600();
    }

    public static Builder newBuilder(State paramState)
    {
      return newBuilder().mergeFrom(paramState);
    }

    public static State parseDelimitedFrom(InputStream paramInputStream)
      throws IOException
    {
      return (State)PARSER.parseDelimitedFrom(paramInputStream);
    }

    public static State parseDelimitedFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return (State)PARSER.parseDelimitedFrom(paramInputStream, paramExtensionRegistryLite);
    }

    public static State parseFrom(ByteString paramByteString)
      throws InvalidProtocolBufferException
    {
      return (State)PARSER.parseFrom(paramByteString);
    }

    public static State parseFrom(ByteString paramByteString, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return (State)PARSER.parseFrom(paramByteString, paramExtensionRegistryLite);
    }

    public static State parseFrom(CodedInputStream paramCodedInputStream)
      throws IOException
    {
      return (State)PARSER.parseFrom(paramCodedInputStream);
    }

    public static State parseFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return (State)PARSER.parseFrom(paramCodedInputStream, paramExtensionRegistryLite);
    }

    public static State parseFrom(InputStream paramInputStream)
      throws IOException
    {
      return (State)PARSER.parseFrom(paramInputStream);
    }

    public static State parseFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return (State)PARSER.parseFrom(paramInputStream, paramExtensionRegistryLite);
    }

    public static State parseFrom(byte[] paramArrayOfByte)
      throws InvalidProtocolBufferException
    {
      return (State)PARSER.parseFrom(paramArrayOfByte);
    }

    public static State parseFrom(byte[] paramArrayOfByte, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return (State)PARSER.parseFrom(paramArrayOfByte, paramExtensionRegistryLite);
    }

    public State getDefaultInstanceForType()
    {
      return defaultInstance;
    }

    public String getDescription(int paramInt)
    {
      return (String)this.description_.get(paramInt);
    }

    public ByteString getDescriptionBytes(int paramInt)
    {
      return this.description_.getByteString(paramInt);
    }

    public int getDescriptionCount()
    {
      return this.description_.size();
    }

    public List<String> getDescriptionList()
    {
      return this.description_;
    }

    public Parser<State> getParserForType()
    {
      return PARSER;
    }

    public int getSerializedSize()
    {
      int i = this.memoizedSerializedSize;
      if (i != -1)
        return i;
      int j = 0x1 & this.bitField0_;
      int k = 0;
      if (j == 1)
        k = 0 + CodedOutputStream.computeBytesSize(1, this.state_);
      int m = 0;
      for (int n = 0; n < this.description_.size(); n++)
        m += CodedOutputStream.computeBytesSizeNoTag(this.description_.getByteString(n));
      int i1 = k + m + 1 * getDescriptionList().size() + extensionsSerializedSize();
      this.memoizedSerializedSize = i1;
      return i1;
    }

    public ByteString getState()
    {
      return this.state_;
    }

    public boolean hasState()
    {
      return (0x1 & this.bitField0_) == 1;
    }

    public final boolean isInitialized()
    {
      int i = this.memoizedIsInitialized;
      if (i != -1)
        return i == 1;
      if (!extensionsAreInitialized())
      {
        this.memoizedIsInitialized = 0;
        return false;
      }
      this.memoizedIsInitialized = 1;
      return true;
    }

    public Builder newBuilderForType()
    {
      return newBuilder();
    }

    public Builder toBuilder()
    {
      return newBuilder(this);
    }

    protected Object writeReplace()
      throws ObjectStreamException
    {
      return super.writeReplace();
    }

    public void writeTo(CodedOutputStream paramCodedOutputStream)
      throws IOException
    {
      getSerializedSize();
      GeneratedMessageLite.ExtendableMessage.ExtensionWriter localExtensionWriter = newExtensionWriter();
      if ((0x1 & this.bitField0_) == 1)
        paramCodedOutputStream.writeBytes(1, this.state_);
      for (int i = 0; i < this.description_.size(); i++)
        paramCodedOutputStream.writeBytes(15, this.description_.getByteString(i));
      localExtensionWriter.writeUntil(536870912, paramCodedOutputStream);
    }

    public static final class Builder extends GeneratedMessageLite.ExtendableBuilder<LatticeP.State, Builder>
      implements LatticeP.StateOrBuilder
    {
      private int bitField0_;
      private LazyStringList description_ = LazyStringArrayList.EMPTY;
      private ByteString state_ = ByteString.EMPTY;

      private Builder()
      {
        maybeForceBuilderInitialization();
      }

      private static Builder create()
      {
        return new Builder();
      }

      private void ensureDescriptionIsMutable()
      {
        if ((0x2 & this.bitField0_) != 2)
        {
          this.description_ = new LazyStringArrayList(this.description_);
          this.bitField0_ = (0x2 | this.bitField0_);
        }
      }

      private void maybeForceBuilderInitialization()
      {
      }

      public Builder addAllDescription(Iterable<String> paramIterable)
      {
        ensureDescriptionIsMutable();
        GeneratedMessageLite.ExtendableBuilder.addAll(paramIterable, this.description_);
        return this;
      }

      public Builder addDescription(String paramString)
      {
        if (paramString == null)
          throw new NullPointerException();
        ensureDescriptionIsMutable();
        this.description_.add(paramString);
        return this;
      }

      public Builder addDescriptionBytes(ByteString paramByteString)
      {
        if (paramByteString == null)
          throw new NullPointerException();
        ensureDescriptionIsMutable();
        this.description_.add(paramByteString);
        return this;
      }

      public LatticeP.State build()
      {
        LatticeP.State localState = buildPartial();
        if (!localState.isInitialized())
          throw newUninitializedMessageException(localState);
        return localState;
      }

      public LatticeP.State buildPartial()
      {
        LatticeP.State localState = new LatticeP.State(this, null);
        int i = 0x1 & this.bitField0_;
        int j = 0;
        if (i == 1)
          j = 0x0 | 0x1;
        LatticeP.State.access$1802(localState, this.state_);
        if ((0x2 & this.bitField0_) == 2)
        {
          this.description_ = new UnmodifiableLazyStringList(this.description_);
          this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
        }
        LatticeP.State.access$1902(localState, this.description_);
        LatticeP.State.access$2002(localState, j);
        return localState;
      }

      public Builder clear()
      {
        super.clear();
        this.state_ = ByteString.EMPTY;
        this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
        this.description_ = LazyStringArrayList.EMPTY;
        this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
        return this;
      }

      public Builder clearDescription()
      {
        this.description_ = LazyStringArrayList.EMPTY;
        this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
        return this;
      }

      public Builder clearState()
      {
        this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
        this.state_ = LatticeP.State.getDefaultInstance().getState();
        return this;
      }

      public Builder clone()
      {
        return create().mergeFrom(buildPartial());
      }

      public LatticeP.State getDefaultInstanceForType()
      {
        return LatticeP.State.getDefaultInstance();
      }

      public String getDescription(int paramInt)
      {
        return (String)this.description_.get(paramInt);
      }

      public ByteString getDescriptionBytes(int paramInt)
      {
        return this.description_.getByteString(paramInt);
      }

      public int getDescriptionCount()
      {
        return this.description_.size();
      }

      public List<String> getDescriptionList()
      {
        return Collections.unmodifiableList(this.description_);
      }

      public ByteString getState()
      {
        return this.state_;
      }

      public boolean hasState()
      {
        return (0x1 & this.bitField0_) == 1;
      }

      public final boolean isInitialized()
      {
        return extensionsAreInitialized();
      }

      public Builder mergeFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
        throws IOException
      {
        LatticeP.State localState1 = null;
        try
        {
          LatticeP.State localState2 = (LatticeP.State)LatticeP.State.PARSER.parsePartialFrom(paramCodedInputStream, paramExtensionRegistryLite);
          return this;
        }
        catch (InvalidProtocolBufferException localInvalidProtocolBufferException)
        {
          localState1 = (LatticeP.State)localInvalidProtocolBufferException.getUnfinishedMessage();
          throw localInvalidProtocolBufferException;
        }
        finally
        {
          if (localState1 != null)
            mergeFrom(localState1);
        }
      }

      public Builder mergeFrom(LatticeP.State paramState)
      {
        if (paramState == LatticeP.State.getDefaultInstance())
          return this;
        if (paramState.hasState())
          setState(paramState.getState());
        if (!paramState.description_.isEmpty())
        {
          if (!this.description_.isEmpty())
            break label75;
          this.description_ = paramState.description_;
          this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
        }
        while (true)
        {
          mergeExtensionFields(paramState);
          return this;
          label75: ensureDescriptionIsMutable();
          this.description_.addAll(paramState.description_);
        }
      }

      public Builder setDescription(int paramInt, String paramString)
      {
        if (paramString == null)
          throw new NullPointerException();
        ensureDescriptionIsMutable();
        this.description_.set(paramInt, paramString);
        return this;
      }

      public Builder setState(ByteString paramByteString)
      {
        if (paramByteString == null)
          throw new NullPointerException();
        this.bitField0_ = (0x1 | this.bitField0_);
        this.state_ = paramByteString;
        return this;
      }
    }
  }

  public static abstract interface StateOrBuilder extends GeneratedMessageLite.ExtendableMessageOrBuilder<LatticeP.State>
  {
    public abstract String getDescription(int paramInt);

    public abstract ByteString getDescriptionBytes(int paramInt);

    public abstract int getDescriptionCount();

    public abstract List<String> getDescriptionList();

    public abstract ByteString getState();

    public abstract boolean hasState();
  }

  public static final class StateType extends GeneratedMessageLite
    implements LatticeP.StateTypeOrBuilder
  {
    public static final int DESCRIPTION_FIELD_NUMBER = 15;
    public static final int NAME_FIELD_NUMBER = 1;
    public static Parser<StateType> PARSER = new AbstractParser()
    {
      public LatticeP.StateType parsePartialFrom(CodedInputStream paramAnonymousCodedInputStream, ExtensionRegistryLite paramAnonymousExtensionRegistryLite)
        throws InvalidProtocolBufferException
      {
        return new LatticeP.StateType(paramAnonymousCodedInputStream, paramAnonymousExtensionRegistryLite, null);
      }
    };
    private static final StateType defaultInstance = new StateType(true);
    private static final long serialVersionUID;
    private int bitField0_;
    private LazyStringList description_;
    private byte memoizedIsInitialized = -1;
    private int memoizedSerializedSize = -1;
    private Object name_;

    static
    {
      defaultInstance.initFields();
    }

    // ERROR //
    private StateType(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      // Byte code:
      //   0: aload_0
      //   1: invokespecial 49	com/google/protobuf/GeneratedMessageLite:<init>	()V
      //   4: aload_0
      //   5: iconst_m1
      //   6: putfield 51	com/google/protos/aksara/lattice/LatticeP$StateType:memoizedIsInitialized	B
      //   9: aload_0
      //   10: iconst_m1
      //   11: putfield 53	com/google/protos/aksara/lattice/LatticeP$StateType:memoizedSerializedSize	I
      //   14: aload_0
      //   15: invokespecial 43	com/google/protos/aksara/lattice/LatticeP$StateType:initFields	()V
      //   18: iconst_0
      //   19: istore_3
      //   20: iconst_0
      //   21: istore 4
      //   23: iload 4
      //   25: ifne +182 -> 207
      //   28: aload_1
      //   29: invokevirtual 59	com/google/protobuf/CodedInputStream:readTag	()I
      //   32: istore 8
      //   34: iload 8
      //   36: lookupswitch	default:+36->72, 0:+198->234, 10:+53->89, 122:+114->150
      //   73: aload_1
      //   74: aload_2
      //   75: iload 8
      //   77: invokevirtual 63	com/google/protos/aksara/lattice/LatticeP$StateType:parseUnknownField	(Lcom/google/protobuf/CodedInputStream;Lcom/google/protobuf/ExtensionRegistryLite;I)Z
      //   80: ifne -57 -> 23
      //   83: iconst_1
      //   84: istore 4
      //   86: goto -63 -> 23
      //   89: aload_0
      //   90: iconst_1
      //   91: aload_0
      //   92: getfield 65	com/google/protos/aksara/lattice/LatticeP$StateType:bitField0_	I
      //   95: ior
      //   96: putfield 65	com/google/protos/aksara/lattice/LatticeP$StateType:bitField0_	I
      //   99: aload_0
      //   100: aload_1
      //   101: invokevirtual 69	com/google/protobuf/CodedInputStream:readBytes	()Lcom/google/protobuf/ByteString;
      //   104: putfield 71	com/google/protos/aksara/lattice/LatticeP$StateType:name_	Ljava/lang/Object;
      //   107: goto -84 -> 23
      //   110: astore 7
      //   112: aload 7
      //   114: aload_0
      //   115: invokevirtual 75	com/google/protobuf/InvalidProtocolBufferException:setUnfinishedMessage	(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/InvalidProtocolBufferException;
      //   118: athrow
      //   119: astore 6
      //   121: iload_3
      //   122: iconst_2
      //   123: iand
      //   124: iconst_2
      //   125: if_icmpne +18 -> 143
      //   128: aload_0
      //   129: new 77	com/google/protobuf/UnmodifiableLazyStringList
      //   132: dup
      //   133: aload_0
      //   134: getfield 79	com/google/protos/aksara/lattice/LatticeP$StateType:description_	Lcom/google/protobuf/LazyStringList;
      //   137: invokespecial 82	com/google/protobuf/UnmodifiableLazyStringList:<init>	(Lcom/google/protobuf/LazyStringList;)V
      //   140: putfield 79	com/google/protos/aksara/lattice/LatticeP$StateType:description_	Lcom/google/protobuf/LazyStringList;
      //   143: aload_0
      //   144: invokevirtual 85	com/google/protos/aksara/lattice/LatticeP$StateType:makeExtensionsImmutable	()V
      //   147: aload 6
      //   149: athrow
      //   150: iload_3
      //   151: iconst_2
      //   152: iand
      //   153: iconst_2
      //   154: if_icmpeq +18 -> 172
      //   157: aload_0
      //   158: new 87	com/google/protobuf/LazyStringArrayList
      //   161: dup
      //   162: invokespecial 88	com/google/protobuf/LazyStringArrayList:<init>	()V
      //   165: putfield 79	com/google/protos/aksara/lattice/LatticeP$StateType:description_	Lcom/google/protobuf/LazyStringList;
      //   168: iload_3
      //   169: iconst_2
      //   170: ior
      //   171: istore_3
      //   172: aload_0
      //   173: getfield 79	com/google/protos/aksara/lattice/LatticeP$StateType:description_	Lcom/google/protobuf/LazyStringList;
      //   176: aload_1
      //   177: invokevirtual 69	com/google/protobuf/CodedInputStream:readBytes	()Lcom/google/protobuf/ByteString;
      //   180: invokeinterface 94 2 0
      //   185: goto -162 -> 23
      //   188: astore 5
      //   190: new 46	com/google/protobuf/InvalidProtocolBufferException
      //   193: dup
      //   194: aload 5
      //   196: invokevirtual 98	java/io/IOException:getMessage	()Ljava/lang/String;
      //   199: invokespecial 101	com/google/protobuf/InvalidProtocolBufferException:<init>	(Ljava/lang/String;)V
      //   202: aload_0
      //   203: invokevirtual 75	com/google/protobuf/InvalidProtocolBufferException:setUnfinishedMessage	(Lcom/google/protobuf/MessageLite;)Lcom/google/protobuf/InvalidProtocolBufferException;
      //   206: athrow
      //   207: iload_3
      //   208: iconst_2
      //   209: iand
      //   210: iconst_2
      //   211: if_icmpne +18 -> 229
      //   214: aload_0
      //   215: new 77	com/google/protobuf/UnmodifiableLazyStringList
      //   218: dup
      //   219: aload_0
      //   220: getfield 79	com/google/protos/aksara/lattice/LatticeP$StateType:description_	Lcom/google/protobuf/LazyStringList;
      //   223: invokespecial 82	com/google/protobuf/UnmodifiableLazyStringList:<init>	(Lcom/google/protobuf/LazyStringList;)V
      //   226: putfield 79	com/google/protos/aksara/lattice/LatticeP$StateType:description_	Lcom/google/protobuf/LazyStringList;
      //   229: aload_0
      //   230: invokevirtual 85	com/google/protos/aksara/lattice/LatticeP$StateType:makeExtensionsImmutable	()V
      //   233: return
      //   234: iconst_1
      //   235: istore 4
      //   237: goto -214 -> 23
      //
      // Exception table:
      //   from	to	target	type
      //   28	34	110	com/google/protobuf/InvalidProtocolBufferException
      //   72	83	110	com/google/protobuf/InvalidProtocolBufferException
      //   89	107	110	com/google/protobuf/InvalidProtocolBufferException
      //   157	168	110	com/google/protobuf/InvalidProtocolBufferException
      //   172	185	110	com/google/protobuf/InvalidProtocolBufferException
      //   28	34	119	finally
      //   72	83	119	finally
      //   89	107	119	finally
      //   112	119	119	finally
      //   157	168	119	finally
      //   172	185	119	finally
      //   190	207	119	finally
      //   28	34	188	java/io/IOException
      //   72	83	188	java/io/IOException
      //   89	107	188	java/io/IOException
      //   157	168	188	java/io/IOException
      //   172	185	188	java/io/IOException
    }

    private StateType(GeneratedMessageLite.Builder paramBuilder)
    {
      super();
    }

    private StateType(boolean paramBoolean)
    {
    }

    public static StateType getDefaultInstance()
    {
      return defaultInstance;
    }

    private void initFields()
    {
      this.name_ = "";
      this.description_ = LazyStringArrayList.EMPTY;
    }

    public static Builder newBuilder()
    {
      return Builder.access$4200();
    }

    public static Builder newBuilder(StateType paramStateType)
    {
      return newBuilder().mergeFrom(paramStateType);
    }

    public static StateType parseDelimitedFrom(InputStream paramInputStream)
      throws IOException
    {
      return (StateType)PARSER.parseDelimitedFrom(paramInputStream);
    }

    public static StateType parseDelimitedFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return (StateType)PARSER.parseDelimitedFrom(paramInputStream, paramExtensionRegistryLite);
    }

    public static StateType parseFrom(ByteString paramByteString)
      throws InvalidProtocolBufferException
    {
      return (StateType)PARSER.parseFrom(paramByteString);
    }

    public static StateType parseFrom(ByteString paramByteString, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return (StateType)PARSER.parseFrom(paramByteString, paramExtensionRegistryLite);
    }

    public static StateType parseFrom(CodedInputStream paramCodedInputStream)
      throws IOException
    {
      return (StateType)PARSER.parseFrom(paramCodedInputStream);
    }

    public static StateType parseFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return (StateType)PARSER.parseFrom(paramCodedInputStream, paramExtensionRegistryLite);
    }

    public static StateType parseFrom(InputStream paramInputStream)
      throws IOException
    {
      return (StateType)PARSER.parseFrom(paramInputStream);
    }

    public static StateType parseFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return (StateType)PARSER.parseFrom(paramInputStream, paramExtensionRegistryLite);
    }

    public static StateType parseFrom(byte[] paramArrayOfByte)
      throws InvalidProtocolBufferException
    {
      return (StateType)PARSER.parseFrom(paramArrayOfByte);
    }

    public static StateType parseFrom(byte[] paramArrayOfByte, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return (StateType)PARSER.parseFrom(paramArrayOfByte, paramExtensionRegistryLite);
    }

    public StateType getDefaultInstanceForType()
    {
      return defaultInstance;
    }

    public String getDescription(int paramInt)
    {
      return (String)this.description_.get(paramInt);
    }

    public ByteString getDescriptionBytes(int paramInt)
    {
      return this.description_.getByteString(paramInt);
    }

    public int getDescriptionCount()
    {
      return this.description_.size();
    }

    public List<String> getDescriptionList()
    {
      return this.description_;
    }

    public String getName()
    {
      Object localObject = this.name_;
      if ((localObject instanceof String))
        return (String)localObject;
      ByteString localByteString = (ByteString)localObject;
      String str = localByteString.toStringUtf8();
      if (localByteString.isValidUtf8())
        this.name_ = str;
      return str;
    }

    public ByteString getNameBytes()
    {
      Object localObject = this.name_;
      if ((localObject instanceof String))
      {
        ByteString localByteString = ByteString.copyFromUtf8((String)localObject);
        this.name_ = localByteString;
        return localByteString;
      }
      return (ByteString)localObject;
    }

    public Parser<StateType> getParserForType()
    {
      return PARSER;
    }

    public int getSerializedSize()
    {
      int i = this.memoizedSerializedSize;
      if (i != -1)
        return i;
      int j = 0x1 & this.bitField0_;
      int k = 0;
      if (j == 1)
        k = 0 + CodedOutputStream.computeBytesSize(1, getNameBytes());
      int m = 0;
      for (int n = 0; n < this.description_.size(); n++)
        m += CodedOutputStream.computeBytesSizeNoTag(this.description_.getByteString(n));
      int i1 = k + m + 1 * getDescriptionList().size();
      this.memoizedSerializedSize = i1;
      return i1;
    }

    public boolean hasName()
    {
      return (0x1 & this.bitField0_) == 1;
    }

    public final boolean isInitialized()
    {
      int i = this.memoizedIsInitialized;
      if (i != -1)
        return i == 1;
      this.memoizedIsInitialized = 1;
      return true;
    }

    public Builder newBuilderForType()
    {
      return newBuilder();
    }

    public Builder toBuilder()
    {
      return newBuilder(this);
    }

    protected Object writeReplace()
      throws ObjectStreamException
    {
      return super.writeReplace();
    }

    public void writeTo(CodedOutputStream paramCodedOutputStream)
      throws IOException
    {
      getSerializedSize();
      if ((0x1 & this.bitField0_) == 1)
        paramCodedOutputStream.writeBytes(1, getNameBytes());
      for (int i = 0; i < this.description_.size(); i++)
        paramCodedOutputStream.writeBytes(15, this.description_.getByteString(i));
    }

    public static final class Builder extends GeneratedMessageLite.Builder<LatticeP.StateType, Builder>
      implements LatticeP.StateTypeOrBuilder
    {
      private int bitField0_;
      private LazyStringList description_ = LazyStringArrayList.EMPTY;
      private Object name_ = "";

      private Builder()
      {
        maybeForceBuilderInitialization();
      }

      private static Builder create()
      {
        return new Builder();
      }

      private void ensureDescriptionIsMutable()
      {
        if ((0x2 & this.bitField0_) != 2)
        {
          this.description_ = new LazyStringArrayList(this.description_);
          this.bitField0_ = (0x2 | this.bitField0_);
        }
      }

      private void maybeForceBuilderInitialization()
      {
      }

      public Builder addAllDescription(Iterable<String> paramIterable)
      {
        ensureDescriptionIsMutable();
        GeneratedMessageLite.Builder.addAll(paramIterable, this.description_);
        return this;
      }

      public Builder addDescription(String paramString)
      {
        if (paramString == null)
          throw new NullPointerException();
        ensureDescriptionIsMutable();
        this.description_.add(paramString);
        return this;
      }

      public Builder addDescriptionBytes(ByteString paramByteString)
      {
        if (paramByteString == null)
          throw new NullPointerException();
        ensureDescriptionIsMutable();
        this.description_.add(paramByteString);
        return this;
      }

      public LatticeP.StateType build()
      {
        LatticeP.StateType localStateType = buildPartial();
        if (!localStateType.isInitialized())
          throw newUninitializedMessageException(localStateType);
        return localStateType;
      }

      public LatticeP.StateType buildPartial()
      {
        LatticeP.StateType localStateType = new LatticeP.StateType(this, null);
        int i = 0x1 & this.bitField0_;
        int j = 0;
        if (i == 1)
          j = 0x0 | 0x1;
        LatticeP.StateType.access$4402(localStateType, this.name_);
        if ((0x2 & this.bitField0_) == 2)
        {
          this.description_ = new UnmodifiableLazyStringList(this.description_);
          this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
        }
        LatticeP.StateType.access$4502(localStateType, this.description_);
        LatticeP.StateType.access$4602(localStateType, j);
        return localStateType;
      }

      public Builder clear()
      {
        super.clear();
        this.name_ = "";
        this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
        this.description_ = LazyStringArrayList.EMPTY;
        this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
        return this;
      }

      public Builder clearDescription()
      {
        this.description_ = LazyStringArrayList.EMPTY;
        this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
        return this;
      }

      public Builder clearName()
      {
        this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
        this.name_ = LatticeP.StateType.getDefaultInstance().getName();
        return this;
      }

      public Builder clone()
      {
        return create().mergeFrom(buildPartial());
      }

      public LatticeP.StateType getDefaultInstanceForType()
      {
        return LatticeP.StateType.getDefaultInstance();
      }

      public String getDescription(int paramInt)
      {
        return (String)this.description_.get(paramInt);
      }

      public ByteString getDescriptionBytes(int paramInt)
      {
        return this.description_.getByteString(paramInt);
      }

      public int getDescriptionCount()
      {
        return this.description_.size();
      }

      public List<String> getDescriptionList()
      {
        return Collections.unmodifiableList(this.description_);
      }

      public String getName()
      {
        Object localObject = this.name_;
        if (!(localObject instanceof String))
        {
          String str = ((ByteString)localObject).toStringUtf8();
          this.name_ = str;
          return str;
        }
        return (String)localObject;
      }

      public ByteString getNameBytes()
      {
        Object localObject = this.name_;
        if ((localObject instanceof String))
        {
          ByteString localByteString = ByteString.copyFromUtf8((String)localObject);
          this.name_ = localByteString;
          return localByteString;
        }
        return (ByteString)localObject;
      }

      public boolean hasName()
      {
        return (0x1 & this.bitField0_) == 1;
      }

      public final boolean isInitialized()
      {
        return true;
      }

      public Builder mergeFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
        throws IOException
      {
        LatticeP.StateType localStateType1 = null;
        try
        {
          LatticeP.StateType localStateType2 = (LatticeP.StateType)LatticeP.StateType.PARSER.parsePartialFrom(paramCodedInputStream, paramExtensionRegistryLite);
          return this;
        }
        catch (InvalidProtocolBufferException localInvalidProtocolBufferException)
        {
          localStateType1 = (LatticeP.StateType)localInvalidProtocolBufferException.getUnfinishedMessage();
          throw localInvalidProtocolBufferException;
        }
        finally
        {
          if (localStateType1 != null)
            mergeFrom(localStateType1);
        }
      }

      public Builder mergeFrom(LatticeP.StateType paramStateType)
      {
        if (paramStateType == LatticeP.StateType.getDefaultInstance());
        do
        {
          return this;
          if (paramStateType.hasName())
          {
            this.bitField0_ = (0x1 | this.bitField0_);
            this.name_ = paramStateType.name_;
          }
        }
        while (paramStateType.description_.isEmpty());
        if (this.description_.isEmpty())
        {
          this.description_ = paramStateType.description_;
          this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
          return this;
        }
        ensureDescriptionIsMutable();
        this.description_.addAll(paramStateType.description_);
        return this;
      }

      public Builder setDescription(int paramInt, String paramString)
      {
        if (paramString == null)
          throw new NullPointerException();
        ensureDescriptionIsMutable();
        this.description_.set(paramInt, paramString);
        return this;
      }

      public Builder setName(String paramString)
      {
        if (paramString == null)
          throw new NullPointerException();
        this.bitField0_ = (0x1 | this.bitField0_);
        this.name_ = paramString;
        return this;
      }

      public Builder setNameBytes(ByteString paramByteString)
      {
        if (paramByteString == null)
          throw new NullPointerException();
        this.bitField0_ = (0x1 | this.bitField0_);
        this.name_ = paramByteString;
        return this;
      }
    }
  }

  public static abstract interface StateTypeOrBuilder extends MessageLiteOrBuilder
  {
    public abstract String getDescription(int paramInt);

    public abstract ByteString getDescriptionBytes(int paramInt);

    public abstract int getDescriptionCount();

    public abstract List<String> getDescriptionList();

    public abstract String getName();

    public abstract ByteString getNameBytes();

    public abstract boolean hasName();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.protos.aksara.lattice.LatticeP
 * JD-Core Version:    0.6.2
 */