package com.google.protos.research_handwriting;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.GeneratedMessageLite.Builder;
import com.google.protobuf.Internal;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.UninitializedMessageException;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectStreamException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class OndeviceSpec
{
  public static void registerAllExtensions(ExtensionRegistryLite paramExtensionRegistryLite)
  {
  }

  public static final class CloudRecognizerSpec extends GeneratedMessageLite
    implements OndeviceSpec.CloudRecognizerSpecOrBuilder
  {
    public static final int CLIENT_NAME_FIELD_NUMBER = 6;
    public static final int CLIENT_VERSION_FIELD_NUMBER = 7;
    public static final int COMPRESS_REQUESTS_FIELD_NUMBER = 3;
    public static final int DEVICE_NAME_FIELD_NUMBER = 4;
    public static final int DEVICE_VERSION_FIELD_NUMBER = 5;
    public static final int FEEDBACK_BATCH_SIZE_FIELD_NUMBER = 9;
    public static final int RECO_PATH_FIELD_NUMBER = 2;
    public static final int SEND_FEEDBACK_IMMEDIATELY_FIELD_NUMBER = 8;
    public static final int SERVER_FIELD_NUMBER = 1;
    private static final CloudRecognizerSpec defaultInstance = new CloudRecognizerSpec(true);
    private static final long serialVersionUID;
    private int bitField0_;
    private Object clientName_;
    private int clientVersion_;
    private boolean compressRequests_;
    private Object deviceName_;
    private int deviceVersion_;
    private int feedbackBatchSize_;
    private byte memoizedIsInitialized = -1;
    private int memoizedSerializedSize = -1;
    private Object recoPath_;
    private boolean sendFeedbackImmediately_;
    private Object server_;

    static
    {
      defaultInstance.initFields();
    }

    private CloudRecognizerSpec(Builder paramBuilder)
    {
      super();
    }

    private CloudRecognizerSpec(boolean paramBoolean)
    {
    }

    private ByteString getClientNameBytes()
    {
      Object localObject = this.clientName_;
      if ((localObject instanceof String))
      {
        ByteString localByteString = ByteString.copyFromUtf8((String)localObject);
        this.clientName_ = localByteString;
        return localByteString;
      }
      return (ByteString)localObject;
    }

    public static CloudRecognizerSpec getDefaultInstance()
    {
      return defaultInstance;
    }

    private ByteString getDeviceNameBytes()
    {
      Object localObject = this.deviceName_;
      if ((localObject instanceof String))
      {
        ByteString localByteString = ByteString.copyFromUtf8((String)localObject);
        this.deviceName_ = localByteString;
        return localByteString;
      }
      return (ByteString)localObject;
    }

    private ByteString getRecoPathBytes()
    {
      Object localObject = this.recoPath_;
      if ((localObject instanceof String))
      {
        ByteString localByteString = ByteString.copyFromUtf8((String)localObject);
        this.recoPath_ = localByteString;
        return localByteString;
      }
      return (ByteString)localObject;
    }

    private ByteString getServerBytes()
    {
      Object localObject = this.server_;
      if ((localObject instanceof String))
      {
        ByteString localByteString = ByteString.copyFromUtf8((String)localObject);
        this.server_ = localByteString;
        return localByteString;
      }
      return (ByteString)localObject;
    }

    private void initFields()
    {
      this.server_ = "";
      this.recoPath_ = "";
      this.compressRequests_ = true;
      this.deviceName_ = "";
      this.deviceVersion_ = 0;
      this.clientName_ = "";
      this.clientVersion_ = 0;
      this.sendFeedbackImmediately_ = true;
      this.feedbackBatchSize_ = 5;
    }

    public static Builder newBuilder()
    {
      return Builder.access$2000();
    }

    public static Builder newBuilder(CloudRecognizerSpec paramCloudRecognizerSpec)
    {
      return newBuilder().mergeFrom(paramCloudRecognizerSpec);
    }

    public static CloudRecognizerSpec parseDelimitedFrom(InputStream paramInputStream)
      throws IOException
    {
      Builder localBuilder = newBuilder();
      if (localBuilder.mergeDelimitedFrom(paramInputStream))
        return localBuilder.buildParsed();
      return null;
    }

    public static CloudRecognizerSpec parseDelimitedFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      Builder localBuilder = newBuilder();
      if (localBuilder.mergeDelimitedFrom(paramInputStream, paramExtensionRegistryLite))
        return localBuilder.buildParsed();
      return null;
    }

    public static CloudRecognizerSpec parseFrom(ByteString paramByteString)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramByteString)).buildParsed();
    }

    public static CloudRecognizerSpec parseFrom(ByteString paramByteString, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramByteString, paramExtensionRegistryLite)).buildParsed();
    }

    public static CloudRecognizerSpec parseFrom(CodedInputStream paramCodedInputStream)
      throws IOException
    {
      return ((Builder)newBuilder().mergeFrom(paramCodedInputStream)).buildParsed();
    }

    public static CloudRecognizerSpec parseFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return newBuilder().mergeFrom(paramCodedInputStream, paramExtensionRegistryLite).buildParsed();
    }

    public static CloudRecognizerSpec parseFrom(InputStream paramInputStream)
      throws IOException
    {
      return ((Builder)newBuilder().mergeFrom(paramInputStream)).buildParsed();
    }

    public static CloudRecognizerSpec parseFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return ((Builder)newBuilder().mergeFrom(paramInputStream, paramExtensionRegistryLite)).buildParsed();
    }

    public static CloudRecognizerSpec parseFrom(byte[] paramArrayOfByte)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramArrayOfByte)).buildParsed();
    }

    public static CloudRecognizerSpec parseFrom(byte[] paramArrayOfByte, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramArrayOfByte, paramExtensionRegistryLite)).buildParsed();
    }

    public String getClientName()
    {
      Object localObject = this.clientName_;
      if ((localObject instanceof String))
        return (String)localObject;
      ByteString localByteString = (ByteString)localObject;
      String str = localByteString.toStringUtf8();
      if (Internal.isValidUtf8(localByteString))
        this.clientName_ = str;
      return str;
    }

    public int getClientVersion()
    {
      return this.clientVersion_;
    }

    public boolean getCompressRequests()
    {
      return this.compressRequests_;
    }

    public CloudRecognizerSpec getDefaultInstanceForType()
    {
      return defaultInstance;
    }

    public String getDeviceName()
    {
      Object localObject = this.deviceName_;
      if ((localObject instanceof String))
        return (String)localObject;
      ByteString localByteString = (ByteString)localObject;
      String str = localByteString.toStringUtf8();
      if (Internal.isValidUtf8(localByteString))
        this.deviceName_ = str;
      return str;
    }

    public int getDeviceVersion()
    {
      return this.deviceVersion_;
    }

    public int getFeedbackBatchSize()
    {
      return this.feedbackBatchSize_;
    }

    public String getRecoPath()
    {
      Object localObject = this.recoPath_;
      if ((localObject instanceof String))
        return (String)localObject;
      ByteString localByteString = (ByteString)localObject;
      String str = localByteString.toStringUtf8();
      if (Internal.isValidUtf8(localByteString))
        this.recoPath_ = str;
      return str;
    }

    public boolean getSendFeedbackImmediately()
    {
      return this.sendFeedbackImmediately_;
    }

    public int getSerializedSize()
    {
      int i = this.memoizedSerializedSize;
      if (i != -1)
        return i;
      int j = 0x1 & this.bitField0_;
      int k = 0;
      if (j == 1)
        k = 0 + CodedOutputStream.computeBytesSize(1, getServerBytes());
      if ((0x2 & this.bitField0_) == 2)
        k += CodedOutputStream.computeBytesSize(2, getRecoPathBytes());
      if ((0x4 & this.bitField0_) == 4)
        k += CodedOutputStream.computeBoolSize(3, this.compressRequests_);
      if ((0x8 & this.bitField0_) == 8)
        k += CodedOutputStream.computeBytesSize(4, getDeviceNameBytes());
      if ((0x10 & this.bitField0_) == 16)
        k += CodedOutputStream.computeInt32Size(5, this.deviceVersion_);
      if ((0x20 & this.bitField0_) == 32)
        k += CodedOutputStream.computeBytesSize(6, getClientNameBytes());
      if ((0x40 & this.bitField0_) == 64)
        k += CodedOutputStream.computeInt32Size(7, this.clientVersion_);
      if ((0x80 & this.bitField0_) == 128)
        k += CodedOutputStream.computeBoolSize(8, this.sendFeedbackImmediately_);
      if ((0x100 & this.bitField0_) == 256)
        k += CodedOutputStream.computeInt32Size(9, this.feedbackBatchSize_);
      this.memoizedSerializedSize = k;
      return k;
    }

    public String getServer()
    {
      Object localObject = this.server_;
      if ((localObject instanceof String))
        return (String)localObject;
      ByteString localByteString = (ByteString)localObject;
      String str = localByteString.toStringUtf8();
      if (Internal.isValidUtf8(localByteString))
        this.server_ = str;
      return str;
    }

    public boolean hasClientName()
    {
      return (0x20 & this.bitField0_) == 32;
    }

    public boolean hasClientVersion()
    {
      return (0x40 & this.bitField0_) == 64;
    }

    public boolean hasCompressRequests()
    {
      return (0x4 & this.bitField0_) == 4;
    }

    public boolean hasDeviceName()
    {
      return (0x8 & this.bitField0_) == 8;
    }

    public boolean hasDeviceVersion()
    {
      return (0x10 & this.bitField0_) == 16;
    }

    public boolean hasFeedbackBatchSize()
    {
      return (0x100 & this.bitField0_) == 256;
    }

    public boolean hasRecoPath()
    {
      return (0x2 & this.bitField0_) == 2;
    }

    public boolean hasSendFeedbackImmediately()
    {
      return (0x80 & this.bitField0_) == 128;
    }

    public boolean hasServer()
    {
      return (0x1 & this.bitField0_) == 1;
    }

    public final boolean isInitialized()
    {
      int i = this.memoizedIsInitialized;
      if (i != -1)
        return i == 1;
      this.memoizedIsInitialized = 1;
      return true;
    }

    public Builder newBuilderForType()
    {
      return newBuilder();
    }

    public Builder toBuilder()
    {
      return newBuilder(this);
    }

    protected Object writeReplace()
      throws ObjectStreamException
    {
      return super.writeReplace();
    }

    public void writeTo(CodedOutputStream paramCodedOutputStream)
      throws IOException
    {
      getSerializedSize();
      if ((0x1 & this.bitField0_) == 1)
        paramCodedOutputStream.writeBytes(1, getServerBytes());
      if ((0x2 & this.bitField0_) == 2)
        paramCodedOutputStream.writeBytes(2, getRecoPathBytes());
      if ((0x4 & this.bitField0_) == 4)
        paramCodedOutputStream.writeBool(3, this.compressRequests_);
      if ((0x8 & this.bitField0_) == 8)
        paramCodedOutputStream.writeBytes(4, getDeviceNameBytes());
      if ((0x10 & this.bitField0_) == 16)
        paramCodedOutputStream.writeInt32(5, this.deviceVersion_);
      if ((0x20 & this.bitField0_) == 32)
        paramCodedOutputStream.writeBytes(6, getClientNameBytes());
      if ((0x40 & this.bitField0_) == 64)
        paramCodedOutputStream.writeInt32(7, this.clientVersion_);
      if ((0x80 & this.bitField0_) == 128)
        paramCodedOutputStream.writeBool(8, this.sendFeedbackImmediately_);
      if ((0x100 & this.bitField0_) == 256)
        paramCodedOutputStream.writeInt32(9, this.feedbackBatchSize_);
    }

    public static final class Builder extends GeneratedMessageLite.Builder<OndeviceSpec.CloudRecognizerSpec, Builder>
      implements OndeviceSpec.CloudRecognizerSpecOrBuilder
    {
      private int bitField0_;
      private Object clientName_ = "";
      private int clientVersion_;
      private boolean compressRequests_ = true;
      private Object deviceName_ = "";
      private int deviceVersion_;
      private int feedbackBatchSize_ = 5;
      private Object recoPath_ = "";
      private boolean sendFeedbackImmediately_ = true;
      private Object server_ = "";

      private Builder()
      {
        maybeForceBuilderInitialization();
      }

      private OndeviceSpec.CloudRecognizerSpec buildParsed()
        throws InvalidProtocolBufferException
      {
        OndeviceSpec.CloudRecognizerSpec localCloudRecognizerSpec = buildPartial();
        if (!localCloudRecognizerSpec.isInitialized())
          throw newUninitializedMessageException(localCloudRecognizerSpec).asInvalidProtocolBufferException();
        return localCloudRecognizerSpec;
      }

      private static Builder create()
      {
        return new Builder();
      }

      private void maybeForceBuilderInitialization()
      {
      }

      public OndeviceSpec.CloudRecognizerSpec build()
      {
        OndeviceSpec.CloudRecognizerSpec localCloudRecognizerSpec = buildPartial();
        if (!localCloudRecognizerSpec.isInitialized())
          throw newUninitializedMessageException(localCloudRecognizerSpec);
        return localCloudRecognizerSpec;
      }

      public OndeviceSpec.CloudRecognizerSpec buildPartial()
      {
        int i = 1;
        OndeviceSpec.CloudRecognizerSpec localCloudRecognizerSpec = new OndeviceSpec.CloudRecognizerSpec(this, null);
        int j = this.bitField0_;
        if ((j & 0x1) == i);
        while (true)
        {
          OndeviceSpec.CloudRecognizerSpec.access$2202(localCloudRecognizerSpec, this.server_);
          if ((j & 0x2) == 2)
            i |= 2;
          OndeviceSpec.CloudRecognizerSpec.access$2302(localCloudRecognizerSpec, this.recoPath_);
          if ((j & 0x4) == 4)
            i |= 4;
          OndeviceSpec.CloudRecognizerSpec.access$2402(localCloudRecognizerSpec, this.compressRequests_);
          if ((j & 0x8) == 8)
            i |= 8;
          OndeviceSpec.CloudRecognizerSpec.access$2502(localCloudRecognizerSpec, this.deviceName_);
          if ((j & 0x10) == 16)
            i |= 16;
          OndeviceSpec.CloudRecognizerSpec.access$2602(localCloudRecognizerSpec, this.deviceVersion_);
          if ((j & 0x20) == 32)
            i |= 32;
          OndeviceSpec.CloudRecognizerSpec.access$2702(localCloudRecognizerSpec, this.clientName_);
          if ((j & 0x40) == 64)
            i |= 64;
          OndeviceSpec.CloudRecognizerSpec.access$2802(localCloudRecognizerSpec, this.clientVersion_);
          if ((j & 0x80) == 128)
            i |= 128;
          OndeviceSpec.CloudRecognizerSpec.access$2902(localCloudRecognizerSpec, this.sendFeedbackImmediately_);
          if ((j & 0x100) == 256)
            i |= 256;
          OndeviceSpec.CloudRecognizerSpec.access$3002(localCloudRecognizerSpec, this.feedbackBatchSize_);
          OndeviceSpec.CloudRecognizerSpec.access$3102(localCloudRecognizerSpec, i);
          return localCloudRecognizerSpec;
          i = 0;
        }
      }

      public Builder clear()
      {
        super.clear();
        this.server_ = "";
        this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
        this.recoPath_ = "";
        this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
        this.compressRequests_ = true;
        this.bitField0_ = (0xFFFFFFFB & this.bitField0_);
        this.deviceName_ = "";
        this.bitField0_ = (0xFFFFFFF7 & this.bitField0_);
        this.deviceVersion_ = 0;
        this.bitField0_ = (0xFFFFFFEF & this.bitField0_);
        this.clientName_ = "";
        this.bitField0_ = (0xFFFFFFDF & this.bitField0_);
        this.clientVersion_ = 0;
        this.bitField0_ = (0xFFFFFFBF & this.bitField0_);
        this.sendFeedbackImmediately_ = true;
        this.bitField0_ = (0xFFFFFF7F & this.bitField0_);
        this.feedbackBatchSize_ = 5;
        this.bitField0_ = (0xFFFFFEFF & this.bitField0_);
        return this;
      }

      public Builder clearClientName()
      {
        this.bitField0_ = (0xFFFFFFDF & this.bitField0_);
        this.clientName_ = OndeviceSpec.CloudRecognizerSpec.getDefaultInstance().getClientName();
        return this;
      }

      public Builder clearClientVersion()
      {
        this.bitField0_ = (0xFFFFFFBF & this.bitField0_);
        this.clientVersion_ = 0;
        return this;
      }

      public Builder clearCompressRequests()
      {
        this.bitField0_ = (0xFFFFFFFB & this.bitField0_);
        this.compressRequests_ = true;
        return this;
      }

      public Builder clearDeviceName()
      {
        this.bitField0_ = (0xFFFFFFF7 & this.bitField0_);
        this.deviceName_ = OndeviceSpec.CloudRecognizerSpec.getDefaultInstance().getDeviceName();
        return this;
      }

      public Builder clearDeviceVersion()
      {
        this.bitField0_ = (0xFFFFFFEF & this.bitField0_);
        this.deviceVersion_ = 0;
        return this;
      }

      public Builder clearFeedbackBatchSize()
      {
        this.bitField0_ = (0xFFFFFEFF & this.bitField0_);
        this.feedbackBatchSize_ = 5;
        return this;
      }

      public Builder clearRecoPath()
      {
        this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
        this.recoPath_ = OndeviceSpec.CloudRecognizerSpec.getDefaultInstance().getRecoPath();
        return this;
      }

      public Builder clearSendFeedbackImmediately()
      {
        this.bitField0_ = (0xFFFFFF7F & this.bitField0_);
        this.sendFeedbackImmediately_ = true;
        return this;
      }

      public Builder clearServer()
      {
        this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
        this.server_ = OndeviceSpec.CloudRecognizerSpec.getDefaultInstance().getServer();
        return this;
      }

      public Builder clone()
      {
        return create().mergeFrom(buildPartial());
      }

      public String getClientName()
      {
        Object localObject = this.clientName_;
        if (!(localObject instanceof String))
        {
          String str = ((ByteString)localObject).toStringUtf8();
          this.clientName_ = str;
          return str;
        }
        return (String)localObject;
      }

      public int getClientVersion()
      {
        return this.clientVersion_;
      }

      public boolean getCompressRequests()
      {
        return this.compressRequests_;
      }

      public OndeviceSpec.CloudRecognizerSpec getDefaultInstanceForType()
      {
        return OndeviceSpec.CloudRecognizerSpec.getDefaultInstance();
      }

      public String getDeviceName()
      {
        Object localObject = this.deviceName_;
        if (!(localObject instanceof String))
        {
          String str = ((ByteString)localObject).toStringUtf8();
          this.deviceName_ = str;
          return str;
        }
        return (String)localObject;
      }

      public int getDeviceVersion()
      {
        return this.deviceVersion_;
      }

      public int getFeedbackBatchSize()
      {
        return this.feedbackBatchSize_;
      }

      public String getRecoPath()
      {
        Object localObject = this.recoPath_;
        if (!(localObject instanceof String))
        {
          String str = ((ByteString)localObject).toStringUtf8();
          this.recoPath_ = str;
          return str;
        }
        return (String)localObject;
      }

      public boolean getSendFeedbackImmediately()
      {
        return this.sendFeedbackImmediately_;
      }

      public String getServer()
      {
        Object localObject = this.server_;
        if (!(localObject instanceof String))
        {
          String str = ((ByteString)localObject).toStringUtf8();
          this.server_ = str;
          return str;
        }
        return (String)localObject;
      }

      public boolean hasClientName()
      {
        return (0x20 & this.bitField0_) == 32;
      }

      public boolean hasClientVersion()
      {
        return (0x40 & this.bitField0_) == 64;
      }

      public boolean hasCompressRequests()
      {
        return (0x4 & this.bitField0_) == 4;
      }

      public boolean hasDeviceName()
      {
        return (0x8 & this.bitField0_) == 8;
      }

      public boolean hasDeviceVersion()
      {
        return (0x10 & this.bitField0_) == 16;
      }

      public boolean hasFeedbackBatchSize()
      {
        return (0x100 & this.bitField0_) == 256;
      }

      public boolean hasRecoPath()
      {
        return (0x2 & this.bitField0_) == 2;
      }

      public boolean hasSendFeedbackImmediately()
      {
        return (0x80 & this.bitField0_) == 128;
      }

      public boolean hasServer()
      {
        return (0x1 & this.bitField0_) == 1;
      }

      public final boolean isInitialized()
      {
        return true;
      }

      public Builder mergeFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
        throws IOException
      {
        while (true)
        {
          int i = paramCodedInputStream.readTag();
          switch (i)
          {
          default:
            if (parseUnknownField(paramCodedInputStream, paramExtensionRegistryLite, i))
              continue;
          case 0:
            return this;
          case 10:
            this.bitField0_ = (0x1 | this.bitField0_);
            this.server_ = paramCodedInputStream.readBytes();
            break;
          case 18:
            this.bitField0_ = (0x2 | this.bitField0_);
            this.recoPath_ = paramCodedInputStream.readBytes();
            break;
          case 24:
            this.bitField0_ = (0x4 | this.bitField0_);
            this.compressRequests_ = paramCodedInputStream.readBool();
            break;
          case 34:
            this.bitField0_ = (0x8 | this.bitField0_);
            this.deviceName_ = paramCodedInputStream.readBytes();
            break;
          case 40:
            this.bitField0_ = (0x10 | this.bitField0_);
            this.deviceVersion_ = paramCodedInputStream.readInt32();
            break;
          case 50:
            this.bitField0_ = (0x20 | this.bitField0_);
            this.clientName_ = paramCodedInputStream.readBytes();
            break;
          case 56:
            this.bitField0_ = (0x40 | this.bitField0_);
            this.clientVersion_ = paramCodedInputStream.readInt32();
            break;
          case 64:
            this.bitField0_ = (0x80 | this.bitField0_);
            this.sendFeedbackImmediately_ = paramCodedInputStream.readBool();
            break;
          case 72:
          }
          this.bitField0_ = (0x100 | this.bitField0_);
          this.feedbackBatchSize_ = paramCodedInputStream.readInt32();
        }
      }

      public Builder mergeFrom(OndeviceSpec.CloudRecognizerSpec paramCloudRecognizerSpec)
      {
        if (paramCloudRecognizerSpec == OndeviceSpec.CloudRecognizerSpec.getDefaultInstance());
        do
        {
          return this;
          if (paramCloudRecognizerSpec.hasServer())
            setServer(paramCloudRecognizerSpec.getServer());
          if (paramCloudRecognizerSpec.hasRecoPath())
            setRecoPath(paramCloudRecognizerSpec.getRecoPath());
          if (paramCloudRecognizerSpec.hasCompressRequests())
            setCompressRequests(paramCloudRecognizerSpec.getCompressRequests());
          if (paramCloudRecognizerSpec.hasDeviceName())
            setDeviceName(paramCloudRecognizerSpec.getDeviceName());
          if (paramCloudRecognizerSpec.hasDeviceVersion())
            setDeviceVersion(paramCloudRecognizerSpec.getDeviceVersion());
          if (paramCloudRecognizerSpec.hasClientName())
            setClientName(paramCloudRecognizerSpec.getClientName());
          if (paramCloudRecognizerSpec.hasClientVersion())
            setClientVersion(paramCloudRecognizerSpec.getClientVersion());
          if (paramCloudRecognizerSpec.hasSendFeedbackImmediately())
            setSendFeedbackImmediately(paramCloudRecognizerSpec.getSendFeedbackImmediately());
        }
        while (!paramCloudRecognizerSpec.hasFeedbackBatchSize());
        setFeedbackBatchSize(paramCloudRecognizerSpec.getFeedbackBatchSize());
        return this;
      }

      public Builder setClientName(String paramString)
      {
        if (paramString == null)
          throw new NullPointerException();
        this.bitField0_ = (0x20 | this.bitField0_);
        this.clientName_ = paramString;
        return this;
      }

      void setClientName(ByteString paramByteString)
      {
        this.bitField0_ = (0x20 | this.bitField0_);
        this.clientName_ = paramByteString;
      }

      public Builder setClientVersion(int paramInt)
      {
        this.bitField0_ = (0x40 | this.bitField0_);
        this.clientVersion_ = paramInt;
        return this;
      }

      public Builder setCompressRequests(boolean paramBoolean)
      {
        this.bitField0_ = (0x4 | this.bitField0_);
        this.compressRequests_ = paramBoolean;
        return this;
      }

      public Builder setDeviceName(String paramString)
      {
        if (paramString == null)
          throw new NullPointerException();
        this.bitField0_ = (0x8 | this.bitField0_);
        this.deviceName_ = paramString;
        return this;
      }

      void setDeviceName(ByteString paramByteString)
      {
        this.bitField0_ = (0x8 | this.bitField0_);
        this.deviceName_ = paramByteString;
      }

      public Builder setDeviceVersion(int paramInt)
      {
        this.bitField0_ = (0x10 | this.bitField0_);
        this.deviceVersion_ = paramInt;
        return this;
      }

      public Builder setFeedbackBatchSize(int paramInt)
      {
        this.bitField0_ = (0x100 | this.bitField0_);
        this.feedbackBatchSize_ = paramInt;
        return this;
      }

      public Builder setRecoPath(String paramString)
      {
        if (paramString == null)
          throw new NullPointerException();
        this.bitField0_ = (0x2 | this.bitField0_);
        this.recoPath_ = paramString;
        return this;
      }

      void setRecoPath(ByteString paramByteString)
      {
        this.bitField0_ = (0x2 | this.bitField0_);
        this.recoPath_ = paramByteString;
      }

      public Builder setSendFeedbackImmediately(boolean paramBoolean)
      {
        this.bitField0_ = (0x80 | this.bitField0_);
        this.sendFeedbackImmediately_ = paramBoolean;
        return this;
      }

      public Builder setServer(String paramString)
      {
        if (paramString == null)
          throw new NullPointerException();
        this.bitField0_ = (0x1 | this.bitField0_);
        this.server_ = paramString;
        return this;
      }

      void setServer(ByteString paramByteString)
      {
        this.bitField0_ = (0x1 | this.bitField0_);
        this.server_ = paramByteString;
      }
    }
  }

  public static abstract interface CloudRecognizerSpecOrBuilder extends MessageLiteOrBuilder
  {
    public abstract String getClientName();

    public abstract int getClientVersion();

    public abstract boolean getCompressRequests();

    public abstract String getDeviceName();

    public abstract int getDeviceVersion();

    public abstract int getFeedbackBatchSize();

    public abstract String getRecoPath();

    public abstract boolean getSendFeedbackImmediately();

    public abstract String getServer();

    public abstract boolean hasClientName();

    public abstract boolean hasClientVersion();

    public abstract boolean hasCompressRequests();

    public abstract boolean hasDeviceName();

    public abstract boolean hasDeviceVersion();

    public abstract boolean hasFeedbackBatchSize();

    public abstract boolean hasRecoPath();

    public abstract boolean hasSendFeedbackImmediately();

    public abstract boolean hasServer();
  }

  public static final class HandwritingRecognizerSpec extends GeneratedMessageLite
    implements OndeviceSpec.HandwritingRecognizerSpecOrBuilder
  {
    public static final int CLOUD_SPEC_FIELD_NUMBER = 8;
    public static final int LANGUAGE_CODE_FIELD_NUMBER = 1;
    public static final int LANGUAGE_NAME_FIELD_NUMBER = 2;
    public static final int MAXIMAL_PARALLEL_REQUESTS_FIELD_NUMBER = 5;
    public static final int NAME_FIELD_NUMBER = 3;
    public static final int SECONDARY_RECOGNIZER_FIELD_NUMBER = 11;
    public static final int SINGLE_CHAR_SPEC_FIELD_NUMBER = 9;
    public static final int TIMEOUT_BEFORE_NEXT_REQUEST_FIELD_NUMBER = 6;
    public static final int USE_SPACES_FIELD_NUMBER = 7;
    public static final int VERBOSITY_FIELD_NUMBER = 4;
    public static final int WORD_SPEC_FIELD_NUMBER = 10;
    private static final HandwritingRecognizerSpec defaultInstance = new HandwritingRecognizerSpec(true);
    private static final long serialVersionUID;
    private int bitField0_;
    private OndeviceSpec.CloudRecognizerSpec cloudSpec_;
    private Object languageCode_;
    private Object languageName_;
    private int maximalParallelRequests_;
    private byte memoizedIsInitialized = -1;
    private int memoizedSerializedSize = -1;
    private Object name_;
    private HandwritingRecognizerSpec secondaryRecognizer_;
    private OndeviceSpec.SingleCharacterRecognizerSpec singleCharSpec_;
    private int timeoutBeforeNextRequest_;
    private boolean useSpaces_;
    private int verbosity_;
    private OndeviceSpec.WordRecognizerSpec wordSpec_;

    static
    {
      defaultInstance.initFields();
    }

    private HandwritingRecognizerSpec(Builder paramBuilder)
    {
      super();
    }

    private HandwritingRecognizerSpec(boolean paramBoolean)
    {
    }

    public static HandwritingRecognizerSpec getDefaultInstance()
    {
      return defaultInstance;
    }

    private ByteString getLanguageCodeBytes()
    {
      Object localObject = this.languageCode_;
      if ((localObject instanceof String))
      {
        ByteString localByteString = ByteString.copyFromUtf8((String)localObject);
        this.languageCode_ = localByteString;
        return localByteString;
      }
      return (ByteString)localObject;
    }

    private ByteString getLanguageNameBytes()
    {
      Object localObject = this.languageName_;
      if ((localObject instanceof String))
      {
        ByteString localByteString = ByteString.copyFromUtf8((String)localObject);
        this.languageName_ = localByteString;
        return localByteString;
      }
      return (ByteString)localObject;
    }

    private ByteString getNameBytes()
    {
      Object localObject = this.name_;
      if ((localObject instanceof String))
      {
        ByteString localByteString = ByteString.copyFromUtf8((String)localObject);
        this.name_ = localByteString;
        return localByteString;
      }
      return (ByteString)localObject;
    }

    private void initFields()
    {
      this.languageCode_ = "";
      this.languageName_ = "";
      this.name_ = "";
      this.verbosity_ = 0;
      this.maximalParallelRequests_ = 0;
      this.timeoutBeforeNextRequest_ = 0;
      this.useSpaces_ = false;
      this.cloudSpec_ = OndeviceSpec.CloudRecognizerSpec.getDefaultInstance();
      this.singleCharSpec_ = OndeviceSpec.SingleCharacterRecognizerSpec.getDefaultInstance();
      this.wordSpec_ = OndeviceSpec.WordRecognizerSpec.getDefaultInstance();
      this.secondaryRecognizer_ = getDefaultInstance();
    }

    public static Builder newBuilder()
    {
      return Builder.access$500();
    }

    public static Builder newBuilder(HandwritingRecognizerSpec paramHandwritingRecognizerSpec)
    {
      return newBuilder().mergeFrom(paramHandwritingRecognizerSpec);
    }

    public static HandwritingRecognizerSpec parseDelimitedFrom(InputStream paramInputStream)
      throws IOException
    {
      Builder localBuilder = newBuilder();
      if (localBuilder.mergeDelimitedFrom(paramInputStream))
        return localBuilder.buildParsed();
      return null;
    }

    public static HandwritingRecognizerSpec parseDelimitedFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      Builder localBuilder = newBuilder();
      if (localBuilder.mergeDelimitedFrom(paramInputStream, paramExtensionRegistryLite))
        return localBuilder.buildParsed();
      return null;
    }

    public static HandwritingRecognizerSpec parseFrom(ByteString paramByteString)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramByteString)).buildParsed();
    }

    public static HandwritingRecognizerSpec parseFrom(ByteString paramByteString, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramByteString, paramExtensionRegistryLite)).buildParsed();
    }

    public static HandwritingRecognizerSpec parseFrom(CodedInputStream paramCodedInputStream)
      throws IOException
    {
      return ((Builder)newBuilder().mergeFrom(paramCodedInputStream)).buildParsed();
    }

    public static HandwritingRecognizerSpec parseFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return newBuilder().mergeFrom(paramCodedInputStream, paramExtensionRegistryLite).buildParsed();
    }

    public static HandwritingRecognizerSpec parseFrom(InputStream paramInputStream)
      throws IOException
    {
      return ((Builder)newBuilder().mergeFrom(paramInputStream)).buildParsed();
    }

    public static HandwritingRecognizerSpec parseFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return ((Builder)newBuilder().mergeFrom(paramInputStream, paramExtensionRegistryLite)).buildParsed();
    }

    public static HandwritingRecognizerSpec parseFrom(byte[] paramArrayOfByte)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramArrayOfByte)).buildParsed();
    }

    public static HandwritingRecognizerSpec parseFrom(byte[] paramArrayOfByte, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramArrayOfByte, paramExtensionRegistryLite)).buildParsed();
    }

    public OndeviceSpec.CloudRecognizerSpec getCloudSpec()
    {
      return this.cloudSpec_;
    }

    public HandwritingRecognizerSpec getDefaultInstanceForType()
    {
      return defaultInstance;
    }

    public String getLanguageCode()
    {
      Object localObject = this.languageCode_;
      if ((localObject instanceof String))
        return (String)localObject;
      ByteString localByteString = (ByteString)localObject;
      String str = localByteString.toStringUtf8();
      if (Internal.isValidUtf8(localByteString))
        this.languageCode_ = str;
      return str;
    }

    public String getLanguageName()
    {
      Object localObject = this.languageName_;
      if ((localObject instanceof String))
        return (String)localObject;
      ByteString localByteString = (ByteString)localObject;
      String str = localByteString.toStringUtf8();
      if (Internal.isValidUtf8(localByteString))
        this.languageName_ = str;
      return str;
    }

    public int getMaximalParallelRequests()
    {
      return this.maximalParallelRequests_;
    }

    public String getName()
    {
      Object localObject = this.name_;
      if ((localObject instanceof String))
        return (String)localObject;
      ByteString localByteString = (ByteString)localObject;
      String str = localByteString.toStringUtf8();
      if (Internal.isValidUtf8(localByteString))
        this.name_ = str;
      return str;
    }

    public HandwritingRecognizerSpec getSecondaryRecognizer()
    {
      return this.secondaryRecognizer_;
    }

    public int getSerializedSize()
    {
      int i = this.memoizedSerializedSize;
      if (i != -1)
        return i;
      int j = 0x1 & this.bitField0_;
      int k = 0;
      if (j == 1)
        k = 0 + CodedOutputStream.computeBytesSize(1, getLanguageCodeBytes());
      if ((0x2 & this.bitField0_) == 2)
        k += CodedOutputStream.computeBytesSize(2, getLanguageNameBytes());
      if ((0x4 & this.bitField0_) == 4)
        k += CodedOutputStream.computeBytesSize(3, getNameBytes());
      if ((0x8 & this.bitField0_) == 8)
        k += CodedOutputStream.computeInt32Size(4, this.verbosity_);
      if ((0x10 & this.bitField0_) == 16)
        k += CodedOutputStream.computeInt32Size(5, this.maximalParallelRequests_);
      if ((0x20 & this.bitField0_) == 32)
        k += CodedOutputStream.computeInt32Size(6, this.timeoutBeforeNextRequest_);
      if ((0x40 & this.bitField0_) == 64)
        k += CodedOutputStream.computeBoolSize(7, this.useSpaces_);
      if ((0x80 & this.bitField0_) == 128)
        k += CodedOutputStream.computeMessageSize(8, this.cloudSpec_);
      if ((0x100 & this.bitField0_) == 256)
        k += CodedOutputStream.computeMessageSize(9, this.singleCharSpec_);
      if ((0x200 & this.bitField0_) == 512)
        k += CodedOutputStream.computeMessageSize(10, this.wordSpec_);
      if ((0x400 & this.bitField0_) == 1024)
        k += CodedOutputStream.computeMessageSize(11, this.secondaryRecognizer_);
      this.memoizedSerializedSize = k;
      return k;
    }

    public OndeviceSpec.SingleCharacterRecognizerSpec getSingleCharSpec()
    {
      return this.singleCharSpec_;
    }

    public int getTimeoutBeforeNextRequest()
    {
      return this.timeoutBeforeNextRequest_;
    }

    public boolean getUseSpaces()
    {
      return this.useSpaces_;
    }

    public int getVerbosity()
    {
      return this.verbosity_;
    }

    public OndeviceSpec.WordRecognizerSpec getWordSpec()
    {
      return this.wordSpec_;
    }

    public boolean hasCloudSpec()
    {
      return (0x80 & this.bitField0_) == 128;
    }

    public boolean hasLanguageCode()
    {
      return (0x1 & this.bitField0_) == 1;
    }

    public boolean hasLanguageName()
    {
      return (0x2 & this.bitField0_) == 2;
    }

    public boolean hasMaximalParallelRequests()
    {
      return (0x10 & this.bitField0_) == 16;
    }

    public boolean hasName()
    {
      return (0x4 & this.bitField0_) == 4;
    }

    public boolean hasSecondaryRecognizer()
    {
      return (0x400 & this.bitField0_) == 1024;
    }

    public boolean hasSingleCharSpec()
    {
      return (0x100 & this.bitField0_) == 256;
    }

    public boolean hasTimeoutBeforeNextRequest()
    {
      return (0x20 & this.bitField0_) == 32;
    }

    public boolean hasUseSpaces()
    {
      return (0x40 & this.bitField0_) == 64;
    }

    public boolean hasVerbosity()
    {
      return (0x8 & this.bitField0_) == 8;
    }

    public boolean hasWordSpec()
    {
      return (0x200 & this.bitField0_) == 512;
    }

    public final boolean isInitialized()
    {
      int i = this.memoizedIsInitialized;
      if (i != -1)
        return i == 1;
      this.memoizedIsInitialized = 1;
      return true;
    }

    public Builder newBuilderForType()
    {
      return newBuilder();
    }

    public Builder toBuilder()
    {
      return newBuilder(this);
    }

    protected Object writeReplace()
      throws ObjectStreamException
    {
      return super.writeReplace();
    }

    public void writeTo(CodedOutputStream paramCodedOutputStream)
      throws IOException
    {
      getSerializedSize();
      if ((0x1 & this.bitField0_) == 1)
        paramCodedOutputStream.writeBytes(1, getLanguageCodeBytes());
      if ((0x2 & this.bitField0_) == 2)
        paramCodedOutputStream.writeBytes(2, getLanguageNameBytes());
      if ((0x4 & this.bitField0_) == 4)
        paramCodedOutputStream.writeBytes(3, getNameBytes());
      if ((0x8 & this.bitField0_) == 8)
        paramCodedOutputStream.writeInt32(4, this.verbosity_);
      if ((0x10 & this.bitField0_) == 16)
        paramCodedOutputStream.writeInt32(5, this.maximalParallelRequests_);
      if ((0x20 & this.bitField0_) == 32)
        paramCodedOutputStream.writeInt32(6, this.timeoutBeforeNextRequest_);
      if ((0x40 & this.bitField0_) == 64)
        paramCodedOutputStream.writeBool(7, this.useSpaces_);
      if ((0x80 & this.bitField0_) == 128)
        paramCodedOutputStream.writeMessage(8, this.cloudSpec_);
      if ((0x100 & this.bitField0_) == 256)
        paramCodedOutputStream.writeMessage(9, this.singleCharSpec_);
      if ((0x200 & this.bitField0_) == 512)
        paramCodedOutputStream.writeMessage(10, this.wordSpec_);
      if ((0x400 & this.bitField0_) == 1024)
        paramCodedOutputStream.writeMessage(11, this.secondaryRecognizer_);
    }

    public static final class Builder extends GeneratedMessageLite.Builder<OndeviceSpec.HandwritingRecognizerSpec, Builder>
      implements OndeviceSpec.HandwritingRecognizerSpecOrBuilder
    {
      private int bitField0_;
      private OndeviceSpec.CloudRecognizerSpec cloudSpec_ = OndeviceSpec.CloudRecognizerSpec.getDefaultInstance();
      private Object languageCode_ = "";
      private Object languageName_ = "";
      private int maximalParallelRequests_;
      private Object name_ = "";
      private OndeviceSpec.HandwritingRecognizerSpec secondaryRecognizer_ = OndeviceSpec.HandwritingRecognizerSpec.getDefaultInstance();
      private OndeviceSpec.SingleCharacterRecognizerSpec singleCharSpec_ = OndeviceSpec.SingleCharacterRecognizerSpec.getDefaultInstance();
      private int timeoutBeforeNextRequest_;
      private boolean useSpaces_;
      private int verbosity_;
      private OndeviceSpec.WordRecognizerSpec wordSpec_ = OndeviceSpec.WordRecognizerSpec.getDefaultInstance();

      private Builder()
      {
        maybeForceBuilderInitialization();
      }

      private OndeviceSpec.HandwritingRecognizerSpec buildParsed()
        throws InvalidProtocolBufferException
      {
        OndeviceSpec.HandwritingRecognizerSpec localHandwritingRecognizerSpec = buildPartial();
        if (!localHandwritingRecognizerSpec.isInitialized())
          throw newUninitializedMessageException(localHandwritingRecognizerSpec).asInvalidProtocolBufferException();
        return localHandwritingRecognizerSpec;
      }

      private static Builder create()
      {
        return new Builder();
      }

      private void maybeForceBuilderInitialization()
      {
      }

      public OndeviceSpec.HandwritingRecognizerSpec build()
      {
        OndeviceSpec.HandwritingRecognizerSpec localHandwritingRecognizerSpec = buildPartial();
        if (!localHandwritingRecognizerSpec.isInitialized())
          throw newUninitializedMessageException(localHandwritingRecognizerSpec);
        return localHandwritingRecognizerSpec;
      }

      public OndeviceSpec.HandwritingRecognizerSpec buildPartial()
      {
        int i = 1;
        OndeviceSpec.HandwritingRecognizerSpec localHandwritingRecognizerSpec = new OndeviceSpec.HandwritingRecognizerSpec(this, null);
        int j = this.bitField0_;
        if ((j & 0x1) == i);
        while (true)
        {
          OndeviceSpec.HandwritingRecognizerSpec.access$702(localHandwritingRecognizerSpec, this.languageCode_);
          if ((j & 0x2) == 2)
            i |= 2;
          OndeviceSpec.HandwritingRecognizerSpec.access$802(localHandwritingRecognizerSpec, this.languageName_);
          if ((j & 0x4) == 4)
            i |= 4;
          OndeviceSpec.HandwritingRecognizerSpec.access$902(localHandwritingRecognizerSpec, this.name_);
          if ((j & 0x8) == 8)
            i |= 8;
          OndeviceSpec.HandwritingRecognizerSpec.access$1002(localHandwritingRecognizerSpec, this.verbosity_);
          if ((j & 0x10) == 16)
            i |= 16;
          OndeviceSpec.HandwritingRecognizerSpec.access$1102(localHandwritingRecognizerSpec, this.maximalParallelRequests_);
          if ((j & 0x20) == 32)
            i |= 32;
          OndeviceSpec.HandwritingRecognizerSpec.access$1202(localHandwritingRecognizerSpec, this.timeoutBeforeNextRequest_);
          if ((j & 0x40) == 64)
            i |= 64;
          OndeviceSpec.HandwritingRecognizerSpec.access$1302(localHandwritingRecognizerSpec, this.useSpaces_);
          if ((j & 0x80) == 128)
            i |= 128;
          OndeviceSpec.HandwritingRecognizerSpec.access$1402(localHandwritingRecognizerSpec, this.cloudSpec_);
          if ((j & 0x100) == 256)
            i |= 256;
          OndeviceSpec.HandwritingRecognizerSpec.access$1502(localHandwritingRecognizerSpec, this.singleCharSpec_);
          if ((j & 0x200) == 512)
            i |= 512;
          OndeviceSpec.HandwritingRecognizerSpec.access$1602(localHandwritingRecognizerSpec, this.wordSpec_);
          if ((j & 0x400) == 1024)
            i |= 1024;
          OndeviceSpec.HandwritingRecognizerSpec.access$1702(localHandwritingRecognizerSpec, this.secondaryRecognizer_);
          OndeviceSpec.HandwritingRecognizerSpec.access$1802(localHandwritingRecognizerSpec, i);
          return localHandwritingRecognizerSpec;
          i = 0;
        }
      }

      public Builder clear()
      {
        super.clear();
        this.languageCode_ = "";
        this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
        this.languageName_ = "";
        this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
        this.name_ = "";
        this.bitField0_ = (0xFFFFFFFB & this.bitField0_);
        this.verbosity_ = 0;
        this.bitField0_ = (0xFFFFFFF7 & this.bitField0_);
        this.maximalParallelRequests_ = 0;
        this.bitField0_ = (0xFFFFFFEF & this.bitField0_);
        this.timeoutBeforeNextRequest_ = 0;
        this.bitField0_ = (0xFFFFFFDF & this.bitField0_);
        this.useSpaces_ = false;
        this.bitField0_ = (0xFFFFFFBF & this.bitField0_);
        this.cloudSpec_ = OndeviceSpec.CloudRecognizerSpec.getDefaultInstance();
        this.bitField0_ = (0xFFFFFF7F & this.bitField0_);
        this.singleCharSpec_ = OndeviceSpec.SingleCharacterRecognizerSpec.getDefaultInstance();
        this.bitField0_ = (0xFFFFFEFF & this.bitField0_);
        this.wordSpec_ = OndeviceSpec.WordRecognizerSpec.getDefaultInstance();
        this.bitField0_ = (0xFFFFFDFF & this.bitField0_);
        this.secondaryRecognizer_ = OndeviceSpec.HandwritingRecognizerSpec.getDefaultInstance();
        this.bitField0_ = (0xFFFFFBFF & this.bitField0_);
        return this;
      }

      public Builder clearCloudSpec()
      {
        this.cloudSpec_ = OndeviceSpec.CloudRecognizerSpec.getDefaultInstance();
        this.bitField0_ = (0xFFFFFF7F & this.bitField0_);
        return this;
      }

      public Builder clearLanguageCode()
      {
        this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
        this.languageCode_ = OndeviceSpec.HandwritingRecognizerSpec.getDefaultInstance().getLanguageCode();
        return this;
      }

      public Builder clearLanguageName()
      {
        this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
        this.languageName_ = OndeviceSpec.HandwritingRecognizerSpec.getDefaultInstance().getLanguageName();
        return this;
      }

      public Builder clearMaximalParallelRequests()
      {
        this.bitField0_ = (0xFFFFFFEF & this.bitField0_);
        this.maximalParallelRequests_ = 0;
        return this;
      }

      public Builder clearName()
      {
        this.bitField0_ = (0xFFFFFFFB & this.bitField0_);
        this.name_ = OndeviceSpec.HandwritingRecognizerSpec.getDefaultInstance().getName();
        return this;
      }

      public Builder clearSecondaryRecognizer()
      {
        this.secondaryRecognizer_ = OndeviceSpec.HandwritingRecognizerSpec.getDefaultInstance();
        this.bitField0_ = (0xFFFFFBFF & this.bitField0_);
        return this;
      }

      public Builder clearSingleCharSpec()
      {
        this.singleCharSpec_ = OndeviceSpec.SingleCharacterRecognizerSpec.getDefaultInstance();
        this.bitField0_ = (0xFFFFFEFF & this.bitField0_);
        return this;
      }

      public Builder clearTimeoutBeforeNextRequest()
      {
        this.bitField0_ = (0xFFFFFFDF & this.bitField0_);
        this.timeoutBeforeNextRequest_ = 0;
        return this;
      }

      public Builder clearUseSpaces()
      {
        this.bitField0_ = (0xFFFFFFBF & this.bitField0_);
        this.useSpaces_ = false;
        return this;
      }

      public Builder clearVerbosity()
      {
        this.bitField0_ = (0xFFFFFFF7 & this.bitField0_);
        this.verbosity_ = 0;
        return this;
      }

      public Builder clearWordSpec()
      {
        this.wordSpec_ = OndeviceSpec.WordRecognizerSpec.getDefaultInstance();
        this.bitField0_ = (0xFFFFFDFF & this.bitField0_);
        return this;
      }

      public Builder clone()
      {
        return create().mergeFrom(buildPartial());
      }

      public OndeviceSpec.CloudRecognizerSpec getCloudSpec()
      {
        return this.cloudSpec_;
      }

      public OndeviceSpec.HandwritingRecognizerSpec getDefaultInstanceForType()
      {
        return OndeviceSpec.HandwritingRecognizerSpec.getDefaultInstance();
      }

      public String getLanguageCode()
      {
        Object localObject = this.languageCode_;
        if (!(localObject instanceof String))
        {
          String str = ((ByteString)localObject).toStringUtf8();
          this.languageCode_ = str;
          return str;
        }
        return (String)localObject;
      }

      public String getLanguageName()
      {
        Object localObject = this.languageName_;
        if (!(localObject instanceof String))
        {
          String str = ((ByteString)localObject).toStringUtf8();
          this.languageName_ = str;
          return str;
        }
        return (String)localObject;
      }

      public int getMaximalParallelRequests()
      {
        return this.maximalParallelRequests_;
      }

      public String getName()
      {
        Object localObject = this.name_;
        if (!(localObject instanceof String))
        {
          String str = ((ByteString)localObject).toStringUtf8();
          this.name_ = str;
          return str;
        }
        return (String)localObject;
      }

      public OndeviceSpec.HandwritingRecognizerSpec getSecondaryRecognizer()
      {
        return this.secondaryRecognizer_;
      }

      public OndeviceSpec.SingleCharacterRecognizerSpec getSingleCharSpec()
      {
        return this.singleCharSpec_;
      }

      public int getTimeoutBeforeNextRequest()
      {
        return this.timeoutBeforeNextRequest_;
      }

      public boolean getUseSpaces()
      {
        return this.useSpaces_;
      }

      public int getVerbosity()
      {
        return this.verbosity_;
      }

      public OndeviceSpec.WordRecognizerSpec getWordSpec()
      {
        return this.wordSpec_;
      }

      public boolean hasCloudSpec()
      {
        return (0x80 & this.bitField0_) == 128;
      }

      public boolean hasLanguageCode()
      {
        return (0x1 & this.bitField0_) == 1;
      }

      public boolean hasLanguageName()
      {
        return (0x2 & this.bitField0_) == 2;
      }

      public boolean hasMaximalParallelRequests()
      {
        return (0x10 & this.bitField0_) == 16;
      }

      public boolean hasName()
      {
        return (0x4 & this.bitField0_) == 4;
      }

      public boolean hasSecondaryRecognizer()
      {
        return (0x400 & this.bitField0_) == 1024;
      }

      public boolean hasSingleCharSpec()
      {
        return (0x100 & this.bitField0_) == 256;
      }

      public boolean hasTimeoutBeforeNextRequest()
      {
        return (0x20 & this.bitField0_) == 32;
      }

      public boolean hasUseSpaces()
      {
        return (0x40 & this.bitField0_) == 64;
      }

      public boolean hasVerbosity()
      {
        return (0x8 & this.bitField0_) == 8;
      }

      public boolean hasWordSpec()
      {
        return (0x200 & this.bitField0_) == 512;
      }

      public final boolean isInitialized()
      {
        return true;
      }

      public Builder mergeCloudSpec(OndeviceSpec.CloudRecognizerSpec paramCloudRecognizerSpec)
      {
        if (((0x80 & this.bitField0_) == 128) && (this.cloudSpec_ != OndeviceSpec.CloudRecognizerSpec.getDefaultInstance()));
        for (this.cloudSpec_ = OndeviceSpec.CloudRecognizerSpec.newBuilder(this.cloudSpec_).mergeFrom(paramCloudRecognizerSpec).buildPartial(); ; this.cloudSpec_ = paramCloudRecognizerSpec)
        {
          this.bitField0_ = (0x80 | this.bitField0_);
          return this;
        }
      }

      public Builder mergeFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
        throws IOException
      {
        while (true)
        {
          int i = paramCodedInputStream.readTag();
          switch (i)
          {
          default:
            if (parseUnknownField(paramCodedInputStream, paramExtensionRegistryLite, i))
              continue;
          case 0:
            return this;
          case 10:
            this.bitField0_ = (0x1 | this.bitField0_);
            this.languageCode_ = paramCodedInputStream.readBytes();
            break;
          case 18:
            this.bitField0_ = (0x2 | this.bitField0_);
            this.languageName_ = paramCodedInputStream.readBytes();
            break;
          case 26:
            this.bitField0_ = (0x4 | this.bitField0_);
            this.name_ = paramCodedInputStream.readBytes();
            break;
          case 32:
            this.bitField0_ = (0x8 | this.bitField0_);
            this.verbosity_ = paramCodedInputStream.readInt32();
            break;
          case 40:
            this.bitField0_ = (0x10 | this.bitField0_);
            this.maximalParallelRequests_ = paramCodedInputStream.readInt32();
            break;
          case 48:
            this.bitField0_ = (0x20 | this.bitField0_);
            this.timeoutBeforeNextRequest_ = paramCodedInputStream.readInt32();
            break;
          case 56:
            this.bitField0_ = (0x40 | this.bitField0_);
            this.useSpaces_ = paramCodedInputStream.readBool();
            break;
          case 66:
            OndeviceSpec.CloudRecognizerSpec.Builder localBuilder3 = OndeviceSpec.CloudRecognizerSpec.newBuilder();
            if (hasCloudSpec())
              localBuilder3.mergeFrom(getCloudSpec());
            paramCodedInputStream.readMessage(localBuilder3, paramExtensionRegistryLite);
            setCloudSpec(localBuilder3.buildPartial());
            break;
          case 74:
            OndeviceSpec.SingleCharacterRecognizerSpec.Builder localBuilder2 = OndeviceSpec.SingleCharacterRecognizerSpec.newBuilder();
            if (hasSingleCharSpec())
              localBuilder2.mergeFrom(getSingleCharSpec());
            paramCodedInputStream.readMessage(localBuilder2, paramExtensionRegistryLite);
            setSingleCharSpec(localBuilder2.buildPartial());
            break;
          case 82:
            OndeviceSpec.WordRecognizerSpec.Builder localBuilder1 = OndeviceSpec.WordRecognizerSpec.newBuilder();
            if (hasWordSpec())
              localBuilder1.mergeFrom(getWordSpec());
            paramCodedInputStream.readMessage(localBuilder1, paramExtensionRegistryLite);
            setWordSpec(localBuilder1.buildPartial());
            break;
          case 90:
          }
          Builder localBuilder = OndeviceSpec.HandwritingRecognizerSpec.newBuilder();
          if (hasSecondaryRecognizer())
            localBuilder.mergeFrom(getSecondaryRecognizer());
          paramCodedInputStream.readMessage(localBuilder, paramExtensionRegistryLite);
          setSecondaryRecognizer(localBuilder.buildPartial());
        }
      }

      public Builder mergeFrom(OndeviceSpec.HandwritingRecognizerSpec paramHandwritingRecognizerSpec)
      {
        if (paramHandwritingRecognizerSpec == OndeviceSpec.HandwritingRecognizerSpec.getDefaultInstance());
        do
        {
          return this;
          if (paramHandwritingRecognizerSpec.hasLanguageCode())
            setLanguageCode(paramHandwritingRecognizerSpec.getLanguageCode());
          if (paramHandwritingRecognizerSpec.hasLanguageName())
            setLanguageName(paramHandwritingRecognizerSpec.getLanguageName());
          if (paramHandwritingRecognizerSpec.hasName())
            setName(paramHandwritingRecognizerSpec.getName());
          if (paramHandwritingRecognizerSpec.hasVerbosity())
            setVerbosity(paramHandwritingRecognizerSpec.getVerbosity());
          if (paramHandwritingRecognizerSpec.hasMaximalParallelRequests())
            setMaximalParallelRequests(paramHandwritingRecognizerSpec.getMaximalParallelRequests());
          if (paramHandwritingRecognizerSpec.hasTimeoutBeforeNextRequest())
            setTimeoutBeforeNextRequest(paramHandwritingRecognizerSpec.getTimeoutBeforeNextRequest());
          if (paramHandwritingRecognizerSpec.hasUseSpaces())
            setUseSpaces(paramHandwritingRecognizerSpec.getUseSpaces());
          if (paramHandwritingRecognizerSpec.hasCloudSpec())
            mergeCloudSpec(paramHandwritingRecognizerSpec.getCloudSpec());
          if (paramHandwritingRecognizerSpec.hasSingleCharSpec())
            mergeSingleCharSpec(paramHandwritingRecognizerSpec.getSingleCharSpec());
          if (paramHandwritingRecognizerSpec.hasWordSpec())
            mergeWordSpec(paramHandwritingRecognizerSpec.getWordSpec());
        }
        while (!paramHandwritingRecognizerSpec.hasSecondaryRecognizer());
        mergeSecondaryRecognizer(paramHandwritingRecognizerSpec.getSecondaryRecognizer());
        return this;
      }

      public Builder mergeSecondaryRecognizer(OndeviceSpec.HandwritingRecognizerSpec paramHandwritingRecognizerSpec)
      {
        if (((0x400 & this.bitField0_) == 1024) && (this.secondaryRecognizer_ != OndeviceSpec.HandwritingRecognizerSpec.getDefaultInstance()));
        for (this.secondaryRecognizer_ = OndeviceSpec.HandwritingRecognizerSpec.newBuilder(this.secondaryRecognizer_).mergeFrom(paramHandwritingRecognizerSpec).buildPartial(); ; this.secondaryRecognizer_ = paramHandwritingRecognizerSpec)
        {
          this.bitField0_ = (0x400 | this.bitField0_);
          return this;
        }
      }

      public Builder mergeSingleCharSpec(OndeviceSpec.SingleCharacterRecognizerSpec paramSingleCharacterRecognizerSpec)
      {
        if (((0x100 & this.bitField0_) == 256) && (this.singleCharSpec_ != OndeviceSpec.SingleCharacterRecognizerSpec.getDefaultInstance()));
        for (this.singleCharSpec_ = OndeviceSpec.SingleCharacterRecognizerSpec.newBuilder(this.singleCharSpec_).mergeFrom(paramSingleCharacterRecognizerSpec).buildPartial(); ; this.singleCharSpec_ = paramSingleCharacterRecognizerSpec)
        {
          this.bitField0_ = (0x100 | this.bitField0_);
          return this;
        }
      }

      public Builder mergeWordSpec(OndeviceSpec.WordRecognizerSpec paramWordRecognizerSpec)
      {
        if (((0x200 & this.bitField0_) == 512) && (this.wordSpec_ != OndeviceSpec.WordRecognizerSpec.getDefaultInstance()));
        for (this.wordSpec_ = OndeviceSpec.WordRecognizerSpec.newBuilder(this.wordSpec_).mergeFrom(paramWordRecognizerSpec).buildPartial(); ; this.wordSpec_ = paramWordRecognizerSpec)
        {
          this.bitField0_ = (0x200 | this.bitField0_);
          return this;
        }
      }

      public Builder setCloudSpec(OndeviceSpec.CloudRecognizerSpec.Builder paramBuilder)
      {
        this.cloudSpec_ = paramBuilder.build();
        this.bitField0_ = (0x80 | this.bitField0_);
        return this;
      }

      public Builder setCloudSpec(OndeviceSpec.CloudRecognizerSpec paramCloudRecognizerSpec)
      {
        if (paramCloudRecognizerSpec == null)
          throw new NullPointerException();
        this.cloudSpec_ = paramCloudRecognizerSpec;
        this.bitField0_ = (0x80 | this.bitField0_);
        return this;
      }

      public Builder setLanguageCode(String paramString)
      {
        if (paramString == null)
          throw new NullPointerException();
        this.bitField0_ = (0x1 | this.bitField0_);
        this.languageCode_ = paramString;
        return this;
      }

      void setLanguageCode(ByteString paramByteString)
      {
        this.bitField0_ = (0x1 | this.bitField0_);
        this.languageCode_ = paramByteString;
      }

      public Builder setLanguageName(String paramString)
      {
        if (paramString == null)
          throw new NullPointerException();
        this.bitField0_ = (0x2 | this.bitField0_);
        this.languageName_ = paramString;
        return this;
      }

      void setLanguageName(ByteString paramByteString)
      {
        this.bitField0_ = (0x2 | this.bitField0_);
        this.languageName_ = paramByteString;
      }

      public Builder setMaximalParallelRequests(int paramInt)
      {
        this.bitField0_ = (0x10 | this.bitField0_);
        this.maximalParallelRequests_ = paramInt;
        return this;
      }

      public Builder setName(String paramString)
      {
        if (paramString == null)
          throw new NullPointerException();
        this.bitField0_ = (0x4 | this.bitField0_);
        this.name_ = paramString;
        return this;
      }

      void setName(ByteString paramByteString)
      {
        this.bitField0_ = (0x4 | this.bitField0_);
        this.name_ = paramByteString;
      }

      public Builder setSecondaryRecognizer(Builder paramBuilder)
      {
        this.secondaryRecognizer_ = paramBuilder.build();
        this.bitField0_ = (0x400 | this.bitField0_);
        return this;
      }

      public Builder setSecondaryRecognizer(OndeviceSpec.HandwritingRecognizerSpec paramHandwritingRecognizerSpec)
      {
        if (paramHandwritingRecognizerSpec == null)
          throw new NullPointerException();
        this.secondaryRecognizer_ = paramHandwritingRecognizerSpec;
        this.bitField0_ = (0x400 | this.bitField0_);
        return this;
      }

      public Builder setSingleCharSpec(OndeviceSpec.SingleCharacterRecognizerSpec.Builder paramBuilder)
      {
        this.singleCharSpec_ = paramBuilder.build();
        this.bitField0_ = (0x100 | this.bitField0_);
        return this;
      }

      public Builder setSingleCharSpec(OndeviceSpec.SingleCharacterRecognizerSpec paramSingleCharacterRecognizerSpec)
      {
        if (paramSingleCharacterRecognizerSpec == null)
          throw new NullPointerException();
        this.singleCharSpec_ = paramSingleCharacterRecognizerSpec;
        this.bitField0_ = (0x100 | this.bitField0_);
        return this;
      }

      public Builder setTimeoutBeforeNextRequest(int paramInt)
      {
        this.bitField0_ = (0x20 | this.bitField0_);
        this.timeoutBeforeNextRequest_ = paramInt;
        return this;
      }

      public Builder setUseSpaces(boolean paramBoolean)
      {
        this.bitField0_ = (0x40 | this.bitField0_);
        this.useSpaces_ = paramBoolean;
        return this;
      }

      public Builder setVerbosity(int paramInt)
      {
        this.bitField0_ = (0x8 | this.bitField0_);
        this.verbosity_ = paramInt;
        return this;
      }

      public Builder setWordSpec(OndeviceSpec.WordRecognizerSpec.Builder paramBuilder)
      {
        this.wordSpec_ = paramBuilder.build();
        this.bitField0_ = (0x200 | this.bitField0_);
        return this;
      }

      public Builder setWordSpec(OndeviceSpec.WordRecognizerSpec paramWordRecognizerSpec)
      {
        if (paramWordRecognizerSpec == null)
          throw new NullPointerException();
        this.wordSpec_ = paramWordRecognizerSpec;
        this.bitField0_ = (0x200 | this.bitField0_);
        return this;
      }
    }
  }

  public static abstract interface HandwritingRecognizerSpecOrBuilder extends MessageLiteOrBuilder
  {
    public abstract OndeviceSpec.CloudRecognizerSpec getCloudSpec();

    public abstract String getLanguageCode();

    public abstract String getLanguageName();

    public abstract int getMaximalParallelRequests();

    public abstract String getName();

    public abstract OndeviceSpec.HandwritingRecognizerSpec getSecondaryRecognizer();

    public abstract OndeviceSpec.SingleCharacterRecognizerSpec getSingleCharSpec();

    public abstract int getTimeoutBeforeNextRequest();

    public abstract boolean getUseSpaces();

    public abstract int getVerbosity();

    public abstract OndeviceSpec.WordRecognizerSpec getWordSpec();

    public abstract boolean hasCloudSpec();

    public abstract boolean hasLanguageCode();

    public abstract boolean hasLanguageName();

    public abstract boolean hasMaximalParallelRequests();

    public abstract boolean hasName();

    public abstract boolean hasSecondaryRecognizer();

    public abstract boolean hasSingleCharSpec();

    public abstract boolean hasTimeoutBeforeNextRequest();

    public abstract boolean hasUseSpaces();

    public abstract boolean hasVerbosity();

    public abstract boolean hasWordSpec();
  }

  public static final class HandwritingRecognizerSpecs extends GeneratedMessageLite
    implements OndeviceSpec.HandwritingRecognizerSpecsOrBuilder
  {
    public static final int SPEC_FIELD_NUMBER = 1;
    private static final HandwritingRecognizerSpecs defaultInstance = new HandwritingRecognizerSpecs(true);
    private static final long serialVersionUID;
    private byte memoizedIsInitialized = -1;
    private int memoizedSerializedSize = -1;
    private List<OndeviceSpec.HandwritingRecognizerSpec> spec_;

    static
    {
      defaultInstance.initFields();
    }

    private HandwritingRecognizerSpecs(Builder paramBuilder)
    {
      super();
    }

    private HandwritingRecognizerSpecs(boolean paramBoolean)
    {
    }

    public static HandwritingRecognizerSpecs getDefaultInstance()
    {
      return defaultInstance;
    }

    private void initFields()
    {
      this.spec_ = Collections.emptyList();
    }

    public static Builder newBuilder()
    {
      return Builder.access$100();
    }

    public static Builder newBuilder(HandwritingRecognizerSpecs paramHandwritingRecognizerSpecs)
    {
      return newBuilder().mergeFrom(paramHandwritingRecognizerSpecs);
    }

    public static HandwritingRecognizerSpecs parseDelimitedFrom(InputStream paramInputStream)
      throws IOException
    {
      Builder localBuilder = newBuilder();
      if (localBuilder.mergeDelimitedFrom(paramInputStream))
        return localBuilder.buildParsed();
      return null;
    }

    public static HandwritingRecognizerSpecs parseDelimitedFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      Builder localBuilder = newBuilder();
      if (localBuilder.mergeDelimitedFrom(paramInputStream, paramExtensionRegistryLite))
        return localBuilder.buildParsed();
      return null;
    }

    public static HandwritingRecognizerSpecs parseFrom(ByteString paramByteString)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramByteString)).buildParsed();
    }

    public static HandwritingRecognizerSpecs parseFrom(ByteString paramByteString, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramByteString, paramExtensionRegistryLite)).buildParsed();
    }

    public static HandwritingRecognizerSpecs parseFrom(CodedInputStream paramCodedInputStream)
      throws IOException
    {
      return ((Builder)newBuilder().mergeFrom(paramCodedInputStream)).buildParsed();
    }

    public static HandwritingRecognizerSpecs parseFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return newBuilder().mergeFrom(paramCodedInputStream, paramExtensionRegistryLite).buildParsed();
    }

    public static HandwritingRecognizerSpecs parseFrom(InputStream paramInputStream)
      throws IOException
    {
      return ((Builder)newBuilder().mergeFrom(paramInputStream)).buildParsed();
    }

    public static HandwritingRecognizerSpecs parseFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return ((Builder)newBuilder().mergeFrom(paramInputStream, paramExtensionRegistryLite)).buildParsed();
    }

    public static HandwritingRecognizerSpecs parseFrom(byte[] paramArrayOfByte)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramArrayOfByte)).buildParsed();
    }

    public static HandwritingRecognizerSpecs parseFrom(byte[] paramArrayOfByte, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramArrayOfByte, paramExtensionRegistryLite)).buildParsed();
    }

    public HandwritingRecognizerSpecs getDefaultInstanceForType()
    {
      return defaultInstance;
    }

    public int getSerializedSize()
    {
      int i = this.memoizedSerializedSize;
      if (i != -1)
        return i;
      int j = 0;
      int k = 0;
      while (j < this.spec_.size())
      {
        k += CodedOutputStream.computeMessageSize(1, (MessageLite)this.spec_.get(j));
        j++;
      }
      this.memoizedSerializedSize = k;
      return k;
    }

    public OndeviceSpec.HandwritingRecognizerSpec getSpec(int paramInt)
    {
      return (OndeviceSpec.HandwritingRecognizerSpec)this.spec_.get(paramInt);
    }

    public int getSpecCount()
    {
      return this.spec_.size();
    }

    public List<OndeviceSpec.HandwritingRecognizerSpec> getSpecList()
    {
      return this.spec_;
    }

    public OndeviceSpec.HandwritingRecognizerSpecOrBuilder getSpecOrBuilder(int paramInt)
    {
      return (OndeviceSpec.HandwritingRecognizerSpecOrBuilder)this.spec_.get(paramInt);
    }

    public List<? extends OndeviceSpec.HandwritingRecognizerSpecOrBuilder> getSpecOrBuilderList()
    {
      return this.spec_;
    }

    public final boolean isInitialized()
    {
      int i = this.memoizedIsInitialized;
      if (i != -1)
        return i == 1;
      this.memoizedIsInitialized = 1;
      return true;
    }

    public Builder newBuilderForType()
    {
      return newBuilder();
    }

    public Builder toBuilder()
    {
      return newBuilder(this);
    }

    protected Object writeReplace()
      throws ObjectStreamException
    {
      return super.writeReplace();
    }

    public void writeTo(CodedOutputStream paramCodedOutputStream)
      throws IOException
    {
      getSerializedSize();
      for (int i = 0; i < this.spec_.size(); i++)
        paramCodedOutputStream.writeMessage(1, (MessageLite)this.spec_.get(i));
    }

    public static final class Builder extends GeneratedMessageLite.Builder<OndeviceSpec.HandwritingRecognizerSpecs, Builder>
      implements OndeviceSpec.HandwritingRecognizerSpecsOrBuilder
    {
      private int bitField0_;
      private List<OndeviceSpec.HandwritingRecognizerSpec> spec_ = Collections.emptyList();

      private Builder()
      {
        maybeForceBuilderInitialization();
      }

      private OndeviceSpec.HandwritingRecognizerSpecs buildParsed()
        throws InvalidProtocolBufferException
      {
        OndeviceSpec.HandwritingRecognizerSpecs localHandwritingRecognizerSpecs = buildPartial();
        if (!localHandwritingRecognizerSpecs.isInitialized())
          throw newUninitializedMessageException(localHandwritingRecognizerSpecs).asInvalidProtocolBufferException();
        return localHandwritingRecognizerSpecs;
      }

      private static Builder create()
      {
        return new Builder();
      }

      private void ensureSpecIsMutable()
      {
        if ((0x1 & this.bitField0_) != 1)
        {
          this.spec_ = new ArrayList(this.spec_);
          this.bitField0_ = (0x1 | this.bitField0_);
        }
      }

      private void maybeForceBuilderInitialization()
      {
      }

      public Builder addAllSpec(Iterable<? extends OndeviceSpec.HandwritingRecognizerSpec> paramIterable)
      {
        ensureSpecIsMutable();
        GeneratedMessageLite.Builder.addAll(paramIterable, this.spec_);
        return this;
      }

      public Builder addSpec(int paramInt, OndeviceSpec.HandwritingRecognizerSpec.Builder paramBuilder)
      {
        ensureSpecIsMutable();
        this.spec_.add(paramInt, paramBuilder.build());
        return this;
      }

      public Builder addSpec(int paramInt, OndeviceSpec.HandwritingRecognizerSpec paramHandwritingRecognizerSpec)
      {
        if (paramHandwritingRecognizerSpec == null)
          throw new NullPointerException();
        ensureSpecIsMutable();
        this.spec_.add(paramInt, paramHandwritingRecognizerSpec);
        return this;
      }

      public Builder addSpec(OndeviceSpec.HandwritingRecognizerSpec.Builder paramBuilder)
      {
        ensureSpecIsMutable();
        this.spec_.add(paramBuilder.build());
        return this;
      }

      public Builder addSpec(OndeviceSpec.HandwritingRecognizerSpec paramHandwritingRecognizerSpec)
      {
        if (paramHandwritingRecognizerSpec == null)
          throw new NullPointerException();
        ensureSpecIsMutable();
        this.spec_.add(paramHandwritingRecognizerSpec);
        return this;
      }

      public OndeviceSpec.HandwritingRecognizerSpecs build()
      {
        OndeviceSpec.HandwritingRecognizerSpecs localHandwritingRecognizerSpecs = buildPartial();
        if (!localHandwritingRecognizerSpecs.isInitialized())
          throw newUninitializedMessageException(localHandwritingRecognizerSpecs);
        return localHandwritingRecognizerSpecs;
      }

      public OndeviceSpec.HandwritingRecognizerSpecs buildPartial()
      {
        OndeviceSpec.HandwritingRecognizerSpecs localHandwritingRecognizerSpecs = new OndeviceSpec.HandwritingRecognizerSpecs(this, null);
        if ((0x1 & this.bitField0_) == 1)
        {
          this.spec_ = Collections.unmodifiableList(this.spec_);
          this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
        }
        OndeviceSpec.HandwritingRecognizerSpecs.access$302(localHandwritingRecognizerSpecs, this.spec_);
        return localHandwritingRecognizerSpecs;
      }

      public Builder clear()
      {
        super.clear();
        this.spec_ = Collections.emptyList();
        this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
        return this;
      }

      public Builder clearSpec()
      {
        this.spec_ = Collections.emptyList();
        this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
        return this;
      }

      public Builder clone()
      {
        return create().mergeFrom(buildPartial());
      }

      public OndeviceSpec.HandwritingRecognizerSpecs getDefaultInstanceForType()
      {
        return OndeviceSpec.HandwritingRecognizerSpecs.getDefaultInstance();
      }

      public OndeviceSpec.HandwritingRecognizerSpec getSpec(int paramInt)
      {
        return (OndeviceSpec.HandwritingRecognizerSpec)this.spec_.get(paramInt);
      }

      public int getSpecCount()
      {
        return this.spec_.size();
      }

      public List<OndeviceSpec.HandwritingRecognizerSpec> getSpecList()
      {
        return Collections.unmodifiableList(this.spec_);
      }

      public final boolean isInitialized()
      {
        return true;
      }

      public Builder mergeFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
        throws IOException
      {
        while (true)
        {
          int i = paramCodedInputStream.readTag();
          switch (i)
          {
          default:
            if (parseUnknownField(paramCodedInputStream, paramExtensionRegistryLite, i))
              continue;
          case 0:
            return this;
          case 10:
          }
          OndeviceSpec.HandwritingRecognizerSpec.Builder localBuilder = OndeviceSpec.HandwritingRecognizerSpec.newBuilder();
          paramCodedInputStream.readMessage(localBuilder, paramExtensionRegistryLite);
          addSpec(localBuilder.buildPartial());
        }
      }

      public Builder mergeFrom(OndeviceSpec.HandwritingRecognizerSpecs paramHandwritingRecognizerSpecs)
      {
        if (paramHandwritingRecognizerSpecs == OndeviceSpec.HandwritingRecognizerSpecs.getDefaultInstance());
        while (paramHandwritingRecognizerSpecs.spec_.isEmpty())
          return this;
        if (this.spec_.isEmpty())
        {
          this.spec_ = paramHandwritingRecognizerSpecs.spec_;
          this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
          return this;
        }
        ensureSpecIsMutable();
        this.spec_.addAll(paramHandwritingRecognizerSpecs.spec_);
        return this;
      }

      public Builder removeSpec(int paramInt)
      {
        ensureSpecIsMutable();
        this.spec_.remove(paramInt);
        return this;
      }

      public Builder setSpec(int paramInt, OndeviceSpec.HandwritingRecognizerSpec.Builder paramBuilder)
      {
        ensureSpecIsMutable();
        this.spec_.set(paramInt, paramBuilder.build());
        return this;
      }

      public Builder setSpec(int paramInt, OndeviceSpec.HandwritingRecognizerSpec paramHandwritingRecognizerSpec)
      {
        if (paramHandwritingRecognizerSpec == null)
          throw new NullPointerException();
        ensureSpecIsMutable();
        this.spec_.set(paramInt, paramHandwritingRecognizerSpec);
        return this;
      }
    }
  }

  public static abstract interface HandwritingRecognizerSpecsOrBuilder extends MessageLiteOrBuilder
  {
    public abstract OndeviceSpec.HandwritingRecognizerSpec getSpec(int paramInt);

    public abstract int getSpecCount();

    public abstract List<OndeviceSpec.HandwritingRecognizerSpec> getSpecList();
  }

  public static final class SingleCharacterRecognizerSpec extends GeneratedMessageLite
    implements OndeviceSpec.SingleCharacterRecognizerSpecOrBuilder
  {
    public static final int CHAR_RESCORING_FILE_FIELD_NUMBER = 3;
    public static final int CLASSIFIER_TYPE_FIELD_NUMBER = 4;
    public static final int INKREADER_FIELD_NUMBER = 1;
    public static final int MODEL_FIELD_NUMBER = 2;
    public static final int NUM_RESULTS_FIELD_NUMBER = 5;
    private static final SingleCharacterRecognizerSpec defaultInstance = new SingleCharacterRecognizerSpec(true);
    private static final long serialVersionUID;
    private int bitField0_;
    private Object charRescoringFile_;
    private Object classifierType_;
    private Object inkreader_;
    private byte memoizedIsInitialized = -1;
    private int memoizedSerializedSize = -1;
    private Object model_;
    private int numResults_;

    static
    {
      defaultInstance.initFields();
    }

    private SingleCharacterRecognizerSpec(Builder paramBuilder)
    {
      super();
    }

    private SingleCharacterRecognizerSpec(boolean paramBoolean)
    {
    }

    private ByteString getCharRescoringFileBytes()
    {
      Object localObject = this.charRescoringFile_;
      if ((localObject instanceof String))
      {
        ByteString localByteString = ByteString.copyFromUtf8((String)localObject);
        this.charRescoringFile_ = localByteString;
        return localByteString;
      }
      return (ByteString)localObject;
    }

    private ByteString getClassifierTypeBytes()
    {
      Object localObject = this.classifierType_;
      if ((localObject instanceof String))
      {
        ByteString localByteString = ByteString.copyFromUtf8((String)localObject);
        this.classifierType_ = localByteString;
        return localByteString;
      }
      return (ByteString)localObject;
    }

    public static SingleCharacterRecognizerSpec getDefaultInstance()
    {
      return defaultInstance;
    }

    private ByteString getInkreaderBytes()
    {
      Object localObject = this.inkreader_;
      if ((localObject instanceof String))
      {
        ByteString localByteString = ByteString.copyFromUtf8((String)localObject);
        this.inkreader_ = localByteString;
        return localByteString;
      }
      return (ByteString)localObject;
    }

    private ByteString getModelBytes()
    {
      Object localObject = this.model_;
      if ((localObject instanceof String))
      {
        ByteString localByteString = ByteString.copyFromUtf8((String)localObject);
        this.model_ = localByteString;
        return localByteString;
      }
      return (ByteString)localObject;
    }

    private void initFields()
    {
      this.inkreader_ = "";
      this.model_ = "";
      this.charRescoringFile_ = "";
      this.classifierType_ = "";
      this.numResults_ = 0;
    }

    public static Builder newBuilder()
    {
      return Builder.access$3300();
    }

    public static Builder newBuilder(SingleCharacterRecognizerSpec paramSingleCharacterRecognizerSpec)
    {
      return newBuilder().mergeFrom(paramSingleCharacterRecognizerSpec);
    }

    public static SingleCharacterRecognizerSpec parseDelimitedFrom(InputStream paramInputStream)
      throws IOException
    {
      Builder localBuilder = newBuilder();
      if (localBuilder.mergeDelimitedFrom(paramInputStream))
        return localBuilder.buildParsed();
      return null;
    }

    public static SingleCharacterRecognizerSpec parseDelimitedFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      Builder localBuilder = newBuilder();
      if (localBuilder.mergeDelimitedFrom(paramInputStream, paramExtensionRegistryLite))
        return localBuilder.buildParsed();
      return null;
    }

    public static SingleCharacterRecognizerSpec parseFrom(ByteString paramByteString)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramByteString)).buildParsed();
    }

    public static SingleCharacterRecognizerSpec parseFrom(ByteString paramByteString, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramByteString, paramExtensionRegistryLite)).buildParsed();
    }

    public static SingleCharacterRecognizerSpec parseFrom(CodedInputStream paramCodedInputStream)
      throws IOException
    {
      return ((Builder)newBuilder().mergeFrom(paramCodedInputStream)).buildParsed();
    }

    public static SingleCharacterRecognizerSpec parseFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return newBuilder().mergeFrom(paramCodedInputStream, paramExtensionRegistryLite).buildParsed();
    }

    public static SingleCharacterRecognizerSpec parseFrom(InputStream paramInputStream)
      throws IOException
    {
      return ((Builder)newBuilder().mergeFrom(paramInputStream)).buildParsed();
    }

    public static SingleCharacterRecognizerSpec parseFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return ((Builder)newBuilder().mergeFrom(paramInputStream, paramExtensionRegistryLite)).buildParsed();
    }

    public static SingleCharacterRecognizerSpec parseFrom(byte[] paramArrayOfByte)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramArrayOfByte)).buildParsed();
    }

    public static SingleCharacterRecognizerSpec parseFrom(byte[] paramArrayOfByte, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramArrayOfByte, paramExtensionRegistryLite)).buildParsed();
    }

    public String getCharRescoringFile()
    {
      Object localObject = this.charRescoringFile_;
      if ((localObject instanceof String))
        return (String)localObject;
      ByteString localByteString = (ByteString)localObject;
      String str = localByteString.toStringUtf8();
      if (Internal.isValidUtf8(localByteString))
        this.charRescoringFile_ = str;
      return str;
    }

    public String getClassifierType()
    {
      Object localObject = this.classifierType_;
      if ((localObject instanceof String))
        return (String)localObject;
      ByteString localByteString = (ByteString)localObject;
      String str = localByteString.toStringUtf8();
      if (Internal.isValidUtf8(localByteString))
        this.classifierType_ = str;
      return str;
    }

    public SingleCharacterRecognizerSpec getDefaultInstanceForType()
    {
      return defaultInstance;
    }

    public String getInkreader()
    {
      Object localObject = this.inkreader_;
      if ((localObject instanceof String))
        return (String)localObject;
      ByteString localByteString = (ByteString)localObject;
      String str = localByteString.toStringUtf8();
      if (Internal.isValidUtf8(localByteString))
        this.inkreader_ = str;
      return str;
    }

    public String getModel()
    {
      Object localObject = this.model_;
      if ((localObject instanceof String))
        return (String)localObject;
      ByteString localByteString = (ByteString)localObject;
      String str = localByteString.toStringUtf8();
      if (Internal.isValidUtf8(localByteString))
        this.model_ = str;
      return str;
    }

    public int getNumResults()
    {
      return this.numResults_;
    }

    public int getSerializedSize()
    {
      int i = this.memoizedSerializedSize;
      if (i != -1)
        return i;
      int j = 0x1 & this.bitField0_;
      int k = 0;
      if (j == 1)
        k = 0 + CodedOutputStream.computeBytesSize(1, getInkreaderBytes());
      if ((0x2 & this.bitField0_) == 2)
        k += CodedOutputStream.computeBytesSize(2, getModelBytes());
      if ((0x4 & this.bitField0_) == 4)
        k += CodedOutputStream.computeBytesSize(3, getCharRescoringFileBytes());
      if ((0x8 & this.bitField0_) == 8)
        k += CodedOutputStream.computeBytesSize(4, getClassifierTypeBytes());
      if ((0x10 & this.bitField0_) == 16)
        k += CodedOutputStream.computeInt32Size(5, this.numResults_);
      this.memoizedSerializedSize = k;
      return k;
    }

    public boolean hasCharRescoringFile()
    {
      return (0x4 & this.bitField0_) == 4;
    }

    public boolean hasClassifierType()
    {
      return (0x8 & this.bitField0_) == 8;
    }

    public boolean hasInkreader()
    {
      return (0x1 & this.bitField0_) == 1;
    }

    public boolean hasModel()
    {
      return (0x2 & this.bitField0_) == 2;
    }

    public boolean hasNumResults()
    {
      return (0x10 & this.bitField0_) == 16;
    }

    public final boolean isInitialized()
    {
      int i = this.memoizedIsInitialized;
      if (i != -1)
        return i == 1;
      this.memoizedIsInitialized = 1;
      return true;
    }

    public Builder newBuilderForType()
    {
      return newBuilder();
    }

    public Builder toBuilder()
    {
      return newBuilder(this);
    }

    protected Object writeReplace()
      throws ObjectStreamException
    {
      return super.writeReplace();
    }

    public void writeTo(CodedOutputStream paramCodedOutputStream)
      throws IOException
    {
      getSerializedSize();
      if ((0x1 & this.bitField0_) == 1)
        paramCodedOutputStream.writeBytes(1, getInkreaderBytes());
      if ((0x2 & this.bitField0_) == 2)
        paramCodedOutputStream.writeBytes(2, getModelBytes());
      if ((0x4 & this.bitField0_) == 4)
        paramCodedOutputStream.writeBytes(3, getCharRescoringFileBytes());
      if ((0x8 & this.bitField0_) == 8)
        paramCodedOutputStream.writeBytes(4, getClassifierTypeBytes());
      if ((0x10 & this.bitField0_) == 16)
        paramCodedOutputStream.writeInt32(5, this.numResults_);
    }

    public static final class Builder extends GeneratedMessageLite.Builder<OndeviceSpec.SingleCharacterRecognizerSpec, Builder>
      implements OndeviceSpec.SingleCharacterRecognizerSpecOrBuilder
    {
      private int bitField0_;
      private Object charRescoringFile_ = "";
      private Object classifierType_ = "";
      private Object inkreader_ = "";
      private Object model_ = "";
      private int numResults_;

      private Builder()
      {
        maybeForceBuilderInitialization();
      }

      private OndeviceSpec.SingleCharacterRecognizerSpec buildParsed()
        throws InvalidProtocolBufferException
      {
        OndeviceSpec.SingleCharacterRecognizerSpec localSingleCharacterRecognizerSpec = buildPartial();
        if (!localSingleCharacterRecognizerSpec.isInitialized())
          throw newUninitializedMessageException(localSingleCharacterRecognizerSpec).asInvalidProtocolBufferException();
        return localSingleCharacterRecognizerSpec;
      }

      private static Builder create()
      {
        return new Builder();
      }

      private void maybeForceBuilderInitialization()
      {
      }

      public OndeviceSpec.SingleCharacterRecognizerSpec build()
      {
        OndeviceSpec.SingleCharacterRecognizerSpec localSingleCharacterRecognizerSpec = buildPartial();
        if (!localSingleCharacterRecognizerSpec.isInitialized())
          throw newUninitializedMessageException(localSingleCharacterRecognizerSpec);
        return localSingleCharacterRecognizerSpec;
      }

      public OndeviceSpec.SingleCharacterRecognizerSpec buildPartial()
      {
        int i = 1;
        OndeviceSpec.SingleCharacterRecognizerSpec localSingleCharacterRecognizerSpec = new OndeviceSpec.SingleCharacterRecognizerSpec(this, null);
        int j = this.bitField0_;
        if ((j & 0x1) == i);
        while (true)
        {
          OndeviceSpec.SingleCharacterRecognizerSpec.access$3502(localSingleCharacterRecognizerSpec, this.inkreader_);
          if ((j & 0x2) == 2)
            i |= 2;
          OndeviceSpec.SingleCharacterRecognizerSpec.access$3602(localSingleCharacterRecognizerSpec, this.model_);
          if ((j & 0x4) == 4)
            i |= 4;
          OndeviceSpec.SingleCharacterRecognizerSpec.access$3702(localSingleCharacterRecognizerSpec, this.charRescoringFile_);
          if ((j & 0x8) == 8)
            i |= 8;
          OndeviceSpec.SingleCharacterRecognizerSpec.access$3802(localSingleCharacterRecognizerSpec, this.classifierType_);
          if ((j & 0x10) == 16)
            i |= 16;
          OndeviceSpec.SingleCharacterRecognizerSpec.access$3902(localSingleCharacterRecognizerSpec, this.numResults_);
          OndeviceSpec.SingleCharacterRecognizerSpec.access$4002(localSingleCharacterRecognizerSpec, i);
          return localSingleCharacterRecognizerSpec;
          i = 0;
        }
      }

      public Builder clear()
      {
        super.clear();
        this.inkreader_ = "";
        this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
        this.model_ = "";
        this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
        this.charRescoringFile_ = "";
        this.bitField0_ = (0xFFFFFFFB & this.bitField0_);
        this.classifierType_ = "";
        this.bitField0_ = (0xFFFFFFF7 & this.bitField0_);
        this.numResults_ = 0;
        this.bitField0_ = (0xFFFFFFEF & this.bitField0_);
        return this;
      }

      public Builder clearCharRescoringFile()
      {
        this.bitField0_ = (0xFFFFFFFB & this.bitField0_);
        this.charRescoringFile_ = OndeviceSpec.SingleCharacterRecognizerSpec.getDefaultInstance().getCharRescoringFile();
        return this;
      }

      public Builder clearClassifierType()
      {
        this.bitField0_ = (0xFFFFFFF7 & this.bitField0_);
        this.classifierType_ = OndeviceSpec.SingleCharacterRecognizerSpec.getDefaultInstance().getClassifierType();
        return this;
      }

      public Builder clearInkreader()
      {
        this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
        this.inkreader_ = OndeviceSpec.SingleCharacterRecognizerSpec.getDefaultInstance().getInkreader();
        return this;
      }

      public Builder clearModel()
      {
        this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
        this.model_ = OndeviceSpec.SingleCharacterRecognizerSpec.getDefaultInstance().getModel();
        return this;
      }

      public Builder clearNumResults()
      {
        this.bitField0_ = (0xFFFFFFEF & this.bitField0_);
        this.numResults_ = 0;
        return this;
      }

      public Builder clone()
      {
        return create().mergeFrom(buildPartial());
      }

      public String getCharRescoringFile()
      {
        Object localObject = this.charRescoringFile_;
        if (!(localObject instanceof String))
        {
          String str = ((ByteString)localObject).toStringUtf8();
          this.charRescoringFile_ = str;
          return str;
        }
        return (String)localObject;
      }

      public String getClassifierType()
      {
        Object localObject = this.classifierType_;
        if (!(localObject instanceof String))
        {
          String str = ((ByteString)localObject).toStringUtf8();
          this.classifierType_ = str;
          return str;
        }
        return (String)localObject;
      }

      public OndeviceSpec.SingleCharacterRecognizerSpec getDefaultInstanceForType()
      {
        return OndeviceSpec.SingleCharacterRecognizerSpec.getDefaultInstance();
      }

      public String getInkreader()
      {
        Object localObject = this.inkreader_;
        if (!(localObject instanceof String))
        {
          String str = ((ByteString)localObject).toStringUtf8();
          this.inkreader_ = str;
          return str;
        }
        return (String)localObject;
      }

      public String getModel()
      {
        Object localObject = this.model_;
        if (!(localObject instanceof String))
        {
          String str = ((ByteString)localObject).toStringUtf8();
          this.model_ = str;
          return str;
        }
        return (String)localObject;
      }

      public int getNumResults()
      {
        return this.numResults_;
      }

      public boolean hasCharRescoringFile()
      {
        return (0x4 & this.bitField0_) == 4;
      }

      public boolean hasClassifierType()
      {
        return (0x8 & this.bitField0_) == 8;
      }

      public boolean hasInkreader()
      {
        return (0x1 & this.bitField0_) == 1;
      }

      public boolean hasModel()
      {
        return (0x2 & this.bitField0_) == 2;
      }

      public boolean hasNumResults()
      {
        return (0x10 & this.bitField0_) == 16;
      }

      public final boolean isInitialized()
      {
        return true;
      }

      public Builder mergeFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
        throws IOException
      {
        while (true)
        {
          int i = paramCodedInputStream.readTag();
          switch (i)
          {
          default:
            if (parseUnknownField(paramCodedInputStream, paramExtensionRegistryLite, i))
              continue;
          case 0:
            return this;
          case 10:
            this.bitField0_ = (0x1 | this.bitField0_);
            this.inkreader_ = paramCodedInputStream.readBytes();
            break;
          case 18:
            this.bitField0_ = (0x2 | this.bitField0_);
            this.model_ = paramCodedInputStream.readBytes();
            break;
          case 26:
            this.bitField0_ = (0x4 | this.bitField0_);
            this.charRescoringFile_ = paramCodedInputStream.readBytes();
            break;
          case 34:
            this.bitField0_ = (0x8 | this.bitField0_);
            this.classifierType_ = paramCodedInputStream.readBytes();
            break;
          case 40:
          }
          this.bitField0_ = (0x10 | this.bitField0_);
          this.numResults_ = paramCodedInputStream.readInt32();
        }
      }

      public Builder mergeFrom(OndeviceSpec.SingleCharacterRecognizerSpec paramSingleCharacterRecognizerSpec)
      {
        if (paramSingleCharacterRecognizerSpec == OndeviceSpec.SingleCharacterRecognizerSpec.getDefaultInstance());
        do
        {
          return this;
          if (paramSingleCharacterRecognizerSpec.hasInkreader())
            setInkreader(paramSingleCharacterRecognizerSpec.getInkreader());
          if (paramSingleCharacterRecognizerSpec.hasModel())
            setModel(paramSingleCharacterRecognizerSpec.getModel());
          if (paramSingleCharacterRecognizerSpec.hasCharRescoringFile())
            setCharRescoringFile(paramSingleCharacterRecognizerSpec.getCharRescoringFile());
          if (paramSingleCharacterRecognizerSpec.hasClassifierType())
            setClassifierType(paramSingleCharacterRecognizerSpec.getClassifierType());
        }
        while (!paramSingleCharacterRecognizerSpec.hasNumResults());
        setNumResults(paramSingleCharacterRecognizerSpec.getNumResults());
        return this;
      }

      public Builder setCharRescoringFile(String paramString)
      {
        if (paramString == null)
          throw new NullPointerException();
        this.bitField0_ = (0x4 | this.bitField0_);
        this.charRescoringFile_ = paramString;
        return this;
      }

      void setCharRescoringFile(ByteString paramByteString)
      {
        this.bitField0_ = (0x4 | this.bitField0_);
        this.charRescoringFile_ = paramByteString;
      }

      public Builder setClassifierType(String paramString)
      {
        if (paramString == null)
          throw new NullPointerException();
        this.bitField0_ = (0x8 | this.bitField0_);
        this.classifierType_ = paramString;
        return this;
      }

      void setClassifierType(ByteString paramByteString)
      {
        this.bitField0_ = (0x8 | this.bitField0_);
        this.classifierType_ = paramByteString;
      }

      public Builder setInkreader(String paramString)
      {
        if (paramString == null)
          throw new NullPointerException();
        this.bitField0_ = (0x1 | this.bitField0_);
        this.inkreader_ = paramString;
        return this;
      }

      void setInkreader(ByteString paramByteString)
      {
        this.bitField0_ = (0x1 | this.bitField0_);
        this.inkreader_ = paramByteString;
      }

      public Builder setModel(String paramString)
      {
        if (paramString == null)
          throw new NullPointerException();
        this.bitField0_ = (0x2 | this.bitField0_);
        this.model_ = paramString;
        return this;
      }

      void setModel(ByteString paramByteString)
      {
        this.bitField0_ = (0x2 | this.bitField0_);
        this.model_ = paramByteString;
      }

      public Builder setNumResults(int paramInt)
      {
        this.bitField0_ = (0x10 | this.bitField0_);
        this.numResults_ = paramInt;
        return this;
      }
    }
  }

  public static abstract interface SingleCharacterRecognizerSpecOrBuilder extends MessageLiteOrBuilder
  {
    public abstract String getCharRescoringFile();

    public abstract String getClassifierType();

    public abstract String getInkreader();

    public abstract String getModel();

    public abstract int getNumResults();

    public abstract boolean hasCharRescoringFile();

    public abstract boolean hasClassifierType();

    public abstract boolean hasInkreader();

    public abstract boolean hasModel();

    public abstract boolean hasNumResults();
  }

  public static final class WordRecognizerSpec extends GeneratedMessageLite
    implements OndeviceSpec.WordRecognizerSpecOrBuilder
  {
    public static final int BEAM_SIZE_FIELD_NUMBER = 7;
    public static final int CLASSIFIER_TYPE_FIELD_NUMBER = 3;
    public static final int INKREADER_FIELD_NUMBER = 1;
    public static final int LANGUAGE_MODEL_FIELD_NUMBER = 5;
    public static final int LM_ORDER_FIELD_NUMBER = 6;
    public static final int MODEL_FIELD_NUMBER = 2;
    public static final int SYMBOLS_FIELD_NUMBER = 4;
    public static final int WEIGHTS_FIELD_NUMBER = 8;
    private static final WordRecognizerSpec defaultInstance = new WordRecognizerSpec(true);
    private static final long serialVersionUID;
    private int beamSize_;
    private int bitField0_;
    private Object classifierType_;
    private Object inkreader_;
    private Object languageModel_;
    private int lmOrder_;
    private byte memoizedIsInitialized = -1;
    private int memoizedSerializedSize = -1;
    private Object model_;
    private Object symbols_;
    private List<Float> weights_;

    static
    {
      defaultInstance.initFields();
    }

    private WordRecognizerSpec(Builder paramBuilder)
    {
      super();
    }

    private WordRecognizerSpec(boolean paramBoolean)
    {
    }

    private ByteString getClassifierTypeBytes()
    {
      Object localObject = this.classifierType_;
      if ((localObject instanceof String))
      {
        ByteString localByteString = ByteString.copyFromUtf8((String)localObject);
        this.classifierType_ = localByteString;
        return localByteString;
      }
      return (ByteString)localObject;
    }

    public static WordRecognizerSpec getDefaultInstance()
    {
      return defaultInstance;
    }

    private ByteString getInkreaderBytes()
    {
      Object localObject = this.inkreader_;
      if ((localObject instanceof String))
      {
        ByteString localByteString = ByteString.copyFromUtf8((String)localObject);
        this.inkreader_ = localByteString;
        return localByteString;
      }
      return (ByteString)localObject;
    }

    private ByteString getLanguageModelBytes()
    {
      Object localObject = this.languageModel_;
      if ((localObject instanceof String))
      {
        ByteString localByteString = ByteString.copyFromUtf8((String)localObject);
        this.languageModel_ = localByteString;
        return localByteString;
      }
      return (ByteString)localObject;
    }

    private ByteString getModelBytes()
    {
      Object localObject = this.model_;
      if ((localObject instanceof String))
      {
        ByteString localByteString = ByteString.copyFromUtf8((String)localObject);
        this.model_ = localByteString;
        return localByteString;
      }
      return (ByteString)localObject;
    }

    private ByteString getSymbolsBytes()
    {
      Object localObject = this.symbols_;
      if ((localObject instanceof String))
      {
        ByteString localByteString = ByteString.copyFromUtf8((String)localObject);
        this.symbols_ = localByteString;
        return localByteString;
      }
      return (ByteString)localObject;
    }

    private void initFields()
    {
      this.inkreader_ = "";
      this.model_ = "";
      this.classifierType_ = "";
      this.symbols_ = "";
      this.languageModel_ = "";
      this.lmOrder_ = 0;
      this.beamSize_ = 0;
      this.weights_ = Collections.emptyList();
    }

    public static Builder newBuilder()
    {
      return Builder.access$4200();
    }

    public static Builder newBuilder(WordRecognizerSpec paramWordRecognizerSpec)
    {
      return newBuilder().mergeFrom(paramWordRecognizerSpec);
    }

    public static WordRecognizerSpec parseDelimitedFrom(InputStream paramInputStream)
      throws IOException
    {
      Builder localBuilder = newBuilder();
      if (localBuilder.mergeDelimitedFrom(paramInputStream))
        return localBuilder.buildParsed();
      return null;
    }

    public static WordRecognizerSpec parseDelimitedFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      Builder localBuilder = newBuilder();
      if (localBuilder.mergeDelimitedFrom(paramInputStream, paramExtensionRegistryLite))
        return localBuilder.buildParsed();
      return null;
    }

    public static WordRecognizerSpec parseFrom(ByteString paramByteString)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramByteString)).buildParsed();
    }

    public static WordRecognizerSpec parseFrom(ByteString paramByteString, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramByteString, paramExtensionRegistryLite)).buildParsed();
    }

    public static WordRecognizerSpec parseFrom(CodedInputStream paramCodedInputStream)
      throws IOException
    {
      return ((Builder)newBuilder().mergeFrom(paramCodedInputStream)).buildParsed();
    }

    public static WordRecognizerSpec parseFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return newBuilder().mergeFrom(paramCodedInputStream, paramExtensionRegistryLite).buildParsed();
    }

    public static WordRecognizerSpec parseFrom(InputStream paramInputStream)
      throws IOException
    {
      return ((Builder)newBuilder().mergeFrom(paramInputStream)).buildParsed();
    }

    public static WordRecognizerSpec parseFrom(InputStream paramInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
      throws IOException
    {
      return ((Builder)newBuilder().mergeFrom(paramInputStream, paramExtensionRegistryLite)).buildParsed();
    }

    public static WordRecognizerSpec parseFrom(byte[] paramArrayOfByte)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramArrayOfByte)).buildParsed();
    }

    public static WordRecognizerSpec parseFrom(byte[] paramArrayOfByte, ExtensionRegistryLite paramExtensionRegistryLite)
      throws InvalidProtocolBufferException
    {
      return ((Builder)newBuilder().mergeFrom(paramArrayOfByte, paramExtensionRegistryLite)).buildParsed();
    }

    public int getBeamSize()
    {
      return this.beamSize_;
    }

    public String getClassifierType()
    {
      Object localObject = this.classifierType_;
      if ((localObject instanceof String))
        return (String)localObject;
      ByteString localByteString = (ByteString)localObject;
      String str = localByteString.toStringUtf8();
      if (Internal.isValidUtf8(localByteString))
        this.classifierType_ = str;
      return str;
    }

    public WordRecognizerSpec getDefaultInstanceForType()
    {
      return defaultInstance;
    }

    public String getInkreader()
    {
      Object localObject = this.inkreader_;
      if ((localObject instanceof String))
        return (String)localObject;
      ByteString localByteString = (ByteString)localObject;
      String str = localByteString.toStringUtf8();
      if (Internal.isValidUtf8(localByteString))
        this.inkreader_ = str;
      return str;
    }

    public String getLanguageModel()
    {
      Object localObject = this.languageModel_;
      if ((localObject instanceof String))
        return (String)localObject;
      ByteString localByteString = (ByteString)localObject;
      String str = localByteString.toStringUtf8();
      if (Internal.isValidUtf8(localByteString))
        this.languageModel_ = str;
      return str;
    }

    public int getLmOrder()
    {
      return this.lmOrder_;
    }

    public String getModel()
    {
      Object localObject = this.model_;
      if ((localObject instanceof String))
        return (String)localObject;
      ByteString localByteString = (ByteString)localObject;
      String str = localByteString.toStringUtf8();
      if (Internal.isValidUtf8(localByteString))
        this.model_ = str;
      return str;
    }

    public int getSerializedSize()
    {
      int i = this.memoizedSerializedSize;
      if (i != -1)
        return i;
      int j = 0x1 & this.bitField0_;
      int k = 0;
      if (j == 1)
        k = 0 + CodedOutputStream.computeBytesSize(1, getInkreaderBytes());
      if ((0x2 & this.bitField0_) == 2)
        k += CodedOutputStream.computeBytesSize(2, getModelBytes());
      if ((0x4 & this.bitField0_) == 4)
        k += CodedOutputStream.computeBytesSize(3, getClassifierTypeBytes());
      if ((0x8 & this.bitField0_) == 8)
        k += CodedOutputStream.computeBytesSize(4, getSymbolsBytes());
      if ((0x10 & this.bitField0_) == 16)
        k += CodedOutputStream.computeBytesSize(5, getLanguageModelBytes());
      if ((0x20 & this.bitField0_) == 32)
        k += CodedOutputStream.computeInt32Size(6, this.lmOrder_);
      if ((0x40 & this.bitField0_) == 64)
        k += CodedOutputStream.computeInt32Size(7, this.beamSize_);
      int m = k + 4 * getWeightsList().size() + 1 * getWeightsList().size();
      this.memoizedSerializedSize = m;
      return m;
    }

    public String getSymbols()
    {
      Object localObject = this.symbols_;
      if ((localObject instanceof String))
        return (String)localObject;
      ByteString localByteString = (ByteString)localObject;
      String str = localByteString.toStringUtf8();
      if (Internal.isValidUtf8(localByteString))
        this.symbols_ = str;
      return str;
    }

    public float getWeights(int paramInt)
    {
      return ((Float)this.weights_.get(paramInt)).floatValue();
    }

    public int getWeightsCount()
    {
      return this.weights_.size();
    }

    public List<Float> getWeightsList()
    {
      return this.weights_;
    }

    public boolean hasBeamSize()
    {
      return (0x40 & this.bitField0_) == 64;
    }

    public boolean hasClassifierType()
    {
      return (0x4 & this.bitField0_) == 4;
    }

    public boolean hasInkreader()
    {
      return (0x1 & this.bitField0_) == 1;
    }

    public boolean hasLanguageModel()
    {
      return (0x10 & this.bitField0_) == 16;
    }

    public boolean hasLmOrder()
    {
      return (0x20 & this.bitField0_) == 32;
    }

    public boolean hasModel()
    {
      return (0x2 & this.bitField0_) == 2;
    }

    public boolean hasSymbols()
    {
      return (0x8 & this.bitField0_) == 8;
    }

    public final boolean isInitialized()
    {
      int i = this.memoizedIsInitialized;
      if (i != -1)
        return i == 1;
      this.memoizedIsInitialized = 1;
      return true;
    }

    public Builder newBuilderForType()
    {
      return newBuilder();
    }

    public Builder toBuilder()
    {
      return newBuilder(this);
    }

    protected Object writeReplace()
      throws ObjectStreamException
    {
      return super.writeReplace();
    }

    public void writeTo(CodedOutputStream paramCodedOutputStream)
      throws IOException
    {
      getSerializedSize();
      if ((0x1 & this.bitField0_) == 1)
        paramCodedOutputStream.writeBytes(1, getInkreaderBytes());
      if ((0x2 & this.bitField0_) == 2)
        paramCodedOutputStream.writeBytes(2, getModelBytes());
      if ((0x4 & this.bitField0_) == 4)
        paramCodedOutputStream.writeBytes(3, getClassifierTypeBytes());
      if ((0x8 & this.bitField0_) == 8)
        paramCodedOutputStream.writeBytes(4, getSymbolsBytes());
      if ((0x10 & this.bitField0_) == 16)
        paramCodedOutputStream.writeBytes(5, getLanguageModelBytes());
      if ((0x20 & this.bitField0_) == 32)
        paramCodedOutputStream.writeInt32(6, this.lmOrder_);
      if ((0x40 & this.bitField0_) == 64)
        paramCodedOutputStream.writeInt32(7, this.beamSize_);
      for (int i = 0; i < this.weights_.size(); i++)
        paramCodedOutputStream.writeFloat(8, ((Float)this.weights_.get(i)).floatValue());
    }

    public static final class Builder extends GeneratedMessageLite.Builder<OndeviceSpec.WordRecognizerSpec, Builder>
      implements OndeviceSpec.WordRecognizerSpecOrBuilder
    {
      private int beamSize_;
      private int bitField0_;
      private Object classifierType_ = "";
      private Object inkreader_ = "";
      private Object languageModel_ = "";
      private int lmOrder_;
      private Object model_ = "";
      private Object symbols_ = "";
      private List<Float> weights_ = Collections.emptyList();

      private Builder()
      {
        maybeForceBuilderInitialization();
      }

      private OndeviceSpec.WordRecognizerSpec buildParsed()
        throws InvalidProtocolBufferException
      {
        OndeviceSpec.WordRecognizerSpec localWordRecognizerSpec = buildPartial();
        if (!localWordRecognizerSpec.isInitialized())
          throw newUninitializedMessageException(localWordRecognizerSpec).asInvalidProtocolBufferException();
        return localWordRecognizerSpec;
      }

      private static Builder create()
      {
        return new Builder();
      }

      private void ensureWeightsIsMutable()
      {
        if ((0x80 & this.bitField0_) != 128)
        {
          this.weights_ = new ArrayList(this.weights_);
          this.bitField0_ = (0x80 | this.bitField0_);
        }
      }

      private void maybeForceBuilderInitialization()
      {
      }

      public Builder addAllWeights(Iterable<? extends Float> paramIterable)
      {
        ensureWeightsIsMutable();
        GeneratedMessageLite.Builder.addAll(paramIterable, this.weights_);
        return this;
      }

      public Builder addWeights(float paramFloat)
      {
        ensureWeightsIsMutable();
        this.weights_.add(Float.valueOf(paramFloat));
        return this;
      }

      public OndeviceSpec.WordRecognizerSpec build()
      {
        OndeviceSpec.WordRecognizerSpec localWordRecognizerSpec = buildPartial();
        if (!localWordRecognizerSpec.isInitialized())
          throw newUninitializedMessageException(localWordRecognizerSpec);
        return localWordRecognizerSpec;
      }

      public OndeviceSpec.WordRecognizerSpec buildPartial()
      {
        int i = 1;
        OndeviceSpec.WordRecognizerSpec localWordRecognizerSpec = new OndeviceSpec.WordRecognizerSpec(this, null);
        int j = this.bitField0_;
        if ((j & 0x1) == i);
        while (true)
        {
          OndeviceSpec.WordRecognizerSpec.access$4402(localWordRecognizerSpec, this.inkreader_);
          if ((j & 0x2) == 2)
            i |= 2;
          OndeviceSpec.WordRecognizerSpec.access$4502(localWordRecognizerSpec, this.model_);
          if ((j & 0x4) == 4)
            i |= 4;
          OndeviceSpec.WordRecognizerSpec.access$4602(localWordRecognizerSpec, this.classifierType_);
          if ((j & 0x8) == 8)
            i |= 8;
          OndeviceSpec.WordRecognizerSpec.access$4702(localWordRecognizerSpec, this.symbols_);
          if ((j & 0x10) == 16)
            i |= 16;
          OndeviceSpec.WordRecognizerSpec.access$4802(localWordRecognizerSpec, this.languageModel_);
          if ((j & 0x20) == 32)
            i |= 32;
          OndeviceSpec.WordRecognizerSpec.access$4902(localWordRecognizerSpec, this.lmOrder_);
          if ((j & 0x40) == 64)
            i |= 64;
          OndeviceSpec.WordRecognizerSpec.access$5002(localWordRecognizerSpec, this.beamSize_);
          if ((0x80 & this.bitField0_) == 128)
          {
            this.weights_ = Collections.unmodifiableList(this.weights_);
            this.bitField0_ = (0xFFFFFF7F & this.bitField0_);
          }
          OndeviceSpec.WordRecognizerSpec.access$5102(localWordRecognizerSpec, this.weights_);
          OndeviceSpec.WordRecognizerSpec.access$5202(localWordRecognizerSpec, i);
          return localWordRecognizerSpec;
          i = 0;
        }
      }

      public Builder clear()
      {
        super.clear();
        this.inkreader_ = "";
        this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
        this.model_ = "";
        this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
        this.classifierType_ = "";
        this.bitField0_ = (0xFFFFFFFB & this.bitField0_);
        this.symbols_ = "";
        this.bitField0_ = (0xFFFFFFF7 & this.bitField0_);
        this.languageModel_ = "";
        this.bitField0_ = (0xFFFFFFEF & this.bitField0_);
        this.lmOrder_ = 0;
        this.bitField0_ = (0xFFFFFFDF & this.bitField0_);
        this.beamSize_ = 0;
        this.bitField0_ = (0xFFFFFFBF & this.bitField0_);
        this.weights_ = Collections.emptyList();
        this.bitField0_ = (0xFFFFFF7F & this.bitField0_);
        return this;
      }

      public Builder clearBeamSize()
      {
        this.bitField0_ = (0xFFFFFFBF & this.bitField0_);
        this.beamSize_ = 0;
        return this;
      }

      public Builder clearClassifierType()
      {
        this.bitField0_ = (0xFFFFFFFB & this.bitField0_);
        this.classifierType_ = OndeviceSpec.WordRecognizerSpec.getDefaultInstance().getClassifierType();
        return this;
      }

      public Builder clearInkreader()
      {
        this.bitField0_ = (0xFFFFFFFE & this.bitField0_);
        this.inkreader_ = OndeviceSpec.WordRecognizerSpec.getDefaultInstance().getInkreader();
        return this;
      }

      public Builder clearLanguageModel()
      {
        this.bitField0_ = (0xFFFFFFEF & this.bitField0_);
        this.languageModel_ = OndeviceSpec.WordRecognizerSpec.getDefaultInstance().getLanguageModel();
        return this;
      }

      public Builder clearLmOrder()
      {
        this.bitField0_ = (0xFFFFFFDF & this.bitField0_);
        this.lmOrder_ = 0;
        return this;
      }

      public Builder clearModel()
      {
        this.bitField0_ = (0xFFFFFFFD & this.bitField0_);
        this.model_ = OndeviceSpec.WordRecognizerSpec.getDefaultInstance().getModel();
        return this;
      }

      public Builder clearSymbols()
      {
        this.bitField0_ = (0xFFFFFFF7 & this.bitField0_);
        this.symbols_ = OndeviceSpec.WordRecognizerSpec.getDefaultInstance().getSymbols();
        return this;
      }

      public Builder clearWeights()
      {
        this.weights_ = Collections.emptyList();
        this.bitField0_ = (0xFFFFFF7F & this.bitField0_);
        return this;
      }

      public Builder clone()
      {
        return create().mergeFrom(buildPartial());
      }

      public int getBeamSize()
      {
        return this.beamSize_;
      }

      public String getClassifierType()
      {
        Object localObject = this.classifierType_;
        if (!(localObject instanceof String))
        {
          String str = ((ByteString)localObject).toStringUtf8();
          this.classifierType_ = str;
          return str;
        }
        return (String)localObject;
      }

      public OndeviceSpec.WordRecognizerSpec getDefaultInstanceForType()
      {
        return OndeviceSpec.WordRecognizerSpec.getDefaultInstance();
      }

      public String getInkreader()
      {
        Object localObject = this.inkreader_;
        if (!(localObject instanceof String))
        {
          String str = ((ByteString)localObject).toStringUtf8();
          this.inkreader_ = str;
          return str;
        }
        return (String)localObject;
      }

      public String getLanguageModel()
      {
        Object localObject = this.languageModel_;
        if (!(localObject instanceof String))
        {
          String str = ((ByteString)localObject).toStringUtf8();
          this.languageModel_ = str;
          return str;
        }
        return (String)localObject;
      }

      public int getLmOrder()
      {
        return this.lmOrder_;
      }

      public String getModel()
      {
        Object localObject = this.model_;
        if (!(localObject instanceof String))
        {
          String str = ((ByteString)localObject).toStringUtf8();
          this.model_ = str;
          return str;
        }
        return (String)localObject;
      }

      public String getSymbols()
      {
        Object localObject = this.symbols_;
        if (!(localObject instanceof String))
        {
          String str = ((ByteString)localObject).toStringUtf8();
          this.symbols_ = str;
          return str;
        }
        return (String)localObject;
      }

      public float getWeights(int paramInt)
      {
        return ((Float)this.weights_.get(paramInt)).floatValue();
      }

      public int getWeightsCount()
      {
        return this.weights_.size();
      }

      public List<Float> getWeightsList()
      {
        return Collections.unmodifiableList(this.weights_);
      }

      public boolean hasBeamSize()
      {
        return (0x40 & this.bitField0_) == 64;
      }

      public boolean hasClassifierType()
      {
        return (0x4 & this.bitField0_) == 4;
      }

      public boolean hasInkreader()
      {
        return (0x1 & this.bitField0_) == 1;
      }

      public boolean hasLanguageModel()
      {
        return (0x10 & this.bitField0_) == 16;
      }

      public boolean hasLmOrder()
      {
        return (0x20 & this.bitField0_) == 32;
      }

      public boolean hasModel()
      {
        return (0x2 & this.bitField0_) == 2;
      }

      public boolean hasSymbols()
      {
        return (0x8 & this.bitField0_) == 8;
      }

      public final boolean isInitialized()
      {
        return true;
      }

      public Builder mergeFrom(CodedInputStream paramCodedInputStream, ExtensionRegistryLite paramExtensionRegistryLite)
        throws IOException
      {
        while (true)
        {
          int i = paramCodedInputStream.readTag();
          switch (i)
          {
          default:
            if (parseUnknownField(paramCodedInputStream, paramExtensionRegistryLite, i))
              continue;
          case 0:
            return this;
          case 10:
            this.bitField0_ = (0x1 | this.bitField0_);
            this.inkreader_ = paramCodedInputStream.readBytes();
            break;
          case 18:
            this.bitField0_ = (0x2 | this.bitField0_);
            this.model_ = paramCodedInputStream.readBytes();
            break;
          case 26:
            this.bitField0_ = (0x4 | this.bitField0_);
            this.classifierType_ = paramCodedInputStream.readBytes();
            break;
          case 34:
            this.bitField0_ = (0x8 | this.bitField0_);
            this.symbols_ = paramCodedInputStream.readBytes();
            break;
          case 42:
            this.bitField0_ = (0x10 | this.bitField0_);
            this.languageModel_ = paramCodedInputStream.readBytes();
            break;
          case 48:
            this.bitField0_ = (0x20 | this.bitField0_);
            this.lmOrder_ = paramCodedInputStream.readInt32();
            break;
          case 56:
            this.bitField0_ = (0x40 | this.bitField0_);
            this.beamSize_ = paramCodedInputStream.readInt32();
            break;
          case 69:
            ensureWeightsIsMutable();
            this.weights_.add(Float.valueOf(paramCodedInputStream.readFloat()));
            break;
          case 66:
          }
          int j = paramCodedInputStream.pushLimit(paramCodedInputStream.readRawVarint32());
          while (paramCodedInputStream.getBytesUntilLimit() > 0)
            addWeights(paramCodedInputStream.readFloat());
          paramCodedInputStream.popLimit(j);
        }
      }

      public Builder mergeFrom(OndeviceSpec.WordRecognizerSpec paramWordRecognizerSpec)
      {
        if (paramWordRecognizerSpec == OndeviceSpec.WordRecognizerSpec.getDefaultInstance());
        do
        {
          return this;
          if (paramWordRecognizerSpec.hasInkreader())
            setInkreader(paramWordRecognizerSpec.getInkreader());
          if (paramWordRecognizerSpec.hasModel())
            setModel(paramWordRecognizerSpec.getModel());
          if (paramWordRecognizerSpec.hasClassifierType())
            setClassifierType(paramWordRecognizerSpec.getClassifierType());
          if (paramWordRecognizerSpec.hasSymbols())
            setSymbols(paramWordRecognizerSpec.getSymbols());
          if (paramWordRecognizerSpec.hasLanguageModel())
            setLanguageModel(paramWordRecognizerSpec.getLanguageModel());
          if (paramWordRecognizerSpec.hasLmOrder())
            setLmOrder(paramWordRecognizerSpec.getLmOrder());
          if (paramWordRecognizerSpec.hasBeamSize())
            setBeamSize(paramWordRecognizerSpec.getBeamSize());
        }
        while (paramWordRecognizerSpec.weights_.isEmpty());
        if (this.weights_.isEmpty())
        {
          this.weights_ = paramWordRecognizerSpec.weights_;
          this.bitField0_ = (0xFFFFFF7F & this.bitField0_);
          return this;
        }
        ensureWeightsIsMutable();
        this.weights_.addAll(paramWordRecognizerSpec.weights_);
        return this;
      }

      public Builder setBeamSize(int paramInt)
      {
        this.bitField0_ = (0x40 | this.bitField0_);
        this.beamSize_ = paramInt;
        return this;
      }

      public Builder setClassifierType(String paramString)
      {
        if (paramString == null)
          throw new NullPointerException();
        this.bitField0_ = (0x4 | this.bitField0_);
        this.classifierType_ = paramString;
        return this;
      }

      void setClassifierType(ByteString paramByteString)
      {
        this.bitField0_ = (0x4 | this.bitField0_);
        this.classifierType_ = paramByteString;
      }

      public Builder setInkreader(String paramString)
      {
        if (paramString == null)
          throw new NullPointerException();
        this.bitField0_ = (0x1 | this.bitField0_);
        this.inkreader_ = paramString;
        return this;
      }

      void setInkreader(ByteString paramByteString)
      {
        this.bitField0_ = (0x1 | this.bitField0_);
        this.inkreader_ = paramByteString;
      }

      public Builder setLanguageModel(String paramString)
      {
        if (paramString == null)
          throw new NullPointerException();
        this.bitField0_ = (0x10 | this.bitField0_);
        this.languageModel_ = paramString;
        return this;
      }

      void setLanguageModel(ByteString paramByteString)
      {
        this.bitField0_ = (0x10 | this.bitField0_);
        this.languageModel_ = paramByteString;
      }

      public Builder setLmOrder(int paramInt)
      {
        this.bitField0_ = (0x20 | this.bitField0_);
        this.lmOrder_ = paramInt;
        return this;
      }

      public Builder setModel(String paramString)
      {
        if (paramString == null)
          throw new NullPointerException();
        this.bitField0_ = (0x2 | this.bitField0_);
        this.model_ = paramString;
        return this;
      }

      void setModel(ByteString paramByteString)
      {
        this.bitField0_ = (0x2 | this.bitField0_);
        this.model_ = paramByteString;
      }

      public Builder setSymbols(String paramString)
      {
        if (paramString == null)
          throw new NullPointerException();
        this.bitField0_ = (0x8 | this.bitField0_);
        this.symbols_ = paramString;
        return this;
      }

      void setSymbols(ByteString paramByteString)
      {
        this.bitField0_ = (0x8 | this.bitField0_);
        this.symbols_ = paramByteString;
      }

      public Builder setWeights(int paramInt, float paramFloat)
      {
        ensureWeightsIsMutable();
        this.weights_.set(paramInt, Float.valueOf(paramFloat));
        return this;
      }
    }
  }

  public static abstract interface WordRecognizerSpecOrBuilder extends MessageLiteOrBuilder
  {
    public abstract int getBeamSize();

    public abstract String getClassifierType();

    public abstract String getInkreader();

    public abstract String getLanguageModel();

    public abstract int getLmOrder();

    public abstract String getModel();

    public abstract String getSymbols();

    public abstract float getWeights(int paramInt);

    public abstract int getWeightsCount();

    public abstract List<Float> getWeightsList();

    public abstract boolean hasBeamSize();

    public abstract boolean hasClassifierType();

    public abstract boolean hasInkreader();

    public abstract boolean hasLanguageModel();

    public abstract boolean hasLmOrder();

    public abstract boolean hasModel();

    public abstract boolean hasSymbols();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.protos.research_handwriting.OndeviceSpec
 * JD-Core Version:    0.6.2
 */