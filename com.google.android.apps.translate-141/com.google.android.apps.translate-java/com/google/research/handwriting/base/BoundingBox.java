package com.google.research.handwriting.base;

import java.util.Iterator;

public class BoundingBox
{
  public final float[] tlbr;

  public BoundingBox(Stroke paramStroke)
  {
    this.tlbr = new float[] { 3.4028235E+38F, 3.4028235E+38F, -3.402824E+038F, -3.402824E+038F };
    Iterator localIterator = paramStroke.iterator();
    while (localIterator.hasNext())
    {
      Stroke.Point localPoint = (Stroke.Point)localIterator.next();
      if (localPoint.y < this.tlbr[0])
        this.tlbr[0] = localPoint.y;
      if (localPoint.x < this.tlbr[1])
        this.tlbr[1] = localPoint.x;
      if (localPoint.y > this.tlbr[2])
        this.tlbr[2] = localPoint.y;
      if (localPoint.x > this.tlbr[3])
        this.tlbr[3] = localPoint.x;
    }
  }

  public BoundingBox(StrokeList paramStrokeList)
  {
    this.tlbr = new float[] { 3.4028235E+38F, 3.4028235E+38F, -3.402824E+038F, -3.402824E+038F };
    Iterator localIterator = paramStrokeList.iterator();
    while (localIterator.hasNext())
    {
      BoundingBox localBoundingBox = new BoundingBox((Stroke)localIterator.next());
      if (localBoundingBox.tlbr[0] < this.tlbr[0])
        this.tlbr[0] = localBoundingBox.tlbr[0];
      if (localBoundingBox.tlbr[1] < this.tlbr[1])
        this.tlbr[1] = localBoundingBox.tlbr[1];
      if (localBoundingBox.tlbr[2] > this.tlbr[2])
        this.tlbr[2] = localBoundingBox.tlbr[2];
      if (localBoundingBox.tlbr[3] > this.tlbr[3])
        this.tlbr[3] = localBoundingBox.tlbr[3];
    }
  }

  public BoundingBox(float[] paramArrayOfFloat)
  {
    this.tlbr = paramArrayOfFloat;
  }

  public float bottom()
  {
    return this.tlbr[2];
  }

  public float getHeight()
  {
    float f = bottom() - top();
    if (f < 1.175494E-038F)
      f = 1.0F;
    return f;
  }

  public float getWidth()
  {
    float f = right() - left();
    if (f < 1.175494E-038F)
      f = 1.0F;
    return f;
  }

  public float left()
  {
    return this.tlbr[1];
  }

  public float right()
  {
    return this.tlbr[3];
  }

  public void setBottom(float paramFloat)
  {
    this.tlbr[2] = paramFloat;
  }

  public void setLeft(float paramFloat)
  {
    this.tlbr[1] = paramFloat;
  }

  public void setRight(float paramFloat)
  {
    this.tlbr[3] = paramFloat;
  }

  public void setTop(float paramFloat)
  {
    this.tlbr[0] = paramFloat;
  }

  public float top()
  {
    return this.tlbr[0];
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.research.handwriting.base.BoundingBox
 * JD-Core Version:    0.6.2
 */