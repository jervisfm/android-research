package com.google.research.handwriting.base;

import com.google.protos.research_handwriting.OndeviceSpec.HandwritingRecognizerSpec;

public abstract interface HandwritingRecognizer
{
  public abstract void feedback(String paramString1, StrokeList paramStrokeList, String paramString2, String paramString3)
    throws HandwritingRecognizer.SendingFeedbackFailedException;

  public abstract void feedback(String paramString1, String paramString2, String paramString3, String paramString4)
    throws HandwritingRecognizer.SendingFeedbackFailedException;

  public abstract HandwritingRecognizer getSecondaryRecognizer();

  public abstract HandwritingRecognizerSettings getSettings();

  public abstract boolean hasSecondaryRecognizer();

  public abstract RecognitionResult recognize(StrokeList paramStrokeList, CancelStruct paramCancelStruct)
    throws HandwritingRecognizer.RecognitionFailedException;

  public abstract void setSecondaryRecognizer(HandwritingRecognizer paramHandwritingRecognizer);

  public static abstract interface CancelStruct
  {
    public static final CancelStruct UNCANCELABLE = new CancelStruct()
    {
      public boolean isCanceled()
      {
        return false;
      }
    };

    public abstract boolean isCanceled();
  }

  public static class HandwritingRecognizerSettings
  {
    public String clientName = "unset";
    public int clientVersion;
    public String deviceName;
    public int deviceVersion;
    public String language = "en";
    public int maxRequestsInParallel;
    public String targetLanguage = "";
    public int timeoutBeforeNextRequest;
    public boolean useSpaces;
    public boolean useTranslateApi = false;
    public int verbosity;

    public HandwritingRecognizerSettings()
    {
    }

    public HandwritingRecognizerSettings(OndeviceSpec.HandwritingRecognizerSpec paramHandwritingRecognizerSpec)
    {
      this.language = paramHandwritingRecognizerSpec.getLanguageCode();
      this.verbosity = paramHandwritingRecognizerSpec.getVerbosity();
      this.maxRequestsInParallel = paramHandwritingRecognizerSpec.getMaximalParallelRequests();
      this.timeoutBeforeNextRequest = paramHandwritingRecognizerSpec.getTimeoutBeforeNextRequest();
      this.useSpaces = paramHandwritingRecognizerSpec.getUseSpaces();
    }
  }

  public static class RecognitionFailedException extends Exception
  {
    public final Exception e;
    public final String s;

    public RecognitionFailedException(String paramString, Exception paramException)
    {
      this.e = paramException;
      this.s = paramString;
    }
  }

  public static class SendingFeedbackFailedException extends Exception
  {
    public final Exception e;
    public final String s;

    public SendingFeedbackFailedException(String paramString, Exception paramException)
    {
      this.e = paramException;
      this.s = paramString;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.research.handwriting.base.HandwritingRecognizer
 * JD-Core Version:    0.6.2
 */