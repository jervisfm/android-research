package com.google.research.handwriting.base;

import android.content.Context;
import android.util.Log;
import com.google.protos.research_handwriting.OndeviceSpec.HandwritingRecognizerSpec;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class HandwritingRecognizerFactory
{
  private static final String TAG = "HandwritingRecognizerFactory";

  public static HandwritingRecognizer create(OndeviceSpec.HandwritingRecognizerSpec paramHandwritingRecognizerSpec)
  {
    try
    {
      HandwritingRecognizer localHandwritingRecognizer = (HandwritingRecognizer)Class.forName(paramHandwritingRecognizerSpec.getName()).getConstructor(new Class[] { OndeviceSpec.HandwritingRecognizerSpec.class }).newInstance(new Object[] { paramHandwritingRecognizerSpec });
      if (paramHandwritingRecognizerSpec.hasSecondaryRecognizer())
        localHandwritingRecognizer.setSecondaryRecognizer(create(paramHandwritingRecognizerSpec.getSecondaryRecognizer()));
      return localHandwritingRecognizer;
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      Log.e("HandwritingRecognizerFactory", "ClassNotFoundException", localClassNotFoundException);
      return null;
    }
    catch (NoSuchMethodException localNoSuchMethodException)
    {
      while (true)
        Log.e("HandwritingRecognizerFactory", "NoSuchMethodException", localNoSuchMethodException);
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      while (true)
        Log.e("HandwritingRecognizerFactory", "IllegalArgumentException", localIllegalArgumentException);
    }
    catch (InstantiationException localInstantiationException)
    {
      while (true)
        Log.e("HandwritingRecognizerFactory", "InstantiationException", localInstantiationException);
    }
    catch (IllegalAccessException localIllegalAccessException)
    {
      while (true)
        Log.e("HandwritingRecognizerFactory", "IllegalAccessException", localIllegalAccessException);
    }
    catch (InvocationTargetException localInvocationTargetException)
    {
      while (true)
        Log.e("HandwritingRecognizerFactory", "InvocationTargetException", localInvocationTargetException);
    }
  }

  public static HandwritingRecognizer create(OndeviceSpec.HandwritingRecognizerSpec paramHandwritingRecognizerSpec, Context paramContext)
  {
    try
    {
      HandwritingRecognizer localHandwritingRecognizer = (HandwritingRecognizer)Class.forName(paramHandwritingRecognizerSpec.getName()).getConstructor(new Class[] { OndeviceSpec.HandwritingRecognizerSpec.class, Context.class }).newInstance(new Object[] { paramHandwritingRecognizerSpec, paramContext });
      if (paramHandwritingRecognizerSpec.hasSecondaryRecognizer())
        localHandwritingRecognizer.setSecondaryRecognizer(create(paramHandwritingRecognizerSpec.getSecondaryRecognizer(), paramContext));
      return localHandwritingRecognizer;
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      Log.e("HandwritingRecognizerFactory", "ClassNotFoundException", localClassNotFoundException);
      return null;
    }
    catch (NoSuchMethodException localNoSuchMethodException)
    {
      while (true)
        Log.e("HandwritingRecognizerFactory", "NoSuchMethodException", localNoSuchMethodException);
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      while (true)
        Log.e("HandwritingRecognizerFactory", "IllegalArgumentException", localIllegalArgumentException);
    }
    catch (InstantiationException localInstantiationException)
    {
      while (true)
        Log.e("HandwritingRecognizerFactory", "InstantiationException", localInstantiationException);
    }
    catch (IllegalAccessException localIllegalAccessException)
    {
      while (true)
        Log.e("HandwritingRecognizerFactory", "IllegalAccessException", localIllegalAccessException);
    }
    catch (InvocationTargetException localInvocationTargetException)
    {
      while (true)
        Log.e("HandwritingRecognizerFactory", "InvocationTargetException", localInvocationTargetException);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.research.handwriting.base.HandwritingRecognizerFactory
 * JD-Core Version:    0.6.2
 */