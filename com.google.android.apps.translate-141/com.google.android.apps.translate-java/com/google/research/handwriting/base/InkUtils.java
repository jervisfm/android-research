package com.google.research.handwriting.base;

import android.util.FloatMath;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeSet;

public class InkUtils
{
  public static List<Integer> getDelayedStrokeOrder(StrokeList paramStrokeList)
  {
    ArrayList localArrayList1 = new ArrayList(paramStrokeList.cuts().size());
    TreeSet localTreeSet = new TreeSet();
    for (int i = 0; i < 1 + paramStrokeList.cuts().size(); i++)
    {
      localTreeSet.clear();
      localTreeSet.add(Integer.valueOf(i));
      localArrayList1.add(new FloatStrokeIdPair(getRank(new BoundingBox(getSubInk(paramStrokeList, localTreeSet))).floatValue(), i));
    }
    Collections.sort(localArrayList1, new Comparator()
    {
      public int compare(InkUtils.FloatStrokeIdPair paramAnonymousFloatStrokeIdPair1, InkUtils.FloatStrokeIdPair paramAnonymousFloatStrokeIdPair2)
      {
        return (int)Math.signum(paramAnonymousFloatStrokeIdPair1.f - paramAnonymousFloatStrokeIdPair2.f);
      }
    });
    ArrayList localArrayList2 = new ArrayList();
    Iterator localIterator = localArrayList1.iterator();
    while (localIterator.hasNext())
      localArrayList2.add(Integer.valueOf(((FloatStrokeIdPair)localIterator.next()).i));
    return localArrayList2;
  }

  private static Float getRank(BoundingBox paramBoundingBox)
  {
    return new Float(0.75F * paramBoundingBox.left() + 0.25F * paramBoundingBox.right());
  }

  public static StrokeList getSubInk(StrokeList paramStrokeList, TreeSet<Integer> paramTreeSet)
  {
    StrokeList localStrokeList = new StrokeList();
    int i = paramStrokeList.cuts().size();
    int j = paramStrokeList.size();
    int k = paramTreeSet.size();
    int[] arrayOfInt = new int[paramTreeSet.size()];
    int m = 0;
    Iterator localIterator = paramTreeSet.iterator();
    while (localIterator.hasNext())
    {
      Integer localInteger = (Integer)localIterator.next();
      int i3 = m + 1;
      arrayOfInt[m] = localInteger.intValue();
      m = i3;
    }
    for (int n = 0; n < k; n++)
    {
      int i1 = arrayOfInt[n];
      while ((n + 1 < k) && (arrayOfInt[(n + 1)] - arrayOfInt[n] == 1))
        n++;
      int i2 = arrayOfInt[n];
      Object localObject;
      if (i1 == 0)
      {
        localObject = new StrokeList.Cut(0, 0);
        if (i2 != i)
          break label330;
      }
      label330: for (StrokeList.Cut localCut1 = new StrokeList.Cut(j - 1, paramStrokeList.getLast().size()); ; localCut1 = (StrokeList.Cut)paramStrokeList.cuts().get(i2))
      {
        Stroke localStroke1 = (Stroke)paramStrokeList.get(((StrokeList.Cut)localObject).stroke);
        Stroke localStroke2 = new Stroke();
        localStrokeList.add(localStroke2);
        while (isBefore((StrokeList.Cut)localObject, localCut1))
        {
          localStroke2.addPoint(localStroke1.get(((StrokeList.Cut)localObject).point));
          StrokeList.Cut localCut2 = next(paramStrokeList, (StrokeList.Cut)localObject);
          if ((localCut2.stroke > ((StrokeList.Cut)localObject).stroke) && (isBefore(localCut2, localCut1)))
          {
            localStroke1 = (Stroke)paramStrokeList.get(localCut2.stroke);
            localStroke2 = new Stroke();
            localStrokeList.add(localStroke2);
          }
          localObject = localCut2;
        }
        localObject = (StrokeList.Cut)paramStrokeList.cuts().get(i1 - 1);
        break;
      }
    }
    return localStrokeList;
  }

  private static boolean isBefore(StrokeList.Cut paramCut1, StrokeList.Cut paramCut2)
  {
    return (paramCut1.stroke < paramCut2.stroke) || ((paramCut1.stroke == paramCut2.stroke) && (paramCut1.point < paramCut2.point));
  }

  public static Stroke moveAndScale(Stroke paramStroke, float paramFloat1, float paramFloat2, float paramFloat3)
  {
    Stroke localStroke = new Stroke(paramStroke.size());
    Iterator localIterator = paramStroke.iterator();
    while (localIterator.hasNext())
    {
      Stroke.Point localPoint = (Stroke.Point)localIterator.next();
      localStroke.addPoint(paramFloat3 * (paramFloat1 + localPoint.x), paramFloat3 * (paramFloat2 + localPoint.y), localPoint.t, localPoint.p);
    }
    return localStroke;
  }

  public static StrokeList moveAndScale(StrokeList paramStrokeList, float paramFloat1, float paramFloat2, float paramFloat3)
  {
    StrokeList localStrokeList = new StrokeList(paramStrokeList.size());
    Iterator localIterator = paramStrokeList.iterator();
    while (localIterator.hasNext())
      localStrokeList.add(moveAndScale((Stroke)localIterator.next(), paramFloat1, paramFloat2, paramFloat3));
    return localStrokeList;
  }

  private static StrokeList.Cut next(StrokeList paramStrokeList, StrokeList.Cut paramCut)
  {
    int i = 1 + paramCut.point;
    int j = paramCut.stroke;
    if (i >= ((Stroke)paramStrokeList.get(j)).size())
    {
      i = 0;
      j++;
    }
    return new StrokeList.Cut(j, i, paramCut.cost, paramCut.type);
  }

  public static StrokeList normalizeHeuristic(StrokeList paramStrokeList, float paramFloat1, float paramFloat2, float paramFloat3)
  {
    int i = 1;
    if ((paramFloat2 < 1.175494E-038F) || (paramFloat2 < 1.175494E-038F))
      i = 0;
    BoundingBox localBoundingBox = new BoundingBox(paramStrokeList);
    float f1 = localBoundingBox.left();
    float f2 = localBoundingBox.getHeight();
    float f3 = localBoundingBox.left();
    float f4 = localBoundingBox.top();
    float f5 = paramFloat2;
    float f6 = f1;
    float f7 = 0.0F;
    if (i != 0)
    {
      boolean bool = paramFloat2 < f2;
      f7 = 0.0F;
      if (bool)
      {
        f5 = f2;
        f6 = f3;
        f7 = f4;
      }
    }
    if (paramFloat3 < 1.0F)
      paramFloat3 = 1.0F;
    if ((i == 0) || (paramFloat2 > paramFloat3 * f2))
    {
      f5 = paramFloat3 * f2;
      f6 = f3 - f2 * (0.5F * (paramFloat3 - 1.0F));
      f7 = f4 - f2 * (0.5F * (paramFloat3 - 1.0F));
    }
    return moveAndScale(paramStrokeList, -f6, -f7, 1.0F / f5);
  }

  public static StrokeList normalizeSizeXY(StrokeList paramStrokeList)
  {
    StrokeList localStrokeList = new StrokeList(paramStrokeList.size());
    BoundingBox localBoundingBox = new BoundingBox(paramStrokeList);
    float f1 = localBoundingBox.getWidth();
    float f2 = localBoundingBox.getHeight();
    Iterator localIterator1 = paramStrokeList.iterator();
    while (localIterator1.hasNext())
    {
      Stroke localStroke1 = (Stroke)localIterator1.next();
      Stroke localStroke2 = new Stroke(localStroke1.size());
      Iterator localIterator2 = localStroke1.iterator();
      while (localIterator2.hasNext())
      {
        Stroke.Point localPoint = (Stroke.Point)localIterator2.next();
        localStroke2.addPoint((localPoint.x - localBoundingBox.left()) / f1, (localPoint.y - localBoundingBox.top()) / f2, localPoint.t, localPoint.p);
      }
      localStrokeList.add(localStroke2);
    }
    return localStrokeList;
  }

  public static StrokeList normalizeWritingGuide(StrokeList paramStrokeList, float paramFloat1, float paramFloat2)
  {
    if ((paramFloat2 < 1.0E-006D) || (paramFloat2 < 1.0E-006D))
      return normalizeY(paramStrokeList);
    return moveAndScale(paramStrokeList, -new BoundingBox(paramStrokeList).left(), 0.0F, 1.0F / paramFloat2);
  }

  public static StrokeList normalizeY(StrokeList paramStrokeList)
  {
    return normalizeY(paramStrokeList, 0.0F);
  }

  public static StrokeList normalizeY(StrokeList paramStrokeList, float paramFloat)
  {
    BoundingBox localBoundingBox = new BoundingBox(paramStrokeList);
    float f1 = localBoundingBox.getHeight();
    float f2 = f1 * paramFloat;
    float f3 = localBoundingBox.top();
    float f4 = f1 + 2.0F * f2;
    return moveAndScale(paramStrokeList, -localBoundingBox.left(), -f3, 1.0F / f4);
  }

  public static StrokeList resampleLinearInterpolation(StrokeList paramStrokeList, float paramFloat, int paramInt)
  {
    StrokeList localStrokeList;
    label10: Iterator localIterator1;
    if (paramStrokeList.size() == 0)
    {
      localStrokeList = null;
      break label34;
      return localStrokeList;
    }
    else
    {
      int i = paramStrokeList.size();
      localStrokeList = new StrokeList(i);
      localIterator1 = paramStrokeList.iterator();
    }
    while (true)
    {
      label34: if (!localIterator1.hasNext())
        break label10;
      Stroke localStroke1 = (Stroke)localIterator1.next();
      if (localStroke1.isEmpty())
        break;
      float[] arrayOfFloat1 = new float[localStroke1.size()];
      float[] arrayOfFloat2 = new float[localStroke1.size()];
      Object localObject = null;
      int j = 0;
      Iterator localIterator2 = localStroke1.iterator();
      while (localIterator2.hasNext())
      {
        Stroke.Point localPoint4 = (Stroke.Point)localIterator2.next();
        if (localObject != null)
        {
          float f7 = localObject.x - localPoint4.x;
          float f8 = localObject.y - localPoint4.y;
          arrayOfFloat1[j] = FloatMath.sqrt(f7 * f7 + f8 * f8);
          arrayOfFloat2[j] = (arrayOfFloat2[(j - 1)] + arrayOfFloat1[j]);
        }
        localObject = localPoint4;
        j++;
      }
      float f1 = arrayOfFloat2[(-1 + arrayOfFloat2.length)];
      int k = (int)FloatMath.ceil(f1 / paramFloat);
      if ((paramInt > 0) && (k + 1 > paramInt * localStroke1.size()))
        k = -1 + paramInt * localStroke1.size();
      float f2 = 0.0F;
      if (k > 0)
        f2 = f1 / k;
      Stroke localStroke2 = new Stroke(k + 1, localStroke1.isPenDown());
      localStrokeList.add(localStroke2);
      Iterator localIterator3 = localStroke1.iterator();
      int m = 0;
      Stroke.Point localPoint1 = (Stroke.Point)localIterator3.next();
      localStroke2.addPoint(localPoint1.x, localPoint1.y, localPoint1.t, localPoint1.p);
      float f3 = 0.0F;
      Stroke.Point localPoint2 = localPoint1;
      Stroke.Point localPoint3 = null;
      for (int n = 1; n <= k; n++)
      {
        f3 += f2;
        while ((localIterator3.hasNext()) && (f3 > arrayOfFloat2[m]))
        {
          m++;
          localPoint3 = localPoint2;
          localPoint2 = (Stroke.Point)localIterator3.next();
        }
        float f4 = Math.abs(f3 - arrayOfFloat2[(m - 1)]);
        float f5 = Math.abs(arrayOfFloat2[m] - f3);
        float f6 = f4 + f5;
        localStroke2.addPoint((f4 * localPoint2.x + f5 * localPoint3.x) / f6, (f4 * localPoint2.y + f5 * localPoint3.y) / f6, (f4 * localPoint2.t + f5 * localPoint3.t) / f6, (f4 * localPoint2.p + f5 * localPoint3.p) / f6);
      }
    }
  }

  public static StrokeList smoothInkRectangleFilter(StrokeList paramStrokeList, int paramInt)
  {
    float[] arrayOfFloat = new float[1 + paramInt * 2];
    Arrays.fill(arrayOfFloat, 1.0F);
    return smoothInkWithFilter(paramStrokeList, arrayOfFloat);
  }

  private static StrokeList smoothInkWithFilter(StrokeList paramStrokeList, float[] paramArrayOfFloat)
  {
    StrokeList localStrokeList = new StrokeList(paramStrokeList.size());
    Iterator localIterator1 = paramStrokeList.iterator();
    while (localIterator1.hasNext())
    {
      Stroke localStroke1 = (Stroke)localIterator1.next();
      int i = localStroke1.size();
      Stroke localStroke2 = new Stroke(i, localStroke1.isPenDown());
      localStrokeList.add(localStroke2);
      int j = 0;
      int k = paramArrayOfFloat.length / 2;
      int m = (1 + paramArrayOfFloat.length) / 2;
      float[] arrayOfFloat1 = new float[i];
      float[] arrayOfFloat2 = new float[i];
      Iterator localIterator2 = localStroke1.iterator();
      while (localIterator2.hasNext())
      {
        Stroke.Point localPoint2 = (Stroke.Point)localIterator2.next();
        arrayOfFloat1[j] = localPoint2.x;
        arrayOfFloat2[j] = localPoint2.y;
        j++;
      }
      int n = 0;
      Iterator localIterator3 = localStroke1.iterator();
      while (localIterator3.hasNext())
      {
        Stroke.Point localPoint1 = (Stroke.Point)localIterator3.next();
        float f1 = 0.0F;
        float f2 = 0.0F;
        float f3 = 0.0F;
        for (int i1 = -k; i1 < m; i1++)
          if ((n + i1 >= 0) && (n + i1 < i))
          {
            f2 += paramArrayOfFloat[(i1 + k)] * arrayOfFloat1[(n + i1)];
            f3 += paramArrayOfFloat[(i1 + k)] * arrayOfFloat2[(n + i1)];
            f1 += paramArrayOfFloat[(i1 + k)];
          }
        localStroke2.addPoint(f2 / f1, f3 / f1, localPoint1.t, localPoint1.p);
        n++;
      }
    }
    return localStrokeList;
  }

  private static class FloatStrokeIdPair
  {
    public final float f;
    public final int i;

    public FloatStrokeIdPair(float paramFloat, int paramInt)
    {
      this.f = paramFloat;
      this.i = paramInt;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.research.handwriting.base.InkUtils
 * JD-Core Version:    0.6.2
 */