package com.google.research.handwriting.base;

import android.util.Log;

public class LogV
{
  private static int mLogVerbosity;

  public static String arrayToString(float[] paramArrayOfFloat)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    for (int i = 0; i < paramArrayOfFloat.length; i++)
    {
      if (i > 0)
        localStringBuilder.append(" ");
      localStringBuilder.append(paramArrayOfFloat[i]);
    }
    return localStringBuilder.toString();
  }

  public static <T> String arrayToString(T[] paramArrayOfT)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    for (int i = 0; i < paramArrayOfT.length; i++)
    {
      if (i > 0)
        localStringBuilder.append(" ");
      localStringBuilder.append(paramArrayOfT[i]);
    }
    return localStringBuilder.toString();
  }

  public static String arrayToString(short[] paramArrayOfShort)
  {
    StringBuilder localStringBuilder = new StringBuilder();
    for (int i = 0; i < paramArrayOfShort.length; i++)
    {
      if (i > 0)
        localStringBuilder.append(" ");
      localStringBuilder.append(paramArrayOfShort[i]);
    }
    return localStringBuilder.toString();
  }

  public static void i(int paramInt, String paramString1, String paramString2)
  {
    if (mLogVerbosity >= paramInt)
      Log.i(paramString1, paramString2);
  }

  public static void i(int paramInt, String paramString1, String paramString2, Throwable paramThrowable)
  {
    if (mLogVerbosity >= paramInt)
      Log.i(paramString1, paramString2, paramThrowable);
  }

  public static int logVerbosity()
  {
    return mLogVerbosity;
  }

  public static void setLogVerbosity(int paramInt)
  {
    mLogVerbosity = paramInt;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.research.handwriting.base.LogV
 * JD-Core Version:    0.6.2
 */