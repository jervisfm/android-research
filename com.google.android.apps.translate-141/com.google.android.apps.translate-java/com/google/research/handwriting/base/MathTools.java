package com.google.research.handwriting.base;

public class MathTools
{
  public static int argmax(double[] paramArrayOfDouble)
  {
    double d = -1.797693134862316E+308D;
    int i = -1;
    for (int j = 0; j < paramArrayOfDouble.length; j++)
      if (paramArrayOfDouble[j] >= d)
      {
        i = j;
        d = paramArrayOfDouble[j];
      }
    return i;
  }

  public static int argmax(float[] paramArrayOfFloat)
  {
    float f = -3.402824E+038F;
    int i = -1;
    for (int j = 0; j < paramArrayOfFloat.length; j++)
      if (paramArrayOfFloat[j] >= f)
      {
        i = j;
        f = paramArrayOfFloat[j];
      }
    return i;
  }

  public static double semiringLogSum(double[] paramArrayOfDouble)
  {
    int i = argmax(paramArrayOfDouble);
    double d1 = paramArrayOfDouble[i];
    double d2 = 0.0D;
    for (int j = 0; j < paramArrayOfDouble.length; j++)
      if (j != i)
        d2 += Math.exp(paramArrayOfDouble[j] - d1);
    return d1 + Math.log1p(d2);
  }

  public static float semiringLogSum(float[] paramArrayOfFloat)
  {
    int i = argmax(paramArrayOfFloat);
    float f1 = paramArrayOfFloat[i];
    float f2 = 0.0F;
    for (int j = 0; j < paramArrayOfFloat.length; j++)
      if (j != i)
        f2 = (float)(f2 + Math.exp(paramArrayOfFloat[j] - f1));
    return f1 + (float)Math.log1p(f2);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.research.handwriting.base.MathTools
 * JD-Core Version:    0.6.2
 */