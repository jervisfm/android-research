package com.google.research.handwriting.base;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class RecognitionResult
  implements Iterable<ScoredCandidate>, Parcelable
{
  public static final RecognitionResult CANCELED = new RecognitionResult("cancelled", Collections.unmodifiableList(new ArrayList()));
  public static final Parcelable.Creator<RecognitionResult> CREATOR = new Parcelable.Creator()
  {
    public RecognitionResult createFromParcel(Parcel paramAnonymousParcel)
    {
      return new RecognitionResult(paramAnonymousParcel);
    }

    public RecognitionResult[] newArray(int paramAnonymousInt)
    {
      return new RecognitionResult[paramAnonymousInt];
    }
  };
  private String debugInfo = null;
  private final String id;
  private final List<ScoredCandidate> scoredResults;

  public RecognitionResult(Parcel paramParcel)
  {
    this.id = paramParcel.readString();
    this.debugInfo = paramParcel.readString();
    this.scoredResults = new ArrayList();
    paramParcel.readTypedList(this.scoredResults, ScoredCandidate.CREATOR);
  }

  public RecognitionResult(String paramString)
  {
    this.id = paramString;
    this.scoredResults = new ArrayList();
  }

  public RecognitionResult(String paramString, List<ScoredCandidate> paramList)
  {
    this.id = paramString;
    this.scoredResults = paramList;
  }

  public void add(ScoredCandidate paramScoredCandidate)
  {
    this.scoredResults.add(paramScoredCandidate);
  }

  public int describeContents()
  {
    return 0;
  }

  public ScoredCandidate get(int paramInt)
  {
    return (ScoredCandidate)this.scoredResults.get(paramInt);
  }

  public String getDebugInfo()
  {
    return this.debugInfo;
  }

  public String getId()
  {
    return this.id;
  }

  public Iterator<ScoredCandidate> iterator()
  {
    return this.scoredResults.iterator();
  }

  public int numResult()
  {
    return this.scoredResults.size();
  }

  public void setDebugInfo(String paramString)
  {
    this.debugInfo = paramString;
  }

  public void sort()
  {
    Collections.sort(this.scoredResults, new Comparator()
    {
      public int compare(ScoredCandidate paramAnonymousScoredCandidate1, ScoredCandidate paramAnonymousScoredCandidate2)
      {
        return (int)Math.signum(paramAnonymousScoredCandidate2.score - paramAnonymousScoredCandidate1.score);
      }
    });
  }

  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramParcel.writeString(this.id);
    if (this.debugInfo == null)
      paramParcel.writeString("");
    while (true)
    {
      paramParcel.writeTypedList(this.scoredResults);
      return;
      paramParcel.writeString(this.debugInfo);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.research.handwriting.base.RecognitionResult
 * JD-Core Version:    0.6.2
 */