package com.google.research.handwriting.base;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.util.ArrayList;
import java.util.List;

public class ScoredCandidate
  implements Parcelable
{
  public static final Parcelable.Creator<ScoredCandidate> CREATOR = new Parcelable.Creator()
  {
    public ScoredCandidate createFromParcel(Parcel paramAnonymousParcel)
    {
      return new ScoredCandidate(paramAnonymousParcel);
    }

    public ScoredCandidate[] newArray(int paramAnonymousInt)
    {
      return new ScoredCandidate[paramAnonymousInt];
    }
  };
  public final List<String> completions;
  public final float score;
  public final String word;

  public ScoredCandidate(Parcel paramParcel)
  {
    this.word = paramParcel.readString();
    this.score = paramParcel.readFloat();
    this.completions = new ArrayList();
    paramParcel.readStringList(this.completions);
  }

  public ScoredCandidate(String paramString, float paramFloat)
  {
    this(paramString, paramFloat, new ArrayList(0));
  }

  public ScoredCandidate(String paramString, float paramFloat, List<String> paramList)
  {
    this.word = paramString;
    this.score = paramFloat;
    this.completions = paramList;
  }

  public int describeContents()
  {
    return 0;
  }

  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramParcel.writeString(this.word);
    paramParcel.writeFloat(this.score);
    paramParcel.writeStringList(this.completions);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.research.handwriting.base.ScoredCandidate
 * JD-Core Version:    0.6.2
 */