package com.google.research.handwriting.base;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.util.ArrayList;
import java.util.Iterator;

public class Stroke
  implements Parcelable, Iterable<Point>
{
  public static final Parcelable.Creator<Stroke> CREATOR = new Parcelable.Creator()
  {
    public Stroke createFromParcel(Parcel paramAnonymousParcel)
    {
      return new Stroke(paramAnonymousParcel);
    }

    public Stroke[] newArray(int paramAnonymousInt)
    {
      return new Stroke[paramAnonymousInt];
    }
  };
  private final boolean penDown;
  private final ArrayList<Point> points = new ArrayList();

  public Stroke()
  {
    this(true);
  }

  public Stroke(int paramInt)
  {
    this(true);
    this.points.ensureCapacity(paramInt);
  }

  public Stroke(int paramInt, boolean paramBoolean)
  {
    this.penDown = paramBoolean;
    this.points.ensureCapacity(paramInt);
  }

  public Stroke(Parcel paramParcel)
  {
    this.penDown = paramParcel.createBooleanArray()[0];
    paramParcel.readTypedList(this.points, Point.CREATOR);
  }

  public Stroke(Stroke paramStroke)
  {
    this(paramStroke.penDown);
    this.points.addAll(paramStroke.points);
  }

  public Stroke(boolean paramBoolean)
  {
    this.penDown = paramBoolean;
  }

  public void addPoint(float paramFloat1, float paramFloat2)
  {
    this.points.add(new Point(paramFloat1, paramFloat2));
  }

  public void addPoint(float paramFloat1, float paramFloat2, float paramFloat3)
  {
    this.points.add(new Point(paramFloat1, paramFloat2, paramFloat3));
  }

  public void addPoint(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
  {
    this.points.add(new Point(paramFloat1, paramFloat2, paramFloat3, paramFloat4));
  }

  public void addPoint(Point paramPoint)
  {
    this.points.add(paramPoint);
  }

  public int describeContents()
  {
    return 0;
  }

  public Point get(int paramInt)
  {
    return (Point)this.points.get(paramInt);
  }

  public Point getFirst()
  {
    return (Point)this.points.get(0);
  }

  public Point getLast()
  {
    return (Point)this.points.get(-1 + this.points.size());
  }

  public boolean isEmpty()
  {
    return this.points.isEmpty();
  }

  public boolean isPenDown()
  {
    return this.penDown;
  }

  public Iterator<Point> iterator()
  {
    return this.points.iterator();
  }

  public void removeFirst()
  {
    this.points.remove(0);
  }

  public void removeLast()
  {
    this.points.remove(-1 + this.points.size());
  }

  public void setPoint(int paramInt, Point paramPoint)
  {
    this.points.set(paramInt, paramPoint);
  }

  public int size()
  {
    return this.points.size();
  }

  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    boolean[] arrayOfBoolean = new boolean[1];
    arrayOfBoolean[0] = this.penDown;
    paramParcel.writeBooleanArray(arrayOfBoolean);
    paramParcel.writeTypedList(this.points);
  }

  public static final class Point
    implements Parcelable
  {
    public static final Parcelable.Creator<Point> CREATOR = new Parcelable.Creator()
    {
      public Stroke.Point createFromParcel(Parcel paramAnonymousParcel)
      {
        return new Stroke.Point(paramAnonymousParcel);
      }

      public Stroke.Point[] newArray(int paramAnonymousInt)
      {
        return new Stroke.Point[paramAnonymousInt];
      }
    };
    public final float p;
    public final float t;
    public final float x;
    public final float y;

    public Point(float paramFloat1, float paramFloat2)
    {
      this(paramFloat1, paramFloat2, -1.0F, 0.0F);
    }

    public Point(float paramFloat1, float paramFloat2, float paramFloat3)
    {
      this(paramFloat1, paramFloat2, paramFloat3, 0.0F);
    }

    public Point(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
    {
      this.x = paramFloat1;
      this.y = paramFloat2;
      this.t = paramFloat3;
      this.p = paramFloat4;
    }

    public Point(Parcel paramParcel)
    {
      this.x = paramParcel.readFloat();
      this.y = paramParcel.readFloat();
      this.t = paramParcel.readFloat();
      this.p = paramParcel.readFloat();
    }

    public int describeContents()
    {
      return 0;
    }

    public String toString()
    {
      Object[] arrayOfObject = new Object[2];
      arrayOfObject[0] = Float.valueOf(this.x);
      arrayOfObject[1] = Float.valueOf(this.y);
      return String.format("(%s, %s)", arrayOfObject);
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
      paramParcel.writeFloat(this.x);
      paramParcel.writeFloat(this.y);
      paramParcel.writeFloat(this.t);
      paramParcel.writeFloat(this.p);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.research.handwriting.base.Stroke
 * JD-Core Version:    0.6.2
 */