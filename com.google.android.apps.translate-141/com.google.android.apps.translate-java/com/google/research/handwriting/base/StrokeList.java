package com.google.research.handwriting.base;

import android.os.Build.VERSION;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONException;

public class StrokeList extends ArrayList<Stroke>
  implements Parcelable
{
  public static final Parcelable.Creator<StrokeList> CREATOR = new Parcelable.Creator()
  {
    public StrokeList createFromParcel(Parcel paramAnonymousParcel)
    {
      return new StrokeList(paramAnonymousParcel);
    }

    public StrokeList[] newArray(int paramAnonymousInt)
    {
      return new StrokeList[paramAnonymousInt];
    }
  };
  public static final StrokeList EMPTY = new StrokeList();
  private static final String FORMAT_STR = "%.4e";
  private static final long serialVersionUID = 1L;
  private final LinkedList<Cut> cuts = new LinkedList();
  private boolean enablePreSpace = false;
  private int inputType = 0;
  private String postContext = "";
  private String preContext = "";
  private int writingGuideHeight;
  private int writingGuideWidth;

  public StrokeList()
  {
  }

  public StrokeList(int paramInt)
  {
    super(paramInt);
  }

  public StrokeList(Parcel paramParcel)
  {
    this();
    this.writingGuideWidth = paramParcel.readInt();
    this.writingGuideHeight = paramParcel.readInt();
    this.inputType = paramParcel.readInt();
    if (paramParcel.readByte() > 0);
    for (boolean bool = true; ; bool = false)
    {
      this.enablePreSpace = bool;
      this.preContext = paramParcel.readString();
      this.postContext = paramParcel.readString();
      paramParcel.readTypedList(this, Stroke.CREATOR);
      return;
    }
  }

  public StrokeList(StrokeList paramStrokeList, boolean paramBoolean)
  {
    super(paramStrokeList.size());
    this.writingGuideHeight = paramStrokeList.writingGuideHeight;
    this.writingGuideWidth = paramStrokeList.writingGuideWidth;
    this.inputType = paramStrokeList.inputType;
    this.enablePreSpace = paramStrokeList.enablePreSpace;
    if (paramBoolean)
    {
      this.preContext = new String(paramStrokeList.preContext);
      this.postContext = new String(paramStrokeList.postContext);
      Iterator localIterator = paramStrokeList.iterator();
      while (localIterator.hasNext())
        add(new Stroke((Stroke)localIterator.next()));
    }
    this.preContext = paramStrokeList.preContext;
    this.postContext = paramStrokeList.postContext;
    addAll(paramStrokeList);
  }

  public static double log2(double paramDouble)
  {
    return Math.log(paramDouble) / Math.log(2.0D);
  }

  private static double roundToSignificantDigits(double paramDouble)
  {
    if (Build.VERSION.SDK_INT > 6)
    {
      Locale localLocale = Locale.ENGLISH;
      Object[] arrayOfObject = new Object[1];
      arrayOfObject[0] = Double.valueOf(paramDouble);
      paramDouble = Double.parseDouble(String.format(localLocale, "%.4e", arrayOfObject));
    }
    return paramDouble;
  }

  public void addCut(int paramInt1, int paramInt2)
  {
    this.cuts.add(new Cut(paramInt1, paramInt2));
  }

  public void addCut(int paramInt1, int paramInt2, float paramFloat, StrokeList.Cut.CutType paramCutType)
  {
    this.cuts.add(new Cut(paramInt1, paramInt2, paramFloat, paramCutType));
  }

  public JSONArray asJsonArray()
    throws JSONException
  {
    JSONArray localJSONArray1 = new JSONArray();
    float f = -1.0F;
    Iterator localIterator1 = iterator();
    while (localIterator1.hasNext())
    {
      Stroke localStroke = (Stroke)localIterator1.next();
      JSONArray localJSONArray2 = new JSONArray();
      JSONArray localJSONArray3 = new JSONArray();
      Iterator localIterator2 = localStroke.iterator();
      while (localIterator2.hasNext())
        localJSONArray3.put(roundToSignificantDigits(((Stroke.Point)localIterator2.next()).x));
      localJSONArray2.put(localJSONArray3);
      JSONArray localJSONArray4 = new JSONArray();
      Iterator localIterator3 = localStroke.iterator();
      while (localIterator3.hasNext())
        localJSONArray4.put(roundToSignificantDigits(((Stroke.Point)localIterator3.next()).y));
      localJSONArray2.put(localJSONArray4);
      JSONArray localJSONArray5 = new JSONArray();
      Iterator localIterator4 = localStroke.iterator();
      while (localIterator4.hasNext())
      {
        Stroke.Point localPoint = (Stroke.Point)localIterator4.next();
        if (f == -1.0F)
          f = localPoint.t;
        localJSONArray5.put(localPoint.t - f);
      }
      localJSONArray2.put(localJSONArray5);
      JSONArray localJSONArray6 = new JSONArray();
      Iterator localIterator5 = localStroke.iterator();
      while (localIterator5.hasNext())
        localJSONArray6.put(roundToSignificantDigits(((Stroke.Point)localIterator5.next()).p));
      localJSONArray2.put(localJSONArray6);
      localJSONArray1.put(localJSONArray2);
    }
    return localJSONArray1;
  }

  public LinkedList<Cut> cuts()
  {
    return this.cuts;
  }

  public int describeContents()
  {
    return 0;
  }

  public boolean getEnablePreSpace()
  {
    return this.enablePreSpace;
  }

  public Stroke getFirst()
  {
    return (Stroke)get(0);
  }

  public int getInputType()
  {
    return this.inputType;
  }

  public Stroke getLast()
  {
    return (Stroke)get(-1 + size());
  }

  public String getPostContext()
  {
    return this.postContext;
  }

  public String getPreContext()
  {
    return this.preContext;
  }

  public int getWritingGuideHeight()
  {
    return this.writingGuideHeight;
  }

  public int getWritingGuideWidth()
  {
    return this.writingGuideWidth;
  }

  public void parseFromJsonArray(JSONArray paramJSONArray)
    throws JSONException
  {
    int i = paramJSONArray.length();
    clear();
    for (int j = 0; j < i; j++)
    {
      JSONArray localJSONArray1 = paramJSONArray.getJSONArray(j);
      Stroke localStroke = new Stroke();
      JSONArray localJSONArray2 = localJSONArray1.getJSONArray(0);
      JSONArray localJSONArray3 = localJSONArray1.getJSONArray(1);
      int k = localJSONArray1.length();
      JSONArray localJSONArray4 = null;
      JSONArray localJSONArray5 = null;
      if (k > 2)
      {
        localJSONArray5 = localJSONArray1.getJSONArray(2);
        int n = localJSONArray1.length();
        localJSONArray4 = null;
        if (n > 3)
          localJSONArray4 = localJSONArray1.getJSONArray(3);
      }
      int m = 0;
      if (m < localJSONArray2.length())
      {
        if (localJSONArray5 != null)
          if (localJSONArray4 != null)
            localStroke.addPoint((float)localJSONArray2.getDouble(m), (float)localJSONArray3.getDouble(m), (float)localJSONArray5.getDouble(m), (float)localJSONArray4.getDouble(m));
        while (true)
        {
          m++;
          break;
          localStroke.addPoint((float)localJSONArray2.getDouble(m), (float)localJSONArray3.getDouble(m), (float)localJSONArray5.getDouble(m), 1.0F);
          continue;
          localStroke.addPoint((float)localJSONArray2.getDouble(m), (float)localJSONArray3.getDouble(m), 0.0F, 1.0F);
        }
      }
      add(localStroke);
    }
  }

  public void setContext(String paramString1, String paramString2)
  {
    this.preContext = paramString1;
    this.postContext = paramString2;
  }

  public void setCuts(LinkedList<Cut> paramLinkedList)
  {
    this.cuts.clear();
    this.cuts.addAll(paramLinkedList);
  }

  public void setEnablePreSpace(boolean paramBoolean)
  {
    this.enablePreSpace = paramBoolean;
  }

  public void setInputType(int paramInt)
  {
    this.inputType = paramInt;
  }

  public void setWritingGuide(int paramInt1, int paramInt2)
  {
    this.writingGuideWidth = paramInt1;
    this.writingGuideHeight = paramInt2;
  }

  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    paramParcel.writeInt(this.writingGuideWidth);
    paramParcel.writeInt(this.writingGuideHeight);
    paramParcel.writeInt(this.inputType);
    if (this.enablePreSpace);
    for (int i = 1; ; i = 0)
    {
      paramParcel.writeByte((byte)i);
      paramParcel.writeString(this.preContext);
      paramParcel.writeString(this.postContext);
      paramParcel.writeTypedList(this);
      return;
    }
  }

  public static class Cut
  {
    public final float cost;
    public final int point;
    public final int stroke;
    public final CutType type;

    public Cut(int paramInt1, int paramInt2)
    {
      this.stroke = paramInt1;
      this.point = paramInt2;
      this.cost = 0.0F;
      this.type = CutType.STROKES;
    }

    public Cut(int paramInt1, int paramInt2, float paramFloat, CutType paramCutType)
    {
      this.stroke = paramInt1;
      this.point = paramInt2;
      this.cost = paramFloat;
      this.type = paramCutType;
    }

    public static enum CutType
    {
      static
      {
        MINIMUM = new CutType("MINIMUM", 1);
        MAXIMUM = new CutType("MAXIMUM", 2);
        BETWEEN = new CutType("BETWEEN", 3);
        VELOCITY = new CutType("VELOCITY", 4);
        RENDERED = new CutType("RENDERED", 5);
        MONOTONIC = new CutType("MONOTONIC", 6);
        CutType[] arrayOfCutType = new CutType[7];
        arrayOfCutType[0] = STROKES;
        arrayOfCutType[1] = MINIMUM;
        arrayOfCutType[2] = MAXIMUM;
        arrayOfCutType[3] = BETWEEN;
        arrayOfCutType[4] = VELOCITY;
        arrayOfCutType[5] = RENDERED;
        arrayOfCutType[6] = MONOTONIC;
      }
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.research.handwriting.base.StrokeList
 * JD-Core Version:    0.6.2
 */