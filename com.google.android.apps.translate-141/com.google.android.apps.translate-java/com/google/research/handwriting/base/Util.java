package com.google.research.handwriting.base;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.Resources;
import android.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class Util
{
  public static final String MODEL_DIRECTORY = "/sdcard/hwrime/";
  private static final String TAG = "Util";

  public static String maybeMakeFilenameFromUrl(String paramString)
  {
    if (!paramString.startsWith("http"))
      return paramString;
    return "/sdcard/hwrime/" + paramString.hashCode();
  }

  public static InputStream openStream(String paramString, Context paramContext)
    throws IOException
  {
    Log.i("Util", "Opening stream " + paramString);
    if (paramContext != null)
    {
      int i = paramContext.getResources().getIdentifier(paramString, null, paramContext.getPackageName());
      Log.i("Util", "Got resource id " + i);
      if (i > 0)
        return paramContext.getResources().openRawResource(i);
    }
    Log.i("Util", "Trying to open as file " + paramString);
    new File(paramString);
    return new FileInputStream(paramString);
  }

  public static StreamAndSize openStreamWithSize(String paramString, Context paramContext)
    throws IOException
  {
    Log.i("Util", "Opening stream " + paramString);
    if (paramContext != null)
    {
      int i = paramContext.getResources().getIdentifier(paramString, null, paramContext.getPackageName());
      Log.i("Util", "Got resource id " + i);
      if (i > 0)
        try
        {
          AssetFileDescriptor localAssetFileDescriptor = paramContext.getResources().openRawResourceFd(i);
          StreamAndSize localStreamAndSize = new StreamAndSize(localAssetFileDescriptor.createInputStream(), localAssetFileDescriptor.getLength());
          return localStreamAndSize;
        }
        catch (Exception localException)
        {
          Log.e("Util", "Caught an exception when trying to open as FD. Trying to open as input stream.", localException);
          return new StreamAndSize(paramContext.getResources().openRawResource(i), -1L);
        }
    }
    Log.i("Util", "Trying to open as file " + paramString);
    File localFile = new File(paramString);
    return new StreamAndSize(new FileInputStream(paramString), localFile.length());
  }

  public static class StreamAndSize
  {
    public final InputStream is;
    public final long size;

    public StreamAndSize(InputStream paramInputStream, long paramLong)
    {
      this.is = paramInputStream;
      this.size = paramLong;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.research.handwriting.base.Util
 * JD-Core Version:    0.6.2
 */