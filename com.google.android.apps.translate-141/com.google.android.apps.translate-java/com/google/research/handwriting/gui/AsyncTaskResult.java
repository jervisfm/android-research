package com.google.research.handwriting.gui;

public class AsyncTaskResult<T>
{
  public final Exception exception;
  public final T result;

  public AsyncTaskResult(Exception paramException)
  {
    this.exception = paramException;
    this.result = null;
  }

  public AsyncTaskResult(T paramT)
  {
    this.result = paramT;
    this.exception = null;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.research.handwriting.gui.AsyncTaskResult
 * JD-Core Version:    0.6.2
 */