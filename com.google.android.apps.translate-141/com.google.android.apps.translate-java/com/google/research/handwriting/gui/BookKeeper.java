package com.google.research.handwriting.gui;

import android.util.Log;
import com.google.research.handwriting.base.LogV;
import java.text.StringCharacterIterator;
import java.util.Iterator;
import java.util.LinkedList;

public class BookKeeper
{
  private static final String TAG = BookKeeper.class.getSimpleName();
  private final LinkedList<RecognizedCharacter> book = new LinkedList();

  private LinkedList<RecognizedCharacter> decomposeIntoCharacters(String paramString, RecognitionRequestWithResults paramRecognitionRequestWithResults)
  {
    LinkedList localLinkedList = new LinkedList();
    StringCharacterIterator localStringCharacterIterator = new StringCharacterIterator(paramString);
    int i = localStringCharacterIterator.first();
    if (i != 65535)
    {
      if (Character.isWhitespace(i))
        localLinkedList.add(new RecognizedCharacter(i, RecognitionRequestWithResults.EMPTY));
      while (true)
      {
        char c = localStringCharacterIterator.next();
        break;
        localLinkedList.add(new RecognizedCharacter(c, paramRecognitionRequestWithResults));
      }
    }
    return localLinkedList;
  }

  private void deleteFromBook(int paramInt1, int paramInt2)
  {
    String str = TAG + ".deleteFromBook";
    Object[] arrayOfObject = new Object[3];
    arrayOfObject[0] = Integer.valueOf(paramInt1);
    arrayOfObject[1] = Integer.valueOf(paramInt2);
    arrayOfObject[2] = contentsAsString();
    LogV.i(2, str, String.format("startPosition=%s stopPosition=%s contents=%s", arrayOfObject));
    for (int i = paramInt1; i < paramInt2; i++)
      if ((paramInt1 >= 0) && (paramInt1 < this.book.size()))
        this.book.remove(paramInt1);
  }

  public void clear()
  {
    this.book.clear();
  }

  public String contentsAsString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    Iterator localIterator = this.book.iterator();
    while (localIterator.hasNext())
      localStringBuilder.append(((RecognizedCharacter)localIterator.next()).character);
    return localStringBuilder.toString();
  }

  public RecognitionRequestWithResults getBookkeepingEntry(int paramInt)
  {
    LogV.i(3, TAG, "getBookkeepingEntry: " + paramInt);
    if ((paramInt >= 0) && (this.book.size() > paramInt))
      return ((RecognizedCharacter)this.book.get(paramInt)).request;
    return RecognitionRequestWithResults.EMPTY;
  }

  public void init(CharSequence paramCharSequence)
  {
    clear();
    update(0, 0, paramCharSequence.toString(), ImeHandwritingRecognizer.RecognitionResult.EMPTY);
  }

  public void logState()
  {
    Log.i(TAG, "Full content of the field:\n'" + contentsAsString() + "'");
  }

  public int size()
  {
    return this.book.size();
  }

  public void update(int paramInt1, int paramInt2, String paramString, ImeHandwritingRecognizer.RecognitionResult paramRecognitionResult)
  {
    if (paramInt1 < 0);
    try
    {
      LogV.i(1, TAG, "Strange: startposition < 0");
      paramInt1 = 0;
      LogV.i(1, TAG + ".update", "startPosition=" + paramInt1 + " stopPosition=" + paramInt2 + " text=" + paramString);
      LinkedList localLinkedList = decomposeIntoCharacters(paramString, new RecognitionRequestWithResults(paramRecognitionResult, paramString));
      deleteFromBook(paramInt1, paramInt2);
      if (paramInt1 > this.book.size())
      {
        LogV.i(1, TAG, "Strange: startposition > book.size()");
        this.book.addAll(localLinkedList);
      }
      while (true)
      {
        if (LogV.logVerbosity() >= 1)
          logState();
        return;
        this.book.addAll(paramInt1, localLinkedList);
      }
    }
    finally
    {
    }
  }

  public static class RecognitionRequestWithResults
  {
    public static final RecognitionRequestWithResults EMPTY = new RecognitionRequestWithResults(ImeHandwritingRecognizer.RecognitionResult.EMPTY, "");
    public final ImeHandwritingRecognizer.RecognitionResult recognitionResult;
    public final String selectedWord;

    public RecognitionRequestWithResults(ImeHandwritingRecognizer.RecognitionResult paramRecognitionResult, String paramString)
    {
      this.recognitionResult = paramRecognitionResult;
      this.selectedWord = paramString;
    }

    public String toString()
    {
      return "RecognitionResult: " + this.recognitionResult.toString() + " selectedWord: " + this.selectedWord;
    }
  }

  public static class RecognizedCharacter
  {
    final char character;
    final BookKeeper.RecognitionRequestWithResults request;

    public RecognizedCharacter(char paramChar, BookKeeper.RecognitionRequestWithResults paramRecognitionRequestWithResults)
    {
      this.character = paramChar;
      this.request = paramRecognitionRequestWithResults;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.research.handwriting.gui.BookKeeper
 * JD-Core Version:    0.6.2
 */