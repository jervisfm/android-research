package com.google.research.handwriting.gui;

import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.PopupWindow;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.TextView;
import com.google.research.handwriting.base.LogV;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

public class CandidateView extends LinearLayout
  implements View.OnClickListener
{
  private static final int MAX_SUGGESTIONS = 32;
  private final int mCandidatePadding;
  private final int mColorCompletionDivider;
  private final int mColorCompletionNormal;
  private final int mColorFirstCandidate;
  private final int mColorIncorrect;
  private final int mColorNonSelectable;
  private final int mColorNormal;
  private final int mColorSelected;
  private boolean mCompletion = false;
  private SuggestedWords mCompletions = SuggestedWords.EMPTY;
  private final Context mContext;
  private final int mDividerWidth;
  private CandidateViewHandler.OnPickSuggestionListener mListener;
  HorizontalScrollView mScrollView;
  private boolean mShowCompletionListBelowCandidates = false;
  private SuggestedWords mSuggestions = SuggestedWords.EMPTY;
  private final ArrayList<View> mWords = new ArrayList();
  private String selectedWord;

  public CandidateView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    this.mContext = paramContext;
    Resources localResources = paramContext.getResources();
    this.mColorNormal = localResources.getColor(R.color.candidate_normal);
    this.mColorSelected = localResources.getColor(R.color.candidate_selected);
    this.mColorIncorrect = localResources.getColor(R.color.candidate_incorrect);
    this.mColorNonSelectable = localResources.getColor(R.color.candidate_not_selectable);
    this.mColorFirstCandidate = localResources.getColor(R.color.candidate_first);
    this.mColorCompletionNormal = localResources.getColor(R.color.completion_normal);
    this.mColorCompletionDivider = localResources.getColor(R.color.completion_divider);
    this.mDividerWidth = localResources.getDimensionPixelSize(R.dimen.completion_divider_width);
    this.mCandidatePadding = localResources.getDimensionPixelSize(R.dimen.candidate_padding);
    LayoutInflater localLayoutInflater = LayoutInflater.from(paramContext);
    int i = 0;
    if (i < 32)
    {
      View localView = localLayoutInflater.inflate(R.layout.candidate, null);
      TextView localTextView = (TextView)localView.findViewById(R.id.candidate_word);
      localTextView.setHeight(100);
      setMinimumHeight(100);
      localTextView.setTag(Integer.valueOf(i));
      localTextView.setOnClickListener(this);
      ImageView localImageView = (ImageView)localView.findViewById(R.id.candidate_divider);
      if (i == 0);
      for (int j = 8; ; j = 0)
      {
        localImageView.setVisibility(j);
        this.mWords.add(localView);
        i++;
        break;
      }
    }
    scrollTo(0, getScrollY());
  }

  private void updateSuggestions(String paramString)
  {
    clear();
    HashMap localHashMap = new HashMap();
    int i = 0;
    if (i < this.mCompletions.size())
    {
      String str1 = this.mCompletions.getWord(i).toString();
      String str2 = this.mCompletions.getExtra(i).getInfo().toString();
      LogV.i(2, "CandidateView.updateSuggestions", "Completion = " + str1 + " base = " + str2);
      if (localHashMap.containsKey(str2))
        ((LinkedList)localHashMap.get(str2)).add(str1);
      while (true)
      {
        i++;
        break;
        LinkedList localLinkedList = new LinkedList();
        localLinkedList.add(str1);
        localHashMap.put(str2, localLinkedList);
      }
    }
    int j = this.mSuggestions.size();
    if (j > 31)
    {
      LogV.i(1, "CandidateView", "Got more results than I can display. Cutting to 31.");
      j = 31;
    }
    int k = 0;
    while (k < j)
    {
      CharSequence localCharSequence = this.mSuggestions.getWord(k);
      if (localCharSequence == null)
      {
        k++;
      }
      else
      {
        View localView = (View)this.mWords.get(k);
        TextView localTextView = (TextView)localView.findViewById(R.id.candidate_word);
        ((TextView)localView.findViewById(R.id.candidate_extra_info)).setVisibility(8);
        localTextView.setTextColor(this.mColorNormal);
        if ((k == 0) && (!this.mCompletion))
          localTextView.setTextColor(this.mColorFirstCandidate);
        label302: int m;
        int n;
        if (paramString.contentEquals(localCharSequence))
        {
          localTextView.setTextColor(this.mColorSelected);
          if ((!this.mSuggestions.clickable()) && (!this.mCompletion))
            localTextView.setTextColor(this.mColorNonSelectable);
          m = localTextView.getPaddingLeft();
          n = localTextView.getPaddingRight();
          if (!localHashMap.containsKey(localCharSequence.toString()))
            break label474;
          localTextView.setLongClickable(true);
          localTextView.setOnLongClickListener(new ShowCompletionsOnLongClickListener((LinkedList)localHashMap.get(localCharSequence.toString()), localTextView));
          localTextView.setBackgroundResource(R.drawable.btn_candidate_with_ellipsis);
        }
        while (true)
        {
          localTextView.setPadding(m, 0, n, 0);
          localTextView.setText(localCharSequence);
          localTextView.setClickable(this.mSuggestions.clickable());
          addView(localView);
          break;
          if ((!"∅[incorrect]".contentEquals(localCharSequence.toString())) && (!"∅[no recognition results]".contentEquals(localCharSequence.toString())))
            break label302;
          localTextView.setTextColor(this.mColorIncorrect);
          break label302;
          label474: localTextView.setLongClickable(false);
          localTextView.setBackgroundResource(R.drawable.btn_candidate);
        }
      }
    }
    scrollTo(0, getScrollY());
    requestLayout();
  }

  public void clear()
  {
    removeAllViews();
  }

  public void onClick(View paramView)
  {
    int i = ((Integer)paramView.getTag()).intValue();
    CharSequence localCharSequence = this.mSuggestions.getWord(i);
    this.mListener.onPickSuggestion(i, localCharSequence, this.mCompletion);
  }

  public void onDetachedFromWindow()
  {
    super.onDetachedFromWindow();
  }

  public void setCompletionListLocationBelowCandidates(boolean paramBoolean)
  {
    this.mShowCompletionListBelowCandidates = paramBoolean;
  }

  public void setListener(CandidateViewHandler.OnPickSuggestionListener paramOnPickSuggestionListener, boolean paramBoolean)
  {
    this.mListener = paramOnPickSuggestionListener;
    this.mCompletion = paramBoolean;
  }

  public void setSelectedWord(String paramString)
  {
    this.selectedWord = paramString;
  }

  public void setSuggestions(SuggestedWords paramSuggestedWords1, SuggestedWords paramSuggestedWords2)
  {
    this.selectedWord = "";
    this.mSuggestions = paramSuggestedWords1;
    this.mCompletions = paramSuggestedWords2;
    updateSuggestions(this.selectedWord);
  }

  private class ShowCompletionsOnLongClickListener
    implements View.OnLongClickListener
  {
    private final LinkedList<String> completions;
    private final TextView textview;

    public ShowCompletionsOnLongClickListener(TextView arg2)
    {
      Object localObject1;
      this.completions = localObject1;
      Object localObject2;
      this.textview = localObject2;
    }

    public boolean onLongClick(View paramView)
    {
      LogV.i(2, "ShowCompletionsOnLongClickListener", "Long clicked with completions");
      final PopupWindow localPopupWindow = new PopupWindow(CandidateView.this.mContext);
      LinearLayoutThatRepositionsItsParentPopupDependingOnItsSize localLinearLayoutThatRepositionsItsParentPopupDependingOnItsSize = new LinearLayoutThatRepositionsItsParentPopupDependingOnItsSize(CandidateView.this.mContext);
      localLinearLayoutThatRepositionsItsParentPopupDependingOnItsSize.setPopup(localPopupWindow);
      localLinearLayoutThatRepositionsItsParentPopupDependingOnItsSize.setAnchor(this.textview);
      localLinearLayoutThatRepositionsItsParentPopupDependingOnItsSize.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
      localLinearLayoutThatRepositionsItsParentPopupDependingOnItsSize.setBackgroundColor(0);
      localLinearLayoutThatRepositionsItsParentPopupDependingOnItsSize.setOrientation(1);
      localPopupWindow.setOutsideTouchable(true);
      localPopupWindow.setOnDismissListener(new PopupWindow.OnDismissListener()
      {
        public void onDismiss()
        {
          LogV.i(2, "OnDismiss", "hit");
        }
      });
      int i = 0;
      int j = this.textview.getWidth();
      Iterator localIterator = this.completions.iterator();
      while (true)
      {
        if (localIterator.hasNext())
        {
          String str = (String)localIterator.next();
          Button localButton = new Button(CandidateView.this.mContext);
          localButton.setText(str);
          localButton.setBackgroundResource(R.drawable.btn_candidate);
          localButton.setTextColor(CandidateView.this.mColorCompletionNormal);
          localButton.setMinimumWidth(j);
          localButton.setPadding(CandidateView.this.mCandidatePadding, 0, CandidateView.this.mCandidatePadding, 0);
          localButton.setOnClickListener(new View.OnClickListener()
          {
            public void onClick(View paramAnonymousView)
            {
              CandidateView.this.mListener.onPickSuggestion(100, ((Button)paramAnonymousView).getText(), true);
              localPopupWindow.dismiss();
            }
          });
          localLinearLayoutThatRepositionsItsParentPopupDependingOnItsSize.addView(localButton);
          i++;
          if (i <= 3);
        }
        else
        {
          localPopupWindow.setContentView(localLinearLayoutThatRepositionsItsParentPopupDependingOnItsSize);
          localPopupWindow.setWidth(-2);
          localPopupWindow.setHeight(-2);
          localPopupWindow.showAsDropDown(this.textview, 0, 0);
          return true;
        }
        TextView localTextView = new TextView(CandidateView.this.mContext);
        localTextView.setBackgroundColor(CandidateView.this.mColorCompletionDivider);
        localTextView.setHeight(CandidateView.this.mDividerWidth);
        localLinearLayoutThatRepositionsItsParentPopupDependingOnItsSize.addView(localTextView);
      }
    }

    class LinearLayoutThatRepositionsItsParentPopupDependingOnItsSize extends LinearLayout
    {
      View anchor;
      PopupWindow popup;

      public LinearLayoutThatRepositionsItsParentPopupDependingOnItsSize(Context arg2)
      {
        super();
      }

      protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
      {
        if (CandidateView.this.mShowCompletionListBelowCandidates)
          return;
        this.popup.update(this.anchor, 0, -4 + (-this.anchor.getHeight() - paramInt2), this.popup.getWidth(), this.popup.getHeight());
        super.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
      }

      public void setAnchor(View paramView)
      {
        this.anchor = paramView;
      }

      public void setPopup(PopupWindow paramPopupWindow)
      {
        this.popup = paramPopupWindow;
      }
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.research.handwriting.gui.CandidateView
 * JD-Core Version:    0.6.2
 */