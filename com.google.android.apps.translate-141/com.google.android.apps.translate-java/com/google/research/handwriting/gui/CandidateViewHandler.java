package com.google.research.handwriting.gui;

import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.HorizontalScrollView;

public class CandidateViewHandler
{
  public static final String NONE_SELECTED = "";
  private final Button askTheCloudButton;
  private final HorizontalScrollView candidateScrollView;
  private final CandidateView candidatesView;
  private final View container;

  public CandidateViewHandler(LayoutInflater paramLayoutInflater)
  {
    this.container = paramLayoutInflater.inflate(R.layout.candidates, null);
    this.candidatesView = ((CandidateView)this.container.findViewById(R.id.candidates));
    this.candidateScrollView = ((HorizontalScrollView)this.container.findViewById(R.id.candidates_scroll_view));
    this.askTheCloudButton = ((Button)this.container.findViewById(R.id.ask_the_cloud));
  }

  public void clear()
  {
    this.candidatesView.clear();
  }

  public View getContainerView()
  {
    return this.container;
  }

  public void scrollCandidatesToBeginning()
  {
    this.candidateScrollView.scrollTo(0, 0);
  }

  public void setAskTheCloudAction(View.OnClickListener paramOnClickListener)
  {
    if (this.askTheCloudButton != null)
      this.askTheCloudButton.setOnClickListener(paramOnClickListener);
  }

  public void setCandidateSuggestions(SuggestedWords paramSuggestedWords1, SuggestedWords paramSuggestedWords2)
  {
    this.candidatesView.setSuggestions(paramSuggestedWords1, paramSuggestedWords2);
  }

  public void setCompletionListLocationBelowCandidates(boolean paramBoolean)
  {
    this.candidatesView.setCompletionListLocationBelowCandidates(paramBoolean);
  }

  public void setListener(OnPickSuggestionListener paramOnPickSuggestionListener)
  {
    this.candidatesView.setListener(paramOnPickSuggestionListener, false);
  }

  public void setSelectedWord(String paramString)
  {
    this.candidatesView.setSelectedWord(paramString);
  }

  public void showAskTheCloudButton(boolean paramBoolean)
  {
    if (this.askTheCloudButton != null)
    {
      if (paramBoolean)
        this.askTheCloudButton.setVisibility(0);
    }
    else
      return;
    this.askTheCloudButton.setVisibility(8);
  }

  public static abstract interface OnPickSuggestionListener
  {
    public abstract void onPickSuggestion(int paramInt, CharSequence paramCharSequence, boolean paramBoolean);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.research.handwriting.gui.CandidateViewHandler
 * JD-Core Version:    0.6.2
 */