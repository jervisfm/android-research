package com.google.research.handwriting.gui;

import android.graphics.Paint;
import android.view.inputmethod.InputConnection;
import com.google.research.handwriting.base.BoundingBox;
import com.google.research.handwriting.base.LogV;
import com.google.research.handwriting.base.StrokeList;
import java.util.Iterator;
import java.util.LinkedList;

public class ContinuousWritingHandler
  implements ContinuousWritingManager.ContinuousWritingCallbacks
{
  public static final String PLACEHOLDER = "…";
  private static final String TAG = ContinuousWritingHandler.class.getSimpleName();
  public boolean mContinuousWritingInternalEditing = false;
  private HandwritingOverlayView mHandwritingOverlayView;
  private Object mIcLock;
  private InputConnection mInputConnection;
  private UIHandler mUIHandler;
  public final LinkedList<RecoResult> recoQueue = new LinkedList();

  public void addToQueue(int paramInt, StrokeList paramStrokeList, boolean paramBoolean, char paramChar)
  {
    try
    {
      LogV.i(3, TAG + "CWH.addtoQueue", "addToQueue: id = " + paramInt + " start " + this.recoQueue.size() + " nstrokes =" + paramStrokeList.size());
      this.recoQueue.add(new RecoResult(paramInt, ImeHandwritingRecognizer.RecognitionResult.EMPTY, paramStrokeList, paramBoolean, paramChar));
      LogV.i(3, TAG + "CWH.addtoQueue", "addToQueue: id = " + paramInt + " done " + this.recoQueue.size() + " nstrokes =" + paramStrokeList.size());
      return;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  public void drawRecoQueue()
  {
    float f = 0.0F;
    Paint localPaint = new Paint();
    localPaint.setColor(-1);
    if (this.mHandwritingOverlayView != null)
    {
      this.mHandwritingOverlayView.clearRegion(0.0F, 0.0F, this.mHandwritingOverlayView.getWidth(), 0.2F * this.mHandwritingOverlayView.getHeight());
      Iterator localIterator = this.recoQueue.iterator();
      while (localIterator.hasNext())
      {
        StrokeList localStrokeList = ((RecoResult)localIterator.next()).strokes;
        BoundingBox localBoundingBox = new BoundingBox(localStrokeList);
        this.mHandwritingOverlayView.drawStrokeList(localStrokeList, 0.2F, f - localBoundingBox.left(), 5.0F - localBoundingBox.top(), localPaint);
        f += 5.0F + (localBoundingBox.right() - localBoundingBox.left());
      }
    }
  }

  public boolean isInternalContinuousWritingEditing()
  {
    return this.mContinuousWritingInternalEditing;
  }

  public int numRecognitionsInBackground()
  {
    try
    {
      int i = this.recoQueue.size();
      return i;
    }
    finally
    {
      localObject = finally;
      throw localObject;
    }
  }

  // ERROR //
  public void onRecognitionDone(int paramInt, ImeHandwritingRecognizer.RecognitionResult paramRecognitionResult, StrokeList paramStrokeList)
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield 164	com/google/research/handwriting/gui/ContinuousWritingHandler:mIcLock	Ljava/lang/Object;
    //   6: astore 5
    //   8: aload 5
    //   10: monitorenter
    //   11: iconst_2
    //   12: new 48	java/lang/StringBuilder
    //   15: dup
    //   16: invokespecial 49	java/lang/StringBuilder:<init>	()V
    //   19: getstatic 34	com/google/research/handwriting/gui/ContinuousWritingHandler:TAG	Ljava/lang/String;
    //   22: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   25: ldc 166
    //   27: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   30: invokevirtual 58	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   33: new 48	java/lang/StringBuilder
    //   36: dup
    //   37: invokespecial 49	java/lang/StringBuilder:<init>	()V
    //   40: ldc 168
    //   42: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   45: iload_1
    //   46: invokevirtual 63	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   49: ldc 170
    //   51: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   54: aload_0
    //   55: getfield 44	com/google/research/handwriting/gui/ContinuousWritingHandler:recoQueue	Ljava/util/LinkedList;
    //   58: invokevirtual 69	java/util/LinkedList:size	()I
    //   61: invokevirtual 63	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   64: ldc 172
    //   66: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   69: aload_3
    //   70: invokevirtual 74	com/google/research/handwriting/base/StrokeList:size	()I
    //   73: invokevirtual 63	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   76: invokevirtual 58	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   79: invokestatic 80	com/google/research/handwriting/base/LogV:i	(ILjava/lang/String;Ljava/lang/String;)V
    //   82: aload_0
    //   83: getfield 44	com/google/research/handwriting/gui/ContinuousWritingHandler:recoQueue	Ljava/util/LinkedList;
    //   86: invokevirtual 175	java/util/LinkedList:isEmpty	()Z
    //   89: ifeq +36 -> 125
    //   92: iconst_3
    //   93: new 48	java/lang/StringBuilder
    //   96: dup
    //   97: invokespecial 49	java/lang/StringBuilder:<init>	()V
    //   100: getstatic 34	com/google/research/handwriting/gui/ContinuousWritingHandler:TAG	Ljava/lang/String;
    //   103: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   106: ldc 166
    //   108: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   111: invokevirtual 58	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   114: ldc 177
    //   116: invokestatic 80	com/google/research/handwriting/base/LogV:i	(ILjava/lang/String;Ljava/lang/String;)V
    //   119: aload 5
    //   121: monitorexit
    //   122: aload_0
    //   123: monitorexit
    //   124: return
    //   125: aload_0
    //   126: getfield 44	com/google/research/handwriting/gui/ContinuousWritingHandler:recoQueue	Ljava/util/LinkedList;
    //   129: invokevirtual 124	java/util/LinkedList:iterator	()Ljava/util/Iterator;
    //   132: astore 7
    //   134: aload 7
    //   136: invokeinterface 130 1 0
    //   141: ifeq +110 -> 251
    //   144: aload 7
    //   146: invokeinterface 134 1 0
    //   151: checkcast 82	com/google/research/handwriting/gui/ContinuousWritingHandler$RecoResult
    //   154: astore 26
    //   156: aload 26
    //   158: getfield 181	com/google/research/handwriting/gui/ContinuousWritingHandler$RecoResult:id	I
    //   161: iload_1
    //   162: if_icmpne -28 -> 134
    //   165: aload_2
    //   166: ifnull +73 -> 239
    //   169: aload_2
    //   170: invokevirtual 185	com/google/research/handwriting/gui/ImeHandwritingRecognizer$RecognitionResult:getWords	()Lcom/google/research/handwriting/gui/SuggestedWords;
    //   173: invokevirtual 188	com/google/research/handwriting/gui/SuggestedWords:size	()I
    //   176: ifle +63 -> 239
    //   179: iconst_2
    //   180: new 48	java/lang/StringBuilder
    //   183: dup
    //   184: invokespecial 49	java/lang/StringBuilder:<init>	()V
    //   187: getstatic 34	com/google/research/handwriting/gui/ContinuousWritingHandler:TAG	Ljava/lang/String;
    //   190: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   193: ldc 166
    //   195: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   198: invokevirtual 58	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   201: new 48	java/lang/StringBuilder
    //   204: dup
    //   205: invokespecial 49	java/lang/StringBuilder:<init>	()V
    //   208: ldc 190
    //   210: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   213: iload_1
    //   214: invokevirtual 63	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   217: ldc 192
    //   219: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   222: aload_2
    //   223: invokevirtual 185	com/google/research/handwriting/gui/ImeHandwritingRecognizer$RecognitionResult:getWords	()Lcom/google/research/handwriting/gui/SuggestedWords;
    //   226: iconst_0
    //   227: invokevirtual 196	com/google/research/handwriting/gui/SuggestedWords:getWord	(I)Ljava/lang/CharSequence;
    //   230: invokevirtual 199	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   233: invokevirtual 58	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   236: invokestatic 80	com/google/research/handwriting/base/LogV:i	(ILjava/lang/String;Ljava/lang/String;)V
    //   239: aload 26
    //   241: aload_2
    //   242: putfield 202	com/google/research/handwriting/gui/ContinuousWritingHandler$RecoResult:result	Lcom/google/research/handwriting/gui/ImeHandwritingRecognizer$RecognitionResult;
    //   245: aload 26
    //   247: aload_3
    //   248: putfield 138	com/google/research/handwriting/gui/ContinuousWritingHandler$RecoResult:strokes	Lcom/google/research/handwriting/base/StrokeList;
    //   251: aload_0
    //   252: iconst_1
    //   253: invokevirtual 206	com/google/research/handwriting/gui/ContinuousWritingHandler:setContinuousWritingInternalEditing	(Z)V
    //   256: ldc 208
    //   258: astore 8
    //   260: aload_0
    //   261: getfield 44	com/google/research/handwriting/gui/ContinuousWritingHandler:recoQueue	Ljava/util/LinkedList;
    //   264: invokevirtual 211	java/util/LinkedList:getFirst	()Ljava/lang/Object;
    //   267: checkcast 82	com/google/research/handwriting/gui/ContinuousWritingHandler$RecoResult
    //   270: astore 9
    //   272: aload_0
    //   273: getfield 213	com/google/research/handwriting/gui/ContinuousWritingHandler:mUIHandler	Lcom/google/research/handwriting/gui/UIHandler;
    //   276: invokevirtual 219	com/google/research/handwriting/gui/UIHandler:getBookKeeper	()Lcom/google/research/handwriting/gui/BookKeeper;
    //   279: astore 10
    //   281: aload 9
    //   283: getfield 202	com/google/research/handwriting/gui/ContinuousWritingHandler$RecoResult:result	Lcom/google/research/handwriting/gui/ImeHandwritingRecognizer$RecognitionResult;
    //   286: getstatic 88	com/google/research/handwriting/gui/ImeHandwritingRecognizer$RecognitionResult:EMPTY	Lcom/google/research/handwriting/gui/ImeHandwritingRecognizer$RecognitionResult;
    //   289: if_acmpeq +246 -> 535
    //   292: aload 9
    //   294: getfield 202	com/google/research/handwriting/gui/ContinuousWritingHandler$RecoResult:result	Lcom/google/research/handwriting/gui/ImeHandwritingRecognizer$RecognitionResult;
    //   297: ifnull +192 -> 489
    //   300: aload 9
    //   302: getfield 202	com/google/research/handwriting/gui/ContinuousWritingHandler$RecoResult:result	Lcom/google/research/handwriting/gui/ImeHandwritingRecognizer$RecognitionResult;
    //   305: invokevirtual 185	com/google/research/handwriting/gui/ImeHandwritingRecognizer$RecognitionResult:getWords	()Lcom/google/research/handwriting/gui/SuggestedWords;
    //   308: invokevirtual 188	com/google/research/handwriting/gui/SuggestedWords:size	()I
    //   311: ifle +178 -> 489
    //   314: aload 10
    //   316: invokevirtual 224	com/google/research/handwriting/gui/BookKeeper:contentsAsString	()Ljava/lang/String;
    //   319: ldc 10
    //   321: invokevirtual 230	java/lang/String:indexOf	(Ljava/lang/String;)I
    //   324: istore 23
    //   326: aload 9
    //   328: getfield 202	com/google/research/handwriting/gui/ContinuousWritingHandler$RecoResult:result	Lcom/google/research/handwriting/gui/ImeHandwritingRecognizer$RecognitionResult;
    //   331: invokevirtual 185	com/google/research/handwriting/gui/ImeHandwritingRecognizer$RecognitionResult:getWords	()Lcom/google/research/handwriting/gui/SuggestedWords;
    //   334: iconst_0
    //   335: invokevirtual 196	com/google/research/handwriting/gui/SuggestedWords:getWord	(I)Ljava/lang/CharSequence;
    //   338: invokevirtual 231	java/lang/Object:toString	()Ljava/lang/String;
    //   341: astore 24
    //   343: aload 9
    //   345: invokevirtual 234	com/google/research/handwriting/gui/ContinuousWritingHandler$RecoResult:isUseAppendChar	()Z
    //   348: ifeq +611 -> 959
    //   351: new 48	java/lang/StringBuilder
    //   354: dup
    //   355: invokespecial 49	java/lang/StringBuilder:<init>	()V
    //   358: ldc 208
    //   360: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   363: aload 9
    //   365: getfield 238	com/google/research/handwriting/gui/ContinuousWritingHandler$RecoResult:appendChar	C
    //   368: invokevirtual 241	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
    //   371: invokevirtual 58	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   374: astore 25
    //   376: iconst_3
    //   377: getstatic 34	com/google/research/handwriting/gui/ContinuousWritingHandler:TAG	Ljava/lang/String;
    //   380: new 48	java/lang/StringBuilder
    //   383: dup
    //   384: invokespecial 49	java/lang/StringBuilder:<init>	()V
    //   387: ldc 243
    //   389: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   392: iload 23
    //   394: invokevirtual 63	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   397: ldc 245
    //   399: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   402: aload 24
    //   404: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   407: ldc 247
    //   409: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   412: aload 25
    //   414: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   417: ldc 249
    //   419: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   422: invokevirtual 58	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   425: invokestatic 80	com/google/research/handwriting/base/LogV:i	(ILjava/lang/String;Ljava/lang/String;)V
    //   428: aload 10
    //   430: iload 23
    //   432: iload 23
    //   434: new 48	java/lang/StringBuilder
    //   437: dup
    //   438: invokespecial 49	java/lang/StringBuilder:<init>	()V
    //   441: aload 24
    //   443: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   446: aload 25
    //   448: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   451: invokevirtual 58	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   454: aload 9
    //   456: getfield 202	com/google/research/handwriting/gui/ContinuousWritingHandler$RecoResult:result	Lcom/google/research/handwriting/gui/ImeHandwritingRecognizer$RecognitionResult;
    //   459: invokevirtual 253	com/google/research/handwriting/gui/BookKeeper:update	(IILjava/lang/String;Lcom/google/research/handwriting/gui/ImeHandwritingRecognizer$RecognitionResult;)V
    //   462: new 48	java/lang/StringBuilder
    //   465: dup
    //   466: invokespecial 49	java/lang/StringBuilder:<init>	()V
    //   469: aload 8
    //   471: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   474: aload 24
    //   476: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   479: aload 25
    //   481: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   484: invokevirtual 58	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   487: astore 8
    //   489: aload_0
    //   490: getfield 44	com/google/research/handwriting/gui/ContinuousWritingHandler:recoQueue	Ljava/util/LinkedList;
    //   493: invokevirtual 256	java/util/LinkedList:removeFirst	()Ljava/lang/Object;
    //   496: pop
    //   497: aload_0
    //   498: getfield 44	com/google/research/handwriting/gui/ContinuousWritingHandler:recoQueue	Ljava/util/LinkedList;
    //   501: invokevirtual 175	java/util/LinkedList:isEmpty	()Z
    //   504: ifeq +462 -> 966
    //   507: iconst_2
    //   508: new 48	java/lang/StringBuilder
    //   511: dup
    //   512: invokespecial 49	java/lang/StringBuilder:<init>	()V
    //   515: getstatic 34	com/google/research/handwriting/gui/ContinuousWritingHandler:TAG	Ljava/lang/String;
    //   518: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   521: ldc 166
    //   523: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   526: invokevirtual 58	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   529: ldc_w 258
    //   532: invokestatic 80	com/google/research/handwriting/base/LogV:i	(ILjava/lang/String;Ljava/lang/String;)V
    //   535: aload_0
    //   536: getfield 44	com/google/research/handwriting/gui/ContinuousWritingHandler:recoQueue	Ljava/util/LinkedList;
    //   539: invokevirtual 175	java/util/LinkedList:isEmpty	()Z
    //   542: ifne +439 -> 981
    //   545: new 48	java/lang/StringBuilder
    //   548: dup
    //   549: invokespecial 49	java/lang/StringBuilder:<init>	()V
    //   552: aload 8
    //   554: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   557: ldc 10
    //   559: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   562: invokevirtual 58	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   565: astore 8
    //   567: aload_0
    //   568: getfield 260	com/google/research/handwriting/gui/ContinuousWritingHandler:mInputConnection	Landroid/view/inputmethod/InputConnection;
    //   571: invokeinterface 265 1 0
    //   576: pop
    //   577: aload_0
    //   578: getfield 260	com/google/research/handwriting/gui/ContinuousWritingHandler:mInputConnection	Landroid/view/inputmethod/InputConnection;
    //   581: new 267	android/view/inputmethod/ExtractedTextRequest
    //   584: dup
    //   585: invokespecial 268	android/view/inputmethod/ExtractedTextRequest:<init>	()V
    //   588: iconst_0
    //   589: invokeinterface 272 3 0
    //   594: astore 13
    //   596: aload 13
    //   598: ifnull +454 -> 1052
    //   601: aload 13
    //   603: getfield 278	android/view/inputmethod/ExtractedText:text	Ljava/lang/CharSequence;
    //   606: ifnull +446 -> 1052
    //   609: aload 13
    //   611: getfield 278	android/view/inputmethod/ExtractedText:text	Ljava/lang/CharSequence;
    //   614: invokevirtual 231	java/lang/Object:toString	()Ljava/lang/String;
    //   617: astore 14
    //   619: iconst_3
    //   620: new 48	java/lang/StringBuilder
    //   623: dup
    //   624: invokespecial 49	java/lang/StringBuilder:<init>	()V
    //   627: getstatic 34	com/google/research/handwriting/gui/ContinuousWritingHandler:TAG	Ljava/lang/String;
    //   630: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   633: ldc 166
    //   635: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   638: invokevirtual 58	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   641: new 48	java/lang/StringBuilder
    //   644: dup
    //   645: invokespecial 49	java/lang/StringBuilder:<init>	()V
    //   648: ldc_w 280
    //   651: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   654: aload 14
    //   656: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   659: ldc_w 282
    //   662: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   665: aload 8
    //   667: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   670: invokevirtual 58	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   673: invokestatic 80	com/google/research/handwriting/base/LogV:i	(ILjava/lang/String;Ljava/lang/String;)V
    //   676: aload 14
    //   678: ldc 10
    //   680: invokevirtual 230	java/lang/String:indexOf	(Ljava/lang/String;)I
    //   683: istore 15
    //   685: iconst_3
    //   686: new 48	java/lang/StringBuilder
    //   689: dup
    //   690: invokespecial 49	java/lang/StringBuilder:<init>	()V
    //   693: getstatic 34	com/google/research/handwriting/gui/ContinuousWritingHandler:TAG	Ljava/lang/String;
    //   696: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   699: ldc 166
    //   701: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   704: invokevirtual 58	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   707: new 48	java/lang/StringBuilder
    //   710: dup
    //   711: invokespecial 49	java/lang/StringBuilder:<init>	()V
    //   714: ldc 243
    //   716: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   719: iload 15
    //   721: invokevirtual 63	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   724: invokevirtual 58	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   727: invokestatic 80	com/google/research/handwriting/base/LogV:i	(ILjava/lang/String;Ljava/lang/String;)V
    //   730: iload 15
    //   732: iflt +327 -> 1059
    //   735: aload_0
    //   736: getfield 260	com/google/research/handwriting/gui/ContinuousWritingHandler:mInputConnection	Landroid/view/inputmethod/InputConnection;
    //   739: iload 15
    //   741: iload 15
    //   743: ldc 10
    //   745: invokevirtual 285	java/lang/String:length	()I
    //   748: iadd
    //   749: invokeinterface 289 3 0
    //   754: istore 16
    //   756: iconst_3
    //   757: new 48	java/lang/StringBuilder
    //   760: dup
    //   761: invokespecial 49	java/lang/StringBuilder:<init>	()V
    //   764: getstatic 34	com/google/research/handwriting/gui/ContinuousWritingHandler:TAG	Ljava/lang/String;
    //   767: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   770: ldc 166
    //   772: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   775: invokevirtual 58	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   778: new 48	java/lang/StringBuilder
    //   781: dup
    //   782: invokespecial 49	java/lang/StringBuilder:<init>	()V
    //   785: ldc_w 291
    //   788: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   791: iload 16
    //   793: invokevirtual 294	java/lang/StringBuilder:append	(Z)Ljava/lang/StringBuilder;
    //   796: invokevirtual 58	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   799: invokestatic 80	com/google/research/handwriting/base/LogV:i	(ILjava/lang/String;Ljava/lang/String;)V
    //   802: aload_0
    //   803: getfield 260	com/google/research/handwriting/gui/ContinuousWritingHandler:mInputConnection	Landroid/view/inputmethod/InputConnection;
    //   806: aload 8
    //   808: iconst_1
    //   809: invokeinterface 298 3 0
    //   814: pop
    //   815: aload_0
    //   816: getfield 260	com/google/research/handwriting/gui/ContinuousWritingHandler:mInputConnection	Landroid/view/inputmethod/InputConnection;
    //   819: invokeinterface 301 1 0
    //   824: pop
    //   825: new 226	java/lang/String
    //   828: dup
    //   829: aload 14
    //   831: invokespecial 304	java/lang/String:<init>	(Ljava/lang/String;)V
    //   834: ldc 10
    //   836: aload 8
    //   838: invokevirtual 308	java/lang/String:replace	(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    //   841: astore 19
    //   843: aload_0
    //   844: getfield 260	com/google/research/handwriting/gui/ContinuousWritingHandler:mInputConnection	Landroid/view/inputmethod/InputConnection;
    //   847: new 267	android/view/inputmethod/ExtractedTextRequest
    //   850: dup
    //   851: invokespecial 268	android/view/inputmethod/ExtractedTextRequest:<init>	()V
    //   854: iconst_0
    //   855: invokeinterface 272 3 0
    //   860: astore 20
    //   862: iconst_3
    //   863: getstatic 34	com/google/research/handwriting/gui/ContinuousWritingHandler:TAG	Ljava/lang/String;
    //   866: new 48	java/lang/StringBuilder
    //   869: dup
    //   870: invokespecial 49	java/lang/StringBuilder:<init>	()V
    //   873: ldc_w 310
    //   876: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   879: aload 20
    //   881: getfield 278	android/view/inputmethod/ExtractedText:text	Ljava/lang/CharSequence;
    //   884: invokevirtual 231	java/lang/Object:toString	()Ljava/lang/String;
    //   887: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   890: ldc_w 312
    //   893: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   896: aload 19
    //   898: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   901: invokevirtual 58	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   904: invokestatic 80	com/google/research/handwriting/base/LogV:i	(ILjava/lang/String;Ljava/lang/String;)V
    //   907: aload_0
    //   908: getfield 213	com/google/research/handwriting/gui/ContinuousWritingHandler:mUIHandler	Lcom/google/research/handwriting/gui/UIHandler;
    //   911: invokevirtual 315	com/google/research/handwriting/gui/UIHandler:updateTranslatedText	()V
    //   914: aload 5
    //   916: monitorexit
    //   917: aload_0
    //   918: invokevirtual 317	com/google/research/handwriting/gui/ContinuousWritingHandler:drawRecoQueue	()V
    //   921: iconst_1
    //   922: new 48	java/lang/StringBuilder
    //   925: dup
    //   926: invokespecial 49	java/lang/StringBuilder:<init>	()V
    //   929: getstatic 34	com/google/research/handwriting/gui/ContinuousWritingHandler:TAG	Ljava/lang/String;
    //   932: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   935: ldc 166
    //   937: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   940: invokevirtual 58	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   943: ldc_w 319
    //   946: invokestatic 80	com/google/research/handwriting/base/LogV:i	(ILjava/lang/String;Ljava/lang/String;)V
    //   949: goto -827 -> 122
    //   952: astore 4
    //   954: aload_0
    //   955: monitorexit
    //   956: aload 4
    //   958: athrow
    //   959: ldc 208
    //   961: astore 25
    //   963: goto -587 -> 376
    //   966: aload_0
    //   967: getfield 44	com/google/research/handwriting/gui/ContinuousWritingHandler:recoQueue	Ljava/util/LinkedList;
    //   970: invokevirtual 211	java/util/LinkedList:getFirst	()Ljava/lang/Object;
    //   973: checkcast 82	com/google/research/handwriting/gui/ContinuousWritingHandler$RecoResult
    //   976: astore 9
    //   978: goto -697 -> 281
    //   981: aload 10
    //   983: invokevirtual 224	com/google/research/handwriting/gui/BookKeeper:contentsAsString	()Ljava/lang/String;
    //   986: ldc 10
    //   988: invokevirtual 230	java/lang/String:indexOf	(Ljava/lang/String;)I
    //   991: istore 11
    //   993: iconst_3
    //   994: getstatic 34	com/google/research/handwriting/gui/ContinuousWritingHandler:TAG	Ljava/lang/String;
    //   997: new 48	java/lang/StringBuilder
    //   1000: dup
    //   1001: invokespecial 49	java/lang/StringBuilder:<init>	()V
    //   1004: ldc_w 321
    //   1007: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1010: iload 11
    //   1012: invokevirtual 63	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   1015: invokevirtual 58	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1018: invokestatic 80	com/google/research/handwriting/base/LogV:i	(ILjava/lang/String;Ljava/lang/String;)V
    //   1021: aload 10
    //   1023: iload 11
    //   1025: iload 11
    //   1027: ldc 10
    //   1029: invokevirtual 285	java/lang/String:length	()I
    //   1032: iadd
    //   1033: ldc 208
    //   1035: getstatic 88	com/google/research/handwriting/gui/ImeHandwritingRecognizer$RecognitionResult:EMPTY	Lcom/google/research/handwriting/gui/ImeHandwritingRecognizer$RecognitionResult;
    //   1038: invokevirtual 253	com/google/research/handwriting/gui/BookKeeper:update	(IILjava/lang/String;Lcom/google/research/handwriting/gui/ImeHandwritingRecognizer$RecognitionResult;)V
    //   1041: goto -474 -> 567
    //   1044: astore 6
    //   1046: aload 5
    //   1048: monitorexit
    //   1049: aload 6
    //   1051: athrow
    //   1052: ldc 208
    //   1054: astore 14
    //   1056: goto -437 -> 619
    //   1059: new 48	java/lang/StringBuilder
    //   1062: dup
    //   1063: invokespecial 49	java/lang/StringBuilder:<init>	()V
    //   1066: getstatic 34	com/google/research/handwriting/gui/ContinuousWritingHandler:TAG	Ljava/lang/String;
    //   1069: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1072: ldc 166
    //   1074: invokevirtual 53	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1077: invokevirtual 58	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   1080: ldc_w 323
    //   1083: invokestatic 329	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
    //   1086: pop
    //   1087: goto -285 -> 802
    //
    // Exception table:
    //   from	to	target	type
    //   2	11	952	finally
    //   917	949	952	finally
    //   1049	1052	952	finally
    //   11	122	1044	finally
    //   125	134	1044	finally
    //   134	165	1044	finally
    //   169	239	1044	finally
    //   239	251	1044	finally
    //   251	256	1044	finally
    //   260	281	1044	finally
    //   281	376	1044	finally
    //   376	489	1044	finally
    //   489	535	1044	finally
    //   535	567	1044	finally
    //   567	596	1044	finally
    //   601	619	1044	finally
    //   619	730	1044	finally
    //   735	802	1044	finally
    //   802	917	1044	finally
    //   966	978	1044	finally
    //   981	1041	1044	finally
    //   1046	1049	1044	finally
    //   1059	1087	1044	finally
  }

  public void setContinuousWritingInternalEditing(boolean paramBoolean)
  {
    this.mContinuousWritingInternalEditing = paramBoolean;
  }

  public void setCurrentInputConnection(InputConnection paramInputConnection)
  {
    this.mInputConnection = paramInputConnection;
  }

  public void setHandwritingOverlayView(HandwritingOverlayView paramHandwritingOverlayView)
  {
    this.mHandwritingOverlayView = paramHandwritingOverlayView;
  }

  public void setIclock(Object paramObject)
  {
    this.mIcLock = paramObject;
  }

  public void setUIHandler(UIHandler paramUIHandler)
  {
    this.mUIHandler = paramUIHandler;
  }

  public class RecoResult
  {
    char appendChar;
    int id;
    ImeHandwritingRecognizer.RecognitionResult result;
    StrokeList strokes;
    boolean useAppendChar;

    public RecoResult(int paramRecognitionResult, ImeHandwritingRecognizer.RecognitionResult paramStrokeList, StrokeList paramBoolean, boolean paramChar, char arg6)
    {
      this.id = paramRecognitionResult;
      this.result = paramStrokeList;
      this.strokes = paramBoolean;
      this.useAppendChar = paramChar;
      char c;
      this.appendChar = c;
    }

    public boolean isUseAppendChar()
    {
      return this.useAppendChar;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.research.handwriting.gui.ContinuousWritingHandler
 * JD-Core Version:    0.6.2
 */