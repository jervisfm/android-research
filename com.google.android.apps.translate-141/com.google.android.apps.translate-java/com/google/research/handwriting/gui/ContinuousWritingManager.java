package com.google.research.handwriting.gui;

import android.os.AsyncTask;
import android.util.Log;
import com.google.research.handwriting.base.StrokeList;
import com.google.research.handwriting.networkrecognizer.CloudRecognizer;

public class ContinuousWritingManager
{
  private static final String FEEDBACK_ONKEY_CONT = "onkey-cont";
  public static final String PLACEHOLDER = "…";
  public static final String TAG = "ContinuousWritingManager";
  private ContinuousWritingCallbacks callbacks;
  protected int recoCounter = 0;
  ImeHandwritingRecognizer recognizer;

  public void setCallback(ContinuousWritingCallbacks paramContinuousWritingCallbacks)
  {
    this.callbacks = paramContinuousWritingCallbacks;
  }

  public void setRecognizer(ImeHandwritingRecognizer paramImeHandwritingRecognizer)
  {
    this.recognizer = paramImeHandwritingRecognizer;
  }

  public int startRecognition(StrokeList paramStrokeList)
  {
    int i = 0;
    while (true)
    {
      AsyncRecognitionTask localAsyncRecognitionTask;
      try
      {
        if (paramStrokeList.isEmpty())
        {
          Log.i("ContinuousWritingManager", "Gracefully answering empty reco request ;-)");
          return i;
        }
        if (paramStrokeList.getPreContext().contains("…"))
          paramStrokeList.setContext("", paramStrokeList.getPostContext());
        if (paramStrokeList.getPostContext().contains("…"))
          paramStrokeList.setContext(paramStrokeList.getPreContext(), "");
        i = this.recoCounter;
        this.recoCounter = (i + 1);
        localAsyncRecognitionTask = new AsyncRecognitionTask(i, paramStrokeList);
        if ((this.recognizer.recognizer() instanceof CloudRecognizer))
        {
          new HandwritingThreadPoolExecutor().execute(localAsyncRecognitionTask, new Object[0]);
          continue;
        }
      }
      finally
      {
      }
      localAsyncRecognitionTask.execute(new Object[0]);
    }
  }

  private class AsyncRecognitionTask extends AsyncTask<Object, Object, ImeHandwritingRecognizer.RecognitionResult>
  {
    private final int id;
    private final StrokeList strokesToRecognize;

    public AsyncRecognitionTask(int paramStrokeList, StrokeList arg3)
    {
      this.id = paramStrokeList;
      Object localObject;
      this.strokesToRecognize = localObject;
    }

    protected ImeHandwritingRecognizer.RecognitionResult doInBackground(Object[] paramArrayOfObject)
    {
      return ContinuousWritingManager.this.recognizer.recognize(this.strokesToRecognize);
    }

    public void onPostExecute(ImeHandwritingRecognizer.RecognitionResult paramRecognitionResult)
    {
      if (paramRecognitionResult != ImeHandwritingRecognizer.RecognitionResult.EMPTY)
        ContinuousWritingManager.this.recognizer.logFeedback(paramRecognitionResult.getWords().getWord(0).toString(), "onkey-cont", paramRecognitionResult, false, "");
      ContinuousWritingManager.this.callbacks.onRecognitionDone(this.id, paramRecognitionResult, this.strokesToRecognize);
    }
  }

  public static abstract interface ContinuousWritingCallbacks
  {
    public abstract void onRecognitionDone(int paramInt, ImeHandwritingRecognizer.RecognitionResult paramRecognitionResult, StrokeList paramStrokeList);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.research.handwriting.gui.ContinuousWritingManager
 * JD-Core Version:    0.6.2
 */