package com.google.research.handwriting.gui;

import com.google.research.handwriting.base.BoundingBox;
import com.google.research.handwriting.base.Stroke;
import com.google.research.handwriting.base.Stroke.Point;
import com.google.research.handwriting.base.StrokeList;
import java.util.Iterator;

public class GestureRecognizer
{
  private static final int BACKSPACE_MIN_WIDTH = 50;
  private static final float HORIZONTAL_GEST_MAX_HEIGHT_TO_WIDTH_RATIO = 0.3F;
  private static final int SPACE_MIN_WIDTH = 200;
  private boolean backspaceGestureEnabled = false;
  private boolean spaceGestureEnabled = false;

  private boolean isBackspaceGesture(Stroke paramStroke, StrokeList paramStrokeList)
  {
    if (!isHorizontalLine(paramStroke, true));
    BoundingBox localBoundingBox1;
    BoundingBox localBoundingBox2;
    do
    {
      float f;
      do
      {
        return false;
        localBoundingBox1 = new BoundingBox(paramStroke);
        f = localBoundingBox1.getWidth();
      }
      while ((localBoundingBox1.getHeight() > 0.3F * f) || (f < 50.0F));
      if (paramStrokeList.isEmpty())
        return true;
      localBoundingBox2 = new BoundingBox(paramStrokeList);
    }
    while ((localBoundingBox1.right() < localBoundingBox2.right()) || (localBoundingBox1.left() > localBoundingBox2.left()));
    return true;
  }

  private boolean isHorizontalLine(Stroke paramStroke, boolean paramBoolean)
  {
    Iterator localIterator = paramStroke.iterator();
    float f1 = 0.0F;
    if (paramBoolean);
    float f2;
    for (f1 = 3.4028235E+38F; localIterator.hasNext(); f1 = f2)
    {
      f2 = ((Stroke.Point)localIterator.next()).x;
      if ((paramBoolean) && (f1 < f2))
        return false;
      if ((!paramBoolean) && (f1 > f2))
        return false;
    }
    return true;
  }

  private boolean isSpaceGesture(Stroke paramStroke)
  {
    if (!isHorizontalLine(paramStroke, false));
    BoundingBox localBoundingBox;
    float f;
    do
    {
      return false;
      localBoundingBox = new BoundingBox(paramStroke);
      f = localBoundingBox.getWidth();
    }
    while ((localBoundingBox.getHeight() > 0.3F * f) || (f < 200.0F));
    return true;
  }

  public Gesture recognizeLastStrokeGesture(StrokeList paramStrokeList)
  {
    if (paramStrokeList.isEmpty())
      return Gesture.NOT_A_GESTURE;
    Stroke localStroke = (Stroke)paramStrokeList.get(-1 + paramStrokeList.size());
    paramStrokeList.remove(-1 + paramStrokeList.size());
    if ((this.backspaceGestureEnabled) && (isBackspaceGesture(localStroke, paramStrokeList)))
      return Gesture.GEST_BACKSPACE;
    if ((this.spaceGestureEnabled) && (isSpaceGesture(localStroke)))
      return Gesture.GEST_SPACE;
    paramStrokeList.add(localStroke);
    return Gesture.NOT_A_GESTURE;
  }

  void setBackspaceGestureEnabled(boolean paramBoolean)
  {
    this.backspaceGestureEnabled = paramBoolean;
  }

  void setSpaceGestureEnabled(boolean paramBoolean)
  {
    this.spaceGestureEnabled = paramBoolean;
  }

  public static enum Gesture
  {
    static
    {
      GEST_BACKSPACE = new Gesture("GEST_BACKSPACE", 1);
      GEST_SPACE = new Gesture("GEST_SPACE", 2);
      Gesture[] arrayOfGesture = new Gesture[3];
      arrayOfGesture[0] = NOT_A_GESTURE;
      arrayOfGesture[1] = GEST_BACKSPACE;
      arrayOfGesture[2] = GEST_SPACE;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.research.handwriting.gui.GestureRecognizer
 * JD-Core Version:    0.6.2
 */