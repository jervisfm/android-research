package com.google.research.handwriting.gui;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Cap;
import android.graphics.Paint.Join;
import android.graphics.Paint.Style;
import android.graphics.PorterDuff.Mode;
import android.graphics.RectF;
import android.graphics.Region.Op;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View.BaseSavedState;
import android.widget.RelativeLayout;
import com.google.research.handwriting.base.Stroke;
import com.google.research.handwriting.base.Stroke.Point;
import com.google.research.handwriting.base.StrokeList;
import java.util.Iterator;

public class HandwritingOverlayView extends RelativeLayout
{
  private static final int PEN_TOLERANCE = 3;
  private float lastX;
  private float lastY;
  private Bitmap mBuffer;
  private Canvas mCanvas;
  private final RectF mClipRect = new RectF();
  private HandwritingOverlayListener mListener;
  private final MultitouchGestureDetector mMultitouchDetector;
  private final Paint mPaintStroke = new Paint();
  private final Paint mPaintStrokeRecognized = new Paint();
  private final PressureNormalizer mPressureNormalizer = new PressureNormalizer();
  private int mStrokeColor = -1;
  private int mStrokeColorRecognized = -256;
  private Bitmap mUndoCopy;
  private boolean mUsingHistoryEvent = true;
  private final boolean mUsingMultitouchDetector = false;

  public HandwritingOverlayView(Context paramContext)
  {
    this(paramContext, null, 0);
  }

  public HandwritingOverlayView(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }

  public HandwritingOverlayView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    this.mStrokeColor = paramContext.getResources().getColor(R.color.strokeColor);
    this.mStrokeColorRecognized = paramContext.getResources().getColor(R.color.strokeColorRecognized);
    this.mMultitouchDetector = null;
    initializeDrawingStyles();
  }

  private void createBuffer()
  {
    int i = Math.max(getHeight(), 1);
    int j = Math.max(getWidth(), 1);
    if ((this.mBuffer == null) || (this.mBuffer.getWidth() != j) || (this.mBuffer.getHeight() != i))
    {
      this.mBuffer = Bitmap.createBitmap(j, i, Bitmap.Config.ARGB_8888);
      this.mCanvas = new Canvas(this.mBuffer);
    }
  }

  private void drawPenDown(float paramFloat1, float paramFloat2, float paramFloat3, Paint paramPaint)
  {
    if (this.mBuffer == null)
      createBuffer();
    RectF localRectF = this.mClipRect;
    localRectF.set(paramFloat1, paramFloat2, paramFloat1, paramFloat2);
    this.mPressureNormalizer.resetSmoothing();
    localRectF.inset(-this.mPressureNormalizer.getMaxRadius(), -this.mPressureNormalizer.getMaxRadius());
    this.mCanvas.clipRect(localRectF, Region.Op.REPLACE);
    paramPaint.setStyle(Paint.Style.FILL);
    this.mCanvas.drawCircle(paramFloat1, paramFloat2, this.mPressureNormalizer.getRadius(paramFloat3), paramPaint);
    invalidate((int)localRectF.left, (int)localRectF.top, 1 + (int)localRectF.right, 1 + (int)localRectF.bottom);
  }

  private void drawPenMove(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, Paint paramPaint)
  {
    if (this.mBuffer == null)
      createBuffer();
    RectF localRectF = this.mClipRect;
    Canvas localCanvas = this.mCanvas;
    localRectF.set(Math.min(paramFloat1, paramFloat3), Math.min(paramFloat2, paramFloat4), Math.max(paramFloat1, paramFloat3), Math.max(paramFloat2, paramFloat4));
    localRectF.inset(-this.mPressureNormalizer.getMaxRadius(), -this.mPressureNormalizer.getMaxRadius());
    paramPaint.setStrokeWidth(this.mPressureNormalizer.getStrokeWidth(paramFloat5));
    paramPaint.setStyle(Paint.Style.STROKE);
    localCanvas.clipRect(localRectF, Region.Op.REPLACE);
    localCanvas.drawLine(paramFloat3, paramFloat4, paramFloat1, paramFloat2, paramPaint);
    invalidate((int)localRectF.left, (int)localRectF.top, 1 + (int)localRectF.right, 1 + (int)localRectF.bottom);
  }

  private void drawPenUp(float paramFloat1, float paramFloat2, float paramFloat3, Paint paramPaint)
  {
    if (this.mBuffer == null)
      createBuffer();
    RectF localRectF = this.mClipRect;
    Canvas localCanvas = this.mCanvas;
    localRectF.set(paramFloat1, paramFloat2, paramFloat1, paramFloat2);
    localRectF.inset(-this.mPressureNormalizer.getMaxRadius(), -this.mPressureNormalizer.getMaxRadius());
    paramPaint.setStyle(Paint.Style.FILL);
    localCanvas.drawCircle(paramFloat1, paramFloat2, this.mPressureNormalizer.getRadius(paramFloat3), paramPaint);
    invalidate((int)localRectF.left, (int)localRectF.top, 1 + (int)localRectF.right, 1 + (int)localRectF.bottom);
  }

  private void drawStrokeListImpl(StrokeList paramStrokeList, float paramFloat1, float paramFloat2, float paramFloat3, Paint paramPaint, boolean paramBoolean)
  {
    Iterator localIterator1 = paramStrokeList.iterator();
    while (localIterator1.hasNext())
    {
      Iterator localIterator2 = ((Stroke)localIterator1.next()).iterator();
      boolean bool = localIterator2.hasNext();
      float f1 = 0.0F;
      float f2 = 0.0F;
      float f3 = 0.0F;
      Stroke.Point localPoint2;
      label109: Stroke.Point localPoint1;
      float f4;
      float f5;
      if (bool)
      {
        localPoint2 = (Stroke.Point)localIterator2.next();
        f1 = paramFloat1 * (paramFloat2 + localPoint2.x);
        f2 = paramFloat1 * (paramFloat3 + localPoint2.y);
        if (paramBoolean)
        {
          f3 = 1.0F;
          drawPenDown(f1, f2, f3, paramPaint);
        }
      }
      else
      {
        if (!localIterator2.hasNext())
          break label229;
        localPoint1 = (Stroke.Point)localIterator2.next();
        f4 = paramFloat1 * (paramFloat2 + localPoint1.x);
        f5 = paramFloat1 * (paramFloat3 + localPoint1.y);
        if (!paramBoolean)
          break label217;
      }
      label217: for (f3 = 1.0F; ; f3 = paramFloat1 * localPoint1.p)
      {
        if ((f4 != f1) || (f5 != f2))
          drawPenMove(f4, f5, f1, f2, f3, paramPaint);
        f1 = f4;
        f2 = f5;
        break label109;
        f3 = paramFloat1 * localPoint2.p;
        break;
      }
      label229: drawPenUp(f1, f2, f3, paramPaint);
    }
  }

  private void initializeDrawingStyles()
  {
    setWillNotDraw(false);
    this.mPaintStroke.setAntiAlias(true);
    this.mPaintStroke.setStyle(Paint.Style.STROKE);
    this.mPaintStroke.setStrokeCap(Paint.Cap.ROUND);
    this.mPaintStroke.setStrokeJoin(Paint.Join.ROUND);
    this.mPaintStroke.setColor(this.mStrokeColor);
    this.mPaintStroke.setStrokeWidth(this.mPressureNormalizer.defaultStrokeWidth());
    this.mPaintStrokeRecognized.setAntiAlias(true);
    this.mPaintStrokeRecognized.setStyle(Paint.Style.STROKE);
    this.mPaintStrokeRecognized.setStrokeCap(Paint.Cap.ROUND);
    this.mPaintStrokeRecognized.setStrokeJoin(Paint.Join.ROUND);
    this.mPaintStrokeRecognized.setColor(this.mStrokeColorRecognized);
    this.mPaintStrokeRecognized.setStrokeWidth(this.mPressureNormalizer.defaultStrokeWidth());
  }

  private void processTouchDown(float paramFloat1, float paramFloat2, long paramLong, float paramFloat3)
  {
    drawPenDown(paramFloat1, paramFloat2, paramFloat3, this.mPaintStroke);
    if (this.mListener != null)
      this.mListener.onPenDown(paramFloat1, paramFloat2, paramLong, paramFloat3);
    this.lastX = paramFloat1;
    this.lastY = paramFloat2;
  }

  private boolean processTouchEvent(MotionEvent paramMotionEvent)
  {
    switch (paramMotionEvent.getAction())
    {
    default:
      return false;
    case 0:
      processTouchDown(paramMotionEvent.getX(), paramMotionEvent.getY(), paramMotionEvent.getEventTime(), paramMotionEvent.getPressure());
      return true;
    case 2:
      int i = paramMotionEvent.getHistorySize();
      if ((this.mUsingHistoryEvent) && (i > 0))
        for (int j = 0; j < i; j++)
          processTouchMove(paramMotionEvent.getHistoricalX(j), paramMotionEvent.getHistoricalY(j), paramMotionEvent.getHistoricalEventTime(j), paramMotionEvent.getHistoricalPressure(j));
      processTouchMove(paramMotionEvent.getX(), paramMotionEvent.getY(), paramMotionEvent.getEventTime(), paramMotionEvent.getPressure());
      return true;
    case 1:
    case 3:
    }
    processTouchUp(paramMotionEvent.getX(), paramMotionEvent.getY(), paramMotionEvent.getEventTime(), paramMotionEvent.getPressure());
    return true;
  }

  private void processTouchMove(float paramFloat1, float paramFloat2, long paramLong, float paramFloat3)
  {
    if ((Math.abs(paramFloat1 - this.lastX) <= 3.0F) && (Math.abs(paramFloat2 - this.lastY) <= 3.0F))
      return;
    drawPenMove(paramFloat1, paramFloat2, this.lastX, this.lastY, paramFloat3, this.mPaintStroke);
    if (this.mListener != null)
      this.mListener.onPenMove(paramFloat1, paramFloat2, paramLong, paramFloat3);
    this.lastX = paramFloat1;
    this.lastY = paramFloat2;
  }

  public void cancelStroke()
  {
    int i = this.mUndoCopy.getWidth();
    int j = this.mUndoCopy.getHeight();
    int[] arrayOfInt = new int[i * j];
    this.mUndoCopy.getPixels(arrayOfInt, 0, i, 0, 0, i, j);
    this.mBuffer.setPixels(arrayOfInt, 0, i, 0, 0, i, j);
    this.mCanvas.drawBitmap(this.mUndoCopy, 0.0F, 0.0F, null);
    invalidate(0, 0, this.mBuffer.getWidth(), this.mBuffer.getHeight());
    this.mListener.cancelStroke();
  }

  public void clear()
  {
    createBuffer();
    this.mCanvas.clipRect(0.0F, 0.0F, this.mCanvas.getWidth(), this.mCanvas.getHeight(), Region.Op.REPLACE);
    this.mCanvas.drawColor(0, PorterDuff.Mode.CLEAR);
    invalidate();
  }

  public void clearRegion(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
  {
    Canvas localCanvas = this.mCanvas;
    if (localCanvas != null)
    {
      localCanvas.clipRect(paramFloat1, paramFloat2, paramFloat3, paramFloat4, Region.Op.REPLACE);
      localCanvas.drawColor(0, PorterDuff.Mode.CLEAR);
      invalidate((int)paramFloat1, (int)paramFloat2, (int)paramFloat3, (int)paramFloat4);
    }
  }

  public boolean dispatchTouchEvent(MotionEvent paramMotionEvent)
  {
    if (!isEnabled())
      return super.dispatchTouchEvent(paramMotionEvent);
    processTouchEvent(paramMotionEvent);
    paramMotionEvent.setAction(3);
    super.dispatchTouchEvent(paramMotionEvent);
    return true;
  }

  public void drawStrokeList(StrokeList paramStrokeList, float paramFloat1, float paramFloat2, float paramFloat3, Paint paramPaint)
  {
    drawStrokeListImpl(paramStrokeList, paramFloat1, paramFloat2, paramFloat3, paramPaint, false);
  }

  public void drawStrokeList(StrokeList paramStrokeList, boolean paramBoolean)
  {
    if (paramBoolean);
    for (Paint localPaint = this.mPaintStrokeRecognized; ; localPaint = this.mPaintStroke)
    {
      drawStrokeList(paramStrokeList, 1.0F, 0.0F, 0.0F, localPaint);
      return;
    }
  }

  public void drawStrokeListWithFixedWidth(StrokeList paramStrokeList, float paramFloat1, float paramFloat2, float paramFloat3, Paint paramPaint, float paramFloat4)
  {
    float f1 = this.mPressureNormalizer.getMinRadius();
    float f2 = this.mPressureNormalizer.getMaxRadius();
    setMinStrokeWidth(paramFloat4);
    setMaxStrokeWidth(paramFloat4);
    drawStrokeListImpl(paramStrokeList, paramFloat1, paramFloat2, paramFloat3, paramPaint, true);
    this.mPressureNormalizer.setMinRadius(f1);
    this.mPressureNormalizer.setMaxRadius(f2);
  }

  public int getStrokeColor()
  {
    return this.mStrokeColor;
  }

  protected void onDetachedFromWindow()
  {
    super.onDetachedFromWindow();
    this.mBuffer = null;
    this.mCanvas = null;
  }

  protected void onDraw(Canvas paramCanvas)
  {
    super.onDraw(paramCanvas);
    if (this.mBuffer == null)
      createBuffer();
    paramCanvas.drawBitmap(this.mBuffer, 0.0F, 0.0F, null);
  }

  public void onRestoreInstanceState(Parcelable paramParcelable)
  {
    if ((paramParcelable instanceof SavedState))
    {
      SavedState localSavedState = (SavedState)paramParcelable;
      super.onRestoreInstanceState(localSavedState.getSuperState());
      this.mStrokeColor = localSavedState.strokeColor;
      this.mStrokeColorRecognized = localSavedState.strokeColorRecognized;
      initializeDrawingStyles();
      return;
    }
    super.onRestoreInstanceState(paramParcelable);
  }

  public Parcelable onSaveInstanceState()
  {
    SavedState localSavedState = new SavedState(super.onSaveInstanceState());
    localSavedState.strokeColor = this.mStrokeColor;
    localSavedState.strokeColorRecognized = this.mStrokeColorRecognized;
    return localSavedState;
  }

  protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
    createBuffer();
    if (this.mListener != null)
      this.mListener.onSizeChanged(paramInt1, paramInt2);
  }

  public void processTouchUp(float paramFloat1, float paramFloat2, long paramLong, float paramFloat3)
  {
    if ((paramFloat1 != this.lastX) && (paramFloat2 != this.lastY))
      drawPenMove(paramFloat1, paramFloat2, this.lastX, this.lastY, paramFloat3, this.mPaintStroke);
    drawPenUp(paramFloat1, paramFloat2, paramFloat3, this.mPaintStroke);
    if (this.mListener != null)
      this.mListener.onPenUp(paramFloat1, paramFloat2, paramLong, paramFloat3);
    this.lastX = paramFloat1;
    this.lastY = paramFloat2;
  }

  public void setHandwritingOverlayListener(HandwritingOverlayListener paramHandwritingOverlayListener)
  {
    this.mListener = paramHandwritingOverlayListener;
  }

  public void setMaxStrokeWidth(float paramFloat)
  {
    this.mPressureNormalizer.setMaxRadius(paramFloat / 2.0F);
  }

  public void setMinStrokeWidth(float paramFloat)
  {
    this.mPressureNormalizer.setMinRadius(paramFloat / 2.0F);
  }

  public void setStrokeColor(int paramInt)
  {
    this.mStrokeColor = paramInt;
    this.mPaintStroke.setColor(this.mStrokeColor);
  }

  public void setUsingHistoryEvent(boolean paramBoolean)
  {
    this.mUsingHistoryEvent = paramBoolean;
  }

  public static abstract interface HandwritingOverlayListener
  {
    public abstract void cancelStroke();

    public abstract void onPenDown(float paramFloat1, float paramFloat2, long paramLong, float paramFloat3);

    public abstract void onPenMove(float paramFloat1, float paramFloat2, long paramLong, float paramFloat3);

    public abstract void onPenUp(float paramFloat1, float paramFloat2, long paramLong, float paramFloat3);

    public abstract void onSizeChanged(int paramInt1, int paramInt2);
  }

  public static class PressureNormalizer
  {
    private static final float ALPHA_OBSERVED = 0.4F;
    private static final float DOCUMENTED_MAX_PRESSURE = 1.0F;
    private static final float DOCUMENTED_MIN_PRESSURE = 0.0F;
    private static final float MAX_DELTA = 0.1F;
    private float deltaRadius = this.maxRadius - this.minRadius;
    private float lastNormalizedPressure = -1.0F;
    private float maxPressure = 1.0F;
    private float maxRadius = 1.0F;
    private float minPressure = 0.0F;
    private float minRadius = 0.0F;
    private float observedMaxPressure = 1.0F;
    private float observedMinPressure = 0.0F;

    private float getNormalizedPressure(float paramFloat)
    {
      if (paramFloat < this.observedMinPressure)
      {
        this.observedMinPressure = paramFloat;
        updateUsedExtrema();
      }
      if (paramFloat > this.observedMaxPressure)
      {
        this.observedMaxPressure = paramFloat;
        updateUsedExtrema();
      }
      return (paramFloat - this.minPressure) / (this.maxPressure - this.minPressure);
    }

    private float getNormalizedSmoothedPressure(float paramFloat)
    {
      float f1 = getNormalizedPressure(paramFloat);
      if (this.lastNormalizedPressure < 0.0F)
      {
        this.lastNormalizedPressure = f1;
        return f1;
      }
      float f2 = this.lastNormalizedPressure - f1;
      if (Math.abs(f2) > 0.1F)
        f1 = this.lastNormalizedPressure - 0.1F * Math.signum(f2);
      this.lastNormalizedPressure = f1;
      return f1;
    }

    private void updateUsedExtrema()
    {
      this.minPressure = (0.0F + 0.4F * this.observedMinPressure);
      this.maxPressure = (0.6F + 0.4F * this.observedMaxPressure);
    }

    public float defaultStrokeWidth()
    {
      return (this.minRadius + this.maxRadius) / 2.0F;
    }

    public float getMaxRadius()
    {
      return this.maxRadius;
    }

    public float getMinRadius()
    {
      return this.minRadius;
    }

    public float getRadius(float paramFloat)
    {
      float f = getNormalizedSmoothedPressure(paramFloat);
      return this.minRadius + f * f * this.deltaRadius;
    }

    public float getStrokeWidth(float paramFloat)
    {
      float f = getNormalizedSmoothedPressure(paramFloat);
      return 2.0F * (this.minRadius + f * f * this.deltaRadius);
    }

    public void resetSmoothing()
    {
      this.lastNormalizedPressure = -1.0F;
    }

    public void setMaxRadius(float paramFloat)
    {
      this.maxRadius = paramFloat;
      this.deltaRadius = (paramFloat - this.minRadius);
    }

    public void setMinRadius(float paramFloat)
    {
      this.minRadius = paramFloat;
      this.deltaRadius = (this.maxRadius - paramFloat);
    }
  }

  private static class SavedState extends View.BaseSavedState
  {
    public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator()
    {
      public HandwritingOverlayView.SavedState createFromParcel(Parcel paramAnonymousParcel)
      {
        return new HandwritingOverlayView.SavedState(paramAnonymousParcel, null);
      }

      public HandwritingOverlayView.SavedState[] newArray(int paramAnonymousInt)
      {
        return new HandwritingOverlayView.SavedState[paramAnonymousInt];
      }
    };
    int strokeColor;
    int strokeColorRecognized;

    private SavedState(Parcel paramParcel)
    {
      super();
      this.strokeColor = paramParcel.readInt();
      this.strokeColorRecognized = paramParcel.readInt();
    }

    SavedState(Parcelable paramParcelable)
    {
      super();
    }

    public void writeToParcel(Parcel paramParcel, int paramInt)
    {
      super.writeToParcel(paramParcel, paramInt);
      paramParcel.writeInt(this.strokeColor);
      paramParcel.writeInt(this.strokeColorRecognized);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.research.handwriting.gui.HandwritingOverlayView
 * JD-Core Version:    0.6.2
 */