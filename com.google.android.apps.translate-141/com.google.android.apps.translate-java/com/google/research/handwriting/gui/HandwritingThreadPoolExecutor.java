package com.google.research.handwriting.gui;

import android.os.AsyncTask;
import android.os.Build.VERSION;

public class HandwritingThreadPoolExecutor<Params, Progress, Result>
{
  public void execute(AsyncTask<Params, Progress, Result> paramAsyncTask, Params[] paramArrayOfParams)
  {
    if (Build.VERSION.SDK_INT >= 11)
    {
      new HoneyCombAndBeyondExecutor().execute(paramAsyncTask, paramArrayOfParams);
      return;
    }
    paramAsyncTask.execute(paramArrayOfParams);
  }

  public class HoneyCombAndBeyondExecutor<Pars, Prog, Res>
  {
    public HoneyCombAndBeyondExecutor()
    {
    }

    public void execute(AsyncTask<Pars, Prog, Res> paramAsyncTask, Pars[] paramArrayOfPars)
    {
      paramAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, paramArrayOfPars);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.research.handwriting.gui.HandwritingThreadPoolExecutor
 * JD-Core Version:    0.6.2
 */