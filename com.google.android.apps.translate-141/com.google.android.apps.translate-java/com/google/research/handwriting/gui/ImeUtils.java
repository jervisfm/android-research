package com.google.research.handwriting.gui;

import android.util.Log;

public class ImeUtils
{
  private static final int[] EDITOR_ACTIONS = { 6, 2, 5, 1, 3, 0, 4 };
  private static final int[] INPUT_TYPES = { 4, 2, 3, 1, 16, 0, 32, 15, 16773120, 4080, 0, 8192, 4096, 0, 16, 65536, 32768, 4096, 16384, 8192, 262144, 131072, 524288, 32, 48, 176, 80, 0, 128, 96, 192, 112, 64, 16, 144, 160, 208, 224 };
  private static final String[] INPUT_TYPE_NAMES = { "CLASS_DATETIME", "CLASS_NUMBER", "CLASS_PHONE", "CLASS_TEXT", "DATETIME_VARIATION_DATE", "DATETIME_VARIATION_NORMAL", "DATETIME_VARIATION_TIME", "MASK_CLASS", "MASK_FLAGS", "MASK_VARIATION", "NULL", "NUMBER_FLAG_DECIMAL", "NUMBER_FLAG_SIGNED", "NUMBER_VARIATION_NORMAL", "NUMBER_VARIATION_PASSWORD", "TEXT_FLAG_AUTO_COMPLETE", "TEXT_FLAG_AUTO_CORRECT", "TEXT_FLAG_CAP_CHARACTERS", "TEXT_FLAG_CAP_SENTENCES", "TEXT_FLAG_CAP_WORDS", "TEXT_FLAG_IME_MULTI_LINE", "TEXT_FLAG_MULTI_LINE", "TEXT_FLAG_NO_SUGGESTIONS", "TEXT_VARIATION_EMAIL_ADDRESS", "TEXT_VARIATION_EMAIL_SUBJECT", "TEXT_VARIATION_FILTER", "TEXT_VARIATION_LONG_MESSAGE", "TEXT_VARIATION_NORMAL", "TEXT_VARIATION_PASSWORD", "TEXT_VARIATION_PERSON_NAME", "TEXT_VARIATION_PHONETIC", "TEXT_VARIATION_POSTAL_ADDRESS", "TEXT_VARIATION_SHORT_MESSAGE", "TEXT_VARIATION_URI", "TEXT_VARIATION_VISIBLE_PASSWORD", "TEXT_VARIATION_WEB_EDIT_TEXT", "TEXT_VARIATION_WEB_EMAIL_ADDRESS", "TEXT_VARIATION_WEB_PASSWORD" };

  public static int getEditorAction(int paramInt)
  {
    if ((paramInt & 0x40000000) == 1073741824)
      return 1;
    for (int i = 0; i < EDITOR_ACTIONS.length; i++)
      if ((paramInt & EDITOR_ACTIONS[i]) == EDITOR_ACTIONS[i])
        return EDITOR_ACTIONS[i];
    Log.i("getEditorAction", "Returning EditorAction Unspecified");
    return 0;
  }

  public static String inputTypeToString(int paramInt)
  {
    String str = paramInt + ":";
    for (int i = 0; i < INPUT_TYPES.length; i++)
      if ((paramInt & INPUT_TYPES[i]) == INPUT_TYPES[i])
        str = str + " " + INPUT_TYPE_NAMES[i];
    return str;
  }

  public static boolean isLanguageWithSpaceGesture(String paramString)
  {
    return isLanguageWithSpaces(paramString);
  }

  public static boolean isLanguageWithSpaces(String paramString)
  {
    return (!paramString.startsWith("zh")) && (!paramString.startsWith("ja")) && (!paramString.startsWith("ko"));
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.research.handwriting.gui.ImeUtils
 * JD-Core Version:    0.6.2
 */