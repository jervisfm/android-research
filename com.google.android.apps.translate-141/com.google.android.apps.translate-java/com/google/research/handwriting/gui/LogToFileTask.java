package com.google.research.handwriting.gui;

import android.os.AsyncTask;
import android.util.Log;
import com.google.research.handwriting.base.LogV;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class LogToFileTask extends AsyncTask<String, Void, Void>
{
  private static final String TAG = LogToFileTask.class.getSimpleName();
  private static BufferedWriter out = null;

  private static void closeFile()
  {
    try
    {
      LogV.i(1, TAG, "Closing log file");
      if (out != null)
        out.close();
      out = null;
      return;
    }
    catch (IOException localIOException)
    {
      Log.e(TAG, "Error closing the reco log file.", localIOException);
    }
  }

  public static void logging(boolean paramBoolean, String paramString)
  {
    if (paramBoolean)
    {
      openFile(paramString);
      return;
    }
    closeFile();
  }

  private static void openFile(String paramString)
  {
    try
    {
      LogV.i(1, TAG, "Openening file " + paramString + " for logging.");
      out = new BufferedWriter(new FileWriter(paramString, true));
      return;
    }
    catch (IOException localIOException)
    {
      out = null;
      Log.e(TAG, "Error opening log file.", localIOException);
    }
  }

  protected Void doInBackground(String[] paramArrayOfString)
  {
    try
    {
      if (out != null)
      {
        out.write(paramArrayOfString[0]);
        out.flush();
      }
      return null;
    }
    catch (IOException localIOException)
    {
      while (true)
        Log.e(TAG, "Error writing to log file.", localIOException);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.research.handwriting.gui.LogToFileTask
 * JD-Core Version:    0.6.2
 */