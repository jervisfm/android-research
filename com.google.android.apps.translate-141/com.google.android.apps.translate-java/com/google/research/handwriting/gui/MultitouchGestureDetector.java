package com.google.research.handwriting.gui;

import android.view.MotionEvent;
import com.google.research.handwriting.base.Stroke;
import com.google.research.handwriting.base.Stroke.Point;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class MultitouchGestureDetector
{
  private ScrollGestureListenner mHorizontalListenner;
  private boolean mIsInProgress = false;
  private final Map<Integer, Stroke> mStrokes = new HashMap();

  static
  {
    if (!MultitouchGestureDetector.class.desiredAssertionStatus());
    for (boolean bool = true; ; bool = false)
    {
      $assertionsDisabled = bool;
      return;
    }
  }

  public MultitouchGestureDetector()
  {
  }

  public MultitouchGestureDetector(ScrollGestureListenner paramScrollGestureListenner)
  {
    this.mHorizontalListenner = paramScrollGestureListenner;
  }

  private float getMeanPointersXCoordinate()
  {
    if (this.mStrokes.size() == 0)
      return 0.0F;
    float f = 0.0F;
    Iterator localIterator = this.mStrokes.values().iterator();
    while (localIterator.hasNext())
      f += ((Stroke)localIterator.next()).getLast().x;
    return f / this.mStrokes.size();
  }

  private void multitouchPointerAdded(MotionEvent paramMotionEvent)
  {
    if (this.mStrokes.isEmpty())
    {
      assert (paramMotionEvent.getPointerCount() == 2);
      this.mStrokes.put(Integer.valueOf(paramMotionEvent.getPointerId(0)), new Stroke());
    }
    this.mStrokes.put(Integer.valueOf(paramMotionEvent.getPointerId(paramMotionEvent.getActionIndex())), new Stroke());
  }

  private void pointerMove(MotionEvent paramMotionEvent)
  {
    int i = paramMotionEvent.getPointerId(paramMotionEvent.getActionIndex());
    for (int j = 0; j < paramMotionEvent.getHistorySize(); j++)
      processMove(i, new Stroke.Point(paramMotionEvent.getHistoricalX(i, j), paramMotionEvent.getHistoricalY(j), (float)paramMotionEvent.getHistoricalEventTime(j), paramMotionEvent.getHistoricalPressure(j)));
    processMove(i, new Stroke.Point(paramMotionEvent.getX(i), paramMotionEvent.getY(i), (float)paramMotionEvent.getEventTime(), paramMotionEvent.getPressure(i)));
  }

  private void processMove(int paramInt, Stroke.Point paramPoint)
  {
    float f = getMeanPointersXCoordinate();
    ((Stroke)this.mStrokes.get(Integer.valueOf(paramInt))).addPoint(paramPoint);
    this.mHorizontalListenner.onScroll(getMeanPointersXCoordinate() - f);
  }

  public boolean isInProgress()
  {
    return this.mIsInProgress;
  }

  public void onTouchEvent(MotionEvent paramMotionEvent)
  {
    switch (paramMotionEvent.getActionMasked())
    {
    case 0:
    case 4:
    case 6:
    default:
    case 5:
    case 2:
    case 1:
      do
      {
        do
        {
          return;
          this.mIsInProgress = true;
          multitouchPointerAdded(paramMotionEvent);
          return;
        }
        while (!this.mIsInProgress);
        pointerMove(paramMotionEvent);
        return;
      }
      while (!this.mIsInProgress);
      this.mStrokes.clear();
      this.mIsInProgress = false;
      return;
    case 3:
    }
    this.mIsInProgress = false;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.research.handwriting.gui.MultitouchGestureDetector
 * JD-Core Version:    0.6.2
 */