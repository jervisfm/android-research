package com.google.research.handwriting.gui;

import com.google.research.handwriting.base.StrokeList;

public abstract interface RecognizerClient
{
  public abstract void delete();

  public abstract void errorMessage(int paramInt, Exception paramException, String paramString);

  public abstract void initializing();

  public abstract boolean isShowDebugInfo();

  public abstract void log(String paramString);

  public abstract void onInitialized(boolean paramBoolean);

  public abstract void onRecognitionEnd(ImeHandwritingRecognizer.RecognitionResult paramRecognitionResult, StrokeList paramStrokeList);

  public abstract void setStatusText(String paramString);

  public abstract void space();
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.research.handwriting.gui.RecognizerClient
 * JD-Core Version:    0.6.2
 */