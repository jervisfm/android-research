package com.google.research.handwriting.gui;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import com.google.research.handwriting.base.LogV;
import com.google.research.handwriting.base.StrokeList;

public class RecognizerHandler
  implements RecognizerClient
{
  private static final String TAG = RecognizerHandler.class.getSimpleName();
  private Context mApplicationContext;
  private boolean mAutoSelect;
  private int mAutoSelectMilli;
  private View mBusyDisplay;
  private TextView mDebugTextView;
  private HandwritingOverlayView mHandwritingOverlayView;
  private ImeHandwritingRecognizer.RecognitionResult mRecognitionResult = ImeHandwritingRecognizer.RecognitionResult.EMPTY;
  private boolean mRecognizerInitialized;
  private boolean mShowDebugInformation;
  private UIHandler mUIHandler;

  public RecognizerHandler(Context paramContext)
  {
  }

  public void delete()
  {
    this.mUIHandler.onKeyDelete();
  }

  public void errorMessage(int paramInt, Exception paramException, final String paramString)
  {
    Log.e(TAG, "errorMessage: (" + paramInt + ") " + paramString, paramException);
    if (this.mApplicationContext != null)
      this.mUIHandler.post(new Runnable()
      {
        public void run()
        {
          Toast.makeText(RecognizerHandler.this.mApplicationContext, paramString, 1).show();
        }
      });
  }

  public ImeHandwritingRecognizer.RecognitionResult getRecognitionResult()
  {
    return this.mRecognitionResult;
  }

  public void initializing()
  {
    this.mRecognizerInitialized = false;
    if (this.mBusyDisplay != null)
      this.mBusyDisplay.setVisibility(0);
  }

  public boolean isInitialized()
  {
    return this.mRecognizerInitialized;
  }

  public boolean isShowDebugInfo()
  {
    return this.mShowDebugInformation;
  }

  public void log(String paramString)
  {
    new LogToFileTask().execute(new String[] { paramString });
  }

  public void onInitialized(boolean paramBoolean)
  {
    if (paramBoolean)
    {
      this.mRecognizerInitialized = true;
      if (this.mBusyDisplay != null)
        this.mBusyDisplay.setVisibility(8);
      return;
    }
    Log.e(TAG, "Recognizer initialization unsuccessful.");
  }

  public void onRecognitionEnd(ImeHandwritingRecognizer.RecognitionResult paramRecognitionResult, StrokeList paramStrokeList)
  {
    LogV.i(1, TAG, "onRecognitionEnd");
    this.mRecognitionResult = paramRecognitionResult;
    SuggestedWords localSuggestedWords = paramRecognitionResult.getWords();
    LogV.i(1, TAG, "clickable: " + localSuggestedWords.clickable());
    this.mUIHandler.showAskTheCloudButton(true);
    this.mUIHandler.setResults(localSuggestedWords, paramRecognitionResult.getCompletions());
    if ((this.mAutoSelect) && (localSuggestedWords.size() > 0) && (localSuggestedWords.clickable()) && (!this.mUIHandler.getEditingPreviouslyEnteredText()))
    {
      LogV.i(1, TAG, "triggering auto select");
      this.mUIHandler.autoSelectSuggestion(localSuggestedWords, this.mAutoSelectMilli);
    }
    while (true)
    {
      if (paramStrokeList != StrokeList.EMPTY)
        this.mHandwritingOverlayView.drawStrokeList(paramStrokeList, true);
      return;
      LogV.i(1, TAG, "NOT triggering auto select");
    }
  }

  public void setApplicationContext(Context paramContext)
  {
    this.mApplicationContext = paramContext;
  }

  public void setAutoSelect(boolean paramBoolean)
  {
    this.mAutoSelect = paramBoolean;
  }

  public void setAutoSelectMilli(int paramInt)
  {
    this.mAutoSelectMilli = paramInt;
  }

  public void setBusyDisplay(View paramView)
  {
    this.mBusyDisplay = paramView;
  }

  public void setDebugView(TextView paramTextView)
  {
    this.mDebugTextView = paramTextView;
  }

  public void setHIHandler(UIHandler paramUIHandler)
  {
    this.mUIHandler = paramUIHandler;
  }

  public void setHandwritingOverlayView(HandwritingOverlayView paramHandwritingOverlayView)
  {
    this.mHandwritingOverlayView = paramHandwritingOverlayView;
  }

  public void setShowDebugInformation(boolean paramBoolean)
  {
    this.mShowDebugInformation = paramBoolean;
  }

  public void setStatusText(String paramString)
  {
    if (this.mDebugTextView != null)
      this.mDebugTextView.setText(paramString);
  }

  public void space()
  {
    this.mUIHandler.onKeyNormal(32);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.research.handwriting.gui.RecognizerHandler
 * JD-Core Version:    0.6.2
 */