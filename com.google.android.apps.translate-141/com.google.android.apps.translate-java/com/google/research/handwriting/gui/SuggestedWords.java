package com.google.research.handwriting.gui;

import android.util.Log;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;

public class SuggestedWords
{
  public static final SuggestedWords EMPTY = new SuggestedWords(null, null, false);
  private final boolean mClickable;
  private final List<ExtraInfo> mExtras;
  private final List<CharSequence> mWords;

  private SuggestedWords(List<CharSequence> paramList, List<ExtraInfo> paramList1, boolean paramBoolean)
  {
    if (paramList != null)
    {
      this.mWords = paramList;
      if (paramList1 == null)
        break label38;
    }
    label38: for (this.mExtras = paramList1; ; this.mExtras = Collections.emptyList())
    {
      this.mClickable = paramBoolean;
      return;
      this.mWords = Collections.emptyList();
      break;
    }
  }

  public boolean clickable()
  {
    return this.mClickable;
  }

  public ExtraInfo getExtra(int paramInt)
  {
    return (ExtraInfo)this.mExtras.get(paramInt);
  }

  public int getExtrasSize()
  {
    return this.mExtras.size();
  }

  public CharSequence getWord(int paramInt)
  {
    if (this.mWords.size() > paramInt)
      return (CharSequence)this.mWords.get(paramInt);
    Log.e("SuggestedWords", "Accessing non-existing result");
    return "";
  }

  public List<CharSequence> getWords()
  {
    return this.mWords;
  }

  public int size()
  {
    return this.mWords.size();
  }

  public String toString()
  {
    String str = "";
    Iterator localIterator = this.mWords.iterator();
    while (localIterator.hasNext())
    {
      CharSequence localCharSequence = (CharSequence)localIterator.next();
      str = str + " " + localCharSequence;
    }
    return str;
  }

  public static class Builder
  {
    private boolean mClickable = false;
    private final List<SuggestedWords.ExtraInfo> mExtras = new ArrayList();
    private final List<CharSequence> mWords = new ArrayList();

    public void addWord(CharSequence paramCharSequence)
    {
      addWord(paramCharSequence, null);
    }

    public void addWord(CharSequence paramCharSequence, SuggestedWords.ExtraInfo paramExtraInfo)
    {
      this.mWords.add(paramCharSequence);
      this.mExtras.add(paramExtraInfo);
    }

    public void addWords(List<CharSequence> paramList, List<SuggestedWords.ExtraInfo> paramList1)
    {
      int i = paramList.size();
      int j = 0;
      if (j < i)
      {
        if ((paramList1 != null) && (j < paramList1.size()));
        for (SuggestedWords.ExtraInfo localExtraInfo = (SuggestedWords.ExtraInfo)paramList1.get(j); ; localExtraInfo = SuggestedWords.ExtraInfo.access$000())
        {
          addWord((CharSequence)paramList.get(j), localExtraInfo);
          j++;
          break;
        }
      }
    }

    public SuggestedWords build()
    {
      return new SuggestedWords(this.mWords, this.mExtras, this.mClickable, null);
    }

    public CharSequence getWord(int paramInt)
    {
      return (CharSequence)this.mWords.get(paramInt);
    }

    public void setClickable(boolean paramBoolean)
    {
      this.mClickable = paramBoolean;
    }

    public int size()
    {
      return this.mWords.size();
    }
  }

  public static class ExtraInfo
  {
    private static final ExtraInfo EMPTY_INFO = new ExtraInfo(null);
    private final CharSequence mInfo;

    public ExtraInfo(CharSequence paramCharSequence)
    {
      this.mInfo = paramCharSequence;
    }

    public CharSequence getInfo()
    {
      return this.mInfo;
    }

    public boolean hasInfo()
    {
      return this != EMPTY_INFO;
    }

    public String toString()
    {
      return this.mInfo.toString();
    }
  }

  public static class TranslationExtraInfo
  {
    private JSONArray mSourceAndTargetTexts;

    public TranslationExtraInfo(CharSequence paramCharSequence)
    {
      try
      {
        this.mSourceAndTargetTexts = new JSONArray(paramCharSequence.toString());
        return;
      }
      catch (JSONException localJSONException)
      {
      }
    }

    public TranslationExtraInfo(CharSequence paramCharSequence1, CharSequence paramCharSequence2)
    {
      if ((paramCharSequence1 == null) || (paramCharSequence2 == null))
        return;
      this.mSourceAndTargetTexts = new JSONArray();
      this.mSourceAndTargetTexts.put(paramCharSequence1);
      this.mSourceAndTargetTexts.put(paramCharSequence2);
    }

    private boolean hasValidExtraInfo()
    {
      return (this.mSourceAndTargetTexts != null) && (this.mSourceAndTargetTexts.length() == 2);
    }

    public String getSourceText()
    {
      try
      {
        if (hasValidExtraInfo())
          return this.mSourceAndTargetTexts.getString(0);
        return "";
      }
      catch (JSONException localJSONException)
      {
      }
      return "";
    }

    public String getTargetText()
    {
      try
      {
        if (hasValidExtraInfo())
          return this.mSourceAndTargetTexts.getString(1);
        return "";
      }
      catch (JSONException localJSONException)
      {
      }
      return "";
    }

    public String toString()
    {
      if (this.mSourceAndTargetTexts == null)
        return "";
      return this.mSourceAndTargetTexts.toString();
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.research.handwriting.gui.SuggestedWords
 * JD-Core Version:    0.6.2
 */