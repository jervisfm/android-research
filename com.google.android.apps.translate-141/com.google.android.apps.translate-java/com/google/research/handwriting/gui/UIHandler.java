package com.google.research.handwriting.gui;

import android.os.Handler;
import android.os.Message;
import android.text.SpannableStringBuilder;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputConnection;
import com.google.research.handwriting.base.HandwritingRecognizer;
import com.google.research.handwriting.base.LogV;
import com.google.research.handwriting.base.StrokeList;

public class UIHandler extends Handler
{
  private static final String FEEDBACK_AUTO = "auto";
  private static final String FEEDBACK_COMPLETION = "completion";
  private static final String FEEDBACK_CONFIRMED = "confirmed";
  private static final String FEEDBACK_DELETED = "deleted";
  private static final String FEEDBACK_FINISHED = "finish";
  private static final String FEEDBACK_ONKEY = "onkey";
  private static final String FEEDBACK_SELECTED = "selected";
  private static final String FEEDBACK_SELECTED_EDITING = "selected-editing";
  private static final int MSG_AUTO_SELECT_SUGGESTION = 2;
  private static final int MSG_SET_RESULTS = 1;
  private static final String TAG = UIHandler.class.getSimpleName();
  private boolean mAutoSpace;
  private final BookKeeper mBookKeeper = new BookKeeper();
  private final UIHandlerCallback mCallback;
  private CandidateViewHandler mCandidateViewHandler;
  private final ContinuousWritingHandler mContinuousWritingHandler;
  private final ContinuousWritingManager mContinuousWritingManager;
  private boolean mEditingPreviouslyEnteredText;
  private View mHandwritingImeView;
  private HandwritingOverlayView mHandwritingOverlayView;
  private final Object mIcLock;
  private boolean mJustAfterSuggestionSelected;
  private boolean mJustAfterTextDeleted;
  private boolean mJustModifiedComposingRegion = false;
  private final SpannableStringBuilder mLastCommittedText = new SpannableStringBuilder();
  private int mLengthOfTextInLimboState = 0;
  private ImeHandwritingRecognizer mRecognizer;
  private RecognizerHandler mRecognizerHandler;
  private boolean mRecognizerHasResult;
  private ImeHandwritingRecognizer.RecognizerUISettings mRecognizerSettings;
  private SuggestedWords mSuggestions = SuggestedWords.EMPTY;
  private String mTranslatedText = "";
  private boolean mUseBookkeeper = true;

  public UIHandler(UIHandlerCallback paramUIHandlerCallback, ContinuousWritingHandler paramContinuousWritingHandler, ContinuousWritingManager paramContinuousWritingManager, Object paramObject)
  {
    this.mCallback = paramUIHandlerCallback;
    this.mContinuousWritingHandler = paramContinuousWritingHandler;
    this.mContinuousWritingManager = paramContinuousWritingManager;
    this.mIcLock = paramObject;
  }

  public void autoSelectSuggestion(SuggestedWords paramSuggestedWords, int paramInt)
  {
    LogV.i(2, TAG, "Triggering auto select of " + paramSuggestedWords.getWord(0) + " in " + paramInt + "ms.");
    sendMessageDelayed(obtainMessage(2, paramSuggestedWords), paramInt);
  }

  public void cancelAutoSelect()
  {
    this.mJustAfterSuggestionSelected = false;
    removeMessages(2);
  }

  public void cancelStroke()
  {
    this.mRecognizer.cancelStroke();
  }

  public void clearRecognizer()
  {
    if (this.mHandwritingOverlayView != null)
      this.mHandwritingOverlayView.clear();
    updateTranslatedText();
    this.mRecognizer.clear();
    setSuggestions(SuggestedWords.EMPTY);
    this.mContinuousWritingHandler.drawRecoQueue();
  }

  public void clearResults()
  {
    setResults(SuggestedWords.EMPTY, SuggestedWords.EMPTY);
    this.mJustAfterSuggestionSelected = false;
    this.mRecognizerHasResult = false;
  }

  public void clearTranslatedText()
  {
    if (!this.mRecognizer.usesTranslateApi())
      return;
    LogV.i(2, TAG, "mTranslatedText cleared");
    this.mTranslatedText = "";
  }

  public void commitDeleteLeft(int paramInt)
  {
    synchronized (this.mIcLock)
    {
      this.mCallback.getImeCurrentInputConnection().deleteSurroundingText(paramInt, 0);
      this.mJustAfterTextDeleted = true;
      this.mRecognizer.setPreContext(getPreContext());
      this.mRecognizer.setPostContext(getPostContext());
      return;
    }
  }

  public void commitSuggestion(CharSequence paramCharSequence, boolean paramBoolean, char paramChar)
  {
    this.mLastCommittedText.clear();
    this.mLastCommittedText.append(paramCharSequence);
    int i = this.mCallback.getCursorSelectionStart();
    int j = this.mCallback.getCursorSelectionEnd();
    if ((!"∅[incorrect]".contentEquals(paramCharSequence.toString())) && (!"∅[no recognition results]".contentEquals(paramCharSequence.toString())))
      if (this.mUseBookkeeper)
      {
        this.mBookKeeper.update(i, j, this.mLastCommittedText.toString(), this.mRecognizerHandler.getRecognitionResult());
        if (paramBoolean)
        {
          this.mBookKeeper.update(j + this.mLastCommittedText.length(), j + this.mLastCommittedText.length(), "" + paramChar, ImeHandwritingRecognizer.RecognitionResult.EMPTY);
          this.mLastCommittedText.append(paramChar);
        }
        commitText(this.mLastCommittedText);
        LogV.i(2, TAG, "commitSuggestion: text='" + this.mLastCommittedText + "'");
      }
    while (true)
    {
      clearResults();
      clearRecognizer();
      this.mRecognizer.setPreContext(getPreContext());
      this.mRecognizer.setPostContext(getPostContext());
      return;
      if (!paramBoolean)
        break;
      this.mLastCommittedText.append(paramChar);
      break;
      commitText("");
      if (this.mUseBookkeeper)
        this.mBookKeeper.update(i, j, "", ImeHandwritingRecognizer.RecognitionResult.EMPTY);
    }
  }

  public void commitText(CharSequence paramCharSequence)
  {
    synchronized (this.mIcLock)
    {
      this.mCallback.getImeCurrentInputConnection().commitText(paramCharSequence, 1);
      this.mRecognizer.setPreContext(getPreContext());
      this.mRecognizer.setPostContext(getPostContext());
      this.mCallback.onCommitText(paramCharSequence);
      return;
    }
  }

  public void finishInput(boolean paramBoolean)
  {
    String str1;
    ImeHandwritingRecognizer localImeHandwritingRecognizer;
    if (this.mSuggestions.size() > 0)
    {
      str1 = this.mRecognizerHandler.getRecognitionResult().getWords().getWord(0).toString();
      if (!this.mSuggestions.clickable())
        break label76;
      localImeHandwritingRecognizer = this.mRecognizer;
      if (!paramBoolean)
        break label69;
    }
    label69: for (String str2 = "confirmed"; ; str2 = "finish")
    {
      localImeHandwritingRecognizer.logFeedback(str1, str2, this.mRecognizerHandler.getRecognitionResult(), false, "");
      return;
    }
    label76: this.mRecognizerHandler.getRecognitionResult().setStrokes(this.mRecognizer.strokes());
    this.mRecognizer.logFeedback(str1, "finish", this.mRecognizerHandler.getRecognitionResult(), false, "");
  }

  public BookKeeper getBookKeeper()
  {
    return this.mBookKeeper;
  }

  public boolean getEditingPreviouslyEnteredText()
  {
    return this.mEditingPreviouslyEnteredText;
  }

  public boolean getJustAfterTextDeleted()
  {
    return this.mJustAfterTextDeleted;
  }

  public boolean getJustModifiedComposingRegion()
  {
    return this.mJustModifiedComposingRegion;
  }

  public String getLanguage()
  {
    return this.mRecognizer.getLanguage();
  }

  public String getPostContext()
  {
    InputConnection localInputConnection = this.mCallback.getImeCurrentInputConnection();
    if ((localInputConnection == null) || (this.mRecognizerSettings == null) || (this.mCallback == null))
      return "";
    CharSequence localCharSequence = localInputConnection.getTextAfterCursor(20, 0);
    if (localCharSequence == null)
      return "";
    return localCharSequence.toString();
  }

  public String getPreContext()
  {
    InputConnection localInputConnection = this.mCallback.getImeCurrentInputConnection();
    if ((localInputConnection == null) || (this.mRecognizerSettings == null) || (this.mCallback == null))
      return "";
    CharSequence localCharSequence = localInputConnection.getTextBeforeCursor(20 + this.mLengthOfTextInLimboState, 0);
    if (localCharSequence == null)
      return "";
    int i = localCharSequence.length() - this.mLengthOfTextInLimboState;
    if (i < 0)
      i = 0;
    return localCharSequence.subSequence(0, i).toString();
  }

  public CharSequence getSuggestion(int paramInt)
  {
    try
    {
      if (paramInt < this.mSuggestions.size())
      {
        CharSequence localCharSequence = this.mSuggestions.getWord(paramInt);
        localObject2 = localCharSequence;
        return localObject2;
      }
      Object localObject2 = "";
    }
    finally
    {
    }
  }

  public String getTranslatedText()
  {
    return this.mTranslatedText;
  }

  public UIHandlerCallback getUIHandlerCallback()
  {
    return this.mCallback;
  }

  public void handleMessage(Message paramMessage)
  {
    LogV.i(3, TAG, "Handling message: " + paramMessage);
    switch (paramMessage.what)
    {
    default:
      super.handleMessage(paramMessage);
      return;
    case 1:
      ResultsAndCompletions localResultsAndCompletions = (ResultsAndCompletions)paramMessage.obj;
      try
      {
        LogV.i(3, TAG, "handleMessage: results=" + localResultsAndCompletions.results.toString());
        LogV.i(3, TAG, "handleMessage: completions=" + localResultsAndCompletions.completions.toString());
        setSuggestions(localResultsAndCompletions.results);
        updateTranslatedText();
        updateResultsInCandidateView(localResultsAndCompletions.results, localResultsAndCompletions.completions);
        if ((this.mContinuousWritingHandler.numRecognitionsInBackground() == 0) && (this.mRecognizerHasResult) && (selectionIsEmpty()))
          makeTopCandidateLimboState(localResultsAndCompletions.results.getWord(0));
        return;
      }
      finally
      {
      }
    case 2:
    }
    makeAutoSelectTopCandidate(((SuggestedWords)paramMessage.obj).getWord(0));
  }

  public boolean imeVisible()
  {
    return this.mHandwritingImeView.getWindowVisibility() == 0;
  }

  public boolean isAutoSpace()
  {
    if (!ImeUtils.isLanguageWithSpaces(this.mCallback.getRecognizerLanguage()))
      return false;
    return this.mAutoSpace;
  }

  public void makeAutoSelectTopCandidate(CharSequence paramCharSequence)
  {
    LogV.i(2, TAG, "Auto select received " + paramCharSequence);
    if (!this.mJustAfterSuggestionSelected)
    {
      LogV.i(2, TAG, "!mJustAfterSuggestionSelected doing it");
      commitSuggestion(paramCharSequence, isAutoSpace(), ' ');
      this.mRecognizer.logFeedback(paramCharSequence.toString(), "auto", this.mRecognizerHandler.getRecognitionResult(), false, "");
    }
    while (true)
    {
      this.mJustAfterSuggestionSelected = true;
      return;
      LogV.i(2, TAG, "mJustAfterSuggestionSelected -> cancelling auto select");
    }
  }

  public void makeTopCandidateLimboState(CharSequence paramCharSequence)
  {
    synchronized (this.mIcLock)
    {
      this.mCallback.getImeCurrentInputConnection().setComposingText(paramCharSequence, 1);
      this.mJustModifiedComposingRegion = true;
      return;
    }
  }

  public void onKeyDelete()
  {
    if ((!this.mRecognizerHasResult) || (this.mEditingPreviouslyEnteredText))
    {
      this.mCallback.deleteText();
      int i = this.mCallback.getCursorSelectionStart();
      if (selectionIsEmpty())
        i--;
      LogV.i(2, TAG, "Now going to delete from bookkeeper: " + i + " -> " + this.mCallback.getCursorSelectionEnd());
      if (this.mUseBookkeeper)
        this.mBookKeeper.update(i, this.mCallback.getCursorSelectionEnd(), "", ImeHandwritingRecognizer.RecognitionResult.EMPTY);
    }
    this.mJustAfterTextDeleted = true;
    if (this.mRecognizer.strokes().size() > 0)
    {
      if (!this.mSuggestions.clickable())
        break label187;
      this.mRecognizer.logFeedback("∅[deleted]", "deleted", this.mRecognizerHandler.getRecognitionResult(), false, "");
    }
    while (true)
    {
      clearRecognizer();
      clearResults();
      commitText("");
      this.mRecognizerHasResult = false;
      return;
      label187: this.mRecognizerHandler.getRecognitionResult().setStrokes(this.mRecognizer.strokes());
      this.mRecognizer.logFeedback("∅[deleted]", "deleted", this.mRecognizerHandler.getRecognitionResult(), false, "");
    }
  }

  public void onKeyNormal(int paramInt)
  {
    StrokeList localStrokeList;
    int i;
    if ((!this.mSuggestions.clickable()) && (!this.mRecognizer.strokes().isEmpty()))
    {
      localStrokeList = (StrokeList)this.mRecognizer.strokes().clone();
      localStrokeList.setContext(getPreContext(), getPostContext());
      i = this.mContinuousWritingManager.startRecognition(localStrokeList);
      if (paramInt == 32)
      {
        this.mContinuousWritingHandler.addToQueue(i, localStrokeList, ImeUtils.isLanguageWithSpaces(this.mCallback.getRecognizerLanguage()), (char)paramInt);
        if (this.mContinuousWritingHandler.numRecognitionsInBackground() == 1)
        {
          LogV.i(3, TAG, "Inserting placeholder");
          commitSuggestion("…", false, (char)paramInt);
        }
        this.mRecognizer.clear();
        this.mHandwritingOverlayView.clear();
      }
    }
    while (true)
    {
      updateTranslatedText();
      this.mContinuousWritingHandler.drawRecoQueue();
      return;
      this.mContinuousWritingHandler.addToQueue(i, localStrokeList, true, (char)paramInt);
      break;
      CharSequence localCharSequence = getSuggestion(0);
      if ((!localCharSequence.equals("")) && (!this.mJustAfterSuggestionSelected))
      {
        if (paramInt == 32)
          commitSuggestion(localCharSequence, ImeUtils.isLanguageWithSpaces(this.mCallback.getRecognizerLanguage()), (char)paramInt);
        while (true)
        {
          this.mRecognizer.logFeedback(localCharSequence.toString(), "onkey", this.mRecognizerHandler.getRecognitionResult(), false, "");
          break;
          commitSuggestion(localCharSequence, true, (char)paramInt);
        }
      }
      this.mCallback.sendKeyChar((char)paramInt);
      if (this.mUseBookkeeper)
        this.mBookKeeper.update(this.mCallback.getCursorSelectionStart(), this.mCallback.getCursorSelectionEnd(), new Character((char)paramInt).toString(), ImeHandwritingRecognizer.RecognitionResult.EMPTY);
      this.mLastCommittedText.append((char)paramInt);
    }
  }

  public void onPenDown(float paramFloat1, float paramFloat2, long paramLong, float paramFloat3)
  {
    cancelAutoSelect();
    this.mLastCommittedText.clear();
    this.mRecognizer.onPenDown(paramFloat1, paramFloat2, paramLong, paramFloat3);
  }

  public void onPenMove(float paramFloat1, float paramFloat2, long paramLong, float paramFloat3)
  {
    cancelAutoSelect();
    this.mRecognizer.onPenMove(paramFloat1, paramFloat2, paramLong, paramFloat3);
  }

  public void onPenUp(float paramFloat1, float paramFloat2, long paramLong, float paramFloat3)
  {
    this.mRecognizer.onPenUp(paramFloat1, paramFloat2, paramLong, paramFloat3);
  }

  public void onPickSuggestion(int paramInt, CharSequence paramCharSequence, boolean paramBoolean)
  {
    cancelAutoSelect();
    while (true)
    {
      synchronized (this.mIcLock)
      {
        InputConnection localInputConnection = this.mCallback.getImeCurrentInputConnection();
        if (localInputConnection != null)
          localInputConnection.beginBatchEdit();
        if ((this.mJustAfterSuggestionSelected) && (!"∅[incorrect]".contentEquals(paramCharSequence.toString())) && (!"∅[no recognition results]".contentEquals(paramCharSequence.toString())))
          commitDeleteLeft(this.mLastCommittedText.length());
        if (this.mEditingPreviouslyEnteredText)
        {
          commitSuggestion(paramCharSequence, false, ' ');
          this.mRecognizer.logFeedback(paramCharSequence.toString(), "selected-editing", this.mRecognizerHandler.getRecognitionResult(), false, "");
          if (localInputConnection != null)
            localInputConnection.endBatchEdit();
          updateTranslatedText();
          this.mJustAfterSuggestionSelected = true;
          clearResults();
          this.mEditingPreviouslyEnteredText = false;
          return;
        }
        commitSuggestion(paramCharSequence, isAutoSpace(), ' ');
        ImeHandwritingRecognizer localImeHandwritingRecognizer = this.mRecognizer;
        String str1 = paramCharSequence.toString();
        if (paramBoolean)
        {
          str2 = "completion";
          localImeHandwritingRecognizer.logFeedback(str1, str2, this.mRecognizerHandler.getRecognitionResult(), paramBoolean, "");
        }
      }
      String str2 = "selected";
    }
  }

  public void onSizeChanged(int paramInt1, int paramInt2)
  {
    this.mRecognizer.setWritingGuide(paramInt1, paramInt2);
  }

  public void onUpdateSelection(int paramInt1, int paramInt2)
  {
    if (!this.mContinuousWritingHandler.isInternalContinuousWritingEditing())
    {
      LogV.i(2, TAG, "!mContinuousWritingInternalEditing");
      clearRecognizer();
      clearResults();
      if (!getJustAfterTextDeleted())
      {
        this.mLastCommittedText.clear();
        if ((!this.mUseBookkeeper) || (this.mCandidateViewHandler == null) || (selectionIsEmpty()))
          break label201;
        LogV.i(1, TAG, "searching in the bookkeeper");
        clearRecognizer();
        clearResults();
        BookKeeper.RecognitionRequestWithResults localRecognitionRequestWithResults = this.mBookKeeper.getBookkeepingEntry((paramInt1 + paramInt2) / 2);
        LogV.i(2, TAG, "fromBookkeeper: " + localRecognitionRequestWithResults);
        setEditingPreviouslyEnteredText(true);
        this.mCandidateViewHandler.setSelectedWord(localRecognitionRequestWithResults.selectedWord);
        this.mRecognizerHandler.onRecognitionEnd(localRecognitionRequestWithResults.recognitionResult, localRecognitionRequestWithResults.recognitionResult.getStrokes());
      }
    }
    while (true)
    {
      if (this.mRecognizer != null)
      {
        this.mRecognizer.setPreContext(getPreContext());
        this.mRecognizer.setPostContext(getPostContext());
      }
      setJustAfterTextDeleted(false);
      return;
      this.mContinuousWritingHandler.setContinuousWritingInternalEditing(false);
      break;
      label201: LogV.i(1, TAG, "NOT searching in the bookkeeper. selectionIsEmpty " + selectionIsEmpty());
    }
  }

  public boolean selectionIsEmpty()
  {
    return this.mCallback.getCursorSelectionStart() == this.mCallback.getCursorSelectionEnd();
  }

  public void setAutoSpace(boolean paramBoolean)
  {
    this.mAutoSpace = paramBoolean;
  }

  public void setCandidateViewHandler(CandidateViewHandler paramCandidateViewHandler)
  {
    this.mCandidateViewHandler = paramCandidateViewHandler;
  }

  public void setEditingPreviouslyEnteredText(boolean paramBoolean)
  {
    this.mEditingPreviouslyEnteredText = paramBoolean;
  }

  public void setHandwritingImeView(View paramView)
  {
    this.mHandwritingImeView = paramView;
  }

  public void setHandwritingOverlayView(HandwritingOverlayView paramHandwritingOverlayView)
  {
    this.mHandwritingOverlayView = paramHandwritingOverlayView;
  }

  public void setHandwritingRecognizer(ImeHandwritingRecognizer paramImeHandwritingRecognizer)
  {
    this.mRecognizer = paramImeHandwritingRecognizer;
  }

  public void setJustAfterTextDeleted(boolean paramBoolean)
  {
    this.mJustAfterTextDeleted = paramBoolean;
  }

  public void setJustModifiedComposingRegion(boolean paramBoolean)
  {
    this.mJustModifiedComposingRegion = paramBoolean;
  }

  public void setLengthOfTextInLimboState(int paramInt)
  {
    Log.i(TAG, "length of text in limbo state: " + paramInt);
    this.mLengthOfTextInLimboState = paramInt;
  }

  public void setRecognizerHandler(RecognizerHandler paramRecognizerHandler)
  {
    this.mRecognizerHandler = paramRecognizerHandler;
  }

  public void setRecognizerSettings(ImeHandwritingRecognizer.RecognizerUISettings paramRecognizerUISettings)
  {
    this.mRecognizerSettings = paramRecognizerUISettings;
  }

  public void setResults(SuggestedWords paramSuggestedWords1, SuggestedWords paramSuggestedWords2)
  {
    LogV.i(2, TAG, "Setting suggestions in UI: " + paramSuggestedWords1.toString());
    obtainMessage(1, new ResultsAndCompletions(paramSuggestedWords1, paramSuggestedWords2)).sendToTarget();
  }

  public void setSuggestions(SuggestedWords paramSuggestedWords)
  {
    this.mSuggestions = paramSuggestedWords;
  }

  public void setUseBookkeeper(boolean paramBoolean)
  {
    this.mUseBookkeeper = paramBoolean;
  }

  public void showAskTheCloudButton(boolean paramBoolean)
  {
    CandidateViewHandler localCandidateViewHandler = this.mCandidateViewHandler;
    if ((paramBoolean) && (this.mRecognizer.recognizer().hasSecondaryRecognizer()));
    for (boolean bool = true; ; bool = false)
    {
      localCandidateViewHandler.showAskTheCloudButton(bool);
      return;
    }
  }

  public void updateResultsInCandidateView(SuggestedWords paramSuggestedWords1, SuggestedWords paramSuggestedWords2)
  {
    LogV.i(2, TAG, "updateResultsInCandidateView");
    if ((this.mCandidateViewHandler != null) && (imeVisible()))
    {
      this.mCandidateViewHandler.setCandidateSuggestions(paramSuggestedWords1, paramSuggestedWords2);
      int i = paramSuggestedWords1.size();
      boolean bool = false;
      if (i > 0)
        bool = true;
      this.mRecognizerHasResult = bool;
      this.mCallback.setSuggestedWords(paramSuggestedWords1);
      this.mCallback.setImeCandidatesViewShown(this.mRecognizerHasResult);
      LogV.i(2, TAG, "CandidateViewShown = " + this.mRecognizerHasResult + " suggestions.size() = " + paramSuggestedWords1.size() + " mHandwritingView.getVisibility = " + this.mHandwritingImeView.getVisibility() + " mHandwritingView.getWindowVisibility = " + this.mHandwritingImeView.getWindowVisibility());
      this.mCandidateViewHandler.scrollCandidatesToBeginning();
      updateTranslatedText();
      return;
    }
    this.mRecognizerHasResult = false;
    this.mCallback.setSuggestedWords(null);
    this.mCallback.setImeCandidatesViewShown(false);
  }

  public void updateTranslatedText()
  {
    if (!this.mRecognizer.usesTranslateApi());
    while ((!this.mRecognizer.usesTranslateApi()) || (this.mSuggestions.getExtrasSize() <= 0) || (this.mSuggestions.getExtra(0) == null))
      return;
    updateTranslatedText(new SuggestedWords.TranslationExtraInfo(this.mSuggestions.getExtra(0).getInfo()), this.mCallback.getSourceTextToTranslate());
  }

  public void updateTranslatedText(SuggestedWords.TranslationExtraInfo paramTranslationExtraInfo, String paramString)
  {
    if (!this.mRecognizer.usesTranslateApi())
      return;
    String str1 = paramTranslationExtraInfo.getSourceText().replace("…", "");
    String str2 = paramTranslationExtraInfo.getTargetText();
    LogV.i(2, TAG, "mTranslatedText update: sourceText=" + str1);
    LogV.i(2, TAG, "mTranslatedText update: translatedText=" + str2);
    LogV.i(2, TAG, "mTranslatedText update: recognizedText=" + paramString);
    if (str1.equals(paramString))
    {
      this.mTranslatedText = str2;
      LogV.i(2, TAG, "mTranslatedText=" + this.mTranslatedText);
      return;
    }
    clearTranslatedText();
  }

  private class ResultsAndCompletions
  {
    public final SuggestedWords completions;
    public final SuggestedWords results;

    public ResultsAndCompletions(SuggestedWords paramSuggestedWords1, SuggestedWords arg3)
    {
      this.results = paramSuggestedWords1;
      Object localObject;
      this.completions = localObject;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.research.handwriting.gui.UIHandler
 * JD-Core Version:    0.6.2
 */