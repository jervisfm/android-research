package com.google.research.handwriting.gui;

import android.view.inputmethod.InputConnection;

public abstract interface UIHandlerCallback
{
  public abstract void deleteText();

  public abstract int getCursorSelectionEnd();

  public abstract int getCursorSelectionStart();

  public abstract InputConnection getImeCurrentInputConnection();

  public abstract String getRecognizerLanguage();

  public abstract String getSourceTextToTranslate();

  public abstract void onCommitText(CharSequence paramCharSequence);

  public abstract void sendKeyChar(char paramChar);

  public abstract void setImeCandidatesViewShown(boolean paramBoolean);

  public abstract void setSuggestedWords(SuggestedWords paramSuggestedWords);
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.research.handwriting.gui.UIHandlerCallback
 * JD-Core Version:    0.6.2
 */