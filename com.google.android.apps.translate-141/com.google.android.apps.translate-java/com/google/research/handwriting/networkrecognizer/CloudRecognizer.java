package com.google.research.handwriting.networkrecognizer;

import android.content.Context;
import android.util.Log;
import com.google.protos.research_handwriting.OndeviceSpec.CloudRecognizerSpec;
import com.google.protos.research_handwriting.OndeviceSpec.HandwritingRecognizerSpec;
import com.google.research.handwriting.base.HandwritingRecognizer;
import com.google.research.handwriting.base.HandwritingRecognizer.CancelStruct;
import com.google.research.handwriting.base.HandwritingRecognizer.HandwritingRecognizerSettings;
import com.google.research.handwriting.base.HandwritingRecognizer.RecognitionFailedException;
import com.google.research.handwriting.base.HandwritingRecognizer.SendingFeedbackFailedException;
import com.google.research.handwriting.base.RecognitionResult;
import com.google.research.handwriting.base.ScoredCandidate;
import com.google.research.handwriting.base.StrokeList;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPOutputStream;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

public class CloudRecognizer
  implements HandwritingRecognizer
{
  private static final String TAG = "CloudRecognizer";
  private final List<Feedback> feedbacks = new ArrayList();
  private final HttpClient httpClient;
  private final CloudRecognizerSettings settings;

  public CloudRecognizer(OndeviceSpec.HandwritingRecognizerSpec paramHandwritingRecognizerSpec)
  {
    this.httpClient = HandwritingHttpClient.getNewHttpClient();
    this.settings = new CloudRecognizerSettings();
    OndeviceSpec.CloudRecognizerSpec localCloudRecognizerSpec = paramHandwritingRecognizerSpec.getCloudSpec();
    this.settings.language = paramHandwritingRecognizerSpec.getLanguageCode();
    if (localCloudRecognizerSpec.hasServer())
      this.settings.server = localCloudRecognizerSpec.getServer();
    if (localCloudRecognizerSpec.hasRecoPath())
      this.settings.recoPath = localCloudRecognizerSpec.getRecoPath();
    this.settings.compressRequests = localCloudRecognizerSpec.getCompressRequests();
    this.settings.deviceName = localCloudRecognizerSpec.getDeviceName();
    this.settings.deviceVersion = localCloudRecognizerSpec.getDeviceVersion();
    this.settings.clientName = localCloudRecognizerSpec.getClientName();
    this.settings.clientVersion = localCloudRecognizerSpec.getClientVersion();
    this.settings.verbosity = paramHandwritingRecognizerSpec.getVerbosity();
    this.settings.sendFeedbackImmediately = localCloudRecognizerSpec.getSendFeedbackImmediately();
    this.settings.feedbackBatchSize = localCloudRecognizerSpec.getFeedbackBatchSize();
  }

  public CloudRecognizer(OndeviceSpec.HandwritingRecognizerSpec paramHandwritingRecognizerSpec, Context paramContext)
  {
    this(paramHandwritingRecognizerSpec);
  }

  public CloudRecognizer(HttpClient paramHttpClient, CloudRecognizerSettings paramCloudRecognizerSettings)
  {
    this.httpClient = paramHttpClient;
    this.settings = paramCloudRecognizerSettings;
  }

  private static void addContext(JSONObject paramJSONObject, String paramString1, String paramString2)
    throws JSONException
  {
    if ((paramString1 != null) && (paramString1.length() > 0))
      paramJSONObject.put("pre_context", paramString1);
    if ((paramString2 != null) && (paramString2.length() > 0))
      paramJSONObject.put("post_context", paramString2);
  }

  private static void addInputType(JSONObject paramJSONObject, int paramInt)
    throws JSONException
  {
    paramJSONObject.put("input_type", paramInt);
  }

  private static void addWritingGuide(JSONObject paramJSONObject, StrokeList paramStrokeList)
    throws JSONException
  {
    JSONObject localJSONObject = new JSONObject();
    localJSONObject.put("writing_area_width", paramStrokeList.getWritingGuideWidth());
    localJSONObject.put("writing_area_height", paramStrokeList.getWritingGuideHeight());
    paramJSONObject.put("writing_guide", localJSONObject);
  }

  private static byte[] gzipString(String paramString)
  {
    ByteArrayOutputStream localByteArrayOutputStream = new ByteArrayOutputStream();
    try
    {
      OutputStreamWriter localOutputStreamWriter = new OutputStreamWriter(new GZIPOutputStream(localByteArrayOutputStream), "UTF-8");
      localOutputStreamWriter.write(paramString);
      localOutputStreamWriter.close();
      return localByteArrayOutputStream.toByteArray();
    }
    catch (IOException localIOException)
    {
      while (true)
        Log.e("CloudRecognizer", "gzipString", localIOException);
    }
  }

  private void logVi(int paramInt, String paramString1, String paramString2)
  {
    if (this.settings == null);
    while (this.settings.verbosity < paramInt)
      return;
    Log.i(paramString1, paramString2);
  }

  private JSONObject prepareJSONObject()
    throws JSONException
  {
    JSONObject localJSONObject = new JSONObject();
    localJSONObject.put("app_version", this.settings.clientVersion);
    localJSONObject.put("api_level", this.settings.deviceVersion);
    localJSONObject.put("device", this.settings.deviceName);
    return localJSONObject;
  }

  private static void setHttpDebugging()
  {
    Logger.getLogger("org.apache.http.wire").setLevel(Level.FINEST);
    Logger.getLogger("org.apache.http.headers").setLevel(Level.FINEST);
    System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.SimpleLog");
    System.setProperty("org.apache.commons.logging.simplelog.showdatetime", "true");
    System.setProperty("org.apache.commons.logging.simplelog.log.httpclient.wire", "debug");
    System.setProperty("org.apache.commons.logging.simplelog.log.org.apache.http", "debug");
    System.setProperty("org.apache.commons.logging.simplelog.log.org.apache.http.headers", "debug");
  }

  private void timing(long paramLong, int paramInt, String paramString)
  {
    logVi(3, "CloudRecognizer", "Timing(" + paramInt + ") " + paramString + ": " + (System.currentTimeMillis() - paramLong));
  }

  public String buildFeedbackRequest(Feedback paramFeedback)
    throws JSONException
  {
    JSONObject localJSONObject = prepareJSONObject();
    if (paramFeedback.strokes != null)
      addInputType(localJSONObject, paramFeedback.strokes.getInputType());
    JSONArray localJSONArray = new JSONArray();
    localJSONArray.put(paramFeedback.asJsonObject(this.settings.language));
    localJSONObject.put("requests", localJSONArray);
    return localJSONObject.toString();
  }

  public String buildFeedbackRequest(String paramString1, StrokeList paramStrokeList, String paramString2, String paramString3)
    throws JSONException
  {
    return buildFeedbackRequest(new Feedback(paramString1, paramStrokeList, paramString2, paramString3));
  }

  public String buildFeedbacksRequest(List<Feedback> paramList)
    throws JSONException
  {
    JSONObject localJSONObject = prepareJSONObject();
    JSONArray localJSONArray = new JSONArray();
    localJSONObject.put("requests", localJSONArray);
    Iterator localIterator = paramList.iterator();
    while (localIterator.hasNext())
    {
      Feedback localFeedback = (Feedback)localIterator.next();
      if ((paramList.size() == 1) && (localFeedback.strokes != null))
        addInputType(localJSONObject, localFeedback.strokes.getInputType());
      localJSONArray.put(localFeedback.asJsonObject(this.settings.language));
    }
    this.feedbacks.clear();
    return localJSONObject.toString();
  }

  String buildRecognitionRequestString(StrokeList paramStrokeList)
    throws JSONException
  {
    JSONObject localJSONObject1 = prepareJSONObject();
    addInputType(localJSONObject1, paramStrokeList.getInputType());
    if (paramStrokeList.getEnablePreSpace())
      localJSONObject1.put("options", "enable_pre_space");
    JSONObject localJSONObject2 = new JSONObject();
    JSONArray localJSONArray = new JSONArray();
    localJSONArray.put(localJSONObject2);
    localJSONObject1.put("requests", localJSONArray);
    addWritingGuide(localJSONObject2, paramStrokeList);
    localJSONObject2.put("language", this.settings.language);
    localJSONObject2.put("ink", paramStrokeList.asJsonArray());
    addContext(localJSONObject2, paramStrokeList.getPreContext(), paramStrokeList.getPostContext());
    Iterator localIterator = this.feedbacks.iterator();
    while (localIterator.hasNext())
      localJSONArray.put(((Feedback)localIterator.next()).asJsonObject(this.settings.language));
    this.feedbacks.clear();
    return localJSONObject1.toString();
  }

  String executeHttpRequest(String paramString, long paramLong, int paramInt)
    throws UnsupportedEncodingException, IOException, ClientProtocolException, HttpResponseException
  {
    timing(paramLong, paramInt, "before creating request");
    String str1 = CloudRecognizerProtocolStrings.recognitionUrl(this.settings.server, this.settings.recoPath, this.settings.clientName, this.settings.useTranslateApi, this.settings.language, this.settings.targetLanguage);
    logVi(3, "CloudRecognizer", "URL = " + str1);
    HttpPost localHttpPost = new HttpPost(str1);
    if (this.settings.compressRequests)
    {
      byte[] arrayOfByte = gzipString(paramString);
      timing(paramLong, paramInt, "passed after building request string");
      ByteArrayEntity localByteArrayEntity = new ByteArrayEntity(arrayOfByte);
      localByteArrayEntity.setContentType("application/octet-stream");
      localHttpPost.setEntity(localByteArrayEntity);
    }
    HttpResponse localHttpResponse;
    int i;
    while (true)
    {
      logVi(3, "CloudRecognizer", "SENDING to " + str1 + ": " + paramString);
      timing(paramLong, paramInt, "before sending request to server");
      localHttpResponse = this.httpClient.execute(localHttpPost);
      timing(paramLong, paramInt, "got response from server");
      i = localHttpResponse.getStatusLine().getStatusCode();
      if (200 != i)
        break;
      String str2 = EntityUtils.toString(localHttpResponse.getEntity());
      localHttpResponse.getEntity().consumeContent();
      timing(paramLong, paramInt, "network handling done entirely");
      return str2;
      StringEntity localStringEntity = new StringEntity(paramString, "UTF-8");
      localStringEntity.setContentType("application/json");
      localHttpPost.setEntity(localStringEntity);
    }
    throw new HttpResponseException(i, localHttpResponse.getStatusLine().toString());
  }

  public void feedback(String paramString1, StrokeList paramStrokeList, String paramString2, String paramString3)
    throws HandwritingRecognizer.SendingFeedbackFailedException
  {
    recordOrSendFeedback(new Feedback(paramString1, paramStrokeList, paramString2, paramString3));
  }

  public void feedback(String paramString1, String paramString2, String paramString3, String paramString4)
    throws HandwritingRecognizer.SendingFeedbackFailedException
  {
    recordOrSendFeedback(new Feedback(paramString1, paramString2, paramString3, paramString4));
  }

  public HandwritingRecognizer getSecondaryRecognizer()
  {
    return null;
  }

  public HandwritingRecognizer.HandwritingRecognizerSettings getSettings()
  {
    return this.settings;
  }

  public boolean hasSecondaryRecognizer()
  {
    return false;
  }

  RecognitionResult parseServerResponse(String paramString, long paramLong, StrokeList paramStrokeList)
    throws JSONException, CloudRecognizer.InvalidResponseFromServerException
  {
    if (paramString == null)
    {
      InvalidResponseFromServerException localInvalidResponseFromServerException1 = new InvalidResponseFromServerException("No answer.");
      throw localInvalidResponseFromServerException1;
    }
    JSONTokener localJSONTokener = new JSONTokener(paramString);
    JSONArray localJSONArray1 = (JSONArray)localJSONTokener.nextValue();
    if (!localJSONArray1.getString(0).contentEquals("SUCCESS"))
    {
      InvalidResponseFromServerException localInvalidResponseFromServerException2 = new InvalidResponseFromServerException("Not SUCCESS.");
      throw localInvalidResponseFromServerException2;
    }
    JSONArray localJSONArray2 = localJSONArray1.getJSONArray(1).getJSONArray(0);
    if (localJSONArray2.length() < 2)
    {
      InvalidResponseFromServerException localInvalidResponseFromServerException3 = new InvalidResponseFromServerException("Invalid response. Less than two entries in response.");
      throw localInvalidResponseFromServerException3;
    }
    JSONArray localJSONArray3;
    Object localObject1;
    Object localObject2;
    label220: String str2;
    label247: CloudRecognitionResult localCloudRecognitionResult;
    if (this.settings.useTranslateApi)
    {
      if (localJSONArray2.length() > 5)
        logVi(1, "CloudRecognizer", "Strange response. " + localJSONArray2.length() + " entries.");
      String str1 = localJSONArray2.getString(0);
      localJSONArray3 = localJSONArray2.getJSONArray(1);
      localObject1 = "";
      if (!this.settings.useTranslateApi)
        break label496;
      if (localJSONArray2.length() <= 4)
        break label439;
      localObject2 = localJSONArray2.getJSONArray(2);
      localObject1 = localJSONArray2.getString(4);
      StringBuilder localStringBuilder = new StringBuilder().append((String)localObject1);
      if (((String)localObject1).length() <= 0)
        break label601;
      str2 = " IME:";
      String str3 = str2 + (System.currentTimeMillis() - paramLong) + "ms";
      localCloudRecognitionResult = new CloudRecognitionResult(str1);
      localCloudRecognitionResult.setDebugInfo(str3);
    }
    for (int j = 0; ; j++)
    {
      ArrayList localArrayList;
      while (true)
      {
        while (true)
        {
          int k = localJSONArray3.length();
          if (j >= k)
            break label643;
          localArrayList = new ArrayList(0);
          if ((localObject2 == null) || (((JSONArray)localObject2).length() <= j))
            break label609;
          JSONArray localJSONArray4 = ((JSONArray)localObject2).getJSONArray(j);
          localArrayList = new ArrayList(localJSONArray4.length());
          for (int m = 0; m < localJSONArray4.length(); m++)
            localArrayList.add(localJSONArray4.getString(m));
          if (localJSONArray2.length() <= 4)
            break;
          logVi(1, "CloudRecognizer", "Strange response. " + localJSONArray2.length() + " entries.");
          break;
          label439: int i2 = localJSONArray2.length();
          localObject2 = null;
          if (i2 <= 2)
            break label220;
          try
          {
            JSONArray localJSONArray7 = localJSONArray2.getJSONArray(2);
            localObject2 = localJSONArray7;
          }
          catch (JSONException localJSONException3)
          {
            logVi(1, "CloudRecognizer", "3 entries, but no completions array -> debug_info");
            localObject1 = localJSONArray2.getString(2);
            localObject2 = null;
          }
        }
        break label220;
        label496: int i = localJSONArray2.length();
        localObject2 = null;
        if (i > 3)
          localObject2 = localJSONArray2.getJSONArray(2);
        try
        {
          String str5 = localJSONArray2.getJSONObject(3).getString("debug_info");
          localObject1 = str5;
          if (localJSONArray2.length() != 3)
            break label220;
        }
        catch (JSONException localJSONException2)
        {
          try
          {
            JSONArray localJSONArray6 = localJSONArray2.getJSONArray(2);
            localObject2 = localJSONArray6;
            break label220;
            localJSONException2 = localJSONException2;
            localObject1 = localJSONArray2.getString(3);
          }
          catch (JSONException localJSONException1)
          {
            logVi(1, "CloudRecognizer", "3 entries, but no completions array -> debug_info");
            localObject1 = localJSONArray2.getString(2);
            localObject2 = null;
          }
        }
      }
      break label220;
      label601: str2 = "IME:";
      break label247;
      label609: ScoredCandidate localScoredCandidate = new ScoredCandidate(localJSONArray3.getString(j), j, localArrayList);
      localCloudRecognitionResult.add(localScoredCandidate);
    }
    label643: JSONArray localJSONArray5;
    if ((this.settings.useTranslateApi) && (localJSONArray2.length() > 3))
    {
      localJSONArray5 = localJSONArray2.getJSONArray(3).getJSONArray(0);
      localCloudRecognitionResult.targetTexts = new ArrayList(localJSONArray5.length());
    }
    for (int n = 0; ; n++)
    {
      int i1 = localJSONArray5.length();
      String str4;
      if (n < i1)
      {
        str4 = localJSONArray5.getString(n);
        if (str4.length() != 0);
      }
      else
      {
        return localCloudRecognitionResult;
      }
      localCloudRecognitionResult.targetTexts.add(str4);
    }
  }

  public RecognitionResult recognize(StrokeList paramStrokeList, HandwritingRecognizer.CancelStruct paramCancelStruct)
    throws HandwritingRecognizer.RecognitionFailedException
  {
    long l = System.currentTimeMillis();
    try
    {
      timing(l, 0, "before creating JSON");
      String str = buildRecognitionRequestString(paramStrokeList);
      timing(l, 0, "JSON created");
      RecognitionResult localRecognitionResult = parseServerResponse(executeHttpRequest(str, l, 0), l, paramStrokeList);
      if (this.settings.useTranslateApi)
      {
        StringBuilder localStringBuilder = new StringBuilder();
        if (paramStrokeList.getPreContext() != null)
          localStringBuilder.append(paramStrokeList.getPreContext());
        if (paramStrokeList.getPostContext() != null)
          localStringBuilder.append(paramStrokeList.getPostContext());
        ((CloudRecognitionResult)localRecognitionResult).sourceText = localStringBuilder.toString();
      }
      return localRecognitionResult;
    }
    catch (HttpResponseException localHttpResponseException)
    {
      throw new HandwritingRecognizer.RecognitionFailedException("network problems", localHttpResponseException);
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      throw new HandwritingRecognizer.RecognitionFailedException("encoding problem", localUnsupportedEncodingException);
    }
    catch (ClientProtocolException localClientProtocolException)
    {
      throw new HandwritingRecognizer.RecognitionFailedException("protocol problem", localClientProtocolException);
    }
    catch (IOException localIOException)
    {
      throw new HandwritingRecognizer.RecognitionFailedException("network io problem", localIOException);
    }
    catch (JSONException localJSONException)
    {
      throw new HandwritingRecognizer.RecognitionFailedException("json problem", localJSONException);
    }
    catch (InvalidResponseFromServerException localInvalidResponseFromServerException)
    {
      throw new HandwritingRecognizer.RecognitionFailedException("invalid response: " + localInvalidResponseFromServerException.errorMessage, localInvalidResponseFromServerException);
    }
  }

  public void recordOrSendFeedback(Feedback paramFeedback)
    throws HandwritingRecognizer.SendingFeedbackFailedException
  {
    this.feedbacks.add(paramFeedback);
    if ((this.settings.sendFeedbackImmediately) || (this.feedbacks.size() >= this.settings.feedbackBatchSize));
    try
    {
      String str = executeHttpRequest(buildFeedbacksRequest(this.feedbacks), System.currentTimeMillis(), 0);
      parseServerResponse(str, 0L, null);
      Log.i("CloudRecognizer", "Sending feedback succeeded: " + str);
      return;
    }
    catch (Exception localException)
    {
      throw new HandwritingRecognizer.SendingFeedbackFailedException("Sending feedback failed", localException);
    }
  }

  public void setSecondaryRecognizer(HandwritingRecognizer paramHandwritingRecognizer)
  {
  }

  public static class CloudRecognitionResult extends RecognitionResult
  {
    public String sourceText;
    public List<String> targetTexts;

    public CloudRecognitionResult(String paramString)
    {
      super();
    }

    public CloudRecognitionResult(String paramString, List<ScoredCandidate> paramList)
    {
      super(paramList);
    }
  }

  public static class CloudRecognizerSettings extends HandwritingRecognizer.HandwritingRecognizerSettings
  {
    public boolean compressRequests = true;
    public int feedbackBatchSize = 5;
    public String recoPath = "/request";
    public boolean sendFeedbackImmediately = true;
    public String server = "https://inputtools.google.com";
  }

  public static class Feedback
  {
    final String debugInfo;
    final String label;
    final String mode;
    final String resultId;
    final StrokeList strokes;

    public Feedback(String paramString1, StrokeList paramStrokeList, String paramString2, String paramString3)
    {
      this.label = paramString1;
      this.strokes = paramStrokeList;
      this.resultId = null;
      this.mode = paramString2;
      this.debugInfo = paramString3;
    }

    public Feedback(String paramString1, String paramString2, String paramString3, String paramString4)
    {
      this.label = paramString1;
      this.strokes = null;
      this.resultId = paramString2;
      this.mode = paramString3;
      this.debugInfo = paramString4;
    }

    public JSONObject asJsonObject(String paramString)
      throws JSONException
    {
      JSONObject localJSONObject = new JSONObject();
      localJSONObject.put("feedback", this.label);
      localJSONObject.put("language", paramString);
      if ((this.debugInfo != null) && (this.debugInfo.length() > 0))
        localJSONObject.put("debug_info", this.debugInfo);
      localJSONObject.put("select_type", this.mode);
      if (this.resultId == null)
      {
        CloudRecognizer.addWritingGuide(localJSONObject, this.strokes);
        localJSONObject.put("ink", this.strokes.asJsonArray());
        CloudRecognizer.addContext(localJSONObject, this.strokes.getPreContext(), this.strokes.getPostContext());
        return localJSONObject;
      }
      localJSONObject.put("ink_hash", this.resultId);
      return localJSONObject;
    }
  }

  class InvalidResponseFromServerException extends Exception
  {
    final String errorMessage;

    InvalidResponseFromServerException(String arg2)
    {
      Object localObject;
      this.errorMessage = localObject;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.research.handwriting.networkrecognizer.CloudRecognizer
 * JD-Core Version:    0.6.2
 */