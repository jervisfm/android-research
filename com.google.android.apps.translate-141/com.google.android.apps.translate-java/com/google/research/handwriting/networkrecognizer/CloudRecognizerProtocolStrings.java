package com.google.research.handwriting.networkrecognizer;

public class CloudRecognizerProtocolStrings
{
  public static final String API_LEVEL = "api_level";
  public static final String APP = "app";
  public static final String APP_VERSION = "app_version";
  public static final String CLIENT_SOURCE = "cs";
  public static final String CLIENT_SOURCE_VALUE = "1";
  public static final String DBG = "dbg";
  public static final String DBG_VALUE = "0";
  public static final String DEBUG_INFO = "debug_info";
  public static final String DEVICE = "device";
  public static final String ENABLE_PRE_SPACE = "enable_pre_space";
  public static final String FEEDBACK = "feedback";
  public static final String FEEDBACK_COMPLETION = "feedback_completion";
  public static final String HANDWRITING = "handwriting";
  public static final String IME = "ime";
  public static final String INK = "ink";
  public static final String INK_HASH = "ink_hash";
  public static final String INPUT_TYPE = "input_type";
  public static final String LANGUAGE = "language";
  public static final String OPTIONS = "options";
  public static final String OUTPUT_ENCODING = "oe";
  public static final String OUTPUT_ENCODING_VALUE = "UTF-8";
  public static final String POST_CONTEXT = "post_context";
  public static final String PRE_CONTEXT = "pre_context";
  public static final String REQUESTS = "requests";
  public static final String SELECT_TYPE = "select_type";
  public static final String SUCCESS = "SUCCESS";
  public static final String TRANSLATE_API_SOURCE_LANGUAGE = "sl";
  public static final String TRANSLATE_API_TARGET_LANGUAGE = "tl";
  public static final String TRANSLATE_CLIENT = "client";
  public static final String TRANSLATE_CLIENT_VALUE = "at";
  private static final String URL_TEMPLATE_FOR_TRANSLATE_API = String.format("%%s%%s?%s=%s&%s=%s&%s=%%s&%s=%s&%s=%s&%s=%s&%s=%%s&%s=%%s", new Object[] { "ime", "handwriting", "client", "at", "app", "dbg", "0", "cs", "1", "oe", "UTF-8", "sl", "tl" });
  private static final String URL_TEMPLATE_WITHOUT_LANGUAGE;
  private static final String URL_TEMPLATE_WITH_LANGUAGE = String.format("%%s%%s?%s=%s_%%s&%s=%%s&%s=%s&%s=%s&%s=%s", new Object[] { "ime", "handwriting", "app", "dbg", "0", "cs", "1", "oe", "UTF-8" });
  public static final String USER_ID = "user_id";
  public static final String WRITING_GUIDE = "writing_guide";
  public static final String WRITING_GUIDE_HEIGHT = "writing_area_height";
  public static final String WRITING_GUIDE_WIDTH = "writing_area_width";

  static
  {
    URL_TEMPLATE_WITHOUT_LANGUAGE = String.format("%%s%%s?%s=%s&%s=%%s&%s=%s&%s=%s&%s=%s", new Object[] { "ime", "handwriting", "app", "dbg", "0", "cs", "1", "oe", "UTF-8" });
  }

  public static String recognitionUrl(String paramString1, String paramString2, String paramString3, boolean paramBoolean, String paramString4, String paramString5)
  {
    if (paramBoolean)
      return String.format(URL_TEMPLATE_FOR_TRANSLATE_API, new Object[] { paramString1, paramString2, paramString3, paramString4, paramString5 });
    return String.format(URL_TEMPLATE_WITHOUT_LANGUAGE, new Object[] { paramString1, paramString2, paramString3 });
  }

  public static String recognitionUrlWithLanguage(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    return String.format(URL_TEMPLATE_WITH_LANGUAGE, new Object[] { paramString1, paramString2, paramString3, paramString4 });
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.research.handwriting.networkrecognizer.CloudRecognizerProtocolStrings
 * JD-Core Version:    0.6.2
 */