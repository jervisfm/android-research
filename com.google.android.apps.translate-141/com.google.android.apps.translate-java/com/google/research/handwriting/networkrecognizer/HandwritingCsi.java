package com.google.research.handwriting.networkrecognizer;

import android.os.AsyncTask;
import android.os.SystemClock;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public class HandwritingCsi
{
  private static final String ACTION_VAR = "&action=";
  private static final String CSI_CLIENT_RECO_PREFIX = "r";
  private static final String CSI_LANG_TIME_PREFIX = "t";
  private static final String CSI_RECO = "rrs";
  private static final String CSI_TIME = "t";
  private static final boolean DEBUG = false;
  private static final String EXPERIMENT_ID = "&e=";
  private static final String INTERVAL_VAR = "&it=";
  private static final int MAX_MAP_SIZE = 100;
  private static final String SERVER_URL = "http://csi.gstatic.com/csi?";
  private static final String SERVER_URL_BACKUP = "http://csi.gstatic.cn/csi?";
  private static final String SERVICE_VAR = "&s=";
  private static final String TAG = "HandwritingCsi";
  private static final String VERSION_VAR = "&v=";
  public static boolean shouldTryBackupUrl = false;
  private String clientName = "";
  private final HttpClient httpClient;
  private int numRecordedActions = 0;
  final HashMap<String, List<RecordedAction>> recordedActions = new HashMap();
  private int reportEveryN = 0;
  private final HashMap<Integer, RunningAction> runningActions = new HashMap();
  private final String serviceName;
  private final int version = 2;

  public HandwritingCsi(String paramString)
  {
    this.serviceName = paramString;
    this.httpClient = new DefaultHttpClient();
  }

  public HandwritingCsi(String paramString, HttpClient paramHttpClient)
  {
    this.serviceName = paramString;
    this.httpClient = paramHttpClient;
  }

  private String actionName()
  {
    Object[] arrayOfObject = new Object[3];
    arrayOfObject[0] = "rrs";
    arrayOfObject[1] = "r";
    arrayOfObject[2] = this.clientName;
    return String.format("%s,%s_%s", arrayOfObject);
  }

  private List<String> actionVariables(String paramString)
  {
    LinkedList localLinkedList = new LinkedList();
    localLinkedList.add("t");
    localLinkedList.add("t_" + paramString);
    return localLinkedList;
  }

  private String buildUrl(List<RecordedAction> paramList, String paramString)
  {
    String str = ((RecordedAction)paramList.get(0)).name;
    StringBuilder localStringBuilder = new StringBuilder(paramString);
    localStringBuilder.append("&v=").append(2);
    localStringBuilder.append("&s=").append(this.serviceName);
    localStringBuilder.append("&action=").append(str);
    if (!((RecordedAction)paramList.get(0)).experiment.equals(""))
      localStringBuilder.append("&e=").append(((RecordedAction)paramList.get(0)).experiment);
    localStringBuilder.append("&it=");
    Iterator localIterator1 = paramList.iterator();
    while (localIterator1.hasNext())
    {
      RecordedAction localRecordedAction = (RecordedAction)localIterator1.next();
      Iterator localIterator2 = localRecordedAction.vars.iterator();
      while (localIterator2.hasNext())
      {
        localStringBuilder.append((String)localIterator2.next());
        localStringBuilder.append(".");
        localStringBuilder.append(localRecordedAction.duration);
        localStringBuilder.append(",");
      }
    }
    if (!paramList.isEmpty())
      localStringBuilder.deleteCharAt(-1 + localStringBuilder.length());
    return localStringBuilder.toString();
  }

  private String languageCode(String paramString)
  {
    if (paramString.length() >= 2)
      return paramString.substring(0, 2);
    return "";
  }

  private void record(RecordedAction paramRecordedAction)
  {
    String str = paramRecordedAction.name + paramRecordedAction.experiment;
    if (!this.recordedActions.containsKey(str))
      this.recordedActions.put(str, new LinkedList());
    ((List)this.recordedActions.get(str)).add(paramRecordedAction);
    this.numRecordedActions = (1 + this.numRecordedActions);
    if (this.numRecordedActions >= this.reportEveryN)
      startReporting();
  }

  private void recordAction(String paramString, List<String> paramList, long paramLong)
  {
    record(new RecordedAction(paramString, paramList, paramLong));
  }

  private void startAction(String paramString, List<String> paramList, int paramInt)
  {
    long l = SystemClock.elapsedRealtime();
    if (this.runningActions.containsKey(Integer.valueOf(paramInt)));
    this.runningActions.put(Integer.valueOf(paramInt), new RunningAction(Integer.valueOf(paramInt), paramString, paramList, l));
  }

  private void startActionWithExperiment(String paramString1, List<String> paramList, int paramInt, String paramString2)
  {
    long l = SystemClock.elapsedRealtime();
    if (this.runningActions.containsKey(Integer.valueOf(paramInt)));
    this.runningActions.put(Integer.valueOf(paramInt), new RunningAction(Integer.valueOf(paramInt), paramString1, paramList, l, paramString2));
  }

  private void startReporting()
  {
    final HashMap localHashMap = new HashMap();
    localHashMap.putAll(this.recordedActions);
    this.recordedActions.clear();
    this.numRecordedActions = 0;
    if (this.runningActions.size() > 100)
      this.runningActions.clear();
    new AsyncTask()
    {
      protected Void doInBackground(Void[] paramAnonymousArrayOfVoid)
      {
        HandwritingCsi.this.report(localHashMap);
        return null;
      }
    }
    .execute(new Void[0]);
  }

  private void stopAction(int paramInt)
  {
    if (!this.runningActions.containsKey(Integer.valueOf(paramInt)))
      return;
    long l = SystemClock.elapsedRealtime();
    record(new RecordedAction((RunningAction)this.runningActions.remove(Integer.valueOf(paramInt)), l));
  }

  protected void finalize()
    throws Throwable
  {
    startReporting();
    super.finalize();
  }

  public void recordRecognition(String paramString, long paramLong)
  {
    recordAction(actionName(), actionVariables(paramString), paramLong);
  }

  void report(HashMap<String, List<RecordedAction>> paramHashMap)
  {
    Iterator localIterator1 = paramHashMap.keySet().iterator();
    boolean bool = localIterator1.hasNext();
    int i = 0;
    if (bool)
      str2 = buildUrl((List)paramHashMap.get((String)localIterator1.next()), "http://csi.gstatic.com/csi?");
    while ((i == 0) || (!shouldTryBackupUrl))
      try
      {
        String str2;
        int k = this.httpClient.execute(new HttpGet(str2)).getStatusLine().getStatusCode();
        if (k == 204)
          break;
        return;
      }
      catch (Exception localException2)
      {
        i = 1;
      }
    Iterator localIterator2 = paramHashMap.keySet().iterator();
    while (localIterator2.hasNext())
    {
      String str1 = buildUrl((List)paramHashMap.get((String)localIterator2.next()), "http://csi.gstatic.cn/csi?");
      try
      {
        int j = this.httpClient.execute(new HttpGet(str1)).getStatusLine().getStatusCode();
        if (j != 204);
      }
      catch (Exception localException1)
      {
      }
    }
  }

  public void setClientName(String paramString)
  {
    this.clientName = paramString;
  }

  public void setReportEveryN(int paramInt)
  {
    this.reportEveryN = paramInt;
  }

  public void startRecognition(String paramString, int paramInt)
  {
    String str = languageCode(paramString);
    if (str.contentEquals(paramString))
    {
      startAction(actionName(), actionVariables(str), paramInt);
      return;
    }
    startActionWithExperiment(actionName(), actionVariables(str), paramInt, paramString);
  }

  public void stopRecognition(int paramInt)
  {
    stopAction(paramInt);
  }

  private class RecordedAction
  {
    private final long duration;
    private final String experiment;
    private final String name;
    private final List<String> vars;

    RecordedAction(HandwritingCsi.RunningAction paramLong, long arg3)
    {
      this.name = HandwritingCsi.RunningAction.access$000(paramLong);
      this.vars = HandwritingCsi.RunningAction.access$100(paramLong);
      Object localObject;
      this.duration = (localObject - HandwritingCsi.RunningAction.access$200(paramLong));
      this.experiment = HandwritingCsi.RunningAction.access$300(paramLong);
    }

    RecordedAction(List<String> paramLong, long arg3)
    {
      this.name = paramLong;
      Object localObject1;
      this.vars = localObject1;
      Object localObject2;
      this.duration = localObject2;
      this.experiment = "";
    }
  }

  private class RunningAction
  {
    private final String experiment;
    private final String name;
    private final long started;
    private final List<String> vars;

    RunningAction(String paramList, List<String> paramLong, long arg4)
    {
      this.name = paramLong;
      Object localObject1;
      this.vars = localObject1;
      Object localObject2;
      this.started = localObject2;
      this.experiment = "";
    }

    RunningAction(String paramList, List<String> paramLong, long arg4, String arg6)
    {
      this.name = paramLong;
      this.vars = ???;
      this.started = paramString1;
      Object localObject;
      this.experiment = localObject;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.research.handwriting.networkrecognizer.HandwritingCsi
 * JD-Core Version:    0.6.2
 */