package com.google.research.handwriting.networkrecognizer;

import android.content.Context;
import org.apache.http.HttpResponse;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.ConnectionKeepAliveStrategy;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HttpContext;

public class HandwritingHttpClient extends DefaultHttpClient
{
  protected static final long CONNECTION_REUSE_MILLISECONDS = 100000L;
  protected static final int CONNECTION_TIMEOUT_MILLIS = 30000;
  protected static final int SOCKET_TIMEOUT_MILLIS = 30000;
  protected String status;

  protected HandwritingHttpClient(ClientConnectionManager paramClientConnectionManager, HttpParams paramHttpParams)
  {
    super(paramClientConnectionManager, paramHttpParams);
    setKeepAliveStrategy(new ConnectionKeepAliveStrategy()
    {
      public long getKeepAliveDuration(HttpResponse paramAnonymousHttpResponse, HttpContext paramAnonymousHttpContext)
      {
        return 100000L;
      }
    });
  }

  public static HandwritingHttpClient getNewHttpClient()
  {
    SchemeRegistry localSchemeRegistry = new SchemeRegistry();
    localSchemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
    SSLSocketFactory.getSocketFactory().setHostnameVerifier((X509HostnameVerifier)SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
    localSchemeRegistry.register(new Scheme("https", SSLSocketFactory.getSocketFactory(), 443));
    BasicHttpParams localBasicHttpParams = new BasicHttpParams();
    HttpConnectionParams.setConnectionTimeout(localBasicHttpParams, 30000);
    HttpConnectionParams.setSoTimeout(localBasicHttpParams, 30000);
    return new HandwritingHttpClient(new ThreadSafeClientConnManager(localBasicHttpParams, localSchemeRegistry), localBasicHttpParams);
  }

  public void authenticate(Context paramContext, RunAfterAuthentication paramRunAfterAuthentication)
  {
  }

  public boolean isAuthenticated()
  {
    return false;
  }

  public static abstract interface RunAfterAuthentication
  {
    public abstract void onAuthenticated();

    public abstract void onAuthenticationFailed(String paramString);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.research.handwriting.networkrecognizer.HandwritingHttpClient
 * JD-Core Version:    0.6.2
 */