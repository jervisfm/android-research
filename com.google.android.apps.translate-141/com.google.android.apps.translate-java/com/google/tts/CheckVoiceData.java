package com.google.tts;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import java.io.File;
import java.util.ArrayList;

public class CheckVoiceData extends Activity
{
  private static final String ESPEAK_DATA_PATH = Environment.getExternalStorageDirectory() + "/espeak-data/";
  private static final String[] baseDataFiles = { "af_dict", "config", "cs_dict", "cy_dict", "de_dict", "el_dict", "en_dict", "eo_dict", "es_dict", "fi_dict", "fr_dict", "grc_dict", "hbs_dict", "hi_dict", "hu_dict", "id_dict", "is_dict", "it_dict", "jbo_dict", "ku_dict", "la_dict", "mk_dict", "nl_dict", "no_dict", "phondata", "phonindex", "phontab", "pl_dict", "pt_dict", "ro_dict", "ru_dict", "sk_dict", "sv_dict", "sw_dict", "ta_dict", "tr_dict", "vi_dict", "zh_dict", "zhy_dict", "mbrola/dummyfile", "mbrola_ph/af1_phtrans", "mbrola_ph/ca1_phtrans", "mbrola_ph/cr1_phtrans", "mbrola_ph/cs_phtrans", "mbrola_ph/de2_phtrans", "mbrola_ph/de4_phtrans", "mbrola_ph/de6_phtrans", "mbrola_ph/en1_phtrans", "mbrola_ph/es_phtrans", "mbrola_ph/es4_phtrans", "mbrola_ph/fr1_phtrans", "mbrola_ph/gr2_phtrans", "mbrola_ph/grc-de6_phtrans", "mbrola_ph/hu1_phtrans", "mbrola_ph/id1_phtrans", "mbrola_ph/in1_phtrans", "mbrola_ph/it3_phtrans", "mbrola_ph/la1_phtrans", "mbrola_ph/nl_phtrans", "mbrola_ph/pl1_phtrans", "mbrola_ph/pt_phtrans", "mbrola_ph/ptbr_phtrans", "mbrola_ph/ptbr4_phtrans", "mbrola_ph/ro1_phtrans", "mbrola_ph/sv_phtrans", "mbrola_ph/sv2_phtrans", "mbrola_ph/us_phtrans", "mbrola_ph/us3_phtrans", "soundicons/dummyfile", "voices/af", "voices/bs", "voices/cs", "voices/cy", "voices/de", "voices/default", "voices/el", "voices/eo", "voices/es", "voices/es-la", "voices/fi", "voices/fr", "voices/fr-be", "voices/grc", "voices/hi", "voices/hr", "voices/hu", "voices/id", "voices/is", "voices/it", "voices/jbo", "voices/ku", "voices/la", "voices/mk", "voices/nl", "voices/no", "voices/pl", "voices/pt", "voices/pt-pt", "voices/ro", "voices/ru", "voices/sk", "voices/sr", "voices/sv", "voices/sw", "voices/ta", "voices/tr", "voices/vi", "voices/zh", "voices/zhy", "voices/!v/croak", "voices/!v/f1", "voices/!v/f2", "voices/!v/f3", "voices/!v/f4", "voices/!v/m1", "voices/!v/m2", "voices/!v/m3", "voices/!v/m4", "voices/!v/m5", "voices/!v/m6", "voices/!v/whisper", "voices/en/en", "voices/en/en-n", "voices/en/en-r", "voices/en/en-rp", "voices/en/en-sc", "voices/en/en-wi", "voices/en/en-wm", "voices/mb/mb-af1", "voices/mb/mb-af1-en", "voices/mb/mb-br1", "voices/mb/mb-br3", "voices/mb/mb-br4", "voices/mb/mb-cr1", "voices/mb/mb-cz2", "voices/mb/mb-de2", "voices/mb/mb-de4", "voices/mb/mb-de4-en", "voices/mb/mb-de5", "voices/mb/mb-de5-en", "voices/mb/mb-de6", "voices/mb/mb-de6-grc", "voices/mb/mb-de7", "voices/mb/mb-en1", "voices/mb/mb-es1", "voices/mb/mb-es2", "voices/mb/mb-fr1", "voices/mb/mb-fr1-en", "voices/mb/mb-fr4", "voices/mb/mb-fr4-en", "voices/mb/mb-gr2", "voices/mb/mb-gr2-en", "voices/mb/mb-hu1", "voices/mb/mb-hu1-en", "voices/mb/mb-id1", "voices/mb/mb-it3", "voices/mb/mb-it4", "voices/mb/mb-la1", "voices/mb/mb-nl2", "voices/mb/mb-nl2-en", "voices/mb/mb-pl1", "voices/mb/mb-pl1-en", "voices/mb/mb-ro1", "voices/mb/mb-ro1-en", "voices/mb/mb-sw1", "voices/mb/mb-sw1-en", "voices/mb/mb-sw2", "voices/mb/mb-sw2-en", "voices/mb/mb-us1", "voices/mb/mb-us2", "voices/mb/mb-us3" };
  private static final String[] supportedLanguages = { "afr", "bos", "zho", "hrv", "ces", "nld", "eng", "eng-USA", "eng-GBR", "epo", "fin", "fra", "deu", "ell", "hin", "hun", "isl", "ind", "ita", "kur", "lat", "mkd", "nor", "pol", "por", "ron", "rus", "srp", "slk", "spa", "spa-MEX", "swa", "swe", "tam", "tur", "vie", "cym" };

  private boolean fileExists(String paramString)
  {
    File localFile1 = new File(ESPEAK_DATA_PATH + paramString);
    File localFile2 = new File(ESPEAK_DATA_PATH + paramString);
    return (localFile1.exists()) || (localFile2.exists());
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    Intent localIntent = new Intent();
    localIntent.putExtra("dataRoot", ESPEAK_DATA_PATH);
    localIntent.putExtra("dataFiles", baseDataFiles);
    localIntent.putExtra("dataFilesInfo", baseDataFiles);
    ArrayList localArrayList1 = new ArrayList();
    ArrayList localArrayList2 = new ArrayList();
    int i = 1;
    int j = 0;
    label79: int m;
    if (j >= baseDataFiles.length)
    {
      if (i == 0)
        break label170;
      m = 0;
      label87: if (m < supportedLanguages.length)
        break label152;
    }
    while (true)
    {
      localIntent.putStringArrayListExtra("availableVoices", localArrayList1);
      localIntent.putStringArrayListExtra("unavailableVoices", localArrayList2);
      setResult(1, localIntent);
      finish();
      return;
      if (!fileExists(baseDataFiles[j]))
      {
        i = 0;
        break label79;
      }
      j++;
      break;
      label152: localArrayList1.add(supportedLanguages[m]);
      m++;
      break label87;
      label170: for (int k = 0; k < supportedLanguages.length; k++)
        localArrayList2.add(supportedLanguages[k]);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.tts.CheckVoiceData
 * JD-Core Version:    0.6.2
 */