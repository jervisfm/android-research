package com.google.tts;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

public class DownloadVoiceData extends Activity
{
  private static final String MARKET_URI = "market://search?q=pname:com.svox.langpack.installer";

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    startActivityForResult(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=pname:com.svox.langpack.installer")), 0);
    finish();
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.tts.DownloadVoiceData
 * JD-Core Version:    0.6.2
 */