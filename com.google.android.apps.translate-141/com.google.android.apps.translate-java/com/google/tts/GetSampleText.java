package com.google.tts;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class GetSampleText extends Activity
{
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    int i = 0;
    Intent localIntent1 = new Intent();
    Intent localIntent2 = getIntent();
    String str = localIntent2.getExtras().getString("language");
    localIntent2.getExtras().getString("country");
    localIntent2.getExtras().getString("variant");
    if (str.equals("afr"))
      localIntent1.putExtra("sampleText", getString(2131165216));
    while (true)
    {
      setResult(i, localIntent1);
      finish();
      return;
      if (str.equals("bos"))
      {
        localIntent1.putExtra("sampleText", getString(2131165217));
        i = 0;
      }
      else if (str.equals("zho"))
      {
        localIntent1.putExtra("sampleText", getString(2131165218));
        i = 0;
      }
      else if (str.equals("hrv"))
      {
        localIntent1.putExtra("sampleText", getString(2131165219));
        i = 0;
      }
      else if (str.equals("ces"))
      {
        localIntent1.putExtra("sampleText", getString(2131165220));
        i = 0;
      }
      else if (str.equals("nld"))
      {
        localIntent1.putExtra("sampleText", getString(2131165221));
        i = 0;
      }
      else if (str.equals("eng"))
      {
        localIntent1.putExtra("sampleText", getString(2131165222));
        i = 0;
      }
      else if (str.equals("epo"))
      {
        localIntent1.putExtra("sampleText", getString(2131165223));
        i = 0;
      }
      else if (str.equals("fin"))
      {
        localIntent1.putExtra("sampleText", getString(2131165224));
        i = 0;
      }
      else if (str.equals("fra"))
      {
        localIntent1.putExtra("sampleText", getString(2131165225));
        i = 0;
      }
      else if (str.equals("deu"))
      {
        localIntent1.putExtra("sampleText", getString(2131165226));
        i = 0;
      }
      else if (str.equals("ell"))
      {
        localIntent1.putExtra("sampleText", getString(2131165227));
        i = 0;
      }
      else if (str.equals("hin"))
      {
        localIntent1.putExtra("sampleText", getString(2131165228));
        i = 0;
      }
      else if (str.equals("hun"))
      {
        localIntent1.putExtra("sampleText", getString(2131165229));
        i = 0;
      }
      else if (str.equals("isl"))
      {
        localIntent1.putExtra("sampleText", getString(2131165230));
        i = 0;
      }
      else if (str.equals("ind"))
      {
        localIntent1.putExtra("sampleText", getString(2131165231));
        i = 0;
      }
      else if (str.equals("ita"))
      {
        localIntent1.putExtra("sampleText", getString(2131165232));
        i = 0;
      }
      else if (str.equals("kur"))
      {
        localIntent1.putExtra("sampleText", getString(2131165233));
        i = 0;
      }
      else if (str.equals("lat"))
      {
        localIntent1.putExtra("sampleText", getString(2131165234));
        i = 0;
      }
      else if (str.equals("mkd"))
      {
        localIntent1.putExtra("sampleText", getString(2131165235));
        i = 0;
      }
      else if (str.equals("nor"))
      {
        localIntent1.putExtra("sampleText", getString(2131165236));
        i = 0;
      }
      else if (str.equals("pol"))
      {
        localIntent1.putExtra("sampleText", getString(2131165237));
        i = 0;
      }
      else if (str.equals("por"))
      {
        localIntent1.putExtra("sampleText", getString(2131165238));
        i = 0;
      }
      else if (str.equals("ron"))
      {
        localIntent1.putExtra("sampleText", getString(2131165239));
        i = 0;
      }
      else if (str.equals("rus"))
      {
        localIntent1.putExtra("sampleText", getString(2131165240));
        i = 0;
      }
      else if (str.equals("srp"))
      {
        localIntent1.putExtra("sampleText", getString(2131165241));
        i = 0;
      }
      else if (str.equals("slk"))
      {
        localIntent1.putExtra("sampleText", getString(2131165242));
        i = 0;
      }
      else if (str.equals("spa"))
      {
        localIntent1.putExtra("sampleText", getString(2131165243));
        i = 0;
      }
      else if (str.equals("swa"))
      {
        localIntent1.putExtra("sampleText", getString(2131165244));
        i = 0;
      }
      else if (str.equals("swe"))
      {
        localIntent1.putExtra("sampleText", getString(2131165245));
        i = 0;
      }
      else if (str.equals("tam"))
      {
        localIntent1.putExtra("sampleText", getString(2131165246));
        i = 0;
      }
      else if (str.equals("tur"))
      {
        localIntent1.putExtra("sampleText", getString(2131165247));
        i = 0;
      }
      else if (str.equals("vie"))
      {
        localIntent1.putExtra("sampleText", getString(2131165248));
        i = 0;
      }
      else if (str.equals("cym"))
      {
        localIntent1.putExtra("sampleText", getString(2131165249));
        i = 0;
      }
      else
      {
        i = -2;
        localIntent1.putExtra("sampleText", "");
      }
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.tts.GetSampleText
 * JD-Core Version:    0.6.2
 */