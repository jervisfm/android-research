package com.google.tts;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public abstract interface ITTSCallback extends IInterface
{
  public abstract void markReached(String paramString)
    throws RemoteException;

  public static abstract class Stub extends Binder
    implements ITTSCallback
  {
    private static final String DESCRIPTOR = "com.google.tts.ITTSCallback";
    static final int TRANSACTION_markReached = 1;

    public Stub()
    {
      attachInterface(this, "com.google.tts.ITTSCallback");
    }

    public static ITTSCallback asInterface(IBinder paramIBinder)
    {
      if (paramIBinder == null)
        return null;
      IInterface localIInterface = paramIBinder.queryLocalInterface("com.google.tts.ITTSCallback");
      if ((localIInterface != null) && ((localIInterface instanceof ITTSCallback)))
        return (ITTSCallback)localIInterface;
      return new Proxy(paramIBinder);
    }

    public IBinder asBinder()
    {
      return this;
    }

    public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
      throws RemoteException
    {
      switch (paramInt1)
      {
      default:
        return super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
      case 1598968902:
        paramParcel2.writeString("com.google.tts.ITTSCallback");
        return true;
      case 1:
      }
      paramParcel1.enforceInterface("com.google.tts.ITTSCallback");
      markReached(paramParcel1.readString());
      return true;
    }

    private static class Proxy
      implements ITTSCallback
    {
      private IBinder mRemote;

      Proxy(IBinder paramIBinder)
      {
        this.mRemote = paramIBinder;
      }

      public IBinder asBinder()
      {
        return this.mRemote;
      }

      public String getInterfaceDescriptor()
      {
        return "com.google.tts.ITTSCallback";
      }

      public void markReached(String paramString)
        throws RemoteException
      {
        Parcel localParcel = Parcel.obtain();
        try
        {
          localParcel.writeInterfaceToken("com.google.tts.ITTSCallback");
          localParcel.writeString(paramString);
          this.mRemote.transact(1, localParcel, null, 1);
          return;
        }
        finally
        {
          localParcel.recycle();
        }
      }
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.tts.ITTSCallback
 * JD-Core Version:    0.6.2
 */