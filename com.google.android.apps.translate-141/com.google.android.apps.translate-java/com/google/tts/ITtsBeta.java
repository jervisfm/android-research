package com.google.tts;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public abstract interface ITtsBeta extends IInterface
{
  public abstract void addEarcon(String paramString1, String paramString2, String paramString3, int paramInt)
    throws RemoteException;

  public abstract void addEarconFile(String paramString1, String paramString2, String paramString3)
    throws RemoteException;

  public abstract void addSpeech(String paramString1, String paramString2, String paramString3, int paramInt)
    throws RemoteException;

  public abstract void addSpeechFile(String paramString1, String paramString2, String paramString3)
    throws RemoteException;

  public abstract boolean areDefaultsEnforced()
    throws RemoteException;

  public abstract String getDefaultEngine()
    throws RemoteException;

  public abstract String[] getLanguage()
    throws RemoteException;

  public abstract int isLanguageAvailable(String paramString1, String paramString2, String paramString3, String[] paramArrayOfString)
    throws RemoteException;

  public abstract boolean isSpeaking()
    throws RemoteException;

  public abstract int playEarcon(String paramString1, String paramString2, int paramInt, String[] paramArrayOfString)
    throws RemoteException;

  public abstract int playSilence(String paramString, long paramLong, int paramInt, String[] paramArrayOfString)
    throws RemoteException;

  public abstract int registerCallback(String paramString, ITtsCallbackBeta paramITtsCallbackBeta)
    throws RemoteException;

  public abstract int setEngineByPackageName(String paramString)
    throws RemoteException;

  public abstract int setLanguage(String paramString1, String paramString2, String paramString3, String paramString4)
    throws RemoteException;

  public abstract int setPitch(String paramString, int paramInt)
    throws RemoteException;

  public abstract int setSpeechRate(String paramString, int paramInt)
    throws RemoteException;

  public abstract int speak(String paramString1, String paramString2, int paramInt, String[] paramArrayOfString)
    throws RemoteException;

  public abstract int stop(String paramString)
    throws RemoteException;

  public abstract boolean synthesizeToFile(String paramString1, String paramString2, String[] paramArrayOfString, String paramString3)
    throws RemoteException;

  public abstract int unregisterCallback(String paramString, ITtsCallbackBeta paramITtsCallbackBeta)
    throws RemoteException;

  public static abstract class Stub extends Binder
    implements ITtsBeta
  {
    private static final String DESCRIPTOR = "com.google.tts.ITtsBeta";
    static final int TRANSACTION_addEarcon = 13;
    static final int TRANSACTION_addEarconFile = 14;
    static final int TRANSACTION_addSpeech = 6;
    static final int TRANSACTION_addSpeechFile = 7;
    static final int TRANSACTION_areDefaultsEnforced = 20;
    static final int TRANSACTION_getDefaultEngine = 19;
    static final int TRANSACTION_getLanguage = 8;
    static final int TRANSACTION_isLanguageAvailable = 9;
    static final int TRANSACTION_isSpeaking = 4;
    static final int TRANSACTION_playEarcon = 12;
    static final int TRANSACTION_playSilence = 17;
    static final int TRANSACTION_registerCallback = 15;
    static final int TRANSACTION_setEngineByPackageName = 18;
    static final int TRANSACTION_setLanguage = 10;
    static final int TRANSACTION_setPitch = 2;
    static final int TRANSACTION_setSpeechRate = 1;
    static final int TRANSACTION_speak = 3;
    static final int TRANSACTION_stop = 5;
    static final int TRANSACTION_synthesizeToFile = 11;
    static final int TRANSACTION_unregisterCallback = 16;

    public Stub()
    {
      attachInterface(this, "com.google.tts.ITtsBeta");
    }

    public static ITtsBeta asInterface(IBinder paramIBinder)
    {
      if (paramIBinder == null)
        return null;
      IInterface localIInterface = paramIBinder.queryLocalInterface("com.google.tts.ITtsBeta");
      if ((localIInterface != null) && ((localIInterface instanceof ITtsBeta)))
        return (ITtsBeta)localIInterface;
      return new Proxy(paramIBinder);
    }

    public IBinder asBinder()
    {
      return this;
    }

    public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
      throws RemoteException
    {
      switch (paramInt1)
      {
      default:
        return super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
      case 1598968902:
        paramParcel2.writeString("com.google.tts.ITtsBeta");
        return true;
      case 1:
        paramParcel1.enforceInterface("com.google.tts.ITtsBeta");
        int i9 = setSpeechRate(paramParcel1.readString(), paramParcel1.readInt());
        paramParcel2.writeNoException();
        paramParcel2.writeInt(i9);
        return true;
      case 2:
        paramParcel1.enforceInterface("com.google.tts.ITtsBeta");
        int i8 = setPitch(paramParcel1.readString(), paramParcel1.readInt());
        paramParcel2.writeNoException();
        paramParcel2.writeInt(i8);
        return true;
      case 3:
        paramParcel1.enforceInterface("com.google.tts.ITtsBeta");
        int i7 = speak(paramParcel1.readString(), paramParcel1.readString(), paramParcel1.readInt(), paramParcel1.createStringArray());
        paramParcel2.writeNoException();
        paramParcel2.writeInt(i7);
        return true;
      case 4:
        paramParcel1.enforceInterface("com.google.tts.ITtsBeta");
        boolean bool3 = isSpeaking();
        paramParcel2.writeNoException();
        int i6 = 0;
        if (bool3)
          i6 = 1;
        paramParcel2.writeInt(i6);
        return true;
      case 5:
        paramParcel1.enforceInterface("com.google.tts.ITtsBeta");
        int i5 = stop(paramParcel1.readString());
        paramParcel2.writeNoException();
        paramParcel2.writeInt(i5);
        return true;
      case 6:
        paramParcel1.enforceInterface("com.google.tts.ITtsBeta");
        addSpeech(paramParcel1.readString(), paramParcel1.readString(), paramParcel1.readString(), paramParcel1.readInt());
        paramParcel2.writeNoException();
        return true;
      case 7:
        paramParcel1.enforceInterface("com.google.tts.ITtsBeta");
        addSpeechFile(paramParcel1.readString(), paramParcel1.readString(), paramParcel1.readString());
        paramParcel2.writeNoException();
        return true;
      case 8:
        paramParcel1.enforceInterface("com.google.tts.ITtsBeta");
        String[] arrayOfString = getLanguage();
        paramParcel2.writeNoException();
        paramParcel2.writeStringArray(arrayOfString);
        return true;
      case 9:
        paramParcel1.enforceInterface("com.google.tts.ITtsBeta");
        int i4 = isLanguageAvailable(paramParcel1.readString(), paramParcel1.readString(), paramParcel1.readString(), paramParcel1.createStringArray());
        paramParcel2.writeNoException();
        paramParcel2.writeInt(i4);
        return true;
      case 10:
        paramParcel1.enforceInterface("com.google.tts.ITtsBeta");
        int i3 = setLanguage(paramParcel1.readString(), paramParcel1.readString(), paramParcel1.readString(), paramParcel1.readString());
        paramParcel2.writeNoException();
        paramParcel2.writeInt(i3);
        return true;
      case 11:
        paramParcel1.enforceInterface("com.google.tts.ITtsBeta");
        boolean bool2 = synthesizeToFile(paramParcel1.readString(), paramParcel1.readString(), paramParcel1.createStringArray(), paramParcel1.readString());
        paramParcel2.writeNoException();
        int i2 = 0;
        if (bool2)
          i2 = 1;
        paramParcel2.writeInt(i2);
        return true;
      case 12:
        paramParcel1.enforceInterface("com.google.tts.ITtsBeta");
        int i1 = playEarcon(paramParcel1.readString(), paramParcel1.readString(), paramParcel1.readInt(), paramParcel1.createStringArray());
        paramParcel2.writeNoException();
        paramParcel2.writeInt(i1);
        return true;
      case 13:
        paramParcel1.enforceInterface("com.google.tts.ITtsBeta");
        addEarcon(paramParcel1.readString(), paramParcel1.readString(), paramParcel1.readString(), paramParcel1.readInt());
        paramParcel2.writeNoException();
        return true;
      case 14:
        paramParcel1.enforceInterface("com.google.tts.ITtsBeta");
        addEarconFile(paramParcel1.readString(), paramParcel1.readString(), paramParcel1.readString());
        paramParcel2.writeNoException();
        return true;
      case 15:
        paramParcel1.enforceInterface("com.google.tts.ITtsBeta");
        int n = registerCallback(paramParcel1.readString(), ITtsCallbackBeta.Stub.asInterface(paramParcel1.readStrongBinder()));
        paramParcel2.writeNoException();
        paramParcel2.writeInt(n);
        return true;
      case 16:
        paramParcel1.enforceInterface("com.google.tts.ITtsBeta");
        int m = unregisterCallback(paramParcel1.readString(), ITtsCallbackBeta.Stub.asInterface(paramParcel1.readStrongBinder()));
        paramParcel2.writeNoException();
        paramParcel2.writeInt(m);
        return true;
      case 17:
        paramParcel1.enforceInterface("com.google.tts.ITtsBeta");
        int k = playSilence(paramParcel1.readString(), paramParcel1.readLong(), paramParcel1.readInt(), paramParcel1.createStringArray());
        paramParcel2.writeNoException();
        paramParcel2.writeInt(k);
        return true;
      case 18:
        paramParcel1.enforceInterface("com.google.tts.ITtsBeta");
        int j = setEngineByPackageName(paramParcel1.readString());
        paramParcel2.writeNoException();
        paramParcel2.writeInt(j);
        return true;
      case 19:
        paramParcel1.enforceInterface("com.google.tts.ITtsBeta");
        String str = getDefaultEngine();
        paramParcel2.writeNoException();
        paramParcel2.writeString(str);
        return true;
      case 20:
      }
      paramParcel1.enforceInterface("com.google.tts.ITtsBeta");
      boolean bool1 = areDefaultsEnforced();
      paramParcel2.writeNoException();
      int i = 0;
      if (bool1)
        i = 1;
      paramParcel2.writeInt(i);
      return true;
    }

    private static class Proxy
      implements ITtsBeta
    {
      private IBinder mRemote;

      Proxy(IBinder paramIBinder)
      {
        this.mRemote = paramIBinder;
      }

      public void addEarcon(String paramString1, String paramString2, String paramString3, int paramInt)
        throws RemoteException
      {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        try
        {
          localParcel1.writeInterfaceToken("com.google.tts.ITtsBeta");
          localParcel1.writeString(paramString1);
          localParcel1.writeString(paramString2);
          localParcel1.writeString(paramString3);
          localParcel1.writeInt(paramInt);
          this.mRemote.transact(13, localParcel1, localParcel2, 0);
          localParcel2.readException();
          return;
        }
        finally
        {
          localParcel2.recycle();
          localParcel1.recycle();
        }
      }

      public void addEarconFile(String paramString1, String paramString2, String paramString3)
        throws RemoteException
      {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        try
        {
          localParcel1.writeInterfaceToken("com.google.tts.ITtsBeta");
          localParcel1.writeString(paramString1);
          localParcel1.writeString(paramString2);
          localParcel1.writeString(paramString3);
          this.mRemote.transact(14, localParcel1, localParcel2, 0);
          localParcel2.readException();
          return;
        }
        finally
        {
          localParcel2.recycle();
          localParcel1.recycle();
        }
      }

      public void addSpeech(String paramString1, String paramString2, String paramString3, int paramInt)
        throws RemoteException
      {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        try
        {
          localParcel1.writeInterfaceToken("com.google.tts.ITtsBeta");
          localParcel1.writeString(paramString1);
          localParcel1.writeString(paramString2);
          localParcel1.writeString(paramString3);
          localParcel1.writeInt(paramInt);
          this.mRemote.transact(6, localParcel1, localParcel2, 0);
          localParcel2.readException();
          return;
        }
        finally
        {
          localParcel2.recycle();
          localParcel1.recycle();
        }
      }

      public void addSpeechFile(String paramString1, String paramString2, String paramString3)
        throws RemoteException
      {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        try
        {
          localParcel1.writeInterfaceToken("com.google.tts.ITtsBeta");
          localParcel1.writeString(paramString1);
          localParcel1.writeString(paramString2);
          localParcel1.writeString(paramString3);
          this.mRemote.transact(7, localParcel1, localParcel2, 0);
          localParcel2.readException();
          return;
        }
        finally
        {
          localParcel2.recycle();
          localParcel1.recycle();
        }
      }

      public boolean areDefaultsEnforced()
        throws RemoteException
      {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        try
        {
          localParcel1.writeInterfaceToken("com.google.tts.ITtsBeta");
          this.mRemote.transact(20, localParcel1, localParcel2, 0);
          localParcel2.readException();
          int i = localParcel2.readInt();
          boolean bool = false;
          if (i != 0)
            bool = true;
          return bool;
        }
        finally
        {
          localParcel2.recycle();
          localParcel1.recycle();
        }
      }

      public IBinder asBinder()
      {
        return this.mRemote;
      }

      public String getDefaultEngine()
        throws RemoteException
      {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        try
        {
          localParcel1.writeInterfaceToken("com.google.tts.ITtsBeta");
          this.mRemote.transact(19, localParcel1, localParcel2, 0);
          localParcel2.readException();
          String str = localParcel2.readString();
          return str;
        }
        finally
        {
          localParcel2.recycle();
          localParcel1.recycle();
        }
      }

      public String getInterfaceDescriptor()
      {
        return "com.google.tts.ITtsBeta";
      }

      public String[] getLanguage()
        throws RemoteException
      {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        try
        {
          localParcel1.writeInterfaceToken("com.google.tts.ITtsBeta");
          this.mRemote.transact(8, localParcel1, localParcel2, 0);
          localParcel2.readException();
          String[] arrayOfString = localParcel2.createStringArray();
          return arrayOfString;
        }
        finally
        {
          localParcel2.recycle();
          localParcel1.recycle();
        }
      }

      public int isLanguageAvailable(String paramString1, String paramString2, String paramString3, String[] paramArrayOfString)
        throws RemoteException
      {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        try
        {
          localParcel1.writeInterfaceToken("com.google.tts.ITtsBeta");
          localParcel1.writeString(paramString1);
          localParcel1.writeString(paramString2);
          localParcel1.writeString(paramString3);
          localParcel1.writeStringArray(paramArrayOfString);
          this.mRemote.transact(9, localParcel1, localParcel2, 0);
          localParcel2.readException();
          int i = localParcel2.readInt();
          return i;
        }
        finally
        {
          localParcel2.recycle();
          localParcel1.recycle();
        }
      }

      public boolean isSpeaking()
        throws RemoteException
      {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        try
        {
          localParcel1.writeInterfaceToken("com.google.tts.ITtsBeta");
          this.mRemote.transact(4, localParcel1, localParcel2, 0);
          localParcel2.readException();
          int i = localParcel2.readInt();
          boolean bool = false;
          if (i != 0)
            bool = true;
          return bool;
        }
        finally
        {
          localParcel2.recycle();
          localParcel1.recycle();
        }
      }

      public int playEarcon(String paramString1, String paramString2, int paramInt, String[] paramArrayOfString)
        throws RemoteException
      {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        try
        {
          localParcel1.writeInterfaceToken("com.google.tts.ITtsBeta");
          localParcel1.writeString(paramString1);
          localParcel1.writeString(paramString2);
          localParcel1.writeInt(paramInt);
          localParcel1.writeStringArray(paramArrayOfString);
          this.mRemote.transact(12, localParcel1, localParcel2, 0);
          localParcel2.readException();
          int i = localParcel2.readInt();
          return i;
        }
        finally
        {
          localParcel2.recycle();
          localParcel1.recycle();
        }
      }

      public int playSilence(String paramString, long paramLong, int paramInt, String[] paramArrayOfString)
        throws RemoteException
      {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        try
        {
          localParcel1.writeInterfaceToken("com.google.tts.ITtsBeta");
          localParcel1.writeString(paramString);
          localParcel1.writeLong(paramLong);
          localParcel1.writeInt(paramInt);
          localParcel1.writeStringArray(paramArrayOfString);
          this.mRemote.transact(17, localParcel1, localParcel2, 0);
          localParcel2.readException();
          int i = localParcel2.readInt();
          return i;
        }
        finally
        {
          localParcel2.recycle();
          localParcel1.recycle();
        }
      }

      public int registerCallback(String paramString, ITtsCallbackBeta paramITtsCallbackBeta)
        throws RemoteException
      {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        try
        {
          localParcel1.writeInterfaceToken("com.google.tts.ITtsBeta");
          localParcel1.writeString(paramString);
          if (paramITtsCallbackBeta != null);
          for (IBinder localIBinder = paramITtsCallbackBeta.asBinder(); ; localIBinder = null)
          {
            localParcel1.writeStrongBinder(localIBinder);
            this.mRemote.transact(15, localParcel1, localParcel2, 0);
            localParcel2.readException();
            int i = localParcel2.readInt();
            return i;
          }
        }
        finally
        {
          localParcel2.recycle();
          localParcel1.recycle();
        }
      }

      public int setEngineByPackageName(String paramString)
        throws RemoteException
      {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        try
        {
          localParcel1.writeInterfaceToken("com.google.tts.ITtsBeta");
          localParcel1.writeString(paramString);
          this.mRemote.transact(18, localParcel1, localParcel2, 0);
          localParcel2.readException();
          int i = localParcel2.readInt();
          return i;
        }
        finally
        {
          localParcel2.recycle();
          localParcel1.recycle();
        }
      }

      public int setLanguage(String paramString1, String paramString2, String paramString3, String paramString4)
        throws RemoteException
      {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        try
        {
          localParcel1.writeInterfaceToken("com.google.tts.ITtsBeta");
          localParcel1.writeString(paramString1);
          localParcel1.writeString(paramString2);
          localParcel1.writeString(paramString3);
          localParcel1.writeString(paramString4);
          this.mRemote.transact(10, localParcel1, localParcel2, 0);
          localParcel2.readException();
          int i = localParcel2.readInt();
          return i;
        }
        finally
        {
          localParcel2.recycle();
          localParcel1.recycle();
        }
      }

      public int setPitch(String paramString, int paramInt)
        throws RemoteException
      {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        try
        {
          localParcel1.writeInterfaceToken("com.google.tts.ITtsBeta");
          localParcel1.writeString(paramString);
          localParcel1.writeInt(paramInt);
          this.mRemote.transact(2, localParcel1, localParcel2, 0);
          localParcel2.readException();
          int i = localParcel2.readInt();
          return i;
        }
        finally
        {
          localParcel2.recycle();
          localParcel1.recycle();
        }
      }

      public int setSpeechRate(String paramString, int paramInt)
        throws RemoteException
      {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        try
        {
          localParcel1.writeInterfaceToken("com.google.tts.ITtsBeta");
          localParcel1.writeString(paramString);
          localParcel1.writeInt(paramInt);
          this.mRemote.transact(1, localParcel1, localParcel2, 0);
          localParcel2.readException();
          int i = localParcel2.readInt();
          return i;
        }
        finally
        {
          localParcel2.recycle();
          localParcel1.recycle();
        }
      }

      public int speak(String paramString1, String paramString2, int paramInt, String[] paramArrayOfString)
        throws RemoteException
      {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        try
        {
          localParcel1.writeInterfaceToken("com.google.tts.ITtsBeta");
          localParcel1.writeString(paramString1);
          localParcel1.writeString(paramString2);
          localParcel1.writeInt(paramInt);
          localParcel1.writeStringArray(paramArrayOfString);
          this.mRemote.transact(3, localParcel1, localParcel2, 0);
          localParcel2.readException();
          int i = localParcel2.readInt();
          return i;
        }
        finally
        {
          localParcel2.recycle();
          localParcel1.recycle();
        }
      }

      public int stop(String paramString)
        throws RemoteException
      {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        try
        {
          localParcel1.writeInterfaceToken("com.google.tts.ITtsBeta");
          localParcel1.writeString(paramString);
          this.mRemote.transact(5, localParcel1, localParcel2, 0);
          localParcel2.readException();
          int i = localParcel2.readInt();
          return i;
        }
        finally
        {
          localParcel2.recycle();
          localParcel1.recycle();
        }
      }

      public boolean synthesizeToFile(String paramString1, String paramString2, String[] paramArrayOfString, String paramString3)
        throws RemoteException
      {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        try
        {
          localParcel1.writeInterfaceToken("com.google.tts.ITtsBeta");
          localParcel1.writeString(paramString1);
          localParcel1.writeString(paramString2);
          localParcel1.writeStringArray(paramArrayOfString);
          localParcel1.writeString(paramString3);
          this.mRemote.transact(11, localParcel1, localParcel2, 0);
          localParcel2.readException();
          int i = localParcel2.readInt();
          boolean bool = false;
          if (i != 0)
            bool = true;
          return bool;
        }
        finally
        {
          localParcel2.recycle();
          localParcel1.recycle();
        }
      }

      public int unregisterCallback(String paramString, ITtsCallbackBeta paramITtsCallbackBeta)
        throws RemoteException
      {
        Parcel localParcel1 = Parcel.obtain();
        Parcel localParcel2 = Parcel.obtain();
        try
        {
          localParcel1.writeInterfaceToken("com.google.tts.ITtsBeta");
          localParcel1.writeString(paramString);
          if (paramITtsCallbackBeta != null);
          for (IBinder localIBinder = paramITtsCallbackBeta.asBinder(); ; localIBinder = null)
          {
            localParcel1.writeStrongBinder(localIBinder);
            this.mRemote.transact(16, localParcel1, localParcel2, 0);
            localParcel2.readException();
            int i = localParcel2.readInt();
            return i;
          }
        }
        finally
        {
          localParcel2.recycle();
          localParcel1.recycle();
        }
      }
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.tts.ITtsBeta
 * JD-Core Version:    0.6.2
 */