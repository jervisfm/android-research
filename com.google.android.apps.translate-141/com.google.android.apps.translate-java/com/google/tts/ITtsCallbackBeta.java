package com.google.tts;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public abstract interface ITtsCallbackBeta extends IInterface
{
  public abstract void utteranceCompleted(String paramString)
    throws RemoteException;

  public static abstract class Stub extends Binder
    implements ITtsCallbackBeta
  {
    private static final String DESCRIPTOR = "com.google.tts.ITtsCallbackBeta";
    static final int TRANSACTION_utteranceCompleted = 1;

    public Stub()
    {
      attachInterface(this, "com.google.tts.ITtsCallbackBeta");
    }

    public static ITtsCallbackBeta asInterface(IBinder paramIBinder)
    {
      if (paramIBinder == null)
        return null;
      IInterface localIInterface = paramIBinder.queryLocalInterface("com.google.tts.ITtsCallbackBeta");
      if ((localIInterface != null) && ((localIInterface instanceof ITtsCallbackBeta)))
        return (ITtsCallbackBeta)localIInterface;
      return new Proxy(paramIBinder);
    }

    public IBinder asBinder()
    {
      return this;
    }

    public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
      throws RemoteException
    {
      switch (paramInt1)
      {
      default:
        return super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
      case 1598968902:
        paramParcel2.writeString("com.google.tts.ITtsCallbackBeta");
        return true;
      case 1:
      }
      paramParcel1.enforceInterface("com.google.tts.ITtsCallbackBeta");
      utteranceCompleted(paramParcel1.readString());
      return true;
    }

    private static class Proxy
      implements ITtsCallbackBeta
    {
      private IBinder mRemote;

      Proxy(IBinder paramIBinder)
      {
        this.mRemote = paramIBinder;
      }

      public IBinder asBinder()
      {
        return this.mRemote;
      }

      public String getInterfaceDescriptor()
      {
        return "com.google.tts.ITtsCallbackBeta";
      }

      public void utteranceCompleted(String paramString)
        throws RemoteException
      {
        Parcel localParcel = Parcel.obtain();
        try
        {
          localParcel.writeInterfaceToken("com.google.tts.ITtsCallbackBeta");
          localParcel.writeString(paramString);
          this.mRemote.transact(1, localParcel, null, 1);
          return;
        }
        finally
        {
          localParcel.recycle();
        }
      }
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.tts.ITtsCallbackBeta
 * JD-Core Version:    0.6.2
 */