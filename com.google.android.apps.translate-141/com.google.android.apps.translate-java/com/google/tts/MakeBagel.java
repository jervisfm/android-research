package com.google.tts;

import android.app.Activity;
import android.content.Intent;
import java.util.Locale;

public class MakeBagel extends Activity
{
  TTS.SpeechCompletedListener completionListener = new TTS.SpeechCompletedListener()
  {
    public void onSpeechCompleted()
    {
      MakeBagel.this.mTts.shutdown();
      MakeBagel.this.self.setResult(-1);
      MakeBagel.this.finish();
    }
  };
  private String langRegionString;
  private TTS mTts;
  private String message;
  private MakeBagel self;

  protected void onDestroy()
  {
    super.onDestroy();
    if (this.mTts != null)
      this.mTts.shutdown();
  }

  protected void onResume()
  {
    super.onResume();
    this.self = this;
    this.self.setResult(0);
    this.message = getIntent().getStringExtra("message");
    Locale localLocale = new Locale(getIntent().getStringExtra("language"), getIntent().getStringExtra("country"), getIntent().getStringExtra("variant"));
    String str1 = localLocale.getISO3Language();
    String str2 = localLocale.getISO3Country();
    this.langRegionString = "";
    if (str1.length() == 3)
      if (str2.length() != 3)
        break label185;
    label185: for (this.langRegionString = (str1 + "-" + str2); ; this.langRegionString = str1)
    {
      this.mTts = new TTS(this, new TTS.InitListener()
      {
        public void onInit(int paramAnonymousInt)
        {
          MakeBagel.this.mTts.setOnSpeechCompletedListener(MakeBagel.this.completionListener);
          if (MakeBagel.this.langRegionString.length() > 0)
            MakeBagel.this.mTts.setLanguage(MakeBagel.this.langRegionString);
          MakeBagel.this.mTts.speak(MakeBagel.this.message, 0, null);
        }
      }
      , true);
      return;
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.tts.MakeBagel
 * JD-Core Version:    0.6.2
 */