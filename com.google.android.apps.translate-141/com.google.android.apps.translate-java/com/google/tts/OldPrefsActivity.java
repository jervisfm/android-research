package com.google.tts;

import android.content.ComponentName;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import java.util.HashMap;
import java.util.List;

public class OldPrefsActivity extends PreferenceActivity
{
  private static final int TTS_VOICE_DATA_CHECK_CODE = 42;
  private static final int TTS_VOICE_DATA_INSTALL_CODE = 43;
  private HashMap<String, Integer> hellos;
  private TTS myTts;
  private TTS.InitListener ttsInitListener = new TTS.InitListener()
  {
    public void onInit(int paramAnonymousInt)
    {
      OldPrefsActivity.this.addPreferencesFromResource(2130968576);
      OldPrefsActivity.this.loadEngines();
      OldPrefsActivity.this.loadHellos();
      OldPrefsActivity.this.findPreference("preview").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener()
      {
        public boolean onPreferenceClick(Preference paramAnonymous2Preference)
        {
          OldPrefsActivity.this.sayHello();
          return true;
        }
      });
    }
  };

  private void loadEngines()
  {
    ListPreference localListPreference = (ListPreference)findPreference("engine_pref");
    Intent localIntent = new Intent("android.intent.action.START_TTS_ENGINE");
    ResolveInfo[] arrayOfResolveInfo1 = new ResolveInfo[0];
    PackageManager localPackageManager = getPackageManager();
    ResolveInfo[] arrayOfResolveInfo2 = (ResolveInfo[])localPackageManager.queryIntentActivities(localIntent, 0).toArray(arrayOfResolveInfo1);
    CharSequence[] arrayOfCharSequence1 = new CharSequence[arrayOfResolveInfo2.length];
    CharSequence[] arrayOfCharSequence2 = new CharSequence[arrayOfResolveInfo2.length];
    for (int i = 0; ; i++)
    {
      if (i >= arrayOfResolveInfo2.length)
      {
        localListPreference.setEntries(arrayOfCharSequence1);
        localListPreference.setEntryValues(arrayOfCharSequence2);
        localListPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener()
        {
          public boolean onPreferenceChange(Preference paramAnonymousPreference, Object paramAnonymousObject)
          {
            if (paramAnonymousObject.toString().equals("com.svox.pico"))
            {
              Intent localIntent = new Intent();
              localIntent.setAction("android.speech.tts.engine.CHECK_TTS_DATA");
              localIntent.setClassName("com.svox.pico", "com.svox.pico.CheckVoiceData");
              OldPrefsActivity.this.startActivityForResult(localIntent, 42);
            }
            return true;
          }
        });
        return;
      }
      arrayOfCharSequence1[i] = arrayOfResolveInfo2[i].loadLabel(localPackageManager);
      arrayOfCharSequence2[i] = arrayOfResolveInfo2[i].activityInfo.packageName;
    }
  }

  private void loadHellos()
  {
    this.hellos = new HashMap();
    this.hellos.put("afr", Integer.valueOf(2131165250));
    this.hellos.put("bos", Integer.valueOf(2131165251));
    this.hellos.put("yue", Integer.valueOf(2131165252));
    this.hellos.put("cmn", Integer.valueOf(2131165253));
    this.hellos.put("hrv", Integer.valueOf(2131165254));
    this.hellos.put("ces", Integer.valueOf(2131165255));
    this.hellos.put("nld", Integer.valueOf(2131165256));
    this.hellos.put("eng-USA", Integer.valueOf(2131165257));
    this.hellos.put("eng-GBR", Integer.valueOf(2131165258));
    this.hellos.put("epo", Integer.valueOf(2131165259));
    this.hellos.put("fin", Integer.valueOf(2131165260));
    this.hellos.put("fra", Integer.valueOf(2131165261));
    this.hellos.put("deu", Integer.valueOf(2131165262));
    this.hellos.put("ell", Integer.valueOf(2131165263));
    this.hellos.put("hin", Integer.valueOf(2131165264));
    this.hellos.put("hun", Integer.valueOf(2131165265));
    this.hellos.put("isl", Integer.valueOf(2131165266));
    this.hellos.put("ind", Integer.valueOf(2131165267));
    this.hellos.put("ita", Integer.valueOf(2131165268));
    this.hellos.put("kur", Integer.valueOf(2131165269));
    this.hellos.put("lat", Integer.valueOf(2131165270));
    this.hellos.put("mkd", Integer.valueOf(2131165271));
    this.hellos.put("nor", Integer.valueOf(2131165272));
    this.hellos.put("pol", Integer.valueOf(2131165273));
    this.hellos.put("por", Integer.valueOf(2131165274));
    this.hellos.put("ron", Integer.valueOf(2131165275));
    this.hellos.put("rus", Integer.valueOf(2131165276));
    this.hellos.put("srp", Integer.valueOf(2131165277));
    this.hellos.put("slk", Integer.valueOf(2131165278));
    this.hellos.put("spa", Integer.valueOf(2131165279));
    this.hellos.put("spa-MEX", Integer.valueOf(2131165280));
    this.hellos.put("swe", Integer.valueOf(2131165281));
    this.hellos.put("swe", Integer.valueOf(2131165282));
    this.hellos.put("tam", Integer.valueOf(2131165283));
    this.hellos.put("tur", Integer.valueOf(2131165284));
    this.hellos.put("vie", Integer.valueOf(2131165285));
    this.hellos.put("cym", Integer.valueOf(2131165286));
  }

  private void sayHello()
  {
    SharedPreferences localSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
    String str1 = localSharedPreferences.getString("engine_pref", "com.svox.pico");
    this.myTts.setEngine(str1);
    String str2 = localSharedPreferences.getString("lang_pref", "eng-USA");
    int i = Integer.parseInt(localSharedPreferences.getString("rate_pref", "140"));
    this.myTts.setLanguage(str2);
    this.myTts.setSpeechRate(i);
    if (!this.hellos.containsKey(str2))
      str2 = "eng-USA";
    String str3 = getString(((Integer)this.hellos.get(str2)).intValue());
    this.myTts.speak(str3, 0, null);
  }

  public void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    if ((paramInt1 == 42) && (paramInt2 != 1))
    {
      Intent localIntent = new Intent();
      localIntent.setAction("android.speech.tts.engine.INSTALL_TTS_DATA");
      localIntent.setClassName("com.svox.pico", "com.svox.pico.DownloadVoiceData");
      startActivityForResult(localIntent, 43);
    }
  }

  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    setVolumeControlStream(3);
    this.myTts = new TTS(this, this.ttsInitListener, true);
  }

  public boolean onCreateOptionsMenu(Menu paramMenu)
  {
    paramMenu.add(0, 2131165185, 0, 2131165185).setIcon(17301583);
    paramMenu.add(0, 2131165186, 0, 2131165186).setIcon(17301569);
    return super.onCreateOptionsMenu(paramMenu);
  }

  protected void onDestroy()
  {
    if (this.myTts != null)
      this.myTts.shutdown();
    super.onDestroy();
  }

  public boolean onOptionsItemSelected(MenuItem paramMenuItem)
  {
    Intent localIntent = new Intent();
    localIntent.setComponent(new ComponentName("com.android.browser", "com.android.browser.BrowserActivity"));
    localIntent.setAction("android.intent.action.VIEW");
    localIntent.addCategory("android.intent.category.BROWSABLE");
    switch (paramMenuItem.getItemId())
    {
    default:
    case 2131165185:
    case 2131165186:
    }
    while (true)
    {
      return super.onOptionsItemSelected(paramMenuItem);
      localIntent.setData(Uri.parse("http://eyes-free.googlecode.com/svn/trunk/documentation/tts_apps.html"));
      startActivity(localIntent);
      continue;
      localIntent.setData(Uri.parse("http://eyes-free.googlecode.com/"));
      startActivity(localIntent);
    }
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.tts.OldPrefsActivity
 * JD-Core Version:    0.6.2
 */