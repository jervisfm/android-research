package com.google.tts;

public final class R
{
  public static final class array
  {
    public static final int language_entries = 2131099655;
    public static final int language_values = 2131099656;
    public static final int rate_entries = 2131099657;
    public static final int rate_values = 2131099658;
    public static final int tts_demo_strings = 2131099652;
    public static final int tts_engine_entries = 2131099653;
    public static final int tts_engine_values = 2131099654;
    public static final int tts_lang_entries = 2131099648;
    public static final int tts_lang_values = 2131099649;
    public static final int tts_rate_entries = 2131099650;
    public static final int tts_rate_values = 2131099651;
  }

  public static final class attr
  {
  }

  public static final class drawable
  {
    public static final int tts = 2130837504;
  }

  public static final class id
  {
    public static final int progress_indicator = 2131230721;
    public static final int root = 2131230720;
  }

  public static final class layout
  {
    public static final int downloading = 2130903040;
  }

  public static final class raw
  {
    public static final int a = 2131034112;
    public static final int a_fem_ = 2131034113;
    public static final int a_robot_ = 2131034114;
    public static final int asterisk = 2131034115;
    public static final int asterisk_fem_ = 2131034116;
    public static final int asterisk_robot_ = 2131034117;
    public static final int b = 2131034118;
    public static final int b_fem_ = 2131034119;
    public static final int b_robot_ = 2131034120;
    public static final int c = 2131034121;
    public static final int c_fem_ = 2131034122;
    public static final int c_robot_ = 2131034123;
    public static final int cancel_snd = 2131034124;
    public static final int comma = 2131034125;
    public static final int comma_fem_ = 2131034126;
    public static final int comma_robot_ = 2131034127;
    public static final int d = 2131034128;
    public static final int d_fem_ = 2131034129;
    public static final int d_robot_ = 2131034130;
    public static final int dot = 2131034131;
    public static final int dot_fem_ = 2131034132;
    public static final int dot_robot_ = 2131034133;
    public static final int e = 2131034134;
    public static final int e_fem_ = 2131034135;
    public static final int e_robot_ = 2131034136;
    public static final int exclamation = 2131034137;
    public static final int exclamation_fem_ = 2131034138;
    public static final int exclamation_robot_ = 2131034139;
    public static final int f = 2131034140;
    public static final int f_fem_ = 2131034141;
    public static final int f_robot_ = 2131034142;
    public static final int g = 2131034143;
    public static final int g_fem_ = 2131034144;
    public static final int g_robot_ = 2131034145;
    public static final int h = 2131034146;
    public static final int h_fem_ = 2131034147;
    public static final int h_robot_ = 2131034148;
    public static final int hundred = 2131034149;
    public static final int hundred_fem_ = 2131034150;
    public static final int hundred_robot_ = 2131034151;
    public static final int i = 2131034152;
    public static final int i_fem_ = 2131034153;
    public static final int i_robot_ = 2131034154;
    public static final int j = 2131034155;
    public static final int j_fem_ = 2131034156;
    public static final int j_robot_ = 2131034157;
    public static final int k = 2131034158;
    public static final int k_fem_ = 2131034159;
    public static final int k_robot_ = 2131034160;
    public static final int l = 2131034161;
    public static final int l_fem_ = 2131034162;
    public static final int l_robot_ = 2131034163;
    public static final int m = 2131034164;
    public static final int m_fem_ = 2131034165;
    public static final int m_robot_ = 2131034166;
    public static final int mode = 2131034167;
    public static final int mode_fem_ = 2131034168;
    public static final int mode_robot_ = 2131034169;
    public static final int n = 2131034170;
    public static final int n_fem_ = 2131034171;
    public static final int n_robot_ = 2131034172;
    public static final int num0 = 2131034173;
    public static final int num0_fem_ = 2131034174;
    public static final int num0_robot_ = 2131034175;
    public static final int num1 = 2131034176;
    public static final int num10 = 2131034177;
    public static final int num10_fem_ = 2131034178;
    public static final int num10_robot_ = 2131034179;
    public static final int num11 = 2131034180;
    public static final int num11_fem_ = 2131034181;
    public static final int num11_robot_ = 2131034182;
    public static final int num12 = 2131034183;
    public static final int num12_fem_ = 2131034184;
    public static final int num12_robot_ = 2131034185;
    public static final int num13 = 2131034186;
    public static final int num13_fem_ = 2131034187;
    public static final int num13_robot_ = 2131034188;
    public static final int num14 = 2131034189;
    public static final int num14_fem_ = 2131034190;
    public static final int num14_robot_ = 2131034191;
    public static final int num15 = 2131034192;
    public static final int num15_fem_ = 2131034193;
    public static final int num15_robot_ = 2131034194;
    public static final int num16 = 2131034195;
    public static final int num16_fem_ = 2131034196;
    public static final int num16_robot_ = 2131034197;
    public static final int num17 = 2131034198;
    public static final int num17_fem_ = 2131034199;
    public static final int num17_robot_ = 2131034200;
    public static final int num18 = 2131034201;
    public static final int num18_fem_ = 2131034202;
    public static final int num18_robot_ = 2131034203;
    public static final int num19 = 2131034204;
    public static final int num19_fem_ = 2131034205;
    public static final int num19_robot_ = 2131034206;
    public static final int num1_fem_ = 2131034207;
    public static final int num1_robot_ = 2131034208;
    public static final int num2 = 2131034209;
    public static final int num20 = 2131034210;
    public static final int num20_fem_ = 2131034211;
    public static final int num20_robot_ = 2131034212;
    public static final int num2_fem_ = 2131034213;
    public static final int num2_robot_ = 2131034214;
    public static final int num3 = 2131034215;
    public static final int num30 = 2131034216;
    public static final int num30_fem_ = 2131034217;
    public static final int num30_robot_ = 2131034218;
    public static final int num3_fem_ = 2131034219;
    public static final int num3_robot_ = 2131034220;
    public static final int num4 = 2131034221;
    public static final int num40 = 2131034222;
    public static final int num40_fem_ = 2131034223;
    public static final int num40_robot_ = 2131034224;
    public static final int num4_fem_ = 2131034225;
    public static final int num4_robot_ = 2131034226;
    public static final int num5 = 2131034227;
    public static final int num50 = 2131034228;
    public static final int num50_fem_ = 2131034229;
    public static final int num50_robot_ = 2131034230;
    public static final int num5_fem_ = 2131034231;
    public static final int num5_robot_ = 2131034232;
    public static final int num6 = 2131034233;
    public static final int num60 = 2131034234;
    public static final int num60_fem_ = 2131034235;
    public static final int num60_robot_ = 2131034236;
    public static final int num6_fem_ = 2131034237;
    public static final int num6_robot_ = 2131034238;
    public static final int num7 = 2131034239;
    public static final int num70 = 2131034240;
    public static final int num70_fem_ = 2131034241;
    public static final int num70_robot_ = 2131034242;
    public static final int num7_fem_ = 2131034243;
    public static final int num7_robot_ = 2131034244;
    public static final int num8 = 2131034245;
    public static final int num80 = 2131034246;
    public static final int num80_fem_ = 2131034247;
    public static final int num80_robot_ = 2131034248;
    public static final int num8_fem_ = 2131034249;
    public static final int num8_robot_ = 2131034250;
    public static final int num9 = 2131034251;
    public static final int num90 = 2131034252;
    public static final int num90_fem_ = 2131034253;
    public static final int num90_robot_ = 2131034254;
    public static final int num9_fem_ = 2131034255;
    public static final int num9_robot_ = 2131034256;
    public static final int o = 2131034257;
    public static final int o_fem_ = 2131034258;
    public static final int o_robot_ = 2131034259;
    public static final int p = 2131034260;
    public static final int p_fem_ = 2131034261;
    public static final int p_robot_ = 2131034262;
    public static final int percent = 2131034263;
    public static final int percent_fem_ = 2131034264;
    public static final int percent_robot_ = 2131034265;
    public static final int pound = 2131034266;
    public static final int pound_fem_ = 2131034267;
    public static final int pound_robot_ = 2131034268;
    public static final int q = 2131034269;
    public static final int q_fem_ = 2131034270;
    public static final int q_robot_ = 2131034271;
    public static final int question = 2131034272;
    public static final int question_fem_ = 2131034273;
    public static final int question_robot_ = 2131034274;
    public static final int r = 2131034275;
    public static final int r_fem_ = 2131034276;
    public static final int r_robot_ = 2131034277;
    public static final int s = 2131034278;
    public static final int s_fem_ = 2131034279;
    public static final int s_robot_ = 2131034280;
    public static final int slnc_snd = 2131034281;
    public static final int soundsamples = 2131034282;
    public static final int space = 2131034283;
    public static final int space_fem_ = 2131034284;
    public static final int space_robot_ = 2131034285;
    public static final int t = 2131034286;
    public static final int t_fem_ = 2131034287;
    public static final int t_robot_ = 2131034288;
    public static final int tick_snd = 2131034289;
    public static final int tock_snd = 2131034290;
    public static final int u = 2131034291;
    public static final int u_fem_ = 2131034292;
    public static final int u_robot_ = 2131034293;
    public static final int v = 2131034294;
    public static final int v_fem_ = 2131034295;
    public static final int v_robot_ = 2131034296;
    public static final int w = 2131034297;
    public static final int w_fem_ = 2131034298;
    public static final int w_robot_ = 2131034299;
    public static final int x = 2131034300;
    public static final int x_fem_ = 2131034301;
    public static final int x_robot_ = 2131034302;
    public static final int y = 2131034303;
    public static final int y_fem_ = 2131034304;
    public static final int y_robot_ = 2131034305;
    public static final int z = 2131034306;
    public static final int z_fem_ = 2131034307;
    public static final int z_robot_ = 2131034308;
  }

  public static final class string
  {
    public static final int af = 2131165250;
    public static final int afr = 2131165216;
    public static final int app_name = 2131165184;
    public static final int bos = 2131165217;
    public static final int bs = 2131165251;
    public static final int ces = 2131165220;
    public static final int cy = 2131165286;
    public static final int cym = 2131165249;
    public static final int cz = 2131165255;
    public static final int de = 2131165262;
    public static final int deu = 2131165226;
    public static final int el = 2131165263;
    public static final int ell = 2131165227;
    public static final int eng = 2131165222;
    public static final int enrGB = 2131165258;
    public static final int enrUS = 2131165257;
    public static final int eo = 2131165259;
    public static final int epo = 2131165223;
    public static final int es = 2131165279;
    public static final int espeak_name = 2131165215;
    public static final int esrMX = 2131165280;
    public static final int fi = 2131165260;
    public static final int fin = 2131165224;
    public static final int fr = 2131165261;
    public static final int fra = 2131165225;
    public static final int hi = 2131165264;
    public static final int hin = 2131165228;
    public static final int homepage = 2131165186;
    public static final int hr = 2131165254;
    public static final int hrv = 2131165219;
    public static final int hu = 2131165265;
    public static final int hun = 2131165229;
    public static final int id = 2131165267;
    public static final int ind = 2131165231;
    public static final int is = 2131165266;
    public static final int isl = 2131165230;
    public static final int it = 2131165268;
    public static final int ita = 2131165232;
    public static final int ku = 2131165269;
    public static final int kur = 2131165233;
    public static final int la = 2131165270;
    public static final int lat = 2131165234;
    public static final int mk = 2131165271;
    public static final int mkd = 2131165235;
    public static final int nl = 2131165256;
    public static final int nld = 2131165221;
    public static final int no = 2131165272;
    public static final int nor = 2131165236;
    public static final int pl = 2131165273;
    public static final int pol = 2131165237;
    public static final int por = 2131165238;
    public static final int pt = 2131165274;
    public static final int ro = 2131165275;
    public static final int ron = 2131165239;
    public static final int ru = 2131165276;
    public static final int rus = 2131165240;
    public static final int sk = 2131165278;
    public static final int slk = 2131165242;
    public static final int spa = 2131165243;
    public static final int sr = 2131165277;
    public static final int srp = 2131165241;
    public static final int sv = 2131165282;
    public static final int sw = 2131165281;
    public static final int swa = 2131165244;
    public static final int swe = 2131165245;
    public static final int ta = 2131165283;
    public static final int tam = 2131165246;
    public static final int tr = 2131165284;
    public static final int tts_apps = 2131165185;
    public static final int tts_data_installed_summary = 2131165204;
    public static final int tts_default_lang_summary = 2131165199;
    public static final int tts_default_lang_title = 2131165198;
    public static final int tts_default_pitch_summary = 2131165197;
    public static final int tts_default_pitch_title = 2131165196;
    public static final int tts_default_rate_summary = 2131165195;
    public static final int tts_default_rate_title = 2131165194;
    public static final int tts_default_settings_section = 2131165191;
    public static final int tts_default_synth_summary = 2131165193;
    public static final int tts_default_synth_title = 2131165192;
    public static final int tts_demo = 2131165205;
    public static final int tts_engine_error = 2131165207;
    public static final int tts_engine_error_config = 2131165208;
    public static final int tts_engine_error_reselect = 2131165209;
    public static final int tts_engine_name_is_disabled_summary = 2131165214;
    public static final int tts_engine_name_is_enabled_summary = 2131165213;
    public static final int tts_engine_name_settings = 2131165212;
    public static final int tts_engine_security_warning = 2131165210;
    public static final int tts_engines_section = 2131165211;
    public static final int tts_install_data_summary = 2131165203;
    public static final int tts_install_data_title = 2131165202;
    public static final int tts_play_example_summary = 2131165201;
    public static final int tts_play_example_title = 2131165200;
    public static final int tts_settings = 2131165187;
    public static final int tts_settings_changed_demo = 2131165206;
    public static final int tts_settings_title = 2131165188;
    public static final int tur = 2131165247;
    public static final int use_default_tts_settings_summary = 2131165190;
    public static final int use_default_tts_settings_title = 2131165189;
    public static final int vi = 2131165285;
    public static final int vie = 2131165248;
    public static final int zh = 2131165253;
    public static final int zho = 2131165218;
    public static final int zhrHK = 2131165252;
  }

  public static final class xml
  {
    public static final int oldprefs = 2130968576;
    public static final int tts_settings = 2130968577;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.tts.R
 * JD-Core Version:    0.6.2
 */