package com.google.tts;

public class SoundResource
{
  public String filename;
  public int resId;
  public String sourcePackageName;

  public SoundResource(String paramString)
  {
    this.sourcePackageName = null;
    this.resId = -1;
    this.filename = paramString;
  }

  public SoundResource(String paramString, int paramInt)
  {
    this.sourcePackageName = paramString;
    this.resId = paramInt;
    this.filename = null;
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.tts.SoundResource
 * JD-Core Version:    0.6.2
 */