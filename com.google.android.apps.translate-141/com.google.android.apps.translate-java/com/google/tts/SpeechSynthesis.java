package com.google.tts;

import android.util.Log;
import java.lang.ref.WeakReference;

public class SpeechSynthesis
{
  private static final String TAG = "SpeechSynthesis";
  private int mNativeContext;

  static
  {
    System.loadLibrary("speechsynthesis");
  }

  public SpeechSynthesis(String paramString)
  {
    native_setup(new WeakReference(this), paramString);
  }

  private final native void native_finalize();

  private final native void native_setup(Object paramObject, String paramString);

  private static void postNativeSpeechSynthesizedInJava(Object paramObject, int paramInt1, int paramInt2)
  {
    Log.i("TTS plugin debug", "bufferPointer: " + paramInt1 + " bufferSize: " + paramInt2);
    ((SpeechSynthesis)((WeakReference)paramObject).get()).playAudioBuffer(paramInt1, paramInt2);
  }

  protected void finalize()
  {
    native_finalize();
  }

  public final native String getLanguage();

  public final native int getRate();

  public final native void playAudioBuffer(int paramInt1, int paramInt2);

  public final native void setLanguage(String paramString);

  public final native void setSpeechRate(int paramInt);

  public final native void shutdown();

  public final native void speak(String paramString);

  public final native void stop();

  public final native void synthesizeToFile(String paramString1, String paramString2);
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.tts.SpeechSynthesis
 * JD-Core Version:    0.6.2
 */