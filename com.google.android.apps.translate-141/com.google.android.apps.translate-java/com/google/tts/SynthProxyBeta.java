package com.google.tts;

import android.os.Build.VERSION;
import android.util.Log;
import java.lang.ref.WeakReference;

public class SynthProxyBeta
{
  private static final String TAG = "SynthProxy";
  private int mJniData = 0;

  static
  {
    String str = "ttssynthproxybeta";
    int i = 4;
    try
    {
      int j = Integer.parseInt(Build.VERSION.SDK);
      i = j;
      if (i > 4)
        str = "ttssynthproxybeta_eclair";
      System.loadLibrary(str);
      return;
    }
    catch (NumberFormatException localNumberFormatException)
    {
      while (true)
        Log.e("SynthProxyBeta", "Unable to parse SDK version: " + Build.VERSION.SDK);
    }
  }

  public SynthProxyBeta(String paramString)
  {
    Log.e("TTS is loading", paramString);
    native_setup(new WeakReference(this), paramString);
  }

  private final native void native_finalize(int paramInt);

  private final native String[] native_getLanguage(int paramInt);

  private final native int native_getRate(int paramInt);

  private final native int native_isLanguageAvailable(int paramInt, String paramString1, String paramString2, String paramString3);

  private final native int native_loadLanguage(int paramInt, String paramString1, String paramString2, String paramString3);

  private final native int native_setLanguage(int paramInt, String paramString1, String paramString2, String paramString3);

  private final native int native_setPitch(int paramInt1, int paramInt2);

  private final native int native_setSpeechRate(int paramInt1, int paramInt2);

  private final native void native_setup(Object paramObject, String paramString);

  private final native void native_shutdown(int paramInt);

  private final native int native_speak(int paramInt1, String paramString, int paramInt2);

  private final native int native_stop(int paramInt);

  private final native int native_stopSync(int paramInt);

  private final native int native_synthesizeToFile(int paramInt, String paramString1, String paramString2);

  private static void postNativeSpeechSynthesizedInJava(Object paramObject, int paramInt1, int paramInt2)
  {
    Log.i("TTS plugin debug", "bufferPointer: " + paramInt1 + " bufferSize: " + paramInt2);
    ((SynthProxyBeta)((WeakReference)paramObject).get());
  }

  protected void finalize()
  {
    native_finalize(this.mJniData);
    this.mJniData = 0;
  }

  public String[] getLanguage()
  {
    return native_getLanguage(this.mJniData);
  }

  public int getRate()
  {
    return native_getRate(this.mJniData);
  }

  public int isLanguageAvailable(String paramString1, String paramString2, String paramString3)
  {
    return native_isLanguageAvailable(this.mJniData, paramString1, paramString2, paramString3);
  }

  public int loadLanguage(String paramString1, String paramString2, String paramString3)
  {
    return native_loadLanguage(this.mJniData, paramString1, paramString2, paramString3);
  }

  public int setLanguage(String paramString1, String paramString2, String paramString3)
  {
    return native_setLanguage(this.mJniData, paramString1, paramString2, paramString3);
  }

  public final int setPitch(int paramInt)
  {
    return native_setPitch(this.mJniData, paramInt);
  }

  public final int setSpeechRate(int paramInt)
  {
    return native_setSpeechRate(this.mJniData, paramInt);
  }

  public void shutdown()
  {
    native_shutdown(this.mJniData);
  }

  public int speak(String paramString, int paramInt)
  {
    if ((paramInt > -1) && (paramInt < 5))
      return native_speak(this.mJniData, paramString, paramInt);
    Log.e("SynthProxy", "Trying to speak with invalid stream type " + paramInt);
    return native_speak(this.mJniData, paramString, 3);
  }

  public int stop()
  {
    return native_stop(this.mJniData);
  }

  public int synthesizeToFile(String paramString1, String paramString2)
  {
    return native_synthesizeToFile(this.mJniData, paramString1, paramString2);
  }
}

/* Location:           D:\Jervis\Documents\Programming\Research\Android\apks\com.google.android.apps.translate-141\classes_dex2jar.jar
 * Qualified Name:     com.google.tts.SynthProxyBeta
 * JD-Core Version:    0.6.2
 */